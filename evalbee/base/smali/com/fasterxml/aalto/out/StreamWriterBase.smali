.class public abstract Lcom/fasterxml/aalto/out/StreamWriterBase;
.super Lorg/codehaus/stax2/ri/Stax2WriterImpl;
.source "SourceFile"

# interfaces
.implements Ljavax/xml/namespace/NamespaceContext;
.implements Lorg/codehaus/stax2/validation/ValidationContext;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/fasterxml/aalto/out/StreamWriterBase$State;
    }
.end annotation


# static fields
.field static final MAX_POOL_SIZE:I = 0x8


# instance fields
.field protected final _cfgCDataAsText:Z

.field protected _cfgCheckAttrs:Z

.field protected _cfgCheckContent:Z

.field protected _cfgCheckStructure:Z

.field protected final _config:Lcom/fasterxml/aalto/out/WriterConfig;

.field protected _currElem:Lcom/fasterxml/aalto/out/OutputElement;

.field protected _dtdRootElemName:Ljava/lang/String;

.field protected _outputElemPool:Lcom/fasterxml/aalto/out/OutputElement;

.field protected _poolSize:I

.field protected _rootNsContext:Ljavax/xml/namespace/NamespaceContext;

.field protected _state:Lcom/fasterxml/aalto/out/StreamWriterBase$State;

.field protected _stateAnyOutput:Z

.field protected _stateEmptyElement:Z

.field protected _stateStartElementOpen:Z

.field protected _symbols:Lcom/fasterxml/aalto/out/WNameTable;

.field protected _validator:Lorg/codehaus/stax2/validation/XMLValidator;

.field protected _valueEncoderFactory:Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory;

.field protected _vldContent:I

.field protected _vldProblemHandler:Lorg/codehaus/stax2/validation/ValidationProblemHandler;

.field protected final _xmlWriter:Lcom/fasterxml/aalto/out/XmlWriter;


# direct methods
.method public constructor <init>(Lcom/fasterxml/aalto/out/WriterConfig;Lcom/fasterxml/aalto/out/XmlWriter;Lcom/fasterxml/aalto/out/WNameTable;)V
    .locals 2

    invoke-direct {p0}, Lorg/codehaus/stax2/ri/Stax2WriterImpl;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_validator:Lorg/codehaus/stax2/validation/XMLValidator;

    const/4 v1, 0x4

    iput v1, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_vldContent:I

    iput-object v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_vldProblemHandler:Lorg/codehaus/stax2/validation/ValidationProblemHandler;

    sget-object v1, Lcom/fasterxml/aalto/out/StreamWriterBase$State;->PROLOG:Lcom/fasterxml/aalto/out/StreamWriterBase$State;

    iput-object v1, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_state:Lcom/fasterxml/aalto/out/StreamWriterBase$State;

    invoke-static {}, Lcom/fasterxml/aalto/out/OutputElement;->createRoot()Lcom/fasterxml/aalto/out/OutputElement;

    move-result-object v1

    iput-object v1, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_currElem:Lcom/fasterxml/aalto/out/OutputElement;

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_stateAnyOutput:Z

    iput-boolean v1, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_stateStartElementOpen:Z

    iput-boolean v1, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_stateEmptyElement:Z

    iput-object v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_dtdRootElemName:Ljava/lang/String;

    iput-object v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_outputElemPool:Lcom/fasterxml/aalto/out/OutputElement;

    iput v1, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_poolSize:I

    iput-object p1, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_config:Lcom/fasterxml/aalto/out/WriterConfig;

    iput-object p2, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_xmlWriter:Lcom/fasterxml/aalto/out/XmlWriter;

    iput-object p3, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_symbols:Lcom/fasterxml/aalto/out/WNameTable;

    invoke-virtual {p1}, Lcom/fasterxml/aalto/out/WriterConfig;->willCheckStructure()Z

    move-result p2

    iput-boolean p2, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_cfgCheckStructure:Z

    invoke-virtual {p1}, Lcom/fasterxml/aalto/out/WriterConfig;->willCheckContent()Z

    move-result p2

    iput-boolean p2, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_cfgCheckContent:Z

    invoke-virtual {p1}, Lcom/fasterxml/aalto/out/WriterConfig;->willCheckAttributes()Z

    move-result p1

    iput-boolean p1, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_cfgCheckAttrs:Z

    iput-boolean v1, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_cfgCDataAsText:Z

    return-void
.end method

.method private final _finishDocument(Z)V
    .locals 2

    iget-object v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_state:Lcom/fasterxml/aalto/out/StreamWriterBase$State;

    sget-object v1, Lcom/fasterxml/aalto/out/StreamWriterBase$State;->EPILOG:Lcom/fasterxml/aalto/out/StreamWriterBase$State;

    if-eq v0, v1, :cond_2

    iget-boolean v1, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_cfgCheckStructure:Z

    if-eqz v1, :cond_0

    sget-object v1, Lcom/fasterxml/aalto/out/StreamWriterBase$State;->PROLOG:Lcom/fasterxml/aalto/out/StreamWriterBase$State;

    if-ne v0, v1, :cond_0

    sget-object v0, Lcom/fasterxml/aalto/impl/ErrorConsts;->WERR_PROLOG_NO_ROOT:Ljava/lang/String;

    invoke-static {v0}, Lcom/fasterxml/aalto/out/StreamWriterBase;->_reportNwfStructure(Ljava/lang/String;)V

    :cond_0
    iget-boolean v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_stateStartElementOpen:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_stateEmptyElement:Z

    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/out/StreamWriterBase;->_closeStartElement(Z)V

    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_state:Lcom/fasterxml/aalto/out/StreamWriterBase$State;

    sget-object v1, Lcom/fasterxml/aalto/out/StreamWriterBase$State;->EPILOG:Lcom/fasterxml/aalto/out/StreamWriterBase$State;

    if-eq v0, v1, :cond_2

    invoke-virtual {p0}, Lcom/fasterxml/aalto/out/StreamWriterBase;->writeEndElement()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_symbols:Lcom/fasterxml/aalto/out/WNameTable;

    invoke-virtual {v0}, Lcom/fasterxml/aalto/out/WNameTable;->maybeDirty()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_symbols:Lcom/fasterxml/aalto/out/WNameTable;

    invoke-virtual {v0}, Lcom/fasterxml/aalto/out/WNameTable;->mergeToParent()Z

    :cond_3
    :try_start_0
    iget-object v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_xmlWriter:Lcom/fasterxml/aalto/out/XmlWriter;

    invoke-virtual {v0, p1}, Lcom/fasterxml/aalto/out/XmlWriter;->close(Z)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    new-instance v0, Lcom/fasterxml/aalto/impl/IoStreamException;

    invoke-direct {v0, p1}, Lcom/fasterxml/aalto/impl/IoStreamException;-><init>(Ljava/io/IOException;)V

    throw v0
.end method

.method public static _reportNwfAttr(Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/fasterxml/aalto/out/StreamWriterBase;->throwOutputError(Ljava/lang/String;)V

    return-void
.end method

.method public static _reportNwfAttr(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 0

    .line 2
    invoke-static {p0, p1}, Lcom/fasterxml/aalto/out/StreamWriterBase;->throwOutputError(Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method

.method public static _reportNwfContent(Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/fasterxml/aalto/out/StreamWriterBase;->throwOutputError(Ljava/lang/String;)V

    return-void
.end method

.method public static _reportNwfContent(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 0

    .line 2
    invoke-static {p0, p1}, Lcom/fasterxml/aalto/out/StreamWriterBase;->throwOutputError(Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method

.method public static _reportNwfName(Ljava/lang/String;)V
    .locals 0

    invoke-static {p0}, Lcom/fasterxml/aalto/out/StreamWriterBase;->throwOutputError(Ljava/lang/String;)V

    return-void
.end method

.method public static _reportNwfStructure(Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lcom/fasterxml/aalto/out/StreamWriterBase;->throwOutputError(Ljava/lang/String;)V

    return-void
.end method

.method public static _reportNwfStructure(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 0

    .line 2
    invoke-static {p0, p1}, Lcom/fasterxml/aalto/out/StreamWriterBase;->throwOutputError(Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method

.method public static reportIllegalArg(Ljava/lang/String;)V
    .locals 1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static reportIllegalMethod(Ljava/lang/String;)V
    .locals 0

    invoke-static {p0}, Lcom/fasterxml/aalto/out/StreamWriterBase;->throwOutputError(Ljava/lang/String;)V

    return-void
.end method

.method private resetValidationFlags()V
    .locals 1

    iget-object v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_config:Lcom/fasterxml/aalto/out/WriterConfig;

    invoke-virtual {v0}, Lcom/fasterxml/aalto/out/WriterConfig;->willCheckStructure()Z

    move-result v0

    iput-boolean v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_cfgCheckStructure:Z

    iget-object v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_config:Lcom/fasterxml/aalto/out/WriterConfig;

    invoke-virtual {v0}, Lcom/fasterxml/aalto/out/WriterConfig;->willCheckAttributes()Z

    move-result v0

    iput-boolean v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_cfgCheckAttrs:Z

    return-void
.end method

.method public static throwFromIOE(Ljava/io/IOException;)V
    .locals 1

    new-instance v0, Lcom/fasterxml/aalto/impl/IoStreamException;

    invoke-direct {v0, p0}, Lcom/fasterxml/aalto/impl/IoStreamException;-><init>(Ljava/io/IOException;)V

    throw v0
.end method

.method public static throwOutputError(Ljava/lang/String;)V
    .locals 1

    .line 1
    new-instance v0, Lcom/fasterxml/aalto/impl/StreamExceptionBase;

    invoke-direct {v0, p0}, Lcom/fasterxml/aalto/impl/StreamExceptionBase;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static throwOutputError(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 0

    .line 2
    filled-new-array {p1}, [Ljava/lang/Object;

    move-result-object p1

    invoke-static {p0, p1}, Ljava/text/MessageFormat;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Lcom/fasterxml/aalto/out/StreamWriterBase;->throwOutputError(Ljava/lang/String;)V

    return-void
.end method

.method private final writeTypedElement(Lorg/codehaus/stax2/ri/typed/AsciiValueEncoder;)V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_stateAnyOutput:Z

    iget-boolean v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_stateStartElementOpen:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_stateEmptyElement:Z

    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/out/StreamWriterBase;->_closeStartElement(Z)V

    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_xmlWriter:Lcom/fasterxml/aalto/out/XmlWriter;

    invoke-virtual {v0, p1}, Lcom/fasterxml/aalto/out/XmlWriter;->writeTypedValue(Lorg/codehaus/stax2/ri/typed/AsciiValueEncoder;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    new-instance v0, Lcom/fasterxml/aalto/impl/IoStreamException;

    invoke-direct {v0, p1}, Lcom/fasterxml/aalto/impl/IoStreamException;-><init>(Ljava/io/IOException;)V

    throw v0
.end method


# virtual methods
.method public _closeStartElement(Z)V
    .locals 2

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_stateStartElementOpen:Z

    if-eqz p1, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_xmlWriter:Lcom/fasterxml/aalto/out/XmlWriter;

    invoke-virtual {v0}, Lcom/fasterxml/aalto/out/XmlWriter;->writeStartTagEmptyEnd()V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_xmlWriter:Lcom/fasterxml/aalto/out/XmlWriter;

    invoke-virtual {v0}, Lcom/fasterxml/aalto/out/XmlWriter;->writeStartTagEnd()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    if-eqz p1, :cond_2

    iget-object p1, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_currElem:Lcom/fasterxml/aalto/out/OutputElement;

    invoke-virtual {p1}, Lcom/fasterxml/aalto/out/OutputElement;->getParent()Lcom/fasterxml/aalto/out/OutputElement;

    move-result-object v0

    iput-object v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_currElem:Lcom/fasterxml/aalto/out/OutputElement;

    invoke-virtual {v0}, Lcom/fasterxml/aalto/out/OutputElement;->isRoot()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/fasterxml/aalto/out/StreamWriterBase$State;->EPILOG:Lcom/fasterxml/aalto/out/StreamWriterBase$State;

    iput-object v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_state:Lcom/fasterxml/aalto/out/StreamWriterBase$State;

    :cond_1
    iget v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_poolSize:I

    const/16 v1, 0x8

    if-ge v0, v1, :cond_2

    iget-object v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_outputElemPool:Lcom/fasterxml/aalto/out/OutputElement;

    invoke-virtual {p1, v0}, Lcom/fasterxml/aalto/out/OutputElement;->addToPool(Lcom/fasterxml/aalto/out/OutputElement;)V

    iput-object p1, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_outputElemPool:Lcom/fasterxml/aalto/out/OutputElement;

    iget p1, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_poolSize:I

    add-int/lit8 p1, p1, 0x1

    iput p1, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_poolSize:I

    :cond_2
    return-void

    :catch_0
    move-exception p1

    new-instance v0, Lcom/fasterxml/aalto/impl/IoStreamException;

    invoke-direct {v0, p1}, Lcom/fasterxml/aalto/impl/IoStreamException;-><init>(Ljava/io/IOException;)V

    throw v0
.end method

.method public _reportInvalidContent(I)V
    .locals 2

    iget v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_vldContent:I

    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Internal error: trying to report invalid content for "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/fasterxml/aalto/impl/ErrorConsts;->VERR_ANY:Ljava/lang/String;

    iget-object v1, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_currElem:Lcom/fasterxml/aalto/out/OutputElement;

    invoke-virtual {v1}, Lcom/fasterxml/aalto/out/OutputElement;->getNameDesc()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1}, Lcom/fasterxml/aalto/impl/ErrorConsts;->tokenTypeDesc(I)Ljava/lang/String;

    move-result-object p1

    filled-new-array {v1, p1}, [Ljava/lang/Object;

    move-result-object p1

    invoke-static {v0, p1}, Ljava/text/MessageFormat;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_1
    sget-object p1, Lcom/fasterxml/aalto/impl/ErrorConsts;->VERR_NON_MIXED:Ljava/lang/String;

    iget-object v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_currElem:Lcom/fasterxml/aalto/out/OutputElement;

    invoke-virtual {v0}, Lcom/fasterxml/aalto/out/OutputElement;->getNameDesc()Ljava/lang/String;

    move-result-object v0

    filled-new-array {v0}, [Ljava/lang/Object;

    move-result-object v0

    invoke-static {p1, v0}, Ljava/text/MessageFormat;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_2
    sget-object v0, Lcom/fasterxml/aalto/impl/ErrorConsts;->VERR_EMPTY:Ljava/lang/String;

    iget-object v1, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_currElem:Lcom/fasterxml/aalto/out/OutputElement;

    invoke-virtual {v1}, Lcom/fasterxml/aalto/out/OutputElement;->getNameDesc()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1}, Lcom/fasterxml/aalto/impl/ErrorConsts;->tokenTypeDesc(I)Ljava/lang/String;

    move-result-object p1

    filled-new-array {v1, p1}, [Ljava/lang/Object;

    move-result-object p1

    invoke-static {v0, p1}, Ljava/text/MessageFormat;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    :goto_0
    invoke-virtual {p0, p1}, Lcom/fasterxml/aalto/out/StreamWriterBase;->_reportValidationProblem(Ljava/lang/String;)V

    return-void
.end method

.method public _reportValidationProblem(Ljava/lang/String;)V
    .locals 3

    new-instance v0, Lorg/codehaus/stax2/validation/XMLValidationProblem;

    invoke-virtual {p0}, Lcom/fasterxml/aalto/out/StreamWriterBase;->getValidationLocation()Ljavax/xml/stream/Location;

    move-result-object v1

    const/4 v2, 0x2

    invoke-direct {v0, v1, p1, v2}, Lorg/codehaus/stax2/validation/XMLValidationProblem;-><init>(Ljavax/xml/stream/Location;Ljava/lang/String;I)V

    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/out/StreamWriterBase;->reportProblem(Lorg/codehaus/stax2/validation/XMLValidationProblem;)V

    return-void
.end method

.method public abstract _serializeQName(Ljavax/xml/namespace/QName;)Ljava/lang/String;
.end method

.method public abstract _setPrefix(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public _verifyRootElement(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    sget-object p1, Lcom/fasterxml/aalto/out/StreamWriterBase$State;->TREE:Lcom/fasterxml/aalto/out/StreamWriterBase$State;

    iput-object p1, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_state:Lcom/fasterxml/aalto/out/StreamWriterBase$State;

    return-void
.end method

.method public _verifyStartElement(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    iget-boolean v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_stateStartElementOpen:Z

    if-eqz v0, :cond_0

    iget-boolean p1, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_stateEmptyElement:Z

    invoke-virtual {p0, p1}, Lcom/fasterxml/aalto/out/StreamWriterBase;->_closeStartElement(Z)V

    goto :goto_1

    :cond_0
    iget-object v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_state:Lcom/fasterxml/aalto/out/StreamWriterBase$State;

    sget-object v1, Lcom/fasterxml/aalto/out/StreamWriterBase$State;->PROLOG:Lcom/fasterxml/aalto/out/StreamWriterBase$State;

    if-ne v0, v1, :cond_1

    invoke-virtual {p0, p1, p2}, Lcom/fasterxml/aalto/out/StreamWriterBase;->_verifyRootElement(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_1
    sget-object v1, Lcom/fasterxml/aalto/out/StreamWriterBase$State;->EPILOG:Lcom/fasterxml/aalto/out/StreamWriterBase$State;

    if-ne v0, v1, :cond_4

    iget-boolean v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_cfgCheckStructure:Z

    if-eqz v0, :cond_3

    if-nez p1, :cond_2

    goto :goto_0

    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, ":"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    :goto_0
    sget-object p1, Lcom/fasterxml/aalto/impl/ErrorConsts;->WERR_PROLOG_SECOND_ROOT:Ljava/lang/String;

    invoke-static {p1, p2}, Lcom/fasterxml/aalto/out/StreamWriterBase;->_reportNwfStructure(Ljava/lang/String;Ljava/lang/Object;)V

    :cond_3
    sget-object p1, Lcom/fasterxml/aalto/out/StreamWriterBase$State;->TREE:Lcom/fasterxml/aalto/out/StreamWriterBase$State;

    iput-object p1, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_state:Lcom/fasterxml/aalto/out/StreamWriterBase$State;

    :cond_4
    :goto_1
    return-void
.end method

.method public final _verifyWriteAttr(Lcom/fasterxml/aalto/out/WName;)V
    .locals 0

    return-void
.end method

.method public final _verifyWriteCData()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_stateAnyOutput:Z

    iget-boolean v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_stateStartElementOpen:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_stateEmptyElement:Z

    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/out/StreamWriterBase;->_closeStartElement(Z)V

    :cond_0
    iget-boolean v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_cfgCheckStructure:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/fasterxml/aalto/out/StreamWriterBase;->inPrologOrEpilog()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/fasterxml/aalto/impl/ErrorConsts;->WERR_PROLOG_CDATA:Ljava/lang/String;

    invoke-static {v0}, Lcom/fasterxml/aalto/out/StreamWriterBase;->_reportNwfStructure(Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method public final _verifyWriteDTD()V
    .locals 3

    iget-boolean v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_cfgCheckStructure:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_state:Lcom/fasterxml/aalto/out/StreamWriterBase$State;

    sget-object v1, Lcom/fasterxml/aalto/out/StreamWriterBase$State;->PROLOG:Lcom/fasterxml/aalto/out/StreamWriterBase$State;

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_dtdRootElemName:Ljava/lang/String;

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    new-instance v0, Ljavax/xml/stream/XMLStreamException;

    const-string v1, "Trying to write multiple DOCTYPE declarations"

    invoke-direct {v0, v1}, Ljavax/xml/stream/XMLStreamException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    new-instance v0, Ljavax/xml/stream/XMLStreamException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Can not write DOCTYPE declaration (DTD) when not in prolog any more (state "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_state:Lcom/fasterxml/aalto/out/StreamWriterBase$State;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v2, "; start element(s) written)"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljavax/xml/stream/XMLStreamException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    :goto_0
    return-void
.end method

.method public final _writeAttribute(Lcom/fasterxml/aalto/out/WName;Ljava/lang/String;)V
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_cfgCheckAttrs:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/fasterxml/aalto/out/StreamWriterBase;->_verifyWriteAttr(Lcom/fasterxml/aalto/out/WName;)V

    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_xmlWriter:Lcom/fasterxml/aalto/out/XmlWriter;

    invoke-virtual {v0, p1, p2}, Lcom/fasterxml/aalto/out/XmlWriter;->writeAttribute(Lcom/fasterxml/aalto/out/WName;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    new-instance p2, Lcom/fasterxml/aalto/impl/IoStreamException;

    invoke-direct {p2, p1}, Lcom/fasterxml/aalto/impl/IoStreamException;-><init>(Ljava/io/IOException;)V

    throw p2
.end method

.method public final _writeAttribute(Lcom/fasterxml/aalto/out/WName;Lorg/codehaus/stax2/ri/typed/AsciiValueEncoder;)V
    .locals 1

    .line 2
    iget-boolean v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_cfgCheckAttrs:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/fasterxml/aalto/out/StreamWriterBase;->_verifyWriteAttr(Lcom/fasterxml/aalto/out/WName;)V

    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_xmlWriter:Lcom/fasterxml/aalto/out/XmlWriter;

    invoke-virtual {v0, p1, p2}, Lcom/fasterxml/aalto/out/XmlWriter;->writeAttribute(Lcom/fasterxml/aalto/out/WName;Lorg/codehaus/stax2/ri/typed/AsciiValueEncoder;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    new-instance p2, Lcom/fasterxml/aalto/impl/IoStreamException;

    invoke-direct {p2, p1}, Lcom/fasterxml/aalto/impl/IoStreamException;-><init>(Ljava/io/IOException;)V

    throw p2
.end method

.method public final _writeDefaultNamespace(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_symbols:Lcom/fasterxml/aalto/out/WNameTable;

    const-string v1, "xmlns"

    invoke-virtual {v0, v1}, Lcom/fasterxml/aalto/out/WNameTable;->findSymbol(Ljava/lang/String;)Lcom/fasterxml/aalto/out/WName;

    move-result-object v0

    :try_start_0
    iget-object v1, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_xmlWriter:Lcom/fasterxml/aalto/out/XmlWriter;

    invoke-virtual {v1, v0, p1}, Lcom/fasterxml/aalto/out/XmlWriter;->writeAttribute(Lcom/fasterxml/aalto/out/WName;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    new-instance v0, Lcom/fasterxml/aalto/impl/IoStreamException;

    invoke-direct {v0, p1}, Lcom/fasterxml/aalto/impl/IoStreamException;-><init>(Ljava/io/IOException;)V

    throw v0
.end method

.method public final _writeNamespace(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_symbols:Lcom/fasterxml/aalto/out/WNameTable;

    const-string v1, "xmlns"

    invoke-virtual {v0, v1, p1}, Lcom/fasterxml/aalto/out/WNameTable;->findSymbol(Ljava/lang/String;Ljava/lang/String;)Lcom/fasterxml/aalto/out/WName;

    move-result-object p1

    :try_start_0
    iget-object v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_xmlWriter:Lcom/fasterxml/aalto/out/XmlWriter;

    invoke-virtual {v0, p1, p2}, Lcom/fasterxml/aalto/out/XmlWriter;->writeAttribute(Lcom/fasterxml/aalto/out/WName;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    new-instance p2, Lcom/fasterxml/aalto/impl/IoStreamException;

    invoke-direct {p2, p1}, Lcom/fasterxml/aalto/impl/IoStreamException;-><init>(Ljava/io/IOException;)V

    throw p2
.end method

.method public _writeStartDocument(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    iget-boolean v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_cfgCheckStructure:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_stateAnyOutput:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/fasterxml/aalto/impl/ErrorConsts;->WERR_DUP_XML_DECL:Ljava/lang/String;

    invoke-static {v0}, Lcom/fasterxml/aalto/out/StreamWriterBase;->_reportNwfStructure(Ljava/lang/String;)V

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_stateAnyOutput:Z

    iget-boolean v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_cfgCheckContent:Z

    const-string v1, "1.1"

    const-string v2, "1.0"

    if-eqz v0, :cond_1

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_1

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Illegal version argument (\'"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "\'); should only use \'"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "\' or \'"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "\'"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fasterxml/aalto/out/StreamWriterBase;->_reportNwfContent(Ljava/lang/String;)V

    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_3

    :cond_2
    move-object p1, v2

    :cond_3
    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_config:Lcom/fasterxml/aalto/out/WriterConfig;

    invoke-virtual {v0}, Lcom/fasterxml/aalto/out/WriterConfig;->enableXml11()V

    iget-object v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_xmlWriter:Lcom/fasterxml/aalto/out/XmlWriter;

    invoke-virtual {v0}, Lcom/fasterxml/aalto/out/XmlWriter;->enableXml11()V

    :cond_4
    if-eqz p2, :cond_5

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_5

    iget-object v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_config:Lcom/fasterxml/aalto/out/WriterConfig;

    invoke-virtual {v0, p2}, Lcom/fasterxml/aalto/out/WriterConfig;->setActualEncodingIfNotSet(Ljava/lang/String;)V

    :cond_5
    :try_start_0
    iget-object v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_xmlWriter:Lcom/fasterxml/aalto/out/XmlWriter;

    invoke-virtual {v0, p1, p2, p3}, Lcom/fasterxml/aalto/out/XmlWriter;->writeXmlDeclaration(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    new-instance p2, Lcom/fasterxml/aalto/impl/IoStreamException;

    invoke-direct {p2, p1}, Lcom/fasterxml/aalto/impl/IoStreamException;-><init>(Ljava/io/IOException;)V

    throw p2
.end method

.method public _writeStartTag(Lcom/fasterxml/aalto/out/WName;Z)V
    .locals 3

    .line 1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_stateAnyOutput:Z

    iput-boolean v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_stateStartElementOpen:Z

    iget-object v1, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_outputElemPool:Lcom/fasterxml/aalto/out/OutputElement;

    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_currElem:Lcom/fasterxml/aalto/out/OutputElement;

    invoke-virtual {v1, v2, p1}, Lcom/fasterxml/aalto/out/OutputElement;->reuseAsChild(Lcom/fasterxml/aalto/out/OutputElement;Lcom/fasterxml/aalto/out/WName;)Lcom/fasterxml/aalto/out/OutputElement;

    move-result-object v2

    iput-object v2, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_outputElemPool:Lcom/fasterxml/aalto/out/OutputElement;

    iget v2, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_poolSize:I

    sub-int/2addr v2, v0

    iput v2, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_poolSize:I

    iput-object v1, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_currElem:Lcom/fasterxml/aalto/out/OutputElement;

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_currElem:Lcom/fasterxml/aalto/out/OutputElement;

    invoke-virtual {v0, p1}, Lcom/fasterxml/aalto/out/OutputElement;->createChild(Lcom/fasterxml/aalto/out/WName;)Lcom/fasterxml/aalto/out/OutputElement;

    move-result-object v0

    iput-object v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_currElem:Lcom/fasterxml/aalto/out/OutputElement;

    :goto_0
    :try_start_0
    iget-object v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_xmlWriter:Lcom/fasterxml/aalto/out/XmlWriter;

    invoke-virtual {v0, p1}, Lcom/fasterxml/aalto/out/XmlWriter;->writeStartTagStart(Lcom/fasterxml/aalto/out/WName;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    iput-boolean p2, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_stateEmptyElement:Z

    return-void

    :catch_0
    move-exception p1

    new-instance p2, Lcom/fasterxml/aalto/impl/IoStreamException;

    invoke-direct {p2, p1}, Lcom/fasterxml/aalto/impl/IoStreamException;-><init>(Ljava/io/IOException;)V

    throw p2
.end method

.method public _writeStartTag(Lcom/fasterxml/aalto/out/WName;ZLjava/lang/String;)V
    .locals 3

    .line 2
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_stateAnyOutput:Z

    iput-boolean v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_stateStartElementOpen:Z

    if-nez p3, :cond_0

    const-string p3, ""

    :cond_0
    iget-object v1, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_outputElemPool:Lcom/fasterxml/aalto/out/OutputElement;

    if-eqz v1, :cond_1

    iget-object v2, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_currElem:Lcom/fasterxml/aalto/out/OutputElement;

    invoke-virtual {v1, v2, p1, p3}, Lcom/fasterxml/aalto/out/OutputElement;->reuseAsChild(Lcom/fasterxml/aalto/out/OutputElement;Lcom/fasterxml/aalto/out/WName;Ljava/lang/String;)Lcom/fasterxml/aalto/out/OutputElement;

    move-result-object p3

    iput-object p3, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_outputElemPool:Lcom/fasterxml/aalto/out/OutputElement;

    iget p3, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_poolSize:I

    sub-int/2addr p3, v0

    iput p3, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_poolSize:I

    iput-object v1, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_currElem:Lcom/fasterxml/aalto/out/OutputElement;

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_currElem:Lcom/fasterxml/aalto/out/OutputElement;

    invoke-virtual {v0, p1, p3}, Lcom/fasterxml/aalto/out/OutputElement;->createChild(Lcom/fasterxml/aalto/out/WName;Ljava/lang/String;)Lcom/fasterxml/aalto/out/OutputElement;

    move-result-object p3

    iput-object p3, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_currElem:Lcom/fasterxml/aalto/out/OutputElement;

    :goto_0
    :try_start_0
    iget-object p3, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_xmlWriter:Lcom/fasterxml/aalto/out/XmlWriter;

    invoke-virtual {p3, p1}, Lcom/fasterxml/aalto/out/XmlWriter;->writeStartTagStart(Lcom/fasterxml/aalto/out/WName;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    iput-boolean p2, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_stateEmptyElement:Z

    return-void

    :catch_0
    move-exception p1

    new-instance p2, Lcom/fasterxml/aalto/impl/IoStreamException;

    invoke-direct {p2, p1}, Lcom/fasterxml/aalto/impl/IoStreamException;-><init>(Ljava/io/IOException;)V

    throw p2
.end method

.method public addDefaultAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    .locals 0

    const/4 p1, -0x1

    return p1
.end method

.method public close()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/fasterxml/aalto/out/StreamWriterBase;->_finishDocument(Z)V

    return-void
.end method

.method public closeCompletely()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/fasterxml/aalto/out/StreamWriterBase;->_finishDocument(Z)V

    return-void
.end method

.method public findAttributeIndex(Ljava/lang/String;Ljava/lang/String;)I
    .locals 0

    const/4 p1, -0x1

    return p1
.end method

.method public flush()V
    .locals 2

    :try_start_0
    iget-object v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_xmlWriter:Lcom/fasterxml/aalto/out/XmlWriter;

    invoke-virtual {v0}, Lcom/fasterxml/aalto/out/XmlWriter;->flush()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Lcom/fasterxml/aalto/impl/IoStreamException;

    invoke-direct {v1, v0}, Lcom/fasterxml/aalto/impl/IoStreamException;-><init>(Ljava/io/IOException;)V

    throw v1
.end method

.method public getAttributeCount()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getAttributeLocalName(I)Ljava/lang/String;
    .locals 0

    const/4 p1, 0x0

    return-object p1
.end method

.method public getAttributeNamespace(I)Ljava/lang/String;
    .locals 0

    const/4 p1, 0x0

    return-object p1
.end method

.method public getAttributePrefix(I)Ljava/lang/String;
    .locals 0

    const/4 p1, 0x0

    return-object p1
.end method

.method public getAttributeType(I)Ljava/lang/String;
    .locals 0

    const-string p1, ""

    return-object p1
.end method

.method public getAttributeValue(I)Ljava/lang/String;
    .locals 0

    .line 1
    const/4 p1, 0x0

    return-object p1
.end method

.method public getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 2
    const/4 p1, 0x0

    return-object p1
.end method

.method public getBaseUri()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getCurrentElementName()Ljavax/xml/namespace/QName;
    .locals 1

    iget-object v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_currElem:Lcom/fasterxml/aalto/out/OutputElement;

    invoke-virtual {v0}, Lcom/fasterxml/aalto/out/OutputElement;->getQName()Ljavax/xml/namespace/QName;

    move-result-object v0

    return-object v0
.end method

.method public getEncoding()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_config:Lcom/fasterxml/aalto/out/WriterConfig;

    invoke-virtual {v0}, Lcom/fasterxml/aalto/out/WriterConfig;->getActualEncoding()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getLocation()Lorg/codehaus/stax2/XMLStreamLocation2;
    .locals 7

    new-instance v6, Lcom/fasterxml/aalto/impl/LocationImpl;

    const/4 v1, 0x0

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_xmlWriter:Lcom/fasterxml/aalto/out/XmlWriter;

    invoke-virtual {v0}, Lcom/fasterxml/aalto/out/XmlWriter;->getAbsOffset()I

    move-result v3

    iget-object v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_xmlWriter:Lcom/fasterxml/aalto/out/XmlWriter;

    invoke-virtual {v0}, Lcom/fasterxml/aalto/out/XmlWriter;->getRow()I

    move-result v4

    iget-object v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_xmlWriter:Lcom/fasterxml/aalto/out/XmlWriter;

    invoke-virtual {v0}, Lcom/fasterxml/aalto/out/XmlWriter;->getColumn()I

    move-result v5

    move-object v0, v6

    invoke-direct/range {v0 .. v5}, Lcom/fasterxml/aalto/impl/LocationImpl;-><init>(Ljava/lang/String;Ljava/lang/String;III)V

    return-object v6
.end method

.method public final getNamespaceContext()Ljavax/xml/namespace/NamespaceContext;
    .locals 0

    return-object p0
.end method

.method public getNamespaceURI(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_currElem:Lcom/fasterxml/aalto/out/OutputElement;

    invoke-virtual {v0, p1}, Lcom/fasterxml/aalto/out/OutputElement;->getNamespaceURI(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v1, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_rootNsContext:Ljavax/xml/namespace/NamespaceContext;

    if-eqz v1, :cond_0

    invoke-interface {v1, p1}, Ljavax/xml/namespace/NamespaceContext;->getNamespaceURI(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method public getPrefix(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_currElem:Lcom/fasterxml/aalto/out/OutputElement;

    invoke-virtual {v0, p1}, Lcom/fasterxml/aalto/out/OutputElement;->getPrefix(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v1, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_rootNsContext:Ljavax/xml/namespace/NamespaceContext;

    if-eqz v1, :cond_0

    invoke-interface {v1, p1}, Ljavax/xml/namespace/NamespaceContext;->getPrefix(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method public getPrefixes(Ljava/lang/String;)Ljava/util/Iterator;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Iterator<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_currElem:Lcom/fasterxml/aalto/out/OutputElement;

    iget-object v1, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_rootNsContext:Ljavax/xml/namespace/NamespaceContext;

    invoke-virtual {v0, p1, v1}, Lcom/fasterxml/aalto/out/OutputElement;->getPrefixes(Ljava/lang/String;Ljavax/xml/namespace/NamespaceContext;)Ljava/util/Iterator;

    move-result-object p1

    return-object p1
.end method

.method public getProperty(Ljava/lang/String;)Ljava/lang/Object;
    .locals 2

    iget-object v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_config:Lcom/fasterxml/aalto/out/WriterConfig;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lcom/fasterxml/aalto/out/WriterConfig;->getProperty(Ljava/lang/String;Z)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public getValidationLocation()Ljavax/xml/stream/Location;
    .locals 1

    invoke-virtual {p0}, Lcom/fasterxml/aalto/out/StreamWriterBase;->getLocation()Lorg/codehaus/stax2/XMLStreamLocation2;

    move-result-object v0

    return-object v0
.end method

.method public getXmlVersion()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_config:Lcom/fasterxml/aalto/out/WriterConfig;

    invoke-virtual {v0}, Lcom/fasterxml/aalto/out/WriterConfig;->isXml11()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "1.1"

    goto :goto_0

    :cond_0
    const-string v0, "1.0"

    :goto_0
    return-object v0
.end method

.method public final inPrologOrEpilog()Z
    .locals 2

    iget-object v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_state:Lcom/fasterxml/aalto/out/StreamWriterBase$State;

    sget-object v1, Lcom/fasterxml/aalto/out/StreamWriterBase$State;->TREE:Lcom/fasterxml/aalto/out/StreamWriterBase$State;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isNotationDeclared(Ljava/lang/String;)Z
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

.method public isPropertySupported(Ljava/lang/String;)Z
    .locals 1

    iget-object v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_config:Lcom/fasterxml/aalto/out/WriterConfig;

    invoke-virtual {v0, p1}, Lcom/fasterxml/aalto/out/WriterConfig;->isPropertySupported(Ljava/lang/String;)Z

    move-result p1

    return p1
.end method

.method public isUnparsedEntityDeclared(Ljava/lang/String;)Z
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

.method public reportProblem(Lorg/codehaus/stax2/validation/XMLValidationProblem;)V
    .locals 2

    iget-object v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_vldProblemHandler:Lorg/codehaus/stax2/validation/ValidationProblemHandler;

    if-eqz v0, :cond_0

    invoke-interface {v0, p1}, Lorg/codehaus/stax2/validation/ValidationProblemHandler;->reportProblem(Lorg/codehaus/stax2/validation/XMLValidationProblem;)V

    return-void

    :cond_0
    invoke-virtual {p1}, Lorg/codehaus/stax2/validation/XMLValidationProblem;->getSeverity()I

    move-result v0

    const/4 v1, 0x2

    if-ge v0, v1, :cond_1

    return-void

    :cond_1
    invoke-static {p1}, Lcom/fasterxml/aalto/ValidationException;->create(Lorg/codehaus/stax2/validation/XMLValidationProblem;)Lcom/fasterxml/aalto/ValidationException;

    move-result-object p1

    throw p1
.end method

.method public abstract setDefaultNamespace(Ljava/lang/String;)V
.end method

.method public setNamespaceContext(Ljavax/xml/namespace/NamespaceContext;)V
    .locals 2

    iget-object v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_state:Lcom/fasterxml/aalto/out/StreamWriterBase$State;

    sget-object v1, Lcom/fasterxml/aalto/out/StreamWriterBase$State;->PROLOG:Lcom/fasterxml/aalto/out/StreamWriterBase$State;

    if-eq v0, v1, :cond_0

    const-string v0, "Called setNamespaceContext() after having already output root element."

    invoke-static {v0}, Lcom/fasterxml/aalto/out/StreamWriterBase;->throwOutputError(Ljava/lang/String;)V

    :cond_0
    iput-object p1, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_rootNsContext:Ljavax/xml/namespace/NamespaceContext;

    return-void
.end method

.method public final setPrefix(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0, p2}, Lcom/fasterxml/aalto/out/StreamWriterBase;->setDefaultNamespace(Ljava/lang/String;)V

    return-void

    :cond_0
    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const-string v0, "xml"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const-string v1, "http://www.w3.org/XML/1998/namespace"

    if-eqz v0, :cond_1

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    sget-object v0, Lcom/fasterxml/aalto/impl/ErrorConsts;->ERR_NS_REDECL_XML:Ljava/lang/String;

    :goto_0
    invoke-static {v0, p2}, Lcom/fasterxml/aalto/out/StreamWriterBase;->throwOutputError(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_2

    :cond_1
    const-string v0, "xmlns"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const-string v2, "http://www.w3.org/2000/xmlns/"

    if-eqz v0, :cond_2

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    sget-object v0, Lcom/fasterxml/aalto/impl/ErrorConsts;->ERR_NS_REDECL_XMLNS:Ljava/lang/String;

    goto :goto_0

    :cond_2
    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object v0, Lcom/fasterxml/aalto/impl/ErrorConsts;->ERR_NS_REDECL_XML_URI:Ljava/lang/String;

    :goto_1
    invoke-static {v0, p1}, Lcom/fasterxml/aalto/out/StreamWriterBase;->throwOutputError(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_2

    :cond_3
    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    sget-object v0, Lcom/fasterxml/aalto/impl/ErrorConsts;->ERR_NS_REDECL_XMLNS_URI:Ljava/lang/String;

    goto :goto_1

    :cond_4
    :goto_2
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_config:Lcom/fasterxml/aalto/out/WriterConfig;

    invoke-virtual {v0}, Lcom/fasterxml/aalto/out/WriterConfig;->isXml11()Z

    move-result v0

    if-nez v0, :cond_5

    sget-object v0, Lcom/fasterxml/aalto/impl/ErrorConsts;->ERR_NS_EMPTY:Ljava/lang/String;

    invoke-static {v0}, Lcom/fasterxml/aalto/out/StreamWriterBase;->throwOutputError(Ljava/lang/String;)V

    :cond_5
    invoke-virtual {p0, p1, p2}, Lcom/fasterxml/aalto/out/StreamWriterBase;->_setPrefix(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public setProperty(Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 1

    iget-object v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_config:Lcom/fasterxml/aalto/out/WriterConfig;

    invoke-virtual {v0, p1, p2}, Lcom/fasterxml/aalto/out/WriterConfig;->setProperty(Ljava/lang/String;Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public setValidationProblemHandler(Lorg/codehaus/stax2/validation/ValidationProblemHandler;)Lorg/codehaus/stax2/validation/ValidationProblemHandler;
    .locals 1

    iget-object v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_vldProblemHandler:Lorg/codehaus/stax2/validation/ValidationProblemHandler;

    iput-object p1, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_vldProblemHandler:Lorg/codehaus/stax2/validation/ValidationProblemHandler;

    return-object v0
.end method

.method public stopValidatingAgainst(Lorg/codehaus/stax2/validation/XMLValidationSchema;)Lorg/codehaus/stax2/validation/XMLValidator;
    .locals 3

    .line 1
    const/4 v0, 0x2

    new-array v0, v0, [Lorg/codehaus/stax2/validation/XMLValidator;

    iget-object v1, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_validator:Lorg/codehaus/stax2/validation/XMLValidator;

    invoke-static {v1, p1, v0}, Lorg/codehaus/stax2/validation/ValidatorPair;->removeValidator(Lorg/codehaus/stax2/validation/XMLValidator;Lorg/codehaus/stax2/validation/XMLValidationSchema;[Lorg/codehaus/stax2/validation/XMLValidator;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x0

    aget-object v1, v0, p1

    const/4 v2, 0x1

    aget-object v0, v0, v2

    iput-object v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_validator:Lorg/codehaus/stax2/validation/XMLValidator;

    invoke-virtual {v1, p1}, Lorg/codehaus/stax2/validation/XMLValidator;->validationCompleted(Z)V

    iget-object p1, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_validator:Lorg/codehaus/stax2/validation/XMLValidator;

    if-nez p1, :cond_1

    invoke-direct {p0}, Lcom/fasterxml/aalto/out/StreamWriterBase;->resetValidationFlags()V

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :cond_1
    :goto_0
    return-object v1
.end method

.method public stopValidatingAgainst(Lorg/codehaus/stax2/validation/XMLValidator;)Lorg/codehaus/stax2/validation/XMLValidator;
    .locals 3

    .line 2
    const/4 v0, 0x2

    new-array v0, v0, [Lorg/codehaus/stax2/validation/XMLValidator;

    iget-object v1, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_validator:Lorg/codehaus/stax2/validation/XMLValidator;

    invoke-static {v1, p1, v0}, Lorg/codehaus/stax2/validation/ValidatorPair;->removeValidator(Lorg/codehaus/stax2/validation/XMLValidator;Lorg/codehaus/stax2/validation/XMLValidator;[Lorg/codehaus/stax2/validation/XMLValidator;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x0

    aget-object v1, v0, p1

    const/4 v2, 0x1

    aget-object v0, v0, v2

    iput-object v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_validator:Lorg/codehaus/stax2/validation/XMLValidator;

    invoke-virtual {v1, p1}, Lorg/codehaus/stax2/validation/XMLValidator;->validationCompleted(Z)V

    iget-object p1, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_validator:Lorg/codehaus/stax2/validation/XMLValidator;

    if-nez p1, :cond_1

    invoke-direct {p0}, Lcom/fasterxml/aalto/out/StreamWriterBase;->resetValidationFlags()V

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :cond_1
    :goto_0
    return-object v1
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[StreamWriter: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", underlying outputter: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_xmlWriter:Lcom/fasterxml/aalto/out/XmlWriter;

    if-nez v1, :cond_0

    const-string v1, "NULL"

    goto :goto_0

    :cond_0
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    :goto_0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public validateAgainst(Lorg/codehaus/stax2/validation/XMLValidationSchema;)Lorg/codehaus/stax2/validation/XMLValidator;
    .locals 2

    invoke-interface {p1, p0}, Lorg/codehaus/stax2/validation/XMLValidationSchema;->createValidator(Lorg/codehaus/stax2/validation/ValidationContext;)Lorg/codehaus/stax2/validation/XMLValidator;

    move-result-object p1

    iget-object v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_validator:Lorg/codehaus/stax2/validation/XMLValidator;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_cfgCheckStructure:Z

    iput-boolean v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_cfgCheckAttrs:Z

    iput-object p1, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_validator:Lorg/codehaus/stax2/validation/XMLValidator;

    goto :goto_0

    :cond_0
    new-instance v1, Lorg/codehaus/stax2/validation/ValidatorPair;

    invoke-direct {v1, v0, p1}, Lorg/codehaus/stax2/validation/ValidatorPair;-><init>(Lorg/codehaus/stax2/validation/XMLValidator;Lorg/codehaus/stax2/validation/XMLValidator;)V

    iput-object v1, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_validator:Lorg/codehaus/stax2/validation/XMLValidator;

    :goto_0
    return-object p1
.end method

.method public final valueEncoderFactory()Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory;
    .locals 1

    iget-object v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_valueEncoderFactory:Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory;

    if-nez v0, :cond_0

    new-instance v0, Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory;

    invoke-direct {v0}, Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory;-><init>()V

    iput-object v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_valueEncoderFactory:Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory;

    :cond_0
    iget-object v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_valueEncoderFactory:Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory;

    return-object v0
.end method

.method public final writeAttribute(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_stateStartElementOpen:Z

    if-nez v0, :cond_0

    sget-object v0, Lcom/fasterxml/aalto/impl/ErrorConsts;->WERR_ATTR_NO_ELEM:Ljava/lang/String;

    invoke-static {v0}, Lcom/fasterxml/aalto/out/StreamWriterBase;->throwOutputError(Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_symbols:Lcom/fasterxml/aalto/out/WNameTable;

    invoke-virtual {v0, p1}, Lcom/fasterxml/aalto/out/WNameTable;->findSymbol(Ljava/lang/String;)Lcom/fasterxml/aalto/out/WName;

    move-result-object p1

    invoke-virtual {p0, p1, p2}, Lcom/fasterxml/aalto/out/StreamWriterBase;->_writeAttribute(Lcom/fasterxml/aalto/out/WName;Ljava/lang/String;)V

    return-void
.end method

.method public abstract writeAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract writeAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public writeBinary(Lorg/codehaus/stax2/typed/Base64Variant;[BII)V
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/fasterxml/aalto/out/StreamWriterBase;->valueEncoderFactory()Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3, p4}, Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory;->getEncoder(Lorg/codehaus/stax2/typed/Base64Variant;[BII)Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory$Base64Encoder;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/fasterxml/aalto/out/StreamWriterBase;->writeTypedElement(Lorg/codehaus/stax2/ri/typed/AsciiValueEncoder;)V

    return-void
.end method

.method public writeBinary([BII)V
    .locals 2

    .line 2
    invoke-static {}, Lorg/codehaus/stax2/typed/Base64Variants;->getDefaultVariant()Lorg/codehaus/stax2/typed/Base64Variant;

    move-result-object v0

    invoke-virtual {p0}, Lcom/fasterxml/aalto/out/StreamWriterBase;->valueEncoderFactory()Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory;

    move-result-object v1

    invoke-virtual {v1, v0, p1, p2, p3}, Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory;->getEncoder(Lorg/codehaus/stax2/typed/Base64Variant;[BII)Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory$Base64Encoder;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/fasterxml/aalto/out/StreamWriterBase;->writeTypedElement(Lorg/codehaus/stax2/ri/typed/AsciiValueEncoder;)V

    return-void
.end method

.method public writeBinaryAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[B)V
    .locals 4

    .line 1
    invoke-static {}, Lorg/codehaus/stax2/typed/Base64Variants;->getDefaultVariant()Lorg/codehaus/stax2/typed/Base64Variant;

    move-result-object v0

    invoke-virtual {p0}, Lcom/fasterxml/aalto/out/StreamWriterBase;->valueEncoderFactory()Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory;

    move-result-object v1

    array-length v2, p4

    const/4 v3, 0x0

    invoke-virtual {v1, v0, p4, v3, v2}, Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory;->getEncoder(Lorg/codehaus/stax2/typed/Base64Variant;[BII)Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory$Base64Encoder;

    move-result-object p4

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/fasterxml/aalto/out/StreamWriterBase;->writeTypedAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lorg/codehaus/stax2/ri/typed/AsciiValueEncoder;)V

    return-void
.end method

.method public writeBinaryAttribute(Lorg/codehaus/stax2/typed/Base64Variant;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[B)V
    .locals 3

    .line 2
    invoke-virtual {p0}, Lcom/fasterxml/aalto/out/StreamWriterBase;->valueEncoderFactory()Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory;

    move-result-object v0

    array-length v1, p5

    const/4 v2, 0x0

    invoke-virtual {v0, p1, p5, v2, v1}, Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory;->getEncoder(Lorg/codehaus/stax2/typed/Base64Variant;[BII)Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory$Base64Encoder;

    move-result-object p1

    invoke-virtual {p0, p2, p3, p4, p1}, Lcom/fasterxml/aalto/out/StreamWriterBase;->writeTypedAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lorg/codehaus/stax2/ri/typed/AsciiValueEncoder;)V

    return-void
.end method

.method public writeBoolean(Z)V
    .locals 1

    invoke-virtual {p0}, Lcom/fasterxml/aalto/out/StreamWriterBase;->valueEncoderFactory()Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory;

    move-result-object v0

    if-eqz p1, :cond_0

    const-string p1, "true"

    goto :goto_0

    :cond_0
    const-string p1, "false"

    :goto_0
    invoke-virtual {v0, p1}, Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory;->getScalarEncoder(Ljava/lang/String;)Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory$ScalarEncoder;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/fasterxml/aalto/out/StreamWriterBase;->writeTypedElement(Lorg/codehaus/stax2/ri/typed/AsciiValueEncoder;)V

    return-void
.end method

.method public final writeBooleanAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 1

    invoke-virtual {p0}, Lcom/fasterxml/aalto/out/StreamWriterBase;->valueEncoderFactory()Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory;

    move-result-object v0

    invoke-virtual {v0, p4}, Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory;->getEncoder(Z)Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory$ScalarEncoder;

    move-result-object p4

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/fasterxml/aalto/out/StreamWriterBase;->writeTypedAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lorg/codehaus/stax2/ri/typed/AsciiValueEncoder;)V

    return-void
.end method

.method public writeCData(Ljava/lang/String;)V
    .locals 2

    .line 1
    iget-boolean v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_cfgCDataAsText:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/fasterxml/aalto/out/StreamWriterBase;->writeCharacters(Ljava/lang/String;)V

    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/fasterxml/aalto/out/StreamWriterBase;->_verifyWriteCData()V

    iget v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_vldContent:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_validator:Lorg/codehaus/stax2/validation/XMLValidator;

    if-eqz v0, :cond_1

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lorg/codehaus/stax2/validation/XMLValidator;->validateText(Ljava/lang/String;Z)V

    :cond_1
    :try_start_0
    iget-object v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_xmlWriter:Lcom/fasterxml/aalto/out/XmlWriter;

    invoke-virtual {v0, p1}, Lcom/fasterxml/aalto/out/XmlWriter;->writeCData(Ljava/lang/String;)I

    move-result p1

    if-ltz p1, :cond_2

    sget-object v0, Lcom/fasterxml/aalto/impl/ErrorConsts;->WERR_CDATA_CONTENT:Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-static {v0, p1}, Lcom/fasterxml/aalto/out/StreamWriterBase;->_reportNwfContent(Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_2
    return-void

    :catch_0
    move-exception p1

    new-instance v0, Lcom/fasterxml/aalto/impl/IoStreamException;

    invoke-direct {v0, p1}, Lcom/fasterxml/aalto/impl/IoStreamException;-><init>(Ljava/io/IOException;)V

    throw v0
.end method

.method public writeCData([CII)V
    .locals 1

    .line 2
    iget-boolean v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_cfgCDataAsText:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1, p2, p3}, Lcom/fasterxml/aalto/out/StreamWriterBase;->writeCharacters([CII)V

    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/fasterxml/aalto/out/StreamWriterBase;->_verifyWriteCData()V

    :try_start_0
    iget-object v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_xmlWriter:Lcom/fasterxml/aalto/out/XmlWriter;

    invoke-virtual {v0, p1, p2, p3}, Lcom/fasterxml/aalto/out/XmlWriter;->writeCData([CII)I

    move-result p1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    if-ltz p1, :cond_1

    sget-object p2, Lcom/fasterxml/aalto/impl/ErrorConsts;->WERR_CDATA_CONTENT:Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-static {p2, p1}, Lcom/fasterxml/aalto/out/StreamWriterBase;->_reportNwfContent(Ljava/lang/String;Ljava/lang/Object;)V

    :cond_1
    return-void

    :catch_0
    move-exception p1

    new-instance p2, Lcom/fasterxml/aalto/impl/IoStreamException;

    invoke-direct {p2, p1}, Lcom/fasterxml/aalto/impl/IoStreamException;-><init>(Ljava/io/IOException;)V

    throw p2
.end method

.method public writeCharacters(Ljava/lang/String;)V
    .locals 2

    .line 1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_stateAnyOutput:Z

    iget-boolean v1, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_stateStartElementOpen:Z

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_stateEmptyElement:Z

    invoke-virtual {p0, v1}, Lcom/fasterxml/aalto/out/StreamWriterBase;->_closeStartElement(Z)V

    :cond_0
    invoke-virtual {p0}, Lcom/fasterxml/aalto/out/StreamWriterBase;->inPrologOrEpilog()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0, p1}, Lcom/fasterxml/aalto/out/StreamWriterBase;->writeSpace(Ljava/lang/String;)V

    return-void

    :cond_1
    iget v1, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_vldContent:I

    if-gt v1, v0, :cond_3

    const/4 v0, 0x4

    if-nez v1, :cond_2

    :goto_0
    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/out/StreamWriterBase;->_reportInvalidContent(I)V

    goto :goto_1

    :cond_2
    iget-object v1, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_config:Lcom/fasterxml/aalto/out/WriterConfig;

    invoke-virtual {v1}, Lcom/fasterxml/aalto/out/WriterConfig;->isXml11()Z

    move-result v1

    invoke-static {p1, v1}, Lcom/fasterxml/aalto/util/TextUtil;->isAllWhitespace(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_4

    goto :goto_0

    :cond_3
    const/4 v0, 0x3

    if-ne v1, v0, :cond_4

    iget-object v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_validator:Lorg/codehaus/stax2/validation/XMLValidator;

    if-eqz v0, :cond_4

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lorg/codehaus/stax2/validation/XMLValidator;->validateText(Ljava/lang/String;Z)V

    :cond_4
    :goto_1
    :try_start_0
    iget-object v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_xmlWriter:Lcom/fasterxml/aalto/out/XmlWriter;

    invoke-virtual {v0, p1}, Lcom/fasterxml/aalto/out/XmlWriter;->writeCharacters(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    new-instance v0, Lcom/fasterxml/aalto/impl/IoStreamException;

    invoke-direct {v0, p1}, Lcom/fasterxml/aalto/impl/IoStreamException;-><init>(Ljava/io/IOException;)V

    throw v0
.end method

.method public writeCharacters([CII)V
    .locals 2

    .line 2
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_stateAnyOutput:Z

    iget-boolean v1, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_stateStartElementOpen:Z

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_stateEmptyElement:Z

    invoke-virtual {p0, v1}, Lcom/fasterxml/aalto/out/StreamWriterBase;->_closeStartElement(Z)V

    :cond_0
    invoke-virtual {p0}, Lcom/fasterxml/aalto/out/StreamWriterBase;->inPrologOrEpilog()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0, p1, p2, p3}, Lcom/fasterxml/aalto/out/StreamWriterBase;->writeSpace([CII)V

    return-void

    :cond_1
    iget v1, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_vldContent:I

    if-gt v1, v0, :cond_3

    const/4 v0, 0x4

    if-nez v1, :cond_2

    :goto_0
    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/out/StreamWriterBase;->_reportInvalidContent(I)V

    goto :goto_1

    :cond_2
    iget-object v1, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_config:Lcom/fasterxml/aalto/out/WriterConfig;

    invoke-virtual {v1}, Lcom/fasterxml/aalto/out/WriterConfig;->isXml11()Z

    move-result v1

    invoke-static {p1, p2, p3, v1}, Lcom/fasterxml/aalto/util/TextUtil;->isAllWhitespace([CIIZ)Z

    move-result v1

    if-nez v1, :cond_4

    goto :goto_0

    :cond_3
    const/4 v0, 0x3

    if-ne v1, v0, :cond_4

    iget-object v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_validator:Lorg/codehaus/stax2/validation/XMLValidator;

    if-eqz v0, :cond_4

    const/4 v1, 0x0

    invoke-virtual {v0, p1, p2, p3, v1}, Lorg/codehaus/stax2/validation/XMLValidator;->validateText([CIIZ)V

    :cond_4
    :goto_1
    if-lez p3, :cond_5

    :try_start_0
    iget-object v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_xmlWriter:Lcom/fasterxml/aalto/out/XmlWriter;

    invoke-virtual {v0, p1, p2, p3}, Lcom/fasterxml/aalto/out/XmlWriter;->writeCharacters([CII)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    :catch_0
    move-exception p1

    new-instance p2, Lcom/fasterxml/aalto/impl/IoStreamException;

    invoke-direct {p2, p1}, Lcom/fasterxml/aalto/impl/IoStreamException;-><init>(Ljava/io/IOException;)V

    throw p2

    :cond_5
    :goto_2
    return-void
.end method

.method public writeComment(Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_stateAnyOutput:Z

    iget-boolean v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_stateStartElementOpen:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_stateEmptyElement:Z

    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/out/StreamWriterBase;->_closeStartElement(Z)V

    :cond_0
    iget v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_vldContent:I

    if-nez v0, :cond_1

    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/out/StreamWriterBase;->_reportInvalidContent(I)V

    :cond_1
    :try_start_0
    iget-object v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_xmlWriter:Lcom/fasterxml/aalto/out/XmlWriter;

    invoke-virtual {v0, p1}, Lcom/fasterxml/aalto/out/XmlWriter;->writeComment(Ljava/lang/String;)I

    move-result p1

    if-ltz p1, :cond_2

    sget-object v0, Lcom/fasterxml/aalto/impl/ErrorConsts;->WERR_COMMENT_CONTENT:Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-static {v0, p1}, Lcom/fasterxml/aalto/out/StreamWriterBase;->_reportNwfContent(Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_2
    return-void

    :catch_0
    move-exception p1

    new-instance v0, Lcom/fasterxml/aalto/impl/IoStreamException;

    invoke-direct {v0, p1}, Lcom/fasterxml/aalto/impl/IoStreamException;-><init>(Ljava/io/IOException;)V

    throw v0
.end method

.method public final writeDTD(Ljava/lang/String;)V
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/fasterxml/aalto/out/StreamWriterBase;->_verifyWriteDTD()V

    const-string v0, ""

    iput-object v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_dtdRootElemName:Ljava/lang/String;

    :try_start_0
    iget-object v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_xmlWriter:Lcom/fasterxml/aalto/out/XmlWriter;

    invoke-virtual {v0, p1}, Lcom/fasterxml/aalto/out/XmlWriter;->writeDTD(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    new-instance v0, Lcom/fasterxml/aalto/impl/IoStreamException;

    invoke-direct {v0, p1}, Lcom/fasterxml/aalto/impl/IoStreamException;-><init>(Ljava/io/IOException;)V

    throw v0
.end method

.method public writeDTD(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 2
    invoke-virtual {p0}, Lcom/fasterxml/aalto/out/StreamWriterBase;->_verifyWriteDTD()V

    iput-object p1, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_dtdRootElemName:Ljava/lang/String;

    :try_start_0
    iget-object v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_xmlWriter:Lcom/fasterxml/aalto/out/XmlWriter;

    iget-object v1, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_symbols:Lcom/fasterxml/aalto/out/WNameTable;

    invoke-virtual {v1, p1}, Lcom/fasterxml/aalto/out/WNameTable;->findSymbol(Ljava/lang/String;)Lcom/fasterxml/aalto/out/WName;

    move-result-object p1

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/fasterxml/aalto/out/XmlWriter;->writeDTD(Lcom/fasterxml/aalto/out/WName;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    new-instance p2, Lcom/fasterxml/aalto/impl/IoStreamException;

    invoke-direct {p2, p1}, Lcom/fasterxml/aalto/impl/IoStreamException;-><init>(Ljava/io/IOException;)V

    throw p2
.end method

.method public writeDTD(Lorg/codehaus/stax2/DTDInfo;)V
    .locals 3

    .line 3
    invoke-interface {p1}, Lorg/codehaus/stax2/DTDInfo;->getDTDRootName()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1}, Lorg/codehaus/stax2/DTDInfo;->getDTDSystemId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1}, Lorg/codehaus/stax2/DTDInfo;->getDTDPublicId()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1}, Lorg/codehaus/stax2/DTDInfo;->getDTDInternalSubset()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, v0, v1, v2, p1}, Lcom/fasterxml/aalto/out/StreamWriterBase;->writeDTD(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public writeDecimal(Ljava/math/BigDecimal;)V
    .locals 1

    invoke-virtual {p0}, Lcom/fasterxml/aalto/out/StreamWriterBase;->valueEncoderFactory()Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory;

    move-result-object v0

    invoke-virtual {p1}, Ljava/math/BigDecimal;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory;->getScalarEncoder(Ljava/lang/String;)Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory$ScalarEncoder;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/fasterxml/aalto/out/StreamWriterBase;->writeTypedElement(Lorg/codehaus/stax2/ri/typed/AsciiValueEncoder;)V

    return-void
.end method

.method public final writeDecimalAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/math/BigDecimal;)V
    .locals 1

    invoke-virtual {p0}, Lcom/fasterxml/aalto/out/StreamWriterBase;->valueEncoderFactory()Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory;

    move-result-object v0

    invoke-virtual {p4}, Ljava/math/BigDecimal;->toString()Ljava/lang/String;

    move-result-object p4

    invoke-virtual {v0, p4}, Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory;->getScalarEncoder(Ljava/lang/String;)Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory$ScalarEncoder;

    move-result-object p4

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/fasterxml/aalto/out/StreamWriterBase;->writeTypedAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lorg/codehaus/stax2/ri/typed/AsciiValueEncoder;)V

    return-void
.end method

.method public abstract writeDefaultNamespace(Ljava/lang/String;)V
.end method

.method public writeDouble(D)V
    .locals 1

    invoke-virtual {p0}, Lcom/fasterxml/aalto/out/StreamWriterBase;->valueEncoderFactory()Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory;->getEncoder(D)Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory$DoubleEncoder;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/fasterxml/aalto/out/StreamWriterBase;->writeTypedElement(Lorg/codehaus/stax2/ri/typed/AsciiValueEncoder;)V

    return-void
.end method

.method public writeDoubleArray([DII)V
    .locals 1

    invoke-virtual {p0}, Lcom/fasterxml/aalto/out/StreamWriterBase;->valueEncoderFactory()Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory;->getEncoder([DII)Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory$DoubleArrayEncoder;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/fasterxml/aalto/out/StreamWriterBase;->writeTypedElement(Lorg/codehaus/stax2/ri/typed/AsciiValueEncoder;)V

    return-void
.end method

.method public writeDoubleArrayAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[D)V
    .locals 3

    invoke-virtual {p0}, Lcom/fasterxml/aalto/out/StreamWriterBase;->valueEncoderFactory()Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory;

    move-result-object v0

    array-length v1, p4

    const/4 v2, 0x0

    invoke-virtual {v0, p4, v2, v1}, Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory;->getEncoder([DII)Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory$DoubleArrayEncoder;

    move-result-object p4

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/fasterxml/aalto/out/StreamWriterBase;->writeTypedAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lorg/codehaus/stax2/ri/typed/AsciiValueEncoder;)V

    return-void
.end method

.method public final writeDoubleAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;D)V
    .locals 1

    invoke-virtual {p0}, Lcom/fasterxml/aalto/out/StreamWriterBase;->valueEncoderFactory()Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory;

    move-result-object v0

    invoke-virtual {v0, p4, p5}, Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory;->getEncoder(D)Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory$DoubleEncoder;

    move-result-object p4

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/fasterxml/aalto/out/StreamWriterBase;->writeTypedAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lorg/codehaus/stax2/ri/typed/AsciiValueEncoder;)V

    return-void
.end method

.method public writeEmptyElement(Ljava/lang/String;)V
    .locals 3

    .line 1
    const/4 v0, 0x0

    invoke-virtual {p0, v0, p1}, Lcom/fasterxml/aalto/out/StreamWriterBase;->_verifyStartElement(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_symbols:Lcom/fasterxml/aalto/out/WNameTable;

    invoke-virtual {v0, p1}, Lcom/fasterxml/aalto/out/WNameTable;->findSymbol(Ljava/lang/String;)Lcom/fasterxml/aalto/out/WName;

    move-result-object v0

    iget-object v1, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_validator:Lorg/codehaus/stax2/validation/XMLValidator;

    if-eqz v1, :cond_0

    const-string v2, ""

    invoke-virtual {v1, p1, v2, v2}, Lorg/codehaus/stax2/validation/XMLValidator;->validateElementStart(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    const/4 p1, 0x1

    invoke-virtual {p0, v0, p1}, Lcom/fasterxml/aalto/out/StreamWriterBase;->_writeStartTag(Lcom/fasterxml/aalto/out/WName;Z)V

    return-void
.end method

.method public abstract writeEmptyElement(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract writeEmptyElement(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public writeEndDocument()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/fasterxml/aalto/out/StreamWriterBase;->_finishDocument(Z)V

    return-void
.end method

.method public writeEndElement()V
    .locals 5

    iget-boolean v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_stateStartElementOpen:Z

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_stateEmptyElement:Z

    if-eqz v0, :cond_0

    iput-boolean v2, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_stateEmptyElement:Z

    invoke-virtual {p0, v1}, Lcom/fasterxml/aalto/out/StreamWriterBase;->_closeStartElement(Z)V

    :cond_0
    iget-object v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_state:Lcom/fasterxml/aalto/out/StreamWriterBase$State;

    sget-object v3, Lcom/fasterxml/aalto/out/StreamWriterBase$State;->TREE:Lcom/fasterxml/aalto/out/StreamWriterBase$State;

    if-eq v0, v3, :cond_1

    const-string v0, "No open start element, when trying to write end element"

    invoke-static {v0}, Lcom/fasterxml/aalto/out/StreamWriterBase;->_reportNwfStructure(Ljava/lang/String;)V

    :cond_1
    iget-object v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_currElem:Lcom/fasterxml/aalto/out/OutputElement;

    invoke-virtual {v0}, Lcom/fasterxml/aalto/out/OutputElement;->getParent()Lcom/fasterxml/aalto/out/OutputElement;

    move-result-object v3

    iput-object v3, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_currElem:Lcom/fasterxml/aalto/out/OutputElement;

    iget v3, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_poolSize:I

    const/16 v4, 0x8

    if-ge v3, v4, :cond_2

    iget-object v3, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_outputElemPool:Lcom/fasterxml/aalto/out/OutputElement;

    invoke-virtual {v0, v3}, Lcom/fasterxml/aalto/out/OutputElement;->addToPool(Lcom/fasterxml/aalto/out/OutputElement;)V

    iput-object v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_outputElemPool:Lcom/fasterxml/aalto/out/OutputElement;

    iget v3, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_poolSize:I

    add-int/2addr v3, v1

    iput v3, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_poolSize:I

    :cond_2
    :try_start_0
    iget-boolean v1, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_stateStartElementOpen:Z

    if-eqz v1, :cond_3

    iput-boolean v2, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_stateStartElementOpen:Z

    iget-object v1, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_xmlWriter:Lcom/fasterxml/aalto/out/XmlWriter;

    invoke-virtual {v1}, Lcom/fasterxml/aalto/out/XmlWriter;->writeStartTagEmptyEnd()V

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_xmlWriter:Lcom/fasterxml/aalto/out/XmlWriter;

    invoke-virtual {v0}, Lcom/fasterxml/aalto/out/OutputElement;->getName()Lcom/fasterxml/aalto/out/WName;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/fasterxml/aalto/out/XmlWriter;->writeEndTag(Lcom/fasterxml/aalto/out/WName;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v1, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_currElem:Lcom/fasterxml/aalto/out/OutputElement;

    invoke-virtual {v1}, Lcom/fasterxml/aalto/out/OutputElement;->isRoot()Z

    move-result v1

    if-eqz v1, :cond_4

    sget-object v1, Lcom/fasterxml/aalto/out/StreamWriterBase$State;->EPILOG:Lcom/fasterxml/aalto/out/StreamWriterBase$State;

    iput-object v1, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_state:Lcom/fasterxml/aalto/out/StreamWriterBase$State;

    :cond_4
    iget-object v1, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_validator:Lorg/codehaus/stax2/validation/XMLValidator;

    if-eqz v1, :cond_5

    invoke-virtual {v0}, Lcom/fasterxml/aalto/out/OutputElement;->getLocalName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/fasterxml/aalto/out/OutputElement;->getNonNullPrefix()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lcom/fasterxml/aalto/out/OutputElement;->getNonNullNamespaceURI()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v3, v0}, Lorg/codehaus/stax2/validation/XMLValidator;->validateElementEnd(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_vldContent:I

    :cond_5
    return-void

    :catch_0
    move-exception v0

    new-instance v1, Lcom/fasterxml/aalto/impl/IoStreamException;

    invoke-direct {v1, v0}, Lcom/fasterxml/aalto/impl/IoStreamException;-><init>(Ljava/io/IOException;)V

    throw v1
.end method

.method public writeEntityRef(Ljava/lang/String;)V
    .locals 2

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_stateAnyOutput:Z

    iget-boolean v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_stateStartElementOpen:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_stateEmptyElement:Z

    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/out/StreamWriterBase;->_closeStartElement(Z)V

    :cond_0
    iget-boolean v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_cfgCheckStructure:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/fasterxml/aalto/out/StreamWriterBase;->inPrologOrEpilog()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/fasterxml/aalto/impl/ErrorConsts;->WERR_PROLOG_ENTITY:Ljava/lang/String;

    invoke-static {v0}, Lcom/fasterxml/aalto/out/StreamWriterBase;->_reportNwfStructure(Ljava/lang/String;)V

    :cond_1
    iget v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_vldContent:I

    if-nez v0, :cond_2

    const/16 v0, 0x9

    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/out/StreamWriterBase;->_reportInvalidContent(I)V

    :cond_2
    :try_start_0
    iget-object v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_xmlWriter:Lcom/fasterxml/aalto/out/XmlWriter;

    iget-object v1, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_symbols:Lcom/fasterxml/aalto/out/WNameTable;

    invoke-virtual {v1, p1}, Lcom/fasterxml/aalto/out/WNameTable;->findSymbol(Ljava/lang/String;)Lcom/fasterxml/aalto/out/WName;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/fasterxml/aalto/out/XmlWriter;->writeEntityReference(Lcom/fasterxml/aalto/out/WName;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    new-instance v0, Lcom/fasterxml/aalto/impl/IoStreamException;

    invoke-direct {v0, p1}, Lcom/fasterxml/aalto/impl/IoStreamException;-><init>(Ljava/io/IOException;)V

    throw v0
.end method

.method public writeFloat(F)V
    .locals 1

    invoke-virtual {p0}, Lcom/fasterxml/aalto/out/StreamWriterBase;->valueEncoderFactory()Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory;->getEncoder(F)Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory$FloatEncoder;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/fasterxml/aalto/out/StreamWriterBase;->writeTypedElement(Lorg/codehaus/stax2/ri/typed/AsciiValueEncoder;)V

    return-void
.end method

.method public writeFloatArray([FII)V
    .locals 1

    invoke-virtual {p0}, Lcom/fasterxml/aalto/out/StreamWriterBase;->valueEncoderFactory()Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory;->getEncoder([FII)Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory$FloatArrayEncoder;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/fasterxml/aalto/out/StreamWriterBase;->writeTypedElement(Lorg/codehaus/stax2/ri/typed/AsciiValueEncoder;)V

    return-void
.end method

.method public writeFloatArrayAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[F)V
    .locals 3

    invoke-virtual {p0}, Lcom/fasterxml/aalto/out/StreamWriterBase;->valueEncoderFactory()Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory;

    move-result-object v0

    array-length v1, p4

    const/4 v2, 0x0

    invoke-virtual {v0, p4, v2, v1}, Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory;->getEncoder([FII)Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory$FloatArrayEncoder;

    move-result-object p4

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/fasterxml/aalto/out/StreamWriterBase;->writeTypedAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lorg/codehaus/stax2/ri/typed/AsciiValueEncoder;)V

    return-void
.end method

.method public final writeFloatAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;F)V
    .locals 1

    invoke-virtual {p0}, Lcom/fasterxml/aalto/out/StreamWriterBase;->valueEncoderFactory()Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory;

    move-result-object v0

    invoke-virtual {v0, p4}, Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory;->getEncoder(F)Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory$FloatEncoder;

    move-result-object p4

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/fasterxml/aalto/out/StreamWriterBase;->writeTypedAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lorg/codehaus/stax2/ri/typed/AsciiValueEncoder;)V

    return-void
.end method

.method public writeFullEndElement()V
    .locals 1

    iget-boolean v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_stateStartElementOpen:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_stateEmptyElement:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_stateEmptyElement:Z

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/out/StreamWriterBase;->_closeStartElement(Z)V

    :cond_0
    invoke-virtual {p0}, Lcom/fasterxml/aalto/out/StreamWriterBase;->writeEndElement()V

    return-void
.end method

.method public writeInt(I)V
    .locals 1

    invoke-virtual {p0}, Lcom/fasterxml/aalto/out/StreamWriterBase;->valueEncoderFactory()Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory;->getEncoder(I)Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory$IntEncoder;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/fasterxml/aalto/out/StreamWriterBase;->writeTypedElement(Lorg/codehaus/stax2/ri/typed/AsciiValueEncoder;)V

    return-void
.end method

.method public final writeIntArray([III)V
    .locals 1

    invoke-virtual {p0}, Lcom/fasterxml/aalto/out/StreamWriterBase;->valueEncoderFactory()Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory;->getEncoder([III)Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory$IntArrayEncoder;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/fasterxml/aalto/out/StreamWriterBase;->writeTypedElement(Lorg/codehaus/stax2/ri/typed/AsciiValueEncoder;)V

    return-void
.end method

.method public writeIntArrayAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[I)V
    .locals 3

    invoke-virtual {p0}, Lcom/fasterxml/aalto/out/StreamWriterBase;->valueEncoderFactory()Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory;

    move-result-object v0

    array-length v1, p4

    const/4 v2, 0x0

    invoke-virtual {v0, p4, v2, v1}, Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory;->getEncoder([III)Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory$IntArrayEncoder;

    move-result-object p4

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/fasterxml/aalto/out/StreamWriterBase;->writeTypedAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lorg/codehaus/stax2/ri/typed/AsciiValueEncoder;)V

    return-void
.end method

.method public final writeIntAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 1

    invoke-virtual {p0}, Lcom/fasterxml/aalto/out/StreamWriterBase;->valueEncoderFactory()Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory;

    move-result-object v0

    invoke-virtual {v0, p4}, Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory;->getEncoder(I)Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory$IntEncoder;

    move-result-object p4

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/fasterxml/aalto/out/StreamWriterBase;->writeTypedAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lorg/codehaus/stax2/ri/typed/AsciiValueEncoder;)V

    return-void
.end method

.method public writeInteger(Ljava/math/BigInteger;)V
    .locals 1

    invoke-virtual {p0}, Lcom/fasterxml/aalto/out/StreamWriterBase;->valueEncoderFactory()Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory;

    move-result-object v0

    invoke-virtual {p1}, Ljava/math/BigInteger;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory;->getScalarEncoder(Ljava/lang/String;)Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory$ScalarEncoder;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/fasterxml/aalto/out/StreamWriterBase;->writeTypedElement(Lorg/codehaus/stax2/ri/typed/AsciiValueEncoder;)V

    return-void
.end method

.method public final writeIntegerAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/math/BigInteger;)V
    .locals 1

    invoke-virtual {p0}, Lcom/fasterxml/aalto/out/StreamWriterBase;->valueEncoderFactory()Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory;

    move-result-object v0

    invoke-virtual {p4}, Ljava/math/BigInteger;->toString()Ljava/lang/String;

    move-result-object p4

    invoke-virtual {v0, p4}, Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory;->getScalarEncoder(Ljava/lang/String;)Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory$ScalarEncoder;

    move-result-object p4

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/fasterxml/aalto/out/StreamWriterBase;->writeTypedAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lorg/codehaus/stax2/ri/typed/AsciiValueEncoder;)V

    return-void
.end method

.method public writeLong(J)V
    .locals 1

    invoke-virtual {p0}, Lcom/fasterxml/aalto/out/StreamWriterBase;->valueEncoderFactory()Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory;->getEncoder(J)Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory$LongEncoder;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/fasterxml/aalto/out/StreamWriterBase;->writeTypedElement(Lorg/codehaus/stax2/ri/typed/AsciiValueEncoder;)V

    return-void
.end method

.method public writeLongArray([JII)V
    .locals 1

    invoke-virtual {p0}, Lcom/fasterxml/aalto/out/StreamWriterBase;->valueEncoderFactory()Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory;->getEncoder([JII)Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory$LongArrayEncoder;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/fasterxml/aalto/out/StreamWriterBase;->writeTypedElement(Lorg/codehaus/stax2/ri/typed/AsciiValueEncoder;)V

    return-void
.end method

.method public writeLongArrayAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[J)V
    .locals 3

    invoke-virtual {p0}, Lcom/fasterxml/aalto/out/StreamWriterBase;->valueEncoderFactory()Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory;

    move-result-object v0

    array-length v1, p4

    const/4 v2, 0x0

    invoke-virtual {v0, p4, v2, v1}, Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory;->getEncoder([JII)Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory$LongArrayEncoder;

    move-result-object p4

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/fasterxml/aalto/out/StreamWriterBase;->writeTypedAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lorg/codehaus/stax2/ri/typed/AsciiValueEncoder;)V

    return-void
.end method

.method public final writeLongAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V
    .locals 1

    invoke-virtual {p0}, Lcom/fasterxml/aalto/out/StreamWriterBase;->valueEncoderFactory()Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory;

    move-result-object v0

    invoke-virtual {v0, p4, p5}, Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory;->getEncoder(J)Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory$LongEncoder;

    move-result-object p4

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/fasterxml/aalto/out/StreamWriterBase;->writeTypedAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lorg/codehaus/stax2/ri/typed/AsciiValueEncoder;)V

    return-void
.end method

.method public abstract writeNamespace(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public writeProcessingInstruction(Ljava/lang/String;)V
    .locals 1

    .line 1
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/fasterxml/aalto/out/StreamWriterBase;->writeProcessingInstruction(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public writeProcessingInstruction(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 2
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_stateAnyOutput:Z

    iget-boolean v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_stateStartElementOpen:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_stateEmptyElement:Z

    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/out/StreamWriterBase;->_closeStartElement(Z)V

    :cond_0
    iget v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_vldContent:I

    if-nez v0, :cond_1

    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/out/StreamWriterBase;->_reportInvalidContent(I)V

    :cond_1
    :try_start_0
    iget-object v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_xmlWriter:Lcom/fasterxml/aalto/out/XmlWriter;

    iget-object v1, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_symbols:Lcom/fasterxml/aalto/out/WNameTable;

    invoke-virtual {v1, p1}, Lcom/fasterxml/aalto/out/WNameTable;->findSymbol(Ljava/lang/String;)Lcom/fasterxml/aalto/out/WName;

    move-result-object p1

    invoke-virtual {v0, p1, p2}, Lcom/fasterxml/aalto/out/XmlWriter;->writePI(Lcom/fasterxml/aalto/out/WName;Ljava/lang/String;)I

    move-result p1

    if-ltz p1, :cond_2

    sget-object p2, Lcom/fasterxml/aalto/impl/ErrorConsts;->WERR_PI_CONTENT:Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-static {p2, p1}, Lcom/fasterxml/aalto/out/StreamWriterBase;->_reportNwfContent(Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_2
    return-void

    :catch_0
    move-exception p1

    new-instance p2, Lcom/fasterxml/aalto/impl/IoStreamException;

    invoke-direct {p2, p1}, Lcom/fasterxml/aalto/impl/IoStreamException;-><init>(Ljava/io/IOException;)V

    throw p2
.end method

.method public writeQName(Ljavax/xml/namespace/QName;)V
    .locals 1

    invoke-virtual {p0}, Lcom/fasterxml/aalto/out/StreamWriterBase;->valueEncoderFactory()Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory;

    move-result-object v0

    invoke-virtual {p0, p1}, Lcom/fasterxml/aalto/out/StreamWriterBase;->_serializeQName(Ljavax/xml/namespace/QName;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory;->getScalarEncoder(Ljava/lang/String;)Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory$ScalarEncoder;

    move-result-object p1

    invoke-direct {p0, p1}, Lcom/fasterxml/aalto/out/StreamWriterBase;->writeTypedElement(Lorg/codehaus/stax2/ri/typed/AsciiValueEncoder;)V

    return-void
.end method

.method public final writeQNameAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljavax/xml/namespace/QName;)V
    .locals 0

    invoke-virtual {p0, p4}, Lcom/fasterxml/aalto/out/StreamWriterBase;->_serializeQName(Ljavax/xml/namespace/QName;)Ljava/lang/String;

    move-result-object p4

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/fasterxml/aalto/out/StreamWriterBase;->writeAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public writeRaw(Ljava/lang/String;)V
    .locals 3

    .line 1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_stateAnyOutput:Z

    iget-boolean v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_stateStartElementOpen:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_stateEmptyElement:Z

    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/out/StreamWriterBase;->_closeStartElement(Z)V

    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_xmlWriter:Lcom/fasterxml/aalto/out/XmlWriter;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {v0, p1, v2, v1}, Lcom/fasterxml/aalto/out/XmlWriter;->writeRaw(Ljava/lang/String;II)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    new-instance v0, Lcom/fasterxml/aalto/impl/IoStreamException;

    invoke-direct {v0, p1}, Lcom/fasterxml/aalto/impl/IoStreamException;-><init>(Ljava/io/IOException;)V

    throw v0
.end method

.method public writeRaw(Ljava/lang/String;II)V
    .locals 1

    .line 2
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_stateAnyOutput:Z

    iget-boolean v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_stateStartElementOpen:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_stateEmptyElement:Z

    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/out/StreamWriterBase;->_closeStartElement(Z)V

    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_xmlWriter:Lcom/fasterxml/aalto/out/XmlWriter;

    invoke-virtual {v0, p1, p2, p3}, Lcom/fasterxml/aalto/out/XmlWriter;->writeRaw(Ljava/lang/String;II)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    new-instance p2, Lcom/fasterxml/aalto/impl/IoStreamException;

    invoke-direct {p2, p1}, Lcom/fasterxml/aalto/impl/IoStreamException;-><init>(Ljava/io/IOException;)V

    throw p2
.end method

.method public writeRaw([CII)V
    .locals 1

    .line 3
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_stateAnyOutput:Z

    iget-boolean v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_stateStartElementOpen:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_stateEmptyElement:Z

    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/out/StreamWriterBase;->_closeStartElement(Z)V

    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_xmlWriter:Lcom/fasterxml/aalto/out/XmlWriter;

    invoke-virtual {v0, p1, p2, p3}, Lcom/fasterxml/aalto/out/XmlWriter;->writeRaw([CII)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    new-instance p2, Lcom/fasterxml/aalto/impl/IoStreamException;

    invoke-direct {p2, p1}, Lcom/fasterxml/aalto/impl/IoStreamException;-><init>(Ljava/io/IOException;)V

    throw p2
.end method

.method public writeSpace(Ljava/lang/String;)V
    .locals 1

    .line 1
    :try_start_0
    iget-object v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_xmlWriter:Lcom/fasterxml/aalto/out/XmlWriter;

    invoke-virtual {v0, p1}, Lcom/fasterxml/aalto/out/XmlWriter;->writeSpace(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    new-instance v0, Lcom/fasterxml/aalto/impl/IoStreamException;

    invoke-direct {v0, p1}, Lcom/fasterxml/aalto/impl/IoStreamException;-><init>(Ljava/io/IOException;)V

    throw v0
.end method

.method public writeSpace([CII)V
    .locals 1

    .line 2
    :try_start_0
    iget-object v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_xmlWriter:Lcom/fasterxml/aalto/out/XmlWriter;

    invoke-virtual {v0, p1, p2, p3}, Lcom/fasterxml/aalto/out/XmlWriter;->writeSpace([CII)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    new-instance p2, Lcom/fasterxml/aalto/impl/IoStreamException;

    invoke-direct {p2, p1}, Lcom/fasterxml/aalto/impl/IoStreamException;-><init>(Ljava/io/IOException;)V

    throw p2
.end method

.method public writeStartDocument()V
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_config:Lcom/fasterxml/aalto/out/WriterConfig;

    invoke-virtual {v0}, Lcom/fasterxml/aalto/out/WriterConfig;->getActualEncoding()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_config:Lcom/fasterxml/aalto/out/WriterConfig;

    const-string v1, "UTF-8"

    invoke-virtual {v0, v1}, Lcom/fasterxml/aalto/out/WriterConfig;->setActualEncodingIfNotSet(Ljava/lang/String;)V

    move-object v0, v1

    :cond_0
    const-string v1, "1.0"

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v0, v2}, Lcom/fasterxml/aalto/out/StreamWriterBase;->_writeStartDocument(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public writeStartDocument(Ljava/lang/String;)V
    .locals 2

    .line 2
    iget-object v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_config:Lcom/fasterxml/aalto/out/WriterConfig;

    invoke-virtual {v0}, Lcom/fasterxml/aalto/out/WriterConfig;->getActualEncoding()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v1}, Lcom/fasterxml/aalto/out/StreamWriterBase;->_writeStartDocument(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public writeStartDocument(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 3
    const/4 v0, 0x0

    invoke-virtual {p0, p2, p1, v0}, Lcom/fasterxml/aalto/out/StreamWriterBase;->_writeStartDocument(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public writeStartDocument(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 0

    .line 4
    if-eqz p3, :cond_0

    const-string p3, "yes"

    goto :goto_0

    :cond_0
    const-string p3, "no"

    :goto_0
    invoke-virtual {p0, p1, p2, p3}, Lcom/fasterxml/aalto/out/StreamWriterBase;->_writeStartDocument(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public writeStartElement(Ljava/lang/String;)V
    .locals 3

    .line 1
    const/4 v0, 0x0

    invoke-virtual {p0, v0, p1}, Lcom/fasterxml/aalto/out/StreamWriterBase;->_verifyStartElement(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_symbols:Lcom/fasterxml/aalto/out/WNameTable;

    invoke-virtual {v0, p1}, Lcom/fasterxml/aalto/out/WNameTable;->findSymbol(Ljava/lang/String;)Lcom/fasterxml/aalto/out/WName;

    move-result-object v0

    iget-object v1, p0, Lcom/fasterxml/aalto/out/StreamWriterBase;->_validator:Lorg/codehaus/stax2/validation/XMLValidator;

    if-eqz v1, :cond_0

    const-string v2, ""

    invoke-virtual {v1, p1, v2, v2}, Lorg/codehaus/stax2/validation/XMLValidator;->validateElementStart(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    const/4 p1, 0x0

    invoke-virtual {p0, v0, p1}, Lcom/fasterxml/aalto/out/StreamWriterBase;->_writeStartTag(Lcom/fasterxml/aalto/out/WName;Z)V

    return-void
.end method

.method public abstract writeStartElement(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract writeStartElement(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract writeTypedAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lorg/codehaus/stax2/ri/typed/AsciiValueEncoder;)V
.end method

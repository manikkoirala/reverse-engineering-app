.class public interface abstract Lcom/fasterxml/aalto/util/XmlConsts;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final CHAR_CR:C = '\r'

.field public static final CHAR_LF:C = '\n'

.field public static final CHAR_NULL:C = '\u0000'

.field public static final CHAR_SPACE:C = ' '

.field public static final MAX_UNICODE_CHAR:I = 0x10ffff

.field public static final STAX_DEFAULT_OUTPUT_ENCODING:Ljava/lang/String; = "UTF-8"

.field public static final STAX_DEFAULT_OUTPUT_VERSION:Ljava/lang/String; = "1.0"

.field public static final XML_DECL_KW_ENCODING:Ljava/lang/String; = "encoding"

.field public static final XML_DECL_KW_STANDALONE:Ljava/lang/String; = "standalone"

.field public static final XML_DECL_KW_VERSION:Ljava/lang/String; = "version"

.field public static final XML_SA_NO:Ljava/lang/String; = "no"

.field public static final XML_SA_YES:Ljava/lang/String; = "yes"

.field public static final XML_V_10:I = 0x100

.field public static final XML_V_10_STR:Ljava/lang/String; = "1.0"

.field public static final XML_V_11:I = 0x110

.field public static final XML_V_11_STR:Ljava/lang/String; = "1.1"

.field public static final XML_V_UNKNOWN:I

.class public final Lcom/fasterxml/aalto/util/BufferRecycler;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private volatile mFullBBuffer:[B

.field private volatile mFullCBuffer:[C

.field private volatile mMediumCBuffer:[C

.field private volatile mSmallCBuffer:[C


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/fasterxml/aalto/util/BufferRecycler;->mSmallCBuffer:[C

    iput-object v0, p0, Lcom/fasterxml/aalto/util/BufferRecycler;->mMediumCBuffer:[C

    iput-object v0, p0, Lcom/fasterxml/aalto/util/BufferRecycler;->mFullCBuffer:[C

    iput-object v0, p0, Lcom/fasterxml/aalto/util/BufferRecycler;->mFullBBuffer:[B

    return-void
.end method


# virtual methods
.method public declared-synchronized getFullBBuffer(I)[B
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/fasterxml/aalto/util/BufferRecycler;->mFullBBuffer:[B

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    array-length v2, v0

    if-lt v2, p1, :cond_0

    iput-object v1, p0, Lcom/fasterxml/aalto/util/BufferRecycler;->mFullBBuffer:[B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :cond_0
    monitor-exit p0

    return-object v1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized getFullCBuffer(I)[C
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/fasterxml/aalto/util/BufferRecycler;->mFullCBuffer:[C

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    array-length v2, v0

    if-lt v2, p1, :cond_0

    iput-object v1, p0, Lcom/fasterxml/aalto/util/BufferRecycler;->mFullCBuffer:[C
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :cond_0
    monitor-exit p0

    return-object v1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized getMediumCBuffer(I)[C
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/fasterxml/aalto/util/BufferRecycler;->mMediumCBuffer:[C

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    array-length v2, v0

    if-lt v2, p1, :cond_0

    iput-object v1, p0, Lcom/fasterxml/aalto/util/BufferRecycler;->mMediumCBuffer:[C
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :cond_0
    monitor-exit p0

    return-object v1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized getSmallCBuffer(I)[C
    .locals 3

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/fasterxml/aalto/util/BufferRecycler;->mSmallCBuffer:[C

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    array-length v2, v0

    if-lt v2, p1, :cond_0

    iput-object v1, p0, Lcom/fasterxml/aalto/util/BufferRecycler;->mSmallCBuffer:[C
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :cond_0
    monitor-exit p0

    return-object v1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized returnFullBBuffer([B)V
    .locals 0

    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/fasterxml/aalto/util/BufferRecycler;->mFullBBuffer:[B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized returnFullCBuffer([C)V
    .locals 0

    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/fasterxml/aalto/util/BufferRecycler;->mFullCBuffer:[C
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized returnMediumCBuffer([C)V
    .locals 0

    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/fasterxml/aalto/util/BufferRecycler;->mMediumCBuffer:[C
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public declared-synchronized returnSmallCBuffer([C)V
    .locals 0

    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/fasterxml/aalto/util/BufferRecycler;->mSmallCBuffer:[C
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

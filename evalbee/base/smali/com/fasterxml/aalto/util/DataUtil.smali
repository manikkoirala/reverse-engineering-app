.class public final Lcom/fasterxml/aalto/util/DataUtil;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field static final EMPTY_CHAR_ARRAY:[C


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    new-array v0, v0, [C

    sput-object v0, Lcom/fasterxml/aalto/util/DataUtil;->EMPTY_CHAR_ARRAY:[C

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getEmptyCharArray()[C
    .locals 1

    sget-object v0, Lcom/fasterxml/aalto/util/DataUtil;->EMPTY_CHAR_ARRAY:[C

    return-object v0
.end method

.method public static growAnyArrayBy(Ljava/lang/Object;I)Ljava/lang/Object;
    .locals 2

    if-eqz p0, :cond_0

    invoke-static {p0}, Ljava/lang/reflect/Array;->getLength(Ljava/lang/Object;)I

    move-result v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getComponentType()Ljava/lang/Class;

    move-result-object v1

    add-int/2addr p1, v0

    invoke-static {v1, p1}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;I)Ljava/lang/Object;

    move-result-object p1

    const/4 v1, 0x0

    invoke-static {p0, v1, p1, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object p1

    :cond_0
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string p1, "Null array"

    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static growArrayBy([CI)[C
    .locals 2

    .line 1
    if-nez p0, :cond_0

    new-array p0, p1, [C

    return-object p0

    :cond_0
    array-length v0, p0

    add-int/2addr p1, v0

    new-array p1, p1, [C

    const/4 v1, 0x0

    invoke-static {p0, v1, p1, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object p1
.end method

.method public static growArrayBy([II)[I
    .locals 2

    .line 2
    if-nez p0, :cond_0

    new-array p0, p1, [I

    return-object p0

    :cond_0
    array-length v0, p0

    add-int/2addr p1, v0

    new-array p1, p1, [I

    const/4 v1, 0x0

    invoke-static {p0, v1, p1, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object p1
.end method

.method public static growArrayBy([Ljava/lang/String;I)[Ljava/lang/String;
    .locals 2

    .line 3
    if-nez p0, :cond_0

    new-array p0, p1, [Ljava/lang/String;

    return-object p0

    :cond_0
    array-length v0, p0

    add-int/2addr p1, v0

    new-array p1, p1, [Ljava/lang/String;

    const/4 v1, 0x0

    invoke-static {p0, v1, p1, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object p1
.end method

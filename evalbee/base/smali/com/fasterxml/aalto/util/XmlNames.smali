.class public final Lcom/fasterxml/aalto/util/XmlNames;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static findIllegalNameChar(Ljava/lang/String;Z)I
    .locals 7

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v1

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    const v3, 0xe000

    const/4 v4, 0x1

    const v5, 0xd800

    if-lt v1, v5, :cond_3

    if-lt v1, v3, :cond_0

    goto :goto_0

    :cond_0
    const/4 v6, 0x2

    if-ge v2, v6, :cond_1

    return v0

    :cond_1
    invoke-virtual {p0, v4}, Ljava/lang/String;->charAt(I)C

    move-result v0

    invoke-static {v1, v0}, Lcom/fasterxml/aalto/util/XmlNames;->validSurrogateNameChar(CC)Z

    move-result v0

    if-nez v0, :cond_2

    return v4

    :cond_2
    move v0, v4

    goto :goto_1

    :cond_3
    :goto_0
    if-eqz p1, :cond_4

    invoke-static {v1}, Lcom/fasterxml/aalto/util/XmlChars;->is11NameStartChar(I)Z

    move-result v1

    if-nez v1, :cond_5

    return v0

    :cond_4
    invoke-static {v1}, Lcom/fasterxml/aalto/util/XmlChars;->is10NameStartChar(I)Z

    move-result v1

    if-nez v1, :cond_5

    return v0

    :cond_5
    :goto_1
    add-int/2addr v0, v4

    if-eqz p1, :cond_b

    :goto_2
    if-ge v0, v2, :cond_11

    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result p1

    if-lt p1, v5, :cond_9

    if-lt p1, v3, :cond_6

    goto :goto_3

    :cond_6
    add-int/lit8 v1, v0, 0x1

    if-lt v1, v2, :cond_7

    return v0

    :cond_7
    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v6

    invoke-static {p1, v6}, Lcom/fasterxml/aalto/util/XmlNames;->validSurrogateNameChar(CC)Z

    move-result p1

    if-nez p1, :cond_8

    return v0

    :cond_8
    move v0, v1

    goto :goto_4

    :cond_9
    :goto_3
    invoke-static {p1}, Lcom/fasterxml/aalto/util/XmlChars;->is11NameChar(I)Z

    move-result p1

    if-nez p1, :cond_a

    return v0

    :cond_a
    :goto_4
    add-int/2addr v0, v4

    goto :goto_2

    :cond_b
    :goto_5
    if-ge v0, v2, :cond_11

    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result p1

    if-lt p1, v5, :cond_f

    if-lt p1, v3, :cond_c

    goto :goto_6

    :cond_c
    add-int/lit8 v1, v0, 0x1

    if-lt v1, v2, :cond_d

    return v0

    :cond_d
    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v6

    invoke-static {p1, v6}, Lcom/fasterxml/aalto/util/XmlNames;->validSurrogateNameChar(CC)Z

    move-result p1

    if-nez p1, :cond_e

    return v0

    :cond_e
    move v0, v1

    goto :goto_7

    :cond_f
    :goto_6
    invoke-static {p1}, Lcom/fasterxml/aalto/util/XmlChars;->is10NameChar(I)Z

    move-result p1

    if-nez p1, :cond_10

    return v0

    :cond_10
    :goto_7
    add-int/2addr v0, v4

    goto :goto_5

    :cond_11
    const/4 p0, -0x1

    return p0
.end method

.method private static validSurrogateNameChar(CC)Z
    .locals 0

    const/4 p0, 0x0

    return p0
.end method

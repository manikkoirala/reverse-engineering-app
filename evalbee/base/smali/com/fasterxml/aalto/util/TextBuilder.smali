.class public final Lcom/fasterxml/aalto/util/TextBuilder;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field static final DEF_INITIAL_BUFFER_SIZE:I = 0x1f4

.field static final INT_SPACE:I = 0x20

.field public static final MAX_INDENT_SPACES:I = 0x20

.field public static final MAX_INDENT_TABS:I = 0x8

.field static final MAX_SEGMENT_LENGTH:I = 0x40000

.field private static final sIndSpaces:Ljava/lang/String; = "\n                                 "

.field private static final sIndSpacesArray:[C

.field private static final sIndSpacesStrings:[Ljava/lang/String;

.field private static final sIndTabs:Ljava/lang/String; = "\n\t\t\t\t\t\t\t\t\t"

.field private static final sIndTabsArray:[C

.field private static final sIndTabsStrings:[Ljava/lang/String;

.field static final sNoChars:[C


# instance fields
.field private final _config:Lcom/fasterxml/aalto/in/ReaderConfig;

.field private _currentSegment:[C

.field private _currentSize:I

.field private _decodeBuffer:[C

.field private _decodeEnd:I

.field private _decodePtr:I

.field private _isIndentation:Z

.field private _resultArray:[C

.field private _resultLen:I

.field private _resultString:Ljava/lang/String;

.field private _segmentSize:I

.field private _segments:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "[C>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    new-array v0, v0, [C

    sput-object v0, Lcom/fasterxml/aalto/util/TextBuilder;->sNoChars:[C

    const-string v0, "\n                                 "

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    sput-object v0, Lcom/fasterxml/aalto/util/TextBuilder;->sIndSpacesArray:[C

    array-length v0, v0

    new-array v0, v0, [Ljava/lang/String;

    sput-object v0, Lcom/fasterxml/aalto/util/TextBuilder;->sIndSpacesStrings:[Ljava/lang/String;

    const-string v0, "\n\t\t\t\t\t\t\t\t\t"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    sput-object v0, Lcom/fasterxml/aalto/util/TextBuilder;->sIndTabsArray:[C

    array-length v0, v0

    new-array v0, v0, [Ljava/lang/String;

    sput-object v0, Lcom/fasterxml/aalto/util/TextBuilder;->sIndTabsStrings:[Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Lcom/fasterxml/aalto/in/ReaderConfig;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_isIndentation:Z

    iput-object p1, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_config:Lcom/fasterxml/aalto/in/ReaderConfig;

    return-void
.end method

.method private final allocBuffer(I)[C
    .locals 1

    const/16 v0, 0x1f4

    invoke-static {v0, p1}, Ljava/lang/Math;->max(II)I

    move-result p1

    iget-object v0, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_config:Lcom/fasterxml/aalto/in/ReaderConfig;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lcom/fasterxml/aalto/in/ReaderConfig;->allocMediumCBuffer(I)[C

    move-result-object v0

    if-eqz v0, :cond_0

    return-object v0

    :cond_0
    new-array p1, p1, [C

    return-object p1
.end method

.method private buildResultArray()[C
    .locals 7

    iget-object v0, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_resultString:Ljava/lang/String;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/fasterxml/aalto/util/TextBuilder;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ge v0, v1, :cond_1

    sget-object v0, Lcom/fasterxml/aalto/util/TextBuilder;->sNoChars:[C

    return-object v0

    :cond_1
    new-array v0, v0, [C

    iget-object v1, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_segments:Ljava/util/ArrayList;

    const/4 v2, 0x0

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    move v3, v2

    move v4, v3

    :goto_0
    if-ge v3, v1, :cond_3

    iget-object v5, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_segments:Ljava/util/ArrayList;

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [C

    array-length v6, v5

    invoke-static {v5, v2, v0, v4, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    add-int/2addr v4, v6

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_2
    move v4, v2

    :cond_3
    iget-object v1, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_currentSegment:[C

    iget v3, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_currentSize:I

    invoke-static {v1, v2, v0, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v0
.end method

.method private calcNewSize(I)I
    .locals 1

    const/16 v0, 0x1f40

    if-ge p1, v0, :cond_0

    move v0, p1

    goto :goto_0

    :cond_0
    shr-int/lit8 v0, p1, 0x1

    :goto_0
    add-int/2addr p1, v0

    const/high16 v0, 0x40000

    invoke-static {p1, v0}, Ljava/lang/Math;->min(II)I

    move-result p1

    return p1
.end method

.method public static createRecyclableBuffer(Lcom/fasterxml/aalto/in/ReaderConfig;)Lcom/fasterxml/aalto/util/TextBuilder;
    .locals 1

    new-instance v0, Lcom/fasterxml/aalto/util/TextBuilder;

    invoke-direct {v0, p0}, Lcom/fasterxml/aalto/util/TextBuilder;-><init>(Lcom/fasterxml/aalto/in/ReaderConfig;)V

    return-object v0
.end method

.method private expand(I)V
    .locals 2

    iget-object v0, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_segments:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_segments:Ljava/util/ArrayList;

    :cond_0
    iget-object v0, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_currentSegment:[C

    iget-object v1, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_segments:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    array-length v0, v0

    iget v1, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_segmentSize:I

    add-int/2addr v1, v0

    iput v1, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_segmentSize:I

    invoke-direct {p0, v0}, Lcom/fasterxml/aalto/util/TextBuilder;->calcNewSize(I)I

    move-result v0

    invoke-static {p1, v0}, Ljava/lang/Math;->max(II)I

    move-result p1

    new-array p1, p1, [C

    const/4 v0, 0x0

    iput v0, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_currentSize:I

    iput-object p1, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_currentSegment:[C

    return-void
.end method

.method private final resetForDecode()V
    .locals 1

    const/4 v0, 0x0

    iput v0, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_decodePtr:I

    iget-object v0, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_segments:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/fasterxml/aalto/util/TextBuilder;->contentsAsArray()[C

    move-result-object v0

    iput-object v0, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_decodeBuffer:[C

    array-length v0, v0

    goto :goto_1

    :cond_1
    :goto_0
    iget-boolean v0, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_isIndentation:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_resultArray:[C

    iput-object v0, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_decodeBuffer:[C

    array-length v0, v0

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_currentSegment:[C

    iput-object v0, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_decodeBuffer:[C

    iget v0, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_currentSize:I

    :goto_1
    iput v0, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_decodeEnd:I

    return-void
.end method


# virtual methods
.method public append(C)V
    .locals 3

    .line 1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_resultString:Ljava/lang/String;

    iput-object v0, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_resultArray:[C

    iget-object v0, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_currentSegment:[C

    iget v1, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_currentSize:I

    array-length v2, v0

    if-lt v1, v2, :cond_0

    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/fasterxml/aalto/util/TextBuilder;->expand(I)V

    :cond_0
    iget v1, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_currentSize:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_currentSize:I

    aput-char p1, v0, v1

    return-void
.end method

.method public append(Ljava/lang/String;)V
    .locals 5

    .line 2
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_resultString:Ljava/lang/String;

    iput-object v0, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_resultArray:[C

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    iget-object v1, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_currentSegment:[C

    array-length v2, v1

    iget v3, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_currentSize:I

    sub-int/2addr v2, v3

    const/4 v4, 0x0

    if-lt v2, v0, :cond_0

    invoke-virtual {p1, v4, v0, v1, v3}, Ljava/lang/String;->getChars(II[CI)V

    iget p1, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_currentSize:I

    add-int/2addr p1, v0

    iput p1, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_currentSize:I

    goto :goto_0

    :cond_0
    if-lez v2, :cond_1

    invoke-virtual {p1, v4, v2, v1, v3}, Ljava/lang/String;->getChars(II[CI)V

    sub-int/2addr v0, v2

    :cond_1
    invoke-direct {p0, v0}, Lcom/fasterxml/aalto/util/TextBuilder;->expand(I)V

    add-int v1, v2, v0

    iget-object v3, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_currentSegment:[C

    invoke-virtual {p1, v2, v1, v3, v4}, Ljava/lang/String;->getChars(II[CI)V

    iput v0, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_currentSize:I

    :goto_0
    return-void
.end method

.method public append([CII)V
    .locals 3

    .line 3
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_resultString:Ljava/lang/String;

    iput-object v0, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_resultArray:[C

    iget-object v0, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_currentSegment:[C

    array-length v1, v0

    iget v2, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_currentSize:I

    sub-int/2addr v1, v2

    if-lt v1, p3, :cond_0

    invoke-static {p1, p2, v0, v2, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget p1, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_currentSize:I

    add-int/2addr p1, p3

    iput p1, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_currentSize:I

    goto :goto_0

    :cond_0
    if-lez v1, :cond_1

    invoke-static {p1, p2, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    add-int/2addr p2, v1

    sub-int/2addr p3, v1

    :cond_1
    invoke-direct {p0, p3}, Lcom/fasterxml/aalto/util/TextBuilder;->expand(I)V

    iget-object v0, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_currentSegment:[C

    const/4 v1, 0x0

    invoke-static {p1, p2, v0, v1, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput p3, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_currentSize:I

    :goto_0
    return-void
.end method

.method public appendSurrogate(I)V
    .locals 2

    shr-int/lit8 v0, p1, 0xa

    const v1, 0xd800

    or-int/2addr v0, v1

    int-to-char v0, v0

    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/util/TextBuilder;->append(C)V

    and-int/lit16 p1, p1, 0x3ff

    const v0, 0xdc00

    or-int/2addr p1, v0

    int-to-char p1, p1

    invoke-virtual {p0, p1}, Lcom/fasterxml/aalto/util/TextBuilder;->append(C)V

    return-void
.end method

.method public contentsAsArray()[C
    .locals 1

    iget-object v0, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_resultArray:[C

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/fasterxml/aalto/util/TextBuilder;->buildResultArray()[C

    move-result-object v0

    iput-object v0, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_resultArray:[C

    :cond_0
    return-object v0
.end method

.method public contentsAsString()Ljava/lang/String;
    .locals 7

    iget-object v0, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_resultString:Ljava/lang/String;

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_resultArray:[C

    if-eqz v0, :cond_0

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>([C)V

    iput-object v1, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_resultString:Ljava/lang/String;

    goto :goto_2

    :cond_0
    iget v0, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_segmentSize:I

    iget v1, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_currentSize:I

    const/4 v2, 0x0

    if-nez v0, :cond_2

    if-nez v1, :cond_1

    const-string v0, ""

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/lang/String;

    iget-object v3, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_currentSegment:[C

    invoke-direct {v0, v3, v2, v1}, Ljava/lang/String;-><init>([CII)V

    :goto_0
    iput-object v0, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_resultString:Ljava/lang/String;

    return-object v0

    :cond_2
    new-instance v3, Ljava/lang/StringBuilder;

    add-int/2addr v0, v1

    invoke-direct {v3, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    iget-object v0, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_segments:Ljava/util/ArrayList;

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    move v4, v2

    :goto_1
    if-ge v4, v0, :cond_3

    iget-object v5, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_segments:Ljava/util/ArrayList;

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [C

    array-length v6, v5

    invoke-virtual {v3, v5, v2, v6}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_currentSegment:[C

    invoke-virtual {v3, v0, v2, v1}, Ljava/lang/StringBuilder;->append([CII)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_resultString:Ljava/lang/String;

    :cond_4
    :goto_2
    iget-object v0, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_resultString:Ljava/lang/String;

    return-object v0
.end method

.method public contentsToArray(I[CII)I
    .locals 8

    iget-object v0, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_segments:Ljava/util/ArrayList;

    const/4 v1, 0x0

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    move v2, v1

    move v3, v2

    :goto_0
    if-ge v2, v0, :cond_2

    iget-object v4, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_segments:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [C

    array-length v5, v4

    sub-int v6, v5, p1

    const/4 v7, 0x1

    if-ge v6, v7, :cond_0

    sub-int/2addr p1, v5

    goto :goto_1

    :cond_0
    if-lt v6, p4, :cond_1

    invoke-static {v4, p1, p2, p3, p4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    add-int/2addr v3, p4

    return v3

    :cond_1
    invoke-static {v4, p1, p2, p3, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    add-int/2addr v3, v6

    add-int/2addr p3, v6

    sub-int/2addr p4, v6

    move p1, v1

    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    move v1, v3

    :cond_3
    if-lez p4, :cond_5

    iget v0, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_currentSize:I

    sub-int/2addr v0, p1

    if-le p4, v0, :cond_4

    move p4, v0

    :cond_4
    if-lez p4, :cond_5

    iget-object v0, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_currentSegment:[C

    invoke-static {v0, p1, p2, p3, p4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    add-int/2addr v1, p4

    :cond_5
    return v1
.end method

.method public decodeElements(Lorg/codehaus/stax2/typed/TypedArrayDecoder;Z)I
    .locals 6

    if-eqz p2, :cond_0

    invoke-direct {p0}, Lcom/fasterxml/aalto/util/TextBuilder;->resetForDecode()V

    :cond_0
    iget p2, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_decodePtr:I

    iget-object v0, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_decodeBuffer:[C

    :try_start_0
    iget v1, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_decodeEnd:I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_3

    const/4 v2, 0x0

    move v3, v2

    move v2, p2

    :goto_0
    if-ge p2, v1, :cond_5

    :cond_1
    :try_start_1
    aget-char v4, v0, p2
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_2

    const/16 v5, 0x20

    if-gt v4, v5, :cond_2

    add-int/lit8 p2, p2, 0x1

    if-lt p2, v1, :cond_1

    goto :goto_2

    :cond_2
    add-int/lit8 v2, p2, 0x1

    :goto_1
    if-ge v2, v1, :cond_3

    :try_start_2
    aget-char v4, v0, v2
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_0

    if-le v4, v5, :cond_3

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :catch_0
    move-exception p1

    move v4, v2

    goto :goto_3

    :cond_3
    add-int/lit8 v3, v3, 0x1

    add-int/lit8 v4, v2, 0x1

    :try_start_3
    invoke-virtual {p1, v0, p2, v2}, Lorg/codehaus/stax2/typed/TypedArrayDecoder;->decodeValue([CII)Z

    move-result v2

    if-eqz v2, :cond_4

    move v2, p2

    move p2, v4

    goto :goto_2

    :cond_4
    iput v4, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_decodePtr:I
    :try_end_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_1

    move v2, p2

    move p2, v4

    goto :goto_0

    :catch_1
    move-exception p1

    goto :goto_3

    :catch_2
    move-exception p1

    move v4, p2

    move p2, v2

    goto :goto_3

    :cond_5
    :goto_2
    :try_start_4
    iput p2, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_decodePtr:I
    :try_end_4
    .catch Ljava/lang/IllegalArgumentException; {:try_start_4 .. :try_end_4} :catch_2

    return v3

    :catch_3
    move-exception p1

    move v4, p2

    :goto_3
    new-instance v1, Ljava/lang/String;

    sub-int/2addr v4, p2

    add-int/lit8 v4, v4, -0x1

    invoke-direct {v1, v0, p2, v4}, Ljava/lang/String;-><init>([CII)V

    new-instance p2, Lorg/codehaus/stax2/typed/TypedXMLStreamException;

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p2, v1, v0, p1}, Lorg/codehaus/stax2/typed/TypedXMLStreamException;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/IllegalArgumentException;)V

    throw p2
.end method

.method public endsWith(Ljava/lang/String;)Z
    .locals 8

    iget-object v0, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_segments:Ljava/util/ArrayList;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    move v0, v1

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    const/4 v3, 0x1

    sub-int/2addr v2, v3

    iget-object v4, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_currentSegment:[C

    iget v5, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_currentSize:I

    :goto_1
    sub-int/2addr v5, v3

    :cond_1
    if-ltz v2, :cond_5

    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v6

    aget-char v7, v4, v5

    if-eq v6, v7, :cond_2

    return v1

    :cond_2
    add-int/lit8 v2, v2, -0x1

    if-nez v2, :cond_3

    goto :goto_2

    :cond_3
    add-int/lit8 v5, v5, -0x1

    if-gez v5, :cond_1

    add-int/lit8 v0, v0, -0x1

    if-gez v0, :cond_4

    return v1

    :cond_4
    iget-object v4, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_segments:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [C

    array-length v5, v4

    goto :goto_1

    :cond_5
    :goto_2
    return v3
.end method

.method public equalsString(Ljava/lang/String;)Z
    .locals 6

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {p0}, Lcom/fasterxml/aalto/util/TextBuilder;->size()I

    move-result v1

    const/4 v2, 0x0

    if-eq v0, v1, :cond_0

    return v2

    :cond_0
    iget-object v1, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_segments:Ljava/util/ArrayList;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-nez v1, :cond_1

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/fasterxml/aalto/util/TextBuilder;->contentsAsArray()[C

    move-result-object v1

    goto :goto_1

    :cond_2
    :goto_0
    iget-object v1, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_currentSegment:[C

    :goto_1
    move v3, v2

    :goto_2
    if-ge v3, v0, :cond_4

    aget-char v4, v1, v3

    invoke-virtual {p1, v3}, Ljava/lang/String;->charAt(I)C

    move-result v5

    if-eq v4, v5, :cond_3

    return v2

    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_4
    const/4 p1, 0x1

    return p1
.end method

.method public finishCurrentSegment()[C
    .locals 2

    iget-object v0, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_segments:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_segments:Ljava/util/ArrayList;

    :cond_0
    iget-object v0, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_segments:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_currentSegment:[C

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_currentSegment:[C

    array-length v0, v0

    iget v1, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_segmentSize:I

    add-int/2addr v1, v0

    iput v1, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_segmentSize:I

    invoke-direct {p0, v0}, Lcom/fasterxml/aalto/util/TextBuilder;->calcNewSize(I)I

    move-result v0

    new-array v0, v0, [C

    const/4 v1, 0x0

    iput v1, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_currentSize:I

    iput-object v0, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_currentSegment:[C

    return-object v0
.end method

.method public fireSaxCharacterEvents(Lorg/xml/sax/ContentHandler;)V
    .locals 5

    iget-object v0, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_resultArray:[C

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    iget v2, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_resultLen:I

    invoke-interface {p1, v0, v1, v2}, Lorg/xml/sax/ContentHandler;->characters([CII)V

    goto :goto_1

    :cond_0
    iget-object v0, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_segments:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    move v2, v1

    :goto_0
    if-ge v2, v0, :cond_1

    iget-object v3, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_segments:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [C

    array-length v4, v3

    invoke-interface {p1, v3, v1, v4}, Lorg/xml/sax/ContentHandler;->characters([CII)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    iget v0, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_currentSize:I

    if-lez v0, :cond_2

    iget-object v2, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_currentSegment:[C

    invoke-interface {p1, v2, v1, v0}, Lorg/xml/sax/ContentHandler;->characters([CII)V

    :cond_2
    :goto_1
    return-void
.end method

.method public fireSaxCommentEvent(Lorg/xml/sax/ext/LexicalHandler;)V
    .locals 3

    iget-object v0, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_resultArray:[C

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    iget v2, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_resultLen:I

    invoke-interface {p1, v0, v1, v2}, Lorg/xml/sax/ext/LexicalHandler;->comment([CII)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_segments:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_1

    invoke-virtual {p0}, Lcom/fasterxml/aalto/util/TextBuilder;->contentsAsArray()[C

    move-result-object v0

    array-length v2, v0

    invoke-interface {p1, v0, v1, v2}, Lorg/xml/sax/ext/LexicalHandler;->comment([CII)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_currentSegment:[C

    iget v2, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_currentSize:I

    invoke-interface {p1, v0, v1, v2}, Lorg/xml/sax/ext/LexicalHandler;->comment([CII)V

    :goto_0
    return-void
.end method

.method public fireSaxSpaceEvents(Lorg/xml/sax/ContentHandler;)V
    .locals 5

    iget-object v0, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_resultArray:[C

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    iget v2, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_resultLen:I

    invoke-interface {p1, v0, v1, v2}, Lorg/xml/sax/ContentHandler;->ignorableWhitespace([CII)V

    goto :goto_1

    :cond_0
    iget-object v0, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_segments:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    move v2, v1

    :goto_0
    if-ge v2, v0, :cond_1

    iget-object v3, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_segments:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [C

    array-length v4, v3

    invoke-interface {p1, v3, v1, v4}, Lorg/xml/sax/ContentHandler;->ignorableWhitespace([CII)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    iget v0, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_currentSize:I

    if-lez v0, :cond_2

    iget-object v2, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_currentSegment:[C

    invoke-interface {p1, v2, v1, v0}, Lorg/xml/sax/ContentHandler;->ignorableWhitespace([CII)V

    :cond_2
    :goto_1
    return-void
.end method

.method public getBufferWithoutReset()[C
    .locals 1

    iget-object v0, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_currentSegment:[C

    return-object v0
.end method

.method public getCurrentLength()I
    .locals 1

    iget v0, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_currentSize:I

    return v0
.end method

.method public getTextBuffer()[C
    .locals 1

    iget-object v0, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_segments:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/fasterxml/aalto/util/TextBuilder;->contentsAsArray()[C

    move-result-object v0

    return-object v0

    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_resultArray:[C

    if-eqz v0, :cond_2

    return-object v0

    :cond_2
    iget-object v0, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_currentSegment:[C

    return-object v0
.end method

.method public isAllWhitespace()Z
    .locals 9

    iget-boolean v0, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_isIndentation:Z

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    return v1

    :cond_0
    iget-object v0, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_segments:Ljava/util/ArrayList;

    const/16 v2, 0x20

    const/4 v3, 0x0

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    move v4, v3

    :goto_0
    if-ge v4, v0, :cond_3

    iget-object v5, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_segments:Ljava/util/ArrayList;

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [C

    array-length v6, v5

    move v7, v3

    :goto_1
    if-ge v7, v6, :cond_2

    aget-char v8, v5, v7

    if-le v8, v2, :cond_1

    return v3

    :cond_1
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_currentSegment:[C

    iget v4, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_currentSize:I

    move v5, v3

    :goto_2
    if-ge v5, v4, :cond_5

    aget-char v6, v0, v5

    if-le v6, v2, :cond_4

    return v3

    :cond_4
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    :cond_5
    return v1
.end method

.method public rawContentsTo(Ljava/io/Writer;)I
    .locals 5

    iget-object v0, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_resultArray:[C

    if-eqz v0, :cond_0

    invoke-virtual {p1, v0}, Ljava/io/Writer;->write([C)V

    iget-object p1, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_resultArray:[C

    array-length p1, p1

    return p1

    :cond_0
    iget-object v0, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_resultString:Ljava/lang/String;

    if-eqz v0, :cond_1

    invoke-virtual {p1, v0}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    iget-object p1, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_resultString:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result p1

    return p1

    :cond_1
    iget-object v0, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_segments:Ljava/util/ArrayList;

    const/4 v1, 0x0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    move v2, v1

    move v3, v2

    :goto_0
    if-ge v2, v0, :cond_3

    iget-object v4, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_segments:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [C

    invoke-virtual {p1, v4}, Ljava/io/Writer;->write([C)V

    array-length v4, v4

    add-int/2addr v3, v4

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    move v3, v1

    :cond_3
    iget v0, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_currentSize:I

    if-lez v0, :cond_4

    iget-object v2, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_currentSegment:[C

    invoke-virtual {p1, v2, v1, v0}, Ljava/io/Writer;->write([CII)V

    iget p1, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_currentSize:I

    add-int/2addr v3, p1

    :cond_4
    return v3
.end method

.method public recycle(Z)V
    .locals 2

    iget-object v0, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_config:Lcom/fasterxml/aalto/in/ReaderConfig;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_currentSegment:[C

    if-eqz v0, :cond_3

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    iput-object v0, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_resultString:Ljava/lang/String;

    iput-object v0, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_resultArray:[C

    goto :goto_0

    :cond_0
    iget p1, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_segmentSize:I

    iget v1, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_currentSize:I

    add-int/2addr p1, v1

    if-lez p1, :cond_1

    return-void

    :cond_1
    :goto_0
    iget-object p1, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_segments:Ljava/util/ArrayList;

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result p1

    if-lez p1, :cond_2

    iget-object p1, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_segments:Ljava/util/ArrayList;

    invoke-virtual {p1}, Ljava/util/ArrayList;->clear()V

    const/4 p1, 0x0

    iput p1, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_segmentSize:I

    :cond_2
    iget-object p1, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_currentSegment:[C

    iput-object v0, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_currentSegment:[C

    iget-object v0, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_config:Lcom/fasterxml/aalto/in/ReaderConfig;

    invoke-virtual {v0, p1}, Lcom/fasterxml/aalto/in/ReaderConfig;->freeMediumCBuffer([C)V

    :cond_3
    return-void
.end method

.method public resetForBinaryDecode(Lorg/codehaus/stax2/typed/Base64Variant;Lorg/codehaus/stax2/ri/typed/CharArrayBase64Decoder;Z)V
    .locals 16

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/fasterxml/aalto/util/TextBuilder;->_segments:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    iget-boolean v1, v0, Lcom/fasterxml/aalto/util/TextBuilder;->_isIndentation:Z

    if-eqz v1, :cond_1

    iget-object v5, v0, Lcom/fasterxml/aalto/util/TextBuilder;->_resultArray:[C

    const/4 v6, 0x0

    array-length v7, v5

    const/4 v8, 0x0

    move-object/from16 v2, p2

    move-object/from16 v3, p1

    move/from16 v4, p3

    invoke-virtual/range {v2 .. v8}, Lorg/codehaus/stax2/ri/typed/CharArrayBase64Decoder;->init(Lorg/codehaus/stax2/typed/Base64Variant;Z[CIILjava/util/List;)V

    return-void

    :cond_1
    iget-object v12, v0, Lcom/fasterxml/aalto/util/TextBuilder;->_currentSegment:[C

    const/4 v13, 0x0

    iget v14, v0, Lcom/fasterxml/aalto/util/TextBuilder;->_currentSize:I

    iget-object v15, v0, Lcom/fasterxml/aalto/util/TextBuilder;->_segments:Ljava/util/ArrayList;

    move-object/from16 v9, p2

    move-object/from16 v10, p1

    move/from16 v11, p3

    invoke-virtual/range {v9 .. v15}, Lorg/codehaus/stax2/ri/typed/CharArrayBase64Decoder;->init(Lorg/codehaus/stax2/typed/Base64Variant;Z[CIILjava/util/List;)V

    return-void
.end method

.method public resetWithChar(C)V
    .locals 3

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_resultString:Ljava/lang/String;

    iput-object v0, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_resultArray:[C

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_isIndentation:Z

    iget-object v1, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_segments:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_0

    iget-object v1, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_segments:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    iput v0, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_segmentSize:I

    :cond_0
    const/4 v1, 0x1

    iput v1, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_currentSize:I

    iget-object v2, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_currentSegment:[C

    if-nez v2, :cond_1

    invoke-direct {p0, v1}, Lcom/fasterxml/aalto/util/TextBuilder;->allocBuffer(I)[C

    move-result-object v1

    iput-object v1, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_currentSegment:[C

    :cond_1
    iget-object v1, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_currentSegment:[C

    aput-char p1, v1, v0

    return-void
.end method

.method public resetWithEmpty()[C
    .locals 2

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_resultString:Ljava/lang/String;

    iput-object v0, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_resultArray:[C

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_isIndentation:Z

    iget-object v1, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_segments:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_0

    iget-object v1, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_segments:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    iput v0, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_segmentSize:I

    :cond_0
    iput v0, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_currentSize:I

    iget-object v1, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_currentSegment:[C

    if-nez v1, :cond_1

    invoke-direct {p0, v0}, Lcom/fasterxml/aalto/util/TextBuilder;->allocBuffer(I)[C

    move-result-object v0

    iput-object v0, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_currentSegment:[C

    :cond_1
    iget-object v0, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_currentSegment:[C

    return-object v0
.end method

.method public resetWithIndentation(IC)V
    .locals 3

    iget-object v0, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_segments:Ljava/util/ArrayList;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_segments:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iput v1, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_segmentSize:I

    :cond_0
    const/4 v0, -0x1

    iput v0, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_currentSize:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_isIndentation:Z

    add-int/lit8 v0, p1, 0x1

    iput v0, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_resultLen:I

    const/16 v2, 0x9

    if-ne p2, v2, :cond_1

    sget-object p2, Lcom/fasterxml/aalto/util/TextBuilder;->sIndTabsArray:[C

    iput-object p2, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_resultArray:[C

    sget-object p2, Lcom/fasterxml/aalto/util/TextBuilder;->sIndTabsStrings:[Ljava/lang/String;

    aget-object v2, p2, p1

    if-nez v2, :cond_2

    const-string v2, "\n\t\t\t\t\t\t\t\t\t"

    invoke-virtual {v2, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    aput-object v2, p2, p1

    goto :goto_0

    :cond_1
    sget-object p2, Lcom/fasterxml/aalto/util/TextBuilder;->sIndSpacesArray:[C

    iput-object p2, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_resultArray:[C

    sget-object p2, Lcom/fasterxml/aalto/util/TextBuilder;->sIndSpacesStrings:[Ljava/lang/String;

    aget-object v2, p2, p1

    if-nez v2, :cond_2

    const-string v2, "\n                                 "

    invoke-virtual {v2, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    aput-object v2, p2, p1

    :cond_2
    :goto_0
    iput-object v2, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_resultString:Ljava/lang/String;

    return-void
.end method

.method public resetWithSurrogate(I)V
    .locals 4

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_resultString:Ljava/lang/String;

    iput-object v0, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_resultArray:[C

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_isIndentation:Z

    iget-object v1, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_segments:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_0

    iget-object v1, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_segments:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    iput v0, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_segmentSize:I

    :cond_0
    const/4 v1, 0x2

    iput v1, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_currentSize:I

    iget-object v2, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_currentSegment:[C

    if-nez v2, :cond_1

    invoke-direct {p0, v1}, Lcom/fasterxml/aalto/util/TextBuilder;->allocBuffer(I)[C

    move-result-object v1

    iput-object v1, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_currentSegment:[C

    :cond_1
    iget-object v1, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_currentSegment:[C

    shr-int/lit8 v2, p1, 0xa

    const v3, 0xd800

    or-int/2addr v2, v3

    int-to-char v2, v2

    aput-char v2, v1, v0

    and-int/lit16 p1, p1, 0x3ff

    const v0, 0xdc00

    or-int/2addr p1, v0

    int-to-char p1, p1

    const/4 v0, 0x1

    aput-char p1, v1, v0

    return-void
.end method

.method public setCurrentLength(I)V
    .locals 0

    iput p1, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_currentSize:I

    return-void
.end method

.method public size()I
    .locals 2

    iget v0, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_currentSize:I

    if-gez v0, :cond_0

    iget v0, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_resultLen:I

    return v0

    :cond_0
    iget v1, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_segmentSize:I

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_resultString:Ljava/lang/String;

    iput-object v0, p0, Lcom/fasterxml/aalto/util/TextBuilder;->_resultArray:[C

    invoke-virtual {p0}, Lcom/fasterxml/aalto/util/TextBuilder;->contentsAsString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

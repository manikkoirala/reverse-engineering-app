.class public Lcom/fasterxml/aalto/util/IllegalCharHandler$ReplacingIllegalCharHandler;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/fasterxml/aalto/util/IllegalCharHandler;
.implements Lcom/fasterxml/aalto/util/XmlConsts;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/fasterxml/aalto/util/IllegalCharHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ReplacingIllegalCharHandler"
.end annotation


# instance fields
.field private final replacedChar:C


# direct methods
.method public constructor <init>(C)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-char p1, p0, Lcom/fasterxml/aalto/util/IllegalCharHandler$ReplacingIllegalCharHandler;->replacedChar:C

    return-void
.end method


# virtual methods
.method public convertIllegalChar(I)C
    .locals 0

    iget-char p1, p0, Lcom/fasterxml/aalto/util/IllegalCharHandler$ReplacingIllegalCharHandler;->replacedChar:C

    return p1
.end method

.class public final enum Lcom/fasterxml/aalto/sax/SAXFeature;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/fasterxml/aalto/sax/SAXFeature;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/fasterxml/aalto/sax/SAXFeature;

.field public static final enum EXTERNAL_GENERAL_ENTITIES:Lcom/fasterxml/aalto/sax/SAXFeature;

.field public static final enum EXTERNAL_PARAMETER_ENTITIES:Lcom/fasterxml/aalto/sax/SAXFeature;

.field public static final enum IS_STANDALONE:Lcom/fasterxml/aalto/sax/SAXFeature;

.field public static final enum LEXICAL_HANDLER_PARAMETER_ENTITIES:Lcom/fasterxml/aalto/sax/SAXFeature;

.field public static final enum NAMESPACES:Lcom/fasterxml/aalto/sax/SAXFeature;

.field public static final enum NAMESPACE_PREFIXES:Lcom/fasterxml/aalto/sax/SAXFeature;

.field public static final enum RESOLVE_DTD_URIS:Lcom/fasterxml/aalto/sax/SAXFeature;

.field public static final STD_FEATURE_PREFIX:Ljava/lang/String; = "http://xml.org/sax/features/"

.field public static final enum STRING_INTERNING:Lcom/fasterxml/aalto/sax/SAXFeature;

.field public static final enum UNICODE_NORMALIZATION_CHECKING:Lcom/fasterxml/aalto/sax/SAXFeature;

.field public static final enum USE_ATTRIBUTES2:Lcom/fasterxml/aalto/sax/SAXFeature;

.field public static final enum USE_ENTITY_RESOLVER2:Lcom/fasterxml/aalto/sax/SAXFeature;

.field public static final enum USE_LOCATOR2:Lcom/fasterxml/aalto/sax/SAXFeature;

.field public static final enum VALIDATION:Lcom/fasterxml/aalto/sax/SAXFeature;

.field public static final enum XMLNS_URIS:Lcom/fasterxml/aalto/sax/SAXFeature;

.field public static final enum XML_1_1:Lcom/fasterxml/aalto/sax/SAXFeature;


# instance fields
.field private final mSuffix:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 18

    new-instance v0, Lcom/fasterxml/aalto/sax/SAXFeature;

    const/4 v1, 0x0

    const-string v2, "external-general-entities"

    const-string v3, "EXTERNAL_GENERAL_ENTITIES"

    invoke-direct {v0, v3, v1, v2}, Lcom/fasterxml/aalto/sax/SAXFeature;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/fasterxml/aalto/sax/SAXFeature;->EXTERNAL_GENERAL_ENTITIES:Lcom/fasterxml/aalto/sax/SAXFeature;

    new-instance v1, Lcom/fasterxml/aalto/sax/SAXFeature;

    const/4 v2, 0x1

    const-string v3, "external-parameter-entities"

    const-string v4, "EXTERNAL_PARAMETER_ENTITIES"

    invoke-direct {v1, v4, v2, v3}, Lcom/fasterxml/aalto/sax/SAXFeature;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/fasterxml/aalto/sax/SAXFeature;->EXTERNAL_PARAMETER_ENTITIES:Lcom/fasterxml/aalto/sax/SAXFeature;

    new-instance v2, Lcom/fasterxml/aalto/sax/SAXFeature;

    const/4 v3, 0x2

    const-string v4, "is-standalone"

    const-string v5, "IS_STANDALONE"

    invoke-direct {v2, v5, v3, v4}, Lcom/fasterxml/aalto/sax/SAXFeature;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v2, Lcom/fasterxml/aalto/sax/SAXFeature;->IS_STANDALONE:Lcom/fasterxml/aalto/sax/SAXFeature;

    new-instance v3, Lcom/fasterxml/aalto/sax/SAXFeature;

    const/4 v4, 0x3

    const-string v5, "lexical-handler/parameter-entities"

    const-string v6, "LEXICAL_HANDLER_PARAMETER_ENTITIES"

    invoke-direct {v3, v6, v4, v5}, Lcom/fasterxml/aalto/sax/SAXFeature;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v3, Lcom/fasterxml/aalto/sax/SAXFeature;->LEXICAL_HANDLER_PARAMETER_ENTITIES:Lcom/fasterxml/aalto/sax/SAXFeature;

    new-instance v4, Lcom/fasterxml/aalto/sax/SAXFeature;

    const/4 v5, 0x4

    const-string v6, "namespaces"

    const-string v7, "NAMESPACES"

    invoke-direct {v4, v7, v5, v6}, Lcom/fasterxml/aalto/sax/SAXFeature;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v4, Lcom/fasterxml/aalto/sax/SAXFeature;->NAMESPACES:Lcom/fasterxml/aalto/sax/SAXFeature;

    new-instance v5, Lcom/fasterxml/aalto/sax/SAXFeature;

    const/4 v6, 0x5

    const-string v7, "namespace-prefixes"

    const-string v8, "NAMESPACE_PREFIXES"

    invoke-direct {v5, v8, v6, v7}, Lcom/fasterxml/aalto/sax/SAXFeature;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/fasterxml/aalto/sax/SAXFeature;->NAMESPACE_PREFIXES:Lcom/fasterxml/aalto/sax/SAXFeature;

    new-instance v6, Lcom/fasterxml/aalto/sax/SAXFeature;

    const/4 v7, 0x6

    const-string v8, "resolve-dtd-uris"

    const-string v9, "RESOLVE_DTD_URIS"

    invoke-direct {v6, v9, v7, v8}, Lcom/fasterxml/aalto/sax/SAXFeature;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v6, Lcom/fasterxml/aalto/sax/SAXFeature;->RESOLVE_DTD_URIS:Lcom/fasterxml/aalto/sax/SAXFeature;

    new-instance v7, Lcom/fasterxml/aalto/sax/SAXFeature;

    const/4 v8, 0x7

    const-string v9, "string-interning"

    const-string v10, "STRING_INTERNING"

    invoke-direct {v7, v10, v8, v9}, Lcom/fasterxml/aalto/sax/SAXFeature;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v7, Lcom/fasterxml/aalto/sax/SAXFeature;->STRING_INTERNING:Lcom/fasterxml/aalto/sax/SAXFeature;

    new-instance v8, Lcom/fasterxml/aalto/sax/SAXFeature;

    const/16 v9, 0x8

    const-string v10, "unicode-normalization-checking"

    const-string v11, "UNICODE_NORMALIZATION_CHECKING"

    invoke-direct {v8, v11, v9, v10}, Lcom/fasterxml/aalto/sax/SAXFeature;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v8, Lcom/fasterxml/aalto/sax/SAXFeature;->UNICODE_NORMALIZATION_CHECKING:Lcom/fasterxml/aalto/sax/SAXFeature;

    new-instance v9, Lcom/fasterxml/aalto/sax/SAXFeature;

    const/16 v10, 0x9

    const-string v11, "use-attributes2"

    const-string v12, "USE_ATTRIBUTES2"

    invoke-direct {v9, v12, v10, v11}, Lcom/fasterxml/aalto/sax/SAXFeature;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v9, Lcom/fasterxml/aalto/sax/SAXFeature;->USE_ATTRIBUTES2:Lcom/fasterxml/aalto/sax/SAXFeature;

    new-instance v10, Lcom/fasterxml/aalto/sax/SAXFeature;

    const/16 v11, 0xa

    const-string v12, "use-locator2"

    const-string v13, "USE_LOCATOR2"

    invoke-direct {v10, v13, v11, v12}, Lcom/fasterxml/aalto/sax/SAXFeature;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v10, Lcom/fasterxml/aalto/sax/SAXFeature;->USE_LOCATOR2:Lcom/fasterxml/aalto/sax/SAXFeature;

    new-instance v11, Lcom/fasterxml/aalto/sax/SAXFeature;

    const/16 v12, 0xb

    const-string v13, "use-entity-resolver2"

    const-string v14, "USE_ENTITY_RESOLVER2"

    invoke-direct {v11, v14, v12, v13}, Lcom/fasterxml/aalto/sax/SAXFeature;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v11, Lcom/fasterxml/aalto/sax/SAXFeature;->USE_ENTITY_RESOLVER2:Lcom/fasterxml/aalto/sax/SAXFeature;

    new-instance v12, Lcom/fasterxml/aalto/sax/SAXFeature;

    const/16 v13, 0xc

    const-string v14, "validation"

    const-string v15, "VALIDATION"

    invoke-direct {v12, v15, v13, v14}, Lcom/fasterxml/aalto/sax/SAXFeature;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v12, Lcom/fasterxml/aalto/sax/SAXFeature;->VALIDATION:Lcom/fasterxml/aalto/sax/SAXFeature;

    new-instance v13, Lcom/fasterxml/aalto/sax/SAXFeature;

    const/16 v14, 0xd

    const-string v15, "xmlns-uris"

    move-object/from16 v16, v12

    const-string v12, "XMLNS_URIS"

    invoke-direct {v13, v12, v14, v15}, Lcom/fasterxml/aalto/sax/SAXFeature;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v13, Lcom/fasterxml/aalto/sax/SAXFeature;->XMLNS_URIS:Lcom/fasterxml/aalto/sax/SAXFeature;

    new-instance v14, Lcom/fasterxml/aalto/sax/SAXFeature;

    const/16 v12, 0xe

    const-string v15, "xml-1.1"

    move-object/from16 v17, v13

    const-string v13, "XML_1_1"

    invoke-direct {v14, v13, v12, v15}, Lcom/fasterxml/aalto/sax/SAXFeature;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v14, Lcom/fasterxml/aalto/sax/SAXFeature;->XML_1_1:Lcom/fasterxml/aalto/sax/SAXFeature;

    move-object/from16 v12, v16

    move-object/from16 v13, v17

    filled-new-array/range {v0 .. v14}, [Lcom/fasterxml/aalto/sax/SAXFeature;

    move-result-object v0

    sput-object v0, Lcom/fasterxml/aalto/sax/SAXFeature;->$VALUES:[Lcom/fasterxml/aalto/sax/SAXFeature;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lcom/fasterxml/aalto/sax/SAXFeature;->mSuffix:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/fasterxml/aalto/sax/SAXFeature;
    .locals 1

    const-class v0, Lcom/fasterxml/aalto/sax/SAXFeature;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/fasterxml/aalto/sax/SAXFeature;

    return-object p0
.end method

.method public static values()[Lcom/fasterxml/aalto/sax/SAXFeature;
    .locals 1

    sget-object v0, Lcom/fasterxml/aalto/sax/SAXFeature;->$VALUES:[Lcom/fasterxml/aalto/sax/SAXFeature;

    invoke-virtual {v0}, [Lcom/fasterxml/aalto/sax/SAXFeature;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/fasterxml/aalto/sax/SAXFeature;

    return-object v0
.end method


# virtual methods
.method public getSuffix()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/fasterxml/aalto/sax/SAXFeature;->mSuffix:Ljava/lang/String;

    return-object v0
.end method

.method public toExternal()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "http://xml.org/sax/features/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/fasterxml/aalto/sax/SAXFeature;->mSuffix:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

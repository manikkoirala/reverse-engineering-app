.class public Lcom/fasterxml/aalto/sax/SAXParserFactoryImpl;
.super Ljavax/xml/parsers/SAXParserFactory;
.source "SourceFile"


# instance fields
.field final mStaxFactory:Lcom/fasterxml/aalto/stax/InputFactoryImpl;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljavax/xml/parsers/SAXParserFactory;-><init>()V

    new-instance v0, Lcom/fasterxml/aalto/stax/InputFactoryImpl;

    invoke-direct {v0}, Lcom/fasterxml/aalto/stax/InputFactoryImpl;-><init>()V

    iput-object v0, p0, Lcom/fasterxml/aalto/sax/SAXParserFactoryImpl;->mStaxFactory:Lcom/fasterxml/aalto/stax/InputFactoryImpl;

    return-void
.end method

.method public static newInstance()Ljavax/xml/parsers/SAXParserFactory;
    .locals 1

    new-instance v0, Lcom/fasterxml/aalto/sax/SAXParserFactoryImpl;

    invoke-direct {v0}, Lcom/fasterxml/aalto/sax/SAXParserFactoryImpl;-><init>()V

    return-object v0
.end method


# virtual methods
.method public getFeature(Ljava/lang/String;)Z
    .locals 2

    invoke-static {p1}, Lcom/fasterxml/aalto/sax/SAXUtil;->findStdFeature(Ljava/lang/String;)Lcom/fasterxml/aalto/sax/SAXFeature;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-static {v0}, Lcom/fasterxml/aalto/sax/SAXUtil;->getFixedStdFeatureValue(Lcom/fasterxml/aalto/sax/SAXFeature;)Ljava/lang/Boolean;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    return p1

    :cond_0
    sget-object v1, Lcom/fasterxml/aalto/sax/SAXParserFactoryImpl$1;->$SwitchMap$com$fasterxml$aalto$sax$SAXFeature:[I

    invoke-virtual {v0}, Ljava/lang/Enum;->ordinal()I

    move-result v0

    aget v0, v1, v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    goto :goto_0

    :cond_1
    return v1

    :cond_2
    :goto_0
    invoke-static {p1}, Lcom/fasterxml/aalto/sax/SAXUtil;->reportUnknownFeature(Ljava/lang/String;)V

    const/4 p1, 0x0

    return p1
.end method

.method public newSAXParser()Ljavax/xml/parsers/SAXParser;
    .locals 2

    new-instance v0, Lcom/fasterxml/aalto/sax/SAXParserImpl;

    iget-object v1, p0, Lcom/fasterxml/aalto/sax/SAXParserFactoryImpl;->mStaxFactory:Lcom/fasterxml/aalto/stax/InputFactoryImpl;

    invoke-direct {v0, v1}, Lcom/fasterxml/aalto/sax/SAXParserImpl;-><init>(Lcom/fasterxml/aalto/stax/InputFactoryImpl;)V

    return-object v0
.end method

.method public setFeature(Ljava/lang/String;Z)V
    .locals 3

    invoke-static {p1}, Lcom/fasterxml/aalto/sax/SAXUtil;->findStdFeature(Ljava/lang/String;)Lcom/fasterxml/aalto/sax/SAXFeature;

    move-result-object v0

    if-eqz v0, :cond_1

    sget-object p1, Lcom/fasterxml/aalto/sax/SAXParserFactoryImpl$1;->$SwitchMap$com$fasterxml$aalto$sax$SAXFeature:[I

    invoke-virtual {v0}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    aget p1, p1, v1

    const/4 v1, 0x0

    const/4 v2, 0x1

    packed-switch p1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    move v1, p2

    goto :goto_0

    :pswitch_1
    xor-int/lit8 v1, p2, 0x1

    goto :goto_0

    :pswitch_2
    move v1, v2

    :goto_0
    :pswitch_3
    if-eqz v1, :cond_0

    return-void

    :cond_0
    new-instance p1, Lorg/xml/sax/SAXNotSupportedException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Setting std feature "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v0, " to "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string p2, " not supported"

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Lorg/xml/sax/SAXNotSupportedException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    invoke-static {p1}, Lcom/fasterxml/aalto/sax/SAXUtil;->reportUnknownFeature(Ljava/lang/String;)V

    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public setNamespaceAware(Z)V
    .locals 1

    if-eqz p1, :cond_0

    invoke-super {p0, p1}, Ljavax/xml/parsers/SAXParserFactory;->setNamespaceAware(Z)V

    return-void

    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Non-namespace-aware mode not implemented"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public setValidating(Z)V
    .locals 1

    if-nez p1, :cond_0

    invoke-super {p0, p1}, Ljavax/xml/parsers/SAXParserFactory;->setValidating(Z)V

    return-void

    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Validating mode not implemented"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

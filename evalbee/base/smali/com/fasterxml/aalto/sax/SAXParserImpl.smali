.class Lcom/fasterxml/aalto/sax/SAXParserImpl;
.super Ljavax/xml/parsers/SAXParser;
.source "SourceFile"

# interfaces
.implements Lorg/xml/sax/Parser;
.implements Lorg/xml/sax/XMLReader;
.implements Lorg/xml/sax/ext/Attributes2;
.implements Lorg/xml/sax/ext/Locator2;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/fasterxml/aalto/sax/SAXParserImpl$AttributesWrapper;,
        Lcom/fasterxml/aalto/sax/SAXParserImpl$DocHandlerWrapper;
    }
.end annotation


# instance fields
.field protected _attrCollector:Lcom/fasterxml/aalto/in/AttributeCollector;

.field private _attrCount:I

.field protected _contentHandler:Lorg/xml/sax/ContentHandler;

.field private _declHandler:Lorg/xml/sax/ext/DeclHandler;

.field protected _dtdHandler:Lorg/xml/sax/DTDHandler;

.field private _entityResolver:Lorg/xml/sax/EntityResolver;

.field private _errorHandler:Lorg/xml/sax/ErrorHandler;

.field private _lexicalHandler:Lorg/xml/sax/ext/LexicalHandler;

.field protected _scanner:Lcom/fasterxml/aalto/in/XmlScanner;

.field final _staxFactory:Lcom/fasterxml/aalto/stax/InputFactoryImpl;


# direct methods
.method public constructor <init>(Lcom/fasterxml/aalto/stax/InputFactoryImpl;)V
    .locals 0

    invoke-direct {p0}, Ljavax/xml/parsers/SAXParser;-><init>()V

    iput-object p1, p0, Lcom/fasterxml/aalto/sax/SAXParserImpl;->_staxFactory:Lcom/fasterxml/aalto/stax/InputFactoryImpl;

    return-void
.end method

.method private final fireAuxEvent(IZ)V
    .locals 2

    const/4 v0, 0x3

    if-eq p1, v0, :cond_7

    const/4 v0, 0x5

    if-eq p1, v0, :cond_6

    const/4 v0, 0x6

    if-eq p1, v0, :cond_5

    const/16 v0, 0xb

    if-eq p1, v0, :cond_4

    const/16 v0, 0xc

    if-eq p1, v0, :cond_2

    const/4 v0, -0x1

    if-ne p1, v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unexpected end-of-input in "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-eqz p2, :cond_0

    const-string p2, "tree"

    goto :goto_0

    :cond_0
    const-string p2, "prolog"

    :goto_0
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p0, p2}, Lcom/fasterxml/aalto/sax/SAXParserImpl;->throwSaxException(Ljava/lang/String;)V

    :cond_1
    new-instance p2, Ljava/lang/RuntimeException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Internal error: unexpected type, "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw p2

    :cond_2
    iget-object p1, p0, Lcom/fasterxml/aalto/sax/SAXParserImpl;->_lexicalHandler:Lorg/xml/sax/ext/LexicalHandler;

    if-eqz p1, :cond_3

    invoke-interface {p1}, Lorg/xml/sax/ext/LexicalHandler;->startCDATA()V

    iget-object p1, p0, Lcom/fasterxml/aalto/sax/SAXParserImpl;->_scanner:Lcom/fasterxml/aalto/in/XmlScanner;

    iget-object p2, p0, Lcom/fasterxml/aalto/sax/SAXParserImpl;->_contentHandler:Lorg/xml/sax/ContentHandler;

    invoke-virtual {p1, p2}, Lcom/fasterxml/aalto/in/XmlScanner;->fireSaxCharacterEvents(Lorg/xml/sax/ContentHandler;)V

    iget-object p1, p0, Lcom/fasterxml/aalto/sax/SAXParserImpl;->_lexicalHandler:Lorg/xml/sax/ext/LexicalHandler;

    invoke-interface {p1}, Lorg/xml/sax/ext/LexicalHandler;->endCDATA()V

    goto :goto_1

    :cond_3
    iget-object p1, p0, Lcom/fasterxml/aalto/sax/SAXParserImpl;->_scanner:Lcom/fasterxml/aalto/in/XmlScanner;

    iget-object p2, p0, Lcom/fasterxml/aalto/sax/SAXParserImpl;->_contentHandler:Lorg/xml/sax/ContentHandler;

    invoke-virtual {p1, p2}, Lcom/fasterxml/aalto/in/XmlScanner;->fireSaxCharacterEvents(Lorg/xml/sax/ContentHandler;)V

    goto :goto_1

    :cond_4
    iget-object p1, p0, Lcom/fasterxml/aalto/sax/SAXParserImpl;->_lexicalHandler:Lorg/xml/sax/ext/LexicalHandler;

    if-eqz p1, :cond_8

    iget-object p1, p0, Lcom/fasterxml/aalto/sax/SAXParserImpl;->_scanner:Lcom/fasterxml/aalto/in/XmlScanner;

    invoke-virtual {p1}, Lcom/fasterxml/aalto/in/XmlScanner;->getName()Lcom/fasterxml/aalto/in/PName;

    move-result-object p1

    iget-object p2, p0, Lcom/fasterxml/aalto/sax/SAXParserImpl;->_lexicalHandler:Lorg/xml/sax/ext/LexicalHandler;

    invoke-virtual {p1}, Lcom/fasterxml/aalto/in/PName;->getPrefixedName()Ljava/lang/String;

    move-result-object p1

    iget-object v0, p0, Lcom/fasterxml/aalto/sax/SAXParserImpl;->_scanner:Lcom/fasterxml/aalto/in/XmlScanner;

    invoke-virtual {v0}, Lcom/fasterxml/aalto/in/XmlScanner;->getDTDPublicId()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/fasterxml/aalto/sax/SAXParserImpl;->_scanner:Lcom/fasterxml/aalto/in/XmlScanner;

    invoke-virtual {v1}, Lcom/fasterxml/aalto/in/XmlScanner;->getDTDSystemId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2, p1, v0, v1}, Lorg/xml/sax/ext/LexicalHandler;->startDTD(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object p1, p0, Lcom/fasterxml/aalto/sax/SAXParserImpl;->_lexicalHandler:Lorg/xml/sax/ext/LexicalHandler;

    invoke-interface {p1}, Lorg/xml/sax/ext/LexicalHandler;->endDTD()V

    goto :goto_1

    :cond_5
    if-eqz p2, :cond_8

    iget-object p1, p0, Lcom/fasterxml/aalto/sax/SAXParserImpl;->_scanner:Lcom/fasterxml/aalto/in/XmlScanner;

    iget-object p2, p0, Lcom/fasterxml/aalto/sax/SAXParserImpl;->_contentHandler:Lorg/xml/sax/ContentHandler;

    invoke-virtual {p1, p2}, Lcom/fasterxml/aalto/in/XmlScanner;->fireSaxSpaceEvents(Lorg/xml/sax/ContentHandler;)V

    goto :goto_1

    :cond_6
    iget-object p1, p0, Lcom/fasterxml/aalto/sax/SAXParserImpl;->_scanner:Lcom/fasterxml/aalto/in/XmlScanner;

    iget-object p2, p0, Lcom/fasterxml/aalto/sax/SAXParserImpl;->_lexicalHandler:Lorg/xml/sax/ext/LexicalHandler;

    invoke-virtual {p1, p2}, Lcom/fasterxml/aalto/in/XmlScanner;->fireSaxCommentEvent(Lorg/xml/sax/ext/LexicalHandler;)V

    goto :goto_1

    :cond_7
    iget-object p1, p0, Lcom/fasterxml/aalto/sax/SAXParserImpl;->_scanner:Lcom/fasterxml/aalto/in/XmlScanner;

    iget-object p2, p0, Lcom/fasterxml/aalto/sax/SAXParserImpl;->_contentHandler:Lorg/xml/sax/ContentHandler;

    invoke-virtual {p1, p2}, Lcom/fasterxml/aalto/in/XmlScanner;->fireSaxPIEvent(Lorg/xml/sax/ContentHandler;)V

    :cond_8
    :goto_1
    return-void
.end method

.method private final fireEndTag()V
    .locals 2

    iget-object v0, p0, Lcom/fasterxml/aalto/sax/SAXParserImpl;->_scanner:Lcom/fasterxml/aalto/in/XmlScanner;

    iget-object v1, p0, Lcom/fasterxml/aalto/sax/SAXParserImpl;->_contentHandler:Lorg/xml/sax/ContentHandler;

    invoke-virtual {v0, v1}, Lcom/fasterxml/aalto/in/XmlScanner;->fireSaxEndElement(Lorg/xml/sax/ContentHandler;)V

    return-void
.end method

.method private final fireEvents()V
    .locals 5

    :goto_0
    iget-object v0, p0, Lcom/fasterxml/aalto/sax/SAXParserImpl;->_scanner:Lcom/fasterxml/aalto/in/XmlScanner;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/fasterxml/aalto/in/XmlScanner;->nextFromProlog(Z)I

    move-result v0

    const/4 v2, 0x0

    if-eq v0, v1, :cond_0

    invoke-direct {p0, v0, v2}, Lcom/fasterxml/aalto/sax/SAXParserImpl;->fireAuxEvent(IZ)V

    goto :goto_0

    :cond_0
    invoke-direct {p0}, Lcom/fasterxml/aalto/sax/SAXParserImpl;->fireStartTag()V

    move v0, v1

    :cond_1
    :goto_1
    iget-object v3, p0, Lcom/fasterxml/aalto/sax/SAXParserImpl;->_scanner:Lcom/fasterxml/aalto/in/XmlScanner;

    invoke-virtual {v3}, Lcom/fasterxml/aalto/in/XmlScanner;->nextFromTree()I

    move-result v3

    if-ne v3, v1, :cond_2

    invoke-direct {p0}, Lcom/fasterxml/aalto/sax/SAXParserImpl;->fireStartTag()V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    const/4 v4, 0x2

    if-ne v3, v4, :cond_5

    invoke-direct {p0}, Lcom/fasterxml/aalto/sax/SAXParserImpl;->fireEndTag()V

    add-int/lit8 v0, v0, -0x1

    if-ge v0, v1, :cond_1

    :goto_2
    iget-object v0, p0, Lcom/fasterxml/aalto/sax/SAXParserImpl;->_scanner:Lcom/fasterxml/aalto/in/XmlScanner;

    invoke-virtual {v0, v2}, Lcom/fasterxml/aalto/in/XmlScanner;->nextFromProlog(Z)I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_3

    return-void

    :cond_3
    const/4 v1, 0x6

    if-ne v0, v1, :cond_4

    goto :goto_2

    :cond_4
    invoke-direct {p0, v0, v2}, Lcom/fasterxml/aalto/sax/SAXParserImpl;->fireAuxEvent(IZ)V

    goto :goto_2

    :cond_5
    const/4 v4, 0x4

    if-ne v3, v4, :cond_6

    iget-object v3, p0, Lcom/fasterxml/aalto/sax/SAXParserImpl;->_scanner:Lcom/fasterxml/aalto/in/XmlScanner;

    iget-object v4, p0, Lcom/fasterxml/aalto/sax/SAXParserImpl;->_contentHandler:Lorg/xml/sax/ContentHandler;

    invoke-virtual {v3, v4}, Lcom/fasterxml/aalto/in/XmlScanner;->fireSaxCharacterEvents(Lorg/xml/sax/ContentHandler;)V

    goto :goto_1

    :cond_6
    invoke-direct {p0, v3, v1}, Lcom/fasterxml/aalto/sax/SAXParserImpl;->fireAuxEvent(IZ)V

    goto :goto_1
.end method

.method private final fireStartTag()V
    .locals 2

    iget-object v0, p0, Lcom/fasterxml/aalto/sax/SAXParserImpl;->_scanner:Lcom/fasterxml/aalto/in/XmlScanner;

    invoke-virtual {v0}, Lcom/fasterxml/aalto/in/XmlScanner;->getAttrCount()I

    move-result v0

    iput v0, p0, Lcom/fasterxml/aalto/sax/SAXParserImpl;->_attrCount:I

    iget-object v0, p0, Lcom/fasterxml/aalto/sax/SAXParserImpl;->_scanner:Lcom/fasterxml/aalto/in/XmlScanner;

    iget-object v1, p0, Lcom/fasterxml/aalto/sax/SAXParserImpl;->_contentHandler:Lorg/xml/sax/ContentHandler;

    invoke-virtual {v0, v1, p0}, Lcom/fasterxml/aalto/in/XmlScanner;->fireSaxStartElement(Lorg/xml/sax/ContentHandler;Lorg/xml/sax/Attributes;)V

    return-void
.end method

.method private throwSaxException(Ljava/lang/Exception;)V
    .locals 2

    .line 1
    new-instance v0, Lorg/xml/sax/SAXParseException;

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p0, p1}, Lorg/xml/sax/SAXParseException;-><init>(Ljava/lang/String;Lorg/xml/sax/Locator;Ljava/lang/Exception;)V

    invoke-virtual {v0}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-virtual {v0, p1}, Ljava/lang/Throwable;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    :cond_0
    iget-object p1, p0, Lcom/fasterxml/aalto/sax/SAXParserImpl;->_errorHandler:Lorg/xml/sax/ErrorHandler;

    if-eqz p1, :cond_1

    invoke-interface {p1, v0}, Lorg/xml/sax/ErrorHandler;->fatalError(Lorg/xml/sax/SAXParseException;)V

    :cond_1
    throw v0
.end method

.method private throwSaxException(Ljava/lang/String;)V
    .locals 1

    .line 2
    new-instance v0, Lorg/xml/sax/SAXParseException;

    invoke-direct {v0, p1, p0}, Lorg/xml/sax/SAXParseException;-><init>(Ljava/lang/String;Lorg/xml/sax/Locator;)V

    iget-object p1, p0, Lcom/fasterxml/aalto/sax/SAXParserImpl;->_errorHandler:Lorg/xml/sax/ErrorHandler;

    if-eqz p1, :cond_0

    invoke-interface {p1, v0}, Lorg/xml/sax/ErrorHandler;->fatalError(Lorg/xml/sax/SAXParseException;)V

    :cond_0
    throw v0
.end method


# virtual methods
.method public getColumnNumber()I
    .locals 1

    iget-object v0, p0, Lcom/fasterxml/aalto/sax/SAXParserImpl;->_scanner:Lcom/fasterxml/aalto/in/XmlScanner;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/fasterxml/aalto/in/XmlScanner;->getCurrentColumnNr()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, -0x1

    :goto_0
    return v0
.end method

.method public getContentHandler()Lorg/xml/sax/ContentHandler;
    .locals 1

    iget-object v0, p0, Lcom/fasterxml/aalto/sax/SAXParserImpl;->_contentHandler:Lorg/xml/sax/ContentHandler;

    return-object v0
.end method

.method public getDTDHandler()Lorg/xml/sax/DTDHandler;
    .locals 1

    iget-object v0, p0, Lcom/fasterxml/aalto/sax/SAXParserImpl;->_dtdHandler:Lorg/xml/sax/DTDHandler;

    return-object v0
.end method

.method public getEncoding()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/fasterxml/aalto/sax/SAXParserImpl;->_scanner:Lcom/fasterxml/aalto/in/XmlScanner;

    invoke-virtual {v0}, Lcom/fasterxml/aalto/in/XmlScanner;->getConfig()Lcom/fasterxml/aalto/in/ReaderConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/fasterxml/aalto/in/ReaderConfig;->getActualEncoding()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/fasterxml/aalto/in/ReaderConfig;->getXmlDeclEncoding()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/fasterxml/aalto/in/ReaderConfig;->getExternalEncoding()Ljava/lang/String;

    move-result-object v1

    :cond_0
    return-object v1
.end method

.method public getEntityResolver()Lorg/xml/sax/EntityResolver;
    .locals 1

    iget-object v0, p0, Lcom/fasterxml/aalto/sax/SAXParserImpl;->_entityResolver:Lorg/xml/sax/EntityResolver;

    return-object v0
.end method

.method public getErrorHandler()Lorg/xml/sax/ErrorHandler;
    .locals 1

    iget-object v0, p0, Lcom/fasterxml/aalto/sax/SAXParserImpl;->_errorHandler:Lorg/xml/sax/ErrorHandler;

    return-object v0
.end method

.method public getFeature(Ljava/lang/String;)Z
    .locals 2

    invoke-static {p1}, Lcom/fasterxml/aalto/sax/SAXUtil;->findStdFeature(Ljava/lang/String;)Lcom/fasterxml/aalto/sax/SAXFeature;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-static {v0}, Lcom/fasterxml/aalto/sax/SAXUtil;->getFixedStdFeatureValue(Lcom/fasterxml/aalto/sax/SAXFeature;)Ljava/lang/Boolean;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    return p1

    :cond_0
    sget-object v1, Lcom/fasterxml/aalto/sax/SAXParserImpl$1;->$SwitchMap$com$fasterxml$aalto$sax$SAXFeature:[I

    invoke-virtual {v0}, Ljava/lang/Enum;->ordinal()I

    move-result v0

    aget v0, v1, v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    goto :goto_0

    :cond_1
    return v1

    :cond_2
    :goto_0
    invoke-static {p1}, Lcom/fasterxml/aalto/sax/SAXUtil;->reportUnknownFeature(Ljava/lang/String;)V

    const/4 p1, 0x0

    return p1
.end method

.method public getIndex(Ljava/lang/String;)I
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/fasterxml/aalto/sax/SAXParserImpl;->_attrCollector:Lcom/fasterxml/aalto/in/AttributeCollector;

    if-nez v0, :cond_0

    const/4 p1, -0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    invoke-virtual {v0, v1, p1}, Lcom/fasterxml/aalto/in/AttributeCollector;->findIndex(Ljava/lang/String;Ljava/lang/String;)I

    move-result p1

    :goto_0
    return p1
.end method

.method public getIndex(Ljava/lang/String;Ljava/lang/String;)I
    .locals 1

    .line 2
    iget-object v0, p0, Lcom/fasterxml/aalto/sax/SAXParserImpl;->_attrCollector:Lcom/fasterxml/aalto/in/AttributeCollector;

    if-nez v0, :cond_0

    const/4 p1, -0x1

    goto :goto_0

    :cond_0
    invoke-virtual {v0, p1, p2}, Lcom/fasterxml/aalto/in/AttributeCollector;->findIndex(Ljava/lang/String;Ljava/lang/String;)I

    move-result p1

    :goto_0
    return p1
.end method

.method public getLength()I
    .locals 1

    iget v0, p0, Lcom/fasterxml/aalto/sax/SAXParserImpl;->_attrCount:I

    return v0
.end method

.method public getLineNumber()I
    .locals 1

    iget-object v0, p0, Lcom/fasterxml/aalto/sax/SAXParserImpl;->_scanner:Lcom/fasterxml/aalto/in/XmlScanner;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/fasterxml/aalto/in/XmlScanner;->getCurrentLineNr()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, -0x1

    :goto_0
    return v0
.end method

.method public getLocalName(I)Ljava/lang/String;
    .locals 1

    if-ltz p1, :cond_1

    iget v0, p0, Lcom/fasterxml/aalto/sax/SAXParserImpl;->_attrCount:I

    if-lt p1, v0, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/fasterxml/aalto/sax/SAXParserImpl;->_attrCollector:Lcom/fasterxml/aalto/in/AttributeCollector;

    invoke-virtual {v0, p1}, Lcom/fasterxml/aalto/in/AttributeCollector;->getName(I)Lcom/fasterxml/aalto/in/PName;

    move-result-object p1

    invoke-virtual {p1}, Lcom/fasterxml/aalto/in/PName;->getLocalName()Ljava/lang/String;

    move-result-object p1

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x0

    :goto_1
    return-object p1
.end method

.method public final getParser()Lorg/xml/sax/Parser;
    .locals 0

    return-object p0
.end method

.method public getProperty(Ljava/lang/String;)Ljava/lang/Object;
    .locals 3

    invoke-static {p1}, Lcom/fasterxml/aalto/sax/SAXUtil;->findStdProperty(Ljava/lang/String;)Lcom/fasterxml/aalto/sax/SAXProperty;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_5

    sget-object v2, Lcom/fasterxml/aalto/sax/SAXParserImpl$1;->$SwitchMap$com$fasterxml$aalto$sax$SAXProperty:[I

    invoke-virtual {v0}, Ljava/lang/Enum;->ordinal()I

    move-result v0

    aget v0, v2, v0

    const/4 v2, 0x1

    if-eq v0, v2, :cond_4

    const/4 v2, 0x2

    if-eq v0, v2, :cond_3

    const/4 v2, 0x3

    if-eq v0, v2, :cond_2

    const/4 v2, 0x4

    if-eq v0, v2, :cond_1

    const/4 v2, 0x5

    if-eq v0, v2, :cond_0

    goto :goto_0

    :cond_0
    return-object v1

    :cond_1
    iget-object p1, p0, Lcom/fasterxml/aalto/sax/SAXParserImpl;->_lexicalHandler:Lorg/xml/sax/ext/LexicalHandler;

    return-object p1

    :cond_2
    return-object v1

    :cond_3
    iget-object p1, p0, Lcom/fasterxml/aalto/sax/SAXParserImpl;->_scanner:Lcom/fasterxml/aalto/in/XmlScanner;

    invoke-virtual {p1}, Lcom/fasterxml/aalto/in/XmlScanner;->getConfig()Lcom/fasterxml/aalto/in/ReaderConfig;

    move-result-object p1

    invoke-virtual {p1}, Lcom/fasterxml/aalto/in/ReaderConfig;->getXmlDeclVersion()Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_4
    iget-object p1, p0, Lcom/fasterxml/aalto/sax/SAXParserImpl;->_declHandler:Lorg/xml/sax/ext/DeclHandler;

    return-object p1

    :cond_5
    :goto_0
    invoke-static {p1}, Lcom/fasterxml/aalto/sax/SAXUtil;->reportUnknownProperty(Ljava/lang/String;)V

    return-object v1
.end method

.method public getPublicId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/fasterxml/aalto/sax/SAXParserImpl;->_scanner:Lcom/fasterxml/aalto/in/XmlScanner;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/fasterxml/aalto/in/XmlScanner;->getInputPublicId()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public getQName(I)Ljava/lang/String;
    .locals 1

    if-ltz p1, :cond_1

    iget v0, p0, Lcom/fasterxml/aalto/sax/SAXParserImpl;->_attrCount:I

    if-lt p1, v0, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/fasterxml/aalto/sax/SAXParserImpl;->_attrCollector:Lcom/fasterxml/aalto/in/AttributeCollector;

    invoke-virtual {v0, p1}, Lcom/fasterxml/aalto/in/AttributeCollector;->getName(I)Lcom/fasterxml/aalto/in/PName;

    move-result-object p1

    invoke-virtual {p1}, Lcom/fasterxml/aalto/in/PName;->getPrefixedName()Ljava/lang/String;

    move-result-object p1

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x0

    :goto_1
    return-object p1
.end method

.method public getSystemId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/fasterxml/aalto/sax/SAXParserImpl;->_scanner:Lcom/fasterxml/aalto/in/XmlScanner;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/fasterxml/aalto/in/XmlScanner;->getInputSystemId()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public getType(I)Ljava/lang/String;
    .locals 1

    .line 1
    if-ltz p1, :cond_1

    iget v0, p0, Lcom/fasterxml/aalto/sax/SAXParserImpl;->_attrCount:I

    if-lt p1, v0, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/fasterxml/aalto/sax/SAXParserImpl;->_scanner:Lcom/fasterxml/aalto/in/XmlScanner;

    invoke-virtual {v0, p1}, Lcom/fasterxml/aalto/in/XmlScanner;->getAttrType(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x0

    :goto_1
    return-object p1
.end method

.method public getType(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .line 2
    invoke-virtual {p0, p1}, Lcom/fasterxml/aalto/sax/SAXParserImpl;->getIndex(Ljava/lang/String;)I

    move-result p1

    if-gez p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/fasterxml/aalto/sax/SAXParserImpl;->_scanner:Lcom/fasterxml/aalto/in/XmlScanner;

    invoke-virtual {v0, p1}, Lcom/fasterxml/aalto/in/XmlScanner;->getAttrType(I)Ljava/lang/String;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public getType(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 3
    invoke-virtual {p0, p1, p2}, Lcom/fasterxml/aalto/sax/SAXParserImpl;->getIndex(Ljava/lang/String;Ljava/lang/String;)I

    move-result p1

    if-gez p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    iget-object p2, p0, Lcom/fasterxml/aalto/sax/SAXParserImpl;->_scanner:Lcom/fasterxml/aalto/in/XmlScanner;

    invoke-virtual {p2, p1}, Lcom/fasterxml/aalto/in/XmlScanner;->getAttrType(I)Ljava/lang/String;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public getURI(I)Ljava/lang/String;
    .locals 1

    if-ltz p1, :cond_2

    iget v0, p0, Lcom/fasterxml/aalto/sax/SAXParserImpl;->_attrCount:I

    if-lt p1, v0, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/fasterxml/aalto/sax/SAXParserImpl;->_attrCollector:Lcom/fasterxml/aalto/in/AttributeCollector;

    invoke-virtual {v0, p1}, Lcom/fasterxml/aalto/in/AttributeCollector;->getName(I)Lcom/fasterxml/aalto/in/PName;

    move-result-object p1

    invoke-virtual {p1}, Lcom/fasterxml/aalto/in/PName;->getNsUri()Ljava/lang/String;

    move-result-object p1

    if-nez p1, :cond_1

    const-string p1, ""

    :cond_1
    return-object p1

    :cond_2
    :goto_0
    const/4 p1, 0x0

    return-object p1
.end method

.method public getValue(I)Ljava/lang/String;
    .locals 1

    .line 1
    if-ltz p1, :cond_1

    iget v0, p0, Lcom/fasterxml/aalto/sax/SAXParserImpl;->_attrCount:I

    if-lt p1, v0, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/fasterxml/aalto/sax/SAXParserImpl;->_attrCollector:Lcom/fasterxml/aalto/in/AttributeCollector;

    invoke-virtual {v0, p1}, Lcom/fasterxml/aalto/in/AttributeCollector;->getValue(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x0

    :goto_1
    return-object p1
.end method

.method public getValue(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .line 2
    invoke-virtual {p0, p1}, Lcom/fasterxml/aalto/sax/SAXParserImpl;->getIndex(Ljava/lang/String;)I

    move-result p1

    if-gez p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/fasterxml/aalto/sax/SAXParserImpl;->_attrCollector:Lcom/fasterxml/aalto/in/AttributeCollector;

    invoke-virtual {v0, p1}, Lcom/fasterxml/aalto/in/AttributeCollector;->getValue(I)Ljava/lang/String;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 3
    invoke-virtual {p0, p1, p2}, Lcom/fasterxml/aalto/sax/SAXParserImpl;->getIndex(Ljava/lang/String;Ljava/lang/String;)I

    move-result p1

    if-gez p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    iget-object p2, p0, Lcom/fasterxml/aalto/sax/SAXParserImpl;->_attrCollector:Lcom/fasterxml/aalto/in/AttributeCollector;

    invoke-virtual {p2, p1}, Lcom/fasterxml/aalto/in/AttributeCollector;->getValue(I)Ljava/lang/String;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public final getXMLReader()Lorg/xml/sax/XMLReader;
    .locals 0

    return-object p0
.end method

.method public getXMLVersion()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/fasterxml/aalto/sax/SAXParserImpl;->_scanner:Lcom/fasterxml/aalto/in/XmlScanner;

    invoke-virtual {v0}, Lcom/fasterxml/aalto/in/XmlScanner;->getConfig()Lcom/fasterxml/aalto/in/ReaderConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/fasterxml/aalto/in/ReaderConfig;->getXmlDeclVersion()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public isDeclared(I)Z
    .locals 0

    .line 1
    const/4 p1, 0x0

    return p1
.end method

.method public isDeclared(Ljava/lang/String;)Z
    .locals 0

    .line 2
    const/4 p1, 0x0

    return p1
.end method

.method public isDeclared(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 0

    .line 3
    const/4 p1, 0x0

    return p1
.end method

.method public isNamespaceAware()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public isSpecified(I)Z
    .locals 0

    .line 1
    const/4 p1, 0x1

    return p1
.end method

.method public isSpecified(Ljava/lang/String;)Z
    .locals 0

    .line 2
    const/4 p1, 0x1

    return p1
.end method

.method public isSpecified(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 0

    .line 3
    const/4 p1, 0x1

    return p1
.end method

.method public isValidating()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public parse(Ljava/lang/String;)V
    .locals 1

    .line 1
    new-instance v0, Lorg/xml/sax/InputSource;

    invoke-direct {v0, p1}, Lorg/xml/sax/InputSource;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/sax/SAXParserImpl;->parse(Lorg/xml/sax/InputSource;)V

    return-void
.end method

.method public parse(Lorg/xml/sax/InputSource;)V
    .locals 7

    .line 2
    invoke-virtual {p1}, Lorg/xml/sax/InputSource;->getEncoding()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lorg/xml/sax/InputSource;->getSystemId()Ljava/lang/String;

    move-result-object v6

    iget-object v0, p0, Lcom/fasterxml/aalto/sax/SAXParserImpl;->_staxFactory:Lcom/fasterxml/aalto/stax/InputFactoryImpl;

    invoke-virtual {p1}, Lorg/xml/sax/InputSource;->getPublicId()Ljava/lang/String;

    move-result-object v2

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, v6

    invoke-virtual/range {v0 .. v5}, Lcom/fasterxml/aalto/stax/InputFactoryImpl;->getNonSharedConfig(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)Lcom/fasterxml/aalto/in/ReaderConfig;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/fasterxml/aalto/in/ReaderConfig;->doParseLazily(Z)V

    invoke-virtual {p1}, Lorg/xml/sax/InputSource;->getCharacterStream()Ljava/io/Reader;

    move-result-object v2

    const/4 v3, 0x0

    if-nez v2, :cond_2

    invoke-virtual {p1}, Lorg/xml/sax/InputSource;->getByteStream()Ljava/io/InputStream;

    move-result-object p1

    if-nez p1, :cond_3

    if-eqz v6, :cond_1

    :try_start_0
    invoke-static {v6}, Lcom/fasterxml/aalto/util/URLUtil;->urlFromSystemId(Ljava/lang/String;)Ljava/net/URL;

    move-result-object p1

    invoke-static {p1}, Lcom/fasterxml/aalto/util/URLUtil;->inputStreamFromURL(Ljava/net/URL;)Ljava/io/InputStream;

    move-result-object p1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    new-instance v0, Lorg/xml/sax/SAXException;

    invoke-direct {v0, p1}, Lorg/xml/sax/SAXException;-><init>(Ljava/lang/Exception;)V

    invoke-virtual {v0}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-virtual {v0, p1}, Ljava/lang/Throwable;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    :cond_0
    throw v0

    :cond_1
    new-instance p1, Lorg/xml/sax/SAXException;

    const-string v0, "Invalid InputSource passed: neither character or byte stream passed, nor system id specified"

    invoke-direct {p1, v0}, Lorg/xml/sax/SAXException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_2
    move-object p1, v3

    :cond_3
    :goto_0
    iget-object v4, p0, Lcom/fasterxml/aalto/sax/SAXParserImpl;->_contentHandler:Lorg/xml/sax/ContentHandler;

    if-eqz v4, :cond_4

    invoke-interface {v4, p0}, Lorg/xml/sax/ContentHandler;->setDocumentLocator(Lorg/xml/sax/Locator;)V

    iget-object v4, p0, Lcom/fasterxml/aalto/sax/SAXParserImpl;->_contentHandler:Lorg/xml/sax/ContentHandler;

    invoke-interface {v4}, Lorg/xml/sax/ContentHandler;->startDocument()V

    :cond_4
    if-eqz v2, :cond_5

    :try_start_1
    invoke-static {v0, v2}, Lcom/fasterxml/aalto/in/CharSourceBootstrapper;->construct(Lcom/fasterxml/aalto/in/ReaderConfig;Ljava/io/Reader;)Lcom/fasterxml/aalto/in/CharSourceBootstrapper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/fasterxml/aalto/in/CharSourceBootstrapper;->bootstrap()Lcom/fasterxml/aalto/in/XmlScanner;

    move-result-object v0

    :goto_1
    iput-object v0, p0, Lcom/fasterxml/aalto/sax/SAXParserImpl;->_scanner:Lcom/fasterxml/aalto/in/XmlScanner;

    goto :goto_2

    :cond_5
    invoke-static {v0, p1}, Lcom/fasterxml/aalto/in/ByteSourceBootstrapper;->construct(Lcom/fasterxml/aalto/in/ReaderConfig;Ljava/io/InputStream;)Lcom/fasterxml/aalto/in/ByteSourceBootstrapper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/fasterxml/aalto/in/ByteSourceBootstrapper;->bootstrap()Lcom/fasterxml/aalto/in/XmlScanner;

    move-result-object v0

    goto :goto_1

    :goto_2
    iget-object v0, p0, Lcom/fasterxml/aalto/sax/SAXParserImpl;->_scanner:Lcom/fasterxml/aalto/in/XmlScanner;

    invoke-virtual {v0}, Lcom/fasterxml/aalto/in/XmlScanner;->getAttrCollector()Lcom/fasterxml/aalto/in/AttributeCollector;

    move-result-object v0

    iput-object v0, p0, Lcom/fasterxml/aalto/sax/SAXParserImpl;->_attrCollector:Lcom/fasterxml/aalto/in/AttributeCollector;

    invoke-direct {p0}, Lcom/fasterxml/aalto/sax/SAXParserImpl;->fireEvents()V
    :try_end_1
    .catch Ljavax/xml/stream/XMLStreamException; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v0, p0, Lcom/fasterxml/aalto/sax/SAXParserImpl;->_contentHandler:Lorg/xml/sax/ContentHandler;

    if-eqz v0, :cond_6

    invoke-interface {v0}, Lorg/xml/sax/ContentHandler;->endDocument()V

    :cond_6
    iget-object v0, p0, Lcom/fasterxml/aalto/sax/SAXParserImpl;->_scanner:Lcom/fasterxml/aalto/in/XmlScanner;

    if-eqz v0, :cond_7

    :try_start_2
    invoke-virtual {v0, v1}, Lcom/fasterxml/aalto/in/XmlScanner;->close(Z)V
    :try_end_2
    .catch Ljavax/xml/stream/XMLStreamException; {:try_start_2 .. :try_end_2} :catch_1

    :catch_1
    iput-object v3, p0, Lcom/fasterxml/aalto/sax/SAXParserImpl;->_scanner:Lcom/fasterxml/aalto/in/XmlScanner;

    :cond_7
    if-eqz v2, :cond_8

    :try_start_3
    invoke-virtual {v2}, Ljava/io/Reader;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    :catch_2
    :cond_8
    if-eqz p1, :cond_c

    :goto_3
    :try_start_4
    invoke-virtual {p1}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_6

    goto :goto_4

    :catchall_0
    move-exception v0

    goto :goto_5

    :catch_3
    move-exception v0

    :try_start_5
    invoke-direct {p0, v0}, Lcom/fasterxml/aalto/sax/SAXParserImpl;->throwSaxException(Ljava/lang/Exception;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    iget-object v0, p0, Lcom/fasterxml/aalto/sax/SAXParserImpl;->_contentHandler:Lorg/xml/sax/ContentHandler;

    if-eqz v0, :cond_9

    invoke-interface {v0}, Lorg/xml/sax/ContentHandler;->endDocument()V

    :cond_9
    iget-object v0, p0, Lcom/fasterxml/aalto/sax/SAXParserImpl;->_scanner:Lcom/fasterxml/aalto/in/XmlScanner;

    if-eqz v0, :cond_a

    :try_start_6
    invoke-virtual {v0, v1}, Lcom/fasterxml/aalto/in/XmlScanner;->close(Z)V
    :try_end_6
    .catch Ljavax/xml/stream/XMLStreamException; {:try_start_6 .. :try_end_6} :catch_4

    :catch_4
    iput-object v3, p0, Lcom/fasterxml/aalto/sax/SAXParserImpl;->_scanner:Lcom/fasterxml/aalto/in/XmlScanner;

    :cond_a
    if-eqz v2, :cond_b

    :try_start_7
    invoke-virtual {v2}, Ljava/io/Reader;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_5

    :catch_5
    :cond_b
    if-eqz p1, :cond_c

    goto :goto_3

    :catch_6
    :cond_c
    :goto_4
    return-void

    :goto_5
    iget-object v4, p0, Lcom/fasterxml/aalto/sax/SAXParserImpl;->_contentHandler:Lorg/xml/sax/ContentHandler;

    if-eqz v4, :cond_d

    invoke-interface {v4}, Lorg/xml/sax/ContentHandler;->endDocument()V

    :cond_d
    iget-object v4, p0, Lcom/fasterxml/aalto/sax/SAXParserImpl;->_scanner:Lcom/fasterxml/aalto/in/XmlScanner;

    if-eqz v4, :cond_e

    :try_start_8
    invoke-virtual {v4, v1}, Lcom/fasterxml/aalto/in/XmlScanner;->close(Z)V
    :try_end_8
    .catch Ljavax/xml/stream/XMLStreamException; {:try_start_8 .. :try_end_8} :catch_7

    :catch_7
    iput-object v3, p0, Lcom/fasterxml/aalto/sax/SAXParserImpl;->_scanner:Lcom/fasterxml/aalto/in/XmlScanner;

    :cond_e
    if-eqz v2, :cond_f

    :try_start_9
    invoke-virtual {v2}, Ljava/io/Reader;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_8

    :catch_8
    :cond_f
    if-eqz p1, :cond_10

    :try_start_a
    invoke-virtual {p1}, Ljava/io/InputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_9

    :catch_9
    :cond_10
    throw v0
.end method

.method public parse(Lorg/xml/sax/InputSource;Lorg/xml/sax/HandlerBase;)V
    .locals 1

    .line 3
    if-eqz p2, :cond_3

    iget-object v0, p0, Lcom/fasterxml/aalto/sax/SAXParserImpl;->_contentHandler:Lorg/xml/sax/ContentHandler;

    if-nez v0, :cond_0

    invoke-virtual {p0, p2}, Lcom/fasterxml/aalto/sax/SAXParserImpl;->setDocumentHandler(Lorg/xml/sax/DocumentHandler;)V

    :cond_0
    iget-object v0, p0, Lcom/fasterxml/aalto/sax/SAXParserImpl;->_entityResolver:Lorg/xml/sax/EntityResolver;

    if-nez v0, :cond_1

    invoke-virtual {p0, p2}, Lcom/fasterxml/aalto/sax/SAXParserImpl;->setEntityResolver(Lorg/xml/sax/EntityResolver;)V

    :cond_1
    iget-object v0, p0, Lcom/fasterxml/aalto/sax/SAXParserImpl;->_errorHandler:Lorg/xml/sax/ErrorHandler;

    if-nez v0, :cond_2

    invoke-virtual {p0, p2}, Lcom/fasterxml/aalto/sax/SAXParserImpl;->setErrorHandler(Lorg/xml/sax/ErrorHandler;)V

    :cond_2
    iget-object v0, p0, Lcom/fasterxml/aalto/sax/SAXParserImpl;->_dtdHandler:Lorg/xml/sax/DTDHandler;

    if-nez v0, :cond_3

    invoke-virtual {p0, p2}, Lcom/fasterxml/aalto/sax/SAXParserImpl;->setDTDHandler(Lorg/xml/sax/DTDHandler;)V

    :cond_3
    invoke-virtual {p0, p1}, Lcom/fasterxml/aalto/sax/SAXParserImpl;->parse(Lorg/xml/sax/InputSource;)V

    return-void
.end method

.method public parse(Lorg/xml/sax/InputSource;Lorg/xml/sax/helpers/DefaultHandler;)V
    .locals 1

    .line 4
    if-eqz p2, :cond_3

    iget-object v0, p0, Lcom/fasterxml/aalto/sax/SAXParserImpl;->_contentHandler:Lorg/xml/sax/ContentHandler;

    if-nez v0, :cond_0

    invoke-virtual {p0, p2}, Lcom/fasterxml/aalto/sax/SAXParserImpl;->setContentHandler(Lorg/xml/sax/ContentHandler;)V

    :cond_0
    iget-object v0, p0, Lcom/fasterxml/aalto/sax/SAXParserImpl;->_entityResolver:Lorg/xml/sax/EntityResolver;

    if-nez v0, :cond_1

    invoke-virtual {p0, p2}, Lcom/fasterxml/aalto/sax/SAXParserImpl;->setEntityResolver(Lorg/xml/sax/EntityResolver;)V

    :cond_1
    iget-object v0, p0, Lcom/fasterxml/aalto/sax/SAXParserImpl;->_errorHandler:Lorg/xml/sax/ErrorHandler;

    if-nez v0, :cond_2

    invoke-virtual {p0, p2}, Lcom/fasterxml/aalto/sax/SAXParserImpl;->setErrorHandler(Lorg/xml/sax/ErrorHandler;)V

    :cond_2
    iget-object v0, p0, Lcom/fasterxml/aalto/sax/SAXParserImpl;->_dtdHandler:Lorg/xml/sax/DTDHandler;

    if-nez v0, :cond_3

    invoke-virtual {p0, p2}, Lcom/fasterxml/aalto/sax/SAXParserImpl;->setDTDHandler(Lorg/xml/sax/DTDHandler;)V

    :cond_3
    invoke-virtual {p0, p1}, Lcom/fasterxml/aalto/sax/SAXParserImpl;->parse(Lorg/xml/sax/InputSource;)V

    return-void
.end method

.method public setContentHandler(Lorg/xml/sax/ContentHandler;)V
    .locals 0

    iput-object p1, p0, Lcom/fasterxml/aalto/sax/SAXParserImpl;->_contentHandler:Lorg/xml/sax/ContentHandler;

    return-void
.end method

.method public setDTDHandler(Lorg/xml/sax/DTDHandler;)V
    .locals 0

    iput-object p1, p0, Lcom/fasterxml/aalto/sax/SAXParserImpl;->_dtdHandler:Lorg/xml/sax/DTDHandler;

    return-void
.end method

.method public setDocumentHandler(Lorg/xml/sax/DocumentHandler;)V
    .locals 1

    new-instance v0, Lcom/fasterxml/aalto/sax/SAXParserImpl$DocHandlerWrapper;

    invoke-direct {v0, p1}, Lcom/fasterxml/aalto/sax/SAXParserImpl$DocHandlerWrapper;-><init>(Lorg/xml/sax/DocumentHandler;)V

    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/sax/SAXParserImpl;->setContentHandler(Lorg/xml/sax/ContentHandler;)V

    return-void
.end method

.method public setEntityResolver(Lorg/xml/sax/EntityResolver;)V
    .locals 0

    iput-object p1, p0, Lcom/fasterxml/aalto/sax/SAXParserImpl;->_entityResolver:Lorg/xml/sax/EntityResolver;

    return-void
.end method

.method public setErrorHandler(Lorg/xml/sax/ErrorHandler;)V
    .locals 0

    iput-object p1, p0, Lcom/fasterxml/aalto/sax/SAXParserImpl;->_errorHandler:Lorg/xml/sax/ErrorHandler;

    return-void
.end method

.method public setFeature(Ljava/lang/String;Z)V
    .locals 0

    invoke-static {p1}, Lcom/fasterxml/aalto/sax/SAXUtil;->findStdFeature(Ljava/lang/String;)Lcom/fasterxml/aalto/sax/SAXFeature;

    move-result-object p2

    if-eqz p2, :cond_0

    goto :goto_0

    :cond_0
    invoke-static {p1}, Lcom/fasterxml/aalto/sax/SAXUtil;->reportUnknownFeature(Ljava/lang/String;)V

    :goto_0
    return-void
.end method

.method public setLocale(Ljava/util/Locale;)V
    .locals 0

    return-void
.end method

.method public setProperty(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 2

    invoke-static {p1}, Lcom/fasterxml/aalto/sax/SAXUtil;->findStdProperty(Ljava/lang/String;)Lcom/fasterxml/aalto/sax/SAXProperty;

    move-result-object v0

    if-eqz v0, :cond_6

    sget-object v1, Lcom/fasterxml/aalto/sax/SAXParserImpl$1;->$SwitchMap$com$fasterxml$aalto$sax$SAXProperty:[I

    invoke-virtual {v0}, Ljava/lang/Enum;->ordinal()I

    move-result v0

    aget v0, v1, v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_5

    const/4 v1, 0x2

    if-eq v0, v1, :cond_3

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x4

    if-eq v0, v1, :cond_1

    const/4 p2, 0x5

    if-eq v0, p2, :cond_0

    goto :goto_1

    :cond_0
    return-void

    :cond_1
    check-cast p2, Lorg/xml/sax/ext/LexicalHandler;

    iput-object p2, p0, Lcom/fasterxml/aalto/sax/SAXParserImpl;->_lexicalHandler:Lorg/xml/sax/ext/LexicalHandler;

    :cond_2
    return-void

    :cond_3
    iget-object p1, p0, Lcom/fasterxml/aalto/sax/SAXParserImpl;->_scanner:Lcom/fasterxml/aalto/in/XmlScanner;

    invoke-virtual {p1}, Lcom/fasterxml/aalto/in/XmlScanner;->getConfig()Lcom/fasterxml/aalto/in/ReaderConfig;

    move-result-object p1

    if-nez p2, :cond_4

    const/4 p2, 0x0

    goto :goto_0

    :cond_4
    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    :goto_0
    invoke-virtual {p1, p2}, Lcom/fasterxml/aalto/in/ReaderConfig;->setXmlVersion(Ljava/lang/String;)V

    return-void

    :cond_5
    check-cast p2, Lorg/xml/sax/ext/DeclHandler;

    iput-object p2, p0, Lcom/fasterxml/aalto/sax/SAXParserImpl;->_declHandler:Lorg/xml/sax/ext/DeclHandler;

    return-void

    :cond_6
    :goto_1
    invoke-static {p1}, Lcom/fasterxml/aalto/sax/SAXUtil;->reportUnknownFeature(Ljava/lang/String;)V

    return-void
.end method

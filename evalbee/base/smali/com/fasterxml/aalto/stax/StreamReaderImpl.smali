.class public Lcom/fasterxml/aalto/stax/StreamReaderImpl;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/codehaus/stax2/XMLStreamReader2;
.implements Lorg/codehaus/stax2/AttributeInfo;
.implements Lorg/codehaus/stax2/DTDInfo;
.implements Lorg/codehaus/stax2/LocationInfo;


# static fields
.field private static final MASK_GET_ELEMENT_TEXT:I = 0x1250

.field private static final MASK_GET_TEXT:I = 0x1a70

.field private static final MASK_GET_TEXT_WITH_WRITER:I = 0x1a78

.field private static final MASK_GET_TEXT_XXX:I = 0x1070

.field private static final MASK_TYPED_ACCESS_ARRAY:I = 0x1056

.field private static final MASK_TYPED_ACCESS_BINARY:I = 0x1052

.field static final STATE_CLOSED:I = 0x3

.field static final STATE_EPILOG:I = 0x2

.field static final STATE_PROLOG:I = 0x0

.field static final STATE_TREE:I = 0x1


# instance fields
.field protected _attrCount:I

.field protected _base64Decoder:Lorg/codehaus/stax2/ri/typed/CharArrayBase64Decoder;

.field protected final _cfgCoalesceText:Z

.field protected final _cfgReportTextAsChars:Z

.field protected _currName:Lcom/fasterxml/aalto/in/PName;

.field protected _currToken:I

.field protected _decoderFactory:Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;

.field protected _dtdRootName:Lcom/fasterxml/aalto/in/PName;

.field protected _parseState:I

.field protected final _scanner:Lcom/fasterxml/aalto/in/XmlScanner;


# direct methods
.method public constructor <init>(Lcom/fasterxml/aalto/in/XmlScanner;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_base64Decoder:Lorg/codehaus/stax2/ri/typed/CharArrayBase64Decoder;

    iput-object p1, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_scanner:Lcom/fasterxml/aalto/in/XmlScanner;

    const/4 v0, 0x7

    iput v0, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_currToken:I

    invoke-virtual {p1}, Lcom/fasterxml/aalto/in/XmlScanner;->getConfig()Lcom/fasterxml/aalto/in/ReaderConfig;

    move-result-object p1

    invoke-virtual {p1}, Lcom/fasterxml/aalto/in/ReaderConfig;->willCoalesceText()Z

    move-result v0

    iput-boolean v0, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_cfgCoalesceText:Z

    invoke-virtual {p1}, Lcom/fasterxml/aalto/in/ReaderConfig;->willReportCData()Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    iput-boolean p1, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_cfgReportTextAsChars:Z

    return-void
.end method

.method private _constructTypeException(Ljava/lang/IllegalArgumentException;Ljava/lang/String;)Lorg/codehaus/stax2/typed/TypedXMLStreamException;
    .locals 3

    .line 1
    new-instance v0, Lorg/codehaus/stax2/typed/TypedXMLStreamException;

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->getStartLocation()Lorg/codehaus/stax2/XMLStreamLocation2;

    move-result-object v2

    invoke-direct {v0, p2, v1, v2, p1}, Lorg/codehaus/stax2/typed/TypedXMLStreamException;-><init>(Ljava/lang/String;Ljava/lang/String;Ljavax/xml/stream/Location;Ljava/lang/IllegalArgumentException;)V

    return-object v0
.end method

.method private _constructTypeException(Ljava/lang/String;Ljava/lang/String;)Lorg/codehaus/stax2/typed/TypedXMLStreamException;
    .locals 2

    .line 2
    new-instance v0, Lorg/codehaus/stax2/typed/TypedXMLStreamException;

    invoke-virtual {p0}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->getStartLocation()Lorg/codehaus/stax2/XMLStreamLocation2;

    move-result-object v1

    invoke-direct {v0, p2, p1, v1}, Lorg/codehaus/stax2/typed/TypedXMLStreamException;-><init>(Ljava/lang/String;Ljava/lang/String;Ljavax/xml/stream/Location;)V

    return-object v0
.end method

.method private _handleEmptyValue(Lorg/codehaus/stax2/typed/TypedValueDecoder;)V
    .locals 1

    :try_start_0
    invoke-virtual {p1}, Lorg/codehaus/stax2/typed/TypedValueDecoder;->handleEmptyValue()V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    const-string v0, ""

    invoke-direct {p0, p1, v0}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_constructTypeException(Ljava/lang/IllegalArgumentException;Ljava/lang/String;)Lorg/codehaus/stax2/typed/TypedXMLStreamException;

    move-result-object p1

    throw p1
.end method

.method public static construct(Lcom/fasterxml/aalto/in/InputBootstrapper;)Lcom/fasterxml/aalto/stax/StreamReaderImpl;
    .locals 1

    new-instance v0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;

    invoke-virtual {p0}, Lcom/fasterxml/aalto/in/InputBootstrapper;->bootstrap()Lcom/fasterxml/aalto/in/XmlScanner;

    move-result-object p0

    invoke-direct {v0, p0}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;-><init>(Lcom/fasterxml/aalto/in/XmlScanner;)V

    return-object v0
.end method

.method private throwNotTextXxx(I)V
    .locals 2

    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "getTextXxx() methods can not be called on "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_currToken:I

    invoke-static {v1}, Lcom/fasterxml/aalto/impl/ErrorConsts;->tokenTypeDesc(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private throwNotTextual(I)V
    .locals 2

    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Not a textual event ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_currToken:I

    invoke-static {v1}, Lcom/fasterxml/aalto/impl/ErrorConsts;->tokenTypeDesc(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public _base64Decoder()Lorg/codehaus/stax2/ri/typed/CharArrayBase64Decoder;
    .locals 1

    iget-object v0, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_base64Decoder:Lorg/codehaus/stax2/ri/typed/CharArrayBase64Decoder;

    if-nez v0, :cond_0

    new-instance v0, Lorg/codehaus/stax2/ri/typed/CharArrayBase64Decoder;

    invoke-direct {v0}, Lorg/codehaus/stax2/ri/typed/CharArrayBase64Decoder;-><init>()V

    iput-object v0, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_base64Decoder:Lorg/codehaus/stax2/ri/typed/CharArrayBase64Decoder;

    :cond_0
    iget-object v0, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_base64Decoder:Lorg/codehaus/stax2/ri/typed/CharArrayBase64Decoder;

    return-object v0
.end method

.method public _closeScanner(Z)V
    .locals 2

    iget v0, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_parseState:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    iput v1, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_parseState:I

    iget v0, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_currToken:I

    const/16 v1, 0x8

    if-eq v0, v1, :cond_0

    iput v1, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_currToken:I

    :cond_0
    iget-object v0, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_scanner:Lcom/fasterxml/aalto/in/XmlScanner;

    invoke-virtual {v0, p1}, Lcom/fasterxml/aalto/in/XmlScanner;->close(Z)V

    return-void
.end method

.method public _constructUnexpectedInTyped(I)Ljavax/xml/stream/XMLStreamException;
    .locals 3

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p1, v0, :cond_0

    const-string p1, "Element content can not contain child START_ELEMENT when using Typed Access methods"

    :goto_0
    invoke-direct {p0, p1, v1}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_constructTypeException(Ljava/lang/String;Ljava/lang/String;)Lorg/codehaus/stax2/typed/TypedXMLStreamException;

    move-result-object p1

    return-object p1

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expected a text token, got "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {p1}, Lcom/fasterxml/aalto/impl/ErrorConsts;->tokenTypeDesc(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0
.end method

.method public final _decoderFactory()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;
    .locals 1

    iget-object v0, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_decoderFactory:Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;

    if-nez v0, :cond_0

    new-instance v0, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;

    invoke-direct {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;-><init>()V

    iput-object v0, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_decoderFactory:Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;

    :cond_0
    iget-object v0, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_decoderFactory:Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;

    return-object v0
.end method

.method public _reportNonTextEvent(I)V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Expected a text token, got "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {p1}, Lcom/fasterxml/aalto/impl/ErrorConsts;->tokenTypeDesc(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "."

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->throwWfe(Ljava/lang/String;)V

    return-void
.end method

.method public final close()V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_closeScanner(Z)V

    return-void
.end method

.method public final closeCompletely()V
    .locals 1

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_closeScanner(Z)V

    return-void
.end method

.method public final findAttributeIndex(Ljava/lang/String;Ljava/lang/String;)I
    .locals 1

    iget-object v0, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_scanner:Lcom/fasterxml/aalto/in/XmlScanner;

    invoke-virtual {v0, p1, p2}, Lcom/fasterxml/aalto/in/XmlScanner;->findAttrIndex(Ljava/lang/String;Ljava/lang/String;)I

    move-result p1

    return p1
.end method

.method public final getAttributeAs(ILorg/codehaus/stax2/typed/TypedValueDecoder;)V
    .locals 2

    iget v0, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_currToken:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_scanner:Lcom/fasterxml/aalto/in/XmlScanner;

    invoke-virtual {v0, p1, p2}, Lcom/fasterxml/aalto/in/XmlScanner;->decodeAttrValue(ILorg/codehaus/stax2/typed/TypedValueDecoder;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p2

    invoke-virtual {p0, p1}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->getAttributeValue(I)Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p2, p1}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_constructTypeException(Ljava/lang/IllegalArgumentException;Ljava/lang/String;)Lorg/codehaus/stax2/typed/TypedXMLStreamException;

    move-result-object p1

    throw p1

    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    sget-object p2, Lcom/fasterxml/aalto/impl/ErrorConsts;->ERR_STATE_NOT_STELEM:Ljava/lang/String;

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public final getAttributeAsArray(ILorg/codehaus/stax2/typed/TypedArrayDecoder;)I
    .locals 2

    iget v0, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_currToken:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_scanner:Lcom/fasterxml/aalto/in/XmlScanner;

    invoke-virtual {v0, p1, p2}, Lcom/fasterxml/aalto/in/XmlScanner;->decodeAttrValues(ILorg/codehaus/stax2/typed/TypedArrayDecoder;)I

    move-result p1

    return p1

    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    sget-object p2, Lcom/fasterxml/aalto/impl/ErrorConsts;->ERR_STATE_NOT_STELEM:Ljava/lang/String;

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public final getAttributeAsBinary(I)[B
    .locals 1

    .line 1
    invoke-static {}, Lorg/codehaus/stax2/typed/Base64Variants;->getDefaultVariant()Lorg/codehaus/stax2/typed/Base64Variant;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->getAttributeAsBinary(ILorg/codehaus/stax2/typed/Base64Variant;)[B

    move-result-object p1

    return-object p1
.end method

.method public final getAttributeAsBinary(ILorg/codehaus/stax2/typed/Base64Variant;)[B
    .locals 2

    .line 2
    iget v0, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_currToken:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_scanner:Lcom/fasterxml/aalto/in/XmlScanner;

    invoke-virtual {p0}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_base64Decoder()Lorg/codehaus/stax2/ri/typed/CharArrayBase64Decoder;

    move-result-object v1

    invoke-virtual {v0, p1, p2, v1}, Lcom/fasterxml/aalto/in/XmlScanner;->decodeAttrBinaryValue(ILorg/codehaus/stax2/typed/Base64Variant;Lorg/codehaus/stax2/ri/typed/CharArrayBase64Decoder;)[B

    move-result-object p1

    return-object p1

    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    sget-object p2, Lcom/fasterxml/aalto/impl/ErrorConsts;->ERR_STATE_NOT_STELEM:Ljava/lang/String;

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public final getAttributeAsBoolean(I)Z
    .locals 1

    invoke-virtual {p0}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_decoderFactory()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;

    move-result-object v0

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;->getBooleanDecoder()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$BooleanDecoder;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->getAttributeAs(ILorg/codehaus/stax2/typed/TypedValueDecoder;)V

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$BooleanDecoder;->getValue()Z

    move-result p1

    return p1
.end method

.method public final getAttributeAsDecimal(I)Ljava/math/BigDecimal;
    .locals 1

    invoke-virtual {p0}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_decoderFactory()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;

    move-result-object v0

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;->getDecimalDecoder()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$DecimalDecoder;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->getAttributeAs(ILorg/codehaus/stax2/typed/TypedValueDecoder;)V

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$DecimalDecoder;->getValue()Ljava/math/BigDecimal;

    move-result-object p1

    return-object p1
.end method

.method public final getAttributeAsDouble(I)D
    .locals 2

    invoke-virtual {p0}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_decoderFactory()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;

    move-result-object v0

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;->getDoubleDecoder()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$DoubleDecoder;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->getAttributeAs(ILorg/codehaus/stax2/typed/TypedValueDecoder;)V

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$DoubleDecoder;->getValue()D

    move-result-wide v0

    return-wide v0
.end method

.method public final getAttributeAsDoubleArray(I)[D
    .locals 1

    invoke-virtual {p0}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_decoderFactory()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;

    move-result-object v0

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;->getDoubleArrayDecoder()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$DoubleArrayDecoder;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->getAttributeAsArray(ILorg/codehaus/stax2/typed/TypedArrayDecoder;)I

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$DoubleArrayDecoder;->getValues()[D

    move-result-object p1

    return-object p1
.end method

.method public final getAttributeAsFloat(I)F
    .locals 1

    invoke-virtual {p0}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_decoderFactory()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;

    move-result-object v0

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;->getFloatDecoder()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$FloatDecoder;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->getAttributeAs(ILorg/codehaus/stax2/typed/TypedValueDecoder;)V

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$FloatDecoder;->getValue()F

    move-result p1

    return p1
.end method

.method public final getAttributeAsFloatArray(I)[F
    .locals 1

    invoke-virtual {p0}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_decoderFactory()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;

    move-result-object v0

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;->getFloatArrayDecoder()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$FloatArrayDecoder;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->getAttributeAsArray(ILorg/codehaus/stax2/typed/TypedArrayDecoder;)I

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$FloatArrayDecoder;->getValues()[F

    move-result-object p1

    return-object p1
.end method

.method public final getAttributeAsInt(I)I
    .locals 1

    invoke-virtual {p0}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_decoderFactory()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;

    move-result-object v0

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;->getIntDecoder()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$IntDecoder;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->getAttributeAs(ILorg/codehaus/stax2/typed/TypedValueDecoder;)V

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$IntDecoder;->getValue()I

    move-result p1

    return p1
.end method

.method public final getAttributeAsIntArray(I)[I
    .locals 1

    invoke-virtual {p0}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_decoderFactory()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;

    move-result-object v0

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;->getIntArrayDecoder()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$IntArrayDecoder;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->getAttributeAsArray(ILorg/codehaus/stax2/typed/TypedArrayDecoder;)I

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$IntArrayDecoder;->getValues()[I

    move-result-object p1

    return-object p1
.end method

.method public final getAttributeAsInteger(I)Ljava/math/BigInteger;
    .locals 1

    invoke-virtual {p0}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_decoderFactory()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;

    move-result-object v0

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;->getIntegerDecoder()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$IntegerDecoder;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->getAttributeAs(ILorg/codehaus/stax2/typed/TypedValueDecoder;)V

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$IntegerDecoder;->getValue()Ljava/math/BigInteger;

    move-result-object p1

    return-object p1
.end method

.method public final getAttributeAsLong(I)J
    .locals 2

    invoke-virtual {p0}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_decoderFactory()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;

    move-result-object v0

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;->getLongDecoder()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$LongDecoder;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->getAttributeAs(ILorg/codehaus/stax2/typed/TypedValueDecoder;)V

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$LongDecoder;->getValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public final getAttributeAsLongArray(I)[J
    .locals 1

    invoke-virtual {p0}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_decoderFactory()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;

    move-result-object v0

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;->getLongArrayDecoder()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$LongArrayDecoder;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->getAttributeAsArray(ILorg/codehaus/stax2/typed/TypedArrayDecoder;)I

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$LongArrayDecoder;->getValues()[J

    move-result-object p1

    return-object p1
.end method

.method public final getAttributeAsQName(I)Ljavax/xml/namespace/QName;
    .locals 2

    invoke-virtual {p0}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_decoderFactory()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;

    move-result-object v0

    invoke-virtual {p0}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->getNamespaceContext()Ljavax/xml/namespace/NamespaceContext;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;->getQNameDecoder(Ljavax/xml/namespace/NamespaceContext;)Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$QNameDecoder;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->getAttributeAs(ILorg/codehaus/stax2/typed/TypedValueDecoder;)V

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$QNameDecoder;->getValue()Ljavax/xml/namespace/QName;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->verifyQName(Ljavax/xml/namespace/QName;)Ljavax/xml/namespace/QName;

    move-result-object p1

    return-object p1
.end method

.method public final getAttributeCount()I
    .locals 2

    iget v0, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_currToken:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_attrCount:I

    return v0

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    sget-object v1, Lcom/fasterxml/aalto/impl/ErrorConsts;->ERR_STATE_NOT_STELEM:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final getAttributeIndex(Ljava/lang/String;Ljava/lang/String;)I
    .locals 2

    iget v0, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_currToken:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    invoke-virtual {p0, p1, p2}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->findAttributeIndex(Ljava/lang/String;Ljava/lang/String;)I

    move-result p1

    return p1

    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    sget-object p2, Lcom/fasterxml/aalto/impl/ErrorConsts;->ERR_STATE_NOT_STELEM:Ljava/lang/String;

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public final getAttributeInfo()Lorg/codehaus/stax2/AttributeInfo;
    .locals 2

    iget v0, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_currToken:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    return-object p0

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    sget-object v1, Lcom/fasterxml/aalto/impl/ErrorConsts;->ERR_STATE_NOT_STELEM:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final getAttributeLocalName(I)Ljava/lang/String;
    .locals 2

    iget v0, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_currToken:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    iget v0, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_attrCount:I

    if-ge p1, v0, :cond_0

    if-gez p1, :cond_1

    :cond_0
    invoke-virtual {p0, p1}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->reportInvalidAttrIndex(I)V

    :cond_1
    iget-object v0, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_scanner:Lcom/fasterxml/aalto/in/XmlScanner;

    invoke-virtual {v0, p1}, Lcom/fasterxml/aalto/in/XmlScanner;->getAttrLocalName(I)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_2
    new-instance p1, Ljava/lang/IllegalStateException;

    sget-object v0, Lcom/fasterxml/aalto/impl/ErrorConsts;->ERR_STATE_NOT_STELEM:Ljava/lang/String;

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public final getAttributeName(I)Ljavax/xml/namespace/QName;
    .locals 2

    iget v0, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_currToken:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    iget v0, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_attrCount:I

    if-ge p1, v0, :cond_0

    if-gez p1, :cond_1

    :cond_0
    invoke-virtual {p0, p1}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->reportInvalidAttrIndex(I)V

    :cond_1
    iget-object v0, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_scanner:Lcom/fasterxml/aalto/in/XmlScanner;

    invoke-virtual {v0, p1}, Lcom/fasterxml/aalto/in/XmlScanner;->getAttrQName(I)Ljavax/xml/namespace/QName;

    move-result-object p1

    return-object p1

    :cond_2
    new-instance p1, Ljava/lang/IllegalStateException;

    sget-object v0, Lcom/fasterxml/aalto/impl/ErrorConsts;->ERR_STATE_NOT_STELEM:Ljava/lang/String;

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public final getAttributeNamespace(I)Ljava/lang/String;
    .locals 2

    iget v0, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_currToken:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    iget v0, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_attrCount:I

    if-ge p1, v0, :cond_0

    if-gez p1, :cond_1

    :cond_0
    invoke-virtual {p0, p1}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->reportInvalidAttrIndex(I)V

    :cond_1
    iget-object v0, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_scanner:Lcom/fasterxml/aalto/in/XmlScanner;

    invoke-virtual {v0, p1}, Lcom/fasterxml/aalto/in/XmlScanner;->getAttrNsURI(I)Ljava/lang/String;

    move-result-object p1

    if-nez p1, :cond_2

    const-string p1, ""

    :cond_2
    return-object p1

    :cond_3
    new-instance p1, Ljava/lang/IllegalStateException;

    sget-object v0, Lcom/fasterxml/aalto/impl/ErrorConsts;->ERR_STATE_NOT_STELEM:Ljava/lang/String;

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public final getAttributePrefix(I)Ljava/lang/String;
    .locals 2

    iget v0, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_currToken:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    iget v0, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_attrCount:I

    if-ge p1, v0, :cond_0

    if-gez p1, :cond_1

    :cond_0
    invoke-virtual {p0, p1}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->reportInvalidAttrIndex(I)V

    :cond_1
    iget-object v0, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_scanner:Lcom/fasterxml/aalto/in/XmlScanner;

    invoke-virtual {v0, p1}, Lcom/fasterxml/aalto/in/XmlScanner;->getAttrPrefix(I)Ljava/lang/String;

    move-result-object p1

    if-nez p1, :cond_2

    const-string p1, ""

    :cond_2
    return-object p1

    :cond_3
    new-instance p1, Ljava/lang/IllegalStateException;

    sget-object v0, Lcom/fasterxml/aalto/impl/ErrorConsts;->ERR_STATE_NOT_STELEM:Ljava/lang/String;

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public final getAttributeType(I)Ljava/lang/String;
    .locals 2

    iget v0, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_currToken:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    iget v0, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_attrCount:I

    if-ge p1, v0, :cond_0

    if-gez p1, :cond_1

    :cond_0
    invoke-virtual {p0, p1}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->reportInvalidAttrIndex(I)V

    :cond_1
    iget-object v0, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_scanner:Lcom/fasterxml/aalto/in/XmlScanner;

    invoke-virtual {v0, p1}, Lcom/fasterxml/aalto/in/XmlScanner;->getAttrType(I)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_2
    new-instance p1, Ljava/lang/IllegalStateException;

    sget-object v0, Lcom/fasterxml/aalto/impl/ErrorConsts;->ERR_STATE_NOT_STELEM:Ljava/lang/String;

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public final getAttributeValue(I)Ljava/lang/String;
    .locals 2

    .line 1
    iget v0, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_currToken:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    iget v0, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_attrCount:I

    if-ge p1, v0, :cond_0

    if-gez p1, :cond_1

    :cond_0
    invoke-virtual {p0, p1}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->reportInvalidAttrIndex(I)V

    :cond_1
    iget-object v0, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_scanner:Lcom/fasterxml/aalto/in/XmlScanner;

    invoke-virtual {v0, p1}, Lcom/fasterxml/aalto/in/XmlScanner;->getAttrValue(I)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_2
    new-instance p1, Ljava/lang/IllegalStateException;

    sget-object v0, Lcom/fasterxml/aalto/impl/ErrorConsts;->ERR_STATE_NOT_STELEM:Ljava/lang/String;

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public final getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .line 2
    iget v0, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_currToken:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_scanner:Lcom/fasterxml/aalto/in/XmlScanner;

    invoke-virtual {v0, p1, p2}, Lcom/fasterxml/aalto/in/XmlScanner;->getAttrValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    sget-object p2, Lcom/fasterxml/aalto/impl/ErrorConsts;->ERR_STATE_NOT_STELEM:Ljava/lang/String;

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public final getCharacterEncodingScheme()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_scanner:Lcom/fasterxml/aalto/in/XmlScanner;

    invoke-virtual {v0}, Lcom/fasterxml/aalto/in/XmlScanner;->getConfig()Lcom/fasterxml/aalto/in/ReaderConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/fasterxml/aalto/in/ReaderConfig;->getXmlDeclEncoding()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getConfig()Lcom/fasterxml/aalto/in/ReaderConfig;
    .locals 1

    iget-object v0, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_scanner:Lcom/fasterxml/aalto/in/XmlScanner;

    invoke-virtual {v0}, Lcom/fasterxml/aalto/in/XmlScanner;->getConfig()Lcom/fasterxml/aalto/in/ReaderConfig;

    move-result-object v0

    return-object v0
.end method

.method public final getCurrentLocation()Lorg/codehaus/stax2/XMLStreamLocation2;
    .locals 1

    iget-object v0, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_scanner:Lcom/fasterxml/aalto/in/XmlScanner;

    invoke-virtual {v0}, Lcom/fasterxml/aalto/in/XmlScanner;->getCurrentLocation()Lorg/codehaus/stax2/XMLStreamLocation2;

    move-result-object v0

    return-object v0
.end method

.method public final getDTDInfo()Lorg/codehaus/stax2/DTDInfo;
    .locals 2

    iget v0, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_currToken:I

    const/16 v1, 0xb

    if-eq v0, v1, :cond_0

    const/4 v0, 0x0

    return-object v0

    :cond_0
    return-object p0
.end method

.method public final getDTDInternalSubset()Ljava/lang/String;
    .locals 2

    iget v0, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_currToken:I

    const/16 v1, 0xb

    if-eq v0, v1, :cond_0

    const/4 v0, 0x0

    return-object v0

    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_scanner:Lcom/fasterxml/aalto/in/XmlScanner;

    invoke-virtual {v0}, Lcom/fasterxml/aalto/in/XmlScanner;->getText()Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catch Ljavax/xml/stream/XMLStreamException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/fasterxml/aalto/UncheckedStreamException;->createFrom(Ljavax/xml/stream/XMLStreamException;)Lcom/fasterxml/aalto/UncheckedStreamException;

    move-result-object v0

    throw v0
.end method

.method public final getDTDPublicId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_scanner:Lcom/fasterxml/aalto/in/XmlScanner;

    invoke-virtual {v0}, Lcom/fasterxml/aalto/in/XmlScanner;->getDTDPublicId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getDTDRootName()Ljava/lang/String;
    .locals 3

    iget v0, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_currToken:I

    const/16 v1, 0xb

    const/4 v2, 0x0

    if-eq v0, v1, :cond_0

    return-object v2

    :cond_0
    iget-object v0, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_currName:Lcom/fasterxml/aalto/in/PName;

    if-nez v0, :cond_1

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Lcom/fasterxml/aalto/in/PName;->getPrefixedName()Ljava/lang/String;

    move-result-object v2

    :goto_0
    return-object v2
.end method

.method public final getDTDSystemId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_scanner:Lcom/fasterxml/aalto/in/XmlScanner;

    invoke-virtual {v0}, Lcom/fasterxml/aalto/in/XmlScanner;->getDTDSystemId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getDepth()I
    .locals 3

    iget-object v0, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_scanner:Lcom/fasterxml/aalto/in/XmlScanner;

    invoke-virtual {v0}, Lcom/fasterxml/aalto/in/XmlScanner;->getDepth()I

    move-result v0

    iget v1, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_currToken:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    add-int/lit8 v0, v0, 0x1

    :cond_0
    return v0
.end method

.method public final getElementAs(Lorg/codehaus/stax2/typed/TypedValueDecoder;)V
    .locals 2

    invoke-virtual {p0}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->getElementText()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_0

    invoke-direct {p0, p1}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_handleEmptyValue(Lorg/codehaus/stax2/typed/TypedValueDecoder;)V

    return-void

    :cond_0
    :try_start_0
    invoke-virtual {p1, v0}, Lorg/codehaus/stax2/typed/TypedValueDecoder;->decode(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    invoke-direct {p0, p1, v0}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_constructTypeException(Ljava/lang/IllegalArgumentException;Ljava/lang/String;)Lorg/codehaus/stax2/typed/TypedXMLStreamException;

    move-result-object p1

    throw p1
.end method

.method public final getElementAsBinary()[B
    .locals 1

    .line 1
    invoke-static {}, Lorg/codehaus/stax2/typed/Base64Variants;->getDefaultVariant()Lorg/codehaus/stax2/typed/Base64Variant;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->getElementAsBinary(Lorg/codehaus/stax2/typed/Base64Variant;)[B

    move-result-object v0

    return-object v0
.end method

.method public final getElementAsBinary(Lorg/codehaus/stax2/typed/Base64Variant;)[B
    .locals 6

    .line 2
    invoke-virtual {p0}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_base64Decoder()Lorg/codehaus/stax2/ri/typed/CharArrayBase64Decoder;

    move-result-object v0

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/CharArrayBase64Decoder;->getByteAggregator()Lorg/codehaus/stax2/ri/Stax2Util$ByteAggregator;

    move-result-object v0

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/Stax2Util$ByteAggregator;->startAggregation()[B

    move-result-object v1

    :goto_0
    array-length v2, v1

    const/4 v3, 0x0

    :cond_0
    invoke-virtual {p0, v1, v3, v2, p1}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->readElementAsBinary([BIILorg/codehaus/stax2/typed/Base64Variant;)I

    move-result v4

    const/4 v5, 0x1

    if-ge v4, v5, :cond_1

    invoke-virtual {v0, v1, v3}, Lorg/codehaus/stax2/ri/Stax2Util$ByteAggregator;->aggregateAll([BI)[B

    move-result-object p1

    return-object p1

    :cond_1
    add-int/2addr v3, v4

    sub-int/2addr v2, v4

    if-gtz v2, :cond_0

    invoke-virtual {v0, v1}, Lorg/codehaus/stax2/ri/Stax2Util$ByteAggregator;->addFullBlock([B)[B

    move-result-object v1

    goto :goto_0
.end method

.method public final getElementAsBoolean()Z
    .locals 1

    invoke-virtual {p0}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_decoderFactory()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;

    move-result-object v0

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;->getBooleanDecoder()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$BooleanDecoder;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->getElementAs(Lorg/codehaus/stax2/typed/TypedValueDecoder;)V

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$BooleanDecoder;->getValue()Z

    move-result v0

    return v0
.end method

.method public final getElementAsDecimal()Ljava/math/BigDecimal;
    .locals 1

    invoke-virtual {p0}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_decoderFactory()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;

    move-result-object v0

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;->getDecimalDecoder()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$DecimalDecoder;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->getElementAs(Lorg/codehaus/stax2/typed/TypedValueDecoder;)V

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$DecimalDecoder;->getValue()Ljava/math/BigDecimal;

    move-result-object v0

    return-object v0
.end method

.method public final getElementAsDouble()D
    .locals 2

    invoke-virtual {p0}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_decoderFactory()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;

    move-result-object v0

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;->getDoubleDecoder()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$DoubleDecoder;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->getElementAs(Lorg/codehaus/stax2/typed/TypedValueDecoder;)V

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$DoubleDecoder;->getValue()D

    move-result-wide v0

    return-wide v0
.end method

.method public final getElementAsFloat()F
    .locals 1

    invoke-virtual {p0}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_decoderFactory()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;

    move-result-object v0

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;->getFloatDecoder()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$FloatDecoder;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->getElementAs(Lorg/codehaus/stax2/typed/TypedValueDecoder;)V

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$FloatDecoder;->getValue()F

    move-result v0

    return v0
.end method

.method public final getElementAsInt()I
    .locals 1

    invoke-virtual {p0}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_decoderFactory()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;

    move-result-object v0

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;->getIntDecoder()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$IntDecoder;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->getElementAs(Lorg/codehaus/stax2/typed/TypedValueDecoder;)V

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$IntDecoder;->getValue()I

    move-result v0

    return v0
.end method

.method public final getElementAsInteger()Ljava/math/BigInteger;
    .locals 1

    invoke-virtual {p0}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_decoderFactory()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;

    move-result-object v0

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;->getIntegerDecoder()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$IntegerDecoder;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->getElementAs(Lorg/codehaus/stax2/typed/TypedValueDecoder;)V

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$IntegerDecoder;->getValue()Ljava/math/BigInteger;

    move-result-object v0

    return-object v0
.end method

.method public final getElementAsLong()J
    .locals 2

    invoke-virtual {p0}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_decoderFactory()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;

    move-result-object v0

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;->getLongDecoder()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$LongDecoder;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->getElementAs(Lorg/codehaus/stax2/typed/TypedValueDecoder;)V

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$LongDecoder;->getValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public final getElementAsQName()Ljavax/xml/namespace/QName;
    .locals 2

    invoke-virtual {p0}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_decoderFactory()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;

    move-result-object v0

    invoke-virtual {p0}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->getNamespaceContext()Ljavax/xml/namespace/NamespaceContext;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;->getQNameDecoder(Ljavax/xml/namespace/NamespaceContext;)Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$QNameDecoder;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->getElementAs(Lorg/codehaus/stax2/typed/TypedValueDecoder;)V

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$QNameDecoder;->getValue()Ljavax/xml/namespace/QName;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->verifyQName(Ljavax/xml/namespace/QName;)Ljavax/xml/namespace/QName;

    move-result-object v0

    return-object v0
.end method

.method public final getElementText()Ljava/lang/String;
    .locals 8

    iget v0, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_currToken:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    sget-object v0, Lcom/fasterxml/aalto/impl/ErrorConsts;->ERR_STATE_NOT_STELEM:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->throwWfe(Ljava/lang/String;)V

    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->next()I

    move-result v0

    const/4 v2, 0x2

    if-ne v0, v2, :cond_1

    const-string v0, ""

    return-object v0

    :cond_1
    const/4 v3, 0x5

    if-eq v0, v3, :cond_0

    const/4 v4, 0x3

    if-ne v0, v4, :cond_2

    goto :goto_0

    :cond_2
    shl-int v5, v1, v0

    and-int/lit16 v5, v5, 0x1250

    if-nez v5, :cond_3

    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_reportNonTextEvent(I)V

    :cond_3
    iget-object v0, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_scanner:Lcom/fasterxml/aalto/in/XmlScanner;

    invoke-virtual {v0}, Lcom/fasterxml/aalto/in/XmlScanner;->getText()Ljava/lang/String;

    move-result-object v0

    const/4 v5, 0x0

    :cond_4
    :goto_1
    invoke-virtual {p0}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->next()I

    move-result v6

    if-eq v6, v2, :cond_7

    shl-int v7, v1, v6

    and-int/lit16 v7, v7, 0x1250

    if-eqz v7, :cond_6

    if-nez v5, :cond_5

    new-instance v5, Lcom/fasterxml/aalto/util/TextAccumulator;

    invoke-direct {v5}, Lcom/fasterxml/aalto/util/TextAccumulator;-><init>()V

    invoke-virtual {v5, v0}, Lcom/fasterxml/aalto/util/TextAccumulator;->addText(Ljava/lang/String;)V

    :cond_5
    invoke-virtual {p0}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->getText()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/fasterxml/aalto/util/TextAccumulator;->addText(Ljava/lang/String;)V

    goto :goto_1

    :cond_6
    if-eq v6, v3, :cond_4

    if-eq v6, v4, :cond_4

    invoke-virtual {p0, v6}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_reportNonTextEvent(I)V

    goto :goto_1

    :cond_7
    if-nez v5, :cond_8

    goto :goto_2

    :cond_8
    invoke-virtual {v5}, Lcom/fasterxml/aalto/util/TextAccumulator;->getAndClear()Ljava/lang/String;

    move-result-object v0

    :goto_2
    return-object v0
.end method

.method public final getEncoding()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_scanner:Lcom/fasterxml/aalto/in/XmlScanner;

    invoke-virtual {v0}, Lcom/fasterxml/aalto/in/XmlScanner;->getConfig()Lcom/fasterxml/aalto/in/ReaderConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/fasterxml/aalto/in/ReaderConfig;->getActualEncoding()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getEndLocation()Lorg/codehaus/stax2/XMLStreamLocation2;
    .locals 1

    iget-object v0, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_scanner:Lcom/fasterxml/aalto/in/XmlScanner;

    invoke-virtual {v0}, Lcom/fasterxml/aalto/in/XmlScanner;->getEndLocation()Lorg/codehaus/stax2/XMLStreamLocation2;

    move-result-object v0

    return-object v0
.end method

.method public final getEndingByteOffset()J
    .locals 2

    iget-object v0, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_scanner:Lcom/fasterxml/aalto/in/XmlScanner;

    invoke-virtual {v0}, Lcom/fasterxml/aalto/in/XmlScanner;->getEndingByteOffset()J

    move-result-wide v0

    return-wide v0
.end method

.method public final getEndingCharOffset()J
    .locals 2

    iget-object v0, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_scanner:Lcom/fasterxml/aalto/in/XmlScanner;

    invoke-virtual {v0}, Lcom/fasterxml/aalto/in/XmlScanner;->getEndingCharOffset()J

    move-result-wide v0

    return-wide v0
.end method

.method public final getEventType()I
    .locals 2

    iget v0, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_currToken:I

    const/16 v1, 0xc

    if-ne v0, v1, :cond_1

    iget-boolean v1, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_cfgCoalesceText:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_cfgReportTextAsChars:Z

    if-eqz v1, :cond_1

    :cond_0
    const/4 v0, 0x4

    :cond_1
    return v0
.end method

.method public final getFeature(Ljava/lang/String;)Ljava/lang/Object;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    const/4 p1, 0x0

    return-object p1
.end method

.method public final getIdAttributeIndex()I
    .locals 1

    const/4 v0, -0x1

    return v0
.end method

.method public getLastCharLocation()Ljavax/xml/stream/Location;
    .locals 1

    iget-object v0, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_scanner:Lcom/fasterxml/aalto/in/XmlScanner;

    invoke-virtual {v0}, Lcom/fasterxml/aalto/in/XmlScanner;->getCurrentLocation()Lorg/codehaus/stax2/XMLStreamLocation2;

    move-result-object v0

    return-object v0
.end method

.method public final getLocalName()Ljava/lang/String;
    .locals 2

    iget v0, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_currToken:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    const/16 v1, 0x9

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Current state not START_ELEMENT, END_ELEMENT or ENTITY_REFERENCE"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_currName:Lcom/fasterxml/aalto/in/PName;

    invoke-virtual {v0}, Lcom/fasterxml/aalto/in/PName;->getLocalName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getLocation()Ljavax/xml/stream/Location;
    .locals 1

    invoke-virtual {p0}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->getStartLocation()Lorg/codehaus/stax2/XMLStreamLocation2;

    move-result-object v0

    return-object v0
.end method

.method public final getLocationInfo()Lorg/codehaus/stax2/LocationInfo;
    .locals 0

    return-object p0
.end method

.method public final getName()Ljavax/xml/namespace/QName;
    .locals 2

    iget v0, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_currToken:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    sget-object v1, Lcom/fasterxml/aalto/impl/ErrorConsts;->ERR_STATE_NOT_ELEM:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_scanner:Lcom/fasterxml/aalto/in/XmlScanner;

    invoke-virtual {v0}, Lcom/fasterxml/aalto/in/XmlScanner;->getQName()Ljavax/xml/namespace/QName;

    move-result-object v0

    return-object v0
.end method

.method public final getNamespaceContext()Ljavax/xml/namespace/NamespaceContext;
    .locals 1

    iget-object v0, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_scanner:Lcom/fasterxml/aalto/in/XmlScanner;

    return-object v0
.end method

.method public final getNamespaceCount()I
    .locals 2

    iget v0, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_currToken:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    sget-object v1, Lcom/fasterxml/aalto/impl/ErrorConsts;->ERR_STATE_NOT_ELEM:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_scanner:Lcom/fasterxml/aalto/in/XmlScanner;

    invoke-virtual {v0}, Lcom/fasterxml/aalto/in/XmlScanner;->getNsCount()I

    move-result v0

    return v0
.end method

.method public final getNamespacePrefix(I)Ljava/lang/String;
    .locals 2

    iget v0, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_currToken:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    sget-object v0, Lcom/fasterxml/aalto/impl/ErrorConsts;->ERR_STATE_NOT_ELEM:Ljava/lang/String;

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_scanner:Lcom/fasterxml/aalto/in/XmlScanner;

    invoke-virtual {v0, p1}, Lcom/fasterxml/aalto/in/XmlScanner;->getNamespacePrefix(I)Ljava/lang/String;

    move-result-object p1

    if-nez p1, :cond_2

    const-string p1, ""

    :cond_2
    return-object p1
.end method

.method public final getNamespaceURI()Ljava/lang/String;
    .locals 2

    .line 1
    iget v0, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_currToken:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    sget-object v1, Lcom/fasterxml/aalto/impl/ErrorConsts;->ERR_STATE_NOT_ELEM:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_scanner:Lcom/fasterxml/aalto/in/XmlScanner;

    invoke-virtual {v0}, Lcom/fasterxml/aalto/in/XmlScanner;->getNamespaceURI()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    const-string v0, ""

    :cond_2
    return-object v0
.end method

.method public final getNamespaceURI(I)Ljava/lang/String;
    .locals 2

    .line 2
    iget v0, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_currToken:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    sget-object v0, Lcom/fasterxml/aalto/impl/ErrorConsts;->ERR_STATE_NOT_ELEM:Ljava/lang/String;

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_scanner:Lcom/fasterxml/aalto/in/XmlScanner;

    invoke-virtual {v0, p1}, Lcom/fasterxml/aalto/in/XmlScanner;->getNamespaceURI(I)Ljava/lang/String;

    move-result-object p1

    if-nez p1, :cond_2

    const-string p1, ""

    :cond_2
    return-object p1
.end method

.method public final getNamespaceURI(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .line 3
    iget v0, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_currToken:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    sget-object v0, Lcom/fasterxml/aalto/impl/ErrorConsts;->ERR_STATE_NOT_ELEM:Ljava/lang/String;

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_scanner:Lcom/fasterxml/aalto/in/XmlScanner;

    invoke-virtual {v0, p1}, Lcom/fasterxml/aalto/in/XmlScanner;->getNamespaceURI(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public final getNonTransientNamespaceContext()Ljavax/xml/namespace/NamespaceContext;
    .locals 1

    iget-object v0, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_scanner:Lcom/fasterxml/aalto/in/XmlScanner;

    invoke-virtual {v0}, Lcom/fasterxml/aalto/in/XmlScanner;->getNonTransientNamespaceContext()Ljavax/xml/namespace/NamespaceContext;

    move-result-object v0

    return-object v0
.end method

.method public final getNotationAttributeIndex()I
    .locals 1

    const/4 v0, -0x1

    return v0
.end method

.method public final getPIData()Ljava/lang/String;
    .locals 2

    iget v0, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_currToken:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_scanner:Lcom/fasterxml/aalto/in/XmlScanner;

    invoke-virtual {v0}, Lcom/fasterxml/aalto/in/XmlScanner;->getText()Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catch Ljavax/xml/stream/XMLStreamException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/fasterxml/aalto/UncheckedStreamException;->createFrom(Ljavax/xml/stream/XMLStreamException;)Lcom/fasterxml/aalto/UncheckedStreamException;

    move-result-object v0

    throw v0

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    sget-object v1, Lcom/fasterxml/aalto/impl/ErrorConsts;->ERR_STATE_NOT_PI:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final getPITarget()Ljava/lang/String;
    .locals 2

    iget v0, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_currToken:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_currName:Lcom/fasterxml/aalto/in/PName;

    invoke-virtual {v0}, Lcom/fasterxml/aalto/in/PName;->getLocalName()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    sget-object v1, Lcom/fasterxml/aalto/impl/ErrorConsts;->ERR_STATE_NOT_PI:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final getPrefix()Ljava/lang/String;
    .locals 2

    iget v0, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_currToken:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    sget-object v1, Lcom/fasterxml/aalto/impl/ErrorConsts;->ERR_STATE_NOT_ELEM:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_currName:Lcom/fasterxml/aalto/in/PName;

    invoke-virtual {v0}, Lcom/fasterxml/aalto/in/PName;->getPrefix()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    const-string v0, ""

    :cond_2
    return-object v0
.end method

.method public final getPrefixedName()Ljava/lang/String;
    .locals 2

    iget v0, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_currToken:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_3

    const/4 v1, 0x2

    if-eq v0, v1, :cond_3

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/16 v1, 0x9

    if-eq v0, v1, :cond_1

    const/16 v1, 0xb

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->getDTDRootName()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Current state not START_ELEMENT, END_ELEMENT, ENTITY_REFERENCE, PROCESSING_INSTRUCTION or DTD"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    invoke-virtual {p0}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->getLocalName()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_2
    invoke-virtual {p0}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->getPITarget()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_3
    iget-object v0, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_currName:Lcom/fasterxml/aalto/in/PName;

    invoke-virtual {v0}, Lcom/fasterxml/aalto/in/PName;->getPrefixedName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getProcessedDTD()Ljava/lang/Object;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public final getProcessedDTDSchema()Lorg/codehaus/stax2/validation/DTDValidationSchema;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getProperty(Ljava/lang/String;)Ljava/lang/Object;
    .locals 2

    const-string v0, "javax.xml.stream.entities"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object p1, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    return-object p1

    :cond_0
    const-string v0, "javax.xml.stream.notations"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object p1, Ljava/util/Collections;->EMPTY_LIST:Ljava/util/List;

    return-object p1

    :cond_1
    iget-object v0, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_scanner:Lcom/fasterxml/aalto/in/XmlScanner;

    invoke-virtual {v0}, Lcom/fasterxml/aalto/in/XmlScanner;->getConfig()Lcom/fasterxml/aalto/in/ReaderConfig;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/fasterxml/aalto/in/ReaderConfig;->getProperty(Ljava/lang/String;Z)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public getScanner()Lcom/fasterxml/aalto/in/XmlScanner;
    .locals 1

    iget-object v0, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_scanner:Lcom/fasterxml/aalto/in/XmlScanner;

    return-object v0
.end method

.method public final getStartLocation()Lorg/codehaus/stax2/XMLStreamLocation2;
    .locals 1

    iget-object v0, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_scanner:Lcom/fasterxml/aalto/in/XmlScanner;

    invoke-virtual {v0}, Lcom/fasterxml/aalto/in/XmlScanner;->getStartLocation()Lorg/codehaus/stax2/XMLStreamLocation2;

    move-result-object v0

    return-object v0
.end method

.method public final getStartingByteOffset()J
    .locals 2

    iget-object v0, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_scanner:Lcom/fasterxml/aalto/in/XmlScanner;

    invoke-virtual {v0}, Lcom/fasterxml/aalto/in/XmlScanner;->getStartingByteOffset()J

    move-result-wide v0

    return-wide v0
.end method

.method public final getStartingCharOffset()J
    .locals 2

    iget-object v0, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_scanner:Lcom/fasterxml/aalto/in/XmlScanner;

    invoke-virtual {v0}, Lcom/fasterxml/aalto/in/XmlScanner;->getStartingCharOffset()J

    move-result-wide v0

    return-wide v0
.end method

.method public final getText(Ljava/io/Writer;Z)I
    .locals 2

    .line 1
    iget v0, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_currToken:I

    const/4 v1, 0x1

    shl-int/2addr v1, v0

    and-int/lit16 v1, v1, 0x1a78

    if-nez v1, :cond_0

    invoke-direct {p0, v0}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->throwNotTextual(I)V

    :cond_0
    iget-object v0, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_scanner:Lcom/fasterxml/aalto/in/XmlScanner;

    invoke-virtual {v0, p1, p2}, Lcom/fasterxml/aalto/in/XmlScanner;->getText(Ljava/io/Writer;Z)I

    move-result p1

    return p1
.end method

.method public final getText()Ljava/lang/String;
    .locals 2

    .line 2
    iget v0, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_currToken:I

    const/4 v1, 0x1

    shl-int/2addr v1, v0

    and-int/lit16 v1, v1, 0x1a70

    if-nez v1, :cond_0

    invoke-direct {p0, v0}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->throwNotTextual(I)V

    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_scanner:Lcom/fasterxml/aalto/in/XmlScanner;

    invoke-virtual {v0}, Lcom/fasterxml/aalto/in/XmlScanner;->getText()Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catch Ljavax/xml/stream/XMLStreamException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/fasterxml/aalto/UncheckedStreamException;->createFrom(Ljavax/xml/stream/XMLStreamException;)Lcom/fasterxml/aalto/UncheckedStreamException;

    move-result-object v0

    throw v0
.end method

.method public final getTextCharacters(I[CII)I
    .locals 2

    .line 1
    iget v0, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_currToken:I

    const/4 v1, 0x1

    shl-int/2addr v1, v0

    and-int/lit16 v1, v1, 0x1070

    if-nez v1, :cond_0

    invoke-direct {p0, v0}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->throwNotTextXxx(I)V

    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_scanner:Lcom/fasterxml/aalto/in/XmlScanner;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/fasterxml/aalto/in/XmlScanner;->getTextCharacters(I[CII)I

    move-result p1
    :try_end_0
    .catch Ljavax/xml/stream/XMLStreamException; {:try_start_0 .. :try_end_0} :catch_0

    return p1

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcom/fasterxml/aalto/UncheckedStreamException;->createFrom(Ljavax/xml/stream/XMLStreamException;)Lcom/fasterxml/aalto/UncheckedStreamException;

    move-result-object p1

    throw p1
.end method

.method public final getTextCharacters()[C
    .locals 2

    .line 2
    iget v0, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_currToken:I

    const/4 v1, 0x1

    shl-int/2addr v1, v0

    and-int/lit16 v1, v1, 0x1070

    if-nez v1, :cond_0

    invoke-direct {p0, v0}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->throwNotTextXxx(I)V

    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_scanner:Lcom/fasterxml/aalto/in/XmlScanner;

    invoke-virtual {v0}, Lcom/fasterxml/aalto/in/XmlScanner;->getTextCharacters()[C

    move-result-object v0
    :try_end_0
    .catch Ljavax/xml/stream/XMLStreamException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/fasterxml/aalto/UncheckedStreamException;->createFrom(Ljavax/xml/stream/XMLStreamException;)Lcom/fasterxml/aalto/UncheckedStreamException;

    move-result-object v0

    throw v0
.end method

.method public final getTextLength()I
    .locals 2

    iget v0, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_currToken:I

    const/4 v1, 0x1

    shl-int/2addr v1, v0

    and-int/lit16 v1, v1, 0x1070

    if-nez v1, :cond_0

    invoke-direct {p0, v0}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->throwNotTextXxx(I)V

    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_scanner:Lcom/fasterxml/aalto/in/XmlScanner;

    invoke-virtual {v0}, Lcom/fasterxml/aalto/in/XmlScanner;->getTextLength()I

    move-result v0
    :try_end_0
    .catch Ljavax/xml/stream/XMLStreamException; {:try_start_0 .. :try_end_0} :catch_0

    return v0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/fasterxml/aalto/UncheckedStreamException;->createFrom(Ljavax/xml/stream/XMLStreamException;)Lcom/fasterxml/aalto/UncheckedStreamException;

    move-result-object v0

    throw v0
.end method

.method public final getTextStart()I
    .locals 2

    iget v0, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_currToken:I

    const/4 v1, 0x1

    shl-int/2addr v1, v0

    and-int/lit16 v1, v1, 0x1070

    if-nez v1, :cond_0

    invoke-direct {p0, v0}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->throwNotTextXxx(I)V

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public getVersion()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_scanner:Lcom/fasterxml/aalto/in/XmlScanner;

    invoke-virtual {v0}, Lcom/fasterxml/aalto/in/XmlScanner;->getConfig()Lcom/fasterxml/aalto/in/ReaderConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/fasterxml/aalto/in/ReaderConfig;->getXmlDeclVersion()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public handlePrologEoi(Z)I
    .locals 0

    invoke-virtual {p0}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->close()V

    if-eqz p1, :cond_0

    sget-object p1, Lcom/fasterxml/aalto/impl/ErrorConsts;->SUFFIX_IN_PROLOG:Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->throwUnexpectedEOI(Ljava/lang/String;)V

    :cond_0
    const/16 p1, 0x8

    return p1
.end method

.method public handleTreeEoi()V
    .locals 1

    const/16 v0, 0x8

    iput v0, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_currToken:I

    sget-object v0, Lcom/fasterxml/aalto/impl/ErrorConsts;->SUFFIX_IN_TREE:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->throwUnexpectedEOI(Ljava/lang/String;)V

    return-void
.end method

.method public final hasName()Z
    .locals 3

    iget v0, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_currToken:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 v2, 0x2

    if-ne v0, v2, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :cond_1
    :goto_0
    return v1
.end method

.method public final hasNext()Z
    .locals 2

    iget v0, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_currToken:I

    const/16 v1, 0x8

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final hasText()Z
    .locals 2

    iget v0, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_currToken:I

    const/4 v1, 0x1

    shl-int v0, v1, v0

    and-int/lit16 v0, v0, 0x1a70

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method public final isAttributeSpecified(I)Z
    .locals 2

    iget v0, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_currToken:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_scanner:Lcom/fasterxml/aalto/in/XmlScanner;

    invoke-virtual {v0, p1}, Lcom/fasterxml/aalto/in/XmlScanner;->isAttrSpecified(I)Z

    move-result p1

    return p1

    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    sget-object v0, Lcom/fasterxml/aalto/impl/ErrorConsts;->ERR_STATE_NOT_STELEM:Ljava/lang/String;

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public final isCharacters()Z
    .locals 2

    invoke-virtual {p0}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->getEventType()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final isEmptyElement()Z
    .locals 2

    iget v0, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_currToken:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_scanner:Lcom/fasterxml/aalto/in/XmlScanner;

    invoke-virtual {v0}, Lcom/fasterxml/aalto/in/XmlScanner;->isEmptyTag()Z

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final isEndElement()Z
    .locals 2

    iget v0, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_currToken:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final isPropertySupported(Ljava/lang/String;)Z
    .locals 1

    iget-object v0, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_scanner:Lcom/fasterxml/aalto/in/XmlScanner;

    invoke-virtual {v0}, Lcom/fasterxml/aalto/in/XmlScanner;->getConfig()Lcom/fasterxml/aalto/in/ReaderConfig;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/fasterxml/aalto/in/ReaderConfig;->isPropertySupported(Ljava/lang/String;)Z

    move-result p1

    return p1
.end method

.method public final isStandalone()Z
    .locals 2

    iget-object v0, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_scanner:Lcom/fasterxml/aalto/in/XmlScanner;

    invoke-virtual {v0}, Lcom/fasterxml/aalto/in/XmlScanner;->getConfig()Lcom/fasterxml/aalto/in/ReaderConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/fasterxml/aalto/in/ReaderConfig;->getXmlDeclStandalone()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method public final isStartElement()Z
    .locals 2

    iget v0, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_currToken:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method public final isWhiteSpace()Z
    .locals 2

    iget v0, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_currToken:I

    const/4 v1, 0x4

    if-eq v0, v1, :cond_2

    const/16 v1, 0xc

    if-ne v0, v1, :cond_0

    goto :goto_1

    :cond_0
    const/4 v1, 0x6

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_2
    :goto_1
    :try_start_0
    iget-object v0, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_scanner:Lcom/fasterxml/aalto/in/XmlScanner;

    invoke-virtual {v0}, Lcom/fasterxml/aalto/in/XmlScanner;->isTextWhitespace()Z

    move-result v0
    :try_end_0
    .catch Ljavax/xml/stream/XMLStreamException; {:try_start_0 .. :try_end_0} :catch_0

    return v0

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/fasterxml/aalto/UncheckedStreamException;->createFrom(Ljavax/xml/stream/XMLStreamException;)Lcom/fasterxml/aalto/UncheckedStreamException;

    move-result-object v0

    throw v0
.end method

.method public final next()I
    .locals 4

    iget v0, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_parseState:I

    const/4 v1, 0x2

    const/4 v2, 0x1

    if-ne v0, v2, :cond_5

    iget-object v0, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_scanner:Lcom/fasterxml/aalto/in/XmlScanner;

    invoke-virtual {v0}, Lcom/fasterxml/aalto/in/XmlScanner;->nextFromTree()I

    move-result v0

    const/4 v3, -0x1

    if-ne v0, v3, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->handleTreeEoi()V

    :cond_0
    iput v0, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_currToken:I

    const/16 v3, 0xc

    if-ne v0, v3, :cond_2

    iget-boolean v1, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_cfgCoalesceText:Z

    if-nez v1, :cond_1

    iget-boolean v1, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_cfgReportTextAsChars:Z

    if-eqz v1, :cond_4

    :cond_1
    const/4 v0, 0x4

    return v0

    :cond_2
    iget-object v3, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_scanner:Lcom/fasterxml/aalto/in/XmlScanner;

    invoke-virtual {v3}, Lcom/fasterxml/aalto/in/XmlScanner;->getName()Lcom/fasterxml/aalto/in/PName;

    move-result-object v3

    iput-object v3, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_currName:Lcom/fasterxml/aalto/in/PName;

    if-ne v0, v1, :cond_3

    iget-object v2, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_scanner:Lcom/fasterxml/aalto/in/XmlScanner;

    invoke-virtual {v2}, Lcom/fasterxml/aalto/in/XmlScanner;->hasEmptyStack()Z

    move-result v2

    if-eqz v2, :cond_4

    iput v1, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_parseState:I

    goto :goto_0

    :cond_3
    if-ne v0, v2, :cond_4

    iget-object v1, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_scanner:Lcom/fasterxml/aalto/in/XmlScanner;

    invoke-virtual {v1}, Lcom/fasterxml/aalto/in/XmlScanner;->getAttrCount()I

    move-result v1

    iput v1, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_attrCount:I

    :cond_4
    :goto_0
    return v0

    :cond_5
    const/4 v3, 0x0

    if-nez v0, :cond_8

    iget-object v0, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_scanner:Lcom/fasterxml/aalto/in/XmlScanner;

    invoke-virtual {v0, v2}, Lcom/fasterxml/aalto/in/XmlScanner;->nextFromProlog(Z)I

    move-result v0

    if-ne v0, v2, :cond_6

    iput v2, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_parseState:I

    iget-object v1, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_scanner:Lcom/fasterxml/aalto/in/XmlScanner;

    invoke-virtual {v1}, Lcom/fasterxml/aalto/in/XmlScanner;->getAttrCount()I

    move-result v1

    iput v1, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_attrCount:I

    goto :goto_1

    :cond_6
    const/16 v1, 0xb

    if-ne v0, v1, :cond_9

    iget-object v1, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_dtdRootName:Lcom/fasterxml/aalto/in/PName;

    if-eqz v1, :cond_7

    const-string v1, "Duplicate DOCTYPE declaration"

    invoke-virtual {p0, v1}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->throwWfe(Ljava/lang/String;)V

    :cond_7
    iget-object v1, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_scanner:Lcom/fasterxml/aalto/in/XmlScanner;

    invoke-virtual {v1}, Lcom/fasterxml/aalto/in/XmlScanner;->getName()Lcom/fasterxml/aalto/in/PName;

    move-result-object v1

    iput-object v1, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_dtdRootName:Lcom/fasterxml/aalto/in/PName;

    goto :goto_1

    :cond_8
    if-ne v0, v1, :cond_c

    iget-object v0, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_scanner:Lcom/fasterxml/aalto/in/XmlScanner;

    invoke-virtual {v0, v3}, Lcom/fasterxml/aalto/in/XmlScanner;->nextFromProlog(Z)I

    move-result v0

    :cond_9
    :goto_1
    if-gez v0, :cond_b

    iget v0, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_parseState:I

    if-nez v0, :cond_a

    goto :goto_2

    :cond_a
    move v2, v3

    :goto_2
    invoke-virtual {p0, v2}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->handlePrologEoi(Z)I

    move-result v0

    return v0

    :cond_b
    iget-object v1, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_scanner:Lcom/fasterxml/aalto/in/XmlScanner;

    invoke-virtual {v1}, Lcom/fasterxml/aalto/in/XmlScanner;->getName()Lcom/fasterxml/aalto/in/PName;

    move-result-object v1

    iput-object v1, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_currName:Lcom/fasterxml/aalto/in/PName;

    iput v0, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_currToken:I

    return v0

    :cond_c
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0
.end method

.method public final nextTag()I
    .locals 3

    :goto_0
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->next()I

    move-result v0

    const/16 v1, 0xc

    if-eq v0, v1, :cond_0

    packed-switch v0, :pswitch_data_0

    goto :goto_1

    :pswitch_1
    return v0

    :cond_0
    :pswitch_2
    invoke-virtual {p0}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->isWhiteSpace()Z

    move-result v1

    if-eqz v1, :cond_1

    goto :goto_0

    :cond_1
    const-string v1, "Received non-all-whitespace CHARACTERS or CDATA event in nextTag()."

    invoke-virtual {p0, v1}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->throwWfe(Ljava/lang/String;)V

    :goto_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Received event "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {v0}, Lcom/fasterxml/aalto/impl/ErrorConsts;->tokenTypeDesc(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, ", instead of START_ELEMENT or END_ELEMENT."

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->throwWfe(Ljava/lang/String;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final readElementAsArray(Lorg/codehaus/stax2/typed/TypedArrayDecoder;)I
    .locals 11

    iget v0, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_currToken:I

    const/4 v1, 0x1

    shl-int v2, v1, v0

    and-int/lit16 v2, v2, 0x1056

    if-eqz v2, :cond_c

    const/16 v2, 0xc

    const/4 v3, 0x3

    const/4 v4, 0x4

    const/4 v5, 0x5

    const/4 v6, 0x2

    const/4 v7, 0x0

    const/4 v8, -0x1

    if-ne v0, v1, :cond_4

    iget-object v0, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_scanner:Lcom/fasterxml/aalto/in/XmlScanner;

    invoke-virtual {v0}, Lcom/fasterxml/aalto/in/XmlScanner;->isEmptyTag()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->next()I

    return v8

    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->next()I

    move-result v0

    if-ne v0, v6, :cond_1

    return v8

    :cond_1
    if-eq v0, v5, :cond_0

    if-ne v0, v3, :cond_2

    goto :goto_0

    :cond_2
    if-eq v0, v4, :cond_9

    if-ne v0, v2, :cond_3

    goto :goto_4

    :cond_3
    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_constructUnexpectedInTyped(I)Ljavax/xml/stream/XMLStreamException;

    move-result-object p1

    throw p1

    :cond_4
    move v9, v7

    :goto_1
    if-eq v0, v6, :cond_a

    if-eq v0, v4, :cond_7

    if-eq v0, v2, :cond_7

    const/4 v10, 0x6

    if-ne v0, v10, :cond_5

    goto :goto_2

    :cond_5
    if-eq v0, v5, :cond_8

    if-ne v0, v3, :cond_6

    goto :goto_3

    :cond_6
    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_constructUnexpectedInTyped(I)Ljavax/xml/stream/XMLStreamException;

    move-result-object p1

    throw p1

    :cond_7
    :goto_2
    iget-object v0, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_scanner:Lcom/fasterxml/aalto/in/XmlScanner;

    invoke-virtual {v0, p1, v9}, Lcom/fasterxml/aalto/in/XmlScanner;->decodeElements(Lorg/codehaus/stax2/typed/TypedArrayDecoder;Z)I

    move-result v0

    add-int/2addr v7, v0

    invoke-virtual {p1}, Lorg/codehaus/stax2/typed/TypedArrayDecoder;->hasRoom()Z

    move-result v0

    if-nez v0, :cond_8

    goto :goto_5

    :cond_8
    :goto_3
    invoke-virtual {p0}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->next()I

    move-result v0

    :cond_9
    :goto_4
    move v9, v1

    goto :goto_1

    :cond_a
    :goto_5
    if-lez v7, :cond_b

    move v8, v7

    :cond_b
    return v8

    :cond_c
    new-instance p1, Ljava/lang/IllegalStateException;

    sget-object v0, Lcom/fasterxml/aalto/impl/ErrorConsts;->ERR_STATE_NOT_STELEM_OR_TEXT:Ljava/lang/String;

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public final readElementAsBinary([BII)I
    .locals 1

    .line 1
    invoke-static {}, Lorg/codehaus/stax2/typed/Base64Variants;->getDefaultVariant()Lorg/codehaus/stax2/typed/Base64Variant;

    move-result-object v0

    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->readElementAsBinary([BIILorg/codehaus/stax2/typed/Base64Variant;)I

    move-result p1

    return p1
.end method

.method public final readElementAsBinary([BIILorg/codehaus/stax2/typed/Base64Variant;)I
    .locals 11

    .line 2
    const-string v0, ""

    if-eqz p1, :cond_12

    if-ltz p2, :cond_11

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-lt p3, v2, :cond_f

    add-int v3, p2, p3

    array-length v4, p1

    if-le v3, v4, :cond_0

    goto/16 :goto_5

    :cond_0
    invoke-virtual {p0}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_base64Decoder()Lorg/codehaus/stax2/ri/typed/CharArrayBase64Decoder;

    move-result-object v3

    iget v4, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_currToken:I

    shl-int v5, v2, v4

    and-int/lit16 v5, v5, 0x1052

    const/4 v6, 0x3

    const/4 v7, 0x5

    const/4 v8, -0x1

    const/4 v9, 0x2

    if-nez v5, :cond_2

    if-ne v4, v9, :cond_1

    invoke-virtual {v3}, Lorg/codehaus/stax2/ri/typed/CharArrayBase64Decoder;->hasData()Z

    move-result v4

    if-nez v4, :cond_8

    return v8

    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    sget-object p2, Lcom/fasterxml/aalto/impl/ErrorConsts;->ERR_STATE_NOT_STELEM_OR_TEXT:Ljava/lang/String;

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_2
    if-ne v4, v2, :cond_8

    iget-object v4, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_scanner:Lcom/fasterxml/aalto/in/XmlScanner;

    invoke-virtual {v4}, Lcom/fasterxml/aalto/in/XmlScanner;->isEmptyTag()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-virtual {p0}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->next()I

    return v8

    :cond_3
    :goto_0
    invoke-virtual {p0}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->next()I

    move-result v4

    if-ne v4, v9, :cond_4

    return v8

    :cond_4
    if-eq v4, v7, :cond_3

    if-ne v4, v6, :cond_5

    goto :goto_0

    :cond_5
    const/4 v5, 0x4

    if-eq v4, v5, :cond_7

    const/16 v5, 0xc

    if-ne v4, v5, :cond_6

    goto :goto_1

    :cond_6
    invoke-virtual {p0, v4}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_constructUnexpectedInTyped(I)Ljavax/xml/stream/XMLStreamException;

    move-result-object p1

    throw p1

    :cond_7
    :goto_1
    iget-object v4, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_scanner:Lcom/fasterxml/aalto/in/XmlScanner;

    invoke-virtual {v4, p4, v3, v2}, Lcom/fasterxml/aalto/in/XmlScanner;->resetForDecoding(Lorg/codehaus/stax2/typed/Base64Variant;Lorg/codehaus/stax2/ri/typed/CharArrayBase64Decoder;Z)V

    :cond_8
    move v4, v1

    :goto_2
    :try_start_0
    invoke-virtual {v3, p1, p2, p3}, Lorg/codehaus/stax2/ri/typed/CharArrayBase64Decoder;->decode([BII)I

    move-result v5
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    add-int/2addr p2, v5

    add-int/2addr v4, v5

    sub-int/2addr p3, v5

    if-lt p3, v2, :cond_d

    iget v5, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_currToken:I

    if-ne v5, v9, :cond_9

    goto :goto_4

    :cond_9
    :goto_3
    invoke-virtual {p0}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->next()I

    move-result v5

    if-eq v5, v7, :cond_9

    if-eq v5, v6, :cond_9

    const/4 v10, 0x6

    if-ne v5, v10, :cond_a

    goto :goto_3

    :cond_a
    if-ne v5, v9, :cond_c

    invoke-virtual {v3}, Lorg/codehaus/stax2/ri/typed/CharArrayBase64Decoder;->endOfContent()I

    move-result v5

    if-ltz v5, :cond_b

    if-lez v5, :cond_d

    goto :goto_2

    :cond_b
    const-string p1, "Incomplete base64 triplet at the end of decoded content"

    invoke-direct {p0, p1, v0}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_constructTypeException(Ljava/lang/String;Ljava/lang/String;)Lorg/codehaus/stax2/typed/TypedXMLStreamException;

    move-result-object p1

    throw p1

    :cond_c
    iget-object v5, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_scanner:Lcom/fasterxml/aalto/in/XmlScanner;

    invoke-virtual {v5, p4, v3, v1}, Lcom/fasterxml/aalto/in/XmlScanner;->resetForDecoding(Lorg/codehaus/stax2/typed/Base64Variant;Lorg/codehaus/stax2/ri/typed/CharArrayBase64Decoder;Z)V

    goto :goto_2

    :cond_d
    :goto_4
    if-lez v4, :cond_e

    move v8, v4

    :cond_e
    return v8

    :catch_0
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1, v0}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_constructTypeException(Ljava/lang/String;Ljava/lang/String;)Lorg/codehaus/stax2/typed/TypedXMLStreamException;

    move-result-object p1

    throw p1

    :cond_f
    :goto_5
    if-nez p3, :cond_10

    return v1

    :cond_10
    new-instance p2, Ljava/lang/IllegalArgumentException;

    new-instance p4, Ljava/lang/StringBuilder;

    invoke-direct {p4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Illegal maxLength ("

    invoke-virtual {p4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p4, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p3, "), has to be positive number, and offset+maxLength can not exceed"

    invoke-virtual {p4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    array-length p1, p1

    invoke-virtual {p4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p2

    :cond_11
    new-instance p3, Ljava/lang/IllegalArgumentException;

    new-instance p4, Ljava/lang/StringBuilder;

    invoke-direct {p4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Illegal offset ("

    invoke-virtual {p4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p2, "), must be [0, "

    invoke-virtual {p4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    array-length p1, p1

    invoke-virtual {p4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, "["

    invoke-virtual {p4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p3, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p3

    :cond_12
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "resultBuffer is null"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public final readElementAsDoubleArray([DII)I
    .locals 1

    invoke-virtual {p0}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_decoderFactory()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;->getDoubleArrayDecoder([DII)Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$DoubleArrayDecoder;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->readElementAsArray(Lorg/codehaus/stax2/typed/TypedArrayDecoder;)I

    move-result p1

    return p1
.end method

.method public final readElementAsFloatArray([FII)I
    .locals 1

    invoke-virtual {p0}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_decoderFactory()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;->getFloatArrayDecoder([FII)Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$FloatArrayDecoder;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->readElementAsArray(Lorg/codehaus/stax2/typed/TypedArrayDecoder;)I

    move-result p1

    return p1
.end method

.method public final readElementAsIntArray([III)I
    .locals 1

    invoke-virtual {p0}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_decoderFactory()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;->getIntArrayDecoder([III)Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$IntArrayDecoder;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->readElementAsArray(Lorg/codehaus/stax2/typed/TypedArrayDecoder;)I

    move-result p1

    return p1
.end method

.method public final readElementAsLongArray([JII)I
    .locals 1

    invoke-virtual {p0}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_decoderFactory()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;->getLongArrayDecoder([JII)Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$LongArrayDecoder;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->readElementAsArray(Lorg/codehaus/stax2/typed/TypedArrayDecoder;)I

    move-result p1

    return p1
.end method

.method public reportInvalidAttrIndex(I)V
    .locals 3

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Illegal attribute index, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, ", current START_ELEMENT has "

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget p1, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_attrCount:I

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, " attributes"

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final require(ILjava/lang/String;Ljava/lang/String;)V
    .locals 7

    iget v0, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_currToken:I

    if-eq v0, p1, :cond_1

    const/16 v1, 0xc

    if-ne v0, v1, :cond_1

    iget-boolean v1, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_cfgCoalesceText:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_cfgReportTextAsChars:Z

    if-eqz v1, :cond_1

    :cond_0
    const/4 v0, 0x4

    :cond_1
    if-eq p1, v0, :cond_2

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expected type "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {p1}, Lcom/fasterxml/aalto/impl/ErrorConsts;->tokenTypeDesc(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, ", current type "

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {v0}, Lcom/fasterxml/aalto/impl/ErrorConsts;->tokenTypeDesc(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->throwWfe(Ljava/lang/String;)V

    :cond_2
    const-string p1, ")"

    const/4 v1, 0x2

    const/4 v2, 0x1

    const-string v3, "\'."

    if-eqz p3, :cond_4

    if-eq v0, v2, :cond_3

    if-eq v0, v1, :cond_3

    const/16 v4, 0x9

    if-eq v0, v4, :cond_3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Expected non-null local name, but current token not a START_ELEMENT, END_ELEMENT or ENTITY_REFERENCE (was "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v5, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_currToken:I

    invoke-static {v5}, Lcom/fasterxml/aalto/impl/ErrorConsts;->tokenTypeDesc(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->throwWfe(Ljava/lang/String;)V

    :cond_3
    invoke-virtual {p0}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->getLocalName()Ljava/lang/String;

    move-result-object v4

    if-eq v4, p3, :cond_4

    invoke-virtual {v4, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Expected local name \'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p3, "\'; current local name \'"

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p0, p3}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->throwWfe(Ljava/lang/String;)V

    :cond_4
    if-eqz p2, :cond_7

    if-eq v0, v2, :cond_5

    if-eq v0, v1, :cond_5

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Expected non-null NS URI, but current token not a START_ELEMENT or END_ELEMENT (was "

    invoke-virtual {p3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {v0}, Lcom/fasterxml/aalto/impl/ErrorConsts;->tokenTypeDesc(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->throwWfe(Ljava/lang/String;)V

    :cond_5
    invoke-virtual {p0}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->getNamespaceURI()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result p3

    if-nez p3, :cond_6

    if-eqz p1, :cond_7

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result p2

    if-lez p2, :cond_7

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string p3, "Expected empty namespace, instead have \'"

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_6
    if-eq p2, p1, :cond_7

    invoke-virtual {p2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p3

    if-nez p3, :cond_7

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Expected namespace \'"

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, "\'; have \'"

    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    :goto_0
    invoke-virtual {p0, p1}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->throwWfe(Ljava/lang/String;)V

    :cond_7
    return-void
.end method

.method public final setFeature(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    return-void
.end method

.method public final setProperty(Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 1

    iget-object v0, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_scanner:Lcom/fasterxml/aalto/in/XmlScanner;

    invoke-virtual {v0}, Lcom/fasterxml/aalto/in/XmlScanner;->getConfig()Lcom/fasterxml/aalto/in/ReaderConfig;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/fasterxml/aalto/in/ReaderConfig;->setProperty(Ljava/lang/String;Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public final setValidationProblemHandler(Lorg/codehaus/stax2/validation/ValidationProblemHandler;)Lorg/codehaus/stax2/validation/ValidationProblemHandler;
    .locals 0

    const/4 p1, 0x0

    return-object p1
.end method

.method public final skipElement()V
    .locals 4

    iget v0, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_currToken:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    move v0, v1

    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->next()I

    move-result v2

    if-ne v2, v1, :cond_1

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    add-int/lit8 v0, v0, -0x1

    if-nez v0, :cond_0

    return-void

    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    sget-object v1, Lcom/fasterxml/aalto/impl/ErrorConsts;->ERR_STATE_NOT_STELEM:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final standaloneSet()Z
    .locals 1

    iget-object v0, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_scanner:Lcom/fasterxml/aalto/in/XmlScanner;

    invoke-virtual {v0}, Lcom/fasterxml/aalto/in/XmlScanner;->getConfig()Lcom/fasterxml/aalto/in/ReaderConfig;

    move-result-object v0

    invoke-virtual {v0}, Lcom/fasterxml/aalto/in/ReaderConfig;->getXmlDeclStandalone()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final stopValidatingAgainst(Lorg/codehaus/stax2/validation/XMLValidationSchema;)Lorg/codehaus/stax2/validation/XMLValidator;
    .locals 0

    .line 1
    const/4 p1, 0x0

    return-object p1
.end method

.method public final stopValidatingAgainst(Lorg/codehaus/stax2/validation/XMLValidator;)Lorg/codehaus/stax2/validation/XMLValidator;
    .locals 0

    .line 2
    const/4 p1, 0x0

    return-object p1
.end method

.method public throwFromIOE(Ljava/io/IOException;)V
    .locals 1

    new-instance v0, Lcom/fasterxml/aalto/impl/IoStreamException;

    invoke-direct {v0, p1}, Lcom/fasterxml/aalto/impl/IoStreamException;-><init>(Ljava/io/IOException;)V

    throw v0
.end method

.method public throwUnexpectedEOI(Ljava/lang/String;)V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unexpected End-of-input"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->throwWfe(Ljava/lang/String;)V

    return-void
.end method

.method public throwWfe(Ljava/lang/String;)V
    .locals 2

    new-instance v0, Lcom/fasterxml/aalto/WFCException;

    invoke-virtual {p0}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->getLastCharLocation()Ljavax/xml/stream/Location;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Lcom/fasterxml/aalto/WFCException;-><init>(Ljava/lang/String;Ljavax/xml/stream/Location;)V

    throw v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[Aalto stream reader, scanner: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_scanner:Lcom/fasterxml/aalto/in/XmlScanner;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final validateAgainst(Lorg/codehaus/stax2/validation/XMLValidationSchema;)Lorg/codehaus/stax2/validation/XMLValidator;
    .locals 0

    const/4 p1, 0x0

    return-object p1
.end method

.method public verifyQName(Ljavax/xml/namespace/QName;)Ljavax/xml/namespace/QName;
    .locals 4

    invoke-virtual {p1}, Ljavax/xml/namespace/QName;->getLocalPart()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/fasterxml/aalto/util/XmlNames;->findIllegalNameChar(Ljava/lang/String;Z)I

    move-result v1

    if-ltz v1, :cond_1

    invoke-virtual {p1}, Ljavax/xml/namespace/QName;->getPrefix()Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, ":"

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_0
    move-object p1, v0

    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Invalid local name \""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "\" (character at #"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, " is invalid)"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_constructTypeException(Ljava/lang/String;Ljava/lang/String;)Lorg/codehaus/stax2/typed/TypedXMLStreamException;

    move-result-object p1

    throw p1

    :cond_1
    return-object p1
.end method

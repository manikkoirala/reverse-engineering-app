.class public final Lcom/fasterxml/aalto/stax/InputFactoryImpl;
.super Lcom/fasterxml/aalto/AsyncXMLInputFactory;
.source "SourceFile"


# instance fields
.field protected _allocator:Ljavax/xml/stream/util/XMLEventAllocator;

.field final _config:Lcom/fasterxml/aalto/in/ReaderConfig;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/fasterxml/aalto/AsyncXMLInputFactory;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/fasterxml/aalto/stax/InputFactoryImpl;->_allocator:Ljavax/xml/stream/util/XMLEventAllocator;

    new-instance v0, Lcom/fasterxml/aalto/in/ReaderConfig;

    invoke-direct {v0}, Lcom/fasterxml/aalto/in/ReaderConfig;-><init>()V

    iput-object v0, p0, Lcom/fasterxml/aalto/stax/InputFactoryImpl;->_config:Lcom/fasterxml/aalto/in/ReaderConfig;

    return-void
.end method


# virtual methods
.method public configureForConvenience()V
    .locals 1

    iget-object v0, p0, Lcom/fasterxml/aalto/stax/InputFactoryImpl;->_config:Lcom/fasterxml/aalto/in/ReaderConfig;

    invoke-virtual {v0}, Lcom/fasterxml/aalto/in/ReaderConfig;->configureForConvenience()V

    return-void
.end method

.method public configureForLowMemUsage()V
    .locals 1

    iget-object v0, p0, Lcom/fasterxml/aalto/stax/InputFactoryImpl;->_config:Lcom/fasterxml/aalto/in/ReaderConfig;

    invoke-virtual {v0}, Lcom/fasterxml/aalto/in/ReaderConfig;->configureForLowMemUsage()V

    return-void
.end method

.method public configureForRoundTripping()V
    .locals 1

    iget-object v0, p0, Lcom/fasterxml/aalto/stax/InputFactoryImpl;->_config:Lcom/fasterxml/aalto/in/ReaderConfig;

    invoke-virtual {v0}, Lcom/fasterxml/aalto/in/ReaderConfig;->configureForRoundTripping()V

    return-void
.end method

.method public configureForSpeed()V
    .locals 1

    iget-object v0, p0, Lcom/fasterxml/aalto/stax/InputFactoryImpl;->_config:Lcom/fasterxml/aalto/in/ReaderConfig;

    invoke-virtual {v0}, Lcom/fasterxml/aalto/in/ReaderConfig;->configureForSpeed()V

    return-void
.end method

.method public configureForXmlConformance()V
    .locals 1

    iget-object v0, p0, Lcom/fasterxml/aalto/stax/InputFactoryImpl;->_config:Lcom/fasterxml/aalto/in/ReaderConfig;

    invoke-virtual {v0}, Lcom/fasterxml/aalto/in/ReaderConfig;->configureForXmlConformance()V

    return-void
.end method

.method public constructER(Lorg/codehaus/stax2/XMLStreamReader2;)Lorg/codehaus/stax2/XMLEventReader2;
    .locals 2

    new-instance v0, Lcom/fasterxml/aalto/evt/EventReaderImpl;

    invoke-virtual {p0}, Lcom/fasterxml/aalto/stax/InputFactoryImpl;->createEventAllocator()Ljavax/xml/stream/util/XMLEventAllocator;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/fasterxml/aalto/evt/EventReaderImpl;-><init>(Ljavax/xml/stream/util/XMLEventAllocator;Lorg/codehaus/stax2/XMLStreamReader2;)V

    return-object v0
.end method

.method public constructSR(Ljava/io/File;Z)Lorg/codehaus/stax2/XMLStreamReader2;
    .locals 7

    .line 1
    :try_start_0
    new-instance v0, Ljava/io/FileInputStream;

    invoke-direct {v0, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-static {p1}, Lcom/fasterxml/aalto/util/URLUtil;->fileToSystemId(Ljava/io/File;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v6, 0x1

    move-object v1, p0

    move v5, p2

    invoke-virtual/range {v1 .. v6}, Lcom/fasterxml/aalto/stax/InputFactoryImpl;->getNonSharedConfig(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)Lcom/fasterxml/aalto/in/ReaderConfig;

    move-result-object p1

    invoke-static {p1, v0}, Lcom/fasterxml/aalto/in/ByteSourceBootstrapper;->construct(Lcom/fasterxml/aalto/in/ReaderConfig;Ljava/io/InputStream;)Lcom/fasterxml/aalto/in/ByteSourceBootstrapper;

    move-result-object p1

    invoke-static {p1}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->construct(Lcom/fasterxml/aalto/in/InputBootstrapper;)Lcom/fasterxml/aalto/stax/StreamReaderImpl;

    move-result-object p1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    new-instance p2, Lcom/fasterxml/aalto/impl/IoStreamException;

    invoke-direct {p2, p1}, Lcom/fasterxml/aalto/impl/IoStreamException;-><init>(Ljava/io/IOException;)V

    throw p2
.end method

.method public constructSR(Ljava/io/InputStream;Ljava/lang/String;Z)Lorg/codehaus/stax2/XMLStreamReader2;
    .locals 6

    .line 2
    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    move-object v3, p2

    move v4, p3

    invoke-virtual/range {v0 .. v5}, Lcom/fasterxml/aalto/stax/InputFactoryImpl;->getNonSharedConfig(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)Lcom/fasterxml/aalto/in/ReaderConfig;

    move-result-object p2

    invoke-static {p2, p1}, Lcom/fasterxml/aalto/in/ByteSourceBootstrapper;->construct(Lcom/fasterxml/aalto/in/ReaderConfig;Ljava/io/InputStream;)Lcom/fasterxml/aalto/in/ByteSourceBootstrapper;

    move-result-object p1

    invoke-static {p1}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->construct(Lcom/fasterxml/aalto/in/InputBootstrapper;)Lcom/fasterxml/aalto/stax/StreamReaderImpl;

    move-result-object p1

    return-object p1
.end method

.method public constructSR(Ljava/lang/String;Ljava/io/InputStream;Z)Lorg/codehaus/stax2/XMLStreamReader2;
    .locals 6

    .line 3
    const/4 v1, 0x0

    const/4 v3, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    move-object v2, p1

    move v4, p3

    invoke-virtual/range {v0 .. v5}, Lcom/fasterxml/aalto/stax/InputFactoryImpl;->getNonSharedConfig(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)Lcom/fasterxml/aalto/in/ReaderConfig;

    move-result-object p1

    invoke-static {p1, p2}, Lcom/fasterxml/aalto/in/ByteSourceBootstrapper;->construct(Lcom/fasterxml/aalto/in/ReaderConfig;Ljava/io/InputStream;)Lcom/fasterxml/aalto/in/ByteSourceBootstrapper;

    move-result-object p1

    invoke-static {p1}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->construct(Lcom/fasterxml/aalto/in/InputBootstrapper;)Lcom/fasterxml/aalto/stax/StreamReaderImpl;

    move-result-object p1

    return-object p1
.end method

.method public constructSR(Ljava/lang/String;Ljava/io/Reader;Z)Lorg/codehaus/stax2/XMLStreamReader2;
    .locals 6

    .line 4
    const/4 v1, 0x0

    const/4 v3, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    move-object v2, p1

    move v4, p3

    invoke-virtual/range {v0 .. v5}, Lcom/fasterxml/aalto/stax/InputFactoryImpl;->getNonSharedConfig(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)Lcom/fasterxml/aalto/in/ReaderConfig;

    move-result-object p1

    invoke-static {p1, p2}, Lcom/fasterxml/aalto/in/CharSourceBootstrapper;->construct(Lcom/fasterxml/aalto/in/ReaderConfig;Ljava/io/Reader;)Lcom/fasterxml/aalto/in/CharSourceBootstrapper;

    move-result-object p1

    invoke-static {p1}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->construct(Lcom/fasterxml/aalto/in/InputBootstrapper;)Lcom/fasterxml/aalto/stax/StreamReaderImpl;

    move-result-object p1

    return-object p1
.end method

.method public constructSR(Ljava/net/URL;Z)Lorg/codehaus/stax2/XMLStreamReader2;
    .locals 7

    .line 5
    :try_start_0
    invoke-static {p1}, Lcom/fasterxml/aalto/util/URLUtil;->inputStreamFromURL(Ljava/net/URL;)Ljava/io/InputStream;

    move-result-object v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    invoke-static {p1}, Lcom/fasterxml/aalto/util/URLUtil;->urlToSystemId(Ljava/net/URL;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v6, 0x1

    move-object v1, p0

    move v5, p2

    invoke-virtual/range {v1 .. v6}, Lcom/fasterxml/aalto/stax/InputFactoryImpl;->getNonSharedConfig(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)Lcom/fasterxml/aalto/in/ReaderConfig;

    move-result-object p1

    invoke-static {p1, v0}, Lcom/fasterxml/aalto/in/ByteSourceBootstrapper;->construct(Lcom/fasterxml/aalto/in/ReaderConfig;Ljava/io/InputStream;)Lcom/fasterxml/aalto/in/ByteSourceBootstrapper;

    move-result-object p1

    invoke-static {p1}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->construct(Lcom/fasterxml/aalto/in/InputBootstrapper;)Lcom/fasterxml/aalto/stax/StreamReaderImpl;

    move-result-object p1

    return-object p1

    :catch_0
    move-exception p1

    new-instance p2, Lcom/fasterxml/aalto/impl/IoStreamException;

    invoke-direct {p2, p1}, Lcom/fasterxml/aalto/impl/IoStreamException;-><init>(Ljava/io/IOException;)V

    throw p2
.end method

.method public constructSR(Ljavax/xml/transform/Source;Z)Lorg/codehaus/stax2/XMLStreamReader2;
    .locals 11

    .line 6
    instance-of v0, p1, Lorg/codehaus/stax2/io/Stax2Source;

    if-eqz v0, :cond_0

    check-cast p1, Lorg/codehaus/stax2/io/Stax2Source;

    invoke-virtual {p0, p1, p2}, Lcom/fasterxml/aalto/stax/InputFactoryImpl;->constructSR2(Lorg/codehaus/stax2/io/Stax2Source;Z)Lorg/codehaus/stax2/XMLStreamReader2;

    move-result-object p1

    return-object p1

    :cond_0
    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    instance-of v0, p1, Ljavax/xml/transform/stream/StreamSource;

    const/4 v4, 0x0

    if-eqz v0, :cond_2

    check-cast p1, Ljavax/xml/transform/stream/StreamSource;

    invoke-virtual {p1}, Ljavax/xml/transform/stream/StreamSource;->getSystemId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Ljavax/xml/transform/stream/StreamSource;->getPublicId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Ljavax/xml/transform/stream/StreamSource;->getInputStream()Ljava/io/InputStream;

    move-result-object v2

    if-nez v2, :cond_1

    invoke-virtual {p1}, Ljavax/xml/transform/stream/StreamSource;->getReader()Ljava/io/Reader;

    move-result-object v4

    :cond_1
    move-object v6, v1

    move-object v8, v3

    goto :goto_1

    :cond_2
    instance-of v0, p1, Ljavax/xml/transform/sax/SAXSource;

    if-eqz v0, :cond_8

    check-cast p1, Ljavax/xml/transform/sax/SAXSource;

    invoke-virtual {p1}, Ljavax/xml/transform/sax/SAXSource;->getSystemId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Ljavax/xml/transform/sax/SAXSource;->getInputSource()Lorg/xml/sax/InputSource;

    move-result-object p1

    if-eqz p1, :cond_4

    invoke-virtual {p1}, Lorg/xml/sax/InputSource;->getSystemId()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lorg/xml/sax/InputSource;->getPublicId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lorg/xml/sax/InputSource;->getEncoding()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lorg/xml/sax/InputSource;->getByteStream()Ljava/io/InputStream;

    move-result-object v2

    if-nez v2, :cond_3

    invoke-virtual {p1}, Lorg/xml/sax/InputSource;->getCharacterStream()Ljava/io/Reader;

    move-result-object v4

    :cond_3
    move-object p1, v4

    move-object v4, v2

    goto :goto_0

    :cond_4
    move-object p1, v4

    :goto_0
    move-object v6, v1

    move-object v8, v3

    move-object v2, v4

    move-object v4, p1

    :goto_1
    const/4 v10, 0x0

    if-eqz v2, :cond_5

    move-object v5, p0

    move-object v7, v0

    move v9, p2

    invoke-virtual/range {v5 .. v10}, Lcom/fasterxml/aalto/stax/InputFactoryImpl;->getNonSharedConfig(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)Lcom/fasterxml/aalto/in/ReaderConfig;

    move-result-object p1

    invoke-static {p1, v2}, Lcom/fasterxml/aalto/in/ByteSourceBootstrapper;->construct(Lcom/fasterxml/aalto/in/ReaderConfig;Ljava/io/InputStream;)Lcom/fasterxml/aalto/in/ByteSourceBootstrapper;

    move-result-object p1

    invoke-static {p1}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->construct(Lcom/fasterxml/aalto/in/InputBootstrapper;)Lcom/fasterxml/aalto/stax/StreamReaderImpl;

    move-result-object p1

    return-object p1

    :cond_5
    if-eqz v4, :cond_6

    move-object v5, p0

    move-object v7, v0

    move v9, p2

    invoke-virtual/range {v5 .. v10}, Lcom/fasterxml/aalto/stax/InputFactoryImpl;->getNonSharedConfig(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)Lcom/fasterxml/aalto/in/ReaderConfig;

    move-result-object p1

    invoke-static {p1, v4}, Lcom/fasterxml/aalto/in/CharSourceBootstrapper;->construct(Lcom/fasterxml/aalto/in/ReaderConfig;Ljava/io/Reader;)Lcom/fasterxml/aalto/in/CharSourceBootstrapper;

    move-result-object p1

    invoke-static {p1}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->construct(Lcom/fasterxml/aalto/in/InputBootstrapper;)Lcom/fasterxml/aalto/stax/StreamReaderImpl;

    move-result-object p1

    return-object p1

    :cond_6
    if-eqz v0, :cond_7

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result p1

    if-lez p1, :cond_7

    const/4 v10, 0x1

    move-object v5, p0

    move-object v7, v0

    move v9, p2

    invoke-virtual/range {v5 .. v10}, Lcom/fasterxml/aalto/stax/InputFactoryImpl;->getNonSharedConfig(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)Lcom/fasterxml/aalto/in/ReaderConfig;

    move-result-object p1

    :try_start_0
    invoke-static {v0}, Lcom/fasterxml/aalto/util/URLUtil;->urlFromSystemId(Ljava/lang/String;)Ljava/net/URL;

    move-result-object p2

    invoke-static {p2}, Lcom/fasterxml/aalto/util/URLUtil;->inputStreamFromURL(Ljava/net/URL;)Ljava/io/InputStream;

    move-result-object p2

    invoke-static {p1, p2}, Lcom/fasterxml/aalto/in/ByteSourceBootstrapper;->construct(Lcom/fasterxml/aalto/in/ReaderConfig;Ljava/io/InputStream;)Lcom/fasterxml/aalto/in/ByteSourceBootstrapper;

    move-result-object p1

    invoke-static {p1}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->construct(Lcom/fasterxml/aalto/in/InputBootstrapper;)Lcom/fasterxml/aalto/stax/StreamReaderImpl;

    move-result-object p1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p1

    new-instance p2, Lcom/fasterxml/aalto/impl/IoStreamException;

    invoke-direct {p2, p1}, Lcom/fasterxml/aalto/impl/IoStreamException;-><init>(Ljava/io/IOException;)V

    throw p2

    :cond_7
    new-instance p1, Ljavax/xml/stream/XMLStreamException;

    const-string p2, "Can not create Stax reader for the Source passed -- neither reader, input stream nor system id was accessible; can not use other types of sources (like embedded SAX streams)"

    invoke-direct {p1, p2}, Ljavax/xml/stream/XMLStreamException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_8
    instance-of v0, p1, Ljavax/xml/transform/dom/DOMSource;

    if-eqz v0, :cond_9

    const/4 v5, 0x0

    move-object v0, p0

    move v4, p2

    invoke-virtual/range {v0 .. v5}, Lcom/fasterxml/aalto/stax/InputFactoryImpl;->getNonSharedConfig(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)Lcom/fasterxml/aalto/in/ReaderConfig;

    move-result-object p2

    check-cast p1, Ljavax/xml/transform/dom/DOMSource;

    invoke-static {p1, p2}, Lcom/fasterxml/aalto/dom/DOMReaderImpl;->createFrom(Ljavax/xml/transform/dom/DOMSource;Lcom/fasterxml/aalto/in/ReaderConfig;)Lcom/fasterxml/aalto/dom/DOMReaderImpl;

    move-result-object p1

    return-object p1

    :cond_9
    new-instance p2, Ljava/lang/IllegalArgumentException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Can not instantiate StAX reader for XML source type "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p1, " (unrecognized type)"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p2
.end method

.method public constructSR2(Lorg/codehaus/stax2/io/Stax2Source;Z)Lorg/codehaus/stax2/XMLStreamReader2;
    .locals 6

    invoke-virtual {p1}, Lorg/codehaus/stax2/io/Stax2Source;->getPublicId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lorg/codehaus/stax2/io/Stax2Source;->getSystemId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lorg/codehaus/stax2/io/Stax2Source;->getEncoding()Ljava/lang/String;

    move-result-object v3

    const/4 v5, 0x1

    move-object v0, p0

    move v4, p2

    invoke-virtual/range {v0 .. v5}, Lcom/fasterxml/aalto/stax/InputFactoryImpl;->getNonSharedConfig(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)Lcom/fasterxml/aalto/in/ReaderConfig;

    move-result-object p2

    instance-of v0, p1, Lorg/codehaus/stax2/io/Stax2ByteArraySource;

    if-eqz v0, :cond_0

    check-cast p1, Lorg/codehaus/stax2/io/Stax2ByteArraySource;

    invoke-virtual {p1}, Lorg/codehaus/stax2/io/Stax2ByteArraySource;->getBuffer()[B

    move-result-object v0

    invoke-virtual {p1}, Lorg/codehaus/stax2/io/Stax2ByteArraySource;->getBufferStart()I

    move-result v1

    invoke-virtual {p1}, Lorg/codehaus/stax2/io/Stax2ByteArraySource;->getBufferLength()I

    move-result p1

    invoke-static {p2, v0, v1, p1}, Lcom/fasterxml/aalto/in/ByteSourceBootstrapper;->construct(Lcom/fasterxml/aalto/in/ReaderConfig;[BII)Lcom/fasterxml/aalto/in/ByteSourceBootstrapper;

    move-result-object p1

    :goto_0
    invoke-static {p1}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->construct(Lcom/fasterxml/aalto/in/InputBootstrapper;)Lcom/fasterxml/aalto/stax/StreamReaderImpl;

    move-result-object p1

    return-object p1

    :cond_0
    instance-of v0, p1, Lorg/codehaus/stax2/io/Stax2CharArraySource;

    if-eqz v0, :cond_1

    check-cast p1, Lorg/codehaus/stax2/io/Stax2CharArraySource;

    invoke-virtual {p1}, Lorg/codehaus/stax2/io/Stax2CharArraySource;->getBuffer()[C

    move-result-object v0

    invoke-virtual {p1}, Lorg/codehaus/stax2/io/Stax2CharArraySource;->getBufferStart()I

    move-result v1

    invoke-virtual {p1}, Lorg/codehaus/stax2/io/Stax2CharArraySource;->getBufferLength()I

    move-result p1

    invoke-static {p2, v0, v1, p1}, Lcom/fasterxml/aalto/in/CharSourceBootstrapper;->construct(Lcom/fasterxml/aalto/in/ReaderConfig;[CII)Lcom/fasterxml/aalto/in/CharSourceBootstrapper;

    move-result-object p1

    goto :goto_0

    :cond_1
    :try_start_0
    invoke-virtual {p1}, Lorg/codehaus/stax2/io/Stax2Source;->constructInputStream()Ljava/io/InputStream;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-static {p2, v0}, Lcom/fasterxml/aalto/in/ByteSourceBootstrapper;->construct(Lcom/fasterxml/aalto/in/ReaderConfig;Ljava/io/InputStream;)Lcom/fasterxml/aalto/in/ByteSourceBootstrapper;

    move-result-object p1

    invoke-static {p1}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->construct(Lcom/fasterxml/aalto/in/InputBootstrapper;)Lcom/fasterxml/aalto/stax/StreamReaderImpl;

    move-result-object p1

    return-object p1

    :cond_2
    invoke-virtual {p1}, Lorg/codehaus/stax2/io/Stax2Source;->constructReader()Ljava/io/Reader;

    move-result-object p1

    if-eqz p1, :cond_3

    invoke-static {p2, p1}, Lcom/fasterxml/aalto/in/CharSourceBootstrapper;->construct(Lcom/fasterxml/aalto/in/ReaderConfig;Ljava/io/Reader;)Lcom/fasterxml/aalto/in/CharSourceBootstrapper;

    move-result-object p1

    invoke-static {p1}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->construct(Lcom/fasterxml/aalto/in/InputBootstrapper;)Lcom/fasterxml/aalto/stax/StreamReaderImpl;

    move-result-object p1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :cond_3
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Can not create stream reader for given Stax2Source: neither InputStream nor Reader available"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    :catch_0
    move-exception p1

    new-instance p2, Lcom/fasterxml/aalto/impl/IoStreamException;

    invoke-direct {p2, p1}, Lcom/fasterxml/aalto/impl/IoStreamException;-><init>(Ljava/io/IOException;)V

    throw p2
.end method

.method public createAsyncFor(Ljava/nio/ByteBuffer;)Lcom/fasterxml/aalto/AsyncXMLStreamReader;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/nio/ByteBuffer;",
            ")",
            "Lcom/fasterxml/aalto/AsyncXMLStreamReader<",
            "Lcom/fasterxml/aalto/AsyncByteBufferFeeder;",
            ">;"
        }
    .end annotation

    .line 1
    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/fasterxml/aalto/stax/InputFactoryImpl;->getNonSharedConfig(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)Lcom/fasterxml/aalto/in/ReaderConfig;

    move-result-object v0

    const-string v1, "UTF-8"

    invoke-virtual {v0, v1}, Lcom/fasterxml/aalto/in/ReaderConfig;->setActualEncoding(Ljava/lang/String;)V

    new-instance v1, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;

    invoke-direct {v1, v0}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;-><init>(Lcom/fasterxml/aalto/in/ReaderConfig;)V

    invoke-virtual {v1, p1}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->feedInput(Ljava/nio/ByteBuffer;)V

    new-instance p1, Lcom/fasterxml/aalto/async/AsyncStreamReaderImpl;

    invoke-direct {p1, v1}, Lcom/fasterxml/aalto/async/AsyncStreamReaderImpl;-><init>(Lcom/fasterxml/aalto/async/AsyncByteScanner;)V

    return-object p1
.end method

.method public createAsyncFor([B)Lcom/fasterxml/aalto/AsyncXMLStreamReader;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([B)",
            "Lcom/fasterxml/aalto/AsyncXMLStreamReader<",
            "Lcom/fasterxml/aalto/AsyncByteArrayFeeder;",
            ">;"
        }
    .end annotation

    .line 2
    array-length v0, p1

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v1, v0}, Lcom/fasterxml/aalto/stax/InputFactoryImpl;->createAsyncFor([BII)Lcom/fasterxml/aalto/AsyncXMLStreamReader;

    move-result-object p1

    return-object p1
.end method

.method public createAsyncFor([BII)Lcom/fasterxml/aalto/AsyncXMLStreamReader;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([BII)",
            "Lcom/fasterxml/aalto/AsyncXMLStreamReader<",
            "Lcom/fasterxml/aalto/AsyncByteArrayFeeder;",
            ">;"
        }
    .end annotation

    .line 3
    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/fasterxml/aalto/stax/InputFactoryImpl;->getNonSharedConfig(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)Lcom/fasterxml/aalto/in/ReaderConfig;

    move-result-object v0

    const-string v1, "UTF-8"

    invoke-virtual {v0, v1}, Lcom/fasterxml/aalto/in/ReaderConfig;->setActualEncoding(Ljava/lang/String;)V

    new-instance v1, Lcom/fasterxml/aalto/async/AsyncByteArrayScanner;

    invoke-direct {v1, v0}, Lcom/fasterxml/aalto/async/AsyncByteArrayScanner;-><init>(Lcom/fasterxml/aalto/in/ReaderConfig;)V

    invoke-virtual {v1, p1, p2, p3}, Lcom/fasterxml/aalto/async/AsyncByteArrayScanner;->feedInput([BII)V

    new-instance p1, Lcom/fasterxml/aalto/async/AsyncStreamReaderImpl;

    invoke-direct {p1, v1}, Lcom/fasterxml/aalto/async/AsyncStreamReaderImpl;-><init>(Lcom/fasterxml/aalto/async/AsyncByteScanner;)V

    return-object p1
.end method

.method public createAsyncForByteArray()Lcom/fasterxml/aalto/AsyncXMLStreamReader;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/fasterxml/aalto/AsyncXMLStreamReader<",
            "Lcom/fasterxml/aalto/AsyncByteArrayFeeder;",
            ">;"
        }
    .end annotation

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/fasterxml/aalto/stax/InputFactoryImpl;->getNonSharedConfig(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)Lcom/fasterxml/aalto/in/ReaderConfig;

    move-result-object v0

    const-string v1, "UTF-8"

    invoke-virtual {v0, v1}, Lcom/fasterxml/aalto/in/ReaderConfig;->setActualEncoding(Ljava/lang/String;)V

    new-instance v1, Lcom/fasterxml/aalto/async/AsyncStreamReaderImpl;

    new-instance v2, Lcom/fasterxml/aalto/async/AsyncByteArrayScanner;

    invoke-direct {v2, v0}, Lcom/fasterxml/aalto/async/AsyncByteArrayScanner;-><init>(Lcom/fasterxml/aalto/in/ReaderConfig;)V

    invoke-direct {v1, v2}, Lcom/fasterxml/aalto/async/AsyncStreamReaderImpl;-><init>(Lcom/fasterxml/aalto/async/AsyncByteScanner;)V

    return-object v1
.end method

.method public createAsyncForByteBuffer()Lcom/fasterxml/aalto/AsyncXMLStreamReader;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/fasterxml/aalto/AsyncXMLStreamReader<",
            "Lcom/fasterxml/aalto/AsyncByteBufferFeeder;",
            ">;"
        }
    .end annotation

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/fasterxml/aalto/stax/InputFactoryImpl;->getNonSharedConfig(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)Lcom/fasterxml/aalto/in/ReaderConfig;

    move-result-object v0

    const-string v1, "UTF-8"

    invoke-virtual {v0, v1}, Lcom/fasterxml/aalto/in/ReaderConfig;->setActualEncoding(Ljava/lang/String;)V

    new-instance v1, Lcom/fasterxml/aalto/async/AsyncStreamReaderImpl;

    new-instance v2, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;

    invoke-direct {v2, v0}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;-><init>(Lcom/fasterxml/aalto/in/ReaderConfig;)V

    invoke-direct {v1, v2}, Lcom/fasterxml/aalto/async/AsyncStreamReaderImpl;-><init>(Lcom/fasterxml/aalto/async/AsyncByteScanner;)V

    return-object v1
.end method

.method public createEventAllocator()Ljavax/xml/stream/util/XMLEventAllocator;
    .locals 1

    iget-object v0, p0, Lcom/fasterxml/aalto/stax/InputFactoryImpl;->_allocator:Ljavax/xml/stream/util/XMLEventAllocator;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljavax/xml/stream/util/XMLEventAllocator;->newInstance()Ljavax/xml/stream/util/XMLEventAllocator;

    move-result-object v0

    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/fasterxml/aalto/stax/InputFactoryImpl;->_config:Lcom/fasterxml/aalto/in/ReaderConfig;

    invoke-virtual {v0}, Lcom/fasterxml/aalto/in/ReaderConfig;->willPreserveLocation()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/fasterxml/aalto/evt/EventAllocatorImpl;->getDefaultInstance()Lcom/fasterxml/aalto/evt/EventAllocatorImpl;

    move-result-object v0

    goto :goto_0

    :cond_1
    invoke-static {}, Lcom/fasterxml/aalto/evt/EventAllocatorImpl;->getFastInstance()Lcom/fasterxml/aalto/evt/EventAllocatorImpl;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public createFilteredReader(Ljavax/xml/stream/XMLEventReader;Ljavax/xml/stream/EventFilter;)Ljavax/xml/stream/XMLEventReader;
    .locals 1

    .line 1
    new-instance v0, Lorg/codehaus/stax2/ri/evt/Stax2FilteredEventReader;

    invoke-static {p1}, Lorg/codehaus/stax2/ri/evt/Stax2EventReaderAdapter;->wrapIfNecessary(Ljavax/xml/stream/XMLEventReader;)Lorg/codehaus/stax2/XMLEventReader2;

    move-result-object p1

    invoke-direct {v0, p1, p2}, Lorg/codehaus/stax2/ri/evt/Stax2FilteredEventReader;-><init>(Lorg/codehaus/stax2/XMLEventReader2;Ljavax/xml/stream/EventFilter;)V

    return-object v0
.end method

.method public createFilteredReader(Ljavax/xml/stream/XMLStreamReader;Ljavax/xml/stream/StreamFilter;)Ljavax/xml/stream/XMLStreamReader;
    .locals 1

    .line 2
    new-instance v0, Lorg/codehaus/stax2/ri/Stax2FilteredStreamReader;

    invoke-direct {v0, p1, p2}, Lorg/codehaus/stax2/ri/Stax2FilteredStreamReader;-><init>(Ljavax/xml/stream/XMLStreamReader;Ljavax/xml/stream/StreamFilter;)V

    invoke-interface {p2, v0}, Ljavax/xml/stream/StreamFilter;->accept(Ljavax/xml/stream/XMLStreamReader;)Z

    move-result p1

    if-nez p1, :cond_0

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/Stax2FilteredStreamReader;->next()I

    :cond_0
    return-object v0
.end method

.method public createXMLEventReader(Ljava/io/InputStream;)Ljavax/xml/stream/XMLEventReader;
    .locals 1

    .line 1
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/fasterxml/aalto/stax/InputFactoryImpl;->createXMLEventReader(Ljava/io/InputStream;Ljava/lang/String;)Ljavax/xml/stream/XMLEventReader;

    move-result-object p1

    return-object p1
.end method

.method public createXMLEventReader(Ljava/io/InputStream;Ljava/lang/String;)Ljavax/xml/stream/XMLEventReader;
    .locals 1

    .line 2
    const/4 v0, 0x1

    invoke-virtual {p0, p1, p2, v0}, Lcom/fasterxml/aalto/stax/InputFactoryImpl;->constructSR(Ljava/io/InputStream;Ljava/lang/String;Z)Lorg/codehaus/stax2/XMLStreamReader2;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/fasterxml/aalto/stax/InputFactoryImpl;->constructER(Lorg/codehaus/stax2/XMLStreamReader2;)Lorg/codehaus/stax2/XMLEventReader2;

    move-result-object p1

    return-object p1
.end method

.method public createXMLEventReader(Ljava/io/Reader;)Ljavax/xml/stream/XMLEventReader;
    .locals 1

    .line 3
    const/4 v0, 0x0

    invoke-virtual {p0, v0, p1}, Lcom/fasterxml/aalto/stax/InputFactoryImpl;->createXMLEventReader(Ljava/lang/String;Ljava/io/Reader;)Ljavax/xml/stream/XMLEventReader;

    move-result-object p1

    return-object p1
.end method

.method public createXMLEventReader(Ljava/lang/String;Ljava/io/InputStream;)Ljavax/xml/stream/XMLEventReader;
    .locals 1

    .line 4
    const/4 v0, 0x1

    invoke-virtual {p0, p1, p2, v0}, Lcom/fasterxml/aalto/stax/InputFactoryImpl;->constructSR(Ljava/lang/String;Ljava/io/InputStream;Z)Lorg/codehaus/stax2/XMLStreamReader2;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/fasterxml/aalto/stax/InputFactoryImpl;->constructER(Lorg/codehaus/stax2/XMLStreamReader2;)Lorg/codehaus/stax2/XMLEventReader2;

    move-result-object p1

    return-object p1
.end method

.method public createXMLEventReader(Ljava/lang/String;Ljava/io/Reader;)Ljavax/xml/stream/XMLEventReader;
    .locals 1

    .line 5
    const/4 v0, 0x1

    invoke-virtual {p0, p1, p2, v0}, Lcom/fasterxml/aalto/stax/InputFactoryImpl;->constructSR(Ljava/lang/String;Ljava/io/Reader;Z)Lorg/codehaus/stax2/XMLStreamReader2;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/fasterxml/aalto/stax/InputFactoryImpl;->constructER(Lorg/codehaus/stax2/XMLStreamReader2;)Lorg/codehaus/stax2/XMLEventReader2;

    move-result-object p1

    return-object p1
.end method

.method public createXMLEventReader(Ljavax/xml/stream/XMLStreamReader;)Ljavax/xml/stream/XMLEventReader;
    .locals 0

    .line 6
    invoke-static {p1}, Lorg/codehaus/stax2/ri/Stax2ReaderAdapter;->wrapIfNecessary(Ljavax/xml/stream/XMLStreamReader;)Lorg/codehaus/stax2/XMLStreamReader2;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/fasterxml/aalto/stax/InputFactoryImpl;->constructER(Lorg/codehaus/stax2/XMLStreamReader2;)Lorg/codehaus/stax2/XMLEventReader2;

    move-result-object p1

    return-object p1
.end method

.method public createXMLEventReader(Ljavax/xml/transform/Source;)Ljavax/xml/stream/XMLEventReader;
    .locals 1

    .line 7
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/fasterxml/aalto/stax/InputFactoryImpl;->constructSR(Ljavax/xml/transform/Source;Z)Lorg/codehaus/stax2/XMLStreamReader2;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/fasterxml/aalto/stax/InputFactoryImpl;->constructER(Lorg/codehaus/stax2/XMLStreamReader2;)Lorg/codehaus/stax2/XMLEventReader2;

    move-result-object p1

    return-object p1
.end method

.method public createXMLEventReader(Ljava/io/File;)Lorg/codehaus/stax2/XMLEventReader2;
    .locals 1

    .line 8
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/fasterxml/aalto/stax/InputFactoryImpl;->constructSR(Ljava/io/File;Z)Lorg/codehaus/stax2/XMLStreamReader2;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/fasterxml/aalto/stax/InputFactoryImpl;->constructER(Lorg/codehaus/stax2/XMLStreamReader2;)Lorg/codehaus/stax2/XMLEventReader2;

    move-result-object p1

    return-object p1
.end method

.method public createXMLEventReader(Ljava/net/URL;)Lorg/codehaus/stax2/XMLEventReader2;
    .locals 1

    .line 9
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/fasterxml/aalto/stax/InputFactoryImpl;->constructSR(Ljava/net/URL;Z)Lorg/codehaus/stax2/XMLStreamReader2;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/fasterxml/aalto/stax/InputFactoryImpl;->constructER(Lorg/codehaus/stax2/XMLStreamReader2;)Lorg/codehaus/stax2/XMLEventReader2;

    move-result-object p1

    return-object p1
.end method

.method public createXMLStreamReader(Ljava/io/InputStream;)Ljavax/xml/stream/XMLStreamReader;
    .locals 2

    .line 1
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v1}, Lcom/fasterxml/aalto/stax/InputFactoryImpl;->constructSR(Ljava/io/InputStream;Ljava/lang/String;Z)Lorg/codehaus/stax2/XMLStreamReader2;

    move-result-object p1

    return-object p1
.end method

.method public createXMLStreamReader(Ljava/io/InputStream;Ljava/lang/String;)Ljavax/xml/stream/XMLStreamReader;
    .locals 1

    .line 2
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/fasterxml/aalto/stax/InputFactoryImpl;->constructSR(Ljava/io/InputStream;Ljava/lang/String;Z)Lorg/codehaus/stax2/XMLStreamReader2;

    move-result-object p1

    return-object p1
.end method

.method public createXMLStreamReader(Ljava/io/Reader;)Ljavax/xml/stream/XMLStreamReader;
    .locals 2

    .line 3
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p1, v1}, Lcom/fasterxml/aalto/stax/InputFactoryImpl;->constructSR(Ljava/lang/String;Ljava/io/Reader;Z)Lorg/codehaus/stax2/XMLStreamReader2;

    move-result-object p1

    return-object p1
.end method

.method public createXMLStreamReader(Ljava/lang/String;Ljava/io/InputStream;)Ljavax/xml/stream/XMLStreamReader;
    .locals 1

    .line 4
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/fasterxml/aalto/stax/InputFactoryImpl;->constructSR(Ljava/lang/String;Ljava/io/InputStream;Z)Lorg/codehaus/stax2/XMLStreamReader2;

    move-result-object p1

    return-object p1
.end method

.method public createXMLStreamReader(Ljava/lang/String;Ljava/io/Reader;)Ljavax/xml/stream/XMLStreamReader;
    .locals 1

    .line 5
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/fasterxml/aalto/stax/InputFactoryImpl;->constructSR(Ljava/lang/String;Ljava/io/Reader;Z)Lorg/codehaus/stax2/XMLStreamReader2;

    move-result-object p1

    return-object p1
.end method

.method public createXMLStreamReader(Ljavax/xml/transform/Source;)Ljavax/xml/stream/XMLStreamReader;
    .locals 1

    .line 6
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/fasterxml/aalto/stax/InputFactoryImpl;->constructSR(Ljavax/xml/transform/Source;Z)Lorg/codehaus/stax2/XMLStreamReader2;

    move-result-object p1

    return-object p1
.end method

.method public createXMLStreamReader(Ljava/io/File;)Lorg/codehaus/stax2/XMLStreamReader2;
    .locals 1

    .line 7
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/fasterxml/aalto/stax/InputFactoryImpl;->constructSR(Ljava/io/File;Z)Lorg/codehaus/stax2/XMLStreamReader2;

    move-result-object p1

    return-object p1
.end method

.method public createXMLStreamReader(Ljava/net/URL;)Lorg/codehaus/stax2/XMLStreamReader2;
    .locals 1

    .line 8
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/fasterxml/aalto/stax/InputFactoryImpl;->constructSR(Ljava/net/URL;Z)Lorg/codehaus/stax2/XMLStreamReader2;

    move-result-object p1

    return-object p1
.end method

.method public getEventAllocator()Ljavax/xml/stream/util/XMLEventAllocator;
    .locals 1

    iget-object v0, p0, Lcom/fasterxml/aalto/stax/InputFactoryImpl;->_allocator:Ljavax/xml/stream/util/XMLEventAllocator;

    return-object v0
.end method

.method public getNonSharedConfig(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)Lcom/fasterxml/aalto/in/ReaderConfig;
    .locals 1

    iget-object v0, p0, Lcom/fasterxml/aalto/stax/InputFactoryImpl;->_config:Lcom/fasterxml/aalto/in/ReaderConfig;

    invoke-virtual {v0, p2, p1, p3}, Lcom/fasterxml/aalto/in/ReaderConfig;->createNonShared(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/fasterxml/aalto/in/ReaderConfig;

    move-result-object p1

    if-eqz p4, :cond_0

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Lcom/fasterxml/aalto/in/ReaderConfig;->doParseLazily(Z)V

    :cond_0
    if-eqz p5, :cond_1

    const/4 p2, 0x1

    invoke-virtual {p1, p2}, Lcom/fasterxml/aalto/in/ReaderConfig;->doAutoCloseInput(Z)V

    :cond_1
    return-object p1
.end method

.method public getProperty(Ljava/lang/String;)Ljava/lang/Object;
    .locals 2

    iget-object v0, p0, Lcom/fasterxml/aalto/stax/InputFactoryImpl;->_config:Lcom/fasterxml/aalto/in/ReaderConfig;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lcom/fasterxml/aalto/in/ReaderConfig;->getProperty(Ljava/lang/String;Z)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public getXMLReporter()Ljavax/xml/stream/XMLReporter;
    .locals 1

    iget-object v0, p0, Lcom/fasterxml/aalto/stax/InputFactoryImpl;->_config:Lcom/fasterxml/aalto/in/ReaderConfig;

    invoke-virtual {v0}, Lcom/fasterxml/aalto/in/ReaderConfig;->getXMLReporter()Ljavax/xml/stream/XMLReporter;

    move-result-object v0

    return-object v0
.end method

.method public getXMLResolver()Ljavax/xml/stream/XMLResolver;
    .locals 1

    iget-object v0, p0, Lcom/fasterxml/aalto/stax/InputFactoryImpl;->_config:Lcom/fasterxml/aalto/in/ReaderConfig;

    invoke-virtual {v0}, Lcom/fasterxml/aalto/in/ReaderConfig;->getXMLResolver()Ljavax/xml/stream/XMLResolver;

    move-result-object v0

    return-object v0
.end method

.method public isPropertySupported(Ljava/lang/String;)Z
    .locals 1

    iget-object v0, p0, Lcom/fasterxml/aalto/stax/InputFactoryImpl;->_config:Lcom/fasterxml/aalto/in/ReaderConfig;

    invoke-virtual {v0, p1}, Lcom/fasterxml/aalto/in/ReaderConfig;->isPropertySupported(Ljava/lang/String;)Z

    move-result p1

    return p1
.end method

.method public setEventAllocator(Ljavax/xml/stream/util/XMLEventAllocator;)V
    .locals 0

    iput-object p1, p0, Lcom/fasterxml/aalto/stax/InputFactoryImpl;->_allocator:Ljavax/xml/stream/util/XMLEventAllocator;

    return-void
.end method

.method public setProperty(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 1

    iget-object v0, p0, Lcom/fasterxml/aalto/stax/InputFactoryImpl;->_config:Lcom/fasterxml/aalto/in/ReaderConfig;

    invoke-virtual {v0, p1, p2}, Lcom/fasterxml/aalto/in/ReaderConfig;->setProperty(Ljava/lang/String;Ljava/lang/Object;)Z

    return-void
.end method

.method public setXMLReporter(Ljavax/xml/stream/XMLReporter;)V
    .locals 1

    iget-object v0, p0, Lcom/fasterxml/aalto/stax/InputFactoryImpl;->_config:Lcom/fasterxml/aalto/in/ReaderConfig;

    invoke-virtual {v0, p1}, Lcom/fasterxml/aalto/in/ReaderConfig;->setXMLReporter(Ljavax/xml/stream/XMLReporter;)V

    return-void
.end method

.method public setXMLResolver(Ljavax/xml/stream/XMLResolver;)V
    .locals 1

    iget-object v0, p0, Lcom/fasterxml/aalto/stax/InputFactoryImpl;->_config:Lcom/fasterxml/aalto/in/ReaderConfig;

    invoke-virtual {v0, p1}, Lcom/fasterxml/aalto/in/ReaderConfig;->setXMLResolver(Ljavax/xml/stream/XMLResolver;)V

    return-void
.end method

.class public Lcom/fasterxml/aalto/async/AsyncStreamReaderImpl;
.super Lcom/fasterxml/aalto/stax/StreamReaderImpl;
.source "SourceFile"

# interfaces
.implements Lcom/fasterxml/aalto/AsyncXMLStreamReader;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<F::",
        "Lcom/fasterxml/aalto/AsyncInputFeeder;",
        ">",
        "Lcom/fasterxml/aalto/stax/StreamReaderImpl;",
        "Lcom/fasterxml/aalto/AsyncXMLStreamReader<",
        "TF;>;"
    }
.end annotation


# instance fields
.field protected final _asyncScanner:Lcom/fasterxml/aalto/async/AsyncByteScanner;


# direct methods
.method public constructor <init>(Lcom/fasterxml/aalto/async/AsyncByteScanner;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;-><init>(Lcom/fasterxml/aalto/in/XmlScanner;)V

    iput-object p1, p0, Lcom/fasterxml/aalto/async/AsyncStreamReaderImpl;->_asyncScanner:Lcom/fasterxml/aalto/async/AsyncByteScanner;

    const/16 p1, 0x101

    iput p1, p0, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_currToken:I

    return-void
.end method


# virtual methods
.method public _reportNonTextEvent(I)V
    .locals 1

    const/16 v0, 0x101

    if-ne p1, v0, :cond_0

    const-string v0, "Can not use text-aggregating methods with non-blocking parser, as they (may) require blocking"

    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->throwWfe(Ljava/lang/String;)V

    :cond_0
    invoke-super {p0, p1}, Lcom/fasterxml/aalto/stax/StreamReaderImpl;->_reportNonTextEvent(I)V

    return-void
.end method

.method public getInputFeeder()Lcom/fasterxml/aalto/AsyncInputFeeder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TF;"
        }
    .end annotation

    iget-object v0, p0, Lcom/fasterxml/aalto/async/AsyncStreamReaderImpl;->_asyncScanner:Lcom/fasterxml/aalto/async/AsyncByteScanner;

    return-object v0
.end method

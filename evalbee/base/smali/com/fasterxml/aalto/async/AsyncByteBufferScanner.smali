.class public Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;
.super Lcom/fasterxml/aalto/async/AsyncByteScanner;
.source "SourceFile"

# interfaces
.implements Lcom/fasterxml/aalto/AsyncByteBufferFeeder;


# instance fields
.field protected _inputBuffer:Ljava/nio/ByteBuffer;

.field protected _origBufferLen:I


# direct methods
.method public constructor <init>(Lcom/fasterxml/aalto/in/ReaderConfig;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/fasterxml/aalto/async/AsyncByteScanner;-><init>(Lcom/fasterxml/aalto/in/ReaderConfig;)V

    const/4 p1, 0x1

    iput p1, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_state:I

    const/16 p1, 0x101

    iput p1, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_currToken:I

    return-void
.end method

.method private finishStartElement(Z)I
    .locals 4

    iput-boolean p1, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_isEmptyTag:Z

    iget-object p1, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_attrCollector:Lcom/fasterxml/aalto/in/AttributeCollector;

    iget v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_elemAttrPtr:I

    invoke-virtual {p1, v0}, Lcom/fasterxml/aalto/in/AttributeCollector;->finishLastValue(I)I

    move-result p1

    if-gez p1, :cond_0

    iget-object p1, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_attrCollector:Lcom/fasterxml/aalto/in/AttributeCollector;

    invoke-virtual {p1}, Lcom/fasterxml/aalto/in/AttributeCollector;->getCount()I

    move-result p1

    iget-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_attrCollector:Lcom/fasterxml/aalto/in/AttributeCollector;

    invoke-virtual {v0}, Lcom/fasterxml/aalto/in/AttributeCollector;->getErrorMsg()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/in/XmlScanner;->reportInputProblem(Ljava/lang/String;)V

    :cond_0
    iput p1, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_attrCount:I

    iget p1, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_depth:I

    const/4 v0, 0x1

    add-int/2addr p1, v0

    iput p1, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_depth:I

    iget-boolean p1, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_elemAllNsBound:Z

    if-nez p1, :cond_3

    iget-object p1, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_tokenName:Lcom/fasterxml/aalto/in/PName;

    invoke-virtual {p1}, Lcom/fasterxml/aalto/in/PName;->isBound()Z

    move-result p1

    const/4 v1, 0x0

    if-nez p1, :cond_1

    iget-object p1, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_tokenName:Lcom/fasterxml/aalto/in/PName;

    invoke-virtual {p0, p1, v1}, Lcom/fasterxml/aalto/in/XmlScanner;->reportUnboundPrefix(Lcom/fasterxml/aalto/in/PName;Z)V

    :cond_1
    iget p1, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_attrCount:I

    :goto_0
    if-ge v1, p1, :cond_3

    iget-object v2, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_attrCollector:Lcom/fasterxml/aalto/in/AttributeCollector;

    invoke-virtual {v2, v1}, Lcom/fasterxml/aalto/in/AttributeCollector;->getName(I)Lcom/fasterxml/aalto/in/PName;

    move-result-object v2

    invoke-virtual {v2}, Lcom/fasterxml/aalto/in/PName;->isBound()Z

    move-result v3

    if-nez v3, :cond_2

    invoke-virtual {p0, v2, v0}, Lcom/fasterxml/aalto/in/XmlScanner;->reportUnboundPrefix(Lcom/fasterxml/aalto/in/PName;Z)V

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_3
    iput v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_currToken:I

    return v0
.end method

.method private final handleAndAppendPending()Z
    .locals 8

    iget v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v1, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    const/4 v2, 0x0

    if-lt v0, v1, :cond_0

    return v2

    :cond_0
    iget v1, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_pendingInput:I

    iput v2, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_pendingInput:I

    const/4 v3, 0x1

    if-gez v1, :cond_3

    const/4 v4, -0x1

    if-ne v1, v4, :cond_2

    iget-object v1, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    const/16 v1, 0xa

    if-ne v0, v1, :cond_1

    iget v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    add-int/2addr v0, v3

    iput v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    :cond_1
    invoke-virtual {p0}, Lcom/fasterxml/aalto/in/ByteBasedScanner;->markLF()V

    iget-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_textBuilder:Lcom/fasterxml/aalto/util/TextBuilder;

    invoke-virtual {v0, v1}, Lcom/fasterxml/aalto/util/TextBuilder;->append(C)V

    return v3

    :cond_2
    invoke-virtual {p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->throwInternal()I

    :cond_3
    iget-object v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_charTypes:Lcom/fasterxml/aalto/util/XmlCharTypes;

    iget-object v0, v0, Lcom/fasterxml/aalto/util/XmlCharTypes;->TEXT_CHARS:[I

    and-int/lit16 v4, v1, 0xff

    aget v0, v0, v4

    const/4 v5, 0x5

    if-eq v0, v5, :cond_d

    const/4 v5, 0x6

    if-eq v0, v5, :cond_a

    const/4 v5, 0x7

    if-eq v0, v5, :cond_4

    invoke-virtual {p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->throwInternal()I

    goto/16 :goto_5

    :cond_4
    iget-object v0, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    iget v5, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    add-int/lit8 v6, v5, 0x1

    iput v6, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {v0, v5}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    and-int/lit16 v0, v0, 0xff

    shr-int/lit8 v5, v1, 0x8

    if-nez v5, :cond_7

    iget v4, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v5, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-lt v4, v5, :cond_5

    :goto_0
    shl-int/lit8 v0, v0, 0x8

    :goto_1
    or-int/2addr v0, v1

    :goto_2
    iput v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_pendingInput:I

    return v2

    :cond_5
    iget-object v5, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    add-int/lit8 v6, v4, 0x1

    iput v6, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {v5, v4}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v4

    and-int/lit16 v4, v4, 0xff

    iget v5, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v6, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-lt v5, v6, :cond_6

    shl-int/lit8 v0, v0, 0x8

    or-int/2addr v0, v1

    shl-int/lit8 v1, v4, 0x10

    or-int/2addr v0, v1

    goto :goto_2

    :cond_6
    iget-object v2, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    add-int/lit8 v6, v5, 0x1

    iput v6, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {v2, v5}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v2

    and-int/lit16 v2, v2, 0xff

    invoke-virtual {p0, v1, v0, v4, v2}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->decodeUtf8_4(IIII)I

    move-result v0

    goto :goto_3

    :cond_7
    and-int/lit16 v5, v5, 0xff

    shr-int/lit8 v6, v1, 0x10

    if-nez v6, :cond_9

    iget v6, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v7, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-lt v6, v7, :cond_8

    shl-int/lit8 v0, v0, 0x10

    goto :goto_1

    :cond_8
    iget-object v1, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    add-int/lit8 v2, v6, 0x1

    iput v2, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {v1, v6}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v1

    and-int/lit16 v1, v1, 0xff

    invoke-virtual {p0, v4, v5, v0, v1}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->decodeUtf8_4(IIII)I

    move-result v0

    goto :goto_3

    :cond_9
    invoke-virtual {p0, v4, v5, v6, v0}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->decodeUtf8_4(IIII)I

    move-result v0

    :goto_3
    iget-object v1, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_textBuilder:Lcom/fasterxml/aalto/util/TextBuilder;

    invoke-virtual {v1, v0}, Lcom/fasterxml/aalto/util/TextBuilder;->appendSurrogate(I)V

    goto :goto_5

    :cond_a
    iget-object v0, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    iget v5, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    add-int/lit8 v6, v5, 0x1

    iput v6, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {v0, v5}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    and-int/lit16 v0, v0, 0xff

    shr-int/lit8 v5, v1, 0x8

    if-nez v5, :cond_c

    iget v4, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v5, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-lt v4, v5, :cond_b

    goto :goto_0

    :cond_b
    iget-object v2, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {v2, v4}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v2

    and-int/lit16 v2, v2, 0xff

    invoke-virtual {p0, v1, v0, v2}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->decodeUtf8_3(III)I

    move-result v0

    goto :goto_4

    :cond_c
    invoke-virtual {p0, v4, v5, v0}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->decodeUtf8_3(III)I

    move-result v0

    :goto_4
    iget-object v1, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_textBuilder:Lcom/fasterxml/aalto/util/TextBuilder;

    int-to-char v0, v0

    invoke-virtual {v1, v0}, Lcom/fasterxml/aalto/util/TextBuilder;->append(C)V

    goto :goto_5

    :cond_d
    iget-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_textBuilder:Lcom/fasterxml/aalto/util/TextBuilder;

    invoke-virtual {p0, v1}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->decodeUtf8_2(I)I

    move-result v1

    int-to-char v1, v1

    invoke-virtual {v0, v1}, Lcom/fasterxml/aalto/util/TextBuilder;->append(C)V

    :goto_5
    return v3
.end method

.method private final handleAttrValuePending()Z
    .locals 9

    iget v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_pendingInput:I

    const/4 v1, -0x1

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-ne v0, v1, :cond_2

    invoke-virtual {p0}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->handlePartialCR()Z

    move-result v0

    if-nez v0, :cond_0

    return v2

    :cond_0
    iget-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_attrCollector:Lcom/fasterxml/aalto/in/AttributeCollector;

    invoke-virtual {v0}, Lcom/fasterxml/aalto/in/AttributeCollector;->continueValue()[C

    move-result-object v0

    iget v1, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_elemAttrPtr:I

    array-length v2, v0

    if-lt v1, v2, :cond_1

    iget-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_attrCollector:Lcom/fasterxml/aalto/in/AttributeCollector;

    invoke-virtual {v0}, Lcom/fasterxml/aalto/in/AttributeCollector;->valueBufferFull()[C

    move-result-object v0

    :cond_1
    iget v1, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_elemAttrPtr:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_elemAttrPtr:I

    const/16 v2, 0x20

    aput-char v2, v0, v1

    return v3

    :cond_2
    iget v1, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v4, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-lt v1, v4, :cond_3

    return v2

    :cond_3
    const/16 v4, -0x3c

    const/16 v5, -0x3f

    const/16 v6, 0x78

    const/16 v7, -0x3d

    const/16 v8, -0x3e

    if-ne v0, v4, :cond_7

    iget-object v0, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    add-int/lit8 v4, v1, 0x1

    iput v4, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    const/16 v1, 0x23

    if-ne v0, v1, :cond_5

    iput v7, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_pendingInput:I

    iget v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v1, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-lt v0, v1, :cond_4

    return v2

    :cond_4
    iget-object v1, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    if-ne v0, v6, :cond_8

    iput v8, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_pendingInput:I

    iget v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    add-int/2addr v0, v3

    iput v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v1, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-lt v0, v1, :cond_a

    return v2

    :cond_5
    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->parseNewEntityName(B)Lcom/fasterxml/aalto/in/PName;

    move-result-object v0

    if-nez v0, :cond_6

    iput v5, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_pendingInput:I

    return v2

    :cond_6
    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->decodeGeneralEntity(Lcom/fasterxml/aalto/in/PName;)I

    move-result v1

    if-nez v1, :cond_d

    goto :goto_0

    :cond_7
    if-ne v0, v7, :cond_9

    iget-object v0, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    if-ne v0, v6, :cond_8

    iput v8, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_pendingInput:I

    iget v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    add-int/2addr v0, v3

    iput v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v1, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-lt v0, v1, :cond_a

    return v2

    :cond_8
    invoke-direct {p0, v3}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->handleDecEntityInAttribute(Z)I

    move-result v0

    goto :goto_1

    :cond_9
    if-ne v0, v8, :cond_b

    :cond_a
    invoke-direct {p0, v3}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->handleHexEntityInAttribute(Z)I

    move-result v0

    goto :goto_1

    :cond_b
    if-ne v0, v5, :cond_e

    invoke-virtual {p0}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->parseEntityName()Lcom/fasterxml/aalto/in/PName;

    move-result-object v0

    if-nez v0, :cond_c

    return v2

    :cond_c
    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->decodeGeneralEntity(Lcom/fasterxml/aalto/in/PName;)I

    move-result v1

    if-nez v1, :cond_d

    :goto_0
    iput-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_tokenName:Lcom/fasterxml/aalto/in/PName;

    iget-object v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_elemAttrName:Lcom/fasterxml/aalto/in/PName;

    invoke-virtual {p0, v0, v2}, Lcom/fasterxml/aalto/in/XmlScanner;->reportUnexpandedEntityInAttr(Lcom/fasterxml/aalto/in/PName;Z)V

    :cond_d
    move v0, v1

    goto :goto_1

    :cond_e
    const/16 v1, -0x40

    if-ne v0, v1, :cond_f

    invoke-direct {p0, v2}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->handleDecEntityInAttribute(Z)I

    move-result v0

    goto :goto_1

    :cond_f
    const/16 v1, -0x41

    if-ne v0, v1, :cond_10

    invoke-direct {p0, v2}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->handleHexEntityInAttribute(Z)I

    move-result v0

    goto :goto_1

    :cond_10
    invoke-direct {p0}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->handleAttrValuePendingUTF8()I

    move-result v0

    :goto_1
    if-nez v0, :cond_11

    return v2

    :cond_11
    iget-object v1, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_attrCollector:Lcom/fasterxml/aalto/in/AttributeCollector;

    invoke-virtual {v1}, Lcom/fasterxml/aalto/in/AttributeCollector;->continueValue()[C

    move-result-object v1

    shr-int/lit8 v2, v0, 0x10

    if-eqz v2, :cond_13

    const/high16 v2, 0x10000

    sub-int/2addr v0, v2

    iget v2, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_elemAttrPtr:I

    array-length v4, v1

    if-lt v2, v4, :cond_12

    iget-object v1, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_attrCollector:Lcom/fasterxml/aalto/in/AttributeCollector;

    invoke-virtual {v1}, Lcom/fasterxml/aalto/in/AttributeCollector;->valueBufferFull()[C

    move-result-object v1

    :cond_12
    iget v2, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_elemAttrPtr:I

    add-int/lit8 v4, v2, 0x1

    iput v4, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_elemAttrPtr:I

    shr-int/lit8 v4, v0, 0xa

    const v5, 0xd800

    or-int/2addr v4, v5

    int-to-char v4, v4

    aput-char v4, v1, v2

    and-int/lit16 v0, v0, 0x3ff

    const v2, 0xdc00

    or-int/2addr v0, v2

    :cond_13
    iget v2, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_elemAttrPtr:I

    array-length v4, v1

    if-lt v2, v4, :cond_14

    iget-object v1, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_attrCollector:Lcom/fasterxml/aalto/in/AttributeCollector;

    invoke-virtual {v1}, Lcom/fasterxml/aalto/in/AttributeCollector;->valueBufferFull()[C

    move-result-object v1

    :cond_14
    iget v2, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_elemAttrPtr:I

    add-int/lit8 v4, v2, 0x1

    iput v4, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_elemAttrPtr:I

    int-to-char v0, v0

    aput-char v0, v1, v2

    return v3
.end method

.method private final handleAttrValuePendingUTF8()I
    .locals 7

    iget v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_pendingInput:I

    const/4 v1, 0x0

    iput v1, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_pendingInput:I

    iget-object v2, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_charTypes:Lcom/fasterxml/aalto/util/XmlCharTypes;

    iget-object v2, v2, Lcom/fasterxml/aalto/util/XmlCharTypes;->TEXT_CHARS:[I

    and-int/lit16 v3, v0, 0xff

    aget v2, v2, v3

    const/4 v4, 0x5

    if-eq v2, v4, :cond_9

    const/4 v4, 0x6

    if-eq v2, v4, :cond_6

    const/4 v4, 0x7

    if-eq v2, v4, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->throwInternal()I

    return v1

    :cond_0
    iget-object v2, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    iget v4, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {v2, v4}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v2

    and-int/lit16 v2, v2, 0xff

    shr-int/lit8 v4, v0, 0x8

    if-nez v4, :cond_3

    iget v3, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v4, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-lt v3, v4, :cond_1

    :goto_0
    shl-int/lit8 v2, v2, 0x8

    :goto_1
    or-int/2addr v0, v2

    iput v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_pendingInput:I

    return v1

    :cond_1
    iget-object v4, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    add-int/lit8 v5, v3, 0x1

    iput v5, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {v4, v3}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v3

    and-int/lit16 v3, v3, 0xff

    iget v4, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v5, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-lt v4, v5, :cond_2

    shl-int/lit8 v2, v2, 0x8

    or-int/2addr v0, v2

    shl-int/lit8 v2, v3, 0x10

    goto :goto_1

    :cond_2
    iget-object v1, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {v1, v4}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v1

    and-int/lit16 v1, v1, 0xff

    invoke-virtual {p0, v0, v2, v3, v1}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->decodeUtf8_4(IIII)I

    move-result v0

    goto :goto_2

    :cond_3
    and-int/lit16 v4, v4, 0xff

    shr-int/lit8 v5, v0, 0x10

    if-nez v5, :cond_5

    iget v5, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v6, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-lt v5, v6, :cond_4

    shl-int/lit8 v2, v2, 0x10

    goto :goto_1

    :cond_4
    iget-object v0, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    add-int/lit8 v1, v5, 0x1

    iput v1, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {v0, v5}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    and-int/lit16 v0, v0, 0xff

    invoke-virtual {p0, v3, v4, v2, v0}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->decodeUtf8_4(IIII)I

    move-result v0

    goto :goto_2

    :cond_5
    invoke-virtual {p0, v3, v4, v5, v2}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->decodeUtf8_4(IIII)I

    move-result v0

    :goto_2
    return v0

    :cond_6
    iget-object v2, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    iget v4, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {v2, v4}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v2

    and-int/lit16 v2, v2, 0xff

    shr-int/lit8 v4, v0, 0x8

    if-nez v4, :cond_8

    iget v3, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v4, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-lt v3, v4, :cond_7

    goto :goto_0

    :cond_7
    iget-object v1, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {v1, v3}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v1

    and-int/lit16 v1, v1, 0xff

    invoke-virtual {p0, v0, v2, v1}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->decodeUtf8_3(III)I

    move-result v0

    goto :goto_3

    :cond_8
    invoke-virtual {p0, v3, v4, v2}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->decodeUtf8_3(III)I

    move-result v0

    :goto_3
    return v0

    :cond_9
    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->decodeUtf8_2(I)I

    move-result v0

    return v0
.end method

.method private handleCData()I
    .locals 3

    iget v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_state:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->parseCDataContents()I

    move-result v0

    return v0

    :cond_0
    iget v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v1, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-lt v0, v1, :cond_1

    const/16 v0, 0x101

    return v0

    :cond_1
    iget-object v1, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    add-int/lit8 v2, v0, 0x1

    iput v2, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    invoke-direct {p0, v0}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->handleCDataStartMarker(B)I

    move-result v0

    return v0
.end method

.method private handleCDataStartMarker(B)I
    .locals 9

    iget v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_state:I

    const-string v1, " (expected \'A\' for CDATA)"

    const/16 v2, 0x41

    const/4 v3, 0x6

    const/4 v4, 0x5

    const/4 v5, 0x4

    const/4 v6, 0x3

    const/4 v7, 0x2

    const/16 v8, 0x101

    if-eqz v0, :cond_0

    if-eq v0, v7, :cond_3

    if-eq v0, v6, :cond_6

    if-eq v0, v5, :cond_9

    if-eq v0, v4, :cond_c

    if-eq v0, v3, :cond_f

    invoke-virtual {p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->throwInternal()I

    move-result p1

    return p1

    :cond_0
    const/16 v0, 0x43

    if-eq p1, v0, :cond_1

    invoke-virtual {p0, p1}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->decodeCharForError(B)I

    move-result p1

    const-string v0, " (expected \'C\' for CDATA)"

    invoke-virtual {p0, p1, v0}, Lcom/fasterxml/aalto/in/XmlScanner;->reportTreeUnexpChar(ILjava/lang/String;)V

    :cond_1
    iput v7, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_state:I

    iget p1, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-lt p1, v0, :cond_2

    return v8

    :cond_2
    iget-object v0, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    add-int/lit8 v7, p1, 0x1

    iput v7, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {v0, p1}, Ljava/nio/ByteBuffer;->get(I)B

    move-result p1

    :cond_3
    const/16 v0, 0x44

    if-eq p1, v0, :cond_4

    invoke-virtual {p0, p1}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->decodeCharForError(B)I

    move-result p1

    const-string v0, " (expected \'D\' for CDATA)"

    invoke-virtual {p0, p1, v0}, Lcom/fasterxml/aalto/in/XmlScanner;->reportTreeUnexpChar(ILjava/lang/String;)V

    :cond_4
    iput v6, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_state:I

    iget p1, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-lt p1, v0, :cond_5

    return v8

    :cond_5
    iget-object v0, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    add-int/lit8 v6, p1, 0x1

    iput v6, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {v0, p1}, Ljava/nio/ByteBuffer;->get(I)B

    move-result p1

    :cond_6
    if-eq p1, v2, :cond_7

    invoke-virtual {p0, p1}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->decodeCharForError(B)I

    move-result p1

    invoke-virtual {p0, p1, v1}, Lcom/fasterxml/aalto/in/XmlScanner;->reportTreeUnexpChar(ILjava/lang/String;)V

    :cond_7
    iput v5, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_state:I

    iget p1, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-lt p1, v0, :cond_8

    return v8

    :cond_8
    iget-object v0, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    add-int/lit8 v5, p1, 0x1

    iput v5, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {v0, p1}, Ljava/nio/ByteBuffer;->get(I)B

    move-result p1

    :cond_9
    const/16 v0, 0x54

    if-eq p1, v0, :cond_a

    invoke-virtual {p0, p1}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->decodeCharForError(B)I

    move-result p1

    const-string v0, " (expected \'T\' for CDATA)"

    invoke-virtual {p0, p1, v0}, Lcom/fasterxml/aalto/in/XmlScanner;->reportTreeUnexpChar(ILjava/lang/String;)V

    :cond_a
    iput v4, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_state:I

    iget p1, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-lt p1, v0, :cond_b

    return v8

    :cond_b
    iget-object v0, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    add-int/lit8 v4, p1, 0x1

    iput v4, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {v0, p1}, Ljava/nio/ByteBuffer;->get(I)B

    move-result p1

    :cond_c
    if-eq p1, v2, :cond_d

    invoke-virtual {p0, p1}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->decodeCharForError(B)I

    move-result p1

    invoke-virtual {p0, p1, v1}, Lcom/fasterxml/aalto/in/XmlScanner;->reportTreeUnexpChar(ILjava/lang/String;)V

    :cond_d
    iput v3, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_state:I

    iget p1, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-lt p1, v0, :cond_e

    return v8

    :cond_e
    iget-object v0, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    add-int/lit8 v1, p1, 0x1

    iput v1, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {v0, p1}, Ljava/nio/ByteBuffer;->get(I)B

    move-result p1

    :cond_f
    const/16 v0, 0x5b

    if-eq p1, v0, :cond_10

    invoke-virtual {p0, p1}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->decodeCharForError(B)I

    move-result p1

    const-string v0, " (expected \'[\' for CDATA)"

    invoke-virtual {p0, p1, v0}, Lcom/fasterxml/aalto/in/XmlScanner;->reportTreeUnexpChar(ILjava/lang/String;)V

    :cond_10
    iget-object p1, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_textBuilder:Lcom/fasterxml/aalto/util/TextBuilder;

    invoke-virtual {p1}, Lcom/fasterxml/aalto/util/TextBuilder;->resetWithEmpty()[C

    const/4 p1, 0x1

    iput p1, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_state:I

    iget p1, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-lt p1, v0, :cond_11

    return v8

    :cond_11
    invoke-virtual {p0}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->parseCDataContents()I

    move-result p1

    return p1
.end method

.method private final handleDecEntityInAttribute(Z)I
    .locals 4

    iget-object v0, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    iget v1, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    const-string v1, " expected a digit (0 - 9) for character entity"

    const/4 v2, 0x0

    if-eqz p1, :cond_3

    const/16 p1, 0x30

    if-lt v0, p1, :cond_0

    const/16 p1, 0x39

    if-le v0, p1, :cond_1

    :cond_0
    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->decodeCharForError(B)I

    move-result p1

    invoke-virtual {p0, p1, v1}, Lcom/fasterxml/aalto/in/XmlScanner;->throwUnexpectedChar(ILjava/lang/String;)V

    :cond_1
    const/16 p1, -0x40

    iput p1, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_pendingInput:I

    add-int/lit8 v0, v0, -0x30

    iput v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_entityValue:I

    iget p1, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-lt p1, v0, :cond_2

    return v2

    :cond_2
    iget-object v0, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    add-int/lit8 v3, p1, 0x1

    iput v3, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {v0, p1}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    :cond_3
    const/16 p1, 0x3b

    if-eq v0, p1, :cond_7

    add-int/lit8 p1, v0, -0x30

    if-ltz p1, :cond_4

    const/16 v3, 0x9

    if-le p1, v3, :cond_5

    :cond_4
    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->decodeCharForError(B)I

    move-result v0

    invoke-virtual {p0, v0, v1}, Lcom/fasterxml/aalto/in/XmlScanner;->throwUnexpectedChar(ILjava/lang/String;)V

    :cond_5
    iget v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_entityValue:I

    mul-int/lit8 v0, v0, 0xa

    add-int/2addr v0, p1

    iput v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_entityValue:I

    const p1, 0x10ffff

    if-le v0, p1, :cond_6

    invoke-virtual {p0}, Lcom/fasterxml/aalto/in/XmlScanner;->reportEntityOverflow()V

    :cond_6
    iget p1, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-lt p1, v0, :cond_2

    return v2

    :cond_7
    iget p1, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_entityValue:I

    invoke-virtual {p0, p1}, Lcom/fasterxml/aalto/in/XmlScanner;->verifyXmlChar(I)V

    iput v2, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_pendingInput:I

    iget p1, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_entityValue:I

    return p1
.end method

.method private handleEndElement()I
    .locals 8

    iget v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_state:I

    const/16 v1, 0x101

    const/4 v2, 0x1

    if-nez v0, :cond_7

    iget-object v3, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_tokenName:Lcom/fasterxml/aalto/in/PName;

    invoke-virtual {v3}, Lcom/fasterxml/aalto/in/PName;->sizeInQuads()I

    move-result v0

    sub-int/2addr v0, v2

    :goto_0
    iget v4, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_quadCount:I

    if-ge v4, v0, :cond_3

    :goto_1
    iget v4, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_currQuadBytes:I

    const/4 v5, 0x4

    if-ge v4, v5, :cond_1

    iget v4, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v5, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-lt v4, v5, :cond_0

    return v1

    :cond_0
    iget v5, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_currQuad:I

    shl-int/lit8 v5, v5, 0x8

    iget-object v6, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    add-int/lit8 v7, v4, 0x1

    iput v7, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {v6, v4}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v4

    and-int/lit16 v4, v4, 0xff

    or-int/2addr v4, v5

    iput v4, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_currQuad:I

    iget v4, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_currQuadBytes:I

    add-int/2addr v4, v2

    iput v4, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_currQuadBytes:I

    goto :goto_1

    :cond_1
    iget v4, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_currQuad:I

    iget v5, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_quadCount:I

    invoke-virtual {v3, v5}, Lcom/fasterxml/aalto/in/PName;->getQuad(I)I

    move-result v5

    if-eq v4, v5, :cond_2

    invoke-virtual {v3}, Lcom/fasterxml/aalto/in/PName;->getPrefixedName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/fasterxml/aalto/in/XmlScanner;->reportUnexpectedEndTag(Ljava/lang/String;)V

    :cond_2
    const/4 v4, 0x0

    iput v4, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_currQuadBytes:I

    iput v4, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_currQuad:I

    iget v4, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_quadCount:I

    add-int/2addr v4, v2

    iput v4, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_quadCount:I

    goto :goto_0

    :cond_3
    invoke-virtual {v3}, Lcom/fasterxml/aalto/in/PName;->getLastQuad()I

    move-result v4

    :cond_4
    iget v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v5, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-lt v0, v5, :cond_5

    return v1

    :cond_5
    iget v5, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_currQuad:I

    shl-int/lit8 v5, v5, 0x8

    iget-object v6, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    add-int/lit8 v7, v0, 0x1

    iput v7, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {v6, v0}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    and-int/lit16 v0, v0, 0xff

    or-int/2addr v0, v5

    iput v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_currQuad:I

    if-ne v0, v4, :cond_6

    goto :goto_2

    :cond_6
    iget v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_currQuadBytes:I

    add-int/2addr v0, v2

    iput v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_currQuadBytes:I

    const/4 v5, 0x3

    if-le v0, v5, :cond_4

    invoke-virtual {v3}, Lcom/fasterxml/aalto/in/PName;->getPrefixedName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/in/XmlScanner;->reportUnexpectedEndTag(Ljava/lang/String;)V

    :goto_2
    iput v2, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_state:I

    goto :goto_3

    :cond_7
    if-eq v0, v2, :cond_8

    invoke-virtual {p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->throwInternal()I

    :cond_8
    :goto_3
    iget v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_pendingInput:I

    if-eqz v0, :cond_9

    invoke-virtual {p0}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->handlePartialCR()Z

    move-result v0

    if-nez v0, :cond_9

    return v1

    :cond_9
    :goto_4
    iget v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v3, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-lt v0, v3, :cond_a

    return v1

    :cond_a
    iget-object v3, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    add-int/lit8 v4, v0, 0x1

    iput v4, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {v3, v0}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    and-int/lit16 v0, v0, 0xff

    const/16 v3, 0x20

    if-gt v0, v3, :cond_f

    const/16 v4, 0xa

    if-ne v0, v4, :cond_c

    :cond_b
    :goto_5
    invoke-virtual {p0}, Lcom/fasterxml/aalto/in/ByteBasedScanner;->markLF()V

    goto :goto_4

    :cond_c
    const/16 v5, 0xd

    if-ne v0, v5, :cond_e

    iget v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v3, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-lt v0, v3, :cond_d

    const/4 v0, -0x1

    iput v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_pendingInput:I

    return v1

    :cond_d
    iget-object v3, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v3, v0}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    if-ne v0, v4, :cond_b

    iget v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    add-int/2addr v0, v2

    iput v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    goto :goto_5

    :cond_e
    if-eq v0, v3, :cond_9

    const/16 v3, 0x9

    if-eq v0, v3, :cond_9

    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/in/XmlScanner;->throwInvalidSpace(I)V

    goto :goto_4

    :cond_f
    const/16 v1, 0x3e

    if-eq v0, v1, :cond_10

    int-to-byte v0, v0

    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->decodeCharForError(B)I

    move-result v0

    const-string v1, " expected space or closing \'>\'"

    invoke-virtual {p0, v0, v1}, Lcom/fasterxml/aalto/in/XmlScanner;->throwUnexpectedChar(ILjava/lang/String;)V

    :cond_10
    const/4 v0, 0x2

    iput v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_currToken:I

    return v0
.end method

.method private handleEndElementStart()I
    .locals 9

    iget v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_depth:I

    const/4 v1, 0x1

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_depth:I

    iget-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_currElem:Lcom/fasterxml/aalto/in/ElementScope;

    invoke-virtual {v0}, Lcom/fasterxml/aalto/in/ElementScope;->getName()Lcom/fasterxml/aalto/in/PName;

    move-result-object v0

    iput-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_tokenName:Lcom/fasterxml/aalto/in/PName;

    invoke-virtual {v0}, Lcom/fasterxml/aalto/in/PName;->sizeInQuads()I

    move-result v0

    iget v2, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    iget v3, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    sub-int/2addr v2, v3

    shl-int/lit8 v3, v0, 0x2

    add-int/2addr v3, v1

    const/4 v4, 0x0

    const/4 v5, 0x2

    if-ge v2, v3, :cond_0

    iput v5, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_nextEvent:I

    iput v4, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_state:I

    iput v4, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_currQuadBytes:I

    iput v4, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_currQuad:I

    iput v4, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_quadCount:I

    invoke-direct {p0}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->handleEndElement()I

    move-result v0

    return v0

    :cond_0
    iget-object v2, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    const/4 v3, -0x1

    add-int/2addr v0, v3

    :goto_0
    if-ge v4, v0, :cond_2

    iget v6, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {v2, v6}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v7

    shl-int/lit8 v7, v7, 0x18

    add-int/lit8 v8, v6, 0x1

    invoke-virtual {v2, v8}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v8

    and-int/lit16 v8, v8, 0xff

    shl-int/lit8 v8, v8, 0x10

    or-int/2addr v7, v8

    add-int/lit8 v8, v6, 0x2

    invoke-virtual {v2, v8}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v8

    and-int/lit16 v8, v8, 0xff

    shl-int/lit8 v8, v8, 0x8

    or-int/2addr v7, v8

    add-int/lit8 v6, v6, 0x3

    invoke-virtual {v2, v6}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v6

    and-int/lit16 v6, v6, 0xff

    or-int/2addr v6, v7

    iget v7, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    add-int/lit8 v7, v7, 0x4

    iput v7, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget-object v7, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_tokenName:Lcom/fasterxml/aalto/in/PName;

    invoke-virtual {v7, v4}, Lcom/fasterxml/aalto/in/PName;->getQuad(I)I

    move-result v7

    if-eq v6, v7, :cond_1

    iget-object v6, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_tokenName:Lcom/fasterxml/aalto/in/PName;

    invoke-virtual {v6}, Lcom/fasterxml/aalto/in/PName;->getPrefixedName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/fasterxml/aalto/in/XmlScanner;->reportUnexpectedEndTag(Ljava/lang/String;)V

    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_2
    iget-object v4, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_tokenName:Lcom/fasterxml/aalto/in/PName;

    invoke-virtual {v4, v0}, Lcom/fasterxml/aalto/in/PName;->getQuad(I)I

    move-result v0

    iget v4, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    add-int/lit8 v6, v4, 0x1

    iput v6, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {v2, v4}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v4

    and-int/lit16 v4, v4, 0xff

    if-eq v4, v0, :cond_3

    shl-int/lit8 v4, v4, 0x8

    iget v6, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    add-int/lit8 v7, v6, 0x1

    iput v7, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {v2, v6}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v6

    and-int/lit16 v6, v6, 0xff

    or-int/2addr v4, v6

    if-eq v4, v0, :cond_3

    shl-int/lit8 v4, v4, 0x8

    iget v6, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    add-int/lit8 v7, v6, 0x1

    iput v7, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {v2, v6}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v6

    and-int/lit16 v6, v6, 0xff

    or-int/2addr v4, v6

    if-eq v4, v0, :cond_3

    shl-int/lit8 v4, v4, 0x8

    iget v6, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    add-int/lit8 v7, v6, 0x1

    iput v7, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {v2, v6}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v2

    and-int/lit16 v2, v2, 0xff

    or-int/2addr v2, v4

    if-eq v2, v0, :cond_3

    iget-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_tokenName:Lcom/fasterxml/aalto/in/PName;

    invoke-virtual {v0}, Lcom/fasterxml/aalto/in/PName;->getPrefixedName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/in/XmlScanner;->reportUnexpectedEndTag(Ljava/lang/String;)V

    :cond_3
    iget-object v0, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    iget v2, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    add-int/lit8 v4, v2, 0x1

    iput v4, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    :goto_1
    and-int/lit16 v0, v0, 0xff

    const/16 v2, 0x20

    if-gt v0, v2, :cond_a

    const/16 v4, 0x101

    const/16 v6, 0xa

    if-ne v0, v6, :cond_5

    :cond_4
    :goto_2
    invoke-virtual {p0}, Lcom/fasterxml/aalto/in/ByteBasedScanner;->markLF()V

    goto :goto_4

    :cond_5
    const/16 v7, 0xd

    if-ne v0, v7, :cond_7

    iget v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v2, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-lt v0, v2, :cond_6

    iput v3, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_pendingInput:I

    :goto_3
    iput v5, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_nextEvent:I

    iput v1, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_state:I

    return v4

    :cond_6
    iget-object v2, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v2, v0}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    if-ne v0, v6, :cond_4

    iget v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    goto :goto_2

    :cond_7
    if-eq v0, v2, :cond_8

    const/16 v2, 0x9

    if-eq v0, v2, :cond_8

    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/in/XmlScanner;->throwInvalidSpace(I)V

    :cond_8
    :goto_4
    iget v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v2, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-lt v0, v2, :cond_9

    goto :goto_3

    :cond_9
    iget-object v2, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    add-int/lit8 v4, v0, 0x1

    iput v4, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {v2, v0}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    goto :goto_1

    :cond_a
    const/16 v1, 0x3e

    if-eq v0, v1, :cond_b

    int-to-byte v0, v0

    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->decodeCharForError(B)I

    move-result v0

    const-string v1, " expected space or closing \'>\'"

    invoke-virtual {p0, v0, v1}, Lcom/fasterxml/aalto/in/XmlScanner;->throwUnexpectedChar(ILjava/lang/String;)V

    :cond_b
    iput v5, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_currToken:I

    return v5
.end method

.method private final handleHexEntityInAttribute(Z)I
    .locals 10

    iget-object v0, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    iget v1, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    const-string v1, " expected a hex digit (0-9a-fA-F) for character entity"

    const/16 v2, 0x66

    const/16 v3, 0x30

    const/16 v4, 0x46

    const/16 v5, 0x39

    const/4 v6, 0x0

    const/16 v7, 0x61

    const/16 v8, 0x41

    if-eqz p1, :cond_4

    if-gt v0, v5, :cond_0

    if-lt v0, v3, :cond_0

    add-int/lit8 v0, v0, -0x30

    goto :goto_1

    :cond_0
    if-gt v0, v4, :cond_1

    if-lt v0, v8, :cond_1

    add-int/lit8 v0, v0, -0x41

    :goto_0
    add-int/lit8 v0, v0, 0xa

    goto :goto_1

    :cond_1
    if-gt v0, v2, :cond_2

    if-lt v0, v7, :cond_2

    add-int/lit8 v0, v0, -0x61

    goto :goto_0

    :cond_2
    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->decodeCharForError(B)I

    move-result p1

    invoke-virtual {p0, p1, v1}, Lcom/fasterxml/aalto/in/XmlScanner;->throwUnexpectedChar(ILjava/lang/String;)V

    :goto_1
    const/16 p1, -0x41

    iput p1, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_pendingInput:I

    iput v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_entityValue:I

    iget p1, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-lt p1, v0, :cond_3

    return v6

    :cond_3
    iget-object v0, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    add-int/lit8 v9, p1, 0x1

    iput v9, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {v0, p1}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    :cond_4
    const/16 p1, 0x3b

    if-eq v0, p1, :cond_9

    if-gt v0, v5, :cond_5

    if-lt v0, v3, :cond_5

    add-int/lit8 v0, v0, -0x30

    goto :goto_3

    :cond_5
    if-gt v0, v4, :cond_6

    if-lt v0, v8, :cond_6

    add-int/lit8 v0, v0, -0x41

    :goto_2
    add-int/lit8 v0, v0, 0xa

    goto :goto_3

    :cond_6
    if-gt v0, v2, :cond_7

    if-lt v0, v7, :cond_7

    add-int/lit8 v0, v0, -0x61

    goto :goto_2

    :cond_7
    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->decodeCharForError(B)I

    move-result p1

    invoke-virtual {p0, p1, v1}, Lcom/fasterxml/aalto/in/XmlScanner;->throwUnexpectedChar(ILjava/lang/String;)V

    :goto_3
    iget p1, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_entityValue:I

    shl-int/lit8 p1, p1, 0x4

    add-int/2addr p1, v0

    iput p1, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_entityValue:I

    const v0, 0x10ffff

    if-le p1, v0, :cond_8

    invoke-virtual {p0}, Lcom/fasterxml/aalto/in/XmlScanner;->reportEntityOverflow()V

    :cond_8
    iget p1, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-lt p1, v0, :cond_3

    return v6

    :cond_9
    iget p1, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_entityValue:I

    invoke-virtual {p0, p1}, Lcom/fasterxml/aalto/in/XmlScanner;->verifyXmlChar(I)V

    iput v6, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_pendingInput:I

    iget p1, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_entityValue:I

    return p1
.end method

.method private final handleNsValuePending()Z
    .locals 9

    iget v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_pendingInput:I

    const/4 v1, -0x1

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-ne v0, v1, :cond_2

    invoke-virtual {p0}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->handlePartialCR()Z

    move-result v0

    if-nez v0, :cond_0

    return v2

    :cond_0
    iget-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_nameBuffer:[C

    iget v1, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_elemNsPtr:I

    array-length v2, v0

    if-lt v1, v2, :cond_1

    array-length v1, v0

    invoke-static {v0, v1}, Lcom/fasterxml/aalto/util/DataUtil;->growArrayBy([CI)[C

    move-result-object v0

    iput-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_nameBuffer:[C

    :cond_1
    iget v1, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_elemNsPtr:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_elemNsPtr:I

    const/16 v2, 0x20

    aput-char v2, v0, v1

    return v3

    :cond_2
    iget v1, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v4, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-lt v1, v4, :cond_3

    return v2

    :cond_3
    const/16 v4, -0x3c

    const/16 v5, -0x3f

    const/16 v6, 0x78

    const/16 v7, -0x3d

    const/16 v8, -0x3e

    if-ne v0, v4, :cond_7

    iget-object v0, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    add-int/lit8 v4, v1, 0x1

    iput v4, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    const/16 v1, 0x23

    if-ne v0, v1, :cond_5

    iput v7, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_pendingInput:I

    iget v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v1, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-lt v0, v1, :cond_4

    return v2

    :cond_4
    iget-object v1, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    if-ne v0, v6, :cond_8

    iput v8, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_pendingInput:I

    iget v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    add-int/2addr v0, v3

    iput v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v1, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-lt v0, v1, :cond_a

    return v2

    :cond_5
    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->parseNewEntityName(B)Lcom/fasterxml/aalto/in/PName;

    move-result-object v0

    if-nez v0, :cond_6

    iput v5, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_pendingInput:I

    return v2

    :cond_6
    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->decodeGeneralEntity(Lcom/fasterxml/aalto/in/PName;)I

    move-result v1

    if-nez v1, :cond_d

    goto :goto_0

    :cond_7
    if-ne v0, v7, :cond_9

    iget-object v0, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    if-ne v0, v6, :cond_8

    iput v8, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_pendingInput:I

    iget v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    add-int/2addr v0, v3

    iput v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v1, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-lt v0, v1, :cond_a

    return v2

    :cond_8
    invoke-direct {p0, v3}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->handleDecEntityInAttribute(Z)I

    move-result v0

    goto :goto_1

    :cond_9
    if-ne v0, v8, :cond_b

    :cond_a
    invoke-direct {p0, v3}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->handleHexEntityInAttribute(Z)I

    move-result v0

    goto :goto_1

    :cond_b
    if-ne v0, v5, :cond_e

    invoke-virtual {p0}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->parseEntityName()Lcom/fasterxml/aalto/in/PName;

    move-result-object v0

    if-nez v0, :cond_c

    return v2

    :cond_c
    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->decodeGeneralEntity(Lcom/fasterxml/aalto/in/PName;)I

    move-result v1

    if-nez v1, :cond_d

    :goto_0
    iput-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_tokenName:Lcom/fasterxml/aalto/in/PName;

    iget-object v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_elemAttrName:Lcom/fasterxml/aalto/in/PName;

    invoke-virtual {p0, v0, v2}, Lcom/fasterxml/aalto/in/XmlScanner;->reportUnexpandedEntityInAttr(Lcom/fasterxml/aalto/in/PName;Z)V

    :cond_d
    move v0, v1

    goto :goto_1

    :cond_e
    const/16 v1, -0x40

    if-ne v0, v1, :cond_f

    invoke-direct {p0, v2}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->handleDecEntityInAttribute(Z)I

    move-result v0

    goto :goto_1

    :cond_f
    const/16 v1, -0x41

    if-ne v0, v1, :cond_10

    invoke-direct {p0, v2}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->handleHexEntityInAttribute(Z)I

    move-result v0

    goto :goto_1

    :cond_10
    invoke-direct {p0}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->handleAttrValuePendingUTF8()I

    move-result v0

    :goto_1
    if-nez v0, :cond_11

    return v2

    :cond_11
    iget-object v1, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_nameBuffer:[C

    shr-int/lit8 v2, v0, 0x10

    if-eqz v2, :cond_13

    const/high16 v2, 0x10000

    sub-int/2addr v0, v2

    iget v2, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_elemNsPtr:I

    array-length v4, v1

    if-lt v2, v4, :cond_12

    array-length v2, v1

    invoke-static {v1, v2}, Lcom/fasterxml/aalto/util/DataUtil;->growArrayBy([CI)[C

    move-result-object v1

    iput-object v1, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_nameBuffer:[C

    :cond_12
    iget v2, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_elemNsPtr:I

    add-int/lit8 v4, v2, 0x1

    iput v4, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_elemNsPtr:I

    shr-int/lit8 v4, v0, 0xa

    const v5, 0xd800

    or-int/2addr v4, v5

    int-to-char v4, v4

    aput-char v4, v1, v2

    and-int/lit16 v0, v0, 0x3ff

    const v2, 0xdc00

    or-int/2addr v0, v2

    :cond_13
    iget v2, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_elemNsPtr:I

    array-length v4, v1

    if-lt v2, v4, :cond_14

    array-length v2, v1

    invoke-static {v1, v2}, Lcom/fasterxml/aalto/util/DataUtil;->growArrayBy([CI)[C

    move-result-object v1

    iput-object v1, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_nameBuffer:[C

    :cond_14
    iget v2, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_elemNsPtr:I

    add-int/lit8 v4, v2, 0x1

    iput v4, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_elemNsPtr:I

    int-to-char v0, v0

    aput-char v0, v1, v2

    return v3
.end method

.method private initAttribute(B)V
    .locals 4

    iput-byte p1, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_elemAttrQuote:B

    iget-object p1, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_elemAttrName:Lcom/fasterxml/aalto/in/PName;

    invoke-virtual {p1}, Lcom/fasterxml/aalto/in/PName;->getPrefix()Ljava/lang/String;

    move-result-object v0

    const-string v1, "xmlns"

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/fasterxml/aalto/in/PName;->getLocalName()Ljava/lang/String;

    move-result-object v0

    if-ne v0, v1, :cond_2

    goto :goto_0

    :cond_0
    if-ne v0, v1, :cond_1

    :goto_0
    move v0, v3

    goto :goto_1

    :cond_1
    invoke-virtual {p0, p1, v0}, Lcom/fasterxml/aalto/in/XmlScanner;->bindName(Lcom/fasterxml/aalto/in/PName;Ljava/lang/String;)Lcom/fasterxml/aalto/in/PName;

    move-result-object p1

    iget-boolean v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_elemAllNsBound:Z

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/fasterxml/aalto/in/PName;->isBound()Z

    move-result v0

    iput-boolean v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_elemAllNsBound:Z

    :cond_2
    move v0, v2

    :goto_1
    if-eqz v0, :cond_3

    const/16 p1, 0x8

    iput p1, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_state:I

    iput v2, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_elemNsPtr:I

    iget p1, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_currNsCount:I

    add-int/2addr p1, v3

    iput p1, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_currNsCount:I

    goto :goto_2

    :cond_3
    const/4 v0, 0x7

    iput v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_state:I

    iget-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_attrCollector:Lcom/fasterxml/aalto/in/AttributeCollector;

    iget v1, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_elemAttrPtr:I

    invoke-virtual {v0, p1, v1}, Lcom/fasterxml/aalto/in/AttributeCollector;->startNewValue(Lcom/fasterxml/aalto/in/PName;I)[C

    :goto_2
    return-void
.end method

.method private initStartElement(Lcom/fasterxml/aalto/in/PName;)V
    .locals 2

    invoke-virtual {p1}, Lcom/fasterxml/aalto/in/PName;->getPrefix()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {p0, p1, v0}, Lcom/fasterxml/aalto/in/XmlScanner;->bindName(Lcom/fasterxml/aalto/in/PName;Ljava/lang/String;)Lcom/fasterxml/aalto/in/PName;

    move-result-object p1

    invoke-virtual {p1}, Lcom/fasterxml/aalto/in/PName;->isBound()Z

    move-result v0

    :goto_0
    iput-boolean v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_elemAllNsBound:Z

    iput-object p1, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_tokenName:Lcom/fasterxml/aalto/in/PName;

    new-instance v0, Lcom/fasterxml/aalto/in/ElementScope;

    iget-object v1, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_currElem:Lcom/fasterxml/aalto/in/ElementScope;

    invoke-direct {v0, p1, v1}, Lcom/fasterxml/aalto/in/ElementScope;-><init>(Lcom/fasterxml/aalto/in/PName;Lcom/fasterxml/aalto/in/ElementScope;)V

    iput-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_currElem:Lcom/fasterxml/aalto/in/ElementScope;

    const/4 p1, 0x0

    iput p1, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_attrCount:I

    iput p1, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_currNsCount:I

    iput p1, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_elemAttrPtr:I

    const/4 p1, 0x2

    iput p1, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_state:I

    return-void
.end method

.method private skipEntityInCharacters()I
    .locals 8

    iget v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    add-int/lit8 v1, v0, 0x3

    iget v2, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-gt v1, v2, :cond_6

    iget-object v1, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    add-int/lit8 v2, v0, 0x1

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    const/16 v1, 0x23

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    const/16 v1, 0x78

    if-ne v0, v1, :cond_0

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {p0, v2}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->handleHexEntityInCharacters(I)I

    move-result v0

    return v0

    :cond_0
    invoke-virtual {p0, v2}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->handleDecEntityInCharacters(I)I

    move-result v0

    return v0

    :cond_1
    const/16 v1, 0x61

    const/16 v3, 0x27

    const/16 v4, 0x6f

    const/16 v5, 0x3b

    if-ne v0, v1, :cond_3

    iget-object v0, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    add-int/lit8 v1, v2, 0x1

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    const/16 v2, 0x6d

    const/16 v6, 0x70

    if-ne v0, v2, :cond_2

    add-int/lit8 v0, v1, 0x1

    iget v2, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    if-ge v0, v2, :cond_6

    iget-object v2, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v2, v1}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v2

    if-ne v2, v6, :cond_6

    iget-object v2, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v2, v0}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    if-ne v0, v5, :cond_6

    add-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    const/16 v0, 0x26

    return v0

    :cond_2
    if-ne v0, v6, :cond_6

    add-int/lit8 v0, v1, 0x2

    iget v2, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    if-ge v0, v2, :cond_6

    iget-object v2, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v2, v1}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v2

    if-ne v2, v4, :cond_6

    iget-object v2, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    add-int/lit8 v4, v1, 0x1

    invoke-virtual {v2, v4}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v2

    const/16 v4, 0x73

    if-ne v2, v4, :cond_6

    iget-object v2, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v2, v0}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    if-ne v0, v5, :cond_6

    add-int/lit8 v1, v1, 0x3

    iput v1, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    return v3

    :cond_3
    const/16 v1, 0x67

    const/16 v6, 0x74

    if-ne v0, v1, :cond_4

    iget-object v0, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    if-ne v0, v6, :cond_6

    iget-object v0, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    add-int/lit8 v1, v2, 0x1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    if-ne v0, v5, :cond_6

    add-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    const/16 v0, 0x3e

    return v0

    :cond_4
    const/16 v1, 0x6c

    if-ne v0, v1, :cond_5

    iget-object v0, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    if-ne v0, v6, :cond_6

    iget-object v0, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    add-int/lit8 v1, v2, 0x1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    if-ne v0, v5, :cond_6

    add-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    const/16 v0, 0x3c

    return v0

    :cond_5
    const/16 v1, 0x71

    if-ne v0, v1, :cond_6

    add-int/lit8 v0, v2, 0x3

    iget v1, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    if-ge v0, v1, :cond_6

    iget-object v1, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v1

    const/16 v7, 0x75

    if-ne v1, v7, :cond_6

    iget-object v1, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    add-int/lit8 v7, v2, 0x1

    invoke-virtual {v1, v7}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v1

    if-ne v1, v4, :cond_6

    iget-object v1, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    add-int/lit8 v4, v2, 0x2

    invoke-virtual {v1, v4}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v1

    if-ne v1, v6, :cond_6

    iget-object v1, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    if-ne v0, v5, :cond_6

    add-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    return v3

    :cond_6
    const/4 v0, 0x0

    return v0
.end method

.method private final skipPending()Z
    .locals 8

    iget v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v1, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    const/4 v2, 0x0

    if-lt v0, v1, :cond_0

    return v2

    :cond_0
    iget v1, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_pendingInput:I

    const/4 v3, 0x1

    if-gez v1, :cond_11

    :cond_1
    iget v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_pendingInput:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_f

    const/16 v1, 0x9

    const/16 v4, 0x5d

    packed-switch v0, :pswitch_data_0

    invoke-virtual {p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->throwInternal()I

    goto/16 :goto_2

    :pswitch_0
    iget-object v0, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    iget v4, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    add-int/lit8 v5, v4, 0x1

    iput v5, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {v0, v4}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    const/16 v4, 0x23

    if-ne v0, v4, :cond_2

    const/16 v0, -0x51

    goto/16 :goto_1

    :cond_2
    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->parseNewEntityName(B)Lcom/fasterxml/aalto/in/PName;

    move-result-object v0

    if-nez v0, :cond_3

    const/16 v0, -0x54

    :goto_0
    iput v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_pendingInput:I

    return v2

    :cond_3
    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->decodeGeneralEntity(Lcom/fasterxml/aalto/in/PName;)I

    move-result v4

    if-nez v4, :cond_4

    iput-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_tokenName:Lcom/fasterxml/aalto/in/PName;

    iput v1, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_nextEvent:I

    :cond_4
    iput v2, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_pendingInput:I

    return v3

    :pswitch_1
    iput v2, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_entityValue:I

    iget-object v0, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    iget v1, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    const/16 v1, 0x78

    if-ne v0, v1, :cond_6

    iget v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    add-int/2addr v0, v3

    iput v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {p0}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->decodeHexEntity()Z

    move-result v0

    if-eqz v0, :cond_5

    iput v2, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_pendingInput:I

    return v3

    :cond_5
    const/16 v0, -0x53

    goto :goto_0

    :cond_6
    invoke-virtual {p0}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->decodeDecEntity()Z

    move-result v0

    if-eqz v0, :cond_7

    iput v2, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_pendingInput:I

    return v3

    :cond_7
    const/16 v0, -0x52

    goto :goto_0

    :pswitch_2
    invoke-virtual {p0}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->decodeDecEntity()Z

    move-result v0

    if-eqz v0, :cond_8

    iput v2, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_pendingInput:I

    return v3

    :cond_8
    return v2

    :pswitch_3
    invoke-virtual {p0}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->decodeHexEntity()Z

    move-result v0

    if-eqz v0, :cond_9

    iput v2, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_pendingInput:I

    return v3

    :cond_9
    return v2

    :pswitch_4
    invoke-virtual {p0}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->parseEntityName()Lcom/fasterxml/aalto/in/PName;

    move-result-object v0

    if-nez v0, :cond_a

    return v2

    :cond_a
    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->decodeGeneralEntity(Lcom/fasterxml/aalto/in/PName;)I

    move-result v4

    if-nez v4, :cond_b

    iput-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_tokenName:Lcom/fasterxml/aalto/in/PName;

    iput v1, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_nextEvent:I

    :cond_b
    iput v2, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_pendingInput:I

    return v3

    :pswitch_5
    iget-object v0, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    iget v1, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    if-eq v0, v4, :cond_c

    iput v2, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_pendingInput:I

    return v3

    :cond_c
    iget v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    add-int/2addr v0, v3

    iput v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    const/16 v0, -0x56

    :goto_1
    iput v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_pendingInput:I

    goto :goto_2

    :pswitch_6
    iget-object v0, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    iget v1, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    if-ne v0, v4, :cond_d

    iget v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    add-int/2addr v0, v3

    iput v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    :goto_2
    iget v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v1, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-lt v0, v1, :cond_1

    return v2

    :cond_d
    const/16 v1, 0x3e

    if-ne v0, v1, :cond_e

    iget v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    add-int/2addr v0, v3

    iput v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    const-string v0, "Encountered \']]>\' in text segment"

    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/in/XmlScanner;->reportInputProblem(Ljava/lang/String;)V

    :cond_e
    iput v2, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_pendingInput:I

    return v3

    :cond_f
    iput v2, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_pendingInput:I

    iget-object v0, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    iget v1, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    const/16 v1, 0xa

    if-ne v0, v1, :cond_10

    iget v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    add-int/2addr v0, v3

    iput v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    :cond_10
    invoke-virtual {p0}, Lcom/fasterxml/aalto/in/ByteBasedScanner;->markLF()V

    return v3

    :cond_11
    iget-object v4, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_charTypes:Lcom/fasterxml/aalto/util/XmlCharTypes;

    iget-object v4, v4, Lcom/fasterxml/aalto/util/XmlCharTypes;->TEXT_CHARS:[I

    and-int/lit16 v5, v1, 0xff

    aget v4, v4, v5

    const/4 v6, 0x5

    if-eq v4, v6, :cond_1b

    const/4 v6, 0x6

    if-eq v4, v6, :cond_18

    const/4 v6, 0x7

    if-eq v4, v6, :cond_12

    invoke-virtual {p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->throwInternal()I

    goto/16 :goto_5

    :cond_12
    iget-object v4, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    add-int/lit8 v6, v0, 0x1

    iput v6, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {v4, v0}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    and-int/lit16 v0, v0, 0xff

    shr-int/lit8 v4, v1, 0x8

    if-nez v4, :cond_15

    iget v4, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v5, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-lt v4, v5, :cond_13

    :goto_3
    shl-int/lit8 v0, v0, 0x8

    :goto_4
    or-int/2addr v0, v1

    goto/16 :goto_0

    :cond_13
    iget-object v5, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    add-int/lit8 v6, v4, 0x1

    iput v6, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {v5, v4}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v4

    and-int/lit16 v4, v4, 0xff

    iget v5, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v6, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-lt v5, v6, :cond_14

    shl-int/lit8 v0, v0, 0x8

    or-int/2addr v0, v1

    shl-int/lit8 v1, v4, 0x10

    or-int/2addr v0, v1

    goto/16 :goto_0

    :cond_14
    iget-object v6, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    add-int/lit8 v7, v5, 0x1

    iput v7, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {v6, v5}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v5

    and-int/lit16 v5, v5, 0xff

    invoke-virtual {p0, v1, v0, v4, v5}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->decodeUtf8_4(IIII)I

    goto :goto_5

    :cond_15
    and-int/lit16 v4, v4, 0xff

    shr-int/lit8 v6, v1, 0x10

    if-nez v6, :cond_17

    iget v6, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v7, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-lt v6, v7, :cond_16

    shl-int/lit8 v0, v0, 0x10

    goto :goto_4

    :cond_16
    iget-object v1, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    add-int/lit8 v7, v6, 0x1

    iput v7, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {v1, v6}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v1

    and-int/lit16 v1, v1, 0xff

    invoke-virtual {p0, v5, v4, v0, v1}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->decodeUtf8_4(IIII)I

    goto :goto_5

    :cond_17
    invoke-virtual {p0, v5, v4, v6, v0}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->decodeUtf8_4(IIII)I

    goto :goto_5

    :cond_18
    iget-object v4, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    add-int/lit8 v6, v0, 0x1

    iput v6, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {v4, v0}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    and-int/lit16 v0, v0, 0xff

    shr-int/lit8 v4, v1, 0x8

    if-nez v4, :cond_1a

    iget v4, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v5, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-lt v4, v5, :cond_19

    goto :goto_3

    :cond_19
    iget-object v5, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    add-int/lit8 v6, v4, 0x1

    iput v6, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {v5, v4}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v4

    and-int/lit16 v4, v4, 0xff

    invoke-virtual {p0, v1, v0, v4}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->decodeUtf8_3(III)I

    goto :goto_5

    :cond_1a
    invoke-virtual {p0, v5, v4, v0}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->decodeUtf8_3(III)I

    goto :goto_5

    :cond_1b
    invoke-virtual {p0, v1}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->skipUtf8_2(I)V

    :goto_5
    iput v2, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_pendingInput:I

    return v3

    nop

    :pswitch_data_0
    .packed-switch -0x56
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public final _currentByte()B
    .locals 2

    iget-object v0, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    iget v1, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    return v0
.end method

.method public final _nextByte()B
    .locals 3

    iget-object v0, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    iget v1, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    return v0
.end method

.method public final _prevByte()B
    .locals 2

    iget-object v0, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    iget v1, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    return v0
.end method

.method public asyncSkipSpace()Z
    .locals 8

    :cond_0
    :goto_0
    iget v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v1, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    const/4 v2, 0x0

    if-ge v0, v1, :cond_7

    iget-object v1, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    and-int/lit16 v1, v0, 0xff

    const/4 v3, -0x1

    const/16 v4, 0x20

    const/4 v5, 0x1

    if-le v1, v4, :cond_2

    iget v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_pendingInput:I

    if-ne v0, v3, :cond_1

    invoke-virtual {p0}, Lcom/fasterxml/aalto/in/ByteBasedScanner;->markLF()V

    iput v2, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_pendingInput:I

    :cond_1
    return v5

    :cond_2
    iget v1, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    add-int/2addr v1, v5

    iput v1, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    const/16 v6, 0xa

    if-ne v0, v6, :cond_4

    :cond_3
    :goto_1
    invoke-virtual {p0}, Lcom/fasterxml/aalto/in/ByteBasedScanner;->markLF()V

    goto :goto_0

    :cond_4
    const/16 v7, 0xd

    if-ne v0, v7, :cond_6

    iget v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-lt v1, v0, :cond_5

    iput v3, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_pendingInput:I

    goto :goto_2

    :cond_5
    iget-object v0, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    if-ne v0, v6, :cond_3

    iget v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    add-int/2addr v0, v5

    iput v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    goto :goto_1

    :cond_6
    if-eq v0, v4, :cond_0

    const/16 v1, 0x9

    if-eq v0, v1, :cond_0

    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/in/XmlScanner;->throwInvalidSpace(I)V

    goto :goto_0

    :cond_7
    :goto_2
    return v2
.end method

.method public final decodeDecEntity()Z
    .locals 4

    iget v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_entityValue:I

    :cond_0
    :goto_0
    iget v1, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v2, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-ge v1, v2, :cond_4

    iget-object v2, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    add-int/lit8 v3, v1, 0x1

    iput v3, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {v2, v1}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v1

    const/16 v2, 0x3b

    if-ne v1, v2, :cond_1

    iput v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_entityValue:I

    const/4 v0, 0x1

    return v0

    :cond_1
    add-int/lit8 v2, v1, -0x30

    if-ltz v2, :cond_2

    const/16 v3, 0x9

    if-le v2, v3, :cond_3

    :cond_2
    invoke-virtual {p0, v1}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->decodeCharForError(B)I

    move-result v1

    const-string v3, " expected a digit (0 - 9) for character entity"

    invoke-virtual {p0, v1, v3}, Lcom/fasterxml/aalto/in/XmlScanner;->throwUnexpectedChar(ILjava/lang/String;)V

    :cond_3
    mul-int/lit8 v0, v0, 0xa

    add-int/2addr v0, v2

    const v1, 0x10ffff

    if-le v0, v1, :cond_0

    iput v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_entityValue:I

    invoke-virtual {p0}, Lcom/fasterxml/aalto/in/XmlScanner;->reportEntityOverflow()V

    goto :goto_0

    :cond_4
    iput v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_entityValue:I

    const/4 v0, 0x0

    return v0
.end method

.method public final decodeGeneralEntity(Lcom/fasterxml/aalto/in/PName;)I
    .locals 3

    iget-object v0, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    iget v1, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    const/16 v1, 0x3b

    if-eq v0, v1, :cond_0

    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->decodeCharForError(B)I

    move-result v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " expected \';\' following entity name (\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/fasterxml/aalto/in/PName;->getPrefixedName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "\")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/fasterxml/aalto/in/XmlScanner;->throwUnexpectedChar(ILjava/lang/String;)V

    :cond_0
    invoke-virtual {p1}, Lcom/fasterxml/aalto/in/PName;->getPrefixedName()Ljava/lang/String;

    move-result-object p1

    const-string v0, "amp"

    if-ne p1, v0, :cond_1

    const/16 p1, 0x26

    return p1

    :cond_1
    const-string v0, "lt"

    if-ne p1, v0, :cond_2

    const/16 p1, 0x3c

    return p1

    :cond_2
    const-string v0, "apos"

    if-ne p1, v0, :cond_3

    const/16 p1, 0x27

    return p1

    :cond_3
    const-string v0, "quot"

    if-ne p1, v0, :cond_4

    const/16 p1, 0x22

    return p1

    :cond_4
    const-string v0, "gt"

    if-ne p1, v0, :cond_5

    const/16 p1, 0x3e

    return p1

    :cond_5
    const/4 p1, 0x0

    return p1
.end method

.method public final decodeHexEntity()Z
    .locals 4

    iget v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_entityValue:I

    :cond_0
    :goto_0
    iget v1, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v2, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-ge v1, v2, :cond_5

    iget-object v2, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    add-int/lit8 v3, v1, 0x1

    iput v3, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {v2, v1}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v1

    const/16 v2, 0x3b

    if-ne v1, v2, :cond_1

    iput v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_entityValue:I

    const/4 v0, 0x1

    return v0

    :cond_1
    const/16 v2, 0x39

    if-gt v1, v2, :cond_2

    const/16 v2, 0x30

    if-lt v1, v2, :cond_2

    add-int/lit8 v1, v1, -0x30

    goto :goto_2

    :cond_2
    const/16 v2, 0x46

    if-gt v1, v2, :cond_3

    const/16 v2, 0x41

    if-lt v1, v2, :cond_3

    add-int/lit8 v1, v1, -0x41

    :goto_1
    add-int/lit8 v1, v1, 0xa

    goto :goto_2

    :cond_3
    const/16 v2, 0x66

    if-gt v1, v2, :cond_4

    const/16 v2, 0x61

    if-lt v1, v2, :cond_4

    add-int/lit8 v1, v1, -0x61

    goto :goto_1

    :cond_4
    invoke-virtual {p0, v1}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->decodeCharForError(B)I

    move-result v2

    const-string v3, " expected a hex digit (0-9a-fA-F) for character entity"

    invoke-virtual {p0, v2, v3}, Lcom/fasterxml/aalto/in/XmlScanner;->throwUnexpectedChar(ILjava/lang/String;)V

    :goto_2
    shl-int/lit8 v0, v0, 0x4

    add-int/2addr v0, v1

    const v1, 0x10ffff

    if-le v0, v1, :cond_0

    iput v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_entityValue:I

    invoke-virtual {p0}, Lcom/fasterxml/aalto/in/XmlScanner;->reportEntityOverflow()V

    goto :goto_0

    :cond_5
    iput v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_entityValue:I

    const/4 v0, 0x0

    return v0
.end method

.method public final decodeUtf8_2(I)I
    .locals 3

    iget-object v0, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    iget v1, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    and-int/lit16 v1, v0, 0xc0

    const/16 v2, 0x80

    if-eq v1, v2, :cond_0

    and-int/lit16 v1, v0, 0xff

    iget v2, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {p0, v1, v2}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->reportInvalidOther(II)V

    :cond_0
    and-int/lit8 p1, p1, 0x1f

    shl-int/lit8 p1, p1, 0x6

    and-int/lit8 v0, v0, 0x3f

    or-int/2addr p1, v0

    return p1
.end method

.method public final decodeUtf8_3(I)I
    .locals 5

    .line 1
    and-int/lit8 p1, p1, 0xf

    iget-object v0, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    iget v1, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    and-int/lit16 v1, v0, 0xc0

    const/16 v2, 0x80

    if-eq v1, v2, :cond_0

    and-int/lit16 v1, v0, 0xff

    iget v3, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {p0, v1, v3}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->reportInvalidOther(II)V

    :cond_0
    shl-int/lit8 v1, p1, 0x6

    and-int/lit8 v0, v0, 0x3f

    or-int/2addr v0, v1

    iget-object v1, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    iget v3, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {v1, v3}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v1

    and-int/lit16 v3, v1, 0xc0

    if-eq v3, v2, :cond_1

    and-int/lit16 v2, v1, 0xff

    iget v3, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {p0, v2, v3}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->reportInvalidOther(II)V

    :cond_1
    shl-int/lit8 v0, v0, 0x6

    and-int/lit8 v1, v1, 0x3f

    or-int/2addr v0, v1

    const/16 v1, 0xd

    if-lt p1, v1, :cond_3

    const p1, 0xd800

    if-lt v0, p1, :cond_3

    const p1, 0xe000

    if-lt v0, p1, :cond_2

    const p1, 0xfffe

    if-lt v0, p1, :cond_3

    const p1, 0xffff

    if-gt v0, p1, :cond_3

    :cond_2
    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/in/XmlScanner;->handleInvalidXmlChar(I)C

    move-result v0

    :cond_3
    return v0
.end method

.method public final decodeUtf8_3(III)I
    .locals 3

    .line 2
    and-int/lit16 v0, p2, 0xc0

    const/16 v1, 0x80

    if-eq v0, v1, :cond_0

    and-int/lit16 v0, p2, 0xff

    iget v2, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {p0, v0, v2}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->reportInvalidOther(II)V

    :cond_0
    and-int/lit16 v0, p3, 0xc0

    if-eq v0, v1, :cond_1

    and-int/lit16 v0, p3, 0xff

    iget v1, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {p0, v0, v1}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->reportInvalidOther(II)V

    :cond_1
    and-int/lit8 v0, p1, 0xf

    shl-int/lit8 v0, v0, 0xc

    and-int/lit8 p2, p2, 0x3f

    shl-int/lit8 p2, p2, 0x6

    or-int/2addr p2, v0

    and-int/lit8 p3, p3, 0x3f

    or-int/2addr p2, p3

    const/16 p3, 0xd

    if-lt p1, p3, :cond_3

    const p1, 0xd800

    if-lt p2, p1, :cond_3

    const p1, 0xe000

    if-lt p2, p1, :cond_2

    const p1, 0xfffe

    if-lt p2, p1, :cond_3

    const p1, 0xffff

    if-gt p2, p1, :cond_3

    :cond_2
    invoke-virtual {p0, p2}, Lcom/fasterxml/aalto/in/XmlScanner;->handleInvalidXmlChar(I)C

    move-result p2

    :cond_3
    return p2
.end method

.method public final decodeUtf8_4(I)I
    .locals 4

    .line 1
    iget-object v0, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    iget v1, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    and-int/lit16 v1, v0, 0xc0

    const/16 v2, 0x80

    if-eq v1, v2, :cond_0

    and-int/lit16 v1, v0, 0xff

    iget v3, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {p0, v1, v3}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->reportInvalidOther(II)V

    :cond_0
    and-int/lit8 p1, p1, 0x7

    shl-int/lit8 p1, p1, 0x6

    and-int/lit8 v0, v0, 0x3f

    or-int/2addr p1, v0

    iget-object v0, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    iget v1, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    add-int/lit8 v3, v1, 0x1

    iput v3, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    and-int/lit16 v1, v0, 0xc0

    if-eq v1, v2, :cond_1

    and-int/lit16 v1, v0, 0xff

    iget v3, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {p0, v1, v3}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->reportInvalidOther(II)V

    :cond_1
    shl-int/lit8 p1, p1, 0x6

    and-int/lit8 v0, v0, 0x3f

    or-int/2addr p1, v0

    iget-object v0, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    iget v1, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    add-int/lit8 v3, v1, 0x1

    iput v3, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    and-int/lit16 v1, v0, 0xc0

    if-eq v1, v2, :cond_2

    and-int/lit16 v1, v0, 0xff

    iget v2, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {p0, v1, v2}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->reportInvalidOther(II)V

    :cond_2
    shl-int/lit8 p1, p1, 0x6

    and-int/lit8 v0, v0, 0x3f

    or-int/2addr p1, v0

    const/high16 v0, 0x10000

    sub-int/2addr p1, v0

    return p1
.end method

.method public final decodeUtf8_4(IIII)I
    .locals 3

    .line 2
    and-int/lit16 v0, p2, 0xc0

    const/16 v1, 0x80

    if-eq v0, v1, :cond_0

    and-int/lit16 v0, p2, 0xff

    iget v2, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    add-int/lit8 v2, v2, -0x2

    invoke-virtual {p0, v0, v2}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->reportInvalidOther(II)V

    :cond_0
    and-int/lit8 p1, p1, 0x7

    shl-int/lit8 p1, p1, 0x6

    and-int/lit8 p2, p2, 0x3f

    or-int/2addr p1, p2

    and-int/lit16 p2, p3, 0xc0

    if-eq p2, v1, :cond_1

    and-int/lit16 p2, p3, 0xff

    iget v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, p2, v0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->reportInvalidOther(II)V

    :cond_1
    shl-int/lit8 p1, p1, 0x6

    and-int/lit8 p2, p3, 0x3f

    or-int/2addr p1, p2

    and-int/lit16 p2, p4, 0xc0

    if-eq p2, v1, :cond_2

    and-int/lit16 p2, p4, 0xff

    iget p3, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {p0, p2, p3}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->reportInvalidOther(II)V

    :cond_2
    shl-int/lit8 p1, p1, 0x6

    and-int/lit8 p2, p4, 0x3f

    or-int/2addr p1, p2

    const/high16 p2, 0x10000

    sub-int/2addr p1, p2

    return p1
.end method

.method public feedInput(Ljava/nio/ByteBuffer;)V
    .locals 5

    iget v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v1, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-lt v0, v1, :cond_1

    iget-boolean v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_endOfInput:Z

    if-nez v0, :cond_0

    iget-wide v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_pastBytesOrChars:J

    iget v2, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_origBufferLen:I

    int-to-long v3, v2

    add-long/2addr v0, v3

    iput-wide v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_pastBytesOrChars:J

    iget v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_rowStartOffset:I

    sub-int/2addr v0, v2

    iput v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_rowStartOffset:I

    invoke-virtual {p1}, Ljava/nio/Buffer;->position()I

    move-result v0

    invoke-virtual {p1}, Ljava/nio/Buffer;->limit()I

    move-result v1

    iput-object p1, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    iput v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iput v1, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    sub-int/2addr v1, v0

    iput v1, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_origBufferLen:I

    return-void

    :cond_0
    new-instance p1, Ljavax/xml/stream/XMLStreamException;

    const-string v0, "Already closed, can not feed more input"

    invoke-direct {p1, v0}, Ljavax/xml/stream/XMLStreamException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    new-instance p1, Ljavax/xml/stream/XMLStreamException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Still have "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    iget v2, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    sub-int/2addr v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, " unread bytes"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljavax/xml/stream/XMLStreamException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public final finishCharacters()V
    .locals 13

    iget v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_pendingInput:I

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->throwInternal()I

    :cond_0
    iget-object v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_charTypes:Lcom/fasterxml/aalto/util/XmlCharTypes;

    iget-object v0, v0, Lcom/fasterxml/aalto/util/XmlCharTypes;->TEXT_CHARS:[I

    iget-object v1, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    iget-object v2, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_textBuilder:Lcom/fasterxml/aalto/util/TextBuilder;

    invoke-virtual {v2}, Lcom/fasterxml/aalto/util/TextBuilder;->getBufferWithoutReset()[C

    move-result-object v2

    iget-object v3, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_textBuilder:Lcom/fasterxml/aalto/util/TextBuilder;

    invoke-virtual {v3}, Lcom/fasterxml/aalto/util/TextBuilder;->getCurrentLength()I

    move-result v3

    :goto_0
    iget v4, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v5, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-lt v4, v5, :cond_1

    goto/16 :goto_a

    :cond_1
    array-length v5, v2

    const/4 v6, 0x0

    if-lt v3, v5, :cond_2

    iget-object v2, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_textBuilder:Lcom/fasterxml/aalto/util/TextBuilder;

    invoke-virtual {v2}, Lcom/fasterxml/aalto/util/TextBuilder;->finishCurrentSegment()[C

    move-result-object v2

    move v3, v6

    :cond_2
    iget v5, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    array-length v7, v2

    sub-int/2addr v7, v3

    add-int/2addr v7, v4

    if-ge v7, v5, :cond_3

    move v5, v7

    :cond_3
    :goto_1
    if-ge v4, v5, :cond_12

    add-int/lit8 v7, v4, 0x1

    invoke-virtual {v1, v4}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v4

    and-int/lit16 v4, v4, 0xff

    aget v8, v0, v4

    if-eqz v8, :cond_11

    iput v7, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    const v5, 0xdc00

    const v9, 0xd800

    const/4 v10, -0x1

    const/4 v11, 0x1

    packed-switch v8, :pswitch_data_0

    :pswitch_0
    goto/16 :goto_b

    :pswitch_1
    move v7, v6

    move v5, v11

    :goto_2
    iget v8, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v9, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    const/16 v12, 0x5d

    if-ge v8, v9, :cond_5

    invoke-virtual {v1, v8}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v7

    if-eq v7, v12, :cond_4

    goto :goto_3

    :cond_4
    iget v8, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    add-int/2addr v8, v11

    iput v8, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    :cond_5
    :goto_3
    const/16 v8, 0x3e

    if-ne v7, v8, :cond_6

    if-le v5, v11, :cond_6

    invoke-virtual {p0}, Lcom/fasterxml/aalto/in/XmlScanner;->reportIllegalCDataEnd()V

    :cond_6
    :goto_4
    add-int/2addr v5, v10

    if-lez v5, :cond_10

    add-int/lit8 v7, v3, 0x1

    aput-char v12, v2, v3

    array-length v3, v2

    if-lt v7, v3, :cond_7

    iget-object v2, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_textBuilder:Lcom/fasterxml/aalto/util/TextBuilder;

    invoke-virtual {v2}, Lcom/fasterxml/aalto/util/TextBuilder;->finishCurrentSegment()[C

    move-result-object v2

    move v3, v6

    goto :goto_4

    :cond_7
    move v3, v7

    goto :goto_4

    :pswitch_2
    invoke-virtual {p0}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->handleEntityInCharacters()I

    move-result v4

    if-nez v4, :cond_8

    goto/16 :goto_9

    :cond_8
    shr-int/lit8 v7, v4, 0x10

    if-eqz v7, :cond_10

    const/high16 v7, 0x10000

    sub-int/2addr v4, v7

    add-int/lit8 v7, v3, 0x1

    shr-int/lit8 v8, v4, 0xa

    or-int/2addr v8, v9

    int-to-char v8, v8

    aput-char v8, v2, v3

    array-length v3, v2

    if-lt v7, v3, :cond_a

    goto :goto_6

    :pswitch_3
    iget v8, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    sub-int v10, v8, v7

    const/4 v11, 0x3

    if-ge v10, v11, :cond_9

    if-le v8, v7, :cond_c

    iget-object v0, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    add-int/lit8 v1, v7, 0x1

    iput v1, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {v0, v7}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x8

    or-int/2addr v4, v0

    iget v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    iget v1, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    if-le v0, v1, :cond_c

    iget-object v0, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x10

    :goto_5
    or-int/2addr v4, v0

    goto :goto_8

    :cond_9
    invoke-virtual {p0, v4}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->decodeUtf8_4(I)I

    move-result v4

    add-int/lit8 v7, v3, 0x1

    shr-int/lit8 v8, v4, 0xa

    or-int/2addr v8, v9

    int-to-char v8, v8

    aput-char v8, v2, v3

    array-length v3, v2

    if-lt v7, v3, :cond_a

    :goto_6
    iget-object v2, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_textBuilder:Lcom/fasterxml/aalto/util/TextBuilder;

    invoke-virtual {v2}, Lcom/fasterxml/aalto/util/TextBuilder;->finishCurrentSegment()[C

    move-result-object v2

    goto :goto_7

    :cond_a
    move v6, v7

    :goto_7
    and-int/lit16 v3, v4, 0x3ff

    or-int v4, v3, v5

    move v3, v6

    goto :goto_b

    :pswitch_4
    iget v5, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    sub-int v6, v5, v7

    const/4 v8, 0x2

    if-ge v6, v8, :cond_b

    if-le v5, v7, :cond_c

    iget-object v0, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    add-int/lit8 v1, v7, 0x1

    iput v1, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {v0, v7}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x8

    goto :goto_5

    :cond_b
    invoke-virtual {p0, v4}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->decodeUtf8_3(I)I

    move-result v4

    goto :goto_b

    :pswitch_5
    iget v5, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-lt v7, v5, :cond_d

    :cond_c
    :goto_8
    iput v4, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_pendingInput:I

    goto :goto_a

    :cond_d
    invoke-virtual {p0, v4}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->decodeUtf8_2(I)I

    move-result v4

    goto :goto_b

    :pswitch_6
    invoke-virtual {p0, v4}, Lcom/fasterxml/aalto/in/ByteBasedScanner;->reportInvalidInitial(I)V

    :goto_9
    :pswitch_7
    iget v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    sub-int/2addr v0, v11

    iput v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    goto :goto_a

    :pswitch_8
    invoke-virtual {p0}, Lcom/fasterxml/aalto/in/ByteBasedScanner;->markLF()V

    goto :goto_b

    :pswitch_9
    invoke-virtual {p0, v4}, Lcom/fasterxml/aalto/in/XmlScanner;->handleInvalidXmlChar(I)C

    :pswitch_a
    iget v4, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v5, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-lt v4, v5, :cond_e

    iput v10, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_pendingInput:I

    :goto_a
    iget-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_textBuilder:Lcom/fasterxml/aalto/util/TextBuilder;

    invoke-virtual {v0, v3}, Lcom/fasterxml/aalto/util/TextBuilder;->setCurrentLength(I)V

    return-void

    :cond_e
    invoke-virtual {v1, v4}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v4

    const/16 v5, 0xa

    if-ne v4, v5, :cond_f

    iget v4, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    add-int/2addr v4, v11

    iput v4, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    :cond_f
    invoke-virtual {p0}, Lcom/fasterxml/aalto/in/ByteBasedScanner;->markLF()V

    move v4, v5

    :cond_10
    :goto_b
    add-int/lit8 v5, v3, 0x1

    int-to-char v4, v4

    aput-char v4, v2, v3

    move v3, v5

    goto/16 :goto_0

    :cond_11
    add-int/lit8 v8, v3, 0x1

    int-to-char v4, v4

    aput-char v4, v2, v3

    move v4, v7

    move v3, v8

    goto/16 :goto_1

    :cond_12
    iput v4, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_9
        :pswitch_a
        :pswitch_8
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_0
        :pswitch_7
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public final finishCharactersCoalescing()I
    .locals 1

    iget v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_pendingInput:I

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->handleAndAppendPending()Z

    move-result v0

    if-nez v0, :cond_0

    const/16 v0, 0x101

    return v0

    :cond_0
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public handleAttrValue()Z
    .locals 11

    iget v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_pendingInput:I

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->handleAttrValuePending()Z

    move-result v0

    if-nez v0, :cond_0

    return v1

    :cond_0
    iput v1, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_pendingInput:I

    :cond_1
    iget-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_attrCollector:Lcom/fasterxml/aalto/in/AttributeCollector;

    invoke-virtual {v0}, Lcom/fasterxml/aalto/in/AttributeCollector;->continueValue()[C

    move-result-object v0

    iget-object v2, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_charTypes:Lcom/fasterxml/aalto/util/XmlCharTypes;

    iget-object v2, v2, Lcom/fasterxml/aalto/util/XmlCharTypes;->ATTR_CHARS:[I

    iget-byte v3, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_elemAttrQuote:B

    :cond_2
    :goto_0
    iget v4, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v5, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-lt v4, v5, :cond_3

    return v1

    :cond_3
    iget v4, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_elemAttrPtr:I

    array-length v5, v0

    if-lt v4, v5, :cond_4

    iget-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_attrCollector:Lcom/fasterxml/aalto/in/AttributeCollector;

    invoke-virtual {v0}, Lcom/fasterxml/aalto/in/AttributeCollector;->valueBufferFull()[C

    move-result-object v0

    :cond_4
    iget v4, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    iget v5, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    array-length v6, v0

    iget v7, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_elemAttrPtr:I

    sub-int/2addr v6, v7

    add-int/2addr v5, v6

    if-ge v5, v4, :cond_5

    move v4, v5

    :cond_5
    :goto_1
    iget v5, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    if-ge v5, v4, :cond_2

    iget-object v6, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    add-int/lit8 v7, v5, 0x1

    iput v7, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {v6, v5}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v5

    and-int/lit16 v5, v5, 0xff

    aget v6, v2, v5

    if-eqz v6, :cond_11

    const/16 v4, 0xe

    const/4 v7, 0x1

    if-eq v6, v4, :cond_f

    const v4, 0xdc00

    const v8, 0xd800

    packed-switch v6, :pswitch_data_0

    goto/16 :goto_3

    :pswitch_0
    iget v6, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    iget v7, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    sub-int v9, v6, v7

    const/4 v10, 0x3

    if-ge v9, v10, :cond_7

    if-le v6, v7, :cond_6

    iget-object v0, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    add-int/lit8 v2, v7, 0x1

    iput v2, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {v0, v7}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x8

    or-int/2addr v5, v0

    iget v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    iget v2, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    if-le v0, v2, :cond_6

    iget-object v0, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x10

    or-int/2addr v5, v0

    :cond_6
    iput v5, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_pendingInput:I

    return v1

    :cond_7
    invoke-virtual {p0, v5}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->decodeUtf8_4(I)I

    move-result v5

    iget v6, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_elemAttrPtr:I

    add-int/lit8 v7, v6, 0x1

    iput v7, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_elemAttrPtr:I

    shr-int/lit8 v9, v5, 0xa

    or-int/2addr v8, v9

    int-to-char v8, v8

    aput-char v8, v0, v6

    and-int/lit16 v5, v5, 0x3ff

    or-int/2addr v5, v4

    array-length v4, v0

    if-lt v7, v4, :cond_10

    :goto_2
    iget-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_attrCollector:Lcom/fasterxml/aalto/in/AttributeCollector;

    invoke-virtual {v0}, Lcom/fasterxml/aalto/in/AttributeCollector;->valueBufferFull()[C

    move-result-object v0

    goto/16 :goto_3

    :pswitch_1
    iget v4, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    iget v6, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    sub-int v7, v4, v6

    const/4 v8, 0x2

    if-ge v7, v8, :cond_9

    if-le v4, v6, :cond_8

    iget-object v0, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    add-int/lit8 v2, v6, 0x1

    iput v2, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {v0, v6}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x8

    or-int/2addr v5, v0

    :cond_8
    iput v5, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_pendingInput:I

    return v1

    :cond_9
    invoke-virtual {p0, v5}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->decodeUtf8_3(I)I

    move-result v5

    goto :goto_3

    :pswitch_2
    iget v4, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v6, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-lt v4, v6, :cond_a

    iput v5, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_pendingInput:I

    return v1

    :cond_a
    invoke-virtual {p0, v5}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->decodeUtf8_2(I)I

    move-result v5

    goto :goto_3

    :pswitch_3
    invoke-virtual {p0, v5}, Lcom/fasterxml/aalto/in/ByteBasedScanner;->reportInvalidInitial(I)V

    :pswitch_4
    const-string v6, "\'<\' not allowed in attribute value"

    invoke-virtual {p0, v5, v6}, Lcom/fasterxml/aalto/in/XmlScanner;->throwUnexpectedChar(ILjava/lang/String;)V

    :pswitch_5
    invoke-virtual {p0}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->handleEntityInAttributeValue()I

    move-result v5

    if-gtz v5, :cond_c

    if-gez v5, :cond_b

    return v1

    :cond_b
    iget-object v6, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_elemAttrName:Lcom/fasterxml/aalto/in/PName;

    invoke-virtual {p0, v6, v1}, Lcom/fasterxml/aalto/in/XmlScanner;->reportUnexpandedEntityInAttr(Lcom/fasterxml/aalto/in/PName;Z)V

    :cond_c
    shr-int/lit8 v6, v5, 0x10

    if-eqz v6, :cond_10

    const/high16 v6, 0x10000

    sub-int/2addr v5, v6

    iget v6, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_elemAttrPtr:I

    add-int/lit8 v7, v6, 0x1

    iput v7, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_elemAttrPtr:I

    shr-int/lit8 v9, v5, 0xa

    or-int/2addr v8, v9

    int-to-char v8, v8

    aput-char v8, v0, v6

    and-int/lit16 v5, v5, 0x3ff

    or-int/2addr v5, v4

    array-length v4, v0

    if-lt v7, v4, :cond_10

    goto :goto_2

    :pswitch_6
    invoke-virtual {p0, v5}, Lcom/fasterxml/aalto/in/XmlScanner;->handleInvalidXmlChar(I)C

    :pswitch_7
    iget v4, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v5, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-lt v4, v5, :cond_d

    const/4 v0, -0x1

    iput v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_pendingInput:I

    return v1

    :cond_d
    iget-object v5, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v5, v4}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v4

    const/16 v5, 0xa

    if-ne v4, v5, :cond_e

    iget v4, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    add-int/2addr v4, v7

    iput v4, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    :cond_e
    :pswitch_8
    invoke-virtual {p0}, Lcom/fasterxml/aalto/in/ByteBasedScanner;->markLF()V

    :pswitch_9
    const/16 v5, 0x20

    goto :goto_3

    :cond_f
    if-ne v5, v3, :cond_10

    return v7

    :cond_10
    :goto_3
    iget v4, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_elemAttrPtr:I

    add-int/lit8 v6, v4, 0x1

    iput v6, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_elemAttrPtr:I

    int-to-char v5, v5

    aput-char v5, v0, v4

    goto/16 :goto_0

    :cond_11
    iget v6, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_elemAttrPtr:I

    add-int/lit8 v7, v6, 0x1

    iput v7, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_elemAttrPtr:I

    int-to-char v5, v5

    aput-char v5, v0, v6

    goto/16 :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_9
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public final handleCDataPending()I
    .locals 7

    iget v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_pendingInput:I

    const/16 v1, -0x1e

    const/16 v2, -0x1f

    const/16 v3, 0x5d

    const/4 v4, 0x0

    const/16 v5, 0x101

    if-ne v0, v1, :cond_2

    iget v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v1, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-lt v0, v1, :cond_0

    return v5

    :cond_0
    iget-object v1, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    if-eq v0, v3, :cond_1

    iget-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_textBuilder:Lcom/fasterxml/aalto/util/TextBuilder;

    invoke-virtual {v0, v3}, Lcom/fasterxml/aalto/util/TextBuilder;->append(C)V

    :goto_0
    iput v4, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_pendingInput:I

    return v4

    :cond_1
    iget v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iput v2, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_pendingInput:I

    iget v1, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-lt v0, v1, :cond_2

    return v5

    :cond_2
    :goto_1
    iget v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_pendingInput:I

    if-ne v0, v2, :cond_6

    iget v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v1, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-lt v0, v1, :cond_3

    return v5

    :cond_3
    iget-object v1, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    add-int/lit8 v6, v0, 0x1

    iput v6, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    const/16 v1, 0x3e

    if-ne v0, v1, :cond_4

    iput v4, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_pendingInput:I

    iput v4, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_state:I

    iput v5, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_nextEvent:I

    const/16 v0, 0xc

    return v0

    :cond_4
    if-eq v0, v3, :cond_5

    iget v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_textBuilder:Lcom/fasterxml/aalto/util/TextBuilder;

    const-string v1, "]]"

    invoke-virtual {v0, v1}, Lcom/fasterxml/aalto/util/TextBuilder;->append(Ljava/lang/String;)V

    goto :goto_0

    :cond_5
    iget-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_textBuilder:Lcom/fasterxml/aalto/util/TextBuilder;

    invoke-virtual {v0, v3}, Lcom/fasterxml/aalto/util/TextBuilder;->append(C)V

    goto :goto_1

    :cond_6
    invoke-direct {p0}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->handleAndAppendPending()Z

    move-result v0

    if-eqz v0, :cond_7

    goto :goto_2

    :cond_7
    move v4, v5

    :goto_2
    return v4
.end method

.method public final handleComment()I
    .locals 5

    iget v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_state:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->parseCommentContents()I

    move-result v0

    return v0

    :cond_0
    iget v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v2, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    const/16 v3, 0x101

    if-lt v0, v2, :cond_1

    return v3

    :cond_1
    iget-object v2, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    add-int/lit8 v4, v0, 0x1

    iput v4, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {v2, v0}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    iget v2, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_state:I

    if-nez v2, :cond_3

    const/16 v2, 0x2d

    if-eq v0, v2, :cond_2

    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->decodeCharForError(B)I

    move-result v0

    const-string v2, " (expected \'-\' for COMMENT)"

    invoke-virtual {p0, v0, v2}, Lcom/fasterxml/aalto/in/XmlScanner;->reportTreeUnexpChar(ILjava/lang/String;)V

    :cond_2
    iput v1, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_state:I

    iget-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_textBuilder:Lcom/fasterxml/aalto/util/TextBuilder;

    invoke-virtual {v0}, Lcom/fasterxml/aalto/util/TextBuilder;->resetWithEmpty()[C

    invoke-virtual {p0}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->parseCommentContents()I

    move-result v0

    return v0

    :cond_3
    const/4 v1, 0x3

    if-ne v2, v1, :cond_5

    const/16 v1, 0x3e

    if-eq v0, v1, :cond_4

    invoke-virtual {p0}, Lcom/fasterxml/aalto/in/XmlScanner;->reportDoubleHyphenInComments()V

    :cond_4
    const/4 v0, 0x0

    iput v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_state:I

    iput v3, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_nextEvent:I

    const/4 v0, 0x5

    return v0

    :cond_5
    invoke-virtual {p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->throwInternal()I

    move-result v0

    return v0
.end method

.method public handleCommentPending()I
    .locals 6

    iget v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v1, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    const/16 v2, 0x101

    if-lt v0, v1, :cond_0

    return v2

    :cond_0
    iget v1, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_pendingInput:I

    const/16 v3, -0x14

    const/16 v4, -0x15

    const/4 v5, 0x0

    if-ne v1, v3, :cond_2

    iget-object v1, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    const/16 v1, 0x2d

    if-eq v0, v1, :cond_1

    iput v5, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_pendingInput:I

    iget-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_textBuilder:Lcom/fasterxml/aalto/util/TextBuilder;

    const-string v1, "-"

    invoke-virtual {v0, v1}, Lcom/fasterxml/aalto/util/TextBuilder;->append(Ljava/lang/String;)V

    return v5

    :cond_1
    iget v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iput v4, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_pendingInput:I

    iget v1, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-lt v0, v1, :cond_2

    return v2

    :cond_2
    iget v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_pendingInput:I

    if-ne v0, v4, :cond_4

    iput v5, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_pendingInput:I

    iget-object v0, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    iget v1, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    add-int/lit8 v3, v1, 0x1

    iput v3, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    const/16 v1, 0x3e

    if-eq v0, v1, :cond_3

    invoke-virtual {p0}, Lcom/fasterxml/aalto/in/XmlScanner;->reportDoubleHyphenInComments()V

    :cond_3
    iput v5, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_state:I

    iput v2, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_nextEvent:I

    const/4 v0, 0x5

    return v0

    :cond_4
    invoke-direct {p0}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->handleAndAppendPending()Z

    move-result v0

    if-eqz v0, :cond_5

    move v2, v5

    :cond_5
    return v2
.end method

.method public final handleDTDInternalSubset(Z)Z
    .locals 9

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    iget-object p1, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_textBuilder:Lcom/fasterxml/aalto/util/TextBuilder;

    invoke-virtual {p1}, Lcom/fasterxml/aalto/util/TextBuilder;->resetWithEmpty()[C

    move-result-object p1

    iput-byte v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_elemAttrQuote:B

    iput-boolean v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_inDtdDeclaration:Z

    move v1, v0

    goto :goto_0

    :cond_0
    iget p1, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_pendingInput:I

    if-eqz p1, :cond_1

    invoke-direct {p0}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->handleAndAppendPending()Z

    move-result p1

    if-nez p1, :cond_1

    return v0

    :cond_1
    iget-object p1, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_textBuilder:Lcom/fasterxml/aalto/util/TextBuilder;

    invoke-virtual {p1}, Lcom/fasterxml/aalto/util/TextBuilder;->getBufferWithoutReset()[C

    move-result-object p1

    iget-object v1, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_textBuilder:Lcom/fasterxml/aalto/util/TextBuilder;

    invoke-virtual {v1}, Lcom/fasterxml/aalto/util/TextBuilder;->getCurrentLength()I

    move-result v1

    :goto_0
    iget-object v2, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_charTypes:Lcom/fasterxml/aalto/util/XmlCharTypes;

    iget-object v2, v2, Lcom/fasterxml/aalto/util/XmlCharTypes;->DTD_CHARS:[I

    iget-object v3, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    :cond_2
    :goto_1
    iget v4, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v5, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-lt v4, v5, :cond_3

    goto/16 :goto_6

    :cond_3
    array-length v4, p1

    if-lt v1, v4, :cond_4

    iget-object p1, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_textBuilder:Lcom/fasterxml/aalto/util/TextBuilder;

    invoke-virtual {p1}, Lcom/fasterxml/aalto/util/TextBuilder;->finishCurrentSegment()[C

    move-result-object p1

    move v1, v0

    :cond_4
    iget v4, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    iget v5, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    array-length v6, p1

    sub-int/2addr v6, v1

    add-int/2addr v5, v6

    if-ge v5, v4, :cond_5

    move v4, v5

    :cond_5
    :goto_2
    iget v5, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    if-ge v5, v4, :cond_2

    add-int/lit8 v6, v5, 0x1

    iput v6, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {v3, v5}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v5

    and-int/lit16 v5, v5, 0xff

    aget v6, v2, v5

    if-eqz v6, :cond_f

    const/4 v4, 0x1

    packed-switch v6, :pswitch_data_0

    goto/16 :goto_7

    :pswitch_0
    iget-boolean v6, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_inDtdDeclaration:Z

    if-nez v6, :cond_e

    iget-byte v6, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_elemAttrQuote:B

    if-nez v6, :cond_e

    iget-object p1, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_textBuilder:Lcom/fasterxml/aalto/util/TextBuilder;

    invoke-virtual {p1, v1}, Lcom/fasterxml/aalto/util/TextBuilder;->setCurrentLength(I)V

    return v4

    :pswitch_1
    iget-byte v4, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_elemAttrQuote:B

    if-nez v4, :cond_e

    iput-boolean v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_inDtdDeclaration:Z

    goto/16 :goto_7

    :pswitch_2
    iget-boolean v6, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_inDtdDeclaration:Z

    if-nez v6, :cond_e

    iput-boolean v4, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_inDtdDeclaration:Z

    goto/16 :goto_7

    :pswitch_3
    iget v4, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    iget v6, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    sub-int v7, v4, v6

    const/4 v8, 0x3

    if-ge v7, v8, :cond_6

    if-le v4, v6, :cond_9

    iget-object p1, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    add-int/lit8 v2, v6, 0x1

    iput v2, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {p1, v6}, Ljava/nio/ByteBuffer;->get(I)B

    move-result p1

    and-int/lit16 p1, p1, 0xff

    shl-int/lit8 p1, p1, 0x8

    or-int/2addr v5, p1

    iget p1, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    iget v2, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    if-le p1, v2, :cond_9

    iget-object p1, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {p1, v2}, Ljava/nio/ByteBuffer;->get(I)B

    move-result p1

    and-int/lit16 p1, p1, 0xff

    shl-int/lit8 p1, p1, 0x10

    :goto_3
    or-int/2addr v5, p1

    goto :goto_5

    :cond_6
    invoke-virtual {p0, v5}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->decodeUtf8_4(I)I

    move-result v4

    add-int/lit8 v5, v1, 0x1

    shr-int/lit8 v6, v4, 0xa

    const v7, 0xd800

    or-int/2addr v6, v7

    int-to-char v6, v6

    aput-char v6, p1, v1

    array-length v1, p1

    if-lt v5, v1, :cond_7

    iget-object p1, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_textBuilder:Lcom/fasterxml/aalto/util/TextBuilder;

    invoke-virtual {p1}, Lcom/fasterxml/aalto/util/TextBuilder;->finishCurrentSegment()[C

    move-result-object p1

    move v1, v0

    goto :goto_4

    :cond_7
    move v1, v5

    :goto_4
    and-int/lit16 v4, v4, 0x3ff

    const v5, 0xdc00

    or-int/2addr v5, v4

    goto :goto_7

    :pswitch_4
    iget v4, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    iget v6, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    sub-int v7, v4, v6

    const/4 v8, 0x2

    if-ge v7, v8, :cond_8

    if-le v4, v6, :cond_9

    iget-object p1, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    add-int/lit8 v2, v6, 0x1

    iput v2, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {p1, v6}, Ljava/nio/ByteBuffer;->get(I)B

    move-result p1

    and-int/lit16 p1, p1, 0xff

    shl-int/lit8 p1, p1, 0x8

    goto :goto_3

    :cond_8
    invoke-virtual {p0, v5}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->decodeUtf8_3(I)I

    move-result v5

    goto :goto_7

    :pswitch_5
    iget v4, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v6, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-lt v4, v6, :cond_a

    :cond_9
    :goto_5
    iput v5, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_pendingInput:I

    goto :goto_6

    :cond_a
    invoke-virtual {p0, v5}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->decodeUtf8_2(I)I

    move-result v5

    goto :goto_7

    :pswitch_6
    invoke-virtual {p0, v5}, Lcom/fasterxml/aalto/in/ByteBasedScanner;->reportInvalidInitial(I)V

    :pswitch_7
    iget-byte v4, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_elemAttrQuote:B

    if-nez v4, :cond_b

    int-to-byte v4, v5

    iput-byte v4, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_elemAttrQuote:B

    goto :goto_7

    :cond_b
    if-ne v4, v5, :cond_e

    iput-byte v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_elemAttrQuote:B

    goto :goto_7

    :pswitch_8
    invoke-virtual {p0}, Lcom/fasterxml/aalto/in/ByteBasedScanner;->markLF()V

    goto :goto_7

    :pswitch_9
    invoke-virtual {p0, v5}, Lcom/fasterxml/aalto/in/XmlScanner;->handleInvalidXmlChar(I)C

    :pswitch_a
    iget v5, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v6, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-lt v5, v6, :cond_c

    const/4 p1, -0x1

    iput p1, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_pendingInput:I

    :goto_6
    iget-object p1, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_textBuilder:Lcom/fasterxml/aalto/util/TextBuilder;

    invoke-virtual {p1, v1}, Lcom/fasterxml/aalto/util/TextBuilder;->setCurrentLength(I)V

    return v0

    :cond_c
    invoke-virtual {v3, v5}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v5

    const/16 v6, 0xa

    if-ne v5, v6, :cond_d

    iget v5, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    add-int/2addr v5, v4

    iput v5, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    :cond_d
    invoke-virtual {p0}, Lcom/fasterxml/aalto/in/ByteBasedScanner;->markLF()V

    move v5, v6

    :cond_e
    :goto_7
    add-int/lit8 v4, v1, 0x1

    int-to-char v5, v5

    aput-char v5, p1, v1

    move v1, v4

    goto/16 :goto_1

    :cond_f
    add-int/lit8 v6, v1, 0x1

    int-to-char v5, v5

    aput-char v5, p1, v1

    move v1, v6

    goto/16 :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_9
        :pswitch_a
        :pswitch_8
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_7
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public handleDecEntityInCharacters(I)I
    .locals 6

    iget-object v0, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    add-int/lit8 v1, p1, 0x1

    invoke-virtual {v0, p1}, Ljava/nio/ByteBuffer;->get(I)B

    move-result p1

    iget v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    const/4 v2, 0x0

    move v3, v2

    :goto_0
    const/16 v4, 0x39

    if-gt p1, v4, :cond_0

    const/16 v4, 0x30

    if-ge p1, v4, :cond_1

    :cond_0
    invoke-virtual {p0, p1}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->decodeCharForError(B)I

    move-result v4

    const-string v5, " expected a digit (0 - 9) for character entity"

    invoke-virtual {p0, v4, v5}, Lcom/fasterxml/aalto/in/XmlScanner;->throwUnexpectedChar(ILjava/lang/String;)V

    :cond_1
    mul-int/lit8 v3, v3, 0xa

    add-int/lit8 p1, p1, -0x30

    add-int/2addr v3, p1

    const p1, 0x10ffff

    if-le v3, p1, :cond_2

    invoke-virtual {p0}, Lcom/fasterxml/aalto/in/XmlScanner;->reportEntityOverflow()V

    :cond_2
    if-lt v1, v0, :cond_3

    return v2

    :cond_3
    iget-object p1, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    add-int/lit8 v4, v1, 0x1

    invoke-virtual {p1, v1}, Ljava/nio/ByteBuffer;->get(I)B

    move-result p1

    const/16 v1, 0x3b

    if-ne p1, v1, :cond_4

    iput v4, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {p0, v3}, Lcom/fasterxml/aalto/in/XmlScanner;->verifyXmlChar(I)V

    return v3

    :cond_4
    move v1, v4

    goto :goto_0
.end method

.method public handleEntityInAttributeValue()I
    .locals 4

    iget v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v1, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    const/4 v2, -0x1

    if-lt v0, v1, :cond_0

    const/16 v0, -0x3c

    :goto_0
    iput v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_pendingInput:I

    return v2

    :cond_0
    iget-object v1, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    add-int/lit8 v3, v0, 0x1

    iput v3, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    const/16 v1, 0x23

    if-ne v0, v1, :cond_5

    const/16 v0, -0x3d

    iput v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_pendingInput:I

    iget v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v1, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-lt v0, v1, :cond_1

    return v2

    :cond_1
    iget-object v1, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    const/16 v1, 0x78

    const/4 v3, 0x1

    if-ne v0, v1, :cond_3

    const/16 v0, -0x3e

    iput v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_pendingInput:I

    iget v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    add-int/2addr v0, v3

    iput v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v1, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-lt v0, v1, :cond_2

    return v2

    :cond_2
    invoke-direct {p0, v3}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->handleHexEntityInAttribute(Z)I

    move-result v0

    goto :goto_1

    :cond_3
    invoke-direct {p0, v3}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->handleDecEntityInAttribute(Z)I

    move-result v0

    :goto_1
    if-nez v0, :cond_4

    return v2

    :cond_4
    return v0

    :cond_5
    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->parseNewEntityName(B)Lcom/fasterxml/aalto/in/PName;

    move-result-object v0

    if-nez v0, :cond_6

    const/16 v0, -0x3f

    goto :goto_0

    :cond_6
    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->decodeGeneralEntity(Lcom/fasterxml/aalto/in/PName;)I

    move-result v1

    if-eqz v1, :cond_7

    return v1

    :cond_7
    iput-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_tokenName:Lcom/fasterxml/aalto/in/PName;

    const/4 v0, 0x0

    return v0
.end method

.method public handleEntityInCharacters()I
    .locals 8

    iget v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    add-int/lit8 v1, v0, 0x3

    iget v2, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-gt v1, v2, :cond_6

    iget-object v1, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    add-int/lit8 v2, v0, 0x1

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    const/16 v1, 0x23

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    const/16 v1, 0x78

    if-ne v0, v1, :cond_0

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {p0, v2}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->handleHexEntityInCharacters(I)I

    move-result v0

    return v0

    :cond_0
    invoke-virtual {p0, v2}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->handleDecEntityInCharacters(I)I

    move-result v0

    return v0

    :cond_1
    const/16 v1, 0x61

    const/16 v3, 0x27

    const/16 v4, 0x6f

    const/16 v5, 0x3b

    if-ne v0, v1, :cond_3

    iget-object v0, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    add-int/lit8 v1, v2, 0x1

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    const/16 v2, 0x6d

    const/16 v6, 0x70

    if-ne v0, v2, :cond_2

    add-int/lit8 v0, v1, 0x1

    iget v2, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    if-ge v0, v2, :cond_6

    iget-object v2, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v2, v1}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v2

    if-ne v2, v6, :cond_6

    iget-object v2, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v2, v0}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    if-ne v0, v5, :cond_6

    add-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    const/16 v0, 0x26

    return v0

    :cond_2
    if-ne v0, v6, :cond_6

    add-int/lit8 v0, v1, 0x2

    iget v2, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    if-ge v0, v2, :cond_6

    iget-object v2, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v2, v1}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v2

    if-ne v2, v4, :cond_6

    iget-object v2, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    add-int/lit8 v4, v1, 0x1

    invoke-virtual {v2, v4}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v2

    const/16 v4, 0x73

    if-ne v2, v4, :cond_6

    iget-object v2, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v2, v0}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    if-ne v0, v5, :cond_6

    add-int/lit8 v1, v1, 0x3

    iput v1, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    return v3

    :cond_3
    const/16 v1, 0x67

    const/16 v6, 0x74

    if-ne v0, v1, :cond_4

    iget-object v0, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    if-ne v0, v6, :cond_6

    iget-object v0, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    add-int/lit8 v1, v2, 0x1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    if-ne v0, v5, :cond_6

    add-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    const/16 v0, 0x3e

    return v0

    :cond_4
    const/16 v1, 0x6c

    if-ne v0, v1, :cond_5

    iget-object v0, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    if-ne v0, v6, :cond_6

    iget-object v0, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    add-int/lit8 v1, v2, 0x1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    if-ne v0, v5, :cond_6

    add-int/lit8 v2, v2, 0x2

    iput v2, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    const/16 v0, 0x3c

    return v0

    :cond_5
    const/16 v1, 0x71

    if-ne v0, v1, :cond_6

    add-int/lit8 v0, v2, 0x3

    iget v1, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    if-ge v0, v1, :cond_6

    iget-object v1, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v1

    const/16 v7, 0x75

    if-ne v1, v7, :cond_6

    iget-object v1, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    add-int/lit8 v7, v2, 0x1

    invoke-virtual {v1, v7}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v1

    if-ne v1, v4, :cond_6

    iget-object v1, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    add-int/lit8 v4, v2, 0x2

    invoke-virtual {v1, v4}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v1

    if-ne v1, v6, :cond_6

    iget-object v1, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    if-ne v0, v5, :cond_6

    add-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    return v3

    :cond_6
    const/4 v0, 0x0

    return v0
.end method

.method public handleEntityStartingToken()I
    .locals 3

    iget-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_textBuilder:Lcom/fasterxml/aalto/util/TextBuilder;

    invoke-virtual {v0}, Lcom/fasterxml/aalto/util/TextBuilder;->resetWithEmpty()[C

    iget-object v0, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    iget v1, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    const/16 v1, 0x23

    const/16 v2, 0x101

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_textBuilder:Lcom/fasterxml/aalto/util/TextBuilder;

    invoke-virtual {v0}, Lcom/fasterxml/aalto/util/TextBuilder;->resetWithEmpty()[C

    const/4 v0, 0x5

    iput v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_state:I

    const/16 v0, -0x46

    iput v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_pendingInput:I

    iget v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v1, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-lt v0, v1, :cond_0

    return v2

    :cond_0
    invoke-virtual {p0}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->handleNumericEntityStartingToken()I

    move-result v0

    return v0

    :cond_1
    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->parseNewEntityName(B)Lcom/fasterxml/aalto/in/PName;

    move-result-object v0

    if-nez v0, :cond_2

    const/4 v0, 0x6

    iput v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_state:I

    return v2

    :cond_2
    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->decodeGeneralEntity(Lcom/fasterxml/aalto/in/PName;)I

    move-result v1

    if-nez v1, :cond_3

    iput-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_tokenName:Lcom/fasterxml/aalto/in/PName;

    const/16 v0, 0x9

    iput v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_currToken:I

    iput v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_nextEvent:I

    return v0

    :cond_3
    iget-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_textBuilder:Lcom/fasterxml/aalto/util/TextBuilder;

    int-to-char v1, v1

    invoke-virtual {v0, v1}, Lcom/fasterxml/aalto/util/TextBuilder;->resetWithChar(C)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_nextEvent:I

    const/4 v0, 0x4

    iput v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_currToken:I

    iget-boolean v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_cfgLazyParsing:Z

    if-eqz v0, :cond_4

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_tokenIncomplete:Z

    goto :goto_0

    :cond_4
    invoke-virtual {p0}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->finishCharacters()V

    :goto_0
    iget v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_currToken:I

    return v0
.end method

.method public handleHexEntityInCharacters(I)I
    .locals 6

    iget-object v0, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    add-int/lit8 v1, p1, 0x1

    invoke-virtual {v0, p1}, Ljava/nio/ByteBuffer;->get(I)B

    move-result p1

    iget v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    const/4 v2, 0x0

    move v3, v2

    :goto_0
    const/16 v4, 0x39

    if-gt p1, v4, :cond_0

    const/16 v4, 0x30

    if-lt p1, v4, :cond_0

    add-int/lit8 p1, p1, -0x30

    goto :goto_2

    :cond_0
    const/16 v4, 0x46

    if-gt p1, v4, :cond_1

    const/16 v4, 0x41

    if-lt p1, v4, :cond_1

    add-int/lit8 p1, p1, -0x41

    :goto_1
    add-int/lit8 p1, p1, 0xa

    goto :goto_2

    :cond_1
    const/16 v4, 0x66

    if-gt p1, v4, :cond_2

    const/16 v4, 0x61

    if-lt p1, v4, :cond_2

    add-int/lit8 p1, p1, -0x61

    goto :goto_1

    :cond_2
    invoke-virtual {p0, p1}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->decodeCharForError(B)I

    move-result v4

    const-string v5, " expected a hex digit (0-9a-fA-F) for character entity"

    invoke-virtual {p0, v4, v5}, Lcom/fasterxml/aalto/in/XmlScanner;->throwUnexpectedChar(ILjava/lang/String;)V

    :goto_2
    shl-int/lit8 v3, v3, 0x4

    add-int/2addr v3, p1

    const p1, 0x10ffff

    if-le v3, p1, :cond_3

    invoke-virtual {p0}, Lcom/fasterxml/aalto/in/XmlScanner;->reportEntityOverflow()V

    :cond_3
    if-lt v1, v0, :cond_4

    return v2

    :cond_4
    iget-object p1, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    add-int/lit8 v4, v1, 0x1

    invoke-virtual {p1, v1}, Ljava/nio/ByteBuffer;->get(I)B

    move-result p1

    const/16 v1, 0x3b

    if-ne p1, v1, :cond_5

    iput v4, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {p0, v3}, Lcom/fasterxml/aalto/in/XmlScanner;->verifyXmlChar(I)V

    return v3

    :cond_5
    move v1, v4

    goto :goto_0
.end method

.method public handleNamedEntityStartingToken()I
    .locals 2

    invoke-virtual {p0}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->parseEntityName()Lcom/fasterxml/aalto/in/PName;

    move-result-object v0

    if-nez v0, :cond_0

    iget v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_nextEvent:I

    return v0

    :cond_0
    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->decodeGeneralEntity(Lcom/fasterxml/aalto/in/PName;)I

    move-result v1

    if-nez v1, :cond_1

    iput-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_tokenName:Lcom/fasterxml/aalto/in/PName;

    const/16 v0, 0x9

    iput v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_currToken:I

    return v0

    :cond_1
    iget-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_textBuilder:Lcom/fasterxml/aalto/util/TextBuilder;

    int-to-char v1, v1

    invoke-virtual {v0, v1}, Lcom/fasterxml/aalto/util/TextBuilder;->resetWithChar(C)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_nextEvent:I

    const/4 v0, 0x4

    iput v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_currToken:I

    iget-boolean v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_cfgLazyParsing:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_tokenIncomplete:Z

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->finishCharacters()V

    :goto_0
    iget v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_currToken:I

    return v0
.end method

.method public handleNsDecl()Z
    .locals 11

    iget-object v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_charTypes:Lcom/fasterxml/aalto/util/XmlCharTypes;

    iget-object v0, v0, Lcom/fasterxml/aalto/util/XmlCharTypes;->ATTR_CHARS:[I

    iget-object v1, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_nameBuffer:[C

    iget-byte v2, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_elemAttrQuote:B

    iget v3, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_pendingInput:I

    const/4 v4, 0x0

    if-eqz v3, :cond_1

    invoke-direct {p0}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->handleNsValuePending()Z

    move-result v3

    if-nez v3, :cond_0

    return v4

    :cond_0
    iput v4, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_pendingInput:I

    :cond_1
    :goto_0
    iget v3, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v5, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-lt v3, v5, :cond_2

    return v4

    :cond_2
    iget v3, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_elemNsPtr:I

    array-length v5, v1

    if-lt v3, v5, :cond_3

    array-length v3, v1

    invoke-static {v1, v3}, Lcom/fasterxml/aalto/util/DataUtil;->growArrayBy([CI)[C

    move-result-object v1

    iput-object v1, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_nameBuffer:[C

    :cond_3
    iget v3, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    iget v5, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    array-length v6, v1

    iget v7, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_elemNsPtr:I

    sub-int/2addr v6, v7

    add-int/2addr v5, v6

    if-ge v5, v3, :cond_4

    move v3, v5

    :cond_4
    :goto_1
    iget v5, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    if-ge v5, v3, :cond_1

    iget-object v6, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    add-int/lit8 v7, v5, 0x1

    iput v7, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {v6, v5}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v5

    and-int/lit16 v5, v5, 0xff

    aget v6, v0, v5

    if-eqz v6, :cond_11

    const/16 v3, 0xe

    const/4 v7, 0x1

    if-eq v6, v3, :cond_e

    const v3, 0xdc00

    const v8, 0xd800

    packed-switch v6, :pswitch_data_0

    goto/16 :goto_3

    :pswitch_0
    iget v6, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    iget v7, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    sub-int v9, v6, v7

    const/4 v10, 0x3

    if-ge v9, v10, :cond_6

    if-le v6, v7, :cond_5

    iget-object v0, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    add-int/lit8 v1, v7, 0x1

    iput v1, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {v0, v7}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x8

    or-int/2addr v5, v0

    iget v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    iget v1, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    if-le v0, v1, :cond_5

    iget-object v0, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x10

    or-int/2addr v5, v0

    :cond_5
    iput v5, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_pendingInput:I

    return v4

    :cond_6
    invoke-virtual {p0, v5}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->decodeUtf8_4(I)I

    move-result v5

    iget v6, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_elemNsPtr:I

    add-int/lit8 v7, v6, 0x1

    iput v7, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_elemNsPtr:I

    shr-int/lit8 v9, v5, 0xa

    or-int/2addr v8, v9

    int-to-char v8, v8

    aput-char v8, v1, v6

    and-int/lit16 v5, v5, 0x3ff

    or-int/2addr v5, v3

    array-length v3, v1

    if-lt v7, v3, :cond_10

    array-length v3, v1

    invoke-static {v1, v3}, Lcom/fasterxml/aalto/util/DataUtil;->growArrayBy([CI)[C

    move-result-object v1

    iput-object v1, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_nameBuffer:[C

    goto/16 :goto_3

    :pswitch_1
    iget v3, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    iget v6, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    sub-int v7, v3, v6

    const/4 v8, 0x2

    if-ge v7, v8, :cond_8

    if-le v3, v6, :cond_7

    iget-object v0, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    add-int/lit8 v1, v6, 0x1

    iput v1, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {v0, v6}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x8

    or-int/2addr v5, v0

    :cond_7
    iput v5, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_pendingInput:I

    return v4

    :cond_8
    invoke-virtual {p0, v5}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->decodeUtf8_3(I)I

    move-result v5

    goto/16 :goto_3

    :pswitch_2
    iget v3, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v6, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-lt v3, v6, :cond_9

    iput v5, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_pendingInput:I

    return v4

    :cond_9
    invoke-virtual {p0, v5}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->decodeUtf8_2(I)I

    move-result v5

    goto/16 :goto_3

    :pswitch_3
    invoke-virtual {p0, v5}, Lcom/fasterxml/aalto/in/ByteBasedScanner;->reportInvalidInitial(I)V

    :pswitch_4
    const-string v6, "\'<\' not allowed in attribute value"

    invoke-virtual {p0, v5, v6}, Lcom/fasterxml/aalto/in/XmlScanner;->throwUnexpectedChar(ILjava/lang/String;)V

    :pswitch_5
    invoke-virtual {p0}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->handleEntityInAttributeValue()I

    move-result v5

    if-gtz v5, :cond_b

    if-gez v5, :cond_a

    return v4

    :cond_a
    iget-object v6, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_elemAttrName:Lcom/fasterxml/aalto/in/PName;

    invoke-virtual {p0, v6, v7}, Lcom/fasterxml/aalto/in/XmlScanner;->reportUnexpandedEntityInAttr(Lcom/fasterxml/aalto/in/PName;Z)V

    :cond_b
    shr-int/lit8 v6, v5, 0x10

    if-eqz v6, :cond_10

    const/high16 v6, 0x10000

    sub-int/2addr v5, v6

    iget v6, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_elemNsPtr:I

    add-int/lit8 v7, v6, 0x1

    iput v7, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_elemNsPtr:I

    shr-int/lit8 v9, v5, 0xa

    or-int/2addr v8, v9

    int-to-char v8, v8

    aput-char v8, v1, v6

    and-int/lit16 v5, v5, 0x3ff

    or-int/2addr v5, v3

    array-length v3, v1

    if-lt v7, v3, :cond_10

    array-length v3, v1

    invoke-static {v1, v3}, Lcom/fasterxml/aalto/util/DataUtil;->growArrayBy([CI)[C

    move-result-object v1

    iput-object v1, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_nameBuffer:[C

    goto :goto_3

    :pswitch_6
    invoke-virtual {p0, v5}, Lcom/fasterxml/aalto/in/XmlScanner;->handleInvalidXmlChar(I)C

    :pswitch_7
    iget v3, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v5, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-lt v3, v5, :cond_c

    const/4 v0, -0x1

    iput v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_pendingInput:I

    return v4

    :cond_c
    iget-object v5, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v5, v3}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v3

    const/16 v5, 0xa

    if-ne v3, v5, :cond_d

    iget v3, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    add-int/2addr v3, v7

    iput v3, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    :cond_d
    :pswitch_8
    invoke-virtual {p0}, Lcom/fasterxml/aalto/in/ByteBasedScanner;->markLF()V

    :pswitch_9
    const/16 v5, 0x20

    goto :goto_3

    :cond_e
    if-ne v5, v2, :cond_10

    iget v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_elemNsPtr:I

    if-nez v0, :cond_f

    iget-object v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_elemAttrName:Lcom/fasterxml/aalto/in/PName;

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Lcom/fasterxml/aalto/in/XmlScanner;->bindNs(Lcom/fasterxml/aalto/in/PName;Ljava/lang/String;)V

    goto :goto_2

    :cond_f
    iget-object v2, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_config:Lcom/fasterxml/aalto/in/ReaderConfig;

    invoke-virtual {v2, v1, v0}, Lcom/fasterxml/aalto/in/ReaderConfig;->canonicalizeURI([CI)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_elemAttrName:Lcom/fasterxml/aalto/in/PName;

    invoke-virtual {p0, v1, v0}, Lcom/fasterxml/aalto/in/XmlScanner;->bindNs(Lcom/fasterxml/aalto/in/PName;Ljava/lang/String;)V

    :goto_2
    return v7

    :cond_10
    :goto_3
    iget v3, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_elemNsPtr:I

    add-int/lit8 v6, v3, 0x1

    iput v6, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_elemNsPtr:I

    int-to-char v5, v5

    aput-char v5, v1, v3

    goto/16 :goto_0

    :cond_11
    iget v6, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_elemNsPtr:I

    add-int/lit8 v7, v6, 0x1

    iput v7, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_elemNsPtr:I

    int-to-char v5, v5

    aput-char v5, v1, v6

    goto/16 :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_9
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public handleNumericEntityStartingToken()I
    .locals 6

    iget v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_pendingInput:I

    const/16 v1, -0x46

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/16 v4, -0x49

    const/16 v5, 0x101

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    iget v1, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    iput v3, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_entityValue:I

    const/16 v1, 0x78

    if-ne v0, v1, :cond_0

    iput v4, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_pendingInput:I

    iget v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    add-int/2addr v0, v2

    iput v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v1, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-lt v0, v1, :cond_1

    return v5

    :cond_0
    const/16 v0, -0x48

    iput v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_pendingInput:I

    :cond_1
    iget v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_pendingInput:I

    if-ne v0, v4, :cond_2

    invoke-virtual {p0}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->decodeHexEntity()Z

    move-result v0

    if-nez v0, :cond_3

    return v5

    :cond_2
    invoke-virtual {p0}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->decodeDecEntity()Z

    move-result v0

    if-nez v0, :cond_3

    return v5

    :cond_3
    iget v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_entityValue:I

    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->verifyAndAppendEntityCharacter(I)V

    const/4 v0, 0x4

    iput v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_currToken:I

    iget-boolean v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_cfgLazyParsing:Z

    if-eqz v0, :cond_4

    iput-boolean v2, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_tokenIncomplete:Z

    goto :goto_0

    :cond_4
    invoke-virtual {p0}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->finishCharacters()V

    :goto_0
    iput v3, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_pendingInput:I

    iget v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_currToken:I

    return v0
.end method

.method public handlePI()I
    .locals 10

    iget v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_state:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->parsePIData()I

    move-result v0

    return v0

    :cond_0
    :goto_0
    iget v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v2, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    const/16 v3, 0x101

    if-lt v0, v2, :cond_1

    return v3

    :cond_1
    iget v2, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_state:I

    const/4 v4, 0x4

    const/16 v5, 0x3e

    const/4 v6, 0x3

    const/4 v7, 0x2

    const/4 v8, 0x1

    if-eqz v2, :cond_5

    if-eq v2, v8, :cond_7

    if-eq v2, v7, :cond_b

    if-eq v2, v6, :cond_4

    if-eq v2, v4, :cond_2

    invoke-virtual {p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->throwInternal()I

    move-result v0

    return v0

    :cond_2
    invoke-virtual {p0}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->parsePName()Lcom/fasterxml/aalto/in/PName;

    move-result-object v0

    iput-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_tokenName:Lcom/fasterxml/aalto/in/PName;

    if-nez v0, :cond_3

    return v3

    :cond_3
    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->checkPITargetName(Lcom/fasterxml/aalto/in/PName;)V

    iput v8, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_state:I

    goto :goto_0

    :cond_4
    iget-object v1, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    add-int/lit8 v2, v0, 0x1

    iput v2, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    if-eq v0, v5, :cond_f

    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->decodeCharForError(B)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/in/XmlScanner;->reportMissingPISpace(I)V

    goto/16 :goto_4

    :cond_5
    iget-object v2, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    add-int/lit8 v9, v0, 0x1

    iput v9, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {v2, v0}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->parseNewName(B)Lcom/fasterxml/aalto/in/PName;

    move-result-object v0

    iput-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_tokenName:Lcom/fasterxml/aalto/in/PName;

    if-nez v0, :cond_6

    iput v4, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_state:I

    return v3

    :cond_6
    iput v8, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_state:I

    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->checkPITargetName(Lcom/fasterxml/aalto/in/PName;)V

    iget v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v2, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-lt v0, v2, :cond_7

    return v3

    :cond_7
    iget-object v0, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    iget v2, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    add-int/lit8 v4, v2, 0x1

    iput v4, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    const/16 v2, 0x3f

    if-ne v0, v2, :cond_9

    iget v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v2, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-ge v0, v2, :cond_8

    iget-object v2, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v2, v0}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    if-ne v0, v5, :cond_8

    iget v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    add-int/2addr v0, v8

    :goto_1
    iput v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    goto :goto_4

    :cond_8
    iput v6, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_state:I

    goto/16 :goto_0

    :cond_9
    const/16 v4, 0x20

    if-eq v0, v4, :cond_d

    const/16 v4, 0xd

    if-eq v0, v4, :cond_d

    const/16 v4, 0xa

    if-eq v0, v4, :cond_d

    const/16 v4, 0x9

    if-ne v0, v4, :cond_a

    goto :goto_3

    :cond_a
    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->decodeCharForError(B)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/in/XmlScanner;->reportMissingPISpace(I)V

    :cond_b
    invoke-virtual {p0}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->asyncSkipSpace()Z

    move-result v0

    if-nez v0, :cond_c

    return v3

    :cond_c
    iput v1, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_state:I

    iget-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_textBuilder:Lcom/fasterxml/aalto/util/TextBuilder;

    invoke-virtual {v0}, Lcom/fasterxml/aalto/util/TextBuilder;->resetWithEmpty()[C

    :goto_2
    invoke-virtual {p0}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->parsePIData()I

    move-result v0

    return v0

    :cond_d
    :goto_3
    invoke-virtual {p0}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->asyncSkipSpace()Z

    move-result v0

    if-nez v0, :cond_e

    iput v7, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_state:I

    return v3

    :cond_e
    iget-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_textBuilder:Lcom/fasterxml/aalto/util/TextBuilder;

    invoke-virtual {v0}, Lcom/fasterxml/aalto/util/TextBuilder;->resetWithEmpty()[C

    iget v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    add-int/lit8 v4, v0, 0x1

    iget v9, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-ge v4, v9, :cond_10

    iget-object v4, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v4, v0}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    if-ne v0, v2, :cond_10

    iget-object v0, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    iget v2, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    add-int/2addr v2, v8

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    if-ne v0, v5, :cond_10

    iget v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    add-int/2addr v0, v7

    goto :goto_1

    :cond_f
    :goto_4
    const/4 v0, 0x0

    iput v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_state:I

    iput v3, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_nextEvent:I

    return v6

    :cond_10
    iput v1, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_state:I

    goto :goto_2
.end method

.method public handlePIPending()I
    .locals 4

    iget v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_pendingInput:I

    const/16 v1, -0xf

    const/16 v2, 0x101

    const/4 v3, 0x0

    if-ne v0, v1, :cond_2

    iget v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v1, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-lt v0, v1, :cond_0

    return v2

    :cond_0
    iget-object v1, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    iput v3, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_pendingInput:I

    const/16 v1, 0x3e

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_textBuilder:Lcom/fasterxml/aalto/util/TextBuilder;

    const/16 v1, 0x3f

    invoke-virtual {v0, v1}, Lcom/fasterxml/aalto/util/TextBuilder;->append(C)V

    return v3

    :cond_1
    iget v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iput v3, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_state:I

    iput v2, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_nextEvent:I

    const/4 v0, 0x3

    return v0

    :cond_2
    invoke-direct {p0}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->handleAndAppendPending()Z

    move-result v0

    if-eqz v0, :cond_3

    move v2, v3

    :cond_3
    return v2
.end method

.method public final handlePartialCR()Z
    .locals 3

    iget v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_pendingInput:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->throwInternal()I

    :cond_0
    iget v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v1, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    const/4 v2, 0x0

    if-lt v0, v1, :cond_1

    return v2

    :cond_1
    iput v2, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_pendingInput:I

    iget-object v1, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    const/16 v1, 0xa

    const/4 v2, 0x1

    if-ne v0, v1, :cond_2

    iget v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    add-int/2addr v0, v2

    iput v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    :cond_2
    iget v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_currRow:I

    add-int/2addr v0, v2

    iput v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_currRow:I

    iget v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iput v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_rowStartOffset:I

    return v2
.end method

.method public handleStartElement()I
    .locals 15

    :goto_0
    iget v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v1, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    const/16 v2, 0x101

    if-lt v0, v1, :cond_0

    return v2

    :cond_0
    iget v1, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_state:I

    const/4 v3, -0x1

    const/16 v4, 0xd

    const/4 v5, 0x0

    const/16 v6, 0x2f

    const/4 v7, 0x3

    const/4 v8, 0x2

    const/4 v9, 0x5

    const/16 v10, 0x3e

    const/16 v11, 0x9

    const/16 v12, 0xa

    const/16 v13, 0x20

    const/4 v14, 0x1

    packed-switch v1, :pswitch_data_0

    invoke-virtual {p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->throwInternal()I

    goto :goto_0

    :pswitch_0
    iget-object v1, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    add-int/lit8 v2, v0, 0x1

    iput v2, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    if-eq v0, v10, :cond_1

    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->decodeCharForError(B)I

    move-result v0

    const-string v1, " expected \'>\'"

    invoke-virtual {p0, v0, v1}, Lcom/fasterxml/aalto/in/XmlScanner;->throwUnexpectedChar(ILjava/lang/String;)V

    :cond_1
    invoke-direct {p0, v14}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->finishStartElement(Z)I

    move-result v0

    return v0

    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->handleNsDecl()Z

    move-result v0

    if-nez v0, :cond_2

    return v2

    :pswitch_2
    invoke-virtual {p0}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->handleAttrValue()Z

    move-result v0

    if-nez v0, :cond_2

    return v2

    :cond_2
    iput v8, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_state:I

    goto :goto_0

    :pswitch_3
    invoke-virtual {p0}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->parsePName()Lcom/fasterxml/aalto/in/PName;

    move-result-object v0

    if-nez v0, :cond_3

    return v2

    :cond_3
    invoke-direct {p0, v0}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->initStartElement(Lcom/fasterxml/aalto/in/PName;)V

    iget v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v1, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-lt v0, v1, :cond_4

    return v2

    :cond_4
    :pswitch_4
    iget v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_pendingInput:I

    if-eqz v0, :cond_5

    invoke-virtual {p0}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->handlePartialCR()Z

    move-result v0

    if-nez v0, :cond_d

    return v2

    :cond_5
    iget-object v0, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    iget v1, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    add-int/lit8 v8, v1, 0x1

    iput v8, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    and-int/lit16 v1, v0, 0xff

    if-gt v1, v13, :cond_a

    if-ne v1, v12, :cond_7

    :cond_6
    :goto_1
    invoke-virtual {p0}, Lcom/fasterxml/aalto/in/ByteBasedScanner;->markLF()V

    goto :goto_3

    :cond_7
    if-ne v1, v4, :cond_9

    iget v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v1, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-lt v0, v1, :cond_8

    iput v3, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_pendingInput:I

    return v2

    :cond_8
    iget-object v1, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    if-ne v0, v12, :cond_6

    iget v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    add-int/2addr v0, v14

    iput v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    goto :goto_1

    :cond_9
    if-eq v1, v13, :cond_d

    if-eq v1, v11, :cond_d

    invoke-virtual {p0, v1}, Lcom/fasterxml/aalto/in/XmlScanner;->throwInvalidSpace(I)V

    goto :goto_3

    :cond_a
    if-ne v1, v10, :cond_b

    invoke-direct {p0, v5}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->finishStartElement(Z)I

    move-result v0

    return v0

    :cond_b
    if-ne v1, v6, :cond_c

    :goto_2
    iput v11, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_state:I

    goto/16 :goto_0

    :cond_c
    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->decodeCharForError(B)I

    move-result v0

    const-string v1, " expected space, or \'>\' or \"/>\""

    invoke-virtual {p0, v0, v1}, Lcom/fasterxml/aalto/in/XmlScanner;->throwUnexpectedChar(ILjava/lang/String;)V

    :cond_d
    :goto_3
    iput v7, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_state:I

    iget v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v1, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-lt v0, v1, :cond_e

    return v2

    :cond_e
    :pswitch_5
    iget v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_pendingInput:I

    if-eqz v0, :cond_10

    invoke-virtual {p0}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->handlePartialCR()Z

    move-result v0

    if-nez v0, :cond_f

    return v2

    :cond_f
    iget v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v1, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-lt v0, v1, :cond_10

    return v2

    :cond_10
    iget-object v0, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    iget v1, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    add-int/lit8 v8, v1, 0x1

    iput v8, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    :goto_4
    and-int/lit16 v1, v0, 0xff

    if-gt v1, v13, :cond_17

    if-ne v1, v12, :cond_12

    :cond_11
    :goto_5
    invoke-virtual {p0}, Lcom/fasterxml/aalto/in/ByteBasedScanner;->markLF()V

    goto :goto_6

    :cond_12
    if-ne v1, v4, :cond_14

    iget v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v1, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-lt v0, v1, :cond_13

    iput v3, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_pendingInput:I

    return v2

    :cond_13
    iget-object v1, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    if-ne v0, v12, :cond_11

    iget v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    add-int/2addr v0, v14

    iput v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    goto :goto_5

    :cond_14
    if-eq v1, v13, :cond_15

    if-eq v1, v11, :cond_15

    invoke-virtual {p0, v1}, Lcom/fasterxml/aalto/in/XmlScanner;->throwInvalidSpace(I)V

    :cond_15
    :goto_6
    iget v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v1, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-lt v0, v1, :cond_16

    return v2

    :cond_16
    iget-object v1, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    add-int/lit8 v8, v0, 0x1

    iput v8, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    goto :goto_4

    :cond_17
    iget v1, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_state:I

    if-eq v1, v7, :cond_1d

    const/4 v3, 0x6

    if-eq v1, v9, :cond_1b

    if-eq v1, v3, :cond_19

    invoke-virtual {p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->throwInternal()I

    :pswitch_6
    invoke-virtual {p0}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->parsePName()Lcom/fasterxml/aalto/in/PName;

    move-result-object v0

    if-nez v0, :cond_18

    return v2

    :cond_18
    iput-object v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_elemAttrName:Lcom/fasterxml/aalto/in/PName;

    iput v9, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_state:I

    goto/16 :goto_0

    :cond_19
    const/16 v1, 0x22

    if-eq v0, v1, :cond_1a

    const/16 v1, 0x27

    if-eq v0, v1, :cond_1a

    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->decodeCharForError(B)I

    move-result v1

    const-string v2, " Expected a quote"

    invoke-virtual {p0, v1, v2}, Lcom/fasterxml/aalto/in/XmlScanner;->throwUnexpectedChar(ILjava/lang/String;)V

    :cond_1a
    invoke-direct {p0, v0}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->initAttribute(B)V

    goto/16 :goto_0

    :cond_1b
    const/16 v1, 0x3d

    if-eq v0, v1, :cond_1c

    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->decodeCharForError(B)I

    move-result v0

    const-string v1, " expected \'=\'"

    invoke-virtual {p0, v0, v1}, Lcom/fasterxml/aalto/in/XmlScanner;->throwUnexpectedChar(ILjava/lang/String;)V

    :cond_1c
    iput v3, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_state:I

    goto/16 :goto_0

    :cond_1d
    if-ne v0, v6, :cond_1e

    goto/16 :goto_2

    :cond_1e
    if-ne v0, v10, :cond_1f

    invoke-direct {p0, v5}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->finishStartElement(Z)I

    move-result v0

    return v0

    :cond_1f
    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->parseNewName(B)Lcom/fasterxml/aalto/in/PName;

    move-result-object v0

    if-nez v0, :cond_20

    const/4 v0, 0x4

    iput v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_state:I

    return v2

    :cond_20
    iput v9, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_state:I

    iput-object v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_elemAttrName:Lcom/fasterxml/aalto/in/PName;

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_5
        :pswitch_5
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public handleStartElementStart(B)I
    .locals 1

    invoke-virtual {p0, p1}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->parseNewName(B)Lcom/fasterxml/aalto/in/PName;

    move-result-object p1

    const/4 v0, 0x1

    iput v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_nextEvent:I

    if-nez p1, :cond_0

    iput v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_state:I

    const/16 p1, 0x101

    return p1

    :cond_0
    invoke-direct {p0, p1}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->initStartElement(Lcom/fasterxml/aalto/in/PName;)V

    invoke-virtual {p0}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->handleStartElement()I

    move-result p1

    return p1
.end method

.method public final needMoreInput()Z
    .locals 2

    iget v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v1, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-lt v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_endOfInput:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public nextFromTree()I
    .locals 10

    iget v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_currToken:I

    const/4 v1, 0x0

    const/4 v2, 0x2

    const/4 v3, 0x1

    const/16 v4, 0x101

    if-eq v0, v4, :cond_4

    if-ne v0, v3, :cond_0

    iget-boolean v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_isEmptyTag:Z

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_depth:I

    sub-int/2addr v0, v3

    iput v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_depth:I

    iput v2, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_currToken:I

    return v2

    :cond_0
    if-ne v0, v2, :cond_1

    iget-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_currElem:Lcom/fasterxml/aalto/in/ElementScope;

    invoke-virtual {v0}, Lcom/fasterxml/aalto/in/ElementScope;->getParent()Lcom/fasterxml/aalto/in/ElementScope;

    move-result-object v0

    iput-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_currElem:Lcom/fasterxml/aalto/in/ElementScope;

    :goto_0
    iget-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_lastNsDecl:Lcom/fasterxml/aalto/in/NsDeclaration;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/fasterxml/aalto/in/NsDeclaration;->getLevel()I

    move-result v0

    iget v5, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_depth:I

    if-lt v0, v5, :cond_1

    iget-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_lastNsDecl:Lcom/fasterxml/aalto/in/NsDeclaration;

    invoke-virtual {v0}, Lcom/fasterxml/aalto/in/NsDeclaration;->unbind()Lcom/fasterxml/aalto/in/NsDeclaration;

    move-result-object v0

    iput-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_lastNsDecl:Lcom/fasterxml/aalto/in/NsDeclaration;

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/fasterxml/aalto/in/ByteBasedScanner;->setStartLocation()V

    iget-boolean v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_tokenIncomplete:Z

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->skipCharacters()Z

    move-result v0

    if-nez v0, :cond_2

    return v4

    :cond_2
    iput-boolean v1, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_tokenIncomplete:Z

    :cond_3
    iput v4, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_nextEvent:I

    iput v4, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_currToken:I

    iput v1, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_state:I

    :cond_4
    iget v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_nextEvent:I

    const/16 v5, 0xc

    const/4 v6, 0x5

    const/4 v7, 0x4

    const/4 v8, 0x3

    if-ne v0, v4, :cond_16

    iget v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_state:I

    if-nez v0, :cond_9

    iget v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_pendingInput:I

    if-eqz v0, :cond_5

    iput v7, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_nextEvent:I

    invoke-virtual {p0}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->startCharactersPending()I

    move-result v0

    return v0

    :cond_5
    iget v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v4, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-lt v0, v4, :cond_6

    iget v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_currToken:I

    return v0

    :cond_6
    iget-object v4, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    add-int/lit8 v9, v0, 0x1

    iput v9, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {v4, v0}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    const/16 v4, 0x3c

    if-ne v0, v4, :cond_7

    iput v3, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_state:I

    goto :goto_1

    :cond_7
    const/16 v4, 0x26

    if-ne v0, v4, :cond_8

    iput v2, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_state:I

    goto :goto_1

    :cond_8
    iput v7, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_nextEvent:I

    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->startCharacters(B)I

    move-result v0

    return v0

    :cond_9
    :goto_1
    iget v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v4, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-lt v0, v4, :cond_a

    iget v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_currToken:I

    return v0

    :cond_a
    iget v4, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_state:I

    if-ne v4, v3, :cond_e

    iget-object v4, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    add-int/lit8 v9, v0, 0x1

    iput v9, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {v4, v0}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    const/16 v4, 0x21

    if-ne v0, v4, :cond_b

    iput v8, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_state:I

    goto :goto_2

    :cond_b
    const/16 v2, 0x3f

    if-ne v0, v2, :cond_c

    iput v8, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_nextEvent:I

    iput v1, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_state:I

    invoke-virtual {p0}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->handlePI()I

    move-result v0

    return v0

    :cond_c
    const/16 v1, 0x2f

    if-ne v0, v1, :cond_d

    invoke-direct {p0}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->handleEndElementStart()I

    move-result v0

    return v0

    :cond_d
    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->handleStartElementStart(B)I

    move-result v0

    return v0

    :cond_e
    if-ne v4, v2, :cond_f

    invoke-virtual {p0}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->handleEntityStartingToken()I

    move-result v0

    return v0

    :cond_f
    const/4 v0, 0x6

    if-ne v4, v0, :cond_10

    invoke-virtual {p0}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->handleNamedEntityStartingToken()I

    move-result v0

    return v0

    :cond_10
    if-ne v4, v6, :cond_11

    invoke-virtual {p0}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->handleNumericEntityStartingToken()I

    move-result v0

    return v0

    :cond_11
    :goto_2
    iget v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_state:I

    if-ne v0, v8, :cond_15

    iget v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v4, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-lt v0, v4, :cond_12

    iget v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_currToken:I

    return v0

    :cond_12
    iget-object v4, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    add-int/lit8 v9, v0, 0x1

    iput v9, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {v4, v0}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    const/16 v4, 0x2d

    if-ne v0, v4, :cond_13

    iput v6, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_nextEvent:I

    :goto_3
    iput v1, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_state:I

    goto :goto_4

    :cond_13
    const/16 v4, 0x5b

    if-ne v0, v4, :cond_14

    iput v5, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_nextEvent:I

    goto :goto_3

    :cond_14
    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->decodeCharForError(B)I

    move-result v0

    const-string v1, " (expected either \'-\' for COMMENT or \'[CDATA[\' for CDATA section)"

    invoke-virtual {p0, v0, v1}, Lcom/fasterxml/aalto/in/XmlScanner;->reportTreeUnexpChar(ILjava/lang/String;)V

    goto :goto_4

    :cond_15
    invoke-virtual {p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->throwInternal()I

    :cond_16
    :goto_4
    iget v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_nextEvent:I

    if-eq v0, v3, :cond_1e

    if-eq v0, v2, :cond_1d

    if-eq v0, v8, :cond_1c

    if-eq v0, v7, :cond_19

    if-eq v0, v6, :cond_18

    if-eq v0, v5, :cond_17

    goto :goto_5

    :cond_17
    invoke-direct {p0}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->handleCData()I

    move-result v0

    return v0

    :cond_18
    invoke-virtual {p0}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->handleComment()I

    move-result v0

    return v0

    :cond_19
    iget-boolean v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_cfgLazyParsing:Z

    if-nez v0, :cond_1a

    iget-boolean v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_cfgCoalescing:Z

    if-eqz v0, :cond_1a

    invoke-virtual {p0}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->finishCharactersCoalescing()I

    move-result v0

    return v0

    :cond_1a
    iget v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_pendingInput:I

    if-eqz v0, :cond_1b

    invoke-virtual {p0}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->startCharactersPending()I

    move-result v0

    return v0

    :cond_1b
    invoke-virtual {p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->throwInternal()I

    :goto_5
    invoke-virtual {p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->throwInternal()I

    move-result v0

    return v0

    :cond_1c
    invoke-virtual {p0}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->handlePI()I

    move-result v0

    return v0

    :cond_1d
    invoke-direct {p0}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->handleEndElement()I

    move-result v0

    return v0

    :cond_1e
    invoke-virtual {p0}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->handleStartElement()I

    move-result v0

    return v0
.end method

.method public final parseCDataContents()I
    .locals 11

    iget v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_pendingInput:I

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->handleCDataPending()I

    move-result v0

    if-eqz v0, :cond_0

    return v0

    :cond_0
    iget-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_textBuilder:Lcom/fasterxml/aalto/util/TextBuilder;

    invoke-virtual {v0}, Lcom/fasterxml/aalto/util/TextBuilder;->getBufferWithoutReset()[C

    move-result-object v0

    iget-object v1, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_textBuilder:Lcom/fasterxml/aalto/util/TextBuilder;

    invoke-virtual {v1}, Lcom/fasterxml/aalto/util/TextBuilder;->getCurrentLength()I

    move-result v1

    iget-object v2, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_charTypes:Lcom/fasterxml/aalto/util/XmlCharTypes;

    iget-object v2, v2, Lcom/fasterxml/aalto/util/XmlCharTypes;->OTHER_CHARS:[I

    iget-object v3, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    :cond_1
    :goto_0
    iget v4, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v5, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    const/16 v6, 0x101

    if-lt v4, v5, :cond_2

    goto/16 :goto_8

    :cond_2
    array-length v4, v0

    const/4 v5, 0x0

    if-lt v1, v4, :cond_3

    iget-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_textBuilder:Lcom/fasterxml/aalto/util/TextBuilder;

    invoke-virtual {v0}, Lcom/fasterxml/aalto/util/TextBuilder;->finishCurrentSegment()[C

    move-result-object v0

    move v1, v5

    :cond_3
    iget v4, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    iget v7, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    array-length v8, v0

    sub-int/2addr v8, v1

    add-int/2addr v7, v8

    if-ge v7, v4, :cond_4

    move v4, v7

    :cond_4
    :goto_1
    iget v7, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    if-ge v7, v4, :cond_1

    add-int/lit8 v8, v7, 0x1

    iput v8, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {v3, v7}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v7

    and-int/lit16 v7, v7, 0xff

    aget v8, v2, v7

    if-eqz v8, :cond_14

    const/16 v4, 0xb

    if-eq v8, v4, :cond_c

    packed-switch v8, :pswitch_data_0

    goto/16 :goto_a

    :pswitch_0
    iget v4, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    iget v8, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    sub-int v9, v4, v8

    const/4 v10, 0x3

    if-ge v9, v10, :cond_5

    if-le v4, v8, :cond_8

    iget-object v0, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    add-int/lit8 v2, v8, 0x1

    iput v2, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {v0, v8}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x8

    or-int/2addr v7, v0

    iget v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    iget v2, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    if-le v0, v2, :cond_8

    iget-object v0, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x10

    :goto_2
    or-int/2addr v7, v0

    goto :goto_4

    :cond_5
    invoke-virtual {p0, v7}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->decodeUtf8_4(I)I

    move-result v4

    add-int/lit8 v6, v1, 0x1

    shr-int/lit8 v7, v4, 0xa

    const v8, 0xd800

    or-int/2addr v7, v8

    int-to-char v7, v7

    aput-char v7, v0, v1

    array-length v1, v0

    if-lt v6, v1, :cond_6

    iget-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_textBuilder:Lcom/fasterxml/aalto/util/TextBuilder;

    invoke-virtual {v0}, Lcom/fasterxml/aalto/util/TextBuilder;->finishCurrentSegment()[C

    move-result-object v0

    goto :goto_3

    :cond_6
    move v5, v6

    :goto_3
    and-int/lit16 v1, v4, 0x3ff

    const v4, 0xdc00

    or-int v7, v1, v4

    move v1, v5

    goto/16 :goto_a

    :pswitch_1
    iget v4, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    iget v5, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    sub-int v8, v4, v5

    const/4 v9, 0x2

    if-ge v8, v9, :cond_7

    if-le v4, v5, :cond_8

    iget-object v0, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    add-int/lit8 v2, v5, 0x1

    iput v2, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {v0, v5}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x8

    goto :goto_2

    :cond_7
    invoke-virtual {p0, v7}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->decodeUtf8_3(I)I

    move-result v7

    goto/16 :goto_a

    :pswitch_2
    iget v4, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v5, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-lt v4, v5, :cond_9

    :cond_8
    :goto_4
    iput v7, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_pendingInput:I

    goto :goto_8

    :cond_9
    invoke-virtual {p0, v7}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->decodeUtf8_2(I)I

    move-result v7

    goto/16 :goto_a

    :pswitch_3
    invoke-virtual {p0, v7}, Lcom/fasterxml/aalto/in/ByteBasedScanner;->reportInvalidInitial(I)V

    goto :goto_5

    :pswitch_4
    invoke-virtual {p0, v7}, Lcom/fasterxml/aalto/in/XmlScanner;->handleInvalidXmlChar(I)C

    :pswitch_5
    iget v4, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v5, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-lt v4, v5, :cond_a

    const/4 v0, -0x1

    goto :goto_7

    :cond_a
    invoke-virtual {v3, v4}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v4

    const/16 v7, 0xa

    if-ne v4, v7, :cond_b

    iget v4, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    :cond_b
    :pswitch_6
    invoke-virtual {p0}, Lcom/fasterxml/aalto/in/ByteBasedScanner;->markLF()V

    goto/16 :goto_a

    :cond_c
    :goto_5
    iget v4, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v8, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-lt v4, v8, :cond_d

    const/16 v0, -0x1e

    goto :goto_7

    :cond_d
    iget-object v8, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v8, v4}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v4

    const/16 v8, 0x5d

    if-ne v4, v8, :cond_13

    iget v4, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    :goto_6
    iget v4, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v7, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-lt v4, v7, :cond_e

    const/16 v0, -0x1f

    :goto_7
    iput v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_pendingInput:I

    :goto_8
    iget-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_textBuilder:Lcom/fasterxml/aalto/util/TextBuilder;

    invoke-virtual {v0, v1}, Lcom/fasterxml/aalto/util/TextBuilder;->setCurrentLength(I)V

    return v6

    :cond_e
    iget-object v7, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v7, v4}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v4

    const/16 v7, 0x3e

    if-ne v4, v7, :cond_f

    iget v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_textBuilder:Lcom/fasterxml/aalto/util/TextBuilder;

    invoke-virtual {v0, v1}, Lcom/fasterxml/aalto/util/TextBuilder;->setCurrentLength(I)V

    iput v5, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_state:I

    iput v6, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_nextEvent:I

    const/16 v0, 0xc

    return v0

    :cond_f
    iget-object v4, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    iget v7, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {v4, v7}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v4

    if-eq v4, v8, :cond_11

    add-int/lit8 v4, v1, 0x1

    aput-char v8, v0, v1

    array-length v1, v0

    if-lt v4, v1, :cond_10

    iget-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_textBuilder:Lcom/fasterxml/aalto/util/TextBuilder;

    invoke-virtual {v0}, Lcom/fasterxml/aalto/util/TextBuilder;->finishCurrentSegment()[C

    move-result-object v0

    goto :goto_9

    :cond_10
    move v5, v4

    :goto_9
    add-int/lit8 v1, v5, 0x1

    aput-char v8, v0, v5

    goto/16 :goto_0

    :cond_11
    iget v4, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    add-int/lit8 v4, v1, 0x1

    aput-char v8, v0, v1

    array-length v1, v0

    if-lt v4, v1, :cond_12

    iget-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_textBuilder:Lcom/fasterxml/aalto/util/TextBuilder;

    invoke-virtual {v0}, Lcom/fasterxml/aalto/util/TextBuilder;->finishCurrentSegment()[C

    move-result-object v0

    move v1, v5

    goto :goto_6

    :cond_12
    move v1, v4

    goto :goto_6

    :cond_13
    :goto_a
    add-int/lit8 v4, v1, 0x1

    int-to-char v5, v7

    aput-char v5, v0, v1

    move v1, v4

    goto/16 :goto_0

    :cond_14
    add-int/lit8 v8, v1, 0x1

    int-to-char v7, v7

    aput-char v7, v0, v1

    move v1, v8

    goto/16 :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public parseCommentContents()I
    .locals 11

    iget v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_pendingInput:I

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->handleCommentPending()I

    move-result v0

    if-eqz v0, :cond_0

    return v0

    :cond_0
    iget-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_textBuilder:Lcom/fasterxml/aalto/util/TextBuilder;

    invoke-virtual {v0}, Lcom/fasterxml/aalto/util/TextBuilder;->getBufferWithoutReset()[C

    move-result-object v0

    iget-object v1, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_textBuilder:Lcom/fasterxml/aalto/util/TextBuilder;

    invoke-virtual {v1}, Lcom/fasterxml/aalto/util/TextBuilder;->getCurrentLength()I

    move-result v1

    iget-object v2, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_charTypes:Lcom/fasterxml/aalto/util/XmlCharTypes;

    iget-object v2, v2, Lcom/fasterxml/aalto/util/XmlCharTypes;->OTHER_CHARS:[I

    iget-object v3, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    :cond_1
    :goto_0
    iget v4, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v5, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    const/16 v6, 0x101

    if-lt v4, v5, :cond_2

    goto/16 :goto_7

    :cond_2
    array-length v4, v0

    const/4 v5, 0x0

    if-lt v1, v4, :cond_3

    iget-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_textBuilder:Lcom/fasterxml/aalto/util/TextBuilder;

    invoke-virtual {v0}, Lcom/fasterxml/aalto/util/TextBuilder;->finishCurrentSegment()[C

    move-result-object v0

    move v1, v5

    :cond_3
    iget v4, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    iget v7, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    array-length v8, v0

    sub-int/2addr v8, v1

    add-int/2addr v7, v8

    if-ge v7, v4, :cond_4

    move v4, v7

    :cond_4
    :goto_1
    iget v7, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    if-ge v7, v4, :cond_1

    add-int/lit8 v8, v7, 0x1

    iput v8, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {v3, v7}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v7

    and-int/lit16 v7, v7, 0xff

    aget v8, v2, v7

    if-eqz v8, :cond_11

    const/16 v4, 0xd

    if-eq v8, v4, :cond_c

    packed-switch v8, :pswitch_data_0

    goto/16 :goto_8

    :pswitch_0
    iget v4, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    iget v8, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    sub-int v9, v4, v8

    const/4 v10, 0x3

    if-ge v9, v10, :cond_5

    if-le v4, v8, :cond_8

    iget-object v0, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    add-int/lit8 v2, v8, 0x1

    iput v2, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {v0, v8}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x8

    or-int/2addr v7, v0

    iget v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    iget v2, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    if-le v0, v2, :cond_8

    iget-object v0, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x10

    :goto_2
    or-int/2addr v7, v0

    goto :goto_4

    :cond_5
    invoke-virtual {p0, v7}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->decodeUtf8_4(I)I

    move-result v4

    add-int/lit8 v6, v1, 0x1

    shr-int/lit8 v7, v4, 0xa

    const v8, 0xd800

    or-int/2addr v7, v8

    int-to-char v7, v7

    aput-char v7, v0, v1

    array-length v1, v0

    if-lt v6, v1, :cond_6

    iget-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_textBuilder:Lcom/fasterxml/aalto/util/TextBuilder;

    invoke-virtual {v0}, Lcom/fasterxml/aalto/util/TextBuilder;->finishCurrentSegment()[C

    move-result-object v0

    goto :goto_3

    :cond_6
    move v5, v6

    :goto_3
    and-int/lit16 v1, v4, 0x3ff

    const v4, 0xdc00

    or-int v7, v1, v4

    move v1, v5

    goto/16 :goto_8

    :pswitch_1
    iget v4, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    iget v5, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    sub-int v8, v4, v5

    const/4 v9, 0x2

    if-ge v8, v9, :cond_7

    if-le v4, v5, :cond_8

    iget-object v0, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    add-int/lit8 v2, v5, 0x1

    iput v2, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {v0, v5}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x8

    goto :goto_2

    :cond_7
    invoke-virtual {p0, v7}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->decodeUtf8_3(I)I

    move-result v7

    goto/16 :goto_8

    :pswitch_2
    iget v4, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v5, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-lt v4, v5, :cond_9

    :cond_8
    :goto_4
    iput v7, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_pendingInput:I

    goto :goto_7

    :cond_9
    invoke-virtual {p0, v7}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->decodeUtf8_2(I)I

    move-result v7

    goto :goto_8

    :pswitch_3
    invoke-virtual {p0, v7}, Lcom/fasterxml/aalto/in/ByteBasedScanner;->reportInvalidInitial(I)V

    goto :goto_6

    :pswitch_4
    invoke-virtual {p0, v7}, Lcom/fasterxml/aalto/in/XmlScanner;->handleInvalidXmlChar(I)C

    :pswitch_5
    iget v4, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v5, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-lt v4, v5, :cond_a

    const/4 v0, -0x1

    :goto_5
    iput v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_pendingInput:I

    goto :goto_7

    :cond_a
    invoke-virtual {v3, v4}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v4

    const/16 v7, 0xa

    if-ne v4, v7, :cond_b

    iget v4, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    :cond_b
    :pswitch_6
    invoke-virtual {p0}, Lcom/fasterxml/aalto/in/ByteBasedScanner;->markLF()V

    goto :goto_8

    :cond_c
    :goto_6
    iget v4, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v8, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-lt v4, v8, :cond_d

    const/16 v0, -0x14

    goto :goto_5

    :cond_d
    iget-object v8, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v8, v4}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v4

    const/16 v8, 0x2d

    if-ne v4, v8, :cond_10

    iget v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v2, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-lt v0, v2, :cond_e

    const/16 v0, -0x15

    goto :goto_5

    :goto_7
    iget-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_textBuilder:Lcom/fasterxml/aalto/util/TextBuilder;

    invoke-virtual {v0, v1}, Lcom/fasterxml/aalto/util/TextBuilder;->setCurrentLength(I)V

    return v6

    :cond_e
    iget-object v2, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    add-int/lit8 v3, v0, 0x1

    iput v3, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {v2, v0}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    const/16 v2, 0x3e

    if-eq v0, v2, :cond_f

    invoke-virtual {p0}, Lcom/fasterxml/aalto/in/XmlScanner;->reportDoubleHyphenInComments()V

    :cond_f
    iget-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_textBuilder:Lcom/fasterxml/aalto/util/TextBuilder;

    invoke-virtual {v0, v1}, Lcom/fasterxml/aalto/util/TextBuilder;->setCurrentLength(I)V

    iput v5, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_state:I

    iput v6, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_nextEvent:I

    const/4 v0, 0x5

    return v0

    :cond_10
    :goto_8
    add-int/lit8 v4, v1, 0x1

    int-to-char v5, v7

    aput-char v5, v0, v1

    move v1, v4

    goto/16 :goto_0

    :cond_11
    add-int/lit8 v8, v1, 0x1

    int-to-char v7, v7

    aput-char v7, v0, v1

    move v1, v8

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final parseEntityName()Lcom/fasterxml/aalto/in/PName;
    .locals 13

    iget v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_currQuad:I

    :goto_0
    iget v1, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_currQuadBytes:I

    const/4 v2, 0x3

    const/4 v3, 0x2

    const/16 v4, 0x2f

    const/16 v5, 0x3a

    const/16 v6, 0x2d

    const/4 v7, 0x0

    const/16 v8, 0x41

    const/4 v9, 0x0

    const/4 v10, 0x1

    if-eqz v1, :cond_0

    if-eq v1, v10, :cond_5

    if-eq v1, v3, :cond_9

    if-eq v1, v2, :cond_f

    goto/16 :goto_1

    :cond_0
    iget v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v1, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-lt v0, v1, :cond_1

    return-object v7

    :cond_1
    iget-object v1, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    add-int/lit8 v11, v0, 0x1

    iput v11, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    and-int/lit16 v0, v0, 0xff

    if-ge v0, v8, :cond_5

    if-lt v0, v6, :cond_2

    if-gt v0, v5, :cond_2

    if-ne v0, v4, :cond_5

    :cond_2
    iget v1, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_quadCount:I

    if-ne v1, v10, :cond_4

    iget-object v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_quadBuffer:[I

    aget v0, v0, v9

    sget v1, Lcom/fasterxml/aalto/in/EntityNames;->ENTITY_APOS_QUAD:I

    if-ne v0, v1, :cond_3

    iget v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    sub-int/2addr v0, v10

    iput v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    sget-object v0, Lcom/fasterxml/aalto/in/EntityNames;->ENTITY_APOS:Lcom/fasterxml/aalto/in/PNameC;

    return-object v0

    :cond_3
    sget v1, Lcom/fasterxml/aalto/in/EntityNames;->ENTITY_QUOT_QUAD:I

    if-ne v0, v1, :cond_4

    iget v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    sub-int/2addr v0, v10

    iput v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    sget-object v0, Lcom/fasterxml/aalto/in/EntityNames;->ENTITY_QUOT:Lcom/fasterxml/aalto/in/PNameC;

    return-object v0

    :cond_4
    invoke-virtual {p0, v0, v9}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->findPName(II)Lcom/fasterxml/aalto/in/PName;

    move-result-object v0

    return-object v0

    :cond_5
    iget v1, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v11, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-lt v1, v11, :cond_6

    iput v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_currQuad:I

    iput v10, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_currQuadBytes:I

    return-object v7

    :cond_6
    iget-object v11, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    add-int/lit8 v12, v1, 0x1

    iput v12, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {v11, v1}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v1

    and-int/lit16 v1, v1, 0xff

    if-ge v1, v8, :cond_8

    if-lt v1, v6, :cond_7

    if-gt v1, v5, :cond_7

    if-ne v1, v4, :cond_8

    :cond_7
    invoke-virtual {p0, v0, v10}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->findPName(II)Lcom/fasterxml/aalto/in/PName;

    move-result-object v0

    return-object v0

    :cond_8
    shl-int/lit8 v0, v0, 0x8

    or-int/2addr v0, v1

    :cond_9
    iget v1, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v11, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-lt v1, v11, :cond_a

    iput v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_currQuad:I

    iput v3, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_currQuadBytes:I

    return-object v7

    :cond_a
    iget-object v11, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    add-int/lit8 v12, v1, 0x1

    iput v12, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {v11, v1}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v1

    and-int/lit16 v1, v1, 0xff

    if-ge v1, v8, :cond_e

    if-lt v1, v6, :cond_b

    if-gt v1, v5, :cond_b

    if-ne v1, v4, :cond_e

    :cond_b
    iget v1, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_quadCount:I

    if-nez v1, :cond_d

    sget v1, Lcom/fasterxml/aalto/in/EntityNames;->ENTITY_GT_QUAD:I

    if-ne v0, v1, :cond_c

    iget v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    sub-int/2addr v0, v10

    iput v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    sget-object v0, Lcom/fasterxml/aalto/in/EntityNames;->ENTITY_GT:Lcom/fasterxml/aalto/in/PNameC;

    return-object v0

    :cond_c
    sget v1, Lcom/fasterxml/aalto/in/EntityNames;->ENTITY_LT_QUAD:I

    if-ne v0, v1, :cond_d

    iget v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    sub-int/2addr v0, v10

    iput v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    sget-object v0, Lcom/fasterxml/aalto/in/EntityNames;->ENTITY_LT:Lcom/fasterxml/aalto/in/PNameC;

    return-object v0

    :cond_d
    invoke-virtual {p0, v0, v3}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->findPName(II)Lcom/fasterxml/aalto/in/PName;

    move-result-object v0

    return-object v0

    :cond_e
    shl-int/lit8 v0, v0, 0x8

    or-int/2addr v0, v1

    :cond_f
    iget v1, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v3, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-lt v1, v3, :cond_10

    iput v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_currQuad:I

    iput v2, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_currQuadBytes:I

    return-object v7

    :cond_10
    iget-object v3, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    add-int/lit8 v7, v1, 0x1

    iput v7, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {v3, v1}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v1

    and-int/lit16 v1, v1, 0xff

    if-ge v1, v8, :cond_13

    if-lt v1, v6, :cond_11

    if-gt v1, v5, :cond_11

    if-ne v1, v4, :cond_13

    :cond_11
    iget v1, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_quadCount:I

    if-nez v1, :cond_12

    sget v1, Lcom/fasterxml/aalto/in/EntityNames;->ENTITY_AMP_QUAD:I

    if-ne v0, v1, :cond_12

    iget v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    sub-int/2addr v0, v10

    iput v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    sget-object v0, Lcom/fasterxml/aalto/in/EntityNames;->ENTITY_AMP:Lcom/fasterxml/aalto/in/PNameC;

    return-object v0

    :cond_12
    invoke-virtual {p0, v0, v2}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->findPName(II)Lcom/fasterxml/aalto/in/PName;

    move-result-object v0

    return-object v0

    :cond_13
    shl-int/lit8 v0, v0, 0x8

    or-int/2addr v0, v1

    :goto_1
    iget v1, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_quadCount:I

    if-nez v1, :cond_14

    iget-object v1, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_quadBuffer:[I

    aput v0, v1, v9

    iput v10, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_quadCount:I

    goto :goto_2

    :cond_14
    iget-object v2, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_quadBuffer:[I

    array-length v3, v2

    if-lt v1, v3, :cond_15

    array-length v1, v2

    invoke-static {v2, v1}, Lcom/fasterxml/aalto/util/DataUtil;->growArrayBy([II)[I

    move-result-object v1

    iput-object v1, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_quadBuffer:[I

    :cond_15
    iget-object v1, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_quadBuffer:[I

    iget v2, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_quadCount:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_quadCount:I

    aput v0, v1, v2

    :goto_2
    iput v9, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_currQuadBytes:I

    goto/16 :goto_0
.end method

.method public final parseNewEntityName(B)Lcom/fasterxml/aalto/in/PName;
    .locals 1

    and-int/lit16 p1, p1, 0xff

    const/16 v0, 0x41

    if-ge p1, v0, :cond_0

    const-string v0, "; expected a name start character"

    invoke-virtual {p0, p1, v0}, Lcom/fasterxml/aalto/in/XmlScanner;->throwUnexpectedChar(ILjava/lang/String;)V

    :cond_0
    const/4 v0, 0x0

    iput v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_quadCount:I

    iput p1, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_currQuad:I

    const/4 p1, 0x1

    iput p1, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_currQuadBytes:I

    invoke-virtual {p0}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->parseEntityName()Lcom/fasterxml/aalto/in/PName;

    move-result-object p1

    return-object p1
.end method

.method public final parseNewName(B)Lcom/fasterxml/aalto/in/PName;
    .locals 1

    and-int/lit16 p1, p1, 0xff

    const/16 v0, 0x41

    if-ge p1, v0, :cond_0

    const-string v0, "; expected a name start character"

    invoke-virtual {p0, p1, v0}, Lcom/fasterxml/aalto/in/XmlScanner;->throwUnexpectedChar(ILjava/lang/String;)V

    :cond_0
    const/4 v0, 0x0

    iput v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_quadCount:I

    iput p1, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_currQuad:I

    const/4 p1, 0x1

    iput p1, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_currQuadBytes:I

    invoke-virtual {p0}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->parsePName()Lcom/fasterxml/aalto/in/PName;

    move-result-object p1

    return-object p1
.end method

.method public parsePIData()I
    .locals 11

    iget v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_pendingInput:I

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->handlePIPending()I

    move-result v0

    if-eqz v0, :cond_0

    return v0

    :cond_0
    iget-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_textBuilder:Lcom/fasterxml/aalto/util/TextBuilder;

    invoke-virtual {v0}, Lcom/fasterxml/aalto/util/TextBuilder;->getBufferWithoutReset()[C

    move-result-object v0

    iget-object v1, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_textBuilder:Lcom/fasterxml/aalto/util/TextBuilder;

    invoke-virtual {v1}, Lcom/fasterxml/aalto/util/TextBuilder;->getCurrentLength()I

    move-result v1

    iget-object v2, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_charTypes:Lcom/fasterxml/aalto/util/XmlCharTypes;

    iget-object v2, v2, Lcom/fasterxml/aalto/util/XmlCharTypes;->OTHER_CHARS:[I

    iget-object v3, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    :cond_1
    :goto_0
    iget v4, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v5, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    const/16 v6, 0x101

    if-lt v4, v5, :cond_2

    goto/16 :goto_7

    :cond_2
    array-length v4, v0

    const/4 v5, 0x0

    if-lt v1, v4, :cond_3

    iget-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_textBuilder:Lcom/fasterxml/aalto/util/TextBuilder;

    invoke-virtual {v0}, Lcom/fasterxml/aalto/util/TextBuilder;->finishCurrentSegment()[C

    move-result-object v0

    move v1, v5

    :cond_3
    iget v4, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    iget v7, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    array-length v8, v0

    sub-int/2addr v8, v1

    add-int/2addr v7, v8

    if-ge v7, v4, :cond_4

    move v4, v7

    :cond_4
    :goto_1
    iget v7, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    if-ge v7, v4, :cond_1

    add-int/lit8 v8, v7, 0x1

    iput v8, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {v3, v7}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v7

    and-int/lit16 v7, v7, 0xff

    aget v8, v2, v7

    if-eqz v8, :cond_f

    const/16 v4, 0xc

    const/4 v9, 0x3

    if-eq v8, v4, :cond_c

    packed-switch v8, :pswitch_data_0

    goto/16 :goto_8

    :pswitch_0
    iget v4, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    iget v8, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    sub-int v10, v4, v8

    if-ge v10, v9, :cond_5

    if-le v4, v8, :cond_8

    iget-object v0, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    add-int/lit8 v2, v8, 0x1

    iput v2, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {v0, v8}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x8

    or-int/2addr v7, v0

    iget v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    iget v2, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    if-le v0, v2, :cond_8

    iget-object v0, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x10

    :goto_2
    or-int/2addr v7, v0

    goto :goto_4

    :cond_5
    invoke-virtual {p0, v7}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->decodeUtf8_4(I)I

    move-result v4

    add-int/lit8 v6, v1, 0x1

    shr-int/lit8 v7, v4, 0xa

    const v8, 0xd800

    or-int/2addr v7, v8

    int-to-char v7, v7

    aput-char v7, v0, v1

    array-length v1, v0

    if-lt v6, v1, :cond_6

    iget-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_textBuilder:Lcom/fasterxml/aalto/util/TextBuilder;

    invoke-virtual {v0}, Lcom/fasterxml/aalto/util/TextBuilder;->finishCurrentSegment()[C

    move-result-object v0

    goto :goto_3

    :cond_6
    move v5, v6

    :goto_3
    and-int/lit16 v1, v4, 0x3ff

    const v4, 0xdc00

    or-int v7, v1, v4

    move v1, v5

    goto/16 :goto_8

    :pswitch_1
    iget v4, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    iget v5, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    sub-int v8, v4, v5

    const/4 v9, 0x2

    if-ge v8, v9, :cond_7

    if-le v4, v5, :cond_8

    iget-object v0, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    add-int/lit8 v2, v5, 0x1

    iput v2, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {v0, v5}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x8

    goto :goto_2

    :cond_7
    invoke-virtual {p0, v7}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->decodeUtf8_3(I)I

    move-result v7

    goto :goto_8

    :pswitch_2
    iget v4, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v5, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-lt v4, v5, :cond_9

    :cond_8
    :goto_4
    iput v7, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_pendingInput:I

    goto :goto_7

    :cond_9
    invoke-virtual {p0, v7}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->decodeUtf8_2(I)I

    move-result v7

    goto :goto_8

    :pswitch_3
    invoke-virtual {p0, v7}, Lcom/fasterxml/aalto/in/ByteBasedScanner;->reportInvalidInitial(I)V

    goto :goto_6

    :pswitch_4
    invoke-virtual {p0, v7}, Lcom/fasterxml/aalto/in/XmlScanner;->handleInvalidXmlChar(I)C

    :pswitch_5
    iget v4, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v5, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-lt v4, v5, :cond_a

    const/4 v0, -0x1

    :goto_5
    iput v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_pendingInput:I

    goto :goto_7

    :cond_a
    invoke-virtual {v3, v4}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v4

    const/16 v7, 0xa

    if-ne v4, v7, :cond_b

    iget v4, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    :cond_b
    :pswitch_6
    invoke-virtual {p0}, Lcom/fasterxml/aalto/in/ByteBasedScanner;->markLF()V

    goto :goto_8

    :cond_c
    :goto_6
    iget v4, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v8, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-lt v4, v8, :cond_d

    const/16 v0, -0xf

    goto :goto_5

    :goto_7
    iget-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_textBuilder:Lcom/fasterxml/aalto/util/TextBuilder;

    invoke-virtual {v0, v1}, Lcom/fasterxml/aalto/util/TextBuilder;->setCurrentLength(I)V

    return v6

    :cond_d
    iget-object v8, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v8, v4}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v4

    const/16 v8, 0x3e

    if-ne v4, v8, :cond_e

    iget v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_textBuilder:Lcom/fasterxml/aalto/util/TextBuilder;

    invoke-virtual {v0, v1}, Lcom/fasterxml/aalto/util/TextBuilder;->setCurrentLength(I)V

    iput v5, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_state:I

    iput v6, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_nextEvent:I

    return v9

    :cond_e
    :goto_8
    add-int/lit8 v4, v1, 0x1

    int-to-char v5, v7

    aput-char v5, v0, v1

    move v1, v4

    goto/16 :goto_0

    :cond_f
    add-int/lit8 v8, v1, 0x1

    int-to-char v7, v7

    aput-char v7, v0, v1

    move v1, v8

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final parsePName()Lcom/fasterxml/aalto/in/PName;
    .locals 13

    iget v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_currQuad:I

    :goto_0
    iget v1, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_currQuadBytes:I

    const/4 v2, 0x0

    const/4 v3, 0x3

    const/4 v4, 0x2

    const/16 v5, 0x2f

    const/16 v6, 0x3a

    const/16 v7, 0x2d

    const/4 v8, 0x0

    const/16 v9, 0x41

    const/4 v10, 0x1

    if-eqz v1, :cond_0

    if-eq v1, v10, :cond_3

    if-eq v1, v4, :cond_7

    if-eq v1, v3, :cond_b

    goto/16 :goto_1

    :cond_0
    iget v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v1, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-lt v0, v1, :cond_1

    return-object v8

    :cond_1
    iget-object v1, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    add-int/lit8 v11, v0, 0x1

    iput v11, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    and-int/lit16 v0, v0, 0xff

    if-ge v0, v9, :cond_3

    if-lt v0, v7, :cond_2

    if-gt v0, v6, :cond_2

    if-ne v0, v5, :cond_3

    :cond_2
    invoke-virtual {p0, v0, v2}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->findPName(II)Lcom/fasterxml/aalto/in/PName;

    move-result-object v0

    return-object v0

    :cond_3
    iget v1, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v11, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-lt v1, v11, :cond_4

    iput v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_currQuad:I

    iput v10, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_currQuadBytes:I

    return-object v8

    :cond_4
    iget-object v11, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    add-int/lit8 v12, v1, 0x1

    iput v12, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {v11, v1}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v1

    and-int/lit16 v1, v1, 0xff

    if-ge v1, v9, :cond_6

    if-lt v1, v7, :cond_5

    if-gt v1, v6, :cond_5

    if-ne v1, v5, :cond_6

    :cond_5
    invoke-virtual {p0, v0, v10}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->findPName(II)Lcom/fasterxml/aalto/in/PName;

    move-result-object v0

    return-object v0

    :cond_6
    shl-int/lit8 v0, v0, 0x8

    or-int/2addr v0, v1

    :cond_7
    iget v1, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v11, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-lt v1, v11, :cond_8

    iput v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_currQuad:I

    iput v4, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_currQuadBytes:I

    return-object v8

    :cond_8
    iget-object v11, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    add-int/lit8 v12, v1, 0x1

    iput v12, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {v11, v1}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v1

    and-int/lit16 v1, v1, 0xff

    if-ge v1, v9, :cond_a

    if-lt v1, v7, :cond_9

    if-gt v1, v6, :cond_9

    if-ne v1, v5, :cond_a

    :cond_9
    invoke-virtual {p0, v0, v4}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->findPName(II)Lcom/fasterxml/aalto/in/PName;

    move-result-object v0

    return-object v0

    :cond_a
    shl-int/lit8 v0, v0, 0x8

    or-int/2addr v0, v1

    :cond_b
    iget v1, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v4, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-lt v1, v4, :cond_c

    iput v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_currQuad:I

    iput v3, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_currQuadBytes:I

    return-object v8

    :cond_c
    iget-object v4, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    add-int/lit8 v8, v1, 0x1

    iput v8, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {v4, v1}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v1

    and-int/lit16 v1, v1, 0xff

    if-ge v1, v9, :cond_e

    if-lt v1, v7, :cond_d

    if-gt v1, v6, :cond_d

    if-ne v1, v5, :cond_e

    :cond_d
    invoke-virtual {p0, v0, v3}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->findPName(II)Lcom/fasterxml/aalto/in/PName;

    move-result-object v0

    return-object v0

    :cond_e
    shl-int/lit8 v0, v0, 0x8

    or-int/2addr v0, v1

    :goto_1
    iget v1, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_quadCount:I

    if-nez v1, :cond_f

    iget-object v1, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_quadBuffer:[I

    aput v0, v1, v2

    iput v10, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_quadCount:I

    goto :goto_2

    :cond_f
    iget-object v3, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_quadBuffer:[I

    array-length v4, v3

    if-lt v1, v4, :cond_10

    array-length v1, v3

    invoke-static {v3, v1}, Lcom/fasterxml/aalto/util/DataUtil;->growArrayBy([II)[I

    move-result-object v1

    iput-object v1, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_quadBuffer:[I

    :cond_10
    iget-object v1, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_quadBuffer:[I

    iget v3, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_quadCount:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_quadCount:I

    aput v0, v1, v3

    :goto_2
    iput v2, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_currQuadBytes:I

    goto/16 :goto_0
.end method

.method public skipCharacters()Z
    .locals 8

    iget v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_pendingInput:I

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->skipPending()Z

    move-result v0

    if-nez v0, :cond_0

    return v1

    :cond_0
    iget-object v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_charTypes:Lcom/fasterxml/aalto/util/XmlCharTypes;

    iget-object v0, v0, Lcom/fasterxml/aalto/util/XmlCharTypes;->TEXT_CHARS:[I

    iget-object v2, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    :cond_1
    :goto_0
    iget v3, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v4, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-lt v3, v4, :cond_2

    goto/16 :goto_7

    :cond_2
    :goto_1
    if-ge v3, v4, :cond_d

    add-int/lit8 v5, v3, 0x1

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v3

    and-int/lit16 v3, v3, 0xff

    aget v6, v0, v3

    if-eqz v6, :cond_c

    iput v5, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    const/4 v4, 0x1

    packed-switch v6, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    :pswitch_1
    move v5, v1

    move v3, v4

    :goto_2
    iget v6, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v7, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-ge v6, v7, :cond_4

    invoke-virtual {v2, v6}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v5

    const/16 v6, 0x5d

    if-eq v5, v6, :cond_3

    goto :goto_3

    :cond_3
    iget v6, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    add-int/2addr v6, v4

    iput v6, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_4
    :goto_3
    const/16 v6, 0x3e

    if-ne v5, v6, :cond_1

    if-le v3, v4, :cond_1

    invoke-virtual {p0}, Lcom/fasterxml/aalto/in/XmlScanner;->reportIllegalCDataEnd()V

    goto :goto_0

    :pswitch_2
    invoke-direct {p0}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->skipEntityInCharacters()I

    move-result v3

    if-nez v3, :cond_1

    const/16 v0, -0x50

    iput v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_pendingInput:I

    iget v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v2, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-ge v0, v2, :cond_5

    invoke-direct {p0}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->skipPending()Z

    move-result v0

    if-eqz v0, :cond_5

    return v4

    :cond_5
    return v1

    :pswitch_3
    iget v4, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    sub-int v6, v4, v5

    const/4 v7, 0x3

    if-ge v6, v7, :cond_6

    if-le v4, v5, :cond_8

    iget-object v0, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    add-int/lit8 v2, v5, 0x1

    iput v2, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {v0, v5}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x8

    or-int/2addr v3, v0

    iget v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    iget v2, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    if-le v0, v2, :cond_8

    iget-object v0, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    add-int/lit8 v4, v2, 0x1

    iput v4, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x10

    :goto_4
    or-int/2addr v3, v0

    goto :goto_5

    :cond_6
    invoke-virtual {p0, v3}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->decodeUtf8_4(I)I

    goto/16 :goto_0

    :pswitch_4
    iget v4, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    sub-int v6, v4, v5

    const/4 v7, 0x2

    if-ge v6, v7, :cond_7

    if-le v4, v5, :cond_8

    iget-object v0, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    add-int/lit8 v2, v5, 0x1

    iput v2, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {v0, v5}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x8

    goto :goto_4

    :cond_7
    invoke-virtual {p0, v3}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->decodeUtf8_3(I)I

    goto/16 :goto_0

    :pswitch_5
    iget v4, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-lt v5, v4, :cond_9

    :cond_8
    :goto_5
    iput v3, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_pendingInput:I

    goto :goto_7

    :cond_9
    invoke-virtual {p0, v3}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->skipUtf8_2(I)V

    goto/16 :goto_0

    :pswitch_6
    invoke-virtual {p0, v3}, Lcom/fasterxml/aalto/in/ByteBasedScanner;->reportInvalidInitial(I)V

    :pswitch_7
    iget v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    sub-int/2addr v0, v4

    iput v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    return v4

    :cond_a
    :goto_6
    :pswitch_8
    invoke-virtual {p0}, Lcom/fasterxml/aalto/in/ByteBasedScanner;->markLF()V

    goto/16 :goto_0

    :pswitch_9
    invoke-virtual {p0, v3}, Lcom/fasterxml/aalto/in/XmlScanner;->handleInvalidXmlChar(I)C

    :pswitch_a
    iget v3, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v5, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-lt v3, v5, :cond_b

    const/4 v0, -0x1

    iput v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_pendingInput:I

    :goto_7
    return v1

    :cond_b
    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v3

    const/16 v5, 0xa

    if-ne v3, v5, :cond_a

    iget v3, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    add-int/2addr v3, v4

    iput v3, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    goto :goto_6

    :cond_c
    move v3, v5

    goto/16 :goto_1

    :cond_d
    iput v3, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_9
        :pswitch_a
        :pswitch_8
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_0
        :pswitch_7
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public skipCoalescedText()Z
    .locals 1

    invoke-virtual {p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->throwInternal()I

    const/4 v0, 0x0

    return v0
.end method

.method public final skipUtf8_2(I)V
    .locals 2

    iget-object p1, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    iget v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->get(I)B

    move-result p1

    and-int/lit16 v0, p1, 0xc0

    const/16 v1, 0x80

    if-eq v0, v1, :cond_0

    and-int/lit16 p1, p1, 0xff

    iget v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {p0, p1, v0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->reportInvalidOther(II)V

    :cond_0
    return-void
.end method

.method public final startCharacters(B)I
    .locals 6

    and-int/lit16 p1, p1, 0xff

    iget-object v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_charTypes:Lcom/fasterxml/aalto/util/XmlCharTypes;

    iget-object v0, v0, Lcom/fasterxml/aalto/util/XmlCharTypes;->TEXT_CHARS:[I

    aget v0, v0, p1

    const/16 v1, 0x101

    const/4 v2, 0x1

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    goto/16 :goto_0

    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->throwInternal()I

    goto/16 :goto_0

    :pswitch_2
    iget v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    iget v3, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    sub-int v4, v0, v3

    const/4 v5, 0x3

    if-ge v4, v5, :cond_1

    if-le v0, v3, :cond_0

    iget-object v0, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    add-int/lit8 v2, v3, 0x1

    iput v2, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {v0, v3}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x8

    or-int/2addr p1, v0

    iget v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    iget v2, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    if-le v0, v2, :cond_0

    iget-object v0, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x10

    or-int/2addr p1, v0

    :cond_0
    iput p1, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_pendingInput:I

    return v1

    :cond_1
    invoke-virtual {p0, p1}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->decodeUtf8_4(I)I

    move-result p1

    iget-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_textBuilder:Lcom/fasterxml/aalto/util/TextBuilder;

    invoke-virtual {v0, p1}, Lcom/fasterxml/aalto/util/TextBuilder;->resetWithSurrogate(I)V

    goto :goto_1

    :pswitch_3
    iget v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    iget v3, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    sub-int v4, v0, v3

    const/4 v5, 0x2

    if-ge v4, v5, :cond_3

    if-le v0, v3, :cond_2

    iget-object v0, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    add-int/lit8 v2, v3, 0x1

    iput v2, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {v0, v3}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x8

    or-int/2addr p1, v0

    :cond_2
    iput p1, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_pendingInput:I

    return v1

    :cond_3
    invoke-virtual {p0, p1}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->decodeUtf8_3(I)I

    move-result p1

    goto :goto_0

    :pswitch_4
    iget v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v3, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-lt v0, v3, :cond_4

    iput p1, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_pendingInput:I

    return v1

    :cond_4
    invoke-virtual {p0, p1}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->decodeUtf8_2(I)I

    move-result p1

    goto :goto_0

    :pswitch_5
    invoke-virtual {p0, p1}, Lcom/fasterxml/aalto/in/ByteBasedScanner;->reportInvalidInitial(I)V

    goto :goto_0

    :pswitch_6
    invoke-virtual {p0}, Lcom/fasterxml/aalto/in/ByteBasedScanner;->markLF()V

    goto :goto_0

    :pswitch_7
    invoke-virtual {p0, p1}, Lcom/fasterxml/aalto/in/XmlScanner;->handleInvalidXmlChar(I)C

    :pswitch_8
    iget p1, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-lt p1, v0, :cond_5

    const/4 p1, -0x1

    iput p1, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_pendingInput:I

    return v1

    :cond_5
    iget-object v0, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, p1}, Ljava/nio/ByteBuffer;->get(I)B

    move-result p1

    const/16 v0, 0xa

    if-ne p1, v0, :cond_6

    iget p1, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    add-int/2addr p1, v2

    iput p1, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    :cond_6
    invoke-virtual {p0}, Lcom/fasterxml/aalto/in/ByteBasedScanner;->markLF()V

    move p1, v0

    :goto_0
    iget-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_textBuilder:Lcom/fasterxml/aalto/util/TextBuilder;

    int-to-char p1, p1

    invoke-virtual {v0, p1}, Lcom/fasterxml/aalto/util/TextBuilder;->resetWithChar(C)V

    :goto_1
    iget-boolean p1, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_cfgCoalescing:Z

    if-eqz p1, :cond_7

    iget-boolean p1, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_cfgLazyParsing:Z

    if-nez p1, :cond_7

    invoke-virtual {p0}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->finishCharactersCoalescing()I

    move-result p1

    return p1

    :cond_7
    const/4 p1, 0x4

    iput p1, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_currToken:I

    iget-boolean p1, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_cfgLazyParsing:Z

    if-eqz p1, :cond_8

    iput-boolean v2, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_tokenIncomplete:Z

    goto :goto_2

    :cond_8
    invoke-virtual {p0}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->finishCharacters()V

    :goto_2
    iget p1, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_currToken:I

    return p1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_7
        :pswitch_8
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public startCharactersPending()I
    .locals 8

    iget v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v1, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    const/16 v2, 0x101

    if-lt v0, v1, :cond_0

    return v2

    :cond_0
    iget v1, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_pendingInput:I

    const/4 v3, 0x0

    iput v3, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_pendingInput:I

    const/4 v3, -0x1

    const/4 v4, 0x4

    const/4 v5, 0x1

    if-ne v1, v3, :cond_2

    iget-object v1, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    const/16 v1, 0xa

    if-ne v0, v1, :cond_1

    iget v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    add-int/2addr v0, v5

    iput v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    :cond_1
    invoke-virtual {p0}, Lcom/fasterxml/aalto/in/ByteBasedScanner;->markLF()V

    iget-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_textBuilder:Lcom/fasterxml/aalto/util/TextBuilder;

    goto/16 :goto_5

    :cond_2
    iget-object v3, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_charTypes:Lcom/fasterxml/aalto/util/XmlCharTypes;

    iget-object v3, v3, Lcom/fasterxml/aalto/util/XmlCharTypes;->TEXT_CHARS:[I

    and-int/lit16 v6, v1, 0xff

    aget v3, v3, v6

    const/4 v7, 0x5

    if-eq v3, v7, :cond_c

    const/4 v7, 0x6

    if-eq v3, v7, :cond_9

    const/4 v7, 0x7

    if-eq v3, v7, :cond_3

    invoke-virtual {p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->throwInternal()I

    goto/16 :goto_6

    :cond_3
    iget-object v3, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    add-int/lit8 v5, v0, 0x1

    iput v5, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {v3, v0}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    and-int/lit16 v0, v0, 0xff

    shr-int/lit8 v3, v1, 0x8

    if-nez v3, :cond_6

    iget v3, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v5, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-lt v3, v5, :cond_4

    :goto_0
    shl-int/lit8 v0, v0, 0x8

    :goto_1
    or-int/2addr v0, v1

    :goto_2
    iput v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_pendingInput:I

    return v2

    :cond_4
    iget-object v5, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    add-int/lit8 v6, v3, 0x1

    iput v6, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {v5, v3}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v3

    and-int/lit16 v3, v3, 0xff

    iget v5, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v6, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-lt v5, v6, :cond_5

    shl-int/lit8 v0, v0, 0x8

    or-int/2addr v0, v1

    shl-int/lit8 v1, v3, 0x10

    or-int/2addr v0, v1

    goto :goto_2

    :cond_5
    iget-object v2, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    add-int/lit8 v6, v5, 0x1

    iput v6, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {v2, v5}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v2

    and-int/lit16 v2, v2, 0xff

    invoke-virtual {p0, v1, v0, v3, v2}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->decodeUtf8_4(IIII)I

    move-result v0

    goto :goto_3

    :cond_6
    and-int/lit16 v3, v3, 0xff

    shr-int/lit8 v5, v1, 0x10

    if-nez v5, :cond_8

    iget v5, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v7, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-lt v5, v7, :cond_7

    shl-int/lit8 v0, v0, 0x10

    goto :goto_1

    :cond_7
    iget-object v1, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    add-int/lit8 v2, v5, 0x1

    iput v2, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {v1, v5}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v1

    and-int/lit16 v1, v1, 0xff

    invoke-virtual {p0, v6, v3, v0, v1}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->decodeUtf8_4(IIII)I

    move-result v0

    goto :goto_3

    :cond_8
    invoke-virtual {p0, v6, v3, v5, v0}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->decodeUtf8_4(IIII)I

    move-result v0

    :goto_3
    iget-object v1, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_textBuilder:Lcom/fasterxml/aalto/util/TextBuilder;

    invoke-virtual {v1, v0}, Lcom/fasterxml/aalto/util/TextBuilder;->resetWithSurrogate(I)V

    iput v4, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_currToken:I

    return v4

    :cond_9
    iget-object v3, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    add-int/lit8 v7, v0, 0x1

    iput v7, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {v3, v0}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v0

    and-int/lit16 v0, v0, 0xff

    shr-int/lit8 v3, v1, 0x8

    if-nez v3, :cond_b

    iget v3, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v6, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-lt v3, v6, :cond_a

    goto :goto_0

    :cond_a
    iget-object v2, p0, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->_inputBuffer:Ljava/nio/ByteBuffer;

    add-int/lit8 v6, v3, 0x1

    iput v6, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v2

    and-int/lit16 v2, v2, 0xff

    invoke-virtual {p0, v1, v0, v2}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->decodeUtf8_3(III)I

    move-result v0

    goto :goto_4

    :cond_b
    invoke-virtual {p0, v6, v3, v0}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->decodeUtf8_3(III)I

    move-result v0

    :goto_4
    iget-object v1, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_textBuilder:Lcom/fasterxml/aalto/util/TextBuilder;

    int-to-char v0, v0

    invoke-virtual {v1, v0}, Lcom/fasterxml/aalto/util/TextBuilder;->resetWithChar(C)V

    goto :goto_6

    :cond_c
    iget-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_textBuilder:Lcom/fasterxml/aalto/util/TextBuilder;

    invoke-virtual {p0, v1}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->decodeUtf8_2(I)I

    move-result v1

    int-to-char v1, v1

    :goto_5
    invoke-virtual {v0, v1}, Lcom/fasterxml/aalto/util/TextBuilder;->resetWithChar(C)V

    :goto_6
    iget-boolean v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_cfgCoalescing:Z

    if-eqz v0, :cond_d

    iget-boolean v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_cfgLazyParsing:Z

    if-nez v0, :cond_d

    invoke-virtual {p0}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->finishCharactersCoalescing()I

    move-result v0

    return v0

    :cond_d
    iput v4, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_currToken:I

    iget-boolean v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_cfgLazyParsing:Z

    if-eqz v0, :cond_e

    iput-boolean v5, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_tokenIncomplete:Z

    goto :goto_7

    :cond_e
    invoke-virtual {p0}, Lcom/fasterxml/aalto/async/AsyncByteBufferScanner;->finishCharacters()V

    :goto_7
    iget v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_currToken:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "asyncScanner; curr="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_currToken:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, " next="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_nextEvent:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", state = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_state:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

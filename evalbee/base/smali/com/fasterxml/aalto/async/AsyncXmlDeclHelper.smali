.class public Lcom/fasterxml/aalto/async/AsyncXmlDeclHelper;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final NAME_ENCODING:Lcom/fasterxml/aalto/in/PName2;

.field private static final NAME_STANDALONE:Lcom/fasterxml/aalto/in/PName3;

.field private static final NAME_VERSION:Lcom/fasterxml/aalto/in/PName2;

.field private static final NAME_XML:Lcom/fasterxml/aalto/in/PName1;

.field private static final QUAD_ENCODING1:I

.field private static final QUAD_ENCODING2:I

.field private static final QUAD_STANDALONE1:I

.field private static final QUAD_STANDALONE2:I

.field private static final QUAD_STANDALONE3:I

.field private static final QUAD_VERSION1:I

.field private static final QUAD_VERSION2:I

.field private static final QUAD_XML:I


# direct methods
.method public static constructor <clinit>()V
    .locals 30

    const-string v0, "xml"

    invoke-static {v0}, Lcom/fasterxml/aalto/async/AsyncXmlDeclHelper;->asciiQuads(Ljava/lang/String;)[I

    move-result-object v0

    const/4 v1, 0x0

    aget v7, v0, v1

    sput v7, Lcom/fasterxml/aalto/async/AsyncXmlDeclHelper;->QUAD_XML:I

    const-string v0, "version"

    invoke-static {v0}, Lcom/fasterxml/aalto/async/AsyncXmlDeclHelper;->asciiQuads(Ljava/lang/String;)[I

    move-result-object v0

    aget v13, v0, v1

    sput v13, Lcom/fasterxml/aalto/async/AsyncXmlDeclHelper;->QUAD_VERSION1:I

    const/4 v2, 0x1

    aget v14, v0, v2

    sput v14, Lcom/fasterxml/aalto/async/AsyncXmlDeclHelper;->QUAD_VERSION2:I

    const-string v0, "standalone"

    invoke-static {v0}, Lcom/fasterxml/aalto/async/AsyncXmlDeclHelper;->asciiQuads(Ljava/lang/String;)[I

    move-result-object v0

    aget v20, v0, v1

    sput v20, Lcom/fasterxml/aalto/async/AsyncXmlDeclHelper;->QUAD_STANDALONE1:I

    aget v21, v0, v2

    sput v21, Lcom/fasterxml/aalto/async/AsyncXmlDeclHelper;->QUAD_STANDALONE2:I

    const/4 v3, 0x2

    aget v22, v0, v3

    sput v22, Lcom/fasterxml/aalto/async/AsyncXmlDeclHelper;->QUAD_STANDALONE3:I

    const-string v0, "encoding"

    invoke-static {v0}, Lcom/fasterxml/aalto/async/AsyncXmlDeclHelper;->asciiQuads(Ljava/lang/String;)[I

    move-result-object v0

    aget v28, v0, v1

    sput v28, Lcom/fasterxml/aalto/async/AsyncXmlDeclHelper;->QUAD_ENCODING1:I

    aget v29, v0, v2

    sput v29, Lcom/fasterxml/aalto/async/AsyncXmlDeclHelper;->QUAD_ENCODING2:I

    new-instance v0, Lcom/fasterxml/aalto/in/PName1;

    const-string v3, "xml"

    const/4 v4, 0x0

    const-string v5, "xml"

    const/4 v6, 0x0

    move-object v2, v0

    invoke-direct/range {v2 .. v7}, Lcom/fasterxml/aalto/in/PName1;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    sput-object v0, Lcom/fasterxml/aalto/async/AsyncXmlDeclHelper;->NAME_XML:Lcom/fasterxml/aalto/in/PName1;

    new-instance v0, Lcom/fasterxml/aalto/in/PName2;

    const-string v9, "version"

    const/4 v10, 0x0

    const-string v11, "version"

    const/4 v12, 0x0

    move-object v8, v0

    invoke-direct/range {v8 .. v14}, Lcom/fasterxml/aalto/in/PName2;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;III)V

    sput-object v0, Lcom/fasterxml/aalto/async/AsyncXmlDeclHelper;->NAME_VERSION:Lcom/fasterxml/aalto/in/PName2;

    new-instance v0, Lcom/fasterxml/aalto/in/PName3;

    const-string v16, "standalone"

    const/16 v17, 0x0

    const-string v18, "standalone"

    const/16 v19, 0x0

    move-object v15, v0

    invoke-direct/range {v15 .. v22}, Lcom/fasterxml/aalto/in/PName3;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIII)V

    sput-object v0, Lcom/fasterxml/aalto/async/AsyncXmlDeclHelper;->NAME_STANDALONE:Lcom/fasterxml/aalto/in/PName3;

    new-instance v0, Lcom/fasterxml/aalto/in/PName2;

    const-string v24, "encoding"

    const/16 v25, 0x0

    const-string v26, "encoding"

    const/16 v27, 0x0

    move-object/from16 v23, v0

    invoke-direct/range {v23 .. v29}, Lcom/fasterxml/aalto/in/PName2;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;III)V

    sput-object v0, Lcom/fasterxml/aalto/async/AsyncXmlDeclHelper;->NAME_ENCODING:Lcom/fasterxml/aalto/in/PName2;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static asciiQuads(Ljava/lang/String;)[I
    .locals 5

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v1, v0, 0x3

    div-int/lit8 v1, v1, 0x4

    new-array v1, v1, [I

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_1

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v3

    add-int/lit8 v2, v2, 0x1

    if-ge v2, v0, :cond_0

    shl-int/lit8 v3, v3, 0x8

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v4

    or-int/2addr v3, v4

    add-int/lit8 v2, v2, 0x1

    if-ge v2, v0, :cond_0

    shl-int/lit8 v3, v3, 0x8

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v4

    or-int/2addr v3, v4

    add-int/lit8 v2, v2, 0x1

    if-ge v2, v0, :cond_0

    shl-int/lit8 v3, v3, 0x8

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v4

    or-int/2addr v3, v4

    :cond_0
    div-int/lit8 v4, v2, 0x4

    aput v3, v1, v4

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    return-object v1
.end method

.method public static find(I)Lcom/fasterxml/aalto/in/PName;
    .locals 1

    .line 1
    sget v0, Lcom/fasterxml/aalto/async/AsyncXmlDeclHelper;->QUAD_XML:I

    if-ne p0, v0, :cond_0

    sget-object p0, Lcom/fasterxml/aalto/async/AsyncXmlDeclHelper;->NAME_XML:Lcom/fasterxml/aalto/in/PName1;

    return-object p0

    :cond_0
    const/4 p0, 0x0

    return-object p0
.end method

.method public static find(II)Lcom/fasterxml/aalto/in/PName;
    .locals 1

    .line 2
    sget v0, Lcom/fasterxml/aalto/async/AsyncXmlDeclHelper;->QUAD_VERSION1:I

    if-ne p0, v0, :cond_0

    sget p0, Lcom/fasterxml/aalto/async/AsyncXmlDeclHelper;->QUAD_VERSION2:I

    if-ne p1, p0, :cond_1

    sget-object p0, Lcom/fasterxml/aalto/async/AsyncXmlDeclHelper;->NAME_VERSION:Lcom/fasterxml/aalto/in/PName2;

    return-object p0

    :cond_0
    sget v0, Lcom/fasterxml/aalto/async/AsyncXmlDeclHelper;->QUAD_ENCODING1:I

    if-ne p0, v0, :cond_1

    sget p0, Lcom/fasterxml/aalto/async/AsyncXmlDeclHelper;->QUAD_ENCODING2:I

    if-ne p1, p0, :cond_1

    sget-object p0, Lcom/fasterxml/aalto/async/AsyncXmlDeclHelper;->NAME_ENCODING:Lcom/fasterxml/aalto/in/PName2;

    return-object p0

    :cond_1
    const/4 p0, 0x0

    return-object p0
.end method

.method public static find(III)Lcom/fasterxml/aalto/in/PName;
    .locals 1

    .line 3
    sget v0, Lcom/fasterxml/aalto/async/AsyncXmlDeclHelper;->QUAD_STANDALONE1:I

    if-ne p0, v0, :cond_0

    sget p0, Lcom/fasterxml/aalto/async/AsyncXmlDeclHelper;->QUAD_STANDALONE2:I

    if-ne p1, p0, :cond_0

    sget p0, Lcom/fasterxml/aalto/async/AsyncXmlDeclHelper;->QUAD_STANDALONE3:I

    if-ne p2, p0, :cond_0

    sget-object p0, Lcom/fasterxml/aalto/async/AsyncXmlDeclHelper;->NAME_STANDALONE:Lcom/fasterxml/aalto/in/PName3;

    return-object p0

    :cond_0
    const/4 p0, 0x0

    return-object p0
.end method

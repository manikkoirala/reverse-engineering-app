.class public abstract Lcom/fasterxml/aalto/async/AsyncByteScanner;
.super Lcom/fasterxml/aalto/in/ByteBasedScanner;
.source "SourceFile"

# interfaces
.implements Lcom/fasterxml/aalto/AsyncInputFeeder;


# static fields
.field protected static final EVENT_INCOMPLETE:I = 0x101

.field protected static final PENDING_STATE_ATTR_VALUE_AMP:I = -0x3c

.field protected static final PENDING_STATE_ATTR_VALUE_AMP_HASH:I = -0x3d

.field protected static final PENDING_STATE_ATTR_VALUE_AMP_HASH_X:I = -0x3e

.field protected static final PENDING_STATE_ATTR_VALUE_DEC_DIGIT:I = -0x40

.field protected static final PENDING_STATE_ATTR_VALUE_ENTITY_NAME:I = -0x3f

.field protected static final PENDING_STATE_ATTR_VALUE_HEX_DIGIT:I = -0x41

.field protected static final PENDING_STATE_CDATA_BRACKET1:I = -0x1e

.field protected static final PENDING_STATE_CDATA_BRACKET2:I = -0x1f

.field protected static final PENDING_STATE_COMMENT_HYPHEN1:I = -0x14

.field protected static final PENDING_STATE_COMMENT_HYPHEN2:I = -0x15

.field protected static final PENDING_STATE_CR:I = -0x1

.field protected static final PENDING_STATE_ENT_IN_DEC_DIGIT:I = -0x48

.field protected static final PENDING_STATE_ENT_IN_HEX_DIGIT:I = -0x49

.field protected static final PENDING_STATE_ENT_SEEN_HASH:I = -0x46

.field protected static final PENDING_STATE_ENT_SEEN_HASH_X:I = -0x47

.field protected static final PENDING_STATE_PI_QMARK:I = -0xf

.field protected static final PENDING_STATE_TEXT_AMP:I = -0x50

.field protected static final PENDING_STATE_TEXT_AMP_HASH:I = -0x51

.field protected static final PENDING_STATE_TEXT_BRACKET1:I = -0x55

.field protected static final PENDING_STATE_TEXT_BRACKET2:I = -0x56

.field protected static final PENDING_STATE_TEXT_DEC_ENTITY:I = -0x52

.field protected static final PENDING_STATE_TEXT_HEX_ENTITY:I = -0x53

.field protected static final PENDING_STATE_TEXT_IN_ENTITY:I = -0x54

.field protected static final PENDING_STATE_XMLDECL_LT:I = -0x5

.field protected static final PENDING_STATE_XMLDECL_LTQ:I = -0x6

.field protected static final PENDING_STATE_XMLDECL_TARGET:I = -0x7

.field protected static final STATE_CDATA_C:I = 0x2

.field protected static final STATE_CDATA_CD:I = 0x3

.field protected static final STATE_CDATA_CDA:I = 0x4

.field protected static final STATE_CDATA_CDAT:I = 0x5

.field protected static final STATE_CDATA_CDATA:I = 0x6

.field protected static final STATE_CDATA_CONTENT:I = 0x1

.field protected static final STATE_COMMENT_CONTENT:I = 0x1

.field protected static final STATE_COMMENT_HYPHEN:I = 0x2

.field protected static final STATE_COMMENT_HYPHEN2:I = 0x3

.field protected static final STATE_DEFAULT:I = 0x0

.field protected static final STATE_DTD_AFTER_DOCTYPE:I = 0x2

.field protected static final STATE_DTD_AFTER_PUBLIC:I = 0x8

.field protected static final STATE_DTD_AFTER_PUBLIC_ID:I = 0xc

.field protected static final STATE_DTD_AFTER_ROOT_NAME:I = 0x5

.field protected static final STATE_DTD_AFTER_SYSTEM:I = 0x9

.field protected static final STATE_DTD_AFTER_SYSTEM_ID:I = 0xf

.field protected static final STATE_DTD_BEFORE_IDS:I = 0x6

.field protected static final STATE_DTD_BEFORE_PUBLIC_ID:I = 0xa

.field protected static final STATE_DTD_BEFORE_ROOT_NAME:I = 0x3

.field protected static final STATE_DTD_BEFORE_SYSTEM_ID:I = 0xd

.field protected static final STATE_DTD_DOCTYPE:I = 0x1

.field protected static final STATE_DTD_EXPECT_CLOSING_GT:I = 0x32

.field protected static final STATE_DTD_INT_SUBSET:I = 0x10

.field protected static final STATE_DTD_PUBLIC_ID:I = 0xb

.field protected static final STATE_DTD_PUBLIC_OR_SYSTEM:I = 0x7

.field protected static final STATE_DTD_ROOT_NAME:I = 0x4

.field protected static final STATE_DTD_SYSTEM_ID:I = 0xe

.field protected static final STATE_EE_NEED_GT:I = 0x1

.field protected static final STATE_PI_AFTER_TARGET:I = 0x1

.field protected static final STATE_PI_AFTER_TARGET_QMARK:I = 0x3

.field protected static final STATE_PI_AFTER_TARGET_WS:I = 0x2

.field protected static final STATE_PI_IN_DATA:I = 0x5

.field protected static final STATE_PI_IN_TARGET:I = 0x4

.field protected static final STATE_PROLOG_DECL:I = 0x3

.field protected static final STATE_PROLOG_INITIAL:I = 0x1

.field protected static final STATE_PROLOG_SEEN_LT:I = 0x2

.field protected static final STATE_SE_ATTR_NAME:I = 0x4

.field protected static final STATE_SE_ATTR_VALUE_NORMAL:I = 0x7

.field protected static final STATE_SE_ATTR_VALUE_NSDECL:I = 0x8

.field protected static final STATE_SE_ELEM_NAME:I = 0x1

.field protected static final STATE_SE_SEEN_SLASH:I = 0x9

.field protected static final STATE_SE_SPACE_OR_ATTRNAME:I = 0x3

.field protected static final STATE_SE_SPACE_OR_ATTRVALUE:I = 0x6

.field protected static final STATE_SE_SPACE_OR_END:I = 0x2

.field protected static final STATE_SE_SPACE_OR_EQ:I = 0x5

.field protected static final STATE_TEXT_AMP:I = 0x4

.field protected static final STATE_TEXT_AMP_NAME:I = 0x6

.field protected static final STATE_TREE_NAMED_ENTITY_START:I = 0x6

.field protected static final STATE_TREE_NUMERIC_ENTITY_START:I = 0x5

.field protected static final STATE_TREE_SEEN_AMP:I = 0x2

.field protected static final STATE_TREE_SEEN_EXCL:I = 0x3

.field protected static final STATE_TREE_SEEN_LT:I = 0x1

.field protected static final STATE_TREE_SEEN_SLASH:I = 0x4

.field protected static final STATE_XMLDECL_AFTER_ENCODING:I = 0xa

.field protected static final STATE_XMLDECL_AFTER_ENCODING_VALUE:I = 0xd

.field protected static final STATE_XMLDECL_AFTER_STANDALONE:I = 0x10

.field protected static final STATE_XMLDECL_AFTER_STANDALONE_VALUE:I = 0x13

.field protected static final STATE_XMLDECL_AFTER_VERSION:I = 0x4

.field protected static final STATE_XMLDECL_AFTER_VERSION_VALUE:I = 0x7

.field protected static final STATE_XMLDECL_AFTER_XML:I = 0x1

.field protected static final STATE_XMLDECL_BEFORE_ENCODING:I = 0x8

.field protected static final STATE_XMLDECL_BEFORE_STANDALONE:I = 0xe

.field protected static final STATE_XMLDECL_BEFORE_VERSION:I = 0x2

.field protected static final STATE_XMLDECL_ENCODING:I = 0x9

.field protected static final STATE_XMLDECL_ENCODING_EQ:I = 0xb

.field protected static final STATE_XMLDECL_ENCODING_VALUE:I = 0xc

.field protected static final STATE_XMLDECL_ENDQ:I = 0x14

.field protected static final STATE_XMLDECL_STANDALONE:I = 0xf

.field protected static final STATE_XMLDECL_STANDALONE_EQ:I = 0x11

.field protected static final STATE_XMLDECL_STANDALONE_VALUE:I = 0x12

.field protected static final STATE_XMLDECL_VERSION:I = 0x3

.field protected static final STATE_XMLDECL_VERSION_EQ:I = 0x5

.field protected static final STATE_XMLDECL_VERSION_VALUE:I = 0x6


# instance fields
.field protected _charTypes:Lcom/fasterxml/aalto/util/XmlCharTypes;

.field protected _currQuad:I

.field protected _currQuadBytes:I

.field protected _elemAllNsBound:Z

.field protected _elemAttrCount:Z

.field protected _elemAttrName:Lcom/fasterxml/aalto/in/PName;

.field protected _elemAttrPtr:I

.field protected _elemAttrQuote:B

.field protected _elemNsPtr:I

.field protected _endOfInput:Z

.field protected _entityValue:I

.field protected _inDtdDeclaration:Z

.field protected _nextEvent:I

.field protected _pendingInput:I

.field protected _quadBuffer:[I

.field protected _quadCount:I

.field protected _state:I

.field protected _surroundingEvent:I

.field protected _symbols:Lcom/fasterxml/aalto/in/ByteBasedPNameTable;


# direct methods
.method public constructor <init>(Lcom/fasterxml/aalto/in/ReaderConfig;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/fasterxml/aalto/in/ByteBasedScanner;-><init>(Lcom/fasterxml/aalto/in/ReaderConfig;)V

    const/16 p1, 0x20

    new-array p1, p1, [I

    iput-object p1, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_quadBuffer:[I

    const/16 p1, 0x101

    iput p1, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_nextEvent:I

    iput p1, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_surroundingEvent:I

    const/4 p1, 0x0

    iput p1, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_pendingInput:I

    iput-boolean p1, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_endOfInput:Z

    iput p1, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_currQuadBytes:I

    iput p1, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_entityValue:I

    return-void
.end method

.method private final _parseNewXmlDeclName(B)Lcom/fasterxml/aalto/in/PName;
    .locals 1

    and-int/lit16 p1, p1, 0xff

    const/16 v0, 0x41

    if-ge p1, v0, :cond_0

    const-string v0, "; expected a name start character"

    invoke-virtual {p0, p1, v0}, Lcom/fasterxml/aalto/in/XmlScanner;->throwUnexpectedChar(ILjava/lang/String;)V

    :cond_0
    const/4 v0, 0x0

    iput v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_quadCount:I

    iput p1, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_currQuad:I

    const/4 p1, 0x1

    iput p1, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_currQuadBytes:I

    invoke-direct {p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_parseXmlDeclName()Lcom/fasterxml/aalto/in/PName;

    move-result-object p1

    return-object p1
.end method

.method private final _parseXmlDeclName()Lcom/fasterxml/aalto/in/PName;
    .locals 12

    iget v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_currQuad:I

    :goto_0
    iget v1, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_currQuadBytes:I

    const/4 v2, 0x0

    const/4 v3, 0x3

    const/4 v4, 0x2

    const/16 v5, 0x2f

    const/16 v6, 0x3a

    const/16 v7, 0x2d

    const/4 v8, 0x0

    const/16 v9, 0x41

    const/4 v10, 0x1

    if-eqz v1, :cond_0

    if-eq v1, v10, :cond_3

    if-eq v1, v4, :cond_7

    if-eq v1, v3, :cond_b

    goto/16 :goto_1

    :cond_0
    iget v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v1, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-lt v0, v1, :cond_1

    return-object v8

    :cond_1
    invoke-virtual {p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_nextByte()B

    move-result v0

    and-int/lit16 v0, v0, 0xff

    if-ge v0, v9, :cond_3

    if-lt v0, v7, :cond_2

    if-gt v0, v6, :cond_2

    if-ne v0, v5, :cond_3

    :cond_2
    invoke-virtual {p0, v0, v2}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_findXmlDeclName(II)Lcom/fasterxml/aalto/in/PName;

    move-result-object v0

    return-object v0

    :cond_3
    iget v1, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v11, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-lt v1, v11, :cond_4

    iput v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_currQuad:I

    iput v10, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_currQuadBytes:I

    return-object v8

    :cond_4
    invoke-virtual {p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_nextByte()B

    move-result v1

    and-int/lit16 v1, v1, 0xff

    if-ge v1, v9, :cond_6

    if-lt v1, v7, :cond_5

    if-gt v1, v6, :cond_5

    if-ne v1, v5, :cond_6

    :cond_5
    invoke-virtual {p0, v0, v10}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_findXmlDeclName(II)Lcom/fasterxml/aalto/in/PName;

    move-result-object v0

    return-object v0

    :cond_6
    shl-int/lit8 v0, v0, 0x8

    or-int/2addr v0, v1

    :cond_7
    iget v1, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v11, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-lt v1, v11, :cond_8

    iput v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_currQuad:I

    iput v4, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_currQuadBytes:I

    return-object v8

    :cond_8
    invoke-virtual {p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_nextByte()B

    move-result v1

    and-int/lit16 v1, v1, 0xff

    if-ge v1, v9, :cond_a

    if-lt v1, v7, :cond_9

    if-gt v1, v6, :cond_9

    if-ne v1, v5, :cond_a

    :cond_9
    invoke-virtual {p0, v0, v4}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_findXmlDeclName(II)Lcom/fasterxml/aalto/in/PName;

    move-result-object v0

    return-object v0

    :cond_a
    shl-int/lit8 v0, v0, 0x8

    or-int/2addr v0, v1

    :cond_b
    iget v1, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v4, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-lt v1, v4, :cond_c

    iput v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_currQuad:I

    iput v3, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_currQuadBytes:I

    return-object v8

    :cond_c
    invoke-virtual {p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_nextByte()B

    move-result v1

    and-int/lit16 v1, v1, 0xff

    if-ge v1, v9, :cond_e

    if-lt v1, v7, :cond_d

    if-gt v1, v6, :cond_d

    if-ne v1, v5, :cond_e

    :cond_d
    invoke-virtual {p0, v0, v3}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_findXmlDeclName(II)Lcom/fasterxml/aalto/in/PName;

    move-result-object v0

    return-object v0

    :cond_e
    shl-int/lit8 v0, v0, 0x8

    or-int/2addr v0, v1

    :goto_1
    iget v1, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_quadCount:I

    if-nez v1, :cond_f

    iget-object v1, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_quadBuffer:[I

    aput v0, v1, v2

    iput v10, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_quadCount:I

    goto :goto_2

    :cond_f
    iget-object v3, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_quadBuffer:[I

    array-length v4, v3

    if-lt v1, v4, :cond_10

    array-length v1, v3

    invoke-static {v3, v1}, Lcom/fasterxml/aalto/util/DataUtil;->growArrayBy([II)[I

    move-result-object v1

    iput-object v1, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_quadBuffer:[I

    :cond_10
    iget-object v1, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_quadBuffer:[I

    iget v3, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_quadCount:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_quadCount:I

    aput v0, v1, v3

    :goto_2
    iput v2, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_currQuadBytes:I

    goto/16 :goto_0
.end method

.method private handleDTD()I
    .locals 16

    move-object/from16 v0, p0

    iget v1, v0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_pendingInput:I

    const/4 v2, -0x1

    const/16 v3, 0x101

    if-ne v1, v2, :cond_0

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->handlePartialCR()Z

    move-result v1

    if-nez v1, :cond_0

    return v3

    :cond_0
    iget v1, v0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_state:I

    const/16 v2, 0x10

    const/16 v4, 0x32

    const/4 v5, 0x0

    if-ne v1, v2, :cond_2

    invoke-virtual {v0, v5}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->handleDTDInternalSubset(Z)Z

    move-result v1

    if-eqz v1, :cond_1

    iput v4, v0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_state:I

    goto :goto_0

    :cond_1
    return v3

    :cond_2
    :goto_0
    iget v1, v0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v6, v0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-ge v1, v6, :cond_32

    iget v1, v0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_state:I

    const/16 v6, 0x3e

    const/16 v7, 0xb

    const/4 v8, 0x1

    if-eq v1, v4, :cond_30

    const-string v10, "SYSTEM"

    const-string v11, "expected \'DOCTYPE\'"

    const/16 v13, 0xf

    const/16 v14, 0xc

    const-string v9, "PUBLIC"

    const-string v15, "DOCTYPE"

    const/16 v12, 0xd

    packed-switch v1, :pswitch_data_0

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->throwInternal()I

    goto :goto_0

    :pswitch_0
    iget-object v1, v0, Lcom/fasterxml/aalto/in/XmlScanner;->_textBuilder:Lcom/fasterxml/aalto/util/TextBuilder;

    invoke-virtual {v1}, Lcom/fasterxml/aalto/util/TextBuilder;->getBufferWithoutReset()[C

    move-result-object v1

    iget-object v9, v0, Lcom/fasterxml/aalto/in/XmlScanner;->_textBuilder:Lcom/fasterxml/aalto/util/TextBuilder;

    invoke-virtual {v9}, Lcom/fasterxml/aalto/util/TextBuilder;->getCurrentLength()I

    move-result v9

    invoke-direct {v0, v1, v9, v8}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->parseDtdId([CIZ)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0

    :cond_3
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->verifyAndSetSystemId()V

    iput v13, v0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_state:I

    iget v1, v0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v9, v0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-lt v1, v9, :cond_4

    goto :goto_0

    :cond_4
    :pswitch_1
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->asyncSkipSpace()Z

    move-result v1

    if-nez v1, :cond_5

    goto :goto_0

    :cond_5
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_nextByte()B

    move-result v1

    if-ne v1, v6, :cond_7

    :cond_6
    :goto_1
    iput v5, v0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_state:I

    iput v3, v0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_nextEvent:I

    return v7

    :cond_7
    const/16 v9, 0x5b

    if-eq v1, v9, :cond_8

    iget-byte v1, v0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_elemAttrQuote:B

    invoke-virtual {v0, v1}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->decodeCharForError(B)I

    move-result v1

    const-string v9, " (expected either \'[\' for internal subset, or \'>\' to end DOCTYPE)"

    invoke-virtual {v0, v8, v1, v9}, Lcom/fasterxml/aalto/in/XmlScanner;->reportPrologUnexpChar(ZILjava/lang/String;)V

    :cond_8
    iput v2, v0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_state:I

    invoke-virtual {v0, v8}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->handleDTDInternalSubset(Z)Z

    move-result v1

    if-eqz v1, :cond_9

    iput v4, v0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_state:I

    goto/16 :goto_12

    :cond_9
    return v3

    :pswitch_2
    iget-object v1, v0, Lcom/fasterxml/aalto/in/XmlScanner;->_textBuilder:Lcom/fasterxml/aalto/util/TextBuilder;

    invoke-virtual {v1}, Lcom/fasterxml/aalto/util/TextBuilder;->getBufferWithoutReset()[C

    move-result-object v1

    iget-object v6, v0, Lcom/fasterxml/aalto/in/XmlScanner;->_textBuilder:Lcom/fasterxml/aalto/util/TextBuilder;

    invoke-virtual {v6}, Lcom/fasterxml/aalto/util/TextBuilder;->getCurrentLength()I

    move-result v6

    invoke-direct {v0, v1, v6, v5}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->parseDtdId([CIZ)Z

    move-result v1

    if-nez v1, :cond_a

    goto/16 :goto_0

    :cond_a
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->verifyAndSetPublicId()V

    iput v14, v0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_state:I

    iget v1, v0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v6, v0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-lt v1, v6, :cond_b

    goto/16 :goto_0

    :cond_b
    :pswitch_3
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_nextByte()B

    move-result v1

    const/16 v6, 0x20

    if-eq v1, v6, :cond_d

    if-eq v1, v12, :cond_d

    const/16 v6, 0xa

    if-eq v1, v6, :cond_d

    const/16 v6, 0x9

    if-ne v1, v6, :cond_c

    goto :goto_2

    :cond_c
    invoke-virtual {v0, v1}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->decodeCharForError(B)I

    move-result v1

    const-string v6, " (expected space after PUBLIC ID)"

    invoke-virtual {v0, v8, v1, v6}, Lcom/fasterxml/aalto/in/XmlScanner;->reportPrologUnexpChar(ZILjava/lang/String;)V

    goto :goto_3

    :cond_d
    :goto_2
    iput v12, v0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_state:I

    :goto_3
    :pswitch_4
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->asyncSkipSpace()Z

    move-result v1

    if-nez v1, :cond_e

    goto/16 :goto_0

    :cond_e
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_nextByte()B

    move-result v1

    iput-byte v1, v0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_elemAttrQuote:B

    const/16 v6, 0x22

    if-eq v1, v6, :cond_f

    const/16 v6, 0x27

    if-eq v1, v6, :cond_f

    invoke-virtual {v0, v1}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->decodeCharForError(B)I

    move-result v1

    const-string v6, " (expected \'\"\' or \'\'\' for SYSTEM ID)"

    invoke-virtual {v0, v8, v1, v6}, Lcom/fasterxml/aalto/in/XmlScanner;->reportPrologUnexpChar(ZILjava/lang/String;)V

    :cond_f
    iget-object v1, v0, Lcom/fasterxml/aalto/in/XmlScanner;->_textBuilder:Lcom/fasterxml/aalto/util/TextBuilder;

    invoke-virtual {v1}, Lcom/fasterxml/aalto/util/TextBuilder;->resetWithEmpty()[C

    move-result-object v1

    iget v6, v0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v7, v0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-ge v6, v7, :cond_11

    invoke-direct {v0, v1, v5, v8}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->parseDtdId([CIZ)Z

    move-result v1

    if-nez v1, :cond_10

    goto :goto_4

    :cond_10
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->verifyAndSetSystemId()V

    iput v13, v0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_state:I

    goto/16 :goto_0

    :cond_11
    :goto_4
    const/16 v1, 0xe

    goto/16 :goto_11

    :pswitch_5
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->asyncSkipSpace()Z

    move-result v1

    if-nez v1, :cond_12

    goto/16 :goto_0

    :cond_12
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_nextByte()B

    move-result v1

    iput-byte v1, v0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_elemAttrQuote:B

    const/16 v6, 0x22

    if-eq v1, v6, :cond_13

    const/16 v6, 0x27

    if-eq v1, v6, :cond_13

    invoke-virtual {v0, v1}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->decodeCharForError(B)I

    move-result v1

    const-string v6, " (expected \'\"\' or \'\'\' for PUBLIC ID)"

    invoke-virtual {v0, v8, v1, v6}, Lcom/fasterxml/aalto/in/XmlScanner;->reportPrologUnexpChar(ZILjava/lang/String;)V

    :cond_13
    iget-object v1, v0, Lcom/fasterxml/aalto/in/XmlScanner;->_textBuilder:Lcom/fasterxml/aalto/util/TextBuilder;

    invoke-virtual {v1}, Lcom/fasterxml/aalto/util/TextBuilder;->resetWithEmpty()[C

    move-result-object v1

    iget v6, v0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v8, v0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-ge v6, v8, :cond_15

    invoke-direct {v0, v1, v5, v5}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->parseDtdId([CIZ)Z

    move-result v1

    if-nez v1, :cond_14

    goto :goto_5

    :cond_14
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->verifyAndSetPublicId()V

    iput v14, v0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_state:I

    goto/16 :goto_0

    :cond_15
    :goto_5
    iput v7, v0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_state:I

    goto/16 :goto_0

    :pswitch_6
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_nextByte()B

    move-result v1

    const/16 v6, 0x20

    if-eq v1, v6, :cond_17

    if-eq v1, v12, :cond_17

    const/16 v6, 0xa

    if-eq v1, v6, :cond_17

    const/16 v6, 0x9

    if-ne v1, v6, :cond_16

    goto :goto_7

    :cond_16
    invoke-virtual {v0, v1}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->decodeCharForError(B)I

    move-result v1

    const-string v6, " (expected space after SYSTEM keyword)"

    :goto_6
    invoke-virtual {v0, v8, v1, v6}, Lcom/fasterxml/aalto/in/XmlScanner;->reportPrologUnexpChar(ZILjava/lang/String;)V

    goto/16 :goto_0

    :cond_17
    :goto_7
    iput v12, v0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_state:I

    goto/16 :goto_0

    :pswitch_7
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_nextByte()B

    move-result v1

    const/16 v6, 0x20

    if-eq v1, v6, :cond_19

    if-eq v1, v12, :cond_19

    const/16 v6, 0xa

    if-eq v1, v6, :cond_1a

    const/16 v7, 0x9

    if-ne v1, v7, :cond_18

    goto :goto_9

    :cond_18
    invoke-virtual {v0, v1}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->decodeCharForError(B)I

    move-result v1

    const-string v6, " (expected space after PUBLIC keyword)"

    goto :goto_6

    :cond_19
    const/16 v6, 0xa

    goto :goto_9

    :pswitch_8
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->parsePName()Lcom/fasterxml/aalto/in/PName;

    move-result-object v1

    if-nez v1, :cond_1b

    :goto_8
    const/4 v6, 0x7

    :cond_1a
    :goto_9
    iput v6, v0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_state:I

    goto/16 :goto_0

    :cond_1b
    invoke-virtual {v1}, Lcom/fasterxml/aalto/in/PName;->getPrefixedName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v9, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1c

    :goto_a
    const/16 v6, 0x8

    goto :goto_9

    :cond_1c
    invoke-virtual {v10, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1d

    :goto_b
    const/16 v6, 0x9

    goto :goto_9

    :cond_1d
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    :goto_c
    const-string v7, "unexpected token \'"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\': expected either PUBLIC or SYSTEM"

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v8, v1}, Lcom/fasterxml/aalto/in/XmlScanner;->reportPrologProblem(ZLjava/lang/String;)V

    goto/16 :goto_0

    :pswitch_9
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->parsePName()Lcom/fasterxml/aalto/in/PName;

    move-result-object v1

    iput-object v1, v0, Lcom/fasterxml/aalto/in/XmlScanner;->_tokenName:Lcom/fasterxml/aalto/in/PName;

    if-nez v1, :cond_1e

    goto/16 :goto_0

    :cond_1e
    const/4 v1, 0x5

    iput v1, v0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_state:I

    iget v1, v0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v11, v0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-lt v1, v11, :cond_1f

    goto/16 :goto_0

    :cond_1f
    :pswitch_a
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_nextByte()B

    move-result v1

    if-ne v1, v6, :cond_20

    goto/16 :goto_1

    :cond_20
    const/16 v11, 0x20

    if-eq v1, v11, :cond_22

    if-eq v1, v12, :cond_22

    const/16 v11, 0xa

    if-eq v1, v11, :cond_22

    const/16 v11, 0x9

    if-ne v1, v11, :cond_21

    goto :goto_d

    :cond_21
    invoke-virtual {v0, v1}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->decodeCharForError(B)I

    move-result v1

    const-string v11, " (expected space after root name in DOCTYPE declaration)"

    invoke-virtual {v0, v8, v1, v11}, Lcom/fasterxml/aalto/in/XmlScanner;->reportPrologUnexpChar(ZILjava/lang/String;)V

    goto :goto_e

    :cond_22
    :goto_d
    const/4 v1, 0x6

    iput v1, v0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_state:I

    :goto_e
    :pswitch_b
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->asyncSkipSpace()Z

    move-result v1

    if-nez v1, :cond_23

    goto/16 :goto_0

    :cond_23
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_nextByte()B

    move-result v1

    if-ne v1, v6, :cond_24

    goto/16 :goto_1

    :cond_24
    invoke-virtual {v0, v1}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->parseNewName(B)Lcom/fasterxml/aalto/in/PName;

    move-result-object v1

    if-nez v1, :cond_25

    goto/16 :goto_8

    :cond_25
    invoke-virtual {v1}, Lcom/fasterxml/aalto/in/PName;->getPrefixedName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v9, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_26

    goto/16 :goto_a

    :cond_26
    invoke-virtual {v10, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_27

    goto/16 :goto_b

    :cond_27
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    goto/16 :goto_c

    :pswitch_c
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->parsePName()Lcom/fasterxml/aalto/in/PName;

    move-result-object v1

    iput-object v1, v0, Lcom/fasterxml/aalto/in/XmlScanner;->_tokenName:Lcom/fasterxml/aalto/in/PName;

    if-nez v1, :cond_28

    iput v8, v0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_state:I

    return v3

    :cond_28
    invoke-virtual {v1}, Lcom/fasterxml/aalto/in/PName;->getPrefixedName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v15, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_29

    invoke-virtual {v0, v8, v11}, Lcom/fasterxml/aalto/in/XmlScanner;->reportPrologProblem(ZLjava/lang/String;)V

    :cond_29
    iget v1, v0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v6, v0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-lt v1, v6, :cond_2a

    goto/16 :goto_0

    :cond_2a
    :pswitch_d
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_nextByte()B

    move-result v1

    const/16 v6, 0x20

    if-eq v1, v6, :cond_2c

    if-eq v1, v12, :cond_2c

    const/16 v6, 0xa

    if-eq v1, v6, :cond_2c

    const/16 v6, 0x9

    if-ne v1, v6, :cond_2b

    goto :goto_f

    :cond_2b
    invoke-virtual {v0, v1}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->decodeCharForError(B)I

    move-result v1

    const-string v6, " (expected space after \'DOCTYPE\')"

    invoke-virtual {v0, v8, v1, v6}, Lcom/fasterxml/aalto/in/XmlScanner;->reportPrologUnexpChar(ZILjava/lang/String;)V

    goto :goto_10

    :cond_2c
    :goto_f
    const/4 v1, 0x3

    iput v1, v0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_state:I

    :goto_10
    :pswitch_e
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->asyncSkipSpace()Z

    move-result v1

    if-nez v1, :cond_2d

    goto/16 :goto_0

    :cond_2d
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_nextByte()B

    move-result v1

    invoke-virtual {v0, v1}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->parseNewName(B)Lcom/fasterxml/aalto/in/PName;

    move-result-object v1

    iput-object v1, v0, Lcom/fasterxml/aalto/in/XmlScanner;->_tokenName:Lcom/fasterxml/aalto/in/PName;

    const/4 v6, 0x4

    goto/16 :goto_9

    :pswitch_f
    const/16 v1, 0x44

    invoke-virtual {v0, v1}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->parseNewName(B)Lcom/fasterxml/aalto/in/PName;

    move-result-object v1

    iput-object v1, v0, Lcom/fasterxml/aalto/in/XmlScanner;->_tokenName:Lcom/fasterxml/aalto/in/PName;

    if-nez v1, :cond_2e

    iput v8, v0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_state:I

    return v3

    :cond_2e
    invoke-virtual {v1}, Lcom/fasterxml/aalto/in/PName;->getPrefixedName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v15, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2f

    invoke-virtual {v0, v8, v11}, Lcom/fasterxml/aalto/in/XmlScanner;->reportPrologProblem(ZLjava/lang/String;)V

    :cond_2f
    const/4 v1, 0x2

    :goto_11
    iput v1, v0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_state:I

    goto/16 :goto_0

    :cond_30
    :goto_12
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->asyncSkipSpace()Z

    move-result v1

    if-nez v1, :cond_31

    goto/16 :goto_0

    :cond_31
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_nextByte()B

    move-result v1

    if-eq v1, v6, :cond_6

    const-string v2, "expected \'>\' to end DTD"

    invoke-virtual {v0, v8, v1, v2}, Lcom/fasterxml/aalto/in/XmlScanner;->reportPrologUnexpChar(ZILjava/lang/String;)V

    goto/16 :goto_1

    :cond_32
    iget v1, v0, Lcom/fasterxml/aalto/in/XmlScanner;->_currToken:I

    return v1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_f
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private final handlePrologDeclStart(Z)I
    .locals 4

    iget v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v1, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    const/16 v2, 0x101

    if-lt v0, v1, :cond_0

    return v2

    :cond_0
    invoke-virtual {p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_nextByte()B

    move-result v0

    const/16 v1, 0x2d

    const/4 v3, 0x0

    if-ne v0, v1, :cond_1

    const/4 p1, 0x5

    iput p1, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_nextEvent:I

    iput v3, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_state:I

    invoke-virtual {p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->handleComment()I

    move-result p1

    return p1

    :cond_1
    const/16 v1, 0x44

    if-ne v0, v1, :cond_2

    const/16 p1, 0xb

    iput p1, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_nextEvent:I

    iput v3, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_state:I

    invoke-direct {p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->handleDTD()I

    move-result p1

    return p1

    :cond_2
    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->decodeCharForError(B)I

    move-result v0

    const-string v1, " (expected \'-\' for COMMENT)"

    invoke-virtual {p0, p1, v0, v1}, Lcom/fasterxml/aalto/in/XmlScanner;->reportPrologUnexpChar(ZILjava/lang/String;)V

    return v2
.end method

.method private handleXmlDeclaration()I
    .locals 16

    move-object/from16 v0, p0

    iget v1, v0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_pendingInput:I

    const/4 v2, -0x1

    const/16 v3, 0x101

    if-ne v1, v2, :cond_0

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->handlePartialCR()Z

    move-result v1

    if-nez v1, :cond_0

    return v3

    :cond_0
    :goto_0
    iget v1, v0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v2, v0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-ge v1, v2, :cond_41

    iget v1, v0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_state:I

    const/16 v2, 0x12

    const/16 v4, 0x13

    const-string v5, "encoding"

    const-string v7, "version"

    const/4 v10, 0x7

    const/16 v12, 0x3d

    const/16 v15, 0x10

    const-string v6, "standalone"

    const/4 v14, 0x0

    const-string v13, "Unexpected keyword \'"

    const/16 v9, 0x14

    const/16 v8, 0x3f

    const/4 v11, 0x1

    packed-switch v1, :pswitch_data_0

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->throwInternal()I

    goto :goto_0

    :pswitch_0
    iget-object v1, v0, Lcom/fasterxml/aalto/in/XmlScanner;->_textBuilder:Lcom/fasterxml/aalto/util/TextBuilder;

    invoke-virtual {v1}, Lcom/fasterxml/aalto/util/TextBuilder;->getBufferWithoutReset()[C

    move-result-object v1

    iget-object v5, v0, Lcom/fasterxml/aalto/in/XmlScanner;->_textBuilder:Lcom/fasterxml/aalto/util/TextBuilder;

    invoke-virtual {v5}, Lcom/fasterxml/aalto/util/TextBuilder;->getCurrentLength()I

    move-result v5

    invoke-virtual {v0, v1, v5}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->parseXmlDeclAttr([CI)Z

    move-result v1

    if-nez v1, :cond_2

    :cond_1
    :goto_1
    iput v2, v0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_state:I

    goto :goto_0

    :cond_2
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->verifyAndSetXmlStandalone()V

    iput v4, v0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_state:I

    iget v1, v0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v2, v0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-lt v1, v2, :cond_3

    goto :goto_0

    :cond_3
    :pswitch_1
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->asyncSkipSpace()Z

    move-result v1

    if-nez v1, :cond_4

    goto :goto_0

    :cond_4
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_nextByte()B

    move-result v1

    if-eq v1, v8, :cond_5

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_prevByte()B

    move-result v1

    invoke-virtual {v0, v1}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->decodeCharForError(B)I

    move-result v1

    const-string v2, " (expected \'?>\' to end xml declaration)"

    invoke-virtual {v0, v11, v1, v2}, Lcom/fasterxml/aalto/in/XmlScanner;->reportPrologUnexpChar(ZILjava/lang/String;)V

    :cond_5
    iput v9, v0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_state:I

    iget v1, v0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v2, v0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-lt v1, v2, :cond_6

    goto :goto_0

    :cond_6
    :pswitch_2
    const/4 v1, 0x0

    iput-object v1, v0, Lcom/fasterxml/aalto/in/XmlScanner;->_tokenName:Lcom/fasterxml/aalto/in/PName;

    iput v14, v0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_state:I

    iput v3, v0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_nextEvent:I

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_nextByte()B

    move-result v1

    const/16 v2, 0x3e

    if-eq v1, v2, :cond_7

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_prevByte()B

    move-result v1

    invoke-virtual {v0, v1}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->decodeCharForError(B)I

    move-result v1

    const-string v2, " (expected \'>\' to end xml declaration)"

    invoke-virtual {v0, v11, v1, v2}, Lcom/fasterxml/aalto/in/XmlScanner;->reportPrologUnexpChar(ZILjava/lang/String;)V

    :cond_7
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_activateEncoding()V

    return v10

    :pswitch_3
    invoke-direct/range {p0 .. p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_parseXmlDeclName()Lcom/fasterxml/aalto/in/PName;

    move-result-object v1

    iput-object v1, v0, Lcom/fasterxml/aalto/in/XmlScanner;->_tokenName:Lcom/fasterxml/aalto/in/PName;

    if-nez v1, :cond_8

    goto/16 :goto_0

    :cond_8
    invoke-virtual {v1, v6}, Lcom/fasterxml/aalto/in/PName;->hasPrefixedName(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_9

    const-string v1, "Unexpected keyword \'encoding\' in XML declaration: expected \'standalone\'"

    invoke-virtual {v0, v1}, Lcom/fasterxml/aalto/in/XmlScanner;->reportInputProblem(Ljava/lang/String;)V

    :cond_9
    iput v15, v0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_state:I

    iget v1, v0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v5, v0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-lt v1, v5, :cond_a

    goto/16 :goto_0

    :cond_a
    :pswitch_4
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->asyncSkipSpace()Z

    move-result v1

    if-nez v1, :cond_b

    goto/16 :goto_0

    :cond_b
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_nextByte()B

    move-result v1

    if-eq v1, v12, :cond_c

    invoke-virtual {v0, v1}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->decodeCharForError(B)I

    move-result v1

    const-string v5, " (expected \'=\' after \'standalone\' in xml declaration)"

    invoke-virtual {v0, v11, v1, v5}, Lcom/fasterxml/aalto/in/XmlScanner;->reportPrologUnexpChar(ZILjava/lang/String;)V

    :cond_c
    const/16 v1, 0x11

    iput v1, v0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_state:I

    iget v1, v0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v5, v0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-lt v1, v5, :cond_d

    goto/16 :goto_0

    :cond_d
    :pswitch_5
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->asyncSkipSpace()Z

    move-result v1

    if-nez v1, :cond_e

    goto/16 :goto_0

    :cond_e
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_nextByte()B

    move-result v1

    iput-byte v1, v0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_elemAttrQuote:B

    const/16 v5, 0x22

    if-eq v1, v5, :cond_f

    const/16 v5, 0x27

    if-eq v1, v5, :cond_f

    invoke-virtual {v0, v1}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->decodeCharForError(B)I

    move-result v1

    const-string v5, " (expected \'\"\' or \'\'\' in xml declaration for standalone value)"

    invoke-virtual {v0, v11, v1, v5}, Lcom/fasterxml/aalto/in/XmlScanner;->reportPrologUnexpChar(ZILjava/lang/String;)V

    :cond_f
    iget-object v1, v0, Lcom/fasterxml/aalto/in/XmlScanner;->_textBuilder:Lcom/fasterxml/aalto/util/TextBuilder;

    invoke-virtual {v1}, Lcom/fasterxml/aalto/util/TextBuilder;->resetWithEmpty()[C

    move-result-object v1

    iget v5, v0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v6, v0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-ge v5, v6, :cond_1

    invoke-virtual {v0, v1, v14}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->parseXmlDeclAttr([CI)Z

    move-result v1

    if-nez v1, :cond_10

    goto/16 :goto_1

    :cond_10
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->verifyAndSetXmlStandalone()V

    iput v4, v0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_state:I

    goto/16 :goto_0

    :pswitch_6
    const/16 v1, 0xd

    goto :goto_3

    :pswitch_7
    iget-object v1, v0, Lcom/fasterxml/aalto/in/XmlScanner;->_textBuilder:Lcom/fasterxml/aalto/util/TextBuilder;

    invoke-virtual {v1}, Lcom/fasterxml/aalto/util/TextBuilder;->getBufferWithoutReset()[C

    move-result-object v1

    iget-object v2, v0, Lcom/fasterxml/aalto/in/XmlScanner;->_textBuilder:Lcom/fasterxml/aalto/util/TextBuilder;

    invoke-virtual {v2}, Lcom/fasterxml/aalto/util/TextBuilder;->getCurrentLength()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->parseXmlDeclAttr([CI)Z

    move-result v1

    if-nez v1, :cond_12

    :cond_11
    :goto_2
    const/16 v1, 0xc

    goto/16 :goto_9

    :cond_12
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->verifyAndSetXmlEncoding()V

    const/16 v1, 0xd

    iput v1, v0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_state:I

    iget v2, v0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v4, v0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-lt v2, v4, :cond_13

    goto/16 :goto_0

    :cond_13
    :goto_3
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_nextByte()B

    move-result v2

    if-ne v2, v8, :cond_14

    goto/16 :goto_a

    :cond_14
    const/16 v4, 0x20

    if-eq v2, v4, :cond_16

    if-eq v2, v1, :cond_16

    const/16 v1, 0xa

    if-eq v2, v1, :cond_16

    const/16 v1, 0x9

    if-ne v2, v1, :cond_15

    goto :goto_4

    :cond_15
    invoke-virtual {v0, v2}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->decodeCharForError(B)I

    move-result v1

    const-string v2, " (expected space after encoding value in xml declaration)"

    invoke-virtual {v0, v11, v1, v2}, Lcom/fasterxml/aalto/in/XmlScanner;->reportPrologUnexpChar(ZILjava/lang/String;)V

    goto :goto_5

    :cond_16
    :goto_4
    const/16 v1, 0xe

    iput v1, v0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_state:I

    :goto_5
    iget v1, v0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v2, v0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-lt v1, v2, :cond_17

    goto/16 :goto_0

    :cond_17
    :pswitch_8
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->asyncSkipSpace()Z

    move-result v1

    if-nez v1, :cond_18

    goto/16 :goto_0

    :cond_18
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_nextByte()B

    move-result v1

    if-ne v1, v8, :cond_19

    goto/16 :goto_a

    :cond_19
    invoke-direct {v0, v1}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_parseNewXmlDeclName(B)Lcom/fasterxml/aalto/in/PName;

    move-result-object v1

    iput-object v1, v0, Lcom/fasterxml/aalto/in/XmlScanner;->_tokenName:Lcom/fasterxml/aalto/in/PName;

    if-nez v1, :cond_1a

    const/16 v1, 0xf

    goto/16 :goto_9

    :cond_1a
    invoke-virtual {v1, v6}, Lcom/fasterxml/aalto/in/PName;->hasPrefixedName(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1d

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, v0, Lcom/fasterxml/aalto/in/XmlScanner;->_tokenName:Lcom/fasterxml/aalto/in/PName;

    invoke-virtual {v2}, Lcom/fasterxml/aalto/in/PName;->getPrefixedName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "\' in XML declaration: expected \'standalone\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/fasterxml/aalto/in/XmlScanner;->reportInputProblem(Ljava/lang/String;)V

    goto :goto_6

    :pswitch_9
    invoke-direct/range {p0 .. p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_parseXmlDeclName()Lcom/fasterxml/aalto/in/PName;

    move-result-object v1

    iput-object v1, v0, Lcom/fasterxml/aalto/in/XmlScanner;->_tokenName:Lcom/fasterxml/aalto/in/PName;

    if-nez v1, :cond_1b

    goto/16 :goto_0

    :cond_1b
    invoke-virtual {v1, v5}, Lcom/fasterxml/aalto/in/PName;->hasPrefixedName(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1c

    const/16 v1, 0xa

    iput v1, v0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_state:I

    goto :goto_7

    :cond_1c
    iget-object v1, v0, Lcom/fasterxml/aalto/in/XmlScanner;->_tokenName:Lcom/fasterxml/aalto/in/PName;

    invoke-virtual {v1, v6}, Lcom/fasterxml/aalto/in/PName;->hasPrefixedName(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1e

    :cond_1d
    :goto_6
    iput v15, v0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_state:I

    goto/16 :goto_0

    :cond_1e
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, v0, Lcom/fasterxml/aalto/in/XmlScanner;->_tokenName:Lcom/fasterxml/aalto/in/PName;

    invoke-virtual {v2}, Lcom/fasterxml/aalto/in/PName;->getPrefixedName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "\' in XML declaration: expected \'encoding\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/fasterxml/aalto/in/XmlScanner;->reportInputProblem(Ljava/lang/String;)V

    :goto_7
    iget v1, v0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v2, v0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-lt v1, v2, :cond_1f

    goto/16 :goto_0

    :cond_1f
    :pswitch_a
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->asyncSkipSpace()Z

    move-result v1

    if-nez v1, :cond_20

    goto/16 :goto_0

    :cond_20
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_nextByte()B

    move-result v1

    if-eq v1, v12, :cond_21

    invoke-virtual {v0, v1}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->decodeCharForError(B)I

    move-result v1

    const-string v2, " (expected \'=\' after \'encoding\' in xml declaration)"

    invoke-virtual {v0, v11, v1, v2}, Lcom/fasterxml/aalto/in/XmlScanner;->reportPrologUnexpChar(ZILjava/lang/String;)V

    :cond_21
    const/16 v1, 0xb

    iput v1, v0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_state:I

    iget v1, v0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v2, v0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-lt v1, v2, :cond_22

    goto/16 :goto_0

    :cond_22
    :pswitch_b
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->asyncSkipSpace()Z

    move-result v1

    if-nez v1, :cond_23

    goto/16 :goto_0

    :cond_23
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_nextByte()B

    move-result v1

    iput-byte v1, v0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_elemAttrQuote:B

    const/16 v2, 0x22

    if-eq v1, v2, :cond_24

    const/16 v2, 0x27

    if-eq v1, v2, :cond_24

    invoke-virtual {v0, v1}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->decodeCharForError(B)I

    move-result v1

    const-string v2, " (expected \'\"\' or \'\'\' in xml declaration for encoding value)"

    invoke-virtual {v0, v11, v1, v2}, Lcom/fasterxml/aalto/in/XmlScanner;->reportPrologUnexpChar(ZILjava/lang/String;)V

    :cond_24
    const/16 v1, 0xc

    iput v1, v0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_state:I

    iget-object v1, v0, Lcom/fasterxml/aalto/in/XmlScanner;->_textBuilder:Lcom/fasterxml/aalto/util/TextBuilder;

    invoke-virtual {v1}, Lcom/fasterxml/aalto/util/TextBuilder;->resetWithEmpty()[C

    move-result-object v1

    iget v2, v0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v4, v0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-ge v2, v4, :cond_11

    invoke-virtual {v0, v1, v14}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->parseXmlDeclAttr([CI)Z

    move-result v1

    if-nez v1, :cond_25

    goto/16 :goto_2

    :cond_25
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->verifyAndSetXmlEncoding()V

    const/16 v1, 0xd

    goto :goto_9

    :pswitch_c
    iget-object v1, v0, Lcom/fasterxml/aalto/in/XmlScanner;->_textBuilder:Lcom/fasterxml/aalto/util/TextBuilder;

    invoke-virtual {v1}, Lcom/fasterxml/aalto/util/TextBuilder;->getBufferWithoutReset()[C

    move-result-object v1

    iget-object v2, v0, Lcom/fasterxml/aalto/in/XmlScanner;->_textBuilder:Lcom/fasterxml/aalto/util/TextBuilder;

    invoke-virtual {v2}, Lcom/fasterxml/aalto/util/TextBuilder;->getCurrentLength()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->parseXmlDeclAttr([CI)Z

    move-result v1

    if-nez v1, :cond_27

    :cond_26
    :goto_8
    const/4 v1, 0x6

    :goto_9
    iput v1, v0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_state:I

    goto/16 :goto_0

    :cond_27
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->verifyAndSetXmlVersion()V

    iput v10, v0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_state:I

    iget v1, v0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v2, v0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-lt v1, v2, :cond_28

    goto/16 :goto_0

    :cond_28
    :pswitch_d
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_nextByte()B

    move-result v1

    if-ne v1, v8, :cond_29

    :goto_a
    iput v9, v0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_state:I

    goto/16 :goto_0

    :cond_29
    const/16 v2, 0x20

    if-eq v1, v2, :cond_2b

    const/16 v2, 0xd

    if-eq v1, v2, :cond_2b

    const/16 v2, 0xa

    if-eq v1, v2, :cond_2b

    const/16 v2, 0x9

    if-ne v1, v2, :cond_2a

    goto :goto_b

    :cond_2a
    invoke-virtual {v0, v1}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->decodeCharForError(B)I

    move-result v1

    const-string v2, " (expected space after version value in xml declaration)"

    invoke-virtual {v0, v11, v1, v2}, Lcom/fasterxml/aalto/in/XmlScanner;->reportPrologUnexpChar(ZILjava/lang/String;)V

    goto :goto_c

    :cond_2b
    :goto_b
    const/16 v1, 0x8

    iput v1, v0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_state:I

    :goto_c
    iget v1, v0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v2, v0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-lt v1, v2, :cond_2c

    goto/16 :goto_0

    :cond_2c
    :pswitch_e
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->asyncSkipSpace()Z

    move-result v1

    if-nez v1, :cond_2d

    goto/16 :goto_0

    :cond_2d
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_nextByte()B

    move-result v1

    if-ne v1, v8, :cond_2e

    goto :goto_a

    :cond_2e
    invoke-direct {v0, v1}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_parseNewXmlDeclName(B)Lcom/fasterxml/aalto/in/PName;

    move-result-object v1

    iput-object v1, v0, Lcom/fasterxml/aalto/in/XmlScanner;->_tokenName:Lcom/fasterxml/aalto/in/PName;

    if-nez v1, :cond_2f

    const/16 v2, 0x9

    goto/16 :goto_1

    :cond_2f
    invoke-virtual {v1, v5}, Lcom/fasterxml/aalto/in/PName;->hasPrefixedName(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_30

    const/16 v1, 0xa

    goto :goto_9

    :cond_30
    iget-object v1, v0, Lcom/fasterxml/aalto/in/XmlScanner;->_tokenName:Lcom/fasterxml/aalto/in/PName;

    invoke-virtual {v1, v6}, Lcom/fasterxml/aalto/in/PName;->hasPrefixedName(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_31

    goto/16 :goto_6

    :cond_31
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, v0, Lcom/fasterxml/aalto/in/XmlScanner;->_tokenName:Lcom/fasterxml/aalto/in/PName;

    invoke-virtual {v2}, Lcom/fasterxml/aalto/in/PName;->getPrefixedName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "\' in XML declaration: expected \'encoding\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/fasterxml/aalto/in/XmlScanner;->reportInputProblem(Ljava/lang/String;)V

    goto/16 :goto_0

    :pswitch_f
    invoke-direct/range {p0 .. p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_parseXmlDeclName()Lcom/fasterxml/aalto/in/PName;

    move-result-object v1

    iput-object v1, v0, Lcom/fasterxml/aalto/in/XmlScanner;->_tokenName:Lcom/fasterxml/aalto/in/PName;

    if-nez v1, :cond_32

    goto/16 :goto_0

    :cond_32
    invoke-virtual {v1, v7}, Lcom/fasterxml/aalto/in/PName;->hasPrefixedName(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_33

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, v0, Lcom/fasterxml/aalto/in/XmlScanner;->_tokenName:Lcom/fasterxml/aalto/in/PName;

    invoke-virtual {v2}, Lcom/fasterxml/aalto/in/PName;->getPrefixedName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "\' in XML declaration: expected \'version\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/fasterxml/aalto/in/XmlScanner;->reportInputProblem(Ljava/lang/String;)V

    :cond_33
    const/4 v1, 0x4

    iput v1, v0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_state:I

    iget v1, v0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v2, v0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-lt v1, v2, :cond_34

    goto/16 :goto_0

    :cond_34
    :pswitch_10
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->asyncSkipSpace()Z

    move-result v1

    if-nez v1, :cond_35

    goto/16 :goto_0

    :cond_35
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_nextByte()B

    move-result v1

    if-eq v1, v12, :cond_36

    invoke-virtual {v0, v1}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->decodeCharForError(B)I

    move-result v1

    const-string v2, " (expected \'=\' after \'version\' in xml declaration)"

    invoke-virtual {v0, v11, v1, v2}, Lcom/fasterxml/aalto/in/XmlScanner;->reportPrologUnexpChar(ZILjava/lang/String;)V

    :cond_36
    const/4 v1, 0x5

    iput v1, v0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_state:I

    iget v1, v0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v2, v0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-lt v1, v2, :cond_37

    goto/16 :goto_0

    :cond_37
    :pswitch_11
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->asyncSkipSpace()Z

    move-result v1

    if-nez v1, :cond_38

    goto/16 :goto_0

    :cond_38
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_nextByte()B

    move-result v1

    iput-byte v1, v0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_elemAttrQuote:B

    const/16 v2, 0x22

    if-eq v1, v2, :cond_39

    const/16 v2, 0x27

    if-eq v1, v2, :cond_39

    invoke-virtual {v0, v1}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->decodeCharForError(B)I

    move-result v1

    const-string v2, " (expected \'\"\' or \'\'\' in xml declaration for version value)"

    invoke-virtual {v0, v11, v1, v2}, Lcom/fasterxml/aalto/in/XmlScanner;->reportPrologUnexpChar(ZILjava/lang/String;)V

    :cond_39
    iget-object v1, v0, Lcom/fasterxml/aalto/in/XmlScanner;->_textBuilder:Lcom/fasterxml/aalto/util/TextBuilder;

    invoke-virtual {v1}, Lcom/fasterxml/aalto/util/TextBuilder;->resetWithEmpty()[C

    move-result-object v1

    iget v2, v0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v4, v0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-ge v2, v4, :cond_26

    invoke-virtual {v0, v1, v14}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->parseXmlDeclAttr([CI)Z

    move-result v1

    if-nez v1, :cond_3a

    goto/16 :goto_8

    :cond_3a
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->verifyAndSetXmlVersion()V

    iput v10, v0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_state:I

    goto/16 :goto_0

    :pswitch_12
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_nextByte()B

    move-result v1

    const/16 v2, 0x20

    if-eq v1, v2, :cond_3c

    const/16 v2, 0xd

    if-eq v1, v2, :cond_3c

    const/16 v2, 0xa

    if-eq v1, v2, :cond_3c

    const/16 v2, 0x9

    if-ne v1, v2, :cond_3b

    goto :goto_d

    :cond_3b
    invoke-virtual {v0, v1}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->decodeCharForError(B)I

    move-result v1

    const-string v2, " (expected space after \'xml\' in xml declaration)"

    invoke-virtual {v0, v11, v1, v2}, Lcom/fasterxml/aalto/in/XmlScanner;->reportPrologUnexpChar(ZILjava/lang/String;)V

    goto :goto_e

    :cond_3c
    :goto_d
    const/4 v1, 0x2

    iput v1, v0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_state:I

    :goto_e
    iget v1, v0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v2, v0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-lt v1, v2, :cond_3d

    goto/16 :goto_0

    :cond_3d
    :pswitch_13
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->asyncSkipSpace()Z

    move-result v1

    if-nez v1, :cond_3e

    goto/16 :goto_0

    :cond_3e
    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_nextByte()B

    move-result v1

    invoke-direct {v0, v1}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_parseNewXmlDeclName(B)Lcom/fasterxml/aalto/in/PName;

    move-result-object v1

    iput-object v1, v0, Lcom/fasterxml/aalto/in/XmlScanner;->_tokenName:Lcom/fasterxml/aalto/in/PName;

    if-nez v1, :cond_3f

    const/4 v1, 0x3

    goto/16 :goto_9

    :cond_3f
    invoke-virtual {v1, v7}, Lcom/fasterxml/aalto/in/PName;->hasPrefixedName(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_40

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, v0, Lcom/fasterxml/aalto/in/XmlScanner;->_tokenName:Lcom/fasterxml/aalto/in/PName;

    invoke-virtual {v2}, Lcom/fasterxml/aalto/in/PName;->getPrefixedName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "\' in XML declaration: expected \'version\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/fasterxml/aalto/in/XmlScanner;->reportInputProblem(Ljava/lang/String;)V

    :cond_40
    const/4 v1, 0x4

    goto/16 :goto_9

    :cond_41
    return v3

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_12
        :pswitch_13
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_7
        :pswitch_6
        :pswitch_8
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private final parseDtdId([CIZ)Z
    .locals 7

    iget-byte v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_elemAttrQuote:B

    :goto_0
    iget v1, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v2, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    const/4 v3, 0x0

    if-ge v1, v2, :cond_4

    invoke-virtual {p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_nextByte()B

    move-result v1

    and-int/lit16 v1, v1, 0xff

    const/4 v2, 0x1

    if-ne v1, v0, :cond_0

    iget-object p1, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_textBuilder:Lcom/fasterxml/aalto/util/TextBuilder;

    invoke-virtual {p1, p2}, Lcom/fasterxml/aalto/util/TextBuilder;->setCurrentLength(I)V

    return v2

    :cond_0
    if-nez p3, :cond_2

    invoke-virtual {p0, v1}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->validPublicIdChar(I)Z

    move-result v4

    if-nez v4, :cond_2

    int-to-byte v4, v1

    invoke-virtual {p0, v4}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->decodeCharForError(B)I

    move-result v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, " (not valid in "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-eqz p3, :cond_1

    const-string v6, "SYSTEM"

    goto :goto_1

    :cond_1
    const-string v6, "PUBLIC"

    :goto_1
    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, " ID)"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v2, v4, v5}, Lcom/fasterxml/aalto/in/XmlScanner;->reportPrologUnexpChar(ZILjava/lang/String;)V

    :cond_2
    array-length v2, p1

    if-lt p2, v2, :cond_3

    iget-object p1, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_textBuilder:Lcom/fasterxml/aalto/util/TextBuilder;

    invoke-virtual {p1}, Lcom/fasterxml/aalto/util/TextBuilder;->finishCurrentSegment()[C

    move-result-object p1

    move p2, v3

    :cond_3
    add-int/lit8 v2, p2, 0x1

    int-to-char v1, v1

    aput-char v1, p1, p2

    move p2, v2

    goto :goto_0

    :cond_4
    iget-object p1, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_textBuilder:Lcom/fasterxml/aalto/util/TextBuilder;

    invoke-virtual {p1, p2}, Lcom/fasterxml/aalto/util/TextBuilder;->setCurrentLength(I)V

    return v3
.end method

.method private final startXmlDeclaration()Ljava/lang/Boolean;
    .locals 8

    iget v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v1, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    const/4 v2, 0x0

    if-lt v0, v1, :cond_0

    return-object v2

    :cond_0
    iget v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_pendingInput:I

    const/4 v1, -0x5

    const/4 v3, -0x6

    const/4 v4, 0x0

    const/4 v5, 0x1

    if-ne v0, v1, :cond_2

    invoke-virtual {p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_currentByte()B

    move-result v0

    const/16 v1, 0x3f

    if-eq v0, v1, :cond_1

    iput v4, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_pendingInput:I

    const/4 v0, 0x2

    iput v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_state:I

    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    return-object v0

    :cond_1
    iget v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    add-int/2addr v0, v5

    iput v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iput v3, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_pendingInput:I

    iget v1, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-lt v0, v1, :cond_2

    return-object v2

    :cond_2
    iget v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_pendingInput:I

    const/4 v1, 0x3

    const-string v6, "xml"

    const/4 v7, -0x7

    if-ne v0, v3, :cond_4

    invoke-virtual {p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_nextByte()B

    move-result v0

    invoke-direct {p0, v0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_parseNewXmlDeclName(B)Lcom/fasterxml/aalto/in/PName;

    move-result-object v0

    iput-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_tokenName:Lcom/fasterxml/aalto/in/PName;

    if-nez v0, :cond_3

    iput v7, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_pendingInput:I

    return-object v2

    :cond_3
    invoke-virtual {v0}, Lcom/fasterxml/aalto/in/PName;->getPrefixedName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    iput v4, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_pendingInput:I

    iput v5, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_state:I

    iput v1, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_nextEvent:I

    iget-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_tokenName:Lcom/fasterxml/aalto/in/PName;

    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->checkPITargetName(Lcom/fasterxml/aalto/in/PName;)V

    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    return-object v0

    :cond_4
    if-ne v0, v7, :cond_6

    invoke-direct {p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_parseXmlDeclName()Lcom/fasterxml/aalto/in/PName;

    move-result-object v0

    iput-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_tokenName:Lcom/fasterxml/aalto/in/PName;

    if-nez v0, :cond_5

    return-object v2

    :cond_5
    invoke-virtual {v0}, Lcom/fasterxml/aalto/in/PName;->getPrefixedName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    iput v4, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_pendingInput:I

    iput v5, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_state:I

    iput v1, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_nextEvent:I

    iget-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_tokenName:Lcom/fasterxml/aalto/in/PName;

    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->checkPITargetName(Lcom/fasterxml/aalto/in/PName;)V

    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    return-object v0

    :cond_6
    invoke-virtual {p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->throwInternal()I

    :cond_7
    iput v4, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_pendingInput:I

    const/4 v0, 0x7

    iput v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_nextEvent:I

    iput v5, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_state:I

    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    return-object v0
.end method


# virtual methods
.method public _activateEncoding()V
    .locals 1

    iget-object v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_symbols:Lcom/fasterxml/aalto/in/ByteBasedPNameTable;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_config:Lcom/fasterxml/aalto/in/ReaderConfig;

    invoke-virtual {v0}, Lcom/fasterxml/aalto/in/ReaderConfig;->getCharTypes()Lcom/fasterxml/aalto/util/XmlCharTypes;

    move-result-object v0

    iput-object v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_charTypes:Lcom/fasterxml/aalto/util/XmlCharTypes;

    iget-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_config:Lcom/fasterxml/aalto/in/ReaderConfig;

    invoke-virtual {v0}, Lcom/fasterxml/aalto/in/ReaderConfig;->getBBSymbols()Lcom/fasterxml/aalto/in/ByteBasedPNameTable;

    move-result-object v0

    iput-object v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_symbols:Lcom/fasterxml/aalto/in/ByteBasedPNameTable;

    :cond_0
    return-void
.end method

.method public _closeSource()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_endOfInput:Z

    return-void
.end method

.method public abstract _currentByte()B
.end method

.method public final _findXmlDeclName(II)Lcom/fasterxml/aalto/in/PName;
    .locals 4

    iget v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_quadCount:I

    if-nez p2, :cond_0

    iget-object p1, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_quadBuffer:[I

    add-int/lit8 v0, v0, -0x1

    aget p1, p1, v0

    :cond_0
    const/4 v1, 0x1

    if-eqz v0, :cond_3

    const/4 v2, 0x0

    if-eq v0, v1, :cond_2

    const/4 v3, 0x2

    if-eq v0, v3, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_quadBuffer:[I

    aget v2, v0, v2

    aget v0, v0, v1

    invoke-static {v2, v0, p1}, Lcom/fasterxml/aalto/async/AsyncXmlDeclHelper;->find(III)Lcom/fasterxml/aalto/in/PName;

    move-result-object v0

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_quadBuffer:[I

    aget v0, v0, v2

    invoke-static {v0, p1}, Lcom/fasterxml/aalto/async/AsyncXmlDeclHelper;->find(II)Lcom/fasterxml/aalto/in/PName;

    move-result-object v0

    goto :goto_0

    :cond_3
    invoke-static {p1}, Lcom/fasterxml/aalto/async/AsyncXmlDeclHelper;->find(I)Lcom/fasterxml/aalto/in/PName;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_4

    iget p1, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    sub-int/2addr p1, v1

    iput p1, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    return-object v0

    :cond_4
    invoke-virtual {p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_activateEncoding()V

    invoke-virtual {p0, p1, p2}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->findPName(II)Lcom/fasterxml/aalto/in/PName;

    move-result-object p1

    return-object p1
.end method

.method public abstract _nextByte()B
.end method

.method public abstract _prevByte()B
.end method

.method public _releaseBuffers()V
    .locals 2

    invoke-super {p0}, Lcom/fasterxml/aalto/in/XmlScanner;->_releaseBuffers()V

    iget-object v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_symbols:Lcom/fasterxml/aalto/in/ByteBasedPNameTable;

    invoke-virtual {v0}, Lcom/fasterxml/aalto/in/ByteBasedPNameTable;->maybeDirty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_config:Lcom/fasterxml/aalto/in/ReaderConfig;

    iget-object v1, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_symbols:Lcom/fasterxml/aalto/in/ByteBasedPNameTable;

    invoke-virtual {v0, v1}, Lcom/fasterxml/aalto/in/ReaderConfig;->updateBBSymbols(Lcom/fasterxml/aalto/in/ByteBasedPNameTable;)V

    :cond_0
    return-void
.end method

.method public _startDocumentNoXmlDecl()I
    .locals 1

    invoke-virtual {p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_activateEncoding()V

    const/4 v0, 0x7

    iput v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_currToken:I

    return v0
.end method

.method public final addPName(Lcom/fasterxml/aalto/in/ByteBasedPNameTable;I[III)Lcom/fasterxml/aalto/in/PName;
    .locals 7

    iget-object v2, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_charTypes:Lcom/fasterxml/aalto/util/XmlCharTypes;

    move-object v0, p0

    move-object v1, p1

    move v3, p2

    move-object v4, p3

    move v5, p4

    move v6, p5

    invoke-virtual/range {v0 .. v6}, Lcom/fasterxml/aalto/in/ByteBasedScanner;->addUTFPName(Lcom/fasterxml/aalto/in/ByteBasedPNameTable;Lcom/fasterxml/aalto/util/XmlCharTypes;I[III)Lcom/fasterxml/aalto/in/PName;

    move-result-object p1

    return-object p1
.end method

.method public abstract asyncSkipSpace()Z
.end method

.method public checkPITargetName(Lcom/fasterxml/aalto/in/PName;)V
    .locals 3

    invoke-virtual {p1}, Lcom/fasterxml/aalto/in/PName;->getLocalName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x3

    if-ne v1, v2, :cond_0

    const-string v1, "xml"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/fasterxml/aalto/in/PName;->hasPrefix()Z

    move-result p1

    if-nez p1, :cond_0

    sget-object p1, Lcom/fasterxml/aalto/impl/ErrorConsts;->ERR_WF_PI_XML_TARGET:Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/fasterxml/aalto/in/XmlScanner;->reportInputProblem(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public decodeCharForError(B)I
    .locals 0

    return p1
.end method

.method public endOfInput()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_endOfInput:Z

    return-void
.end method

.method public final findPName(II)Lcom/fasterxml/aalto/in/PName;
    .locals 8

    iget v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    const/4 v1, 0x1

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_quadCount:I

    if-nez p2, :cond_0

    iget-object p1, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_quadBuffer:[I

    add-int/lit8 v0, v0, -0x1

    aget p1, p1, v0

    const/4 p2, 0x4

    :cond_0
    move v7, p2

    if-gt v0, v1, :cond_4

    const/4 p2, 0x0

    if-nez v0, :cond_2

    invoke-static {p1}, Lcom/fasterxml/aalto/in/ByteBasedPNameTable;->calcHash(I)I

    move-result v4

    iget-object v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_symbols:Lcom/fasterxml/aalto/in/ByteBasedPNameTable;

    invoke-virtual {v0, v4, p1, p2}, Lcom/fasterxml/aalto/in/ByteBasedPNameTable;->findSymbol(III)Lcom/fasterxml/aalto/in/ByteBasedPName;

    move-result-object v0

    if-nez v0, :cond_1

    iget-object v5, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_quadBuffer:[I

    aput p1, v5, p2

    iget-object v3, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_symbols:Lcom/fasterxml/aalto/in/ByteBasedPNameTable;

    const/4 v6, 0x1

    move-object v2, p0

    invoke-virtual/range {v2 .. v7}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->addPName(Lcom/fasterxml/aalto/in/ByteBasedPNameTable;I[III)Lcom/fasterxml/aalto/in/PName;

    move-result-object v0

    :cond_1
    return-object v0

    :cond_2
    iget-object v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_quadBuffer:[I

    aget p2, v0, p2

    invoke-static {p2, p1}, Lcom/fasterxml/aalto/in/ByteBasedPNameTable;->calcHash(II)I

    move-result v4

    iget-object v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_symbols:Lcom/fasterxml/aalto/in/ByteBasedPNameTable;

    invoke-virtual {v0, v4, p2, p1}, Lcom/fasterxml/aalto/in/ByteBasedPNameTable;->findSymbol(III)Lcom/fasterxml/aalto/in/ByteBasedPName;

    move-result-object p2

    if-nez p2, :cond_3

    iget-object v5, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_quadBuffer:[I

    aput p1, v5, v1

    iget-object v3, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_symbols:Lcom/fasterxml/aalto/in/ByteBasedPNameTable;

    const/4 v6, 0x2

    move-object v2, p0

    invoke-virtual/range {v2 .. v7}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->addPName(Lcom/fasterxml/aalto/in/ByteBasedPNameTable;I[III)Lcom/fasterxml/aalto/in/PName;

    move-result-object p2

    :cond_3
    return-object p2

    :cond_4
    iget-object p2, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_quadBuffer:[I

    array-length v1, p2

    if-lt v0, v1, :cond_5

    array-length v1, p2

    invoke-static {p2, v1}, Lcom/fasterxml/aalto/util/DataUtil;->growArrayBy([II)[I

    move-result-object p2

    iput-object p2, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_quadBuffer:[I

    :cond_5
    iget-object p2, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_quadBuffer:[I

    add-int/lit8 v6, v0, 0x1

    aput p1, p2, v0

    invoke-static {p2, v6}, Lcom/fasterxml/aalto/in/ByteBasedPNameTable;->calcHash([II)I

    move-result v4

    iget-object p1, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_symbols:Lcom/fasterxml/aalto/in/ByteBasedPNameTable;

    iget-object p2, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_quadBuffer:[I

    invoke-virtual {p1, v4, p2, v6}, Lcom/fasterxml/aalto/in/ByteBasedPNameTable;->findSymbol(I[II)Lcom/fasterxml/aalto/in/ByteBasedPName;

    move-result-object p1

    if-nez p1, :cond_6

    iget-object v3, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_symbols:Lcom/fasterxml/aalto/in/ByteBasedPNameTable;

    iget-object v5, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_quadBuffer:[I

    move-object v2, p0

    invoke-virtual/range {v2 .. v7}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->addPName(Lcom/fasterxml/aalto/in/ByteBasedPNameTable;I[III)Lcom/fasterxml/aalto/in/PName;

    move-result-object p1

    :cond_6
    return-object p1
.end method

.method public finishCData()V
    .locals 0

    invoke-virtual {p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->throwInternal()I

    return-void
.end method

.method public abstract finishCharacters()V
.end method

.method public finishComment()V
    .locals 0

    invoke-virtual {p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->throwInternal()I

    return-void
.end method

.method public finishDTD(Z)V
    .locals 0

    invoke-virtual {p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->throwInternal()I

    return-void
.end method

.method public finishPI()V
    .locals 0

    invoke-virtual {p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->throwInternal()I

    return-void
.end method

.method public finishSpace()V
    .locals 0

    invoke-virtual {p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->throwInternal()I

    return-void
.end method

.method public final finishToken()V
    .locals 2

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_tokenIncomplete:Z

    iget v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_currToken:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_5

    const/4 v1, 0x4

    if-eq v0, v1, :cond_4

    const/4 v1, 0x5

    if-eq v0, v1, :cond_3

    const/4 v1, 0x6

    if-eq v0, v1, :cond_2

    const/16 v1, 0xb

    if-eq v0, v1, :cond_1

    const/16 v1, 0xc

    if-eq v0, v1, :cond_0

    invoke-static {}, Lcom/fasterxml/aalto/impl/ErrorConsts;->throwInternalError()V

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->finishCData()V

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->finishDTD(Z)V

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->finishSpace()V

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->finishComment()V

    goto :goto_0

    :cond_4
    invoke-virtual {p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->finishCharacters()V

    goto :goto_0

    :cond_5
    invoke-virtual {p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->finishPI()V

    :goto_0
    return-void
.end method

.method public abstract handleAttrValue()Z
.end method

.method public abstract handleComment()I
.end method

.method public abstract handleDTDInternalSubset(Z)Z
.end method

.method public abstract handleNsDecl()Z
.end method

.method public abstract handlePI()I
.end method

.method public abstract handlePartialCR()Z
.end method

.method public abstract handleStartElement()I
.end method

.method public abstract handleStartElementStart(B)I
.end method

.method public loadMore()Z
    .locals 1

    invoke-virtual {p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->throwInternal()I

    const/4 v0, 0x0

    return v0
.end method

.method public final nextFromProlog(Z)I
    .locals 7

    iget v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_currToken:I

    const/4 v1, 0x7

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x3

    const/16 v5, 0x101

    if-eq v0, v5, :cond_1

    invoke-virtual {p0}, Lcom/fasterxml/aalto/in/ByteBasedScanner;->setStartLocation()V

    iget v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_currToken:I

    if-ne v0, v1, :cond_0

    iput v5, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_currToken:I

    iget-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_tokenName:Lcom/fasterxml/aalto/in/PName;

    if-eqz v0, :cond_1

    iput v4, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_nextEvent:I

    iput v3, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_state:I

    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->checkPITargetName(Lcom/fasterxml/aalto/in/PName;)V

    invoke-virtual {p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->handlePI()I

    move-result p1

    return p1

    :cond_0
    iput v5, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_nextEvent:I

    iput v5, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_currToken:I

    iput v2, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_state:I

    :cond_1
    iget v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_nextEvent:I

    if-ne v0, v5, :cond_19

    iget v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_state:I

    const/16 v1, 0x3c

    if-ne v0, v3, :cond_9

    iget p1, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-lt p1, v0, :cond_2

    iget p1, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_currToken:I

    return p1

    :cond_2
    iget p1, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_pendingInput:I

    if-eqz p1, :cond_5

    invoke-direct {p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->startXmlDeclaration()Ljava/lang/Boolean;

    move-result-object p1

    if-nez p1, :cond_3

    return v5

    :cond_3
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    if-ne p1, v0, :cond_4

    invoke-virtual {p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_startDocumentNoXmlDecl()I

    move-result p1

    return p1

    :cond_4
    invoke-direct {p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->handleXmlDeclaration()I

    move-result p1

    return p1

    :cond_5
    invoke-virtual {p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_currentByte()B

    move-result p1

    if-ne p1, v1, :cond_8

    iget p1, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    add-int/2addr p1, v3

    iput p1, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    const/4 p1, -0x5

    iput p1, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_pendingInput:I

    invoke-direct {p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->startXmlDeclaration()Ljava/lang/Boolean;

    move-result-object p1

    if-nez p1, :cond_6

    return v5

    :cond_6
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    if-ne p1, v0, :cond_7

    invoke-virtual {p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_startDocumentNoXmlDecl()I

    move-result p1

    return p1

    :cond_7
    invoke-direct {p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->handleXmlDeclaration()I

    move-result p1

    return p1

    :cond_8
    iput v2, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_state:I

    invoke-virtual {p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_startDocumentNoXmlDecl()I

    move-result p1

    return p1

    :cond_9
    iget v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_pendingInput:I

    if-eqz v0, :cond_a

    invoke-virtual {p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->handlePartialCR()Z

    move-result v0

    if-nez v0, :cond_a

    iget p1, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_currToken:I

    return p1

    :cond_a
    :goto_0
    iget v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_state:I

    const/4 v3, 0x2

    if-nez v0, :cond_11

    iget v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v5, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    const/4 v6, -0x1

    if-lt v0, v5, :cond_c

    iget-boolean p1, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_endOfInput:Z

    if-eqz p1, :cond_b

    invoke-virtual {p0}, Lcom/fasterxml/aalto/in/ByteBasedScanner;->setStartLocation()V

    return v6

    :cond_b
    iget p1, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_currToken:I

    return p1

    :cond_c
    invoke-virtual {p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_nextByte()B

    move-result v0

    if-ne v0, v1, :cond_d

    iput v3, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_state:I

    goto :goto_2

    :cond_d
    const/16 v3, 0x20

    if-eq v0, v3, :cond_f

    const/16 v3, 0xd

    if-eq v0, v3, :cond_f

    const/16 v3, 0xa

    if-eq v0, v3, :cond_f

    const/16 v3, 0x9

    if-ne v0, v3, :cond_e

    goto :goto_1

    :cond_e
    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->decodeCharForError(B)I

    move-result v0

    const/4 v3, 0x0

    invoke-virtual {p0, p1, v0, v3}, Lcom/fasterxml/aalto/in/XmlScanner;->reportPrologUnexpChar(ZILjava/lang/String;)V

    goto :goto_0

    :cond_f
    :goto_1
    invoke-virtual {p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->asyncSkipSpace()Z

    move-result v0

    if-nez v0, :cond_a

    iget-boolean p1, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_endOfInput:Z

    if-eqz p1, :cond_10

    invoke-virtual {p0}, Lcom/fasterxml/aalto/in/ByteBasedScanner;->setStartLocation()V

    return v6

    :cond_10
    iget p1, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_currToken:I

    return p1

    :cond_11
    :goto_2
    iget v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_state:I

    if-ne v0, v3, :cond_17

    iget v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v1, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    if-lt v0, v1, :cond_12

    iget p1, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_currToken:I

    return p1

    :cond_12
    invoke-virtual {p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_nextByte()B

    move-result v0

    const/16 v1, 0x21

    if-ne v0, v1, :cond_13

    iput v4, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_state:I

    invoke-direct {p0, p1}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->handlePrologDeclStart(Z)I

    move-result p1

    return p1

    :cond_13
    const/16 v1, 0x3f

    if-ne v0, v1, :cond_14

    iput v4, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_nextEvent:I

    iput v2, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_state:I

    invoke-virtual {p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->handlePI()I

    move-result p1

    return p1

    :cond_14
    const/16 v1, 0x2f

    if-eq v0, v1, :cond_15

    if-nez p1, :cond_16

    :cond_15
    invoke-virtual {p0, p1, v0}, Lcom/fasterxml/aalto/in/XmlScanner;->reportPrologUnexpElement(ZI)V

    :cond_16
    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->handleStartElementStart(B)I

    move-result p1

    return p1

    :cond_17
    if-ne v0, v4, :cond_18

    invoke-direct {p0, p1}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->handlePrologDeclStart(Z)I

    move-result p1

    return p1

    :cond_18
    invoke-virtual {p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->throwInternal()I

    move-result p1

    return p1

    :cond_19
    if-eq v0, v3, :cond_1e

    if-eq v0, v4, :cond_1d

    const/4 p1, 0x5

    if-eq v0, p1, :cond_1c

    if-eq v0, v1, :cond_1b

    const/16 p1, 0xb

    if-eq v0, p1, :cond_1a

    invoke-virtual {p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->throwInternal()I

    move-result p1

    return p1

    :cond_1a
    invoke-direct {p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->handleDTD()I

    move-result p1

    return p1

    :cond_1b
    invoke-direct {p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->handleXmlDeclaration()I

    move-result p1

    return p1

    :cond_1c
    invoke-virtual {p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->handleComment()I

    move-result p1

    return p1

    :cond_1d
    invoke-virtual {p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->handlePI()I

    move-result p1

    return p1

    :cond_1e
    invoke-virtual {p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->handleStartElement()I

    move-result p1

    return p1
.end method

.method public abstract parseNewName(B)Lcom/fasterxml/aalto/in/PName;
.end method

.method public abstract parsePName()Lcom/fasterxml/aalto/in/PName;
.end method

.method public parseXmlDeclAttr([CI)Z
    .locals 6

    iget-byte v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_elemAttrQuote:B

    :goto_0
    iget v1, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v2, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputEnd:I

    const/4 v3, 0x0

    if-ge v1, v2, :cond_4

    invoke-virtual {p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_nextByte()B

    move-result v1

    and-int/lit16 v1, v1, 0xff

    const/4 v2, 0x1

    if-ne v1, v0, :cond_0

    iget-object p1, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_textBuilder:Lcom/fasterxml/aalto/util/TextBuilder;

    invoke-virtual {p1, p2}, Lcom/fasterxml/aalto/util/TextBuilder;->setCurrentLength(I)V

    return v2

    :cond_0
    const/16 v4, 0x20

    if-le v1, v4, :cond_1

    const/16 v4, 0x7a

    if-le v1, v4, :cond_2

    :cond_1
    int-to-byte v4, v1

    invoke-virtual {p0, v4}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->decodeCharForError(B)I

    move-result v4

    const-string v5, " (not valid in XML pseudo-attribute values)"

    invoke-virtual {p0, v2, v4, v5}, Lcom/fasterxml/aalto/in/XmlScanner;->reportPrologUnexpChar(ZILjava/lang/String;)V

    :cond_2
    array-length v2, p1

    if-lt p2, v2, :cond_3

    iget-object p1, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_textBuilder:Lcom/fasterxml/aalto/util/TextBuilder;

    invoke-virtual {p1}, Lcom/fasterxml/aalto/util/TextBuilder;->finishCurrentSegment()[C

    move-result-object p1

    move p2, v3

    :cond_3
    add-int/lit8 v2, p2, 0x1

    int-to-char v1, v1

    aput-char v1, p1, p2

    move p2, v2

    goto :goto_0

    :cond_4
    iget-object p1, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_textBuilder:Lcom/fasterxml/aalto/util/TextBuilder;

    invoke-virtual {p1, p2}, Lcom/fasterxml/aalto/util/TextBuilder;->setCurrentLength(I)V

    return v3
.end method

.method public reportInvalidOther(II)V
    .locals 0

    iput p2, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    invoke-virtual {p0, p1}, Lcom/fasterxml/aalto/in/ByteBasedScanner;->reportInvalidOther(I)V

    return-void
.end method

.method public skipCData()V
    .locals 0

    invoke-virtual {p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->throwInternal()I

    return-void
.end method

.method public abstract skipCharacters()Z
.end method

.method public skipComment()V
    .locals 0

    invoke-virtual {p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->throwInternal()I

    return-void
.end method

.method public skipPI()V
    .locals 0

    invoke-virtual {p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->throwInternal()I

    return-void
.end method

.method public skipSpace()V
    .locals 0

    invoke-virtual {p0}, Lcom/fasterxml/aalto/async/AsyncByteScanner;->throwInternal()I

    return-void
.end method

.method public abstract startCharacters(B)I
.end method

.method public throwInternal()I
    .locals 2

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Internal error: should never execute this code path"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public validPublicIdChar(I)Z
    .locals 1

    const/16 v0, 0xa

    if-eq p1, v0, :cond_7

    const/16 v0, 0xd

    if-eq p1, v0, :cond_7

    const/16 v0, 0x20

    if-eq p1, v0, :cond_7

    const/16 v0, 0x30

    if-lt p1, v0, :cond_0

    const/16 v0, 0x39

    if-le p1, v0, :cond_7

    :cond_0
    const/16 v0, 0x40

    if-lt p1, v0, :cond_1

    const/16 v0, 0x5a

    if-le p1, v0, :cond_7

    :cond_1
    const/16 v0, 0x61

    if-lt p1, v0, :cond_2

    const/16 v0, 0x7a

    if-le p1, v0, :cond_7

    :cond_2
    const/16 v0, 0x21

    if-eq p1, v0, :cond_7

    const/16 v0, 0x23

    if-lt p1, v0, :cond_3

    const/16 v0, 0x25

    if-le p1, v0, :cond_7

    :cond_3
    const/16 v0, 0x27

    if-lt p1, v0, :cond_4

    const/16 v0, 0x2f

    if-le p1, v0, :cond_7

    :cond_4
    const/16 v0, 0x3a

    if-lt p1, v0, :cond_5

    const/16 v0, 0x3b

    if-le p1, v0, :cond_7

    :cond_5
    const/16 v0, 0x3d

    if-eq p1, v0, :cond_7

    const/16 v0, 0x3f

    if-eq p1, v0, :cond_7

    const/16 v0, 0x5f

    if-ne p1, v0, :cond_6

    goto :goto_0

    :cond_6
    const/4 p1, 0x0

    goto :goto_1

    :cond_7
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method

.method public verifyAndAppendEntityCharacter(I)V
    .locals 3

    invoke-virtual {p0, p1}, Lcom/fasterxml/aalto/in/XmlScanner;->verifyXmlChar(I)V

    shr-int/lit8 v0, p1, 0x10

    if-eqz v0, :cond_0

    const/high16 v0, 0x10000

    sub-int/2addr p1, v0

    iget-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_textBuilder:Lcom/fasterxml/aalto/util/TextBuilder;

    shr-int/lit8 v1, p1, 0xa

    const v2, 0xd800

    or-int/2addr v1, v2

    int-to-char v1, v1

    invoke-virtual {v0, v1}, Lcom/fasterxml/aalto/util/TextBuilder;->append(C)V

    and-int/lit16 p1, p1, 0x3ff

    const v0, 0xdc00

    or-int/2addr p1, v0

    :cond_0
    iget-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_textBuilder:Lcom/fasterxml/aalto/util/TextBuilder;

    int-to-char p1, p1

    invoke-virtual {v0, p1}, Lcom/fasterxml/aalto/util/TextBuilder;->append(C)V

    return-void
.end method

.method public verifyAndSetPublicId()V
    .locals 1

    iget-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_textBuilder:Lcom/fasterxml/aalto/util/TextBuilder;

    invoke-virtual {v0}, Lcom/fasterxml/aalto/util/TextBuilder;->contentsAsString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_publicId:Ljava/lang/String;

    return-void
.end method

.method public verifyAndSetSystemId()V
    .locals 1

    iget-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_textBuilder:Lcom/fasterxml/aalto/util/TextBuilder;

    invoke-virtual {v0}, Lcom/fasterxml/aalto/util/TextBuilder;->contentsAsString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_systemId:Ljava/lang/String;

    return-void
.end method

.method public verifyAndSetXmlEncoding()V
    .locals 3

    iget-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_textBuilder:Lcom/fasterxml/aalto/util/TextBuilder;

    invoke-virtual {v0}, Lcom/fasterxml/aalto/util/TextBuilder;->contentsAsString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/fasterxml/aalto/util/CharsetNames;->normalize(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "UTF-8"

    if-eq v1, v0, :cond_0

    const-string v1, "US-ASCII"

    if-eq v1, v0, :cond_0

    const-string v1, "ISO-8859-1"

    if-eq v1, v0, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unsupported encoding \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "\': only UTF-8 and US-ASCII support by async parser"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/fasterxml/aalto/in/XmlScanner;->reportInputProblem(Ljava/lang/String;)V

    :cond_0
    iget-object v1, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_config:Lcom/fasterxml/aalto/in/ReaderConfig;

    invoke-virtual {v1, v0}, Lcom/fasterxml/aalto/in/ReaderConfig;->setXmlEncoding(Ljava/lang/String;)V

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_config:Lcom/fasterxml/aalto/in/ReaderConfig;

    invoke-virtual {v1, v0}, Lcom/fasterxml/aalto/in/ReaderConfig;->setActualEncoding(Ljava/lang/String;)V

    :cond_1
    iget-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_config:Lcom/fasterxml/aalto/in/ReaderConfig;

    invoke-virtual {v0}, Lcom/fasterxml/aalto/in/ReaderConfig;->getCharTypes()Lcom/fasterxml/aalto/util/XmlCharTypes;

    move-result-object v0

    iput-object v0, p0, Lcom/fasterxml/aalto/async/AsyncByteScanner;->_charTypes:Lcom/fasterxml/aalto/util/XmlCharTypes;

    return-void
.end method

.method public verifyAndSetXmlStandalone()V
    .locals 2

    iget-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_textBuilder:Lcom/fasterxml/aalto/util/TextBuilder;

    const-string v1, "yes"

    invoke-virtual {v0, v1}, Lcom/fasterxml/aalto/util/TextBuilder;->equalsString(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_config:Lcom/fasterxml/aalto/in/ReaderConfig;

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    :goto_0
    invoke-virtual {v0, v1}, Lcom/fasterxml/aalto/in/ReaderConfig;->setXmlStandalone(Ljava/lang/Boolean;)V

    goto :goto_1

    :cond_0
    iget-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_textBuilder:Lcom/fasterxml/aalto/util/TextBuilder;

    const-string v1, "no"

    invoke-virtual {v0, v1}, Lcom/fasterxml/aalto/util/TextBuilder;->equalsString(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_config:Lcom/fasterxml/aalto/in/ReaderConfig;

    sget-object v1, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Invalid standalone value \'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_textBuilder:Lcom/fasterxml/aalto/util/TextBuilder;

    invoke-virtual {v1}, Lcom/fasterxml/aalto/util/TextBuilder;->contentsAsString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\': can only use \'yes\' and \'no\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/in/XmlScanner;->reportInputProblem(Ljava/lang/String;)V

    :goto_1
    return-void
.end method

.method public verifyAndSetXmlVersion()V
    .locals 2

    iget-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_textBuilder:Lcom/fasterxml/aalto/util/TextBuilder;

    const-string v1, "1.0"

    invoke-virtual {v0, v1}, Lcom/fasterxml/aalto/util/TextBuilder;->equalsString(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    iget-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_config:Lcom/fasterxml/aalto/in/ReaderConfig;

    invoke-virtual {v0, v1}, Lcom/fasterxml/aalto/in/ReaderConfig;->setXmlVersion(Ljava/lang/String;)V

    goto :goto_1

    :cond_0
    iget-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_textBuilder:Lcom/fasterxml/aalto/util/TextBuilder;

    const-string v1, "1.1"

    invoke-virtual {v0, v1}, Lcom/fasterxml/aalto/util/TextBuilder;->equalsString(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unrecognized XML version \'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_textBuilder:Lcom/fasterxml/aalto/util/TextBuilder;

    invoke-virtual {v1}, Lcom/fasterxml/aalto/util/TextBuilder;->contentsAsString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\' (expected \'1.0\' or \'1.1\')"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/in/XmlScanner;->reportInputProblem(Ljava/lang/String;)V

    :goto_1
    return-void
.end method

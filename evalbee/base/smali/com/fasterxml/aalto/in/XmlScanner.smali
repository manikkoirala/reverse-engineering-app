.class public abstract Lcom/fasterxml/aalto/in/XmlScanner;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/fasterxml/aalto/util/XmlConsts;
.implements Ljavax/xml/stream/XMLStreamConstants;
.implements Ljavax/xml/namespace/NamespaceContext;


# static fields
.field private static final BIND_CACHE_MASK:I = 0x3f

.field private static final BIND_CACHE_SIZE:I = 0x40

.field private static final BIND_MISSES_TO_ACTIVATE_CACHE:I = 0xa

.field protected static final INT_0:I = 0x30

.field protected static final INT_9:I = 0x39

.field protected static final INT_A:I = 0x41

.field protected static final INT_AMP:I = 0x26

.field protected static final INT_APOS:I = 0x27

.field protected static final INT_COLON:I = 0x3a

.field protected static final INT_CR:I = 0xd

.field protected static final INT_EQ:I = 0x3d

.field protected static final INT_EXCL:I = 0x21

.field protected static final INT_F:I = 0x46

.field protected static final INT_GT:I = 0x3e

.field protected static final INT_HYPHEN:I = 0x2d

.field protected static final INT_LBRACKET:I = 0x5b

.field protected static final INT_LF:I = 0xa

.field protected static final INT_LT:I = 0x3c

.field protected static final INT_NULL:I = 0x0

.field protected static final INT_QMARK:I = 0x3f

.field protected static final INT_QUOTE:I = 0x22

.field protected static final INT_RBRACKET:I = 0x5d

.field protected static final INT_SLASH:I = 0x2f

.field protected static final INT_SPACE:I = 0x20

.field protected static final INT_TAB:I = 0x9

.field protected static final INT_a:I = 0x61

.field protected static final INT_f:I = 0x66

.field protected static final INT_z:I = 0x7a

.field protected static final MAX_UNICODE_CHAR:I = 0x10ffff

.field public static final TOKEN_EOI:I = -0x1


# instance fields
.field protected final CDATA_STR:Ljava/lang/String;

.field protected final _attrCollector:Lcom/fasterxml/aalto/in/AttributeCollector;

.field protected _attrCount:I

.field protected final _cfgCoalescing:Z

.field protected _cfgLazyParsing:Z

.field protected final _config:Lcom/fasterxml/aalto/in/ReaderConfig;

.field protected _currElem:Lcom/fasterxml/aalto/in/ElementScope;

.field protected _currNsCount:I

.field protected _currRow:I

.field protected _currToken:I

.field protected _defaultNs:Lcom/fasterxml/aalto/in/NsBinding;

.field protected _depth:I

.field protected _entityPending:Z

.field protected _isEmptyTag:Z

.field protected _lastNsContext:Lcom/fasterxml/aalto/in/FixedNsContext;

.field protected _lastNsDecl:Lcom/fasterxml/aalto/in/NsDeclaration;

.field protected _nameBuffer:[C

.field protected _nsBindMisses:I

.field protected _nsBindingCache:[Lcom/fasterxml/aalto/in/PName;

.field protected _nsBindingCount:I

.field protected _nsBindings:[Lcom/fasterxml/aalto/in/NsBinding;

.field protected _pastBytesOrChars:J

.field protected _publicId:Ljava/lang/String;

.field protected _rowStartOffset:I

.field protected _startColumn:J

.field protected _startRawOffset:J

.field protected _startRow:J

.field protected _systemId:Ljava/lang/String;

.field protected final _textBuilder:Lcom/fasterxml/aalto/util/TextBuilder;

.field protected _tokenIncomplete:Z

.field protected _tokenName:Lcom/fasterxml/aalto/in/PName;

.field protected final _xml11:Z


# direct methods
.method public constructor <init>(Lcom/fasterxml/aalto/in/ReaderConfig;)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "CDATA["

    iput-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->CDATA_STR:Ljava/lang/String;

    const/4 v0, 0x7

    iput v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_currToken:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_tokenIncomplete:Z

    iput v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_depth:I

    iput-boolean v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_entityPending:Z

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_nameBuffer:[C

    iput-object v1, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_tokenName:Lcom/fasterxml/aalto/in/PName;

    iput-boolean v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_isEmptyTag:Z

    iput-object v1, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_lastNsDecl:Lcom/fasterxml/aalto/in/NsDeclaration;

    iput v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_currNsCount:I

    invoke-static {}, Lcom/fasterxml/aalto/in/NsBinding;->createDefaultNs()Lcom/fasterxml/aalto/in/NsBinding;

    move-result-object v2

    iput-object v2, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_defaultNs:Lcom/fasterxml/aalto/in/NsBinding;

    iput v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_nsBindingCount:I

    iput-object v1, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_nsBindingCache:[Lcom/fasterxml/aalto/in/PName;

    iput v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_nsBindMisses:I

    sget-object v1, Lcom/fasterxml/aalto/in/FixedNsContext;->EMPTY_CONTEXT:Lcom/fasterxml/aalto/in/FixedNsContext;

    iput-object v1, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_lastNsContext:Lcom/fasterxml/aalto/in/FixedNsContext;

    iput v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_attrCount:I

    const-wide/16 v1, -0x1

    iput-wide v1, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_startRow:J

    iput-wide v1, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_startColumn:J

    iput-object p1, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_config:Lcom/fasterxml/aalto/in/ReaderConfig;

    invoke-virtual {p1}, Lcom/fasterxml/aalto/in/ReaderConfig;->willCoalesceText()Z

    move-result v1

    iput-boolean v1, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_cfgCoalescing:Z

    invoke-virtual {p1}, Lcom/fasterxml/aalto/in/ReaderConfig;->willParseLazily()Z

    move-result v1

    iput-boolean v1, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_cfgLazyParsing:Z

    invoke-virtual {p1}, Lcom/fasterxml/aalto/in/ReaderConfig;->isXml11()Z

    move-result v1

    iput-boolean v1, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_xml11:Z

    invoke-static {p1}, Lcom/fasterxml/aalto/util/TextBuilder;->createRecyclableBuffer(Lcom/fasterxml/aalto/in/ReaderConfig;)Lcom/fasterxml/aalto/util/TextBuilder;

    move-result-object v1

    iput-object v1, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_textBuilder:Lcom/fasterxml/aalto/util/TextBuilder;

    new-instance v1, Lcom/fasterxml/aalto/in/AttributeCollector;

    invoke-direct {v1, p1}, Lcom/fasterxml/aalto/in/AttributeCollector;-><init>(Lcom/fasterxml/aalto/in/ReaderConfig;)V

    iput-object v1, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_attrCollector:Lcom/fasterxml/aalto/in/AttributeCollector;

    const/16 v1, 0x3c

    invoke-virtual {p1, v1}, Lcom/fasterxml/aalto/in/ReaderConfig;->allocSmallCBuffer(I)[C

    move-result-object p1

    iput-object p1, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_nameBuffer:[C

    iput v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_currRow:I

    return-void
.end method

.method private findCurrNsDecl(I)Lcom/fasterxml/aalto/in/NsDeclaration;
    .locals 4

    iget-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_lastNsDecl:Lcom/fasterxml/aalto/in/NsDeclaration;

    iget v1, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_depth:I

    iget v2, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_currToken:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    iget v2, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_currNsCount:I

    sub-int/2addr v2, v3

    sub-int/2addr v2, p1

    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    :cond_0
    move v2, p1

    :goto_0
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/fasterxml/aalto/in/NsDeclaration;->getLevel()I

    move-result v3

    if-ne v3, v1, :cond_2

    if-nez v2, :cond_1

    return-object v0

    :cond_1
    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v0}, Lcom/fasterxml/aalto/in/NsDeclaration;->getPrev()Lcom/fasterxml/aalto/in/NsDeclaration;

    move-result-object v0

    goto :goto_0

    :cond_2
    invoke-virtual {p0, p1}, Lcom/fasterxml/aalto/in/XmlScanner;->reportInvalidNsIndex(I)V

    const/4 p1, 0x0

    return-object p1
.end method


# virtual methods
.method public abstract _closeSource()V
.end method

.method public _releaseBuffers()V
    .locals 2

    iget-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_textBuilder:Lcom/fasterxml/aalto/util/TextBuilder;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/fasterxml/aalto/util/TextBuilder;->recycle(Z)V

    iget-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_nameBuffer:[C

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_nameBuffer:[C

    iget-object v1, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_config:Lcom/fasterxml/aalto/in/ReaderConfig;

    invoke-virtual {v1, v0}, Lcom/fasterxml/aalto/in/ReaderConfig;->freeSmallCBuffer([C)V

    :cond_0
    return-void
.end method

.method public final bindName(Lcom/fasterxml/aalto/in/PName;Ljava/lang/String;)Lcom/fasterxml/aalto/in/PName;
    .locals 5

    iget-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_nsBindingCache:[Lcom/fasterxml/aalto/in/PName;

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/fasterxml/aalto/in/PName;->unboundHashCode()I

    move-result v1

    and-int/lit8 v1, v1, 0x3f

    aget-object v0, v0, v1

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lcom/fasterxml/aalto/in/PName;->unboundEquals(Lcom/fasterxml/aalto/in/PName;)Z

    move-result v1

    if-eqz v1, :cond_0

    return-object v0

    :cond_0
    iget v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_nsBindingCount:I

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_5

    iget-object v2, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_nsBindings:[Lcom/fasterxml/aalto/in/NsBinding;

    aget-object v3, v2, v1

    iget-object v4, v3, Lcom/fasterxml/aalto/in/NsBinding;->mPrefix:Ljava/lang/String;

    if-eq v4, p2, :cond_1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    if-lez v1, :cond_2

    add-int/lit8 p2, v1, -0x1

    aget-object v0, v2, p2

    aput-object v0, v2, v1

    aput-object v3, v2, p2

    :cond_2
    invoke-virtual {p1, v3}, Lcom/fasterxml/aalto/in/PName;->createBoundName(Lcom/fasterxml/aalto/in/NsBinding;)Lcom/fasterxml/aalto/in/PName;

    move-result-object p1

    iget-object p2, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_nsBindingCache:[Lcom/fasterxml/aalto/in/PName;

    if-nez p2, :cond_4

    iget p2, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_nsBindMisses:I

    add-int/lit8 p2, p2, 0x1

    iput p2, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_nsBindMisses:I

    const/16 v0, 0xa

    if-ge p2, v0, :cond_3

    return-object p1

    :cond_3
    const/16 p2, 0x40

    new-array p2, p2, [Lcom/fasterxml/aalto/in/PName;

    iput-object p2, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_nsBindingCache:[Lcom/fasterxml/aalto/in/PName;

    :cond_4
    iget-object p2, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_nsBindingCache:[Lcom/fasterxml/aalto/in/PName;

    invoke-virtual {p1}, Lcom/fasterxml/aalto/in/PName;->unboundHashCode()I

    move-result v0

    and-int/lit8 v0, v0, 0x3f

    aput-object p1, p2, v0

    return-object p1

    :cond_5
    const-string v0, "xml"

    if-ne p2, v0, :cond_6

    sget-object p2, Lcom/fasterxml/aalto/in/NsBinding;->XML_BINDING:Lcom/fasterxml/aalto/in/NsBinding;

    invoke-virtual {p1, p2}, Lcom/fasterxml/aalto/in/PName;->createBoundName(Lcom/fasterxml/aalto/in/NsBinding;)Lcom/fasterxml/aalto/in/PName;

    move-result-object p1

    return-object p1

    :cond_6
    iget v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_nsBindMisses:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_nsBindMisses:I

    new-instance v0, Lcom/fasterxml/aalto/in/NsBinding;

    invoke-direct {v0, p2}, Lcom/fasterxml/aalto/in/NsBinding;-><init>(Ljava/lang/String;)V

    iget p2, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_nsBindingCount:I

    if-nez p2, :cond_7

    const/16 p2, 0x10

    new-array p2, p2, [Lcom/fasterxml/aalto/in/NsBinding;

    iput-object p2, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_nsBindings:[Lcom/fasterxml/aalto/in/NsBinding;

    goto :goto_1

    :cond_7
    iget-object v1, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_nsBindings:[Lcom/fasterxml/aalto/in/NsBinding;

    array-length v2, v1

    if-lt p2, v2, :cond_8

    array-length p2, v1

    invoke-static {v1, p2}, Lcom/fasterxml/aalto/util/DataUtil;->growAnyArrayBy(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, [Lcom/fasterxml/aalto/in/NsBinding;

    iput-object p2, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_nsBindings:[Lcom/fasterxml/aalto/in/NsBinding;

    :cond_8
    :goto_1
    iget-object p2, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_nsBindings:[Lcom/fasterxml/aalto/in/NsBinding;

    iget v1, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_nsBindingCount:I

    aput-object v0, p2, v1

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_nsBindingCount:I

    invoke-virtual {p1, v0}, Lcom/fasterxml/aalto/in/PName;->createBoundName(Lcom/fasterxml/aalto/in/NsBinding;)Lcom/fasterxml/aalto/in/PName;

    move-result-object p1

    return-object p1
.end method

.method public final bindNs(Lcom/fasterxml/aalto/in/PName;Ljava/lang/String;)V
    .locals 3

    invoke-virtual {p1}, Lcom/fasterxml/aalto/in/PName;->getPrefix()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object p1, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_defaultNs:Lcom/fasterxml/aalto/in/NsBinding;

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Lcom/fasterxml/aalto/in/PName;->getLocalName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/in/XmlScanner;->findOrCreateBinding(Ljava/lang/String;)Lcom/fasterxml/aalto/in/NsBinding;

    move-result-object p1

    invoke-virtual {p1}, Lcom/fasterxml/aalto/in/NsBinding;->isImmutable()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0, v0, p2}, Lcom/fasterxml/aalto/in/XmlScanner;->checkImmutableBinding(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    :goto_0
    invoke-virtual {p1}, Lcom/fasterxml/aalto/in/NsBinding;->isImmutable()Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, "http://www.w3.org/XML/1998/namespace"

    if-ne p2, v1, :cond_2

    const-string v2, "xml"

    :goto_1
    invoke-virtual {p0, v2, v1}, Lcom/fasterxml/aalto/in/XmlScanner;->reportIllegalNsDecl(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    :cond_2
    const-string v1, "http://www.w3.org/2000/xmlns/"

    if-ne p2, v1, :cond_3

    const-string v2, "xmlns"

    goto :goto_1

    :cond_3
    :goto_2
    iget-object v1, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_lastNsDecl:Lcom/fasterxml/aalto/in/NsDeclaration;

    if-eqz v1, :cond_4

    iget v2, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_depth:I

    invoke-virtual {v1, v0, v2}, Lcom/fasterxml/aalto/in/NsDeclaration;->alreadyDeclared(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/in/XmlScanner;->reportDuplicateNsDecl(Ljava/lang/String;)V

    :cond_4
    new-instance v0, Lcom/fasterxml/aalto/in/NsDeclaration;

    iget-object v1, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_lastNsDecl:Lcom/fasterxml/aalto/in/NsDeclaration;

    iget v2, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_depth:I

    invoke-direct {v0, p1, p2, v1, v2}, Lcom/fasterxml/aalto/in/NsDeclaration;-><init>(Lcom/fasterxml/aalto/in/NsBinding;Ljava/lang/String;Lcom/fasterxml/aalto/in/NsDeclaration;I)V

    iput-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_lastNsDecl:Lcom/fasterxml/aalto/in/NsDeclaration;

    return-void
.end method

.method public final checkImmutableBinding(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    const-string v0, "xml"

    if-ne p1, v0, :cond_0

    const-string v0, "http://www.w3.org/XML/1998/namespace"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-nez p2, :cond_1

    :cond_0
    invoke-virtual {p0, p1}, Lcom/fasterxml/aalto/in/XmlScanner;->reportIllegalNsDecl(Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method public final close(Z)V
    .locals 1

    invoke-virtual {p0}, Lcom/fasterxml/aalto/in/XmlScanner;->_releaseBuffers()V

    if-nez p1, :cond_0

    iget-object p1, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_config:Lcom/fasterxml/aalto/in/ReaderConfig;

    invoke-virtual {p1}, Lcom/fasterxml/aalto/in/ReaderConfig;->willAutoCloseInput()Z

    move-result p1

    if-eqz p1, :cond_1

    :cond_0
    :try_start_0
    invoke-virtual {p0}, Lcom/fasterxml/aalto/in/XmlScanner;->_closeSource()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    return-void

    :catch_0
    move-exception p1

    new-instance v0, Lcom/fasterxml/aalto/impl/IoStreamException;

    invoke-direct {v0, p1}, Lcom/fasterxml/aalto/impl/IoStreamException;-><init>(Ljava/io/IOException;)V

    throw v0
.end method

.method public final decodeAttrBinaryValue(ILorg/codehaus/stax2/typed/Base64Variant;Lorg/codehaus/stax2/ri/typed/CharArrayBase64Decoder;)[B
    .locals 1

    iget-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_attrCollector:Lcom/fasterxml/aalto/in/AttributeCollector;

    invoke-virtual {v0, p1, p2, p3, p0}, Lcom/fasterxml/aalto/in/AttributeCollector;->decodeBinaryValue(ILorg/codehaus/stax2/typed/Base64Variant;Lorg/codehaus/stax2/ri/typed/CharArrayBase64Decoder;Lcom/fasterxml/aalto/in/XmlScanner;)[B

    move-result-object p1

    return-object p1
.end method

.method public final decodeAttrValue(ILorg/codehaus/stax2/typed/TypedValueDecoder;)V
    .locals 1

    iget-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_attrCollector:Lcom/fasterxml/aalto/in/AttributeCollector;

    invoke-virtual {v0, p1, p2}, Lcom/fasterxml/aalto/in/AttributeCollector;->decodeValue(ILorg/codehaus/stax2/typed/TypedValueDecoder;)V

    return-void
.end method

.method public final decodeAttrValues(ILorg/codehaus/stax2/typed/TypedArrayDecoder;)I
    .locals 1

    iget-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_attrCollector:Lcom/fasterxml/aalto/in/AttributeCollector;

    invoke-virtual {v0, p1, p2, p0}, Lcom/fasterxml/aalto/in/AttributeCollector;->decodeValues(ILorg/codehaus/stax2/typed/TypedArrayDecoder;Lcom/fasterxml/aalto/in/XmlScanner;)I

    move-result p1

    return p1
.end method

.method public final decodeElements(Lorg/codehaus/stax2/typed/TypedArrayDecoder;Z)I
    .locals 3

    iget-boolean v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_tokenIncomplete:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/aalto/in/XmlScanner;->finishToken()V

    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_textBuilder:Lcom/fasterxml/aalto/util/TextBuilder;

    invoke-virtual {v0, p1, p2}, Lcom/fasterxml/aalto/util/TextBuilder;->decodeElements(Lorg/codehaus/stax2/typed/TypedArrayDecoder;Z)I

    move-result p1
    :try_end_0
    .catch Lorg/codehaus/stax2/typed/TypedXMLStreamException; {:try_start_0 .. :try_end_0} :catch_0

    return p1

    :catch_0
    move-exception p1

    invoke-virtual {p0}, Lcom/fasterxml/aalto/in/XmlScanner;->getCurrentLocation()Lorg/codehaus/stax2/XMLStreamLocation2;

    move-result-object p2

    invoke-virtual {p1}, Lorg/codehaus/stax2/typed/TypedXMLStreamException;->getLexical()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    check-cast v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Lorg/codehaus/stax2/typed/TypedXMLStreamException;

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v2, v0, p1, p2, v1}, Lorg/codehaus/stax2/typed/TypedXMLStreamException;-><init>(Ljava/lang/String;Ljava/lang/String;Ljavax/xml/stream/Location;Ljava/lang/IllegalArgumentException;)V

    throw v2
.end method

.method public final findAttrIndex(Ljava/lang/String;Ljava/lang/String;)I
    .locals 2

    iget v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_attrCount:I

    const/4 v1, 0x1

    if-ge v0, v1, :cond_0

    const/4 p1, -0x1

    return p1

    :cond_0
    iget-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_attrCollector:Lcom/fasterxml/aalto/in/AttributeCollector;

    invoke-virtual {v0, p1, p2}, Lcom/fasterxml/aalto/in/AttributeCollector;->findIndex(Ljava/lang/String;Ljava/lang/String;)I

    move-result p1

    return p1
.end method

.method public final findOrCreateBinding(Ljava/lang/String;)Lcom/fasterxml/aalto/in/NsBinding;
    .locals 5

    iget v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_nsBindingCount:I

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_2

    iget-object v2, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_nsBindings:[Lcom/fasterxml/aalto/in/NsBinding;

    aget-object v3, v2, v1

    iget-object v4, v3, Lcom/fasterxml/aalto/in/NsBinding;->mPrefix:Ljava/lang/String;

    if-ne v4, p1, :cond_1

    if-lez v1, :cond_0

    add-int/lit8 p1, v1, -0x1

    aget-object v0, v2, p1

    aput-object v0, v2, v1

    aput-object v3, v2, p1

    :cond_0
    return-object v3

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    const-string v0, "xml"

    if-ne p1, v0, :cond_3

    sget-object p1, Lcom/fasterxml/aalto/in/NsBinding;->XML_BINDING:Lcom/fasterxml/aalto/in/NsBinding;

    return-object p1

    :cond_3
    const-string v0, "xmlns"

    if-ne p1, v0, :cond_4

    sget-object p1, Lcom/fasterxml/aalto/in/NsBinding;->XMLNS_BINDING:Lcom/fasterxml/aalto/in/NsBinding;

    return-object p1

    :cond_4
    new-instance v0, Lcom/fasterxml/aalto/in/NsBinding;

    invoke-direct {v0, p1}, Lcom/fasterxml/aalto/in/NsBinding;-><init>(Ljava/lang/String;)V

    iget p1, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_nsBindingCount:I

    if-nez p1, :cond_5

    const/16 p1, 0x10

    new-array p1, p1, [Lcom/fasterxml/aalto/in/NsBinding;

    iput-object p1, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_nsBindings:[Lcom/fasterxml/aalto/in/NsBinding;

    goto :goto_1

    :cond_5
    iget-object v1, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_nsBindings:[Lcom/fasterxml/aalto/in/NsBinding;

    array-length v2, v1

    if-lt p1, v2, :cond_6

    array-length p1, v1

    invoke-static {v1, p1}, Lcom/fasterxml/aalto/util/DataUtil;->growAnyArrayBy(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, [Lcom/fasterxml/aalto/in/NsBinding;

    iput-object p1, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_nsBindings:[Lcom/fasterxml/aalto/in/NsBinding;

    :cond_6
    :goto_1
    iget-object p1, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_nsBindings:[Lcom/fasterxml/aalto/in/NsBinding;

    iget v1, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_nsBindingCount:I

    aput-object v0, p1, v1

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_nsBindingCount:I

    return-object v0
.end method

.method public abstract finishCData()V
.end method

.method public abstract finishCharacters()V
.end method

.method public abstract finishComment()V
.end method

.method public abstract finishDTD(Z)V
.end method

.method public abstract finishPI()V
.end method

.method public abstract finishSpace()V
.end method

.method public abstract finishToken()V
.end method

.method public fireSaxCharacterEvents(Lorg/xml/sax/ContentHandler;)V
    .locals 1

    if-eqz p1, :cond_1

    iget-boolean v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_tokenIncomplete:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/aalto/in/XmlScanner;->finishToken()V

    :cond_0
    iget-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_textBuilder:Lcom/fasterxml/aalto/util/TextBuilder;

    invoke-virtual {v0, p1}, Lcom/fasterxml/aalto/util/TextBuilder;->fireSaxCharacterEvents(Lorg/xml/sax/ContentHandler;)V

    :cond_1
    return-void
.end method

.method public fireSaxCommentEvent(Lorg/xml/sax/ext/LexicalHandler;)V
    .locals 1

    if-eqz p1, :cond_1

    iget-boolean v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_tokenIncomplete:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/aalto/in/XmlScanner;->finishToken()V

    :cond_0
    iget-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_textBuilder:Lcom/fasterxml/aalto/util/TextBuilder;

    invoke-virtual {v0, p1}, Lcom/fasterxml/aalto/util/TextBuilder;->fireSaxCommentEvent(Lorg/xml/sax/ext/LexicalHandler;)V

    :cond_1
    return-void
.end method

.method public fireSaxEndElement(Lorg/xml/sax/ContentHandler;)V
    .locals 4

    if-eqz p1, :cond_2

    invoke-virtual {p0}, Lcom/fasterxml/aalto/in/XmlScanner;->getName()Lcom/fasterxml/aalto/in/PName;

    move-result-object v0

    invoke-virtual {v0}, Lcom/fasterxml/aalto/in/PName;->getNsUri()Ljava/lang/String;

    move-result-object v1

    const-string v2, ""

    if-nez v1, :cond_0

    move-object v1, v2

    :cond_0
    invoke-virtual {v0}, Lcom/fasterxml/aalto/in/PName;->getLocalName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lcom/fasterxml/aalto/in/PName;->getPrefixedName()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v1, v3, v0}, Lorg/xml/sax/ContentHandler;->endElement(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_lastNsDecl:Lcom/fasterxml/aalto/in/NsDeclaration;

    iget v1, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_depth:I

    :goto_0
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/fasterxml/aalto/in/NsDeclaration;->getLevel()I

    move-result v3

    if-ne v3, v1, :cond_2

    invoke-virtual {v0}, Lcom/fasterxml/aalto/in/NsDeclaration;->getPrefix()Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_1

    move-object v3, v2

    :cond_1
    invoke-interface {p1, v3}, Lorg/xml/sax/ContentHandler;->endPrefixMapping(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/fasterxml/aalto/in/NsDeclaration;->getPrev()Lcom/fasterxml/aalto/in/NsDeclaration;

    move-result-object v0

    goto :goto_0

    :cond_2
    return-void
.end method

.method public fireSaxPIEvent(Lorg/xml/sax/ContentHandler;)V
    .locals 2

    if-eqz p1, :cond_1

    iget-boolean v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_tokenIncomplete:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/aalto/in/XmlScanner;->finishToken()V

    :cond_0
    iget-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_tokenName:Lcom/fasterxml/aalto/in/PName;

    invoke-virtual {v0}, Lcom/fasterxml/aalto/in/PName;->getLocalName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/fasterxml/aalto/in/XmlScanner;->getText()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v0, v1}, Lorg/xml/sax/ContentHandler;->processingInstruction(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method public fireSaxSpaceEvents(Lorg/xml/sax/ContentHandler;)V
    .locals 1

    if-eqz p1, :cond_1

    iget-boolean v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_tokenIncomplete:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/aalto/in/XmlScanner;->finishToken()V

    :cond_0
    iget-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_textBuilder:Lcom/fasterxml/aalto/util/TextBuilder;

    invoke-virtual {v0, p1}, Lcom/fasterxml/aalto/util/TextBuilder;->fireSaxSpaceEvents(Lorg/xml/sax/ContentHandler;)V

    :cond_1
    return-void
.end method

.method public fireSaxStartElement(Lorg/xml/sax/ContentHandler;Lorg/xml/sax/Attributes;)V
    .locals 5

    if-eqz p1, :cond_3

    iget-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_lastNsDecl:Lcom/fasterxml/aalto/in/NsDeclaration;

    iget v1, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_depth:I

    add-int/lit8 v1, v1, -0x1

    :goto_0
    const-string v2, ""

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/fasterxml/aalto/in/NsDeclaration;->getLevel()I

    move-result v3

    if-ne v3, v1, :cond_1

    invoke-virtual {v0}, Lcom/fasterxml/aalto/in/NsDeclaration;->getPrefix()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lcom/fasterxml/aalto/in/NsDeclaration;->getCurrNsURI()Ljava/lang/String;

    move-result-object v4

    if-nez v3, :cond_0

    goto :goto_1

    :cond_0
    move-object v2, v3

    :goto_1
    invoke-interface {p1, v2, v4}, Lorg/xml/sax/ContentHandler;->startPrefixMapping(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/fasterxml/aalto/in/NsDeclaration;->getPrev()Lcom/fasterxml/aalto/in/NsDeclaration;

    move-result-object v0

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/fasterxml/aalto/in/XmlScanner;->getName()Lcom/fasterxml/aalto/in/PName;

    move-result-object v0

    invoke-virtual {v0}, Lcom/fasterxml/aalto/in/PName;->getNsUri()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_2

    goto :goto_2

    :cond_2
    move-object v2, v1

    :goto_2
    invoke-virtual {v0}, Lcom/fasterxml/aalto/in/PName;->getLocalName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/fasterxml/aalto/in/PName;->getPrefixedName()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v2, v1, v0, p2}, Lorg/xml/sax/ContentHandler;->startElement(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lorg/xml/sax/Attributes;)V

    :cond_3
    return-void
.end method

.method public getAttrCollector()Lcom/fasterxml/aalto/in/AttributeCollector;
    .locals 1

    iget-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_attrCollector:Lcom/fasterxml/aalto/in/AttributeCollector;

    return-object v0
.end method

.method public final getAttrCount()I
    .locals 1

    iget v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_attrCount:I

    return v0
.end method

.method public final getAttrLocalName(I)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_attrCollector:Lcom/fasterxml/aalto/in/AttributeCollector;

    invoke-virtual {v0, p1}, Lcom/fasterxml/aalto/in/AttributeCollector;->getName(I)Lcom/fasterxml/aalto/in/PName;

    move-result-object p1

    invoke-virtual {p1}, Lcom/fasterxml/aalto/in/PName;->getLocalName()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public final getAttrNsURI(I)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_attrCollector:Lcom/fasterxml/aalto/in/AttributeCollector;

    invoke-virtual {v0, p1}, Lcom/fasterxml/aalto/in/AttributeCollector;->getName(I)Lcom/fasterxml/aalto/in/PName;

    move-result-object p1

    invoke-virtual {p1}, Lcom/fasterxml/aalto/in/PName;->getNsUri()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public final getAttrPrefix(I)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_attrCollector:Lcom/fasterxml/aalto/in/AttributeCollector;

    invoke-virtual {v0, p1}, Lcom/fasterxml/aalto/in/AttributeCollector;->getName(I)Lcom/fasterxml/aalto/in/PName;

    move-result-object p1

    invoke-virtual {p1}, Lcom/fasterxml/aalto/in/PName;->getPrefix()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public final getAttrPrefixedName(I)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_attrCollector:Lcom/fasterxml/aalto/in/AttributeCollector;

    invoke-virtual {v0, p1}, Lcom/fasterxml/aalto/in/AttributeCollector;->getName(I)Lcom/fasterxml/aalto/in/PName;

    move-result-object p1

    invoke-virtual {p1}, Lcom/fasterxml/aalto/in/PName;->getPrefixedName()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public final getAttrQName(I)Ljavax/xml/namespace/QName;
    .locals 1

    iget-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_attrCollector:Lcom/fasterxml/aalto/in/AttributeCollector;

    invoke-virtual {v0, p1}, Lcom/fasterxml/aalto/in/AttributeCollector;->getQName(I)Ljavax/xml/namespace/QName;

    move-result-object p1

    return-object p1
.end method

.method public final getAttrType(I)Ljava/lang/String;
    .locals 0

    const-string p1, "CDATA"

    return-object p1
.end method

.method public final getAttrValue(I)Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_attrCollector:Lcom/fasterxml/aalto/in/AttributeCollector;

    invoke-virtual {v0, p1}, Lcom/fasterxml/aalto/in/AttributeCollector;->getValue(I)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public final getAttrValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .line 2
    iget v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_attrCount:I

    const/4 v1, 0x1

    if-ge v0, v1, :cond_0

    const/4 p1, 0x0

    return-object p1

    :cond_0
    iget-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_attrCollector:Lcom/fasterxml/aalto/in/AttributeCollector;

    invoke-virtual {v0, p1, p2}, Lcom/fasterxml/aalto/in/AttributeCollector;->getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getConfig()Lcom/fasterxml/aalto/in/ReaderConfig;
    .locals 1

    iget-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_config:Lcom/fasterxml/aalto/in/ReaderConfig;

    return-object v0
.end method

.method public abstract getCurrentColumnNr()I
.end method

.method public final getCurrentLineNr()I
    .locals 1

    iget v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_currRow:I

    add-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public abstract getCurrentLocation()Lorg/codehaus/stax2/XMLStreamLocation2;
.end method

.method public final getDTDPublicId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_publicId:Ljava/lang/String;

    return-object v0
.end method

.method public final getDTDSystemId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_systemId:Ljava/lang/String;

    return-object v0
.end method

.method public final getDepth()I
    .locals 1

    iget v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_depth:I

    return v0
.end method

.method public getEndLocation()Lorg/codehaus/stax2/XMLStreamLocation2;
    .locals 1

    iget-boolean v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_tokenIncomplete:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/aalto/in/XmlScanner;->finishToken()V

    :cond_0
    invoke-virtual {p0}, Lcom/fasterxml/aalto/in/XmlScanner;->getCurrentLocation()Lorg/codehaus/stax2/XMLStreamLocation2;

    move-result-object v0

    return-object v0
.end method

.method public abstract getEndingByteOffset()J
.end method

.method public abstract getEndingCharOffset()J
.end method

.method public final getInputPublicId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_config:Lcom/fasterxml/aalto/in/ReaderConfig;

    invoke-virtual {v0}, Lcom/fasterxml/aalto/in/ReaderConfig;->getPublicId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getInputSystemId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_config:Lcom/fasterxml/aalto/in/ReaderConfig;

    invoke-virtual {v0}, Lcom/fasterxml/aalto/in/ReaderConfig;->getSystemId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getName()Lcom/fasterxml/aalto/in/PName;
    .locals 1

    iget-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_tokenName:Lcom/fasterxml/aalto/in/PName;

    return-object v0
.end method

.method public final getNamespacePrefix(I)Ljava/lang/String;
    .locals 0

    invoke-direct {p0, p1}, Lcom/fasterxml/aalto/in/XmlScanner;->findCurrNsDecl(I)Lcom/fasterxml/aalto/in/NsDeclaration;

    move-result-object p1

    invoke-virtual {p1}, Lcom/fasterxml/aalto/in/NsDeclaration;->getBinding()Lcom/fasterxml/aalto/in/NsBinding;

    move-result-object p1

    iget-object p1, p1, Lcom/fasterxml/aalto/in/NsBinding;->mPrefix:Ljava/lang/String;

    return-object p1
.end method

.method public final getNamespaceURI()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_tokenName:Lcom/fasterxml/aalto/in/PName;

    invoke-virtual {v0}, Lcom/fasterxml/aalto/in/PName;->getNsUri()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_defaultNs:Lcom/fasterxml/aalto/in/NsBinding;

    iget-object v0, v0, Lcom/fasterxml/aalto/in/NsBinding;->mURI:Ljava/lang/String;

    :cond_0
    return-object v0
.end method

.method public final getNamespaceURI(I)Ljava/lang/String;
    .locals 0

    .line 2
    invoke-direct {p0, p1}, Lcom/fasterxml/aalto/in/XmlScanner;->findCurrNsDecl(I)Lcom/fasterxml/aalto/in/NsDeclaration;

    move-result-object p1

    invoke-virtual {p1}, Lcom/fasterxml/aalto/in/NsDeclaration;->getBinding()Lcom/fasterxml/aalto/in/NsBinding;

    move-result-object p1

    iget-object p1, p1, Lcom/fasterxml/aalto/in/NsBinding;->mURI:Ljava/lang/String;

    return-object p1
.end method

.method public getNamespaceURI(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .line 3
    if-eqz p1, :cond_6

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    iget-object p1, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_defaultNs:Lcom/fasterxml/aalto/in/NsBinding;

    iget-object p1, p1, Lcom/fasterxml/aalto/in/NsBinding;->mURI:Ljava/lang/String;

    if-nez p1, :cond_0

    const-string p1, ""

    :cond_0
    return-object p1

    :cond_1
    const-string v0, "xml"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string p1, "http://www.w3.org/XML/1998/namespace"

    return-object p1

    :cond_2
    const-string v0, "xmlns"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string p1, "http://www.w3.org/2000/xmlns/"

    return-object p1

    :cond_3
    iget-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_lastNsDecl:Lcom/fasterxml/aalto/in/NsDeclaration;

    :goto_0
    if-eqz v0, :cond_5

    invoke-virtual {v0, p1}, Lcom/fasterxml/aalto/in/NsDeclaration;->hasPrefix(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-virtual {v0}, Lcom/fasterxml/aalto/in/NsDeclaration;->getCurrNsURI()Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_4
    invoke-virtual {v0}, Lcom/fasterxml/aalto/in/NsDeclaration;->getPrev()Lcom/fasterxml/aalto/in/NsDeclaration;

    move-result-object v0

    goto :goto_0

    :cond_5
    const/4 p1, 0x0

    return-object p1

    :cond_6
    new-instance p1, Ljava/lang/IllegalArgumentException;

    sget-object v0, Lcom/fasterxml/aalto/impl/ErrorConsts;->ERR_NULL_ARG:Ljava/lang/String;

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public final getNonTransientNamespaceContext()Ljavax/xml/namespace/NamespaceContext;
    .locals 2

    iget-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_lastNsContext:Lcom/fasterxml/aalto/in/FixedNsContext;

    iget-object v1, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_lastNsDecl:Lcom/fasterxml/aalto/in/NsDeclaration;

    invoke-virtual {v0, v1}, Lcom/fasterxml/aalto/in/FixedNsContext;->reuseOrCreate(Lcom/fasterxml/aalto/in/NsDeclaration;)Lcom/fasterxml/aalto/in/FixedNsContext;

    move-result-object v0

    iput-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_lastNsContext:Lcom/fasterxml/aalto/in/FixedNsContext;

    return-object v0
.end method

.method public final getNsCount()I
    .locals 2

    iget v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_currToken:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_currNsCount:I

    return v0

    :cond_0
    iget-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_lastNsDecl:Lcom/fasterxml/aalto/in/NsDeclaration;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iget v1, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_depth:I

    invoke-virtual {v0, v1}, Lcom/fasterxml/aalto/in/NsDeclaration;->countDeclsOnLevel(I)I

    move-result v0

    :goto_0
    return v0
.end method

.method public getPrefix(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    if-eqz p1, :cond_7

    const-string v0, "http://www.w3.org/XML/1998/namespace"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string p1, "xml"

    return-object p1

    :cond_0
    const-string v0, "http://www.w3.org/2000/xmlns/"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string p1, "xmlns"

    return-object p1

    :cond_1
    iget-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_defaultNs:Lcom/fasterxml/aalto/in/NsBinding;

    iget-object v0, v0, Lcom/fasterxml/aalto/in/NsBinding;->mURI:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string p1, ""

    return-object p1

    :cond_2
    iget-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_lastNsDecl:Lcom/fasterxml/aalto/in/NsDeclaration;

    :goto_0
    if-eqz v0, :cond_6

    invoke-virtual {v0, p1}, Lcom/fasterxml/aalto/in/NsDeclaration;->hasNsURI(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-virtual {v0}, Lcom/fasterxml/aalto/in/NsDeclaration;->getPrefix()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_5

    iget-object v2, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_lastNsDecl:Lcom/fasterxml/aalto/in/NsDeclaration;

    :goto_1
    if-eq v2, v0, :cond_4

    invoke-virtual {v2, v1}, Lcom/fasterxml/aalto/in/NsDeclaration;->hasPrefix(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_2

    :cond_3
    invoke-virtual {v2}, Lcom/fasterxml/aalto/in/NsDeclaration;->getPrev()Lcom/fasterxml/aalto/in/NsDeclaration;

    move-result-object v2

    goto :goto_1

    :cond_4
    return-object v1

    :cond_5
    :goto_2
    invoke-virtual {v0}, Lcom/fasterxml/aalto/in/NsDeclaration;->getPrev()Lcom/fasterxml/aalto/in/NsDeclaration;

    move-result-object v0

    goto :goto_0

    :cond_6
    const/4 p1, 0x0

    return-object p1

    :cond_7
    new-instance p1, Ljava/lang/IllegalArgumentException;

    sget-object v0, Lcom/fasterxml/aalto/impl/ErrorConsts;->ERR_NULL_ARG:Ljava/lang/String;

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public getPrefixes(Ljava/lang/String;)Ljava/util/Iterator;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Iterator<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    if-eqz p1, :cond_a

    const-string v0, "http://www.w3.org/XML/1998/namespace"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance p1, Lcom/fasterxml/aalto/util/SingletonIterator;

    const-string v0, "xml"

    invoke-direct {p1, v0}, Lcom/fasterxml/aalto/util/SingletonIterator;-><init>(Ljava/lang/String;)V

    return-object p1

    :cond_0
    const-string v0, "http://www.w3.org/2000/xmlns/"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance p1, Lcom/fasterxml/aalto/util/SingletonIterator;

    const-string v0, "xmlns"

    invoke-direct {p1, v0}, Lcom/fasterxml/aalto/util/SingletonIterator;-><init>(Ljava/lang/String;)V

    return-object p1

    :cond_1
    iget-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_defaultNs:Lcom/fasterxml/aalto/in/NsBinding;

    iget-object v0, v0, Lcom/fasterxml/aalto/in/NsBinding;->mURI:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_lastNsDecl:Lcom/fasterxml/aalto/in/NsDeclaration;

    :goto_1
    if-eqz v1, :cond_7

    invoke-virtual {v1, p1}, Lcom/fasterxml/aalto/in/NsDeclaration;->hasNsURI(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-virtual {v1}, Lcom/fasterxml/aalto/in/NsDeclaration;->getPrefix()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_6

    iget-object v3, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_lastNsDecl:Lcom/fasterxml/aalto/in/NsDeclaration;

    :goto_2
    if-eq v3, v1, :cond_4

    invoke-virtual {v3, v2}, Lcom/fasterxml/aalto/in/NsDeclaration;->hasPrefix(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    goto :goto_3

    :cond_3
    invoke-virtual {v3}, Lcom/fasterxml/aalto/in/NsDeclaration;->getPrev()Lcom/fasterxml/aalto/in/NsDeclaration;

    move-result-object v3

    goto :goto_2

    :cond_4
    if-nez v0, :cond_5

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    :cond_5
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_6
    :goto_3
    invoke-virtual {v1}, Lcom/fasterxml/aalto/in/NsDeclaration;->getPrev()Lcom/fasterxml/aalto/in/NsDeclaration;

    move-result-object v1

    goto :goto_1

    :cond_7
    if-nez v0, :cond_8

    invoke-static {}, Lcom/fasterxml/aalto/util/EmptyIterator;->getInstance()Lcom/fasterxml/aalto/util/EmptyIterator;

    move-result-object p1

    return-object p1

    :cond_8
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result p1

    const/4 v1, 0x1

    if-ne p1, v1, :cond_9

    new-instance p1, Lcom/fasterxml/aalto/util/SingletonIterator;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {p1, v0}, Lcom/fasterxml/aalto/util/SingletonIterator;-><init>(Ljava/lang/String;)V

    return-object p1

    :cond_9
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p1

    return-object p1

    :cond_a
    new-instance p1, Ljava/lang/IllegalArgumentException;

    sget-object v0, Lcom/fasterxml/aalto/impl/ErrorConsts;->ERR_NULL_ARG:Ljava/lang/String;

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public final getQName()Ljavax/xml/namespace/QName;
    .locals 2

    iget-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_tokenName:Lcom/fasterxml/aalto/in/PName;

    iget-object v1, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_defaultNs:Lcom/fasterxml/aalto/in/NsBinding;

    invoke-virtual {v0, v1}, Lcom/fasterxml/aalto/in/PName;->constructQName(Lcom/fasterxml/aalto/in/NsBinding;)Ljavax/xml/namespace/QName;

    move-result-object v0

    return-object v0
.end method

.method public final getStartLocation()Lorg/codehaus/stax2/XMLStreamLocation2;
    .locals 8

    iget-wide v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_startRow:J

    long-to-int v6, v0

    iget-wide v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_startColumn:J

    long-to-int v7, v0

    iget-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_config:Lcom/fasterxml/aalto/in/ReaderConfig;

    invoke-virtual {v0}, Lcom/fasterxml/aalto/in/ReaderConfig;->getPublicId()Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_config:Lcom/fasterxml/aalto/in/ReaderConfig;

    invoke-virtual {v0}, Lcom/fasterxml/aalto/in/ReaderConfig;->getSystemId()Ljava/lang/String;

    move-result-object v3

    iget-wide v4, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_startRawOffset:J

    invoke-static/range {v2 .. v7}, Lcom/fasterxml/aalto/impl/LocationImpl;->fromZeroBased(Ljava/lang/String;Ljava/lang/String;JII)Lcom/fasterxml/aalto/impl/LocationImpl;

    move-result-object v0

    return-object v0
.end method

.method public abstract getStartingByteOffset()J
.end method

.method public abstract getStartingCharOffset()J
.end method

.method public final getText(Ljava/io/Writer;Z)I
    .locals 0

    .line 1
    iget-boolean p2, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_tokenIncomplete:Z

    if-eqz p2, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/aalto/in/XmlScanner;->finishToken()V

    :cond_0
    :try_start_0
    iget-object p2, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_textBuilder:Lcom/fasterxml/aalto/util/TextBuilder;

    invoke-virtual {p2, p1}, Lcom/fasterxml/aalto/util/TextBuilder;->rawContentsTo(Ljava/io/Writer;)I

    move-result p1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return p1

    :catch_0
    move-exception p1

    new-instance p2, Lcom/fasterxml/aalto/impl/IoStreamException;

    invoke-direct {p2, p1}, Lcom/fasterxml/aalto/impl/IoStreamException;-><init>(Ljava/io/IOException;)V

    throw p2
.end method

.method public final getText()Ljava/lang/String;
    .locals 1

    .line 2
    iget-boolean v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_tokenIncomplete:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/aalto/in/XmlScanner;->finishToken()V

    :cond_0
    iget-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_textBuilder:Lcom/fasterxml/aalto/util/TextBuilder;

    invoke-virtual {v0}, Lcom/fasterxml/aalto/util/TextBuilder;->contentsAsString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getTextCharacters(I[CII)I
    .locals 1

    .line 1
    iget-boolean v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_tokenIncomplete:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/aalto/in/XmlScanner;->finishToken()V

    :cond_0
    iget-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_textBuilder:Lcom/fasterxml/aalto/util/TextBuilder;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/fasterxml/aalto/util/TextBuilder;->contentsToArray(I[CII)I

    move-result p1

    return p1
.end method

.method public final getTextCharacters()[C
    .locals 1

    .line 2
    iget-boolean v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_tokenIncomplete:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/aalto/in/XmlScanner;->finishToken()V

    :cond_0
    iget-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_textBuilder:Lcom/fasterxml/aalto/util/TextBuilder;

    invoke-virtual {v0}, Lcom/fasterxml/aalto/util/TextBuilder;->getTextBuffer()[C

    move-result-object v0

    return-object v0
.end method

.method public final getTextLength()I
    .locals 1

    iget-boolean v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_tokenIncomplete:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/aalto/in/XmlScanner;->finishToken()V

    :cond_0
    iget-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_textBuilder:Lcom/fasterxml/aalto/util/TextBuilder;

    invoke-virtual {v0}, Lcom/fasterxml/aalto/util/TextBuilder;->size()I

    move-result v0

    return v0
.end method

.method public handleInvalidXmlChar(I)C
    .locals 3

    iget-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_config:Lcom/fasterxml/aalto/in/ReaderConfig;

    invoke-virtual {v0}, Lcom/fasterxml/aalto/in/ReaderConfig;->getIllegalCharHandler()Lcom/fasterxml/aalto/util/IllegalCharHandler;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0, p1}, Lcom/fasterxml/aalto/util/IllegalCharHandler;->convertIllegalChar(I)C

    move-result p1

    return p1

    :cond_0
    int-to-char v0, p1

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/fasterxml/aalto/in/XmlScanner;->throwNullChar()V

    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Illegal XML character ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {v0}, Lcom/fasterxml/aalto/util/XmlChars;->getCharDesc(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-boolean v2, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_xml11:Z

    if-eqz v2, :cond_2

    const/16 v2, 0x20

    if-ge p1, v2, :cond_2

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " [note: in XML 1.1, it could be included via entity expansion]"

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    :cond_2
    invoke-virtual {p0, v1}, Lcom/fasterxml/aalto/in/XmlScanner;->reportInputProblem(Ljava/lang/String;)V

    return v0
.end method

.method public final hasEmptyStack()Z
    .locals 1

    iget v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_depth:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final isAttrSpecified(I)Z
    .locals 0

    const/4 p1, 0x1

    return p1
.end method

.method public final isEmptyTag()Z
    .locals 1

    iget-boolean v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_isEmptyTag:Z

    return v0
.end method

.method public final isTextWhitespace()Z
    .locals 1

    iget-boolean v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_tokenIncomplete:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/aalto/in/XmlScanner;->finishToken()V

    :cond_0
    iget-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_textBuilder:Lcom/fasterxml/aalto/util/TextBuilder;

    invoke-virtual {v0}, Lcom/fasterxml/aalto/util/TextBuilder;->isAllWhitespace()Z

    move-result v0

    return v0
.end method

.method public abstract loadMore()Z
.end method

.method public final loadMoreGuaranteed()V
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/fasterxml/aalto/in/XmlScanner;->loadMore()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unexpected end-of-input when trying to parse "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_currToken:I

    invoke-static {v1}, Lcom/fasterxml/aalto/impl/ErrorConsts;->tokenTypeDesc(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/in/XmlScanner;->reportInputProblem(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public final loadMoreGuaranteed(I)V
    .locals 2

    .line 2
    invoke-virtual {p0}, Lcom/fasterxml/aalto/in/XmlScanner;->loadMore()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unexpected end-of-input when trying to parse "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {p1}, Lcom/fasterxml/aalto/impl/ErrorConsts;->tokenTypeDesc(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/fasterxml/aalto/in/XmlScanner;->reportInputProblem(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public abstract nextFromProlog(Z)I
.end method

.method public abstract nextFromTree()I
.end method

.method public reportDoubleHyphenInComments()V
    .locals 1

    const-string v0, "String \'--\' not allowed in comment (missing \'>\'?)"

    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/in/XmlScanner;->reportInputProblem(Ljava/lang/String;)V

    return-void
.end method

.method public reportDuplicateNsDecl(Ljava/lang/String;)V
    .locals 2

    if-nez p1, :cond_0

    const-string p1, "Duplicate namespace declaration for the default namespace"

    goto :goto_0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Duplicate namespace declaration for prefix \'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "\'"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    :goto_0
    invoke-virtual {p0, p1}, Lcom/fasterxml/aalto/in/XmlScanner;->reportInputProblem(Ljava/lang/String;)V

    return-void
.end method

.method public reportEntityOverflow()V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Illegal character entity: value higher than max allowed (0x"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const v1, 0x10ffff

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/in/XmlScanner;->reportInputProblem(Ljava/lang/String;)V

    return-void
.end method

.method public reportEofInName([CI)V
    .locals 0

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string p2, "Unexpected end-of-input in name (parsing "

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget p2, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_currToken:I

    invoke-static {p2}, Lcom/fasterxml/aalto/impl/ErrorConsts;->tokenTypeDesc(I)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, ")"

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/fasterxml/aalto/in/XmlScanner;->reportInputProblem(Ljava/lang/String;)V

    return-void
.end method

.method public reportIllegalCDataEnd()V
    .locals 1

    const-string v0, "String \']]>\' not allowed in textual content, except as the end marker of CDATA section"

    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/in/XmlScanner;->reportInputProblem(Ljava/lang/String;)V

    return-void
.end method

.method public reportIllegalNsDecl(Ljava/lang/String;)V
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Illegal namespace declaration: can not re-bind prefix \'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "\'"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/fasterxml/aalto/in/XmlScanner;->reportInputProblem(Ljava/lang/String;)V

    return-void
.end method

.method public reportIllegalNsDecl(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .line 2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Illegal namespace declaration: can not bind URI \'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, "\' to prefix other than \'"

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "\'"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/fasterxml/aalto/in/XmlScanner;->reportInputProblem(Ljava/lang/String;)V

    return-void
.end method

.method public reportInputProblem(Ljava/lang/String;)V
    .locals 2

    new-instance v0, Lcom/fasterxml/aalto/WFCException;

    invoke-virtual {p0}, Lcom/fasterxml/aalto/in/XmlScanner;->getCurrentLocation()Lorg/codehaus/stax2/XMLStreamLocation2;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Lcom/fasterxml/aalto/WFCException;-><init>(Ljava/lang/String;Ljavax/xml/stream/Location;)V

    throw v0
.end method

.method public reportInvalidNameChar(II)V
    .locals 2

    const/16 v0, 0x3a

    if-ne p1, v0, :cond_0

    const-string v0, "Invalid colon in name: at most one colon allowed in element/attribute names, and none in PI target or entity names"

    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/in/XmlScanner;->reportInputProblem(Ljava/lang/String;)V

    :cond_0
    const-string v0, ")"

    if-nez p2, :cond_1

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Invalid name start character (0x"

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p0, p2}, Lcom/fasterxml/aalto/in/XmlScanner;->reportInputProblem(Ljava/lang/String;)V

    :cond_1
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Invalid name character (0x"

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/fasterxml/aalto/in/XmlScanner;->reportInputProblem(Ljava/lang/String;)V

    return-void
.end method

.method public reportInvalidNsIndex(I)V
    .locals 3

    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Illegal namespace declaration index, "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, ", current START_ELEMENT/END_ELEMENT has "

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/fasterxml/aalto/in/XmlScanner;->getNsCount()I

    move-result p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, " declarations"

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public reportInvalidXmlChar(I)V
    .locals 3

    if-nez p1, :cond_0

    const-string v0, "Invalid null character"

    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/in/XmlScanner;->reportInputProblem(Ljava/lang/String;)V

    :cond_0
    const/16 v0, 0x20

    const-string v1, ")"

    if-ge p1, v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid white space character (0x"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/in/XmlScanner;->reportInputProblem(Ljava/lang/String;)V

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid xml content character (0x"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/fasterxml/aalto/in/XmlScanner;->reportInputProblem(Ljava/lang/String;)V

    return-void
.end method

.method public reportMissingPISpace(I)V
    .locals 1

    const-string v0, ": expected either white space, or closing \'?>\'"

    invoke-virtual {p0, p1, v0}, Lcom/fasterxml/aalto/in/XmlScanner;->throwUnexpectedChar(ILjava/lang/String;)V

    return-void
.end method

.method public reportMultipleColonsInName()V
    .locals 1

    const-string v0, "Multiple colons not allowed in names"

    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/in/XmlScanner;->reportInputProblem(Ljava/lang/String;)V

    return-void
.end method

.method public reportPrologProblem(ZLjava/lang/String;)V
    .locals 1

    if-eqz p1, :cond_0

    sget-object p1, Lcom/fasterxml/aalto/impl/ErrorConsts;->SUFFIX_IN_PROLOG:Ljava/lang/String;

    goto :goto_0

    :cond_0
    sget-object p1, Lcom/fasterxml/aalto/impl/ErrorConsts;->SUFFIX_IN_EPILOG:Ljava/lang/String;

    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, ": "

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/fasterxml/aalto/in/XmlScanner;->reportInputProblem(Ljava/lang/String;)V

    return-void
.end method

.method public reportPrologUnexpChar(ZILjava/lang/String;)V
    .locals 1

    if-eqz p1, :cond_0

    sget-object p1, Lcom/fasterxml/aalto/impl/ErrorConsts;->SUFFIX_IN_PROLOG:Ljava/lang/String;

    goto :goto_0

    :cond_0
    sget-object p1, Lcom/fasterxml/aalto/impl/ErrorConsts;->SUFFIX_IN_EPILOG:Ljava/lang/String;

    :goto_0
    if-nez p3, :cond_1

    const/16 p3, 0x26

    if-ne p2, p3, :cond_2

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "; no entities allowed"

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p0, p2, p3}, Lcom/fasterxml/aalto/in/XmlScanner;->throwUnexpectedChar(ILjava/lang/String;)V

    goto :goto_1

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    :cond_2
    :goto_1
    invoke-virtual {p0, p2, p1}, Lcom/fasterxml/aalto/in/XmlScanner;->throwUnexpectedChar(ILjava/lang/String;)V

    return-void
.end method

.method public reportPrologUnexpElement(ZI)V
    .locals 2

    if-gez p2, :cond_0

    const v0, 0x7ffff

    and-int/2addr p2, v0

    :cond_0
    const/16 v0, 0x2f

    if-ne p2, v0, :cond_2

    if-eqz p1, :cond_1

    const-string v0, "Unexpected end element in prolog: malformed XML document, expected root element"

    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/in/XmlScanner;->reportInputProblem(Ljava/lang/String;)V

    :cond_1
    const-string v0, "Unexpected end element in epilog: malformed XML document (unbalanced start/end tags?)"

    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/in/XmlScanner;->reportInputProblem(Ljava/lang/String;)V

    :cond_2
    const/16 v0, 0x20

    if-ge p2, v0, :cond_4

    if-eqz p1, :cond_3

    sget-object p1, Lcom/fasterxml/aalto/impl/ErrorConsts;->SUFFIX_IN_PROLOG:Ljava/lang/String;

    goto :goto_0

    :cond_3
    sget-object p1, Lcom/fasterxml/aalto/impl/ErrorConsts;->SUFFIX_IN_EPILOG:Ljava/lang/String;

    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unrecognized directive "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p2, p1}, Lcom/fasterxml/aalto/in/XmlScanner;->throwUnexpectedChar(ILjava/lang/String;)V

    :cond_4
    const-string p1, "Second root element in content: malformed XML document, only one allowed"

    invoke-virtual {p0, p1}, Lcom/fasterxml/aalto/in/XmlScanner;->reportInputProblem(Ljava/lang/String;)V

    return-void
.end method

.method public reportTreeUnexpChar(ILjava/lang/String;)V
    .locals 2

    sget-object v0, Lcom/fasterxml/aalto/impl/ErrorConsts;->SUFFIX_IN_TREE:Ljava/lang/String;

    if-eqz p2, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_0
    invoke-virtual {p0, p1, v0}, Lcom/fasterxml/aalto/in/XmlScanner;->throwUnexpectedChar(ILjava/lang/String;)V

    return-void
.end method

.method public reportUnboundPrefix(Lcom/fasterxml/aalto/in/PName;Z)V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unbound namespace prefix \'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/fasterxml/aalto/in/PName;->getPrefix()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\' (for "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-eqz p2, :cond_0

    const-string p2, "attribute"

    goto :goto_0

    :cond_0
    const-string p2, "element"

    :goto_0
    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, " name \'"

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/fasterxml/aalto/in/PName;->getPrefixedName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "\')"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/fasterxml/aalto/in/XmlScanner;->reportInputProblem(Ljava/lang/String;)V

    return-void
.end method

.method public reportUnexpandedEntityInAttr(Lcom/fasterxml/aalto/in/PName;Z)V
    .locals 1

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Unexpanded ENTITY_REFERENCE ("

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_tokenName:Lcom/fasterxml/aalto/in/PName;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v0, ") in "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-eqz p2, :cond_0

    const-string p2, "namespace declaration"

    goto :goto_0

    :cond_0
    const-string p2, "attribute value"

    :goto_0
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/fasterxml/aalto/in/XmlScanner;->reportInputProblem(Ljava/lang/String;)V

    return-void
.end method

.method public reportUnexpectedEndTag(Ljava/lang/String;)V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unexpected end tag: expected </"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, ">"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/fasterxml/aalto/in/XmlScanner;->reportInputProblem(Ljava/lang/String;)V

    return-void
.end method

.method public final resetForDecoding(Lorg/codehaus/stax2/typed/Base64Variant;Lorg/codehaus/stax2/ri/typed/CharArrayBase64Decoder;Z)V
    .locals 1

    iget-boolean v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_tokenIncomplete:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/aalto/in/XmlScanner;->finishToken()V

    :cond_0
    iget-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_textBuilder:Lcom/fasterxml/aalto/util/TextBuilder;

    invoke-virtual {v0, p1, p2, p3}, Lcom/fasterxml/aalto/util/TextBuilder;->resetForBinaryDecode(Lorg/codehaus/stax2/typed/Base64Variant;Lorg/codehaus/stax2/ri/typed/CharArrayBase64Decoder;Z)V

    return-void
.end method

.method public abstract skipCData()V
.end method

.method public abstract skipCharacters()Z
.end method

.method public abstract skipCoalescedText()Z
.end method

.method public abstract skipComment()V
.end method

.method public abstract skipPI()V
.end method

.method public abstract skipSpace()V
.end method

.method public final skipToken()Z
    .locals 5

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_tokenIncomplete:Z

    iget v1, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_currToken:I

    const/4 v2, 0x3

    if-eq v1, v2, :cond_6

    const/4 v2, 0x4

    const/4 v3, 0x1

    const/16 v4, 0x9

    if-eq v1, v2, :cond_4

    const/4 v2, 0x5

    if-eq v1, v2, :cond_3

    const/4 v2, 0x6

    if-eq v1, v2, :cond_2

    const/16 v2, 0xb

    if-eq v1, v2, :cond_1

    const/16 v2, 0xc

    if-ne v1, v2, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/aalto/in/XmlScanner;->skipCData()V

    iget-boolean v1, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_cfgCoalescing:Z

    if-eqz v1, :cond_7

    invoke-virtual {p0}, Lcom/fasterxml/aalto/in/XmlScanner;->skipCoalescedText()Z

    iget-boolean v1, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_entityPending:Z

    if-eqz v1, :cond_7

    iput v4, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_currToken:I

    return v3

    :cond_0
    new-instance v0, Ljava/lang/Error;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Internal error, unexpected incomplete token type "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_currToken:I

    invoke-static {v2}, Lcom/fasterxml/aalto/impl/ErrorConsts;->tokenTypeDesc(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/Error;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/in/XmlScanner;->finishDTD(Z)V

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/fasterxml/aalto/in/XmlScanner;->skipSpace()V

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/fasterxml/aalto/in/XmlScanner;->skipComment()V

    goto :goto_0

    :cond_4
    invoke-virtual {p0}, Lcom/fasterxml/aalto/in/XmlScanner;->skipCharacters()Z

    move-result v1

    if-eqz v1, :cond_5

    iput v4, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_currToken:I

    return v3

    :cond_5
    iget-boolean v1, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_cfgCoalescing:Z

    if-eqz v1, :cond_7

    invoke-virtual {p0}, Lcom/fasterxml/aalto/in/XmlScanner;->skipCoalescedText()Z

    move-result v1

    if-eqz v1, :cond_7

    iput v4, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_currToken:I

    return v3

    :cond_6
    invoke-virtual {p0}, Lcom/fasterxml/aalto/in/XmlScanner;->skipPI()V

    :cond_7
    :goto_0
    return v0
.end method

.method public throwInvalidSpace(I)V
    .locals 3

    int-to-char v0, p1

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/aalto/in/XmlScanner;->throwNullChar()V

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Illegal character ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {v0}, Lcom/fasterxml/aalto/util/XmlChars;->getCharDesc(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, ")"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-boolean v1, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_xml11:Z

    if-eqz v1, :cond_1

    const/16 v1, 0x20

    if-ge p1, v1, :cond_1

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " [note: in XML 1.1, it could be included via entity expansion]"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_1
    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/in/XmlScanner;->reportInputProblem(Ljava/lang/String;)V

    return-void
.end method

.method public throwNullChar()V
    .locals 1

    const-string v0, "Illegal character (NULL, unicode 0) encountered: not valid in any content"

    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/in/XmlScanner;->reportInputProblem(Ljava/lang/String;)V

    return-void
.end method

.method public throwUnexpectedChar(ILjava/lang/String;)V
    .locals 2

    const/16 v0, 0x20

    if-ge p1, v0, :cond_0

    const/16 v0, 0xd

    if-eq p1, v0, :cond_0

    const/16 v0, 0xa

    if-eq p1, v0, :cond_0

    const/16 v0, 0x9

    if-eq p1, v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/fasterxml/aalto/in/XmlScanner;->throwInvalidSpace(I)V

    :cond_0
    int-to-char p1, p1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unexpected character "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {p1}, Lcom/fasterxml/aalto/util/XmlChars;->getCharDesc(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/fasterxml/aalto/in/XmlScanner;->reportInputProblem(Ljava/lang/String;)V

    return-void
.end method

.method public final verifyXmlChar(I)V
    .locals 1

    const v0, 0xd800

    if-lt p1, v0, :cond_2

    const v0, 0xe000

    if-ge p1, v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/fasterxml/aalto/in/XmlScanner;->reportInvalidXmlChar(I)V

    :cond_0
    const v0, 0xfffe

    if-eq p1, v0, :cond_1

    const v0, 0xffff

    if-ne p1, v0, :cond_3

    :cond_1
    :goto_0
    invoke-virtual {p0, p1}, Lcom/fasterxml/aalto/in/XmlScanner;->reportInvalidXmlChar(I)V

    goto :goto_1

    :cond_2
    const/16 v0, 0x20

    if-ge p1, v0, :cond_3

    const/16 v0, 0xa

    if-eq p1, v0, :cond_3

    const/16 v0, 0xd

    if-eq p1, v0, :cond_3

    const/16 v0, 0x9

    if-eq p1, v0, :cond_3

    iget-boolean v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_xml11:Z

    if-eqz v0, :cond_1

    if-nez p1, :cond_3

    goto :goto_0

    :cond_3
    :goto_1
    return-void
.end method

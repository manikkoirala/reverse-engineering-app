.class public abstract Lcom/fasterxml/aalto/in/InputBootstrapper;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/fasterxml/aalto/util/XmlConsts;


# static fields
.field public static final ERR_XMLDECL_END_MARKER:Ljava/lang/String; = "; expected \"?>\" end marker"

.field public static final ERR_XMLDECL_EXP_ATTRVAL:Ljava/lang/String; = "; expected a quote character enclosing value for "

.field public static final ERR_XMLDECL_EXP_EQ:Ljava/lang/String; = "; expected \'=\' after "

.field public static final ERR_XMLDECL_EXP_SPACE:Ljava/lang/String; = "; expected a white space"

.field public static final ERR_XMLDECL_KW_ENCODING:Ljava/lang/String; = "; expected keyword \'encoding\'"

.field public static final ERR_XMLDECL_KW_STANDALONE:Ljava/lang/String; = "; expected keyword \'standalone\'"

.field public static final ERR_XMLDECL_KW_VERSION:Ljava/lang/String; = "; expected keyword \'version\'"


# instance fields
.field final _config:Lcom/fasterxml/aalto/in/ReaderConfig;

.field protected _inputProcessed:I

.field protected _inputRow:I

.field protected _inputRowStart:I

.field mDeclaredXmlVersion:I

.field mFoundEncoding:Ljava/lang/String;

.field final mKeyword:[C

.field mStandalone:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/fasterxml/aalto/in/ReaderConfig;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/fasterxml/aalto/in/InputBootstrapper;->_inputProcessed:I

    iput v0, p0, Lcom/fasterxml/aalto/in/InputBootstrapper;->_inputRow:I

    iput v0, p0, Lcom/fasterxml/aalto/in/InputBootstrapper;->_inputRowStart:I

    iput v0, p0, Lcom/fasterxml/aalto/in/InputBootstrapper;->mDeclaredXmlVersion:I

    iput-object p1, p0, Lcom/fasterxml/aalto/in/InputBootstrapper;->_config:Lcom/fasterxml/aalto/in/ReaderConfig;

    const/16 v0, 0x3c

    invoke-virtual {p1, v0}, Lcom/fasterxml/aalto/in/ReaderConfig;->allocSmallCBuffer(I)[C

    move-result-object p1

    iput-object p1, p0, Lcom/fasterxml/aalto/in/InputBootstrapper;->mKeyword:[C

    return-void
.end method

.method private final getWsOrChar(I)I
    .locals 3

    invoke-virtual {p0}, Lcom/fasterxml/aalto/in/InputBootstrapper;->getNext()I

    move-result v0

    if-ne v0, p1, :cond_0

    return v0

    :cond_0
    const/16 v1, 0x20

    if-le v0, v1, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "; expected either \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    int-to-char p1, p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string p1, "\' or white space"

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, v0, p1}, Lcom/fasterxml/aalto/in/InputBootstrapper;->reportUnexpectedChar(ILjava/lang/String;)V

    :cond_1
    const/16 p1, 0xa

    if-eq v0, p1, :cond_2

    const/16 p1, 0xd

    if-ne v0, p1, :cond_3

    :cond_2
    invoke-virtual {p0}, Lcom/fasterxml/aalto/in/InputBootstrapper;->pushback()V

    :cond_3
    const/4 p1, 0x0

    invoke-virtual {p0, p1}, Lcom/fasterxml/aalto/in/InputBootstrapper;->getNextAfterWs(Z)I

    move-result p1

    return p1
.end method

.method private final handleEq(Ljava/lang/String;)I
    .locals 5

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/in/InputBootstrapper;->getNextAfterWs(Z)I

    move-result v1

    const/16 v2, 0x3d

    const-string v3, "\'"

    if-eq v1, v2, :cond_0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "; expected \'=\' after \'"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lcom/fasterxml/aalto/in/InputBootstrapper;->reportUnexpectedChar(ILjava/lang/String;)V

    :cond_0
    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/in/InputBootstrapper;->getNextAfterWs(Z)I

    move-result v0

    const/16 v1, 0x22

    if-eq v0, v1, :cond_1

    const/16 v1, 0x27

    if-eq v0, v1, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "; expected a quote character enclosing value for \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, v0, p1}, Lcom/fasterxml/aalto/in/InputBootstrapper;->reportUnexpectedChar(ILjava/lang/String;)V

    :cond_1
    return v0
.end method

.method private final readXmlEncoding()Ljava/lang/String;
    .locals 4

    const-string v0, "encoding"

    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/in/InputBootstrapper;->checkKeyword(Ljava/lang/String;)I

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0, v1, v0}, Lcom/fasterxml/aalto/in/InputBootstrapper;->reportUnexpectedChar(ILjava/lang/String;)V

    :cond_0
    invoke-direct {p0, v0}, Lcom/fasterxml/aalto/in/InputBootstrapper;->handleEq(Ljava/lang/String;)I

    move-result v1

    iget-object v2, p0, Lcom/fasterxml/aalto/in/InputBootstrapper;->mKeyword:[C

    invoke-virtual {p0, v2, v1}, Lcom/fasterxml/aalto/in/InputBootstrapper;->readQuotedValue([CI)I

    move-result v1

    if-nez v1, :cond_1

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v2, v2, v2}, Lcom/fasterxml/aalto/in/InputBootstrapper;->reportPseudoAttrProblem(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    new-instance v0, Ljava/lang/String;

    if-gez v1, :cond_2

    iget-object v1, p0, Lcom/fasterxml/aalto/in/InputBootstrapper;->mKeyword:[C

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([C)V

    return-object v0

    :cond_2
    iget-object v2, p0, Lcom/fasterxml/aalto/in/InputBootstrapper;->mKeyword:[C

    const/4 v3, 0x0

    invoke-direct {v0, v2, v3, v1}, Ljava/lang/String;-><init>([CII)V

    return-object v0
.end method

.method private final readXmlStandalone()Ljava/lang/String;
    .locals 10

    const-string v0, "standalone"

    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/in/InputBootstrapper;->checkKeyword(Ljava/lang/String;)I

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0, v1, v0}, Lcom/fasterxml/aalto/in/InputBootstrapper;->reportUnexpectedChar(ILjava/lang/String;)V

    :cond_0
    invoke-direct {p0, v0}, Lcom/fasterxml/aalto/in/InputBootstrapper;->handleEq(Ljava/lang/String;)I

    move-result v1

    iget-object v2, p0, Lcom/fasterxml/aalto/in/InputBootstrapper;->mKeyword:[C

    invoke-virtual {p0, v2, v1}, Lcom/fasterxml/aalto/in/InputBootstrapper;->readQuotedValue([CI)I

    move-result v1

    const-string v2, "yes"

    const-string v3, "no"

    const/4 v4, 0x1

    const/4 v5, 0x2

    const/4 v6, 0x0

    if-ne v1, v5, :cond_1

    iget-object v5, p0, Lcom/fasterxml/aalto/in/InputBootstrapper;->mKeyword:[C

    aget-char v7, v5, v6

    const/16 v8, 0x6e

    if-ne v7, v8, :cond_2

    aget-char v4, v5, v4

    const/16 v5, 0x6f

    if-ne v4, v5, :cond_2

    return-object v3

    :cond_1
    const/4 v7, 0x3

    if-ne v1, v7, :cond_2

    iget-object v7, p0, Lcom/fasterxml/aalto/in/InputBootstrapper;->mKeyword:[C

    aget-char v8, v7, v6

    const/16 v9, 0x79

    if-ne v8, v9, :cond_2

    aget-char v4, v7, v4

    const/16 v8, 0x65

    if-ne v4, v8, :cond_2

    aget-char v4, v7, v5

    const/16 v5, 0x73

    if-ne v4, v5, :cond_2

    return-object v2

    :cond_2
    const-string v4, "\'"

    if-gez v1, :cond_3

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v4, Ljava/lang/String;

    iget-object v5, p0, Lcom/fasterxml/aalto/in/InputBootstrapper;->mKeyword:[C

    invoke-direct {v4, v5}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "[..]\'"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_3
    if-nez v1, :cond_4

    const-string v1, "<empty>"

    goto :goto_0

    :cond_4
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v7, Ljava/lang/String;

    iget-object v8, p0, Lcom/fasterxml/aalto/in/InputBootstrapper;->mKeyword:[C

    invoke-direct {v7, v8, v6, v1}, Ljava/lang/String;-><init>([CII)V

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    :goto_0
    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/fasterxml/aalto/in/InputBootstrapper;->reportPseudoAttrProblem(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v1
.end method

.method private final readXmlVersion()I
    .locals 7

    const-string v0, "version"

    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/in/InputBootstrapper;->checkKeyword(Ljava/lang/String;)I

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0, v1, v0}, Lcom/fasterxml/aalto/in/InputBootstrapper;->reportUnexpectedChar(ILjava/lang/String;)V

    :cond_0
    invoke-direct {p0, v0}, Lcom/fasterxml/aalto/in/InputBootstrapper;->handleEq(Ljava/lang/String;)I

    move-result v1

    iget-object v2, p0, Lcom/fasterxml/aalto/in/InputBootstrapper;->mKeyword:[C

    invoke-virtual {p0, v2, v1}, Lcom/fasterxml/aalto/in/InputBootstrapper;->readQuotedValue([CI)I

    move-result v1

    const/4 v2, 0x3

    const/4 v3, 0x0

    if-ne v1, v2, :cond_2

    iget-object v2, p0, Lcom/fasterxml/aalto/in/InputBootstrapper;->mKeyword:[C

    aget-char v4, v2, v3

    const/16 v5, 0x31

    if-ne v4, v5, :cond_2

    const/4 v4, 0x1

    aget-char v4, v2, v4

    const/16 v6, 0x2e

    if-ne v4, v6, :cond_2

    const/4 v4, 0x2

    aget-char v2, v2, v4

    const/16 v4, 0x30

    if-ne v2, v4, :cond_1

    const/16 v0, 0x100

    return v0

    :cond_1
    if-ne v2, v5, :cond_2

    const/16 v0, 0x110

    return v0

    :cond_2
    const-string v2, "\'"

    if-gez v1, :cond_3

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v2, Ljava/lang/String;

    iget-object v4, p0, Lcom/fasterxml/aalto/in/InputBootstrapper;->mKeyword:[C

    invoke-direct {v2, v4}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "[..]\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_3
    if-nez v1, :cond_4

    const-string v1, "<empty>"

    goto :goto_0

    :cond_4
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v5, Ljava/lang/String;

    iget-object v6, p0, Lcom/fasterxml/aalto/in/InputBootstrapper;->mKeyword:[C

    invoke-direct {v5, v6, v3, v1}, Ljava/lang/String;-><init>([CII)V

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    :goto_0
    const-string v2, "1.0"

    const-string v4, "1.1"

    invoke-virtual {p0, v0, v1, v2, v4}, Lcom/fasterxml/aalto/in/InputBootstrapper;->reportPseudoAttrProblem(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return v3
.end method


# virtual methods
.method public abstract bootstrap()Lcom/fasterxml/aalto/in/XmlScanner;
.end method

.method public abstract checkKeyword(Ljava/lang/String;)I
.end method

.method public abstract getLocation()Ljavax/xml/stream/Location;
.end method

.method public abstract getNext()I
.end method

.method public abstract getNextAfterWs(Z)I
.end method

.method public abstract pushback()V
.end method

.method public abstract readQuotedValue([CI)I
.end method

.method public readXmlDeclaration()V
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/in/InputBootstrapper;->getNextAfterWs(Z)I

    move-result v0

    const/16 v1, 0x76

    const/16 v2, 0x3f

    if-eq v0, v1, :cond_0

    const-string v1, "; expected keyword \'version\'"

    invoke-virtual {p0, v0, v1}, Lcom/fasterxml/aalto/in/InputBootstrapper;->reportUnexpectedChar(ILjava/lang/String;)V

    goto :goto_0

    :cond_0
    invoke-direct {p0}, Lcom/fasterxml/aalto/in/InputBootstrapper;->readXmlVersion()I

    move-result v0

    iput v0, p0, Lcom/fasterxml/aalto/in/InputBootstrapper;->mDeclaredXmlVersion:I

    invoke-direct {p0, v2}, Lcom/fasterxml/aalto/in/InputBootstrapper;->getWsOrChar(I)I

    move-result v0

    :goto_0
    const/16 v1, 0x65

    if-ne v0, v1, :cond_1

    invoke-direct {p0}, Lcom/fasterxml/aalto/in/InputBootstrapper;->readXmlEncoding()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/fasterxml/aalto/in/InputBootstrapper;->mFoundEncoding:Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/fasterxml/aalto/in/InputBootstrapper;->getWsOrChar(I)I

    move-result v0

    :cond_1
    const/16 v1, 0x73

    if-ne v0, v1, :cond_2

    invoke-direct {p0}, Lcom/fasterxml/aalto/in/InputBootstrapper;->readXmlStandalone()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/fasterxml/aalto/in/InputBootstrapper;->mStandalone:Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/fasterxml/aalto/in/InputBootstrapper;->getWsOrChar(I)I

    move-result v0

    :cond_2
    const-string v1, "; expected \"?>\" end marker"

    if-eq v0, v2, :cond_3

    invoke-virtual {p0, v0, v1}, Lcom/fasterxml/aalto/in/InputBootstrapper;->reportUnexpectedChar(ILjava/lang/String;)V

    :cond_3
    invoke-virtual {p0}, Lcom/fasterxml/aalto/in/InputBootstrapper;->getNext()I

    move-result v0

    const/16 v2, 0x3e

    if-eq v0, v2, :cond_4

    invoke-virtual {p0, v0, v1}, Lcom/fasterxml/aalto/in/InputBootstrapper;->reportUnexpectedChar(ILjava/lang/String;)V

    :cond_4
    return-void
.end method

.method public reportEof()V
    .locals 1

    const-string v0, "Unexpected end-of-input in xml declaration"

    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/in/InputBootstrapper;->reportXmlProblem(Ljava/lang/String;)V

    return-void
.end method

.method public reportNull()V
    .locals 1

    const-string v0, "Illegal null byte/char in input stream"

    invoke-virtual {p0, v0}, Lcom/fasterxml/aalto/in/InputBootstrapper;->reportXmlProblem(Ljava/lang/String;)V

    return-void
.end method

.method public final reportPseudoAttrProblem(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    if-nez p3, :cond_0

    const-string p3, ""

    goto :goto_0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "; expected \""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p3, "\" or \""

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p3, "\""

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    :goto_0
    if-eqz p2, :cond_1

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result p4

    if-nez p4, :cond_2

    :cond_1
    new-instance p4, Ljava/lang/StringBuilder;

    invoke-direct {p4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Missing XML pseudo-attribute \'"

    invoke-virtual {p4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "\' value"

    invoke-virtual {p4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p4

    invoke-virtual {p0, p4}, Lcom/fasterxml/aalto/in/InputBootstrapper;->reportXmlProblem(Ljava/lang/String;)V

    :cond_2
    new-instance p4, Ljava/lang/StringBuilder;

    invoke-direct {p4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Invalid XML pseudo-attribute \'"

    invoke-virtual {p4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "\' value "

    invoke-virtual {p4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/fasterxml/aalto/in/InputBootstrapper;->reportXmlProblem(Ljava/lang/String;)V

    return-void
.end method

.method public reportUnexpectedChar(ILjava/lang/String;)V
    .locals 4

    int-to-char v0, p1

    invoke-static {v0}, Ljava/lang/Character;->isISOControl(C)Z

    move-result v1

    const-string v2, ")"

    if-eqz v1, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unexpected character (CTRL-CHAR, code "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unexpected character \'"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v0, "\' (code "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    :goto_0
    invoke-virtual {p0, p1}, Lcom/fasterxml/aalto/in/InputBootstrapper;->reportXmlProblem(Ljava/lang/String;)V

    return-void
.end method

.method public reportXmlProblem(Ljava/lang/String;)V
    .locals 2

    new-instance v0, Lcom/fasterxml/aalto/WFCException;

    invoke-virtual {p0}, Lcom/fasterxml/aalto/in/InputBootstrapper;->getLocation()Ljavax/xml/stream/Location;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Lcom/fasterxml/aalto/WFCException;-><init>(Ljava/lang/String;Ljavax/xml/stream/Location;)V

    throw v0
.end method

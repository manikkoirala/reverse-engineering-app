.class public final Lcom/fasterxml/aalto/in/AttributeCollector;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final DEFAULT_BUFFER_LENGTH:I = 0x78

.field private static final DEFAULT_ENTRY_COUNT:I = 0xc

.field private static final INT_SPACE:I = 0x20


# instance fields
.field private _allAttrValues:Ljava/lang/String;

.field private _attrCount:I

.field protected _attrMap:[I

.field final _config:Lcom/fasterxml/aalto/in/ReaderConfig;

.field private _errorMsg:Ljava/lang/String;

.field protected _hashAreaSize:I

.field private _names:[Lcom/fasterxml/aalto/in/PName;

.field private _needToResetValues:Z

.field protected _spillAreaEnd:I

.field private _valueBuffer:[C

.field private _valueOffsets:[I


# direct methods
.method public constructor <init>(Lcom/fasterxml/aalto/in/ReaderConfig;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/fasterxml/aalto/in/AttributeCollector;->_names:[Lcom/fasterxml/aalto/in/PName;

    iput-object v0, p0, Lcom/fasterxml/aalto/in/AttributeCollector;->_valueBuffer:[C

    iput-object v0, p0, Lcom/fasterxml/aalto/in/AttributeCollector;->_attrMap:[I

    iput-object v0, p0, Lcom/fasterxml/aalto/in/AttributeCollector;->_valueOffsets:[I

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/fasterxml/aalto/in/AttributeCollector;->_needToResetValues:Z

    iput-object v0, p0, Lcom/fasterxml/aalto/in/AttributeCollector;->_errorMsg:Ljava/lang/String;

    iput-object v0, p0, Lcom/fasterxml/aalto/in/AttributeCollector;->_allAttrValues:Ljava/lang/String;

    iput-object p1, p0, Lcom/fasterxml/aalto/in/AttributeCollector;->_config:Lcom/fasterxml/aalto/in/ReaderConfig;

    const/4 p1, 0x0

    iput p1, p0, Lcom/fasterxml/aalto/in/AttributeCollector;->_attrCount:I

    return-void
.end method

.method private final checkExpand(Lorg/codehaus/stax2/typed/TypedArrayDecoder;)Z
    .locals 1

    instance-of v0, p1, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$BaseArrayDecoder;

    if-eqz v0, :cond_0

    check-cast p1, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$BaseArrayDecoder;

    invoke-virtual {p1}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$BaseArrayDecoder;->expand()V

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method private final decodeValues(Lorg/codehaus/stax2/typed/TypedArrayDecoder;[CIILcom/fasterxml/aalto/in/XmlScanner;)I
    .locals 5

    .line 2
    const/4 v0, 0x0

    move v1, v0

    move v0, p3

    :goto_0
    if-ge p3, p4, :cond_4

    :cond_0
    :try_start_0
    aget-char v2, p2, p3
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_2

    const/16 v3, 0x20

    if-gt v2, v3, :cond_1

    add-int/lit8 p3, p3, 0x1

    if-lt p3, p4, :cond_0

    goto :goto_3

    :cond_1
    add-int/lit8 v0, p3, 0x1

    :goto_1
    if-ge v0, p4, :cond_2

    :try_start_1
    aget-char v2, p2, v0
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0

    if-le v2, v3, :cond_2

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :catch_0
    move-exception p1

    move v4, v0

    move v0, p3

    move p3, v4

    goto :goto_2

    :cond_2
    add-int/lit8 v2, v0, 0x1

    add-int/lit8 v1, v1, 0x1

    :try_start_2
    invoke-virtual {p1, p2, p3, v0}, Lorg/codehaus/stax2/typed/TypedArrayDecoder;->decodeValue([CII)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-direct {p0, p1}, Lcom/fasterxml/aalto/in/AttributeCollector;->checkExpand(Lorg/codehaus/stax2/typed/TypedArrayDecoder;)Z

    move-result v0
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_1

    if-nez v0, :cond_3

    goto :goto_3

    :cond_3
    move v0, p3

    move p3, v2

    goto :goto_0

    :catch_1
    move-exception p1

    move v0, p3

    move p3, v2

    goto :goto_2

    :catch_2
    move-exception p1

    :goto_2
    invoke-virtual {p5}, Lcom/fasterxml/aalto/in/XmlScanner;->getCurrentLocation()Lorg/codehaus/stax2/XMLStreamLocation2;

    move-result-object p4

    new-instance p5, Ljava/lang/String;

    sub-int/2addr p3, v0

    invoke-direct {p5, p2, v0, p3}, Ljava/lang/String;-><init>([CII)V

    new-instance p2, Lorg/codehaus/stax2/typed/TypedXMLStreamException;

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object p3

    invoke-direct {p2, p5, p3, p4, p1}, Lorg/codehaus/stax2/typed/TypedXMLStreamException;-><init>(Ljava/lang/String;Ljava/lang/String;Ljavax/xml/stream/Location;Ljava/lang/IllegalArgumentException;)V

    throw p2

    :cond_4
    :goto_3
    return v1
.end method

.method private static final isSpace(C)Z
    .locals 1

    const/16 v0, 0x20

    if-gt p0, v0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method private noteDupAttr(II)V
    .locals 3

    sget-object v0, Lcom/fasterxml/aalto/impl/ErrorConsts;->ERR_WF_DUP_ATTRS:Ljava/lang/String;

    iget-object v1, p0, Lcom/fasterxml/aalto/in/AttributeCollector;->_names:[Lcom/fasterxml/aalto/in/PName;

    aget-object v1, v1, p1

    invoke-virtual {v1}, Lcom/fasterxml/aalto/in/PName;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    iget-object v2, p0, Lcom/fasterxml/aalto/in/AttributeCollector;->_names:[Lcom/fasterxml/aalto/in/PName;

    aget-object v2, v2, p2

    invoke-virtual {v2}, Lcom/fasterxml/aalto/in/PName;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    filled-new-array {v1, p1, v2, p2}, [Ljava/lang/Object;

    move-result-object p1

    invoke-static {v0, p1}, Ljava/text/MessageFormat;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lcom/fasterxml/aalto/in/AttributeCollector;->_errorMsg:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public continueValue()[C
    .locals 1

    iget-object v0, p0, Lcom/fasterxml/aalto/in/AttributeCollector;->_valueBuffer:[C

    return-object v0
.end method

.method public decodeBinaryValue(ILorg/codehaus/stax2/typed/Base64Variant;Lorg/codehaus/stax2/ri/typed/CharArrayBase64Decoder;Lcom/fasterxml/aalto/in/XmlScanner;)[B
    .locals 9

    if-ltz p1, :cond_1

    iget v0, p0, Lcom/fasterxml/aalto/in/AttributeCollector;->_attrCount:I

    if-ge p1, v0, :cond_1

    if-nez p1, :cond_0

    iget-object p1, p0, Lcom/fasterxml/aalto/in/AttributeCollector;->_valueOffsets:[I

    const/4 v0, 0x0

    aget p1, p1, v0

    move v7, p1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/fasterxml/aalto/in/AttributeCollector;->_valueOffsets:[I

    add-int/lit8 v1, p1, -0x1

    aget v1, v0, v1

    aget p1, v0, p1

    move v7, p1

    move v0, v1

    :goto_0
    sub-int p1, v7, v0

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/fasterxml/aalto/in/AttributeCollector;->_valueBuffer:[C

    const/4 v8, 0x0

    move-object v2, p3

    move-object v3, p2

    move v6, v0

    invoke-virtual/range {v2 .. v8}, Lorg/codehaus/stax2/ri/typed/CharArrayBase64Decoder;->init(Lorg/codehaus/stax2/typed/Base64Variant;Z[CIILjava/util/List;)V

    :try_start_0
    invoke-virtual {p3}, Lorg/codehaus/stax2/ri/typed/CharArrayBase64Decoder;->decodeCompletely()[B

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p2

    new-instance p3, Ljava/lang/String;

    iget-object v1, p0, Lcom/fasterxml/aalto/in/AttributeCollector;->_valueBuffer:[C

    invoke-direct {p3, v1, v0, p1}, Ljava/lang/String;-><init>([CII)V

    new-instance p1, Lorg/codehaus/stax2/typed/TypedXMLStreamException;

    invoke-virtual {p2}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p4}, Lcom/fasterxml/aalto/in/XmlScanner;->getCurrentLocation()Lorg/codehaus/stax2/XMLStreamLocation2;

    move-result-object p4

    invoke-direct {p1, p3, v0, p4, p2}, Lorg/codehaus/stax2/typed/TypedXMLStreamException;-><init>(Ljava/lang/String;Ljava/lang/String;Ljavax/xml/stream/Location;Ljava/lang/IllegalArgumentException;)V

    throw p1

    :cond_1
    new-instance p2, Ljava/lang/IllegalArgumentException;

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string p4, "Invalid index "

    invoke-virtual {p3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, "; current element has only "

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget p1, p0, Lcom/fasterxml/aalto/in/AttributeCollector;->_attrCount:I

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, " attributes"

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p2
.end method

.method public final decodeValue(ILorg/codehaus/stax2/typed/TypedValueDecoder;)V
    .locals 3

    if-ltz p1, :cond_4

    iget v0, p0, Lcom/fasterxml/aalto/in/AttributeCollector;->_attrCount:I

    if-ge p1, v0, :cond_4

    if-nez p1, :cond_0

    iget-object p1, p0, Lcom/fasterxml/aalto/in/AttributeCollector;->_valueOffsets:[I

    const/4 v0, 0x0

    aget p1, p1, v0

    move v1, v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/fasterxml/aalto/in/AttributeCollector;->_valueOffsets:[I

    add-int/lit8 v1, p1, -0x1

    aget v1, v0, v1

    aget p1, v0, p1

    :goto_0
    move v0, p1

    iget-object v2, p0, Lcom/fasterxml/aalto/in/AttributeCollector;->_valueBuffer:[C

    :goto_1
    if-lt v1, v0, :cond_1

    invoke-virtual {p2}, Lorg/codehaus/stax2/typed/TypedValueDecoder;->handleEmptyValue()V

    return-void

    :cond_1
    aget-char p1, v2, v1

    invoke-static {p1}, Lcom/fasterxml/aalto/in/AttributeCollector;->isSpace(C)Z

    move-result p1

    if-nez p1, :cond_3

    :goto_2
    add-int/lit8 v0, v0, -0x1

    if-le v0, v1, :cond_2

    aget-char p1, v2, v0

    invoke-static {p1}, Lcom/fasterxml/aalto/in/AttributeCollector;->isSpace(C)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_2

    :cond_2
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p2, v2, v1, v0}, Lorg/codehaus/stax2/typed/TypedValueDecoder;->decode([CII)V

    return-void

    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_4
    new-instance p2, Ljava/lang/IllegalArgumentException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Invalid index "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, "; current element has only "

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget p1, p0, Lcom/fasterxml/aalto/in/AttributeCollector;->_attrCount:I

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, " attributes"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p2
.end method

.method public final decodeValues(ILorg/codehaus/stax2/typed/TypedArrayDecoder;Lcom/fasterxml/aalto/in/XmlScanner;)I
    .locals 8

    .line 1
    if-ltz p1, :cond_1

    iget v0, p0, Lcom/fasterxml/aalto/in/AttributeCollector;->_attrCount:I

    if-ge p1, v0, :cond_1

    if-nez p1, :cond_0

    iget-object p1, p0, Lcom/fasterxml/aalto/in/AttributeCollector;->_valueOffsets:[I

    const/4 v0, 0x0

    aget p1, p1, v0

    move v6, p1

    move v5, v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/fasterxml/aalto/in/AttributeCollector;->_valueOffsets:[I

    add-int/lit8 v1, p1, -0x1

    aget v1, v0, v1

    aget p1, v0, p1

    move v6, p1

    move v5, v1

    :goto_0
    iget-object v4, p0, Lcom/fasterxml/aalto/in/AttributeCollector;->_valueBuffer:[C

    move-object v2, p0

    move-object v3, p2

    move-object v7, p3

    invoke-direct/range {v2 .. v7}, Lcom/fasterxml/aalto/in/AttributeCollector;->decodeValues(Lorg/codehaus/stax2/typed/TypedArrayDecoder;[CIILcom/fasterxml/aalto/in/XmlScanner;)I

    move-result p1

    return p1

    :cond_1
    new-instance p2, Ljava/lang/IllegalArgumentException;

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Invalid index "

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, "; current element has only "

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget p1, p0, Lcom/fasterxml/aalto/in/AttributeCollector;->_attrCount:I

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, " attributes"

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p2
.end method

.method public findIndex(Ljava/lang/String;Ljava/lang/String;)I
    .locals 6

    iget v0, p0, Lcom/fasterxml/aalto/in/AttributeCollector;->_hashAreaSize:I

    const/4 v1, 0x1

    const/4 v2, -0x1

    if-ge v0, v1, :cond_2

    iget v0, p0, Lcom/fasterxml/aalto/in/AttributeCollector;->_attrCount:I

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_1

    iget-object v3, p0, Lcom/fasterxml/aalto/in/AttributeCollector;->_names:[Lcom/fasterxml/aalto/in/PName;

    aget-object v3, v3, v1

    invoke-virtual {v3, p1, p2}, Lcom/fasterxml/aalto/in/PName;->boundEquals(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    return v1

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return v2

    :cond_2
    invoke-static {p1, p2}, Lcom/fasterxml/aalto/in/PName;->boundHashCode(Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    iget-object v3, p0, Lcom/fasterxml/aalto/in/AttributeCollector;->_attrMap:[I

    add-int/lit8 v4, v0, -0x1

    and-int/2addr v4, v1

    aget v3, v3, v4

    if-lez v3, :cond_6

    add-int/2addr v3, v2

    iget-object v4, p0, Lcom/fasterxml/aalto/in/AttributeCollector;->_names:[Lcom/fasterxml/aalto/in/PName;

    aget-object v4, v4, v3

    invoke-virtual {v4, p1, p2}, Lcom/fasterxml/aalto/in/PName;->boundEquals(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    return v3

    :cond_3
    iget v3, p0, Lcom/fasterxml/aalto/in/AttributeCollector;->_spillAreaEnd:I

    :goto_1
    if-ge v0, v3, :cond_6

    iget-object v4, p0, Lcom/fasterxml/aalto/in/AttributeCollector;->_attrMap:[I

    aget v5, v4, v0

    if-eq v5, v1, :cond_4

    goto :goto_2

    :cond_4
    add-int/lit8 v5, v0, 0x1

    aget v4, v4, v5

    iget-object v5, p0, Lcom/fasterxml/aalto/in/AttributeCollector;->_names:[Lcom/fasterxml/aalto/in/PName;

    aget-object v5, v5, v4

    invoke-virtual {v5, p1, p2}, Lcom/fasterxml/aalto/in/PName;->boundEquals(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_5

    return v4

    :cond_5
    :goto_2
    add-int/lit8 v0, v0, 0x2

    goto :goto_1

    :cond_6
    return v2
.end method

.method public final finishLastValue(I)I
    .locals 5

    iget-boolean v0, p0, Lcom/fasterxml/aalto/in/AttributeCollector;->_needToResetValues:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return v1

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/fasterxml/aalto/in/AttributeCollector;->_needToResetValues:Z

    iget v2, p0, Lcom/fasterxml/aalto/in/AttributeCollector;->_attrCount:I

    iget-object v3, p0, Lcom/fasterxml/aalto/in/AttributeCollector;->_valueOffsets:[I

    add-int/lit8 v4, v2, -0x1

    aput p1, v3, v4

    const/4 p1, 0x3

    if-ge v2, p1, :cond_2

    iput v1, p0, Lcom/fasterxml/aalto/in/AttributeCollector;->_hashAreaSize:I

    const/4 p1, 0x2

    if-ne v2, p1, :cond_1

    iget-object p1, p0, Lcom/fasterxml/aalto/in/AttributeCollector;->_names:[Lcom/fasterxml/aalto/in/PName;

    aget-object v3, p1, v1

    aget-object p1, p1, v0

    invoke-virtual {v3, p1}, Lcom/fasterxml/aalto/in/PName;->boundEquals(Lcom/fasterxml/aalto/in/PName;)Z

    move-result p1

    if-eqz p1, :cond_1

    invoke-direct {p0, v1, v0}, Lcom/fasterxml/aalto/in/AttributeCollector;->noteDupAttr(II)V

    const/4 p1, -0x1

    return p1

    :cond_1
    return v2

    :cond_2
    invoke-virtual {p0}, Lcom/fasterxml/aalto/in/AttributeCollector;->finishLastValue2()I

    move-result p1

    return p1
.end method

.method public final finishLastValue2()I
    .locals 14

    iget v0, p0, Lcom/fasterxml/aalto/in/AttributeCollector;->_attrCount:I

    iget-object v1, p0, Lcom/fasterxml/aalto/in/AttributeCollector;->_names:[Lcom/fasterxml/aalto/in/PName;

    iget-object v2, p0, Lcom/fasterxml/aalto/in/AttributeCollector;->_attrMap:[I

    shr-int/lit8 v3, v0, 0x2

    add-int/2addr v3, v0

    const/16 v4, 0x8

    move v5, v4

    :goto_0
    if-ge v5, v3, :cond_0

    add-int/2addr v5, v5

    goto :goto_0

    :cond_0
    iput v5, p0, Lcom/fasterxml/aalto/in/AttributeCollector;->_hashAreaSize:I

    shr-int/lit8 v3, v5, 0x4

    add-int/2addr v3, v5

    const/4 v6, 0x0

    if-eqz v2, :cond_2

    array-length v7, v2

    if-ge v7, v3, :cond_1

    goto :goto_2

    :cond_1
    const/4 v3, 0x7

    aput v6, v2, v3

    const/4 v3, 0x6

    aput v6, v2, v3

    const/4 v3, 0x5

    aput v6, v2, v3

    const/4 v3, 0x4

    aput v6, v2, v3

    const/4 v3, 0x3

    aput v6, v2, v3

    const/4 v3, 0x2

    aput v6, v2, v3

    const/4 v3, 0x1

    aput v6, v2, v3

    aput v6, v2, v6

    move v3, v4

    :goto_1
    if-ge v3, v5, :cond_3

    aput v6, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_2
    :goto_2
    new-array v2, v3, [I

    :cond_3
    add-int/lit8 v3, v5, -0x1

    move v7, v5

    :goto_3
    if-ge v6, v0, :cond_9

    aget-object v8, v1, v6

    invoke-virtual {v8}, Lcom/fasterxml/aalto/in/PName;->boundHashCode()I

    move-result v9

    and-int v10, v9, v3

    aget v11, v2, v10

    if-nez v11, :cond_4

    add-int/lit8 v8, v6, 0x1

    aput v8, v2, v10

    goto :goto_6

    :cond_4
    add-int/lit8 v11, v11, -0x1

    aget-object v10, v1, v11

    invoke-virtual {v10, v8}, Lcom/fasterxml/aalto/in/PName;->boundEquals(Lcom/fasterxml/aalto/in/PName;)Z

    move-result v10

    if-eqz v10, :cond_5

    iget-object v10, p0, Lcom/fasterxml/aalto/in/AttributeCollector;->_errorMsg:Ljava/lang/String;

    if-nez v10, :cond_5

    invoke-direct {p0, v11, v6}, Lcom/fasterxml/aalto/in/AttributeCollector;->noteDupAttr(II)V

    :cond_5
    add-int/lit8 v10, v7, 0x1

    array-length v11, v2

    if-lt v10, v11, :cond_6

    invoke-static {v2, v4}, Lcom/fasterxml/aalto/util/DataUtil;->growArrayBy([II)[I

    move-result-object v2

    :cond_6
    move v11, v5

    :goto_4
    if-ge v11, v7, :cond_8

    aget v12, v2, v11

    if-ne v12, v9, :cond_7

    add-int/lit8 v12, v11, 0x1

    aget v12, v2, v12

    aget-object v13, v1, v12

    invoke-virtual {v13, v8}, Lcom/fasterxml/aalto/in/PName;->boundEquals(Lcom/fasterxml/aalto/in/PName;)Z

    move-result v13

    if-eqz v13, :cond_7

    iget-object v8, p0, Lcom/fasterxml/aalto/in/AttributeCollector;->_errorMsg:Ljava/lang/String;

    if-nez v8, :cond_8

    invoke-direct {p0, v12, v6}, Lcom/fasterxml/aalto/in/AttributeCollector;->noteDupAttr(II)V

    goto :goto_5

    :cond_7
    add-int/lit8 v11, v11, 0x2

    goto :goto_4

    :cond_8
    :goto_5
    aput v9, v2, v7

    add-int/lit8 v7, v10, 0x1

    aput v6, v2, v10

    :goto_6
    add-int/lit8 v6, v6, 0x1

    goto :goto_3

    :cond_9
    iput v7, p0, Lcom/fasterxml/aalto/in/AttributeCollector;->_spillAreaEnd:I

    iput-object v2, p0, Lcom/fasterxml/aalto/in/AttributeCollector;->_attrMap:[I

    iget-object v1, p0, Lcom/fasterxml/aalto/in/AttributeCollector;->_errorMsg:Ljava/lang/String;

    if-nez v1, :cond_a

    goto :goto_7

    :cond_a
    const/4 v0, -0x1

    :goto_7
    return v0
.end method

.method public final getCount()I
    .locals 1

    iget v0, p0, Lcom/fasterxml/aalto/in/AttributeCollector;->_attrCount:I

    return v0
.end method

.method public getErrorMsg()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/fasterxml/aalto/in/AttributeCollector;->_errorMsg:Ljava/lang/String;

    return-object v0
.end method

.method public final getName(I)Lcom/fasterxml/aalto/in/PName;
    .locals 1

    iget-object v0, p0, Lcom/fasterxml/aalto/in/AttributeCollector;->_names:[Lcom/fasterxml/aalto/in/PName;

    aget-object p1, v0, p1

    return-object p1
.end method

.method public final getQName(I)Ljavax/xml/namespace/QName;
    .locals 1

    iget-object v0, p0, Lcom/fasterxml/aalto/in/AttributeCollector;->_names:[Lcom/fasterxml/aalto/in/PName;

    aget-object p1, v0, p1

    invoke-virtual {p1}, Lcom/fasterxml/aalto/in/PName;->constructQName()Ljavax/xml/namespace/QName;

    move-result-object p1

    return-object p1
.end method

.method public getValue(I)Ljava/lang/String;
    .locals 6

    .line 1
    iget v0, p0, Lcom/fasterxml/aalto/in/AttributeCollector;->_attrCount:I

    iget-object v1, p0, Lcom/fasterxml/aalto/in/AttributeCollector;->_allAttrValues:Ljava/lang/String;

    const-string v2, ""

    const/4 v3, 0x0

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/fasterxml/aalto/in/AttributeCollector;->_valueOffsets:[I

    add-int/lit8 v4, v0, -0x1

    aget v1, v1, v4

    if-nez v1, :cond_0

    move-object v4, v2

    goto :goto_0

    :cond_0
    new-instance v4, Ljava/lang/String;

    iget-object v5, p0, Lcom/fasterxml/aalto/in/AttributeCollector;->_valueBuffer:[C

    invoke-direct {v4, v5, v3, v1}, Ljava/lang/String;-><init>([CII)V

    :goto_0
    iput-object v4, p0, Lcom/fasterxml/aalto/in/AttributeCollector;->_allAttrValues:Ljava/lang/String;

    :cond_1
    if-nez p1, :cond_4

    const/4 p1, 0x1

    if-ne v0, p1, :cond_2

    iget-object p1, p0, Lcom/fasterxml/aalto/in/AttributeCollector;->_allAttrValues:Ljava/lang/String;

    return-object p1

    :cond_2
    iget-object p1, p0, Lcom/fasterxml/aalto/in/AttributeCollector;->_valueOffsets:[I

    aget p1, p1, v3

    if-nez p1, :cond_3

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/fasterxml/aalto/in/AttributeCollector;->_allAttrValues:Ljava/lang/String;

    invoke-virtual {v0, v3, p1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    :goto_1
    return-object v2

    :cond_4
    iget-object v0, p0, Lcom/fasterxml/aalto/in/AttributeCollector;->_valueOffsets:[I

    add-int/lit8 v1, p1, -0x1

    aget v1, v0, v1

    aget p1, v0, p1

    if-ne v1, p1, :cond_5

    goto :goto_2

    :cond_5
    iget-object v0, p0, Lcom/fasterxml/aalto/in/AttributeCollector;->_allAttrValues:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    :goto_2
    return-object v2
.end method

.method public getValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 2
    invoke-virtual {p0, p1, p2}, Lcom/fasterxml/aalto/in/AttributeCollector;->findIndex(Ljava/lang/String;Ljava/lang/String;)I

    move-result p1

    if-ltz p1, :cond_0

    invoke-virtual {p0, p1}, Lcom/fasterxml/aalto/in/AttributeCollector;->getValue(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return-object p1
.end method

.method public startNewValue(Lcom/fasterxml/aalto/in/PName;I)[C
    .locals 7

    iget-boolean v0, p0, Lcom/fasterxml/aalto/in/AttributeCollector;->_needToResetValues:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    iput-boolean v1, p0, Lcom/fasterxml/aalto/in/AttributeCollector;->_needToResetValues:Z

    iput v1, p0, Lcom/fasterxml/aalto/in/AttributeCollector;->_attrCount:I

    const/4 p2, 0x0

    iput-object p2, p0, Lcom/fasterxml/aalto/in/AttributeCollector;->_allAttrValues:Ljava/lang/String;

    iget-object p2, p0, Lcom/fasterxml/aalto/in/AttributeCollector;->_valueBuffer:[C

    if-nez p2, :cond_3

    const/16 p2, 0xc

    new-array v0, p2, [Lcom/fasterxml/aalto/in/PName;

    iput-object v0, p0, Lcom/fasterxml/aalto/in/AttributeCollector;->_names:[Lcom/fasterxml/aalto/in/PName;

    const/16 v0, 0x78

    new-array v0, v0, [C

    iput-object v0, p0, Lcom/fasterxml/aalto/in/AttributeCollector;->_valueBuffer:[C

    new-array p2, p2, [I

    iput-object p2, p0, Lcom/fasterxml/aalto/in/AttributeCollector;->_valueOffsets:[I

    goto :goto_1

    :cond_0
    iget v0, p0, Lcom/fasterxml/aalto/in/AttributeCollector;->_attrCount:I

    iget-object v2, p0, Lcom/fasterxml/aalto/in/AttributeCollector;->_valueOffsets:[I

    array-length v3, v2

    if-lt v0, v3, :cond_1

    iget-object v3, p0, Lcom/fasterxml/aalto/in/AttributeCollector;->_names:[Lcom/fasterxml/aalto/in/PName;

    array-length v4, v2

    add-int v5, v4, v4

    new-array v6, v5, [I

    iput-object v6, p0, Lcom/fasterxml/aalto/in/AttributeCollector;->_valueOffsets:[I

    new-array v5, v5, [Lcom/fasterxml/aalto/in/PName;

    iput-object v5, p0, Lcom/fasterxml/aalto/in/AttributeCollector;->_names:[Lcom/fasterxml/aalto/in/PName;

    :goto_0
    if-ge v1, v4, :cond_1

    iget-object v5, p0, Lcom/fasterxml/aalto/in/AttributeCollector;->_valueOffsets:[I

    aget v6, v2, v1

    aput v6, v5, v1

    iget-object v5, p0, Lcom/fasterxml/aalto/in/AttributeCollector;->_names:[Lcom/fasterxml/aalto/in/PName;

    aget-object v6, v3, v1

    aput-object v6, v5, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    if-lez v0, :cond_2

    iget-object v1, p0, Lcom/fasterxml/aalto/in/AttributeCollector;->_valueOffsets:[I

    add-int/lit8 v2, v0, -0x1

    aput p2, v1, v2

    :cond_2
    move v1, v0

    :cond_3
    :goto_1
    iget-object p2, p0, Lcom/fasterxml/aalto/in/AttributeCollector;->_names:[Lcom/fasterxml/aalto/in/PName;

    aput-object p1, p2, v1

    iget p1, p0, Lcom/fasterxml/aalto/in/AttributeCollector;->_attrCount:I

    add-int/lit8 p1, p1, 0x1

    iput p1, p0, Lcom/fasterxml/aalto/in/AttributeCollector;->_attrCount:I

    iget-object p1, p0, Lcom/fasterxml/aalto/in/AttributeCollector;->_valueBuffer:[C

    return-object p1
.end method

.method public valueBufferFull()[C
    .locals 2

    iget-object v0, p0, Lcom/fasterxml/aalto/in/AttributeCollector;->_valueBuffer:[C

    array-length v1, v0

    invoke-static {v0, v1}, Lcom/fasterxml/aalto/util/DataUtil;->growArrayBy([CI)[C

    move-result-object v0

    iput-object v0, p0, Lcom/fasterxml/aalto/in/AttributeCollector;->_valueBuffer:[C

    return-object v0
.end method

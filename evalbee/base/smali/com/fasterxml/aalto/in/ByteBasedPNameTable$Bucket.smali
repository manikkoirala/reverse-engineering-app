.class final Lcom/fasterxml/aalto/in/ByteBasedPNameTable$Bucket;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/fasterxml/aalto/in/ByteBasedPNameTable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Bucket"
.end annotation


# instance fields
.field final mName:Lcom/fasterxml/aalto/in/ByteBasedPName;

.field final mNext:Lcom/fasterxml/aalto/in/ByteBasedPNameTable$Bucket;


# direct methods
.method public constructor <init>(Lcom/fasterxml/aalto/in/ByteBasedPName;Lcom/fasterxml/aalto/in/ByteBasedPNameTable$Bucket;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/fasterxml/aalto/in/ByteBasedPNameTable$Bucket;->mName:Lcom/fasterxml/aalto/in/ByteBasedPName;

    iput-object p2, p0, Lcom/fasterxml/aalto/in/ByteBasedPNameTable$Bucket;->mNext:Lcom/fasterxml/aalto/in/ByteBasedPNameTable$Bucket;

    return-void
.end method


# virtual methods
.method public find(III)Lcom/fasterxml/aalto/in/ByteBasedPName;
    .locals 3

    .line 1
    iget-object v0, p0, Lcom/fasterxml/aalto/in/ByteBasedPNameTable$Bucket;->mName:Lcom/fasterxml/aalto/in/ByteBasedPName;

    invoke-virtual {v0, p1, p2, p3}, Lcom/fasterxml/aalto/in/ByteBasedPName;->hashEquals(III)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object p1, p0, Lcom/fasterxml/aalto/in/ByteBasedPNameTable$Bucket;->mName:Lcom/fasterxml/aalto/in/ByteBasedPName;

    return-object p1

    :cond_0
    iget-object v0, p0, Lcom/fasterxml/aalto/in/ByteBasedPNameTable$Bucket;->mNext:Lcom/fasterxml/aalto/in/ByteBasedPNameTable$Bucket;

    :goto_0
    if-eqz v0, :cond_2

    iget-object v1, v0, Lcom/fasterxml/aalto/in/ByteBasedPNameTable$Bucket;->mName:Lcom/fasterxml/aalto/in/ByteBasedPName;

    invoke-virtual {v1, p1, p2, p3}, Lcom/fasterxml/aalto/in/ByteBasedPName;->hashEquals(III)Z

    move-result v2

    if-eqz v2, :cond_1

    return-object v1

    :cond_1
    iget-object v0, v0, Lcom/fasterxml/aalto/in/ByteBasedPNameTable$Bucket;->mNext:Lcom/fasterxml/aalto/in/ByteBasedPNameTable$Bucket;

    goto :goto_0

    :cond_2
    const/4 p1, 0x0

    return-object p1
.end method

.method public find(I[II)Lcom/fasterxml/aalto/in/ByteBasedPName;
    .locals 3

    .line 2
    iget-object v0, p0, Lcom/fasterxml/aalto/in/ByteBasedPNameTable$Bucket;->mName:Lcom/fasterxml/aalto/in/ByteBasedPName;

    invoke-virtual {v0, p1, p2, p3}, Lcom/fasterxml/aalto/in/ByteBasedPName;->hashEquals(I[II)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object p1, p0, Lcom/fasterxml/aalto/in/ByteBasedPNameTable$Bucket;->mName:Lcom/fasterxml/aalto/in/ByteBasedPName;

    return-object p1

    :cond_0
    iget-object v0, p0, Lcom/fasterxml/aalto/in/ByteBasedPNameTable$Bucket;->mNext:Lcom/fasterxml/aalto/in/ByteBasedPNameTable$Bucket;

    :goto_0
    if-eqz v0, :cond_2

    iget-object v1, v0, Lcom/fasterxml/aalto/in/ByteBasedPNameTable$Bucket;->mName:Lcom/fasterxml/aalto/in/ByteBasedPName;

    invoke-virtual {v1, p1, p2, p3}, Lcom/fasterxml/aalto/in/ByteBasedPName;->hashEquals(I[II)Z

    move-result v2

    if-eqz v2, :cond_1

    return-object v1

    :cond_1
    iget-object v0, v0, Lcom/fasterxml/aalto/in/ByteBasedPNameTable$Bucket;->mNext:Lcom/fasterxml/aalto/in/ByteBasedPNameTable$Bucket;

    goto :goto_0

    :cond_2
    const/4 p1, 0x0

    return-object p1
.end method

.method public length()I
    .locals 2

    iget-object v0, p0, Lcom/fasterxml/aalto/in/ByteBasedPNameTable$Bucket;->mNext:Lcom/fasterxml/aalto/in/ByteBasedPNameTable$Bucket;

    const/4 v1, 0x1

    :goto_0
    if-eqz v0, :cond_0

    add-int/lit8 v1, v1, 0x1

    iget-object v0, v0, Lcom/fasterxml/aalto/in/ByteBasedPNameTable$Bucket;->mNext:Lcom/fasterxml/aalto/in/ByteBasedPNameTable$Bucket;

    goto :goto_0

    :cond_0
    return v1
.end method

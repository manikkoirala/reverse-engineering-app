.class public abstract Lcom/fasterxml/aalto/in/ByteBasedScanner;
.super Lcom/fasterxml/aalto/in/XmlScanner;
.source "SourceFile"


# static fields
.field protected static final BYTE_A:B = 0x41t

.field protected static final BYTE_AMP:B = 0x26t

.field protected static final BYTE_APOS:B = 0x27t

.field protected static final BYTE_C:B = 0x43t

.field protected static final BYTE_CR:B = 0xdt

.field protected static final BYTE_D:B = 0x44t

.field protected static final BYTE_EQ:B = 0x3dt

.field protected static final BYTE_EXCL:B = 0x21t

.field protected static final BYTE_GT:B = 0x3et

.field protected static final BYTE_HASH:B = 0x23t

.field protected static final BYTE_HYPHEN:B = 0x2dt

.field protected static final BYTE_LBRACKET:B = 0x5bt

.field protected static final BYTE_LF:B = 0xat

.field protected static final BYTE_LT:B = 0x3ct

.field protected static final BYTE_NULL:B = 0x0t

.field protected static final BYTE_P:B = 0x50t

.field protected static final BYTE_QMARK:B = 0x3ft

.field protected static final BYTE_QUOT:B = 0x22t

.field protected static final BYTE_RBRACKET:B = 0x5dt

.field protected static final BYTE_S:B = 0x53t

.field protected static final BYTE_SEMICOLON:B = 0x3bt

.field protected static final BYTE_SLASH:B = 0x2ft

.field protected static final BYTE_SPACE:B = 0x20t

.field protected static final BYTE_T:B = 0x54t

.field protected static final BYTE_TAB:B = 0x9t

.field protected static final BYTE_a:B = 0x61t

.field protected static final BYTE_g:B = 0x67t

.field protected static final BYTE_l:B = 0x6ct

.field protected static final BYTE_m:B = 0x6dt

.field protected static final BYTE_o:B = 0x6ft

.field protected static final BYTE_p:B = 0x70t

.field protected static final BYTE_q:B = 0x71t

.field protected static final BYTE_s:B = 0x73t

.field protected static final BYTE_t:B = 0x74t

.field protected static final BYTE_u:B = 0x75t

.field protected static final BYTE_x:B = 0x78t


# instance fields
.field protected _inputEnd:I

.field protected _inputPtr:I

.field protected _tmpChar:I


# direct methods
.method public constructor <init>(Lcom/fasterxml/aalto/in/ReaderConfig;)V
    .locals 2

    invoke-direct {p0, p1}, Lcom/fasterxml/aalto/in/XmlScanner;-><init>(Lcom/fasterxml/aalto/in/ReaderConfig;)V

    const/4 p1, 0x0

    iput p1, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_tmpChar:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_pastBytesOrChars:J

    iput p1, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_rowStartOffset:I

    return-void
.end method


# virtual methods
.method public abstract _closeSource()V
.end method

.method public final addUTFPName(Lcom/fasterxml/aalto/in/ByteBasedPNameTable;Lcom/fasterxml/aalto/util/XmlCharTypes;I[III)Lcom/fasterxml/aalto/in/PName;
    .locals 20

    move-object/from16 v0, p0

    move/from16 v1, p6

    shl-int/lit8 v2, p5, 0x2

    const/4 v3, 0x4

    sub-int/2addr v2, v3

    add-int/2addr v2, v1

    const/4 v4, 0x0

    const/4 v5, 0x3

    if-ge v1, v3, :cond_0

    add-int/lit8 v6, p5, -0x1

    aget v7, p4, v6

    rsub-int/lit8 v8, v1, 0x4

    shl-int/2addr v8, v5

    shl-int v8, v7, v8

    aput v8, p4, v6

    goto :goto_0

    :cond_0
    move v7, v4

    :goto_0
    aget v6, p4, v4

    ushr-int/lit8 v6, v6, 0x18

    iget-object v8, v0, Lcom/fasterxml/aalto/in/XmlScanner;->_nameBuffer:[C

    move-object/from16 v9, p2

    iget-object v9, v9, Lcom/fasterxml/aalto/util/XmlCharTypes;->NAME_CHARS:[I

    aget v10, v9, v6

    const/16 v14, 0xf0

    const/16 v15, 0xe0

    const/16 v12, 0xc0

    const/4 v13, 0x1

    const/4 v11, 0x2

    if-eqz v10, :cond_b

    if-eq v10, v13, :cond_b

    if-eq v10, v11, :cond_b

    if-eq v10, v5, :cond_a

    if-eq v10, v3, :cond_b

    and-int/lit16 v10, v6, 0xe0

    if-ne v10, v12, :cond_1

    and-int/lit8 v6, v6, 0x1f

    move v10, v13

    goto :goto_1

    :cond_1
    and-int/lit16 v10, v6, 0xf0

    if-ne v10, v15, :cond_2

    and-int/lit8 v6, v6, 0xf

    move v10, v11

    goto :goto_1

    :cond_2
    and-int/lit16 v10, v6, 0xf8

    if-ne v10, v14, :cond_3

    and-int/lit8 v6, v6, 0x7

    move v10, v5

    goto :goto_1

    :cond_3
    invoke-virtual {v0, v6}, Lcom/fasterxml/aalto/in/ByteBasedScanner;->reportInvalidInitial(I)V

    move v6, v13

    move v10, v6

    :goto_1
    add-int v14, v13, v10

    if-le v14, v2, :cond_4

    invoke-virtual {v0, v8, v4}, Lcom/fasterxml/aalto/in/XmlScanner;->reportEofInName([CI)V

    :cond_4
    aget v15, p4, v4

    shr-int/lit8 v12, v15, 0x10

    and-int/lit16 v12, v12, 0xff

    and-int/lit16 v3, v12, 0xc0

    const/16 v5, 0x80

    if-eq v3, v5, :cond_5

    invoke-virtual {v0, v12}, Lcom/fasterxml/aalto/in/ByteBasedScanner;->reportInvalidOther(I)V

    :cond_5
    shl-int/lit8 v3, v6, 0x6

    and-int/lit8 v5, v12, 0x3f

    or-int/2addr v3, v5

    if-le v10, v13, :cond_8

    shr-int/lit8 v5, v15, 0x8

    and-int/lit16 v5, v5, 0xff

    and-int/lit16 v6, v5, 0xc0

    const/16 v12, 0x80

    if-eq v6, v12, :cond_6

    invoke-virtual {v0, v5}, Lcom/fasterxml/aalto/in/ByteBasedScanner;->reportInvalidOther(I)V

    :cond_6
    shl-int/lit8 v3, v3, 0x6

    and-int/lit8 v5, v5, 0x3f

    or-int/2addr v3, v5

    if-le v10, v11, :cond_8

    and-int/lit16 v5, v15, 0xff

    and-int/lit16 v6, v5, 0xc0

    const/16 v12, 0x80

    if-eq v6, v12, :cond_7

    and-int/lit16 v6, v5, 0xff

    invoke-virtual {v0, v6}, Lcom/fasterxml/aalto/in/ByteBasedScanner;->reportInvalidOther(I)V

    :cond_7
    shl-int/lit8 v3, v3, 0x6

    and-int/lit8 v5, v5, 0x3f

    or-int/2addr v3, v5

    :cond_8
    move v6, v3

    invoke-static {v6}, Lcom/fasterxml/aalto/util/XmlChars;->is10NameStartChar(I)Z

    move-result v3

    if-le v10, v11, :cond_9

    const/high16 v5, 0x10000

    sub-int/2addr v6, v5

    shr-int/lit8 v5, v6, 0xa

    const v10, 0xd800

    add-int/2addr v5, v10

    int-to-char v5, v5

    aput-char v5, v8, v4

    and-int/lit16 v5, v6, 0x3ff

    const v6, 0xdc00

    or-int/2addr v5, v6

    move v6, v5

    move v5, v13

    goto :goto_2

    :cond_9
    move v5, v4

    goto :goto_2

    :cond_a
    move v5, v4

    move v3, v13

    move v14, v3

    goto :goto_2

    :cond_b
    move v3, v4

    move v5, v3

    move v14, v13

    :goto_2
    if-nez v3, :cond_c

    invoke-virtual {v0, v6, v4}, Lcom/fasterxml/aalto/in/XmlScanner;->reportInvalidNameChar(II)V

    :cond_c
    add-int/lit8 v3, v5, 0x1

    int-to-char v6, v6

    aput-char v6, v8, v5

    const/4 v5, -0x1

    :goto_3
    if-ge v14, v2, :cond_1e

    shr-int/lit8 v6, v14, 0x2

    aget v6, p4, v6

    and-int/lit8 v10, v14, 0x3

    const/4 v12, 0x3

    rsub-int/lit8 v10, v10, 0x3

    shl-int/2addr v10, v12

    shr-int/2addr v6, v10

    and-int/lit16 v6, v6, 0xff

    add-int/lit8 v14, v14, 0x1

    aget v10, v9, v6

    if-eqz v10, :cond_1b

    if-eq v10, v13, :cond_19

    if-eq v10, v11, :cond_18

    if-eq v10, v12, :cond_18

    const/4 v12, 0x4

    if-eq v10, v12, :cond_17

    and-int/lit16 v10, v6, 0xe0

    const/16 v12, 0xc0

    if-ne v10, v12, :cond_d

    and-int/lit8 v6, v6, 0x1f

    move v10, v13

    const/16 v15, 0xe0

    goto :goto_4

    :cond_d
    and-int/lit16 v10, v6, 0xf0

    const/16 v15, 0xe0

    if-ne v10, v15, :cond_e

    and-int/lit8 v6, v6, 0xf

    move v10, v11

    goto :goto_4

    :cond_e
    and-int/lit16 v10, v6, 0xf8

    const/16 v12, 0xf0

    if-ne v10, v12, :cond_f

    and-int/lit8 v6, v6, 0x7

    const/4 v10, 0x3

    goto :goto_4

    :cond_f
    invoke-virtual {v0, v6}, Lcom/fasterxml/aalto/in/ByteBasedScanner;->reportInvalidInitial(I)V

    move v6, v13

    move v10, v6

    :goto_4
    add-int v12, v14, v10

    if-le v12, v2, :cond_10

    invoke-virtual {v0, v8, v3}, Lcom/fasterxml/aalto/in/XmlScanner;->reportEofInName([CI)V

    :cond_10
    shr-int/lit8 v12, v14, 0x2

    aget v12, p4, v12

    and-int/lit8 v18, v14, 0x3

    const/16 v19, 0x3

    rsub-int/lit8 v18, v18, 0x3

    shl-int/lit8 v18, v18, 0x3

    shr-int v12, v12, v18

    add-int/lit8 v14, v14, 0x1

    and-int/lit16 v15, v12, 0xc0

    const/16 v4, 0x80

    if-eq v15, v4, :cond_11

    invoke-virtual {v0, v12}, Lcom/fasterxml/aalto/in/ByteBasedScanner;->reportInvalidOther(I)V

    :cond_11
    shl-int/lit8 v4, v6, 0x6

    and-int/lit8 v6, v12, 0x3f

    or-int/2addr v4, v6

    if-le v10, v13, :cond_14

    shr-int/lit8 v6, v14, 0x2

    aget v6, p4, v6

    and-int/lit8 v12, v14, 0x3

    const/4 v15, 0x3

    rsub-int/lit8 v12, v12, 0x3

    shl-int/2addr v12, v15

    shr-int/2addr v6, v12

    add-int/lit8 v14, v14, 0x1

    and-int/lit16 v12, v6, 0xc0

    const/16 v15, 0x80

    if-eq v12, v15, :cond_12

    invoke-virtual {v0, v6}, Lcom/fasterxml/aalto/in/ByteBasedScanner;->reportInvalidOther(I)V

    :cond_12
    shl-int/lit8 v4, v4, 0x6

    and-int/lit8 v6, v6, 0x3f

    or-int/2addr v4, v6

    if-le v10, v11, :cond_14

    shr-int/lit8 v6, v14, 0x2

    aget v6, p4, v6

    and-int/lit8 v12, v14, 0x3

    const/4 v15, 0x3

    rsub-int/lit8 v12, v12, 0x3

    shl-int/2addr v12, v15

    shr-int/2addr v6, v12

    add-int/lit8 v14, v14, 0x1

    and-int/lit16 v12, v6, 0xc0

    const/16 v13, 0x80

    if-eq v12, v13, :cond_13

    and-int/lit16 v12, v6, 0xff

    invoke-virtual {v0, v12}, Lcom/fasterxml/aalto/in/ByteBasedScanner;->reportInvalidOther(I)V

    :cond_13
    shl-int/lit8 v4, v4, 0x6

    and-int/lit8 v6, v6, 0x3f

    or-int/2addr v4, v6

    goto :goto_5

    :cond_14
    const/16 v13, 0x80

    const/4 v15, 0x3

    :goto_5
    move v6, v4

    invoke-static {v6}, Lcom/fasterxml/aalto/util/XmlChars;->is10NameChar(I)Z

    move-result v4

    if-le v10, v11, :cond_16

    const/high16 v10, 0x10000

    sub-int/2addr v6, v10

    array-length v12, v8

    if-lt v3, v12, :cond_15

    array-length v12, v8

    invoke-static {v8, v12}, Lcom/fasterxml/aalto/util/DataUtil;->growArrayBy([CI)[C

    move-result-object v8

    iput-object v8, v0, Lcom/fasterxml/aalto/in/XmlScanner;->_nameBuffer:[C

    :cond_15
    add-int/lit8 v12, v3, 0x1

    shr-int/lit8 v17, v6, 0xa

    const v16, 0xd800

    add-int v10, v17, v16

    int-to-char v10, v10

    aput-char v10, v8, v3

    and-int/lit16 v3, v6, 0x3ff

    const v10, 0xdc00

    or-int v6, v3, v10

    move v3, v12

    goto :goto_9

    :cond_16
    const v10, 0xdc00

    const v16, 0xd800

    goto :goto_9

    :cond_17
    const v10, 0xdc00

    const/16 v13, 0x80

    const/4 v15, 0x3

    :goto_6
    const v16, 0xd800

    goto :goto_8

    :cond_18
    move v15, v12

    const v10, 0xdc00

    const/16 v13, 0x80

    const v16, 0xd800

    :goto_7
    const/4 v4, 0x1

    goto :goto_9

    :cond_19
    move v15, v12

    const v10, 0xdc00

    const/16 v13, 0x80

    const v16, 0xd800

    if-ltz v5, :cond_1a

    invoke-virtual/range {p0 .. p0}, Lcom/fasterxml/aalto/in/XmlScanner;->reportMultipleColonsInName()V

    :cond_1a
    move v5, v3

    goto :goto_7

    :cond_1b
    move v15, v12

    const v10, 0xdc00

    const/16 v13, 0x80

    goto :goto_6

    :goto_8
    const/4 v4, 0x0

    :goto_9
    if-nez v4, :cond_1c

    invoke-virtual {v0, v6, v3}, Lcom/fasterxml/aalto/in/XmlScanner;->reportInvalidNameChar(II)V

    :cond_1c
    array-length v4, v8

    if-lt v3, v4, :cond_1d

    array-length v4, v8

    invoke-static {v8, v4}, Lcom/fasterxml/aalto/util/DataUtil;->growArrayBy([CI)[C

    move-result-object v4

    iput-object v4, v0, Lcom/fasterxml/aalto/in/XmlScanner;->_nameBuffer:[C

    move-object v8, v4

    :cond_1d
    add-int/lit8 v4, v3, 0x1

    int-to-char v6, v6

    aput-char v6, v8, v3

    move v3, v4

    const/4 v4, 0x0

    const/4 v13, 0x1

    goto/16 :goto_3

    :cond_1e
    new-instance v4, Ljava/lang/String;

    const/4 v2, 0x0

    invoke-direct {v4, v8, v2, v3}, Ljava/lang/String;-><init>([CII)V

    const/4 v2, 0x4

    if-ge v1, v2, :cond_1f

    add-int/lit8 v1, p5, -0x1

    aput v7, p4, v1

    :cond_1f
    move-object/from16 v1, p1

    move/from16 v2, p3

    move-object v3, v4

    move v4, v5

    move-object/from16 v5, p4

    move/from16 v6, p5

    invoke-virtual/range {v1 .. v6}, Lcom/fasterxml/aalto/in/ByteBasedPNameTable;->addSymbol(ILjava/lang/String;I[II)Lcom/fasterxml/aalto/in/ByteBasedPName;

    move-result-object v1

    return-object v1
.end method

.method public abstract decodeCharForError(B)I
.end method

.method public getCurrentColumnNr()I
    .locals 2

    iget v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iget v1, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_rowStartOffset:I

    sub-int/2addr v0, v1

    return v0
.end method

.method public getCurrentLocation()Lorg/codehaus/stax2/XMLStreamLocation2;
    .locals 7

    iget-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_config:Lcom/fasterxml/aalto/in/ReaderConfig;

    invoke-virtual {v0}, Lcom/fasterxml/aalto/in/ReaderConfig;->getPublicId()Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_config:Lcom/fasterxml/aalto/in/ReaderConfig;

    invoke-virtual {v0}, Lcom/fasterxml/aalto/in/ReaderConfig;->getSystemId()Ljava/lang/String;

    move-result-object v2

    iget-wide v3, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_pastBytesOrChars:J

    iget v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    int-to-long v5, v0

    add-long/2addr v3, v5

    iget v5, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_currRow:I

    iget v6, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_rowStartOffset:I

    sub-int v6, v0, v6

    invoke-static/range {v1 .. v6}, Lcom/fasterxml/aalto/impl/LocationImpl;->fromZeroBased(Ljava/lang/String;Ljava/lang/String;JII)Lcom/fasterxml/aalto/impl/LocationImpl;

    move-result-object v0

    return-object v0
.end method

.method public getEndingByteOffset()J
    .locals 4

    iget-boolean v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_tokenIncomplete:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/aalto/in/XmlScanner;->finishToken()V

    :cond_0
    iget-wide v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_pastBytesOrChars:J

    iget v2, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    int-to-long v2, v2

    add-long/2addr v0, v2

    return-wide v0
.end method

.method public getEndingCharOffset()J
    .locals 2

    const-wide/16 v0, -0x1

    return-wide v0
.end method

.method public getStartingByteOffset()J
    .locals 2

    iget-wide v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_startRawOffset:J

    return-wide v0
.end method

.method public getStartingCharOffset()J
    .locals 2

    const-wide/16 v0, -0x1

    return-wide v0
.end method

.method public final markLF()V
    .locals 1

    .line 1
    iget v0, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    iput v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_rowStartOffset:I

    iget v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_currRow:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_currRow:I

    return-void
.end method

.method public final markLF(I)V
    .locals 0

    .line 2
    iput p1, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_rowStartOffset:I

    iget p1, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_currRow:I

    add-int/lit8 p1, p1, 0x1

    iput p1, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_currRow:I

    return-void
.end method

.method public reportInvalidInitial(I)V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Invalid UTF-8 start byte 0x"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/fasterxml/aalto/in/XmlScanner;->reportInputProblem(Ljava/lang/String;)V

    return-void
.end method

.method public reportInvalidOther(I)V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Invalid UTF-8 middle byte 0x"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lcom/fasterxml/aalto/in/XmlScanner;->reportInputProblem(Ljava/lang/String;)V

    return-void
.end method

.method public final setStartLocation()V
    .locals 5

    iget-wide v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_pastBytesOrChars:J

    iget v2, p0, Lcom/fasterxml/aalto/in/ByteBasedScanner;->_inputPtr:I

    int-to-long v3, v2

    add-long/2addr v0, v3

    iput-wide v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_startRawOffset:J

    iget v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_currRow:I

    int-to-long v0, v0

    iput-wide v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_startRow:J

    iget v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_rowStartOffset:I

    sub-int/2addr v2, v0

    int-to-long v0, v2

    iput-wide v0, p0, Lcom/fasterxml/aalto/in/XmlScanner;->_startColumn:J

    return-void
.end method

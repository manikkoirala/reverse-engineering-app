.class public abstract Lg0;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lg0$b;,
        Lg0$c;,
        Lg0$a;
    }
.end annotation


# static fields
.field public static final n:J

.field public static final o:J

.field public static final p:J

.field public static final q:J

.field public static final r:J


# instance fields
.field public a:Lcom/google/firebase/firestore/util/AsyncQueue$b;

.field public b:Lcom/google/firebase/firestore/util/AsyncQueue$b;

.field public final c:Lw30;

.field public final d:Lio/grpc/MethodDescriptor;

.field public final e:Lg0$b;

.field public final f:Lcom/google/firebase/firestore/util/AsyncQueue;

.field public final g:Lcom/google/firebase/firestore/util/AsyncQueue$TimerId;

.field public final h:Lcom/google/firebase/firestore/util/AsyncQueue$TimerId;

.field public i:Lcom/google/firebase/firestore/remote/Stream$State;

.field public j:J

.field public k:Lio/grpc/ClientCall;

.field public final l:Lcom/google/firebase/firestore/util/a;

.field public final m:Lcr1;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    .line 1
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v1, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v3

    sput-wide v3, Lg0;->n:J

    sget-object v3, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v3, v1, v2}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v4

    sput-wide v4, Lg0;->o:J

    invoke-virtual {v3, v1, v2}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v1

    sput-wide v1, Lg0;->p:J

    const-wide/16 v1, 0xa

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v3

    sput-wide v3, Lg0;->q:J

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lg0;->r:J

    return-void
.end method

.method public constructor <init>(Lw30;Lio/grpc/MethodDescriptor;Lcom/google/firebase/firestore/util/AsyncQueue;Lcom/google/firebase/firestore/util/AsyncQueue$TimerId;Lcom/google/firebase/firestore/util/AsyncQueue$TimerId;Lcom/google/firebase/firestore/util/AsyncQueue$TimerId;Lcr1;)V
    .locals 11

    .line 1
    move-object v0, p0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v1, Lcom/google/firebase/firestore/remote/Stream$State;->Initial:Lcom/google/firebase/firestore/remote/Stream$State;

    iput-object v1, v0, Lg0;->i:Lcom/google/firebase/firestore/remote/Stream$State;

    const-wide/16 v1, 0x0

    iput-wide v1, v0, Lg0;->j:J

    move-object v1, p1

    iput-object v1, v0, Lg0;->c:Lw30;

    move-object v1, p2

    iput-object v1, v0, Lg0;->d:Lio/grpc/MethodDescriptor;

    move-object v2, p3

    iput-object v2, v0, Lg0;->f:Lcom/google/firebase/firestore/util/AsyncQueue;

    move-object/from16 v1, p5

    iput-object v1, v0, Lg0;->g:Lcom/google/firebase/firestore/util/AsyncQueue$TimerId;

    move-object/from16 v1, p6

    iput-object v1, v0, Lg0;->h:Lcom/google/firebase/firestore/util/AsyncQueue$TimerId;

    move-object/from16 v1, p7

    iput-object v1, v0, Lg0;->m:Lcr1;

    new-instance v1, Lg0$b;

    invoke-direct {v1, p0}, Lg0$b;-><init>(Lg0;)V

    iput-object v1, v0, Lg0;->e:Lg0$b;

    new-instance v10, Lcom/google/firebase/firestore/util/a;

    sget-wide v4, Lg0;->n:J

    const-wide/high16 v6, 0x3ff8000000000000L    # 1.5

    sget-wide v8, Lg0;->o:J

    move-object v1, v10

    move-object v3, p4

    invoke-direct/range {v1 .. v9}, Lcom/google/firebase/firestore/util/a;-><init>(Lcom/google/firebase/firestore/util/AsyncQueue;Lcom/google/firebase/firestore/util/AsyncQueue$TimerId;JDJ)V

    iput-object v10, v0, Lg0;->l:Lcom/google/firebase/firestore/util/a;

    return-void
.end method

.method public static synthetic a(Lg0;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lg0;->p()V

    return-void
.end method

.method public static synthetic b(Lg0;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lg0;->o()V

    return-void
.end method

.method public static synthetic c(Lg0;)Lcom/google/firebase/firestore/util/AsyncQueue;
    .locals 0

    .line 1
    iget-object p0, p0, Lg0;->f:Lcom/google/firebase/firestore/util/AsyncQueue;

    return-object p0
.end method

.method public static synthetic d(Lg0;)J
    .locals 2

    .line 1
    iget-wide v0, p0, Lg0;->j:J

    return-wide v0
.end method

.method public static synthetic e(Lg0;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lg0;->s()V

    return-void
.end method

.method public static synthetic f(Lg0;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lg0;->j()V

    return-void
.end method

.method private synthetic o()V
    .locals 1

    .line 1
    invoke-virtual {p0}, Lg0;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/firebase/firestore/remote/Stream$State;->Healthy:Lcom/google/firebase/firestore/remote/Stream$State;

    iput-object v0, p0, Lg0;->i:Lcom/google/firebase/firestore/remote/Stream$State;

    :cond_0
    return-void
.end method

.method private synthetic p()V
    .locals 4

    .line 1
    iget-object v0, p0, Lg0;->i:Lcom/google/firebase/firestore/remote/Stream$State;

    sget-object v1, Lcom/google/firebase/firestore/remote/Stream$State;->Backoff:Lcom/google/firebase/firestore/remote/Stream$State;

    const/4 v2, 0x0

    if-ne v0, v1, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    move v1, v2

    :goto_0
    const-string v3, "State should still be backoff but was %s"

    filled-new-array {v0}, [Ljava/lang/Object;

    move-result-object v0

    invoke-static {v1, v3, v0}, Lg9;->d(ZLjava/lang/String;[Ljava/lang/Object;)V

    sget-object v0, Lcom/google/firebase/firestore/remote/Stream$State;->Initial:Lcom/google/firebase/firestore/remote/Stream$State;

    iput-object v0, p0, Lg0;->i:Lcom/google/firebase/firestore/remote/Stream$State;

    invoke-virtual {p0}, Lg0;->u()V

    invoke-virtual {p0}, Lg0;->n()Z

    move-result v0

    const-string v1, "Stream should have started"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lg9;->d(ZLjava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public final g()V
    .locals 1

    .line 1
    iget-object v0, p0, Lg0;->a:Lcom/google/firebase/firestore/util/AsyncQueue$b;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/firebase/firestore/util/AsyncQueue$b;->c()V

    const/4 v0, 0x0

    iput-object v0, p0, Lg0;->a:Lcom/google/firebase/firestore/util/AsyncQueue$b;

    :cond_0
    return-void
.end method

.method public final h()V
    .locals 1

    .line 1
    iget-object v0, p0, Lg0;->b:Lcom/google/firebase/firestore/util/AsyncQueue$b;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/firebase/firestore/util/AsyncQueue$b;->c()V

    const/4 v0, 0x0

    iput-object v0, p0, Lg0;->b:Lcom/google/firebase/firestore/util/AsyncQueue$b;

    :cond_0
    return-void
.end method

.method public final i(Lcom/google/firebase/firestore/remote/Stream$State;Lio/grpc/Status;)V
    .locals 5

    .line 1
    invoke-virtual {p0}, Lg0;->n()Z

    move-result v0

    const/4 v1, 0x0

    new-array v2, v1, [Ljava/lang/Object;

    const-string v3, "Only started streams should be closed."

    invoke-static {v0, v3, v2}, Lg9;->d(ZLjava/lang/String;[Ljava/lang/Object;)V

    sget-object v0, Lcom/google/firebase/firestore/remote/Stream$State;->Error:Lcom/google/firebase/firestore/remote/Stream$State;

    if-eq p1, v0, :cond_1

    invoke-virtual {p2}, Lio/grpc/Status;->isOk()Z

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_0

    :cond_0
    move v2, v1

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v2, 0x1

    :goto_1
    const-string v3, "Can\'t provide an error when not in an error state."

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v2, v3, v1}, Lg9;->d(ZLjava/lang/String;[Ljava/lang/Object;)V

    iget-object v1, p0, Lg0;->f:Lcom/google/firebase/firestore/util/AsyncQueue;

    invoke-virtual {v1}, Lcom/google/firebase/firestore/util/AsyncQueue;->p()V

    invoke-static {p2}, Lcom/google/firebase/firestore/remote/d;->e(Lio/grpc/Status;)Z

    move-result v1

    if-eqz v1, :cond_2

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "The Cloud Firestore client failed to establish a secure connection. This is likely a problem with your app, rather than with Cloud Firestore itself. See https://bit.ly/2XFpdma for instructions on how to enable TLS on Android 4.x devices."

    invoke-virtual {p2}, Lio/grpc/Status;->getCause()Ljava/lang/Throwable;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-static {v1}, Lo22;->o(Ljava/lang/RuntimeException;)V

    :cond_2
    invoke-virtual {p0}, Lg0;->h()V

    invoke-virtual {p0}, Lg0;->g()V

    iget-object v1, p0, Lg0;->l:Lcom/google/firebase/firestore/util/a;

    invoke-virtual {v1}, Lcom/google/firebase/firestore/util/a;->c()V

    iget-wide v1, p0, Lg0;->j:J

    const-wide/16 v3, 0x1

    add-long/2addr v1, v3

    iput-wide v1, p0, Lg0;->j:J

    invoke-virtual {p2}, Lio/grpc/Status;->getCode()Lio/grpc/Status$Code;

    move-result-object v1

    sget-object v2, Lio/grpc/Status$Code;->OK:Lio/grpc/Status$Code;

    if-ne v1, v2, :cond_3

    iget-object v1, p0, Lg0;->l:Lcom/google/firebase/firestore/util/a;

    invoke-virtual {v1}, Lcom/google/firebase/firestore/util/a;->f()V

    goto :goto_2

    :cond_3
    sget-object v2, Lio/grpc/Status$Code;->RESOURCE_EXHAUSTED:Lio/grpc/Status$Code;

    if-ne v1, v2, :cond_4

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    filled-new-array {v2}, [Ljava/lang/Object;

    move-result-object v2

    const-string v3, "(%x) Using maximum backoff delay to prevent overloading the backend."

    invoke-static {v1, v3, v2}, Lcom/google/firebase/firestore/util/Logger;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v1, p0, Lg0;->l:Lcom/google/firebase/firestore/util/a;

    invoke-virtual {v1}, Lcom/google/firebase/firestore/util/a;->g()V

    goto :goto_2

    :cond_4
    sget-object v2, Lio/grpc/Status$Code;->UNAUTHENTICATED:Lio/grpc/Status$Code;

    if-ne v1, v2, :cond_5

    iget-object v2, p0, Lg0;->i:Lcom/google/firebase/firestore/remote/Stream$State;

    sget-object v3, Lcom/google/firebase/firestore/remote/Stream$State;->Healthy:Lcom/google/firebase/firestore/remote/Stream$State;

    if-eq v2, v3, :cond_5

    iget-object v1, p0, Lg0;->c:Lw30;

    invoke-virtual {v1}, Lw30;->d()V

    goto :goto_2

    :cond_5
    sget-object v2, Lio/grpc/Status$Code;->UNAVAILABLE:Lio/grpc/Status$Code;

    if-ne v1, v2, :cond_7

    invoke-virtual {p2}, Lio/grpc/Status;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    instance-of v1, v1, Ljava/net/UnknownHostException;

    if-nez v1, :cond_6

    invoke-virtual {p2}, Lio/grpc/Status;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    instance-of v1, v1, Ljava/net/ConnectException;

    if-eqz v1, :cond_7

    :cond_6
    iget-object v1, p0, Lg0;->l:Lcom/google/firebase/firestore/util/a;

    sget-wide v2, Lg0;->r:J

    invoke-virtual {v1, v2, v3}, Lcom/google/firebase/firestore/util/a;->h(J)V

    :cond_7
    :goto_2
    if-eq p1, v0, :cond_8

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    filled-new-array {v1}, [Ljava/lang/Object;

    move-result-object v1

    const-string v2, "(%x) Performing stream teardown"

    invoke-static {v0, v2, v1}, Lcom/google/firebase/firestore/util/Logger;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-virtual {p0}, Lg0;->w()V

    :cond_8
    iget-object v0, p0, Lg0;->k:Lio/grpc/ClientCall;

    if-eqz v0, :cond_a

    invoke-virtual {p2}, Lio/grpc/Status;->isOk()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    filled-new-array {v1}, [Ljava/lang/Object;

    move-result-object v1

    const-string v2, "(%x) Closing stream client-side"

    invoke-static {v0, v2, v1}, Lcom/google/firebase/firestore/util/Logger;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lg0;->k:Lio/grpc/ClientCall;

    invoke-virtual {v0}, Lio/grpc/ClientCall;->halfClose()V

    :cond_9
    const/4 v0, 0x0

    iput-object v0, p0, Lg0;->k:Lio/grpc/ClientCall;

    :cond_a
    iput-object p1, p0, Lg0;->i:Lcom/google/firebase/firestore/remote/Stream$State;

    iget-object p1, p0, Lg0;->m:Lcr1;

    invoke-interface {p1, p2}, Lcr1;->a(Lio/grpc/Status;)V

    return-void
.end method

.method public final j()V
    .locals 2

    .line 1
    invoke-virtual {p0}, Lg0;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/firebase/firestore/remote/Stream$State;->Initial:Lcom/google/firebase/firestore/remote/Stream$State;

    sget-object v1, Lio/grpc/Status;->OK:Lio/grpc/Status;

    invoke-virtual {p0, v0, v1}, Lg0;->i(Lcom/google/firebase/firestore/remote/Stream$State;Lio/grpc/Status;)V

    :cond_0
    return-void
.end method

.method public k(Lio/grpc/Status;)V
    .locals 3

    .line 1
    invoke-virtual {p0}, Lg0;->n()Z

    move-result v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "Can\'t handle server close on non-started stream!"

    invoke-static {v0, v2, v1}, Lg9;->d(ZLjava/lang/String;[Ljava/lang/Object;)V

    sget-object v0, Lcom/google/firebase/firestore/remote/Stream$State;->Error:Lcom/google/firebase/firestore/remote/Stream$State;

    invoke-virtual {p0, v0, p1}, Lg0;->i(Lcom/google/firebase/firestore/remote/Stream$State;Lio/grpc/Status;)V

    return-void
.end method

.method public l()V
    .locals 3

    .line 1
    invoke-virtual {p0}, Lg0;->n()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "Can only inhibit backoff after in a stopped state"

    invoke-static {v0, v2, v1}, Lg9;->d(ZLjava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lg0;->f:Lcom/google/firebase/firestore/util/AsyncQueue;

    invoke-virtual {v0}, Lcom/google/firebase/firestore/util/AsyncQueue;->p()V

    sget-object v0, Lcom/google/firebase/firestore/remote/Stream$State;->Initial:Lcom/google/firebase/firestore/remote/Stream$State;

    iput-object v0, p0, Lg0;->i:Lcom/google/firebase/firestore/remote/Stream$State;

    iget-object v0, p0, Lg0;->l:Lcom/google/firebase/firestore/util/a;

    invoke-virtual {v0}, Lcom/google/firebase/firestore/util/a;->f()V

    return-void
.end method

.method public m()Z
    .locals 2

    .line 1
    iget-object v0, p0, Lg0;->f:Lcom/google/firebase/firestore/util/AsyncQueue;

    invoke-virtual {v0}, Lcom/google/firebase/firestore/util/AsyncQueue;->p()V

    iget-object v0, p0, Lg0;->i:Lcom/google/firebase/firestore/remote/Stream$State;

    sget-object v1, Lcom/google/firebase/firestore/remote/Stream$State;->Open:Lcom/google/firebase/firestore/remote/Stream$State;

    if-eq v0, v1, :cond_1

    sget-object v1, Lcom/google/firebase/firestore/remote/Stream$State;->Healthy:Lcom/google/firebase/firestore/remote/Stream$State;

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public n()Z
    .locals 2

    .line 1
    iget-object v0, p0, Lg0;->f:Lcom/google/firebase/firestore/util/AsyncQueue;

    invoke-virtual {v0}, Lcom/google/firebase/firestore/util/AsyncQueue;->p()V

    iget-object v0, p0, Lg0;->i:Lcom/google/firebase/firestore/remote/Stream$State;

    sget-object v1, Lcom/google/firebase/firestore/remote/Stream$State;->Starting:Lcom/google/firebase/firestore/remote/Stream$State;

    if-eq v0, v1, :cond_1

    sget-object v1, Lcom/google/firebase/firestore/remote/Stream$State;->Backoff:Lcom/google/firebase/firestore/remote/Stream$State;

    if-eq v0, v1, :cond_1

    invoke-virtual {p0}, Lg0;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public q()V
    .locals 5

    .line 1
    invoke-virtual {p0}, Lg0;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lg0;->b:Lcom/google/firebase/firestore/util/AsyncQueue$b;

    if-nez v0, :cond_0

    iget-object v0, p0, Lg0;->f:Lcom/google/firebase/firestore/util/AsyncQueue;

    iget-object v1, p0, Lg0;->g:Lcom/google/firebase/firestore/util/AsyncQueue$TimerId;

    sget-wide v2, Lg0;->p:J

    iget-object v4, p0, Lg0;->e:Lg0$b;

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/firebase/firestore/util/AsyncQueue;->h(Lcom/google/firebase/firestore/util/AsyncQueue$TimerId;JLjava/lang/Runnable;)Lcom/google/firebase/firestore/util/AsyncQueue$b;

    move-result-object v0

    iput-object v0, p0, Lg0;->b:Lcom/google/firebase/firestore/util/AsyncQueue$b;

    :cond_0
    return-void
.end method

.method public abstract r(Ljava/lang/Object;)V
.end method

.method public final s()V
    .locals 5

    .line 1
    sget-object v0, Lcom/google/firebase/firestore/remote/Stream$State;->Open:Lcom/google/firebase/firestore/remote/Stream$State;

    iput-object v0, p0, Lg0;->i:Lcom/google/firebase/firestore/remote/Stream$State;

    iget-object v0, p0, Lg0;->m:Lcr1;

    invoke-interface {v0}, Lcr1;->b()V

    iget-object v0, p0, Lg0;->a:Lcom/google/firebase/firestore/util/AsyncQueue$b;

    if-nez v0, :cond_0

    iget-object v0, p0, Lg0;->f:Lcom/google/firebase/firestore/util/AsyncQueue;

    iget-object v1, p0, Lg0;->h:Lcom/google/firebase/firestore/util/AsyncQueue$TimerId;

    sget-wide v2, Lg0;->q:J

    new-instance v4, Lf0;

    invoke-direct {v4, p0}, Lf0;-><init>(Lg0;)V

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/firebase/firestore/util/AsyncQueue;->h(Lcom/google/firebase/firestore/util/AsyncQueue$TimerId;JLjava/lang/Runnable;)Lcom/google/firebase/firestore/util/AsyncQueue$b;

    move-result-object v0

    iput-object v0, p0, Lg0;->a:Lcom/google/firebase/firestore/util/AsyncQueue$b;

    :cond_0
    return-void
.end method

.method public final t()V
    .locals 3

    .line 1
    iget-object v0, p0, Lg0;->i:Lcom/google/firebase/firestore/remote/Stream$State;

    sget-object v1, Lcom/google/firebase/firestore/remote/Stream$State;->Error:Lcom/google/firebase/firestore/remote/Stream$State;

    const/4 v2, 0x0

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    move v0, v2

    :goto_0
    const-string v1, "Should only perform backoff in an error state"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lg9;->d(ZLjava/lang/String;[Ljava/lang/Object;)V

    sget-object v0, Lcom/google/firebase/firestore/remote/Stream$State;->Backoff:Lcom/google/firebase/firestore/remote/Stream$State;

    iput-object v0, p0, Lg0;->i:Lcom/google/firebase/firestore/remote/Stream$State;

    iget-object v0, p0, Lg0;->l:Lcom/google/firebase/firestore/util/a;

    new-instance v1, Le0;

    invoke-direct {v1, p0}, Le0;-><init>(Lg0;)V

    invoke-virtual {v0, v1}, Lcom/google/firebase/firestore/util/a;->b(Ljava/lang/Runnable;)V

    return-void
.end method

.method public u()V
    .locals 5

    .line 1
    iget-object v0, p0, Lg0;->f:Lcom/google/firebase/firestore/util/AsyncQueue;

    invoke-virtual {v0}, Lcom/google/firebase/firestore/util/AsyncQueue;->p()V

    iget-object v0, p0, Lg0;->k:Lio/grpc/ClientCall;

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-nez v0, :cond_0

    move v0, v1

    goto :goto_0

    :cond_0
    move v0, v2

    :goto_0
    const-string v3, "Last call still set"

    new-array v4, v2, [Ljava/lang/Object;

    invoke-static {v0, v3, v4}, Lg9;->d(ZLjava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lg0;->b:Lcom/google/firebase/firestore/util/AsyncQueue$b;

    if-nez v0, :cond_1

    move v0, v1

    goto :goto_1

    :cond_1
    move v0, v2

    :goto_1
    const-string v3, "Idle timer still set"

    new-array v4, v2, [Ljava/lang/Object;

    invoke-static {v0, v3, v4}, Lg9;->d(ZLjava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lg0;->i:Lcom/google/firebase/firestore/remote/Stream$State;

    sget-object v3, Lcom/google/firebase/firestore/remote/Stream$State;->Error:Lcom/google/firebase/firestore/remote/Stream$State;

    if-ne v0, v3, :cond_2

    invoke-virtual {p0}, Lg0;->t()V

    return-void

    :cond_2
    sget-object v3, Lcom/google/firebase/firestore/remote/Stream$State;->Initial:Lcom/google/firebase/firestore/remote/Stream$State;

    if-ne v0, v3, :cond_3

    goto :goto_2

    :cond_3
    move v1, v2

    :goto_2
    const-string v0, "Already started"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1, v0, v2}, Lg9;->d(ZLjava/lang/String;[Ljava/lang/Object;)V

    new-instance v0, Lg0$a;

    iget-wide v1, p0, Lg0;->j:J

    invoke-direct {v0, p0, v1, v2}, Lg0$a;-><init>(Lg0;J)V

    new-instance v1, Lg0$c;

    invoke-direct {v1, p0, v0}, Lg0$c;-><init>(Lg0;Lg0$a;)V

    iget-object v0, p0, Lg0;->c:Lw30;

    iget-object v2, p0, Lg0;->d:Lio/grpc/MethodDescriptor;

    invoke-virtual {v0, v2, v1}, Lw30;->g(Lio/grpc/MethodDescriptor;Lpe0;)Lio/grpc/ClientCall;

    move-result-object v0

    iput-object v0, p0, Lg0;->k:Lio/grpc/ClientCall;

    sget-object v0, Lcom/google/firebase/firestore/remote/Stream$State;->Starting:Lcom/google/firebase/firestore/remote/Stream$State;

    iput-object v0, p0, Lg0;->i:Lcom/google/firebase/firestore/remote/Stream$State;

    return-void
.end method

.method public v()V
    .locals 2

    .line 1
    invoke-virtual {p0}, Lg0;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/firebase/firestore/remote/Stream$State;->Initial:Lcom/google/firebase/firestore/remote/Stream$State;

    sget-object v1, Lio/grpc/Status;->OK:Lio/grpc/Status;

    invoke-virtual {p0, v0, v1}, Lg0;->i(Lcom/google/firebase/firestore/remote/Stream$State;Lio/grpc/Status;)V

    :cond_0
    return-void
.end method

.method public w()V
    .locals 0

    .line 1
    return-void
.end method

.method public x(Ljava/lang/Object;)V
    .locals 3

    .line 1
    iget-object v0, p0, Lg0;->f:Lcom/google/firebase/firestore/util/AsyncQueue;

    invoke-virtual {v0}, Lcom/google/firebase/firestore/util/AsyncQueue;->p()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    filled-new-array {v1, p1}, [Ljava/lang/Object;

    move-result-object v1

    const-string v2, "(%x) Stream sending: %s"

    invoke-static {v0, v2, v1}, Lcom/google/firebase/firestore/util/Logger;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-virtual {p0}, Lg0;->h()V

    iget-object v0, p0, Lg0;->k:Lio/grpc/ClientCall;

    invoke-virtual {v0, p1}, Lio/grpc/ClientCall;->sendMessage(Ljava/lang/Object;)V

    return-void
.end method

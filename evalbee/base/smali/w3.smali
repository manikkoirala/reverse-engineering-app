.class public Lw3;
.super Lu80;
.source "SourceFile"


# instance fields
.field public a:Landroid/content/Context;

.field public b:Ly01;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/String;

.field public i:Lcom/google/android/material/textfield/TextInputLayout;

.field public j:Lcom/google/android/material/textfield/TextInputLayout;

.field public k:Lcom/google/android/material/textfield/TextInputLayout;

.field public l:Lcom/google/android/material/textfield/TextInputLayout;

.field public m:Lcom/google/android/material/textfield/TextInputLayout;

.field public n:Landroid/widget/Button;

.field public p:Landroid/text/TextWatcher;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ly01;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 1
    invoke-direct {p0, p1}, Lu80;-><init>(Landroid/content/Context;)V

    new-instance v0, Lw3$a;

    invoke-direct {v0, p0}, Lw3$a;-><init>(Lw3;)V

    iput-object v0, p0, Lw3;->p:Landroid/text/TextWatcher;

    iput-object p1, p0, Lw3;->a:Landroid/content/Context;

    iput-object p3, p0, Lw3;->b:Ly01;

    iput-object p4, p0, Lw3;->c:Ljava/lang/String;

    iput-object p5, p0, Lw3;->d:Ljava/lang/String;

    iput-object p7, p0, Lw3;->e:Ljava/lang/String;

    iput-object p8, p0, Lw3;->f:Ljava/lang/String;

    iput-object p6, p0, Lw3;->g:Ljava/lang/String;

    iput-object p2, p0, Lw3;->h:Ljava/lang/String;

    return-void
.end method

.method public static synthetic a(Lw3;)Landroid/widget/Button;
    .locals 0

    .line 1
    iget-object p0, p0, Lw3;->n:Landroid/widget/Button;

    return-object p0
.end method

.method public static synthetic b(Lw3;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lw3;->g:Ljava/lang/String;

    return-object p0
.end method

.method public static synthetic c(Lw3;)Ly01;
    .locals 0

    .line 1
    iget-object p0, p0, Lw3;->b:Ly01;

    return-object p0
.end method


# virtual methods
.method public final d()V
    .locals 2

    .line 1
    iget-object v0, p0, Lw3;->n:Landroid/widget/Button;

    new-instance v1, Lw3$d;

    invoke-direct {v1, p0}, Lw3$d;-><init>(Lw3;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final e()V
    .locals 2

    .line 1
    const v0, 0x7f090437

    invoke-virtual {p0, v0}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/appcompat/widget/Toolbar;

    new-instance v1, Lw3$b;

    invoke-direct {v1, p0}, Lw3$b;-><init>(Lw3;)V

    invoke-virtual {v0, v1}, Landroidx/appcompat/widget/Toolbar;->setNavigationOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    .line 1
    invoke-super {p0, p1}, Lu80;->onCreate(Landroid/os/Bundle;)V

    const p1, 0x7f0c0061

    invoke-virtual {p0, p1}, Landroid/app/Dialog;->setContentView(I)V

    invoke-virtual {p0}, Lw3;->e()V

    const p1, 0x7f090098

    invoke-virtual {p0, p1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/Button;

    iput-object p1, p0, Lw3;->n:Landroid/widget/Button;

    const p1, 0x7f09039c

    invoke-virtual {p0, p1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/google/android/material/textfield/TextInputLayout;

    iput-object p1, p0, Lw3;->i:Lcom/google/android/material/textfield/TextInputLayout;

    const p1, 0x7f09039d

    invoke-virtual {p0, p1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/google/android/material/textfield/TextInputLayout;

    iput-object p1, p0, Lw3;->j:Lcom/google/android/material/textfield/TextInputLayout;

    const p1, 0x7f09039b

    invoke-virtual {p0, p1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/google/android/material/textfield/TextInputLayout;

    iput-object p1, p0, Lw3;->l:Lcom/google/android/material/textfield/TextInputLayout;

    const p1, 0x7f090395

    invoke-virtual {p0, p1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/google/android/material/textfield/TextInputLayout;

    iput-object p1, p0, Lw3;->m:Lcom/google/android/material/textfield/TextInputLayout;

    const p1, 0x7f090394

    invoke-virtual {p0, p1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/google/android/material/textfield/TextInputLayout;

    iput-object p1, p0, Lw3;->k:Lcom/google/android/material/textfield/TextInputLayout;

    iget-object p1, p0, Lw3;->j:Lcom/google/android/material/textfield/TextInputLayout;

    invoke-virtual {p1}, Lcom/google/android/material/textfield/TextInputLayout;->getEditText()Landroid/widget/EditText;

    move-result-object p1

    iget-object v0, p0, Lw3;->p:Landroid/text/TextWatcher;

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object p1, p0, Lw3;->i:Lcom/google/android/material/textfield/TextInputLayout;

    invoke-virtual {p1}, Lcom/google/android/material/textfield/TextInputLayout;->getEditText()Landroid/widget/EditText;

    move-result-object p1

    iget-object v0, p0, Lw3;->p:Landroid/text/TextWatcher;

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object p1, p0, Lw3;->k:Lcom/google/android/material/textfield/TextInputLayout;

    invoke-virtual {p1}, Lcom/google/android/material/textfield/TextInputLayout;->getEditText()Landroid/widget/EditText;

    move-result-object p1

    iget-object v0, p0, Lw3;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object p1, p0, Lw3;->j:Lcom/google/android/material/textfield/TextInputLayout;

    invoke-virtual {p1}, Lcom/google/android/material/textfield/TextInputLayout;->getEditText()Landroid/widget/EditText;

    move-result-object p1

    iget-object v0, p0, Lw3;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object p1, p0, Lw3;->i:Lcom/google/android/material/textfield/TextInputLayout;

    invoke-virtual {p1}, Lcom/google/android/material/textfield/TextInputLayout;->getEditText()Landroid/widget/EditText;

    move-result-object p1

    iget-object v0, p0, Lw3;->c:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object p1, p0, Lw3;->m:Lcom/google/android/material/textfield/TextInputLayout;

    invoke-virtual {p1}, Lcom/google/android/material/textfield/TextInputLayout;->getEditText()Landroid/widget/EditText;

    move-result-object p1

    iget-object v0, p0, Lw3;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object p1, p0, Lw3;->l:Lcom/google/android/material/textfield/TextInputLayout;

    invoke-virtual {p1}, Lcom/google/android/material/textfield/TextInputLayout;->getEditText()Landroid/widget/EditText;

    move-result-object p1

    iget-object v0, p0, Lw3;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lw3;->d()V

    const p1, 0x7f0900ae

    invoke-virtual {p0, p1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object p1

    new-instance v0, Lw3$c;

    invoke-direct {v0, p0}, Lw3$c;-><init>(Lw3;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

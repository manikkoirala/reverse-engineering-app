.class public Lc92;
.super Landroidx/work/WorkManager;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lc92$a;
    }
.end annotation


# static fields
.field public static final k:Ljava/lang/String;

.field public static l:Lc92;

.field public static m:Lc92;

.field public static final n:Ljava/lang/Object;


# instance fields
.field public a:Landroid/content/Context;

.field public b:Landroidx/work/a;

.field public c:Landroidx/work/impl/WorkDatabase;

.field public d:Lhu1;

.field public e:Ljava/util/List;

.field public f:Lq81;

.field public g:Lr71;

.field public h:Z

.field public i:Landroid/content/BroadcastReceiver$PendingResult;

.field public final j:Lky1;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .line 1
    const-string v0, "WorkManagerImpl"

    invoke-static {v0}, Lxl0;->i(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lc92;->k:Ljava/lang/String;

    const/4 v0, 0x0

    sput-object v0, Lc92;->l:Lc92;

    sput-object v0, Lc92;->m:Lc92;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lc92;->n:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroidx/work/a;Lhu1;Landroidx/work/impl/WorkDatabase;Ljava/util/List;Lq81;Lky1;)V
    .locals 2

    .line 1
    invoke-direct {p0}, Landroidx/work/WorkManager;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lc92;->h:Z

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lc92$a;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Lxl0$a;

    invoke-virtual {p2}, Landroidx/work/a;->j()I

    move-result v1

    invoke-direct {v0, v1}, Lxl0$a;-><init>(I)V

    invoke-static {v0}, Lxl0;->h(Lxl0;)V

    iput-object p1, p0, Lc92;->a:Landroid/content/Context;

    iput-object p3, p0, Lc92;->d:Lhu1;

    iput-object p4, p0, Lc92;->c:Landroidx/work/impl/WorkDatabase;

    iput-object p6, p0, Lc92;->f:Lq81;

    iput-object p7, p0, Lc92;->j:Lky1;

    iput-object p2, p0, Lc92;->b:Landroidx/work/a;

    iput-object p5, p0, Lc92;->e:Ljava/util/List;

    new-instance p6, Lr71;

    invoke-direct {p6, p4}, Lr71;-><init>(Landroidx/work/impl/WorkDatabase;)V

    iput-object p6, p0, Lc92;->g:Lr71;

    iget-object p4, p0, Lc92;->f:Lq81;

    invoke-interface {p3}, Lhu1;->d()Ljl1;

    move-result-object p3

    iget-object p6, p0, Lc92;->c:Landroidx/work/impl/WorkDatabase;

    invoke-static {p5, p4, p3, p6, p2}, Lnj1;->g(Ljava/util/List;Lq81;Ljava/util/concurrent/Executor;Landroidx/work/impl/WorkDatabase;Landroidx/work/a;)V

    iget-object p2, p0, Lc92;->d:Lhu1;

    new-instance p3, Landroidx/work/impl/utils/ForceStopRunnable;

    invoke-direct {p3, p1, p0}, Landroidx/work/impl/utils/ForceStopRunnable;-><init>(Landroid/content/Context;Lc92;)V

    invoke-interface {p2, p3}, Lhu1;->b(Ljava/lang/Runnable;)V

    return-void

    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Cannot initialize WorkManager in direct boot mode"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public static e(Landroid/content/Context;Landroidx/work/a;)V
    .locals 3

    .line 1
    sget-object v0, Lc92;->n:Ljava/lang/Object;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lc92;->l:Lc92;

    if-eqz v1, :cond_1

    sget-object v2, Lc92;->m:Lc92;

    if-nez v2, :cond_0

    goto :goto_0

    :cond_0
    new-instance p0, Ljava/lang/IllegalStateException;

    const-string p1, "WorkManager is already initialized.  Did you try to initialize it manually without disabling WorkManagerInitializer? See WorkManager#initialize(Context, Configuration) or the class level Javadoc for more information."

    invoke-direct {p0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p0

    :cond_1
    :goto_0
    if-nez v1, :cond_3

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p0

    sget-object v1, Lc92;->m:Lc92;

    if-nez v1, :cond_2

    invoke-static {p0, p1}, Landroidx/work/impl/WorkManagerImplExtKt;->c(Landroid/content/Context;Landroidx/work/a;)Lc92;

    move-result-object p0

    sput-object p0, Lc92;->m:Lc92;

    :cond_2
    sget-object p0, Lc92;->m:Lc92;

    sput-object p0, Lc92;->l:Lc92;

    :cond_3
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p0

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p0
.end method

.method public static i()Lc92;
    .locals 2

    .line 1
    sget-object v0, Lc92;->n:Ljava/lang/Object;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lc92;->l:Lc92;

    if-eqz v1, :cond_0

    monitor-exit v0

    return-object v1

    :cond_0
    sget-object v1, Lc92;->m:Lc92;

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public static j(Landroid/content/Context;)Lc92;
    .locals 2

    .line 1
    sget-object v0, Lc92;->n:Ljava/lang/Object;

    monitor-enter v0

    :try_start_0
    invoke-static {}, Lc92;->i()Lc92;

    move-result-object v1

    if-eqz v1, :cond_0

    monitor-exit v0

    return-object v1

    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    new-instance p0, Ljava/lang/IllegalStateException;

    const-string v1, "WorkManager is not initialized properly.  You have explicitly disabled WorkManagerInitializer in your manifest, have not manually called WorkManager#initialize at this point, and your Application does not implement Configuration.Provider."

    invoke-direct {p0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p0

    :catchall_0
    move-exception p0

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p0
.end method


# virtual methods
.method public a(Ljava/lang/String;)Landroidx/work/d;
    .locals 1

    .line 1
    invoke-static {p1, p0}, Lbf;->d(Ljava/lang/String;Lc92;)Lbf;

    move-result-object p1

    iget-object v0, p0, Lc92;->d:Lhu1;

    invoke-interface {v0, p1}, Lhu1;->b(Ljava/lang/Runnable;)V

    invoke-virtual {p1}, Lbf;->e()Landroidx/work/d;

    move-result-object p1

    return-object p1
.end method

.method public c(Ljava/util/List;)Landroidx/work/d;
    .locals 1

    .line 1
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Lj82;

    invoke-direct {v0, p0, p1}, Lj82;-><init>(Lc92;Ljava/util/List;)V

    invoke-virtual {v0}, Lj82;->a()Landroidx/work/d;

    move-result-object p1

    return-object p1

    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "enqueue needs at least one WorkRequest."

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public f(Ljava/util/UUID;)Landroidx/work/d;
    .locals 1

    .line 1
    invoke-static {p1, p0}, Lbf;->b(Ljava/util/UUID;Lc92;)Lbf;

    move-result-object p1

    iget-object v0, p0, Lc92;->d:Lhu1;

    invoke-interface {v0, p1}, Lhu1;->b(Ljava/lang/Runnable;)V

    invoke-virtual {p1}, Lbf;->e()Landroidx/work/d;

    move-result-object p1

    return-object p1
.end method

.method public g()Landroid/content/Context;
    .locals 1

    .line 1
    iget-object v0, p0, Lc92;->a:Landroid/content/Context;

    return-object v0
.end method

.method public h()Landroidx/work/a;
    .locals 1

    .line 1
    iget-object v0, p0, Lc92;->b:Landroidx/work/a;

    return-object v0
.end method

.method public k()Lr71;
    .locals 1

    .line 1
    iget-object v0, p0, Lc92;->g:Lr71;

    return-object v0
.end method

.method public l()Lq81;
    .locals 1

    .line 1
    iget-object v0, p0, Lc92;->f:Lq81;

    return-object v0
.end method

.method public m()Ljava/util/List;
    .locals 1

    .line 1
    iget-object v0, p0, Lc92;->e:Ljava/util/List;

    return-object v0
.end method

.method public n()Lky1;
    .locals 1

    .line 1
    iget-object v0, p0, Lc92;->j:Lky1;

    return-object v0
.end method

.method public o()Landroidx/work/impl/WorkDatabase;
    .locals 1

    .line 1
    iget-object v0, p0, Lc92;->c:Landroidx/work/impl/WorkDatabase;

    return-object v0
.end method

.method public p()Lhu1;
    .locals 1

    .line 1
    iget-object v0, p0, Lc92;->d:Lhu1;

    return-object v0
.end method

.method public q()V
    .locals 2

    .line 1
    sget-object v0, Lc92;->n:Ljava/lang/Object;

    monitor-enter v0

    const/4 v1, 0x1

    :try_start_0
    iput-boolean v1, p0, Lc92;->h:Z

    iget-object v1, p0, Lc92;->i:Landroid/content/BroadcastReceiver$PendingResult;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/content/BroadcastReceiver$PendingResult;->finish()V

    const/4 v1, 0x0

    iput-object v1, p0, Lc92;->i:Landroid/content/BroadcastReceiver$PendingResult;

    :cond_0
    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public r()V
    .locals 3

    .line 1
    invoke-virtual {p0}, Lc92;->g()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lpt1;->c(Landroid/content/Context;)V

    invoke-virtual {p0}, Lc92;->o()Landroidx/work/impl/WorkDatabase;

    move-result-object v0

    invoke-virtual {v0}, Landroidx/work/impl/WorkDatabase;->K()Lq92;

    move-result-object v0

    invoke-interface {v0}, Lq92;->u()I

    invoke-virtual {p0}, Lc92;->h()Landroidx/work/a;

    move-result-object v0

    invoke-virtual {p0}, Lc92;->o()Landroidx/work/impl/WorkDatabase;

    move-result-object v1

    invoke-virtual {p0}, Lc92;->m()Ljava/util/List;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lnj1;->h(Landroidx/work/a;Landroidx/work/impl/WorkDatabase;Ljava/util/List;)V

    return-void
.end method

.method public s(Landroid/content/BroadcastReceiver$PendingResult;)V
    .locals 2

    .line 1
    sget-object v0, Lc92;->n:Ljava/lang/Object;

    monitor-enter v0

    :try_start_0
    iget-object v1, p0, Lc92;->i:Landroid/content/BroadcastReceiver$PendingResult;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/content/BroadcastReceiver$PendingResult;->finish()V

    :cond_0
    iput-object p1, p0, Lc92;->i:Landroid/content/BroadcastReceiver$PendingResult;

    iget-boolean v1, p0, Lc92;->h:Z

    if-eqz v1, :cond_1

    invoke-virtual {p1}, Landroid/content/BroadcastReceiver$PendingResult;->finish()V

    const/4 p1, 0x0

    iput-object p1, p0, Lc92;->i:Landroid/content/BroadcastReceiver$PendingResult;

    :cond_1
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public t(Lx82;)V
    .locals 4

    .line 1
    iget-object v0, p0, Lc92;->d:Lhu1;

    new-instance v1, Lcq1;

    iget-object v2, p0, Lc92;->f:Lq81;

    new-instance v3, Lop1;

    invoke-direct {v3, p1}, Lop1;-><init>(Lx82;)V

    const/4 p1, 0x1

    invoke-direct {v1, v2, v3, p1}, Lcq1;-><init>(Lq81;Lop1;Z)V

    invoke-interface {v0, v1}, Lhu1;->b(Ljava/lang/Runnable;)V

    return-void
.end method

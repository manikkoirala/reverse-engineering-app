.class public abstract Lt42;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Lg52;

.field public static final b:Landroid/util/Property;

.field public static final c:Landroid/util/Property;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .line 1
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x1d

    if-lt v0, v1, :cond_0

    new-instance v0, Lf52;

    invoke-direct {v0}, Lf52;-><init>()V

    :goto_0
    sput-object v0, Lt42;->a:Lg52;

    goto :goto_1

    :cond_0
    new-instance v0, Le52;

    invoke-direct {v0}, Le52;-><init>()V

    goto :goto_0

    :goto_1
    new-instance v0, Lt42$a;

    const-class v1, Ljava/lang/Float;

    const-string v2, "translationAlpha"

    invoke-direct {v0, v1, v2}, Lt42$a;-><init>(Ljava/lang/Class;Ljava/lang/String;)V

    sput-object v0, Lt42;->b:Landroid/util/Property;

    new-instance v0, Lt42$b;

    const-class v1, Landroid/graphics/Rect;

    const-string v2, "clipBounds"

    invoke-direct {v0, v1, v2}, Lt42$b;-><init>(Ljava/lang/Class;Ljava/lang/String;)V

    sput-object v0, Lt42;->c:Landroid/util/Property;

    return-void
.end method

.method public static a(Landroid/view/View;)V
    .locals 1

    .line 1
    sget-object v0, Lt42;->a:Lg52;

    invoke-virtual {v0, p0}, Lg52;->a(Landroid/view/View;)V

    return-void
.end method

.method public static b(Landroid/view/View;)Le42;
    .locals 1

    .line 1
    new-instance v0, Ld42;

    invoke-direct {v0, p0}, Ld42;-><init>(Landroid/view/View;)V

    return-object v0
.end method

.method public static c(Landroid/view/View;)F
    .locals 1

    .line 1
    sget-object v0, Lt42;->a:Lg52;

    invoke-virtual {v0, p0}, Lg52;->b(Landroid/view/View;)F

    move-result p0

    return p0
.end method

.method public static d(Landroid/view/View;)Lg62;
    .locals 1

    .line 1
    new-instance v0, Lf62;

    invoke-direct {v0, p0}, Lf62;-><init>(Landroid/view/View;)V

    return-object v0
.end method

.method public static e(Landroid/view/View;)V
    .locals 1

    .line 1
    sget-object v0, Lt42;->a:Lg52;

    invoke-virtual {v0, p0}, Lg52;->c(Landroid/view/View;)V

    return-void
.end method

.method public static f(Landroid/view/View;IIII)V
    .locals 6

    .line 1
    sget-object v0, Lt42;->a:Lg52;

    move-object v1, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-virtual/range {v0 .. v5}, Lg52;->d(Landroid/view/View;IIII)V

    return-void
.end method

.method public static g(Landroid/view/View;F)V
    .locals 1

    .line 1
    sget-object v0, Lt42;->a:Lg52;

    invoke-virtual {v0, p0, p1}, Lg52;->e(Landroid/view/View;F)V

    return-void
.end method

.method public static h(Landroid/view/View;I)V
    .locals 1

    .line 1
    sget-object v0, Lt42;->a:Lg52;

    invoke-virtual {v0, p0, p1}, Lg52;->f(Landroid/view/View;I)V

    return-void
.end method

.method public static i(Landroid/view/View;Landroid/graphics/Matrix;)V
    .locals 1

    .line 1
    sget-object v0, Lt42;->a:Lg52;

    invoke-virtual {v0, p0, p1}, Lg52;->g(Landroid/view/View;Landroid/graphics/Matrix;)V

    return-void
.end method

.method public static j(Landroid/view/View;Landroid/graphics/Matrix;)V
    .locals 1

    .line 1
    sget-object v0, Lt42;->a:Lg52;

    invoke-virtual {v0, p0, p1}, Lg52;->h(Landroid/view/View;Landroid/graphics/Matrix;)V

    return-void
.end method

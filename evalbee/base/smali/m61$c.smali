.class public Lm61$c;
.super Lhr1;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lm61;->f()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic w:Lm61;


# direct methods
.method public constructor <init>(Lm61;ILjava/lang/String;Lcom/android/volley/d$b;Lcom/android/volley/d$a;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lm61$c;->w:Lm61;

    invoke-direct {p0, p2, p3, p4, p5}, Lhr1;-><init>(ILjava/lang/String;Lcom/android/volley/d$b;Lcom/android/volley/d$a;)V

    return-void
.end method


# virtual methods
.method public k()[B
    .locals 5

    .line 1
    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p0, Lm61$c;->w:Lm61;

    invoke-static {v1}, Lm61;->c(Lm61;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    iget-object v2, p0, Lm61$c;->w:Lm61;

    invoke-static {v2}, Lm61;->c(Lm61;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    iget v1, v1, Landroid/content/pm/PackageInfo;->versionCode:I

    invoke-static {}, Lcom/google/firebase/auth/FirebaseAuth;->getInstance()Lcom/google/firebase/auth/FirebaseAuth;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/firebase/auth/FirebaseAuth;->e()Lr30;

    move-result-object v2

    invoke-virtual {v2}, Lr30;->getEmail()Ljava/lang/String;

    move-result-object v2

    const-string v3, "emailKey"

    invoke-static {v2, v3}, Lb;->i(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/ekodroid/omrevaluator/serializable/ResponseModel/SMSBuyLink;

    iget-object v4, p0, Lm61$c;->w:Lm61;

    invoke-static {v4}, Lm61;->d(Lm61;)I

    move-result v4

    invoke-direct {v3, v2, v4, v0, v1}, Lcom/ekodroid/omrevaluator/serializable/ResponseModel/SMSBuyLink;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    new-instance v1, Lgc0;

    invoke-direct {v1}, Lgc0;-><init>()V

    invoke-virtual {v1, v3}, Lgc0;->s(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lm61$c;->w:Lm61;

    invoke-static {v2}, Lm61;->a(Lm61;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lb;->i(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "UTF-8"

    invoke-virtual {v1, v2}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v0
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v1

    goto :goto_0

    :catch_1
    move-exception v1

    :goto_0
    invoke-virtual {v1}, Ljava/lang/Throwable;->printStackTrace()V

    return-object v0
.end method

.method public l()Ljava/lang/String;
    .locals 1

    .line 1
    const-string v0, "application/json"

    return-object v0
.end method

.method public o()Ljava/util/Map;
    .locals 3

    .line 1
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iget-object v1, p0, Lm61$c;->w:Lm61;

    invoke-static {v1}, Lm61;->a(Lm61;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "Time"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object v0
.end method

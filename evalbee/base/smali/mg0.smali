.class public Lmg0;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:Landroid/content/Context;

.field public b:Ly01;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ly01;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lmg0;->b:Ly01;

    iput-object p1, p0, Lmg0;->a:Landroid/content/Context;

    invoke-virtual {p0}, Lmg0;->b()V

    return-void
.end method

.method public static synthetic a(Lmg0;Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/PendingJoinRequest;Lcom/ekodroid/omrevaluator/more/models/Teacher$MemberStatus;)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1, p2}, Lmg0;->c(Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/PendingJoinRequest;Lcom/ekodroid/omrevaluator/more/models/Teacher$MemberStatus;)V

    return-void
.end method


# virtual methods
.method public final b()V
    .locals 5

    .line 1
    new-instance v0, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    iget-object v1, p0, Lmg0;->a:Landroid/content/Context;

    const v2, 0x7f130151

    invoke-direct {v0, v1, v2}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;-><init>(Landroid/content/Context;I)V

    const v1, 0x7f120122

    invoke-virtual {v0, v1}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->setTitle(I)Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    iget-object v1, p0, Lmg0;->a:Landroid/content/Context;

    invoke-static {v1}, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;

    move-result-object v1

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;->getRequest()Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/PendingJoinRequest;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/PendingJoinRequest;->getSentByName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, " ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/PendingJoinRequest;->getSentByEmail()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, ") had invited you to join "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/PendingJoinRequest;->getOrgName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "\'s organization account as a Teacher"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->setMessage(Ljava/lang/CharSequence;)Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    new-instance v3, Lmg0$a;

    invoke-direct {v3, p0, v2}, Lmg0$a;-><init>(Lmg0;Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/PendingJoinRequest;)V

    const v4, 0x7f12001c

    invoke-virtual {v0, v4, v3}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    new-instance v3, Lmg0$b;

    invoke-direct {v3, p0, v2}, Lmg0$b;-><init>(Lmg0;Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/PendingJoinRequest;)V

    const v2, 0x7f120093

    invoke-virtual {v0, v2, v3}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    invoke-virtual {v0}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->create()Landroidx/appcompat/app/a;

    move-result-object v2

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;->getRequest()Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/PendingJoinRequest;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-virtual {v2}, Lq6;->dismiss()V

    :cond_0
    invoke-virtual {v0}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->create()Landroidx/appcompat/app/a;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    return-void
.end method

.method public final c(Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/PendingJoinRequest;Lcom/ekodroid/omrevaluator/more/models/Teacher$MemberStatus;)V
    .locals 3

    .line 1
    new-instance v0, Landroid/app/ProgressDialog;

    iget-object v1, p0, Lmg0;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    const-string v1, "Please wait, updating request..."

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    new-instance v1, Li61;

    new-instance v2, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/PendingJoinRequestDto;

    invoke-direct {v2, p1, p2}, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/PendingJoinRequestDto;-><init>(Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/PendingJoinRequest;Lcom/ekodroid/omrevaluator/more/models/Teacher$MemberStatus;)V

    iget-object p1, p0, Lmg0;->a:Landroid/content/Context;

    new-instance p2, Lmg0$c;

    invoke-direct {p2, p0, v0}, Lmg0$c;-><init>(Lmg0;Landroid/app/ProgressDialog;)V

    invoke-direct {v1, v2, p1, p2}, Li61;-><init>(Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/PendingJoinRequestDto;Landroid/content/Context;Lzg;)V

    invoke-virtual {v1}, Li61;->g()V

    return-void
.end method

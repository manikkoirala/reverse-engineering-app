.class public Lw82$a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lw82;->a(Landroid/content/Context;Ljava/util/UUID;Lp70;)Lik0;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Lum1;

.field public final synthetic b:Ljava/util/UUID;

.field public final synthetic c:Lp70;

.field public final synthetic d:Landroid/content/Context;

.field public final synthetic e:Lw82;


# direct methods
.method public constructor <init>(Lw82;Lum1;Ljava/util/UUID;Lp70;Landroid/content/Context;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lw82$a;->e:Lw82;

    iput-object p2, p0, Lw82$a;->a:Lum1;

    iput-object p3, p0, Lw82$a;->b:Ljava/util/UUID;

    iput-object p4, p0, Lw82$a;->c:Lp70;

    iput-object p5, p0, Lw82$a;->d:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .line 1
    :try_start_0
    iget-object v0, p0, Lw82$a;->a:Lum1;

    invoke-virtual {v0}, Landroidx/work/impl/utils/futures/AbstractFuture;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lw82$a;->b:Ljava/util/UUID;

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lw82$a;->e:Lw82;

    iget-object v1, v1, Lw82;->c:Lq92;

    invoke-interface {v1, v0}, Lq92;->s(Ljava/lang/String;)Lp92;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v2, v1, Lp92;->b:Landroidx/work/WorkInfo$State;

    invoke-virtual {v2}, Landroidx/work/WorkInfo$State;->isFinished()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lw82$a;->e:Lw82;

    iget-object v2, v2, Lw82;->b:Lq70;

    iget-object v3, p0, Lw82$a;->c:Lp70;

    invoke-interface {v2, v0, v3}, Lq70;->a(Ljava/lang/String;Lp70;)V

    iget-object v0, p0, Lw82$a;->d:Landroid/content/Context;

    invoke-static {v1}, Lu92;->a(Lp92;)Lx82;

    move-result-object v1

    iget-object v2, p0, Lw82$a;->c:Lp70;

    invoke-static {v0, v1, v2}, Landroidx/work/impl/foreground/a;->e(Landroid/content/Context;Lx82;Lp70;)Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Lw82$a;->d:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_0

    :cond_0
    const-string v0, "Calls to setForegroundAsync() must complete before a ListenableWorker signals completion of work by returning an instance of Result."

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    :goto_0
    iget-object v0, p0, Lw82$a;->a:Lum1;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lum1;->o(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lw82$a;->a:Lum1;

    invoke-virtual {v1, v0}, Lum1;->p(Ljava/lang/Throwable;)Z

    :goto_1
    return-void
.end method

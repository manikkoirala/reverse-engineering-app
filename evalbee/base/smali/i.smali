.class public final Li;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lj;


# instance fields
.field public final a:Lcm0;

.field public final b:Lcm0;

.field public final c:Lcm0;

.field public final d:Lcm0;

.field public final e:Lcm0;

.field public final f:Lcm0;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lcom/google/common/cache/LongAddables;->a()Lcm0;

    move-result-object v0

    iput-object v0, p0, Li;->a:Lcm0;

    invoke-static {}, Lcom/google/common/cache/LongAddables;->a()Lcm0;

    move-result-object v0

    iput-object v0, p0, Li;->b:Lcm0;

    invoke-static {}, Lcom/google/common/cache/LongAddables;->a()Lcm0;

    move-result-object v0

    iput-object v0, p0, Li;->c:Lcm0;

    invoke-static {}, Lcom/google/common/cache/LongAddables;->a()Lcm0;

    move-result-object v0

    iput-object v0, p0, Li;->d:Lcm0;

    invoke-static {}, Lcom/google/common/cache/LongAddables;->a()Lcm0;

    move-result-object v0

    iput-object v0, p0, Li;->e:Lcm0;

    invoke-static {}, Lcom/google/common/cache/LongAddables;->a()Lcm0;

    move-result-object v0

    iput-object v0, p0, Li;->f:Lcm0;

    return-void
.end method

.method public static h(J)J
    .locals 2

    .line 1
    const-wide/16 v0, 0x0

    cmp-long v0, p0, v0

    if-ltz v0, :cond_0

    goto :goto_0

    :cond_0
    const-wide p0, 0x7fffffffffffffffL

    :goto_0
    return-wide p0
.end method


# virtual methods
.method public a(I)V
    .locals 3

    .line 1
    iget-object v0, p0, Li;->a:Lcm0;

    int-to-long v1, p1

    invoke-interface {v0, v1, v2}, Lcm0;->add(J)V

    return-void
.end method

.method public b()V
    .locals 1

    .line 1
    iget-object v0, p0, Li;->f:Lcm0;

    invoke-interface {v0}, Lcm0;->increment()V

    return-void
.end method

.method public c(J)V
    .locals 1

    .line 1
    iget-object v0, p0, Li;->c:Lcm0;

    invoke-interface {v0}, Lcm0;->increment()V

    iget-object v0, p0, Li;->e:Lcm0;

    invoke-interface {v0, p1, p2}, Lcm0;->add(J)V

    return-void
.end method

.method public d(I)V
    .locals 3

    .line 1
    iget-object v0, p0, Li;->b:Lcm0;

    int-to-long v1, p1

    invoke-interface {v0, v1, v2}, Lcm0;->add(J)V

    return-void
.end method

.method public e(J)V
    .locals 1

    .line 1
    iget-object v0, p0, Li;->d:Lcm0;

    invoke-interface {v0}, Lcm0;->increment()V

    iget-object v0, p0, Li;->e:Lcm0;

    invoke-interface {v0, p1, p2}, Lcm0;->add(J)V

    return-void
.end method

.method public f()Lpe;
    .locals 14

    .line 1
    new-instance v13, Lpe;

    iget-object v0, p0, Li;->a:Lcm0;

    invoke-interface {v0}, Lcm0;->sum()J

    move-result-wide v0

    invoke-static {v0, v1}, Li;->h(J)J

    move-result-wide v1

    iget-object v0, p0, Li;->b:Lcm0;

    invoke-interface {v0}, Lcm0;->sum()J

    move-result-wide v3

    invoke-static {v3, v4}, Li;->h(J)J

    move-result-wide v3

    iget-object v0, p0, Li;->c:Lcm0;

    invoke-interface {v0}, Lcm0;->sum()J

    move-result-wide v5

    invoke-static {v5, v6}, Li;->h(J)J

    move-result-wide v5

    iget-object v0, p0, Li;->d:Lcm0;

    invoke-interface {v0}, Lcm0;->sum()J

    move-result-wide v7

    invoke-static {v7, v8}, Li;->h(J)J

    move-result-wide v7

    iget-object v0, p0, Li;->e:Lcm0;

    invoke-interface {v0}, Lcm0;->sum()J

    move-result-wide v9

    invoke-static {v9, v10}, Li;->h(J)J

    move-result-wide v9

    iget-object v0, p0, Li;->f:Lcm0;

    invoke-interface {v0}, Lcm0;->sum()J

    move-result-wide v11

    invoke-static {v11, v12}, Li;->h(J)J

    move-result-wide v11

    move-object v0, v13

    invoke-direct/range {v0 .. v12}, Lpe;-><init>(JJJJJJ)V

    return-object v13
.end method

.method public g(Lj;)V
    .locals 3

    .line 1
    invoke-interface {p1}, Lj;->f()Lpe;

    move-result-object p1

    iget-object v0, p0, Li;->a:Lcm0;

    invoke-virtual {p1}, Lpe;->b()J

    move-result-wide v1

    invoke-interface {v0, v1, v2}, Lcm0;->add(J)V

    iget-object v0, p0, Li;->b:Lcm0;

    invoke-virtual {p1}, Lpe;->e()J

    move-result-wide v1

    invoke-interface {v0, v1, v2}, Lcm0;->add(J)V

    iget-object v0, p0, Li;->c:Lcm0;

    invoke-virtual {p1}, Lpe;->d()J

    move-result-wide v1

    invoke-interface {v0, v1, v2}, Lcm0;->add(J)V

    iget-object v0, p0, Li;->d:Lcm0;

    invoke-virtual {p1}, Lpe;->c()J

    move-result-wide v1

    invoke-interface {v0, v1, v2}, Lcm0;->add(J)V

    iget-object v0, p0, Li;->e:Lcm0;

    invoke-virtual {p1}, Lpe;->f()J

    move-result-wide v1

    invoke-interface {v0, v1, v2}, Lcm0;->add(J)V

    iget-object v0, p0, Li;->f:Lcm0;

    invoke-virtual {p1}, Lpe;->a()J

    move-result-wide v1

    invoke-interface {v0, v1, v2}, Lcm0;->add(J)V

    return-void
.end method

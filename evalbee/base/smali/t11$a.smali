.class public final Lt11$a;
.super Ln92$a;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lt11;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# direct methods
.method public constructor <init>(Ljava/lang/Class;)V
    .locals 1

    .line 1
    const-string v0, "workerClass"

    invoke-static {p1, v0}, Lfg0;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Ln92$a;-><init>(Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic c()Ln92;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lt11$a;->l()Lt11;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic g()Ln92$a;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lt11$a;->m()Lt11$a;

    move-result-object v0

    return-object v0
.end method

.method public l()Lt11;
    .locals 2

    .line 1
    invoke-virtual {p0}, Ln92$a;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Ln92$a;->h()Lp92;

    move-result-object v0

    iget-object v0, v0, Lp92;->j:Lzk;

    invoke-virtual {v0}, Lzk;->h()Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_2

    new-instance v0, Lt11;

    invoke-direct {v0, p0}, Lt11;-><init>(Lt11$a;)V

    return-object v0

    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Cannot set backoff criteria on an idle mode job"

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public m()Lt11$a;
    .locals 0

    .line 1
    return-object p0
.end method

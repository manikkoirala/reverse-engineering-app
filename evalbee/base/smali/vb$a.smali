.class public final Lvb$a;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lvb;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# instance fields
.field public a:Z

.field public b:I

.field public c:Lzu1;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-static {v0}, Lvb;->e(Ljava/util/Locale;)Z

    move-result v0

    invoke-virtual {p0, v0}, Lvb$a;->c(Z)V

    return-void
.end method

.method public static b(Z)Lvb;
    .locals 0

    .line 1
    if-eqz p0, :cond_0

    sget-object p0, Lvb;->h:Lvb;

    goto :goto_0

    :cond_0
    sget-object p0, Lvb;->g:Lvb;

    :goto_0
    return-object p0
.end method


# virtual methods
.method public a()Lvb;
    .locals 4

    .line 1
    iget v0, p0, Lvb$a;->b:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lvb$a;->c:Lzu1;

    sget-object v1, Lvb;->d:Lzu1;

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lvb$a;->a:Z

    invoke-static {v0}, Lvb$a;->b(Z)Lvb;

    move-result-object v0

    return-object v0

    :cond_0
    new-instance v0, Lvb;

    iget-boolean v1, p0, Lvb$a;->a:Z

    iget v2, p0, Lvb$a;->b:I

    iget-object v3, p0, Lvb$a;->c:Lzu1;

    invoke-direct {v0, v1, v2, v3}, Lvb;-><init>(ZILzu1;)V

    return-object v0
.end method

.method public final c(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lvb$a;->a:Z

    sget-object p1, Lvb;->d:Lzu1;

    iput-object p1, p0, Lvb$a;->c:Lzu1;

    const/4 p1, 0x2

    iput p1, p0, Lvb$a;->b:I

    return-void
.end method

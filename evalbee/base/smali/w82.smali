.class public Lw82;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lr70;


# static fields
.field public static final d:Ljava/lang/String;


# instance fields
.field public final a:Lhu1;

.field public final b:Lq70;

.field public final c:Lq92;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .line 1
    const-string v0, "WMFgUpdater"

    invoke-static {v0}, Lxl0;->i(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lw82;->d:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroidx/work/impl/WorkDatabase;Lq70;Lhu1;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lw82;->b:Lq70;

    iput-object p3, p0, Lw82;->a:Lhu1;

    invoke-virtual {p1}, Landroidx/work/impl/WorkDatabase;->K()Lq92;

    move-result-object p1

    iput-object p1, p0, Lw82;->c:Lq92;

    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;Ljava/util/UUID;Lp70;)Lik0;
    .locals 9

    .line 1
    invoke-static {}, Lum1;->s()Lum1;

    move-result-object v6

    iget-object v7, p0, Lw82;->a:Lhu1;

    new-instance v8, Lw82$a;

    move-object v0, v8

    move-object v1, p0

    move-object v2, v6

    move-object v3, p2

    move-object v4, p3

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lw82$a;-><init>(Lw82;Lum1;Ljava/util/UUID;Lp70;Landroid/content/Context;)V

    invoke-interface {v7, v8}, Lhu1;->b(Ljava/lang/Runnable;)V

    return-object v6
.end method

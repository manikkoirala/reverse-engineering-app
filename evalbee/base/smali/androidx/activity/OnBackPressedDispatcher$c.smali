.class public final Landroidx/activity/OnBackPressedDispatcher$c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcf;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroidx/activity/OnBackPressedDispatcher;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "c"
.end annotation


# instance fields
.field public final a:Lh11;

.field public final synthetic b:Landroidx/activity/OnBackPressedDispatcher;


# direct methods
.method public constructor <init>(Landroidx/activity/OnBackPressedDispatcher;Lh11;)V
    .locals 1

    .line 1
    const-string v0, "onBackPressedCallback"

    invoke-static {p2, v0}, Lfg0;->e(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Landroidx/activity/OnBackPressedDispatcher$c;->b:Landroidx/activity/OnBackPressedDispatcher;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Landroidx/activity/OnBackPressedDispatcher$c;->a:Lh11;

    return-void
.end method


# virtual methods
.method public cancel()V
    .locals 2

    iget-object v0, p0, Landroidx/activity/OnBackPressedDispatcher$c;->b:Landroidx/activity/OnBackPressedDispatcher;

    invoke-static {v0}, Landroidx/activity/OnBackPressedDispatcher;->b(Landroidx/activity/OnBackPressedDispatcher;)Lj8;

    move-result-object v0

    iget-object v1, p0, Landroidx/activity/OnBackPressedDispatcher$c;->a:Lh11;

    invoke-virtual {v0, v1}, Lj8;->remove(Ljava/lang/Object;)Z

    iget-object v0, p0, Landroidx/activity/OnBackPressedDispatcher$c;->b:Landroidx/activity/OnBackPressedDispatcher;

    invoke-static {v0}, Landroidx/activity/OnBackPressedDispatcher;->a(Landroidx/activity/OnBackPressedDispatcher;)Lh11;

    move-result-object v0

    iget-object v1, p0, Landroidx/activity/OnBackPressedDispatcher$c;->a:Lh11;

    invoke-static {v0, v1}, Lfg0;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroidx/activity/OnBackPressedDispatcher$c;->a:Lh11;

    invoke-virtual {v0}, Lh11;->c()V

    iget-object v0, p0, Landroidx/activity/OnBackPressedDispatcher$c;->b:Landroidx/activity/OnBackPressedDispatcher;

    invoke-static {v0, v1}, Landroidx/activity/OnBackPressedDispatcher;->f(Landroidx/activity/OnBackPressedDispatcher;Lh11;)V

    :cond_0
    iget-object v0, p0, Landroidx/activity/OnBackPressedDispatcher$c;->a:Lh11;

    invoke-virtual {v0, p0}, Lh11;->i(Lcf;)V

    iget-object v0, p0, Landroidx/activity/OnBackPressedDispatcher$c;->a:Lh11;

    invoke-virtual {v0}, Lh11;->b()La90;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {v0}, La90;->invoke()Ljava/lang/Object;

    :cond_1
    iget-object v0, p0, Landroidx/activity/OnBackPressedDispatcher$c;->a:Lh11;

    invoke-virtual {v0, v1}, Lh11;->k(La90;)V

    return-void
.end method

.class public final Landroidx/activity/OnBackPressedDispatcher$b$a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/window/OnBackAnimationCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Landroidx/activity/OnBackPressedDispatcher$b;->a(Lc90;Lc90;La90;La90;)Landroid/window/OnBackInvokedCallback;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic a:Lc90;

.field public final synthetic b:Lc90;

.field public final synthetic c:La90;

.field public final synthetic d:La90;


# direct methods
.method public constructor <init>(Lc90;Lc90;La90;La90;)V
    .locals 0

    .line 1
    iput-object p1, p0, Landroidx/activity/OnBackPressedDispatcher$b$a;->a:Lc90;

    iput-object p2, p0, Landroidx/activity/OnBackPressedDispatcher$b$a;->b:Lc90;

    iput-object p3, p0, Landroidx/activity/OnBackPressedDispatcher$b$a;->c:La90;

    iput-object p4, p0, Landroidx/activity/OnBackPressedDispatcher$b$a;->d:La90;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onBackCancelled()V
    .locals 1

    iget-object v0, p0, Landroidx/activity/OnBackPressedDispatcher$b$a;->d:La90;

    invoke-interface {v0}, La90;->invoke()Ljava/lang/Object;

    return-void
.end method

.method public onBackInvoked()V
    .locals 1

    iget-object v0, p0, Landroidx/activity/OnBackPressedDispatcher$b$a;->c:La90;

    invoke-interface {v0}, La90;->invoke()Ljava/lang/Object;

    return-void
.end method

.method public onBackProgressed(Landroid/window/BackEvent;)V
    .locals 2

    const-string v0, "backEvent"

    invoke-static {p1, v0}, Lfg0;->e(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Landroidx/activity/OnBackPressedDispatcher$b$a;->b:Lc90;

    new-instance v1, Lxa;

    invoke-direct {v1, p1}, Lxa;-><init>(Landroid/window/BackEvent;)V

    invoke-interface {v0, v1}, Lc90;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public onBackStarted(Landroid/window/BackEvent;)V
    .locals 2

    const-string v0, "backEvent"

    invoke-static {p1, v0}, Lfg0;->e(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Landroidx/activity/OnBackPressedDispatcher$b$a;->a:Lc90;

    new-instance v1, Lxa;

    invoke-direct {v1, p1}, Lxa;-><init>(Landroid/window/BackEvent;)V

    invoke-interface {v0, v1}, Lc90;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.class public interface abstract Landroidx/lifecycle/o$b;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroidx/lifecycle/o;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "b"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroidx/lifecycle/o$b$a;
    }
.end annotation


# static fields
.field public static final a:Landroidx/lifecycle/o$b$a;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    sget-object v0, Landroidx/lifecycle/o$b$a;->a:Landroidx/lifecycle/o$b$a;

    sput-object v0, Landroidx/lifecycle/o$b;->a:Landroidx/lifecycle/o$b$a;

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Class;Ldo;)Ly32;
    .locals 1

    .line 1
    const-string v0, "modelClass"

    invoke-static {p1, v0}, Lfg0;->e(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "extras"

    invoke-static {p2, v0}, Lfg0;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p0, p1}, Landroidx/lifecycle/o$b;->b(Ljava/lang/Class;)Ly32;

    move-result-object p1

    return-object p1
.end method

.method public b(Ljava/lang/Class;)Ly32;
    .locals 1

    .line 1
    const-string v0, "modelClass"

    invoke-static {p1, v0}, Lfg0;->e(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance p1, Ljava/lang/UnsupportedOperationException;

    const-string v0, "Factory.create(String) is unsupported.  This Factory requires `CreationExtras` to be passed into `create` method."

    invoke-direct {p1, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.class public Landroidx/lifecycle/o;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroidx/lifecycle/o$a;,
        Landroidx/lifecycle/o$b;,
        Landroidx/lifecycle/o$c;,
        Landroidx/lifecycle/o$d;
    }
.end annotation


# instance fields
.field public final a:Lb42;

.field public final b:Landroidx/lifecycle/o$b;

.field public final c:Ldo;


# direct methods
.method public constructor <init>(Lb42;Landroidx/lifecycle/o$b;)V
    .locals 7

    .line 1
    const-string v0, "store"

    invoke-static {p1, v0}, Lfg0;->e(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "factory"

    invoke-static {p2, v0}, Lfg0;->e(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v4, 0x0

    const/4 v5, 0x4

    const/4 v6, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    invoke-direct/range {v1 .. v6}, Landroidx/lifecycle/o;-><init>(Lb42;Landroidx/lifecycle/o$b;Ldo;ILgq;)V

    return-void
.end method

.method public constructor <init>(Lb42;Landroidx/lifecycle/o$b;Ldo;)V
    .locals 1

    .line 2
    const-string v0, "store"

    invoke-static {p1, v0}, Lfg0;->e(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "factory"

    invoke-static {p2, v0}, Lfg0;->e(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "defaultCreationExtras"

    invoke-static {p3, v0}, Lfg0;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Landroidx/lifecycle/o;->a:Lb42;

    iput-object p2, p0, Landroidx/lifecycle/o;->b:Landroidx/lifecycle/o$b;

    iput-object p3, p0, Landroidx/lifecycle/o;->c:Ldo;

    return-void
.end method

.method public synthetic constructor <init>(Lb42;Landroidx/lifecycle/o$b;Ldo;ILgq;)V
    .locals 0

    .line 3
    and-int/lit8 p4, p4, 0x4

    if-eqz p4, :cond_0

    sget-object p3, Ldo$a;->b:Ldo$a;

    :cond_0
    invoke-direct {p0, p1, p2, p3}, Landroidx/lifecycle/o;-><init>(Lb42;Landroidx/lifecycle/o$b;Ldo;)V

    return-void
.end method

.method public constructor <init>(Lc42;Landroidx/lifecycle/o$b;)V
    .locals 1

    .line 4
    const-string v0, "owner"

    invoke-static {p1, v0}, Lfg0;->e(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "factory"

    invoke-static {p2, v0}, Lfg0;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-interface {p1}, Lc42;->getViewModelStore()Lb42;

    move-result-object v0

    invoke-static {p1}, La42;->a(Lc42;)Ldo;

    move-result-object p1

    invoke-direct {p0, v0, p2, p1}, Landroidx/lifecycle/o;-><init>(Lb42;Landroidx/lifecycle/o$b;Ldo;)V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Class;)Ly32;
    .locals 3

    .line 1
    const-string v0, "modelClass"

    invoke-static {p1, v0}, Lfg0;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "androidx.lifecycle.ViewModelProvider.DefaultKey:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p1}, Landroidx/lifecycle/o;->b(Ljava/lang/String;Ljava/lang/Class;)Ly32;

    move-result-object p1

    return-object p1

    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Local and anonymous classes can not be ViewModels"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public b(Ljava/lang/String;Ljava/lang/Class;)Ly32;
    .locals 2

    .line 1
    const-string v0, "key"

    invoke-static {p1, v0}, Lfg0;->e(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "modelClass"

    invoke-static {p2, v0}, Lfg0;->e(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Landroidx/lifecycle/o;->a:Lb42;

    invoke-virtual {v0, p1}, Lb42;->b(Ljava/lang/String;)Ly32;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object p1, p0, Landroidx/lifecycle/o;->b:Landroidx/lifecycle/o$b;

    instance-of p2, p1, Landroidx/lifecycle/o$d;

    if-eqz p2, :cond_0

    check-cast p1, Landroidx/lifecycle/o$d;

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    if-eqz p1, :cond_1

    invoke-static {v0}, Lfg0;->b(Ljava/lang/Object;)V

    invoke-virtual {p1, v0}, Landroidx/lifecycle/o$d;->c(Ly32;)V

    :cond_1
    const-string p1, "null cannot be cast to non-null type T of androidx.lifecycle.ViewModelProvider.get"

    invoke-static {v0, p1}, Lfg0;->c(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0

    :cond_2
    new-instance v0, Lsx0;

    iget-object v1, p0, Landroidx/lifecycle/o;->c:Ldo;

    invoke-direct {v0, v1}, Lsx0;-><init>(Ldo;)V

    sget-object v1, Landroidx/lifecycle/o$c;->d:Ldo$b;

    invoke-virtual {v0, v1, p1}, Lsx0;->c(Ldo$b;Ljava/lang/Object;)V

    :try_start_0
    iget-object v1, p0, Landroidx/lifecycle/o;->b:Landroidx/lifecycle/o$b;

    invoke-interface {v1, p2, v0}, Landroidx/lifecycle/o$b;->a(Ljava/lang/Class;Ldo;)Ly32;

    move-result-object p2
    :try_end_0
    .catch Ljava/lang/AbstractMethodError; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    iget-object v0, p0, Landroidx/lifecycle/o;->b:Landroidx/lifecycle/o$b;

    invoke-interface {v0, p2}, Landroidx/lifecycle/o$b;->b(Ljava/lang/Class;)Ly32;

    move-result-object p2

    :goto_1
    iget-object v0, p0, Landroidx/lifecycle/o;->a:Lb42;

    invoke-virtual {v0, p1, p2}, Lb42;->d(Ljava/lang/String;Ly32;)V

    return-object p2
.end method

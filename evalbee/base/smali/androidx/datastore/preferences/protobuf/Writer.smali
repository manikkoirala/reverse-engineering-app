.class public interface abstract Landroidx/datastore/preferences/protobuf/Writer;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroidx/datastore/preferences/protobuf/Writer$FieldOrder;
    }
.end annotation


# virtual methods
.method public abstract A(ILjava/util/List;Z)V
.end method

.method public abstract B()Landroidx/datastore/preferences/protobuf/Writer$FieldOrder;
.end method

.method public abstract C(IJ)V
.end method

.method public abstract D(ILjava/util/List;Z)V
.end method

.method public abstract E(ILjava/util/List;Z)V
.end method

.method public abstract F(IF)V
.end method

.method public abstract G(II)V
.end method

.method public abstract H(ILjava/util/List;Z)V
.end method

.method public abstract I(II)V
.end method

.method public abstract J(ILandroidx/datastore/preferences/protobuf/ByteString;)V
.end method

.method public abstract K(ILjava/lang/Object;Landroidx/datastore/preferences/protobuf/f0;)V
.end method

.method public abstract L(ILandroidx/datastore/preferences/protobuf/v$a;Ljava/util/Map;)V
.end method

.method public abstract M(ILjava/util/List;Landroidx/datastore/preferences/protobuf/f0;)V
.end method

.method public abstract N(ILjava/lang/Object;Landroidx/datastore/preferences/protobuf/f0;)V
.end method

.method public abstract O(ILjava/util/List;Landroidx/datastore/preferences/protobuf/f0;)V
.end method

.method public abstract a(ILjava/util/List;Z)V
.end method

.method public abstract b(ILjava/lang/Object;)V
.end method

.method public abstract c(II)V
.end method

.method public abstract d(ILjava/lang/String;)V
.end method

.method public abstract e(IJ)V
.end method

.method public abstract f(ILjava/util/List;Z)V
.end method

.method public abstract g(II)V
.end method

.method public abstract h(ILjava/util/List;Z)V
.end method

.method public abstract i(ILjava/util/List;Z)V
.end method

.method public abstract j(IJ)V
.end method

.method public abstract k(II)V
.end method

.method public abstract l(ILjava/util/List;Z)V
.end method

.method public abstract m(IJ)V
.end method

.method public abstract n(IZ)V
.end method

.method public abstract o(II)V
.end method

.method public abstract p(I)V
.end method

.method public abstract q(ILjava/util/List;Z)V
.end method

.method public abstract r(I)V
.end method

.method public abstract s(ILjava/util/List;Z)V
.end method

.method public abstract t(ILjava/util/List;Z)V
.end method

.method public abstract u(ILjava/util/List;)V
.end method

.method public abstract v(ILjava/util/List;)V
.end method

.method public abstract w(IJ)V
.end method

.method public abstract x(ILjava/util/List;Z)V
.end method

.method public abstract y(ILjava/util/List;Z)V
.end method

.method public abstract z(ID)V
.end method

.class public final Landroidx/datastore/preferences/a;
.super Landroidx/datastore/preferences/protobuf/GeneratedMessageLite;
.source "SourceFile"

# interfaces
.implements Lyv0;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroidx/datastore/preferences/a$a;
    }
.end annotation


# static fields
.field private static final DEFAULT_INSTANCE:Landroidx/datastore/preferences/a;

.field private static volatile PARSER:Lc31; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lc31;"
        }
    .end annotation
.end field

.field public static final STRINGS_FIELD_NUMBER:I = 0x1


# instance fields
.field private strings_:Landroidx/datastore/preferences/protobuf/r$d;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroidx/datastore/preferences/protobuf/r$d;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Landroidx/datastore/preferences/a;

    invoke-direct {v0}, Landroidx/datastore/preferences/a;-><init>()V

    sput-object v0, Landroidx/datastore/preferences/a;->DEFAULT_INSTANCE:Landroidx/datastore/preferences/a;

    const-class v1, Landroidx/datastore/preferences/a;

    invoke-static {v1, v0}, Landroidx/datastore/preferences/protobuf/GeneratedMessageLite;->H(Ljava/lang/Class;Landroidx/datastore/preferences/protobuf/GeneratedMessageLite;)V

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroidx/datastore/preferences/protobuf/GeneratedMessageLite;-><init>()V

    invoke-static {}, Landroidx/datastore/preferences/protobuf/GeneratedMessageLite;->v()Landroidx/datastore/preferences/protobuf/r$d;

    move-result-object v0

    iput-object v0, p0, Landroidx/datastore/preferences/a;->strings_:Landroidx/datastore/preferences/protobuf/r$d;

    return-void
.end method

.method public static synthetic J()Landroidx/datastore/preferences/a;
    .locals 1

    .line 1
    sget-object v0, Landroidx/datastore/preferences/a;->DEFAULT_INSTANCE:Landroidx/datastore/preferences/a;

    return-object v0
.end method

.method public static synthetic K(Landroidx/datastore/preferences/a;Ljava/lang/Iterable;)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Landroidx/datastore/preferences/a;->L(Ljava/lang/Iterable;)V

    return-void
.end method

.method public static N()Landroidx/datastore/preferences/a;
    .locals 1

    .line 1
    sget-object v0, Landroidx/datastore/preferences/a;->DEFAULT_INSTANCE:Landroidx/datastore/preferences/a;

    return-object v0
.end method

.method public static P()Landroidx/datastore/preferences/a$a;
    .locals 1

    .line 1
    sget-object v0, Landroidx/datastore/preferences/a;->DEFAULT_INSTANCE:Landroidx/datastore/preferences/a;

    invoke-virtual {v0}, Landroidx/datastore/preferences/protobuf/GeneratedMessageLite;->r()Landroidx/datastore/preferences/protobuf/GeneratedMessageLite$a;

    move-result-object v0

    check-cast v0, Landroidx/datastore/preferences/a$a;

    return-object v0
.end method


# virtual methods
.method public final L(Ljava/lang/Iterable;)V
    .locals 1

    .line 1
    invoke-virtual {p0}, Landroidx/datastore/preferences/a;->M()V

    iget-object v0, p0, Landroidx/datastore/preferences/a;->strings_:Landroidx/datastore/preferences/protobuf/r$d;

    invoke-static {p1, v0}, Landroidx/datastore/preferences/protobuf/a;->h(Ljava/lang/Iterable;Ljava/util/List;)V

    return-void
.end method

.method public final M()V
    .locals 1

    .line 1
    iget-object v0, p0, Landroidx/datastore/preferences/a;->strings_:Landroidx/datastore/preferences/protobuf/r$d;

    invoke-interface {v0}, Landroidx/datastore/preferences/protobuf/r$d;->h()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Landroidx/datastore/preferences/a;->strings_:Landroidx/datastore/preferences/protobuf/r$d;

    invoke-static {v0}, Landroidx/datastore/preferences/protobuf/GeneratedMessageLite;->C(Landroidx/datastore/preferences/protobuf/r$d;)Landroidx/datastore/preferences/protobuf/r$d;

    move-result-object v0

    iput-object v0, p0, Landroidx/datastore/preferences/a;->strings_:Landroidx/datastore/preferences/protobuf/r$d;

    :cond_0
    return-void
.end method

.method public O()Ljava/util/List;
    .locals 1

    .line 1
    iget-object v0, p0, Landroidx/datastore/preferences/a;->strings_:Landroidx/datastore/preferences/protobuf/r$d;

    return-object v0
.end method

.method public final u(Landroidx/datastore/preferences/protobuf/GeneratedMessageLite$MethodToInvoke;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 1
    sget-object p2, Lw71;->a:[I

    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result p1

    aget p1, p2, p1

    const/4 p2, 0x0

    packed-switch p1, :pswitch_data_0

    new-instance p1, Ljava/lang/UnsupportedOperationException;

    invoke-direct {p1}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw p1

    :pswitch_0
    return-object p2

    :pswitch_1
    const/4 p1, 0x1

    invoke-static {p1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object p1

    return-object p1

    :pswitch_2
    sget-object p1, Landroidx/datastore/preferences/a;->PARSER:Lc31;

    if-nez p1, :cond_1

    const-class p2, Landroidx/datastore/preferences/a;

    monitor-enter p2

    :try_start_0
    sget-object p1, Landroidx/datastore/preferences/a;->PARSER:Lc31;

    if-nez p1, :cond_0

    new-instance p1, Landroidx/datastore/preferences/protobuf/GeneratedMessageLite$b;

    sget-object p3, Landroidx/datastore/preferences/a;->DEFAULT_INSTANCE:Landroidx/datastore/preferences/a;

    invoke-direct {p1, p3}, Landroidx/datastore/preferences/protobuf/GeneratedMessageLite$b;-><init>(Landroidx/datastore/preferences/protobuf/GeneratedMessageLite;)V

    sput-object p1, Landroidx/datastore/preferences/a;->PARSER:Lc31;

    :cond_0
    monitor-exit p2

    goto :goto_0

    :catchall_0
    move-exception p1

    monitor-exit p2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1

    :cond_1
    :goto_0
    return-object p1

    :pswitch_3
    sget-object p1, Landroidx/datastore/preferences/a;->DEFAULT_INSTANCE:Landroidx/datastore/preferences/a;

    return-object p1

    :pswitch_4
    const-string p1, "strings_"

    filled-new-array {p1}, [Ljava/lang/Object;

    move-result-object p1

    const-string p2, "\u0001\u0001\u0000\u0000\u0001\u0001\u0001\u0000\u0001\u0000\u0001\u001a"

    sget-object p3, Landroidx/datastore/preferences/a;->DEFAULT_INSTANCE:Landroidx/datastore/preferences/a;

    invoke-static {p3, p2, p1}, Landroidx/datastore/preferences/protobuf/GeneratedMessageLite;->E(Landroidx/datastore/preferences/protobuf/y;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    return-object p1

    :pswitch_5
    new-instance p1, Landroidx/datastore/preferences/a$a;

    invoke-direct {p1, p2}, Landroidx/datastore/preferences/a$a;-><init>(Lw71;)V

    return-object p1

    :pswitch_6
    new-instance p1, Landroidx/datastore/preferences/a;

    invoke-direct {p1}, Landroidx/datastore/preferences/a;-><init>()V

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.class public final Landroidx/work/a;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroidx/work/a$a;,
        Landroidx/work/a$b;
    }
.end annotation


# static fields
.field public static final p:Landroidx/work/a$b;


# instance fields
.field public final a:Ljava/util/concurrent/Executor;

.field public final b:Ljava/util/concurrent/Executor;

.field public final c:Lch;

.field public final d:Laa2;

.field public final e:Llf0;

.field public final f:Lag1;

.field public final g:Ldl;

.field public final h:Ldl;

.field public final i:Ljava/lang/String;

.field public final j:I

.field public final k:I

.field public final l:I

.field public final m:I

.field public final n:I

.field public final o:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Landroidx/work/a$b;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Landroidx/work/a$b;-><init>(Lgq;)V

    sput-object v0, Landroidx/work/a;->p:Landroidx/work/a$b;

    return-void
.end method

.method public constructor <init>(Landroidx/work/a$a;)V
    .locals 3

    const-string v0, "builder"

    invoke-static {p1, v0}, Lfg0;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroidx/work/a$a;->e()Ljava/util/concurrent/Executor;

    move-result-object v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    invoke-static {v1}, Lik;->a(Z)Ljava/util/concurrent/Executor;

    move-result-object v0

    :cond_0
    iput-object v0, p0, Landroidx/work/a;->a:Ljava/util/concurrent/Executor;

    invoke-virtual {p1}, Landroidx/work/a$a;->n()Ljava/util/concurrent/Executor;

    move-result-object v0

    const/4 v2, 0x1

    if-nez v0, :cond_1

    move v1, v2

    :cond_1
    iput-boolean v1, p0, Landroidx/work/a;->o:Z

    invoke-virtual {p1}, Landroidx/work/a$a;->n()Ljava/util/concurrent/Executor;

    move-result-object v0

    if-nez v0, :cond_2

    invoke-static {v2}, Lik;->a(Z)Ljava/util/concurrent/Executor;

    move-result-object v0

    :cond_2
    iput-object v0, p0, Landroidx/work/a;->b:Ljava/util/concurrent/Executor;

    invoke-virtual {p1}, Landroidx/work/a$a;->b()Lch;

    move-result-object v0

    if-nez v0, :cond_3

    new-instance v0, Ldt1;

    invoke-direct {v0}, Ldt1;-><init>()V

    :cond_3
    iput-object v0, p0, Landroidx/work/a;->c:Lch;

    invoke-virtual {p1}, Landroidx/work/a$a;->o()Laa2;

    move-result-object v0

    if-nez v0, :cond_4

    invoke-static {}, Laa2;->c()Laa2;

    move-result-object v0

    const-string v1, "getDefaultWorkerFactory()"

    invoke-static {v0, v1}, Lfg0;->d(Ljava/lang/Object;Ljava/lang/String;)V

    :cond_4
    iput-object v0, p0, Landroidx/work/a;->d:Laa2;

    invoke-virtual {p1}, Landroidx/work/a$a;->g()Llf0;

    move-result-object v0

    if-nez v0, :cond_5

    sget-object v0, Llz0;->a:Llz0;

    :cond_5
    iput-object v0, p0, Landroidx/work/a;->e:Llf0;

    invoke-virtual {p1}, Landroidx/work/a$a;->l()Lag1;

    move-result-object v0

    if-nez v0, :cond_6

    new-instance v0, Lxq;

    invoke-direct {v0}, Lxq;-><init>()V

    :cond_6
    iput-object v0, p0, Landroidx/work/a;->f:Lag1;

    invoke-virtual {p1}, Landroidx/work/a$a;->h()I

    move-result v0

    iput v0, p0, Landroidx/work/a;->j:I

    invoke-virtual {p1}, Landroidx/work/a$a;->k()I

    move-result v0

    iput v0, p0, Landroidx/work/a;->k:I

    invoke-virtual {p1}, Landroidx/work/a$a;->i()I

    move-result v0

    iput v0, p0, Landroidx/work/a;->l:I

    invoke-virtual {p1}, Landroidx/work/a$a;->j()I

    move-result v0

    iput v0, p0, Landroidx/work/a;->n:I

    invoke-virtual {p1}, Landroidx/work/a$a;->f()Ldl;

    move-result-object v0

    iput-object v0, p0, Landroidx/work/a;->g:Ldl;

    invoke-virtual {p1}, Landroidx/work/a$a;->m()Ldl;

    move-result-object v0

    iput-object v0, p0, Landroidx/work/a;->h:Ldl;

    invoke-virtual {p1}, Landroidx/work/a$a;->d()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Landroidx/work/a;->i:Ljava/lang/String;

    invoke-virtual {p1}, Landroidx/work/a$a;->c()I

    move-result p1

    iput p1, p0, Landroidx/work/a;->m:I

    return-void
.end method


# virtual methods
.method public final a()Lch;
    .locals 1

    .line 1
    iget-object v0, p0, Landroidx/work/a;->c:Lch;

    return-object v0
.end method

.method public final b()I
    .locals 1

    .line 1
    iget v0, p0, Landroidx/work/a;->m:I

    return v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Landroidx/work/a;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Ljava/util/concurrent/Executor;
    .locals 1

    .line 1
    iget-object v0, p0, Landroidx/work/a;->a:Ljava/util/concurrent/Executor;

    return-object v0
.end method

.method public final e()Ldl;
    .locals 1

    .line 1
    iget-object v0, p0, Landroidx/work/a;->g:Ldl;

    return-object v0
.end method

.method public final f()Llf0;
    .locals 1

    .line 1
    iget-object v0, p0, Landroidx/work/a;->e:Llf0;

    return-object v0
.end method

.method public final g()I
    .locals 1

    .line 1
    iget v0, p0, Landroidx/work/a;->l:I

    return v0
.end method

.method public final h()I
    .locals 1

    .line 1
    iget v0, p0, Landroidx/work/a;->n:I

    return v0
.end method

.method public final i()I
    .locals 1

    .line 1
    iget v0, p0, Landroidx/work/a;->k:I

    return v0
.end method

.method public final j()I
    .locals 1

    .line 1
    iget v0, p0, Landroidx/work/a;->j:I

    return v0
.end method

.method public final k()Lag1;
    .locals 1

    .line 1
    iget-object v0, p0, Landroidx/work/a;->f:Lag1;

    return-object v0
.end method

.method public final l()Ldl;
    .locals 1

    .line 1
    iget-object v0, p0, Landroidx/work/a;->h:Ldl;

    return-object v0
.end method

.method public final m()Ljava/util/concurrent/Executor;
    .locals 1

    .line 1
    iget-object v0, p0, Landroidx/work/a;->b:Ljava/util/concurrent/Executor;

    return-object v0
.end method

.method public final n()Laa2;
    .locals 1

    .line 1
    iget-object v0, p0, Landroidx/work/a;->d:Laa2;

    return-object v0
.end method

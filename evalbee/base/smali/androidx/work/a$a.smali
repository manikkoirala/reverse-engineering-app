.class public final Landroidx/work/a$a;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroidx/work/a;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# instance fields
.field public a:Ljava/util/concurrent/Executor;

.field public b:Laa2;

.field public c:Llf0;

.field public d:Ljava/util/concurrent/Executor;

.field public e:Lch;

.field public f:Lag1;

.field public g:Ldl;

.field public h:Ldl;

.field public i:Ljava/lang/String;

.field public j:I

.field public k:I

.field public l:I

.field public m:I

.field public n:I


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x4

    iput v0, p0, Landroidx/work/a$a;->j:I

    const v0, 0x7fffffff

    iput v0, p0, Landroidx/work/a$a;->l:I

    const/16 v0, 0x14

    iput v0, p0, Landroidx/work/a$a;->m:I

    invoke-static {}, Lik;->c()I

    move-result v0

    iput v0, p0, Landroidx/work/a$a;->n:I

    return-void
.end method


# virtual methods
.method public final a()Landroidx/work/a;
    .locals 1

    .line 1
    new-instance v0, Landroidx/work/a;

    invoke-direct {v0, p0}, Landroidx/work/a;-><init>(Landroidx/work/a$a;)V

    return-object v0
.end method

.method public final b()Lch;
    .locals 1

    .line 1
    iget-object v0, p0, Landroidx/work/a$a;->e:Lch;

    return-object v0
.end method

.method public final c()I
    .locals 1

    .line 1
    iget v0, p0, Landroidx/work/a$a;->n:I

    return v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Landroidx/work/a$a;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final e()Ljava/util/concurrent/Executor;
    .locals 1

    .line 1
    iget-object v0, p0, Landroidx/work/a$a;->a:Ljava/util/concurrent/Executor;

    return-object v0
.end method

.method public final f()Ldl;
    .locals 1

    .line 1
    iget-object v0, p0, Landroidx/work/a$a;->g:Ldl;

    return-object v0
.end method

.method public final g()Llf0;
    .locals 1

    .line 1
    iget-object v0, p0, Landroidx/work/a$a;->c:Llf0;

    return-object v0
.end method

.method public final h()I
    .locals 1

    .line 1
    iget v0, p0, Landroidx/work/a$a;->j:I

    return v0
.end method

.method public final i()I
    .locals 1

    .line 1
    iget v0, p0, Landroidx/work/a$a;->l:I

    return v0
.end method

.method public final j()I
    .locals 1

    .line 1
    iget v0, p0, Landroidx/work/a$a;->m:I

    return v0
.end method

.method public final k()I
    .locals 1

    .line 1
    iget v0, p0, Landroidx/work/a$a;->k:I

    return v0
.end method

.method public final l()Lag1;
    .locals 1

    .line 1
    iget-object v0, p0, Landroidx/work/a$a;->f:Lag1;

    return-object v0
.end method

.method public final m()Ldl;
    .locals 1

    .line 1
    iget-object v0, p0, Landroidx/work/a$a;->h:Ldl;

    return-object v0
.end method

.method public final n()Ljava/util/concurrent/Executor;
    .locals 1

    .line 1
    iget-object v0, p0, Landroidx/work/a$a;->d:Ljava/util/concurrent/Executor;

    return-object v0
.end method

.method public final o()Laa2;
    .locals 1

    .line 1
    iget-object v0, p0, Landroidx/work/a$a;->b:Laa2;

    return-object v0
.end method

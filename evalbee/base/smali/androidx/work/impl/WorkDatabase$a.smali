.class public final Landroidx/work/impl/WorkDatabase$a;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroidx/work/impl/WorkDatabase;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lgq;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Landroidx/work/impl/WorkDatabase$a;-><init>()V

    return-void
.end method

.method public static synthetic a(Landroid/content/Context;Lts1$b;)Lts1;
    .locals 0

    .line 1
    invoke-static {p0, p1}, Landroidx/work/impl/WorkDatabase$a;->c(Landroid/content/Context;Lts1$b;)Lts1;

    move-result-object p0

    return-object p0
.end method

.method public static final c(Landroid/content/Context;Lts1$b;)Lts1;
    .locals 1

    .line 1
    const-string v0, "$context"

    invoke-static {p0, v0}, Lfg0;->e(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "configuration"

    invoke-static {p1, v0}, Lfg0;->e(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lts1$b;->f:Lts1$b$b;

    invoke-virtual {v0, p0}, Lts1$b$b;->a(Landroid/content/Context;)Lts1$b$a;

    move-result-object p0

    iget-object v0, p1, Lts1$b;->b:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lts1$b$a;->d(Ljava/lang/String;)Lts1$b$a;

    move-result-object v0

    iget-object p1, p1, Lts1$b;->c:Lts1$a;

    invoke-virtual {v0, p1}, Lts1$b$a;->c(Lts1$a;)Lts1$b$a;

    move-result-object p1

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lts1$b$a;->e(Z)Lts1$b$a;

    move-result-object p1

    invoke-virtual {p1, v0}, Lts1$b$a;->a(Z)Lts1$b$a;

    new-instance p1, Lr80;

    invoke-direct {p1}, Lr80;-><init>()V

    invoke-virtual {p0}, Lts1$b$a;->b()Lts1$b;

    move-result-object p0

    invoke-virtual {p1, p0}, Lr80;->a(Lts1$b;)Lts1;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final b(Landroid/content/Context;Ljava/util/concurrent/Executor;Lch;Z)Landroidx/work/impl/WorkDatabase;
    .locals 4

    .line 1
    const-string v0, "context"

    invoke-static {p1, v0}, Lfg0;->e(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "queryExecutor"

    invoke-static {p2, v0}, Lfg0;->e(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "clock"

    invoke-static {p3, v0}, Lfg0;->e(Ljava/lang/Object;Ljava/lang/String;)V

    const-class v0, Landroidx/work/impl/WorkDatabase;

    if-eqz p4, :cond_0

    invoke-static {p1, v0}, Lpf1;->c(Landroid/content/Context;Ljava/lang/Class;)Landroidx/room/RoomDatabase$a;

    move-result-object p4

    invoke-virtual {p4}, Landroidx/room/RoomDatabase$a;->c()Landroidx/room/RoomDatabase$a;

    move-result-object p4

    goto :goto_0

    :cond_0
    const-string p4, "androidx.work.workdb"

    invoke-static {p1, v0, p4}, Lpf1;->a(Landroid/content/Context;Ljava/lang/Class;Ljava/lang/String;)Landroidx/room/RoomDatabase$a;

    move-result-object p4

    new-instance v0, Lk82;

    invoke-direct {v0, p1}, Lk82;-><init>(Landroid/content/Context;)V

    invoke-virtual {p4, v0}, Landroidx/room/RoomDatabase$a;->f(Lts1$c;)Landroidx/room/RoomDatabase$a;

    move-result-object p4

    :goto_0
    invoke-virtual {p4, p2}, Landroidx/room/RoomDatabase$a;->g(Ljava/util/concurrent/Executor;)Landroidx/room/RoomDatabase$a;

    move-result-object p2

    new-instance p4, Lyg;

    invoke-direct {p4, p3}, Lyg;-><init>(Lch;)V

    invoke-virtual {p2, p4}, Landroidx/room/RoomDatabase$a;->a(Landroidx/room/RoomDatabase$b;)Landroidx/room/RoomDatabase$a;

    move-result-object p2

    const/4 p3, 0x1

    new-array p4, p3, [Lkw0;

    sget-object v0, Lpw0;->c:Lpw0;

    const/4 v1, 0x0

    aput-object v0, p4, v1

    invoke-virtual {p2, p4}, Landroidx/room/RoomDatabase$a;->b([Lkw0;)Landroidx/room/RoomDatabase$a;

    move-result-object p2

    new-array p4, p3, [Lkw0;

    new-instance v0, Lfe1;

    const/4 v2, 0x2

    const/4 v3, 0x3

    invoke-direct {v0, p1, v2, v3}, Lfe1;-><init>(Landroid/content/Context;II)V

    aput-object v0, p4, v1

    invoke-virtual {p2, p4}, Landroidx/room/RoomDatabase$a;->b([Lkw0;)Landroidx/room/RoomDatabase$a;

    move-result-object p2

    new-array p4, p3, [Lkw0;

    sget-object v0, Lqw0;->c:Lqw0;

    aput-object v0, p4, v1

    invoke-virtual {p2, p4}, Landroidx/room/RoomDatabase$a;->b([Lkw0;)Landroidx/room/RoomDatabase$a;

    move-result-object p2

    new-array p4, p3, [Lkw0;

    sget-object v0, Lrw0;->c:Lrw0;

    aput-object v0, p4, v1

    invoke-virtual {p2, p4}, Landroidx/room/RoomDatabase$a;->b([Lkw0;)Landroidx/room/RoomDatabase$a;

    move-result-object p2

    new-array p4, p3, [Lkw0;

    new-instance v0, Lfe1;

    const/4 v2, 0x5

    const/4 v3, 0x6

    invoke-direct {v0, p1, v2, v3}, Lfe1;-><init>(Landroid/content/Context;II)V

    aput-object v0, p4, v1

    invoke-virtual {p2, p4}, Landroidx/room/RoomDatabase$a;->b([Lkw0;)Landroidx/room/RoomDatabase$a;

    move-result-object p2

    new-array p4, p3, [Lkw0;

    sget-object v0, Lsw0;->c:Lsw0;

    aput-object v0, p4, v1

    invoke-virtual {p2, p4}, Landroidx/room/RoomDatabase$a;->b([Lkw0;)Landroidx/room/RoomDatabase$a;

    move-result-object p2

    new-array p4, p3, [Lkw0;

    sget-object v0, Ltw0;->c:Ltw0;

    aput-object v0, p4, v1

    invoke-virtual {p2, p4}, Landroidx/room/RoomDatabase$a;->b([Lkw0;)Landroidx/room/RoomDatabase$a;

    move-result-object p2

    new-array p4, p3, [Lkw0;

    sget-object v0, Luw0;->c:Luw0;

    aput-object v0, p4, v1

    invoke-virtual {p2, p4}, Landroidx/room/RoomDatabase$a;->b([Lkw0;)Landroidx/room/RoomDatabase$a;

    move-result-object p2

    new-array p4, p3, [Lkw0;

    new-instance v0, Le92;

    invoke-direct {v0, p1}, Le92;-><init>(Landroid/content/Context;)V

    aput-object v0, p4, v1

    invoke-virtual {p2, p4}, Landroidx/room/RoomDatabase$a;->b([Lkw0;)Landroidx/room/RoomDatabase$a;

    move-result-object p2

    new-array p4, p3, [Lkw0;

    new-instance v0, Lfe1;

    const/16 v2, 0xa

    const/16 v3, 0xb

    invoke-direct {v0, p1, v2, v3}, Lfe1;-><init>(Landroid/content/Context;II)V

    aput-object v0, p4, v1

    invoke-virtual {p2, p4}, Landroidx/room/RoomDatabase$a;->b([Lkw0;)Landroidx/room/RoomDatabase$a;

    move-result-object p1

    new-array p2, p3, [Lkw0;

    sget-object p4, Llw0;->c:Llw0;

    aput-object p4, p2, v1

    invoke-virtual {p1, p2}, Landroidx/room/RoomDatabase$a;->b([Lkw0;)Landroidx/room/RoomDatabase$a;

    move-result-object p1

    new-array p2, p3, [Lkw0;

    sget-object p4, Lmw0;->c:Lmw0;

    aput-object p4, p2, v1

    invoke-virtual {p1, p2}, Landroidx/room/RoomDatabase$a;->b([Lkw0;)Landroidx/room/RoomDatabase$a;

    move-result-object p1

    new-array p2, p3, [Lkw0;

    sget-object p4, Lnw0;->c:Lnw0;

    aput-object p4, p2, v1

    invoke-virtual {p1, p2}, Landroidx/room/RoomDatabase$a;->b([Lkw0;)Landroidx/room/RoomDatabase$a;

    move-result-object p1

    new-array p2, p3, [Lkw0;

    sget-object p3, Low0;->c:Low0;

    aput-object p3, p2, v1

    invoke-virtual {p1, p2}, Landroidx/room/RoomDatabase$a;->b([Lkw0;)Landroidx/room/RoomDatabase$a;

    move-result-object p1

    invoke-virtual {p1}, Landroidx/room/RoomDatabase$a;->e()Landroidx/room/RoomDatabase$a;

    move-result-object p1

    invoke-virtual {p1}, Landroidx/room/RoomDatabase$a;->d()Landroidx/room/RoomDatabase;

    move-result-object p1

    check-cast p1, Landroidx/work/impl/WorkDatabase;

    return-object p1
.end method

.class public abstract Landroidx/work/impl/constraints/controllers/ConstraintController;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Ltk;


# direct methods
.method public constructor <init>(Ltk;)V
    .locals 1

    .line 1
    const-string v0, "tracker"

    invoke-static {p1, v0}, Lfg0;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Landroidx/work/impl/constraints/controllers/ConstraintController;->a:Ltk;

    return-void
.end method

.method public static final synthetic a(Landroidx/work/impl/constraints/controllers/ConstraintController;)Ltk;
    .locals 0

    .line 1
    iget-object p0, p0, Landroidx/work/impl/constraints/controllers/ConstraintController;->a:Ltk;

    return-object p0
.end method


# virtual methods
.method public abstract b()I
.end method

.method public abstract c(Lp92;)Z
.end method

.method public final d(Lp92;)Z
    .locals 1

    .line 1
    const-string v0, "workSpec"

    invoke-static {p1, v0}, Lfg0;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, p1}, Landroidx/work/impl/constraints/controllers/ConstraintController;->c(Lp92;)Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Landroidx/work/impl/constraints/controllers/ConstraintController;->a:Ltk;

    invoke-virtual {p1}, Ltk;->e()Ljava/lang/Object;

    move-result-object p1

    invoke-virtual {p0, p1}, Landroidx/work/impl/constraints/controllers/ConstraintController;->e(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public abstract e(Ljava/lang/Object;)Z
.end method

.method public final f()Ly40;
    .locals 2

    .line 1
    new-instance v0, Landroidx/work/impl/constraints/controllers/ConstraintController$track$1;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Landroidx/work/impl/constraints/controllers/ConstraintController$track$1;-><init>(Landroidx/work/impl/constraints/controllers/ConstraintController;Lvl;)V

    invoke-static {v0}, Ld50;->c(Lq90;)Ly40;

    move-result-object v0

    return-object v0
.end method

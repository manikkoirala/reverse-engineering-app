.class public final Lzd2;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public volatile a:I

.field public final b:Lsb2;

.field public volatile c:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lsb2;)V
    .locals 1

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lzd2;->c:Z

    iput v0, p0, Lzd2;->a:I

    iput-object p2, p0, Lzd2;->b:Lsb2;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    check-cast p1, Landroid/app/Application;

    invoke-static {p1}, Lcom/google/android/gms/common/api/internal/BackgroundDetector;->initialize(Landroid/app/Application;)V

    invoke-static {}, Lcom/google/android/gms/common/api/internal/BackgroundDetector;->getInstance()Lcom/google/android/gms/common/api/internal/BackgroundDetector;

    move-result-object p1

    new-instance p2, Lee2;

    invoke-direct {p2, p0}, Lee2;-><init>(Lzd2;)V

    invoke-virtual {p1, p2}, Lcom/google/android/gms/common/api/internal/BackgroundDetector;->addListener(Lcom/google/android/gms/common/api/internal/BackgroundDetector$BackgroundStateChangeListener;)V

    return-void
.end method

.method public constructor <init>(Lr10;)V
    .locals 2

    .line 1
    invoke-virtual {p1}, Lr10;->l()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Lsb2;

    invoke-direct {v1, p1}, Lsb2;-><init>(Lr10;)V

    invoke-direct {p0, v0, v1}, Lzd2;-><init>(Landroid/content/Context;Lsb2;)V

    return-void
.end method

.method public static bridge synthetic a(Lzd2;)Lsb2;
    .locals 0

    .line 1
    iget-object p0, p0, Lzd2;->b:Lsb2;

    return-object p0
.end method

.method public static bridge synthetic e(Lzd2;Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lzd2;->c:Z

    return-void
.end method

.method public static bridge synthetic g(Lzd2;)Z
    .locals 0

    .line 1
    invoke-virtual {p0}, Lzd2;->f()Z

    move-result p0

    return p0
.end method


# virtual methods
.method public final b()V
    .locals 1

    .line 1
    iget-object v0, p0, Lzd2;->b:Lsb2;

    invoke-virtual {v0}, Lsb2;->b()V

    return-void
.end method

.method public final c(I)V
    .locals 1

    .line 1
    if-lez p1, :cond_0

    iget v0, p0, Lzd2;->a:I

    if-nez v0, :cond_0

    iput p1, p0, Lzd2;->a:I

    invoke-virtual {p0}, Lzd2;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lzd2;->b:Lsb2;

    invoke-virtual {v0}, Lsb2;->c()V

    goto :goto_0

    :cond_0
    if-nez p1, :cond_1

    iget v0, p0, Lzd2;->a:I

    if-eqz v0, :cond_1

    iget-object v0, p0, Lzd2;->b:Lsb2;

    invoke-virtual {v0}, Lsb2;->b()V

    :cond_1
    :goto_0
    iput p1, p0, Lzd2;->a:I

    return-void
.end method

.method public final d(Lcom/google/android/gms/internal/firebase-auth-api/zzafn;)V
    .locals 6

    .line 1
    if-nez p1, :cond_0

    return-void

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/gms/internal/firebase-auth-api/zzafn;->zza()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-gtz v2, :cond_1

    const-wide/16 v0, 0xe10

    :cond_1
    invoke-virtual {p1}, Lcom/google/android/gms/internal/firebase-auth-api/zzafn;->zzb()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    mul-long/2addr v0, v4

    add-long/2addr v2, v0

    iget-object p1, p0, Lzd2;->b:Lsb2;

    iput-wide v2, p1, Lsb2;->b:J

    const-wide/16 v0, -0x1

    iput-wide v0, p1, Lsb2;->c:J

    invoke-virtual {p0}, Lzd2;->f()Z

    move-result p1

    if-eqz p1, :cond_2

    iget-object p1, p0, Lzd2;->b:Lsb2;

    invoke-virtual {p1}, Lsb2;->c()V

    :cond_2
    return-void
.end method

.method public final f()Z
    .locals 1

    .line 1
    iget v0, p0, Lzd2;->a:I

    if-lez v0, :cond_0

    iget-boolean v0, p0, Lzd2;->c:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.class public Ltb0$a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Ltb0;->e()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Landroid/widget/TextView;

.field public final synthetic b:Landroid/widget/Button;

.field public final synthetic c:Landroid/widget/Spinner;

.field public final synthetic d:Landroid/widget/Spinner;

.field public final synthetic e:I

.field public final synthetic f:Ltb0;


# direct methods
.method public constructor <init>(Ltb0;Landroid/widget/TextView;Landroid/widget/Button;Landroid/widget/Spinner;Landroid/widget/Spinner;I)V
    .locals 0

    .line 1
    iput-object p1, p0, Ltb0$a;->f:Ltb0;

    iput-object p2, p0, Ltb0$a;->a:Landroid/widget/TextView;

    iput-object p3, p0, Ltb0$a;->b:Landroid/widget/Button;

    iput-object p4, p0, Ltb0$a;->c:Landroid/widget/Spinner;

    iput-object p5, p0, Ltb0$a;->d:Landroid/widget/Spinner;

    iput p6, p0, Ltb0$a;->e:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 0

    .line 1
    iget-object p1, p0, Ltb0$a;->a:Landroid/widget/TextView;

    const/16 p2, 0x8

    invoke-virtual {p1, p2}, Landroid/view/View;->setVisibility(I)V

    iget-object p1, p0, Ltb0$a;->b:Landroid/widget/Button;

    const/4 p2, 0x1

    invoke-virtual {p1, p2}, Landroid/view/View;->setEnabled(Z)V

    iget-object p1, p0, Ltb0$a;->b:Landroid/widget/Button;

    iget-object p3, p0, Ltb0$a;->f:Ltb0;

    iget-object p3, p3, Ltb0;->a:Landroid/content/Context;

    invoke-virtual {p3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p3

    const p4, 0x7f06004c

    invoke-virtual {p3, p4}, Landroid/content/res/Resources;->getColor(I)I

    move-result p3

    invoke-virtual {p1, p3}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object p1, p0, Ltb0$a;->f:Ltb0;

    iget-object p3, p0, Ltb0$a;->c:Landroid/widget/Spinner;

    invoke-virtual {p3}, Landroid/widget/AdapterView;->getSelectedItem()Ljava/lang/Object;

    move-result-object p3

    invoke-virtual {p3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p3

    invoke-static {p3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result p3

    invoke-static {p1, p3}, Ltb0;->b(Ltb0;I)I

    iget-object p1, p0, Ltb0$a;->f:Ltb0;

    iget-object p3, p0, Ltb0$a;->d:Landroid/widget/Spinner;

    invoke-virtual {p3}, Landroid/widget/AdapterView;->getSelectedItem()Ljava/lang/Object;

    move-result-object p3

    invoke-virtual {p3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p3

    invoke-static {p3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result p3

    invoke-static {p1, p3}, Ltb0;->d(Ltb0;I)I

    iget p1, p0, Ltb0$a;->e:I

    add-int/lit8 p1, p1, -0x5

    iget-object p3, p0, Ltb0$a;->f:Ltb0;

    invoke-static {p3}, Ltb0;->a(Ltb0;)I

    move-result p3

    iget-object p4, p0, Ltb0$a;->f:Ltb0;

    invoke-static {p4}, Ltb0;->c(Ltb0;)I

    move-result p4

    mul-int/lit8 p4, p4, 0x5

    add-int/2addr p4, p2

    mul-int/2addr p3, p4

    if-le p1, p3, :cond_0

    iget-object p1, p0, Ltb0$a;->a:Landroid/widget/TextView;

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Landroid/view/View;->setVisibility(I)V

    iget-object p1, p0, Ltb0$a;->b:Landroid/widget/Button;

    invoke-virtual {p1, p2}, Landroid/view/View;->setEnabled(Z)V

    iget-object p1, p0, Ltb0$a;->b:Landroid/widget/Button;

    iget-object p2, p0, Ltb0$a;->f:Ltb0;

    iget-object p2, p2, Ltb0;->a:Landroid/content/Context;

    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    const p3, 0x7f060044

    invoke-virtual {p2, p3}, Landroid/content/res/Resources;->getColor(I)I

    move-result p2

    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_0
    return-void
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0

    .line 1
    return-void
.end method

.class public Lcb0;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Landroid/content/SharedPreferences;

.field public c:Lee1;

.field public d:Ly01;

.field public e:Ljava/lang/String;

.field public f:Landroid/content/Context;

.field public g:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lee1;Ly01;Ljava/lang/String;)V
    .locals 2

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "https://"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, La91;->u()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ":8759/userDataProfile"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcb0;->a:Ljava/lang/String;

    const/4 v0, 0x0

    iput v0, p0, Lcb0;->g:I

    iput-object p2, p0, Lcb0;->c:Lee1;

    iput-object p3, p0, Lcb0;->d:Ly01;

    iput-object p4, p0, Lcb0;->e:Ljava/lang/String;

    iput-object p1, p0, Lcb0;->f:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    const-string p2, "MyPref"

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object p1

    iput-object p1, p0, Lcb0;->b:Landroid/content/SharedPreferences;

    invoke-virtual {p0}, Lcb0;->g()V

    return-void
.end method

.method public static synthetic a(Lcb0;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lcb0;->e:Ljava/lang/String;

    return-object p0
.end method

.method public static synthetic b(Lcb0;Ljava/lang/Object;)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcb0;->h(Ljava/lang/Object;)V

    return-void
.end method

.method public static synthetic c(Lcb0;)Landroid/content/Context;
    .locals 0

    .line 1
    iget-object p0, p0, Lcb0;->f:Landroid/content/Context;

    return-object p0
.end method

.method public static synthetic d(Lcb0;I)I
    .locals 0

    .line 1
    iput p1, p0, Lcb0;->g:I

    return p1
.end method

.method public static synthetic e(Lcb0;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lcb0;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final f(Ljava/lang/String;)Ljava/lang/String;
    .locals 23

    .line 1
    move-object/from16 v1, p0

    const-string v0, ""

    invoke-static {}, Lcom/google/firebase/auth/FirebaseAuth;->getInstance()Lcom/google/firebase/auth/FirebaseAuth;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/firebase/auth/FirebaseAuth;->e()Lr30;

    move-result-object v2

    const/4 v3, 0x0

    if-nez v2, :cond_0

    return-object v3

    :cond_0
    :try_start_0
    invoke-virtual {v2}, Lr30;->getEmail()Ljava/lang/String;

    move-result-object v4

    const-string v5, "emailKey"

    invoke-static {v4, v5}, Lb;->i(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2}, Lr30;->getDisplayName()Ljava/lang/String;

    move-result-object v16

    iget-object v2, v1, Lcb0;->b:Landroid/content/SharedPreferences;

    const-string v4, "count_saveScan"

    const/4 v5, 0x0

    invoke-interface {v2, v4, v5}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v8

    iget-object v2, v1, Lcb0;->b:Landroid/content/SharedPreferences;

    const-string v4, "count_cancelScan"

    invoke-interface {v2, v4, v5}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v9

    iget-object v2, v1, Lcb0;->b:Landroid/content/SharedPreferences;

    const-string v4, "count_email"

    invoke-interface {v2, v4, v5}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v10

    iget-object v2, v1, Lcb0;->b:Landroid/content/SharedPreferences;

    const-string v4, "count_sms"

    invoke-interface {v2, v4, v5}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v11

    iget-object v2, v1, Lcb0;->b:Landroid/content/SharedPreferences;

    const-string v4, "count_saveImportScan"

    invoke-interface {v2, v4, v5}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v13

    iget-object v2, v1, Lcb0;->b:Landroid/content/SharedPreferences;

    const-string v4, "count_errorImportScan"

    invoke-interface {v2, v4, v5}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v14

    iget-object v2, v1, Lcb0;->b:Landroid/content/SharedPreferences;

    const-string v4, "user_country"

    invoke-interface {v2, v4, v0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v15

    iget-object v2, v1, Lcb0;->b:Landroid/content/SharedPreferences;

    const-string v4, "user_city"

    invoke-interface {v2, v4, v0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v21

    iget-object v2, v1, Lcb0;->b:Landroid/content/SharedPreferences;

    const-string v4, "user_organization"

    invoke-interface {v2, v4, v0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v22

    sget-object v17, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, La91;->f()I

    move-result v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    iget-object v0, v1, Lcb0;->b:Landroid/content/SharedPreferences;

    const-string v2, "fcmId"

    const-string v4, " "

    invoke-interface {v0, v2, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    new-instance v0, Lcom/ekodroid/omrevaluator/serializable/UserAccount;

    invoke-static {}, La91;->n()I

    move-result v12

    move-object v6, v0

    move-object/from16 v20, p1

    invoke-direct/range {v6 .. v22}, Lcom/ekodroid/omrevaluator/serializable/UserAccount;-><init>(Ljava/lang/String;IIIIIIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    new-instance v2, Lgc0;

    invoke-direct {v2}, Lgc0;-><init>()V

    invoke-virtual {v2, v0}, Lgc0;->s(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    move-object/from16 v2, p1

    invoke-static {v0, v2}, Lb;->i(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    return-object v3
.end method

.method public final g()V
    .locals 7

    .line 1
    new-instance v6, Lcb0$c;

    const/4 v2, 0x1

    iget-object v3, p0, Lcb0;->a:Ljava/lang/String;

    new-instance v4, Lcb0$a;

    invoke-direct {v4, p0}, Lcb0$a;-><init>(Lcb0;)V

    new-instance v5, Lcb0$b;

    invoke-direct {v5, p0}, Lcb0$b;-><init>(Lcb0;)V

    move-object v0, v6

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcb0$c;-><init>(Lcb0;ILjava/lang/String;Lcom/android/volley/d$b;Lcom/android/volley/d$a;)V

    new-instance v0, Lwq;

    const/4 v1, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    const v3, 0x9c40

    invoke-direct {v0, v3, v1, v2}, Lwq;-><init>(IIF)V

    invoke-virtual {v6, v0}, Lcom/android/volley/Request;->L(Ljf1;)Lcom/android/volley/Request;

    iget-object v0, p0, Lcb0;->c:Lee1;

    invoke-virtual {v0, v6}, Lee1;->a(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    return-void
.end method

.method public final h(Ljava/lang/Object;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcb0;->d:Ly01;

    if-eqz v0, :cond_0

    invoke-interface {v0, p1}, Ly01;->a(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.class public final Ldu;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Comparable;


# static fields
.field public static final b:Ljava/util/Comparator;

.field public static final c:Lcom/google/firebase/database/collection/c;


# instance fields
.field public final a:Lke1;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .line 1
    new-instance v0, Lcu;

    invoke-direct {v0}, Lcu;-><init>()V

    sput-object v0, Ldu;->b:Ljava/util/Comparator;

    new-instance v1, Lcom/google/firebase/database/collection/c;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcom/google/firebase/database/collection/c;-><init>(Ljava/util/List;Ljava/util/Comparator;)V

    sput-object v1, Ldu;->c:Lcom/google/firebase/database/collection/c;

    return-void
.end method

.method public constructor <init>(Lke1;)V
    .locals 3

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Ldu;->o(Lke1;)Z

    move-result v0

    const-string v1, "Not a document key path: %s"

    filled-new-array {p1}, [Ljava/lang/Object;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lg9;->d(ZLjava/lang/String;[Ljava/lang/Object;)V

    iput-object p1, p0, Ldu;->a:Lke1;

    return-void
.end method

.method public static a()Ljava/util/Comparator;
    .locals 1

    .line 1
    sget-object v0, Ldu;->b:Ljava/util/Comparator;

    return-object v0
.end method

.method public static d()Ldu;
    .locals 1

    .line 1
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Ldu;->h(Ljava/util/List;)Ldu;

    move-result-object v0

    return-object v0
.end method

.method public static e()Lcom/google/firebase/database/collection/c;
    .locals 1

    .line 1
    sget-object v0, Ldu;->c:Lcom/google/firebase/database/collection/c;

    return-object v0
.end method

.method public static f(Ljava/lang/String;)Ldu;
    .locals 4

    .line 1
    invoke-static {p0}, Lke1;->q(Ljava/lang/String;)Lke1;

    move-result-object p0

    invoke-virtual {p0}, Ljb;->l()I

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x4

    if-le v0, v2, :cond_0

    invoke-virtual {p0, v1}, Ljb;->h(I)Ljava/lang/String;

    move-result-object v0

    const-string v3, "projects"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Ljb;->h(I)Ljava/lang/String;

    move-result-object v0

    const-string v3, "databases"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, v2}, Ljb;->h(I)Ljava/lang/String;

    move-result-object v0

    const-string v2, "documents"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    :cond_0
    const-string v0, "Tried to parse an invalid key: %s"

    filled-new-array {p0}, [Ljava/lang/Object;

    move-result-object v2

    invoke-static {v1, v0, v2}, Lg9;->d(ZLjava/lang/String;[Ljava/lang/Object;)V

    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Ljb;->m(I)Ljb;

    move-result-object p0

    check-cast p0, Lke1;

    invoke-static {p0}, Ldu;->g(Lke1;)Ldu;

    move-result-object p0

    return-object p0
.end method

.method public static g(Lke1;)Ldu;
    .locals 1

    .line 1
    new-instance v0, Ldu;

    invoke-direct {v0, p0}, Ldu;-><init>(Lke1;)V

    return-object v0
.end method

.method public static h(Ljava/util/List;)Ldu;
    .locals 1

    .line 1
    new-instance v0, Ldu;

    invoke-static {p0}, Lke1;->p(Ljava/util/List;)Lke1;

    move-result-object p0

    invoke-direct {v0, p0}, Ldu;-><init>(Lke1;)V

    return-object v0
.end method

.method public static o(Lke1;)Z
    .locals 0

    .line 1
    invoke-virtual {p0}, Ljb;->l()I

    move-result p0

    rem-int/lit8 p0, p0, 0x2

    if-nez p0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method


# virtual methods
.method public c(Ldu;)I
    .locals 1

    .line 1
    iget-object v0, p0, Ldu;->a:Lke1;

    iget-object p1, p1, Ldu;->a:Lke1;

    invoke-virtual {v0, p1}, Ljb;->e(Ljb;)I

    move-result p1

    return p1
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 0

    .line 1
    check-cast p1, Ldu;

    invoke-virtual {p0, p1}, Ldu;->c(Ldu;)I

    move-result p1

    return p1
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .line 1
    if-ne p0, p1, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_0
    if-eqz p1, :cond_2

    const-class v0, Ldu;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    if-eq v0, v1, :cond_1

    goto :goto_0

    :cond_1
    check-cast p1, Ldu;

    iget-object v0, p0, Ldu;->a:Lke1;

    iget-object p1, p1, Ldu;->a:Lke1;

    invoke-virtual {v0, p1}, Ljb;->equals(Ljava/lang/Object;)Z

    move-result p1

    return p1

    :cond_2
    :goto_0
    const/4 p1, 0x0

    return p1
.end method

.method public hashCode()I
    .locals 1

    .line 1
    iget-object v0, p0, Ldu;->a:Lke1;

    invoke-virtual {v0}, Ljb;->hashCode()I

    move-result v0

    return v0
.end method

.method public j()Ljava/lang/String;
    .locals 2

    .line 1
    iget-object v0, p0, Ldu;->a:Lke1;

    invoke-virtual {v0}, Ljb;->l()I

    move-result v1

    add-int/lit8 v1, v1, -0x2

    invoke-virtual {v0, v1}, Ljb;->h(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public k()Lke1;
    .locals 1

    .line 1
    iget-object v0, p0, Ldu;->a:Lke1;

    invoke-virtual {v0}, Ljb;->n()Ljb;

    move-result-object v0

    check-cast v0, Lke1;

    return-object v0
.end method

.method public l()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Ldu;->a:Lke1;

    invoke-virtual {v0}, Ljb;->g()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public m()Lke1;
    .locals 1

    .line 1
    iget-object v0, p0, Ldu;->a:Lke1;

    return-object v0
.end method

.method public n(Ljava/lang/String;)Z
    .locals 3

    .line 1
    iget-object v0, p0, Ldu;->a:Lke1;

    invoke-virtual {v0}, Ljb;->l()I

    move-result v0

    const/4 v1, 0x2

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Ldu;->a:Lke1;

    iget-object v2, v0, Ljb;->a:Ljava/util/List;

    invoke-virtual {v0}, Ljb;->l()I

    move-result v0

    sub-int/2addr v0, v1

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Ldu;->a:Lke1;

    invoke-virtual {v0}, Ljb;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

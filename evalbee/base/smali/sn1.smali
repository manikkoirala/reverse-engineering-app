.class public Lsn1;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;


# direct methods
.method public constructor <init>(Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lsn1;->a:Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;

    return-void
.end method


# virtual methods
.method public a(ILcom/ekodroid/omrevaluator/templateui/models/Point2Integer;III)[Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;
    .locals 7

    .line 1
    new-array v0, p3, [I

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, p3, :cond_0

    add-int/lit8 v3, p3, -0x1

    sub-int/2addr v3, v2

    int-to-double v3, v3

    const-wide/high16 v5, 0x4024000000000000L    # 10.0

    invoke-static {v5, v6, v3, v4}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v3

    double-to-int v3, v3

    div-int v4, p1, v3

    aput v4, v0, v2

    rem-int/2addr p1, v3

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {p0, p2, p3, p4}, Lsn1;->h(Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;II)[[Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    move-result-object p1

    new-array p2, p3, [Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    :goto_1
    if-ge v1, p3, :cond_1

    aget p4, v0, v1

    sub-int/2addr p4, p5

    add-int/lit8 p4, p4, 0xa

    rem-int/lit8 p4, p4, 0xa

    aget-object p4, p1, p4

    aget-object p4, p4, v1

    aput-object p4, p2, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_1
    return-object p2
.end method

.method public b(Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;[I)[Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;
    .locals 13

    .line 1
    sget-object v0, Lsn1$a;->a:[I

    iget-object v1, p1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->type:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x5

    const/4 v2, 0x3

    const/4 v3, 0x2

    const/16 v4, 0xa

    const/4 v5, 0x4

    const/4 v6, 0x0

    const/4 v7, 0x1

    packed-switch v0, :pswitch_data_0

    const/4 p1, 0x0

    return-object p1

    :pswitch_0
    new-instance v0, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    iget v1, p1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->column:I

    iget v2, p1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->row:I

    invoke-direct {v0, v1, v2}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;-><init>(II)V

    iget p1, p1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->column:I

    sub-int/2addr p1, v7

    aget p1, p2, p1

    invoke-virtual {p0, v0, v4, p1}, Lsn1;->c(Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;II)[Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    move-result-object p1

    return-object p1

    :pswitch_1
    new-instance v0, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    iget v1, p1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->column:I

    iget v2, p1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->row:I

    invoke-direct {v0, v1, v2}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;-><init>(II)V

    iget p1, p1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->column:I

    sub-int/2addr p1, v7

    aget p1, p2, p1

    const/16 p2, 0x8

    invoke-virtual {p0, v0, p2, p1}, Lsn1;->c(Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;II)[Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    move-result-object p1

    return-object p1

    :pswitch_2
    new-instance v0, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    iget v1, p1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->column:I

    iget v2, p1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->row:I

    invoke-direct {v0, v1, v2}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;-><init>(II)V

    iget p1, p1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->column:I

    sub-int/2addr p1, v7

    aget p1, p2, p1

    const/4 p2, 0x6

    invoke-virtual {p0, v0, p2, p1}, Lsn1;->c(Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;II)[Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    move-result-object p1

    return-object p1

    :pswitch_3
    iget-object v0, p1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->payload:Ljava/lang/String;

    invoke-static {v0}, Lbc;->b(Ljava/lang/String;)Lcom/ekodroid/omrevaluator/templateui/models/DecimalOptionPayload;

    move-result-object v0

    iget-boolean v1, v0, Lcom/ekodroid/omrevaluator/templateui/models/DecimalOptionPayload;->decimalAllowed:Z

    if-eqz v1, :cond_0

    const/16 v4, 0xb

    :cond_0
    iget v1, v0, Lcom/ekodroid/omrevaluator/templateui/models/DecimalOptionPayload;->digits:I

    mul-int/2addr v1, v4

    iget-boolean v2, v0, Lcom/ekodroid/omrevaluator/templateui/models/DecimalOptionPayload;->hasNegetive:Z

    if-eqz v2, :cond_1

    add-int/lit8 v1, v1, 0x1

    :cond_1
    new-array v2, v1, [Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    move v8, v6

    :goto_0
    if-ge v8, v4, :cond_3

    new-instance v9, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    iget v10, p1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->column:I

    iget v11, p1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->row:I

    add-int/2addr v11, v7

    add-int/2addr v11, v8

    invoke-direct {v9, v10, v11}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;-><init>(II)V

    iget v10, v0, Lcom/ekodroid/omrevaluator/templateui/models/DecimalOptionPayload;->digits:I

    iget v11, p1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->subIndex:I

    mul-int/2addr v11, v5

    add-int/2addr v10, v11

    iget v11, p1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->column:I

    sub-int/2addr v11, v7

    aget v11, p2, v11

    invoke-virtual {p0, v9, v10, v11}, Lsn1;->c(Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;II)[Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    move-result-object v9

    move v10, v6

    :goto_1
    iget v11, v0, Lcom/ekodroid/omrevaluator/templateui/models/DecimalOptionPayload;->digits:I

    if-ge v10, v11, :cond_2

    mul-int/2addr v11, v8

    add-int/2addr v11, v10

    iget v12, p1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->subIndex:I

    mul-int/2addr v12, v5

    add-int/2addr v12, v10

    aget-object v12, v9, v12

    aput-object v12, v2, v11

    add-int/lit8 v10, v10, 0x1

    goto :goto_1

    :cond_2
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    :cond_3
    iget-boolean v0, v0, Lcom/ekodroid/omrevaluator/templateui/models/DecimalOptionPayload;->hasNegetive:Z

    if-eqz v0, :cond_4

    new-instance v0, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    iget v4, p1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->column:I

    iget v6, p1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->row:I

    invoke-direct {v0, v4, v6}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;-><init>(II)V

    iget v4, p1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->subIndex:I

    mul-int/2addr v4, v5

    add-int/2addr v4, v3

    iget v3, p1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->column:I

    sub-int/2addr v3, v7

    aget p2, p2, v3

    invoke-virtual {p0, v0, v4, p2}, Lsn1;->c(Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;II)[Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    move-result-object p2

    sub-int/2addr v1, v7

    iget p1, p1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->subIndex:I

    mul-int/2addr p1, v5

    add-int/2addr p1, v7

    aget-object p1, p2, p1

    aput-object p1, v2, v1

    :cond_4
    return-object v2

    :pswitch_4
    iget-object v0, p1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->payload:Ljava/lang/String;

    invoke-static {v0}, Lbc;->d(Ljava/lang/String;)Lcom/ekodroid/omrevaluator/templateui/models/NumericalOptionPayload;

    move-result-object v0

    if-eqz v0, :cond_9

    iget v2, v0, Lcom/ekodroid/omrevaluator/templateui/models/NumericalOptionPayload;->digits:I

    if-gt v2, v7, :cond_5

    iget-boolean v5, v0, Lcom/ekodroid/omrevaluator/templateui/models/NumericalOptionPayload;->hasNegetive:Z

    if-eqz v5, :cond_9

    :cond_5
    mul-int/2addr v2, v4

    iget-boolean v1, v0, Lcom/ekodroid/omrevaluator/templateui/models/NumericalOptionPayload;->hasNegetive:Z

    if-eqz v1, :cond_6

    add-int/lit8 v2, v2, 0x1

    :cond_6
    new-array v1, v2, [Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    move v5, v6

    :goto_2
    if-ge v5, v4, :cond_8

    new-instance v8, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    iget v9, p1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->column:I

    iget v10, p1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->row:I

    add-int/2addr v10, v7

    add-int/2addr v10, v5

    invoke-direct {v8, v9, v10}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;-><init>(II)V

    iget v9, v0, Lcom/ekodroid/omrevaluator/templateui/models/NumericalOptionPayload;->digits:I

    iget v10, p1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->column:I

    sub-int/2addr v10, v7

    aget v10, p2, v10

    invoke-virtual {p0, v8, v9, v10}, Lsn1;->c(Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;II)[Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    move-result-object v8

    move v9, v6

    :goto_3
    array-length v10, v8

    if-ge v9, v10, :cond_7

    iget v10, v0, Lcom/ekodroid/omrevaluator/templateui/models/NumericalOptionPayload;->digits:I

    mul-int/2addr v10, v5

    add-int/2addr v10, v9

    aget-object v11, v8, v9

    aput-object v11, v1, v10

    add-int/lit8 v9, v9, 0x1

    goto :goto_3

    :cond_7
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    :cond_8
    iget-boolean v0, v0, Lcom/ekodroid/omrevaluator/templateui/models/NumericalOptionPayload;->hasNegetive:Z

    if-eqz v0, :cond_b

    new-instance v0, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    iget v4, p1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->column:I

    iget v5, p1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->row:I

    invoke-direct {v0, v4, v5}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;-><init>(II)V

    iget p1, p1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->column:I

    sub-int/2addr p1, v7

    aget p1, p2, p1

    invoke-virtual {p0, v0, v3, p1}, Lsn1;->c(Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;II)[Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    move-result-object p1

    sub-int/2addr v2, v7

    aget-object p1, p1, v7

    aput-object p1, v1, v2

    goto :goto_5

    :cond_9
    iget v0, p1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->subIndex:I

    new-array v2, v4, [Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    :goto_4
    if-ge v6, v4, :cond_a

    new-instance v3, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    iget v5, p1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->column:I

    iget v8, p1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->row:I

    add-int/2addr v8, v7

    add-int/2addr v8, v6

    invoke-direct {v3, v5, v8}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;-><init>(II)V

    iget v5, p1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->column:I

    sub-int/2addr v5, v7

    aget v5, p2, v5

    invoke-virtual {p0, v3, v1, v5}, Lsn1;->c(Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;II)[Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    move-result-object v3

    aget-object v3, v3, v0

    aput-object v3, v2, v6

    add-int/lit8 v6, v6, 0x1

    goto :goto_4

    :cond_a
    move-object v1, v2

    :cond_b
    :goto_5
    return-object v1

    :pswitch_5
    iget-object v0, p1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->payload:Ljava/lang/String;

    invoke-static {v0}, Lbc;->c(Ljava/lang/String;)Lcom/ekodroid/omrevaluator/templateui/models/MatrixOptionPayload;

    move-result-object v0

    if-eqz v0, :cond_c

    iget v5, v0, Lcom/ekodroid/omrevaluator/templateui/models/MatrixOptionPayload;->primaryOptions:I

    iget v1, v0, Lcom/ekodroid/omrevaluator/templateui/models/MatrixOptionPayload;->secondaryOptions:I

    :cond_c
    mul-int v0, v5, v1

    new-array v0, v0, [Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    move v2, v6

    :goto_6
    if-ge v2, v5, :cond_e

    new-instance v3, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    iget v4, p1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->column:I

    iget v8, p1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->row:I

    add-int/2addr v8, v7

    add-int/2addr v8, v2

    invoke-direct {v3, v4, v8}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;-><init>(II)V

    iget v4, p1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->column:I

    sub-int/2addr v4, v7

    aget v4, p2, v4

    invoke-virtual {p0, v3, v1, v4}, Lsn1;->c(Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;II)[Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    move-result-object v3

    move v4, v6

    :goto_7
    array-length v8, v3

    if-ge v4, v8, :cond_d

    mul-int v8, v2, v1

    add-int/2addr v8, v4

    aget-object v9, v3, v4

    aput-object v9, v0, v8

    add-int/lit8 v4, v4, 0x1

    goto :goto_7

    :cond_d
    add-int/lit8 v2, v2, 0x1

    goto :goto_6

    :cond_e
    return-object v0

    :pswitch_6
    new-instance v0, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    iget v1, p1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->column:I

    iget v4, p1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->row:I

    invoke-direct {v0, v1, v4}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;-><init>(II)V

    iget p1, p1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->column:I

    sub-int/2addr p1, v7

    aget p1, p2, p1

    invoke-virtual {p0, v0, v5, p1}, Lsn1;->c(Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;II)[Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    move-result-object p1

    new-array p2, v3, [Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    aget-object v0, p1, v7

    aput-object v0, p2, v6

    aget-object p1, p1, v2

    aput-object p1, p2, v7

    return-object p2

    :pswitch_7
    new-instance v0, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    iget v1, p1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->column:I

    iget v3, p1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->row:I

    invoke-direct {v0, v1, v3}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;-><init>(II)V

    iget p1, p1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->column:I

    sub-int/2addr p1, v7

    aget p1, p2, p1

    invoke-virtual {p0, v0, v2, p1}, Lsn1;->c(Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;II)[Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    move-result-object p1

    return-object p1

    :pswitch_8
    new-instance v0, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    iget v2, p1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->column:I

    iget v3, p1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->row:I

    invoke-direct {v0, v2, v3}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;-><init>(II)V

    iget p1, p1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->column:I

    sub-int/2addr p1, v7

    aget p1, p2, p1

    invoke-virtual {p0, v0, v1, p1}, Lsn1;->c(Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;II)[Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    move-result-object p1

    return-object p1

    :pswitch_9
    new-instance v0, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    iget v1, p1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->column:I

    iget v2, p1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->row:I

    invoke-direct {v0, v1, v2}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;-><init>(II)V

    iget p1, p1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->column:I

    sub-int/2addr p1, v7

    aget p1, p2, p1

    invoke-virtual {p0, v0, v5, p1}, Lsn1;->c(Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;II)[Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    move-result-object p1

    return-object p1

    :pswitch_a
    iget v0, p1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->subIndex:I

    new-array v1, v0, [Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    new-array v3, v3, [[Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    iget v4, p1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->column:I

    add-int/lit8 v5, v4, -0x1

    aget v5, p2, v5

    sub-int/2addr v5, v2

    new-instance v8, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    iget v9, p1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->row:I

    add-int/2addr v9, v7

    invoke-direct {v8, v4, v9}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;-><init>(II)V

    iget v4, p1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->column:I

    sub-int/2addr v4, v7

    aget v4, p2, v4

    invoke-virtual {p0, v8, v5, v4}, Lsn1;->c(Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;II)[Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    move-result-object v4

    aput-object v4, v3, v6

    if-le v0, v5, :cond_f

    new-instance v4, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    iget v8, p1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->column:I

    iget v9, p1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->row:I

    add-int/2addr v9, v2

    invoke-direct {v4, v8, v9}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;-><init>(II)V

    iget p1, p1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->column:I

    sub-int/2addr p1, v7

    aget p1, p2, p1

    invoke-virtual {p0, v4, v5, p1}, Lsn1;->c(Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;II)[Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    move-result-object p1

    aput-object p1, v3, v7

    :cond_f
    :goto_8
    if-ge v6, v0, :cond_10

    div-int p1, v6, v5

    aget-object p1, v3, p1

    rem-int p2, v6, v5

    aget-object p1, p1, p2

    aput-object p1, v1, v6

    add-int/lit8 v6, v6, 0x1

    goto :goto_8

    :cond_10
    return-object v1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public c(Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;II)[Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;
    .locals 8

    .line 1
    new-array v0, p2, [Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    invoke-virtual {p0, p1}, Lsn1;->e(Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;)[Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    move-result-object p1

    if-nez p1, :cond_0

    const/4 p1, 0x0

    return-object p1

    :cond_0
    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, p2, :cond_1

    aget-object v3, p1, v1

    const/4 v4, 0x1

    aget-object v5, p1, v4

    mul-int/lit8 v6, v2, 0x2

    add-int/lit8 v6, v6, 0x5

    mul-int/lit8 v7, p3, 0x2

    add-int/2addr v7, v4

    invoke-static {v3, v5, v6, v7}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->g(Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;II)Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    move-result-object v3

    aput-object v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method public d(Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;)Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;
    .locals 9

    .line 1
    new-instance v0, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    iget v1, p1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->column:I

    iget v2, p1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->row:I

    invoke-direct {v0, v1, v2}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;-><init>(II)V

    invoke-virtual {p0, v0}, Lsn1;->e(Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;)[Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    move-result-object v0

    const/4 v1, 0x1

    aget-object v1, v0, v1

    iget-wide v2, v1, Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;->x:D

    const/4 v4, 0x0

    aget-object v0, v0, v4

    iget-wide v4, v0, Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;->x:D

    sub-double/2addr v2, v4

    const-wide/high16 v6, 0x4010000000000000L    # 4.0

    div-double/2addr v2, v6

    new-instance v6, Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    add-double/2addr v4, v2

    iget p1, p1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->subIndex:I

    int-to-double v7, p1

    mul-double/2addr v7, v2

    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    mul-double/2addr v7, v2

    add-double/2addr v4, v7

    iget-wide v7, v0, Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;->y:D

    iget-wide v0, v1, Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;->y:D

    add-double/2addr v7, v0

    div-double/2addr v7, v2

    invoke-direct {v6, v4, v5, v7, v8}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;-><init>(DD)V

    return-object v6
.end method

.method public final e(Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;)[Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;
    .locals 12

    .line 1
    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->getX()I

    move-result v0

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->getY()I

    move-result p1

    add-int/lit8 v1, p1, -0x1

    rem-int/lit8 v2, v1, 0x5

    iget-object v3, p0, Lsn1;->a:Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;

    iget-object v3, v3, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;->sheetMarkers:[[Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    array-length v4, v3

    add-int/lit8 v4, v4, -0x1

    const/4 v5, 0x5

    mul-int/2addr v4, v5

    add-int/lit8 v4, v4, 0x1

    div-int/2addr v1, v5

    if-ne p1, v4, :cond_0

    add-int/lit8 p1, v1, -0x1

    aget-object p1, v3, p1

    add-int/lit8 v2, v0, -0x1

    aget-object v4, p1, v2

    aget-object p1, p1, v0

    aget-object v1, v3, v1

    aget-object v2, v1, v2

    aget-object v0, v1, v0

    move-object v8, p1

    move-object v10, v0

    move-object v9, v2

    move-object v7, v4

    move v11, v5

    goto :goto_0

    :cond_0
    aget-object p1, v3, v1

    add-int/lit8 v4, v0, -0x1

    aget-object v5, p1, v4

    aget-object p1, p1, v0

    add-int/lit8 v1, v1, 0x1

    aget-object v1, v3, v1

    aget-object v3, v1, v4

    aget-object v0, v1, v0

    move-object v8, p1

    move-object v10, v0

    move v11, v2

    move-object v9, v3

    move-object v7, v5

    :goto_0
    move-object v6, p0

    invoke-virtual/range {v6 .. v11}, Lsn1;->g(Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;I)[Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    move-result-object p1

    return-object p1
.end method

.method public f()Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;
    .locals 1

    .line 1
    iget-object v0, p0, Lsn1;->a:Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;

    return-object v0
.end method

.method public final g(Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;I)[Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;
    .locals 2

    .line 1
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    const/4 v1, 0x6

    invoke-static {p1, p3, p5, v1}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->g(Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;II)Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    move-result-object p1

    const/4 p3, 0x0

    aput-object p1, v0, p3

    const/4 p1, 0x1

    invoke-static {p2, p4, p5, v1}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->g(Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;II)Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    move-result-object p2

    aput-object p2, v0, p1

    return-object v0
.end method

.method public h(Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;II)[[Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;
    .locals 3

    .line 1
    const/16 v0, 0xa

    filled-new-array {v0, p2}, [I

    move-result-object v0

    const-class v1, Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    invoke-static {v1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    const/4 v1, 0x0

    :goto_0
    array-length v2, v0

    if-ge v1, v2, :cond_0

    add-int/lit8 v2, v1, 0x2

    invoke-virtual {p1, v2}, Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;->addy(I)Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;

    move-result-object v2

    invoke-virtual {p0, v2, p2, p3}, Lsn1;->c(Lcom/ekodroid/omrevaluator/templateui/models/Point2Integer;II)[Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    move-result-object v2

    aput-object v2, v0, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-object v0
.end method

.class public Lbn;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:Lr10;

.field public final c:Ldp;

.field public final d:Lm11;

.field public final e:J

.field public f:Lcn;

.field public g:Lcn;

.field public h:Z

.field public i:Lzm;

.field public final j:Lde0;

.field public final k:Lz00;

.field public final l:Lsc;

.field public final m:Lm4;

.field public final n:Ljava/util/concurrent/ExecutorService;

.field public final o:Lxm;

.field public final p:Lwm;

.field public final q:Ldn;

.field public final r:Lhd1;


# direct methods
.method public constructor <init>(Lr10;Lde0;Ldn;Ldp;Lsc;Lm4;Lz00;Ljava/util/concurrent/ExecutorService;Lwm;Lhd1;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lbn;->b:Lr10;

    iput-object p4, p0, Lbn;->c:Ldp;

    invoke-virtual {p1}, Lr10;->l()Landroid/content/Context;

    move-result-object p1

    iput-object p1, p0, Lbn;->a:Landroid/content/Context;

    iput-object p2, p0, Lbn;->j:Lde0;

    iput-object p3, p0, Lbn;->q:Ldn;

    iput-object p5, p0, Lbn;->l:Lsc;

    iput-object p6, p0, Lbn;->m:Lm4;

    iput-object p8, p0, Lbn;->n:Ljava/util/concurrent/ExecutorService;

    iput-object p7, p0, Lbn;->k:Lz00;

    new-instance p1, Lxm;

    invoke-direct {p1, p8}, Lxm;-><init>(Ljava/util/concurrent/Executor;)V

    iput-object p1, p0, Lbn;->o:Lxm;

    iput-object p9, p0, Lbn;->p:Lwm;

    iput-object p10, p0, Lbn;->r:Lhd1;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide p1

    iput-wide p1, p0, Lbn;->e:J

    new-instance p1, Lm11;

    invoke-direct {p1}, Lm11;-><init>()V

    iput-object p1, p0, Lbn;->d:Lm11;

    return-void
.end method

.method public static synthetic a(Lbn;Lzm1;)Lcom/google/android/gms/tasks/Task;
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lbn;->f(Lzm1;)Lcom/google/android/gms/tasks/Task;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic b(Lbn;)Lcn;
    .locals 0

    .line 1
    iget-object p0, p0, Lbn;->f:Lcn;

    return-object p0
.end method

.method public static synthetic c(Lbn;)Lzm;
    .locals 0

    .line 1
    iget-object p0, p0, Lbn;->i:Lzm;

    return-object p0
.end method

.method public static i()Ljava/lang/String;
    .locals 1

    .line 1
    const-string v0, "18.6.0"

    return-object v0
.end method

.method public static j(Ljava/lang/String;Z)Z
    .locals 2

    .line 1
    const/4 v0, 0x1

    if-nez p1, :cond_0

    invoke-static {}, Lzl0;->f()Lzl0;

    move-result-object p0

    const-string p1, "Configured not to require a build ID."

    invoke-virtual {p0, p1}, Lzl0;->i(Ljava/lang/String;)V

    return v0

    :cond_0
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result p0

    if-nez p0, :cond_1

    return v0

    :cond_1
    const-string p0, "FirebaseCrashlytics"

    const-string p1, "."

    invoke-static {p0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, ".     |  | "

    invoke-static {p0, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, ".     |  |"

    invoke-static {p0, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {p0, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, ".   \\ |  | /"

    invoke-static {p0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, ".    \\    /"

    invoke-static {p0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, ".     \\  /"

    invoke-static {p0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, ".      \\/"

    invoke-static {p0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {p0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "The Crashlytics build ID is missing. This occurs when the Crashlytics Gradle plugin is missing from your app\'s build configuration. Please review the Firebase Crashlytics onboarding instructions at https://firebase.google.com/docs/crashlytics/get-started?platform=android#add-plugin"

    invoke-static {p0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {p0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, ".      /\\"

    invoke-static {p0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, ".     /  \\"

    invoke-static {p0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, ".    /    \\"

    invoke-static {p0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, ".   / |  | \\"

    invoke-static {p0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {p0, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {p0, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {p0, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {p0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 p0, 0x0

    return p0
.end method


# virtual methods
.method public final d()V
    .locals 2

    .line 1
    iget-object v0, p0, Lbn;->o:Lxm;

    new-instance v1, Lbn$d;

    invoke-direct {v1, p0}, Lbn$d;-><init>(Lbn;)V

    invoke-virtual {v0, v1}, Lxm;->g(Ljava/util/concurrent/Callable;)Lcom/google/android/gms/tasks/Task;

    move-result-object v0

    :try_start_0
    invoke-static {v0}, Lv22;->f(Lcom/google/android/gms/tasks/Task;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v1, v0}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v0

    :goto_0
    iput-boolean v0, p0, Lbn;->h:Z

    return-void

    :catch_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lbn;->f:Lcn;

    invoke-virtual {v0}, Lcn;->c()Z

    move-result v0

    return v0
.end method

.method public final f(Lzm1;)Lcom/google/android/gms/tasks/Task;
    .locals 3

    .line 1
    const-string v0, "Collection of crash reports disabled in Crashlytics settings."

    invoke-virtual {p0}, Lbn;->m()V

    :try_start_0
    iget-object v1, p0, Lbn;->l:Lsc;

    new-instance v2, Lan;

    invoke-direct {v2, p0}, Lan;-><init>(Lbn;)V

    invoke-interface {v1, v2}, Lsc;->a(Lrc;)V

    iget-object v1, p0, Lbn;->i:Lzm;

    invoke-virtual {v1}, Lzm;->S()V

    invoke-interface {p1}, Lzm1;->a()Lvm1;

    move-result-object v1

    iget-object v1, v1, Lvm1;->b:Lvm1$a;

    iget-boolean v1, v1, Lvm1$a;->a:Z

    if-nez v1, :cond_0

    invoke-static {}, Lzl0;->f()Lzl0;

    move-result-object p1

    invoke-virtual {p1, v0}, Lzl0;->b(Ljava/lang/String;)V

    new-instance p1, Ljava/lang/RuntimeException;

    invoke-direct {p1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Lcom/google/android/gms/tasks/Tasks;->forException(Ljava/lang/Exception;)Lcom/google/android/gms/tasks/Task;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p0}, Lbn;->l()V

    return-object p1

    :cond_0
    :try_start_1
    iget-object v0, p0, Lbn;->i:Lzm;

    invoke-virtual {v0, p1}, Lzm;->z(Lzm1;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {}, Lzl0;->f()Lzl0;

    move-result-object v0

    const-string v1, "Previous sessions could not be finalized."

    invoke-virtual {v0, v1}, Lzl0;->k(Ljava/lang/String;)V

    :cond_1
    iget-object v0, p0, Lbn;->i:Lzm;

    invoke-interface {p1}, Lzm1;->b()Lcom/google/android/gms/tasks/Task;

    move-result-object p1

    invoke-virtual {v0, p1}, Lzm;->U(Lcom/google/android/gms/tasks/Task;)Lcom/google/android/gms/tasks/Task;

    move-result-object p1
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-virtual {p0}, Lbn;->l()V

    return-object p1

    :catchall_0
    move-exception p1

    goto :goto_0

    :catch_0
    move-exception p1

    :try_start_2
    invoke-static {}, Lzl0;->f()Lzl0;

    move-result-object v0

    const-string v1, "Crashlytics encountered a problem during asynchronous initialization."

    invoke-virtual {v0, v1, p1}, Lzl0;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-static {p1}, Lcom/google/android/gms/tasks/Tasks;->forException(Ljava/lang/Exception;)Lcom/google/android/gms/tasks/Task;

    move-result-object p1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    invoke-virtual {p0}, Lbn;->l()V

    return-object p1

    :goto_0
    invoke-virtual {p0}, Lbn;->l()V

    throw p1
.end method

.method public g(Lzm1;)Lcom/google/android/gms/tasks/Task;
    .locals 2

    .line 1
    iget-object v0, p0, Lbn;->n:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lbn$a;

    invoke-direct {v1, p0, p1}, Lbn$a;-><init>(Lbn;Lzm1;)V

    invoke-static {v0, v1}, Lv22;->h(Ljava/util/concurrent/Executor;Ljava/util/concurrent/Callable;)Lcom/google/android/gms/tasks/Task;

    move-result-object p1

    return-object p1
.end method

.method public final h(Lzm1;)V
    .locals 3

    .line 1
    new-instance v0, Lbn$b;

    invoke-direct {v0, p0, p1}, Lbn$b;-><init>(Lbn;Lzm1;)V

    iget-object p1, p0, Lbn;->n:Ljava/util/concurrent/ExecutorService;

    invoke-interface {p1, v0}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    move-result-object p1

    invoke-static {}, Lzl0;->f()Lzl0;

    move-result-object v0

    const-string v1, "Crashlytics detected incomplete initialization on previous app launch. Will initialize synchronously."

    invoke-virtual {v0, v1}, Lzl0;->b(Ljava/lang/String;)V

    :try_start_0
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v1, 0x3

    invoke-interface {p1, v1, v2, v0}, Ljava/util/concurrent/Future;->get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception p1

    invoke-static {}, Lzl0;->f()Lzl0;

    move-result-object v0

    const-string v1, "Crashlytics timed out during initialization."

    goto :goto_0

    :catch_1
    move-exception p1

    invoke-static {}, Lzl0;->f()Lzl0;

    move-result-object v0

    const-string v1, "Crashlytics encountered a problem during initialization."

    goto :goto_0

    :catch_2
    move-exception p1

    invoke-static {}, Lzl0;->f()Lzl0;

    move-result-object v0

    const-string v1, "Crashlytics was interrupted during initialization."

    :goto_0
    invoke-virtual {v0, v1, p1}, Lzl0;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    :goto_1
    return-void
.end method

.method public k(Ljava/lang/String;)V
    .locals 4

    .line 1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lbn;->e:J

    sub-long/2addr v0, v2

    iget-object v2, p0, Lbn;->i:Lzm;

    invoke-virtual {v2, v0, v1, p1}, Lzm;->X(JLjava/lang/String;)V

    return-void
.end method

.method public l()V
    .locals 2

    .line 1
    iget-object v0, p0, Lbn;->o:Lxm;

    new-instance v1, Lbn$c;

    invoke-direct {v1, p0}, Lbn$c;-><init>(Lbn;)V

    invoke-virtual {v0, v1}, Lxm;->g(Ljava/util/concurrent/Callable;)Lcom/google/android/gms/tasks/Task;

    return-void
.end method

.method public m()V
    .locals 2

    .line 1
    iget-object v0, p0, Lbn;->o:Lxm;

    invoke-virtual {v0}, Lxm;->b()V

    iget-object v0, p0, Lbn;->f:Lcn;

    invoke-virtual {v0}, Lcn;->a()Z

    invoke-static {}, Lzl0;->f()Lzl0;

    move-result-object v0

    const-string v1, "Initialization marker file was created."

    invoke-virtual {v0, v1}, Lzl0;->i(Ljava/lang/String;)V

    return-void
.end method

.method public n(Ls7;Lzm1;)Z
    .locals 28

    .line 1
    move-object/from16 v1, p0

    move-object/from16 v0, p2

    iget-object v2, v1, Lbn;->a:Landroid/content/Context;

    const-string v3, "com.crashlytics.RequireBuildId"

    const/4 v12, 0x1

    invoke-static {v2, v3, v12}, Lcom/google/firebase/crashlytics/internal/common/CommonUtils;->i(Landroid/content/Context;Ljava/lang/String;Z)Z

    move-result v2

    move-object/from16 v15, p1

    iget-object v3, v15, Ls7;->b:Ljava/lang/String;

    invoke-static {v3, v2}, Lbn;->j(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_1

    new-instance v2, Lvd;

    iget-object v3, v1, Lbn;->j:Lde0;

    invoke-direct {v2, v3}, Lvd;-><init>(Lde0;)V

    invoke-virtual {v2}, Lvd;->toString()Ljava/lang/String;

    move-result-object v14

    const/16 v27, 0x0

    :try_start_0
    new-instance v2, Lcn;

    const-string v3, "crash_marker"

    iget-object v4, v1, Lbn;->k:Lz00;

    invoke-direct {v2, v3, v4}, Lcn;-><init>(Ljava/lang/String;Lz00;)V

    iput-object v2, v1, Lbn;->g:Lcn;

    new-instance v2, Lcn;

    const-string v3, "initialization_marker"

    iget-object v4, v1, Lbn;->k:Lz00;

    invoke-direct {v2, v3, v4}, Lcn;-><init>(Ljava/lang/String;Lz00;)V

    iput-object v2, v1, Lbn;->f:Lcn;

    new-instance v13, Lf22;

    iget-object v2, v1, Lbn;->k:Lz00;

    iget-object v3, v1, Lbn;->o:Lxm;

    invoke-direct {v13, v14, v2, v3}, Lf22;-><init>(Ljava/lang/String;Lz00;Lxm;)V

    new-instance v11, Lul0;

    iget-object v2, v1, Lbn;->k:Lz00;

    invoke-direct {v11, v2}, Lul0;-><init>(Lz00;)V

    new-instance v8, Liw0;

    new-array v2, v12, [Llp1;

    new-instance v3, Lqd1;

    const/16 v4, 0xa

    invoke-direct {v3, v4}, Lqd1;-><init>(I)V

    aput-object v3, v2, v27

    const/16 v3, 0x400

    invoke-direct {v8, v3, v2}, Liw0;-><init>(I[Llp1;)V

    iget-object v2, v1, Lbn;->r:Lhd1;

    invoke-virtual {v2, v13}, Lhd1;->c(Lf22;)V

    iget-object v2, v1, Lbn;->a:Landroid/content/Context;

    iget-object v3, v1, Lbn;->j:Lde0;

    iget-object v4, v1, Lbn;->k:Lz00;

    iget-object v10, v1, Lbn;->d:Lm11;

    iget-object v9, v1, Lbn;->p:Lwm;

    move-object/from16 v5, p1

    move-object v6, v11

    move-object v7, v13

    move-object/from16 v16, v9

    move-object/from16 v9, p2

    move-object/from16 v22, v11

    move-object/from16 v11, v16

    invoke-static/range {v2 .. v11}, Lnm1;->h(Landroid/content/Context;Lde0;Lz00;Ls7;Lul0;Lf22;Llp1;Lzm1;Lm11;Lwm;)Lnm1;

    move-result-object v23

    new-instance v2, Lzm;

    iget-object v3, v1, Lbn;->a:Landroid/content/Context;

    iget-object v4, v1, Lbn;->o:Lxm;

    iget-object v5, v1, Lbn;->j:Lde0;

    iget-object v6, v1, Lbn;->c:Ldp;

    iget-object v7, v1, Lbn;->k:Lz00;

    iget-object v8, v1, Lbn;->g:Lcn;

    iget-object v9, v1, Lbn;->q:Ldn;

    iget-object v10, v1, Lbn;->m:Lm4;

    iget-object v11, v1, Lbn;->p:Lwm;

    move-object/from16 v21, v13

    move-object v13, v2

    move-object v12, v14

    move-object v14, v3

    move-object v15, v4

    move-object/from16 v16, v5

    move-object/from16 v17, v6

    move-object/from16 v18, v7

    move-object/from16 v19, v8

    move-object/from16 v20, p1

    move-object/from16 v24, v9

    move-object/from16 v25, v10

    move-object/from16 v26, v11

    invoke-direct/range {v13 .. v26}, Lzm;-><init>(Landroid/content/Context;Lxm;Lde0;Ldp;Lz00;Lcn;Ls7;Lf22;Lul0;Lnm1;Ldn;Lm4;Lwm;)V

    iput-object v2, v1, Lbn;->i:Lzm;

    invoke-virtual/range {p0 .. p0}, Lbn;->e()Z

    move-result v2

    invoke-virtual/range {p0 .. p0}, Lbn;->d()V

    iget-object v3, v1, Lbn;->i:Lzm;

    invoke-static {}, Ljava/lang/Thread;->getDefaultUncaughtExceptionHandler()Ljava/lang/Thread$UncaughtExceptionHandler;

    move-result-object v4

    invoke-virtual {v3, v12, v4, v0}, Lzm;->x(Ljava/lang/String;Ljava/lang/Thread$UncaughtExceptionHandler;Lzm1;)V

    if-eqz v2, :cond_0

    iget-object v2, v1, Lbn;->a:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/firebase/crashlytics/internal/common/CommonUtils;->d(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {}, Lzl0;->f()Lzl0;

    move-result-object v2

    const-string v3, "Crashlytics did not finish previous background initialization. Initializing synchronously."

    invoke-virtual {v2, v3}, Lzl0;->b(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Lbn;->h(Lzm1;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return v27

    :cond_0
    invoke-static {}, Lzl0;->f()Lzl0;

    move-result-object v0

    const-string v2, "Successfully configured exception handler."

    invoke-virtual {v0, v2}, Lzl0;->b(Ljava/lang/String;)V

    const/4 v0, 0x1

    return v0

    :catch_0
    move-exception v0

    invoke-static {}, Lzl0;->f()Lzl0;

    move-result-object v2

    const-string v3, "Crashlytics was not started due to an exception during initialization"

    invoke-virtual {v2, v3, v0}, Lzl0;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    const/4 v0, 0x0

    iput-object v0, v1, Lbn;->i:Lzm;

    return v27

    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v2, "The Crashlytics build ID is missing. This occurs when the Crashlytics Gradle plugin is missing from your app\'s build configuration. Please review the Firebase Crashlytics onboarding instructions at https://firebase.google.com/docs/crashlytics/get-started?platform=android#add-plugin"

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

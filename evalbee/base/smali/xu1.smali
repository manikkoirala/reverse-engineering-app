.class public abstract Lxu1;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(Landroid/content/Context;)V
    .locals 2

    .line 1
    invoke-static {p0}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;

    move-result-object v0

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getTemplatesJson()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->getExamId()Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    move-result-object v1

    invoke-static {p0, v1}, Lxu1;->b(Landroid/content/Context;Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public static b(Landroid/content/Context;Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)V
    .locals 3

    .line 1
    invoke-static {p0}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getTemplateJson(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->getFolderPath()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {p0}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;

    move-result-object v2

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->getFolderPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, p1, v1}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->deleteAllSheetImageForExam(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;Ljava/lang/String;)Z

    :cond_0
    invoke-static {p0}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;

    move-result-object p0

    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->deleteAllResultForExam(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)Z

    invoke-virtual {v0, p1}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->deleteTemplateJson(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)Z

    return-void
.end method

.method public static c(Landroid/content/Context;Lcom/ekodroid/omrevaluator/templateui/models/ExamId;Lcom/ekodroid/omrevaluator/serializable/ExmVersions/ExamDataV1;)Z
    .locals 10

    .line 1
    invoke-virtual {p2}, Lcom/ekodroid/omrevaluator/serializable/ExmVersions/ExamDataV1;->getResults()Ljava/util/ArrayList;

    move-result-object p2

    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :cond_0
    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/ekodroid/omrevaluator/serializable/ExmVersions/ResultDataV1;

    new-instance v9, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;->getExamName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/serializable/ExmVersions/ResultDataV1;->getRollno()I

    move-result v3

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;->getClassName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;->getExamDate()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/serializable/ExmVersions/ResultDataV1;->getResultItem2()Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;

    move-result-object v6

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v1, v9

    invoke-direct/range {v1 .. v8}, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;ZZ)V

    invoke-static {p0}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;

    move-result-object v0

    invoke-virtual {v9}, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;->getRollNo()I

    move-result v1

    invoke-virtual {v0, p1, v1}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->deleteStudentResult(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;I)Z

    invoke-virtual {v9}, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;->getRollNo()I

    move-result v1

    invoke-virtual {v0, p1, v1}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->deleteSheetImages(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;I)Z

    invoke-virtual {v0, v9}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->saveOrUpdateResultJsonAsNotSynced(Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/google/firebase/analytics/FirebaseAnalytics;->getInstance(Landroid/content/Context;)Lcom/google/firebase/analytics/FirebaseAnalytics;

    move-result-object v0

    const-string v1, "ResultImportSave"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/firebase/analytics/FirebaseAnalytics;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_0

    :cond_1
    const/4 p0, 0x1

    return p0
.end method

.method public static d(Landroid/content/Context;Lcom/ekodroid/omrevaluator/templateui/models/ExamId;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;Ljava/lang/String;)Z
    .locals 9

    .line 1
    invoke-static {p0}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getTemplateJson(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->getFolderPath()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {p0}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;

    move-result-object v2

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->getFolderPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, p1, v1}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->deleteAllSheetImageForExam(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;Ljava/lang/String;)Z

    :cond_0
    invoke-static {p0}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->deleteAllResultForExam(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)Z

    invoke-virtual {v0, p1}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->deleteTemplateJson(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)Z

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;->getExamName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->setName(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;->getExamDate()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lb10;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    new-instance p0, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;->getExamName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;->getClassName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;->getExamDate()Ljava/lang/String;

    move-result-object v5

    move-object v2, p0

    move-object v6, p2

    move-object v8, p3

    invoke-direct/range {v2 .. v8}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->saveOrUpdateTemplateJson(Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;)Z

    move-result p0

    if-eqz p0, :cond_1

    const/4 p0, 0x1

    return p0

    :cond_1
    const/4 p0, 0x0

    return p0
.end method

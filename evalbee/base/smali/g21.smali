.class public Lg21;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lr91;
.implements Ljr;


# static fields
.field public static final c:Ljr$a;

.field public static final d:Lr91;


# instance fields
.field public a:Ljr$a;

.field public volatile b:Lr91;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .line 1
    new-instance v0, Ld21;

    invoke-direct {v0}, Ld21;-><init>()V

    sput-object v0, Lg21;->c:Ljr$a;

    new-instance v0, Le21;

    invoke-direct {v0}, Le21;-><init>()V

    sput-object v0, Lg21;->d:Lr91;

    return-void
.end method

.method public constructor <init>(Ljr$a;Lr91;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lg21;->a:Ljr$a;

    iput-object p2, p0, Lg21;->b:Lr91;

    return-void
.end method

.method public static synthetic b()Ljava/lang/Object;
    .locals 1

    .line 1
    invoke-static {}, Lg21;->g()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public static synthetic c(Lr91;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lg21;->f(Lr91;)V

    return-void
.end method

.method public static synthetic d(Ljr$a;Ljr$a;Lr91;)V
    .locals 0

    .line 1
    invoke-static {p0, p1, p2}, Lg21;->h(Ljr$a;Ljr$a;Lr91;)V

    return-void
.end method

.method public static e()Lg21;
    .locals 3

    .line 1
    new-instance v0, Lg21;

    sget-object v1, Lg21;->c:Ljr$a;

    sget-object v2, Lg21;->d:Lr91;

    invoke-direct {v0, v1, v2}, Lg21;-><init>(Ljr$a;Lr91;)V

    return-object v0
.end method

.method public static synthetic f(Lr91;)V
    .locals 0

    .line 1
    return-void
.end method

.method public static synthetic g()Ljava/lang/Object;
    .locals 1

    .line 1
    const/4 v0, 0x0

    return-object v0
.end method

.method public static synthetic h(Ljr$a;Ljr$a;Lr91;)V
    .locals 0

    .line 1
    invoke-interface {p0, p2}, Ljr$a;->a(Lr91;)V

    invoke-interface {p1, p2}, Ljr$a;->a(Lr91;)V

    return-void
.end method

.method public static i(Lr91;)Lg21;
    .locals 2

    .line 1
    new-instance v0, Lg21;

    const/4 v1, 0x0

    invoke-direct {v0, v1, p0}, Lg21;-><init>(Ljr$a;Lr91;)V

    return-object v0
.end method


# virtual methods
.method public a(Ljr$a;)V
    .locals 3

    .line 1
    iget-object v0, p0, Lg21;->b:Lr91;

    sget-object v1, Lg21;->d:Lr91;

    if-eq v0, v1, :cond_0

    invoke-interface {p1, v0}, Ljr$a;->a(Lr91;)V

    return-void

    :cond_0
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lg21;->b:Lr91;

    if-eq v0, v1, :cond_1

    move-object v1, v0

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lg21;->a:Ljr$a;

    new-instance v2, Lf21;

    invoke-direct {v2, v1, p1}, Lf21;-><init>(Ljr$a;Ljr$a;)V

    iput-object v2, p0, Lg21;->a:Ljr$a;

    const/4 v1, 0x0

    :goto_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_2

    invoke-interface {p1, v0}, Ljr$a;->a(Lr91;)V

    :cond_2
    return-void

    :catchall_0
    move-exception p1

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw p1
.end method

.method public get()Ljava/lang/Object;
    .locals 1

    .line 1
    iget-object v0, p0, Lg21;->b:Lr91;

    invoke-interface {v0}, Lr91;->get()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public j(Lr91;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lg21;->b:Lr91;

    sget-object v1, Lg21;->d:Lr91;

    if-ne v0, v1, :cond_0

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lg21;->a:Ljr$a;

    const/4 v1, 0x0

    iput-object v1, p0, Lg21;->a:Ljr$a;

    iput-object p1, p0, Lg21;->b:Lr91;

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-interface {v0, p1}, Ljr$a;->a(Lr91;)V

    return-void

    :catchall_0
    move-exception p1

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw p1

    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "provide() can be called only once."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

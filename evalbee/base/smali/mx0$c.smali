.class public final Lmx0$c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lmx0;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/content/Intent;Llg0;Ljava/util/concurrent/Executor;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public final synthetic a:Lmx0;


# direct methods
.method public constructor <init>(Lmx0;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lmx0$c;->a:Lmx0;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 1

    .line 1
    const-string v0, "name"

    invoke-static {p1, v0}, Lfg0;->e(Ljava/lang/Object;Ljava/lang/String;)V

    const-string p1, "service"

    invoke-static {p2, p1}, Lfg0;->e(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p1, p0, Lmx0$c;->a:Lmx0;

    invoke-static {p2}, Lwd0$a;->p(Landroid/os/IBinder;)Lwd0;

    move-result-object p2

    invoke-virtual {p1, p2}, Lmx0;->m(Lwd0;)V

    iget-object p1, p0, Lmx0$c;->a:Lmx0;

    invoke-virtual {p1}, Lmx0;->d()Ljava/util/concurrent/Executor;

    move-result-object p1

    iget-object p2, p0, Lmx0$c;->a:Lmx0;

    invoke-virtual {p2}, Lmx0;->i()Ljava/lang/Runnable;

    move-result-object p2

    invoke-interface {p1, p2}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 1

    .line 1
    const-string v0, "name"

    invoke-static {p1, v0}, Lfg0;->e(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p1, p0, Lmx0$c;->a:Lmx0;

    invoke-virtual {p1}, Lmx0;->d()Ljava/util/concurrent/Executor;

    move-result-object p1

    iget-object v0, p0, Lmx0$c;->a:Lmx0;

    invoke-virtual {v0}, Lmx0;->g()Ljava/lang/Runnable;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    iget-object p1, p0, Lmx0$c;->a:Lmx0;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lmx0;->m(Lwd0;)V

    return-void
.end method

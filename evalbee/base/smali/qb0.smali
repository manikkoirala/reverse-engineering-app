.class public Lqb0;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lij1;
.implements Lk11;
.implements Lqy;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lqb0$b;
    }
.end annotation


# static fields
.field public static final p:Ljava/lang/String;


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:Ljava/util/Map;

.field public c:Lor;

.field public d:Z

.field public final e:Ljava/lang/Object;

.field public final f:Lpp1;

.field public final g:Lq81;

.field public final h:La92;

.field public final i:Landroidx/work/a;

.field public final j:Ljava/util/Map;

.field public k:Ljava/lang/Boolean;

.field public final l:Landroidx/work/impl/constraints/WorkConstraintsTracker;

.field public final m:Lhu1;

.field public final n:Ljw1;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .line 1
    const-string v0, "GreedyScheduler"

    invoke-static {v0}, Lxl0;->i(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lqb0;->p:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroidx/work/a;Lky1;Lq81;La92;Lhu1;)V
    .locals 2

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lqb0;->b:Ljava/util/Map;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lqb0;->e:Ljava/lang/Object;

    new-instance v0, Lpp1;

    invoke-direct {v0}, Lpp1;-><init>()V

    iput-object v0, p0, Lqb0;->f:Lpp1;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lqb0;->j:Ljava/util/Map;

    iput-object p1, p0, Lqb0;->a:Landroid/content/Context;

    invoke-virtual {p2}, Landroidx/work/a;->k()Lag1;

    move-result-object p1

    new-instance v0, Lor;

    invoke-virtual {p2}, Landroidx/work/a;->a()Lch;

    move-result-object v1

    invoke-direct {v0, p0, p1, v1}, Lor;-><init>(Lij1;Lag1;Lch;)V

    iput-object v0, p0, Lqb0;->c:Lor;

    new-instance v0, Ljw1;

    invoke-direct {v0, p1, p5}, Ljw1;-><init>(Lag1;La92;)V

    iput-object v0, p0, Lqb0;->n:Ljw1;

    iput-object p6, p0, Lqb0;->m:Lhu1;

    new-instance p1, Landroidx/work/impl/constraints/WorkConstraintsTracker;

    invoke-direct {p1, p3}, Landroidx/work/impl/constraints/WorkConstraintsTracker;-><init>(Lky1;)V

    iput-object p1, p0, Lqb0;->l:Landroidx/work/impl/constraints/WorkConstraintsTracker;

    iput-object p2, p0, Lqb0;->i:Landroidx/work/a;

    iput-object p4, p0, Lqb0;->g:Lq81;

    iput-object p5, p0, Lqb0;->h:La92;

    return-void
.end method


# virtual methods
.method public varargs a([Lp92;)V
    .locals 11

    .line 1
    iget-object v0, p0, Lqb0;->k:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lqb0;->f()V

    :cond_0
    iget-object v0, p0, Lqb0;->k:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {}, Lxl0;->e()Lxl0;

    move-result-object p1

    sget-object v0, Lqb0;->p:Ljava/lang/String;

    const-string v1, "Ignoring schedule request in a secondary process"

    invoke-virtual {p1, v0, v1}, Lxl0;->f(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_1
    invoke-virtual {p0}, Lqb0;->g()V

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    array-length v2, p1

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_8

    aget-object v4, p1, v3

    invoke-static {v4}, Lu92;->a(Lp92;)Lx82;

    move-result-object v5

    iget-object v6, p0, Lqb0;->f:Lpp1;

    invoke-virtual {v6, v5}, Lpp1;->a(Lx82;)Z

    move-result v5

    if-eqz v5, :cond_2

    goto/16 :goto_2

    :cond_2
    invoke-virtual {p0, v4}, Lqb0;->i(Lp92;)J

    move-result-wide v5

    invoke-virtual {v4}, Lp92;->c()J

    move-result-wide v7

    invoke-static {v7, v8, v5, v6}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v5

    iget-object v7, p0, Lqb0;->i:Landroidx/work/a;

    invoke-virtual {v7}, Landroidx/work/a;->a()Lch;

    move-result-object v7

    invoke-interface {v7}, Lch;->currentTimeMillis()J

    move-result-wide v7

    iget-object v9, v4, Lp92;->b:Landroidx/work/WorkInfo$State;

    sget-object v10, Landroidx/work/WorkInfo$State;->ENQUEUED:Landroidx/work/WorkInfo$State;

    if-ne v9, v10, :cond_7

    cmp-long v7, v7, v5

    if-gez v7, :cond_3

    iget-object v7, p0, Lqb0;->c:Lor;

    if-eqz v7, :cond_7

    invoke-virtual {v7, v4, v5, v6}, Lor;->a(Lp92;J)V

    goto/16 :goto_2

    :cond_3
    invoke-virtual {v4}, Lp92;->k()Z

    move-result v5

    if-eqz v5, :cond_6

    iget-object v5, v4, Lp92;->j:Lzk;

    invoke-virtual {v5}, Lzk;->h()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-static {}, Lxl0;->e()Lxl0;

    move-result-object v5

    sget-object v6, Lqb0;->p:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Ignoring "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v4, ". Requires device idle."

    :goto_1
    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v6, v4}, Lxl0;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    :cond_4
    iget-object v5, v4, Lp92;->j:Lzk;

    invoke-virtual {v5}, Lzk;->e()Z

    move-result v5

    if-eqz v5, :cond_5

    invoke-static {}, Lxl0;->e()Lxl0;

    move-result-object v5

    sget-object v6, Lqb0;->p:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Ignoring "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v4, ". Requires ContentUri triggers."

    goto :goto_1

    :cond_5
    invoke-interface {v0, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iget-object v4, v4, Lp92;->a:Ljava/lang/String;

    invoke-interface {v1, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_6
    iget-object v5, p0, Lqb0;->f:Lpp1;

    invoke-static {v4}, Lu92;->a(Lp92;)Lx82;

    move-result-object v6

    invoke-virtual {v5, v6}, Lpp1;->a(Lx82;)Z

    move-result v5

    if-nez v5, :cond_7

    invoke-static {}, Lxl0;->e()Lxl0;

    move-result-object v5

    sget-object v6, Lqb0;->p:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Starting work for "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v8, v4, Lp92;->a:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Lxl0;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v5, p0, Lqb0;->f:Lpp1;

    invoke-virtual {v5, v4}, Lpp1;->e(Lp92;)Lop1;

    move-result-object v4

    iget-object v5, p0, Lqb0;->n:Ljw1;

    invoke-virtual {v5, v4}, Ljw1;->c(Lop1;)V

    iget-object v5, p0, Lqb0;->h:La92;

    invoke-interface {v5, v4}, La92;->e(Lop1;)V

    :cond_7
    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    :cond_8
    iget-object p1, p0, Lqb0;->e:Ljava/lang/Object;

    monitor-enter p1

    :try_start_0
    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_a

    const-string v2, ","

    invoke-static {v2, v1}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lxl0;->e()Lxl0;

    move-result-object v2

    sget-object v3, Lqb0;->p:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Starting tracking for "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v3, v1}, Lxl0;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_9
    :goto_3
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_a

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lp92;

    invoke-static {v1}, Lu92;->a(Lp92;)Lx82;

    move-result-object v2

    iget-object v3, p0, Lqb0;->b:Ljava/util/Map;

    invoke-interface {v3, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_9

    iget-object v3, p0, Lqb0;->l:Landroidx/work/impl/constraints/WorkConstraintsTracker;

    iget-object v4, p0, Lqb0;->m:Lhu1;

    invoke-interface {v4}, Lhu1;->a()Lkotlinx/coroutines/CoroutineDispatcher;

    move-result-object v4

    invoke-static {v3, v1, v4, p0}, Landroidx/work/impl/constraints/WorkConstraintsTrackerKt;->b(Landroidx/work/impl/constraints/WorkConstraintsTracker;Lp92;Lkotlinx/coroutines/CoroutineDispatcher;Lk11;)Lkotlinx/coroutines/n;

    move-result-object v1

    iget-object v3, p0, Lqb0;->b:Ljava/util/Map;

    invoke-interface {v3, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_3

    :cond_a
    monitor-exit p1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public b()Z
    .locals 1

    .line 1
    const/4 v0, 0x0

    return v0
.end method

.method public c(Lp92;Landroidx/work/impl/constraints/a;)V
    .locals 4

    .line 1
    invoke-static {p1}, Lu92;->a(Lp92;)Lx82;

    move-result-object p1

    instance-of v0, p2, Landroidx/work/impl/constraints/a$a;

    if-eqz v0, :cond_0

    iget-object p2, p0, Lqb0;->f:Lpp1;

    invoke-virtual {p2, p1}, Lpp1;->a(Lx82;)Z

    move-result p2

    if-nez p2, :cond_1

    invoke-static {}, Lxl0;->e()Lxl0;

    move-result-object p2

    sget-object v0, Lqb0;->p:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Constraints met: Scheduling work ID "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Lxl0;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object p2, p0, Lqb0;->f:Lpp1;

    invoke-virtual {p2, p1}, Lpp1;->d(Lx82;)Lop1;

    move-result-object p1

    iget-object p2, p0, Lqb0;->n:Ljw1;

    invoke-virtual {p2, p1}, Ljw1;->c(Lop1;)V

    iget-object p2, p0, Lqb0;->h:La92;

    invoke-interface {p2, p1}, La92;->e(Lop1;)V

    goto :goto_0

    :cond_0
    invoke-static {}, Lxl0;->e()Lxl0;

    move-result-object v0

    sget-object v1, Lqb0;->p:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Constraints not met: Cancelling work ID "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lxl0;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lqb0;->f:Lpp1;

    invoke-virtual {v0, p1}, Lpp1;->b(Lx82;)Lop1;

    move-result-object p1

    if-eqz p1, :cond_1

    iget-object v0, p0, Lqb0;->n:Ljw1;

    invoke-virtual {v0, p1}, Ljw1;->b(Lop1;)V

    check-cast p2, Landroidx/work/impl/constraints/a$b;

    invoke-virtual {p2}, Landroidx/work/impl/constraints/a$b;->a()I

    move-result p2

    iget-object v0, p0, Lqb0;->h:La92;

    invoke-interface {v0, p1, p2}, La92;->c(Lop1;I)V

    :cond_1
    :goto_0
    return-void
.end method

.method public d(Lx82;Z)V
    .locals 2

    .line 1
    iget-object v0, p0, Lqb0;->f:Lpp1;

    invoke-virtual {v0, p1}, Lpp1;->b(Lx82;)Lop1;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lqb0;->n:Ljw1;

    invoke-virtual {v1, v0}, Ljw1;->b(Lop1;)V

    :cond_0
    invoke-virtual {p0, p1}, Lqb0;->h(Lx82;)V

    if-nez p2, :cond_1

    iget-object p2, p0, Lqb0;->e:Ljava/lang/Object;

    monitor-enter p2

    :try_start_0
    iget-object v0, p0, Lqb0;->j:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit p2

    goto :goto_0

    :catchall_0
    move-exception p1

    monitor-exit p2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1

    :cond_1
    :goto_0
    return-void
.end method

.method public e(Ljava/lang/String;)V
    .locals 4

    .line 1
    iget-object v0, p0, Lqb0;->k:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lqb0;->f()V

    :cond_0
    iget-object v0, p0, Lqb0;->k:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {}, Lxl0;->e()Lxl0;

    move-result-object p1

    sget-object v0, Lqb0;->p:Ljava/lang/String;

    const-string v1, "Ignoring schedule request in non-main process"

    invoke-virtual {p1, v0, v1}, Lxl0;->f(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_1
    invoke-virtual {p0}, Lqb0;->g()V

    invoke-static {}, Lxl0;->e()Lxl0;

    move-result-object v0

    sget-object v1, Lqb0;->p:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Cancelling work ID "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lxl0;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lqb0;->c:Lor;

    if-eqz v0, :cond_2

    invoke-virtual {v0, p1}, Lor;->b(Ljava/lang/String;)V

    :cond_2
    iget-object v0, p0, Lqb0;->f:Lpp1;

    invoke-virtual {v0, p1}, Lpp1;->c(Ljava/lang/String;)Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lop1;

    iget-object v1, p0, Lqb0;->n:Ljw1;

    invoke-virtual {v1, v0}, Ljw1;->b(Lop1;)V

    iget-object v1, p0, Lqb0;->h:La92;

    invoke-interface {v1, v0}, La92;->b(Lop1;)V

    goto :goto_0

    :cond_3
    return-void
.end method

.method public final f()V
    .locals 2

    .line 1
    iget-object v0, p0, Lqb0;->a:Landroid/content/Context;

    iget-object v1, p0, Lqb0;->i:Landroidx/work/a;

    invoke-static {v0, v1}, Lm81;->b(Landroid/content/Context;Landroidx/work/a;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lqb0;->k:Ljava/lang/Boolean;

    return-void
.end method

.method public final g()V
    .locals 1

    .line 1
    iget-boolean v0, p0, Lqb0;->d:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lqb0;->g:Lq81;

    invoke-virtual {v0, p0}, Lq81;->e(Lqy;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lqb0;->d:Z

    :cond_0
    return-void
.end method

.method public final h(Lx82;)V
    .locals 5

    .line 1
    iget-object v0, p0, Lqb0;->e:Ljava/lang/Object;

    monitor-enter v0

    :try_start_0
    iget-object v1, p0, Lqb0;->b:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lkotlinx/coroutines/n;

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_0

    invoke-static {}, Lxl0;->e()Lxl0;

    move-result-object v0

    sget-object v2, Lqb0;->p:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Stopping tracking for "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, v2, p1}, Lxl0;->a(Ljava/lang/String;Ljava/lang/String;)V

    const/4 p1, 0x0

    invoke-interface {v1, p1}, Lkotlinx/coroutines/n;->b(Ljava/util/concurrent/CancellationException;)V

    :cond_0
    return-void

    :catchall_0
    move-exception p1

    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw p1
.end method

.method public final i(Lp92;)J
    .locals 7

    .line 1
    iget-object v0, p0, Lqb0;->e:Ljava/lang/Object;

    monitor-enter v0

    :try_start_0
    invoke-static {p1}, Lu92;->a(Lp92;)Lx82;

    move-result-object v1

    iget-object v2, p0, Lqb0;->j:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lqb0$b;

    if-nez v2, :cond_0

    new-instance v2, Lqb0$b;

    iget v3, p1, Lp92;->k:I

    iget-object v4, p0, Lqb0;->i:Landroidx/work/a;

    invoke-virtual {v4}, Landroidx/work/a;->a()Lch;

    move-result-object v4

    invoke-interface {v4}, Lch;->currentTimeMillis()J

    move-result-wide v4

    const/4 v6, 0x0

    invoke-direct {v2, v3, v4, v5, v6}, Lqb0$b;-><init>(IJLqb0$a;)V

    iget-object v3, p0, Lqb0;->j:Ljava/util/Map;

    invoke-interface {v3, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    iget-wide v3, v2, Lqb0$b;->b:J

    iget p1, p1, Lp92;->k:I

    iget v1, v2, Lqb0$b;->a:I

    sub-int/2addr p1, v1

    add-int/lit8 p1, p1, -0x5

    const/4 v1, 0x0

    invoke-static {p1, v1}, Ljava/lang/Math;->max(II)I

    move-result p1

    int-to-long v1, p1

    const-wide/16 v5, 0x7530

    mul-long/2addr v1, v5

    add-long/2addr v3, v1

    monitor-exit v0

    return-wide v3

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

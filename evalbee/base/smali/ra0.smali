.class public Lra0;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Landroid/content/SharedPreferences;

.field public c:Lee1;

.field public d:Ly01;

.field public e:Ljava/lang/String;

.field public f:Landroid/content/Context;

.field public g:I

.field public h:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lee1;Ly01;Ljava/lang/String;)V
    .locals 2

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "https://"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, La91;->u()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ":8759/sessionProfile2"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lra0;->a:Ljava/lang/String;

    const/4 v0, 0x0

    iput v0, p0, Lra0;->g:I

    const/4 v1, 0x0

    iput-object v1, p0, Lra0;->h:Ljava/lang/String;

    iput-object p2, p0, Lra0;->c:Lee1;

    iput-object p3, p0, Lra0;->d:Ly01;

    iput-object p4, p0, Lra0;->e:Ljava/lang/String;

    iput-object p1, p0, Lra0;->f:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    const-string p2, "MyPref"

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object p1

    iput-object p1, p0, Lra0;->b:Landroid/content/SharedPreferences;

    invoke-virtual {p0}, Lra0;->i()V

    return-void
.end method

.method public static synthetic a(Lra0;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lra0;->e:Ljava/lang/String;

    return-object p0
.end method

.method public static synthetic b(Lra0;)I
    .locals 0

    .line 1
    iget p0, p0, Lra0;->g:I

    return p0
.end method

.method public static synthetic c(Lra0;I)I
    .locals 0

    .line 1
    iput p1, p0, Lra0;->g:I

    return p1
.end method

.method public static synthetic d(Lra0;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lra0;->h:Ljava/lang/String;

    return-object p0
.end method

.method public static synthetic e(Lra0;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 1
    iput-object p1, p0, Lra0;->h:Ljava/lang/String;

    return-object p1
.end method

.method public static synthetic f(Lra0;Ljava/lang/Object;)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lra0;->j(Ljava/lang/Object;)V

    return-void
.end method

.method public static synthetic g(Lra0;)Landroid/content/Context;
    .locals 0

    .line 1
    iget-object p0, p0, Lra0;->f:Landroid/content/Context;

    return-object p0
.end method

.method public static h()Ljava/lang/String;
    .locals 2

    .line 1
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "yyyy-MM-dd"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    invoke-virtual {v0, v1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final i()V
    .locals 7

    .line 1
    new-instance v6, Lra0$c;

    const/4 v2, 0x1

    iget-object v3, p0, Lra0;->a:Ljava/lang/String;

    new-instance v4, Lra0$a;

    invoke-direct {v4, p0}, Lra0$a;-><init>(Lra0;)V

    new-instance v5, Lra0$b;

    invoke-direct {v5, p0}, Lra0$b;-><init>(Lra0;)V

    move-object v0, v6

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lra0$c;-><init>(Lra0;ILjava/lang/String;Lcom/android/volley/d$b;Lcom/android/volley/d$a;)V

    new-instance v0, Lwq;

    const/4 v1, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    const v3, 0x9c40

    invoke-direct {v0, v3, v1, v2}, Lwq;-><init>(IIF)V

    invoke-virtual {v6, v0}, Lcom/android/volley/Request;->L(Ljf1;)Lcom/android/volley/Request;

    iget-object v0, p0, Lra0;->c:Lee1;

    invoke-virtual {v0, v6}, Lee1;->a(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    return-void
.end method

.method public final j(Ljava/lang/Object;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lra0;->d:Ly01;

    if-eqz v0, :cond_0

    invoke-interface {v0, p1}, Ly01;->a(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

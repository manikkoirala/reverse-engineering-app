.class public abstract Lpx0;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final f:Ljava/lang/Object;

.field public static final g:Ljava/lang/Object;


# instance fields
.field public a:[[D

.field public b:[Ljava/lang/Object;

.field public c:I

.field public d:D

.field public final e:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .line 1
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lpx0;->f:Ljava/lang/Object;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lpx0;->g:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(I)V
    .locals 3

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x2

    const/4 v1, 0x0

    filled-new-array {v0, v1}, [I

    move-result-object v0

    sget-object v2, Ljava/lang/Double;->TYPE:Ljava/lang/Class;

    invoke-static {v2, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[D

    iput-object v0, p0, Lpx0;->a:[[D

    array-length v0, v0

    new-array v0, v0, [Ljava/lang/Object;

    iput-object v0, p0, Lpx0;->b:[Ljava/lang/Object;

    iput v1, p0, Lpx0;->c:I

    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    iput-wide v0, p0, Lpx0;->d:D

    if-lez p1, :cond_0

    iput p1, p0, Lpx0;->e:I

    return-void

    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "dimension > 0 required"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public final a([DLjava/lang/Object;)V
    .locals 2

    .line 1
    if-eqz p1, :cond_2

    array-length v0, p1

    iget v1, p0, Lpx0;->e:I

    if-lt v0, v1, :cond_1

    iget v0, p0, Lpx0;->c:I

    if-nez v0, :cond_0

    sget-object p2, Lpx0;->f:Ljava/lang/Object;

    :cond_0
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Lpx0;->b(I)V

    iget-object v0, p0, Lpx0;->a:[[D

    iget v1, p0, Lpx0;->c:I

    aput-object p1, v0, v1

    iget-object p1, p0, Lpx0;->b:[Ljava/lang/Object;

    aput-object p2, p1, v1

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lpx0;->c:I

    return-void

    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "p.length >= dimension required"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_2
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Point cannot be null."

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public b(I)V
    .locals 4

    .line 1
    iget-object v0, p0, Lpx0;->a:[[D

    array-length v1, v0

    if-ge v1, p1, :cond_3

    array-length v0, v0

    mul-int/lit8 v0, v0, 0x2

    if-ge v0, p1, :cond_0

    goto :goto_0

    :cond_0
    move p1, v0

    :goto_0
    new-array v0, p1, [[D

    const/4 v1, 0x0

    move v2, v1

    :goto_1
    iget v3, p0, Lpx0;->c:I

    if-ge v2, v3, :cond_1

    iget-object v3, p0, Lpx0;->a:[[D

    aget-object v3, v3, v2

    aput-object v3, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_1
    new-array p1, p1, [Ljava/lang/Object;

    :goto_2
    iget v2, p0, Lpx0;->c:I

    if-ge v1, v2, :cond_2

    iget-object v2, p0, Lpx0;->b:[Ljava/lang/Object;

    aget-object v2, v2, v1

    aput-object v2, p1, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_2
    iput-object v0, p0, Lpx0;->a:[[D

    iput-object p1, p0, Lpx0;->b:[Ljava/lang/Object;

    :cond_3
    return-void
.end method

.method public c()I
    .locals 1

    .line 1
    iget v0, p0, Lpx0;->e:I

    return v0
.end method

.method public d()D
    .locals 2

    .line 1
    iget-wide v0, p0, Lpx0;->d:D

    return-wide v0
.end method

.method public e([D)V
    .locals 1

    .line 1
    sget-object v0, Lpx0;->g:Ljava/lang/Object;

    invoke-virtual {p0, p1, v0}, Lpx0;->a([DLjava/lang/Object;)V

    return-void
.end method

.method public f([D)V
    .locals 1

    .line 1
    sget-object v0, Lpx0;->f:Ljava/lang/Object;

    invoke-virtual {p0, p1, v0}, Lpx0;->a([DLjava/lang/Object;)V

    return-void
.end method

.method public g(D)V
    .locals 2

    .line 1
    const-wide/16 v0, 0x0

    cmpg-double v0, p1, v0

    if-lez v0, :cond_0

    iput-wide p1, p0, Lpx0;->d:D

    return-void

    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "flatness > 0 required"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.class public Lm20;
.super Ll20;
.source "SourceFile"


# instance fields
.field public final a:Lcom/google/android/gms/common/api/GoogleApi;

.field public final b:Lr91;

.field public final c:Lr10;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/api/GoogleApi;Lr10;Lr91;)V
    .locals 0

    .line 2
    invoke-direct {p0}, Ll20;-><init>()V

    iput-object p1, p0, Lm20;->a:Lcom/google/android/gms/common/api/GoogleApi;

    invoke-static {p2}, Lcom/google/android/gms/common/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lr10;

    iput-object p1, p0, Lm20;->c:Lr10;

    iput-object p3, p0, Lm20;->b:Lr91;

    invoke-interface {p3}, Lr91;->get()Ljava/lang/Object;

    move-result-object p1

    if-nez p1, :cond_0

    const-string p1, "FDL"

    const-string p2, "FDL logging failed. Add a dependency for Firebase Analytics to your app to enable logging of Dynamic Link events."

    invoke-static {p1, p2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void
.end method

.method public constructor <init>(Lr10;Lr91;)V
    .locals 2

    .line 1
    new-instance v0, Lsv;

    invoke-virtual {p1}, Lr10;->l()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lsv;-><init>(Landroid/content/Context;)V

    invoke-direct {p0, v0, p1, p2}, Lm20;-><init>(Lcom/google/android/gms/common/api/GoogleApi;Lr10;Lr91;)V

    return-void
.end method

.class public Lh62$d;
.super Lh62$e;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lh62;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "d"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lh62$d$a;
    }
.end annotation


# instance fields
.field public final e:Landroid/view/WindowInsetsAnimation;


# direct methods
.method public constructor <init>(ILandroid/view/animation/Interpolator;J)V
    .locals 0

    .line 1
    invoke-static {p1, p2, p3, p4}, Ln62;->a(ILandroid/view/animation/Interpolator;J)Landroid/view/WindowInsetsAnimation;

    move-result-object p1

    invoke-direct {p0, p1}, Lh62$d;-><init>(Landroid/view/WindowInsetsAnimation;)V

    return-void
.end method

.method public constructor <init>(Landroid/view/WindowInsetsAnimation;)V
    .locals 4

    .line 2
    const/4 v0, 0x0

    const-wide/16 v1, 0x0

    const/4 v3, 0x0

    invoke-direct {p0, v3, v0, v1, v2}, Lh62$e;-><init>(ILandroid/view/animation/Interpolator;J)V

    iput-object p1, p0, Lh62$d;->e:Landroid/view/WindowInsetsAnimation;

    return-void
.end method

.method public static e(Lh62$a;)Landroid/view/WindowInsetsAnimation$Bounds;
    .locals 1

    .line 1
    invoke-static {}, Lp62;->a()V

    invoke-virtual {p0}, Lh62$a;->a()Lnf0;

    move-result-object v0

    invoke-virtual {v0}, Lnf0;->e()Landroid/graphics/Insets;

    move-result-object v0

    invoke-virtual {p0}, Lh62$a;->b()Lnf0;

    move-result-object p0

    invoke-virtual {p0}, Lnf0;->e()Landroid/graphics/Insets;

    move-result-object p0

    invoke-static {v0, p0}, Lo62;->a(Landroid/graphics/Insets;Landroid/graphics/Insets;)Landroid/view/WindowInsetsAnimation$Bounds;

    move-result-object p0

    return-object p0
.end method

.method public static f(Landroid/view/WindowInsetsAnimation$Bounds;)Lnf0;
    .locals 0

    .line 1
    invoke-static {p0}, Lr62;->a(Landroid/view/WindowInsetsAnimation$Bounds;)Landroid/graphics/Insets;

    move-result-object p0

    invoke-static {p0}, Lnf0;->d(Landroid/graphics/Insets;)Lnf0;

    move-result-object p0

    return-object p0
.end method

.method public static g(Landroid/view/WindowInsetsAnimation$Bounds;)Lnf0;
    .locals 0

    .line 1
    invoke-static {p0}, Lq62;->a(Landroid/view/WindowInsetsAnimation$Bounds;)Landroid/graphics/Insets;

    move-result-object p0

    invoke-static {p0}, Lnf0;->d(Landroid/graphics/Insets;)Lnf0;

    move-result-object p0

    return-object p0
.end method

.method public static h(Landroid/view/View;Lh62$b;)V
    .locals 1

    .line 1
    if-eqz p1, :cond_0

    new-instance v0, Lh62$d$a;

    invoke-direct {v0, p1}, Lh62$d$a;-><init>(Lh62$b;)V

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-static {p0, v0}, Lk62;->a(Landroid/view/View;Landroid/view/WindowInsetsAnimation$Callback;)V

    return-void
.end method


# virtual methods
.method public a()J
    .locals 2

    .line 1
    iget-object v0, p0, Lh62$d;->e:Landroid/view/WindowInsetsAnimation;

    invoke-static {v0}, Li62;->a(Landroid/view/WindowInsetsAnimation;)J

    move-result-wide v0

    return-wide v0
.end method

.method public b()F
    .locals 1

    .line 1
    iget-object v0, p0, Lh62$d;->e:Landroid/view/WindowInsetsAnimation;

    invoke-static {v0}, Lj62;->a(Landroid/view/WindowInsetsAnimation;)F

    move-result v0

    return v0
.end method

.method public c()I
    .locals 1

    .line 1
    iget-object v0, p0, Lh62$d;->e:Landroid/view/WindowInsetsAnimation;

    invoke-static {v0}, Ll62;->a(Landroid/view/WindowInsetsAnimation;)I

    move-result v0

    return v0
.end method

.method public d(F)V
    .locals 1

    .line 1
    iget-object v0, p0, Lh62$d;->e:Landroid/view/WindowInsetsAnimation;

    invoke-static {v0, p1}, Lm62;->a(Landroid/view/WindowInsetsAnimation;F)V

    return-void
.end method

.class public Lze1$b;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lze1;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "b"
.end annotation


# instance fields
.field public final synthetic a:Lze1;


# direct methods
.method public constructor <init>(Lze1;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lze1$b;->a:Lze1;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lze1;Lze1$a;)V
    .locals 0

    .line 2
    invoke-direct {p0, p1}, Lze1$b;-><init>(Lze1;)V

    return-void
.end method


# virtual methods
.method public varargs a([Ljava/lang/String;)Ljava/lang/Void;
    .locals 11

    .line 1
    iget-object p1, p0, Lze1$b;->a:Lze1;

    iget-object v0, p1, Lze1;->b:Ltd1;

    invoke-static {p1, v0}, Lze1;->f(Lze1;Ltd1;)[B

    move-result-object v0

    invoke-static {p1, v0}, Lze1;->e(Lze1;[B)[B

    iget-object p1, p0, Lze1$b;->a:Lze1;

    invoke-static {p1}, Lze1;->d(Lze1;)[B

    move-result-object p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lze1$b;->a:Lze1;

    iget-object p1, p1, Lze1;->b:Ltd1;

    iget-object p1, p1, Ltd1;->d:Ljava/lang/String;

    const-string v0, "[^a-zA-Z0-9_-]"

    const-string v1, "_"

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    const/4 v0, 0x0

    :try_start_0
    iget-object v2, p0, Lze1$b;->a:Lze1;

    invoke-static {v2}, Lze1;->a(Lze1;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    iget-object v3, p0, Lze1$b;->a:Lze1;

    invoke-static {v3}, Lze1;->a(Lze1;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v0}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v2

    iget v0, v2, Landroid/content/pm/PackageInfo;->versionCode:I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v2

    invoke-virtual {v2}, Ljava/lang/Throwable;->printStackTrace()V

    :goto_0
    move v9, v0

    iget-object v0, p0, Lze1$b;->a:Lze1;

    new-instance v2, Lcom/ekodroid/omrevaluator/exams/models/MessageReport;

    iget-object v3, v0, Lze1;->b:Ltd1;

    iget-object v4, v3, Ltd1;->b:Ljava/lang/String;

    iget-object v5, v3, Ltd1;->c:Ljava/lang/String;

    const/4 v6, 0x3

    invoke-static {v0}, Lze1;->d(Lze1;)[B

    move-result-object v7

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Report_"

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, p0, Lze1$b;->a:Lze1;

    iget-object p1, p1, Lze1;->b:Ltd1;

    iget p1, p1, Ltd1;->l:I

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    iget-object p1, p0, Lze1$b;->a:Lze1;

    iget-object p1, p1, Lze1;->b:Ltd1;

    iget-object v10, p1, Ltd1;->a:Ljava/lang/String;

    move-object v3, v2

    invoke-direct/range {v3 .. v10}, Lcom/ekodroid/omrevaluator/exams/models/MessageReport;-><init>(Ljava/lang/String;Ljava/lang/String;I[BLjava/lang/String;ILjava/lang/String;)V

    iput-object v2, v0, Lze1;->c:Lcom/ekodroid/omrevaluator/exams/models/MessageReport;

    :cond_0
    const/4 p1, 0x0

    return-object p1
.end method

.method public b(Ljava/lang/Void;)V
    .locals 8

    .line 1
    iget-object p1, p0, Lze1$b;->a:Lze1;

    iget-object v0, p1, Lze1;->c:Lcom/ekodroid/omrevaluator/exams/models/MessageReport;

    if-eqz v0, :cond_0

    new-instance v0, Ln52;

    invoke-static {p1}, Lze1;->a(Lze1;)Landroid/content/Context;

    move-result-object p1

    invoke-static {}, La91;->w()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Ln52;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {v0}, Ln52;->b()Lee1;

    move-result-object p1

    new-instance v0, Ll61;

    new-instance v7, Lcom/ekodroid/omrevaluator/serializable/ReportData;

    iget-object v1, p0, Lze1$b;->a:Lze1;

    iget-object v1, v1, Lze1;->c:Lcom/ekodroid/omrevaluator/exams/models/MessageReport;

    iget-object v2, v1, Lcom/ekodroid/omrevaluator/exams/models/MessageReport;->emailId:Ljava/lang/String;

    iget-object v3, v1, Lcom/ekodroid/omrevaluator/exams/models/MessageReport;->ccEmailID:Ljava/lang/String;

    iget-object v4, v1, Lcom/ekodroid/omrevaluator/exams/models/MessageReport;->mailSubject:Ljava/lang/String;

    iget v5, v1, Lcom/ekodroid/omrevaluator/exams/models/MessageReport;->appVersion:I

    iget-object v6, v1, Lcom/ekodroid/omrevaluator/exams/models/MessageReport;->studentName:Ljava/lang/String;

    move-object v1, v7

    invoke-direct/range {v1 .. v6}, Lcom/ekodroid/omrevaluator/serializable/ReportData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V

    iget-object v1, p0, Lze1$b;->a:Lze1;

    iget-object v2, v1, Lze1;->c:Lcom/ekodroid/omrevaluator/exams/models/MessageReport;

    iget-object v2, v2, Lcom/ekodroid/omrevaluator/exams/models/MessageReport;->data:[B

    iget-object v1, v1, Lze1;->e:Ly01;

    invoke-direct {v0, v7, v2, p1, v1}, Ll61;-><init>(Lcom/ekodroid/omrevaluator/serializable/ReportData;[BLee1;Ly01;)V

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    invoke-static {p1, v0}, Lze1;->c(Lze1;Z)V

    :goto_0
    return-void
.end method

.method public varargs c([Ljava/lang/Integer;)V
    .locals 0

    .line 1
    return-void
.end method

.method public bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 1
    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lze1$b;->a([Ljava/lang/String;)Ljava/lang/Void;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 1
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lze1$b;->b(Ljava/lang/Void;)V

    return-void
.end method

.method public onPreExecute()V
    .locals 0

    .line 1
    return-void
.end method

.method public bridge synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0

    .line 1
    check-cast p1, [Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lze1$b;->c([Ljava/lang/Integer;)V

    return-void
.end method

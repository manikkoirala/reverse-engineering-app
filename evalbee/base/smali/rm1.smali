.class public final Lrm1;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final c:Lrm1;

.field public static final d:Lrm1;


# instance fields
.field public final a:Z

.field public final b:Lq00;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .line 1
    new-instance v0, Lrm1;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lrm1;-><init>(ZLq00;)V

    sput-object v0, Lrm1;->c:Lrm1;

    new-instance v0, Lrm1;

    const/4 v1, 0x1

    invoke-direct {v0, v1, v2}, Lrm1;-><init>(ZLq00;)V

    sput-object v0, Lrm1;->d:Lrm1;

    return-void
.end method

.method public constructor <init>(ZLq00;)V
    .locals 3

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    if-eqz p2, :cond_1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    move v1, v0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v1, 0x1

    :goto_1
    const-string v2, "Cannot specify a fieldMask for non-merge sets()"

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {v1, v2, v0}, Lk71;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    iput-boolean p1, p0, Lrm1;->a:Z

    iput-object p2, p0, Lrm1;->b:Lq00;

    return-void
.end method

.method public static c()Lrm1;
    .locals 1

    .line 1
    sget-object v0, Lrm1;->d:Lrm1;

    return-object v0
.end method


# virtual methods
.method public a()Lq00;
    .locals 1

    .line 1
    iget-object v0, p0, Lrm1;->b:Lq00;

    return-object v0
.end method

.method public b()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lrm1;->a:Z

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .line 1
    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-eqz p1, :cond_5

    const-class v2, Lrm1;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_1

    goto :goto_1

    :cond_1
    check-cast p1, Lrm1;

    iget-boolean v2, p0, Lrm1;->a:Z

    iget-boolean v3, p1, Lrm1;->a:Z

    if-eq v2, v3, :cond_2

    return v1

    :cond_2
    iget-object v2, p0, Lrm1;->b:Lq00;

    iget-object p1, p1, Lrm1;->b:Lq00;

    if-eqz v2, :cond_3

    invoke-virtual {v2, p1}, Lq00;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    :cond_3
    if-nez p1, :cond_4

    goto :goto_0

    :cond_4
    move v0, v1

    :goto_0
    return v0

    :cond_5
    :goto_1
    return v1
.end method

.method public hashCode()I
    .locals 2

    .line 1
    iget-boolean v0, p0, Lrm1;->a:Z

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lrm1;->b:Lq00;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lq00;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    add-int/2addr v0, v1

    return v0
.end method

.class public Ljk1;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public A:Landroid/widget/Spinner;

.field public B:Landroid/widget/CheckBox;

.field public C:Landroid/widget/CheckBox;

.field public D:Landroid/widget/CheckBox;

.field public E:Landroid/widget/CheckBox;

.field public F:Landroid/widget/EditText;

.field public G:Landroid/widget/LinearLayout;

.field public H:Landroid/widget/LinearLayout;

.field public I:Landroid/widget/LinearLayout;

.field public a:Landroid/content/Context;

.field public b:Lcom/ekodroid/omrevaluator/templateui/models/Section2;

.field public c:Ly01;

.field public d:Landroid/widget/LinearLayout;

.field public e:Landroid/widget/ArrayAdapter;

.field public f:Landroid/widget/ArrayAdapter;

.field public g:Landroid/widget/ArrayAdapter;

.field public h:Landroid/widget/ArrayAdapter;

.field public i:Landroid/widget/ArrayAdapter;

.field public j:Landroid/widget/ArrayAdapter;

.field public k:Landroid/widget/ArrayAdapter;

.field public l:Ljava/util/ArrayList;

.field public m:Ljava/util/ArrayList;

.field public n:D

.field public o:D

.field public p:[Ljava/lang/Integer;

.field public q:[Ljava/lang/Integer;

.field public r:[Ljava/lang/Integer;

.field public s:Landroid/widget/Spinner;

.field public t:Landroid/widget/Spinner;

.field public u:Landroid/widget/Spinner;

.field public v:Landroid/widget/Spinner;

.field public w:Landroid/widget/Spinner;

.field public x:Landroid/widget/Spinner;

.field public y:Landroid/widget/Spinner;

.field public z:Landroid/widget/Spinner;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/ekodroid/omrevaluator/templateui/models/Section2;Ljava/lang/String;[Ljava/lang/Integer;Ljava/util/ArrayList;Ljava/util/ArrayList;Ly01;)V
    .locals 27

    .line 1
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p4

    move-object/from16 v3, p5

    move-object/from16 v4, p6

    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    const-wide/high16 v5, 0x3ff0000000000000L    # 1.0

    iput-wide v5, v0, Ljk1;->n:D

    const-wide/16 v5, 0x0

    iput-wide v5, v0, Ljk1;->o:D

    const/16 v5, 0x8

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    new-array v7, v5, [Ljava/lang/Integer;

    const/4 v8, 0x2

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    const/4 v10, 0x0

    aput-object v9, v7, v10

    const/4 v11, 0x3

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    const/4 v13, 0x1

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    aput-object v12, v7, v13

    const/4 v15, 0x4

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    aput-object v16, v7, v8

    const/16 v17, 0x5

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    aput-object v18, v7, v11

    const/16 v19, 0x6

    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v20

    aput-object v20, v7, v15

    const/16 v21, 0x7

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v22

    aput-object v22, v7, v17

    aput-object v6, v7, v19

    const/16 v23, 0x9

    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v24

    aput-object v24, v7, v21

    iput-object v7, v0, Ljk1;->p:[Ljava/lang/Integer;

    const/16 v7, 0xa

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v25

    new-array v5, v7, [Ljava/lang/Integer;

    aput-object v14, v5, v10

    aput-object v9, v5, v13

    aput-object v12, v5, v8

    aput-object v16, v5, v11

    aput-object v18, v5, v15

    aput-object v20, v5, v17

    aput-object v22, v5, v19

    aput-object v6, v5, v21

    const/16 v26, 0x8

    aput-object v24, v5, v26

    aput-object v25, v5, v23

    iput-object v5, v0, Ljk1;->q:[Ljava/lang/Integer;

    new-array v5, v7, [Ljava/lang/Integer;

    aput-object v14, v5, v10

    aput-object v9, v5, v13

    aput-object v12, v5, v8

    aput-object v16, v5, v11

    aput-object v18, v5, v15

    aput-object v20, v5, v17

    aput-object v22, v5, v19

    aput-object v6, v5, v21

    const/16 v6, 0x8

    aput-object v24, v5, v6

    aput-object v25, v5, v23

    iput-object v5, v0, Ljk1;->r:[Ljava/lang/Integer;

    iput-object v1, v0, Ljk1;->a:Landroid/content/Context;

    move-object/from16 v5, p2

    iput-object v5, v0, Ljk1;->b:Lcom/ekodroid/omrevaluator/templateui/models/Section2;

    iput-object v3, v0, Ljk1;->l:Ljava/util/ArrayList;

    iput-object v4, v0, Ljk1;->m:Ljava/util/ArrayList;

    move-object/from16 v5, p7

    iput-object v5, v0, Ljk1;->c:Ly01;

    new-instance v5, Landroid/widget/ArrayAdapter;

    sget-object v6, Lgr1;->a:[Ljava/lang/String;

    const v7, 0x7f0c00f0

    invoke-direct {v5, v1, v7, v6}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    iput-object v5, v0, Ljk1;->e:Landroid/widget/ArrayAdapter;

    new-instance v5, Landroid/widget/ArrayAdapter;

    sget-object v6, Lgr1;->b:[Ljava/lang/String;

    invoke-direct {v5, v1, v7, v6}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    iput-object v5, v0, Ljk1;->f:Landroid/widget/ArrayAdapter;

    new-instance v5, Landroid/widget/ArrayAdapter;

    invoke-direct {v5, v1, v7, v2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    iput-object v5, v0, Ljk1;->g:Landroid/widget/ArrayAdapter;

    new-instance v5, Landroid/widget/ArrayAdapter;

    invoke-direct {v5, v1, v7, v2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    iput-object v5, v0, Ljk1;->h:Landroid/widget/ArrayAdapter;

    new-instance v2, Landroid/widget/ArrayAdapter;

    iget-object v5, v0, Ljk1;->p:[Ljava/lang/Integer;

    invoke-direct {v2, v1, v7, v5}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    iput-object v2, v0, Ljk1;->i:Landroid/widget/ArrayAdapter;

    new-instance v2, Landroid/widget/ArrayAdapter;

    iget-object v5, v0, Ljk1;->q:[Ljava/lang/Integer;

    invoke-direct {v2, v1, v7, v5}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    iput-object v2, v0, Ljk1;->j:Landroid/widget/ArrayAdapter;

    new-instance v2, Landroid/widget/ArrayAdapter;

    iget-object v5, v0, Ljk1;->r:[Ljava/lang/Integer;

    invoke-direct {v2, v1, v7, v5}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    iput-object v2, v0, Ljk1;->k:Landroid/widget/ArrayAdapter;

    const-string v2, "layout_inflater"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    if-eqz v1, :cond_0

    const v2, 0x7f0c0095

    const/4 v5, 0x0

    invoke-virtual {v1, v2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, v0, Ljk1;->d:Landroid/widget/LinearLayout;

    const v2, 0x7f09014d

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, v0, Ljk1;->F:Landroid/widget/EditText;

    iget-object v1, v0, Ljk1;->d:Landroid/widget/LinearLayout;

    const v2, 0x7f090354

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Spinner;

    iput-object v1, v0, Ljk1;->s:Landroid/widget/Spinner;

    iget-object v1, v0, Ljk1;->d:Landroid/widget/LinearLayout;

    const v2, 0x7f090356

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Spinner;

    iput-object v1, v0, Ljk1;->t:Landroid/widget/Spinner;

    iget-object v1, v0, Ljk1;->d:Landroid/widget/LinearLayout;

    const v2, 0x7f090352

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Spinner;

    iput-object v1, v0, Ljk1;->u:Landroid/widget/Spinner;

    iget-object v1, v0, Ljk1;->d:Landroid/widget/LinearLayout;

    const v2, 0x7f090353

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Spinner;

    iput-object v1, v0, Ljk1;->v:Landroid/widget/Spinner;

    iget-object v1, v0, Ljk1;->d:Landroid/widget/LinearLayout;

    const v2, 0x7f090351

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Spinner;

    iput-object v1, v0, Ljk1;->w:Landroid/widget/Spinner;

    iget-object v1, v0, Ljk1;->d:Landroid/widget/LinearLayout;

    const v2, 0x7f090357

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Spinner;

    iput-object v1, v0, Ljk1;->x:Landroid/widget/Spinner;

    iget-object v1, v0, Ljk1;->d:Landroid/widget/LinearLayout;

    const v2, 0x7f090350

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Spinner;

    iput-object v1, v0, Ljk1;->y:Landroid/widget/Spinner;

    iget-object v1, v0, Ljk1;->d:Landroid/widget/LinearLayout;

    const v2, 0x7f0900f5

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    iput-object v1, v0, Ljk1;->E:Landroid/widget/CheckBox;

    iget-object v1, v0, Ljk1;->d:Landroid/widget/LinearLayout;

    const v2, 0x7f0900fa

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    iput-object v1, v0, Ljk1;->B:Landroid/widget/CheckBox;

    iget-object v1, v0, Ljk1;->d:Landroid/widget/LinearLayout;

    const v2, 0x7f0900f9

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    iput-object v1, v0, Ljk1;->D:Landroid/widget/CheckBox;

    iget-object v1, v0, Ljk1;->d:Landroid/widget/LinearLayout;

    const v2, 0x7f0900f8

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    iput-object v1, v0, Ljk1;->C:Landroid/widget/CheckBox;

    iget-object v1, v0, Ljk1;->d:Landroid/widget/LinearLayout;

    const v2, 0x7f090206

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, v0, Ljk1;->G:Landroid/widget/LinearLayout;

    iget-object v1, v0, Ljk1;->d:Landroid/widget/LinearLayout;

    const v2, 0x7f090203

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, v0, Ljk1;->H:Landroid/widget/LinearLayout;

    iget-object v1, v0, Ljk1;->d:Landroid/widget/LinearLayout;

    const v2, 0x7f090208

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, v0, Ljk1;->I:Landroid/widget/LinearLayout;

    iget-object v1, v0, Ljk1;->d:Landroid/widget/LinearLayout;

    const v2, 0x7f090358

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Spinner;

    iput-object v1, v0, Ljk1;->A:Landroid/widget/Spinner;

    iget-object v1, v0, Ljk1;->d:Landroid/widget/LinearLayout;

    const v2, 0x7f090355

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Spinner;

    iput-object v1, v0, Ljk1;->z:Landroid/widget/Spinner;

    iget-object v1, v0, Ljk1;->w:Landroid/widget/Spinner;

    iget-object v2, v0, Ljk1;->i:Landroid/widget/ArrayAdapter;

    invoke-virtual {v1, v2}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    iget-object v1, v0, Ljk1;->s:Landroid/widget/Spinner;

    iget-object v2, v0, Ljk1;->g:Landroid/widget/ArrayAdapter;

    invoke-virtual {v1, v2}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    iget-object v1, v0, Ljk1;->z:Landroid/widget/Spinner;

    iget-object v2, v0, Ljk1;->h:Landroid/widget/ArrayAdapter;

    invoke-virtual {v1, v2}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    iget-object v1, v0, Ljk1;->t:Landroid/widget/Spinner;

    iget-object v2, v0, Ljk1;->e:Landroid/widget/ArrayAdapter;

    invoke-virtual {v1, v2}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    iget-object v1, v0, Ljk1;->y:Landroid/widget/Spinner;

    iget-object v2, v0, Ljk1;->k:Landroid/widget/ArrayAdapter;

    invoke-virtual {v1, v2}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    iget-object v1, v0, Ljk1;->x:Landroid/widget/Spinner;

    iget-object v2, v0, Ljk1;->j:Landroid/widget/ArrayAdapter;

    invoke-virtual {v1, v2}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    iget-object v1, v0, Ljk1;->A:Landroid/widget/Spinner;

    iget-object v2, v0, Ljk1;->f:Landroid/widget/ArrayAdapter;

    invoke-virtual {v1, v2}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    iget-object v1, v0, Ljk1;->F:Landroid/widget/EditText;

    move-object/from16 v2, p3

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, v0, Ljk1;->F:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->length()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setSelection(I)V

    iget-object v1, v0, Ljk1;->s:Landroid/widget/Spinner;

    invoke-virtual {v1, v10}, Landroid/widget/AdapterView;->setSelection(I)V

    iget-object v1, v0, Ljk1;->t:Landroid/widget/Spinner;

    invoke-virtual {v1, v11}, Landroid/widget/AdapterView;->setSelection(I)V

    iget-object v1, v0, Ljk1;->x:Landroid/widget/Spinner;

    invoke-virtual {v1, v11}, Landroid/widget/AdapterView;->setSelection(I)V

    iget-object v1, v0, Ljk1;->y:Landroid/widget/Spinner;

    invoke-virtual {v1, v15}, Landroid/widget/AdapterView;->setSelection(I)V

    invoke-virtual {v0, v3, v4}, Ljk1;->u(Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    invoke-virtual/range {p0 .. p0}, Ljk1;->z()V

    invoke-virtual/range {p0 .. p0}, Ljk1;->A()V

    invoke-virtual/range {p0 .. p0}, Ljk1;->v()V

    invoke-virtual/range {p0 .. p0}, Ljk1;->w()V

    :cond_0
    return-void
.end method

.method public static synthetic a(Ljk1;)Landroid/widget/Spinner;
    .locals 0

    .line 1
    iget-object p0, p0, Ljk1;->z:Landroid/widget/Spinner;

    return-object p0
.end method

.method public static synthetic b(Ljk1;)Landroid/widget/LinearLayout;
    .locals 0

    .line 1
    iget-object p0, p0, Ljk1;->I:Landroid/widget/LinearLayout;

    return-object p0
.end method

.method public static synthetic c(Ljk1;)Landroid/widget/LinearLayout;
    .locals 0

    .line 1
    iget-object p0, p0, Ljk1;->G:Landroid/widget/LinearLayout;

    return-object p0
.end method

.method public static synthetic d(Ljk1;)Landroid/widget/LinearLayout;
    .locals 0

    .line 1
    iget-object p0, p0, Ljk1;->H:Landroid/widget/LinearLayout;

    return-object p0
.end method

.method public static synthetic e(Ljk1;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Ljk1;->B()V

    return-void
.end method

.method public static synthetic f(Ljk1;)Landroid/widget/Spinner;
    .locals 0

    .line 1
    iget-object p0, p0, Ljk1;->u:Landroid/widget/Spinner;

    return-object p0
.end method

.method public static synthetic g(Ljk1;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Ljk1;->C()V

    return-void
.end method

.method public static synthetic h(Ljk1;)Landroid/widget/Spinner;
    .locals 0

    .line 1
    iget-object p0, p0, Ljk1;->v:Landroid/widget/Spinner;

    return-object p0
.end method


# virtual methods
.method public final A()V
    .locals 2

    .line 1
    iget-object v0, p0, Ljk1;->s:Landroid/widget/Spinner;

    new-instance v1, Ljk1$c;

    invoke-direct {v1, p0}, Ljk1$c;-><init>(Ljk1;)V

    invoke-virtual {v0, v1}, Landroid/widget/AdapterView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    return-void
.end method

.method public final B()V
    .locals 5

    .line 1
    new-instance v0, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    iget-object v1, p0, Ljk1;->a:Landroid/content/Context;

    const v2, 0x7f130151

    invoke-direct {v0, v1, v2}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;-><init>(Landroid/content/Context;I)V

    iget-object v1, p0, Ljk1;->a:Landroid/content/Context;

    const-string v2, "layout_inflater"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    const v2, 0x7f0c005e

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->setView(Landroid/view/View;)Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    const v2, 0x7f120022

    invoke-virtual {v0, v2}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->setTitle(I)Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    const v3, 0x7f0903e0

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v4, 0x7f12014a

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    const v3, 0x7f090146

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    new-instance v3, Ljk1$h;

    invoke-direct {v3, p0, v1}, Ljk1$h;-><init>(Ljk1;Landroid/widget/EditText;)V

    invoke-virtual {v0, v2, v3}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    invoke-virtual {v0}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->create()Landroidx/appcompat/app/a;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    return-void
.end method

.method public final C()V
    .locals 5

    .line 1
    new-instance v0, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    iget-object v1, p0, Ljk1;->a:Landroid/content/Context;

    const v2, 0x7f130151

    invoke-direct {v0, v1, v2}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;-><init>(Landroid/content/Context;I)V

    iget-object v1, p0, Ljk1;->a:Landroid/content/Context;

    const-string v2, "layout_inflater"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    const v2, 0x7f0c005e

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->setView(Landroid/view/View;)Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    const v2, 0x7f120022

    invoke-virtual {v0, v2}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->setTitle(I)Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    const v3, 0x7f0903e0

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    const v4, 0x7f12014b

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    const v3, 0x7f090146

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    new-instance v3, Ljk1$i;

    invoke-direct {v3, p0, v1}, Ljk1$i;-><init>(Ljk1;Landroid/widget/EditText;)V

    invoke-virtual {v0, v2, v3}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    invoke-virtual {v0}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->create()Landroidx/appcompat/app/a;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    return-void
.end method

.method public final i(Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 2

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sget-object v1, Lok;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method public j()I
    .locals 1

    .line 1
    iget-object v0, p0, Ljk1;->z:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/AdapterView;->getSelectedItem()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public k()I
    .locals 1

    .line 1
    iget-object v0, p0, Ljk1;->s:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/AdapterView;->getSelectedItem()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public l()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Ljk1;->A:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/AdapterView;->getSelectedItem()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public m()Ljava/lang/String;
    .locals 5

    .line 1
    invoke-virtual {p0}, Ljk1;->p()Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    move-result-object v0

    sget-object v1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;->DECIMAL:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    if-ne v0, v1, :cond_0

    new-instance v0, Lgc0;

    invoke-direct {v0}, Lgc0;-><init>()V

    new-instance v1, Lcom/ekodroid/omrevaluator/templateui/models/DecimalOptionPayload;

    iget-object v2, p0, Ljk1;->C:Landroid/widget/CheckBox;

    invoke-virtual {v2}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v2

    iget-object v3, p0, Ljk1;->w:Landroid/widget/Spinner;

    invoke-virtual {v3}, Landroid/widget/AdapterView;->getSelectedItemPosition()I

    move-result v3

    add-int/lit8 v3, v3, 0x2

    iget-object v4, p0, Ljk1;->E:Landroid/widget/CheckBox;

    invoke-virtual {v4}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v4

    invoke-direct {v1, v2, v3, v4}, Lcom/ekodroid/omrevaluator/templateui/models/DecimalOptionPayload;-><init>(ZIZ)V

    :goto_0
    invoke-virtual {v0, v1}, Lgc0;->s(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    invoke-virtual {p0}, Ljk1;->p()Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    move-result-object v0

    sget-object v1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;->MATRIX:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    if-ne v0, v1, :cond_1

    new-instance v0, Lgc0;

    invoke-direct {v0}, Lgc0;-><init>()V

    new-instance v1, Lcom/ekodroid/omrevaluator/templateui/models/MatrixOptionPayload;

    iget-object v2, p0, Ljk1;->x:Landroid/widget/Spinner;

    invoke-virtual {v2}, Landroid/widget/AdapterView;->getSelectedItemPosition()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    iget-object v3, p0, Ljk1;->y:Landroid/widget/Spinner;

    invoke-virtual {v3}, Landroid/widget/AdapterView;->getSelectedItemPosition()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-direct {v1, v2, v3}, Lcom/ekodroid/omrevaluator/templateui/models/MatrixOptionPayload;-><init>(II)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    return-object v0
.end method

.method public n()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Ljk1;->F:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public o()Landroid/widget/LinearLayout;
    .locals 1

    .line 1
    iget-object v0, p0, Ljk1;->d:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method public p()Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;
    .locals 1

    .line 1
    iget-object v0, p0, Ljk1;->t:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/AdapterView;->getSelectedItemPosition()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    sget-object v0, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;->FOUROPTION:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    return-object v0

    :pswitch_0
    sget-object v0, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;->DECIMAL:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    return-object v0

    :pswitch_1
    sget-object v0, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;->MATRIX:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    return-object v0

    :pswitch_2
    sget-object v0, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;->TENOPTION:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    return-object v0

    :pswitch_3
    sget-object v0, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;->EIGHTOPTION:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    return-object v0

    :pswitch_4
    sget-object v0, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;->SIXOPTION:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    return-object v0

    :pswitch_5
    sget-object v0, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;->FIVEOPTION:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    return-object v0

    :pswitch_6
    sget-object v0, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;->FOUROPTION:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    return-object v0

    :pswitch_7
    sget-object v0, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;->THREEOPTION:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    return-object v0

    :pswitch_8
    sget-object v0, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;->TRUEORFALSE:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public q()D
    .locals 2

    .line 1
    iget-object v0, p0, Ljk1;->m:Ljava/util/ArrayList;

    iget-object v1, p0, Ljk1;->v:Landroid/widget/Spinner;

    invoke-virtual {v1}, Landroid/widget/AdapterView;->getSelectedItemPosition()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    return-wide v0
.end method

.method public r()D
    .locals 2

    .line 1
    iget-object v0, p0, Ljk1;->l:Ljava/util/ArrayList;

    iget-object v1, p0, Ljk1;->u:Landroid/widget/Spinner;

    invoke-virtual {v1}, Landroid/widget/AdapterView;->getSelectedItemPosition()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    return-wide v0
.end method

.method public s()Z
    .locals 1

    .line 1
    iget-object v0, p0, Ljk1;->B:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v0

    return v0
.end method

.method public t()Z
    .locals 1

    .line 1
    iget-object v0, p0, Ljk1;->D:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v0

    return v0
.end method

.method public u(Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 4

    .line 1
    iput-object p1, p0, Ljk1;->l:Ljava/util/ArrayList;

    iput-object p2, p0, Ljk1;->m:Ljava/util/ArrayList;

    new-instance v0, Lp3;

    iget-object v1, p0, Ljk1;->a:Landroid/content/Context;

    invoke-virtual {p0, p1}, Ljk1;->i(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lp3;-><init>(Landroid/content/Context;Ljava/util/ArrayList;)V

    new-instance v1, Lp3;

    iget-object v2, p0, Ljk1;->a:Landroid/content/Context;

    invoke-virtual {p0, p2}, Ljk1;->i(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lp3;-><init>(Landroid/content/Context;Ljava/util/ArrayList;)V

    iget-object v2, p0, Ljk1;->u:Landroid/widget/Spinner;

    invoke-virtual {v2, v0}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    iget-object v0, p0, Ljk1;->v:Landroid/widget/Spinner;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    iget-wide v0, p0, Ljk1;->n:D

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    const/4 v1, 0x0

    if-gez v0, :cond_0

    move v0, v1

    :cond_0
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Double;

    invoke-virtual {p1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    iput-wide v2, p0, Ljk1;->n:D

    iget-wide v2, p0, Ljk1;->o:D

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object p1

    invoke-virtual {p2, p1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result p1

    if-gez p1, :cond_1

    goto :goto_0

    :cond_1
    move v1, p1

    :goto_0
    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Double;

    invoke-virtual {p1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide p1

    iput-wide p1, p0, Ljk1;->o:D

    iget-object p1, p0, Ljk1;->u:Landroid/widget/Spinner;

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p1, v0}, Landroid/widget/AdapterView;->setSelection(I)V

    iget-object p1, p0, Ljk1;->v:Landroid/widget/Spinner;

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {p1, v1}, Landroid/widget/AdapterView;->setSelection(I)V

    invoke-virtual {p0}, Ljk1;->x()V

    invoke-virtual {p0}, Ljk1;->y()V

    return-void
.end method

.method public final v()V
    .locals 2

    .line 1
    iget-object v0, p0, Ljk1;->D:Landroid/widget/CheckBox;

    new-instance v1, Ljk1$d;

    invoke-direct {v1, p0}, Ljk1$d;-><init>(Ljk1;)V

    invoke-virtual {v0, v1}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    return-void
.end method

.method public final w()V
    .locals 2

    .line 1
    iget-object v0, p0, Ljk1;->d:Landroid/widget/LinearLayout;

    const v1, 0x7f09008c

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Ljk1$a;

    invoke-direct {v1, p0}, Ljk1$a;-><init>(Ljk1;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Ljk1;->d:Landroid/widget/LinearLayout;

    const v1, 0x7f09008b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Ljk1$b;

    invoke-direct {v1, p0}, Ljk1$b;-><init>(Ljk1;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final x()V
    .locals 2

    .line 1
    iget-object v0, p0, Ljk1;->u:Landroid/widget/Spinner;

    new-instance v1, Ljk1$f;

    invoke-direct {v1, p0}, Ljk1$f;-><init>(Ljk1;)V

    invoke-virtual {v0, v1}, Landroid/widget/AdapterView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    return-void
.end method

.method public final y()V
    .locals 2

    .line 1
    iget-object v0, p0, Ljk1;->v:Landroid/widget/Spinner;

    new-instance v1, Ljk1$g;

    invoke-direct {v1, p0}, Ljk1$g;-><init>(Ljk1;)V

    invoke-virtual {v0, v1}, Landroid/widget/AdapterView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    return-void
.end method

.method public final z()V
    .locals 2

    .line 1
    iget-object v0, p0, Ljk1;->t:Landroid/widget/Spinner;

    new-instance v1, Ljk1$e;

    invoke-direct {v1, p0}, Ljk1$e;-><init>(Ljk1;)V

    invoke-virtual {v0, v1}, Landroid/widget/AdapterView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    return-void
.end method

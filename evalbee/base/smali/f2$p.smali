.class public Lf2$p;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lf2;->z()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Lf2;


# direct methods
.method public constructor <init>(Lf2;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lf2$p;->a:Lf2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 0

    .line 1
    add-int/lit8 p3, p3, -0x1

    iget-object p1, p0, Lf2$p;->a:Lf2;

    iget-object p1, p1, Lf2;->f:Ljava/util/ArrayList;

    invoke-virtual {p1, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lay;

    const p3, 0x7f08008c

    invoke-virtual {p2, p3}, Landroid/view/View;->setBackgroundResource(I)V

    new-instance p2, Landroid/content/Intent;

    iget-object p3, p0, Lf2$p;->a:Lf2;

    iget-object p3, p3, Lf2;->a:Landroid/content/Context;

    const-class p4, Lcom/ekodroid/omrevaluator/exams/ExamDetailActivity;

    invoke-direct {p2, p3, p4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string p3, "EXAM_ID"

    invoke-virtual {p1}, Lay;->b()Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    move-result-object p1

    invoke-virtual {p2, p3, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    iget-object p1, p0, Lf2$p;->a:Lf2;

    invoke-virtual {p1, p2}, Landroidx/fragment/app/Fragment;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

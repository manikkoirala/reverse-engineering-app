.class public Lj20;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Lbn;


# direct methods
.method public constructor <init>(Lbn;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lj20;->a:Lbn;

    return-void
.end method

.method public static a(Lr10;Lr20;Ljr;Ljr;Ljr;)Lj20;
    .locals 18

    .line 1
    invoke-virtual/range {p0 .. p0}, Lr10;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lzl0;->f()Lzl0;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Initializing Firebase Crashlytics "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Lbn;->i()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, " for "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lzl0;->g(Ljava/lang/String;)V

    new-instance v15, Lz00;

    invoke-direct {v15, v0}, Lz00;-><init>(Landroid/content/Context;)V

    new-instance v3, Ldp;

    move-object/from16 v2, p0

    invoke-direct {v3, v2}, Ldp;-><init>(Lr10;)V

    new-instance v14, Lde0;

    move-object/from16 v4, p1

    invoke-direct {v14, v0, v1, v4, v3}, Lde0;-><init>(Landroid/content/Context;Ljava/lang/String;Lr20;Ldp;)V

    new-instance v7, Lgn;

    move-object/from16 v1, p2

    invoke-direct {v7, v1}, Lgn;-><init>(Ljr;)V

    new-instance v1, Ll4;

    move-object/from16 v4, p3

    invoke-direct {v1, v4}, Ll4;-><init>(Ljr;)V

    const-string v4, "Crashlytics Exception Handler"

    invoke-static {v4}, Lvy;->c(Ljava/lang/String;)Ljava/util/concurrent/ExecutorService;

    move-result-object v12

    new-instance v13, Lwm;

    invoke-direct {v13, v3, v15}, Lwm;-><init>(Ldp;Lz00;)V

    invoke-static {v13}, Lcom/google/firebase/sessions/api/FirebaseSessionsDependencies;->e(Lcom/google/firebase/sessions/api/SessionSubscriber;)V

    new-instance v11, Lhd1;

    move-object/from16 v4, p4

    invoke-direct {v11, v4}, Lhd1;-><init>(Ljr;)V

    new-instance v10, Lbn;

    invoke-virtual {v1}, Ll4;->e()Lsc;

    move-result-object v9

    invoke-virtual {v1}, Ll4;->d()Lm4;

    move-result-object v1

    move-object v4, v10

    move-object/from16 v5, p0

    move-object v6, v14

    move-object v8, v3

    move-object/from16 v16, v10

    move-object v10, v1

    move-object v1, v11

    move-object v11, v15

    move-object/from16 v17, v14

    move-object v14, v1

    invoke-direct/range {v4 .. v14}, Lbn;-><init>(Lr10;Lde0;Ldn;Ldp;Lsc;Lm4;Lz00;Ljava/util/concurrent/ExecutorService;Lwm;Lhd1;)V

    invoke-virtual/range {p0 .. p0}, Lr10;->p()Le30;

    move-result-object v1

    invoke-virtual {v1}, Le30;->c()Ljava/lang/String;

    move-result-object v7

    invoke-static {v0}, Lcom/google/firebase/crashlytics/internal/common/CommonUtils;->m(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v0}, Lcom/google/firebase/crashlytics/internal/common/CommonUtils;->j(Landroid/content/Context;)Ljava/util/List;

    move-result-object v5

    invoke-static {}, Lzl0;->f()Lzl0;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Mapping file ID is: "

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lzl0;->b(Ljava/lang/String;)V

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lzc;

    invoke-static {}, Lzl0;->f()Lzl0;

    move-result-object v6

    invoke-virtual {v2}, Lzc;->c()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2}, Lzc;->a()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v2}, Lzc;->b()Ljava/lang/String;

    move-result-object v2

    filled-new-array {v8, v9, v2}, [Ljava/lang/Object;

    move-result-object v2

    const-string v8, "Build id for %s on %s: %s"

    invoke-static {v8, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v6, v2}, Lzl0;->b(Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    new-instance v6, Lts;

    invoke-direct {v6, v0}, Lts;-><init>(Landroid/content/Context;)V

    move-object v1, v0

    move-object/from16 v2, v17

    move-object v8, v3

    move-object v3, v7

    :try_start_0
    invoke-static/range {v1 .. v6}, Ls7;->a(Landroid/content/Context;Lde0;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Lts;)Ls7;

    move-result-object v9
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    invoke-static {}, Lzl0;->f()Lzl0;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Installer package name is: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, v9, Ls7;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lzl0;->i(Ljava/lang/String;)V

    const-string v1, "com.google.firebase.crashlytics.startup"

    invoke-static {v1}, Lvy;->c(Ljava/lang/String;)Ljava/util/concurrent/ExecutorService;

    move-result-object v10

    new-instance v4, Ljd0;

    invoke-direct {v4}, Ljd0;-><init>()V

    iget-object v5, v9, Ls7;->f:Ljava/lang/String;

    iget-object v6, v9, Ls7;->g:Ljava/lang/String;

    move-object v1, v0

    move-object v2, v7

    move-object/from16 v3, v17

    move-object v7, v15

    invoke-static/range {v1 .. v8}, Lcom/google/firebase/crashlytics/internal/settings/a;->l(Landroid/content/Context;Ljava/lang/String;Lde0;Ljd0;Ljava/lang/String;Ljava/lang/String;Lz00;Ldp;)Lcom/google/firebase/crashlytics/internal/settings/a;

    move-result-object v0

    invoke-virtual {v0, v10}, Lcom/google/firebase/crashlytics/internal/settings/a;->p(Ljava/util/concurrent/Executor;)Lcom/google/android/gms/tasks/Task;

    move-result-object v1

    new-instance v2, Lj20$a;

    invoke-direct {v2}, Lj20$a;-><init>()V

    invoke-virtual {v1, v10, v2}, Lcom/google/android/gms/tasks/Task;->continueWith(Ljava/util/concurrent/Executor;Lcom/google/android/gms/tasks/Continuation;)Lcom/google/android/gms/tasks/Task;

    move-object/from16 v1, v16

    invoke-virtual {v1, v9, v0}, Lbn;->n(Ls7;Lzm1;)Z

    move-result v2

    new-instance v3, Lj20$b;

    invoke-direct {v3, v2, v1, v0}, Lj20$b;-><init>(ZLbn;Lcom/google/firebase/crashlytics/internal/settings/a;)V

    invoke-static {v10, v3}, Lcom/google/android/gms/tasks/Tasks;->call(Ljava/util/concurrent/Executor;Ljava/util/concurrent/Callable;)Lcom/google/android/gms/tasks/Task;

    new-instance v0, Lj20;

    invoke-direct {v0, v1}, Lj20;-><init>(Lbn;)V

    return-object v0

    :catch_0
    move-exception v0

    move-object v1, v0

    invoke-static {}, Lzl0;->f()Lzl0;

    move-result-object v0

    const-string v2, "Error retrieving app package info."

    invoke-virtual {v0, v2, v1}, Lzl0;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    const/4 v0, 0x0

    return-object v0
.end method

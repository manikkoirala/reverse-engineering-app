.class public Lzm$b$a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/tasks/SuccessContinuation;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lzm$b;->a()Lcom/google/android/gms/tasks/Task;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Ljava/util/concurrent/Executor;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Lzm$b;


# direct methods
.method public constructor <init>(Lzm$b;Ljava/util/concurrent/Executor;Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lzm$b$a;->c:Lzm$b;

    iput-object p2, p0, Lzm$b$a;->a:Ljava/util/concurrent/Executor;

    iput-object p3, p0, Lzm$b$a;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lvm1;)Lcom/google/android/gms/tasks/Task;
    .locals 4

    .line 1
    const/4 v0, 0x0

    if-nez p1, :cond_0

    invoke-static {}, Lzl0;->f()Lzl0;

    move-result-object p1

    const-string v1, "Received null app settings, cannot send reports at crash time."

    invoke-virtual {p1, v1}, Lzl0;->k(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/google/android/gms/tasks/Tasks;->forResult(Ljava/lang/Object;)Lcom/google/android/gms/tasks/Task;

    move-result-object p1

    return-object p1

    :cond_0
    const/4 p1, 0x2

    new-array p1, p1, [Lcom/google/android/gms/tasks/Task;

    iget-object v1, p0, Lzm$b$a;->c:Lzm$b;

    iget-object v1, v1, Lzm$b;->f:Lzm;

    invoke-static {v1}, Lzm;->n(Lzm;)Lcom/google/android/gms/tasks/Task;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, p1, v2

    iget-object v1, p0, Lzm$b$a;->c:Lzm$b;

    iget-object v1, v1, Lzm$b;->f:Lzm;

    invoke-static {v1}, Lzm;->h(Lzm;)Lnm1;

    move-result-object v1

    iget-object v2, p0, Lzm$b$a;->a:Ljava/util/concurrent/Executor;

    iget-object v3, p0, Lzm$b$a;->c:Lzm$b;

    iget-boolean v3, v3, Lzm$b;->e:Z

    if-eqz v3, :cond_1

    iget-object v0, p0, Lzm$b$a;->b:Ljava/lang/String;

    :cond_1
    invoke-virtual {v1, v2, v0}, Lnm1;->x(Ljava/util/concurrent/Executor;Ljava/lang/String;)Lcom/google/android/gms/tasks/Task;

    move-result-object v0

    const/4 v1, 0x1

    aput-object v0, p1, v1

    invoke-static {p1}, Lcom/google/android/gms/tasks/Tasks;->whenAll([Lcom/google/android/gms/tasks/Task;)Lcom/google/android/gms/tasks/Task;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic then(Ljava/lang/Object;)Lcom/google/android/gms/tasks/Task;
    .locals 0

    .line 1
    check-cast p1, Lvm1;

    invoke-virtual {p0, p1}, Lzm$b$a;->a(Lvm1;)Lcom/google/android/gms/tasks/Task;

    move-result-object p1

    return-object p1
.end method

.class public Lp61;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:Lee1;

.field public b:Ly01;

.field public c:Lcom/ekodroid/omrevaluator/exams/sms/SmsData;

.field public d:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/ekodroid/omrevaluator/exams/sms/SmsData;Lee1;Ly01;)V
    .locals 2

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "https://"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, La91;->u()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ":8759/smsMessages2"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lp61;->d:Ljava/lang/String;

    iput-object p2, p0, Lp61;->a:Lee1;

    iput-object p3, p0, Lp61;->b:Ly01;

    iput-object p1, p0, Lp61;->c:Lcom/ekodroid/omrevaluator/exams/sms/SmsData;

    invoke-virtual {p0}, Lp61;->f()V

    return-void
.end method

.method public static synthetic a(Lp61;Ljava/lang/Object;)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lp61;->e(Ljava/lang/Object;)V

    return-void
.end method

.method public static synthetic b(Lp61;)Ly01;
    .locals 0

    .line 1
    iget-object p0, p0, Lp61;->b:Ly01;

    return-object p0
.end method

.method public static synthetic c(Lp61;)Lcom/ekodroid/omrevaluator/exams/sms/SmsData;
    .locals 0

    .line 1
    iget-object p0, p0, Lp61;->c:Lcom/ekodroid/omrevaluator/exams/sms/SmsData;

    return-object p0
.end method


# virtual methods
.method public d()V
    .locals 1

    .line 1
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lp61;->e(Ljava/lang/Object;)V

    iput-object v0, p0, Lp61;->b:Ly01;

    return-void
.end method

.method public final e(Ljava/lang/Object;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lp61;->b:Ly01;

    if-eqz v0, :cond_0

    invoke-interface {v0, p1}, Ly01;->a(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public final f()V
    .locals 7

    .line 1
    new-instance v6, Lp61$c;

    const/4 v2, 0x1

    iget-object v3, p0, Lp61;->d:Ljava/lang/String;

    new-instance v4, Lp61$a;

    invoke-direct {v4, p0}, Lp61$a;-><init>(Lp61;)V

    new-instance v5, Lp61$b;

    invoke-direct {v5, p0}, Lp61$b;-><init>(Lp61;)V

    move-object v0, v6

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lp61$c;-><init>(Lp61;ILjava/lang/String;Lcom/android/volley/d$b;Lcom/android/volley/d$a;)V

    new-instance v0, Lwq;

    const/4 v1, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    const v3, 0xea60

    invoke-direct {v0, v3, v1, v2}, Lwq;-><init>(IIF)V

    invoke-virtual {v6, v0}, Lcom/android/volley/Request;->L(Ljf1;)Lcom/android/volley/Request;

    iget-object v0, p0, Lp61;->a:Lee1;

    invoke-virtual {v0, v6}, Lee1;->a(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    return-void
.end method

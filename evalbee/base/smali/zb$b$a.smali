.class public Lzb$b$a;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lzb$b;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation


# instance fields
.field public a:Lt81;

.field public b:Ljava/lang/String;


# direct methods
.method public synthetic constructor <init>(Lrc2;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static bridge synthetic c(Lzb$b$a;)Lt81;
    .locals 0

    .line 1
    iget-object p0, p0, Lzb$b$a;->a:Lt81;

    return-object p0
.end method

.method public static bridge synthetic d(Lzb$b$a;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lzb$b$a;->b:Ljava/lang/String;

    return-object p0
.end method


# virtual methods
.method public a()Lzb$b;
    .locals 2

    .line 1
    iget-object v0, p0, Lzb$b$a;->a:Lt81;

    const-string v1, "ProductDetails is required for constructing ProductDetailsParams."

    invoke-static {v0, v1}, Lcom/google/android/gms/internal/play_billing/zzx;->zzc(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lzb$b$a;->b:Ljava/lang/String;

    const-string v1, "offerToken is required for constructing ProductDetailsParams."

    invoke-static {v0, v1}, Lcom/google/android/gms/internal/play_billing/zzx;->zzc(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lzb$b;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lzb$b;-><init>(Lzb$b$a;Ltc2;)V

    return-object v0
.end method

.method public b(Lt81;)Lzb$b$a;
    .locals 1

    .line 1
    iput-object p1, p0, Lzb$b$a;->a:Lt81;

    invoke-virtual {p1}, Lt81;->a()Lt81$a;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lt81;->a()Lt81$a;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-virtual {p1}, Lt81;->a()Lt81$a;

    move-result-object p1

    invoke-virtual {p1}, Lt81$a;->b()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lzb$b$a;->b:Ljava/lang/String;

    :cond_0
    return-object p0
.end method

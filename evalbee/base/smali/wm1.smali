.class public Lwm1;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Lio;


# direct methods
.method public constructor <init>(Lio;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lwm1;->a:Lio;

    return-void
.end method

.method public static a(I)Lxm1;
    .locals 3

    .line 1
    const/4 v0, 0x3

    if-eq p0, v0, :cond_0

    invoke-static {}, Lzl0;->f()Lzl0;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Could not determine SettingsJsonTransform for settings version "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p0, ". Using default settings values."

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, p0}, Lzl0;->d(Ljava/lang/String;)V

    new-instance p0, Lbr;

    invoke-direct {p0}, Lbr;-><init>()V

    return-object p0

    :cond_0
    new-instance p0, Lcn1;

    invoke-direct {p0}, Lcn1;-><init>()V

    return-object p0
.end method


# virtual methods
.method public b(Lorg/json/JSONObject;)Lvm1;
    .locals 2

    .line 1
    const-string v0, "settings_version"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Lwm1;->a(I)Lxm1;

    move-result-object v0

    iget-object v1, p0, Lwm1;->a:Lio;

    invoke-interface {v0, v1, p1}, Lxm1;->a(Lio;Lorg/json/JSONObject;)Lvm1;

    move-result-object p1

    return-object p1
.end method

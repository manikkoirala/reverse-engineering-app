.class public final Lf4;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf4$a;
    }
.end annotation


# static fields
.field public static final d:Lf4$a;

.field public static final e:Lf4;


# instance fields
.field public final a:Ljava/util/List;

.field public final b:I

.field public final c:I


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .line 1
    new-instance v0, Lf4$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lf4$a;-><init>(Lgq;)V

    sput-object v0, Lf4;->d:Lf4$a;

    new-instance v0, Lf4;

    invoke-static {}, Lnh;->g()Ljava/util/List;

    move-result-object v1

    const v2, 0x7fffffff

    invoke-direct {v0, v1, v2, v2}, Lf4;-><init>(Ljava/util/List;II)V

    sput-object v0, Lf4;->e:Lf4;

    return-void
.end method

.method public constructor <init>(Ljava/util/List;II)V
    .locals 1

    .line 1
    const-string v0, "matches"

    invoke-static {p1, v0}, Lfg0;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lf4;->a:Ljava/util/List;

    iput p2, p0, Lf4;->b:I

    iput p3, p0, Lf4;->c:I

    return-void
.end method


# virtual methods
.method public a(Lf4;)I
    .locals 2

    .line 1
    const-string v0, "other"

    invoke-static {p1, v0}, Lfg0;->e(Ljava/lang/Object;Ljava/lang/String;)V

    iget v0, p0, Lf4;->c:I

    iget v1, p1, Lf4;->c:I

    invoke-static {v0, v1}, Lfg0;->f(II)I

    move-result v0

    if-eqz v0, :cond_0

    return v0

    :cond_0
    iget v0, p0, Lf4;->b:I

    iget p1, p1, Lf4;->b:I

    invoke-static {v0, p1}, Lfg0;->f(II)I

    move-result p1

    return p1
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 0

    .line 1
    check-cast p1, Lf4;

    invoke-virtual {p0, p1}, Lf4;->a(Lf4;)I

    move-result p1

    return p1
.end method

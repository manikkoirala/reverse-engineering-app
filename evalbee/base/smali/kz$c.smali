.class public Lkz$c;
.super Lo1;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkz;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "c"
.end annotation


# instance fields
.field public final synthetic b:Lkz;


# direct methods
.method public constructor <init>(Lkz;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lkz$c;->b:Lkz;

    invoke-direct {p0}, Lo1;-><init>()V

    return-void
.end method


# virtual methods
.method public b(I)Ln1;
    .locals 1

    .line 1
    iget-object v0, p0, Lkz$c;->b:Lkz;

    invoke-virtual {v0, p1}, Lkz;->obtainAccessibilityNodeInfo(I)Ln1;

    move-result-object p1

    invoke-static {p1}, Ln1;->M(Ln1;)Ln1;

    move-result-object p1

    return-object p1
.end method

.method public d(I)Ln1;
    .locals 1

    .line 1
    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    iget-object p1, p0, Lkz$c;->b:Lkz;

    iget p1, p1, Lkz;->mAccessibilityFocusedVirtualViewId:I

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lkz$c;->b:Lkz;

    iget p1, p1, Lkz;->mKeyboardFocusedVirtualViewId:I

    :goto_0
    const/high16 v0, -0x80000000

    if-ne p1, v0, :cond_1

    const/4 p1, 0x0

    return-object p1

    :cond_1
    invoke-virtual {p0, p1}, Lkz$c;->b(I)Ln1;

    move-result-object p1

    return-object p1
.end method

.method public f(IILandroid/os/Bundle;)Z
    .locals 1

    .line 1
    iget-object v0, p0, Lkz$c;->b:Lkz;

    invoke-virtual {v0, p1, p2, p3}, Lkz;->performAction(IILandroid/os/Bundle;)Z

    move-result p1

    return p1
.end method

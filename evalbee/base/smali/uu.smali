.class public Luu;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# instance fields
.field public a:Landroid/content/Context;

.field public b:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

.field public c:Z

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

.field public g:Landroid/app/ProgressDialog;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/ekodroid/omrevaluator/templateui/models/ExamId;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    iput-object p1, p0, Luu;->a:Landroid/content/Context;

    iput-object p3, p0, Luu;->b:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    iput-object p2, p0, Luu;->f:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {p3}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getName()Ljava/lang/String;

    move-result-object p2

    const-string p3, "[^a-zA-Z0-9_-]"

    const-string v0, "_"

    invoke-virtual {p2, p3, v0}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    iput-object p2, p0, Luu;->d:Ljava/lang/String;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object p3, Landroid/os/Environment;->DIRECTORY_DOCUMENTS:Ljava/lang/String;

    invoke-virtual {p1, p3}, Landroid/content/Context;->getExternalFilesDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object p1

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p1, "/"

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, p0, Luu;->d:Ljava/lang/String;

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "_key.pdf"

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Luu;->e:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final a(Lni0;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)V
    .locals 16

    .line 1
    move-object/from16 v6, p0

    move-object/from16 v7, p1

    invoke-virtual/range {p2 .. p2}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getLabelProfile()Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    move-result-object v8

    invoke-virtual {v8}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getQueNoString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v8}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getCorrectString()Ljava/lang/String;

    move-result-object v1

    const/4 v9, 0x0

    invoke-virtual {v7, v0, v1, v9}, Lni0;->d(Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-virtual/range {p2 .. p2}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getAnswerKeys()[Lcom/ekodroid/omrevaluator/templateui/models/AnswerSetKey;

    move-result-object v10

    if-eqz v10, :cond_6

    array-length v0, v10

    if-nez v0, :cond_0

    goto/16 :goto_3

    :cond_0
    move v11, v9

    :goto_0
    array-length v0, v10

    if-ge v11, v0, :cond_6

    array-length v0, v10

    const/4 v1, 0x1

    if-le v0, v1, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getExamSetString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " - "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getExamSetLabels()[Ljava/lang/String;

    move-result-object v2

    aget-object v2, v2, v11

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0, v1}, Lni0;->e(Ljava/lang/String;Z)V

    :cond_1
    invoke-virtual/range {p2 .. p2}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getInvalidQuestionSets()[Lcom/ekodroid/omrevaluator/templateui/models/InvalidQuestionSet;

    move-result-object v0

    if-eqz v0, :cond_2

    array-length v1, v0

    if-le v1, v11, :cond_2

    aget-object v0, v0, v11

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/templateui/models/InvalidQuestionSet;->getInvalidQuestions()[I

    move-result-object v0

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    :goto_1
    move-object v12, v0

    invoke-virtual/range {p2 .. p2}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getTemplateParams()Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;

    move-result-object v0

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;->getSubjects()[Lcom/ekodroid/omrevaluator/templateui/models/Subject2;

    move-result-object v0

    invoke-virtual {v6, v7, v0, v9, v9}, Luu;->e(Lni0;[Lcom/ekodroid/omrevaluator/templateui/models/Subject2;II)V

    move v0, v9

    move v1, v0

    move v13, v1

    :goto_2
    aget-object v2, v10, v11

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerSetKey;->getAnswerOptionKeys()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v13, v2, :cond_5

    aget-object v2, v10, v11

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerSetKey;->getAnswerOptionKeys()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, v13}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;->getSubjectId()I

    move-result v3

    if-ne v0, v3, :cond_3

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;->getSectionId()I

    move-result v3

    if-eq v1, v3, :cond_4

    :cond_3
    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;->getSubjectId()I

    move-result v0

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;->getSectionId()I

    move-result v1

    invoke-virtual/range {p2 .. p2}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getTemplateParams()Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;

    move-result-object v3

    invoke-virtual {v3}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;->getSubjects()[Lcom/ekodroid/omrevaluator/templateui/models/Subject2;

    move-result-object v3

    invoke-virtual {v6, v7, v3, v0, v1}, Luu;->e(Lni0;[Lcom/ekodroid/omrevaluator/templateui/models/Subject2;II)V

    :cond_4
    move v14, v0

    move v15, v1

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v3, p2

    move v4, v13

    move-object v5, v12

    invoke-virtual/range {v0 .. v5}, Luu;->c(Lni0;Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;I[I)V

    add-int/lit8 v13, v13, 0x1

    move v0, v14

    move v1, v15

    goto :goto_2

    :cond_5
    add-int/lit8 v11, v11, 0x1

    goto/16 :goto_0

    :cond_6
    :goto_3
    return-void
.end method

.method public final b(Lni0;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)V
    .locals 1

    .line 1
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, p0, Luu;->f:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;->getExamName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "_"

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Luu;->f:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;->getExamDate()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "_key"

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Lni0;->a(Ljava/lang/String;)V

    return-void
.end method

.method public final c(Lni0;Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;I[I)V
    .locals 5

    .line 1
    invoke-virtual {p3}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getLabelProfile()Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    move-result-object v0

    invoke-virtual {p2}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;->getQuestionNumber()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getQuestionNumberLabel(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getLabelProfile()Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    move-result-object p3

    invoke-static {p2, p3}, Le5;->q(Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;)Ljava/lang/String;

    move-result-object p2

    const/4 p3, 0x0

    if-eqz p5, :cond_1

    array-length v1, p5

    if-lez v1, :cond_1

    array-length v1, p5

    move v2, p3

    :goto_0
    if-ge v2, v1, :cond_1

    aget v3, p5, v2

    add-int/lit8 v4, p4, 0x1

    if-ne v3, v4, :cond_0

    const-string p2, "Invalid"

    invoke-virtual {p1, v0, p2, p3}, Lni0;->c(Ljava/lang/String;Ljava/lang/String;Z)V

    return-void

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {p1, v0, p2, p3}, Lni0;->c(Ljava/lang/String;Ljava/lang/String;Z)V

    return-void
.end method

.method public final d(Lorg/apache/pdfbox/pdmodel/graphics/image/PDImageXObject;Lorg/apache/pdfbox/pdmodel/PDPage;Lorg/apache/pdfbox/pdmodel/PDPageContentStream;)V
    .locals 10

    .line 1
    invoke-virtual {p2}, Lorg/apache/pdfbox/pdmodel/PDPage;->getBBox()Lorg/apache/pdfbox/pdmodel/common/PDRectangle;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->getWidth()F

    move-result v0

    const/high16 v1, 0x41a00000    # 20.0f

    sub-float/2addr v0, v1

    invoke-virtual {p2}, Lorg/apache/pdfbox/pdmodel/PDPage;->getBBox()Lorg/apache/pdfbox/pdmodel/common/PDRectangle;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->getHeight()F

    move-result v2

    sub-float/2addr v2, v1

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/graphics/image/PDImageXObject;->getWidth()I

    move-result v1

    const/16 v3, 0x320

    if-le v1, v3, :cond_0

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/graphics/image/PDImageXObject;->getWidth()I

    move-result v3

    :cond_0
    div-float v1, v0, v2

    float-to-double v4, v1

    :try_start_0
    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/graphics/image/PDImageXObject;->getWidth()I

    move-result v1

    int-to-double v6, v1

    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    mul-double/2addr v6, v8

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/graphics/image/PDImageXObject;->getHeight()I

    move-result v1

    int-to-double v8, v1

    div-double/2addr v6, v8

    cmpl-double v1, v4, v6

    if-lez v1, :cond_1

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/graphics/image/PDImageXObject;->getHeight()I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v2, v0

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/graphics/image/PDImageXObject;->getWidth()I

    move-result v1

    int-to-float v1, v1

    div-float v2, v0, v1

    :goto_0
    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/graphics/image/PDImageXObject;->getWidth()I

    move-result v0

    int-to-float v0, v0

    mul-float/2addr v2, v0

    int-to-float v0, v3

    div-float/2addr v2, v0

    invoke-virtual {p2}, Lorg/apache/pdfbox/pdmodel/PDPage;->getBBox()Lorg/apache/pdfbox/pdmodel/common/PDRectangle;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->getWidth()F

    move-result v0

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/graphics/image/PDImageXObject;->getWidth()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v1, v2

    sub-float/2addr v0, v1

    const/high16 v1, 0x40000000    # 2.0f

    div-float v5, v0, v1

    invoke-virtual {p2}, Lorg/apache/pdfbox/pdmodel/PDPage;->getBBox()Lorg/apache/pdfbox/pdmodel/common/PDRectangle;

    move-result-object p2

    invoke-virtual {p2}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->getHeight()F

    move-result p2

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/graphics/image/PDImageXObject;->getHeight()I

    move-result v0

    int-to-float v0, v0

    mul-float/2addr v0, v2

    sub-float/2addr p2, v0

    div-float v6, p2, v1

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/graphics/image/PDImageXObject;->getWidth()I

    move-result p2

    int-to-float p2, p2

    mul-float v7, p2, v2

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/graphics/image/PDImageXObject;->getHeight()I

    move-result p2

    int-to-float p2, p2

    mul-float v8, p2, v2

    move-object v3, p3

    move-object v4, p1

    invoke-virtual/range {v3 .. v8}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->drawImage(Lorg/apache/pdfbox/pdmodel/graphics/image/PDImageXObject;FFFF)V

    const-string p1, "TAG"

    const-string p2, "image converted to pdf"

    invoke-static {p1, p2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V

    :goto_1
    return-void
.end method

.method public bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 1
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Luu;->g([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object p1

    return-object p1
.end method

.method public final e(Lni0;[Lcom/ekodroid/omrevaluator/templateui/models/Subject2;II)V
    .locals 1

    .line 1
    aget-object p2, p2, p3

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2}, Lcom/ekodroid/omrevaluator/templateui/models/Subject2;->getSubName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " - "

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Lcom/ekodroid/omrevaluator/templateui/models/Subject2;->getSections()[Lcom/ekodroid/omrevaluator/templateui/models/Section2;

    move-result-object p2

    aget-object p2, p2, p4

    invoke-virtual {p2}, Lcom/ekodroid/omrevaluator/templateui/models/Section2;->getName()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    const/4 p3, 0x0

    invoke-virtual {p1, p2, p3}, Lni0;->e(Ljava/lang/String;Z)V

    return-void
.end method

.method public final f(Ljava/lang/String;)Z
    .locals 6

    .line 1
    new-instance v0, Lorg/apache/pdfbox/pdmodel/PDDocument;

    invoke-direct {v0}, Lorg/apache/pdfbox/pdmodel/PDDocument;-><init>()V

    new-instance v1, Lni0;

    invoke-direct {v1}, Lni0;-><init>()V

    :try_start_0
    iget-object v2, p0, Luu;->b:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    invoke-virtual {p0, v1, v2}, Luu;->b(Lni0;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)V

    iget-object v2, p0, Luu;->b:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    invoke-virtual {p0, v1, v2}, Luu;->a(Lni0;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)V

    invoke-virtual {v1}, Lni0;->g()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/Bitmap;

    new-instance v3, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v3}, Ljava/io/ByteArrayOutputStream;-><init>()V

    sget-object v4, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v5, 0x64

    invoke-virtual {v2, v4, v5, v3}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v3

    new-instance v4, Ljava/io/ByteArrayInputStream;

    invoke-direct {v4, v3}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-static {v0, v4}, Lorg/apache/pdfbox/pdmodel/graphics/image/JPEGFactory;->createFromStream(Lorg/apache/pdfbox/pdmodel/PDDocument;Ljava/io/InputStream;)Lorg/apache/pdfbox/pdmodel/graphics/image/PDImageXObject;

    move-result-object v3

    new-instance v4, Lorg/apache/pdfbox/pdmodel/PDPage;

    sget-object v5, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->A4:Lorg/apache/pdfbox/pdmodel/common/PDRectangle;

    invoke-direct {v4, v5}, Lorg/apache/pdfbox/pdmodel/PDPage;-><init>(Lorg/apache/pdfbox/pdmodel/common/PDRectangle;)V

    invoke-virtual {v0, v4}, Lorg/apache/pdfbox/pdmodel/PDDocument;->addPage(Lorg/apache/pdfbox/pdmodel/PDPage;)V

    new-instance v5, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;

    invoke-direct {v5, v0, v4}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;-><init>(Lorg/apache/pdfbox/pdmodel/PDDocument;Lorg/apache/pdfbox/pdmodel/PDPage;)V

    invoke-virtual {p0, v3, v4, v5}, Luu;->d(Lorg/apache/pdfbox/pdmodel/graphics/image/PDImageXObject;Lorg/apache/pdfbox/pdmodel/PDPage;Lorg/apache/pdfbox/pdmodel/PDPageContentStream;)V

    invoke-virtual {v5}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->close()V

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_0

    :cond_0
    invoke-virtual {v0, p1}, Lorg/apache/pdfbox/pdmodel/PDDocument;->save(Ljava/lang/String;)V

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/PDDocument;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const/4 p1, 0x1

    return p1

    :catch_0
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V

    const/4 p1, 0x0

    return p1
.end method

.method public varargs g([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 0

    .line 1
    iget-object p1, p0, Luu;->e:Ljava/lang/String;

    invoke-virtual {p0, p1}, Luu;->f(Ljava/lang/String;)Z

    move-result p1

    iput-boolean p1, p0, Luu;->c:Z

    const/4 p1, 0x0

    return-object p1
.end method

.method public h(Ljava/lang/Void;)V
    .locals 3

    .line 1
    iget-object p1, p0, Luu;->g:Landroid/app/ProgressDialog;

    invoke-virtual {p1}, Landroid/app/Dialog;->isShowing()Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Luu;->g:Landroid/app/ProgressDialog;

    invoke-virtual {p1}, Landroid/app/Dialog;->dismiss()V

    :cond_0
    iget-boolean p1, p0, Luu;->c:Z

    if-eqz p1, :cond_2

    new-instance p1, Landroid/content/Intent;

    const-string v0, "android.intent.action.VIEW"

    invoke-direct {p1, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Luu;->e:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Luu;->a:Landroid/content/Context;

    const-string v2, "com.ekodroid.omrevaluator.fileprovider"

    invoke-static {v1, v2, v0}, Landroidx/core/content/FileProvider;->f(Landroid/content/Context;Ljava/lang/String;Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    const-string v1, "android.intent.extra.STREAM"

    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string v1, "application/pdf"

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    const/high16 v0, 0x40000000    # 2.0f

    invoke-virtual {p1, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    iget-object v0, p0, Luu;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_1

    iget-object v0, p0, Luu;->a:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :cond_1
    iget-object p1, p0, Luu;->a:Landroid/content/Context;

    const v0, 0x7f0800bd

    const v1, 0x7f08016e

    const v2, 0x7f12026b

    invoke-static {p1, v2, v0, v1}, La91;->G(Landroid/content/Context;III)V

    :cond_2
    :goto_0
    return-void
.end method

.method public varargs i([Ljava/lang/Integer;)V
    .locals 0

    .line 1
    return-void
.end method

.method public bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 1
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Luu;->h(Ljava/lang/Void;)V

    return-void
.end method

.method public onPreExecute()V
    .locals 2

    .line 1
    new-instance v0, Landroid/app/ProgressDialog;

    iget-object v1, p0, Luu;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Luu;->g:Landroid/app/ProgressDialog;

    const-string v1, "Creating pdf, please wait..."

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Luu;->g:Landroid/app/ProgressDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setCancelable(Z)V

    iget-object v0, p0, Luu;->g:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    return-void
.end method

.method public bridge synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0

    .line 1
    check-cast p1, [Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Luu;->i([Ljava/lang/Integer;)V

    return-void
.end method

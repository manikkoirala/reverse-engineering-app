.class public Ln80;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroidx/lifecycle/c;
.implements Laj1;
.implements Lc42;


# instance fields
.field public final a:Landroidx/fragment/app/Fragment;

.field public final b:Lb42;

.field public c:Landroidx/lifecycle/g;

.field public d:Lzi1;


# direct methods
.method public constructor <init>(Landroidx/fragment/app/Fragment;Lb42;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Ln80;->c:Landroidx/lifecycle/g;

    iput-object v0, p0, Ln80;->d:Lzi1;

    iput-object p1, p0, Ln80;->a:Landroidx/fragment/app/Fragment;

    iput-object p2, p0, Ln80;->b:Lb42;

    return-void
.end method


# virtual methods
.method public a(Landroidx/lifecycle/Lifecycle$Event;)V
    .locals 1

    .line 1
    iget-object v0, p0, Ln80;->c:Landroidx/lifecycle/g;

    invoke-virtual {v0, p1}, Landroidx/lifecycle/g;->h(Landroidx/lifecycle/Lifecycle$Event;)V

    return-void
.end method

.method public b()V
    .locals 1

    .line 1
    iget-object v0, p0, Ln80;->c:Landroidx/lifecycle/g;

    if-nez v0, :cond_0

    new-instance v0, Landroidx/lifecycle/g;

    invoke-direct {v0, p0}, Landroidx/lifecycle/g;-><init>(Lqj0;)V

    iput-object v0, p0, Ln80;->c:Landroidx/lifecycle/g;

    invoke-static {p0}, Lzi1;->a(Laj1;)Lzi1;

    move-result-object v0

    iput-object v0, p0, Ln80;->d:Lzi1;

    :cond_0
    return-void
.end method

.method public c()Z
    .locals 1

    .line 1
    iget-object v0, p0, Ln80;->c:Landroidx/lifecycle/g;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public d(Landroid/os/Bundle;)V
    .locals 1

    .line 1
    iget-object v0, p0, Ln80;->d:Lzi1;

    invoke-virtual {v0, p1}, Lzi1;->d(Landroid/os/Bundle;)V

    return-void
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 1

    .line 1
    iget-object v0, p0, Ln80;->d:Lzi1;

    invoke-virtual {v0, p1}, Lzi1;->e(Landroid/os/Bundle;)V

    return-void
.end method

.method public f(Landroidx/lifecycle/Lifecycle$State;)V
    .locals 1

    .line 1
    iget-object v0, p0, Ln80;->c:Landroidx/lifecycle/g;

    invoke-virtual {v0, p1}, Landroidx/lifecycle/g;->n(Landroidx/lifecycle/Lifecycle$State;)V

    return-void
.end method

.method public getLifecycle()Landroidx/lifecycle/Lifecycle;
    .locals 1

    .line 1
    invoke-virtual {p0}, Ln80;->b()V

    iget-object v0, p0, Ln80;->c:Landroidx/lifecycle/g;

    return-object v0
.end method

.method public getSavedStateRegistry()Landroidx/savedstate/a;
    .locals 1

    .line 1
    invoke-virtual {p0}, Ln80;->b()V

    iget-object v0, p0, Ln80;->d:Lzi1;

    invoke-virtual {v0}, Lzi1;->b()Landroidx/savedstate/a;

    move-result-object v0

    return-object v0
.end method

.method public getViewModelStore()Lb42;
    .locals 1

    .line 1
    invoke-virtual {p0}, Ln80;->b()V

    iget-object v0, p0, Ln80;->b:Lb42;

    return-object v0
.end method

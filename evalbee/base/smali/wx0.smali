.class public abstract Lwx0;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Ldu;

.field public final b:Lh71;

.field public final c:Ljava/util/List;


# direct methods
.method public constructor <init>(Ldu;Lh71;)V
    .locals 1

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-direct {p0, p1, p2, v0}, Lwx0;-><init>(Ldu;Lh71;Ljava/util/List;)V

    return-void
.end method

.method public constructor <init>(Ldu;Lh71;Ljava/util/List;)V
    .locals 0

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lwx0;->a:Ldu;

    iput-object p2, p0, Lwx0;->b:Lh71;

    iput-object p3, p0, Lwx0;->c:Ljava/util/List;

    return-void
.end method

.method public static c(Lcom/google/firebase/firestore/model/MutableDocument;Lq00;)Lwx0;
    .locals 6

    .line 1
    invoke-virtual {p0}, Lcom/google/firebase/firestore/model/MutableDocument;->g()Z

    move-result v0

    if-eqz v0, :cond_6

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lq00;->c()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    goto/16 :goto_1

    :cond_0
    if-nez p1, :cond_2

    invoke-virtual {p0}, Lcom/google/firebase/firestore/model/MutableDocument;->c()Z

    move-result p1

    if-eqz p1, :cond_1

    new-instance p1, Lhs;

    invoke-virtual {p0}, Lcom/google/firebase/firestore/model/MutableDocument;->getKey()Ldu;

    move-result-object p0

    sget-object v0, Lh71;->c:Lh71;

    invoke-direct {p1, p0, v0}, Lhs;-><init>(Ldu;Lh71;)V

    return-object p1

    :cond_1
    new-instance p1, Lqm1;

    invoke-virtual {p0}, Lcom/google/firebase/firestore/model/MutableDocument;->getKey()Ldu;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/firebase/firestore/model/MutableDocument;->getData()La11;

    move-result-object p0

    sget-object v1, Lh71;->c:Lh71;

    invoke-direct {p1, v0, p0, v1}, Lqm1;-><init>(Ldu;La11;Lh71;)V

    return-object p1

    :cond_2
    invoke-virtual {p0}, Lcom/google/firebase/firestore/model/MutableDocument;->getData()La11;

    move-result-object v0

    new-instance v1, La11;

    invoke-direct {v1}, La11;-><init>()V

    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    invoke-virtual {p1}, Lq00;->c()Ljava/util/Set;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_3
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ls00;

    invoke-virtual {v2, v3}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_3

    invoke-virtual {v0, v3}, La11;->j(Ls00;)Lcom/google/firestore/v1/Value;

    move-result-object v4

    if-nez v4, :cond_4

    invoke-virtual {v3}, Ljb;->l()I

    move-result v4

    const/4 v5, 0x1

    if-le v4, v5, :cond_4

    invoke-virtual {v3}, Ljb;->n()Ljb;

    move-result-object v3

    check-cast v3, Ls00;

    :cond_4
    invoke-virtual {v0, v3}, La11;->j(Ls00;)Lcom/google/firestore/v1/Value;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, La11;->m(Ls00;Lcom/google/firestore/v1/Value;)V

    invoke-virtual {v2, v3}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_5
    new-instance p1, Le31;

    invoke-virtual {p0}, Lcom/google/firebase/firestore/model/MutableDocument;->getKey()Ldu;

    move-result-object p0

    invoke-static {v2}, Lq00;->b(Ljava/util/Set;)Lq00;

    move-result-object v0

    sget-object v2, Lh71;->c:Lh71;

    invoke-direct {p1, p0, v1, v0, v2}, Le31;-><init>(Ldu;La11;Lq00;Lh71;)V

    return-object p1

    :cond_6
    :goto_1
    const/4 p0, 0x0

    return-object p0
.end method


# virtual methods
.method public abstract a(Lcom/google/firebase/firestore/model/MutableDocument;Lq00;Lpw1;)Lq00;
.end method

.method public abstract b(Lcom/google/firebase/firestore/model/MutableDocument;Lay0;)V
.end method

.method public d(Lzt;)La11;
    .locals 5

    .line 1
    iget-object v0, p0, Lwx0;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v1, 0x0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lu00;

    invoke-virtual {v2}, Lu00;->a()Ls00;

    move-result-object v3

    invoke-interface {p1, v3}, Lzt;->j(Ls00;)Lcom/google/firestore/v1/Value;

    move-result-object v3

    invoke-virtual {v2}, Lu00;->b()Loy1;

    move-result-object v4

    invoke-interface {v4, v3}, Loy1;->a(Lcom/google/firestore/v1/Value;)Lcom/google/firestore/v1/Value;

    move-result-object v3

    if-eqz v3, :cond_0

    if-nez v1, :cond_1

    new-instance v1, La11;

    invoke-direct {v1}, La11;-><init>()V

    :cond_1
    invoke-virtual {v2}, Lu00;->a()Ls00;

    move-result-object v2

    invoke-virtual {v1, v2, v3}, La11;->m(Ls00;Lcom/google/firestore/v1/Value;)V

    goto :goto_0

    :cond_2
    return-object v1
.end method

.method public abstract e()Lq00;
.end method

.method public f()Ljava/util/List;
    .locals 1

    .line 1
    iget-object v0, p0, Lwx0;->c:Ljava/util/List;

    return-object v0
.end method

.method public g()Ldu;
    .locals 1

    .line 1
    iget-object v0, p0, Lwx0;->a:Ldu;

    return-object v0
.end method

.method public h()Lh71;
    .locals 1

    .line 1
    iget-object v0, p0, Lwx0;->b:Lh71;

    return-object v0
.end method

.method public i(Lwx0;)Z
    .locals 2

    .line 1
    iget-object v0, p0, Lwx0;->a:Ldu;

    iget-object v1, p1, Lwx0;->a:Ldu;

    invoke-virtual {v0, v1}, Ldu;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lwx0;->b:Lh71;

    iget-object p1, p1, Lwx0;->b:Lh71;

    invoke-virtual {v0, p1}, Lh71;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public j()I
    .locals 2

    .line 1
    invoke-virtual {p0}, Lwx0;->g()Ldu;

    move-result-object v0

    invoke-virtual {v0}, Ldu;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lwx0;->b:Lh71;

    invoke-virtual {v1}, Lh71;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public k()Ljava/lang/String;
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "key="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lwx0;->a:Ldu;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", precondition="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lwx0;->b:Lh71;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public l(Lpw1;Lcom/google/firebase/firestore/model/MutableDocument;)Ljava/util/Map;
    .locals 5

    .line 1
    new-instance v0, Ljava/util/HashMap;

    iget-object v1, p0, Lwx0;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    iget-object v1, p0, Lwx0;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lu00;

    invoke-virtual {v2}, Lu00;->b()Loy1;

    move-result-object v3

    invoke-virtual {v2}, Lu00;->a()Ls00;

    move-result-object v4

    invoke-virtual {p2, v4}, Lcom/google/firebase/firestore/model/MutableDocument;->j(Ls00;)Lcom/google/firestore/v1/Value;

    move-result-object v4

    invoke-virtual {v2}, Lu00;->a()Ls00;

    move-result-object v2

    invoke-interface {v3, v4, p1}, Loy1;->b(Lcom/google/firestore/v1/Value;Lpw1;)Lcom/google/firestore/v1/Value;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method public m(Lcom/google/firebase/firestore/model/MutableDocument;Ljava/util/List;)Ljava/util/Map;
    .locals 6

    .line 1
    new-instance v0, Ljava/util/HashMap;

    iget-object v1, p0, Lwx0;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    iget-object v1, p0, Lwx0;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v2

    const/4 v3, 0x0

    if-ne v1, v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    :cond_0
    move v1, v3

    :goto_0
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iget-object v4, p0, Lwx0;->c:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    filled-new-array {v2, v4}, [Ljava/lang/Object;

    move-result-object v2

    const-string v4, "server transform count (%d) should match field transform count (%d)"

    invoke-static {v1, v4, v2}, Lg9;->d(ZLjava/lang/String;[Ljava/lang/Object;)V

    :goto_1
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v1

    if-ge v3, v1, :cond_1

    iget-object v1, p0, Lwx0;->c:Ljava/util/List;

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lu00;

    invoke-virtual {v1}, Lu00;->b()Loy1;

    move-result-object v2

    invoke-virtual {v1}, Lu00;->a()Ls00;

    move-result-object v4

    invoke-virtual {p1, v4}, Lcom/google/firebase/firestore/model/MutableDocument;->j(Ls00;)Lcom/google/firestore/v1/Value;

    move-result-object v4

    invoke-virtual {v1}, Lu00;->a()Ls00;

    move-result-object v1

    invoke-interface {p2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/firestore/v1/Value;

    invoke-interface {v2, v4, v5}, Loy1;->c(Lcom/google/firestore/v1/Value;Lcom/google/firestore/v1/Value;)Lcom/google/firestore/v1/Value;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_1
    return-object v0
.end method

.method public n(Lcom/google/firebase/firestore/model/MutableDocument;)V
    .locals 2

    .line 1
    invoke-virtual {p1}, Lcom/google/firebase/firestore/model/MutableDocument;->getKey()Ldu;

    move-result-object p1

    invoke-virtual {p0}, Lwx0;->g()Ldu;

    move-result-object v0

    invoke-virtual {p1, v0}, Ldu;->equals(Ljava/lang/Object;)Z

    move-result p1

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    const-string v1, "Can only apply a mutation to a document with the same key"

    invoke-static {p1, v1, v0}, Lg9;->d(ZLjava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

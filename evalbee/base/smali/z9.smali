.class public final Lz9;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lts1$c;


# instance fields
.field public final a:Lts1$c;

.field public final b:Ly9;


# direct methods
.method public constructor <init>(Lts1$c;Ly9;)V
    .locals 1

    .line 1
    const-string v0, "delegate"

    invoke-static {p1, v0}, Lfg0;->e(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "autoCloser"

    invoke-static {p2, v0}, Lfg0;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lz9;->a:Lts1$c;

    iput-object p2, p0, Lz9;->b:Ly9;

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Lts1$b;)Lts1;
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lz9;->b(Lts1$b;)Landroidx/room/AutoClosingRoomOpenHelper;

    move-result-object p1

    return-object p1
.end method

.method public b(Lts1$b;)Landroidx/room/AutoClosingRoomOpenHelper;
    .locals 2

    .line 1
    const-string v0, "configuration"

    invoke-static {p1, v0}, Lfg0;->e(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Landroidx/room/AutoClosingRoomOpenHelper;

    iget-object v1, p0, Lz9;->a:Lts1$c;

    invoke-interface {v1, p1}, Lts1$c;->a(Lts1$b;)Lts1;

    move-result-object p1

    iget-object v1, p0, Lz9;->b:Ly9;

    invoke-direct {v0, p1, v1}, Landroidx/room/AutoClosingRoomOpenHelper;-><init>(Lts1;Ly9;)V

    return-object v0
.end method

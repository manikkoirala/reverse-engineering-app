.class public Lf2$q;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lf2;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "q"
.end annotation


# instance fields
.field public final synthetic a:Lf2;


# direct methods
.method public constructor <init>(Lf2;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lf2$q;->a:Lf2;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lf2;Lf2$h;)V
    .locals 0

    .line 2
    invoke-direct {p0, p1}, Lf2$q;-><init>(Lf2;)V

    return-void
.end method


# virtual methods
.method public varargs a([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 19

    .line 1
    move-object/from16 v0, p0

    iget-object v1, v0, Lf2$q;->a:Lf2;

    iget-object v1, v1, Lf2;->a:Landroid/content/Context;

    invoke-static {v1}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;

    move-result-object v1

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getTemplatesJson()Ljava/util/ArrayList;

    move-result-object v1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iget-object v3, v0, Lf2$q;->a:Lf2;

    iget-object v3, v3, Lf2;->a:Landroid/content/Context;

    invoke-static {v3}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;

    move-result-object v3

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    const/4 v4, 0x0

    move v5, v4

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;

    new-instance v8, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v6}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->getClassName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v6}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->getExamDate()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v8, v7, v9, v10}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v6}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->getSheetTemplate()Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    move-result-object v7

    invoke-virtual {v7}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getAnswerKeys()[Lcom/ekodroid/omrevaluator/templateui/models/AnswerSetKey;

    move-result-object v9

    const/4 v10, 0x1

    if-eqz v9, :cond_0

    invoke-virtual {v7}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getAnswerKeys()[Lcom/ekodroid/omrevaluator/templateui/models/AnswerSetKey;

    move-result-object v9

    array-length v9, v9

    if-lez v9, :cond_0

    invoke-virtual {v7}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getAnswerKeys()[Lcom/ekodroid/omrevaluator/templateui/models/AnswerSetKey;

    move-result-object v9

    aget-object v9, v9, v4

    if-eqz v9, :cond_0

    move v12, v10

    goto :goto_1

    :cond_0
    move v12, v4

    :goto_1
    invoke-virtual {v7}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getAnswerOptions()Ljava/util/ArrayList;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    add-int/lit8 v9, v7, -0x1

    invoke-virtual {v3, v8}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->getSheetImageCountForExam(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)J

    move-result-wide v10

    long-to-int v11, v10

    invoke-virtual {v3, v8}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->getAllResultCount(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)J

    move-result-wide v13

    long-to-int v10, v13

    invoke-virtual {v8}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;->getExamDate()Ljava/lang/String;

    move-result-object v7

    const/16 v13, 0x1e

    invoke-static {v7, v13}, La91;->C(Ljava/lang/String;I)Z

    move-result v14

    if-eqz v14, :cond_1

    add-int/lit8 v5, v5, 0x1

    :cond_1
    invoke-virtual {v6}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->isArchiving()Z

    move-result v13

    if-eqz v13, :cond_2

    invoke-virtual {v6}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->getArchivePayload()Lcom/ekodroid/omrevaluator/serializable/ArchivedResult/ArchiveProgress;

    move-result-object v7

    invoke-virtual {v7}, Lcom/ekodroid/omrevaluator/serializable/ArchivedResult/ArchiveProgress;->getProgress()I

    move-result v7

    move v15, v7

    goto :goto_2

    :cond_2
    move v15, v4

    :goto_2
    new-instance v7, Lay;

    invoke-virtual {v6}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->getArchiveId()Ljava/lang/String;

    move-result-object v16

    invoke-virtual {v6}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->isCloudSyncOn()Z

    move-result v17

    invoke-virtual {v6}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->getSharedType()Lcom/ekodroid/omrevaluator/serializable/SharedType;

    move-result-object v18

    move-object v6, v7

    invoke-direct/range {v7 .. v18}, Lay;-><init>(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;IIIZZZILjava/lang/String;ZLcom/ekodroid/omrevaluator/serializable/SharedType;)V

    invoke-virtual {v2, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :cond_3
    invoke-static {v2}, Ljava/util/Collections;->reverse(Ljava/util/List;)V

    iget-object v1, v0, Lf2$q;->a:Lf2;

    iput-object v2, v1, Lf2;->f:Ljava/util/ArrayList;

    invoke-static {v1, v5}, Lf2;->n(Lf2;I)I

    const/4 v1, 0x0

    return-object v1
.end method

.method public b(Ljava/lang/Void;)V
    .locals 1

    .line 1
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    iget-object p1, p0, Lf2$q;->a:Lf2;

    invoke-static {p1}, Lf2;->o(Lf2;)V

    iget-object p1, p0, Lf2$q;->a:Lf2;

    iget-object p1, p1, Lf2;->d:Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    return-void
.end method

.method public bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 1
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lf2$q;->a([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 1
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lf2$q;->b(Ljava/lang/Void;)V

    return-void
.end method

.method public onPreExecute()V
    .locals 2

    .line 1
    iget-object v0, p0, Lf2$q;->a:Lf2;

    iget-object v0, v0, Lf2;->d:Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    return-void
.end method

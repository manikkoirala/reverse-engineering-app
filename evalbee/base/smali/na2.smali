.class public Lna2;
.super Lr30;
.source "SourceFile"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lna2;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:Lcom/google/android/gms/internal/firebase-auth-api/zzafn;

.field public b:Ltj2;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/util/List;

.field public f:Ljava/util/List;

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/Boolean;

.field public i:Lra2;

.field public j:Z

.field public k:Llf2;

.field public l:Lcd2;

.field public m:Ljava/util/List;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .line 1
    new-instance v0, Lta2;

    invoke-direct {v0}, Lta2;-><init>()V

    sput-object v0, Lna2;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/internal/firebase-auth-api/zzafn;Ltj2;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/lang/Boolean;Lra2;ZLlf2;Lcd2;Ljava/util/List;)V
    .locals 0

    .line 2
    invoke-direct {p0}, Lr30;-><init>()V

    iput-object p1, p0, Lna2;->a:Lcom/google/android/gms/internal/firebase-auth-api/zzafn;

    iput-object p2, p0, Lna2;->b:Ltj2;

    iput-object p3, p0, Lna2;->c:Ljava/lang/String;

    iput-object p4, p0, Lna2;->d:Ljava/lang/String;

    iput-object p5, p0, Lna2;->e:Ljava/util/List;

    iput-object p6, p0, Lna2;->f:Ljava/util/List;

    iput-object p7, p0, Lna2;->g:Ljava/lang/String;

    iput-object p8, p0, Lna2;->h:Ljava/lang/Boolean;

    iput-object p9, p0, Lna2;->i:Lra2;

    iput-boolean p10, p0, Lna2;->j:Z

    iput-object p11, p0, Lna2;->k:Llf2;

    iput-object p12, p0, Lna2;->l:Lcd2;

    iput-object p13, p0, Lna2;->m:Ljava/util/List;

    return-void
.end method

.method public constructor <init>(Lr10;Ljava/util/List;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lr30;-><init>()V

    invoke-static {p1}, Lcom/google/android/gms/common/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p1}, Lr10;->o()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lna2;->c:Ljava/lang/String;

    const-string p1, "com.google.firebase.auth.internal.DefaultFirebaseUser"

    iput-object p1, p0, Lna2;->d:Ljava/lang/String;

    const-string p1, "2"

    iput-object p1, p0, Lna2;->g:Ljava/lang/String;

    invoke-virtual {p0, p2}, Lr30;->m0(Ljava/util/List;)Lr30;

    return-void
.end method


# virtual methods
.method public final A0()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lna2;->j:Z

    return v0
.end method

.method public E()Ls30;
    .locals 1

    .line 1
    iget-object v0, p0, Lna2;->i:Lra2;

    return-object v0
.end method

.method public synthetic H()Lex0;
    .locals 1

    .line 1
    new-instance v0, Lwa2;

    invoke-direct {v0, p0}, Lwa2;-><init>(Lna2;)V

    return-object v0
.end method

.method public J()Ljava/util/List;
    .locals 1

    .line 1
    iget-object v0, p0, Lna2;->e:Ljava/util/List;

    return-object v0
.end method

.method public K()Ljava/lang/String;
    .locals 3

    .line 1
    iget-object v0, p0, Lna2;->a:Lcom/google/android/gms/internal/firebase-auth-api/zzafn;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/gms/internal/firebase-auth-api/zzafn;->zzc()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lna2;->a:Lcom/google/android/gms/internal/firebase-auth-api/zzafn;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/firebase-auth-api/zzafn;->zzc()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lsc2;->a(Ljava/lang/String;)Lya0;

    move-result-object v0

    invoke-virtual {v0}, Lya0;->a()Ljava/util/Map;

    move-result-object v0

    const-string v2, "firebase"

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    if-eqz v0, :cond_0

    const-string v1, "tenant"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    move-object v1, v0

    :cond_0
    return-object v1
.end method

.method public O()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lna2;->b:Ltj2;

    invoke-virtual {v0}, Ltj2;->i()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public R()Z
    .locals 3

    .line 1
    iget-object v0, p0, Lna2;->h:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_0
    iget-object v0, p0, Lna2;->a:Lcom/google/android/gms/internal/firebase-auth-api/zzafn;

    const-string v1, ""

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/google/android/gms/internal/firebase-auth-api/zzafn;->zzc()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lsc2;->a(Ljava/lang/String;)Lya0;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lya0;->b()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    :cond_1
    invoke-virtual {p0}, Lr30;->J()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v2, 0x1

    if-gt v0, v2, :cond_2

    if-eqz v1, :cond_3

    const-string v0, "custom"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    goto :goto_0

    :cond_2
    const/4 v2, 0x0

    :cond_3
    :goto_0
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lna2;->h:Ljava/lang/Boolean;

    :cond_4
    iget-object v0, p0, Lna2;->h:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lna2;->b:Ltj2;

    invoke-virtual {v0}, Ltj2;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDisplayName()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lna2;->b:Ltj2;

    invoke-virtual {v0}, Ltj2;->getDisplayName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getEmail()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lna2;->b:Ltj2;

    invoke-virtual {v0}, Ltj2;->getEmail()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final i0()Lr10;
    .locals 1

    .line 1
    iget-object v0, p0, Lna2;->c:Ljava/lang/String;

    invoke-static {v0}, Lr10;->n(Ljava/lang/String;)Lr10;

    move-result-object v0

    return-object v0
.end method

.method public final declared-synchronized m0(Ljava/util/List;)Lr30;
    .locals 5

    .line 1
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lcom/google/android/gms/common/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lna2;->e:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lna2;->f:Ljava/util/List;

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lc22;

    invoke-interface {v2}, Lc22;->b()Ljava/lang/String;

    move-result-object v3

    const-string v4, "firebase"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    move-object v3, v2

    check-cast v3, Ltj2;

    iput-object v3, p0, Lna2;->b:Ltj2;

    goto :goto_1

    :cond_0
    iget-object v3, p0, Lna2;->f:Ljava/util/List;

    invoke-interface {v2}, Lc22;->b()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_1
    iget-object v3, p0, Lna2;->e:Ljava/util/List;

    check-cast v2, Ltj2;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    iget-object p1, p0, Lna2;->b:Ltj2;

    if-nez p1, :cond_2

    iget-object p1, p0, Lna2;->e:Ljava/util/List;

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ltj2;

    iput-object p1, p0, Lna2;->b:Ltj2;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_2
    monitor-exit p0

    return-object p0

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public final o0(Lcom/google/android/gms/internal/firebase-auth-api/zzafn;)V
    .locals 0

    .line 1
    invoke-static {p1}, Lcom/google/android/gms/common/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/google/android/gms/internal/firebase-auth-api/zzafn;

    iput-object p1, p0, Lna2;->a:Lcom/google/android/gms/internal/firebase-auth-api/zzafn;

    return-void
.end method

.method public final synthetic p0()Lr30;
    .locals 1

    .line 1
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    iput-object v0, p0, Lna2;->h:Ljava/lang/Boolean;

    return-object p0
.end method

.method public final q0(Ljava/util/List;)V
    .locals 0

    .line 1
    invoke-static {p1}, Lcd2;->E(Ljava/util/List;)Lcd2;

    move-result-object p1

    iput-object p1, p0, Lna2;->l:Lcd2;

    return-void
.end method

.method public final r0()Lcom/google/android/gms/internal/firebase-auth-api/zzafn;
    .locals 1

    .line 1
    iget-object v0, p0, Lna2;->a:Lcom/google/android/gms/internal/firebase-auth-api/zzafn;

    return-object v0
.end method

.method public final s0()Ljava/util/List;
    .locals 1

    .line 1
    iget-object v0, p0, Lna2;->f:Ljava/util/List;

    return-object v0
.end method

.method public final t0(Ljava/lang/String;)Lna2;
    .locals 0

    .line 1
    iput-object p1, p0, Lna2;->g:Ljava/lang/String;

    return-object p0
.end method

.method public final u0(Lra2;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lna2;->i:Lra2;

    return-void
.end method

.method public final v0(Llf2;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lna2;->k:Llf2;

    return-void
.end method

.method public final w0(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lna2;->j:Z

    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 4

    .line 1
    invoke-static {p1}, Lcom/google/android/gms/common/internal/safeparcel/SafeParcelWriter;->beginObjectHeader(Landroid/os/Parcel;)I

    move-result v0

    invoke-virtual {p0}, Lr30;->r0()Lcom/google/android/gms/internal/firebase-auth-api/zzafn;

    move-result-object v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-static {p1, v2, v1, p2, v3}, Lcom/google/android/gms/common/internal/safeparcel/SafeParcelWriter;->writeParcelable(Landroid/os/Parcel;ILandroid/os/Parcelable;IZ)V

    const/4 v1, 0x2

    iget-object v2, p0, Lna2;->b:Ltj2;

    invoke-static {p1, v1, v2, p2, v3}, Lcom/google/android/gms/common/internal/safeparcel/SafeParcelWriter;->writeParcelable(Landroid/os/Parcel;ILandroid/os/Parcelable;IZ)V

    const/4 v1, 0x3

    iget-object v2, p0, Lna2;->c:Ljava/lang/String;

    invoke-static {p1, v1, v2, v3}, Lcom/google/android/gms/common/internal/safeparcel/SafeParcelWriter;->writeString(Landroid/os/Parcel;ILjava/lang/String;Z)V

    const/4 v1, 0x4

    iget-object v2, p0, Lna2;->d:Ljava/lang/String;

    invoke-static {p1, v1, v2, v3}, Lcom/google/android/gms/common/internal/safeparcel/SafeParcelWriter;->writeString(Landroid/os/Parcel;ILjava/lang/String;Z)V

    const/4 v1, 0x5

    iget-object v2, p0, Lna2;->e:Ljava/util/List;

    invoke-static {p1, v1, v2, v3}, Lcom/google/android/gms/common/internal/safeparcel/SafeParcelWriter;->writeTypedList(Landroid/os/Parcel;ILjava/util/List;Z)V

    const/4 v1, 0x6

    invoke-virtual {p0}, Lr30;->s0()Ljava/util/List;

    move-result-object v2

    invoke-static {p1, v1, v2, v3}, Lcom/google/android/gms/common/internal/safeparcel/SafeParcelWriter;->writeStringList(Landroid/os/Parcel;ILjava/util/List;Z)V

    const/4 v1, 0x7

    iget-object v2, p0, Lna2;->g:Ljava/lang/String;

    invoke-static {p1, v1, v2, v3}, Lcom/google/android/gms/common/internal/safeparcel/SafeParcelWriter;->writeString(Landroid/os/Parcel;ILjava/lang/String;Z)V

    invoke-virtual {p0}, Lr30;->R()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    const/16 v2, 0x8

    invoke-static {p1, v2, v1, v3}, Lcom/google/android/gms/common/internal/safeparcel/SafeParcelWriter;->writeBooleanObject(Landroid/os/Parcel;ILjava/lang/Boolean;Z)V

    const/16 v1, 0x9

    invoke-virtual {p0}, Lr30;->E()Ls30;

    move-result-object v2

    invoke-static {p1, v1, v2, p2, v3}, Lcom/google/android/gms/common/internal/safeparcel/SafeParcelWriter;->writeParcelable(Landroid/os/Parcel;ILandroid/os/Parcelable;IZ)V

    const/16 v1, 0xa

    iget-boolean v2, p0, Lna2;->j:Z

    invoke-static {p1, v1, v2}, Lcom/google/android/gms/common/internal/safeparcel/SafeParcelWriter;->writeBoolean(Landroid/os/Parcel;IZ)V

    const/16 v1, 0xb

    iget-object v2, p0, Lna2;->k:Llf2;

    invoke-static {p1, v1, v2, p2, v3}, Lcom/google/android/gms/common/internal/safeparcel/SafeParcelWriter;->writeParcelable(Landroid/os/Parcel;ILandroid/os/Parcelable;IZ)V

    const/16 v1, 0xc

    iget-object v2, p0, Lna2;->l:Lcd2;

    invoke-static {p1, v1, v2, p2, v3}, Lcom/google/android/gms/common/internal/safeparcel/SafeParcelWriter;->writeParcelable(Landroid/os/Parcel;ILandroid/os/Parcelable;IZ)V

    const/16 p2, 0xd

    iget-object v1, p0, Lna2;->m:Ljava/util/List;

    invoke-static {p1, p2, v1, v3}, Lcom/google/android/gms/common/internal/safeparcel/SafeParcelWriter;->writeTypedList(Landroid/os/Parcel;ILjava/util/List;Z)V

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/safeparcel/SafeParcelWriter;->finishObjectHeader(Landroid/os/Parcel;I)V

    return-void
.end method

.method public final x0(Ljava/util/List;)V
    .locals 0

    .line 1
    invoke-static {p1}, Lcom/google/android/gms/common/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    iput-object p1, p0, Lna2;->m:Ljava/util/List;

    return-void
.end method

.method public final y0()Llf2;
    .locals 1

    .line 1
    iget-object v0, p0, Lna2;->k:Llf2;

    return-object v0
.end method

.method public final z0()Ljava/util/List;
    .locals 1

    .line 1
    iget-object v0, p0, Lna2;->e:Ljava/util/List;

    return-object v0
.end method

.method public final zzd()Ljava/lang/String;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lr30;->r0()Lcom/google/android/gms/internal/firebase-auth-api/zzafn;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/internal/firebase-auth-api/zzafn;->zzc()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final zze()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lna2;->a:Lcom/google/android/gms/internal/firebase-auth-api/zzafn;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/firebase-auth-api/zzafn;->zzf()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final zzh()Ljava/util/List;
    .locals 1

    .line 1
    iget-object v0, p0, Lna2;->l:Lcd2;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcd2;->i()Ljava/util/List;

    move-result-object v0

    return-object v0

    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    return-object v0
.end method

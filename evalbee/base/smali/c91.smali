.class public Lc91;
.super Landroid/view/animation/Animation;
.source "SourceFile"


# instance fields
.field public a:Landroid/widget/ProgressBar;

.field public b:Landroid/widget/TextView;

.field public c:F

.field public d:F

.field public e:F


# direct methods
.method public constructor <init>(Landroid/widget/ProgressBar;Landroid/widget/TextView;FF)V
    .locals 0

    .line 1
    invoke-direct {p0}, Landroid/view/animation/Animation;-><init>()V

    iput-object p1, p0, Lc91;->a:Landroid/widget/ProgressBar;

    iput-object p2, p0, Lc91;->b:Landroid/widget/TextView;

    iput p3, p0, Lc91;->c:F

    iput p4, p0, Lc91;->d:F

    const p1, 0x3f333333    # 0.7f

    mul-float/2addr p4, p1

    iput p4, p0, Lc91;->e:F

    return-void
.end method


# virtual methods
.method public applyTransformation(FLandroid/view/animation/Transformation;)V
    .locals 1

    .line 1
    invoke-super {p0, p1, p2}, Landroid/view/animation/Animation;->applyTransformation(FLandroid/view/animation/Transformation;)V

    iget p2, p0, Lc91;->c:F

    iget v0, p0, Lc91;->d:F

    sub-float/2addr v0, p2

    mul-float/2addr v0, p1

    add-float/2addr p2, v0

    iget p1, p0, Lc91;->e:F

    cmpl-float p1, p2, p1

    if-lez p1, :cond_0

    iget-object p1, p0, Lc91;->b:Landroid/widget/TextView;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    move-result p1

    const/16 v0, 0x8

    if-ne p1, v0, :cond_0

    iget-object p1, p0, Lc91;->b:Landroid/widget/TextView;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    iget-object p1, p0, Lc91;->a:Landroid/widget/ProgressBar;

    float-to-int p2, p2

    invoke-virtual {p1, p2}, Landroid/widget/ProgressBar;->setProgress(I)V

    return-void
.end method

.class public Lxe0;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lxe0$b;,
        Lxe0$a;
    }
.end annotation


# instance fields
.field public final a:Lh21;

.field public final b:Lxe0$a;

.field public final c:Lxe0$b;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lh21;

    invoke-direct {v0}, Lh21;-><init>()V

    iput-object v0, p0, Lxe0;->a:Lh21;

    new-instance v0, Lxe0$a;

    invoke-direct {v0, p0}, Lxe0$a;-><init>(Lxe0;)V

    iput-object v0, p0, Lxe0;->b:Lxe0$a;

    new-instance v0, Lxe0$b;

    invoke-direct {v0, p0}, Lxe0$b;-><init>(Lxe0;)V

    iput-object v0, p0, Lxe0;->c:Lxe0$b;

    return-void
.end method

.method public static synthetic a(Lxe0;)Lh21;
    .locals 0

    .line 1
    iget-object p0, p0, Lxe0;->a:Lh21;

    return-object p0
.end method


# virtual methods
.method public b(Lcom/google/firebase/firestore/model/FieldIndex$Segment$Kind;)Lbt;
    .locals 1

    .line 1
    sget-object v0, Lcom/google/firebase/firestore/model/FieldIndex$Segment$Kind;->DESCENDING:Lcom/google/firebase/firestore/model/FieldIndex$Segment$Kind;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lxe0;->c:Lxe0$b;

    return-object p1

    :cond_0
    iget-object p1, p0, Lxe0;->b:Lxe0$a;

    return-object p1
.end method

.method public c()[B
    .locals 1

    .line 1
    iget-object v0, p0, Lxe0;->a:Lh21;

    invoke-virtual {v0}, Lh21;->a()[B

    move-result-object v0

    return-object v0
.end method

.method public d([B)V
    .locals 1

    .line 1
    iget-object v0, p0, Lxe0;->a:Lh21;

    invoke-virtual {v0, p1}, Lh21;->c([B)V

    return-void
.end method

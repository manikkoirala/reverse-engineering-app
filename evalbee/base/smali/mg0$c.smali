.class public Lmg0$c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lzg;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lmg0;->c(Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/PendingJoinRequest;Lcom/ekodroid/omrevaluator/more/models/Teacher$MemberStatus;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Landroid/app/ProgressDialog;

.field public final synthetic b:Lmg0;


# direct methods
.method public constructor <init>(Lmg0;Landroid/app/ProgressDialog;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lmg0$c;->b:Lmg0;

    iput-object p2, p0, Lmg0$c;->a:Landroid/app/ProgressDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(ZILjava/lang/Object;)V
    .locals 1

    .line 1
    iget-object p2, p0, Lmg0$c;->a:Landroid/app/ProgressDialog;

    invoke-virtual {p2}, Landroid/app/Dialog;->dismiss()V

    const/4 p2, 0x0

    if-eqz p1, :cond_0

    iget-object p1, p0, Lmg0$c;->b:Lmg0;

    iget-object p1, p1, Lmg0;->a:Landroid/content/Context;

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;->reset(Landroid/content/Context;)V

    iget-object p1, p0, Lmg0$c;->b:Lmg0;

    iget-object p1, p1, Lmg0;->b:Ly01;

    if-eqz p1, :cond_1

    invoke-interface {p1, p2}, Ly01;->a(Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lmg0$c;->b:Lmg0;

    iget-object p1, p1, Lmg0;->a:Landroid/content/Context;

    invoke-static {p1}, Lcom/google/firebase/analytics/FirebaseAnalytics;->getInstance(Landroid/content/Context;)Lcom/google/firebase/analytics/FirebaseAnalytics;

    move-result-object p1

    const-string p3, "INVITE_RESPONSE_FAIL"

    invoke-virtual {p1, p3, p2}, Lcom/google/firebase/analytics/FirebaseAnalytics;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    iget-object p1, p0, Lmg0$c;->b:Lmg0;

    iget-object p1, p1, Lmg0;->a:Landroid/content/Context;

    const p2, 0x7f0800bd

    const p3, 0x7f08016e

    const-string v0, "Update failed, please try again"

    invoke-static {p1, v0, p2, p3}, La91;->H(Landroid/content/Context;Ljava/lang/String;II)V

    :cond_1
    :goto_0
    return-void
.end method

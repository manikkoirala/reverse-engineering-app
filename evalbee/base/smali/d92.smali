.class public Ld92;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lhu1;


# instance fields
.field public final a:Lkl1;

.field public final b:Lkotlinx/coroutines/CoroutineDispatcher;

.field public final c:Landroid/os/Handler;

.field public final d:Ljava/util/concurrent/Executor;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/Executor;)V
    .locals 2

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Ld92;->c:Landroid/os/Handler;

    new-instance v0, Ld92$a;

    invoke-direct {v0, p0}, Ld92$a;-><init>(Ld92;)V

    iput-object v0, p0, Ld92;->d:Ljava/util/concurrent/Executor;

    new-instance v0, Lkl1;

    invoke-direct {v0, p1}, Lkl1;-><init>(Ljava/util/concurrent/Executor;)V

    iput-object v0, p0, Ld92;->a:Lkl1;

    invoke-static {v0}, Lxy;->a(Ljava/util/concurrent/Executor;)Lkotlinx/coroutines/CoroutineDispatcher;

    move-result-object p1

    iput-object p1, p0, Ld92;->b:Lkotlinx/coroutines/CoroutineDispatcher;

    return-void
.end method


# virtual methods
.method public a()Lkotlinx/coroutines/CoroutineDispatcher;
    .locals 1

    .line 1
    iget-object v0, p0, Ld92;->b:Lkotlinx/coroutines/CoroutineDispatcher;

    return-object v0
.end method

.method public c()Ljava/util/concurrent/Executor;
    .locals 1

    .line 1
    iget-object v0, p0, Ld92;->d:Ljava/util/concurrent/Executor;

    return-object v0
.end method

.method public bridge synthetic d()Ljl1;
    .locals 1

    .line 1
    invoke-virtual {p0}, Ld92;->e()Lkl1;

    move-result-object v0

    return-object v0
.end method

.method public e()Lkl1;
    .locals 1

    .line 1
    iget-object v0, p0, Ld92;->a:Lkl1;

    return-object v0
.end method

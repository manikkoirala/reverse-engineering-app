.class public interface abstract Lzt;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Ljava/util/Comparator;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .line 1
    new-instance v0, Lyt;

    invoke-direct {v0}, Lyt;-><init>()V

    sput-object v0, Lzt;->a:Ljava/util/Comparator;

    return-void
.end method

.method public static synthetic h(Lzt;Lzt;)I
    .locals 0

    .line 1
    invoke-interface {p0}, Lzt;->getKey()Ldu;

    move-result-object p0

    invoke-interface {p1}, Lzt;->getKey()Ldu;

    move-result-object p1

    invoke-virtual {p0, p1}, Ldu;->c(Ldu;)I

    move-result p0

    return p0
.end method

.method public static synthetic k(Lzt;Lzt;)I
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lzt;->h(Lzt;Lzt;)I

    move-result p0

    return p0
.end method


# virtual methods
.method public abstract a()Lcom/google/firebase/firestore/model/MutableDocument;
.end method

.method public abstract b()Z
.end method

.method public abstract c()Z
.end method

.method public abstract d()Z
.end method

.method public abstract e()Lqo1;
.end method

.method public abstract f()Z
.end method

.method public abstract g()Z
.end method

.method public abstract getData()La11;
.end method

.method public abstract getKey()Ldu;
.end method

.method public abstract getVersion()Lqo1;
.end method

.method public abstract i()Z
.end method

.method public abstract j(Ls00;)Lcom/google/firestore/v1/Value;
.end method

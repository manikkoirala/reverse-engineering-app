.class public Ly11;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:Landroid/content/Context;

.field public b:Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;

.field public c:Ljava/util/ArrayList;

.field public d:I

.field public e:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;Lsn1;Landroid/graphics/Bitmap;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Ly11;->c:Ljava/util/ArrayList;

    const/16 v0, 0x20

    iput v0, p0, Ly11;->d:I

    const/16 v0, 0x28

    iput v0, p0, Ly11;->e:I

    iput-object p1, p0, Ly11;->a:Landroid/content/Context;

    iput-object p2, p0, Ly11;->b:Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;

    invoke-virtual/range {p0 .. p5}, Ly11;->a(Landroid/content/Context;Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;Lsn1;Landroid/graphics/Bitmap;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;Lsn1;Landroid/graphics/Bitmap;)V
    .locals 2

    .line 1
    sget-object v0, Ly11$a;->a:[I

    invoke-virtual {p2}, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->getType()Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    invoke-virtual/range {p0 .. p5}, Ly11;->b(Landroid/content/Context;Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;Lsn1;Landroid/graphics/Bitmap;)V

    goto :goto_0

    :pswitch_1
    invoke-virtual/range {p0 .. p5}, Ly11;->e(Landroid/content/Context;Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;Lsn1;Landroid/graphics/Bitmap;)V

    goto :goto_0

    :pswitch_2
    invoke-virtual/range {p0 .. p5}, Ly11;->d(Landroid/content/Context;Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;Lsn1;Landroid/graphics/Bitmap;)V

    goto :goto_0

    :pswitch_3
    invoke-virtual/range {p0 .. p5}, Ly11;->f(Landroid/content/Context;Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;Lsn1;Landroid/graphics/Bitmap;)V

    :goto_0
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method

.method public final b(Landroid/content/Context;Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;Lsn1;Landroid/graphics/Bitmap;)V
    .locals 17

    .line 1
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual/range {p2 .. p2}, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->getPayload()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lbc;->b(Ljava/lang/String;)Lcom/ekodroid/omrevaluator/templateui/models/DecimalOptionPayload;

    move-result-object v2

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    invoke-virtual {v0, v3, v4, v5, v6}, Ly11;->h(Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;Lsn1;Landroid/graphics/Bitmap;)[Landroid/graphics/Bitmap;

    move-result-object v3

    const/16 v4, 0x8

    invoke-virtual {v0, v4, v1}, Ly11;->g(ILandroid/content/Context;)I

    move-result v4

    iget v5, v0, Ly11;->d:I

    invoke-virtual {v0, v5, v1}, Ly11;->g(ILandroid/content/Context;)I

    move-result v5

    iget-boolean v6, v2, Lcom/ekodroid/omrevaluator/templateui/models/DecimalOptionPayload;->decimalAllowed:Z

    if-eqz v6, :cond_0

    const/16 v6, 0xb

    goto :goto_0

    :cond_0
    const/16 v6, 0xa

    :goto_0
    iget v7, v2, Lcom/ekodroid/omrevaluator/templateui/models/DecimalOptionPayload;->digits:I

    iget-boolean v2, v2, Lcom/ekodroid/omrevaluator/templateui/models/DecimalOptionPayload;->hasNegetive:Z

    const/high16 v8, 0x3f800000    # 1.0f

    const/4 v9, -0x2

    const/4 v10, 0x0

    const/4 v11, 0x1

    if-eqz v2, :cond_1

    new-instance v2, Landroid/widget/LinearLayout;

    invoke-direct {v2, v1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    new-instance v12, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v12, v5, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v12, v4, v4, v4, v4}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    iget v12, v0, Ly11;->e:I

    invoke-virtual {v0, v12, v1}, Ly11;->g(ILandroid/content/Context;)I

    move-result v12

    new-instance v13, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v13, v12, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v13, v4, v4, v4, v4}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    new-instance v12, Landroid/widget/ImageView;

    invoke-direct {v12, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2, v12, v13}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v12, Lcom/google/android/flexbox/FlexboxLayout;

    invoke-direct {v12, v1}, Lcom/google/android/flexbox/FlexboxLayout;-><init>(Landroid/content/Context;)V

    invoke-virtual {v12, v10}, Lcom/google/android/flexbox/FlexboxLayout;->setFlexDirection(I)V

    invoke-virtual {v12, v11}, Lcom/google/android/flexbox/FlexboxLayout;->setFlexWrap(I)V

    new-instance v13, Lcom/google/android/flexbox/FlexboxLayout$LayoutParams;

    invoke-direct {v13, v5, v5}, Lcom/google/android/flexbox/FlexboxLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v13, v4, v4, v4, v4}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    new-instance v14, Landroid/widget/ImageView;

    invoke-direct {v14, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    sget-object v15, Landroid/widget/ImageView$ScaleType;->FIT_XY:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v14, v15}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    invoke-virtual {v14, v11}, Landroid/widget/ImageView;->setAdjustViewBounds(Z)V

    array-length v15, v3

    sub-int/2addr v15, v11

    aget-object v15, v3, v15

    invoke-virtual {v14, v15}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    invoke-virtual {v12, v14, v13}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v13, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v13, v10, v9}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    iput v8, v13, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    invoke-virtual {v2, v12, v13}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v12, v0, Ly11;->c:Ljava/util/ArrayList;

    invoke-virtual {v12, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    move v2, v10

    :goto_1
    if-ge v2, v6, :cond_3

    new-instance v12, Landroid/widget/LinearLayout;

    invoke-direct {v12, v1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    new-instance v13, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v13, v5, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v13, v4, v4, v4, v4}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    iget v13, v0, Ly11;->e:I

    invoke-virtual {v0, v13, v1}, Ly11;->g(ILandroid/content/Context;)I

    move-result v13

    new-instance v14, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v14, v13, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v14, v4, v4, v4, v4}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    new-instance v13, Landroid/widget/ImageView;

    invoke-direct {v13, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    invoke-virtual {v12, v13, v14}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v13, Lcom/google/android/flexbox/FlexboxLayout;

    invoke-direct {v13, v1}, Lcom/google/android/flexbox/FlexboxLayout;-><init>(Landroid/content/Context;)V

    invoke-virtual {v13, v10}, Lcom/google/android/flexbox/FlexboxLayout;->setFlexDirection(I)V

    invoke-virtual {v13, v11}, Lcom/google/android/flexbox/FlexboxLayout;->setFlexWrap(I)V

    new-instance v14, Lcom/google/android/flexbox/FlexboxLayout$LayoutParams;

    invoke-direct {v14, v5, v5}, Lcom/google/android/flexbox/FlexboxLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v14, v4, v4, v4, v4}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    move v15, v10

    :goto_2
    if-ge v15, v7, :cond_2

    mul-int v16, v2, v7

    add-int v16, v16, v15

    new-instance v8, Landroid/widget/ImageView;

    invoke-direct {v8, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    sget-object v9, Landroid/widget/ImageView$ScaleType;->FIT_XY:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v8, v9}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    invoke-virtual {v8, v11}, Landroid/widget/ImageView;->setAdjustViewBounds(Z)V

    aget-object v9, v3, v16

    invoke-virtual {v8, v9}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    invoke-virtual {v13, v8, v14}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    add-int/lit8 v15, v15, 0x1

    const/high16 v8, 0x3f800000    # 1.0f

    const/4 v9, -0x2

    goto :goto_2

    :cond_2
    new-instance v8, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v9, -0x2

    invoke-direct {v8, v10, v9}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    const/high16 v14, 0x3f800000    # 1.0f

    iput v14, v8, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    invoke-virtual {v12, v13, v8}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v8, v0, Ly11;->c:Ljava/util/ArrayList;

    invoke-virtual {v8, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    move v8, v14

    goto :goto_1

    :cond_3
    return-void
.end method

.method public final c(Landroid/content/Context;Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;Lsn1;Landroid/graphics/Bitmap;)V
    .locals 16

    .line 1
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual/range {p2 .. p2}, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->getPayload()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lbc;->d(Ljava/lang/String;)Lcom/ekodroid/omrevaluator/templateui/models/NumericalOptionPayload;

    move-result-object v2

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    invoke-virtual {v0, v3, v4, v5, v6}, Ly11;->h(Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;Lsn1;Landroid/graphics/Bitmap;)[Landroid/graphics/Bitmap;

    move-result-object v3

    new-instance v4, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v5, -0x1

    const/4 v6, 0x0

    invoke-direct {v4, v6, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    const/16 v5, 0x10

    invoke-virtual {v0, v5, v1}, Ly11;->g(ILandroid/content/Context;)I

    move-result v5

    iput v5, v4, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    const/high16 v5, 0x3f800000    # 1.0f

    iput v5, v4, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    const/16 v5, 0x8

    invoke-virtual {v0, v5, v1}, Ly11;->g(ILandroid/content/Context;)I

    move-result v5

    iget-boolean v7, v2, Lcom/ekodroid/omrevaluator/templateui/models/NumericalOptionPayload;->hasNegetive:Z

    const/4 v8, 0x5

    const/4 v9, 0x1

    if-eqz v7, :cond_2

    new-instance v7, Landroid/widget/LinearLayout;

    invoke-direct {v7, v1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    new-instance v10, Landroid/widget/ImageView;

    invoke-direct {v10, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    invoke-virtual {v7, v10, v4}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    move v10, v6

    :goto_0
    if-ge v10, v8, :cond_1

    new-instance v11, Landroid/widget/ImageView;

    invoke-direct {v11, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    if-ne v10, v9, :cond_0

    sget-object v12, Landroid/widget/ImageView$ScaleType;->FIT_XY:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v11, v12}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    invoke-virtual {v11, v9}, Landroid/widget/ImageView;->setAdjustViewBounds(Z)V

    array-length v12, v3

    sub-int/2addr v12, v9

    aget-object v12, v3, v12

    invoke-virtual {v11, v12}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    :cond_0
    invoke-virtual {v7, v11, v4}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    add-int/lit8 v10, v10, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {v7, v5, v5, v5, v5}, Landroid/view/View;->setPadding(IIII)V

    iget-object v10, v0, Ly11;->c:Ljava/util/ArrayList;

    invoke-virtual {v10, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    iget v7, v2, Lcom/ekodroid/omrevaluator/templateui/models/NumericalOptionPayload;->digits:I

    sub-int/2addr v7, v9

    div-int/2addr v7, v8

    add-int/2addr v7, v9

    move v10, v6

    :goto_1
    const/16 v11, 0xa

    if-ge v10, v11, :cond_6

    move v11, v6

    :goto_2
    if-ge v11, v7, :cond_5

    new-instance v12, Landroid/widget/LinearLayout;

    invoke-direct {v12, v1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    new-instance v13, Landroid/widget/ImageView;

    invoke-direct {v13, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    invoke-virtual {v12, v13, v4}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    move v13, v6

    :goto_3
    if-ge v13, v8, :cond_4

    new-instance v14, Landroid/widget/ImageView;

    invoke-direct {v14, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    mul-int/lit8 v15, v11, 0x5

    add-int/2addr v15, v13

    iget v6, v2, Lcom/ekodroid/omrevaluator/templateui/models/NumericalOptionPayload;->digits:I

    if-ge v15, v6, :cond_3

    sget-object v6, Landroid/widget/ImageView$ScaleType;->FIT_XY:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v14, v6}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    invoke-virtual {v14, v9}, Landroid/widget/ImageView;->setAdjustViewBounds(Z)V

    iget v6, v2, Lcom/ekodroid/omrevaluator/templateui/models/NumericalOptionPayload;->digits:I

    mul-int/2addr v6, v10

    add-int/2addr v6, v15

    aget-object v6, v3, v6

    invoke-virtual {v14, v6}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    :cond_3
    invoke-virtual {v12, v14, v4}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    add-int/lit8 v13, v13, 0x1

    const/4 v6, 0x0

    goto :goto_3

    :cond_4
    invoke-virtual {v12, v5, v5, v5, v5}, Landroid/view/View;->setPadding(IIII)V

    iget-object v6, v0, Ly11;->c:Ljava/util/ArrayList;

    invoke-virtual {v6, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v11, v11, 0x1

    const/4 v6, 0x0

    goto :goto_2

    :cond_5
    add-int/lit8 v10, v10, 0x1

    const/4 v6, 0x0

    goto :goto_1

    :cond_6
    return-void
.end method

.method public final d(Landroid/content/Context;Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;Lsn1;Landroid/graphics/Bitmap;)V
    .locals 16

    .line 1
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    const/16 v2, 0x8

    invoke-virtual {v0, v2, v1}, Ly11;->g(ILandroid/content/Context;)I

    move-result v2

    iget v3, v0, Ly11;->d:I

    invoke-virtual {v0, v3, v1}, Ly11;->g(ILandroid/content/Context;)I

    move-result v3

    invoke-virtual/range {p2 .. p2}, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->getPayload()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lbc;->c(Ljava/lang/String;)Lcom/ekodroid/omrevaluator/templateui/models/MatrixOptionPayload;

    move-result-object v4

    if-eqz v4, :cond_0

    iget v5, v4, Lcom/ekodroid/omrevaluator/templateui/models/MatrixOptionPayload;->primaryOptions:I

    iget v4, v4, Lcom/ekodroid/omrevaluator/templateui/models/MatrixOptionPayload;->secondaryOptions:I

    goto :goto_0

    :cond_0
    const/4 v5, 0x4

    const/4 v4, 0x5

    :goto_0
    move-object/from16 v6, p2

    move-object/from16 v7, p3

    move-object/from16 v8, p4

    move-object/from16 v9, p5

    invoke-virtual {v0, v6, v7, v8, v9}, Ly11;->h(Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;Lsn1;Landroid/graphics/Bitmap;)[Landroid/graphics/Bitmap;

    move-result-object v6

    const/4 v7, 0x0

    move v8, v7

    :goto_1
    if-ge v8, v5, :cond_2

    new-instance v9, Landroid/widget/LinearLayout;

    invoke-direct {v9, v1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    new-instance v10, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v10, v3, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v10, v2, v2, v2, v2}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    iget v10, v0, Ly11;->e:I

    invoke-virtual {v0, v10, v1}, Ly11;->g(ILandroid/content/Context;)I

    move-result v10

    new-instance v11, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v11, v10, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v11, v2, v2, v2, v2}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    new-instance v10, Landroid/widget/ImageView;

    invoke-direct {v10, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    invoke-virtual {v9, v10, v11}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v10, Lcom/google/android/flexbox/FlexboxLayout;

    invoke-direct {v10, v1}, Lcom/google/android/flexbox/FlexboxLayout;-><init>(Landroid/content/Context;)V

    invoke-virtual {v10, v7}, Lcom/google/android/flexbox/FlexboxLayout;->setFlexDirection(I)V

    const/4 v11, 0x1

    invoke-virtual {v10, v11}, Lcom/google/android/flexbox/FlexboxLayout;->setFlexWrap(I)V

    new-instance v12, Lcom/google/android/flexbox/FlexboxLayout$LayoutParams;

    invoke-direct {v12, v3, v3}, Lcom/google/android/flexbox/FlexboxLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v12, v2, v2, v2, v2}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    move v13, v7

    :goto_2
    if-ge v13, v4, :cond_1

    mul-int v14, v8, v4

    add-int/2addr v14, v13

    new-instance v15, Landroid/widget/ImageView;

    invoke-direct {v15, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    sget-object v7, Landroid/widget/ImageView$ScaleType;->FIT_XY:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v15, v7}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    invoke-virtual {v15, v11}, Landroid/widget/ImageView;->setAdjustViewBounds(Z)V

    aget-object v7, v6, v14

    invoke-virtual {v15, v7}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    invoke-virtual {v10, v15, v12}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    add-int/lit8 v13, v13, 0x1

    const/4 v7, 0x0

    goto :goto_2

    :cond_1
    new-instance v7, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v11, -0x2

    const/4 v12, 0x0

    invoke-direct {v7, v12, v11}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    const/high16 v11, 0x3f800000    # 1.0f

    iput v11, v7, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    invoke-virtual {v9, v10, v7}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v7, v0, Ly11;->c:Ljava/util/ArrayList;

    invoke-virtual {v7, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v8, v8, 0x1

    move v7, v12

    goto :goto_1

    :cond_2
    return-void
.end method

.method public final e(Landroid/content/Context;Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;Lsn1;Landroid/graphics/Bitmap;)V
    .locals 7

    .line 1
    invoke-virtual {p2}, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->getPayload()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lbc;->d(Ljava/lang/String;)Lcom/ekodroid/omrevaluator/templateui/models/NumericalOptionPayload;

    move-result-object v0

    const/4 v1, 0x1

    if-eqz v0, :cond_1

    iget-boolean v2, v0, Lcom/ekodroid/omrevaluator/templateui/models/NumericalOptionPayload;->hasNegetive:Z

    if-nez v2, :cond_0

    iget v0, v0, Lcom/ekodroid/omrevaluator/templateui/models/NumericalOptionPayload;->digits:I

    if-le v0, v1, :cond_1

    :cond_0
    invoke-virtual/range {p0 .. p5}, Ly11;->c(Landroid/content/Context;Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;Lsn1;Landroid/graphics/Bitmap;)V

    return-void

    :cond_1
    invoke-virtual {p0, p2, p3, p4, p5}, Ly11;->h(Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;Lsn1;Landroid/graphics/Bitmap;)[Landroid/graphics/Bitmap;

    move-result-object p2

    new-instance p3, Landroid/widget/LinearLayout$LayoutParams;

    const/4 p4, -0x1

    const/4 p5, 0x0

    invoke-direct {p3, p5, p4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    const/16 p4, 0x10

    invoke-virtual {p0, p4, p1}, Ly11;->g(ILandroid/content/Context;)I

    move-result p4

    iput p4, p3, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    const/high16 p4, 0x3f800000    # 1.0f

    iput p4, p3, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    const/16 p4, 0x8

    invoke-virtual {p0, p4, p1}, Ly11;->g(ILandroid/content/Context;)I

    move-result p4

    move v0, p5

    :goto_0
    const/4 v2, 0x2

    if-ge v0, v2, :cond_3

    new-instance v2, Landroid/widget/LinearLayout;

    invoke-direct {v2, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    new-instance v3, Landroid/widget/ImageView;

    invoke-direct {v3, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2, v3, p3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    move v3, p5

    :goto_1
    const/4 v4, 0x5

    if-ge v3, v4, :cond_2

    mul-int/lit8 v4, v0, 0x5

    add-int/2addr v4, v3

    new-instance v5, Landroid/widget/ImageView;

    invoke-direct {v5, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    sget-object v6, Landroid/widget/ImageView$ScaleType;->FIT_XY:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    invoke-virtual {v5, v1}, Landroid/widget/ImageView;->setAdjustViewBounds(Z)V

    aget-object v4, p2, v4

    invoke-virtual {v5, v4}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    invoke-virtual {v2, v5, p3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_2
    invoke-virtual {v2, p4, p4, p4, p4}, Landroid/view/View;->setPadding(IIII)V

    iget-object v3, p0, Ly11;->c:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    return-void
.end method

.method public final f(Landroid/content/Context;Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;Lsn1;Landroid/graphics/Bitmap;)V
    .locals 5

    .line 1
    invoke-virtual {p0, p2, p3, p4, p5}, Ly11;->h(Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;Lsn1;Landroid/graphics/Bitmap;)[Landroid/graphics/Bitmap;

    move-result-object p2

    const/16 p3, 0x8

    invoke-virtual {p0, p3, p1}, Ly11;->g(ILandroid/content/Context;)I

    move-result p3

    iget p4, p0, Ly11;->d:I

    invoke-virtual {p0, p4, p1}, Ly11;->g(ILandroid/content/Context;)I

    move-result p4

    new-instance p5, Landroid/widget/LinearLayout;

    invoke-direct {p5, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, p4, p4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, p3, p3, p3, p3}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    iget v0, p0, Ly11;->e:I

    invoke-virtual {p0, v0, p1}, Ly11;->g(ILandroid/content/Context;)I

    move-result v0

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v1, v0, p4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v1, p3, p3, p3, p3}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    new-instance v0, Landroid/widget/ImageView;

    invoke-direct {v0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    invoke-virtual {p5, v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v0, Lcom/google/android/flexbox/FlexboxLayout;

    invoke-direct {v0, p1}, Lcom/google/android/flexbox/FlexboxLayout;-><init>(Landroid/content/Context;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/flexbox/FlexboxLayout;->setFlexDirection(I)V

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/google/android/flexbox/FlexboxLayout;->setFlexWrap(I)V

    new-instance v3, Lcom/google/android/flexbox/FlexboxLayout$LayoutParams;

    invoke-direct {v3, p4, p4}, Lcom/google/android/flexbox/FlexboxLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v3, p3, p3, p3, p3}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    move p3, v1

    :goto_0
    array-length p4, p2

    if-ge p3, p4, :cond_0

    new-instance p4, Landroid/widget/ImageView;

    invoke-direct {p4, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    sget-object v4, Landroid/widget/ImageView$ScaleType;->FIT_XY:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {p4, v4}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    invoke-virtual {p4, v2}, Landroid/widget/ImageView;->setAdjustViewBounds(Z)V

    aget-object v4, p2, p3

    invoke-virtual {p4, v4}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    invoke-virtual {v0, p4, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    add-int/lit8 p3, p3, 0x1

    goto :goto_0

    :cond_0
    new-instance p1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 p2, -0x2

    invoke-direct {p1, v1, p2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    const/high16 p2, 0x3f800000    # 1.0f

    iput p2, p1, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    invoke-virtual {p5, v0, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    iget-object p1, p0, Ly11;->c:Ljava/util/ArrayList;

    invoke-virtual {p1, p5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public final g(ILandroid/content/Context;)I
    .locals 0

    .line 1
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    invoke-virtual {p2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object p2

    iget p2, p2, Landroid/util/DisplayMetrics;->density:F

    invoke-static {p2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object p2

    int-to-float p1, p1

    invoke-virtual {p2}, Ljava/lang/Float;->floatValue()F

    move-result p2

    mul-float/2addr p1, p2

    float-to-int p1, p1

    return p1
.end method

.method public final h(Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;Lsn1;Landroid/graphics/Bitmap;)[Landroid/graphics/Bitmap;
    .locals 6

    .line 1
    invoke-virtual {p3}, Lsn1;->f()Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;

    move-result-object v0

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;->getBubbleSize()D

    move-result-wide v0

    double-to-int v0, v0

    mul-int/lit8 v0, v0, 0x2

    invoke-virtual {p2}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getAnswerOptions()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->getQuestionNumber()I

    move-result p1

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;

    const/4 v1, 0x0

    invoke-virtual {p2, v1}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getColumnWidthInBubbles(I)[I

    move-result-object p2

    invoke-virtual {p3, p1, p2}, Lsn1;->b(Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;[I)[Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    move-result-object p1

    array-length p2, p1

    new-array p2, p2, [Landroid/graphics/Bitmap;

    :goto_0
    array-length p3, p1

    if-ge v1, p3, :cond_0

    aget-object p3, p1, v1

    iget-wide v2, p3, Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;->x:D

    double-to-int v2, v2

    div-int/lit8 v3, v0, 0x2

    sub-int/2addr v2, v3

    iget-wide v4, p3, Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;->y:D

    double-to-int p3, v4

    sub-int/2addr p3, v3

    invoke-static {p4, v2, p3, v0, v0}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIII)Landroid/graphics/Bitmap;

    move-result-object p3

    aput-object p3, p2, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-object p2
.end method

.method public i()Ljava/util/ArrayList;
    .locals 1

    .line 1
    iget-object v0, p0, Ly11;->c:Ljava/util/ArrayList;

    return-object v0
.end method

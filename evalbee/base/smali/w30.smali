.class public Lw30;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final g:Lio/grpc/Metadata$Key;

.field public static final h:Lio/grpc/Metadata$Key;

.field public static final i:Lio/grpc/Metadata$Key;

.field public static volatile j:Ljava/lang/String;


# instance fields
.field public final a:Lcom/google/firebase/firestore/util/AsyncQueue;

.field public final b:Leo;

.field public final c:Leo;

.field public final d:Lec0;

.field public final e:Ljava/lang/String;

.field public final f:Lfc0;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .line 1
    sget-object v0, Lio/grpc/Metadata;->ASCII_STRING_MARSHALLER:Lio/grpc/Metadata$AsciiMarshaller;

    const-string v1, "x-goog-api-client"

    invoke-static {v1, v0}, Lio/grpc/Metadata$Key;->of(Ljava/lang/String;Lio/grpc/Metadata$AsciiMarshaller;)Lio/grpc/Metadata$Key;

    move-result-object v1

    sput-object v1, Lw30;->g:Lio/grpc/Metadata$Key;

    const-string v1, "google-cloud-resource-prefix"

    invoke-static {v1, v0}, Lio/grpc/Metadata$Key;->of(Ljava/lang/String;Lio/grpc/Metadata$AsciiMarshaller;)Lio/grpc/Metadata$Key;

    move-result-object v1

    sput-object v1, Lw30;->h:Lio/grpc/Metadata$Key;

    const-string v1, "x-goog-request-params"

    invoke-static {v1, v0}, Lio/grpc/Metadata$Key;->of(Ljava/lang/String;Lio/grpc/Metadata$AsciiMarshaller;)Lio/grpc/Metadata$Key;

    move-result-object v0

    sput-object v0, Lw30;->i:Lio/grpc/Metadata$Key;

    const-string v0, "gl-java/"

    sput-object v0, Lw30;->j:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/firebase/firestore/util/AsyncQueue;Landroid/content/Context;Leo;Leo;Lrp;Lfc0;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lw30;->a:Lcom/google/firebase/firestore/util/AsyncQueue;

    iput-object p6, p0, Lw30;->f:Lfc0;

    iput-object p3, p0, Lw30;->b:Leo;

    iput-object p4, p0, Lw30;->c:Leo;

    new-instance p6, Lu30;

    invoke-direct {p6, p3, p4}, Lu30;-><init>(Leo;Leo;)V

    new-instance p3, Lec0;

    invoke-direct {p3, p1, p2, p5, p6}, Lec0;-><init>(Lcom/google/firebase/firestore/util/AsyncQueue;Landroid/content/Context;Lrp;Lio/grpc/CallCredentials;)V

    iput-object p3, p0, Lw30;->d:Lec0;

    invoke-virtual {p5}, Lrp;->a()Lqp;

    move-result-object p1

    invoke-virtual {p1}, Lqp;->f()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1}, Lqp;->e()Ljava/lang/String;

    move-result-object p1

    filled-new-array {p2, p1}, [Ljava/lang/Object;

    move-result-object p1

    const-string p2, "projects/%s/databases/%s"

    invoke-static {p2, p1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lw30;->e:Ljava/lang/String;

    return-void
.end method

.method public static synthetic a(Lw30;[Lio/grpc/ClientCall;Lpe0;Lcom/google/android/gms/tasks/Task;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p3}, Lw30;->e([Lio/grpc/ClientCall;Lpe0;Lcom/google/android/gms/tasks/Task;)V

    return-void
.end method

.method public static synthetic b(Lw30;)Lcom/google/firebase/firestore/util/AsyncQueue;
    .locals 0

    .line 1
    iget-object p0, p0, Lw30;->a:Lcom/google/firebase/firestore/util/AsyncQueue;

    return-object p0
.end method

.method private synthetic e([Lio/grpc/ClientCall;Lpe0;Lcom/google/android/gms/tasks/Task;)V
    .locals 3

    .line 1
    invoke-virtual {p3}, Lcom/google/android/gms/tasks/Task;->getResult()Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lio/grpc/ClientCall;

    const/4 v0, 0x0

    aput-object p3, p1, v0

    new-instance v1, Lw30$a;

    invoke-direct {v1, p0, p2, p1}, Lw30$a;-><init>(Lw30;Lpe0;[Lio/grpc/ClientCall;)V

    invoke-virtual {p0}, Lw30;->f()Lio/grpc/Metadata;

    move-result-object v2

    invoke-virtual {p3, v1, v2}, Lio/grpc/ClientCall;->start(Lio/grpc/ClientCall$Listener;Lio/grpc/Metadata;)V

    invoke-interface {p2}, Lpe0;->b()V

    aget-object p1, p1, v0

    const/4 p2, 0x1

    invoke-virtual {p1, p2}, Lio/grpc/ClientCall;->request(I)V

    return-void
.end method

.method public static h(Ljava/lang/String;)V
    .locals 0

    .line 1
    sput-object p0, Lw30;->j:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final c()Ljava/lang/String;
    .locals 2

    .line 1
    sget-object v0, Lw30;->j:Ljava/lang/String;

    const-string v1, "24.10.0"

    filled-new-array {v0, v1}, [Ljava/lang/Object;

    move-result-object v0

    const-string v1, "%s fire/%s grpc/"

    invoke-static {v1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public d()V
    .locals 1

    .line 1
    iget-object v0, p0, Lw30;->b:Leo;

    invoke-virtual {v0}, Leo;->b()V

    iget-object v0, p0, Lw30;->c:Leo;

    invoke-virtual {v0}, Leo;->b()V

    return-void
.end method

.method public final f()Lio/grpc/Metadata;
    .locals 3

    .line 1
    new-instance v0, Lio/grpc/Metadata;

    invoke-direct {v0}, Lio/grpc/Metadata;-><init>()V

    sget-object v1, Lw30;->g:Lio/grpc/Metadata$Key;

    invoke-virtual {p0}, Lw30;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lio/grpc/Metadata;->put(Lio/grpc/Metadata$Key;Ljava/lang/Object;)V

    sget-object v1, Lw30;->h:Lio/grpc/Metadata$Key;

    iget-object v2, p0, Lw30;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lio/grpc/Metadata;->put(Lio/grpc/Metadata$Key;Ljava/lang/Object;)V

    sget-object v1, Lw30;->i:Lio/grpc/Metadata$Key;

    iget-object v2, p0, Lw30;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lio/grpc/Metadata;->put(Lio/grpc/Metadata$Key;Ljava/lang/Object;)V

    iget-object v1, p0, Lw30;->f:Lfc0;

    if-eqz v1, :cond_0

    invoke-interface {v1, v0}, Lfc0;->a(Lio/grpc/Metadata;)V

    :cond_0
    return-object v0
.end method

.method public g(Lio/grpc/MethodDescriptor;Lpe0;)Lio/grpc/ClientCall;
    .locals 3

    .line 1
    const/4 v0, 0x1

    new-array v0, v0, [Lio/grpc/ClientCall;

    const/4 v1, 0x0

    const/4 v2, 0x0

    aput-object v2, v0, v1

    iget-object v1, p0, Lw30;->d:Lec0;

    invoke-virtual {v1, p1}, Lec0;->i(Lio/grpc/MethodDescriptor;)Lcom/google/android/gms/tasks/Task;

    move-result-object p1

    iget-object v1, p0, Lw30;->a:Lcom/google/firebase/firestore/util/AsyncQueue;

    invoke-virtual {v1}, Lcom/google/firebase/firestore/util/AsyncQueue;->j()Ljava/util/concurrent/Executor;

    move-result-object v1

    new-instance v2, Lv30;

    invoke-direct {v2, p0, v0, p2}, Lv30;-><init>(Lw30;[Lio/grpc/ClientCall;Lpe0;)V

    invoke-virtual {p1, v1, v2}, Lcom/google/android/gms/tasks/Task;->addOnCompleteListener(Ljava/util/concurrent/Executor;Lcom/google/android/gms/tasks/OnCompleteListener;)Lcom/google/android/gms/tasks/Task;

    new-instance p2, Lw30$b;

    invoke-direct {p2, p0, v0, p1}, Lw30$b;-><init>(Lw30;[Lio/grpc/ClientCall;Lcom/google/android/gms/tasks/Task;)V

    return-object p2
.end method

.class public Ln1$a;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ln1;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation


# static fields
.field public static final A:Ln1$a;

.field public static final B:Ln1$a;

.field public static final C:Ln1$a;

.field public static final D:Ln1$a;

.field public static final E:Ln1$a;

.field public static final F:Ln1$a;

.field public static final G:Ln1$a;

.field public static final H:Ln1$a;

.field public static final I:Ln1$a;

.field public static final J:Ln1$a;

.field public static final K:Ln1$a;

.field public static final L:Ln1$a;

.field public static final M:Ln1$a;

.field public static final N:Ln1$a;

.field public static final O:Ln1$a;

.field public static final P:Ln1$a;

.field public static final Q:Ln1$a;

.field public static final R:Ln1$a;

.field public static final S:Ln1$a;

.field public static final T:Ln1$a;

.field public static final U:Ln1$a;

.field public static final e:Ln1$a;

.field public static final f:Ln1$a;

.field public static final g:Ln1$a;

.field public static final h:Ln1$a;

.field public static final i:Ln1$a;

.field public static final j:Ln1$a;

.field public static final k:Ln1$a;

.field public static final l:Ln1$a;

.field public static final m:Ln1$a;

.field public static final n:Ln1$a;

.field public static final o:Ln1$a;

.field public static final p:Ln1$a;

.field public static final q:Ln1$a;

.field public static final r:Ln1$a;

.field public static final s:Ln1$a;

.field public static final t:Ln1$a;

.field public static final u:Ln1$a;

.field public static final v:Ln1$a;

.field public static final w:Ln1$a;

.field public static final x:Ln1$a;

.field public static final y:Ln1$a;

.field public static final z:Ln1$a;


# instance fields
.field public final a:Ljava/lang/Object;

.field public final b:I

.field public final c:Ljava/lang/Class;

.field public final d:Lq1;


# direct methods
.method public static constructor <clinit>()V
    .locals 17

    .line 1
    new-instance v0, Ln1$a;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Ln1$a;-><init>(ILjava/lang/CharSequence;)V

    sput-object v0, Ln1$a;->e:Ln1$a;

    new-instance v0, Ln1$a;

    const/4 v1, 0x2

    invoke-direct {v0, v1, v2}, Ln1$a;-><init>(ILjava/lang/CharSequence;)V

    sput-object v0, Ln1$a;->f:Ln1$a;

    new-instance v0, Ln1$a;

    const/4 v1, 0x4

    invoke-direct {v0, v1, v2}, Ln1$a;-><init>(ILjava/lang/CharSequence;)V

    sput-object v0, Ln1$a;->g:Ln1$a;

    new-instance v0, Ln1$a;

    const/16 v1, 0x8

    invoke-direct {v0, v1, v2}, Ln1$a;-><init>(ILjava/lang/CharSequence;)V

    sput-object v0, Ln1$a;->h:Ln1$a;

    new-instance v0, Ln1$a;

    const/16 v1, 0x10

    invoke-direct {v0, v1, v2}, Ln1$a;-><init>(ILjava/lang/CharSequence;)V

    sput-object v0, Ln1$a;->i:Ln1$a;

    new-instance v0, Ln1$a;

    const/16 v1, 0x20

    invoke-direct {v0, v1, v2}, Ln1$a;-><init>(ILjava/lang/CharSequence;)V

    sput-object v0, Ln1$a;->j:Ln1$a;

    new-instance v0, Ln1$a;

    const/16 v3, 0x40

    invoke-direct {v0, v3, v2}, Ln1$a;-><init>(ILjava/lang/CharSequence;)V

    sput-object v0, Ln1$a;->k:Ln1$a;

    new-instance v0, Ln1$a;

    const/16 v3, 0x80

    invoke-direct {v0, v3, v2}, Ln1$a;-><init>(ILjava/lang/CharSequence;)V

    sput-object v0, Ln1$a;->l:Ln1$a;

    new-instance v0, Ln1$a;

    const/16 v3, 0x100

    const-class v4, Lq1$b;

    invoke-direct {v0, v3, v2, v4}, Ln1$a;-><init>(ILjava/lang/CharSequence;Ljava/lang/Class;)V

    sput-object v0, Ln1$a;->m:Ln1$a;

    new-instance v0, Ln1$a;

    const/16 v3, 0x200

    invoke-direct {v0, v3, v2, v4}, Ln1$a;-><init>(ILjava/lang/CharSequence;Ljava/lang/Class;)V

    sput-object v0, Ln1$a;->n:Ln1$a;

    new-instance v0, Ln1$a;

    const/16 v3, 0x400

    const-class v4, Lq1$c;

    invoke-direct {v0, v3, v2, v4}, Ln1$a;-><init>(ILjava/lang/CharSequence;Ljava/lang/Class;)V

    sput-object v0, Ln1$a;->o:Ln1$a;

    new-instance v0, Ln1$a;

    const/16 v3, 0x800

    invoke-direct {v0, v3, v2, v4}, Ln1$a;-><init>(ILjava/lang/CharSequence;Ljava/lang/Class;)V

    sput-object v0, Ln1$a;->p:Ln1$a;

    new-instance v0, Ln1$a;

    const/16 v3, 0x1000

    invoke-direct {v0, v3, v2}, Ln1$a;-><init>(ILjava/lang/CharSequence;)V

    sput-object v0, Ln1$a;->q:Ln1$a;

    new-instance v0, Ln1$a;

    const/16 v3, 0x2000

    invoke-direct {v0, v3, v2}, Ln1$a;-><init>(ILjava/lang/CharSequence;)V

    sput-object v0, Ln1$a;->r:Ln1$a;

    new-instance v0, Ln1$a;

    const/16 v3, 0x4000

    invoke-direct {v0, v3, v2}, Ln1$a;-><init>(ILjava/lang/CharSequence;)V

    sput-object v0, Ln1$a;->s:Ln1$a;

    new-instance v0, Ln1$a;

    const v3, 0x8000

    invoke-direct {v0, v3, v2}, Ln1$a;-><init>(ILjava/lang/CharSequence;)V

    sput-object v0, Ln1$a;->t:Ln1$a;

    new-instance v0, Ln1$a;

    const/high16 v3, 0x10000

    invoke-direct {v0, v3, v2}, Ln1$a;-><init>(ILjava/lang/CharSequence;)V

    sput-object v0, Ln1$a;->u:Ln1$a;

    new-instance v0, Ln1$a;

    const/high16 v3, 0x20000

    const-class v4, Lq1$g;

    invoke-direct {v0, v3, v2, v4}, Ln1$a;-><init>(ILjava/lang/CharSequence;Ljava/lang/Class;)V

    sput-object v0, Ln1$a;->v:Ln1$a;

    new-instance v0, Ln1$a;

    const/high16 v3, 0x40000

    invoke-direct {v0, v3, v2}, Ln1$a;-><init>(ILjava/lang/CharSequence;)V

    sput-object v0, Ln1$a;->w:Ln1$a;

    new-instance v0, Ln1$a;

    const/high16 v3, 0x80000

    invoke-direct {v0, v3, v2}, Ln1$a;-><init>(ILjava/lang/CharSequence;)V

    sput-object v0, Ln1$a;->x:Ln1$a;

    new-instance v0, Ln1$a;

    const/high16 v3, 0x100000

    invoke-direct {v0, v3, v2}, Ln1$a;-><init>(ILjava/lang/CharSequence;)V

    sput-object v0, Ln1$a;->y:Ln1$a;

    new-instance v0, Ln1$a;

    const/high16 v3, 0x200000

    const-class v4, Lq1$h;

    invoke-direct {v0, v3, v2, v4}, Ln1$a;-><init>(ILjava/lang/CharSequence;Ljava/lang/Class;)V

    sput-object v0, Ln1$a;->z:Ln1$a;

    new-instance v0, Ln1$a;

    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    sget-object v6, Landroid/view/accessibility/AccessibilityNodeInfo$AccessibilityAction;->ACTION_SHOW_ON_SCREEN:Landroid/view/accessibility/AccessibilityNodeInfo$AccessibilityAction;

    const v7, 0x1020036

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object v5, v0

    invoke-direct/range {v5 .. v10}, Ln1$a;-><init>(Ljava/lang/Object;ILjava/lang/CharSequence;Lq1;Ljava/lang/Class;)V

    sput-object v0, Ln1$a;->A:Ln1$a;

    new-instance v0, Ln1$a;

    sget-object v12, Landroid/view/accessibility/AccessibilityNodeInfo$AccessibilityAction;->ACTION_SCROLL_TO_POSITION:Landroid/view/accessibility/AccessibilityNodeInfo$AccessibilityAction;

    const v13, 0x1020037

    const/4 v14, 0x0

    const/4 v15, 0x0

    const-class v16, Lq1$e;

    move-object v11, v0

    invoke-direct/range {v11 .. v16}, Ln1$a;-><init>(Ljava/lang/Object;ILjava/lang/CharSequence;Lq1;Ljava/lang/Class;)V

    sput-object v0, Ln1$a;->B:Ln1$a;

    new-instance v0, Ln1$a;

    sget-object v5, Landroid/view/accessibility/AccessibilityNodeInfo$AccessibilityAction;->ACTION_SCROLL_UP:Landroid/view/accessibility/AccessibilityNodeInfo$AccessibilityAction;

    const v6, 0x1020038

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object v4, v0

    invoke-direct/range {v4 .. v9}, Ln1$a;-><init>(Ljava/lang/Object;ILjava/lang/CharSequence;Lq1;Ljava/lang/Class;)V

    sput-object v0, Ln1$a;->C:Ln1$a;

    new-instance v0, Ln1$a;

    sget-object v11, Landroid/view/accessibility/AccessibilityNodeInfo$AccessibilityAction;->ACTION_SCROLL_LEFT:Landroid/view/accessibility/AccessibilityNodeInfo$AccessibilityAction;

    const v12, 0x1020039

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    move-object v10, v0

    invoke-direct/range {v10 .. v15}, Ln1$a;-><init>(Ljava/lang/Object;ILjava/lang/CharSequence;Lq1;Ljava/lang/Class;)V

    sput-object v0, Ln1$a;->D:Ln1$a;

    new-instance v0, Ln1$a;

    sget-object v5, Landroid/view/accessibility/AccessibilityNodeInfo$AccessibilityAction;->ACTION_SCROLL_DOWN:Landroid/view/accessibility/AccessibilityNodeInfo$AccessibilityAction;

    const v6, 0x102003a

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object v4, v0

    invoke-direct/range {v4 .. v9}, Ln1$a;-><init>(Ljava/lang/Object;ILjava/lang/CharSequence;Lq1;Ljava/lang/Class;)V

    sput-object v0, Ln1$a;->E:Ln1$a;

    new-instance v0, Ln1$a;

    sget-object v11, Landroid/view/accessibility/AccessibilityNodeInfo$AccessibilityAction;->ACTION_SCROLL_RIGHT:Landroid/view/accessibility/AccessibilityNodeInfo$AccessibilityAction;

    const v12, 0x102003b

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    move-object v10, v0

    invoke-direct/range {v10 .. v15}, Ln1$a;-><init>(Ljava/lang/Object;ILjava/lang/CharSequence;Lq1;Ljava/lang/Class;)V

    sput-object v0, Ln1$a;->F:Ln1$a;

    new-instance v0, Ln1$a;

    const/16 v10, 0x1d

    if-lt v3, v10, :cond_0

    invoke-static {}, La1;->a()Landroid/view/accessibility/AccessibilityNodeInfo$AccessibilityAction;

    move-result-object v4

    move-object v5, v4

    goto :goto_0

    :cond_0
    move-object v5, v2

    :goto_0
    const v6, 0x1020046

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object v4, v0

    invoke-direct/range {v4 .. v9}, Ln1$a;-><init>(Ljava/lang/Object;ILjava/lang/CharSequence;Lq1;Ljava/lang/Class;)V

    sput-object v0, Ln1$a;->G:Ln1$a;

    new-instance v0, Ln1$a;

    if-lt v3, v10, :cond_1

    invoke-static {}, Lh1;->a()Landroid/view/accessibility/AccessibilityNodeInfo$AccessibilityAction;

    move-result-object v4

    move-object v12, v4

    goto :goto_1

    :cond_1
    move-object v12, v2

    :goto_1
    const v13, 0x1020047

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    move-object v11, v0

    invoke-direct/range {v11 .. v16}, Ln1$a;-><init>(Ljava/lang/Object;ILjava/lang/CharSequence;Lq1;Ljava/lang/Class;)V

    sput-object v0, Ln1$a;->H:Ln1$a;

    new-instance v0, Ln1$a;

    if-lt v3, v10, :cond_2

    invoke-static {}, Li1;->a()Landroid/view/accessibility/AccessibilityNodeInfo$AccessibilityAction;

    move-result-object v4

    move-object v5, v4

    goto :goto_2

    :cond_2
    move-object v5, v2

    :goto_2
    const v6, 0x1020048

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object v4, v0

    invoke-direct/range {v4 .. v9}, Ln1$a;-><init>(Ljava/lang/Object;ILjava/lang/CharSequence;Lq1;Ljava/lang/Class;)V

    sput-object v0, Ln1$a;->I:Ln1$a;

    new-instance v0, Ln1$a;

    if-lt v3, v10, :cond_3

    invoke-static {}, Lj1;->a()Landroid/view/accessibility/AccessibilityNodeInfo$AccessibilityAction;

    move-result-object v4

    move-object v12, v4

    goto :goto_3

    :cond_3
    move-object v12, v2

    :goto_3
    const v13, 0x1020049

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    move-object v11, v0

    invoke-direct/range {v11 .. v16}, Ln1$a;-><init>(Ljava/lang/Object;ILjava/lang/CharSequence;Lq1;Ljava/lang/Class;)V

    sput-object v0, Ln1$a;->J:Ln1$a;

    new-instance v0, Ln1$a;

    sget-object v5, Landroid/view/accessibility/AccessibilityNodeInfo$AccessibilityAction;->ACTION_CONTEXT_CLICK:Landroid/view/accessibility/AccessibilityNodeInfo$AccessibilityAction;

    const v6, 0x102003c

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object v4, v0

    invoke-direct/range {v4 .. v9}, Ln1$a;-><init>(Ljava/lang/Object;ILjava/lang/CharSequence;Lq1;Ljava/lang/Class;)V

    sput-object v0, Ln1$a;->K:Ln1$a;

    new-instance v0, Ln1$a;

    sget-object v11, Landroid/view/accessibility/AccessibilityNodeInfo$AccessibilityAction;->ACTION_SET_PROGRESS:Landroid/view/accessibility/AccessibilityNodeInfo$AccessibilityAction;

    const v12, 0x102003d

    const/4 v13, 0x0

    const/4 v14, 0x0

    const-class v15, Lq1$f;

    move-object v10, v0

    invoke-direct/range {v10 .. v15}, Ln1$a;-><init>(Ljava/lang/Object;ILjava/lang/CharSequence;Lq1;Ljava/lang/Class;)V

    sput-object v0, Ln1$a;->L:Ln1$a;

    new-instance v0, Ln1$a;

    const/16 v4, 0x1a

    if-lt v3, v4, :cond_4

    invoke-static {}, Lk1;->a()Landroid/view/accessibility/AccessibilityNodeInfo$AccessibilityAction;

    move-result-object v4

    move-object v5, v4

    goto :goto_4

    :cond_4
    move-object v5, v2

    :goto_4
    const v6, 0x1020042

    const/4 v7, 0x0

    const/4 v8, 0x0

    const-class v9, Lq1$d;

    move-object v4, v0

    invoke-direct/range {v4 .. v9}, Ln1$a;-><init>(Ljava/lang/Object;ILjava/lang/CharSequence;Lq1;Ljava/lang/Class;)V

    sput-object v0, Ln1$a;->M:Ln1$a;

    new-instance v0, Ln1$a;

    const/16 v4, 0x1c

    if-lt v3, v4, :cond_5

    invoke-static {}, Ll1;->a()Landroid/view/accessibility/AccessibilityNodeInfo$AccessibilityAction;

    move-result-object v5

    move-object v11, v5

    goto :goto_5

    :cond_5
    move-object v11, v2

    :goto_5
    const v12, 0x1020044

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    move-object v10, v0

    invoke-direct/range {v10 .. v15}, Ln1$a;-><init>(Ljava/lang/Object;ILjava/lang/CharSequence;Lq1;Ljava/lang/Class;)V

    sput-object v0, Ln1$a;->N:Ln1$a;

    new-instance v0, Ln1$a;

    if-lt v3, v4, :cond_6

    invoke-static {}, Lm1;->a()Landroid/view/accessibility/AccessibilityNodeInfo$AccessibilityAction;

    move-result-object v4

    move-object v6, v4

    goto :goto_6

    :cond_6
    move-object v6, v2

    :goto_6
    const v7, 0x1020045

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object v5, v0

    invoke-direct/range {v5 .. v10}, Ln1$a;-><init>(Ljava/lang/Object;ILjava/lang/CharSequence;Lq1;Ljava/lang/Class;)V

    sput-object v0, Ln1$a;->O:Ln1$a;

    new-instance v0, Ln1$a;

    const/16 v4, 0x1e

    if-lt v3, v4, :cond_7

    invoke-static {}, Lb1;->a()Landroid/view/accessibility/AccessibilityNodeInfo$AccessibilityAction;

    move-result-object v5

    move-object v12, v5

    goto :goto_7

    :cond_7
    move-object v12, v2

    :goto_7
    const v13, 0x102004a

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    move-object v11, v0

    invoke-direct/range {v11 .. v16}, Ln1$a;-><init>(Ljava/lang/Object;ILjava/lang/CharSequence;Lq1;Ljava/lang/Class;)V

    sput-object v0, Ln1$a;->P:Ln1$a;

    new-instance v0, Ln1$a;

    if-lt v3, v4, :cond_8

    invoke-static {}, Lc1;->a()Landroid/view/accessibility/AccessibilityNodeInfo$AccessibilityAction;

    move-result-object v4

    move-object v6, v4

    goto :goto_8

    :cond_8
    move-object v6, v2

    :goto_8
    const v7, 0x1020054

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object v5, v0

    invoke-direct/range {v5 .. v10}, Ln1$a;-><init>(Ljava/lang/Object;ILjava/lang/CharSequence;Lq1;Ljava/lang/Class;)V

    sput-object v0, Ln1$a;->Q:Ln1$a;

    new-instance v0, Ln1$a;

    if-lt v3, v1, :cond_9

    invoke-static {}, Ld1;->a()Landroid/view/accessibility/AccessibilityNodeInfo$AccessibilityAction;

    move-result-object v4

    move-object v12, v4

    goto :goto_9

    :cond_9
    move-object v12, v2

    :goto_9
    const v13, 0x1020055

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    move-object v11, v0

    invoke-direct/range {v11 .. v16}, Ln1$a;-><init>(Ljava/lang/Object;ILjava/lang/CharSequence;Lq1;Ljava/lang/Class;)V

    sput-object v0, Ln1$a;->R:Ln1$a;

    new-instance v0, Ln1$a;

    if-lt v3, v1, :cond_a

    invoke-static {}, Le1;->a()Landroid/view/accessibility/AccessibilityNodeInfo$AccessibilityAction;

    move-result-object v4

    move-object v5, v4

    goto :goto_a

    :cond_a
    move-object v5, v2

    :goto_a
    const v6, 0x1020056

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object v4, v0

    invoke-direct/range {v4 .. v9}, Ln1$a;-><init>(Ljava/lang/Object;ILjava/lang/CharSequence;Lq1;Ljava/lang/Class;)V

    sput-object v0, Ln1$a;->S:Ln1$a;

    new-instance v0, Ln1$a;

    if-lt v3, v1, :cond_b

    invoke-static {}, Lf1;->a()Landroid/view/accessibility/AccessibilityNodeInfo$AccessibilityAction;

    move-result-object v1

    move-object v11, v1

    goto :goto_b

    :cond_b
    move-object v11, v2

    :goto_b
    const v12, 0x1020057

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    move-object v10, v0

    invoke-direct/range {v10 .. v15}, Ln1$a;-><init>(Ljava/lang/Object;ILjava/lang/CharSequence;Lq1;Ljava/lang/Class;)V

    sput-object v0, Ln1$a;->T:Ln1$a;

    new-instance v0, Ln1$a;

    const/16 v1, 0x21

    if-lt v3, v1, :cond_c

    invoke-static {}, Lg1;->a()Landroid/view/accessibility/AccessibilityNodeInfo$AccessibilityAction;

    move-result-object v2

    :cond_c
    move-object v5, v2

    const v6, 0x1020058

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object v4, v0

    invoke-direct/range {v4 .. v9}, Ln1$a;-><init>(Ljava/lang/Object;ILjava/lang/CharSequence;Lq1;Ljava/lang/Class;)V

    sput-object v0, Ln1$a;->U:Ln1$a;

    return-void
.end method

.method public constructor <init>(ILjava/lang/CharSequence;)V
    .locals 6

    .line 1
    const/4 v1, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    move v2, p1

    move-object v3, p2

    invoke-direct/range {v0 .. v5}, Ln1$a;-><init>(Ljava/lang/Object;ILjava/lang/CharSequence;Lq1;Ljava/lang/Class;)V

    return-void
.end method

.method public constructor <init>(ILjava/lang/CharSequence;Ljava/lang/Class;)V
    .locals 6

    .line 3
    const/4 v1, 0x0

    const/4 v4, 0x0

    move-object v0, p0

    move v2, p1

    move-object v3, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Ln1$a;-><init>(Ljava/lang/Object;ILjava/lang/CharSequence;Lq1;Ljava/lang/Class;)V

    return-void
.end method

.method public constructor <init>(ILjava/lang/CharSequence;Lq1;)V
    .locals 6

    .line 2
    const/4 v1, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    move v2, p1

    move-object v3, p2

    move-object v4, p3

    invoke-direct/range {v0 .. v5}, Ln1$a;-><init>(Ljava/lang/Object;ILjava/lang/CharSequence;Lq1;Ljava/lang/Class;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Object;)V
    .locals 6

    .line 4
    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Ln1$a;-><init>(Ljava/lang/Object;ILjava/lang/CharSequence;Lq1;Ljava/lang/Class;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Object;ILjava/lang/CharSequence;Lq1;Ljava/lang/Class;)V
    .locals 0

    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p2, p0, Ln1$a;->b:I

    iput-object p4, p0, Ln1$a;->d:Lq1;

    if-nez p1, :cond_0

    new-instance p1, Landroid/view/accessibility/AccessibilityNodeInfo$AccessibilityAction;

    invoke-direct {p1, p2, p3}, Landroid/view/accessibility/AccessibilityNodeInfo$AccessibilityAction;-><init>(ILjava/lang/CharSequence;)V

    :cond_0
    iput-object p1, p0, Ln1$a;->a:Ljava/lang/Object;

    iput-object p5, p0, Ln1$a;->c:Ljava/lang/Class;

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/CharSequence;Lq1;)Ln1$a;
    .locals 7

    .line 1
    new-instance v6, Ln1$a;

    const/4 v1, 0x0

    iget v2, p0, Ln1$a;->b:I

    iget-object v5, p0, Ln1$a;->c:Ljava/lang/Class;

    move-object v0, v6

    move-object v3, p1

    move-object v4, p2

    invoke-direct/range {v0 .. v5}, Ln1$a;-><init>(Ljava/lang/Object;ILjava/lang/CharSequence;Lq1;Ljava/lang/Class;)V

    return-object v6
.end method

.method public b()I
    .locals 1

    .line 1
    iget-object v0, p0, Ln1$a;->a:Ljava/lang/Object;

    check-cast v0, Landroid/view/accessibility/AccessibilityNodeInfo$AccessibilityAction;

    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityNodeInfo$AccessibilityAction;->getId()I

    move-result v0

    return v0
.end method

.method public c()Ljava/lang/CharSequence;
    .locals 1

    .line 1
    iget-object v0, p0, Ln1$a;->a:Ljava/lang/Object;

    check-cast v0, Landroid/view/accessibility/AccessibilityNodeInfo$AccessibilityAction;

    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityNodeInfo$AccessibilityAction;->getLabel()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public d(Landroid/view/View;Landroid/os/Bundle;)Z
    .locals 4

    .line 1
    iget-object p2, p0, Ln1$a;->d:Lq1;

    const/4 v0, 0x0

    if-eqz p2, :cond_2

    iget-object p2, p0, Ln1$a;->c:Ljava/lang/Class;

    const/4 v1, 0x0

    if-nez p2, :cond_0

    goto :goto_1

    :cond_0
    :try_start_0
    new-array v2, v0, [Ljava/lang/Class;

    invoke-virtual {p2, v2}, Ljava/lang/Class;->getDeclaredConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object p2

    new-array v0, v0, [Ljava/lang/Object;

    invoke-virtual {p2, v0}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    invoke-static {p2}, Lzu0;->a(Ljava/lang/Object;)V

    throw v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception p2

    iget-object v0, p0, Ln1$a;->c:Ljava/lang/Class;

    if-nez v0, :cond_1

    const-string v0, "null"

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Failed to execute command with argument class ViewCommandArgument: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v2, "A11yActionCompat"

    invoke-static {v2, v0, p2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_1
    iget-object p2, p0, Ln1$a;->d:Lq1;

    invoke-interface {p2, p1, v1}, Lq1;->perform(Landroid/view/View;Lq1$a;)Z

    move-result p1

    return p1

    :cond_2
    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .line 1
    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    :cond_0
    instance-of v1, p1, Ln1$a;

    if-nez v1, :cond_1

    return v0

    :cond_1
    check-cast p1, Ln1$a;

    iget-object v1, p0, Ln1$a;->a:Ljava/lang/Object;

    iget-object p1, p1, Ln1$a;->a:Ljava/lang/Object;

    if-nez v1, :cond_2

    if-eqz p1, :cond_3

    return v0

    :cond_2
    invoke-virtual {v1, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_3

    return v0

    :cond_3
    const/4 p1, 0x1

    return p1
.end method

.method public hashCode()I
    .locals 1

    .line 1
    iget-object v0, p0, Ln1$a;->a:Ljava/lang/Object;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

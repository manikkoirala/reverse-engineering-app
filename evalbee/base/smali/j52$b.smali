.class public Lj52$b;
.super Landroid/animation/AnimatorListenerAdapter;
.source "SourceFile"

# interfaces
.implements Lqy1$g;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lj52;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "b"
.end annotation


# instance fields
.field public final a:Landroid/view/View;

.field public final b:I

.field public final c:Landroid/view/ViewGroup;

.field public final d:Z

.field public e:Z

.field public f:Z


# direct methods
.method public constructor <init>(Landroid/view/View;IZ)V
    .locals 1

    .line 1
    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lj52$b;->f:Z

    iput-object p1, p0, Lj52$b;->a:Landroid/view/View;

    iput p2, p0, Lj52$b;->b:I

    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object p1

    check-cast p1, Landroid/view/ViewGroup;

    iput-object p1, p0, Lj52$b;->c:Landroid/view/ViewGroup;

    iput-boolean p3, p0, Lj52$b;->d:Z

    const/4 p1, 0x1

    invoke-virtual {p0, p1}, Lj52$b;->b(Z)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .line 1
    iget-boolean v0, p0, Lj52$b;->f:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lj52$b;->a:Landroid/view/View;

    iget v1, p0, Lj52$b;->b:I

    invoke-static {v0, v1}, Lt42;->h(Landroid/view/View;I)V

    iget-object v0, p0, Lj52$b;->c:Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->invalidate()V

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lj52$b;->b(Z)V

    return-void
.end method

.method public final b(Z)V
    .locals 1

    .line 1
    iget-boolean v0, p0, Lj52$b;->d:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lj52$b;->e:Z

    if-eq v0, p1, :cond_0

    iget-object v0, p0, Lj52$b;->c:Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    iput-boolean p1, p0, Lj52$b;->e:Z

    invoke-static {v0, p1}, Lw32;->c(Landroid/view/ViewGroup;Z)V

    :cond_0
    return-void
.end method

.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 0

    .line 1
    const/4 p1, 0x1

    iput-boolean p1, p0, Lj52$b;->f:Z

    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lj52$b;->a()V

    return-void
.end method

.method public onAnimationPause(Landroid/animation/Animator;)V
    .locals 1

    .line 1
    iget-boolean p1, p0, Lj52$b;->f:Z

    if-nez p1, :cond_0

    iget-object p1, p0, Lj52$b;->a:Landroid/view/View;

    iget v0, p0, Lj52$b;->b:I

    invoke-static {p1, v0}, Lt42;->h(Landroid/view/View;I)V

    :cond_0
    return-void
.end method

.method public onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 0

    .line 1
    return-void
.end method

.method public onAnimationResume(Landroid/animation/Animator;)V
    .locals 1

    .line 1
    iget-boolean p1, p0, Lj52$b;->f:Z

    if-nez p1, :cond_0

    iget-object p1, p0, Lj52$b;->a:Landroid/view/View;

    const/4 v0, 0x0

    invoke-static {p1, v0}, Lt42;->h(Landroid/view/View;I)V

    :cond_0
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 0

    .line 1
    return-void
.end method

.method public onTransitionCancel(Lqy1;)V
    .locals 0

    .line 1
    return-void
.end method

.method public onTransitionEnd(Lqy1;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lj52$b;->a()V

    invoke-virtual {p1, p0}, Lqy1;->removeListener(Lqy1$g;)Lqy1;

    return-void
.end method

.method public onTransitionPause(Lqy1;)V
    .locals 0

    .line 1
    const/4 p1, 0x0

    invoke-virtual {p0, p1}, Lj52$b;->b(Z)V

    return-void
.end method

.method public onTransitionResume(Lqy1;)V
    .locals 0

    .line 1
    const/4 p1, 0x1

    invoke-virtual {p0, p1}, Lj52$b;->b(Z)V

    return-void
.end method

.method public onTransitionStart(Lqy1;)V
    .locals 0

    .line 1
    return-void
.end method

.class public final Lb92;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements La92;


# instance fields
.field public final a:Lq81;

.field public final b:Lhu1;


# direct methods
.method public constructor <init>(Lq81;Lhu1;)V
    .locals 1

    .line 1
    const-string v0, "processor"

    invoke-static {p1, v0}, Lfg0;->e(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "workTaskExecutor"

    invoke-static {p2, v0}, Lfg0;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lb92;->a:Lq81;

    iput-object p2, p0, Lb92;->b:Lhu1;

    return-void
.end method


# virtual methods
.method public a(Lop1;Landroidx/work/WorkerParameters$a;)V
    .locals 2

    .line 1
    const-string v0, "workSpecId"

    invoke-static {p1, v0}, Lfg0;->e(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lqp1;

    iget-object v1, p0, Lb92;->a:Lq81;

    invoke-direct {v0, v1, p1, p2}, Lqp1;-><init>(Lq81;Lop1;Landroidx/work/WorkerParameters$a;)V

    iget-object p1, p0, Lb92;->b:Lhu1;

    invoke-interface {p1, v0}, Lhu1;->b(Ljava/lang/Runnable;)V

    return-void
.end method

.method public d(Lop1;I)V
    .locals 4

    .line 1
    const-string v0, "workSpecId"

    invoke-static {p1, v0}, Lfg0;->e(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lb92;->b:Lhu1;

    new-instance v1, Lcq1;

    iget-object v2, p0, Lb92;->a:Lq81;

    const/4 v3, 0x0

    invoke-direct {v1, v2, p1, v3, p2}, Lcq1;-><init>(Lq81;Lop1;ZI)V

    invoke-interface {v0, v1}, Lhu1;->b(Ljava/lang/Runnable;)V

    return-void
.end method

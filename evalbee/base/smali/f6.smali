.class public abstract Lf6;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf6$b;,
        Lf6$a;
    }
.end annotation


# static fields
.field public static a:Lw7$a;

.field public static b:I

.field public static c:Lnl0;

.field public static d:Lnl0;

.field public static e:Ljava/lang/Boolean;

.field public static f:Z

.field public static final g:Ls8;

.field public static final h:Ljava/lang/Object;

.field public static final i:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lw7$a;

    new-instance v1, Lw7$b;

    invoke-direct {v1}, Lw7$b;-><init>()V

    invoke-direct {v0, v1}, Lw7$a;-><init>(Ljava/util/concurrent/Executor;)V

    sput-object v0, Lf6;->a:Lw7$a;

    const/16 v0, -0x64

    sput v0, Lf6;->b:I

    const/4 v0, 0x0

    sput-object v0, Lf6;->c:Lnl0;

    sput-object v0, Lf6;->d:Lnl0;

    sput-object v0, Lf6;->e:Ljava/lang/Boolean;

    const/4 v0, 0x0

    sput-boolean v0, Lf6;->f:Z

    new-instance v0, Ls8;

    invoke-direct {v0}, Ls8;-><init>()V

    sput-object v0, Lf6;->g:Ls8;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lf6;->h:Ljava/lang/Object;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lf6;->i:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static C(Lf6;)V
    .locals 1

    .line 1
    sget-object v0, Lf6;->h:Ljava/lang/Object;

    monitor-enter v0

    :try_start_0
    invoke-static {p0}, Lf6;->D(Lf6;)V

    monitor-exit v0

    return-void

    :catchall_0
    move-exception p0

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p0
.end method

.method public static D(Lf6;)V
    .locals 3

    .line 1
    sget-object v0, Lf6;->h:Ljava/lang/Object;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lf6;->g:Ls8;

    invoke-virtual {v1}, Ls8;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lf6;

    if-eq v2, p0, :cond_1

    if-nez v2, :cond_0

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    :cond_2
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p0

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p0
.end method

.method public static M(Landroid/content/Context;)V
    .locals 3

    .line 1
    invoke-static {p0}, Lf6;->s(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-static {}, Lyc;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-boolean v0, Lf6;->f:Z

    if-nez v0, :cond_6

    sget-object v0, Lf6;->a:Lw7$a;

    new-instance v1, Le6;

    invoke-direct {v1, p0}, Le6;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Lw7$a;->execute(Ljava/lang/Runnable;)V

    goto :goto_1

    :cond_1
    sget-object v0, Lf6;->i:Ljava/lang/Object;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lf6;->c:Lnl0;

    if-nez v1, :cond_4

    sget-object v1, Lf6;->d:Lnl0;

    if-nez v1, :cond_2

    invoke-static {p0}, Lw7;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Lnl0;->b(Ljava/lang/String;)Lnl0;

    move-result-object p0

    sput-object p0, Lf6;->d:Lnl0;

    :cond_2
    sget-object p0, Lf6;->d:Lnl0;

    invoke-virtual {p0}, Lnl0;->e()Z

    move-result p0

    if-eqz p0, :cond_3

    monitor-exit v0

    return-void

    :cond_3
    sget-object p0, Lf6;->d:Lnl0;

    sput-object p0, Lf6;->c:Lnl0;

    goto :goto_0

    :cond_4
    sget-object v2, Lf6;->d:Lnl0;

    invoke-virtual {v1, v2}, Lnl0;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    sget-object v1, Lf6;->c:Lnl0;

    sput-object v1, Lf6;->d:Lnl0;

    invoke-virtual {v1}, Lnl0;->g()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lw7;->a(Landroid/content/Context;Ljava/lang/String;)V

    :cond_5
    :goto_0
    monitor-exit v0

    :cond_6
    :goto_1
    return-void

    :catchall_0
    move-exception p0

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p0
.end method

.method public static synthetic a(Landroid/content/Context;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lf6;->t(Landroid/content/Context;)V

    return-void
.end method

.method public static b(Lf6;)V
    .locals 3

    .line 1
    sget-object v0, Lf6;->h:Ljava/lang/Object;

    monitor-enter v0

    :try_start_0
    invoke-static {p0}, Lf6;->D(Lf6;)V

    sget-object v1, Lf6;->g:Ls8;

    new-instance v2, Ljava/lang/ref/WeakReference;

    invoke-direct {v2, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v1, v2}, Ls8;->add(Ljava/lang/Object;)Z

    monitor-exit v0

    return-void

    :catchall_0
    move-exception p0

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p0
.end method

.method public static f(Landroid/app/Activity;Lz5;)Lf6;
    .locals 1

    .line 1
    new-instance v0, Lg6;

    invoke-direct {v0, p0, p1}, Lg6;-><init>(Landroid/app/Activity;Lz5;)V

    return-object v0
.end method

.method public static g(Landroid/app/Dialog;Lz5;)Lf6;
    .locals 1

    .line 1
    new-instance v0, Lg6;

    invoke-direct {v0, p0, p1}, Lg6;-><init>(Landroid/app/Dialog;Lz5;)V

    return-object v0
.end method

.method public static i()Lnl0;
    .locals 1

    .line 1
    invoke-static {}, Lyc;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lf6;->m()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-static {v0}, Lf6$b;->a(Ljava/lang/Object;)Landroid/os/LocaleList;

    move-result-object v0

    invoke-static {v0}, Lnl0;->h(Landroid/os/LocaleList;)Lnl0;

    move-result-object v0

    return-object v0

    :cond_0
    sget-object v0, Lf6;->c:Lnl0;

    if-eqz v0, :cond_1

    return-object v0

    :cond_1
    invoke-static {}, Lnl0;->d()Lnl0;

    move-result-object v0

    return-object v0
.end method

.method public static k()I
    .locals 1

    .line 1
    sget v0, Lf6;->b:I

    return v0
.end method

.method public static m()Ljava/lang/Object;
    .locals 2

    .line 1
    sget-object v0, Lf6;->g:Ls8;

    invoke-virtual {v0}, Ls8;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/Reference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lf6;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lf6;->j()Landroid/content/Context;

    move-result-object v1

    if-eqz v1, :cond_0

    const-string v0, "locale"

    invoke-virtual {v1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    return-object v0

    :cond_1
    const/4 v0, 0x0

    return-object v0
.end method

.method public static o()Lnl0;
    .locals 1

    .line 1
    sget-object v0, Lf6;->c:Lnl0;

    return-object v0
.end method

.method public static s(Landroid/content/Context;)Z
    .locals 1

    .line 1
    sget-object v0, Lf6;->e:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    :try_start_0
    invoke-static {p0}, Lu7;->a(Landroid/content/Context;)Landroid/content/pm/ServiceInfo;

    move-result-object p0

    iget-object p0, p0, Landroid/content/pm/ServiceInfo;->metaData:Landroid/os/Bundle;

    if-eqz p0, :cond_0

    const-string v0, "autoStoreLocales"

    invoke-virtual {p0, v0}, Landroid/os/BaseBundle;->getBoolean(Ljava/lang/String;)Z

    move-result p0

    invoke-static {p0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    sput-object p0, Lf6;->e:Ljava/lang/Boolean;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    const-string p0, "AppCompatDelegate"

    const-string v0, "Checking for metadata for AppLocalesMetadataHolderService : Service not found"

    invoke-static {p0, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    sget-object p0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    sput-object p0, Lf6;->e:Ljava/lang/Boolean;

    :cond_0
    :goto_0
    sget-object p0, Lf6;->e:Ljava/lang/Boolean;

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    return p0
.end method

.method public static synthetic t(Landroid/content/Context;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lw7;->c(Landroid/content/Context;)V

    const/4 p0, 0x1

    sput-boolean p0, Lf6;->f:Z

    return-void
.end method


# virtual methods
.method public abstract A()V
.end method

.method public abstract B()V
.end method

.method public abstract E(I)Z
.end method

.method public abstract F(I)V
.end method

.method public abstract G(Landroid/view/View;)V
.end method

.method public abstract H(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
.end method

.method public I(Landroid/window/OnBackInvokedDispatcher;)V
    .locals 0

    .line 1
    return-void
.end method

.method public abstract J(Landroidx/appcompat/widget/Toolbar;)V
.end method

.method public abstract K(I)V
.end method

.method public abstract L(Ljava/lang/CharSequence;)V
.end method

.method public abstract c(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
.end method

.method public d(Landroid/content/Context;)V
    .locals 0

    .line 1
    return-void
.end method

.method public e(Landroid/content/Context;)Landroid/content/Context;
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lf6;->d(Landroid/content/Context;)V

    return-object p1
.end method

.method public abstract h(I)Landroid/view/View;
.end method

.method public abstract j()Landroid/content/Context;
.end method

.method public abstract l()I
.end method

.method public abstract n()Landroid/view/MenuInflater;
.end method

.method public abstract p()Lt1;
.end method

.method public abstract q()V
.end method

.method public abstract r()V
.end method

.method public abstract u(Landroid/content/res/Configuration;)V
.end method

.method public abstract v(Landroid/os/Bundle;)V
.end method

.method public abstract w()V
.end method

.method public abstract x(Landroid/os/Bundle;)V
.end method

.method public abstract y()V
.end method

.method public abstract z(Landroid/os/Bundle;)V
.end method

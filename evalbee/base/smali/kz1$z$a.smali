.class public Lkz1$z$a;
.super Lhz1;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lkz1$z;->a(Lgc0;Lcom/google/gson/reflect/TypeToken;)Lhz1;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/Class;

.field public final synthetic b:Lkz1$z;


# direct methods
.method public constructor <init>(Lkz1$z;Ljava/lang/Class;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lkz1$z$a;->b:Lkz1$z;

    iput-object p2, p0, Lkz1$z$a;->a:Ljava/lang/Class;

    invoke-direct {p0}, Lhz1;-><init>()V

    return-void
.end method


# virtual methods
.method public b(Lrh0;)Ljava/lang/Object;
    .locals 4

    .line 1
    iget-object v0, p0, Lkz1$z$a;->b:Lkz1$z;

    iget-object v0, v0, Lkz1$z;->b:Lhz1;

    invoke-virtual {v0, p1}, Lhz1;->b(Lrh0;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lkz1$z$a;->a:Ljava/lang/Class;

    invoke-virtual {v1, v0}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    new-instance v1, Lcom/google/gson/JsonSyntaxException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Expected a "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lkz1$z$a;->a:Ljava/lang/Class;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, " but was "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "; at path "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lrh0;->j()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v1, p1}, Lcom/google/gson/JsonSyntaxException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    :goto_0
    return-object v0
.end method

.method public d(Lvh0;Ljava/lang/Object;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lkz1$z$a;->b:Lkz1$z;

    iget-object v0, v0, Lkz1$z;->b:Lhz1;

    invoke-virtual {v0, p1, p2}, Lhz1;->d(Lvh0;Ljava/lang/Object;)V

    return-void
.end method

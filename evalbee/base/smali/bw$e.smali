.class public Lbw$e;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lzg;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lbw;->k(Ljava/lang/String;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Landroid/app/ProgressDialog;

.field public final synthetic b:Ljava/lang/String;

.field public final synthetic c:Ljava/lang/String;

.field public final synthetic d:Lbw;


# direct methods
.method public constructor <init>(Lbw;Landroid/app/ProgressDialog;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lbw$e;->d:Lbw;

    iput-object p2, p0, Lbw$e;->a:Landroid/app/ProgressDialog;

    iput-object p3, p0, Lbw$e;->b:Ljava/lang/String;

    iput-object p4, p0, Lbw$e;->c:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(ZILjava/lang/Object;)V
    .locals 1

    .line 1
    iget-object p2, p0, Lbw$e;->a:Landroid/app/ProgressDialog;

    invoke-virtual {p2}, Landroid/app/Dialog;->dismiss()V

    if-eqz p1, :cond_1

    iget-object p1, p0, Lbw$e;->d:Lbw;

    iget-object p1, p1, Lbw;->a:Landroid/content/Context;

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;

    move-result-object p1

    iget-object p2, p0, Lbw$e;->b:Ljava/lang/String;

    invoke-virtual {p1, p2}, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;->setDisplayName(Ljava/lang/String;)V

    iget-object p2, p0, Lbw$e;->c:Ljava/lang/String;

    invoke-virtual {p1, p2}, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;->setOrganization(Ljava/lang/String;)V

    iget-object p2, p0, Lbw$e;->d:Lbw;

    iget-object p2, p2, Lbw;->a:Landroid/content/Context;

    invoke-virtual {p1, p2}, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;->save(Landroid/content/Context;)V

    iget-object p1, p0, Lbw$e;->d:Lbw;

    iget-object p1, p1, Lbw;->b:Ly01;

    if-eqz p1, :cond_0

    const/4 p2, 0x0

    invoke-interface {p1, p2}, Ly01;->a(Ljava/lang/Object;)V

    :cond_0
    iget-object p1, p0, Lbw$e;->d:Lbw;

    iget-object p1, p1, Lbw;->n:Lbw;

    invoke-virtual {p1}, Landroid/app/Dialog;->dismiss()V

    goto :goto_0

    :cond_1
    iget-object p1, p0, Lbw$e;->d:Lbw;

    iget-object p1, p1, Lbw;->a:Landroid/content/Context;

    const p2, 0x7f0800bd

    const p3, 0x7f08016e

    const-string v0, "Save failed, please try again"

    invoke-static {p1, v0, p2, p3}, La91;->H(Landroid/content/Context;Ljava/lang/String;II)V

    :goto_0
    return-void
.end method

.class public abstract Ln92;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ln92$a;,
        Ln92$b;
    }
.end annotation


# static fields
.field public static final d:Ln92$b;


# instance fields
.field public final a:Ljava/util/UUID;

.field public final b:Lp92;

.field public final c:Ljava/util/Set;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Ln92$b;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ln92$b;-><init>(Lgq;)V

    sput-object v0, Ln92;->d:Ln92$b;

    return-void
.end method

.method public constructor <init>(Ljava/util/UUID;Lp92;Ljava/util/Set;)V
    .locals 1

    .line 1
    const-string v0, "id"

    invoke-static {p1, v0}, Lfg0;->e(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "workSpec"

    invoke-static {p2, v0}, Lfg0;->e(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "tags"

    invoke-static {p3, v0}, Lfg0;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Ln92;->a:Ljava/util/UUID;

    iput-object p2, p0, Ln92;->b:Lp92;

    iput-object p3, p0, Ln92;->c:Ljava/util/Set;

    return-void
.end method


# virtual methods
.method public a()Ljava/util/UUID;
    .locals 1

    .line 1
    iget-object v0, p0, Ln92;->a:Ljava/util/UUID;

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 2

    .line 1
    invoke-virtual {p0}, Ln92;->a()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "id.toString()"

    invoke-static {v0, v1}, Lfg0;->d(Ljava/lang/Object;Ljava/lang/String;)V

    return-object v0
.end method

.method public final c()Ljava/util/Set;
    .locals 1

    .line 1
    iget-object v0, p0, Ln92;->c:Ljava/util/Set;

    return-object v0
.end method

.method public final d()Lp92;
    .locals 1

    .line 1
    iget-object v0, p0, Ln92;->b:Lp92;

    return-object v0
.end method

.class public Ly6;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Landroid/widget/ImageView;

.field public b:Lrw1;

.field public c:Lrw1;

.field public d:Lrw1;

.field public e:I


# direct methods
.method public constructor <init>(Landroid/widget/ImageView;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Ly6;->e:I

    iput-object p1, p0, Ly6;->a:Landroid/widget/ImageView;

    return-void
.end method


# virtual methods
.method public final a(Landroid/graphics/drawable/Drawable;)Z
    .locals 3

    .line 1
    iget-object v0, p0, Ly6;->d:Lrw1;

    if-nez v0, :cond_0

    new-instance v0, Lrw1;

    invoke-direct {v0}, Lrw1;-><init>()V

    iput-object v0, p0, Ly6;->d:Lrw1;

    :cond_0
    iget-object v0, p0, Ly6;->d:Lrw1;

    invoke-virtual {v0}, Lrw1;->a()V

    iget-object v1, p0, Ly6;->a:Landroid/widget/ImageView;

    invoke-static {v1}, Lje0;->a(Landroid/widget/ImageView;)Landroid/content/res/ColorStateList;

    move-result-object v1

    const/4 v2, 0x1

    if-eqz v1, :cond_1

    iput-boolean v2, v0, Lrw1;->d:Z

    iput-object v1, v0, Lrw1;->a:Landroid/content/res/ColorStateList;

    :cond_1
    iget-object v1, p0, Ly6;->a:Landroid/widget/ImageView;

    invoke-static {v1}, Lje0;->b(Landroid/widget/ImageView;)Landroid/graphics/PorterDuff$Mode;

    move-result-object v1

    if-eqz v1, :cond_2

    iput-boolean v2, v0, Lrw1;->c:Z

    iput-object v1, v0, Lrw1;->b:Landroid/graphics/PorterDuff$Mode;

    :cond_2
    iget-boolean v1, v0, Lrw1;->d:Z

    if-nez v1, :cond_4

    iget-boolean v1, v0, Lrw1;->c:Z

    if-eqz v1, :cond_3

    goto :goto_0

    :cond_3
    const/4 p1, 0x0

    return p1

    :cond_4
    :goto_0
    iget-object v1, p0, Ly6;->a:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/view/View;->getDrawableState()[I

    move-result-object v1

    invoke-static {p1, v0, v1}, Ls6;->i(Landroid/graphics/drawable/Drawable;Lrw1;[I)V

    return v2
.end method

.method public b()V
    .locals 2

    .line 1
    iget-object v0, p0, Ly6;->a:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ly6;->a:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iget v1, p0, Ly6;->e:I

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setLevel(I)Z

    :cond_0
    return-void
.end method

.method public c()V
    .locals 3

    .line 1
    iget-object v0, p0, Ly6;->a:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {v0}, Lfv;->b(Landroid/graphics/drawable/Drawable;)V

    :cond_0
    if-eqz v0, :cond_3

    invoke-virtual {p0}, Ly6;->l()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0, v0}, Ly6;->a(Landroid/graphics/drawable/Drawable;)Z

    move-result v1

    if-eqz v1, :cond_1

    return-void

    :cond_1
    iget-object v1, p0, Ly6;->c:Lrw1;

    if-eqz v1, :cond_2

    iget-object v2, p0, Ly6;->a:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/view/View;->getDrawableState()[I

    move-result-object v2

    invoke-static {v0, v1, v2}, Ls6;->i(Landroid/graphics/drawable/Drawable;Lrw1;[I)V

    goto :goto_0

    :cond_2
    iget-object v1, p0, Ly6;->b:Lrw1;

    if-eqz v1, :cond_3

    iget-object v2, p0, Ly6;->a:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/view/View;->getDrawableState()[I

    move-result-object v2

    invoke-static {v0, v1, v2}, Ls6;->i(Landroid/graphics/drawable/Drawable;Lrw1;[I)V

    :cond_3
    :goto_0
    return-void
.end method

.method public d()Landroid/content/res/ColorStateList;
    .locals 1

    .line 1
    iget-object v0, p0, Ly6;->c:Lrw1;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lrw1;->a:Landroid/content/res/ColorStateList;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public e()Landroid/graphics/PorterDuff$Mode;
    .locals 1

    .line 1
    iget-object v0, p0, Ly6;->c:Lrw1;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lrw1;->b:Landroid/graphics/PorterDuff$Mode;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public f()Z
    .locals 1

    .line 1
    iget-object v0, p0, Ly6;->a:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    instance-of v0, v0, Landroid/graphics/drawable/RippleDrawable;

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    return v0

    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public g(Landroid/util/AttributeSet;I)V
    .locals 8

    .line 1
    iget-object v0, p0, Ly6;->a:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    sget-object v3, Lbc1;->P:[I

    const/4 v1, 0x0

    invoke-static {v0, p1, v3, p2, v1}, Ltw1;->v(Landroid/content/Context;Landroid/util/AttributeSet;[III)Ltw1;

    move-result-object v0

    iget-object v1, p0, Ly6;->a:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v0}, Ltw1;->r()Landroid/content/res/TypedArray;

    move-result-object v5

    const/4 v7, 0x0

    move-object v4, p1

    move v6, p2

    invoke-static/range {v1 .. v7}, Lo32;->o0(Landroid/view/View;Landroid/content/Context;[ILandroid/util/AttributeSet;Landroid/content/res/TypedArray;II)V

    :try_start_0
    iget-object p1, p0, Ly6;->a:Landroid/widget/ImageView;

    invoke-virtual {p1}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object p1

    const/4 p2, -0x1

    if-nez p1, :cond_0

    sget v1, Lbc1;->Q:I

    invoke-virtual {v0, v1, p2}, Ltw1;->n(II)I

    move-result v1

    if-eq v1, p2, :cond_0

    iget-object p1, p0, Ly6;->a:Landroid/widget/ImageView;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p1, v1}, Lg7;->b(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    if-eqz p1, :cond_0

    iget-object v1, p0, Ly6;->a:Landroid/widget/ImageView;

    invoke-virtual {v1, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    :cond_0
    if-eqz p1, :cond_1

    invoke-static {p1}, Lfv;->b(Landroid/graphics/drawable/Drawable;)V

    :cond_1
    sget p1, Lbc1;->R:I

    invoke-virtual {v0, p1}, Ltw1;->s(I)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Ly6;->a:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Ltw1;->c(I)Landroid/content/res/ColorStateList;

    move-result-object p1

    invoke-static {v1, p1}, Lje0;->c(Landroid/widget/ImageView;Landroid/content/res/ColorStateList;)V

    :cond_2
    sget p1, Lbc1;->S:I

    invoke-virtual {v0, p1}, Ltw1;->s(I)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Ly6;->a:Landroid/widget/ImageView;

    invoke-virtual {v0, p1, p2}, Ltw1;->k(II)I

    move-result p1

    const/4 p2, 0x0

    invoke-static {p1, p2}, Lfv;->e(ILandroid/graphics/PorterDuff$Mode;)Landroid/graphics/PorterDuff$Mode;

    move-result-object p1

    invoke-static {v1, p1}, Lje0;->d(Landroid/widget/ImageView;Landroid/graphics/PorterDuff$Mode;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_3
    invoke-virtual {v0}, Ltw1;->w()V

    return-void

    :catchall_0
    move-exception p1

    invoke-virtual {v0}, Ltw1;->w()V

    throw p1
.end method

.method public h(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .line 1
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getLevel()I

    move-result p1

    iput p1, p0, Ly6;->e:I

    return-void
.end method

.method public i(I)V
    .locals 1

    .line 1
    if-eqz p1, :cond_1

    iget-object v0, p0, Ly6;->a:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, Lg7;->b(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-static {p1}, Lfv;->b(Landroid/graphics/drawable/Drawable;)V

    :cond_0
    iget-object v0, p0, Ly6;->a:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    :cond_1
    iget-object p1, p0, Ly6;->a:Landroid/widget/ImageView;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    :goto_0
    invoke-virtual {p0}, Ly6;->c()V

    return-void
.end method

.method public j(Landroid/content/res/ColorStateList;)V
    .locals 1

    .line 1
    iget-object v0, p0, Ly6;->c:Lrw1;

    if-nez v0, :cond_0

    new-instance v0, Lrw1;

    invoke-direct {v0}, Lrw1;-><init>()V

    iput-object v0, p0, Ly6;->c:Lrw1;

    :cond_0
    iget-object v0, p0, Ly6;->c:Lrw1;

    iput-object p1, v0, Lrw1;->a:Landroid/content/res/ColorStateList;

    const/4 p1, 0x1

    iput-boolean p1, v0, Lrw1;->d:Z

    invoke-virtual {p0}, Ly6;->c()V

    return-void
.end method

.method public k(Landroid/graphics/PorterDuff$Mode;)V
    .locals 1

    .line 1
    iget-object v0, p0, Ly6;->c:Lrw1;

    if-nez v0, :cond_0

    new-instance v0, Lrw1;

    invoke-direct {v0}, Lrw1;-><init>()V

    iput-object v0, p0, Ly6;->c:Lrw1;

    :cond_0
    iget-object v0, p0, Ly6;->c:Lrw1;

    iput-object p1, v0, Lrw1;->b:Landroid/graphics/PorterDuff$Mode;

    const/4 p1, 0x1

    iput-boolean p1, v0, Lrw1;->c:Z

    invoke-virtual {p0}, Ly6;->c()V

    return-void
.end method

.method public final l()Z
    .locals 1

    .line 1
    iget-object v0, p0, Ly6;->b:Lrw1;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

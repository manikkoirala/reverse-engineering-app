.class public final Lmh0;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lzw;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmh0$b;
    }
.end annotation


# static fields
.field public static final e:Lw01;

.field public static final f:Lx22;

.field public static final g:Lx22;

.field public static final h:Lmh0$b;


# instance fields
.field public final a:Ljava/util/Map;

.field public final b:Ljava/util/Map;

.field public c:Lw01;

.field public d:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Ljh0;

    invoke-direct {v0}, Ljh0;-><init>()V

    sput-object v0, Lmh0;->e:Lw01;

    new-instance v0, Lkh0;

    invoke-direct {v0}, Lkh0;-><init>()V

    sput-object v0, Lmh0;->f:Lx22;

    new-instance v0, Llh0;

    invoke-direct {v0}, Llh0;-><init>()V

    sput-object v0, Lmh0;->g:Lx22;

    new-instance v0, Lmh0$b;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lmh0$b;-><init>(Lmh0$a;)V

    sput-object v0, Lmh0;->h:Lmh0$b;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lmh0;->a:Ljava/util/Map;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lmh0;->b:Ljava/util/Map;

    sget-object v0, Lmh0;->e:Lw01;

    iput-object v0, p0, Lmh0;->c:Lw01;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmh0;->d:Z

    const-class v0, Ljava/lang/String;

    sget-object v1, Lmh0;->f:Lx22;

    invoke-virtual {p0, v0, v1}, Lmh0;->p(Ljava/lang/Class;Lx22;)Lmh0;

    const-class v0, Ljava/lang/Boolean;

    sget-object v1, Lmh0;->g:Lx22;

    invoke-virtual {p0, v0, v1}, Lmh0;->p(Ljava/lang/Class;Lx22;)Lmh0;

    const-class v0, Ljava/util/Date;

    sget-object v1, Lmh0;->h:Lmh0$b;

    invoke-virtual {p0, v0, v1}, Lmh0;->p(Ljava/lang/Class;Lx22;)Lmh0;

    return-void
.end method

.method public static synthetic b(Ljava/lang/Object;Lx01;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lmh0;->l(Ljava/lang/Object;Lx01;)V

    return-void
.end method

.method public static synthetic c(Ljava/lang/String;Ly22;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lmh0;->m(Ljava/lang/String;Ly22;)V

    return-void
.end method

.method public static synthetic d(Ljava/lang/Boolean;Ly22;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lmh0;->n(Ljava/lang/Boolean;Ly22;)V

    return-void
.end method

.method public static synthetic e(Lmh0;)Ljava/util/Map;
    .locals 0

    .line 1
    iget-object p0, p0, Lmh0;->a:Ljava/util/Map;

    return-object p0
.end method

.method public static synthetic f(Lmh0;)Ljava/util/Map;
    .locals 0

    .line 1
    iget-object p0, p0, Lmh0;->b:Ljava/util/Map;

    return-object p0
.end method

.method public static synthetic g(Lmh0;)Lw01;
    .locals 0

    .line 1
    iget-object p0, p0, Lmh0;->c:Lw01;

    return-object p0
.end method

.method public static synthetic h(Lmh0;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lmh0;->d:Z

    return p0
.end method

.method public static synthetic l(Ljava/lang/Object;Lx01;)V
    .locals 2

    .line 1
    new-instance p1, Lcom/google/firebase/encoders/EncodingException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Couldn\'t find encoder for type "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {p1, p0}, Lcom/google/firebase/encoders/EncodingException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public static synthetic m(Ljava/lang/String;Ly22;)V
    .locals 0

    .line 1
    invoke-interface {p1, p0}, Ly22;->add(Ljava/lang/String;)Ly22;

    return-void
.end method

.method public static synthetic n(Ljava/lang/Boolean;Ly22;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    invoke-interface {p1, p0}, Ly22;->a(Z)Ly22;

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Class;Lw01;)Lzw;
    .locals 0

    .line 1
    invoke-virtual {p0, p1, p2}, Lmh0;->o(Ljava/lang/Class;Lw01;)Lmh0;

    move-result-object p1

    return-object p1
.end method

.method public i()Lhp;
    .locals 1

    .line 1
    new-instance v0, Lmh0$a;

    invoke-direct {v0, p0}, Lmh0$a;-><init>(Lmh0;)V

    return-object v0
.end method

.method public j(Ljk;)Lmh0;
    .locals 0

    .line 1
    invoke-interface {p1, p0}, Ljk;->configure(Lzw;)V

    return-object p0
.end method

.method public k(Z)Lmh0;
    .locals 0

    .line 1
    iput-boolean p1, p0, Lmh0;->d:Z

    return-object p0
.end method

.method public o(Ljava/lang/Class;Lw01;)Lmh0;
    .locals 1

    .line 1
    iget-object v0, p0, Lmh0;->a:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object p2, p0, Lmh0;->b:Ljava/util/Map;

    invoke-interface {p2, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public p(Ljava/lang/Class;Lx22;)Lmh0;
    .locals 1

    .line 1
    iget-object v0, p0, Lmh0;->b:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object p2, p0, Lmh0;->a:Ljava/util/Map;

    invoke-interface {p2, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

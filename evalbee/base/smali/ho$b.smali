.class public Lho$b;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lho;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "b"
.end annotation


# instance fields
.field public a:I

.field public b:I

.field public c:Landroid/app/ProgressDialog;

.field public final synthetic d:Lho;


# direct methods
.method public constructor <init>(Lho;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lho$b;->d:Lho;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    const/4 p1, 0x0

    iput p1, p0, Lho$b;->a:I

    iput p1, p0, Lho$b;->b:I

    return-void
.end method

.method public synthetic constructor <init>(Lho;Lho$a;)V
    .locals 0

    .line 2
    invoke-direct {p0, p1}, Lho$b;-><init>(Lho;)V

    return-void
.end method


# virtual methods
.method public varargs a([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 12

    .line 1
    const-string p1, "[0-9]+"

    iget-object v0, p0, Lho$b;->d:Lho;

    invoke-static {v0}, Lho;->b(Lho;)Ljava/io/InputStream;

    move-result-object v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, p0, Lho$b;->d:Lho;

    iget-object v2, v0, Lho;->i:Ljava/lang/String;

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "Header not found, make sure you are importing comma separated format"

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    iput-object p1, v0, Lho;->i:Ljava/lang/String;

    return-object v1

    :cond_0
    :try_start_0
    new-instance v0, Lyd;

    new-instance v2, Ljava/io/InputStreamReader;

    iget-object v3, p0, Lho$b;->d:Lho;

    invoke-static {v3}, Lho;->b(Lho;)Ljava/io/InputStream;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v0, v2}, Lyd;-><init>(Ljava/io/Reader;)V

    invoke-virtual {v0}, Lyd;->c()[Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    :goto_0
    array-length v4, v2

    if-ge v3, v4, :cond_6

    aget-object v4, v2, v3

    invoke-virtual {v4}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v4

    const-string v5, "NAME"

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v4, p0, Lho$b;->d:Lho;

    invoke-static {v4, v3}, Lho;->d(Lho;I)I

    goto :goto_1

    :cond_1
    aget-object v4, v2, v3

    invoke-virtual {v4}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v4

    const-string v5, "ROLLNO"

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object v4, p0, Lho$b;->d:Lho;

    invoke-static {v4, v3}, Lho;->f(Lho;I)I

    goto :goto_1

    :cond_2
    aget-object v4, v2, v3

    invoke-virtual {v4}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v4

    const-string v5, "EMAILID"

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_3

    iget-object v4, p0, Lho$b;->d:Lho;

    invoke-static {v4, v3}, Lho;->h(Lho;I)I

    goto :goto_1

    :cond_3
    aget-object v4, v2, v3

    invoke-virtual {v4}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v4

    const-string v5, "PHONENO"

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_4

    iget-object v4, p0, Lho$b;->d:Lho;

    invoke-static {v4, v3}, Lho;->j(Lho;I)I

    goto :goto_1

    :cond_4
    aget-object v4, v2, v3

    invoke-virtual {v4}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v4

    const-string v5, "CLASS"

    invoke-virtual {v4, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_5

    iget-object v4, p0, Lho$b;->d:Lho;

    invoke-static {v4, v3}, Lho;->l(Lho;I)I

    :cond_5
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_6
    iget-object v2, p0, Lho$b;->d:Lho;

    invoke-static {v2}, Lho;->c(Lho;)I

    move-result v2

    const/4 v3, -0x1

    if-ne v2, v3, :cond_7

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, p0, Lho$b;->d:Lho;

    iget-object v2, v0, Lho;->i:Ljava/lang/String;

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "column NAME not found in file"

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    iput-object p1, v0, Lho;->i:Ljava/lang/String;

    return-object v1

    :cond_7
    iget-object v2, p0, Lho$b;->d:Lho;

    invoke-static {v2}, Lho;->e(Lho;)I

    move-result v2

    if-ne v2, v3, :cond_8

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, p0, Lho$b;->d:Lho;

    iget-object v2, v0, Lho;->i:Ljava/lang/String;

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "column ROLLNO not found in file"

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    iput-object p1, v0, Lho;->i:Ljava/lang/String;

    return-object v1

    :cond_8
    iget-object v2, p0, Lho$b;->d:Lho;

    invoke-static {v2}, Lho;->k(Lho;)I

    move-result v2

    if-ne v2, v3, :cond_9

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, p0, Lho$b;->d:Lho;

    iget-object v2, v0, Lho;->i:Ljava/lang/String;

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "column CLASS not found in file"

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    iput-object p1, v0, Lho;->i:Ljava/lang/String;

    return-object v1

    :cond_9
    :goto_2
    invoke-virtual {v0}, Lyd;->c()[Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_12

    array-length v4, v2

    const/4 v5, 0x1

    sub-int/2addr v4, v5

    iget-object v6, p0, Lho$b;->d:Lho;

    invoke-static {v6}, Lho;->c(Lho;)I

    move-result v6

    if-lt v4, v6, :cond_11

    iget-object v4, p0, Lho$b;->d:Lho;

    invoke-static {v4}, Lho;->c(Lho;)I

    move-result v4

    aget-object v4, v2, v4

    invoke-virtual {v4}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-ge v4, v5, :cond_a

    goto/16 :goto_8

    :cond_a
    iget-object v4, p0, Lho$b;->d:Lho;

    invoke-static {v4}, Lho;->c(Lho;)I

    move-result v4

    aget-object v8, v2, v4

    array-length v4, v2

    sub-int/2addr v4, v5

    iget-object v6, p0, Lho$b;->d:Lho;

    invoke-static {v6}, Lho;->e(Lho;)I

    move-result v6

    if-lt v4, v6, :cond_10

    iget-object v4, p0, Lho$b;->d:Lho;

    invoke-static {v4}, Lho;->e(Lho;)I

    move-result v4

    aget-object v4, v2, v4

    invoke-virtual {v4, p1}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_b

    goto/16 :goto_7

    :cond_b
    iget-object v4, p0, Lho$b;->d:Lho;

    invoke-static {v4}, Lho;->e(Lho;)I

    move-result v4

    aget-object v4, v2, v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    array-length v6, v2

    sub-int/2addr v6, v5

    iget-object v7, p0, Lho$b;->d:Lho;

    invoke-static {v7}, Lho;->k(Lho;)I

    move-result v7

    if-lt v6, v7, :cond_f

    iget-object v6, p0, Lho$b;->d:Lho;

    invoke-static {v6}, Lho;->k(Lho;)I

    move-result v6

    aget-object v6, v2, v6

    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    if-ge v6, v5, :cond_c

    goto/16 :goto_6

    :cond_c
    iget-object v6, p0, Lho$b;->d:Lho;

    invoke-static {v6}, Lho;->k(Lho;)I

    move-result v6

    aget-object v11, v2, v6

    iget-object v6, p0, Lho$b;->d:Lho;

    invoke-static {v6}, Lho;->g(Lho;)I

    move-result v6
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    const-string v7, ""

    if-eq v6, v3, :cond_d

    :try_start_1
    array-length v6, v2

    iget-object v9, p0, Lho$b;->d:Lho;

    invoke-static {v9}, Lho;->g(Lho;)I

    move-result v9

    if-le v6, v9, :cond_d

    iget-object v6, p0, Lho$b;->d:Lho;

    invoke-static {v6}, Lho;->g(Lho;)I

    move-result v6

    aget-object v6, v2, v6

    invoke-virtual {v6}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v6

    const/16 v9, 0x40

    invoke-virtual {v6, v9}, Ljava/lang/String;->indexOf(I)I

    move-result v6

    if-lez v6, :cond_d

    iget-object v6, p0, Lho$b;->d:Lho;

    invoke-static {v6}, Lho;->g(Lho;)I

    move-result v6

    aget-object v6, v2, v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    const/4 v9, 0x5

    if-le v6, v9, :cond_d

    iget-object v6, p0, Lho$b;->d:Lho;

    invoke-static {v6}, Lho;->g(Lho;)I

    move-result v6

    aget-object v6, v2, v6

    move-object v9, v6

    goto :goto_3

    :cond_d
    move-object v9, v7

    :goto_3
    iget-object v6, p0, Lho$b;->d:Lho;

    invoke-static {v6}, Lho;->i(Lho;)I

    move-result v6

    if-eq v6, v3, :cond_e

    array-length v6, v2

    iget-object v10, p0, Lho$b;->d:Lho;

    invoke-static {v10}, Lho;->i(Lho;)I

    move-result v10

    if-le v6, v10, :cond_e

    iget-object v6, p0, Lho$b;->d:Lho;

    invoke-static {v6}, Lho;->i(Lho;)I

    move-result v6

    aget-object v6, v2, v6

    invoke-virtual {v6, p1}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_e

    iget-object v6, p0, Lho$b;->d:Lho;

    invoke-static {v6}, Lho;->i(Lho;)I

    move-result v6

    aget-object v6, v2, v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    const/4 v10, 0x7

    if-le v6, v10, :cond_e

    iget-object v6, p0, Lho$b;->d:Lho;

    invoke-static {v6}, Lho;->i(Lho;)I

    move-result v6

    aget-object v2, v2, v6

    move-object v10, v2

    goto :goto_4

    :cond_e
    move-object v10, v7

    :goto_4
    new-instance v2, Lcom/ekodroid/omrevaluator/database/StudentDataModel;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    move-object v6, v2

    invoke-direct/range {v6 .. v11}, Lcom/ekodroid/omrevaluator/database/StudentDataModel;-><init>(Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v4, p0, Lho$b;->d:Lho;

    iget-object v4, v4, Lho;->l:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget v2, p0, Lho$b;->a:I

    :goto_5
    add-int/2addr v2, v5

    iput v2, p0, Lho$b;->a:I

    goto/16 :goto_2

    :cond_f
    :goto_6
    iget v2, p0, Lho$b;->b:I

    add-int/2addr v2, v5

    iput v2, p0, Lho$b;->b:I

    iget v2, p0, Lho$b;->a:I

    goto :goto_5

    :cond_10
    :goto_7
    iget v2, p0, Lho$b;->b:I

    add-int/2addr v2, v5

    iput v2, p0, Lho$b;->b:I

    iget v2, p0, Lho$b;->a:I

    goto :goto_5

    :cond_11
    :goto_8
    iget v2, p0, Lho$b;->b:I

    add-int/2addr v2, v5

    iput v2, p0, Lho$b;->b:I

    iget v2, p0, Lho$b;->a:I

    goto :goto_5

    :cond_12
    iget-object p1, p0, Lho$b;->d:Lho;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Valid rows : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lho$b;->a:I

    iget v3, p0, Lho$b;->b:I

    sub-int/2addr v2, v3

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, ", Invalid rows : "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lho$b;->b:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p1, Lho;->j:Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_9

    :catch_0
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V

    :goto_9
    return-object v1
.end method

.method public b(Ljava/lang/Void;)V
    .locals 1

    .line 1
    iget-object p1, p0, Lho$b;->c:Landroid/app/ProgressDialog;

    invoke-virtual {p1}, Landroid/app/Dialog;->isShowing()Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lho$b;->c:Landroid/app/ProgressDialog;

    invoke-virtual {p1}, Landroid/app/Dialog;->dismiss()V

    :cond_0
    iget-object p1, p0, Lho$b;->d:Lho;

    invoke-static {p1}, Lho;->m(Lho;)Ly01;

    move-result-object p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lho$b;->d:Lho;

    invoke-static {p1}, Lho;->m(Lho;)Ly01;

    move-result-object p1

    iget-object v0, p0, Lho$b;->d:Lho;

    iget-object v0, v0, Lho;->k:Lho;

    invoke-interface {p1, v0}, Ly01;->a(Ljava/lang/Object;)V

    :cond_1
    return-void
.end method

.method public bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 1
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lho$b;->a([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 1
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lho$b;->b(Ljava/lang/Void;)V

    return-void
.end method

.method public onPreExecute()V
    .locals 2

    .line 1
    new-instance v0, Landroid/app/ProgressDialog;

    iget-object v1, p0, Lho$b;->d:Lho;

    invoke-static {v1}, Lho;->a(Lho;)Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lho$b;->c:Landroid/app/ProgressDialog;

    const-string v1, "Importing Csv file, please wait..."

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lho$b;->c:Landroid/app/ProgressDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setCancelable(Z)V

    iget-object v0, p0, Lho$b;->c:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    return-void
.end method

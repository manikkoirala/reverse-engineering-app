.class public Lt92$i;
.super Lix;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lt92;-><init>(Landroidx/room/RoomDatabase;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic d:Lt92;


# direct methods
.method public constructor <init>(Lt92;Landroidx/room/RoomDatabase;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lt92$i;->d:Lt92;

    invoke-direct {p0, p2}, Lix;-><init>(Landroidx/room/RoomDatabase;)V

    return-void
.end method


# virtual methods
.method public e()Ljava/lang/String;
    .locals 1

    .line 1
    const-string v0, "INSERT OR IGNORE INTO `WorkSpec` (`id`,`state`,`worker_class_name`,`input_merger_class_name`,`input`,`output`,`initial_delay`,`interval_duration`,`flex_duration`,`run_attempt_count`,`backoff_policy`,`backoff_delay_duration`,`last_enqueue_time`,`minimum_retention_duration`,`schedule_requested_at`,`run_in_foreground`,`out_of_quota_policy`,`period_count`,`generation`,`next_schedule_time_override`,`next_schedule_time_override_generation`,`stop_reason`,`required_network_type`,`requires_charging`,`requires_device_idle`,`requires_battery_not_low`,`requires_storage_not_low`,`trigger_content_update_delay`,`trigger_max_content_delay`,`content_uri_triggers`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"

    return-object v0
.end method

.method public bridge synthetic i(Lws1;Ljava/lang/Object;)V
    .locals 0

    .line 1
    check-cast p2, Lp92;

    invoke-virtual {p0, p1, p2}, Lt92$i;->k(Lws1;Lp92;)V

    return-void
.end method

.method public k(Lws1;Lp92;)V
    .locals 10

    .line 1
    iget-object v0, p2, Lp92;->a:Ljava/lang/String;

    const/4 v1, 0x1

    if-nez v0, :cond_0

    invoke-interface {p1, v1}, Lus1;->I(I)V

    goto :goto_0

    :cond_0
    invoke-interface {p1, v1, v0}, Lus1;->y(ILjava/lang/String;)V

    :goto_0
    sget-object v0, Lz92;->a:Lz92;

    iget-object v0, p2, Lp92;->b:Landroidx/work/WorkInfo$State;

    invoke-static {v0}, Lz92;->j(Landroidx/work/WorkInfo$State;)I

    move-result v0

    const/4 v1, 0x2

    int-to-long v2, v0

    invoke-interface {p1, v1, v2, v3}, Lus1;->A(IJ)V

    iget-object v0, p2, Lp92;->c:Ljava/lang/String;

    const/4 v1, 0x3

    if-nez v0, :cond_1

    invoke-interface {p1, v1}, Lus1;->I(I)V

    goto :goto_1

    :cond_1
    invoke-interface {p1, v1, v0}, Lus1;->y(ILjava/lang/String;)V

    :goto_1
    iget-object v0, p2, Lp92;->d:Ljava/lang/String;

    const/4 v1, 0x4

    if-nez v0, :cond_2

    invoke-interface {p1, v1}, Lus1;->I(I)V

    goto :goto_2

    :cond_2
    invoke-interface {p1, v1, v0}, Lus1;->y(ILjava/lang/String;)V

    :goto_2
    iget-object v0, p2, Lp92;->e:Landroidx/work/b;

    invoke-static {v0}, Landroidx/work/b;->k(Landroidx/work/b;)[B

    move-result-object v0

    const/4 v1, 0x5

    if-nez v0, :cond_3

    invoke-interface {p1, v1}, Lus1;->I(I)V

    goto :goto_3

    :cond_3
    invoke-interface {p1, v1, v0}, Lus1;->D(I[B)V

    :goto_3
    iget-object v0, p2, Lp92;->f:Landroidx/work/b;

    invoke-static {v0}, Landroidx/work/b;->k(Landroidx/work/b;)[B

    move-result-object v0

    const/4 v1, 0x6

    if-nez v0, :cond_4

    invoke-interface {p1, v1}, Lus1;->I(I)V

    goto :goto_4

    :cond_4
    invoke-interface {p1, v1, v0}, Lus1;->D(I[B)V

    :goto_4
    const/4 v0, 0x7

    iget-wide v1, p2, Lp92;->g:J

    invoke-interface {p1, v0, v1, v2}, Lus1;->A(IJ)V

    const/16 v0, 0x8

    iget-wide v1, p2, Lp92;->h:J

    invoke-interface {p1, v0, v1, v2}, Lus1;->A(IJ)V

    const/16 v0, 0x9

    iget-wide v1, p2, Lp92;->i:J

    invoke-interface {p1, v0, v1, v2}, Lus1;->A(IJ)V

    iget v0, p2, Lp92;->k:I

    int-to-long v0, v0

    const/16 v2, 0xa

    invoke-interface {p1, v2, v0, v1}, Lus1;->A(IJ)V

    iget-object v0, p2, Lp92;->l:Landroidx/work/BackoffPolicy;

    invoke-static {v0}, Lz92;->a(Landroidx/work/BackoffPolicy;)I

    move-result v0

    const/16 v1, 0xb

    int-to-long v2, v0

    invoke-interface {p1, v1, v2, v3}, Lus1;->A(IJ)V

    const/16 v0, 0xc

    iget-wide v1, p2, Lp92;->m:J

    invoke-interface {p1, v0, v1, v2}, Lus1;->A(IJ)V

    const/16 v0, 0xd

    iget-wide v1, p2, Lp92;->n:J

    invoke-interface {p1, v0, v1, v2}, Lus1;->A(IJ)V

    const/16 v0, 0xe

    iget-wide v1, p2, Lp92;->o:J

    invoke-interface {p1, v0, v1, v2}, Lus1;->A(IJ)V

    const/16 v0, 0xf

    iget-wide v1, p2, Lp92;->p:J

    invoke-interface {p1, v0, v1, v2}, Lus1;->A(IJ)V

    iget-boolean v0, p2, Lp92;->q:Z

    const/16 v1, 0x10

    int-to-long v2, v0

    invoke-interface {p1, v1, v2, v3}, Lus1;->A(IJ)V

    iget-object v0, p2, Lp92;->r:Landroidx/work/OutOfQuotaPolicy;

    invoke-static {v0}, Lz92;->h(Landroidx/work/OutOfQuotaPolicy;)I

    move-result v0

    const/16 v1, 0x11

    int-to-long v2, v0

    invoke-interface {p1, v1, v2, v3}, Lus1;->A(IJ)V

    invoke-virtual {p2}, Lp92;->i()I

    move-result v0

    int-to-long v0, v0

    const/16 v2, 0x12

    invoke-interface {p1, v2, v0, v1}, Lus1;->A(IJ)V

    invoke-virtual {p2}, Lp92;->f()I

    move-result v0

    int-to-long v0, v0

    const/16 v2, 0x13

    invoke-interface {p1, v2, v0, v1}, Lus1;->A(IJ)V

    const/16 v0, 0x14

    invoke-virtual {p2}, Lp92;->g()J

    move-result-wide v1

    invoke-interface {p1, v0, v1, v2}, Lus1;->A(IJ)V

    invoke-virtual {p2}, Lp92;->h()I

    move-result v0

    int-to-long v0, v0

    const/16 v2, 0x15

    invoke-interface {p1, v2, v0, v1}, Lus1;->A(IJ)V

    invoke-virtual {p2}, Lp92;->j()I

    move-result v0

    int-to-long v0, v0

    const/16 v2, 0x16

    invoke-interface {p1, v2, v0, v1}, Lus1;->A(IJ)V

    iget-object p2, p2, Lp92;->j:Lzk;

    const/16 v0, 0x1d

    const/16 v1, 0x1c

    const/16 v2, 0x1b

    const/16 v3, 0x1a

    const/16 v4, 0x19

    const/16 v5, 0x18

    const/16 v6, 0x17

    const/16 v7, 0x1e

    if-eqz p2, :cond_6

    invoke-virtual {p2}, Lzk;->d()Landroidx/work/NetworkType;

    move-result-object v8

    invoke-static {v8}, Lz92;->g(Landroidx/work/NetworkType;)I

    move-result v8

    int-to-long v8, v8

    invoke-interface {p1, v6, v8, v9}, Lus1;->A(IJ)V

    invoke-virtual {p2}, Lzk;->g()Z

    move-result v6

    int-to-long v8, v6

    invoke-interface {p1, v5, v8, v9}, Lus1;->A(IJ)V

    invoke-virtual {p2}, Lzk;->h()Z

    move-result v5

    int-to-long v5, v5

    invoke-interface {p1, v4, v5, v6}, Lus1;->A(IJ)V

    invoke-virtual {p2}, Lzk;->f()Z

    move-result v4

    int-to-long v4, v4

    invoke-interface {p1, v3, v4, v5}, Lus1;->A(IJ)V

    invoke-virtual {p2}, Lzk;->i()Z

    move-result v3

    int-to-long v3, v3

    invoke-interface {p1, v2, v3, v4}, Lus1;->A(IJ)V

    invoke-virtual {p2}, Lzk;->b()J

    move-result-wide v2

    invoke-interface {p1, v1, v2, v3}, Lus1;->A(IJ)V

    invoke-virtual {p2}, Lzk;->a()J

    move-result-wide v1

    invoke-interface {p1, v0, v1, v2}, Lus1;->A(IJ)V

    invoke-virtual {p2}, Lzk;->c()Ljava/util/Set;

    move-result-object p2

    invoke-static {p2}, Lz92;->i(Ljava/util/Set;)[B

    move-result-object p2

    if-nez p2, :cond_5

    goto :goto_5

    :cond_5
    invoke-interface {p1, v7, p2}, Lus1;->D(I[B)V

    goto :goto_6

    :cond_6
    invoke-interface {p1, v6}, Lus1;->I(I)V

    invoke-interface {p1, v5}, Lus1;->I(I)V

    invoke-interface {p1, v4}, Lus1;->I(I)V

    invoke-interface {p1, v3}, Lus1;->I(I)V

    invoke-interface {p1, v2}, Lus1;->I(I)V

    invoke-interface {p1, v1}, Lus1;->I(I)V

    invoke-interface {p1, v0}, Lus1;->I(I)V

    :goto_5
    invoke-interface {p1, v7}, Lus1;->I(I)V

    :goto_6
    return-void
.end method

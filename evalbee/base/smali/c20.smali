.class public final Lc20;
.super Leo;
.source "SourceFile"


# instance fields
.field public final a:Lee0;

.field public b:Lzf0;

.field public c:Llk0;

.field public d:I

.field public e:Z


# direct methods
.method public constructor <init>(Ljr;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Leo;-><init>()V

    new-instance v0, Lz10;

    invoke-direct {v0, p0}, Lz10;-><init>(Lc20;)V

    iput-object v0, p0, Lc20;->a:Lee0;

    new-instance v0, La20;

    invoke-direct {v0, p0}, La20;-><init>(Lc20;)V

    invoke-interface {p1, v0}, Ljr;->a(Ljr$a;)V

    return-void
.end method

.method public static synthetic d(Lc20;ILcom/google/android/gms/tasks/Task;)Lcom/google/android/gms/tasks/Task;
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lc20;->h(ILcom/google/android/gms/tasks/Task;)Lcom/google/android/gms/tasks/Task;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic e(Lc20;Lr91;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lc20;->j(Lr91;)V

    return-void
.end method

.method public static synthetic f(Lc20;Lcg0;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lc20;->i(Lcg0;)V

    return-void
.end method

.method private synthetic h(ILcom/google/android/gms/tasks/Task;)Lcom/google/android/gms/tasks/Task;
    .locals 1

    .line 1
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lc20;->d:I

    if-eq p1, v0, :cond_0

    const-string p1, "FirebaseAuthCredentialsProvider"

    const-string p2, "getToken aborted due to token change"

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p1, p2, v0}, Lcom/google/firebase/firestore/util/Logger;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-virtual {p0}, Lc20;->a()Lcom/google/android/gms/tasks/Task;

    move-result-object p1

    monitor-exit p0

    return-object p1

    :cond_0
    invoke-virtual {p2}, Lcom/google/android/gms/tasks/Task;->isSuccessful()Z

    move-result p1

    if-eqz p1, :cond_1

    invoke-virtual {p2}, Lcom/google/android/gms/tasks/Task;->getResult()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lya0;

    invoke-virtual {p1}, Lya0;->c()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lcom/google/android/gms/tasks/Tasks;->forResult(Ljava/lang/Object;)Lcom/google/android/gms/tasks/Task;

    move-result-object p1

    monitor-exit p0

    return-object p1

    :cond_1
    invoke-virtual {p2}, Lcom/google/android/gms/tasks/Task;->getException()Ljava/lang/Exception;

    move-result-object p1

    invoke-static {p1}, Lcom/google/android/gms/tasks/Tasks;->forException(Ljava/lang/Exception;)Lcom/google/android/gms/tasks/Task;

    move-result-object p1

    monitor-exit p0

    return-object p1

    :catchall_0
    move-exception p1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method private synthetic i(Lcg0;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lc20;->k()V

    return-void
.end method

.method private synthetic j(Lr91;)V
    .locals 1

    .line 1
    monitor-enter p0

    :try_start_0
    invoke-interface {p1}, Lr91;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lzf0;

    iput-object p1, p0, Lc20;->b:Lzf0;

    invoke-virtual {p0}, Lc20;->k()V

    iget-object p1, p0, Lc20;->b:Lzf0;

    iget-object v0, p0, Lc20;->a:Lee0;

    invoke-interface {p1, v0}, Lzf0;->b(Lee0;)V

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method


# virtual methods
.method public declared-synchronized a()Lcom/google/android/gms/tasks/Task;
    .locals 4

    .line 1
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lc20;->b:Lzf0;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/firebase/FirebaseApiNotAvailableException;

    const-string v1, "auth is not available"

    invoke-direct {v0, v1}, Lcom/google/firebase/FirebaseApiNotAvailableException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/google/android/gms/tasks/Tasks;->forException(Ljava/lang/Exception;)Lcom/google/android/gms/tasks/Task;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :cond_0
    :try_start_1
    iget-boolean v1, p0, Lc20;->e:Z

    invoke-interface {v0, v1}, Lzf0;->c(Z)Lcom/google/android/gms/tasks/Task;

    move-result-object v0

    const/4 v1, 0x0

    iput-boolean v1, p0, Lc20;->e:Z

    iget v1, p0, Lc20;->d:I

    sget-object v2, Lwy;->b:Ljava/util/concurrent/Executor;

    new-instance v3, Lb20;

    invoke-direct {v3, p0, v1}, Lb20;-><init>(Lc20;I)V

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/tasks/Task;->continueWithTask(Ljava/util/concurrent/Executor;Lcom/google/android/gms/tasks/Continuation;)Lcom/google/android/gms/tasks/Task;

    move-result-object v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized b()V
    .locals 1

    .line 1
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lc20;->e:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized c(Llk0;)V
    .locals 1

    .line 1
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lc20;->c:Llk0;

    invoke-virtual {p0}, Lc20;->g()Lu12;

    move-result-object v0

    invoke-interface {p1, v0}, Llk0;->a(Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public final declared-synchronized g()Lu12;
    .locals 2

    .line 1
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lc20;->b:Lzf0;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    invoke-interface {v0}, Lzf0;->a()Ljava/lang/String;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_1

    new-instance v1, Lu12;

    invoke-direct {v1, v0}, Lu12;-><init>(Ljava/lang/String;)V

    goto :goto_1

    :cond_1
    sget-object v1, Lu12;->b:Lu12;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_1
    monitor-exit p0

    return-object v1

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized k()V
    .locals 2

    .line 1
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lc20;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lc20;->d:I

    iget-object v0, p0, Lc20;->c:Llk0;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lc20;->g()Lu12;

    move-result-object v1

    invoke-interface {v0, v1}, Llk0;->a(Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

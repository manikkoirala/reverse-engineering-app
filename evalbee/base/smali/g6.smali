.class public Lg6;
.super Lf6;
.source "SourceFile"

# interfaces
.implements Landroidx/appcompat/view/menu/e$a;
.implements Landroid/view/LayoutInflater$Factory2;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lg6$m;,
        Lg6$l;,
        Lg6$k;,
        Lg6$j;,
        Lg6$i;,
        Lg6$o;,
        Lg6$q;,
        Lg6$p;,
        Lg6$n;,
        Lg6$f;,
        Lg6$s;,
        Lg6$t;,
        Lg6$g;,
        Lg6$u;,
        Lg6$h;,
        Lg6$r;
    }
.end annotation


# static fields
.field public static final D1:Lco1;

.field public static final E1:Z

.field public static final F1:[I

.field public static final G1:Z

.field public static final H1:Z


# instance fields
.field public A:Landroid/widget/PopupWindow;

.field public A0:Z

.field public A1:Lvi0;

.field public B1:Landroid/window/OnBackInvokedDispatcher;

.field public C:Ljava/lang/Runnable;

.field public C0:Z

.field public C1:Landroid/window/OnBackInvokedCallback;

.field public D:Li42;

.field public F:Z

.field public G:Z

.field public H:Landroid/view/ViewGroup;

.field public I:Landroid/widget/TextView;

.field public J:Landroid/view/View;

.field public K:Z

.field public M:Z

.field public O:Z

.field public P:Z

.field public Q:Z

.field public U:Z

.field public V:Z

.field public W:Z

.field public Y:[Lg6$t;

.field public Z:Lg6$t;

.field public b1:Landroid/content/res/Configuration;

.field public c0:Z

.field public c1:I

.field public d1:I

.field public g1:I

.field public final j:Ljava/lang/Object;

.field public final k:Landroid/content/Context;

.field public k0:Z

.field public k1:Z

.field public l:Landroid/view/Window;

.field public m:Lg6$n;

.field public final n:Lz5;

.field public p:Lt1;

.field public p1:Lg6$p;

.field public q:Landroid/view/MenuInflater;

.field public s1:Lg6$p;

.field public t:Ljava/lang/CharSequence;

.field public t1:Z

.field public u1:I

.field public v:Lcq;

.field public final v1:Ljava/lang/Runnable;

.field public w:Lg6$g;

.field public w1:Z

.field public x:Lg6$u;

.field public x1:Landroid/graphics/Rect;

.field public y:Lc2;

.field public y1:Landroid/graphics/Rect;

.field public z:Landroidx/appcompat/widget/ActionBarContextView;

.field public z1:Lr7;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lco1;

    invoke-direct {v0}, Lco1;-><init>()V

    sput-object v0, Lg6;->D1:Lco1;

    const/4 v0, 0x0

    sput-boolean v0, Lg6;->E1:Z

    const v0, 0x1010054

    filled-new-array {v0}, [I

    move-result-object v0

    sput-object v0, Lg6;->F1:[I

    const-string v0, "robolectric"

    sget-object v1, Landroid/os/Build;->FINGERPRINT:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x1

    xor-int/2addr v0, v1

    sput-boolean v0, Lg6;->G1:Z

    sput-boolean v1, Lg6;->H1:Z

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;Lz5;)V
    .locals 1

    .line 1
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2, p1}, Lg6;-><init>(Landroid/content/Context;Landroid/view/Window;Lz5;Ljava/lang/Object;)V

    return-void
.end method

.method public constructor <init>(Landroid/app/Dialog;Lz5;)V
    .locals 2

    .line 2
    invoke-virtual {p1}, Landroid/app/Dialog;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p1}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-direct {p0, v0, v1, p2, p1}, Lg6;-><init>(Landroid/content/Context;Landroid/view/Window;Lz5;Ljava/lang/Object;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/view/Window;Lz5;Ljava/lang/Object;)V
    .locals 2

    .line 3
    invoke-direct {p0}, Lf6;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lg6;->D:Li42;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lg6;->F:Z

    const/16 v0, -0x64

    iput v0, p0, Lg6;->c1:I

    new-instance v1, Lg6$a;

    invoke-direct {v1, p0}, Lg6$a;-><init>(Lg6;)V

    iput-object v1, p0, Lg6;->v1:Ljava/lang/Runnable;

    iput-object p1, p0, Lg6;->k:Landroid/content/Context;

    iput-object p3, p0, Lg6;->n:Lz5;

    iput-object p4, p0, Lg6;->j:Ljava/lang/Object;

    iget p1, p0, Lg6;->c1:I

    if-ne p1, v0, :cond_0

    instance-of p1, p4, Landroid/app/Dialog;

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lg6;->W0()Lv5;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lv5;->m()Lf6;

    move-result-object p1

    invoke-virtual {p1}, Lf6;->l()I

    move-result p1

    iput p1, p0, Lg6;->c1:I

    :cond_0
    iget p1, p0, Lg6;->c1:I

    if-ne p1, v0, :cond_1

    sget-object p1, Lg6;->D1:Lco1;

    invoke-virtual {p4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p3

    invoke-virtual {p3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p1, p3}, Lco1;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Ljava/lang/Integer;

    if-eqz p3, :cond_1

    invoke-virtual {p3}, Ljava/lang/Integer;->intValue()I

    move-result p3

    iput p3, p0, Lg6;->c1:I

    invoke-virtual {p4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p3

    invoke-virtual {p3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p1, p3}, Lco1;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    if-eqz p2, :cond_2

    invoke-virtual {p0, p2}, Lg6;->R(Landroid/view/Window;)V

    :cond_2
    invoke-static {}, Ls6;->h()V

    return-void
.end method

.method public static j0(Landroid/content/res/Configuration;Landroid/content/res/Configuration;)Landroid/content/res/Configuration;
    .locals 5

    .line 1
    new-instance v0, Landroid/content/res/Configuration;

    invoke-direct {v0}, Landroid/content/res/Configuration;-><init>()V

    const/4 v1, 0x0

    iput v1, v0, Landroid/content/res/Configuration;->fontScale:F

    if-eqz p1, :cond_14

    invoke-virtual {p0, p1}, Landroid/content/res/Configuration;->diff(Landroid/content/res/Configuration;)I

    move-result v1

    if-nez v1, :cond_0

    goto/16 :goto_0

    :cond_0
    iget v1, p0, Landroid/content/res/Configuration;->fontScale:F

    iget v2, p1, Landroid/content/res/Configuration;->fontScale:F

    cmpl-float v1, v1, v2

    if-eqz v1, :cond_1

    iput v2, v0, Landroid/content/res/Configuration;->fontScale:F

    :cond_1
    iget v1, p0, Landroid/content/res/Configuration;->mcc:I

    iget v2, p1, Landroid/content/res/Configuration;->mcc:I

    if-eq v1, v2, :cond_2

    iput v2, v0, Landroid/content/res/Configuration;->mcc:I

    :cond_2
    iget v1, p0, Landroid/content/res/Configuration;->mnc:I

    iget v2, p1, Landroid/content/res/Configuration;->mnc:I

    if-eq v1, v2, :cond_3

    iput v2, v0, Landroid/content/res/Configuration;->mnc:I

    :cond_3
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-static {p0, p1, v0}, Lg6$k;->a(Landroid/content/res/Configuration;Landroid/content/res/Configuration;Landroid/content/res/Configuration;)V

    iget v2, p0, Landroid/content/res/Configuration;->touchscreen:I

    iget v3, p1, Landroid/content/res/Configuration;->touchscreen:I

    if-eq v2, v3, :cond_4

    iput v3, v0, Landroid/content/res/Configuration;->touchscreen:I

    :cond_4
    iget v2, p0, Landroid/content/res/Configuration;->keyboard:I

    iget v3, p1, Landroid/content/res/Configuration;->keyboard:I

    if-eq v2, v3, :cond_5

    iput v3, v0, Landroid/content/res/Configuration;->keyboard:I

    :cond_5
    iget v2, p0, Landroid/content/res/Configuration;->keyboardHidden:I

    iget v3, p1, Landroid/content/res/Configuration;->keyboardHidden:I

    if-eq v2, v3, :cond_6

    iput v3, v0, Landroid/content/res/Configuration;->keyboardHidden:I

    :cond_6
    iget v2, p0, Landroid/content/res/Configuration;->navigation:I

    iget v3, p1, Landroid/content/res/Configuration;->navigation:I

    if-eq v2, v3, :cond_7

    iput v3, v0, Landroid/content/res/Configuration;->navigation:I

    :cond_7
    iget v2, p0, Landroid/content/res/Configuration;->navigationHidden:I

    iget v3, p1, Landroid/content/res/Configuration;->navigationHidden:I

    if-eq v2, v3, :cond_8

    iput v3, v0, Landroid/content/res/Configuration;->navigationHidden:I

    :cond_8
    iget v2, p0, Landroid/content/res/Configuration;->orientation:I

    iget v3, p1, Landroid/content/res/Configuration;->orientation:I

    if-eq v2, v3, :cond_9

    iput v3, v0, Landroid/content/res/Configuration;->orientation:I

    :cond_9
    iget v2, p0, Landroid/content/res/Configuration;->screenLayout:I

    and-int/lit8 v2, v2, 0xf

    iget v3, p1, Landroid/content/res/Configuration;->screenLayout:I

    and-int/lit8 v4, v3, 0xf

    if-eq v2, v4, :cond_a

    iget v2, v0, Landroid/content/res/Configuration;->screenLayout:I

    and-int/lit8 v3, v3, 0xf

    or-int/2addr v2, v3

    iput v2, v0, Landroid/content/res/Configuration;->screenLayout:I

    :cond_a
    iget v2, p0, Landroid/content/res/Configuration;->screenLayout:I

    and-int/lit16 v2, v2, 0xc0

    iget v3, p1, Landroid/content/res/Configuration;->screenLayout:I

    and-int/lit16 v4, v3, 0xc0

    if-eq v2, v4, :cond_b

    iget v2, v0, Landroid/content/res/Configuration;->screenLayout:I

    and-int/lit16 v3, v3, 0xc0

    or-int/2addr v2, v3

    iput v2, v0, Landroid/content/res/Configuration;->screenLayout:I

    :cond_b
    iget v2, p0, Landroid/content/res/Configuration;->screenLayout:I

    and-int/lit8 v2, v2, 0x30

    iget v3, p1, Landroid/content/res/Configuration;->screenLayout:I

    and-int/lit8 v4, v3, 0x30

    if-eq v2, v4, :cond_c

    iget v2, v0, Landroid/content/res/Configuration;->screenLayout:I

    and-int/lit8 v3, v3, 0x30

    or-int/2addr v2, v3

    iput v2, v0, Landroid/content/res/Configuration;->screenLayout:I

    :cond_c
    iget v2, p0, Landroid/content/res/Configuration;->screenLayout:I

    and-int/lit16 v2, v2, 0x300

    iget v3, p1, Landroid/content/res/Configuration;->screenLayout:I

    and-int/lit16 v4, v3, 0x300

    if-eq v2, v4, :cond_d

    iget v2, v0, Landroid/content/res/Configuration;->screenLayout:I

    and-int/lit16 v3, v3, 0x300

    or-int/2addr v2, v3

    iput v2, v0, Landroid/content/res/Configuration;->screenLayout:I

    :cond_d
    const/16 v2, 0x1a

    if-lt v1, v2, :cond_e

    invoke-static {p0, p1, v0}, Lg6$l;->a(Landroid/content/res/Configuration;Landroid/content/res/Configuration;Landroid/content/res/Configuration;)V

    :cond_e
    iget v1, p0, Landroid/content/res/Configuration;->uiMode:I

    and-int/lit8 v1, v1, 0xf

    iget v2, p1, Landroid/content/res/Configuration;->uiMode:I

    and-int/lit8 v3, v2, 0xf

    if-eq v1, v3, :cond_f

    iget v1, v0, Landroid/content/res/Configuration;->uiMode:I

    and-int/lit8 v2, v2, 0xf

    or-int/2addr v1, v2

    iput v1, v0, Landroid/content/res/Configuration;->uiMode:I

    :cond_f
    iget v1, p0, Landroid/content/res/Configuration;->uiMode:I

    and-int/lit8 v1, v1, 0x30

    iget v2, p1, Landroid/content/res/Configuration;->uiMode:I

    and-int/lit8 v3, v2, 0x30

    if-eq v1, v3, :cond_10

    iget v1, v0, Landroid/content/res/Configuration;->uiMode:I

    and-int/lit8 v2, v2, 0x30

    or-int/2addr v1, v2

    iput v1, v0, Landroid/content/res/Configuration;->uiMode:I

    :cond_10
    iget v1, p0, Landroid/content/res/Configuration;->screenWidthDp:I

    iget v2, p1, Landroid/content/res/Configuration;->screenWidthDp:I

    if-eq v1, v2, :cond_11

    iput v2, v0, Landroid/content/res/Configuration;->screenWidthDp:I

    :cond_11
    iget v1, p0, Landroid/content/res/Configuration;->screenHeightDp:I

    iget v2, p1, Landroid/content/res/Configuration;->screenHeightDp:I

    if-eq v1, v2, :cond_12

    iput v2, v0, Landroid/content/res/Configuration;->screenHeightDp:I

    :cond_12
    iget v1, p0, Landroid/content/res/Configuration;->smallestScreenWidthDp:I

    iget v2, p1, Landroid/content/res/Configuration;->smallestScreenWidthDp:I

    if-eq v1, v2, :cond_13

    iput v2, v0, Landroid/content/res/Configuration;->smallestScreenWidthDp:I

    :cond_13
    invoke-static {p0, p1, v0}, Lg6$i;->b(Landroid/content/res/Configuration;Landroid/content/res/Configuration;Landroid/content/res/Configuration;)V

    :cond_14
    :goto_0
    return-object v0
.end method


# virtual methods
.method public A()V
    .locals 2

    .line 1
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lg6;->O(ZZ)Z

    return-void
.end method

.method public A0(ILandroid/view/KeyEvent;)Z
    .locals 3

    .line 1
    const/4 v0, 0x4

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eq p1, v0, :cond_1

    const/16 v0, 0x52

    if-eq p1, v0, :cond_0

    goto :goto_1

    :cond_0
    invoke-virtual {p0, v2, p2}, Lg6;->B0(ILandroid/view/KeyEvent;)Z

    return v1

    :cond_1
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getFlags()I

    move-result p1

    and-int/lit16 p1, p1, 0x80

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    move v1, v2

    :goto_0
    iput-boolean v1, p0, Lg6;->c0:Z

    :goto_1
    return v2
.end method

.method public B()V
    .locals 2

    .line 1
    invoke-virtual {p0}, Lg6;->p()Lt1;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lt1;->r(Z)V

    :cond_0
    return-void
.end method

.method public final B0(ILandroid/view/KeyEvent;)Z
    .locals 1

    .line 1
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lg6;->p0(IZ)Lg6$t;

    move-result-object p1

    iget-boolean v0, p1, Lg6$t;->o:Z

    if-nez v0, :cond_0

    invoke-virtual {p0, p1, p2}, Lg6;->L0(Lg6$t;Landroid/view/KeyEvent;)Z

    move-result p1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method public C0(ILandroid/view/KeyEvent;)Z
    .locals 3

    .line 1
    invoke-virtual {p0}, Lg6;->p()Lt1;

    move-result-object v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1, p2}, Lt1;->n(ILandroid/view/KeyEvent;)Z

    move-result p1

    if-eqz p1, :cond_0

    return v1

    :cond_0
    iget-object p1, p0, Lg6;->Z:Lg6$t;

    if-eqz p1, :cond_2

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    invoke-virtual {p0, p1, v0, p2, v1}, Lg6;->K0(Lg6$t;ILandroid/view/KeyEvent;I)Z

    move-result p1

    if-eqz p1, :cond_2

    iget-object p1, p0, Lg6;->Z:Lg6$t;

    if-eqz p1, :cond_1

    iput-boolean v1, p1, Lg6$t;->n:Z

    :cond_1
    return v1

    :cond_2
    iget-object p1, p0, Lg6;->Z:Lg6$t;

    const/4 v0, 0x0

    if-nez p1, :cond_3

    invoke-virtual {p0, v0, v1}, Lg6;->p0(IZ)Lg6$t;

    move-result-object p1

    invoke-virtual {p0, p1, p2}, Lg6;->L0(Lg6$t;Landroid/view/KeyEvent;)Z

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v2

    invoke-virtual {p0, p1, v2, p2, v1}, Lg6;->K0(Lg6$t;ILandroid/view/KeyEvent;I)Z

    move-result p2

    iput-boolean v0, p1, Lg6$t;->m:Z

    if-eqz p2, :cond_3

    return v1

    :cond_3
    return v0
.end method

.method public D0(ILandroid/view/KeyEvent;)Z
    .locals 3

    .line 1
    const/4 v0, 0x4

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eq p1, v0, :cond_1

    const/16 v0, 0x52

    if-eq p1, v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {p0, v2, p2}, Lg6;->E0(ILandroid/view/KeyEvent;)Z

    return v1

    :cond_1
    invoke-virtual {p0}, Lg6;->z0()Z

    move-result p1

    if-eqz p1, :cond_2

    return v1

    :cond_2
    :goto_0
    return v2
.end method

.method public E(I)Z
    .locals 4

    .line 1
    invoke-virtual {p0, p1}, Lg6;->N0(I)I

    move-result p1

    iget-boolean v0, p0, Lg6;->V:Z

    const/4 v1, 0x0

    const/16 v2, 0x6c

    if-eqz v0, :cond_0

    if-ne p1, v2, :cond_0

    return v1

    :cond_0
    iget-boolean v0, p0, Lg6;->O:Z

    const/4 v3, 0x1

    if-eqz v0, :cond_1

    if-ne p1, v3, :cond_1

    iput-boolean v1, p0, Lg6;->O:Z

    :cond_1
    if-eq p1, v3, :cond_7

    const/4 v0, 0x2

    if-eq p1, v0, :cond_6

    const/4 v0, 0x5

    if-eq p1, v0, :cond_5

    const/16 v0, 0xa

    if-eq p1, v0, :cond_4

    if-eq p1, v2, :cond_3

    const/16 v0, 0x6d

    if-eq p1, v0, :cond_2

    iget-object v0, p0, Lg6;->l:Landroid/view/Window;

    invoke-virtual {v0, p1}, Landroid/view/Window;->requestFeature(I)Z

    move-result p1

    return p1

    :cond_2
    invoke-virtual {p0}, Lg6;->V0()V

    iput-boolean v3, p0, Lg6;->P:Z

    return v3

    :cond_3
    invoke-virtual {p0}, Lg6;->V0()V

    iput-boolean v3, p0, Lg6;->O:Z

    return v3

    :cond_4
    invoke-virtual {p0}, Lg6;->V0()V

    iput-boolean v3, p0, Lg6;->Q:Z

    return v3

    :cond_5
    invoke-virtual {p0}, Lg6;->V0()V

    iput-boolean v3, p0, Lg6;->M:Z

    return v3

    :cond_6
    invoke-virtual {p0}, Lg6;->V0()V

    iput-boolean v3, p0, Lg6;->K:Z

    return v3

    :cond_7
    invoke-virtual {p0}, Lg6;->V0()V

    iput-boolean v3, p0, Lg6;->V:Z

    return v3
.end method

.method public final E0(ILandroid/view/KeyEvent;)Z
    .locals 4

    .line 1
    iget-object v0, p0, Lg6;->y:Lc2;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return v1

    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lg6;->p0(IZ)Lg6$t;

    move-result-object v2

    if-nez p1, :cond_2

    iget-object p1, p0, Lg6;->v:Lcq;

    if-eqz p1, :cond_2

    invoke-interface {p1}, Lcq;->a()Z

    move-result p1

    if-eqz p1, :cond_2

    iget-object p1, p0, Lg6;->k:Landroid/content/Context;

    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object p1

    invoke-virtual {p1}, Landroid/view/ViewConfiguration;->hasPermanentMenuKey()Z

    move-result p1

    if-nez p1, :cond_2

    iget-object p1, p0, Lg6;->v:Lcq;

    invoke-interface {p1}, Lcq;->c()Z

    move-result p1

    if-nez p1, :cond_1

    iget-boolean p1, p0, Lg6;->C0:Z

    if-nez p1, :cond_5

    invoke-virtual {p0, v2, p2}, Lg6;->L0(Lg6$t;Landroid/view/KeyEvent;)Z

    move-result p1

    if-eqz p1, :cond_5

    iget-object p1, p0, Lg6;->v:Lcq;

    invoke-interface {p1}, Lcq;->b()Z

    move-result v0

    goto :goto_2

    :cond_1
    iget-object p1, p0, Lg6;->v:Lcq;

    invoke-interface {p1}, Lcq;->d()Z

    move-result v0

    goto :goto_2

    :cond_2
    iget-boolean p1, v2, Lg6$t;->o:Z

    if-nez p1, :cond_6

    iget-boolean v3, v2, Lg6$t;->n:Z

    if-eqz v3, :cond_3

    goto :goto_1

    :cond_3
    iget-boolean p1, v2, Lg6$t;->m:Z

    if-eqz p1, :cond_5

    iget-boolean p1, v2, Lg6$t;->r:Z

    if-eqz p1, :cond_4

    iput-boolean v1, v2, Lg6$t;->m:Z

    invoke-virtual {p0, v2, p2}, Lg6;->L0(Lg6$t;Landroid/view/KeyEvent;)Z

    move-result p1

    goto :goto_0

    :cond_4
    move p1, v0

    :goto_0
    if-eqz p1, :cond_5

    invoke-virtual {p0, v2, p2}, Lg6;->I0(Lg6$t;Landroid/view/KeyEvent;)V

    goto :goto_2

    :cond_5
    move v0, v1

    goto :goto_2

    :cond_6
    :goto_1
    invoke-virtual {p0, v2, v0}, Lg6;->Y(Lg6$t;Z)V

    move v0, p1

    :goto_2
    if-eqz v0, :cond_8

    iget-object p1, p0, Lg6;->k:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    const-string p2, "audio"

    invoke-virtual {p1, p2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/media/AudioManager;

    if-eqz p1, :cond_7

    invoke-virtual {p1, v1}, Landroid/media/AudioManager;->playSoundEffect(I)V

    goto :goto_3

    :cond_7
    const-string p1, "AppCompatDelegate"

    const-string p2, "Couldn\'t get audio manager"

    invoke-static {p1, p2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_8
    :goto_3
    return v0
.end method

.method public F(I)V
    .locals 2

    .line 1
    invoke-virtual {p0}, Lg6;->g0()V

    iget-object v0, p0, Lg6;->H:Landroid/view/ViewGroup;

    const v1, 0x1020002

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    iget-object v1, p0, Lg6;->k:Landroid/content/Context;

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    invoke-virtual {v1, p1, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    iget-object p1, p0, Lg6;->m:Lg6$n;

    iget-object v0, p0, Lg6;->l:Landroid/view/Window;

    invoke-virtual {v0}, Landroid/view/Window;->getCallback()Landroid/view/Window$Callback;

    move-result-object v0

    invoke-virtual {p1, v0}, Lg6$n;->c(Landroid/view/Window$Callback;)V

    return-void
.end method

.method public F0(I)V
    .locals 1

    .line 1
    const/16 v0, 0x6c

    if-ne p1, v0, :cond_0

    invoke-virtual {p0}, Lg6;->p()Lt1;

    move-result-object p1

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lt1;->h(Z)V

    :cond_0
    return-void
.end method

.method public G(Landroid/view/View;)V
    .locals 2

    .line 1
    invoke-virtual {p0}, Lg6;->g0()V

    iget-object v0, p0, Lg6;->H:Landroid/view/ViewGroup;

    const v1, 0x1020002

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    iget-object p1, p0, Lg6;->m:Lg6$n;

    iget-object v0, p0, Lg6;->l:Landroid/view/Window;

    invoke-virtual {v0}, Landroid/view/Window;->getCallback()Landroid/view/Window$Callback;

    move-result-object v0

    invoke-virtual {p1, v0}, Lg6$n;->c(Landroid/view/Window$Callback;)V

    return-void
.end method

.method public G0(I)V
    .locals 2

    .line 1
    const/16 v0, 0x6c

    const/4 v1, 0x0

    if-ne p1, v0, :cond_0

    invoke-virtual {p0}, Lg6;->p()Lt1;

    move-result-object p1

    if-eqz p1, :cond_1

    invoke-virtual {p1, v1}, Lt1;->h(Z)V

    goto :goto_0

    :cond_0
    if-nez p1, :cond_1

    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lg6;->p0(IZ)Lg6$t;

    move-result-object p1

    iget-boolean v0, p1, Lg6$t;->o:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0, p1, v1}, Lg6;->Y(Lg6$t;Z)V

    :cond_1
    :goto_0
    return-void
.end method

.method public H(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .locals 2

    .line 1
    invoke-virtual {p0}, Lg6;->g0()V

    iget-object v0, p0, Lg6;->H:Landroid/view/ViewGroup;

    const v1, 0x1020002

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    invoke-virtual {v0, p1, p2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    iget-object p1, p0, Lg6;->m:Lg6$n;

    iget-object p2, p0, Lg6;->l:Landroid/view/Window;

    invoke-virtual {p2}, Landroid/view/Window;->getCallback()Landroid/view/Window$Callback;

    move-result-object p2

    invoke-virtual {p1, p2}, Lg6$n;->c(Landroid/view/Window$Callback;)V

    return-void
.end method

.method public H0(Landroid/view/ViewGroup;)V
    .locals 0

    .line 1
    return-void
.end method

.method public I(Landroid/window/OnBackInvokedDispatcher;)V
    .locals 2

    .line 1
    invoke-super {p0, p1}, Lf6;->I(Landroid/window/OnBackInvokedDispatcher;)V

    iget-object v0, p0, Lg6;->B1:Landroid/window/OnBackInvokedDispatcher;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lg6;->C1:Landroid/window/OnBackInvokedCallback;

    if-eqz v1, :cond_0

    invoke-static {v0, v1}, Lg6$m;->c(Ljava/lang/Object;Ljava/lang/Object;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lg6;->C1:Landroid/window/OnBackInvokedCallback;

    :cond_0
    if-nez p1, :cond_1

    iget-object v0, p0, Lg6;->j:Ljava/lang/Object;

    instance-of v1, v0, Landroid/app/Activity;

    if-eqz v1, :cond_1

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object p1, p0, Lg6;->j:Ljava/lang/Object;

    check-cast p1, Landroid/app/Activity;

    invoke-static {p1}, Lg6$m;->a(Landroid/app/Activity;)Landroid/window/OnBackInvokedDispatcher;

    move-result-object p1

    :cond_1
    iput-object p1, p0, Lg6;->B1:Landroid/window/OnBackInvokedDispatcher;

    invoke-virtual {p0}, Lg6;->Z0()V

    return-void
.end method

.method public final I0(Lg6$t;Landroid/view/KeyEvent;)V
    .locals 12

    .line 1
    iget-boolean v0, p1, Lg6$t;->o:Z

    if-nez v0, :cond_11

    iget-boolean v0, p0, Lg6;->C0:Z

    if-eqz v0, :cond_0

    goto/16 :goto_4

    :cond_0
    iget v0, p1, Lg6$t;->a:I

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-nez v0, :cond_2

    iget-object v0, p0, Lg6;->k:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->screenLayout:I

    and-int/lit8 v0, v0, 0xf

    const/4 v3, 0x4

    if-ne v0, v3, :cond_1

    move v0, v2

    goto :goto_0

    :cond_1
    move v0, v1

    :goto_0
    if-eqz v0, :cond_2

    return-void

    :cond_2
    invoke-virtual {p0}, Lg6;->r0()Landroid/view/Window$Callback;

    move-result-object v0

    if-eqz v0, :cond_3

    iget v3, p1, Lg6$t;->a:I

    iget-object v4, p1, Lg6$t;->j:Landroidx/appcompat/view/menu/e;

    invoke-interface {v0, v3, v4}, Landroid/view/Window$Callback;->onMenuOpened(ILandroid/view/Menu;)Z

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {p0, p1, v2}, Lg6;->Y(Lg6$t;Z)V

    return-void

    :cond_3
    iget-object v0, p0, Lg6;->k:Landroid/content/Context;

    const-string v3, "window"

    invoke-virtual {v0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    if-nez v0, :cond_4

    return-void

    :cond_4
    invoke-virtual {p0, p1, p2}, Lg6;->L0(Lg6$t;Landroid/view/KeyEvent;)Z

    move-result p2

    if-nez p2, :cond_5

    return-void

    :cond_5
    iget-object p2, p1, Lg6$t;->g:Landroid/view/ViewGroup;

    const/4 v3, -0x2

    if-eqz p2, :cond_7

    iget-boolean v4, p1, Lg6$t;->q:Z

    if-eqz v4, :cond_6

    goto :goto_1

    :cond_6
    iget-object p2, p1, Lg6$t;->i:Landroid/view/View;

    if-eqz p2, :cond_e

    invoke-virtual {p2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p2

    if-eqz p2, :cond_e

    iget p2, p2, Landroid/view/ViewGroup$LayoutParams;->width:I

    const/4 v4, -0x1

    if-ne p2, v4, :cond_e

    move v5, v4

    goto :goto_2

    :cond_7
    :goto_1
    if-nez p2, :cond_9

    invoke-virtual {p0, p1}, Lg6;->u0(Lg6$t;)Z

    move-result p2

    if-eqz p2, :cond_8

    iget-object p2, p1, Lg6$t;->g:Landroid/view/ViewGroup;

    if-nez p2, :cond_a

    :cond_8
    return-void

    :cond_9
    iget-boolean v4, p1, Lg6$t;->q:Z

    if-eqz v4, :cond_a

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getChildCount()I

    move-result p2

    if-lez p2, :cond_a

    iget-object p2, p1, Lg6$t;->g:Landroid/view/ViewGroup;

    invoke-virtual {p2}, Landroid/view/ViewGroup;->removeAllViews()V

    :cond_a
    invoke-virtual {p0, p1}, Lg6;->t0(Lg6$t;)Z

    move-result p2

    if-eqz p2, :cond_10

    invoke-virtual {p1}, Lg6$t;->b()Z

    move-result p2

    if-nez p2, :cond_b

    goto :goto_3

    :cond_b
    iget-object p2, p1, Lg6$t;->h:Landroid/view/View;

    invoke-virtual {p2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p2

    if-nez p2, :cond_c

    new-instance p2, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {p2, v3, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    :cond_c
    iget v4, p1, Lg6$t;->b:I

    iget-object v5, p1, Lg6$t;->g:Landroid/view/ViewGroup;

    invoke-virtual {v5, v4}, Landroid/view/View;->setBackgroundResource(I)V

    iget-object v4, p1, Lg6$t;->h:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v4

    instance-of v5, v4, Landroid/view/ViewGroup;

    if-eqz v5, :cond_d

    check-cast v4, Landroid/view/ViewGroup;

    iget-object v5, p1, Lg6$t;->h:Landroid/view/View;

    invoke-virtual {v4, v5}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    :cond_d
    iget-object v4, p1, Lg6$t;->g:Landroid/view/ViewGroup;

    iget-object v5, p1, Lg6$t;->h:Landroid/view/View;

    invoke-virtual {v4, v5, p2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    iget-object p2, p1, Lg6$t;->h:Landroid/view/View;

    invoke-virtual {p2}, Landroid/view/View;->hasFocus()Z

    move-result p2

    if-nez p2, :cond_e

    iget-object p2, p1, Lg6$t;->h:Landroid/view/View;

    invoke-virtual {p2}, Landroid/view/View;->requestFocus()Z

    :cond_e
    move v5, v3

    :goto_2
    iput-boolean v1, p1, Lg6$t;->n:Z

    new-instance p2, Landroid/view/WindowManager$LayoutParams;

    const/4 v6, -0x2

    iget v7, p1, Lg6$t;->d:I

    iget v8, p1, Lg6$t;->e:I

    const/16 v9, 0x3ea

    const/high16 v10, 0x820000

    const/4 v11, -0x3

    move-object v4, p2

    invoke-direct/range {v4 .. v11}, Landroid/view/WindowManager$LayoutParams;-><init>(IIIIIII)V

    iget v1, p1, Lg6$t;->c:I

    iput v1, p2, Landroid/view/WindowManager$LayoutParams;->gravity:I

    iget v1, p1, Lg6$t;->f:I

    iput v1, p2, Landroid/view/WindowManager$LayoutParams;->windowAnimations:I

    iget-object v1, p1, Lg6$t;->g:Landroid/view/ViewGroup;

    invoke-interface {v0, v1, p2}, Landroid/view/ViewManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    iput-boolean v2, p1, Lg6$t;->o:Z

    iget p1, p1, Lg6$t;->a:I

    if-nez p1, :cond_f

    invoke-virtual {p0}, Lg6;->Z0()V

    :cond_f
    return-void

    :cond_10
    :goto_3
    iput-boolean v2, p1, Lg6$t;->q:Z

    :cond_11
    :goto_4
    return-void
.end method

.method public J(Landroidx/appcompat/widget/Toolbar;)V
    .locals 3

    .line 1
    iget-object v0, p0, Lg6;->j:Ljava/lang/Object;

    instance-of v0, v0, Landroid/app/Activity;

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0}, Lg6;->p()Lt1;

    move-result-object v0

    instance-of v1, v0, Le62;

    if-nez v1, :cond_3

    const/4 v1, 0x0

    iput-object v1, p0, Lg6;->q:Landroid/view/MenuInflater;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lt1;->m()V

    :cond_1
    iput-object v1, p0, Lg6;->p:Lt1;

    if-eqz p1, :cond_2

    new-instance v0, Lbx1;

    invoke-virtual {p0}, Lg6;->q0()Ljava/lang/CharSequence;

    move-result-object v1

    iget-object v2, p0, Lg6;->m:Lg6$n;

    invoke-direct {v0, p1, v1, v2}, Lbx1;-><init>(Landroidx/appcompat/widget/Toolbar;Ljava/lang/CharSequence;Landroid/view/Window$Callback;)V

    iput-object v0, p0, Lg6;->p:Lt1;

    iget-object v1, p0, Lg6;->m:Lg6$n;

    iget-object v0, v0, Lbx1;->c:Lg6$f;

    invoke-virtual {v1, v0}, Lg6$n;->e(Lg6$f;)V

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroidx/appcompat/widget/Toolbar;->setBackInvokedCallbackEnabled(Z)V

    goto :goto_0

    :cond_2
    iget-object p1, p0, Lg6;->m:Lg6$n;

    invoke-virtual {p1, v1}, Lg6$n;->e(Lg6$f;)V

    :goto_0
    invoke-virtual {p0}, Lg6;->r()V

    return-void

    :cond_3
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "This Activity already has an action bar supplied by the window decor. Do not request Window.FEATURE_SUPPORT_ACTION_BAR and set windowActionBar to false in your theme to use a Toolbar instead."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public final J0()Lt1;
    .locals 1

    .line 1
    iget-object v0, p0, Lg6;->p:Lt1;

    return-object v0
.end method

.method public K(I)V
    .locals 0

    .line 1
    iput p1, p0, Lg6;->d1:I

    return-void
.end method

.method public final K0(Lg6$t;ILandroid/view/KeyEvent;I)Z
    .locals 2

    .line 1
    invoke-virtual {p3}, Landroid/view/KeyEvent;->isSystem()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return v1

    :cond_0
    iget-boolean v0, p1, Lg6$t;->m:Z

    if-nez v0, :cond_1

    invoke-virtual {p0, p1, p3}, Lg6;->L0(Lg6$t;Landroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    iget-object v0, p1, Lg6$t;->j:Landroidx/appcompat/view/menu/e;

    if-eqz v0, :cond_2

    invoke-virtual {v0, p2, p3, p4}, Landroidx/appcompat/view/menu/e;->performShortcut(ILandroid/view/KeyEvent;I)Z

    move-result v1

    :cond_2
    if-eqz v1, :cond_3

    const/4 p2, 0x1

    and-int/lit8 p3, p4, 0x1

    if-nez p3, :cond_3

    iget-object p3, p0, Lg6;->v:Lcq;

    if-nez p3, :cond_3

    invoke-virtual {p0, p1, p2}, Lg6;->Y(Lg6$t;Z)V

    :cond_3
    return v1
.end method

.method public final L(Ljava/lang/CharSequence;)V
    .locals 1

    .line 1
    iput-object p1, p0, Lg6;->t:Ljava/lang/CharSequence;

    iget-object v0, p0, Lg6;->v:Lcq;

    if-eqz v0, :cond_0

    invoke-interface {v0, p1}, Lcq;->setWindowTitle(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lg6;->J0()Lt1;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lg6;->J0()Lt1;

    move-result-object v0

    invoke-virtual {v0, p1}, Lt1;->u(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lg6;->I:Landroid/widget/TextView;

    if-eqz v0, :cond_2

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_2
    :goto_0
    return-void
.end method

.method public final L0(Lg6$t;Landroid/view/KeyEvent;)Z
    .locals 8

    .line 1
    iget-boolean v0, p0, Lg6;->C0:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    return v1

    :cond_0
    iget-boolean v0, p1, Lg6$t;->m:Z

    const/4 v2, 0x1

    if-eqz v0, :cond_1

    return v2

    :cond_1
    iget-object v0, p0, Lg6;->Z:Lg6$t;

    if-eqz v0, :cond_2

    if-eq v0, p1, :cond_2

    invoke-virtual {p0, v0, v1}, Lg6;->Y(Lg6$t;Z)V

    :cond_2
    invoke-virtual {p0}, Lg6;->r0()Landroid/view/Window$Callback;

    move-result-object v0

    if-eqz v0, :cond_3

    iget v3, p1, Lg6$t;->a:I

    invoke-interface {v0, v3}, Landroid/view/Window$Callback;->onCreatePanelView(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, p1, Lg6$t;->i:Landroid/view/View;

    :cond_3
    iget v3, p1, Lg6$t;->a:I

    if-eqz v3, :cond_5

    const/16 v4, 0x6c

    if-ne v3, v4, :cond_4

    goto :goto_0

    :cond_4
    move v3, v1

    goto :goto_1

    :cond_5
    :goto_0
    move v3, v2

    :goto_1
    if-eqz v3, :cond_6

    iget-object v4, p0, Lg6;->v:Lcq;

    if-eqz v4, :cond_6

    invoke-interface {v4}, Lcq;->f()V

    :cond_6
    iget-object v4, p1, Lg6$t;->i:Landroid/view/View;

    if-nez v4, :cond_15

    if-eqz v3, :cond_7

    invoke-virtual {p0}, Lg6;->J0()Lt1;

    move-result-object v4

    instance-of v4, v4, Lbx1;

    if-nez v4, :cond_15

    :cond_7
    iget-object v4, p1, Lg6$t;->j:Landroidx/appcompat/view/menu/e;

    const/4 v5, 0x0

    if-eqz v4, :cond_8

    iget-boolean v6, p1, Lg6$t;->r:Z

    if-eqz v6, :cond_f

    :cond_8
    if-nez v4, :cond_a

    invoke-virtual {p0, p1}, Lg6;->v0(Lg6$t;)Z

    move-result v4

    if-eqz v4, :cond_9

    iget-object v4, p1, Lg6$t;->j:Landroidx/appcompat/view/menu/e;

    if-nez v4, :cond_a

    :cond_9
    return v1

    :cond_a
    if-eqz v3, :cond_c

    iget-object v4, p0, Lg6;->v:Lcq;

    if-eqz v4, :cond_c

    iget-object v4, p0, Lg6;->w:Lg6$g;

    if-nez v4, :cond_b

    new-instance v4, Lg6$g;

    invoke-direct {v4, p0}, Lg6$g;-><init>(Lg6;)V

    iput-object v4, p0, Lg6;->w:Lg6$g;

    :cond_b
    iget-object v4, p0, Lg6;->v:Lcq;

    iget-object v6, p1, Lg6$t;->j:Landroidx/appcompat/view/menu/e;

    iget-object v7, p0, Lg6;->w:Lg6$g;

    invoke-interface {v4, v6, v7}, Lcq;->e(Landroid/view/Menu;Landroidx/appcompat/view/menu/i$a;)V

    :cond_c
    iget-object v4, p1, Lg6$t;->j:Landroidx/appcompat/view/menu/e;

    invoke-virtual {v4}, Landroidx/appcompat/view/menu/e;->stopDispatchingItemsChanged()V

    iget v4, p1, Lg6$t;->a:I

    iget-object v6, p1, Lg6$t;->j:Landroidx/appcompat/view/menu/e;

    invoke-interface {v0, v4, v6}, Landroid/view/Window$Callback;->onCreatePanelMenu(ILandroid/view/Menu;)Z

    move-result v4

    if-nez v4, :cond_e

    invoke-virtual {p1, v5}, Lg6$t;->c(Landroidx/appcompat/view/menu/e;)V

    if-eqz v3, :cond_d

    iget-object p1, p0, Lg6;->v:Lcq;

    if-eqz p1, :cond_d

    iget-object p2, p0, Lg6;->w:Lg6$g;

    invoke-interface {p1, v5, p2}, Lcq;->e(Landroid/view/Menu;Landroidx/appcompat/view/menu/i$a;)V

    :cond_d
    return v1

    :cond_e
    iput-boolean v1, p1, Lg6$t;->r:Z

    :cond_f
    iget-object v4, p1, Lg6$t;->j:Landroidx/appcompat/view/menu/e;

    invoke-virtual {v4}, Landroidx/appcompat/view/menu/e;->stopDispatchingItemsChanged()V

    iget-object v4, p1, Lg6$t;->s:Landroid/os/Bundle;

    if-eqz v4, :cond_10

    iget-object v6, p1, Lg6$t;->j:Landroidx/appcompat/view/menu/e;

    invoke-virtual {v6, v4}, Landroidx/appcompat/view/menu/e;->restoreActionViewStates(Landroid/os/Bundle;)V

    iput-object v5, p1, Lg6$t;->s:Landroid/os/Bundle;

    :cond_10
    iget-object v4, p1, Lg6$t;->i:Landroid/view/View;

    iget-object v6, p1, Lg6$t;->j:Landroidx/appcompat/view/menu/e;

    invoke-interface {v0, v1, v4, v6}, Landroid/view/Window$Callback;->onPreparePanel(ILandroid/view/View;Landroid/view/Menu;)Z

    move-result v0

    if-nez v0, :cond_12

    if-eqz v3, :cond_11

    iget-object p2, p0, Lg6;->v:Lcq;

    if-eqz p2, :cond_11

    iget-object v0, p0, Lg6;->w:Lg6$g;

    invoke-interface {p2, v5, v0}, Lcq;->e(Landroid/view/Menu;Landroidx/appcompat/view/menu/i$a;)V

    :cond_11
    iget-object p1, p1, Lg6$t;->j:Landroidx/appcompat/view/menu/e;

    invoke-virtual {p1}, Landroidx/appcompat/view/menu/e;->startDispatchingItemsChanged()V

    return v1

    :cond_12
    if-eqz p2, :cond_13

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getDeviceId()I

    move-result p2

    goto :goto_2

    :cond_13
    const/4 p2, -0x1

    :goto_2
    invoke-static {p2}, Landroid/view/KeyCharacterMap;->load(I)Landroid/view/KeyCharacterMap;

    move-result-object p2

    invoke-virtual {p2}, Landroid/view/KeyCharacterMap;->getKeyboardType()I

    move-result p2

    if-eq p2, v2, :cond_14

    move p2, v2

    goto :goto_3

    :cond_14
    move p2, v1

    :goto_3
    iput-boolean p2, p1, Lg6$t;->p:Z

    iget-object v0, p1, Lg6$t;->j:Landroidx/appcompat/view/menu/e;

    invoke-virtual {v0, p2}, Landroidx/appcompat/view/menu/e;->setQwertyMode(Z)V

    iget-object p2, p1, Lg6$t;->j:Landroidx/appcompat/view/menu/e;

    invoke-virtual {p2}, Landroidx/appcompat/view/menu/e;->startDispatchingItemsChanged()V

    :cond_15
    iput-boolean v2, p1, Lg6$t;->m:Z

    iput-boolean v1, p1, Lg6$t;->n:Z

    iput-object p1, p0, Lg6;->Z:Lg6$t;

    return v2
.end method

.method public final M0(Z)V
    .locals 5

    .line 1
    iget-object v0, p0, Lg6;->v:Lcq;

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_5

    invoke-interface {v0}, Lcq;->a()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lg6;->k:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->hasPermanentMenuKey()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lg6;->v:Lcq;

    invoke-interface {v0}, Lcq;->g()Z

    move-result v0

    if-eqz v0, :cond_5

    :cond_0
    invoke-virtual {p0}, Lg6;->r0()Landroid/view/Window$Callback;

    move-result-object v0

    iget-object v3, p0, Lg6;->v:Lcq;

    invoke-interface {v3}, Lcq;->c()Z

    move-result v3

    const/16 v4, 0x6c

    if-eqz v3, :cond_2

    if-nez p1, :cond_1

    goto :goto_0

    :cond_1
    iget-object p1, p0, Lg6;->v:Lcq;

    invoke-interface {p1}, Lcq;->d()Z

    iget-boolean p1, p0, Lg6;->C0:Z

    if-nez p1, :cond_4

    invoke-virtual {p0, v2, v1}, Lg6;->p0(IZ)Lg6$t;

    move-result-object p1

    iget-object p1, p1, Lg6$t;->j:Landroidx/appcompat/view/menu/e;

    invoke-interface {v0, v4, p1}, Landroid/view/Window$Callback;->onPanelClosed(ILandroid/view/Menu;)V

    goto :goto_1

    :cond_2
    :goto_0
    if-eqz v0, :cond_4

    iget-boolean p1, p0, Lg6;->C0:Z

    if-nez p1, :cond_4

    iget-boolean p1, p0, Lg6;->t1:Z

    if-eqz p1, :cond_3

    iget p1, p0, Lg6;->u1:I

    and-int/2addr p1, v1

    if-eqz p1, :cond_3

    iget-object p1, p0, Lg6;->l:Landroid/view/Window;

    invoke-virtual {p1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object p1

    iget-object v3, p0, Lg6;->v1:Ljava/lang/Runnable;

    invoke-virtual {p1, v3}, Landroid/view/View;->removeCallbacks(Ljava/lang/Runnable;)Z

    iget-object p1, p0, Lg6;->v1:Ljava/lang/Runnable;

    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    :cond_3
    invoke-virtual {p0, v2, v1}, Lg6;->p0(IZ)Lg6$t;

    move-result-object p1

    iget-object v1, p1, Lg6$t;->j:Landroidx/appcompat/view/menu/e;

    if-eqz v1, :cond_4

    iget-boolean v3, p1, Lg6$t;->r:Z

    if-nez v3, :cond_4

    iget-object v3, p1, Lg6$t;->i:Landroid/view/View;

    invoke-interface {v0, v2, v3, v1}, Landroid/view/Window$Callback;->onPreparePanel(ILandroid/view/View;Landroid/view/Menu;)Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object p1, p1, Lg6$t;->j:Landroidx/appcompat/view/menu/e;

    invoke-interface {v0, v4, p1}, Landroid/view/Window$Callback;->onMenuOpened(ILandroid/view/Menu;)Z

    iget-object p1, p0, Lg6;->v:Lcq;

    invoke-interface {p1}, Lcq;->b()Z

    :cond_4
    :goto_1
    return-void

    :cond_5
    invoke-virtual {p0, v2, v1}, Lg6;->p0(IZ)Lg6$t;

    move-result-object p1

    iput-boolean v1, p1, Lg6$t;->q:Z

    invoke-virtual {p0, p1, v2}, Lg6;->Y(Lg6$t;Z)V

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lg6;->I0(Lg6$t;Landroid/view/KeyEvent;)V

    return-void
.end method

.method public final N(Z)Z
    .locals 1

    .line 1
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lg6;->O(ZZ)Z

    move-result p1

    return p1
.end method

.method public final N0(I)I
    .locals 2

    .line 1
    const/16 v0, 0x8

    const-string v1, "AppCompatDelegate"

    if-ne p1, v0, :cond_0

    const-string p1, "You should now use the AppCompatDelegate.FEATURE_SUPPORT_ACTION_BAR id when requesting this feature."

    invoke-static {v1, p1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/16 p1, 0x6c

    return p1

    :cond_0
    const/16 v0, 0x9

    if-ne p1, v0, :cond_1

    const-string p1, "You should now use the AppCompatDelegate.FEATURE_SUPPORT_ACTION_BAR_OVERLAY id when requesting this feature."

    invoke-static {v1, p1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/16 p1, 0x6d

    :cond_1
    return p1
.end method

.method public final O(ZZ)Z
    .locals 4

    .line 1
    iget-boolean v0, p0, Lg6;->C0:Z

    if-eqz v0, :cond_0

    const/4 p1, 0x0

    return p1

    :cond_0
    invoke-virtual {p0}, Lg6;->T()I

    move-result v0

    iget-object v1, p0, Lg6;->k:Landroid/content/Context;

    invoke-virtual {p0, v1, v0}, Lg6;->y0(Landroid/content/Context;I)I

    move-result v1

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x21

    if-ge v2, v3, :cond_1

    iget-object v2, p0, Lg6;->k:Landroid/content/Context;

    invoke-virtual {p0, v2}, Lg6;->S(Landroid/content/Context;)Lnl0;

    move-result-object v2

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    :goto_0
    if-nez p2, :cond_2

    if-eqz v2, :cond_2

    iget-object p2, p0, Lg6;->k:Landroid/content/Context;

    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p2

    invoke-virtual {p2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object p2

    invoke-virtual {p0, p2}, Lg6;->o0(Landroid/content/res/Configuration;)Lnl0;

    move-result-object v2

    :cond_2
    invoke-virtual {p0, v1, v2, p1}, Lg6;->Y0(ILnl0;Z)Z

    move-result p1

    if-nez v0, :cond_3

    iget-object p2, p0, Lg6;->k:Landroid/content/Context;

    invoke-virtual {p0, p2}, Lg6;->n0(Landroid/content/Context;)Lg6$p;

    move-result-object p2

    invoke-virtual {p2}, Lg6$p;->e()V

    goto :goto_1

    :cond_3
    iget-object p2, p0, Lg6;->p1:Lg6$p;

    if-eqz p2, :cond_4

    invoke-virtual {p2}, Lg6$p;->a()V

    :cond_4
    :goto_1
    const/4 p2, 0x3

    if-ne v0, p2, :cond_5

    iget-object p2, p0, Lg6;->k:Landroid/content/Context;

    invoke-virtual {p0, p2}, Lg6;->m0(Landroid/content/Context;)Lg6$p;

    move-result-object p2

    invoke-virtual {p2}, Lg6$p;->e()V

    goto :goto_2

    :cond_5
    iget-object p2, p0, Lg6;->s1:Lg6$p;

    if-eqz p2, :cond_6

    invoke-virtual {p2}, Lg6$p;->a()V

    :cond_6
    :goto_2
    return p1
.end method

.method public O0(Landroid/content/res/Configuration;Lnl0;)V
    .locals 0

    .line 1
    invoke-static {p1, p2}, Lg6$k;->d(Landroid/content/res/Configuration;Lnl0;)V

    return-void
.end method

.method public P()Z
    .locals 1

    .line 1
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lg6;->N(Z)Z

    move-result v0

    return v0
.end method

.method public P0(Lnl0;)V
    .locals 0

    .line 1
    invoke-static {p1}, Lg6$k;->c(Lnl0;)V

    return-void
.end method

.method public final Q()V
    .locals 5

    .line 1
    iget-object v0, p0, Lg6;->H:Landroid/view/ViewGroup;

    const v1, 0x1020002

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/appcompat/widget/ContentFrameLayout;

    iget-object v1, p0, Lg6;->l:Landroid/view/Window;

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getPaddingLeft()I

    move-result v2

    invoke-virtual {v1}, Landroid/view/View;->getPaddingTop()I

    move-result v3

    invoke-virtual {v1}, Landroid/view/View;->getPaddingRight()I

    move-result v4

    invoke-virtual {v1}, Landroid/view/View;->getPaddingBottom()I

    move-result v1

    invoke-virtual {v0, v2, v3, v4, v1}, Landroidx/appcompat/widget/ContentFrameLayout;->a(IIII)V

    iget-object v1, p0, Lg6;->k:Landroid/content/Context;

    sget-object v2, Lbc1;->y0:[I

    invoke-virtual {v1, v2}, Landroid/content/Context;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v1

    sget v2, Lbc1;->K0:I

    invoke-virtual {v0}, Landroidx/appcompat/widget/ContentFrameLayout;->getMinWidthMajor()Landroid/util/TypedValue;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getValue(ILandroid/util/TypedValue;)Z

    sget v2, Lbc1;->L0:I

    invoke-virtual {v0}, Landroidx/appcompat/widget/ContentFrameLayout;->getMinWidthMinor()Landroid/util/TypedValue;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getValue(ILandroid/util/TypedValue;)Z

    sget v2, Lbc1;->I0:I

    invoke-virtual {v1, v2}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v0}, Landroidx/appcompat/widget/ContentFrameLayout;->getFixedWidthMajor()Landroid/util/TypedValue;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getValue(ILandroid/util/TypedValue;)Z

    :cond_0
    sget v2, Lbc1;->J0:I

    invoke-virtual {v1, v2}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v0}, Landroidx/appcompat/widget/ContentFrameLayout;->getFixedWidthMinor()Landroid/util/TypedValue;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getValue(ILandroid/util/TypedValue;)Z

    :cond_1
    sget v2, Lbc1;->G0:I

    invoke-virtual {v1, v2}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {v0}, Landroidx/appcompat/widget/ContentFrameLayout;->getFixedHeightMajor()Landroid/util/TypedValue;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getValue(ILandroid/util/TypedValue;)Z

    :cond_2
    sget v2, Lbc1;->H0:I

    invoke-virtual {v1, v2}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {v0}, Landroidx/appcompat/widget/ContentFrameLayout;->getFixedHeightMinor()Landroid/util/TypedValue;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getValue(ILandroid/util/TypedValue;)Z

    :cond_3
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    invoke-virtual {v0}, Landroid/view/View;->requestLayout()V

    return-void
.end method

.method public final Q0()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lg6;->G:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lg6;->H:Landroid/view/ViewGroup;

    if-eqz v0, :cond_0

    invoke-static {v0}, Lo32;->U(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final R(Landroid/view/Window;)V
    .locals 3

    .line 1
    iget-object v0, p0, Lg6;->l:Landroid/view/Window;

    const-string v1, "AppCompat has already installed itself into the Window"

    if-nez v0, :cond_3

    invoke-virtual {p1}, Landroid/view/Window;->getCallback()Landroid/view/Window$Callback;

    move-result-object v0

    instance-of v2, v0, Lg6$n;

    if-nez v2, :cond_2

    new-instance v1, Lg6$n;

    invoke-direct {v1, p0, v0}, Lg6$n;-><init>(Lg6;Landroid/view/Window$Callback;)V

    iput-object v1, p0, Lg6;->m:Lg6$n;

    invoke-virtual {p1, v1}, Landroid/view/Window;->setCallback(Landroid/view/Window$Callback;)V

    iget-object v0, p0, Lg6;->k:Landroid/content/Context;

    sget-object v1, Lg6;->F1:[I

    const/4 v2, 0x0

    invoke-static {v0, v2, v1}, Ltw1;->u(Landroid/content/Context;Landroid/util/AttributeSet;[I)Ltw1;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ltw1;->h(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p1, v1}, Landroid/view/Window;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    :cond_0
    invoke-virtual {v0}, Ltw1;->w()V

    iput-object p1, p0, Lg6;->l:Landroid/view/Window;

    sget p1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v0, 0x21

    if-lt p1, v0, :cond_1

    iget-object p1, p0, Lg6;->B1:Landroid/window/OnBackInvokedDispatcher;

    if-nez p1, :cond_1

    invoke-virtual {p0, v2}, Lg6;->I(Landroid/window/OnBackInvokedDispatcher;)V

    :cond_1
    return-void

    :cond_2
    new-instance p1, Ljava/lang/IllegalStateException;

    invoke-direct {p1, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_3
    new-instance p1, Ljava/lang/IllegalStateException;

    invoke-direct {p1, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public final R0(Landroid/view/ViewParent;)Z
    .locals 3

    .line 1
    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    :cond_0
    iget-object v1, p0, Lg6;->l:Landroid/view/Window;

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    :goto_0
    if-nez p1, :cond_1

    const/4 p1, 0x1

    return p1

    :cond_1
    if-eq p1, v1, :cond_3

    instance-of v2, p1, Landroid/view/View;

    if-eqz v2, :cond_3

    move-object v2, p1

    check-cast v2, Landroid/view/View;

    invoke-static {v2}, Lo32;->T(Landroid/view/View;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto :goto_1

    :cond_2
    invoke-interface {p1}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object p1

    goto :goto_0

    :cond_3
    :goto_1
    return v0
.end method

.method public S(Landroid/content/Context;)Lnl0;
    .locals 3

    .line 1
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x21

    const/4 v2, 0x0

    if-lt v0, v1, :cond_0

    return-object v2

    :cond_0
    invoke-static {}, Lf6;->o()Lnl0;

    move-result-object v0

    if-nez v0, :cond_1

    return-object v2

    :cond_1
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object p1

    invoke-virtual {p0, p1}, Lg6;->o0(Landroid/content/res/Configuration;)Lnl0;

    move-result-object p1

    invoke-static {v0, p1}, Lql0;->b(Lnl0;Lnl0;)Lnl0;

    move-result-object v0

    invoke-virtual {v0}, Lnl0;->e()Z

    move-result v1

    if-eqz v1, :cond_2

    goto :goto_0

    :cond_2
    move-object p1, v0

    :goto_0
    return-object p1
.end method

.method public S0()Z
    .locals 3

    .line 1
    iget-object v0, p0, Lg6;->B1:Landroid/window/OnBackInvokedDispatcher;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    :cond_0
    invoke-virtual {p0, v1, v1}, Lg6;->p0(IZ)Lg6$t;

    move-result-object v0

    const/4 v2, 0x1

    if-eqz v0, :cond_1

    iget-boolean v0, v0, Lg6$t;->o:Z

    if-eqz v0, :cond_1

    return v2

    :cond_1
    iget-object v0, p0, Lg6;->y:Lc2;

    if-eqz v0, :cond_2

    return v2

    :cond_2
    return v1
.end method

.method public final T()I
    .locals 2

    .line 1
    iget v0, p0, Lg6;->c1:I

    const/16 v1, -0x64

    if-eq v0, v1, :cond_0

    goto :goto_0

    :cond_0
    invoke-static {}, Lf6;->k()I

    move-result v0

    :goto_0
    return v0
.end method

.method public T0(Lc2$a;)Lc2;
    .locals 2

    .line 1
    if-eqz p1, :cond_3

    iget-object v0, p0, Lg6;->y:Lc2;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lc2;->a()V

    :cond_0
    new-instance v0, Lg6$h;

    invoke-direct {v0, p0, p1}, Lg6$h;-><init>(Lg6;Lc2$a;)V

    invoke-virtual {p0}, Lg6;->p()Lt1;

    move-result-object p1

    if-eqz p1, :cond_1

    invoke-virtual {p1, v0}, Lt1;->v(Lc2$a;)Lc2;

    move-result-object p1

    iput-object p1, p0, Lg6;->y:Lc2;

    if-eqz p1, :cond_1

    iget-object v1, p0, Lg6;->n:Lz5;

    if-eqz v1, :cond_1

    invoke-interface {v1, p1}, Lz5;->onSupportActionModeStarted(Lc2;)V

    :cond_1
    iget-object p1, p0, Lg6;->y:Lc2;

    if-nez p1, :cond_2

    invoke-virtual {p0, v0}, Lg6;->U0(Lc2$a;)Lc2;

    move-result-object p1

    iput-object p1, p0, Lg6;->y:Lc2;

    :cond_2
    invoke-virtual {p0}, Lg6;->Z0()V

    iget-object p1, p0, Lg6;->y:Lc2;

    return-object p1

    :cond_3
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "ActionMode callback can not be null."

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public U(ILg6$t;Landroid/view/Menu;)V
    .locals 2

    .line 1
    if-nez p3, :cond_1

    if-nez p2, :cond_0

    if-ltz p1, :cond_0

    iget-object v0, p0, Lg6;->Y:[Lg6$t;

    array-length v1, v0

    if-ge p1, v1, :cond_0

    aget-object p2, v0, p1

    :cond_0
    if-eqz p2, :cond_1

    iget-object p3, p2, Lg6$t;->j:Landroidx/appcompat/view/menu/e;

    :cond_1
    if-eqz p2, :cond_2

    iget-boolean p2, p2, Lg6$t;->o:Z

    if-nez p2, :cond_2

    return-void

    :cond_2
    iget-boolean p2, p0, Lg6;->C0:Z

    if-nez p2, :cond_3

    iget-object p2, p0, Lg6;->m:Lg6$n;

    iget-object v0, p0, Lg6;->l:Landroid/view/Window;

    invoke-virtual {v0}, Landroid/view/Window;->getCallback()Landroid/view/Window$Callback;

    move-result-object v0

    invoke-virtual {p2, v0, p1, p3}, Lg6$n;->d(Landroid/view/Window$Callback;ILandroid/view/Menu;)V

    :cond_3
    return-void
.end method

.method public U0(Lc2$a;)Lc2;
    .locals 7

    .line 1
    invoke-virtual {p0}, Lg6;->f0()V

    iget-object v0, p0, Lg6;->y:Lc2;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lc2;->a()V

    :cond_0
    instance-of v0, p1, Lg6$h;

    if-nez v0, :cond_1

    new-instance v0, Lg6$h;

    invoke-direct {v0, p0, p1}, Lg6$h;-><init>(Lg6;Lc2$a;)V

    move-object p1, v0

    :cond_1
    iget-object v0, p0, Lg6;->n:Lz5;

    const/4 v1, 0x0

    if-eqz v0, :cond_2

    iget-boolean v2, p0, Lg6;->C0:Z

    if-nez v2, :cond_2

    :try_start_0
    invoke-interface {v0, p1}, Lz5;->onWindowStartingSupportActionMode(Lc2$a;)Lc2;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/AbstractMethodError; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    :cond_2
    move-object v0, v1

    :goto_0
    if-eqz v0, :cond_3

    iput-object v0, p0, Lg6;->y:Lc2;

    goto/16 :goto_5

    :cond_3
    iget-object v0, p0, Lg6;->z:Landroidx/appcompat/widget/ActionBarContextView;

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-nez v0, :cond_6

    iget-boolean v0, p0, Lg6;->U:Z

    if-eqz v0, :cond_5

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    iget-object v4, p0, Lg6;->k:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v4

    sget v5, Lsa1;->d:I

    invoke-virtual {v4, v5, v0, v3}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    iget v5, v0, Landroid/util/TypedValue;->resourceId:I

    if-eqz v5, :cond_4

    iget-object v5, p0, Lg6;->k:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->newTheme()Landroid/content/res/Resources$Theme;

    move-result-object v5

    invoke-virtual {v5, v4}, Landroid/content/res/Resources$Theme;->setTo(Landroid/content/res/Resources$Theme;)V

    iget v4, v0, Landroid/util/TypedValue;->resourceId:I

    invoke-virtual {v5, v4, v3}, Landroid/content/res/Resources$Theme;->applyStyle(IZ)V

    new-instance v4, Lul;

    iget-object v6, p0, Lg6;->k:Landroid/content/Context;

    invoke-direct {v4, v6, v2}, Lul;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v4}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v6

    invoke-virtual {v6, v5}, Landroid/content/res/Resources$Theme;->setTo(Landroid/content/res/Resources$Theme;)V

    goto :goto_1

    :cond_4
    iget-object v4, p0, Lg6;->k:Landroid/content/Context;

    :goto_1
    new-instance v5, Landroidx/appcompat/widget/ActionBarContextView;

    invoke-direct {v5, v4}, Landroidx/appcompat/widget/ActionBarContextView;-><init>(Landroid/content/Context;)V

    iput-object v5, p0, Lg6;->z:Landroidx/appcompat/widget/ActionBarContextView;

    new-instance v5, Landroid/widget/PopupWindow;

    sget v6, Lsa1;->f:I

    invoke-direct {v5, v4, v1, v6}, Landroid/widget/PopupWindow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v5, p0, Lg6;->A:Landroid/widget/PopupWindow;

    const/4 v6, 0x2

    invoke-static {v5, v6}, Lc61;->b(Landroid/widget/PopupWindow;I)V

    iget-object v5, p0, Lg6;->A:Landroid/widget/PopupWindow;

    iget-object v6, p0, Lg6;->z:Landroidx/appcompat/widget/ActionBarContextView;

    invoke-virtual {v5, v6}, Landroid/widget/PopupWindow;->setContentView(Landroid/view/View;)V

    iget-object v5, p0, Lg6;->A:Landroid/widget/PopupWindow;

    const/4 v6, -0x1

    invoke-virtual {v5, v6}, Landroid/widget/PopupWindow;->setWidth(I)V

    invoke-virtual {v4}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v5

    sget v6, Lsa1;->b:I

    invoke-virtual {v5, v6, v0, v3}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    iget v0, v0, Landroid/util/TypedValue;->data:I

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    invoke-static {v0, v4}, Landroid/util/TypedValue;->complexToDimensionPixelSize(ILandroid/util/DisplayMetrics;)I

    move-result v0

    iget-object v4, p0, Lg6;->z:Landroidx/appcompat/widget/ActionBarContextView;

    invoke-virtual {v4, v0}, Landroidx/appcompat/widget/ActionBarContextView;->setContentHeight(I)V

    iget-object v0, p0, Lg6;->A:Landroid/widget/PopupWindow;

    const/4 v4, -0x2

    invoke-virtual {v0, v4}, Landroid/widget/PopupWindow;->setHeight(I)V

    new-instance v0, Lg6$d;

    invoke-direct {v0, p0}, Lg6$d;-><init>(Lg6;)V

    iput-object v0, p0, Lg6;->C:Ljava/lang/Runnable;

    goto :goto_2

    :cond_5
    iget-object v0, p0, Lg6;->H:Landroid/view/ViewGroup;

    sget v4, Ldb1;->h:I

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/appcompat/widget/ViewStubCompat;

    if-eqz v0, :cond_6

    invoke-virtual {p0}, Lg6;->k0()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroidx/appcompat/widget/ViewStubCompat;->setLayoutInflater(Landroid/view/LayoutInflater;)V

    invoke-virtual {v0}, Landroidx/appcompat/widget/ViewStubCompat;->a()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/appcompat/widget/ActionBarContextView;

    iput-object v0, p0, Lg6;->z:Landroidx/appcompat/widget/ActionBarContextView;

    :cond_6
    :goto_2
    iget-object v0, p0, Lg6;->z:Landroidx/appcompat/widget/ActionBarContextView;

    if-eqz v0, :cond_b

    invoke-virtual {p0}, Lg6;->f0()V

    iget-object v0, p0, Lg6;->z:Landroidx/appcompat/widget/ActionBarContextView;

    invoke-virtual {v0}, Landroidx/appcompat/widget/ActionBarContextView;->k()V

    new-instance v0, Lmp1;

    iget-object v4, p0, Lg6;->z:Landroidx/appcompat/widget/ActionBarContextView;

    invoke-virtual {v4}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    iget-object v5, p0, Lg6;->z:Landroidx/appcompat/widget/ActionBarContextView;

    iget-object v6, p0, Lg6;->A:Landroid/widget/PopupWindow;

    if-nez v6, :cond_7

    goto :goto_3

    :cond_7
    move v3, v2

    :goto_3
    invoke-direct {v0, v4, v5, p1, v3}, Lmp1;-><init>(Landroid/content/Context;Landroidx/appcompat/widget/ActionBarContextView;Lc2$a;Z)V

    invoke-virtual {v0}, Lc2;->c()Landroid/view/Menu;

    move-result-object v3

    invoke-interface {p1, v0, v3}, Lc2$a;->c(Lc2;Landroid/view/Menu;)Z

    move-result p1

    if-eqz p1, :cond_a

    invoke-virtual {v0}, Lc2;->i()V

    iget-object p1, p0, Lg6;->z:Landroidx/appcompat/widget/ActionBarContextView;

    invoke-virtual {p1, v0}, Landroidx/appcompat/widget/ActionBarContextView;->h(Lc2;)V

    iput-object v0, p0, Lg6;->y:Lc2;

    invoke-virtual {p0}, Lg6;->Q0()Z

    move-result p1

    const/high16 v0, 0x3f800000    # 1.0f

    if-eqz p1, :cond_8

    iget-object p1, p0, Lg6;->z:Landroidx/appcompat/widget/ActionBarContextView;

    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Landroid/view/View;->setAlpha(F)V

    iget-object p1, p0, Lg6;->z:Landroidx/appcompat/widget/ActionBarContextView;

    invoke-static {p1}, Lo32;->e(Landroid/view/View;)Li42;

    move-result-object p1

    invoke-virtual {p1, v0}, Li42;->b(F)Li42;

    move-result-object p1

    iput-object p1, p0, Lg6;->D:Li42;

    new-instance v0, Lg6$e;

    invoke-direct {v0, p0}, Lg6$e;-><init>(Lg6;)V

    invoke-virtual {p1, v0}, Li42;->h(Lk42;)Li42;

    goto :goto_4

    :cond_8
    iget-object p1, p0, Lg6;->z:Landroidx/appcompat/widget/ActionBarContextView;

    invoke-virtual {p1, v0}, Landroid/view/View;->setAlpha(F)V

    iget-object p1, p0, Lg6;->z:Landroidx/appcompat/widget/ActionBarContextView;

    invoke-virtual {p1, v2}, Landroidx/appcompat/widget/ActionBarContextView;->setVisibility(I)V

    iget-object p1, p0, Lg6;->z:Landroidx/appcompat/widget/ActionBarContextView;

    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object p1

    instance-of p1, p1, Landroid/view/View;

    if-eqz p1, :cond_9

    iget-object p1, p0, Lg6;->z:Landroidx/appcompat/widget/ActionBarContextView;

    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object p1

    check-cast p1, Landroid/view/View;

    invoke-static {p1}, Lo32;->n0(Landroid/view/View;)V

    :cond_9
    :goto_4
    iget-object p1, p0, Lg6;->A:Landroid/widget/PopupWindow;

    if-eqz p1, :cond_b

    iget-object p1, p0, Lg6;->l:Landroid/view/Window;

    invoke-virtual {p1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object p1

    iget-object v0, p0, Lg6;->C:Ljava/lang/Runnable;

    invoke-virtual {p1, v0}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    goto :goto_5

    :cond_a
    iput-object v1, p0, Lg6;->y:Lc2;

    :cond_b
    :goto_5
    iget-object p1, p0, Lg6;->y:Lc2;

    if-eqz p1, :cond_c

    iget-object v0, p0, Lg6;->n:Lz5;

    if-eqz v0, :cond_c

    invoke-interface {v0, p1}, Lz5;->onSupportActionModeStarted(Lc2;)V

    :cond_c
    invoke-virtual {p0}, Lg6;->Z0()V

    iget-object p1, p0, Lg6;->y:Lc2;

    return-object p1
.end method

.method public V(Landroidx/appcompat/view/menu/e;)V
    .locals 2

    .line 1
    iget-boolean v0, p0, Lg6;->W:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lg6;->W:Z

    iget-object v0, p0, Lg6;->v:Lcq;

    invoke-interface {v0}, Lcq;->i()V

    invoke-virtual {p0}, Lg6;->r0()Landroid/view/Window$Callback;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-boolean v1, p0, Lg6;->C0:Z

    if-nez v1, :cond_1

    const/16 v1, 0x6c

    invoke-interface {v0, v1, p1}, Landroid/view/Window$Callback;->onPanelClosed(ILandroid/view/Menu;)V

    :cond_1
    const/4 p1, 0x0

    iput-boolean p1, p0, Lg6;->W:Z

    return-void
.end method

.method public final V0()V
    .locals 2

    .line 1
    iget-boolean v0, p0, Lg6;->G:Z

    if-nez v0, :cond_0

    return-void

    :cond_0
    new-instance v0, Landroid/util/AndroidRuntimeException;

    const-string v1, "Window feature must be requested before adding content"

    invoke-direct {v0, v1}, Landroid/util/AndroidRuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final W()V
    .locals 1

    .line 1
    iget-object v0, p0, Lg6;->p1:Lg6$p;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lg6$p;->a()V

    :cond_0
    iget-object v0, p0, Lg6;->s1:Lg6$p;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lg6$p;->a()V

    :cond_1
    return-void
.end method

.method public final W0()Lv5;
    .locals 3

    .line 1
    iget-object v0, p0, Lg6;->k:Landroid/content/Context;

    :goto_0
    const/4 v1, 0x0

    if-eqz v0, :cond_1

    instance-of v2, v0, Lv5;

    if-eqz v2, :cond_0

    check-cast v0, Lv5;

    return-object v0

    :cond_0
    instance-of v2, v0, Landroid/content/ContextWrapper;

    if-eqz v2, :cond_1

    check-cast v0, Landroid/content/ContextWrapper;

    invoke-virtual {v0}, Landroid/content/ContextWrapper;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    goto :goto_0

    :cond_1
    return-object v1
.end method

.method public X(I)V
    .locals 1

    .line 1
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lg6;->p0(IZ)Lg6$t;

    move-result-object p1

    invoke-virtual {p0, p1, v0}, Lg6;->Y(Lg6$t;Z)V

    return-void
.end method

.method public final X0(Landroid/content/res/Configuration;)V
    .locals 3

    .line 1
    iget-object v0, p0, Lg6;->j:Ljava/lang/Object;

    check-cast v0, Landroid/app/Activity;

    instance-of v1, v0, Lqj0;

    if-eqz v1, :cond_0

    move-object v1, v0

    check-cast v1, Lqj0;

    invoke-interface {v1}, Lqj0;->getLifecycle()Landroidx/lifecycle/Lifecycle;

    move-result-object v1

    invoke-virtual {v1}, Landroidx/lifecycle/Lifecycle;->b()Landroidx/lifecycle/Lifecycle$State;

    move-result-object v1

    sget-object v2, Landroidx/lifecycle/Lifecycle$State;->CREATED:Landroidx/lifecycle/Lifecycle$State;

    invoke-virtual {v1, v2}, Landroidx/lifecycle/Lifecycle$State;->isAtLeast(Landroidx/lifecycle/Lifecycle$State;)Z

    move-result v1

    if-eqz v1, :cond_1

    goto :goto_0

    :cond_0
    iget-boolean v1, p0, Lg6;->A0:Z

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Lg6;->C0:Z

    if-nez v1, :cond_1

    :goto_0
    invoke-virtual {v0, p1}, Landroid/app/Activity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    :cond_1
    return-void
.end method

.method public Y(Lg6$t;Z)V
    .locals 3

    .line 1
    if-eqz p2, :cond_0

    iget v0, p1, Lg6$t;->a:I

    if-nez v0, :cond_0

    iget-object v0, p0, Lg6;->v:Lcq;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcq;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object p1, p1, Lg6$t;->j:Landroidx/appcompat/view/menu/e;

    invoke-virtual {p0, p1}, Lg6;->V(Landroidx/appcompat/view/menu/e;)V

    return-void

    :cond_0
    iget-object v0, p0, Lg6;->k:Landroid/content/Context;

    const-string v1, "window"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    iget-boolean v2, p1, Lg6$t;->o:Z

    if-eqz v2, :cond_1

    iget-object v2, p1, Lg6$t;->g:Landroid/view/ViewGroup;

    if-eqz v2, :cond_1

    invoke-interface {v0, v2}, Landroid/view/ViewManager;->removeView(Landroid/view/View;)V

    if-eqz p2, :cond_1

    iget p2, p1, Lg6$t;->a:I

    invoke-virtual {p0, p2, p1, v1}, Lg6;->U(ILg6$t;Landroid/view/Menu;)V

    :cond_1
    const/4 p2, 0x0

    iput-boolean p2, p1, Lg6$t;->m:Z

    iput-boolean p2, p1, Lg6$t;->n:Z

    iput-boolean p2, p1, Lg6$t;->o:Z

    iput-object v1, p1, Lg6$t;->h:Landroid/view/View;

    const/4 p2, 0x1

    iput-boolean p2, p1, Lg6$t;->q:Z

    iget-object p2, p0, Lg6;->Z:Lg6$t;

    if-ne p2, p1, :cond_2

    iput-object v1, p0, Lg6;->Z:Lg6$t;

    :cond_2
    iget p1, p1, Lg6$t;->a:I

    if-nez p1, :cond_3

    invoke-virtual {p0}, Lg6;->Z0()V

    :cond_3
    return-void
.end method

.method public final Y0(ILnl0;Z)Z
    .locals 8

    .line 1
    iget-object v1, p0, Lg6;->k:Landroid/content/Context;

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    move v2, p1

    move-object v3, p2

    invoke-virtual/range {v0 .. v5}, Lg6;->Z(Landroid/content/Context;ILnl0;Landroid/content/res/Configuration;Z)Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v1, p0, Lg6;->k:Landroid/content/Context;

    invoke-virtual {p0, v1}, Lg6;->l0(Landroid/content/Context;)I

    move-result v1

    iget-object v2, p0, Lg6;->b1:Landroid/content/res/Configuration;

    if-nez v2, :cond_0

    iget-object v2, p0, Lg6;->k:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    :cond_0
    iget v3, v2, Landroid/content/res/Configuration;->uiMode:I

    and-int/lit8 v3, v3, 0x30

    iget v4, v0, Landroid/content/res/Configuration;->uiMode:I

    and-int/lit8 v4, v4, 0x30

    invoke-virtual {p0, v2}, Lg6;->o0(Landroid/content/res/Configuration;)Lnl0;

    move-result-object v2

    const/4 v5, 0x0

    if-nez p2, :cond_1

    move-object v0, v5

    goto :goto_0

    :cond_1
    invoke-virtual {p0, v0}, Lg6;->o0(Landroid/content/res/Configuration;)Lnl0;

    move-result-object v0

    :goto_0
    const/4 v6, 0x0

    if-eq v3, v4, :cond_2

    const/16 v3, 0x200

    goto :goto_1

    :cond_2
    move v3, v6

    :goto_1
    if-eqz v0, :cond_3

    invoke-virtual {v2, v0}, Lnl0;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    or-int/lit8 v2, v3, 0x4

    or-int/lit16 v3, v2, 0x2000

    :cond_3
    not-int v2, v1

    and-int/2addr v2, v3

    const/4 v7, 0x1

    if-eqz v2, :cond_5

    if-eqz p3, :cond_5

    iget-boolean p3, p0, Lg6;->k0:Z

    if-eqz p3, :cond_5

    sget-boolean p3, Lg6;->G1:Z

    if-nez p3, :cond_4

    iget-boolean p3, p0, Lg6;->A0:Z

    if-eqz p3, :cond_5

    :cond_4
    iget-object p3, p0, Lg6;->j:Ljava/lang/Object;

    instance-of v2, p3, Landroid/app/Activity;

    if-eqz v2, :cond_5

    check-cast p3, Landroid/app/Activity;

    invoke-virtual {p3}, Landroid/app/Activity;->isChild()Z

    move-result p3

    if-nez p3, :cond_5

    iget-object p3, p0, Lg6;->j:Ljava/lang/Object;

    check-cast p3, Landroid/app/Activity;

    invoke-static {p3}, Lh2;->f(Landroid/app/Activity;)V

    move p3, v7

    goto :goto_2

    :cond_5
    move p3, v6

    :goto_2
    if-nez p3, :cond_7

    if-eqz v3, :cond_7

    and-int p3, v3, v1

    if-ne p3, v3, :cond_6

    move v6, v7

    :cond_6
    invoke-virtual {p0, v4, v0, v6, v5}, Lg6;->a1(ILnl0;ZLandroid/content/res/Configuration;)V

    goto :goto_3

    :cond_7
    move v7, p3

    :goto_3
    if-eqz v7, :cond_9

    iget-object p3, p0, Lg6;->j:Ljava/lang/Object;

    instance-of v1, p3, Lv5;

    if-eqz v1, :cond_9

    and-int/lit16 v1, v3, 0x200

    if-eqz v1, :cond_8

    check-cast p3, Lv5;

    invoke-virtual {p3, p1}, Lv5;->s(I)V

    :cond_8
    and-int/lit8 p1, v3, 0x4

    if-eqz p1, :cond_9

    iget-object p1, p0, Lg6;->j:Ljava/lang/Object;

    check-cast p1, Lv5;

    invoke-virtual {p1, p2}, Lv5;->r(Lnl0;)V

    :cond_9
    if-eqz v7, :cond_a

    if-eqz v0, :cond_a

    iget-object p1, p0, Lg6;->k:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object p1

    invoke-virtual {p0, p1}, Lg6;->o0(Landroid/content/res/Configuration;)Lnl0;

    move-result-object p1

    invoke-virtual {p0, p1}, Lg6;->P0(Lnl0;)V

    :cond_a
    return v7
.end method

.method public final Z(Landroid/content/Context;ILnl0;Landroid/content/res/Configuration;Z)Landroid/content/res/Configuration;
    .locals 1

    .line 1
    const/4 v0, 0x1

    if-eq p2, v0, :cond_2

    const/4 v0, 0x2

    if-eq p2, v0, :cond_1

    if-eqz p5, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object p1

    invoke-virtual {p1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object p1

    iget p1, p1, Landroid/content/res/Configuration;->uiMode:I

    and-int/lit8 p1, p1, 0x30

    goto :goto_0

    :cond_1
    const/16 p1, 0x20

    goto :goto_0

    :cond_2
    const/16 p1, 0x10

    :goto_0
    new-instance p2, Landroid/content/res/Configuration;

    invoke-direct {p2}, Landroid/content/res/Configuration;-><init>()V

    const/4 p5, 0x0

    iput p5, p2, Landroid/content/res/Configuration;->fontScale:F

    if-eqz p4, :cond_3

    invoke-virtual {p2, p4}, Landroid/content/res/Configuration;->setTo(Landroid/content/res/Configuration;)V

    :cond_3
    iget p4, p2, Landroid/content/res/Configuration;->uiMode:I

    and-int/lit8 p4, p4, -0x31

    or-int/2addr p1, p4

    iput p1, p2, Landroid/content/res/Configuration;->uiMode:I

    if-eqz p3, :cond_4

    invoke-virtual {p0, p2, p3}, Lg6;->O0(Landroid/content/res/Configuration;Lnl0;)V

    :cond_4
    return-object p2
.end method

.method public Z0()V
    .locals 2

    .line 1
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x21

    if-lt v0, v1, :cond_1

    invoke-virtual {p0}, Lg6;->S0()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lg6;->C1:Landroid/window/OnBackInvokedCallback;

    if-nez v1, :cond_0

    iget-object v0, p0, Lg6;->B1:Landroid/window/OnBackInvokedDispatcher;

    invoke-static {v0, p0}, Lg6$m;->b(Ljava/lang/Object;Lg6;)Landroid/window/OnBackInvokedCallback;

    move-result-object v0

    iput-object v0, p0, Lg6;->C1:Landroid/window/OnBackInvokedCallback;

    goto :goto_0

    :cond_0
    if-nez v0, :cond_1

    iget-object v0, p0, Lg6;->C1:Landroid/window/OnBackInvokedCallback;

    if-eqz v0, :cond_1

    iget-object v1, p0, Lg6;->B1:Landroid/window/OnBackInvokedDispatcher;

    invoke-static {v1, v0}, Lg6$m;->c(Ljava/lang/Object;Ljava/lang/Object;)V

    :cond_1
    :goto_0
    return-void
.end method

.method public final a0()Landroid/view/ViewGroup;
    .locals 7

    .line 1
    iget-object v0, p0, Lg6;->k:Landroid/content/Context;

    sget-object v1, Lbc1;->y0:[I

    invoke-virtual {v0, v1}, Landroid/content/Context;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v0

    sget v1, Lbc1;->D0:I

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->hasValue(I)Z

    move-result v2

    if-eqz v2, :cond_10

    sget v2, Lbc1;->M0:I

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v2

    const/4 v4, 0x1

    if-eqz v2, :cond_0

    invoke-virtual {p0, v4}, Lg6;->E(I)Z

    goto :goto_0

    :cond_0
    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v1, 0x6c

    invoke-virtual {p0, v1}, Lg6;->E(I)Z

    :cond_1
    :goto_0
    sget v1, Lbc1;->E0:I

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    const/16 v2, 0x6d

    if-eqz v1, :cond_2

    invoke-virtual {p0, v2}, Lg6;->E(I)Z

    :cond_2
    sget v1, Lbc1;->F0:I

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    if-eqz v1, :cond_3

    const/16 v1, 0xa

    invoke-virtual {p0, v1}, Lg6;->E(I)Z

    :cond_3
    sget v1, Lbc1;->z0:I

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lg6;->U:Z

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    invoke-virtual {p0}, Lg6;->h0()V

    iget-object v0, p0, Lg6;->l:Landroid/view/Window;

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    iget-object v0, p0, Lg6;->k:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iget-boolean v1, p0, Lg6;->V:Z

    const/4 v5, 0x0

    if-nez v1, :cond_9

    iget-boolean v1, p0, Lg6;->U:Z

    if-eqz v1, :cond_4

    sget v1, Lob1;->f:I

    invoke-virtual {v0, v1, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-boolean v3, p0, Lg6;->P:Z

    iput-boolean v3, p0, Lg6;->O:Z

    goto/16 :goto_3

    :cond_4
    iget-boolean v0, p0, Lg6;->O:Z

    if-eqz v0, :cond_8

    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    iget-object v1, p0, Lg6;->k:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    sget v6, Lsa1;->d:I

    invoke-virtual {v1, v6, v0, v4}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    iget v1, v0, Landroid/util/TypedValue;->resourceId:I

    if-eqz v1, :cond_5

    new-instance v1, Lul;

    iget-object v4, p0, Lg6;->k:Landroid/content/Context;

    iget v0, v0, Landroid/util/TypedValue;->resourceId:I

    invoke-direct {v1, v4, v0}, Lul;-><init>(Landroid/content/Context;I)V

    goto :goto_1

    :cond_5
    iget-object v1, p0, Lg6;->k:Landroid/content/Context;

    :goto_1
    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lob1;->p:I

    invoke-virtual {v0, v1, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    sget v1, Ldb1;->p:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcq;

    iput-object v1, p0, Lg6;->v:Lcq;

    invoke-virtual {p0}, Lg6;->r0()Landroid/view/Window$Callback;

    move-result-object v4

    invoke-interface {v1, v4}, Lcq;->setWindowCallback(Landroid/view/Window$Callback;)V

    iget-boolean v1, p0, Lg6;->P:Z

    if-eqz v1, :cond_6

    iget-object v1, p0, Lg6;->v:Lcq;

    invoke-interface {v1, v2}, Lcq;->h(I)V

    :cond_6
    iget-boolean v1, p0, Lg6;->K:Z

    if-eqz v1, :cond_7

    iget-object v1, p0, Lg6;->v:Lcq;

    const/4 v2, 0x2

    invoke-interface {v1, v2}, Lcq;->h(I)V

    :cond_7
    iget-boolean v1, p0, Lg6;->M:Z

    if-eqz v1, :cond_b

    iget-object v1, p0, Lg6;->v:Lcq;

    const/4 v2, 0x5

    invoke-interface {v1, v2}, Lcq;->h(I)V

    goto :goto_3

    :cond_8
    move-object v0, v5

    goto :goto_3

    :cond_9
    iget-boolean v1, p0, Lg6;->Q:Z

    if-eqz v1, :cond_a

    sget v1, Lob1;->o:I

    goto :goto_2

    :cond_a
    sget v1, Lob1;->n:I

    :goto_2
    invoke-virtual {v0, v1, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    :cond_b
    :goto_3
    if-eqz v0, :cond_f

    new-instance v1, Lg6$b;

    invoke-direct {v1, p0}, Lg6$b;-><init>(Lg6;)V

    invoke-static {v0, v1}, Lo32;->E0(Landroid/view/View;Lg11;)V

    iget-object v1, p0, Lg6;->v:Lcq;

    if-nez v1, :cond_c

    sget v1, Ldb1;->M:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lg6;->I:Landroid/widget/TextView;

    :cond_c
    invoke-static {v0}, Lu42;->c(Landroid/view/View;)V

    sget v1, Ldb1;->b:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroidx/appcompat/widget/ContentFrameLayout;

    iget-object v2, p0, Lg6;->l:Landroid/view/Window;

    const v4, 0x1020002

    invoke-virtual {v2, v4}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    if-eqz v2, :cond_e

    :goto_4
    invoke-virtual {v2}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v6

    if-lez v6, :cond_d

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->removeViewAt(I)V

    invoke-virtual {v1, v6}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto :goto_4

    :cond_d
    const/4 v3, -0x1

    invoke-virtual {v2, v3}, Landroid/view/View;->setId(I)V

    invoke-virtual {v1, v4}, Landroid/view/View;->setId(I)V

    instance-of v3, v2, Landroid/widget/FrameLayout;

    if-eqz v3, :cond_e

    check-cast v2, Landroid/widget/FrameLayout;

    invoke-virtual {v2, v5}, Landroid/view/View;->setForeground(Landroid/graphics/drawable/Drawable;)V

    :cond_e
    iget-object v2, p0, Lg6;->l:Landroid/view/Window;

    invoke-virtual {v2, v0}, Landroid/view/Window;->setContentView(Landroid/view/View;)V

    new-instance v2, Lg6$c;

    invoke-direct {v2, p0}, Lg6$c;-><init>(Lg6;)V

    invoke-virtual {v1, v2}, Landroidx/appcompat/widget/ContentFrameLayout;->setAttachListener(Landroidx/appcompat/widget/ContentFrameLayout$a;)V

    return-object v0

    :cond_f
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "AppCompat does not support the current theme features: { windowActionBar: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v2, p0, Lg6;->O:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v2, ", windowActionBarOverlay: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v2, p0, Lg6;->P:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v2, ", android:windowIsFloating: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v2, p0, Lg6;->U:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v2, ", windowActionModeOverlay: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v2, p0, Lg6;->Q:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v2, ", windowNoTitle: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v2, p0, Lg6;->V:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    const-string v2, " }"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_10
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "You need to use a Theme.AppCompat theme (or descendant) with this activity."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a1(ILnl0;ZLandroid/content/res/Configuration;)V
    .locals 3

    .line 1
    iget-object v0, p0, Lg6;->k:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    new-instance v1, Landroid/content/res/Configuration;

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/content/res/Configuration;-><init>(Landroid/content/res/Configuration;)V

    if-eqz p4, :cond_0

    invoke-virtual {v1, p4}, Landroid/content/res/Configuration;->updateFrom(Landroid/content/res/Configuration;)I

    :cond_0
    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object p4

    iget p4, p4, Landroid/content/res/Configuration;->uiMode:I

    and-int/lit8 p4, p4, -0x31

    or-int/2addr p1, p4

    iput p1, v1, Landroid/content/res/Configuration;->uiMode:I

    if-eqz p2, :cond_1

    invoke-virtual {p0, v1, p2}, Lg6;->O0(Landroid/content/res/Configuration;Lnl0;)V

    :cond_1
    const/4 p1, 0x0

    invoke-virtual {v0, v1, p1}, Landroid/content/res/Resources;->updateConfiguration(Landroid/content/res/Configuration;Landroid/util/DisplayMetrics;)V

    sget p1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 p2, 0x1a

    if-ge p1, p2, :cond_2

    invoke-static {v0}, Loe1;->a(Landroid/content/res/Resources;)V

    :cond_2
    iget p1, p0, Lg6;->d1:I

    if-eqz p1, :cond_3

    iget-object p2, p0, Lg6;->k:Landroid/content/Context;

    invoke-virtual {p2, p1}, Landroid/content/Context;->setTheme(I)V

    iget-object p1, p0, Lg6;->k:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object p1

    iget p2, p0, Lg6;->d1:I

    const/4 p4, 0x1

    invoke-virtual {p1, p2, p4}, Landroid/content/res/Resources$Theme;->applyStyle(IZ)V

    :cond_3
    if-eqz p3, :cond_4

    iget-object p1, p0, Lg6;->j:Ljava/lang/Object;

    instance-of p1, p1, Landroid/app/Activity;

    if-eqz p1, :cond_4

    invoke-virtual {p0, v1}, Lg6;->X0(Landroid/content/res/Configuration;)V

    :cond_4
    return-void
.end method

.method public b0(Landroid/view/View;Ljava/lang/String;Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/view/View;
    .locals 11

    .line 1
    iget-object v0, p0, Lg6;->z1:Lr7;

    const/4 v1, 0x0

    if-nez v0, :cond_1

    iget-object v0, p0, Lg6;->k:Landroid/content/Context;

    sget-object v2, Lbc1;->y0:[I

    invoke-virtual {v0, v2}, Landroid/content/Context;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v0

    sget v2, Lbc1;->C0:I

    invoke-virtual {v0, v2}, Landroid/content/res/TypedArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Lr7;

    invoke-direct {v0}, Lr7;-><init>()V

    :goto_0
    iput-object v0, p0, Lg6;->z1:Lr7;

    goto :goto_1

    :cond_0
    :try_start_0
    iget-object v2, p0, Lg6;->k:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2

    new-array v3, v1, [Ljava/lang/Class;

    invoke-virtual {v2, v3}, Ljava/lang/Class;->getDeclaredConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v2

    new-array v3, v1, [Ljava/lang/Object;

    invoke-virtual {v2, v3}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lr7;

    iput-object v2, p0, Lg6;->z1:Lr7;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Failed to instantiate custom view inflater "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, ". Falling back to default."

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v3, "AppCompatDelegate"

    invoke-static {v3, v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    new-instance v0, Lr7;

    invoke-direct {v0}, Lr7;-><init>()V

    goto :goto_0

    :cond_1
    :goto_1
    sget-boolean v8, Lg6;->E1:Z

    if-eqz v8, :cond_5

    iget-object v0, p0, Lg6;->A1:Lvi0;

    if-nez v0, :cond_2

    new-instance v0, Lvi0;

    invoke-direct {v0}, Lvi0;-><init>()V

    iput-object v0, p0, Lg6;->A1:Lvi0;

    :cond_2
    iget-object v0, p0, Lg6;->A1:Lvi0;

    invoke-virtual {v0, p4}, Lvi0;->a(Landroid/util/AttributeSet;)Z

    move-result v0

    const/4 v2, 0x1

    if-eqz v0, :cond_3

    move v7, v2

    goto :goto_3

    :cond_3
    instance-of v0, p4, Lorg/xmlpull/v1/XmlPullParser;

    if-eqz v0, :cond_4

    move-object v0, p4

    check-cast v0, Lorg/xmlpull/v1/XmlPullParser;

    invoke-interface {v0}, Lorg/xmlpull/v1/XmlPullParser;->getDepth()I

    move-result v0

    if-le v0, v2, :cond_5

    move v1, v2

    goto :goto_2

    :cond_4
    move-object v0, p1

    check-cast v0, Landroid/view/ViewParent;

    invoke-virtual {p0, v0}, Lg6;->R0(Landroid/view/ViewParent;)Z

    move-result v0

    move v1, v0

    :cond_5
    :goto_2
    move v7, v1

    :goto_3
    iget-object v2, p0, Lg6;->z1:Lr7;

    const/4 v9, 0x1

    invoke-static {}, Lf32;->c()Z

    move-result v10

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    invoke-virtual/range {v2 .. v10}, Lr7;->createView(Landroid/view/View;Ljava/lang/String;Landroid/content/Context;Landroid/util/AttributeSet;ZZZZ)Landroid/view/View;

    move-result-object p1

    return-object p1
.end method

.method public final b1(Lu62;Landroid/graphics/Rect;)I
    .locals 10

    .line 1
    const/4 v0, 0x0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lu62;->k()I

    move-result v1

    goto :goto_0

    :cond_0
    if-eqz p2, :cond_1

    iget v1, p2, Landroid/graphics/Rect;->top:I

    goto :goto_0

    :cond_1
    move v1, v0

    :goto_0
    iget-object v2, p0, Lg6;->z:Landroidx/appcompat/widget/ActionBarContextView;

    const/16 v3, 0x8

    if-eqz v2, :cond_10

    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    instance-of v2, v2, Landroid/view/ViewGroup$MarginLayoutParams;

    if-eqz v2, :cond_10

    iget-object v2, p0, Lg6;->z:Landroidx/appcompat/widget/ActionBarContextView;

    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup$MarginLayoutParams;

    iget-object v4, p0, Lg6;->z:Landroidx/appcompat/widget/ActionBarContextView;

    invoke-virtual {v4}, Landroid/view/View;->isShown()Z

    move-result v4

    const/4 v5, 0x1

    if-eqz v4, :cond_e

    iget-object v4, p0, Lg6;->x1:Landroid/graphics/Rect;

    if-nez v4, :cond_2

    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V

    iput-object v4, p0, Lg6;->x1:Landroid/graphics/Rect;

    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V

    iput-object v4, p0, Lg6;->y1:Landroid/graphics/Rect;

    :cond_2
    iget-object v4, p0, Lg6;->x1:Landroid/graphics/Rect;

    iget-object v6, p0, Lg6;->y1:Landroid/graphics/Rect;

    if-nez p1, :cond_3

    invoke-virtual {v4, p2}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Lu62;->i()I

    move-result p2

    invoke-virtual {p1}, Lu62;->k()I

    move-result v7

    invoke-virtual {p1}, Lu62;->j()I

    move-result v8

    invoke-virtual {p1}, Lu62;->h()I

    move-result p1

    invoke-virtual {v4, p2, v7, v8, p1}, Landroid/graphics/Rect;->set(IIII)V

    :goto_1
    iget-object p1, p0, Lg6;->H:Landroid/view/ViewGroup;

    invoke-static {p1, v4, v6}, Lu42;->a(Landroid/view/View;Landroid/graphics/Rect;Landroid/graphics/Rect;)V

    iget p1, v4, Landroid/graphics/Rect;->top:I

    iget p2, v4, Landroid/graphics/Rect;->left:I

    iget v4, v4, Landroid/graphics/Rect;->right:I

    iget-object v6, p0, Lg6;->H:Landroid/view/ViewGroup;

    invoke-static {v6}, Lo32;->H(Landroid/view/View;)Lu62;

    move-result-object v6

    if-nez v6, :cond_4

    move v7, v0

    goto :goto_2

    :cond_4
    invoke-virtual {v6}, Lu62;->i()I

    move-result v7

    :goto_2
    if-nez v6, :cond_5

    move v6, v0

    goto :goto_3

    :cond_5
    invoke-virtual {v6}, Lu62;->j()I

    move-result v6

    :goto_3
    iget v8, v2, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    if-ne v8, p1, :cond_7

    iget v8, v2, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    if-ne v8, p2, :cond_7

    iget v8, v2, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    if-eq v8, v4, :cond_6

    goto :goto_4

    :cond_6
    move p2, v0

    goto :goto_5

    :cond_7
    :goto_4
    iput p1, v2, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    iput p2, v2, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    iput v4, v2, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    move p2, v5

    :goto_5
    if-lez p1, :cond_8

    iget-object p1, p0, Lg6;->J:Landroid/view/View;

    if-nez p1, :cond_8

    new-instance p1, Landroid/view/View;

    iget-object v4, p0, Lg6;->k:Landroid/content/Context;

    invoke-direct {p1, v4}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-object p1, p0, Lg6;->J:Landroid/view/View;

    invoke-virtual {p1, v3}, Landroid/view/View;->setVisibility(I)V

    new-instance p1, Landroid/widget/FrameLayout$LayoutParams;

    iget v4, v2, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    const/16 v8, 0x33

    const/4 v9, -0x1

    invoke-direct {p1, v9, v4, v8}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    iput v7, p1, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    iput v6, p1, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    iget-object v4, p0, Lg6;->H:Landroid/view/ViewGroup;

    iget-object v6, p0, Lg6;->J:Landroid/view/View;

    invoke-virtual {v4, v6, v9, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    goto :goto_6

    :cond_8
    iget-object p1, p0, Lg6;->J:Landroid/view/View;

    if-eqz p1, :cond_a

    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object p1

    check-cast p1, Landroid/view/ViewGroup$MarginLayoutParams;

    iget v4, p1, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    iget v8, v2, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    if-ne v4, v8, :cond_9

    iget v4, p1, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    if-ne v4, v7, :cond_9

    iget v4, p1, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    if-eq v4, v6, :cond_a

    :cond_9
    iput v8, p1, Landroid/view/ViewGroup$MarginLayoutParams;->height:I

    iput v7, p1, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    iput v6, p1, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    iget-object v4, p0, Lg6;->J:Landroid/view/View;

    invoke-virtual {v4, p1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_a
    :goto_6
    iget-object p1, p0, Lg6;->J:Landroid/view/View;

    if-eqz p1, :cond_b

    goto :goto_7

    :cond_b
    move v5, v0

    :goto_7
    if-eqz v5, :cond_c

    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    move-result p1

    if-eqz p1, :cond_c

    iget-object p1, p0, Lg6;->J:Landroid/view/View;

    invoke-virtual {p0, p1}, Lg6;->c1(Landroid/view/View;)V

    :cond_c
    iget-boolean p1, p0, Lg6;->Q:Z

    if-nez p1, :cond_d

    if-eqz v5, :cond_d

    move v1, v0

    :cond_d
    move p1, v5

    move v5, p2

    goto :goto_8

    :cond_e
    iget p1, v2, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    if-eqz p1, :cond_f

    iput v0, v2, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    move p1, v0

    goto :goto_8

    :cond_f
    move p1, v0

    move v5, p1

    :goto_8
    if-eqz v5, :cond_11

    iget-object p2, p0, Lg6;->z:Landroidx/appcompat/widget/ActionBarContextView;

    invoke-virtual {p2, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_9

    :cond_10
    move p1, v0

    :cond_11
    :goto_9
    iget-object p2, p0, Lg6;->J:Landroid/view/View;

    if-eqz p2, :cond_13

    if-eqz p1, :cond_12

    goto :goto_a

    :cond_12
    move v0, v3

    :goto_a
    invoke-virtual {p2, v0}, Landroid/view/View;->setVisibility(I)V

    :cond_13
    return v1
.end method

.method public c(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .locals 2

    .line 1
    invoke-virtual {p0}, Lg6;->g0()V

    iget-object v0, p0, Lg6;->H:Landroid/view/ViewGroup;

    const v1, 0x1020002

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0, p1, p2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    iget-object p1, p0, Lg6;->m:Lg6$n;

    iget-object p2, p0, Lg6;->l:Landroid/view/Window;

    invoke-virtual {p2}, Landroid/view/Window;->getCallback()Landroid/view/Window$Callback;

    move-result-object p2

    invoke-virtual {p1, p2}, Lg6$n;->c(Landroid/view/Window$Callback;)V

    return-void
.end method

.method public c0()V
    .locals 2

    .line 1
    iget-object v0, p0, Lg6;->v:Lcq;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcq;->i()V

    :cond_0
    iget-object v0, p0, Lg6;->A:Landroid/widget/PopupWindow;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lg6;->l:Landroid/view/Window;

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lg6;->C:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/view/View;->removeCallbacks(Ljava/lang/Runnable;)Z

    iget-object v0, p0, Lg6;->A:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    :try_start_0
    iget-object v0, p0, Lg6;->A:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lg6;->A:Landroid/widget/PopupWindow;

    :cond_2
    invoke-virtual {p0}, Lg6;->f0()V

    const/4 v0, 0x0

    invoke-virtual {p0, v0, v0}, Lg6;->p0(IZ)Lg6$t;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, v0, Lg6$t;->j:Landroidx/appcompat/view/menu/e;

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Landroidx/appcompat/view/menu/e;->close()V

    :cond_3
    return-void
.end method

.method public final c1(Landroid/view/View;)V
    .locals 2

    .line 1
    invoke-static {p1}, Lo32;->M(Landroid/view/View;)I

    move-result v0

    and-int/lit16 v0, v0, 0x2000

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    iget-object v0, p0, Lg6;->k:Landroid/content/Context;

    sget v1, Lwa1;->b:I

    goto :goto_1

    :cond_1
    iget-object v0, p0, Lg6;->k:Landroid/content/Context;

    sget v1, Lwa1;->a:I

    :goto_1
    invoke-static {v0, v1}, Lsl;->getColor(Landroid/content/Context;I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/view/View;->setBackgroundColor(I)V

    return-void
.end method

.method public d0(Landroid/view/KeyEvent;)Z
    .locals 3

    .line 1
    iget-object v0, p0, Lg6;->j:Ljava/lang/Object;

    instance-of v1, v0, Lmi0$a;

    const/4 v2, 0x1

    if-nez v1, :cond_0

    instance-of v0, v0, Lq6;

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lg6;->l:Landroid/view/Window;

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-static {v0, p1}, Lmi0;->d(Landroid/view/View;Landroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_1

    return v2

    :cond_1
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    const/16 v1, 0x52

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lg6;->m:Lg6$n;

    iget-object v1, p0, Lg6;->l:Landroid/view/Window;

    invoke-virtual {v1}, Landroid/view/Window;->getCallback()Landroid/view/Window$Callback;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lg6$n;->b(Landroid/view/Window$Callback;Landroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_2

    return v2

    :cond_2
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0

    :cond_3
    const/4 v2, 0x0

    :goto_0
    if-eqz v2, :cond_4

    invoke-virtual {p0, v0, p1}, Lg6;->A0(ILandroid/view/KeyEvent;)Z

    move-result p1

    goto :goto_1

    :cond_4
    invoke-virtual {p0, v0, p1}, Lg6;->D0(ILandroid/view/KeyEvent;)Z

    move-result p1

    :goto_1
    return p1
.end method

.method public e(Landroid/content/Context;)Landroid/content/Context;
    .locals 9

    .line 1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lg6;->k0:Z

    invoke-virtual {p0}, Lg6;->T()I

    move-result v1

    invoke-virtual {p0, p1, v1}, Lg6;->y0(Landroid/content/Context;I)I

    move-result v1

    invoke-static {p1}, Lf6;->s(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {p1}, Lf6;->M(Landroid/content/Context;)V

    :cond_0
    invoke-virtual {p0, p1}, Lg6;->S(Landroid/content/Context;)Lnl0;

    move-result-object v8

    sget-boolean v2, Lg6;->H1:Z

    if-eqz v2, :cond_1

    instance-of v2, p1, Landroid/view/ContextThemeWrapper;

    if-eqz v2, :cond_1

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v2, p0

    move-object v3, p1

    move v4, v1

    move-object v5, v8

    invoke-virtual/range {v2 .. v7}, Lg6;->Z(Landroid/content/Context;ILnl0;Landroid/content/res/Configuration;Z)Landroid/content/res/Configuration;

    move-result-object v2

    :try_start_0
    move-object v3, p1

    check-cast v3, Landroid/view/ContextThemeWrapper;

    invoke-static {v3, v2}, Lg6$r;->a(Landroid/view/ContextThemeWrapper;Landroid/content/res/Configuration;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    :cond_1
    instance-of v2, p1, Lul;

    if-eqz v2, :cond_2

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v2, p0

    move-object v3, p1

    move v4, v1

    move-object v5, v8

    invoke-virtual/range {v2 .. v7}, Lg6;->Z(Landroid/content/Context;ILnl0;Landroid/content/res/Configuration;Z)Landroid/content/res/Configuration;

    move-result-object v2

    :try_start_1
    move-object v3, p1

    check-cast v3, Lul;

    invoke-virtual {v3, v2}, Lul;->a(Landroid/content/res/Configuration;)V
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_1

    return-object p1

    :catch_1
    :cond_2
    sget-boolean v2, Lg6;->G1:Z

    if-nez v2, :cond_3

    invoke-super {p0, p1}, Lf6;->e(Landroid/content/Context;)Landroid/content/Context;

    move-result-object p1

    return-object p1

    :cond_3
    new-instance v2, Landroid/content/res/Configuration;

    invoke-direct {v2}, Landroid/content/res/Configuration;-><init>()V

    const/4 v3, -0x1

    iput v3, v2, Landroid/content/res/Configuration;->uiMode:I

    const/4 v3, 0x0

    iput v3, v2, Landroid/content/res/Configuration;->fontScale:F

    invoke-static {p1, v2}, Lg6$i;->a(Landroid/content/Context;Landroid/content/res/Configuration;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget v4, v3, Landroid/content/res/Configuration;->uiMode:I

    iput v4, v2, Landroid/content/res/Configuration;->uiMode:I

    invoke-virtual {v2, v3}, Landroid/content/res/Configuration;->equals(Landroid/content/res/Configuration;)Z

    move-result v4

    if-nez v4, :cond_4

    invoke-static {v2, v3}, Lg6;->j0(Landroid/content/res/Configuration;Landroid/content/res/Configuration;)Landroid/content/res/Configuration;

    move-result-object v2

    goto :goto_0

    :cond_4
    const/4 v2, 0x0

    :goto_0
    move-object v6, v2

    const/4 v7, 0x1

    move-object v2, p0

    move-object v3, p1

    move v4, v1

    move-object v5, v8

    invoke-virtual/range {v2 .. v7}, Lg6;->Z(Landroid/content/Context;ILnl0;Landroid/content/res/Configuration;Z)Landroid/content/res/Configuration;

    move-result-object v1

    new-instance v2, Lul;

    sget v3, Ltb1;->e:I

    invoke-direct {v2, p1, v3}, Lul;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v2, v1}, Lul;->a(Landroid/content/res/Configuration;)V

    const/4 v1, 0x0

    :try_start_2
    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object p1
    :try_end_2
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_2

    if-eqz p1, :cond_5

    goto :goto_1

    :cond_5
    move v0, v1

    :goto_1
    move v1, v0

    :catch_2
    if-eqz v1, :cond_6

    invoke-virtual {v2}, Lul;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object p1

    invoke-static {p1}, Lle1$f;->a(Landroid/content/res/Resources$Theme;)V

    :cond_6
    invoke-super {p0, v2}, Lf6;->e(Landroid/content/Context;)Landroid/content/Context;

    move-result-object p1

    return-object p1
.end method

.method public e0(I)V
    .locals 4

    .line 1
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lg6;->p0(IZ)Lg6$t;

    move-result-object v1

    iget-object v2, v1, Lg6$t;->j:Landroidx/appcompat/view/menu/e;

    if-eqz v2, :cond_1

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    iget-object v3, v1, Lg6$t;->j:Landroidx/appcompat/view/menu/e;

    invoke-virtual {v3, v2}, Landroidx/appcompat/view/menu/e;->saveActionViewStates(Landroid/os/Bundle;)V

    invoke-virtual {v2}, Landroid/os/BaseBundle;->size()I

    move-result v3

    if-lez v3, :cond_0

    iput-object v2, v1, Lg6$t;->s:Landroid/os/Bundle;

    :cond_0
    iget-object v2, v1, Lg6$t;->j:Landroidx/appcompat/view/menu/e;

    invoke-virtual {v2}, Landroidx/appcompat/view/menu/e;->stopDispatchingItemsChanged()V

    iget-object v2, v1, Lg6$t;->j:Landroidx/appcompat/view/menu/e;

    invoke-virtual {v2}, Landroidx/appcompat/view/menu/e;->clear()V

    :cond_1
    iput-boolean v0, v1, Lg6$t;->r:Z

    iput-boolean v0, v1, Lg6$t;->q:Z

    const/16 v0, 0x6c

    if-eq p1, v0, :cond_2

    if-nez p1, :cond_3

    :cond_2
    iget-object p1, p0, Lg6;->v:Lcq;

    if-eqz p1, :cond_3

    const/4 p1, 0x0

    invoke-virtual {p0, p1, p1}, Lg6;->p0(IZ)Lg6$t;

    move-result-object v0

    if-eqz v0, :cond_3

    iput-boolean p1, v0, Lg6$t;->m:Z

    const/4 p1, 0x0

    invoke-virtual {p0, v0, p1}, Lg6;->L0(Lg6$t;Landroid/view/KeyEvent;)Z

    :cond_3
    return-void
.end method

.method public f0()V
    .locals 1

    .line 1
    iget-object v0, p0, Lg6;->D:Li42;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Li42;->c()V

    :cond_0
    return-void
.end method

.method public final g0()V
    .locals 2

    .line 1
    iget-boolean v0, p0, Lg6;->G:Z

    if-nez v0, :cond_4

    invoke-virtual {p0}, Lg6;->a0()Landroid/view/ViewGroup;

    move-result-object v0

    iput-object v0, p0, Lg6;->H:Landroid/view/ViewGroup;

    invoke-virtual {p0}, Lg6;->q0()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lg6;->v:Lcq;

    if-eqz v1, :cond_0

    invoke-interface {v1, v0}, Lcq;->setWindowTitle(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lg6;->J0()Lt1;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lg6;->J0()Lt1;

    move-result-object v1

    invoke-virtual {v1, v0}, Lt1;->u(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lg6;->I:Landroid/widget/TextView;

    if-eqz v1, :cond_2

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_2
    :goto_0
    invoke-virtual {p0}, Lg6;->Q()V

    iget-object v0, p0, Lg6;->H:Landroid/view/ViewGroup;

    invoke-virtual {p0, v0}, Lg6;->H0(Landroid/view/ViewGroup;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lg6;->G:Z

    const/4 v0, 0x0

    invoke-virtual {p0, v0, v0}, Lg6;->p0(IZ)Lg6$t;

    move-result-object v0

    iget-boolean v1, p0, Lg6;->C0:Z

    if-nez v1, :cond_4

    if-eqz v0, :cond_3

    iget-object v0, v0, Lg6$t;->j:Landroidx/appcompat/view/menu/e;

    if-nez v0, :cond_4

    :cond_3
    const/16 v0, 0x6c

    invoke-virtual {p0, v0}, Lg6;->w0(I)V

    :cond_4
    return-void
.end method

.method public h(I)Landroid/view/View;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lg6;->g0()V

    iget-object v0, p0, Lg6;->l:Landroid/view/Window;

    invoke-virtual {v0, p1}, Landroid/view/Window;->findViewById(I)Landroid/view/View;

    move-result-object p1

    return-object p1
.end method

.method public final h0()V
    .locals 2

    .line 1
    iget-object v0, p0, Lg6;->l:Landroid/view/Window;

    if-nez v0, :cond_0

    iget-object v0, p0, Lg6;->j:Ljava/lang/Object;

    instance-of v1, v0, Landroid/app/Activity;

    if-eqz v1, :cond_0

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {p0, v0}, Lg6;->R(Landroid/view/Window;)V

    :cond_0
    iget-object v0, p0, Lg6;->l:Landroid/view/Window;

    if-eqz v0, :cond_1

    return-void

    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "We have not been given a Window"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public i0(Landroid/view/Menu;)Lg6$t;
    .locals 5

    .line 1
    iget-object v0, p0, Lg6;->Y:[Lg6$t;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    array-length v2, v0

    goto :goto_0

    :cond_0
    move v2, v1

    :goto_0
    if-ge v1, v2, :cond_2

    aget-object v3, v0, v1

    if-eqz v3, :cond_1

    iget-object v4, v3, Lg6$t;->j:Landroidx/appcompat/view/menu/e;

    if-ne v4, p1, :cond_1

    return-object v3

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    const/4 p1, 0x0

    return-object p1
.end method

.method public j()Landroid/content/Context;
    .locals 1

    .line 1
    iget-object v0, p0, Lg6;->k:Landroid/content/Context;

    return-object v0
.end method

.method public final k0()Landroid/content/Context;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lg6;->p()Lt1;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lt1;->j()Landroid/content/Context;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-nez v0, :cond_1

    iget-object v0, p0, Lg6;->k:Landroid/content/Context;

    :cond_1
    return-object v0
.end method

.method public l()I
    .locals 1

    .line 1
    iget v0, p0, Lg6;->c1:I

    return v0
.end method

.method public final l0(Landroid/content/Context;)I
    .locals 5

    .line 1
    iget-boolean v0, p0, Lg6;->k1:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lg6;->j:Ljava/lang/Object;

    instance-of v0, v0, Landroid/app/Activity;

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    :cond_0
    :try_start_0
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x1d

    if-lt v2, v3, :cond_1

    const/high16 v2, 0x100c0000

    goto :goto_0

    :cond_1
    const/high16 v2, 0xc0000

    :goto_0
    new-instance v3, Landroid/content/ComponentName;

    iget-object v4, p0, Lg6;->j:Ljava/lang/Object;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-direct {v3, p1, v4}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v3, v2}, Landroid/content/pm/PackageManager;->getActivityInfo(Landroid/content/ComponentName;I)Landroid/content/pm/ActivityInfo;

    move-result-object p1

    if-eqz p1, :cond_2

    iget p1, p1, Landroid/content/pm/ActivityInfo;->configChanges:I

    iput p1, p0, Lg6;->g1:I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception p1

    const-string v0, "AppCompatDelegate"

    const-string v2, "Exception while getting ActivityInfo"

    invoke-static {v0, v2, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    iput v1, p0, Lg6;->g1:I

    :cond_2
    :goto_1
    const/4 p1, 0x1

    iput-boolean p1, p0, Lg6;->k1:Z

    iget p1, p0, Lg6;->g1:I

    return p1
.end method

.method public final m0(Landroid/content/Context;)Lg6$p;
    .locals 1

    .line 1
    iget-object v0, p0, Lg6;->s1:Lg6$p;

    if-nez v0, :cond_0

    new-instance v0, Lg6$o;

    invoke-direct {v0, p0, p1}, Lg6$o;-><init>(Lg6;Landroid/content/Context;)V

    iput-object v0, p0, Lg6;->s1:Lg6$p;

    :cond_0
    iget-object p1, p0, Lg6;->s1:Lg6$p;

    return-object p1
.end method

.method public n()Landroid/view/MenuInflater;
    .locals 2

    .line 1
    iget-object v0, p0, Lg6;->q:Landroid/view/MenuInflater;

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lg6;->s0()V

    new-instance v0, Lls1;

    iget-object v1, p0, Lg6;->p:Lt1;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lt1;->j()Landroid/content/Context;

    move-result-object v1

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lg6;->k:Landroid/content/Context;

    :goto_0
    invoke-direct {v0, v1}, Lls1;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lg6;->q:Landroid/view/MenuInflater;

    :cond_1
    iget-object v0, p0, Lg6;->q:Landroid/view/MenuInflater;

    return-object v0
.end method

.method public final n0(Landroid/content/Context;)Lg6$p;
    .locals 1

    .line 1
    iget-object v0, p0, Lg6;->p1:Lg6$p;

    if-nez v0, :cond_0

    new-instance v0, Lg6$q;

    invoke-static {p1}, Lfz1;->a(Landroid/content/Context;)Lfz1;

    move-result-object p1

    invoke-direct {v0, p0, p1}, Lg6$q;-><init>(Lg6;Lfz1;)V

    iput-object v0, p0, Lg6;->p1:Lg6$p;

    :cond_0
    iget-object p1, p0, Lg6;->p1:Lg6$p;

    return-object p1
.end method

.method public o0(Landroid/content/res/Configuration;)Lnl0;
    .locals 0

    .line 1
    invoke-static {p1}, Lg6$k;->b(Landroid/content/res/Configuration;)Lnl0;

    move-result-object p1

    return-object p1
.end method

.method public final onCreateView(Landroid/view/View;Ljava/lang/String;Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/view/View;
    .locals 0

    .line 1
    invoke-virtual {p0, p1, p2, p3, p4}, Lg6;->b0(Landroid/view/View;Ljava/lang/String;Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/view/View;

    move-result-object p1

    return-object p1
.end method

.method public onCreateView(Ljava/lang/String;Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/view/View;
    .locals 1

    .line 2
    const/4 v0, 0x0

    invoke-virtual {p0, v0, p1, p2, p3}, Lg6;->onCreateView(Landroid/view/View;Ljava/lang/String;Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/view/View;

    move-result-object p1

    return-object p1
.end method

.method public onMenuItemSelected(Landroidx/appcompat/view/menu/e;Landroid/view/MenuItem;)Z
    .locals 2

    .line 1
    invoke-virtual {p0}, Lg6;->r0()Landroid/view/Window$Callback;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-boolean v1, p0, Lg6;->C0:Z

    if-nez v1, :cond_0

    invoke-virtual {p1}, Landroidx/appcompat/view/menu/e;->getRootMenu()Landroidx/appcompat/view/menu/e;

    move-result-object p1

    invoke-virtual {p0, p1}, Lg6;->i0(Landroid/view/Menu;)Lg6$t;

    move-result-object p1

    if-eqz p1, :cond_0

    iget p1, p1, Lg6$t;->a:I

    invoke-interface {v0, p1, p2}, Landroid/view/Window$Callback;->onMenuItemSelected(ILandroid/view/MenuItem;)Z

    move-result p1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method public onMenuModeChange(Landroidx/appcompat/view/menu/e;)V
    .locals 0

    .line 1
    const/4 p1, 0x1

    invoke-virtual {p0, p1}, Lg6;->M0(Z)V

    return-void
.end method

.method public p()Lt1;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lg6;->s0()V

    iget-object v0, p0, Lg6;->p:Lt1;

    return-object v0
.end method

.method public p0(IZ)Lg6$t;
    .locals 3

    .line 1
    iget-object p2, p0, Lg6;->Y:[Lg6$t;

    if-eqz p2, :cond_0

    array-length v0, p2

    if-gt v0, p1, :cond_2

    :cond_0
    add-int/lit8 v0, p1, 0x1

    new-array v0, v0, [Lg6$t;

    if-eqz p2, :cond_1

    array-length v1, p2

    const/4 v2, 0x0

    invoke-static {p2, v2, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    iput-object v0, p0, Lg6;->Y:[Lg6$t;

    move-object p2, v0

    :cond_2
    aget-object v0, p2, p1

    if-nez v0, :cond_3

    new-instance v0, Lg6$t;

    invoke-direct {v0, p1}, Lg6$t;-><init>(I)V

    aput-object v0, p2, p1

    :cond_3
    return-object v0
.end method

.method public q()V
    .locals 2

    .line 1
    iget-object v0, p0, Lg6;->k:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/LayoutInflater;->getFactory()Landroid/view/LayoutInflater$Factory;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-static {v0, p0}, Lwi0;->a(Landroid/view/LayoutInflater;Landroid/view/LayoutInflater$Factory2;)V

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Landroid/view/LayoutInflater;->getFactory2()Landroid/view/LayoutInflater$Factory2;

    move-result-object v0

    instance-of v0, v0, Lg6;

    if-nez v0, :cond_1

    const-string v0, "AppCompatDelegate"

    const-string v1, "The Activity\'s LayoutInflater already has a Factory installed so we can not install AppCompat\'s"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    :goto_0
    return-void
.end method

.method public final q0()Ljava/lang/CharSequence;
    .locals 2

    .line 1
    iget-object v0, p0, Lg6;->j:Ljava/lang/Object;

    instance-of v1, v0, Landroid/app/Activity;

    if-eqz v1, :cond_0

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getTitle()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0

    :cond_0
    iget-object v0, p0, Lg6;->t:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public r()V
    .locals 1

    .line 1
    invoke-virtual {p0}, Lg6;->J0()Lt1;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lg6;->p()Lt1;

    move-result-object v0

    invoke-virtual {v0}, Lt1;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lg6;->w0(I)V

    :cond_1
    :goto_0
    return-void
.end method

.method public final r0()Landroid/view/Window$Callback;
    .locals 1

    .line 1
    iget-object v0, p0, Lg6;->l:Landroid/view/Window;

    invoke-virtual {v0}, Landroid/view/Window;->getCallback()Landroid/view/Window$Callback;

    move-result-object v0

    return-object v0
.end method

.method public final s0()V
    .locals 3

    .line 1
    invoke-virtual {p0}, Lg6;->g0()V

    iget-boolean v0, p0, Lg6;->O:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lg6;->p:Lt1;

    if-eqz v0, :cond_0

    goto :goto_2

    :cond_0
    iget-object v0, p0, Lg6;->j:Ljava/lang/Object;

    instance-of v1, v0, Landroid/app/Activity;

    if-eqz v1, :cond_1

    new-instance v0, Le62;

    iget-object v1, p0, Lg6;->j:Ljava/lang/Object;

    check-cast v1, Landroid/app/Activity;

    iget-boolean v2, p0, Lg6;->P:Z

    invoke-direct {v0, v1, v2}, Le62;-><init>(Landroid/app/Activity;Z)V

    :goto_0
    iput-object v0, p0, Lg6;->p:Lt1;

    goto :goto_1

    :cond_1
    instance-of v0, v0, Landroid/app/Dialog;

    if-eqz v0, :cond_2

    new-instance v0, Le62;

    iget-object v1, p0, Lg6;->j:Ljava/lang/Object;

    check-cast v1, Landroid/app/Dialog;

    invoke-direct {v0, v1}, Le62;-><init>(Landroid/app/Dialog;)V

    goto :goto_0

    :cond_2
    :goto_1
    iget-object v0, p0, Lg6;->p:Lt1;

    if-eqz v0, :cond_3

    iget-boolean v1, p0, Lg6;->w1:Z

    invoke-virtual {v0, v1}, Lt1;->q(Z)V

    :cond_3
    :goto_2
    return-void
.end method

.method public final t0(Lg6$t;)Z
    .locals 3

    .line 1
    iget-object v0, p1, Lg6$t;->i:Landroid/view/View;

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    iput-object v0, p1, Lg6$t;->h:Landroid/view/View;

    return v1

    :cond_0
    iget-object v0, p1, Lg6$t;->j:Landroidx/appcompat/view/menu/e;

    const/4 v2, 0x0

    if-nez v0, :cond_1

    return v2

    :cond_1
    iget-object v0, p0, Lg6;->x:Lg6$u;

    if-nez v0, :cond_2

    new-instance v0, Lg6$u;

    invoke-direct {v0, p0}, Lg6$u;-><init>(Lg6;)V

    iput-object v0, p0, Lg6;->x:Lg6$u;

    :cond_2
    iget-object v0, p0, Lg6;->x:Lg6$u;

    invoke-virtual {p1, v0}, Lg6$t;->a(Landroidx/appcompat/view/menu/i$a;)Landroidx/appcompat/view/menu/j;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iput-object v0, p1, Lg6$t;->h:Landroid/view/View;

    if-eqz v0, :cond_3

    goto :goto_0

    :cond_3
    move v1, v2

    :goto_0
    return v1
.end method

.method public u(Landroid/content/res/Configuration;)V
    .locals 1

    .line 1
    iget-boolean v0, p0, Lg6;->O:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lg6;->G:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lg6;->p()Lt1;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lt1;->l(Landroid/content/res/Configuration;)V

    :cond_0
    invoke-static {}, Ls6;->b()Ls6;

    move-result-object p1

    iget-object v0, p0, Lg6;->k:Landroid/content/Context;

    invoke-virtual {p1, v0}, Ls6;->g(Landroid/content/Context;)V

    new-instance p1, Landroid/content/res/Configuration;

    iget-object v0, p0, Lg6;->k:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    invoke-direct {p1, v0}, Landroid/content/res/Configuration;-><init>(Landroid/content/res/Configuration;)V

    iput-object p1, p0, Lg6;->b1:Landroid/content/res/Configuration;

    const/4 p1, 0x0

    invoke-virtual {p0, p1, p1}, Lg6;->O(ZZ)Z

    return-void
.end method

.method public final u0(Lg6$t;)Z
    .locals 2

    .line 1
    invoke-virtual {p0}, Lg6;->k0()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p1, v0}, Lg6$t;->d(Landroid/content/Context;)V

    new-instance v0, Lg6$s;

    iget-object v1, p1, Lg6$t;->l:Landroid/content/Context;

    invoke-direct {v0, p0, v1}, Lg6$s;-><init>(Lg6;Landroid/content/Context;)V

    iput-object v0, p1, Lg6$t;->g:Landroid/view/ViewGroup;

    const/16 v0, 0x51

    iput v0, p1, Lg6$t;->c:I

    const/4 p1, 0x1

    return p1
.end method

.method public v(Landroid/os/Bundle;)V
    .locals 2

    .line 1
    const/4 p1, 0x1

    iput-boolean p1, p0, Lg6;->k0:Z

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lg6;->N(Z)Z

    invoke-virtual {p0}, Lg6;->h0()V

    iget-object v0, p0, Lg6;->j:Ljava/lang/Object;

    instance-of v1, v0, Landroid/app/Activity;

    if-eqz v1, :cond_2

    :try_start_0
    check-cast v0, Landroid/app/Activity;

    invoke-static {v0}, Lhy0;->c(Landroid/app/Activity;)Ljava/lang/String;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lg6;->J0()Lt1;

    move-result-object v0

    if-nez v0, :cond_0

    iput-boolean p1, p0, Lg6;->w1:Z

    goto :goto_1

    :cond_0
    invoke-virtual {v0, p1}, Lt1;->q(Z)V

    :cond_1
    :goto_1
    invoke-static {p0}, Lf6;->b(Lf6;)V

    :cond_2
    new-instance v0, Landroid/content/res/Configuration;

    iget-object v1, p0, Lg6;->k:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/content/res/Configuration;-><init>(Landroid/content/res/Configuration;)V

    iput-object v0, p0, Lg6;->b1:Landroid/content/res/Configuration;

    iput-boolean p1, p0, Lg6;->A0:Z

    return-void
.end method

.method public final v0(Lg6$t;)Z
    .locals 6

    .line 1
    iget-object v0, p0, Lg6;->k:Landroid/content/Context;

    iget v1, p1, Lg6$t;->a:I

    const/4 v2, 0x1

    if-eqz v1, :cond_0

    const/16 v3, 0x6c

    if-ne v1, v3, :cond_4

    :cond_0
    iget-object v1, p0, Lg6;->v:Lcq;

    if-eqz v1, :cond_4

    new-instance v1, Landroid/util/TypedValue;

    invoke-direct {v1}, Landroid/util/TypedValue;-><init>()V

    invoke-virtual {v0}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v3

    sget v4, Lsa1;->d:I

    invoke-virtual {v3, v4, v1, v2}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    iget v4, v1, Landroid/util/TypedValue;->resourceId:I

    if-eqz v4, :cond_1

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->newTheme()Landroid/content/res/Resources$Theme;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/content/res/Resources$Theme;->setTo(Landroid/content/res/Resources$Theme;)V

    iget v5, v1, Landroid/util/TypedValue;->resourceId:I

    invoke-virtual {v4, v5, v2}, Landroid/content/res/Resources$Theme;->applyStyle(IZ)V

    sget v5, Lsa1;->e:I

    invoke-virtual {v4, v5, v1, v2}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    goto :goto_0

    :cond_1
    sget v4, Lsa1;->e:I

    invoke-virtual {v3, v4, v1, v2}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    const/4 v4, 0x0

    :goto_0
    iget v5, v1, Landroid/util/TypedValue;->resourceId:I

    if-eqz v5, :cond_3

    if-nez v4, :cond_2

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->newTheme()Landroid/content/res/Resources$Theme;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/content/res/Resources$Theme;->setTo(Landroid/content/res/Resources$Theme;)V

    :cond_2
    iget v1, v1, Landroid/util/TypedValue;->resourceId:I

    invoke-virtual {v4, v1, v2}, Landroid/content/res/Resources$Theme;->applyStyle(IZ)V

    :cond_3
    if-eqz v4, :cond_4

    new-instance v1, Lul;

    const/4 v3, 0x0

    invoke-direct {v1, v0, v3}, Lul;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/content/res/Resources$Theme;->setTo(Landroid/content/res/Resources$Theme;)V

    move-object v0, v1

    :cond_4
    new-instance v1, Landroidx/appcompat/view/menu/e;

    invoke-direct {v1, v0}, Landroidx/appcompat/view/menu/e;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, p0}, Landroidx/appcompat/view/menu/e;->setCallback(Landroidx/appcompat/view/menu/e$a;)V

    invoke-virtual {p1, v1}, Lg6$t;->c(Landroidx/appcompat/view/menu/e;)V

    return v2
.end method

.method public w()V
    .locals 3

    .line 1
    iget-object v0, p0, Lg6;->j:Ljava/lang/Object;

    instance-of v0, v0, Landroid/app/Activity;

    if-eqz v0, :cond_0

    invoke-static {p0}, Lf6;->C(Lf6;)V

    :cond_0
    iget-boolean v0, p0, Lg6;->t1:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lg6;->l:Landroid/view/Window;

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lg6;->v1:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/view/View;->removeCallbacks(Ljava/lang/Runnable;)Z

    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lg6;->C0:Z

    iget v0, p0, Lg6;->c1:I

    const/16 v1, -0x64

    if-eq v0, v1, :cond_2

    iget-object v0, p0, Lg6;->j:Ljava/lang/Object;

    instance-of v1, v0, Landroid/app/Activity;

    if-eqz v1, :cond_2

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->isChangingConfigurations()Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lg6;->D1:Lco1;

    iget-object v1, p0, Lg6;->j:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    iget v2, p0, Lg6;->c1:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lco1;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_2
    sget-object v0, Lg6;->D1:Lco1;

    iget-object v1, p0, Lg6;->j:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lco1;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :goto_0
    iget-object v0, p0, Lg6;->p:Lt1;

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lt1;->m()V

    :cond_3
    invoke-virtual {p0}, Lg6;->W()V

    return-void
.end method

.method public final w0(I)V
    .locals 2

    .line 1
    iget v0, p0, Lg6;->u1:I

    const/4 v1, 0x1

    shl-int p1, v1, p1

    or-int/2addr p1, v0

    iput p1, p0, Lg6;->u1:I

    iget-boolean p1, p0, Lg6;->t1:Z

    if-nez p1, :cond_0

    iget-object p1, p0, Lg6;->l:Landroid/view/Window;

    invoke-virtual {p1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object p1

    iget-object v0, p0, Lg6;->v1:Ljava/lang/Runnable;

    invoke-static {p1, v0}, Lo32;->i0(Landroid/view/View;Ljava/lang/Runnable;)V

    iput-boolean v1, p0, Lg6;->t1:Z

    :cond_0
    return-void
.end method

.method public x(Landroid/os/Bundle;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lg6;->g0()V

    return-void
.end method

.method public x0()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lg6;->F:Z

    return v0
.end method

.method public y()V
    .locals 2

    .line 1
    invoke-virtual {p0}, Lg6;->p()Lt1;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lt1;->r(Z)V

    :cond_0
    return-void
.end method

.method public y0(Landroid/content/Context;I)I
    .locals 2

    .line 1
    const/16 v0, -0x64

    const/4 v1, -0x1

    if-eq p2, v0, :cond_4

    if-eq p2, v1, :cond_3

    if-eqz p2, :cond_1

    const/4 v0, 0x1

    if-eq p2, v0, :cond_3

    const/4 v0, 0x2

    if-eq p2, v0, :cond_3

    const/4 v0, 0x3

    if-ne p2, v0, :cond_0

    invoke-virtual {p0, p1}, Lg6;->m0(Landroid/content/Context;)Lg6$p;

    move-result-object p1

    :goto_0
    invoke-virtual {p1}, Lg6$p;->c()I

    move-result p1

    return p1

    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Unknown value set for night mode. Please use one of the MODE_NIGHT values from AppCompatDelegate."

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p2

    const-string v0, "uimode"

    invoke-virtual {p2, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Landroid/app/UiModeManager;

    invoke-virtual {p2}, Landroid/app/UiModeManager;->getNightMode()I

    move-result p2

    if-nez p2, :cond_2

    return v1

    :cond_2
    invoke-virtual {p0, p1}, Lg6;->n0(Landroid/content/Context;)Lg6$p;

    move-result-object p1

    goto :goto_0

    :cond_3
    return p2

    :cond_4
    return v1
.end method

.method public z(Landroid/os/Bundle;)V
    .locals 0

    .line 1
    return-void
.end method

.method public z0()Z
    .locals 5

    .line 1
    iget-boolean v0, p0, Lg6;->c0:Z

    const/4 v1, 0x0

    iput-boolean v1, p0, Lg6;->c0:Z

    invoke-virtual {p0, v1, v1}, Lg6;->p0(IZ)Lg6$t;

    move-result-object v2

    const/4 v3, 0x1

    if-eqz v2, :cond_1

    iget-boolean v4, v2, Lg6$t;->o:Z

    if-eqz v4, :cond_1

    if-nez v0, :cond_0

    invoke-virtual {p0, v2, v3}, Lg6;->Y(Lg6$t;Z)V

    :cond_0
    return v3

    :cond_1
    iget-object v0, p0, Lg6;->y:Lc2;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lc2;->a()V

    return v3

    :cond_2
    invoke-virtual {p0}, Lg6;->p()Lt1;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lt1;->g()Z

    move-result v0

    if-eqz v0, :cond_3

    return v3

    :cond_3
    return v1
.end method

.class public Lwd1;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:Ljava/util/ArrayList;

.field public b:I

.field public c:I

.field public d:I

.field public e:[I

.field public f:I

.field public g:I

.field public h:I

.field public i:Landroid/graphics/Canvas;

.field public j:Landroid/graphics/Paint;

.field public k:Landroid/graphics/Paint;

.field public l:Landroid/graphics/Paint;

.field public m:Landroid/graphics/Paint;

.field public n:Landroid/graphics/Paint;

.field public o:Ljava/lang/String;

.field public p:Ljava/lang/String;

.field public q:Ljava/lang/String;

.field public r:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 7

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lwd1;->c:I

    const/4 v1, 0x0

    iput v1, p0, Lwd1;->d:I

    const/4 v2, 0x2

    new-array v2, v2, [I

    iput-object v2, p0, Lwd1;->e:[I

    const/16 v2, 0x4b0

    iput v2, p0, Lwd1;->f:I

    const/16 v2, 0x708

    iput v2, p0, Lwd1;->g:I

    const/16 v2, 0x64

    iput v2, p0, Lwd1;->h:I

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lwd1;->a:Ljava/util/ArrayList;

    iget v3, p0, Lwd1;->f:I

    iget v4, p0, Lwd1;->c:I

    mul-int/2addr v3, v4

    iget v5, p0, Lwd1;->g:I

    mul-int/2addr v5, v4

    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v3, v5, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v3

    new-instance v4, Landroid/graphics/Canvas;

    invoke-direct {v4, v3}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v4, p0, Lwd1;->i:Landroid/graphics/Canvas;

    const/16 v5, 0xff

    invoke-virtual {v4, v5, v5, v5, v5}, Landroid/graphics/Canvas;->drawARGB(IIII)V

    iget-object v4, p0, Lwd1;->a:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3}, Landroid/graphics/Paint;-><init>()V

    iput-object v3, p0, Lwd1;->j:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v3, p0, Lwd1;->j:Landroid/graphics/Paint;

    invoke-virtual {v3, v0}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget-object v3, p0, Lwd1;->j:Landroid/graphics/Paint;

    const/high16 v4, -0x1000000

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v3, p0, Lwd1;->j:Landroid/graphics/Paint;

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    iget-object v3, p0, Lwd1;->j:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    iget-object v3, p0, Lwd1;->e:[I

    iget v4, p0, Lwd1;->c:I

    mul-int/lit8 v6, v4, 0x64

    aput v6, v3, v1

    mul-int/2addr v4, v2

    aput v4, v3, v0

    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lwd1;->k:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v1, p0, Lwd1;->k:Landroid/graphics/Paint;

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget-object v1, p0, Lwd1;->k:Landroid/graphics/Paint;

    const/16 v2, 0xc8

    invoke-static {v2, v5, v2}, Landroid/graphics/Color;->rgb(III)I

    move-result v3

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setColor(I)V

    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lwd1;->l:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v1, p0, Lwd1;->l:Landroid/graphics/Paint;

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget-object v1, p0, Lwd1;->l:Landroid/graphics/Paint;

    invoke-static {v5, v2, v2}, Landroid/graphics/Color;->rgb(III)I

    move-result v3

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setColor(I)V

    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lwd1;->m:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v1, p0, Lwd1;->m:Landroid/graphics/Paint;

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget-object v1, p0, Lwd1;->m:Landroid/graphics/Paint;

    invoke-static {v5, v5, v2}, Landroid/graphics/Color;->rgb(III)I

    move-result v3

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setColor(I)V

    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lwd1;->n:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v1, p0, Lwd1;->n:Landroid/graphics/Paint;

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget-object v0, p0, Lwd1;->n:Landroid/graphics/Paint;

    invoke-static {v2, v2, v5}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 8

    .line 1
    iget v0, p0, Lwd1;->c:I

    mul-int/lit8 v0, v0, 0x28

    iget-object v1, p0, Lwd1;->j:Landroid/graphics/Paint;

    int-to-float v2, v0

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setTextSize(F)V

    iget v1, p0, Lwd1;->f:I

    div-int/lit8 v1, v1, 0x2

    iget v2, p0, Lwd1;->c:I

    mul-int/2addr v1, v2

    iget-object v2, p0, Lwd1;->j:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    iget-object v2, p0, Lwd1;->j:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    iget-object v2, p0, Lwd1;->i:Landroid/graphics/Canvas;

    int-to-float v3, v1

    iget-object v4, p0, Lwd1;->e:[I

    const/4 v5, 0x0

    aget v4, v4, v5

    div-int/lit8 v0, v0, 0x2

    add-int/2addr v4, v0

    add-int/lit8 v4, v4, -0x2

    int-to-float v4, v4

    iget-object v6, p0, Lwd1;->j:Landroid/graphics/Paint;

    const-string v7, " - "

    invoke-virtual {v2, v7, v3, v4, v6}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    iget-object v2, p0, Lwd1;->j:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Paint$Align;->RIGHT:Landroid/graphics/Paint$Align;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    iget-object v2, p0, Lwd1;->i:Landroid/graphics/Canvas;

    add-int/lit8 v3, v1, -0x1e

    int-to-float v3, v3

    iget-object v4, p0, Lwd1;->e:[I

    aget v4, v4, v5

    add-int/2addr v4, v0

    add-int/lit8 v4, v4, -0x2

    int-to-float v4, v4

    iget-object v6, p0, Lwd1;->j:Landroid/graphics/Paint;

    invoke-virtual {v2, p1, v3, v4, v6}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    iget-object p1, p0, Lwd1;->j:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    invoke-virtual {p1, v2}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    iget-object p1, p0, Lwd1;->i:Landroid/graphics/Canvas;

    add-int/lit8 v1, v1, 0x1e

    int-to-float v1, v1

    iget-object v2, p0, Lwd1;->e:[I

    aget v2, v2, v5

    add-int/2addr v2, v0

    add-int/lit8 v2, v2, -0x2

    int-to-float v0, v2

    iget-object v2, p0, Lwd1;->j:Landroid/graphics/Paint;

    invoke-virtual {p1, p2, v1, v0, v2}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    iget-object p1, p0, Lwd1;->e:[I

    aget p2, p1, v5

    iget v0, p0, Lwd1;->c:I

    mul-int/lit8 v1, v0, 0x50

    add-int/2addr p2, v1

    aput p2, p1, v5

    const/4 p2, 0x1

    aget v1, p1, p2

    mul-int/lit8 v0, v0, 0x50

    add-int/2addr v1, v0

    aput v1, p1, p2

    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 5

    .line 1
    iget v0, p0, Lwd1;->c:I

    mul-int/lit8 v0, v0, 0x14

    iget-object v1, p0, Lwd1;->j:Landroid/graphics/Paint;

    int-to-float v2, v0

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setTextSize(F)V

    iget-object v1, p0, Lwd1;->j:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    iget-object v1, p0, Lwd1;->j:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    iget-object v1, p0, Lwd1;->i:Landroid/graphics/Canvas;

    iget v2, p0, Lwd1;->h:I

    int-to-float v2, v2

    iget-object v3, p0, Lwd1;->e:[I

    const/4 v4, 0x0

    aget v3, v3, v4

    div-int/lit8 v0, v0, 0x2

    add-int/2addr v3, v0

    add-int/lit8 v3, v3, -0x2

    int-to-float v0, v3

    iget-object v3, p0, Lwd1;->j:Landroid/graphics/Paint;

    invoke-virtual {v1, p1, v2, v0, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    iget-object p1, p0, Lwd1;->e:[I

    aget v0, p1, v4

    iget v1, p0, Lwd1;->c:I

    mul-int/lit8 v2, v1, 0x1e

    add-int/2addr v0, v2

    aput v0, p1, v4

    const/4 v0, 0x1

    aget v2, p1, v0

    mul-int/lit8 v1, v1, 0x1e

    add-int/2addr v2, v1

    aput v2, p1, v0

    return-void
.end method

.method public c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 13

    .line 1
    move-object v7, p0

    new-instance v8, Landroid/graphics/Paint;

    invoke-direct {v8}, Landroid/graphics/Paint;-><init>()V

    sget-object v0, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v8, v0}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    const/4 v9, 0x1

    invoke-virtual {v8, v9}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    const/high16 v0, -0x1000000

    invoke-virtual {v8, v0}, Landroid/graphics/Paint;->setColor(I)V

    iget v0, v7, Lwd1;->c:I

    mul-int/lit8 v0, v0, 0x14

    iget-object v1, v7, Lwd1;->j:Landroid/graphics/Paint;

    int-to-float v2, v0

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setTextSize(F)V

    iget v1, v7, Lwd1;->c:I

    mul-int/lit16 v10, v1, 0xc8

    iget-object v1, v7, Lwd1;->j:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    iget-object v1, v7, Lwd1;->j:Landroid/graphics/Paint;

    if-eqz p6, :cond_0

    sget-object v2, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    goto :goto_0

    :cond_0
    sget-object v2, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    :goto_0
    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    iget-object v1, v7, Lwd1;->i:Landroid/graphics/Canvas;

    iget v2, v7, Lwd1;->h:I

    div-int/lit8 v3, v10, 0x2

    add-int/2addr v2, v3

    int-to-float v2, v2

    iget-object v4, v7, Lwd1;->e:[I

    const/4 v11, 0x0

    aget v4, v4, v11

    div-int/lit8 v12, v0, 0x2

    add-int/2addr v4, v12

    add-int/lit8 v4, v4, -0x2

    int-to-float v0, v4

    iget-object v4, v7, Lwd1;->j:Landroid/graphics/Paint;

    move-object v5, p1

    invoke-virtual {v1, p1, v2, v0, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    iget-object v1, v7, Lwd1;->i:Landroid/graphics/Canvas;

    iget v0, v7, Lwd1;->h:I

    add-int v2, v0, v3

    iget-object v0, v7, Lwd1;->e:[I

    aget v3, v0, v11

    iget v0, v7, Lwd1;->c:I

    mul-int/lit8 v5, v0, 0x1e

    move-object v0, p0

    move v4, v10

    move-object v6, v8

    invoke-virtual/range {v0 .. v6}, Lwd1;->i(Landroid/graphics/Canvas;IIIILandroid/graphics/Paint;)V

    iget-object v0, v7, Lwd1;->i:Landroid/graphics/Canvas;

    iget v1, v7, Lwd1;->h:I

    mul-int/lit8 v2, v10, 0x3

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    int-to-float v1, v1

    iget-object v3, v7, Lwd1;->e:[I

    aget v3, v3, v11

    add-int/2addr v3, v12

    add-int/lit8 v3, v3, -0x2

    int-to-float v3, v3

    iget-object v4, v7, Lwd1;->j:Landroid/graphics/Paint;

    move-object v5, p2

    invoke-virtual {v0, p2, v1, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    iget-object v1, v7, Lwd1;->i:Landroid/graphics/Canvas;

    iget v0, v7, Lwd1;->h:I

    add-int/2addr v2, v0

    iget-object v0, v7, Lwd1;->e:[I

    aget v3, v0, v11

    iget v0, v7, Lwd1;->c:I

    mul-int/lit8 v5, v0, 0x1e

    move-object v0, p0

    move v4, v10

    invoke-virtual/range {v0 .. v6}, Lwd1;->i(Landroid/graphics/Canvas;IIIILandroid/graphics/Paint;)V

    iget-object v0, v7, Lwd1;->i:Landroid/graphics/Canvas;

    iget v1, v7, Lwd1;->h:I

    mul-int/lit8 v2, v10, 0x5

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    int-to-float v1, v1

    iget-object v3, v7, Lwd1;->e:[I

    aget v3, v3, v11

    add-int/2addr v3, v12

    add-int/lit8 v3, v3, -0x2

    int-to-float v3, v3

    iget-object v4, v7, Lwd1;->j:Landroid/graphics/Paint;

    move-object/from16 v5, p3

    invoke-virtual {v0, v5, v1, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    iget-object v1, v7, Lwd1;->i:Landroid/graphics/Canvas;

    iget v0, v7, Lwd1;->h:I

    add-int/2addr v2, v0

    iget-object v0, v7, Lwd1;->e:[I

    aget v3, v0, v11

    iget v0, v7, Lwd1;->c:I

    mul-int/lit8 v5, v0, 0x1e

    move-object v0, p0

    move v4, v10

    invoke-virtual/range {v0 .. v6}, Lwd1;->i(Landroid/graphics/Canvas;IIIILandroid/graphics/Paint;)V

    iget-object v0, v7, Lwd1;->i:Landroid/graphics/Canvas;

    iget v1, v7, Lwd1;->h:I

    mul-int/lit8 v2, v10, 0x7

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    int-to-float v1, v1

    iget-object v3, v7, Lwd1;->e:[I

    aget v3, v3, v11

    add-int/2addr v3, v12

    add-int/lit8 v3, v3, -0x2

    int-to-float v3, v3

    iget-object v4, v7, Lwd1;->j:Landroid/graphics/Paint;

    move-object/from16 v5, p4

    invoke-virtual {v0, v5, v1, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    iget-object v1, v7, Lwd1;->i:Landroid/graphics/Canvas;

    iget v0, v7, Lwd1;->h:I

    add-int/2addr v2, v0

    iget-object v0, v7, Lwd1;->e:[I

    aget v3, v0, v11

    iget v0, v7, Lwd1;->c:I

    mul-int/lit8 v5, v0, 0x1e

    move-object v0, p0

    move v4, v10

    invoke-virtual/range {v0 .. v6}, Lwd1;->i(Landroid/graphics/Canvas;IIIILandroid/graphics/Paint;)V

    iget-object v0, v7, Lwd1;->i:Landroid/graphics/Canvas;

    iget v1, v7, Lwd1;->h:I

    mul-int/lit8 v2, v10, 0x9

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    int-to-float v1, v1

    iget-object v3, v7, Lwd1;->e:[I

    aget v3, v3, v11

    add-int/2addr v3, v12

    add-int/lit8 v3, v3, -0x2

    int-to-float v3, v3

    iget-object v4, v7, Lwd1;->j:Landroid/graphics/Paint;

    move-object/from16 v5, p5

    invoke-virtual {v0, v5, v1, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    iget-object v1, v7, Lwd1;->i:Landroid/graphics/Canvas;

    iget v0, v7, Lwd1;->h:I

    add-int/2addr v2, v0

    iget-object v0, v7, Lwd1;->e:[I

    aget v3, v0, v11

    iget v0, v7, Lwd1;->c:I

    mul-int/lit8 v5, v0, 0x1e

    move-object v0, p0

    move v4, v10

    invoke-virtual/range {v0 .. v6}, Lwd1;->i(Landroid/graphics/Canvas;IIIILandroid/graphics/Paint;)V

    iget-object v0, v7, Lwd1;->e:[I

    aget v1, v0, v11

    iget v2, v7, Lwd1;->c:I

    mul-int/lit8 v3, v2, 0x1e

    add-int/2addr v1, v3

    aput v1, v0, v11

    aget v1, v0, v9

    mul-int/lit8 v2, v2, 0x1e

    add-int/2addr v1, v2

    aput v1, v0, v9

    return-void
.end method

.method public final d()V
    .locals 5

    .line 1
    iget v0, p0, Lwd1;->f:I

    iget v1, p0, Lwd1;->c:I

    mul-int/2addr v0, v1

    iget v2, p0, Lwd1;->g:I

    mul-int/2addr v2, v1

    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v2, v1}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iget-object v1, p0, Lwd1;->a:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget v0, p0, Lwd1;->b:I

    const/4 v1, 0x1

    add-int/2addr v0, v1

    iput v0, p0, Lwd1;->b:I

    new-instance v0, Landroid/graphics/Canvas;

    iget-object v2, p0, Lwd1;->a:Ljava/util/ArrayList;

    iget v3, p0, Lwd1;->b:I

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/Bitmap;

    invoke-direct {v0, v2}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v0, p0, Lwd1;->i:Landroid/graphics/Canvas;

    const/16 v2, 0xff

    invoke-virtual {v0, v2, v2, v2, v2}, Landroid/graphics/Canvas;->drawARGB(IIII)V

    iget-object v0, p0, Lwd1;->e:[I

    iget v2, p0, Lwd1;->c:I

    mul-int/lit8 v3, v2, 0x64

    const/4 v4, 0x0

    aput v3, v0, v4

    const/16 v3, 0x64

    mul-int/2addr v2, v3

    aput v2, v0, v1

    iput v4, p0, Lwd1;->d:I

    iput v3, p0, Lwd1;->h:I

    return-void
.end method

.method public e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;)V
    .locals 12

    .line 1
    move-object v7, p0

    move-object/from16 v8, p6

    iget v0, v7, Lwd1;->g:I

    iget v1, v7, Lwd1;->c:I

    mul-int/2addr v0, v1

    iget-object v1, v7, Lwd1;->e:[I

    iget v2, v7, Lwd1;->d:I

    aget v1, v1, v2

    sub-int/2addr v0, v1

    const/16 v1, 0x32

    const/4 v9, 0x1

    if-ge v0, v1, :cond_1

    if-ge v2, v9, :cond_0

    add-int/2addr v2, v9

    iput v2, v7, Lwd1;->d:I

    iget v0, v7, Lwd1;->h:I

    add-int/lit16 v0, v0, 0x226

    iput v0, v7, Lwd1;->h:I

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lwd1;->d()V

    :goto_0
    iget-object v1, v7, Lwd1;->o:Ljava/lang/String;

    iget-object v2, v7, Lwd1;->p:Ljava/lang/String;

    iget-object v3, v7, Lwd1;->q:Ljava/lang/String;

    iget-object v4, v7, Lwd1;->r:Ljava/lang/String;

    const/4 v5, 0x0

    sget-object v6, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;->UNATTEMPTED:Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;

    move-object v0, p0

    invoke-virtual/range {v0 .. v6}, Lwd1;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;)V

    :cond_1
    new-instance v10, Landroid/graphics/Paint;

    invoke-direct {v10}, Landroid/graphics/Paint;-><init>()V

    sget-object v0, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v10, v0}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    invoke-virtual {v10, v9}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    const/high16 v0, -0x1000000

    invoke-virtual {v10, v0}, Landroid/graphics/Paint;->setColor(I)V

    iget v0, v7, Lwd1;->c:I

    mul-int/lit8 v0, v0, 0x14

    iget-object v1, v7, Lwd1;->j:Landroid/graphics/Paint;

    int-to-float v2, v0

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setTextSize(F)V

    iget v1, v7, Lwd1;->c:I

    mul-int/lit8 v9, v1, 0x73

    iget-object v1, v7, Lwd1;->j:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    iget-object v1, v7, Lwd1;->j:Landroid/graphics/Paint;

    if-eqz p5, :cond_2

    sget-object v2, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    goto :goto_1

    :cond_2
    sget-object v2, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    :goto_1
    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    iget-object v1, v7, Lwd1;->i:Landroid/graphics/Canvas;

    iget v2, v7, Lwd1;->h:I

    div-int/lit8 v3, v9, 0x2

    add-int/2addr v2, v3

    int-to-float v2, v2

    iget-object v4, v7, Lwd1;->e:[I

    iget v5, v7, Lwd1;->d:I

    aget v4, v4, v5

    div-int/lit8 v11, v0, 0x2

    add-int/2addr v4, v11

    add-int/lit8 v4, v4, -0x2

    int-to-float v0, v4

    iget-object v4, v7, Lwd1;->j:Landroid/graphics/Paint;

    move-object v5, p1

    invoke-virtual {v1, p1, v2, v0, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    iget-object v1, v7, Lwd1;->i:Landroid/graphics/Canvas;

    iget v0, v7, Lwd1;->h:I

    add-int v2, v0, v3

    iget-object v0, v7, Lwd1;->e:[I

    iget v3, v7, Lwd1;->d:I

    aget v3, v0, v3

    iget v0, v7, Lwd1;->c:I

    mul-int/lit8 v5, v0, 0x1e

    move-object v0, p0

    move v4, v9

    move-object v6, v10

    invoke-virtual/range {v0 .. v6}, Lwd1;->i(Landroid/graphics/Canvas;IIIILandroid/graphics/Paint;)V

    sget-object v0, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;->CORRECT:Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;

    if-ne v8, v0, :cond_3

    iget-object v1, v7, Lwd1;->i:Landroid/graphics/Canvas;

    iget v0, v7, Lwd1;->h:I

    mul-int/lit8 v2, v9, 0x3

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v2, v0

    iget-object v0, v7, Lwd1;->e:[I

    iget v3, v7, Lwd1;->d:I

    aget v3, v0, v3

    iget v0, v7, Lwd1;->c:I

    mul-int/lit8 v5, v0, 0x1e

    iget-object v6, v7, Lwd1;->k:Landroid/graphics/Paint;

    move-object v0, p0

    move v4, v9

    invoke-virtual/range {v0 .. v6}, Lwd1;->i(Landroid/graphics/Canvas;IIIILandroid/graphics/Paint;)V

    :cond_3
    sget-object v0, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;->INCORRECT:Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;

    if-ne v8, v0, :cond_4

    iget-object v1, v7, Lwd1;->i:Landroid/graphics/Canvas;

    iget v0, v7, Lwd1;->h:I

    mul-int/lit8 v2, v9, 0x3

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v2, v0

    iget-object v0, v7, Lwd1;->e:[I

    iget v3, v7, Lwd1;->d:I

    aget v3, v0, v3

    iget v0, v7, Lwd1;->c:I

    mul-int/lit8 v5, v0, 0x1e

    iget-object v6, v7, Lwd1;->l:Landroid/graphics/Paint;

    move-object v0, p0

    move v4, v9

    invoke-virtual/range {v0 .. v6}, Lwd1;->i(Landroid/graphics/Canvas;IIIILandroid/graphics/Paint;)V

    :cond_4
    sget-object v0, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;->PARTIAL_CORRECT:Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;

    if-ne v8, v0, :cond_5

    iget-object v1, v7, Lwd1;->i:Landroid/graphics/Canvas;

    iget v0, v7, Lwd1;->h:I

    mul-int/lit8 v2, v9, 0x3

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v2, v0

    iget-object v0, v7, Lwd1;->e:[I

    iget v3, v7, Lwd1;->d:I

    aget v3, v0, v3

    iget v0, v7, Lwd1;->c:I

    mul-int/lit8 v5, v0, 0x1e

    iget-object v6, v7, Lwd1;->m:Landroid/graphics/Paint;

    move-object v0, p0

    move v4, v9

    invoke-virtual/range {v0 .. v6}, Lwd1;->i(Landroid/graphics/Canvas;IIIILandroid/graphics/Paint;)V

    :cond_5
    sget-object v0, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;->INVALID:Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;

    if-ne v8, v0, :cond_6

    iget-object v1, v7, Lwd1;->i:Landroid/graphics/Canvas;

    iget v0, v7, Lwd1;->h:I

    mul-int/lit8 v2, v9, 0x3

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v2, v0

    iget-object v0, v7, Lwd1;->e:[I

    iget v3, v7, Lwd1;->d:I

    aget v3, v0, v3

    iget v0, v7, Lwd1;->c:I

    mul-int/lit8 v5, v0, 0x1e

    iget-object v6, v7, Lwd1;->m:Landroid/graphics/Paint;

    move-object v0, p0

    move v4, v9

    invoke-virtual/range {v0 .. v6}, Lwd1;->i(Landroid/graphics/Canvas;IIIILandroid/graphics/Paint;)V

    :cond_6
    iget-object v0, v7, Lwd1;->i:Landroid/graphics/Canvas;

    iget v1, v7, Lwd1;->h:I

    mul-int/lit8 v2, v9, 0x3

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    int-to-float v1, v1

    iget-object v3, v7, Lwd1;->e:[I

    iget v4, v7, Lwd1;->d:I

    aget v3, v3, v4

    add-int/2addr v3, v11

    add-int/lit8 v3, v3, -0x2

    int-to-float v3, v3

    iget-object v4, v7, Lwd1;->j:Landroid/graphics/Paint;

    move-object v5, p2

    invoke-virtual {v0, p2, v1, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    iget-object v1, v7, Lwd1;->i:Landroid/graphics/Canvas;

    iget v0, v7, Lwd1;->h:I

    add-int/2addr v2, v0

    iget-object v0, v7, Lwd1;->e:[I

    iget v3, v7, Lwd1;->d:I

    aget v3, v0, v3

    iget v0, v7, Lwd1;->c:I

    mul-int/lit8 v5, v0, 0x1e

    move-object v0, p0

    move v4, v9

    move-object v6, v10

    invoke-virtual/range {v0 .. v6}, Lwd1;->i(Landroid/graphics/Canvas;IIIILandroid/graphics/Paint;)V

    iget-object v0, v7, Lwd1;->i:Landroid/graphics/Canvas;

    iget v1, v7, Lwd1;->h:I

    mul-int/lit8 v2, v9, 0x5

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    int-to-float v1, v1

    iget-object v3, v7, Lwd1;->e:[I

    iget v4, v7, Lwd1;->d:I

    aget v3, v3, v4

    add-int/2addr v3, v11

    add-int/lit8 v3, v3, -0x2

    int-to-float v3, v3

    iget-object v4, v7, Lwd1;->j:Landroid/graphics/Paint;

    move-object v5, p3

    invoke-virtual {v0, p3, v1, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    iget-object v1, v7, Lwd1;->i:Landroid/graphics/Canvas;

    iget v0, v7, Lwd1;->h:I

    add-int/2addr v2, v0

    iget-object v0, v7, Lwd1;->e:[I

    iget v3, v7, Lwd1;->d:I

    aget v3, v0, v3

    iget v0, v7, Lwd1;->c:I

    mul-int/lit8 v5, v0, 0x1e

    move-object v0, p0

    move v4, v9

    invoke-virtual/range {v0 .. v6}, Lwd1;->i(Landroid/graphics/Canvas;IIIILandroid/graphics/Paint;)V

    iget-object v0, v7, Lwd1;->i:Landroid/graphics/Canvas;

    iget v1, v7, Lwd1;->h:I

    mul-int/lit8 v2, v9, 0x7

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    int-to-float v1, v1

    iget-object v3, v7, Lwd1;->e:[I

    iget v4, v7, Lwd1;->d:I

    aget v3, v3, v4

    add-int/2addr v3, v11

    add-int/lit8 v3, v3, -0x2

    int-to-float v3, v3

    iget-object v4, v7, Lwd1;->j:Landroid/graphics/Paint;

    move-object/from16 v5, p4

    invoke-virtual {v0, v5, v1, v3, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    iget-object v1, v7, Lwd1;->i:Landroid/graphics/Canvas;

    iget v0, v7, Lwd1;->h:I

    add-int/2addr v2, v0

    iget-object v0, v7, Lwd1;->e:[I

    iget v3, v7, Lwd1;->d:I

    aget v3, v0, v3

    iget v0, v7, Lwd1;->c:I

    mul-int/lit8 v5, v0, 0x1e

    move-object v0, p0

    move v4, v9

    invoke-virtual/range {v0 .. v6}, Lwd1;->i(Landroid/graphics/Canvas;IIIILandroid/graphics/Paint;)V

    iget-object v0, v7, Lwd1;->e:[I

    iget v1, v7, Lwd1;->d:I

    aget v2, v0, v1

    iget v3, v7, Lwd1;->c:I

    mul-int/lit8 v3, v3, 0x1e

    add-int/2addr v2, v3

    aput v2, v0, v1

    return-void
.end method

.method public f(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 7

    .line 1
    iput-object p1, p0, Lwd1;->o:Ljava/lang/String;

    iput-object p2, p0, Lwd1;->p:Ljava/lang/String;

    iput-object p3, p0, Lwd1;->q:Ljava/lang/String;

    iput-object p4, p0, Lwd1;->r:Ljava/lang/String;

    sget-object v6, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;->UNATTEMPTED:Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v6}, Lwd1;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;)V

    return-void
.end method

.method public g(Ljava/lang/String;Z)V
    .locals 7

    .line 1
    new-instance v6, Landroid/graphics/Paint;

    invoke-direct {v6}, Landroid/graphics/Paint;-><init>()V

    sget-object p2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v6, p2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    const/4 p2, 0x1

    invoke-virtual {v6, p2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    const/high16 p2, -0x1000000

    invoke-virtual {v6, p2}, Landroid/graphics/Paint;->setColor(I)V

    iget p2, p0, Lwd1;->c:I

    mul-int/lit8 p2, p2, 0x14

    iget-object v0, p0, Lwd1;->j:Landroid/graphics/Paint;

    int-to-float v1, p2

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    iget v0, p0, Lwd1;->c:I

    mul-int/lit8 v0, v0, 0x73

    iget-object v1, p0, Lwd1;->j:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    iget-object v1, p0, Lwd1;->j:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    iget-object v1, p0, Lwd1;->i:Landroid/graphics/Canvas;

    iget v2, p0, Lwd1;->h:I

    mul-int/lit8 v3, v0, 0x2

    add-int/2addr v2, v3

    int-to-float v2, v2

    iget-object v4, p0, Lwd1;->e:[I

    iget v5, p0, Lwd1;->d:I

    aget v4, v4, v5

    div-int/lit8 p2, p2, 0x2

    add-int/2addr v4, p2

    add-int/lit8 v4, v4, -0x2

    int-to-float p2, v4

    iget-object v4, p0, Lwd1;->j:Landroid/graphics/Paint;

    invoke-virtual {v1, p1, v2, p2, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    iget-object v1, p0, Lwd1;->i:Landroid/graphics/Canvas;

    iget p1, p0, Lwd1;->h:I

    add-int v2, p1, v3

    iget-object p1, p0, Lwd1;->e:[I

    iget p2, p0, Lwd1;->d:I

    aget v3, p1, p2

    mul-int/lit8 v4, v0, 0x4

    iget p1, p0, Lwd1;->c:I

    mul-int/lit8 v5, p1, 0x1e

    move-object v0, p0

    invoke-virtual/range {v0 .. v6}, Lwd1;->i(Landroid/graphics/Canvas;IIIILandroid/graphics/Paint;)V

    iget-object p1, p0, Lwd1;->e:[I

    iget p2, p0, Lwd1;->d:I

    aget v0, p1, p2

    iget v1, p0, Lwd1;->c:I

    mul-int/lit8 v1, v1, 0x1e

    add-int/2addr v0, v1

    aput v0, p1, p2

    return-void
.end method

.method public h(I)V
    .locals 5

    .line 1
    iget-object v0, p0, Lwd1;->e:[I

    const/4 v1, 0x0

    aget v2, v0, v1

    iget v3, p0, Lwd1;->c:I

    mul-int v4, p1, v3

    add-int/2addr v2, v4

    aput v2, v0, v1

    const/4 v1, 0x1

    aget v2, v0, v1

    mul-int/2addr p1, v3

    add-int/2addr v2, p1

    aput v2, v0, v1

    return-void
.end method

.method public final i(Landroid/graphics/Canvas;IIIILandroid/graphics/Paint;)V
    .locals 7

    .line 1
    div-int/lit8 p4, p4, 0x2

    sub-int v0, p2, p4

    int-to-float v2, v0

    div-int/lit8 p5, p5, 0x2

    sub-int v0, p3, p5

    int-to-float v3, v0

    add-int/2addr p2, p4

    int-to-float v4, p2

    add-int/2addr p3, p5

    int-to-float v5, p3

    move-object v1, p1

    move-object v6, p6

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    return-void
.end method

.method public j()Ljava/util/ArrayList;
    .locals 1

    .line 1
    iget-object v0, p0, Lwd1;->a:Ljava/util/ArrayList;

    return-object v0
.end method

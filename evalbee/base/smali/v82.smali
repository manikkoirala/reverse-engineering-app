.class public Lv82;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# static fields
.field public static final g:Ljava/lang/String;


# instance fields
.field public final a:Lum1;

.field public final b:Landroid/content/Context;

.field public final c:Lp92;

.field public final d:Landroidx/work/c;

.field public final e:Lr70;

.field public final f:Lhu1;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .line 1
    const-string v0, "WorkForegroundRunnable"

    invoke-static {v0}, Lxl0;->i(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lv82;->g:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lp92;Landroidx/work/c;Lr70;Lhu1;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lum1;->s()Lum1;

    move-result-object v0

    iput-object v0, p0, Lv82;->a:Lum1;

    iput-object p1, p0, Lv82;->b:Landroid/content/Context;

    iput-object p2, p0, Lv82;->c:Lp92;

    iput-object p3, p0, Lv82;->d:Landroidx/work/c;

    iput-object p4, p0, Lv82;->e:Lr70;

    iput-object p5, p0, Lv82;->f:Lhu1;

    return-void
.end method

.method public static synthetic a(Lv82;Lum1;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lv82;->c(Lum1;)V

    return-void
.end method

.method private synthetic c(Lum1;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lv82;->a:Lum1;

    invoke-virtual {v0}, Landroidx/work/impl/utils/futures/AbstractFuture;->isCancelled()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lv82;->d:Landroidx/work/c;

    invoke-virtual {v0}, Landroidx/work/c;->getForegroundInfoAsync()Lik0;

    move-result-object v0

    invoke-virtual {p1, v0}, Lum1;->q(Lik0;)Z

    goto :goto_0

    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroidx/work/impl/utils/futures/AbstractFuture;->cancel(Z)Z

    :goto_0
    return-void
.end method


# virtual methods
.method public b()Lik0;
    .locals 1

    .line 1
    iget-object v0, p0, Lv82;->a:Lum1;

    return-object v0
.end method

.method public run()V
    .locals 3

    .line 1
    iget-object v0, p0, Lv82;->c:Lp92;

    iget-boolean v0, v0, Lp92;->q:Z

    if-eqz v0, :cond_1

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x1f

    if-lt v0, v1, :cond_0

    goto :goto_0

    :cond_0
    invoke-static {}, Lum1;->s()Lum1;

    move-result-object v0

    iget-object v1, p0, Lv82;->f:Lhu1;

    invoke-interface {v1}, Lhu1;->c()Ljava/util/concurrent/Executor;

    move-result-object v1

    new-instance v2, Lu82;

    invoke-direct {v2, p0, v0}, Lu82;-><init>(Lv82;Lum1;)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    new-instance v1, Lv82$a;

    invoke-direct {v1, p0, v0}, Lv82$a;-><init>(Lv82;Lum1;)V

    iget-object v2, p0, Lv82;->f:Lhu1;

    invoke-interface {v2}, Lhu1;->c()Ljava/util/concurrent/Executor;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroidx/work/impl/utils/futures/AbstractFuture;->addListener(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V

    return-void

    :cond_1
    :goto_0
    iget-object v0, p0, Lv82;->a:Lum1;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lum1;->o(Ljava/lang/Object;)Z

    return-void
.end method

.class public Li32$b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/android/volley/d$b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Li32;->f(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Li32;


# direct methods
.method public constructor <init>(Li32;)V
    .locals 0

    .line 1
    iput-object p1, p0, Li32$b;->a:Li32;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .line 1
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Li32$b;->b(Ljava/lang/String;)V

    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 5

    .line 1
    const/4 v0, 0x0

    :try_start_0
    new-instance v1, Lgc0;

    invoke-direct {v1}, Lgc0;-><init>()V

    const-class v2, Lcom/ekodroid/omrevaluator/serializable/ResponseModel/PurchaseAccount;

    invoke-virtual {v1, p1, v2}, Lgc0;->j(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/ekodroid/omrevaluator/serializable/ResponseModel/PurchaseAccount;

    if-eqz v1, :cond_0

    iget-object v2, p0, Li32$b;->a:Li32;

    invoke-static {v2}, Li32;->c(Li32;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "MyPref"

    invoke-virtual {v2, v3, v0}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    sget-object v3, Lok;->y:Ljava/lang/String;

    invoke-static {}, Lcom/google/firebase/auth/FirebaseAuth;->getInstance()Lcom/google/firebase/auth/FirebaseAuth;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/firebase/auth/FirebaseAuth;->e()Lr30;

    move-result-object v4

    invoke-virtual {v4}, Lr30;->O()Ljava/lang/String;

    move-result-object v4

    invoke-static {p1, v4}, Lb;->i(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-interface {v2, v3, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    :cond_0
    iget-object p1, p0, Li32$b;->a:Li32;

    const/4 v2, 0x1

    const/16 v3, 0xc8

    invoke-static {p1, v2, v3, v1}, Li32;->b(Li32;ZILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V

    iget-object p1, p0, Li32$b;->a:Li32;

    const/16 v1, 0x190

    const/4 v2, 0x0

    invoke-static {p1, v0, v1, v2}, Li32;->b(Li32;ZILjava/lang/Object;)V

    :goto_0
    return-void
.end method

.class public Lgc0$f;
.super Lll1;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lgc0;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "f"
.end annotation


# instance fields
.field public a:Lhz1;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Lll1;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lgc0$f;->a:Lhz1;

    return-void
.end method


# virtual methods
.method public b(Lrh0;)Ljava/lang/Object;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lgc0$f;->f()Lhz1;

    move-result-object v0

    invoke-virtual {v0, p1}, Lhz1;->b(Lrh0;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public d(Lvh0;Ljava/lang/Object;)V
    .locals 1

    .line 1
    invoke-virtual {p0}, Lgc0$f;->f()Lhz1;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lhz1;->d(Lvh0;Ljava/lang/Object;)V

    return-void
.end method

.method public e()Lhz1;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lgc0$f;->f()Lhz1;

    move-result-object v0

    return-object v0
.end method

.method public final f()Lhz1;
    .locals 2

    .line 1
    iget-object v0, p0, Lgc0$f;->a:Lhz1;

    if-eqz v0, :cond_0

    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Adapter for type with cyclic dependency has been used before dependency has been resolved"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public g(Lhz1;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lgc0$f;->a:Lhz1;

    if-nez v0, :cond_0

    iput-object p1, p0, Lgc0$f;->a:Lhz1;

    return-void

    :cond_0
    new-instance p1, Ljava/lang/AssertionError;

    const-string v0, "Delegate is already set"

    invoke-direct {p1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw p1
.end method

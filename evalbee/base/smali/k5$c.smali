.class public Lk5$c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ly01;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lk5;->n(Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;)Landroid/widget/LinearLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Landroid/widget/TextView;

.field public final synthetic b:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;

.field public final synthetic c:Lk5;


# direct methods
.method public constructor <init>(Lk5;Landroid/widget/TextView;Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lk5$c;->c:Lk5;

    iput-object p2, p0, Lk5$c;->a:Landroid/widget/TextView;

    iput-object p3, p0, Lk5$c;->b:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Object;)V
    .locals 2

    .line 1
    iget-object p1, p0, Lk5$c;->a:Landroid/widget/TextView;

    iget-object v0, p0, Lk5$c;->c:Lk5;

    iget-object v0, v0, Lk5;->a:Landroid/content/Context;

    const v1, 0x7f120170

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object p1, p0, Lk5$c;->b:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;->getPartialAnswerOptionKeys()Ljava/util/ArrayList;

    move-result-object p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lk5$c;->b:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;->getPartialAnswerOptionKeys()Ljava/util/ArrayList;

    move-result-object p1

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result p1

    if-lez p1, :cond_0

    iget-object p1, p0, Lk5$c;->a:Landroid/widget/TextView;

    iget-object v0, p0, Lk5$c;->c:Lk5;

    iget-object v0, v0, Lk5;->a:Landroid/content/Context;

    const v1, 0x7f120196

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method

.class public Lre;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Lk70$c;

.field public final b:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Lk70$c;Landroid/os/Handler;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lre;->a:Lk70$c;

    iput-object p2, p0, Lre;->b:Landroid/os/Handler;

    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 3

    .line 1
    iget-object v0, p0, Lre;->a:Lk70$c;

    iget-object v1, p0, Lre;->b:Landroid/os/Handler;

    new-instance v2, Lre$b;

    invoke-direct {v2, p0, v0, p1}, Lre$b;-><init>(Lre;Lk70$c;I)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public b(Li70$e;)V
    .locals 1

    .line 1
    invoke-virtual {p1}, Li70$e;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object p1, p1, Li70$e;->a:Landroid/graphics/Typeface;

    invoke-virtual {p0, p1}, Lre;->c(Landroid/graphics/Typeface;)V

    goto :goto_0

    :cond_0
    iget p1, p1, Li70$e;->b:I

    invoke-virtual {p0, p1}, Lre;->a(I)V

    :goto_0
    return-void
.end method

.method public final c(Landroid/graphics/Typeface;)V
    .locals 3

    .line 1
    iget-object v0, p0, Lre;->a:Lk70$c;

    iget-object v1, p0, Lre;->b:Landroid/os/Handler;

    new-instance v2, Lre$a;

    invoke-direct {v2, p0, v0, p1}, Lre$a;-><init>(Lre;Lk70$c;Landroid/graphics/Typeface;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

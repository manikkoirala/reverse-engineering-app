.class public Lug$b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lug;->m()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Lug$c;

.field public final synthetic b:Lug;


# direct methods
.method public constructor <init>(Lug;Lug$c;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lug$b;->b:Lug;

    iput-object p2, p0, Lug$b;->a:Lug$c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 0

    .line 1
    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 0

    .line 1
    return-void
.end method

.method public onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 4

    .line 1
    iget-object v0, p0, Lug$b;->b:Lug;

    iget-object v1, p0, Lug$b;->a:Lug$c;

    const/4 v2, 0x1

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {v0, v3, v1, v2}, Lug;->b(FLug$c;Z)V

    iget-object v0, p0, Lug$b;->a:Lug$c;

    invoke-virtual {v0}, Lug$c;->A()V

    iget-object v0, p0, Lug$b;->a:Lug$c;

    invoke-virtual {v0}, Lug$c;->l()V

    iget-object v0, p0, Lug$b;->b:Lug;

    iget-boolean v1, v0, Lug;->f:Z

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    iput-boolean v1, v0, Lug;->f:Z

    invoke-virtual {p1}, Landroid/animation/Animator;->cancel()V

    const-wide/16 v2, 0x534

    invoke-virtual {p1, v2, v3}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    invoke-virtual {p1}, Landroid/animation/Animator;->start()V

    iget-object p1, p0, Lug$b;->a:Lug$c;

    invoke-virtual {p1, v1}, Lug$c;->x(Z)V

    goto :goto_0

    :cond_0
    iget p1, v0, Lug;->e:F

    add-float/2addr p1, v3

    iput p1, v0, Lug;->e:F

    :goto_0
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 1

    .line 1
    iget-object p1, p0, Lug$b;->b:Lug;

    const/4 v0, 0x0

    iput v0, p1, Lug;->e:F

    return-void
.end method

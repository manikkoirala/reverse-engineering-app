.class public Lcb0$a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/android/volley/d$b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcb0;->g()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Lcb0;


# direct methods
.method public constructor <init>(Lcb0;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcb0$a;->a:Lcb0;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .line 1
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcb0$a;->b(Ljava/lang/String;)V

    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 3

    .line 1
    :try_start_0
    new-instance v0, Lgc0;

    invoke-direct {v0}, Lgc0;-><init>()V

    const-class v1, Lcom/ekodroid/omrevaluator/serializable/UserProfile;

    invoke-virtual {v0, p1, v1}, Lgc0;->j(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/ekodroid/omrevaluator/serializable/UserProfile;

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/serializable/UserProfile;->getUserData()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v0, Lgc0;

    invoke-direct {v0}, Lgc0;-><init>()V

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/serializable/UserProfile;->getUserData()Ljava/lang/String;

    move-result-object p1

    iget-object v1, p0, Lcb0$a;->a:Lcb0;

    invoke-static {v1}, Lcb0;->a(Lcb0;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v1}, Lb;->g(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    const-class v1, Lcom/ekodroid/omrevaluator/serializable/UserAccount;

    invoke-virtual {v0, p1, v1}, Lgc0;->j(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/ekodroid/omrevaluator/serializable/UserAccount;

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcb0$a;->a:Lcb0;

    iget-object v0, v0, Lcb0;->b:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "count_saveScan"

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/serializable/UserAccount;->getScanSave()I

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    iget-object v0, p0, Lcb0$a;->a:Lcb0;

    iget-object v0, v0, Lcb0;->b:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "count_cancelScan"

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/serializable/UserAccount;->getScanCancel()I

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    iget-object v0, p0, Lcb0$a;->a:Lcb0;

    iget-object v0, v0, Lcb0;->b:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "count_email"

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/serializable/UserAccount;->getEmailSent()I

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    iget-object v0, p0, Lcb0$a;->a:Lcb0;

    iget-object v0, v0, Lcb0;->b:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "count_sms"

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/serializable/UserAccount;->getSmsSent()I

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    iget-object v0, p0, Lcb0$a;->a:Lcb0;

    iget-object v0, v0, Lcb0;->b:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "count_saveImportScan"

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/serializable/UserAccount;->getImageScan()I

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    iget-object v0, p0, Lcb0$a;->a:Lcb0;

    iget-object v0, v0, Lcb0;->b:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "count_errorImportScan"

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/serializable/UserAccount;->getImageScanError()I

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    iget-object v0, p0, Lcb0$a;->a:Lcb0;

    iget-object v0, v0, Lcb0;->b:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "user_country"

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/serializable/UserAccount;->getCountry()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    iget-object v0, p0, Lcb0$a;->a:Lcb0;

    iget-object v0, v0, Lcb0;->b:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "user_city"

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/serializable/UserAccount;->getCity()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    iget-object v0, p0, Lcb0$a;->a:Lcb0;

    iget-object v0, v0, Lcb0;->b:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "user_organization"

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/serializable/UserAccount;->getOrganization()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    :cond_0
    iget-object p1, p0, Lcb0$a;->a:Lcb0;

    const-string v0, "Success"

    invoke-static {p1, v0}, Lcb0;->b(Lcb0;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V

    iget-object p1, p0, Lcb0$a;->a:Lcb0;

    const/4 v0, 0x0

    invoke-static {p1, v0}, Lcb0;->b(Lcb0;Ljava/lang/Object;)V

    :goto_0
    return-void
.end method

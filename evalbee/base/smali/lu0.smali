.class public Llu0;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lt90;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a([DI)D
    .locals 6

    .line 1
    if-nez p2, :cond_0

    const-wide p1, 0x7fefffffffffffffL    # Double.MAX_VALUE

    return-wide p1

    :cond_0
    const-wide v0, -0x10000000000001L

    const/4 v2, 0x0

    :goto_0
    if-ge v2, p2, :cond_2

    aget-wide v3, p1, v2

    cmpl-double v5, v3, v0

    if-lez v5, :cond_1

    move-wide v0, v3

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    return-wide v0
.end method

.method public b(I)Z
    .locals 0

    .line 1
    if-ltz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .line 1
    const-string v0, "max(x1, x2, ..., xn)"

    return-object v0
.end method

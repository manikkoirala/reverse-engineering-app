.class public Lx11;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroidx/work/d;


# instance fields
.field public final c:Ltx0;

.field public final d:Lum1;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ltx0;

    invoke-direct {v0}, Ltx0;-><init>()V

    iput-object v0, p0, Lx11;->c:Ltx0;

    invoke-static {}, Lum1;->s()Lum1;

    move-result-object v0

    iput-object v0, p0, Lx11;->d:Lum1;

    sget-object v0, Landroidx/work/d;->b:Landroidx/work/d$b$b;

    invoke-virtual {p0, v0}, Lx11;->a(Landroidx/work/d$b;)V

    return-void
.end method


# virtual methods
.method public a(Landroidx/work/d$b;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lx11;->c:Ltx0;

    invoke-virtual {v0, p1}, Ltx0;->l(Ljava/lang/Object;)V

    instance-of v0, p1, Landroidx/work/d$b$c;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lx11;->d:Lum1;

    check-cast p1, Landroidx/work/d$b$c;

    invoke-virtual {v0, p1}, Lum1;->o(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    instance-of v0, p1, Landroidx/work/d$b$a;

    if-eqz v0, :cond_1

    check-cast p1, Landroidx/work/d$b$a;

    iget-object v0, p0, Lx11;->d:Lum1;

    invoke-virtual {p1}, Landroidx/work/d$b$a;->a()Ljava/lang/Throwable;

    move-result-object p1

    invoke-virtual {v0, p1}, Lum1;->p(Ljava/lang/Throwable;)Z

    :cond_1
    :goto_0
    return-void
.end method

.method public getResult()Lik0;
    .locals 1

    .line 1
    iget-object v0, p0, Lx11;->d:Lum1;

    return-object v0
.end method

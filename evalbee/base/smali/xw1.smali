.class public final Lxw1;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Lgr;

.field public final b:Lah;

.field public volatile c:Z

.field public volatile d:I

.field public volatile e:J

.field public volatile f:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lgr;Lah;)V
    .locals 2

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lxw1;->a:Lgr;

    iput-object p3, p0, Lxw1;->b:Lah;

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lxw1;->e:J

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    check-cast p1, Landroid/app/Application;

    invoke-static {p1}, Lcom/google/android/gms/common/api/internal/BackgroundDetector;->initialize(Landroid/app/Application;)V

    invoke-static {}, Lcom/google/android/gms/common/api/internal/BackgroundDetector;->getInstance()Lcom/google/android/gms/common/api/internal/BackgroundDetector;

    move-result-object p1

    new-instance v0, Lxw1$a;

    invoke-direct {v0, p0, p2, p3}, Lxw1$a;-><init>(Lxw1;Lgr;Lah;)V

    invoke-virtual {p1, v0}, Lcom/google/android/gms/common/api/internal/BackgroundDetector;->addListener(Lcom/google/android/gms/common/api/internal/BackgroundDetector$BackgroundStateChangeListener;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lpq;Ljava/util/concurrent/Executor;Ljava/util/concurrent/ScheduledExecutorService;)V
    .locals 1

    .line 1
    invoke-static {p1}, Lcom/google/android/gms/common/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/content/Context;

    new-instance v0, Lgr;

    invoke-static {p2}, Lcom/google/android/gms/common/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lpq;

    invoke-direct {v0, p2, p3, p4}, Lgr;-><init>(Lpq;Ljava/util/concurrent/Executor;Ljava/util/concurrent/ScheduledExecutorService;)V

    new-instance p2, Lah$a;

    invoke-direct {p2}, Lah$a;-><init>()V

    invoke-direct {p0, p1, v0, p2}, Lxw1;-><init>(Landroid/content/Context;Lgr;Lah;)V

    return-void
.end method

.method public static synthetic a(Lxw1;Z)Z
    .locals 0

    .line 1
    iput-boolean p1, p0, Lxw1;->c:Z

    return p1
.end method

.method public static synthetic b(Lxw1;)Z
    .locals 0

    .line 1
    invoke-virtual {p0}, Lxw1;->e()Z

    move-result p0

    return p0
.end method

.method public static synthetic c(Lxw1;)J
    .locals 2

    .line 1
    iget-wide v0, p0, Lxw1;->e:J

    return-wide v0
.end method


# virtual methods
.method public d(I)V
    .locals 5

    .line 1
    iget v0, p0, Lxw1;->d:I

    if-nez v0, :cond_0

    if-lez p1, :cond_0

    iput p1, p0, Lxw1;->d:I

    invoke-virtual {p0}, Lxw1;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lxw1;->a:Lgr;

    iget-wide v1, p0, Lxw1;->e:J

    iget-object v3, p0, Lxw1;->b:Lah;

    invoke-interface {v3}, Lah;->currentTimeMillis()J

    move-result-wide v3

    sub-long/2addr v1, v3

    invoke-virtual {v0, v1, v2}, Lgr;->g(J)V

    goto :goto_0

    :cond_0
    iget v0, p0, Lxw1;->d:I

    if-lez v0, :cond_1

    if-nez p1, :cond_1

    iget-object v0, p0, Lxw1;->a:Lgr;

    invoke-virtual {v0}, Lgr;->c()V

    :cond_1
    :goto_0
    iput p1, p0, Lxw1;->d:I

    return-void
.end method

.method public final e()Z
    .locals 4

    .line 1
    iget-boolean v0, p0, Lxw1;->f:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lxw1;->c:Z

    if-nez v0, :cond_0

    iget v0, p0, Lxw1;->d:I

    if-lez v0, :cond_0

    iget-wide v0, p0, Lxw1;->e:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

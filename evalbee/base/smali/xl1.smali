.class public final Lxl1;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Lcom/google/firebase/sessions/EventType;

.field public final b:Lam1;

.field public final c:Ly7;


# direct methods
.method public constructor <init>(Lcom/google/firebase/sessions/EventType;Lam1;Ly7;)V
    .locals 1

    .line 1
    const-string v0, "eventType"

    invoke-static {p1, v0}, Lfg0;->e(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "sessionData"

    invoke-static {p2, v0}, Lfg0;->e(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "applicationInfo"

    invoke-static {p3, v0}, Lfg0;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lxl1;->a:Lcom/google/firebase/sessions/EventType;

    iput-object p2, p0, Lxl1;->b:Lam1;

    iput-object p3, p0, Lxl1;->c:Ly7;

    return-void
.end method


# virtual methods
.method public final a()Ly7;
    .locals 1

    .line 1
    iget-object v0, p0, Lxl1;->c:Ly7;

    return-object v0
.end method

.method public final b()Lcom/google/firebase/sessions/EventType;
    .locals 1

    .line 1
    iget-object v0, p0, Lxl1;->a:Lcom/google/firebase/sessions/EventType;

    return-object v0
.end method

.method public final c()Lam1;
    .locals 1

    .line 1
    iget-object v0, p0, Lxl1;->b:Lam1;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .line 1
    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    instance-of v1, p1, Lxl1;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    :cond_1
    check-cast p1, Lxl1;

    iget-object v1, p0, Lxl1;->a:Lcom/google/firebase/sessions/EventType;

    iget-object v3, p1, Lxl1;->a:Lcom/google/firebase/sessions/EventType;

    if-eq v1, v3, :cond_2

    return v2

    :cond_2
    iget-object v1, p0, Lxl1;->b:Lam1;

    iget-object v3, p1, Lxl1;->b:Lam1;

    invoke-static {v1, v3}, Lfg0;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    return v2

    :cond_3
    iget-object v1, p0, Lxl1;->c:Ly7;

    iget-object p1, p1, Lxl1;->c:Ly7;

    invoke-static {v1, p1}, Lfg0;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_4

    return v2

    :cond_4
    return v0
.end method

.method public hashCode()I
    .locals 2

    .line 1
    iget-object v0, p0, Lxl1;->a:Lcom/google/firebase/sessions/EventType;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lxl1;->b:Lam1;

    invoke-virtual {v1}, Lam1;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lxl1;->c:Ly7;

    invoke-virtual {v1}, Ly7;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SessionEvent(eventType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lxl1;->a:Lcom/google/firebase/sessions/EventType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", sessionData="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lxl1;->b:Lam1;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", applicationInfo="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lxl1;->c:Ly7;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x29

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

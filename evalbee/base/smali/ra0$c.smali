.class public Lra0$c;
.super Lhr1;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lra0;->i()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic w:Lra0;


# direct methods
.method public constructor <init>(Lra0;ILjava/lang/String;Lcom/android/volley/d$b;Lcom/android/volley/d$a;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lra0$c;->w:Lra0;

    invoke-direct {p0, p2, p3, p4, p5}, Lhr1;-><init>(ILjava/lang/String;Lcom/android/volley/d$b;Lcom/android/volley/d$a;)V

    return-void
.end method


# virtual methods
.method public k()[B
    .locals 9

    .line 1
    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p0, Lra0$c;->w:Lra0;

    invoke-static {v1}, Lra0;->g(Lra0;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    iget-object v2, p0, Lra0$c;->w:Lra0;

    invoke-static {v2}, Lra0;->g(Lra0;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    iget-object v2, p0, Lra0$c;->w:Lra0;

    iget v1, v1, Landroid/content/pm/PackageInfo;->versionCode:I

    invoke-static {v2, v1}, Lra0;->c(Lra0;I)I

    invoke-static {}, Lra0;->h()Ljava/lang/String;

    move-result-object v4

    new-instance v1, Lcom/ekodroid/omrevaluator/serializable/AppConfig;

    iget-object v2, p0, Lra0$c;->w:Lra0;

    invoke-static {v2}, Lra0;->b(Lra0;)I

    move-result v5

    const/4 v6, 0x0

    invoke-static {}, La91;->n()I

    move-result v7

    iget-object v2, p0, Lra0$c;->w:Lra0;

    invoke-static {v2}, Lra0;->b(Lra0;)I

    move-result v8

    move-object v3, v1

    invoke-direct/range {v3 .. v8}, Lcom/ekodroid/omrevaluator/serializable/AppConfig;-><init>(Ljava/lang/String;IZII)V

    iget-object v2, p0, Lra0$c;->w:Lra0;

    invoke-static {v2}, Lra0;->a(Lra0;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "phoneKey"

    invoke-static {v2, v3}, Lb;->i(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lgc0;

    invoke-direct {v3}, Lgc0;-><init>()V

    invoke-virtual {v3, v1}, Lgc0;->s(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lra0$c;->w:Lra0;

    invoke-static {v3}, Lra0;->a(Lra0;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lb;->i(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-instance v3, Lcom/ekodroid/omrevaluator/serializable/SerialData;

    invoke-static {}, La91;->n()I

    move-result v4

    invoke-static {}, La91;->s()[I

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/ekodroid/omrevaluator/serializable/SerialData;-><init>(I[I)V

    iget-object v4, p0, Lra0$c;->w:Lra0;

    new-instance v5, Lgc0;

    invoke-direct {v5}, Lgc0;-><init>()V

    invoke-virtual {v5, v3}, Lgc0;->s(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    iget-object v5, p0, Lra0$c;->w:Lra0;

    invoke-static {v5}, Lra0;->a(Lra0;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lb;->i(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Lra0;->e(Lra0;Ljava/lang/String;)Ljava/lang/String;

    new-instance v3, Lcom/ekodroid/omrevaluator/serializable/SessionProfile;

    iget-object v4, p0, Lra0$c;->w:Lra0;

    invoke-static {v4}, Lra0;->d(Lra0;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v2, v1, v4, v0}, Lcom/ekodroid/omrevaluator/serializable/SessionProfile;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    new-instance v1, Lgc0;

    invoke-direct {v1}, Lgc0;-><init>()V

    invoke-virtual {v1, v3}, Lgc0;->s(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "UTF-8"

    invoke-virtual {v1, v2}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v0
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v1

    goto :goto_0

    :catch_1
    move-exception v1

    :goto_0
    invoke-virtual {v1}, Ljava/lang/Throwable;->printStackTrace()V

    return-object v0
.end method

.method public l()Ljava/lang/String;
    .locals 1

    .line 1
    const-string v0, "application/json"

    return-object v0
.end method

.class public Lni0;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:Ljava/util/ArrayList;

.field public b:I

.field public c:I

.field public d:I

.field public e:[I

.field public f:I

.field public g:I

.field public h:I

.field public i:Landroid/graphics/Canvas;

.field public j:Landroid/graphics/Paint;

.field public k:Landroid/graphics/Paint;

.field public l:Landroid/graphics/Paint;

.field public m:Landroid/graphics/Paint;

.field public n:Ljava/lang/String;

.field public o:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 7

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lni0;->c:I

    const/4 v1, 0x0

    iput v1, p0, Lni0;->d:I

    const/4 v2, 0x4

    new-array v2, v2, [I

    iput-object v2, p0, Lni0;->e:[I

    const/16 v2, 0x4b0

    iput v2, p0, Lni0;->f:I

    const/16 v2, 0x708

    iput v2, p0, Lni0;->g:I

    const/16 v2, 0x64

    iput v2, p0, Lni0;->h:I

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lni0;->a:Ljava/util/ArrayList;

    iget v3, p0, Lni0;->f:I

    iget v4, p0, Lni0;->c:I

    mul-int/2addr v3, v4

    iget v5, p0, Lni0;->g:I

    mul-int/2addr v5, v4

    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v3, v5, v4}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v3

    new-instance v4, Landroid/graphics/Canvas;

    invoke-direct {v4, v3}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v4, p0, Lni0;->i:Landroid/graphics/Canvas;

    const/16 v5, 0xff

    invoke-virtual {v4, v5, v5, v5, v5}, Landroid/graphics/Canvas;->drawARGB(IIII)V

    iget-object v4, p0, Lni0;->a:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3}, Landroid/graphics/Paint;-><init>()V

    iput-object v3, p0, Lni0;->j:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v3, p0, Lni0;->j:Landroid/graphics/Paint;

    invoke-virtual {v3, v0}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget-object v3, p0, Lni0;->j:Landroid/graphics/Paint;

    const/high16 v4, -0x1000000

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v3, p0, Lni0;->j:Landroid/graphics/Paint;

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    iget-object v3, p0, Lni0;->j:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    iget-object v3, p0, Lni0;->e:[I

    iget v4, p0, Lni0;->c:I

    mul-int/lit8 v6, v4, 0x64

    aput v6, v3, v1

    mul-int/lit8 v1, v4, 0x64

    aput v1, v3, v0

    mul-int/lit8 v1, v4, 0x64

    const/4 v6, 0x2

    aput v1, v3, v6

    const/4 v1, 0x3

    mul-int/2addr v4, v2

    aput v4, v3, v1

    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lni0;->k:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v1, p0, Lni0;->k:Landroid/graphics/Paint;

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget-object v1, p0, Lni0;->k:Landroid/graphics/Paint;

    const/16 v2, 0xc8

    invoke-static {v2, v5, v2}, Landroid/graphics/Color;->rgb(III)I

    move-result v3

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setColor(I)V

    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lni0;->l:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v1, p0, Lni0;->l:Landroid/graphics/Paint;

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget-object v1, p0, Lni0;->l:Landroid/graphics/Paint;

    invoke-static {v5, v2, v2}, Landroid/graphics/Color;->rgb(III)I

    move-result v3

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setColor(I)V

    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lni0;->m:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v1, p0, Lni0;->m:Landroid/graphics/Paint;

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget-object v0, p0, Lni0;->m:Landroid/graphics/Paint;

    invoke-static {v5, v5, v2}, Landroid/graphics/Color;->rgb(III)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)V
    .locals 6

    .line 1
    iget v0, p0, Lni0;->c:I

    mul-int/lit8 v0, v0, 0x1e

    iget-object v1, p0, Lni0;->j:Landroid/graphics/Paint;

    int-to-float v2, v0

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setTextSize(F)V

    iget v1, p0, Lni0;->f:I

    const/4 v2, 0x2

    div-int/2addr v1, v2

    iget v3, p0, Lni0;->c:I

    mul-int/2addr v1, v3

    iget-object v3, p0, Lni0;->j:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    iget-object v3, p0, Lni0;->j:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    iget-object v3, p0, Lni0;->i:Landroid/graphics/Canvas;

    int-to-float v1, v1

    iget-object v4, p0, Lni0;->e:[I

    const/4 v5, 0x0

    aget v4, v4, v5

    div-int/2addr v0, v2

    add-int/2addr v4, v0

    sub-int/2addr v4, v2

    int-to-float v0, v4

    iget-object v4, p0, Lni0;->j:Landroid/graphics/Paint;

    invoke-virtual {v3, p1, v1, v0, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    iget-object p1, p0, Lni0;->e:[I

    aget v0, p1, v5

    iget v1, p0, Lni0;->c:I

    mul-int/lit8 v3, v1, 0x3c

    add-int/2addr v0, v3

    aput v0, p1, v5

    const/4 v0, 0x1

    aget v3, p1, v0

    mul-int/lit8 v4, v1, 0x3c

    add-int/2addr v3, v4

    aput v3, p1, v0

    aget v0, p1, v2

    mul-int/lit8 v3, v1, 0x3c

    add-int/2addr v0, v3

    aput v0, p1, v2

    const/4 v0, 0x3

    aget v2, p1, v0

    mul-int/lit8 v1, v1, 0x3c

    add-int/2addr v2, v1

    aput v2, p1, v0

    return-void
.end method

.method public final b()V
    .locals 5

    .line 1
    iget v0, p0, Lni0;->f:I

    iget v1, p0, Lni0;->c:I

    mul-int/2addr v0, v1

    iget v2, p0, Lni0;->g:I

    mul-int/2addr v2, v1

    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v2, v1}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iget-object v1, p0, Lni0;->a:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget v0, p0, Lni0;->b:I

    const/4 v1, 0x1

    add-int/2addr v0, v1

    iput v0, p0, Lni0;->b:I

    new-instance v0, Landroid/graphics/Canvas;

    iget-object v2, p0, Lni0;->a:Ljava/util/ArrayList;

    iget v3, p0, Lni0;->b:I

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/Bitmap;

    invoke-direct {v0, v2}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v0, p0, Lni0;->i:Landroid/graphics/Canvas;

    const/16 v2, 0xff

    invoke-virtual {v0, v2, v2, v2, v2}, Landroid/graphics/Canvas;->drawARGB(IIII)V

    iget-object v0, p0, Lni0;->e:[I

    iget v2, p0, Lni0;->c:I

    mul-int/lit8 v3, v2, 0x64

    const/4 v4, 0x0

    aput v3, v0, v4

    mul-int/lit8 v3, v2, 0x64

    aput v3, v0, v1

    mul-int/lit8 v1, v2, 0x64

    const/4 v3, 0x2

    aput v1, v0, v3

    const/4 v1, 0x3

    const/16 v3, 0x64

    mul-int/2addr v2, v3

    aput v2, v0, v1

    iput v4, p0, Lni0;->d:I

    iput v3, p0, Lni0;->h:I

    return-void
.end method

.method public c(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 12

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v1

    const/16 v2, 0xd

    const/4 v3, 0x0

    const/4 v4, 0x1

    if-ge v1, v2, :cond_0

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_0
    const-string v1, ","

    invoke-virtual {p2, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p2

    const-string v2, ""

    move-object v6, v2

    move v5, v3

    :goto_0
    array-length v7, p2

    if-ge v5, v7, :cond_3

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v7

    const/16 v8, 0xc

    if-le v7, v8, :cond_1

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object v6, v2

    :cond_1
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-object v6, p2, v5

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    array-length v7, p2

    sub-int/2addr v7, v4

    if-eq v5, v7, :cond_2

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    :cond_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    :cond_3
    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_1
    iget p2, p0, Lni0;->g:I

    iget v1, p0, Lni0;->c:I

    mul-int/2addr p2, v1

    iget-object v1, p0, Lni0;->e:[I

    iget v2, p0, Lni0;->d:I

    aget v1, v1, v2

    sub-int/2addr p2, v1

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x32

    if-ge p2, v1, :cond_5

    iget p2, p0, Lni0;->d:I

    const/4 v1, 0x3

    if-ge p2, v1, :cond_4

    add-int/2addr p2, v4

    iput p2, p0, Lni0;->d:I

    iget p2, p0, Lni0;->h:I

    add-int/lit16 p2, p2, 0x10e

    iput p2, p0, Lni0;->h:I

    goto :goto_2

    :cond_4
    invoke-virtual {p0}, Lni0;->b()V

    :goto_2
    iget-object p2, p0, Lni0;->n:Ljava/lang/String;

    iget-object v1, p0, Lni0;->o:Ljava/lang/String;

    invoke-virtual {p0, p2, v1, p3}, Lni0;->c(Ljava/lang/String;Ljava/lang/String;Z)V

    :cond_5
    new-instance p2, Landroid/graphics/Paint;

    invoke-direct {p2}, Landroid/graphics/Paint;-><init>()V

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {p2, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    invoke-virtual {p2, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    const/high16 v1, -0x1000000

    invoke-virtual {p2, v1}, Landroid/graphics/Paint;->setColor(I)V

    iget v1, p0, Lni0;->c:I

    mul-int/lit8 v1, v1, 0x14

    iget-object v2, p0, Lni0;->j:Landroid/graphics/Paint;

    int-to-float v5, v1

    invoke-virtual {v2, v5}, Landroid/graphics/Paint;->setTextSize(F)V

    iget v2, p0, Lni0;->c:I

    mul-int/lit8 v2, v2, 0x17

    iget-object v5, p0, Lni0;->j:Landroid/graphics/Paint;

    sget-object v6, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v5, v6}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    if-eqz p3, :cond_6

    iget-object p3, p0, Lni0;->j:Landroid/graphics/Paint;

    sget-object v5, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    goto :goto_3

    :cond_6
    iget-object p3, p0, Lni0;->j:Landroid/graphics/Paint;

    sget-object v5, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    :goto_3
    invoke-virtual {p3, v5}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    iget-object p3, p0, Lni0;->i:Landroid/graphics/Canvas;

    iget v5, p0, Lni0;->h:I

    mul-int/lit8 v6, v2, 0x2

    add-int/2addr v5, v6

    int-to-float v5, v5

    iget-object v7, p0, Lni0;->e:[I

    iget v8, p0, Lni0;->d:I

    aget v7, v7, v8

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v7, v1

    add-int/lit8 v7, v7, -0x2

    int-to-float v7, v7

    iget-object v8, p0, Lni0;->j:Landroid/graphics/Paint;

    invoke-virtual {p3, p1, v5, v7, v8}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    :goto_4
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result p1

    if-ge v3, p1, :cond_7

    iget-object p1, p0, Lni0;->i:Landroid/graphics/Canvas;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Ljava/lang/String;

    iget v5, p0, Lni0;->h:I

    mul-int/lit8 v7, v2, 0x7

    add-int/2addr v5, v7

    int-to-float v5, v5

    iget-object v7, p0, Lni0;->e:[I

    iget v8, p0, Lni0;->d:I

    aget v7, v7, v8

    iget v8, p0, Lni0;->c:I

    mul-int/lit8 v8, v8, 0x1e

    mul-int/2addr v8, v3

    add-int/2addr v7, v8

    add-int/2addr v7, v1

    add-int/lit8 v7, v7, -0x2

    int-to-float v7, v7

    iget-object v8, p0, Lni0;->j:Landroid/graphics/Paint;

    invoke-virtual {p1, p3, v5, v7, v8}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    :cond_7
    iget-object p1, p0, Lni0;->i:Landroid/graphics/Canvas;

    iget p3, p0, Lni0;->h:I

    add-int v7, p3, v6

    iget-object p3, p0, Lni0;->e:[I

    iget v1, p0, Lni0;->d:I

    aget p3, p3, v1

    iget v1, p0, Lni0;->c:I

    mul-int/lit8 v1, v1, 0xf

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    sub-int/2addr v3, v4

    mul-int/2addr v1, v3

    add-int v8, p3, v1

    mul-int/lit8 v9, v2, 0x4

    iget p3, p0, Lni0;->c:I

    mul-int/lit8 p3, p3, 0x1e

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    mul-int v10, p3, v1

    move-object v5, p0

    move-object v6, p1

    move-object v11, p2

    invoke-virtual/range {v5 .. v11}, Lni0;->f(Landroid/graphics/Canvas;IIIILandroid/graphics/Paint;)V

    iget-object v6, p0, Lni0;->i:Landroid/graphics/Canvas;

    iget p1, p0, Lni0;->h:I

    mul-int/lit8 p3, v2, 0x7

    add-int v7, p1, p3

    iget-object p1, p0, Lni0;->e:[I

    iget p3, p0, Lni0;->d:I

    aget p1, p1, p3

    iget p3, p0, Lni0;->c:I

    mul-int/lit8 p3, p3, 0xf

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    sub-int/2addr v1, v4

    mul-int/2addr p3, v1

    add-int v8, p1, p3

    mul-int/lit8 v9, v2, 0x6

    iget p1, p0, Lni0;->c:I

    mul-int/lit8 p1, p1, 0x1e

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result p3

    mul-int v10, p1, p3

    invoke-virtual/range {v5 .. v11}, Lni0;->f(Landroid/graphics/Canvas;IIIILandroid/graphics/Paint;)V

    iget-object p1, p0, Lni0;->e:[I

    iget p2, p0, Lni0;->d:I

    aget p3, p1, p2

    iget v1, p0, Lni0;->c:I

    mul-int/lit8 v1, v1, 0x1e

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    mul-int/2addr v1, v0

    add-int/2addr p3, v1

    aput p3, p1, p2

    return-void
.end method

.method public d(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 0

    .line 1
    iput-object p1, p0, Lni0;->n:Ljava/lang/String;

    iput-object p2, p0, Lni0;->o:Ljava/lang/String;

    invoke-virtual {p0, p1, p2, p3}, Lni0;->c(Ljava/lang/String;Ljava/lang/String;Z)V

    return-void
.end method

.method public e(Ljava/lang/String;Z)V
    .locals 7

    .line 1
    new-instance v6, Landroid/graphics/Paint;

    invoke-direct {v6}, Landroid/graphics/Paint;-><init>()V

    sget-object p2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v6, p2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    const/4 p2, 0x1

    invoke-virtual {v6, p2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    const/high16 p2, -0x1000000

    invoke-virtual {v6, p2}, Landroid/graphics/Paint;->setColor(I)V

    iget p2, p0, Lni0;->c:I

    mul-int/lit8 p2, p2, 0x14

    iget-object v0, p0, Lni0;->j:Landroid/graphics/Paint;

    int-to-float v1, p2

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    iget v0, p0, Lni0;->c:I

    mul-int/lit8 v0, v0, 0x73

    iget-object v1, p0, Lni0;->j:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    iget-object v1, p0, Lni0;->j:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    iget-object v1, p0, Lni0;->i:Landroid/graphics/Canvas;

    iget v2, p0, Lni0;->h:I

    add-int/2addr v2, v0

    int-to-float v2, v2

    iget-object v3, p0, Lni0;->e:[I

    iget v4, p0, Lni0;->d:I

    aget v3, v3, v4

    div-int/lit8 p2, p2, 0x2

    add-int/2addr v3, p2

    add-int/lit8 v3, v3, -0x2

    int-to-float p2, v3

    iget-object v3, p0, Lni0;->j:Landroid/graphics/Paint;

    invoke-virtual {v1, p1, v2, p2, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    iget-object v1, p0, Lni0;->i:Landroid/graphics/Canvas;

    iget p1, p0, Lni0;->h:I

    add-int v2, p1, v0

    iget-object p1, p0, Lni0;->e:[I

    iget p2, p0, Lni0;->d:I

    aget v3, p1, p2

    mul-int/lit8 v4, v0, 0x2

    iget p1, p0, Lni0;->c:I

    mul-int/lit8 v5, p1, 0x1e

    move-object v0, p0

    invoke-virtual/range {v0 .. v6}, Lni0;->f(Landroid/graphics/Canvas;IIIILandroid/graphics/Paint;)V

    iget-object p1, p0, Lni0;->e:[I

    iget p2, p0, Lni0;->d:I

    aget v0, p1, p2

    iget v1, p0, Lni0;->c:I

    mul-int/lit8 v1, v1, 0x1e

    add-int/2addr v0, v1

    aput v0, p1, p2

    return-void
.end method

.method public final f(Landroid/graphics/Canvas;IIIILandroid/graphics/Paint;)V
    .locals 7

    .line 1
    div-int/lit8 p4, p4, 0x2

    sub-int v0, p2, p4

    int-to-float v2, v0

    div-int/lit8 p5, p5, 0x2

    sub-int v0, p3, p5

    int-to-float v3, v0

    add-int/2addr p2, p4

    int-to-float v4, p2

    add-int/2addr p3, p5

    int-to-float v5, p3

    move-object v1, p1

    move-object v6, p6

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    return-void
.end method

.method public g()Ljava/util/ArrayList;
    .locals 1

    .line 1
    iget-object v0, p0, Lni0;->a:Ljava/util/ArrayList;

    return-object v0
.end method

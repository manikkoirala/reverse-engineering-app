.class public Lgs$a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lgs;->d()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Landroid/widget/RadioButton;

.field public final synthetic b:Lgs;


# direct methods
.method public constructor <init>(Lgs;Landroid/widget/RadioButton;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lgs$a;->b:Lgs;

    iput-object p2, p0, Lgs$a;->a:Landroid/widget/RadioButton;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 11

    .line 1
    iget-object p2, p0, Lgs$a;->b:Lgs;

    invoke-static {p2}, Lgs;->a(Lgs;)Landroid/content/Context;

    move-result-object p2

    invoke-virtual {p2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p2

    const-string v0, "android_id"

    invoke-static {p2, v0}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    iget-object v0, p0, Lgs$a;->b:Lgs;

    invoke-static {v0}, Lgs;->a(Lgs;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/firebase/analytics/FirebaseAnalytics;->getInstance(Landroid/content/Context;)Lcom/google/firebase/analytics/FirebaseAnalytics;

    move-result-object v0

    iget-object v1, p0, Lgs$a;->a:Landroid/widget/RadioButton;

    invoke-virtual {v1}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lgs$a;->b:Lgs;

    invoke-static {v1}, Lgs;->b(Lgs;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    iget-object v3, p0, Lgs$a;->b:Lgs;

    invoke-static {v3}, Lgs;->a(Lgs;)Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getTemplateJson(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v4, p0, Lgs$a;->b:Lgs;

    invoke-static {v4}, Lgs;->a(Lgs;)Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;

    move-result-object v4

    invoke-virtual {v3}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->getFolderPath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v2, v3}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->deleteAllSheetImageForExam(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;Ljava/lang/String;)Z

    :cond_0
    const-string v2, "DELETE_SHEET_IMAGES"

    const-string v3, "ExamSheetImageDelete"

    invoke-static {v0, v2, p2, v3}, Lo4;->b(Lcom/google/firebase/analytics/FirebaseAnalytics;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    iget-object p2, p0, Lgs$a;->b:Lgs;

    invoke-static {p2}, Lgs;->a(Lgs;)Landroid/content/Context;

    move-result-object p2

    const v0, 0x7f0800cf

    const v1, 0x7f08016d

    const v2, 0x7f1201eb

    invoke-static {p2, v2, v0, v1}, La91;->G(Landroid/content/Context;III)V

    goto/16 :goto_2

    :cond_2
    iget-object v1, p0, Lgs$a;->b:Lgs;

    invoke-static {v1}, Lgs;->a(Lgs;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;

    move-result-object v1

    invoke-static {}, Lcom/google/firebase/auth/FirebaseAuth;->getInstance()Lcom/google/firebase/auth/FirebaseAuth;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/firebase/auth/FirebaseAuth;->e()Lr30;

    move-result-object v2

    invoke-virtual {v2}, Lr30;->O()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iget-object v4, p0, Lgs$a;->b:Lgs;

    invoke-static {v4}, Lgs;->b(Lgs;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    iget-object v6, p0, Lgs$a;->b:Lgs;

    invoke-static {v6}, Lgs;->a(Lgs;)Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;

    move-result-object v6

    invoke-virtual {v6, v5}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getTemplateJson(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;

    move-result-object v7

    if-eqz v7, :cond_3

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;->getOrgId()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_3

    invoke-virtual {v7}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->isCloudSyncOn()Z

    move-result v8

    if-eqz v8, :cond_3

    invoke-virtual {v7}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->getUserId()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_3

    iget-object v5, p0, Lgs$a;->b:Lgs;

    invoke-static {v5}, Lgs;->a(Lgs;)Landroid/content/Context;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v7, p0, Lgs$a;->b:Lgs;

    invoke-static {v7}, Lgs;->a(Lgs;)Landroid/content/Context;

    move-result-object v7

    const v8, 0x7f1201a1

    invoke-virtual {v7, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const v7, 0x7f0800bd

    const v8, 0x7f08016e

    invoke-static {v5, v6, v7, v8}, La91;->H(Landroid/content/Context;Ljava/lang/String;II)V

    goto :goto_1

    :cond_3
    if-eqz v7, :cond_4

    iget-object v8, p0, Lgs$a;->b:Lgs;

    invoke-static {v8}, Lgs;->a(Lgs;)Landroid/content/Context;

    move-result-object v8

    invoke-static {v8}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;

    move-result-object v8

    invoke-virtual {v7}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->getFolderPath()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v5, v9}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->deleteAllSheetImageForExam(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;Ljava/lang/String;)Z

    invoke-virtual {v7}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->isCloudSyncOn()Z

    move-result v8

    if-eqz v8, :cond_4

    new-instance v8, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/ExamAction;

    invoke-virtual {v7}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->getCloudId()Ljava/lang/Long;

    move-result-object v7

    const/4 v9, 0x0

    const/4 v10, 0x1

    invoke-direct {v8, v7, v9, v10}, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/ExamAction;-><init>(Ljava/lang/Long;ZZ)V

    invoke-virtual {v3, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_4
    invoke-virtual {v6, v5}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->deleteTemplateJson(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)Z

    iget-object v6, p0, Lgs$a;->b:Lgs;

    invoke-static {v6}, Lgs;->a(Lgs;)Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;

    move-result-object v6

    invoke-virtual {v6, v5}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->deleteAllResultForExam(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)Z

    const-string v5, "DELETE_EXAM"

    const-string v6, "ExamDeleteComplete"

    invoke-static {v0, v5, p2, v6}, Lo4;->b(Lcom/google/firebase/analytics/FirebaseAnalytics;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_5
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result p2

    if-lez p2, :cond_6

    iget-object p2, p0, Lgs$a;->b:Lgs;

    invoke-static {p2}, Lgs;->a(Lgs;)Landroid/content/Context;

    move-result-object p2

    invoke-static {p2, v3}, La91;->I(Landroid/content/Context;Ljava/util/ArrayList;)Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/ExamsPendingActionFile;

    :cond_6
    :goto_2
    iget-object p2, p0, Lgs$a;->b:Lgs;

    invoke-static {p2}, Lgs;->a(Lgs;)Landroid/content/Context;

    move-result-object p2

    invoke-static {p2}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;

    move-result-object p2

    invoke-virtual {p2}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->vaccumDataBase()Z

    iget-object p2, p0, Lgs$a;->b:Lgs;

    invoke-static {p2}, Lgs;->c(Lgs;)Ly01;

    move-result-object p2

    const/4 v0, 0x0

    invoke-interface {p2, v0}, Ly01;->a(Ljava/lang/Object;)V

    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    return-void
.end method

.class public Luf$j;
.super Lry1;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Luf;->createAnimator(Landroid/view/ViewGroup;Lxy1;Lxy1;)Landroid/animation/Animator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public a:Z

.field public final synthetic b:Landroid/view/ViewGroup;

.field public final synthetic c:Luf;


# direct methods
.method public constructor <init>(Luf;Landroid/view/ViewGroup;)V
    .locals 0

    .line 1
    iput-object p1, p0, Luf$j;->c:Luf;

    iput-object p2, p0, Luf$j;->b:Landroid/view/ViewGroup;

    invoke-direct {p0}, Lry1;-><init>()V

    const/4 p1, 0x0

    iput-boolean p1, p0, Luf$j;->a:Z

    return-void
.end method


# virtual methods
.method public onTransitionCancel(Lqy1;)V
    .locals 1

    .line 1
    iget-object p1, p0, Luf$j;->b:Landroid/view/ViewGroup;

    const/4 v0, 0x0

    invoke-static {p1, v0}, Lw32;->c(Landroid/view/ViewGroup;Z)V

    const/4 p1, 0x1

    iput-boolean p1, p0, Luf$j;->a:Z

    return-void
.end method

.method public onTransitionEnd(Lqy1;)V
    .locals 2

    .line 1
    iget-boolean v0, p0, Luf$j;->a:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Luf$j;->b:Landroid/view/ViewGroup;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lw32;->c(Landroid/view/ViewGroup;Z)V

    :cond_0
    invoke-virtual {p1, p0}, Lqy1;->removeListener(Lqy1$g;)Lqy1;

    return-void
.end method

.method public onTransitionPause(Lqy1;)V
    .locals 1

    .line 1
    iget-object p1, p0, Luf$j;->b:Landroid/view/ViewGroup;

    const/4 v0, 0x0

    invoke-static {p1, v0}, Lw32;->c(Landroid/view/ViewGroup;Z)V

    return-void
.end method

.method public onTransitionResume(Lqy1;)V
    .locals 1

    .line 1
    iget-object p1, p0, Luf$j;->b:Landroid/view/ViewGroup;

    const/4 v0, 0x1

    invoke-static {p1, v0}, Lw32;->c(Landroid/view/ViewGroup;Z)V

    return-void
.end method

.class public Lk10;
.super Lu80;
.source "SourceFile"


# instance fields
.field public a:Landroid/content/Context;

.field public b:Lj10;

.field public c:Lzx;

.field public d:Landroid/widget/RadioButton;

.field public e:Landroid/widget/RadioButton;

.field public f:Landroid/widget/RadioButton;

.field public g:Landroid/widget/RadioButton;

.field public h:Landroid/widget/RadioButton;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lzx;Lj10;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lu80;-><init>(Landroid/content/Context;)V

    iput-object p1, p0, Lk10;->a:Landroid/content/Context;

    iput-object p3, p0, Lk10;->b:Lj10;

    iput-object p2, p0, Lk10;->c:Lzx;

    return-void
.end method

.method public static synthetic a(Lk10;)Lj10;
    .locals 0

    .line 1
    iget-object p0, p0, Lk10;->b:Lj10;

    return-object p0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    .line 1
    invoke-super {p0, p1}, Lu80;->onCreate(Landroid/os/Bundle;)V

    const p1, 0x7f0c006b

    invoke-virtual {p0, p1}, Landroid/app/Dialog;->setContentView(I)V

    const p1, 0x7f090441

    invoke-virtual {p0, p1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroidx/appcompat/widget/Toolbar;

    new-instance v0, Lk10$a;

    invoke-direct {v0, p0}, Lk10$a;-><init>(Lk10;)V

    invoke-virtual {p1, v0}, Landroidx/appcompat/widget/Toolbar;->setNavigationOnClickListener(Landroid/view/View$OnClickListener;)V

    const p1, 0x7f0902ef

    invoke-virtual {p0, p1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/RadioButton;

    iput-object p1, p0, Lk10;->d:Landroid/widget/RadioButton;

    const p1, 0x7f0902f2

    invoke-virtual {p0, p1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/RadioButton;

    iput-object p1, p0, Lk10;->e:Landroid/widget/RadioButton;

    const p1, 0x7f0902ee

    invoke-virtual {p0, p1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/RadioButton;

    iput-object p1, p0, Lk10;->h:Landroid/widget/RadioButton;

    const p1, 0x7f0902ed

    invoke-virtual {p0, p1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/RadioButton;

    iput-object p1, p0, Lk10;->f:Landroid/widget/RadioButton;

    const p1, 0x7f0902f0

    invoke-virtual {p0, p1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/RadioButton;

    iput-object p1, p0, Lk10;->g:Landroid/widget/RadioButton;

    const p1, 0x7f090084

    invoke-virtual {p0, p1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object p1

    new-instance v0, Lk10$b;

    invoke-direct {v0, p0}, Lk10$b;-><init>(Lk10;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const p1, 0x7f0900a0

    invoke-virtual {p0, p1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object p1

    new-instance v0, Lk10$c;

    invoke-direct {v0, p0}, Lk10$c;-><init>(Lk10;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object p1, p0, Lk10;->c:Lzx;

    if-eqz p1, :cond_3

    iget-object v0, p0, Lk10;->g:Landroid/widget/RadioButton;

    iget-boolean p1, p1, Lzx;->b:Z

    const/4 v1, 0x1

    xor-int/2addr p1, v1

    invoke-virtual {v0, p1}, Landroid/widget/CompoundButton;->setChecked(Z)V

    iget-object p1, p0, Lk10;->f:Landroid/widget/RadioButton;

    iget-object v0, p0, Lk10;->c:Lzx;

    iget-boolean v0, v0, Lzx;->b:Z

    invoke-virtual {p1, v0}, Landroid/widget/CompoundButton;->setChecked(Z)V

    iget-object p1, p0, Lk10;->e:Landroid/widget/RadioButton;

    iget-object v0, p0, Lk10;->c:Lzx;

    iget-object v0, v0, Lzx;->a:Lcom/ekodroid/omrevaluator/util/DataModels/SortArchiveExamList$SortBy;

    sget-object v2, Lcom/ekodroid/omrevaluator/util/DataModels/SortArchiveExamList$SortBy;->NAME:Lcom/ekodroid/omrevaluator/util/DataModels/SortArchiveExamList$SortBy;

    const/4 v3, 0x0

    if-ne v0, v2, :cond_0

    move v0, v1

    goto :goto_0

    :cond_0
    move v0, v3

    :goto_0
    invoke-virtual {p1, v0}, Landroid/widget/CompoundButton;->setChecked(Z)V

    iget-object p1, p0, Lk10;->d:Landroid/widget/RadioButton;

    iget-object v0, p0, Lk10;->c:Lzx;

    iget-object v0, v0, Lzx;->a:Lcom/ekodroid/omrevaluator/util/DataModels/SortArchiveExamList$SortBy;

    sget-object v2, Lcom/ekodroid/omrevaluator/util/DataModels/SortArchiveExamList$SortBy;->DATE:Lcom/ekodroid/omrevaluator/util/DataModels/SortArchiveExamList$SortBy;

    if-ne v0, v2, :cond_1

    move v0, v1

    goto :goto_1

    :cond_1
    move v0, v3

    :goto_1
    invoke-virtual {p1, v0}, Landroid/widget/CompoundButton;->setChecked(Z)V

    iget-object p1, p0, Lk10;->h:Landroid/widget/RadioButton;

    iget-object v0, p0, Lk10;->c:Lzx;

    iget-object v0, v0, Lzx;->a:Lcom/ekodroid/omrevaluator/util/DataModels/SortArchiveExamList$SortBy;

    sget-object v2, Lcom/ekodroid/omrevaluator/util/DataModels/SortArchiveExamList$SortBy;->CLASS:Lcom/ekodroid/omrevaluator/util/DataModels/SortArchiveExamList$SortBy;

    if-ne v0, v2, :cond_2

    goto :goto_2

    :cond_2
    move v1, v3

    :goto_2
    invoke-virtual {p1, v1}, Landroid/widget/CompoundButton;->setChecked(Z)V

    :cond_3
    return-void
.end method

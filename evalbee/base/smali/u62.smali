.class public Lu62;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lu62$a;,
        Lu62$n;,
        Lu62$m;,
        Lu62$e;,
        Lu62$d;,
        Lu62$c;,
        Lu62$f;,
        Lu62$b;,
        Lu62$k;,
        Lu62$j;,
        Lu62$i;,
        Lu62$h;,
        Lu62$g;,
        Lu62$l;
    }
.end annotation


# static fields
.field public static final b:Lu62;


# instance fields
.field public final a:Lu62$l;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .line 1
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x1e

    if-lt v0, v1, :cond_0

    sget-object v0, Lu62$k;->q:Lu62;

    goto :goto_0

    :cond_0
    sget-object v0, Lu62$l;->b:Lu62;

    :goto_0
    sput-object v0, Lu62;->b:Lu62;

    return-void
.end method

.method public constructor <init>(Landroid/view/WindowInsets;)V
    .locals 2

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x1e

    if-lt v0, v1, :cond_0

    new-instance v0, Lu62$k;

    invoke-direct {v0, p0, p1}, Lu62$k;-><init>(Lu62;Landroid/view/WindowInsets;)V

    :goto_0
    iput-object v0, p0, Lu62;->a:Lu62$l;

    goto :goto_1

    :cond_0
    const/16 v1, 0x1d

    if-lt v0, v1, :cond_1

    new-instance v0, Lu62$j;

    invoke-direct {v0, p0, p1}, Lu62$j;-><init>(Lu62;Landroid/view/WindowInsets;)V

    goto :goto_0

    :cond_1
    const/16 v1, 0x1c

    if-lt v0, v1, :cond_2

    new-instance v0, Lu62$i;

    invoke-direct {v0, p0, p1}, Lu62$i;-><init>(Lu62;Landroid/view/WindowInsets;)V

    goto :goto_0

    :cond_2
    new-instance v0, Lu62$h;

    invoke-direct {v0, p0, p1}, Lu62$h;-><init>(Lu62;Landroid/view/WindowInsets;)V

    goto :goto_0

    :goto_1
    return-void
.end method

.method public constructor <init>(Lu62;)V
    .locals 2

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-eqz p1, :cond_5

    iget-object p1, p1, Lu62;->a:Lu62$l;

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x1e

    if-lt v0, v1, :cond_0

    instance-of v1, p1, Lu62$k;

    if-eqz v1, :cond_0

    new-instance v0, Lu62$k;

    move-object v1, p1

    check-cast v1, Lu62$k;

    invoke-direct {v0, p0, v1}, Lu62$k;-><init>(Lu62;Lu62$k;)V

    :goto_0
    iput-object v0, p0, Lu62;->a:Lu62$l;

    goto :goto_1

    :cond_0
    const/16 v1, 0x1d

    if-lt v0, v1, :cond_1

    instance-of v1, p1, Lu62$j;

    if-eqz v1, :cond_1

    new-instance v0, Lu62$j;

    move-object v1, p1

    check-cast v1, Lu62$j;

    invoke-direct {v0, p0, v1}, Lu62$j;-><init>(Lu62;Lu62$j;)V

    goto :goto_0

    :cond_1
    const/16 v1, 0x1c

    if-lt v0, v1, :cond_2

    instance-of v0, p1, Lu62$i;

    if-eqz v0, :cond_2

    new-instance v0, Lu62$i;

    move-object v1, p1

    check-cast v1, Lu62$i;

    invoke-direct {v0, p0, v1}, Lu62$i;-><init>(Lu62;Lu62$i;)V

    goto :goto_0

    :cond_2
    instance-of v0, p1, Lu62$h;

    if-eqz v0, :cond_3

    new-instance v0, Lu62$h;

    move-object v1, p1

    check-cast v1, Lu62$h;

    invoke-direct {v0, p0, v1}, Lu62$h;-><init>(Lu62;Lu62$h;)V

    goto :goto_0

    :cond_3
    instance-of v0, p1, Lu62$g;

    if-eqz v0, :cond_4

    new-instance v0, Lu62$g;

    move-object v1, p1

    check-cast v1, Lu62$g;

    invoke-direct {v0, p0, v1}, Lu62$g;-><init>(Lu62;Lu62$g;)V

    goto :goto_0

    :cond_4
    new-instance v0, Lu62$l;

    invoke-direct {v0, p0}, Lu62$l;-><init>(Lu62;)V

    goto :goto_0

    :goto_1
    invoke-virtual {p1, p0}, Lu62$l;->e(Lu62;)V

    goto :goto_2

    :cond_5
    new-instance p1, Lu62$l;

    invoke-direct {p1, p0}, Lu62$l;-><init>(Lu62;)V

    iput-object p1, p0, Lu62;->a:Lu62$l;

    :goto_2
    return-void
.end method

.method public static n(Lnf0;IIII)Lnf0;
    .locals 5

    .line 1
    iget v0, p0, Lnf0;->a:I

    sub-int/2addr v0, p1

    const/4 v1, 0x0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iget v2, p0, Lnf0;->b:I

    sub-int/2addr v2, p2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    iget v3, p0, Lnf0;->c:I

    sub-int/2addr v3, p3

    invoke-static {v1, v3}, Ljava/lang/Math;->max(II)I

    move-result v3

    iget v4, p0, Lnf0;->d:I

    sub-int/2addr v4, p4

    invoke-static {v1, v4}, Ljava/lang/Math;->max(II)I

    move-result v1

    if-ne v0, p1, :cond_0

    if-ne v2, p2, :cond_0

    if-ne v3, p3, :cond_0

    if-ne v1, p4, :cond_0

    return-object p0

    :cond_0
    invoke-static {v0, v2, v3, v1}, Lnf0;->b(IIII)Lnf0;

    move-result-object p0

    return-object p0
.end method

.method public static v(Landroid/view/WindowInsets;)Lu62;
    .locals 1

    .line 1
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lu62;->w(Landroid/view/WindowInsets;Landroid/view/View;)Lu62;

    move-result-object p0

    return-object p0
.end method

.method public static w(Landroid/view/WindowInsets;Landroid/view/View;)Lu62;
    .locals 1

    .line 1
    new-instance v0, Lu62;

    invoke-static {p0}, Ll71;->g(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Landroid/view/WindowInsets;

    invoke-direct {v0, p0}, Lu62;-><init>(Landroid/view/WindowInsets;)V

    if-eqz p1, :cond_0

    invoke-static {p1}, Lo32;->T(Landroid/view/View;)Z

    move-result p0

    if-eqz p0, :cond_0

    invoke-static {p1}, Lo32;->H(Landroid/view/View;)Lu62;

    move-result-object p0

    invoke-virtual {v0, p0}, Lu62;->s(Lu62;)V

    invoke-virtual {p1}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object p0

    invoke-virtual {v0, p0}, Lu62;->d(Landroid/view/View;)V

    :cond_0
    return-object v0
.end method


# virtual methods
.method public a()Lu62;
    .locals 1

    .line 1
    iget-object v0, p0, Lu62;->a:Lu62$l;

    invoke-virtual {v0}, Lu62$l;->a()Lu62;

    move-result-object v0

    return-object v0
.end method

.method public b()Lu62;
    .locals 1

    .line 1
    iget-object v0, p0, Lu62;->a:Lu62$l;

    invoke-virtual {v0}, Lu62$l;->b()Lu62;

    move-result-object v0

    return-object v0
.end method

.method public c()Lu62;
    .locals 1

    .line 1
    iget-object v0, p0, Lu62;->a:Lu62$l;

    invoke-virtual {v0}, Lu62$l;->c()Lu62;

    move-result-object v0

    return-object v0
.end method

.method public d(Landroid/view/View;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lu62;->a:Lu62$l;

    invoke-virtual {v0, p1}, Lu62$l;->d(Landroid/view/View;)V

    return-void
.end method

.method public e()Lrt;
    .locals 1

    .line 1
    iget-object v0, p0, Lu62;->a:Lu62$l;

    invoke-virtual {v0}, Lu62$l;->f()Lrt;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    .line 1
    if-ne p0, p1, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_0
    instance-of v0, p1, Lu62;

    if-nez v0, :cond_1

    const/4 p1, 0x0

    return p1

    :cond_1
    check-cast p1, Lu62;

    iget-object v0, p0, Lu62;->a:Lu62$l;

    iget-object p1, p1, Lu62;->a:Lu62$l;

    invoke-static {v0, p1}, Lc11;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public f(I)Lnf0;
    .locals 1

    .line 1
    iget-object v0, p0, Lu62;->a:Lu62$l;

    invoke-virtual {v0, p1}, Lu62$l;->g(I)Lnf0;

    move-result-object p1

    return-object p1
.end method

.method public g()Lnf0;
    .locals 1

    .line 1
    iget-object v0, p0, Lu62;->a:Lu62$l;

    invoke-virtual {v0}, Lu62$l;->i()Lnf0;

    move-result-object v0

    return-object v0
.end method

.method public h()I
    .locals 1

    .line 1
    iget-object v0, p0, Lu62;->a:Lu62$l;

    invoke-virtual {v0}, Lu62$l;->k()Lnf0;

    move-result-object v0

    iget v0, v0, Lnf0;->d:I

    return v0
.end method

.method public hashCode()I
    .locals 1

    .line 1
    iget-object v0, p0, Lu62;->a:Lu62$l;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Lu62$l;->hashCode()I

    move-result v0

    :goto_0
    return v0
.end method

.method public i()I
    .locals 1

    .line 1
    iget-object v0, p0, Lu62;->a:Lu62$l;

    invoke-virtual {v0}, Lu62$l;->k()Lnf0;

    move-result-object v0

    iget v0, v0, Lnf0;->a:I

    return v0
.end method

.method public j()I
    .locals 1

    .line 1
    iget-object v0, p0, Lu62;->a:Lu62$l;

    invoke-virtual {v0}, Lu62$l;->k()Lnf0;

    move-result-object v0

    iget v0, v0, Lnf0;->c:I

    return v0
.end method

.method public k()I
    .locals 1

    .line 1
    iget-object v0, p0, Lu62;->a:Lu62$l;

    invoke-virtual {v0}, Lu62$l;->k()Lnf0;

    move-result-object v0

    iget v0, v0, Lnf0;->b:I

    return v0
.end method

.method public l()Z
    .locals 2

    .line 1
    iget-object v0, p0, Lu62;->a:Lu62$l;

    invoke-virtual {v0}, Lu62$l;->k()Lnf0;

    move-result-object v0

    sget-object v1, Lnf0;->e:Lnf0;

    invoke-virtual {v0, v1}, Lnf0;->equals(Ljava/lang/Object;)Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public m(IIII)Lu62;
    .locals 1

    .line 1
    iget-object v0, p0, Lu62;->a:Lu62$l;

    invoke-virtual {v0, p1, p2, p3, p4}, Lu62$l;->m(IIII)Lu62;

    move-result-object p1

    return-object p1
.end method

.method public o()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lu62;->a:Lu62$l;

    invoke-virtual {v0}, Lu62$l;->n()Z

    move-result v0

    return v0
.end method

.method public p(IIII)Lu62;
    .locals 1

    .line 1
    new-instance v0, Lu62$b;

    invoke-direct {v0, p0}, Lu62$b;-><init>(Lu62;)V

    invoke-static {p1, p2, p3, p4}, Lnf0;->b(IIII)Lnf0;

    move-result-object p1

    invoke-virtual {v0, p1}, Lu62$b;->d(Lnf0;)Lu62$b;

    move-result-object p1

    invoke-virtual {p1}, Lu62$b;->a()Lu62;

    move-result-object p1

    return-object p1
.end method

.method public q([Lnf0;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lu62;->a:Lu62$l;

    invoke-virtual {v0, p1}, Lu62$l;->p([Lnf0;)V

    return-void
.end method

.method public r(Lnf0;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lu62;->a:Lu62$l;

    invoke-virtual {v0, p1}, Lu62$l;->q(Lnf0;)V

    return-void
.end method

.method public s(Lu62;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lu62;->a:Lu62$l;

    invoke-virtual {v0, p1}, Lu62$l;->r(Lu62;)V

    return-void
.end method

.method public t(Lnf0;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lu62;->a:Lu62$l;

    invoke-virtual {v0, p1}, Lu62$l;->s(Lnf0;)V

    return-void
.end method

.method public u()Landroid/view/WindowInsets;
    .locals 2

    .line 1
    iget-object v0, p0, Lu62;->a:Lu62$l;

    instance-of v1, v0, Lu62$g;

    if-eqz v1, :cond_0

    check-cast v0, Lu62$g;

    iget-object v0, v0, Lu62$g;->c:Landroid/view/WindowInsets;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.class public Le20;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lfc0;


# static fields
.field public static final d:Lio/grpc/Metadata$Key;

.field public static final e:Lio/grpc/Metadata$Key;

.field public static final f:Lio/grpc/Metadata$Key;


# instance fields
.field public final a:Lr91;

.field public final b:Lr91;

.field public final c:Le30;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .line 1
    sget-object v0, Lio/grpc/Metadata;->ASCII_STRING_MARSHALLER:Lio/grpc/Metadata$AsciiMarshaller;

    const-string v1, "x-firebase-client-log-type"

    invoke-static {v1, v0}, Lio/grpc/Metadata$Key;->of(Ljava/lang/String;Lio/grpc/Metadata$AsciiMarshaller;)Lio/grpc/Metadata$Key;

    move-result-object v1

    sput-object v1, Le20;->d:Lio/grpc/Metadata$Key;

    const-string v1, "x-firebase-client"

    invoke-static {v1, v0}, Lio/grpc/Metadata$Key;->of(Ljava/lang/String;Lio/grpc/Metadata$AsciiMarshaller;)Lio/grpc/Metadata$Key;

    move-result-object v1

    sput-object v1, Le20;->e:Lio/grpc/Metadata$Key;

    const-string v1, "x-firebase-gmpid"

    invoke-static {v1, v0}, Lio/grpc/Metadata$Key;->of(Ljava/lang/String;Lio/grpc/Metadata$AsciiMarshaller;)Lio/grpc/Metadata$Key;

    move-result-object v0

    sput-object v0, Le20;->f:Lio/grpc/Metadata$Key;

    return-void
.end method

.method public constructor <init>(Lr91;Lr91;Le30;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Le20;->b:Lr91;

    iput-object p2, p0, Le20;->a:Lr91;

    iput-object p3, p0, Le20;->c:Le30;

    return-void
.end method


# virtual methods
.method public a(Lio/grpc/Metadata;)V
    .locals 2

    .line 1
    iget-object v0, p0, Le20;->a:Lr91;

    invoke-interface {v0}, Lr91;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Le20;->b:Lr91;

    invoke-interface {v0}, Lr91;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Le20;->a:Lr91;

    invoke-interface {v0}, Lr91;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/firebase/heartbeatinfo/HeartBeatInfo;

    const-string v1, "fire-fst"

    invoke-interface {v0, v1}, Lcom/google/firebase/heartbeatinfo/HeartBeatInfo;->a(Ljava/lang/String;)Lcom/google/firebase/heartbeatinfo/HeartBeatInfo$HeartBeat;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/firebase/heartbeatinfo/HeartBeatInfo$HeartBeat;->getCode()I

    move-result v0

    if-eqz v0, :cond_1

    sget-object v1, Le20;->d:Lio/grpc/Metadata$Key;

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lio/grpc/Metadata;->put(Lio/grpc/Metadata$Key;Ljava/lang/Object;)V

    :cond_1
    sget-object v0, Le20;->e:Lio/grpc/Metadata$Key;

    iget-object v1, p0, Le20;->b:Lr91;

    invoke-interface {v1}, Lr91;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lv12;

    invoke-interface {v1}, Lv12;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lio/grpc/Metadata;->put(Lio/grpc/Metadata$Key;Ljava/lang/Object;)V

    invoke-virtual {p0, p1}, Le20;->b(Lio/grpc/Metadata;)V

    :cond_2
    :goto_0
    return-void
.end method

.method public final b(Lio/grpc/Metadata;)V
    .locals 2

    .line 1
    iget-object v0, p0, Le20;->c:Le30;

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {v0}, Le30;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_1

    sget-object v1, Le20;->f:Lio/grpc/Metadata$Key;

    invoke-virtual {p1, v1, v0}, Lio/grpc/Metadata;->put(Lio/grpc/Metadata$Key;Ljava/lang/Object;)V

    :cond_1
    return-void
.end method

.class public final Lvy0;
.super Landroidx/work/impl/constraints/controllers/ConstraintController;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lvy0$a;
    }
.end annotation


# static fields
.field public static final c:Lvy0$a;

.field public static final d:Ljava/lang/String;


# instance fields
.field public final b:I


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lvy0$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lvy0$a;-><init>(Lgq;)V

    sput-object v0, Lvy0;->c:Lvy0$a;

    const-string v0, "NetworkMeteredCtrlr"

    invoke-static {v0}, Lxl0;->i(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "tagWithPrefix(\"NetworkMeteredCtrlr\")"

    invoke-static {v0, v1}, Lfg0;->d(Ljava/lang/Object;Ljava/lang/String;)V

    sput-object v0, Lvy0;->d:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ltk;)V
    .locals 1

    .line 1
    const-string v0, "tracker"

    invoke-static {p1, v0}, Lfg0;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Landroidx/work/impl/constraints/controllers/ConstraintController;-><init>(Ltk;)V

    const/4 p1, 0x7

    iput p1, p0, Lvy0;->b:I

    return-void
.end method


# virtual methods
.method public b()I
    .locals 1

    .line 1
    iget v0, p0, Lvy0;->b:I

    return v0
.end method

.method public c(Lp92;)Z
    .locals 1

    .line 1
    const-string v0, "workSpec"

    invoke-static {p1, v0}, Lfg0;->e(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p1, p1, Lp92;->j:Lzk;

    invoke-virtual {p1}, Lzk;->d()Landroidx/work/NetworkType;

    move-result-object p1

    sget-object v0, Landroidx/work/NetworkType;->METERED:Landroidx/work/NetworkType;

    if-ne p1, v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public bridge synthetic e(Ljava/lang/Object;)Z
    .locals 0

    .line 1
    check-cast p1, Lzy0;

    invoke-virtual {p0, p1}, Lvy0;->g(Lzy0;)Z

    move-result p1

    return p1
.end method

.method public g(Lzy0;)Z
    .locals 5

    .line 1
    const-string v0, "value"

    invoke-static {p1, v0}, Lfg0;->e(Ljava/lang/Object;Ljava/lang/String;)V

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x1a

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-ge v0, v1, :cond_0

    invoke-static {}, Lxl0;->e()Lxl0;

    move-result-object v0

    sget-object v1, Lvy0;->d:Ljava/lang/String;

    const-string v4, "Metered network constraint is not supported before API 26, only checking for connected state."

    invoke-virtual {v0, v1, v4}, Lxl0;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p1}, Lzy0;->a()Z

    move-result p1

    if-nez p1, :cond_2

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Lzy0;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lzy0;->b()Z

    move-result p1

    if-nez p1, :cond_2

    :cond_1
    :goto_0
    move v2, v3

    :cond_2
    return v2
.end method

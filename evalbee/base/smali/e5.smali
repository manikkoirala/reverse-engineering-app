.class public abstract Le5;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static A(Ljava/lang/String;Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;)[Z
    .locals 3

    .line 1
    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getThreeOptionLabels()[Ljava/lang/String;

    move-result-object p1

    array-length v0, p1

    new-array v0, v0, [Z

    const/4 v1, 0x0

    :goto_0
    array-length v2, p1

    if-ge v1, v2, :cond_1

    aget-object v2, p1, v1

    invoke-virtual {p0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    aput-boolean v2, v0, v1

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method public static B([ZLcom/ekodroid/omrevaluator/templateui/models/LabelProfile;)Ljava/lang/String;
    .locals 3

    .line 1
    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getTrueOrFalseLabel()[Ljava/lang/String;

    move-result-object p1

    const-string v0, ""

    const/4 v1, 0x0

    :goto_0
    array-length v2, p0

    if-ge v1, v2, :cond_2

    aget-boolean v2, p0, v1

    if-eqz v2, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, ", "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-object v0, p1, v1

    goto :goto_1

    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-object v0, p1, v1

    :goto_1
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    return-object v0
.end method

.method public static C(Ljava/lang/String;Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;)[Z
    .locals 3

    .line 1
    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getTrueOrFalseLabel()[Ljava/lang/String;

    move-result-object p1

    array-length v0, p1

    new-array v0, v0, [Z

    const/4 v1, 0x0

    :goto_0
    array-length v2, p1

    if-ge v1, v2, :cond_1

    aget-object v2, p1, v1

    invoke-virtual {p0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    aput-boolean v2, v0, v1

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method public static D(Ljava/util/ArrayList;)V
    .locals 1

    .line 1
    new-instance v0, Le5$a;

    invoke-direct {v0}, Le5$a;-><init>()V

    invoke-static {p0, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    return-void
.end method

.method public static E(Ljava/util/ArrayList;)V
    .locals 1

    .line 1
    new-instance v0, Le5$b;

    invoke-direct {v0}, Le5$b;-><init>()V

    invoke-static {p0, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    return-void
.end method

.method public static a([Z)[Z
    .locals 4

    .line 1
    if-nez p0, :cond_0

    const/4 p0, 0x0

    return-object p0

    :cond_0
    array-length v0, p0

    new-array v1, v0, [Z

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_1

    aget-boolean v3, p0, v2

    aput-boolean v3, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    return-object v1
.end method

.method public static b(Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;)D
    .locals 19

    .line 1
    invoke-virtual/range {p0 .. p0}, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->getMarkedState()Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;

    move-result-object v0

    sget-object v1, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;->UNATTEMPTED:Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;

    const-wide/16 v2, 0x0

    if-ne v0, v1, :cond_0

    return-wide v2

    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->getMarkedValues()[Z

    move-result-object v0

    invoke-virtual/range {p0 .. p0}, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->getPayload()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lbc;->b(Ljava/lang/String;)Lcom/ekodroid/omrevaluator/templateui/models/DecimalOptionPayload;

    move-result-object v1

    iget v4, v1, Lcom/ekodroid/omrevaluator/templateui/models/DecimalOptionPayload;->digits:I

    iget-boolean v5, v1, Lcom/ekodroid/omrevaluator/templateui/models/DecimalOptionPayload;->decimalAllowed:Z

    if-eqz v5, :cond_1

    const/16 v6, 0xb

    goto :goto_0

    :cond_1
    const/16 v6, 0xa

    :goto_0
    const-string v7, " "

    if-eqz v5, :cond_2

    const-string v8, "."

    const-string v9, "0"

    const-string v10, "1"

    const-string v11, "2"

    const-string v12, "3"

    const-string v13, "4"

    const-string v14, "5"

    const-string v15, "6"

    const-string v16, "7"

    const-string v17, "8"

    const-string v18, "9"

    filled-new-array/range {v7 .. v18}, [Ljava/lang/String;

    move-result-object v5

    goto :goto_1

    :cond_2
    const-string v8, "0"

    const-string v9, "1"

    const-string v10, "2"

    const-string v11, "3"

    const-string v12, "4"

    const-string v13, "5"

    const-string v14, "6"

    const-string v15, "7"

    const-string v16, "8"

    const-string v17, "9"

    filled-new-array/range {v7 .. v17}, [Ljava/lang/String;

    move-result-object v5

    :goto_1
    const/4 v7, 0x0

    const-string v8, ""

    move v9, v7

    :goto_2
    if-ge v9, v4, :cond_5

    move v10, v7

    move v11, v10

    :goto_3
    if-ge v10, v6, :cond_4

    mul-int v12, v10, v4

    add-int/2addr v12, v9

    aget-boolean v12, v0, v12

    if-eqz v12, :cond_3

    add-int/lit8 v11, v10, 0x1

    :cond_3
    add-int/lit8 v10, v10, 0x1

    goto :goto_3

    :cond_4
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-object v8, v5, v11

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    add-int/lit8 v9, v9, 0x1

    goto :goto_2

    :cond_5
    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    const-string v5, "[ ]"

    const-string v6, "0"

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iget-boolean v7, v1, Lcom/ekodroid/omrevaluator/templateui/models/DecimalOptionPayload;->decimalAllowed:Z

    if-eqz v7, :cond_6

    const-string v7, "#"

    const-string v8, "[.]"

    invoke-virtual {v4, v8, v7}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v8, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "[#]"

    const-string v6, "."

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    :cond_6
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v5

    if-nez v5, :cond_7

    return-wide v2

    :cond_7
    invoke-static {v4}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v2

    iget-boolean v1, v1, Lcom/ekodroid/omrevaluator/templateui/models/DecimalOptionPayload;->hasNegetive:Z

    if-eqz v1, :cond_8

    array-length v1, v0

    add-int/lit8 v1, v1, -0x1

    aget-boolean v0, v0, v1

    if-eqz v0, :cond_8

    const-wide/high16 v0, -0x4010000000000000L    # -1.0

    mul-double/2addr v2, v0

    :cond_8
    return-wide v2
.end method

.method public static c(Ljava/lang/String;Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;Ljava/lang/String;Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;)[Z
    .locals 1

    .line 1
    sget-object v0, Le5$c;->a:[I

    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result p1

    aget p1, v0, p1

    packed-switch p1, :pswitch_data_0

    const/4 p0, 0x0

    return-object p0

    :pswitch_0
    invoke-static {p0, p3}, Le5;->y(Ljava/lang/String;Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;)[Z

    move-result-object p0

    return-object p0

    :pswitch_1
    invoke-static {p0, p3}, Le5;->f(Ljava/lang/String;Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;)[Z

    move-result-object p0

    return-object p0

    :pswitch_2
    invoke-static {p0, p3}, Le5;->w(Ljava/lang/String;Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;)[Z

    move-result-object p0

    return-object p0

    :pswitch_3
    invoke-static {p0, p3, p2}, Le5;->o(Ljava/lang/String;Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;Ljava/lang/String;)[Z

    move-result-object p0

    return-object p0

    :pswitch_4
    invoke-static {p0, p3, p2}, Le5;->n(Ljava/lang/String;Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;Ljava/lang/String;)[Z

    move-result-object p0

    return-object p0

    :pswitch_5
    invoke-static {p0, p3}, Le5;->C(Ljava/lang/String;Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;)[Z

    move-result-object p0

    return-object p0

    :pswitch_6
    invoke-static {p0, p3}, Le5;->A(Ljava/lang/String;Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;)[Z

    move-result-object p0

    return-object p0

    :pswitch_7
    invoke-static {p0, p3}, Le5;->h(Ljava/lang/String;Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;)[Z

    move-result-object p0

    return-object p0

    :pswitch_8
    invoke-static {p0, p3}, Le5;->k(Ljava/lang/String;Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;)[Z

    move-result-object p0

    return-object p0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static d(Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    .line 1
    :try_start_0
    const-string v0, "[,]"

    invoke-virtual {p0, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p0

    array-length v0, p0

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    aget-object p0, p0, v1

    invoke-static {p0}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v0

    new-instance p0, Lgc0;

    invoke-direct {p0}, Lgc0;-><init>()V

    new-instance v2, Lcom/ekodroid/omrevaluator/templateui/models/DecimalRange;

    invoke-direct {v2, v0, v1, v0, v1}, Lcom/ekodroid/omrevaluator/templateui/models/DecimalRange;-><init>(DD)V

    invoke-virtual {p0, v2}, Lgc0;->s(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_0
    array-length v0, p0

    const/4 v3, 0x2

    if-ne v0, v3, :cond_1

    aget-object v0, p0, v1

    invoke-static {v0}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v0

    aget-object p0, p0, v2

    invoke-static {p0}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v2

    new-instance p0, Lgc0;

    invoke-direct {p0}, Lgc0;-><init>()V

    new-instance v4, Lcom/ekodroid/omrevaluator/templateui/models/DecimalRange;

    invoke-direct {v4, v0, v1, v2, v3}, Lcom/ekodroid/omrevaluator/templateui/models/DecimalRange;-><init>(DD)V

    invoke-virtual {p0, v4}, Lgc0;->s(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    :cond_1
    const/4 p0, 0x0

    return-object p0
.end method

.method public static e([ZLcom/ekodroid/omrevaluator/templateui/models/LabelProfile;)Ljava/lang/String;
    .locals 3

    .line 1
    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getEightOptionLabels()[Ljava/lang/String;

    move-result-object p1

    const-string v0, ""

    const/4 v1, 0x0

    :goto_0
    array-length v2, p0

    if-ge v1, v2, :cond_2

    aget-boolean v2, p0, v1

    if-eqz v2, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, ", "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-object v0, p1, v1

    goto :goto_1

    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-object v0, p1, v1

    :goto_1
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    return-object v0
.end method

.method public static f(Ljava/lang/String;Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;)[Z
    .locals 3

    .line 1
    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getEightOptionLabels()[Ljava/lang/String;

    move-result-object p1

    array-length v0, p1

    new-array v0, v0, [Z

    const/4 v1, 0x0

    :goto_0
    array-length v2, p1

    if-ge v1, v2, :cond_1

    aget-object v2, p1, v1

    invoke-virtual {p0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    aput-boolean v2, v0, v1

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method public static g([ZLcom/ekodroid/omrevaluator/templateui/models/LabelProfile;)Ljava/lang/String;
    .locals 3

    .line 1
    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getFiveOptionLabels()[Ljava/lang/String;

    move-result-object p1

    const-string v0, ""

    const/4 v1, 0x0

    :goto_0
    array-length v2, p0

    if-ge v1, v2, :cond_2

    aget-boolean v2, p0, v1

    if-eqz v2, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, ", "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-object v0, p1, v1

    goto :goto_1

    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-object v0, p1, v1

    :goto_1
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    return-object v0
.end method

.method public static h(Ljava/lang/String;Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;)[Z
    .locals 3

    .line 1
    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getFiveOptionLabels()[Ljava/lang/String;

    move-result-object p1

    array-length v0, p1

    new-array v0, v0, [Z

    const/4 v1, 0x0

    :goto_0
    array-length v2, p1

    if-ge v1, v2, :cond_1

    aget-object v2, p1, v1

    invoke-virtual {p0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    aput-boolean v2, v0, v1

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method public static i(D)Ljava/lang/String;
    .locals 2

    .line 1
    new-instance v0, Ljava/text/DecimalFormat;

    const-string v1, "0.#"

    invoke-direct {v0, v1}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0, p1}, Ljava/text/NumberFormat;->format(D)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static j([ZLcom/ekodroid/omrevaluator/templateui/models/LabelProfile;)Ljava/lang/String;
    .locals 3

    .line 1
    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getFourOptionLabels()[Ljava/lang/String;

    move-result-object p1

    const-string v0, ""

    const/4 v1, 0x0

    :goto_0
    array-length v2, p0

    if-ge v1, v2, :cond_2

    aget-boolean v2, p0, v1

    if-eqz v2, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, ", "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-object v0, p1, v1

    goto :goto_1

    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-object v0, p1, v1

    :goto_1
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    return-object v0
.end method

.method public static k(Ljava/lang/String;Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;)[Z
    .locals 3

    .line 1
    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getFourOptionLabels()[Ljava/lang/String;

    move-result-object p1

    array-length v0, p1

    new-array v0, v0, [Z

    const/4 v1, 0x0

    :goto_0
    array-length v2, p1

    if-ge v1, v2, :cond_1

    aget-object v2, p1, v1

    invoke-virtual {p0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    aput-boolean v2, v0, v1

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method public static l(C)I
    .locals 0

    .line 1
    :try_start_0
    invoke-static {p0}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result p0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return p0

    :catch_0
    const/4 p0, 0x0

    return p0
.end method

.method public static m([ZLcom/ekodroid/omrevaluator/templateui/models/LabelProfile;Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    .line 1
    invoke-static {p2}, Lbc;->c(Ljava/lang/String;)Lcom/ekodroid/omrevaluator/templateui/models/MatrixOptionPayload;

    move-result-object p2

    if-eqz p2, :cond_0

    iget p2, p2, Lcom/ekodroid/omrevaluator/templateui/models/MatrixOptionPayload;->secondaryOptions:I

    goto :goto_0

    :cond_0
    const/4 p2, 0x5

    :goto_0
    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getMatrixPrimary()[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getMatrixSecondary()[Ljava/lang/String;

    move-result-object p1

    const-string v1, ""

    const/4 v2, 0x0

    :goto_1
    array-length v3, p0

    if-ge v2, v3, :cond_3

    div-int v3, v2, p2

    rem-int v4, v2, p2

    aget-boolean v5, p0, v2

    if-eqz v5, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v5

    if-lez v5, :cond_1

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", "

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-object v1, v0, v3

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-object v1, p1, v4

    goto :goto_2

    :cond_1
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-object v1, v0, v3

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-object v1, p1, v4

    :goto_2
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_3
    return-object v1
.end method

.method public static n(Ljava/lang/String;Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;Ljava/lang/String;)[Z
    .locals 9

    .line 1
    invoke-static {p2}, Lbc;->c(Ljava/lang/String;)Lcom/ekodroid/omrevaluator/templateui/models/MatrixOptionPayload;

    move-result-object p2

    if-eqz p2, :cond_0

    iget v0, p2, Lcom/ekodroid/omrevaluator/templateui/models/MatrixOptionPayload;->primaryOptions:I

    iget p2, p2, Lcom/ekodroid/omrevaluator/templateui/models/MatrixOptionPayload;->secondaryOptions:I

    goto :goto_0

    :cond_0
    const/4 v0, 0x4

    const/4 p2, 0x5

    :goto_0
    mul-int/2addr v0, p2

    new-array v1, v0, [Z

    new-array v2, v0, [Ljava/lang/String;

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getMatrixPrimary()[Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getMatrixSecondary()[Ljava/lang/String;

    move-result-object p1

    const/4 v4, 0x0

    move v5, v4

    :goto_1
    if-ge v5, v0, :cond_1

    div-int v6, v5, p2

    rem-int v7, v5, p2

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    aget-object v6, v3, v6

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-object v6, p1, v7

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v2, v5

    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    :cond_1
    :goto_2
    if-ge v4, v0, :cond_3

    aget-object p1, v2, v4

    invoke-virtual {p0, p1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_2

    const/4 p1, 0x1

    aput-boolean p1, v1, v4

    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    :cond_3
    return-object v1
.end method

.method public static o(Ljava/lang/String;Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;Ljava/lang/String;)[Z
    .locals 7

    .line 1
    invoke-static {p2}, Lbc;->d(Ljava/lang/String;)Lcom/ekodroid/omrevaluator/templateui/models/NumericalOptionPayload;

    move-result-object p1

    iget p2, p1, Lcom/ekodroid/omrevaluator/templateui/models/NumericalOptionPayload;->digits:I

    mul-int/lit8 p2, p2, 0xa

    iget-boolean v0, p1, Lcom/ekodroid/omrevaluator/templateui/models/NumericalOptionPayload;->hasNegetive:Z

    if-eqz v0, :cond_0

    add-int/lit8 p2, p2, 0x1

    :cond_0
    new-array v0, p2, [Z

    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0x2d

    const/4 v4, 0x1

    if-ne v2, v3, :cond_1

    sub-int/2addr p2, v4

    aput-boolean v4, v0, p2

    invoke-virtual {p0, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p0

    :cond_1
    const-string p2, "[.]"

    invoke-virtual {p0, p2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p0

    aget-object p2, p0, v1

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result p2

    iget v2, p1, Lcom/ekodroid/omrevaluator/templateui/models/NumericalOptionPayload;->digits:I

    iget v3, p1, Lcom/ekodroid/omrevaluator/templateui/models/NumericalOptionPayload;->decimalDigits:I

    sub-int/2addr v2, v3

    const-string v3, "0"

    const-string v5, ""

    if-le p2, v2, :cond_2

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-object v2, p0, v1

    iget v5, p1, Lcom/ekodroid/omrevaluator/templateui/models/NumericalOptionPayload;->digits:I

    iget v6, p1, Lcom/ekodroid/omrevaluator/templateui/models/NumericalOptionPayload;->decimalDigits:I

    sub-int/2addr v5, v6

    invoke-virtual {v2, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    :goto_0
    invoke-virtual {p2, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    goto :goto_2

    :cond_2
    aget-object p2, p0, v1

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result p2

    iget v2, p1, Lcom/ekodroid/omrevaluator/templateui/models/NumericalOptionPayload;->digits:I

    iget v6, p1, Lcom/ekodroid/omrevaluator/templateui/models/NumericalOptionPayload;->decimalDigits:I

    sub-int/2addr v2, v6

    if-ne p2, v2, :cond_3

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-object v2, p0, v1

    goto :goto_0

    :cond_3
    move p2, v1

    :goto_1
    iget v2, p1, Lcom/ekodroid/omrevaluator/templateui/models/NumericalOptionPayload;->digits:I

    iget v6, p1, Lcom/ekodroid/omrevaluator/templateui/models/NumericalOptionPayload;->decimalDigits:I

    sub-int/2addr v2, v6

    aget-object v6, p0, v1

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    sub-int/2addr v2, v6

    if-ge p2, v2, :cond_4

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    add-int/lit8 p2, p2, 0x1

    goto :goto_1

    :cond_4
    move-object p2, v5

    :goto_2
    iget v2, p1, Lcom/ekodroid/omrevaluator/templateui/models/NumericalOptionPayload;->decimalDigits:I

    if-lez v2, :cond_7

    array-length v2, p0

    if-ne v2, v4, :cond_5

    move p0, v1

    :goto_3
    iget v2, p1, Lcom/ekodroid/omrevaluator/templateui/models/NumericalOptionPayload;->decimalDigits:I

    if-ge p0, v2, :cond_7

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    add-int/lit8 p0, p0, 0x1

    goto :goto_3

    :cond_5
    move v2, v1

    :goto_4
    iget v5, p1, Lcom/ekodroid/omrevaluator/templateui/models/NumericalOptionPayload;->decimalDigits:I

    if-ge v2, v5, :cond_7

    aget-object v5, p0, v4

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    if-ge v2, v5, :cond_6

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-object p2, p0, v4

    invoke-virtual {p2, v2}, Ljava/lang/String;->charAt(I)C

    move-result p2

    invoke-static {p2}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_5

    :cond_6
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_5
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    :cond_7
    :goto_6
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result p0

    if-ge v1, p0, :cond_8

    invoke-virtual {p2, v1}, Ljava/lang/String;->charAt(I)C

    move-result p0

    invoke-static {p0}, Le5;->l(C)I

    move-result p0

    iget v2, p1, Lcom/ekodroid/omrevaluator/templateui/models/NumericalOptionPayload;->digits:I

    mul-int/2addr p0, v2

    add-int/2addr p0, v1

    aput-boolean v4, v0, p0

    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    :cond_8
    return-object v0
.end method

.method public static p([ZLcom/ekodroid/omrevaluator/templateui/models/LabelProfile;Ljava/lang/String;)Ljava/lang/String;
    .locals 8

    .line 1
    invoke-static {p2}, Lbc;->d(Ljava/lang/String;)Lcom/ekodroid/omrevaluator/templateui/models/NumericalOptionPayload;

    move-result-object p2

    iget-boolean v0, p2, Lcom/ekodroid/omrevaluator/templateui/models/NumericalOptionPayload;->hasNegetive:Z

    const-string v1, ""

    if-eqz v0, :cond_0

    array-length v0, p0

    add-int/lit8 v0, v0, -0x1

    aget-boolean v0, p0, v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "-"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    move-object v0, v1

    :goto_0
    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getNumericalOptionLabels()[Ljava/lang/String;

    move-result-object p1

    const/4 v2, 0x0

    move v3, v2

    :goto_1
    iget v4, p2, Lcom/ekodroid/omrevaluator/templateui/models/NumericalOptionPayload;->digits:I

    if-ge v3, v4, :cond_5

    iget v5, p2, Lcom/ekodroid/omrevaluator/templateui/models/NumericalOptionPayload;->decimalDigits:I

    sub-int/2addr v4, v5

    if-ne v4, v3, :cond_1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "."

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_1
    const/4 v4, -0x1

    move v5, v2

    move v6, v4

    :goto_2
    const/16 v7, 0xa

    if-ge v5, v7, :cond_3

    iget v7, p2, Lcom/ekodroid/omrevaluator/templateui/models/NumericalOptionPayload;->digits:I

    mul-int/2addr v7, v5

    add-int/2addr v7, v3

    aget-boolean v7, p0, v7

    if-eqz v7, :cond_2

    move v6, v5

    :cond_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    :cond_3
    if-le v6, v4, :cond_4

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-object v0, p1, v6

    goto :goto_3

    :cond_4
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " "

    :goto_3
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_5
    return-object v0
.end method

.method public static q(Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;)Ljava/lang/String;
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;->getType()Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    move-result-object v0

    sget-object v1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;->DECIMAL:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;->getMarkedPayload()Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Le5;->u(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_0
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;->getMarkedValues()[Z

    move-result-object v0

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;->getType()Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    move-result-object v1

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;->getPayload()Ljava/lang/String;

    move-result-object p0

    invoke-static {v0, v1, p0, p1}, Le5;->s([ZLcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;Ljava/lang/String;Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static r(Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;)Ljava/lang/String;
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->getType()Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    move-result-object v0

    sget-object v1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;->DECIMAL:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    if-ne v0, v1, :cond_1

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->getMarkedState()Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;

    move-result-object p1

    sget-object v0, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;->UNATTEMPTED:Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;

    const-string v1, ""

    if-ne p1, v0, :cond_0

    return-object v1

    :cond_0
    :try_start_0
    invoke-static {p0}, Le5;->b(Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;)D

    move-result-wide p0

    invoke-static {p0, p1}, Le5;->i(D)Ljava/lang/String;

    move-result-object p0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    return-object v1

    :cond_1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->getMarkedValues()[Z

    move-result-object v0

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->getType()Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    move-result-object v1

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->getPayload()Ljava/lang/String;

    move-result-object p0

    invoke-static {v0, v1, p0, p1}, Le5;->s([ZLcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;Ljava/lang/String;Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static s([ZLcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;Ljava/lang/String;Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;)Ljava/lang/String;
    .locals 1

    .line 1
    sget-object v0, Le5$c;->a:[I

    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result p1

    aget p1, v0, p1

    packed-switch p1, :pswitch_data_0

    const/4 p0, 0x0

    return-object p0

    :pswitch_0
    invoke-static {p0, p3}, Le5;->x([ZLcom/ekodroid/omrevaluator/templateui/models/LabelProfile;)Ljava/lang/String;

    move-result-object p0

    return-object p0

    :pswitch_1
    invoke-static {p0, p3}, Le5;->e([ZLcom/ekodroid/omrevaluator/templateui/models/LabelProfile;)Ljava/lang/String;

    move-result-object p0

    return-object p0

    :pswitch_2
    invoke-static {p0, p3}, Le5;->v([ZLcom/ekodroid/omrevaluator/templateui/models/LabelProfile;)Ljava/lang/String;

    move-result-object p0

    return-object p0

    :pswitch_3
    invoke-static {p0, p3, p2}, Le5;->p([ZLcom/ekodroid/omrevaluator/templateui/models/LabelProfile;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    return-object p0

    :pswitch_4
    invoke-static {p0, p3, p2}, Le5;->m([ZLcom/ekodroid/omrevaluator/templateui/models/LabelProfile;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    return-object p0

    :pswitch_5
    invoke-static {p0, p3}, Le5;->B([ZLcom/ekodroid/omrevaluator/templateui/models/LabelProfile;)Ljava/lang/String;

    move-result-object p0

    return-object p0

    :pswitch_6
    invoke-static {p0, p3}, Le5;->z([ZLcom/ekodroid/omrevaluator/templateui/models/LabelProfile;)Ljava/lang/String;

    move-result-object p0

    return-object p0

    :pswitch_7
    invoke-static {p0, p3}, Le5;->g([ZLcom/ekodroid/omrevaluator/templateui/models/LabelProfile;)Ljava/lang/String;

    move-result-object p0

    return-object p0

    :pswitch_8
    invoke-static {p0, p3}, Le5;->j([ZLcom/ekodroid/omrevaluator/templateui/models/LabelProfile;)Ljava/lang/String;

    move-result-object p0

    return-object p0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static t(Lcom/ekodroid/omrevaluator/templateui/models/PartialAnswerOptionKey;Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;)Ljava/lang/String;
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/models/PartialAnswerOptionKey;->getType()Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    move-result-object v0

    sget-object v1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;->DECIMAL:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/models/PartialAnswerOptionKey;->getMarkedPayload()Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Le5;->u(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_0
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/models/PartialAnswerOptionKey;->getMarkedValues()[Z

    move-result-object v0

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/models/PartialAnswerOptionKey;->getType()Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    move-result-object v1

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/models/PartialAnswerOptionKey;->getPayload()Ljava/lang/String;

    move-result-object p0

    invoke-static {v0, v1, p0, p1}, Le5;->s([ZLcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;Ljava/lang/String;Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static u(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .line 1
    new-instance v0, Lgc0;

    invoke-direct {v0}, Lgc0;-><init>()V

    const-class v1, Lcom/ekodroid/omrevaluator/templateui/models/DecimalRange;

    invoke-virtual {v0, p0, v1}, Lgc0;->j(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/ekodroid/omrevaluator/templateui/models/DecimalRange;

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/models/DecimalRange;->getLower()D

    move-result-wide v0

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/models/DecimalRange;->getUpper()D

    move-result-wide v2

    cmpl-double v0, v0, v2

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/models/DecimalRange;->getLower()D

    move-result-wide v0

    invoke-static {v0, v1}, Le5;->i(D)Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/models/DecimalRange;->getLower()D

    move-result-wide v1

    invoke-static {v1, v2}, Le5;->i(D)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/models/DecimalRange;->getUpper()D

    move-result-wide v1

    invoke-static {v1, v2}, Le5;->i(D)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static v([ZLcom/ekodroid/omrevaluator/templateui/models/LabelProfile;)Ljava/lang/String;
    .locals 3

    .line 1
    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getSixOptionLabels()[Ljava/lang/String;

    move-result-object p1

    const-string v0, ""

    const/4 v1, 0x0

    :goto_0
    array-length v2, p0

    if-ge v1, v2, :cond_2

    aget-boolean v2, p0, v1

    if-eqz v2, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, ", "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-object v0, p1, v1

    goto :goto_1

    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-object v0, p1, v1

    :goto_1
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    return-object v0
.end method

.method public static w(Ljava/lang/String;Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;)[Z
    .locals 3

    .line 1
    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getSixOptionLabels()[Ljava/lang/String;

    move-result-object p1

    array-length v0, p1

    new-array v0, v0, [Z

    const/4 v1, 0x0

    :goto_0
    array-length v2, p1

    if-ge v1, v2, :cond_1

    aget-object v2, p1, v1

    invoke-virtual {p0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    aput-boolean v2, v0, v1

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method public static x([ZLcom/ekodroid/omrevaluator/templateui/models/LabelProfile;)Ljava/lang/String;
    .locals 3

    .line 1
    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getTenOptionLabels()[Ljava/lang/String;

    move-result-object p1

    const-string v0, ""

    const/4 v1, 0x0

    :goto_0
    array-length v2, p0

    if-ge v1, v2, :cond_2

    aget-boolean v2, p0, v1

    if-eqz v2, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, ", "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-object v0, p1, v1

    goto :goto_1

    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-object v0, p1, v1

    :goto_1
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    return-object v0
.end method

.method public static y(Ljava/lang/String;Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;)[Z
    .locals 3

    .line 1
    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getTenOptionLabels()[Ljava/lang/String;

    move-result-object p1

    array-length v0, p1

    new-array v0, v0, [Z

    const/4 v1, 0x0

    :goto_0
    array-length v2, p1

    if-ge v1, v2, :cond_1

    aget-object v2, p1, v1

    invoke-virtual {p0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    aput-boolean v2, v0, v1

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method public static z([ZLcom/ekodroid/omrevaluator/templateui/models/LabelProfile;)Ljava/lang/String;
    .locals 3

    .line 1
    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getThreeOptionLabels()[Ljava/lang/String;

    move-result-object p1

    const-string v0, ""

    const/4 v1, 0x0

    :goto_0
    array-length v2, p0

    if-ge v1, v2, :cond_2

    aget-boolean v2, p0, v1

    if-eqz v2, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, ", "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-object v0, p1, v1

    goto :goto_1

    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-object v0, p1, v1

    :goto_1
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    return-object v0
.end method

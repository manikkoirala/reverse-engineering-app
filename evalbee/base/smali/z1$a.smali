.class public Lz1$a;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lz1;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "a"
.end annotation


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Z

.field public e:Ljava/lang/String;

.field public f:Z

.field public g:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lz1$a;->f:Z

    return-void
.end method

.method public synthetic constructor <init>(Lla2;)V
    .locals 0

    .line 2
    invoke-direct {p0}, Lz1$a;-><init>()V

    return-void
.end method

.method public static bridge synthetic f(Lz1$a;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lz1$a;->e:Ljava/lang/String;

    return-object p0
.end method

.method public static bridge synthetic g(Lz1$a;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lz1$a;->c:Ljava/lang/String;

    return-object p0
.end method

.method public static bridge synthetic h(Lz1$a;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lz1$a;->g:Ljava/lang/String;

    return-object p0
.end method

.method public static bridge synthetic i(Lz1$a;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lz1$a;->b:Ljava/lang/String;

    return-object p0
.end method

.method public static bridge synthetic j(Lz1$a;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lz1$a;->a:Ljava/lang/String;

    return-object p0
.end method

.method public static bridge synthetic k(Lz1$a;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lz1$a;->d:Z

    return p0
.end method

.method public static bridge synthetic l(Lz1$a;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lz1$a;->f:Z

    return p0
.end method


# virtual methods
.method public a()Lz1;
    .locals 2

    .line 1
    iget-object v0, p0, Lz1$a;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    new-instance v0, Lz1;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lz1;-><init>(Lz1$a;Lhe2;)V

    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Cannot build ActionCodeSettings with null URL. Call #setUrl(String) before calling build()"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public b(Ljava/lang/String;ZLjava/lang/String;)Lz1$a;
    .locals 0

    .line 1
    iput-object p1, p0, Lz1$a;->c:Ljava/lang/String;

    iput-boolean p2, p0, Lz1$a;->d:Z

    iput-object p3, p0, Lz1$a;->e:Ljava/lang/String;

    return-object p0
.end method

.method public c(Z)Lz1$a;
    .locals 0

    .line 1
    iput-boolean p1, p0, Lz1$a;->f:Z

    return-object p0
.end method

.method public d(Ljava/lang/String;)Lz1$a;
    .locals 0

    .line 1
    iput-object p1, p0, Lz1$a;->b:Ljava/lang/String;

    return-object p0
.end method

.method public e(Ljava/lang/String;)Lz1$a;
    .locals 0

    .line 1
    iput-object p1, p0, Lz1$a;->a:Ljava/lang/String;

    return-object p0
.end method

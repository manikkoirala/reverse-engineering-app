.class public Ltb0;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:Landroid/content/Context;

.field public b:Ly01;

.field public c:Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;

.field public d:I

.field public e:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Ly01;Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Ltb0;->a:Landroid/content/Context;

    iput-object p2, p0, Ltb0;->b:Ly01;

    iput-object p3, p0, Ltb0;->c:Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;

    invoke-virtual {p0}, Ltb0;->e()V

    return-void
.end method

.method public static synthetic a(Ltb0;)I
    .locals 0

    .line 1
    iget p0, p0, Ltb0;->d:I

    return p0
.end method

.method public static synthetic b(Ltb0;I)I
    .locals 0

    .line 1
    iput p1, p0, Ltb0;->d:I

    return p1
.end method

.method public static synthetic c(Ltb0;)I
    .locals 0

    .line 1
    iget p0, p0, Ltb0;->e:I

    return p0
.end method

.method public static synthetic d(Ltb0;I)I
    .locals 0

    .line 1
    iput p1, p0, Ltb0;->e:I

    return p1
.end method


# virtual methods
.method public final e()V
    .locals 14

    .line 1
    new-instance v0, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    iget-object v1, p0, Ltb0;->a:Landroid/content/Context;

    const v2, 0x7f130151

    invoke-direct {v0, v1, v2}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;-><init>(Landroid/content/Context;I)V

    iget-object v1, p0, Ltb0;->a:Landroid/content/Context;

    const-string v2, "layout_inflater"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    const v2, 0x7f0c0080

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->setView(Landroid/view/View;)Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    const v2, 0x7f1202c0

    invoke-virtual {v0, v2}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->setTitle(I)Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->setCancelable(Z)Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    invoke-virtual {v0}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->create()Landroidx/appcompat/app/a;

    move-result-object v0

    new-instance v2, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;

    invoke-direct {v2, v3, v3}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;-><init>(Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder$BuildType;Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;)V

    iget-object v3, p0, Ltb0;->c:Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;

    invoke-virtual {v2, v3}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateBuilder;->o(Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;)I

    move-result v2

    const v3, 0x7f090339

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Spinner;

    const v4, 0x7f09033b

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    move-object v11, v4

    check-cast v11, Landroid/widget/Spinner;

    new-instance v4, Landroid/widget/ArrayAdapter;

    iget-object v5, p0, Ltb0;->a:Landroid/content/Context;

    sget-object v6, Lgr1;->d:[Ljava/lang/String;

    const v7, 0x7f0c00f0

    invoke-direct {v4, v5, v7, v6}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    invoke-virtual {v3, v4}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    new-instance v4, Landroid/widget/ArrayAdapter;

    iget-object v5, p0, Ltb0;->a:Landroid/content/Context;

    sget-object v6, Lgr1;->e:[Ljava/lang/String;

    invoke-direct {v4, v5, v7, v6}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    invoke-virtual {v11, v4}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    const v4, 0x7f0903c9

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    move-object v12, v4

    check-cast v12, Landroid/widget/TextView;

    const v4, 0x7f0900db

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    const/16 v4, 0x8

    invoke-virtual {v12, v4}, Landroid/view/View;->setVisibility(I)V

    new-instance v13, Ltb0$a;

    move-object v4, v13

    move-object v5, p0

    move-object v6, v12

    move-object v7, v1

    move-object v8, v3

    move-object v9, v11

    move v10, v2

    invoke-direct/range {v4 .. v10}, Ltb0$a;-><init>(Ltb0;Landroid/widget/TextView;Landroid/widget/Button;Landroid/widget/Spinner;Landroid/widget/Spinner;I)V

    invoke-virtual {v3, v13}, Landroid/widget/AdapterView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    new-instance v13, Ltb0$b;

    move-object v4, v13

    invoke-direct/range {v4 .. v10}, Ltb0$b;-><init>(Ltb0;Landroid/widget/TextView;Landroid/widget/Button;Landroid/widget/Spinner;Landroid/widget/Spinner;I)V

    invoke-virtual {v11, v13}, Landroid/widget/AdapterView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    new-instance v2, Ltb0$c;

    invoke-direct {v2, p0, v0}, Ltb0$c;-><init>(Ltb0;Landroidx/appcompat/app/a;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    return-void
.end method

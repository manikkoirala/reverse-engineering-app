.class public Lj9;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lrx;


# instance fields
.field public final a:Ljava/util/concurrent/Executor;

.field public final b:Lrx;

.field public volatile c:Z


# direct methods
.method public constructor <init>(Ljava/util/concurrent/Executor;Lrx;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lj9;->c:Z

    iput-object p1, p0, Lj9;->a:Ljava/util/concurrent/Executor;

    iput-object p2, p0, Lj9;->b:Lrx;

    return-void
.end method

.method public static synthetic b(Lj9;Ljava/lang/Object;Lcom/google/firebase/firestore/FirebaseFirestoreException;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lj9;->c(Ljava/lang/Object;Lcom/google/firebase/firestore/FirebaseFirestoreException;)V

    return-void
.end method

.method private synthetic c(Ljava/lang/Object;Lcom/google/firebase/firestore/FirebaseFirestoreException;)V
    .locals 1

    .line 1
    iget-boolean v0, p0, Lj9;->c:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lj9;->b:Lrx;

    invoke-interface {v0, p1, p2}, Lrx;->a(Ljava/lang/Object;Lcom/google/firebase/firestore/FirebaseFirestoreException;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Object;Lcom/google/firebase/firestore/FirebaseFirestoreException;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lj9;->a:Ljava/util/concurrent/Executor;

    new-instance v1, Li9;

    invoke-direct {v1, p0, p1, p2}, Li9;-><init>(Lj9;Ljava/lang/Object;Lcom/google/firebase/firestore/FirebaseFirestoreException;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public d()V
    .locals 1

    .line 1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lj9;->c:Z

    return-void
.end method

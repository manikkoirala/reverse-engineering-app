.class public Lpq;
.super Ls10;
.source "SourceFile"


# instance fields
.field public final a:Lr10;

.field public final b:Lr91;

.field public final c:Ljava/util/List;

.field public final d:Ljava/util/List;

.field public final e:Lcom/google/firebase/appcheck/internal/StorageHelper;

.field public final f:Lxw1;

.field public final g:Ljava/util/concurrent/Executor;

.field public final h:Ljava/util/concurrent/Executor;

.field public final i:Ljava/util/concurrent/Executor;

.field public final j:Lcom/google/android/gms/tasks/Task;

.field public final k:Lah;

.field public l:Ls5;


# direct methods
.method public constructor <init>(Lr10;Lr91;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Ljava/util/concurrent/ScheduledExecutorService;)V
    .locals 2

    .line 1
    invoke-direct {p0}, Ls10;-><init>()V

    invoke-static {p1}, Lcom/google/android/gms/common/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Lcom/google/android/gms/common/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    iput-object p1, p0, Lpq;->a:Lr10;

    iput-object p2, p0, Lpq;->b:Lr91;

    new-instance p2, Ljava/util/ArrayList;

    invoke-direct {p2}, Ljava/util/ArrayList;-><init>()V

    iput-object p2, p0, Lpq;->c:Ljava/util/List;

    new-instance p2, Ljava/util/ArrayList;

    invoke-direct {p2}, Ljava/util/ArrayList;-><init>()V

    iput-object p2, p0, Lpq;->d:Ljava/util/List;

    new-instance p2, Lcom/google/firebase/appcheck/internal/StorageHelper;

    invoke-virtual {p1}, Lr10;->l()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p1}, Lr10;->q()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p2, v0, v1}, Lcom/google/firebase/appcheck/internal/StorageHelper;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object p2, p0, Lpq;->e:Lcom/google/firebase/appcheck/internal/StorageHelper;

    new-instance p2, Lxw1;

    invoke-virtual {p1}, Lr10;->l()Landroid/content/Context;

    move-result-object p1

    invoke-direct {p2, p1, p0, p4, p6}, Lxw1;-><init>(Landroid/content/Context;Lpq;Ljava/util/concurrent/Executor;Ljava/util/concurrent/ScheduledExecutorService;)V

    iput-object p2, p0, Lpq;->f:Lxw1;

    iput-object p3, p0, Lpq;->g:Ljava/util/concurrent/Executor;

    iput-object p4, p0, Lpq;->h:Ljava/util/concurrent/Executor;

    iput-object p5, p0, Lpq;->i:Ljava/util/concurrent/Executor;

    invoke-virtual {p0, p5}, Lpq;->i(Ljava/util/concurrent/Executor;)Lcom/google/android/gms/tasks/Task;

    move-result-object p1

    iput-object p1, p0, Lpq;->j:Lcom/google/android/gms/tasks/Task;

    new-instance p1, Lah$a;

    invoke-direct {p1}, Lah$a;-><init>()V

    iput-object p1, p0, Lpq;->k:Lah;

    return-void
.end method

.method public static synthetic c(Lpq;ZLcom/google/android/gms/tasks/Task;)Lcom/google/android/gms/tasks/Task;
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lpq;->g(ZLcom/google/android/gms/tasks/Task;)Lcom/google/android/gms/tasks/Task;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic d(Lpq;Lcom/google/android/gms/tasks/TaskCompletionSource;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lpq;->h(Lcom/google/android/gms/tasks/TaskCompletionSource;)V

    return-void
.end method

.method private synthetic g(ZLcom/google/android/gms/tasks/Task;)Lcom/google/android/gms/tasks/Task;
    .locals 0

    .line 1
    if-nez p1, :cond_0

    invoke-virtual {p0}, Lpq;->f()Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lpq;->l:Ls5;

    invoke-static {p1}, Lfq;->c(Ls5;)Lfq;

    move-result-object p1

    :goto_0
    invoke-static {p1}, Lcom/google/android/gms/tasks/Tasks;->forResult(Ljava/lang/Object;)Lcom/google/android/gms/tasks/Task;

    move-result-object p1

    return-object p1

    :cond_0
    new-instance p1, Lcom/google/firebase/FirebaseException;

    const-string p2, "No AppCheckProvider installed."

    invoke-direct {p1, p2}, Lcom/google/firebase/FirebaseException;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Lfq;->d(Lcom/google/firebase/FirebaseException;)Lfq;

    move-result-object p1

    goto :goto_0
.end method

.method private synthetic h(Lcom/google/android/gms/tasks/TaskCompletionSource;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lpq;->e:Lcom/google/firebase/appcheck/internal/StorageHelper;

    invoke-virtual {v0}, Lcom/google/firebase/appcheck/internal/StorageHelper;->d()Ls5;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, v0}, Lpq;->j(Ls5;)V

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/google/android/gms/tasks/TaskCompletionSource;->setResult(Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public a(Z)Lcom/google/android/gms/tasks/Task;
    .locals 3

    .line 1
    iget-object v0, p0, Lpq;->j:Lcom/google/android/gms/tasks/Task;

    iget-object v1, p0, Lpq;->h:Ljava/util/concurrent/Executor;

    new-instance v2, Lnq;

    invoke-direct {v2, p0, p1}, Lnq;-><init>(Lpq;Z)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/tasks/Task;->continueWithTask(Ljava/util/concurrent/Executor;Lcom/google/android/gms/tasks/Continuation;)Lcom/google/android/gms/tasks/Task;

    move-result-object p1

    return-object p1
.end method

.method public b(Lt5;)V
    .locals 3

    .line 1
    invoke-static {p1}, Lcom/google/android/gms/common/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lpq;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lpq;->f:Lxw1;

    iget-object v1, p0, Lpq;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    iget-object v2, p0, Lpq;->d:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/2addr v1, v2

    invoke-virtual {v0, v1}, Lxw1;->d(I)V

    invoke-virtual {p0}, Lpq;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lpq;->l:Ls5;

    invoke-static {v0}, Lfq;->c(Ls5;)Lfq;

    move-result-object v0

    invoke-interface {p1, v0}, Lt5;->a(Lu5;)V

    :cond_0
    return-void
.end method

.method public e()Lcom/google/android/gms/tasks/Task;
    .locals 1

    .line 1
    const/4 v0, 0x0

    throw v0
.end method

.method public final f()Z
    .locals 4

    .line 1
    iget-object v0, p0, Lpq;->l:Ls5;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ls5;->a()J

    move-result-wide v0

    iget-object v2, p0, Lpq;->k:Lah;

    invoke-interface {v2}, Lah;->currentTimeMillis()J

    move-result-wide v2

    sub-long/2addr v0, v2

    const-wide/32 v2, 0x493e0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final i(Ljava/util/concurrent/Executor;)Lcom/google/android/gms/tasks/Task;
    .locals 2

    .line 1
    new-instance v0, Lcom/google/android/gms/tasks/TaskCompletionSource;

    invoke-direct {v0}, Lcom/google/android/gms/tasks/TaskCompletionSource;-><init>()V

    new-instance v1, Loq;

    invoke-direct {v1, p0, v0}, Loq;-><init>(Lpq;Lcom/google/android/gms/tasks/TaskCompletionSource;)V

    invoke-interface {p1, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Lcom/google/android/gms/tasks/TaskCompletionSource;->getTask()Lcom/google/android/gms/tasks/Task;

    move-result-object p1

    return-object p1
.end method

.method public j(Ls5;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lpq;->l:Ls5;

    return-void
.end method

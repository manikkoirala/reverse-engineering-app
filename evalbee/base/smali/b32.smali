.class public Lb32;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:Z

.field public b:[Ljava/lang/String;

.field public c:[D

.field public d:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lb32;-><init>(Z)V

    return-void
.end method

.method public constructor <init>(Z)V
    .locals 2

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x2

    new-array v1, v0, [Ljava/lang/String;

    iput-object v1, p0, Lb32;->b:[Ljava/lang/String;

    new-array v0, v0, [D

    iput-object v0, p0, Lb32;->c:[D

    const/4 v0, 0x0

    iput v0, p0, Lb32;->d:I

    iput-boolean p1, p0, Lb32;->a:Z

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)D
    .locals 3

    .line 1
    const/4 v0, 0x0

    :goto_0
    iget v1, p0, Lb32;->d:I

    if-ge v0, v1, :cond_3

    iget-boolean v1, p0, Lb32;->a:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lb32;->b:[Ljava/lang/String;

    aget-object v1, v1, v0

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    iget-boolean v1, p0, Lb32;->a:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lb32;->b:[Ljava/lang/String;

    aget-object v1, v1, v0

    invoke-virtual {v1, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_1
    iget-object p1, p0, Lb32;->c:[D

    aget-wide v0, p1, v0

    return-wide v0

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "variable value has not been set: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public b(Ljava/lang/String;D)V
    .locals 6

    .line 1
    if-eqz p1, :cond_6

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget v2, p0, Lb32;->d:I

    if-ge v1, v2, :cond_3

    iget-boolean v2, p0, Lb32;->a:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lb32;->b:[Ljava/lang/String;

    aget-object v2, v2, v1

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    iget-boolean v2, p0, Lb32;->a:Z

    if-nez v2, :cond_2

    iget-object v2, p0, Lb32;->b:[Ljava/lang/String;

    aget-object v2, v2, v1

    invoke-virtual {v2, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_1
    iget-object p1, p0, Lb32;->c:[D

    aput-wide p2, p1, v1

    return-void

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lb32;->b:[Ljava/lang/String;

    array-length v1, v1

    if-ne v2, v1, :cond_5

    mul-int/lit8 v2, v2, 0x2

    new-array v1, v2, [Ljava/lang/String;

    new-array v2, v2, [D

    :goto_1
    iget v3, p0, Lb32;->d:I

    if-ge v0, v3, :cond_4

    iget-object v3, p0, Lb32;->b:[Ljava/lang/String;

    aget-object v3, v3, v0

    aput-object v3, v1, v0

    iget-object v3, p0, Lb32;->c:[D

    aget-wide v4, v3, v0

    aput-wide v4, v2, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_4
    iput-object v1, p0, Lb32;->b:[Ljava/lang/String;

    iput-object v2, p0, Lb32;->c:[D

    :cond_5
    iget-object v0, p0, Lb32;->b:[Ljava/lang/String;

    iget v1, p0, Lb32;->d:I

    aput-object p1, v0, v1

    iget-object p1, p0, Lb32;->c:[D

    aput-wide p2, p1, v1

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lb32;->d:I

    return-void

    :cond_6
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "varName cannot be null"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

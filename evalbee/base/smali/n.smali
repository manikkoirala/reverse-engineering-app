.class public abstract Ln;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lqc0;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic c(Ljava/lang/CharSequence;)Lc81;
    .locals 0

    .line 2
    invoke-virtual {p0, p1}, Ln;->c(Ljava/lang/CharSequence;)Lqc0;

    move-result-object p1

    return-object p1
.end method

.method public c(Ljava/lang/CharSequence;)Lqc0;
    .locals 3

    .line 1
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    invoke-interface {p1, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    invoke-virtual {p0, v2}, Ln;->k(C)Lqc0;

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-object p0
.end method

.method public bridge synthetic d(Ljava/lang/CharSequence;Ljava/nio/charset/Charset;)Lc81;
    .locals 0

    .line 2
    invoke-virtual {p0, p1, p2}, Ln;->d(Ljava/lang/CharSequence;Ljava/nio/charset/Charset;)Lqc0;

    move-result-object p1

    return-object p1
.end method

.method public d(Ljava/lang/CharSequence;Ljava/nio/charset/Charset;)Lqc0;
    .locals 0

    .line 1
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1, p2}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object p1

    invoke-virtual {p0, p1}, Ln;->j([B)Lqc0;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic f([B)Lc81;
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Ln;->j([B)Lqc0;

    move-result-object p1

    return-object p1
.end method

.method public g(Ljava/lang/Object;Lcom/google/common/hash/Funnel;)Lqc0;
    .locals 0

    .line 1
    invoke-interface {p2, p1, p0}, Lcom/google/common/hash/Funnel;->funnel(Ljava/lang/Object;Lc81;)V

    return-object p0
.end method

.method public abstract h([BII)Lqc0;
.end method

.method public j([B)Lqc0;
    .locals 2

    .line 1
    array-length v0, p1

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v1, v0}, Ln;->h([BII)Lqc0;

    move-result-object p1

    return-object p1
.end method

.method public abstract k(C)Lqc0;
.end method

.class public abstract Lja0;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Ljava/lang/Object;

.field public static final b:Ljava/lang/Object;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .line 1
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lja0;->a:Ljava/lang/Object;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lja0;->b:Ljava/lang/Object;

    return-void
.end method

.method public static a([D[D[D[DI)D
    .locals 15

    .line 1
    move/from16 v0, p4

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, v0, :cond_0

    aget-wide v3, p1, v2

    aget-wide v5, p0, v2

    sub-double/2addr v3, v5

    aput-wide v3, p3, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    const-wide/16 v2, 0x0

    move v4, v1

    move-wide v5, v2

    :goto_1
    if-ge v4, v0, :cond_1

    aget-wide v7, p3, v4

    mul-double/2addr v7, v7

    add-double/2addr v5, v7

    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :cond_1
    cmpl-double v4, v5, v2

    if-eqz v4, :cond_3

    move v4, v1

    move-wide v7, v2

    :goto_2
    if-ge v4, v0, :cond_2

    aget-wide v9, p3, v4

    aget-wide v11, p2, v4

    aget-wide v13, p0, v4

    sub-double/2addr v11, v13

    mul-double/2addr v9, v11

    add-double/2addr v7, v9

    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    :cond_2
    div-double/2addr v7, v5

    goto :goto_3

    :cond_3
    move-wide v7, v2

    :goto_3
    cmpg-double v4, v7, v2

    if-gez v4, :cond_4

    move-wide v7, v2

    goto :goto_4

    :cond_4
    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    cmpl-double v6, v7, v4

    if-lez v6, :cond_5

    move-wide v7, v4

    :cond_5
    :goto_4
    move v4, v1

    :goto_5
    if-ge v4, v0, :cond_6

    aget-wide v5, p0, v4

    aget-wide v9, p3, v4

    mul-double/2addr v9, v7

    add-double/2addr v5, v9

    aput-wide v5, p3, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_5

    :cond_6
    aput-wide v7, p3, v0

    :goto_6
    if-ge v1, v0, :cond_7

    aget-wide v4, p2, v1

    aget-wide v6, p3, v1

    sub-double/2addr v4, v6

    mul-double/2addr v4, v4

    add-double/2addr v2, v4

    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    :cond_7
    return-wide v2
.end method

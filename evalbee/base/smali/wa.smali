.class public abstract Lwa;
.super Lz21;
.source "SourceFile"


# static fields
.field public static k:[I

.field public static l:[I

.field public static m:[D


# instance fields
.field public d:Lz22;

.field public e:D

.field public f:D

.field public g:I

.field public h:I

.field public i:I

.field public j:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .line 1
    const/4 v0, 0x0

    new-array v1, v0, [I

    sput-object v1, Lwa;->k:[I

    new-array v1, v0, [I

    sput-object v1, Lwa;->l:[I

    new-array v0, v0, [D

    sput-object v0, Lwa;->m:[D

    return-void
.end method

.method public constructor <init>(Lzl;Lub0;)V
    .locals 1

    .line 1
    invoke-direct {p0, p1, p2}, Lz21;-><init>(Lzl;Lub0;)V

    new-instance p1, Lz22;

    const/16 p2, 0x8

    new-array v0, p2, [D

    fill-array-data v0, :array_0

    invoke-direct {p1, v0, p2}, Lz22;-><init>([DI)V

    iput-object p1, p0, Lwa;->d:Lz22;

    const-wide/16 p1, 0x0

    iput-wide p1, p0, Lwa;->e:D

    const-wide/high16 p1, 0x3ff0000000000000L    # 1.0

    iput-wide p1, p0, Lwa;->f:D

    const/4 p1, 0x1

    iput p1, p0, Lwa;->g:I

    const/4 p2, 0x4

    iput p2, p0, Lwa;->h:I

    const/4 p2, 0x0

    iput p2, p0, Lwa;->i:I

    iput-boolean p1, p0, Lwa;->j:Z

    return-void

    nop

    :array_0
    .array-data 8
        0x0
        0x0
        0x0
        0x0
        0x3ff0000000000000L    # 1.0
        0x3ff0000000000000L    # 1.0
        0x3ff0000000000000L    # 1.0
        0x3ff0000000000000L    # 1.0
    .end array-data
.end method


# virtual methods
.method public abstract c([D)V
.end method

.method public d()I
    .locals 1

    .line 1
    iget v0, p0, Lwa;->g:I

    return v0
.end method

.method public e(DI)D
    .locals 21

    .line 1
    move-object/from16 v0, p0

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    iget v3, v0, Lwa;->h:I

    const-wide/16 v4, 0x0

    if-ge v2, v3, :cond_b

    sget-object v6, Lwa;->m:[D

    add-int v7, p3, v2

    aget-wide v8, v6, v7

    const/4 v10, 0x1

    add-int/2addr v7, v10

    aget-wide v11, v6, v7

    cmpl-double v6, p1, v8

    if-ltz v6, :cond_a

    cmpg-double v6, p1, v11

    if-gtz v6, :cond_a

    cmpl-double v6, v8, v11

    if-eqz v6, :cond_a

    add-int/lit8 v6, v3, -0x2

    sub-int/2addr v3, v2

    sub-int/2addr v3, v10

    :goto_1
    if-ltz v3, :cond_0

    sget-object v7, Lwa;->k:[I

    aput v1, v7, v3

    add-int/lit8 v3, v3, -0x1

    goto :goto_1

    :cond_0
    if-lez v2, :cond_2

    move v3, v1

    :goto_2
    if-ge v3, v2, :cond_1

    sget-object v7, Lwa;->l:[I

    aput v3, v7, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_1
    sget-object v3, Lwa;->l:[I

    const v7, 0x7fffffff

    aput v7, v3, v2

    goto :goto_3

    :cond_2
    sget-object v3, Lwa;->l:[I

    aput v6, v3, v1

    iget v7, v0, Lwa;->h:I

    aput v7, v3, v10

    :cond_3
    :goto_3
    move v3, v1

    :goto_4
    sget-object v7, Lwa;->l:[I

    aget v8, v7, v3

    add-int/lit8 v9, v3, 0x1

    aget v7, v7, v9

    sub-int/2addr v7, v10

    if-ge v8, v7, :cond_9

    sub-int v7, v6, v2

    add-int/lit8 v8, v2, -0x1

    iget v9, v0, Lwa;->h:I

    const-wide/high16 v11, 0x3ff0000000000000L    # 1.0

    move v14, v1

    move v13, v6

    :goto_5
    if-ltz v13, :cond_5

    if-ltz v8, :cond_4

    sget-object v15, Lwa;->l:[I

    aget v15, v15, v8

    if-ne v15, v13, :cond_4

    add-int v15, p3, v14

    sget-object v16, Lwa;->m:[D

    add-int v17, v15, v9

    aget-wide v17, v16, v17

    sub-double v19, v17, p1

    add-int/2addr v15, v10

    aget-wide v15, v16, v15

    sub-double v17, v17, v15

    div-double v19, v19, v17

    mul-double v11, v11, v19

    add-int/lit8 v14, v14, 0x1

    add-int/lit8 v8, v8, -0x1

    goto :goto_6

    :cond_4
    sget-object v15, Lwa;->k:[I

    aget v15, v15, v7

    add-int v15, p3, v15

    sget-object v16, Lwa;->m:[D

    aget-wide v17, v16, v15

    sub-double v19, p1, v17

    add-int/2addr v15, v9

    sub-int/2addr v15, v10

    aget-wide v15, v16, v15

    sub-double v15, v15, v17

    div-double v19, v19, v15

    mul-double v11, v11, v19

    add-int/lit8 v7, v7, -0x1

    :goto_6
    add-int/lit8 v13, v13, -0x1

    add-int/lit8 v9, v9, -0x1

    goto :goto_5

    :cond_5
    if-lez v2, :cond_7

    move v7, v1

    move v8, v7

    :goto_7
    sget-object v9, Lwa;->k:[I

    aget v13, v9, v7

    add-int/2addr v13, v10

    aput v13, v9, v7

    if-le v13, v2, :cond_6

    add-int/lit8 v7, v7, 0x1

    move v8, v10

    goto :goto_7

    :cond_6
    if-eqz v8, :cond_7

    add-int/lit8 v8, v7, -0x1

    :goto_8
    if-ltz v8, :cond_7

    sget-object v9, Lwa;->k:[I

    aget v13, v9, v7

    aput v13, v9, v8

    add-int/lit8 v8, v8, -0x1

    goto :goto_8

    :cond_7
    add-double/2addr v4, v11

    sget-object v7, Lwa;->l:[I

    aget v8, v7, v3

    add-int/2addr v8, v10

    aput v8, v7, v3

    if-le v8, v6, :cond_8

    goto :goto_a

    :cond_8
    move v7, v1

    :goto_9
    if-ge v7, v3, :cond_3

    sget-object v8, Lwa;->l:[I

    aput v7, v8, v7

    add-int/lit8 v7, v7, 0x1

    goto :goto_9

    :cond_9
    move v3, v9

    goto/16 :goto_4

    :cond_a
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_0

    :cond_b
    :goto_a
    return-wide v4
.end method

.method public f(Lpx0;)V
    .locals 18

    .line 1
    move-object/from16 v6, p0

    move-object/from16 v5, p1

    iget-object v0, v6, Lmo;->b:Lub0;

    iget-object v1, v6, Lmo;->a:Lzl;

    invoke-virtual {v1}, Lzl;->c()I

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Lub0;->b(II)Z

    move-result v0

    if-eqz v0, :cond_d

    iget-object v0, v6, Lmo;->b:Lub0;

    invoke-virtual {v0}, Lub0;->a()I

    move-result v0

    iget v1, v6, Lwa;->h:I

    sub-int v3, v0, v1

    if-ltz v3, :cond_c

    add-int/2addr v0, v1

    sget-object v1, Lwa;->m:[D

    array-length v1, v1

    if-ge v1, v0, :cond_0

    mul-int/lit8 v1, v0, 0x2

    new-array v1, v1, [D

    sput-object v1, Lwa;->m:[D

    :cond_0
    iget-wide v7, v6, Lwa;->e:D

    iget-wide v9, v6, Lwa;->f:D

    iget v1, v6, Lwa;->i:I

    const/4 v4, 0x2

    const/4 v11, 0x1

    if-ne v1, v4, :cond_3

    iget-object v1, v6, Lwa;->d:Lz22;

    invoke-virtual {v1}, Lz22;->f()I

    move-result v1

    if-ne v1, v0, :cond_2

    sget-object v1, Lwa;->m:[D

    iget-object v3, v6, Lwa;->d:Lz22;

    invoke-virtual {v3, v2}, Lz22;->c(I)D

    move-result-wide v12

    aput-wide v12, v1, v2

    move v1, v11

    :goto_0
    if-ge v1, v0, :cond_9

    sget-object v2, Lwa;->m:[D

    iget-object v3, v6, Lwa;->d:Lz22;

    invoke-virtual {v3, v1}, Lz22;->c(I)D

    move-result-wide v12

    aput-wide v12, v2, v1

    sget-object v2, Lwa;->m:[D

    aget-wide v12, v2, v1

    add-int/lit8 v3, v1, -0x1

    aget-wide v14, v2, v3

    cmpg-double v2, v12, v14

    if-ltz v2, :cond_1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Knot not in sorted order! (knot["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v3, "] < knot["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, "-1])"

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "knotVector.size("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, v6, Lwa;->d:Lz22;

    invoke-virtual {v3}, Lz22;->f()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v3, ") != "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_3
    const-wide/high16 v12, 0x3ff0000000000000L    # 1.0

    if-ne v1, v11, :cond_5

    add-int/lit8 v1, v0, -0x1

    int-to-double v14, v1

    div-double v14, v12, v14

    :goto_1
    if-ge v2, v0, :cond_4

    sget-object v1, Lwa;->m:[D

    int-to-double v4, v2

    mul-double/2addr v4, v14

    aput-wide v4, v1, v2

    add-int/lit8 v2, v2, 0x1

    move-object/from16 v5, p1

    const/4 v4, 0x2

    goto :goto_1

    :cond_4
    iget-boolean v0, v6, Lwa;->j:Z

    if-eqz v0, :cond_9

    iget v0, v6, Lwa;->h:I

    add-int/lit8 v1, v0, -0x1

    int-to-double v1, v1

    mul-double v7, v1, v14

    sub-int/2addr v0, v11

    int-to-double v0, v0

    mul-double/2addr v0, v14

    sub-double v9, v12, v0

    goto :goto_5

    :cond_5
    if-nez v1, :cond_9

    add-int/lit8 v1, v3, 0x1

    int-to-double v4, v1

    div-double v4, v12, v4

    :goto_2
    iget v1, v6, Lwa;->h:I

    const-wide/16 v14, 0x0

    if-ge v2, v1, :cond_6

    sget-object v1, Lwa;->m:[D

    aput-wide v14, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_6
    move v2, v11

    :goto_3
    if-gt v2, v3, :cond_7

    sget-object v16, Lwa;->m:[D

    add-int/lit8 v17, v1, 0x1

    int-to-double v14, v2

    mul-double/2addr v14, v4

    aput-wide v14, v16, v1

    add-int/lit8 v2, v2, 0x1

    move/from16 v1, v17

    const-wide/16 v14, 0x0

    goto :goto_3

    :cond_7
    :goto_4
    if-ge v1, v0, :cond_8

    sget-object v2, Lwa;->m:[D

    aput-wide v12, v2, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    :cond_8
    iget-boolean v0, v6, Lwa;->j:Z

    if-eqz v0, :cond_9

    move-wide v3, v12

    const-wide/16 v1, 0x0

    goto :goto_6

    :cond_9
    :goto_5
    move-wide v1, v7

    move-wide v3, v9

    :goto_6
    sget-object v0, Lwa;->k:[I

    array-length v0, v0

    iget v5, v6, Lwa;->h:I

    if-ge v0, v5, :cond_a

    mul-int/lit8 v0, v5, 0x2

    new-array v0, v0, [I

    sput-object v0, Lwa;->k:[I

    const/4 v0, 0x2

    mul-int/2addr v5, v0

    new-array v0, v5, [I

    sput-object v0, Lwa;->l:[I

    :cond_a
    invoke-virtual/range {p1 .. p1}, Lpx0;->c()I

    move-result v0

    add-int/2addr v0, v11

    new-array v0, v0, [D

    invoke-virtual/range {p1 .. p1}, Lpx0;->c()I

    move-result v5

    aput-wide v1, v0, v5

    invoke-virtual {v6, v0}, Lwa;->c([D)V

    iget-boolean v5, v6, Lmo;->c:Z

    if-eqz v5, :cond_b

    move-object/from16 v5, p1

    invoke-virtual {v5, v0}, Lpx0;->e([D)V

    goto :goto_7

    :cond_b
    move-object/from16 v5, p1

    invoke-virtual {v5, v0}, Lpx0;->f([D)V

    :goto_7
    move-object/from16 v0, p0

    move-object/from16 v5, p1

    invoke-static/range {v0 .. v5}, Lac;->b(Lz21;DDLpx0;)V

    return-void

    :cond_c
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "group iterator size - degree < 0"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_d
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Group iterator not in range"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public g(I)V
    .locals 1

    .line 1
    if-lez p1, :cond_0

    add-int/lit8 p1, p1, 0x1

    iput p1, p0, Lwa;->h:I

    return-void

    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Degree > 0 required."

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public h(Lz22;)V
    .locals 1

    .line 1
    if-eqz p1, :cond_0

    iput-object p1, p0, Lwa;->d:Lz22;

    return-void

    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Knot-vector cannot be null."

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public i(I)V
    .locals 1

    .line 1
    if-ltz p1, :cond_0

    const/4 v0, 0x2

    if-gt p1, v0, :cond_0

    iput p1, p0, Lwa;->i:I

    return-void

    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Unknown knot-vector type."

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

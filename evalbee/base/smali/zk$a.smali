.class public final Lzk$a;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lzk;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# instance fields
.field public a:Z

.field public b:Z

.field public c:Landroidx/work/NetworkType;

.field public d:Z

.field public e:Z

.field public f:J

.field public g:J

.field public h:Ljava/util/Set;


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Landroidx/work/NetworkType;->NOT_REQUIRED:Landroidx/work/NetworkType;

    iput-object v0, p0, Lzk$a;->c:Landroidx/work/NetworkType;

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lzk$a;->f:J

    iput-wide v0, p0, Lzk$a;->g:J

    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v0, p0, Lzk$a;->h:Ljava/util/Set;

    return-void
.end method


# virtual methods
.method public final a()Lzk;
    .locals 12

    .line 1
    iget-object v0, p0, Lzk$a;->h:Ljava/util/Set;

    check-cast v0, Ljava/lang/Iterable;

    invoke-static {v0}, Lvh;->S(Ljava/lang/Iterable;)Ljava/util/Set;

    move-result-object v11

    iget-wide v7, p0, Lzk$a;->f:J

    iget-wide v9, p0, Lzk$a;->g:J

    iget-boolean v3, p0, Lzk$a;->a:Z

    iget-boolean v0, p0, Lzk$a;->b:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    move v4, v0

    iget-object v2, p0, Lzk$a;->c:Landroidx/work/NetworkType;

    iget-boolean v5, p0, Lzk$a;->d:Z

    iget-boolean v6, p0, Lzk$a;->e:Z

    new-instance v0, Lzk;

    move-object v1, v0

    invoke-direct/range {v1 .. v11}, Lzk;-><init>(Landroidx/work/NetworkType;ZZZZJJLjava/util/Set;)V

    return-object v0
.end method

.method public final b(Landroidx/work/NetworkType;)Lzk$a;
    .locals 1

    .line 1
    const-string v0, "networkType"

    invoke-static {p1, v0}, Lfg0;->e(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lzk$a;->c:Landroidx/work/NetworkType;

    return-object p0
.end method

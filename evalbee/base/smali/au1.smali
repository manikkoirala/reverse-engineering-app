.class public final Lau1;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Lcom/google/firebase/firestore/core/q;

.field public final b:I

.field public final c:J

.field public final d:Lcom/google/firebase/firestore/local/QueryPurpose;

.field public final e:Lqo1;

.field public final f:Lqo1;

.field public final g:Lcom/google/protobuf/ByteString;

.field public final h:Ljava/lang/Integer;


# direct methods
.method public constructor <init>(Lcom/google/firebase/firestore/core/q;IJLcom/google/firebase/firestore/local/QueryPurpose;)V
    .locals 10

    .line 1
    sget-object v7, Lqo1;->b:Lqo1;

    sget-object v8, Lcom/google/firebase/firestore/remote/i;->t:Lcom/google/protobuf/ByteString;

    const/4 v9, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-wide v3, p3

    move-object v5, p5

    move-object v6, v7

    invoke-direct/range {v0 .. v9}, Lau1;-><init>(Lcom/google/firebase/firestore/core/q;IJLcom/google/firebase/firestore/local/QueryPurpose;Lqo1;Lqo1;Lcom/google/protobuf/ByteString;Ljava/lang/Integer;)V

    return-void
.end method

.method public constructor <init>(Lcom/google/firebase/firestore/core/q;IJLcom/google/firebase/firestore/local/QueryPurpose;Lqo1;Lqo1;Lcom/google/protobuf/ByteString;Ljava/lang/Integer;)V
    .locals 0

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lk71;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/google/firebase/firestore/core/q;

    iput-object p1, p0, Lau1;->a:Lcom/google/firebase/firestore/core/q;

    iput p2, p0, Lau1;->b:I

    iput-wide p3, p0, Lau1;->c:J

    iput-object p7, p0, Lau1;->f:Lqo1;

    iput-object p5, p0, Lau1;->d:Lcom/google/firebase/firestore/local/QueryPurpose;

    invoke-static {p6}, Lk71;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lqo1;

    iput-object p1, p0, Lau1;->e:Lqo1;

    invoke-static {p8}, Lk71;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/google/protobuf/ByteString;

    iput-object p1, p0, Lau1;->g:Lcom/google/protobuf/ByteString;

    iput-object p9, p0, Lau1;->h:Ljava/lang/Integer;

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/Integer;
    .locals 1

    .line 1
    iget-object v0, p0, Lau1;->h:Ljava/lang/Integer;

    return-object v0
.end method

.method public b()Lqo1;
    .locals 1

    .line 1
    iget-object v0, p0, Lau1;->f:Lqo1;

    return-object v0
.end method

.method public c()Lcom/google/firebase/firestore/local/QueryPurpose;
    .locals 1

    .line 1
    iget-object v0, p0, Lau1;->d:Lcom/google/firebase/firestore/local/QueryPurpose;

    return-object v0
.end method

.method public d()Lcom/google/protobuf/ByteString;
    .locals 1

    .line 1
    iget-object v0, p0, Lau1;->g:Lcom/google/protobuf/ByteString;

    return-object v0
.end method

.method public e()J
    .locals 2

    .line 1
    iget-wide v0, p0, Lau1;->c:J

    return-wide v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6

    .line 1
    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-eqz p1, :cond_3

    const-class v2, Lau1;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_1

    goto :goto_1

    :cond_1
    check-cast p1, Lau1;

    iget-object v2, p0, Lau1;->a:Lcom/google/firebase/firestore/core/q;

    iget-object v3, p1, Lau1;->a:Lcom/google/firebase/firestore/core/q;

    invoke-virtual {v2, v3}, Lcom/google/firebase/firestore/core/q;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget v2, p0, Lau1;->b:I

    iget v3, p1, Lau1;->b:I

    if-ne v2, v3, :cond_2

    iget-wide v2, p0, Lau1;->c:J

    iget-wide v4, p1, Lau1;->c:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_2

    iget-object v2, p0, Lau1;->d:Lcom/google/firebase/firestore/local/QueryPurpose;

    iget-object v3, p1, Lau1;->d:Lcom/google/firebase/firestore/local/QueryPurpose;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lau1;->e:Lqo1;

    iget-object v3, p1, Lau1;->e:Lqo1;

    invoke-virtual {v2, v3}, Lqo1;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lau1;->f:Lqo1;

    iget-object v3, p1, Lau1;->f:Lqo1;

    invoke-virtual {v2, v3}, Lqo1;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lau1;->g:Lcom/google/protobuf/ByteString;

    iget-object v3, p1, Lau1;->g:Lcom/google/protobuf/ByteString;

    invoke-virtual {v2, v3}, Lcom/google/protobuf/ByteString;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lau1;->h:Ljava/lang/Integer;

    iget-object p1, p1, Lau1;->h:Ljava/lang/Integer;

    invoke-static {v2, p1}, Ljava/util/Objects;->equals(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    move v0, v1

    :goto_0
    return v0

    :cond_3
    :goto_1
    return v1
.end method

.method public f()Lqo1;
    .locals 1

    .line 1
    iget-object v0, p0, Lau1;->e:Lqo1;

    return-object v0
.end method

.method public g()Lcom/google/firebase/firestore/core/q;
    .locals 1

    .line 1
    iget-object v0, p0, Lau1;->a:Lcom/google/firebase/firestore/core/q;

    return-object v0
.end method

.method public h()I
    .locals 1

    .line 1
    iget v0, p0, Lau1;->b:I

    return v0
.end method

.method public hashCode()I
    .locals 3

    .line 1
    iget-object v0, p0, Lau1;->a:Lcom/google/firebase/firestore/core/q;

    invoke-virtual {v0}, Lcom/google/firebase/firestore/core/q;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lau1;->b:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v1, p0, Lau1;->c:J

    long-to-int v1, v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lau1;->d:Lcom/google/firebase/firestore/local/QueryPurpose;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lau1;->e:Lqo1;

    invoke-virtual {v1}, Lqo1;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lau1;->f:Lqo1;

    invoke-virtual {v1}, Lqo1;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lau1;->g:Lcom/google/protobuf/ByteString;

    invoke-virtual {v1}, Lcom/google/protobuf/ByteString;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lau1;->h:Ljava/lang/Integer;

    invoke-static {v1}, Ljava/util/Objects;->hashCode(Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public i(Ljava/lang/Integer;)Lau1;
    .locals 11

    .line 1
    new-instance v10, Lau1;

    iget-object v1, p0, Lau1;->a:Lcom/google/firebase/firestore/core/q;

    iget v2, p0, Lau1;->b:I

    iget-wide v3, p0, Lau1;->c:J

    iget-object v5, p0, Lau1;->d:Lcom/google/firebase/firestore/local/QueryPurpose;

    iget-object v6, p0, Lau1;->e:Lqo1;

    iget-object v7, p0, Lau1;->f:Lqo1;

    iget-object v8, p0, Lau1;->g:Lcom/google/protobuf/ByteString;

    move-object v0, v10

    move-object v9, p1

    invoke-direct/range {v0 .. v9}, Lau1;-><init>(Lcom/google/firebase/firestore/core/q;IJLcom/google/firebase/firestore/local/QueryPurpose;Lqo1;Lqo1;Lcom/google/protobuf/ByteString;Ljava/lang/Integer;)V

    return-object v10
.end method

.method public j(Lqo1;)Lau1;
    .locals 11

    .line 1
    new-instance v10, Lau1;

    iget-object v1, p0, Lau1;->a:Lcom/google/firebase/firestore/core/q;

    iget v2, p0, Lau1;->b:I

    iget-wide v3, p0, Lau1;->c:J

    iget-object v5, p0, Lau1;->d:Lcom/google/firebase/firestore/local/QueryPurpose;

    iget-object v6, p0, Lau1;->e:Lqo1;

    iget-object v8, p0, Lau1;->g:Lcom/google/protobuf/ByteString;

    iget-object v9, p0, Lau1;->h:Ljava/lang/Integer;

    move-object v0, v10

    move-object v7, p1

    invoke-direct/range {v0 .. v9}, Lau1;-><init>(Lcom/google/firebase/firestore/core/q;IJLcom/google/firebase/firestore/local/QueryPurpose;Lqo1;Lqo1;Lcom/google/protobuf/ByteString;Ljava/lang/Integer;)V

    return-object v10
.end method

.method public k(Lcom/google/protobuf/ByteString;Lqo1;)Lau1;
    .locals 11

    .line 1
    new-instance v10, Lau1;

    iget-object v1, p0, Lau1;->a:Lcom/google/firebase/firestore/core/q;

    iget v2, p0, Lau1;->b:I

    iget-wide v3, p0, Lau1;->c:J

    iget-object v5, p0, Lau1;->d:Lcom/google/firebase/firestore/local/QueryPurpose;

    iget-object v7, p0, Lau1;->f:Lqo1;

    const/4 v9, 0x0

    move-object v0, v10

    move-object v6, p2

    move-object v8, p1

    invoke-direct/range {v0 .. v9}, Lau1;-><init>(Lcom/google/firebase/firestore/core/q;IJLcom/google/firebase/firestore/local/QueryPurpose;Lqo1;Lqo1;Lcom/google/protobuf/ByteString;Ljava/lang/Integer;)V

    return-object v10
.end method

.method public l(J)Lau1;
    .locals 11

    .line 1
    new-instance v10, Lau1;

    iget-object v1, p0, Lau1;->a:Lcom/google/firebase/firestore/core/q;

    iget v2, p0, Lau1;->b:I

    iget-object v5, p0, Lau1;->d:Lcom/google/firebase/firestore/local/QueryPurpose;

    iget-object v6, p0, Lau1;->e:Lqo1;

    iget-object v7, p0, Lau1;->f:Lqo1;

    iget-object v8, p0, Lau1;->g:Lcom/google/protobuf/ByteString;

    iget-object v9, p0, Lau1;->h:Ljava/lang/Integer;

    move-object v0, v10

    move-wide v3, p1

    invoke-direct/range {v0 .. v9}, Lau1;-><init>(Lcom/google/firebase/firestore/core/q;IJLcom/google/firebase/firestore/local/QueryPurpose;Lqo1;Lqo1;Lcom/google/protobuf/ByteString;Ljava/lang/Integer;)V

    return-object v10
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "TargetData{target="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lau1;->a:Lcom/google/firebase/firestore/core/q;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", targetId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lau1;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", sequenceNumber="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lau1;->c:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", purpose="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lau1;->d:Lcom/google/firebase/firestore/local/QueryPurpose;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", snapshotVersion="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lau1;->e:Lqo1;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", lastLimboFreeSnapshotVersion="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lau1;->f:Lqo1;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", resumeToken="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lau1;->g:Lcom/google/protobuf/ByteString;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", expectedCount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lau1;->h:Ljava/lang/Integer;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

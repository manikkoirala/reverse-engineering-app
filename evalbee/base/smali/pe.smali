.class public final Lpe;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:J

.field public final b:J

.field public final c:J

.field public final d:J

.field public final e:J

.field public final f:J


# direct methods
.method public constructor <init>(JJJJJJ)V
    .locals 18

    .line 1
    move-object/from16 v0, p0

    move-wide/from16 v1, p1

    move-wide/from16 v3, p3

    move-wide/from16 v5, p5

    move-wide/from16 v7, p7

    move-wide/from16 v9, p9

    move-wide/from16 v11, p11

    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v13, 0x0

    cmp-long v15, v1, v13

    const/16 v16, 0x1

    const/16 v17, 0x0

    if-ltz v15, :cond_0

    move/from16 v15, v16

    goto :goto_0

    :cond_0
    move/from16 v15, v17

    :goto_0
    invoke-static {v15}, Li71;->d(Z)V

    cmp-long v15, v3, v13

    if-ltz v15, :cond_1

    move/from16 v15, v16

    goto :goto_1

    :cond_1
    move/from16 v15, v17

    :goto_1
    invoke-static {v15}, Li71;->d(Z)V

    cmp-long v15, v5, v13

    if-ltz v15, :cond_2

    move/from16 v15, v16

    goto :goto_2

    :cond_2
    move/from16 v15, v17

    :goto_2
    invoke-static {v15}, Li71;->d(Z)V

    cmp-long v15, v7, v13

    if-ltz v15, :cond_3

    move/from16 v15, v16

    goto :goto_3

    :cond_3
    move/from16 v15, v17

    :goto_3
    invoke-static {v15}, Li71;->d(Z)V

    cmp-long v15, v9, v13

    if-ltz v15, :cond_4

    move/from16 v15, v16

    goto :goto_4

    :cond_4
    move/from16 v15, v17

    :goto_4
    invoke-static {v15}, Li71;->d(Z)V

    cmp-long v13, v11, v13

    if-ltz v13, :cond_5

    goto :goto_5

    :cond_5
    move/from16 v16, v17

    :goto_5
    invoke-static/range {v16 .. v16}, Li71;->d(Z)V

    iput-wide v1, v0, Lpe;->a:J

    iput-wide v3, v0, Lpe;->b:J

    iput-wide v5, v0, Lpe;->c:J

    iput-wide v7, v0, Lpe;->d:J

    iput-wide v9, v0, Lpe;->e:J

    iput-wide v11, v0, Lpe;->f:J

    return-void
.end method


# virtual methods
.method public a()J
    .locals 2

    .line 1
    iget-wide v0, p0, Lpe;->f:J

    return-wide v0
.end method

.method public b()J
    .locals 2

    .line 1
    iget-wide v0, p0, Lpe;->a:J

    return-wide v0
.end method

.method public c()J
    .locals 2

    .line 1
    iget-wide v0, p0, Lpe;->d:J

    return-wide v0
.end method

.method public d()J
    .locals 2

    .line 1
    iget-wide v0, p0, Lpe;->c:J

    return-wide v0
.end method

.method public e()J
    .locals 2

    .line 1
    iget-wide v0, p0, Lpe;->b:J

    return-wide v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6

    .line 1
    instance-of v0, p1, Lpe;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    check-cast p1, Lpe;

    iget-wide v2, p0, Lpe;->a:J

    iget-wide v4, p1, Lpe;->a:J

    cmp-long v0, v2, v4

    if-nez v0, :cond_0

    iget-wide v2, p0, Lpe;->b:J

    iget-wide v4, p1, Lpe;->b:J

    cmp-long v0, v2, v4

    if-nez v0, :cond_0

    iget-wide v2, p0, Lpe;->c:J

    iget-wide v4, p1, Lpe;->c:J

    cmp-long v0, v2, v4

    if-nez v0, :cond_0

    iget-wide v2, p0, Lpe;->d:J

    iget-wide v4, p1, Lpe;->d:J

    cmp-long v0, v2, v4

    if-nez v0, :cond_0

    iget-wide v2, p0, Lpe;->e:J

    iget-wide v4, p1, Lpe;->e:J

    cmp-long v0, v2, v4

    if-nez v0, :cond_0

    iget-wide v2, p0, Lpe;->f:J

    iget-wide v4, p1, Lpe;->f:J

    cmp-long p1, v2, v4

    if-nez p1, :cond_0

    const/4 v1, 0x1

    :cond_0
    return v1
.end method

.method public f()J
    .locals 2

    .line 1
    iget-wide v0, p0, Lpe;->e:J

    return-wide v0
.end method

.method public hashCode()I
    .locals 8

    .line 1
    iget-wide v0, p0, Lpe;->a:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iget-wide v0, p0, Lpe;->b:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    iget-wide v0, p0, Lpe;->c:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    iget-wide v0, p0, Lpe;->d:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    iget-wide v0, p0, Lpe;->e:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    iget-wide v0, p0, Lpe;->f:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    filled-new-array/range {v2 .. v7}, [Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lb11;->b([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .line 1
    invoke-static {p0}, Lcom/google/common/base/a;->c(Ljava/lang/Object;)Lcom/google/common/base/a$b;

    move-result-object v0

    const-string v1, "hitCount"

    iget-wide v2, p0, Lpe;->a:J

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/common/base/a$b;->c(Ljava/lang/String;J)Lcom/google/common/base/a$b;

    move-result-object v0

    const-string v1, "missCount"

    iget-wide v2, p0, Lpe;->b:J

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/common/base/a$b;->c(Ljava/lang/String;J)Lcom/google/common/base/a$b;

    move-result-object v0

    const-string v1, "loadSuccessCount"

    iget-wide v2, p0, Lpe;->c:J

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/common/base/a$b;->c(Ljava/lang/String;J)Lcom/google/common/base/a$b;

    move-result-object v0

    const-string v1, "loadExceptionCount"

    iget-wide v2, p0, Lpe;->d:J

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/common/base/a$b;->c(Ljava/lang/String;J)Lcom/google/common/base/a$b;

    move-result-object v0

    const-string v1, "totalLoadTime"

    iget-wide v2, p0, Lpe;->e:J

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/common/base/a$b;->c(Ljava/lang/String;J)Lcom/google/common/base/a$b;

    move-result-object v0

    const-string v1, "evictionCount"

    iget-wide v2, p0, Lpe;->f:J

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/common/base/a$b;->c(Ljava/lang/String;J)Lcom/google/common/base/a$b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/common/base/a$b;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

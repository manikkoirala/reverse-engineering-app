.class public final Lui0;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SourceFile"

# interfaces
.implements Lxv0;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lui0$b;
    }
.end annotation


# static fields
.field private static final DEFAULT_INSTANCE:Lui0;

.field public static final LATITUDE_FIELD_NUMBER:I = 0x1

.field public static final LONGITUDE_FIELD_NUMBER:I = 0x2

.field private static volatile PARSER:Lb31;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lb31;"
        }
    .end annotation
.end field


# instance fields
.field private latitude_:D

.field private longitude_:D


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lui0;

    invoke-direct {v0}, Lui0;-><init>()V

    sput-object v0, Lui0;->DEFAULT_INSTANCE:Lui0;

    const-class v1, Lui0;

    invoke-static {v1, v0}, Lcom/google/protobuf/GeneratedMessageLite;->V(Ljava/lang/Class;Lcom/google/protobuf/GeneratedMessageLite;)V

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    return-void
.end method

.method public static synthetic Z()Lui0;
    .locals 1

    .line 1
    sget-object v0, Lui0;->DEFAULT_INSTANCE:Lui0;

    return-object v0
.end method

.method public static synthetic a0(Lui0;D)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1, p2}, Lui0;->g0(D)V

    return-void
.end method

.method public static synthetic b0(Lui0;D)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1, p2}, Lui0;->h0(D)V

    return-void
.end method

.method public static c0()Lui0;
    .locals 1

    .line 1
    sget-object v0, Lui0;->DEFAULT_INSTANCE:Lui0;

    return-object v0
.end method

.method public static f0()Lui0$b;
    .locals 1

    .line 1
    sget-object v0, Lui0;->DEFAULT_INSTANCE:Lui0;

    invoke-virtual {v0}, Lcom/google/protobuf/GeneratedMessageLite;->u()Lcom/google/protobuf/GeneratedMessageLite$a;

    move-result-object v0

    check-cast v0, Lui0$b;

    return-object v0
.end method


# virtual methods
.method public d0()D
    .locals 2

    .line 1
    iget-wide v0, p0, Lui0;->latitude_:D

    return-wide v0
.end method

.method public e0()D
    .locals 2

    .line 1
    iget-wide v0, p0, Lui0;->longitude_:D

    return-wide v0
.end method

.method public final g0(D)V
    .locals 0

    .line 1
    iput-wide p1, p0, Lui0;->latitude_:D

    return-void
.end method

.method public final h0(D)V
    .locals 0

    .line 1
    iput-wide p1, p0, Lui0;->longitude_:D

    return-void
.end method

.method public final y(Lcom/google/protobuf/GeneratedMessageLite$MethodToInvoke;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 1
    sget-object p2, Lui0$a;->a:[I

    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result p1

    aget p1, p2, p1

    const/4 p2, 0x0

    packed-switch p1, :pswitch_data_0

    new-instance p1, Ljava/lang/UnsupportedOperationException;

    invoke-direct {p1}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw p1

    :pswitch_0
    return-object p2

    :pswitch_1
    const/4 p1, 0x1

    invoke-static {p1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object p1

    return-object p1

    :pswitch_2
    sget-object p1, Lui0;->PARSER:Lb31;

    if-nez p1, :cond_1

    const-class p2, Lui0;

    monitor-enter p2

    :try_start_0
    sget-object p1, Lui0;->PARSER:Lb31;

    if-nez p1, :cond_0

    new-instance p1, Lcom/google/protobuf/GeneratedMessageLite$b;

    sget-object p3, Lui0;->DEFAULT_INSTANCE:Lui0;

    invoke-direct {p1, p3}, Lcom/google/protobuf/GeneratedMessageLite$b;-><init>(Lcom/google/protobuf/GeneratedMessageLite;)V

    sput-object p1, Lui0;->PARSER:Lb31;

    :cond_0
    monitor-exit p2

    goto :goto_0

    :catchall_0
    move-exception p1

    monitor-exit p2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1

    :cond_1
    :goto_0
    return-object p1

    :pswitch_3
    sget-object p1, Lui0;->DEFAULT_INSTANCE:Lui0;

    return-object p1

    :pswitch_4
    const-string p1, "latitude_"

    const-string p2, "longitude_"

    filled-new-array {p1, p2}, [Ljava/lang/Object;

    move-result-object p1

    const-string p2, "\u0000\u0002\u0000\u0000\u0001\u0002\u0002\u0000\u0000\u0000\u0001\u0000\u0002\u0000"

    sget-object p3, Lui0;->DEFAULT_INSTANCE:Lui0;

    invoke-static {p3, p2, p1}, Lcom/google/protobuf/GeneratedMessageLite;->N(Lcom/google/protobuf/a0;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    return-object p1

    :pswitch_5
    new-instance p1, Lui0$b;

    invoke-direct {p1, p2}, Lui0$b;-><init>(Lui0$a;)V

    return-object p1

    :pswitch_6
    new-instance p1, Lui0;

    invoke-direct {p1}, Lui0;-><init>()V

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

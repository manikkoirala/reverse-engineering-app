.class public Lz1;
.super Lcom/google/android/gms/common/internal/safeparcel/AbstractSafeParcelable;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lz1$a;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lz1;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public final e:Z

.field public final f:Ljava/lang/String;

.field public final g:Z

.field public h:Ljava/lang/String;

.field public i:I

.field public j:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .line 1
    new-instance v0, Lmc2;

    invoke-direct {v0}, Lmc2;-><init>()V

    sput-object v0, Lz1;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;ZLjava/lang/String;ILjava/lang/String;)V
    .locals 0

    .line 3
    invoke-direct {p0}, Lcom/google/android/gms/common/internal/safeparcel/AbstractSafeParcelable;-><init>()V

    iput-object p1, p0, Lz1;->a:Ljava/lang/String;

    iput-object p2, p0, Lz1;->b:Ljava/lang/String;

    iput-object p3, p0, Lz1;->c:Ljava/lang/String;

    iput-object p4, p0, Lz1;->d:Ljava/lang/String;

    iput-boolean p5, p0, Lz1;->e:Z

    iput-object p6, p0, Lz1;->f:Ljava/lang/String;

    iput-boolean p7, p0, Lz1;->g:Z

    iput-object p8, p0, Lz1;->h:Ljava/lang/String;

    iput p9, p0, Lz1;->i:I

    iput-object p10, p0, Lz1;->j:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lz1$a;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/google/android/gms/common/internal/safeparcel/AbstractSafeParcelable;-><init>()V

    invoke-static {p1}, Lz1$a;->j(Lz1$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lz1;->a:Ljava/lang/String;

    invoke-static {p1}, Lz1$a;->i(Lz1$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lz1;->b:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lz1;->c:Ljava/lang/String;

    invoke-static {p1}, Lz1$a;->g(Lz1$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lz1;->d:Ljava/lang/String;

    invoke-static {p1}, Lz1$a;->k(Lz1$a;)Z

    move-result v0

    iput-boolean v0, p0, Lz1;->e:Z

    invoke-static {p1}, Lz1$a;->f(Lz1$a;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lz1;->f:Ljava/lang/String;

    invoke-static {p1}, Lz1$a;->l(Lz1$a;)Z

    move-result v0

    iput-boolean v0, p0, Lz1;->g:Z

    invoke-static {p1}, Lz1$a;->h(Lz1$a;)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lz1;->j:Ljava/lang/String;

    return-void
.end method

.method public synthetic constructor <init>(Lz1$a;Lhe2;)V
    .locals 0

    .line 2
    invoke-direct {p0, p1}, Lz1;-><init>(Lz1$a;)V

    return-void
.end method

.method public static R()Lz1$a;
    .locals 2

    .line 1
    new-instance v0, Lz1$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lz1$a;-><init>(Lla2;)V

    return-object v0
.end method


# virtual methods
.method public E()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lz1;->e:Z

    return v0
.end method

.method public H()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lz1;->f:Ljava/lang/String;

    return-object v0
.end method

.method public J()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lz1;->d:Ljava/lang/String;

    return-object v0
.end method

.method public K()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lz1;->b:Ljava/lang/String;

    return-object v0
.end method

.method public O()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lz1;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final Z(Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lz1;->h:Ljava/lang/String;

    return-void
.end method

.method public i()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lz1;->g:Z

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .line 1
    invoke-static {p1}, Lcom/google/android/gms/common/internal/safeparcel/SafeParcelWriter;->beginObjectHeader(Landroid/os/Parcel;)I

    move-result p2

    invoke-virtual {p0}, Lz1;->O()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-static {p1, v1, v0, v2}, Lcom/google/android/gms/common/internal/safeparcel/SafeParcelWriter;->writeString(Landroid/os/Parcel;ILjava/lang/String;Z)V

    const/4 v0, 0x2

    invoke-virtual {p0}, Lz1;->K()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1, v2}, Lcom/google/android/gms/common/internal/safeparcel/SafeParcelWriter;->writeString(Landroid/os/Parcel;ILjava/lang/String;Z)V

    const/4 v0, 0x3

    iget-object v1, p0, Lz1;->c:Ljava/lang/String;

    invoke-static {p1, v0, v1, v2}, Lcom/google/android/gms/common/internal/safeparcel/SafeParcelWriter;->writeString(Landroid/os/Parcel;ILjava/lang/String;Z)V

    const/4 v0, 0x4

    invoke-virtual {p0}, Lz1;->J()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1, v2}, Lcom/google/android/gms/common/internal/safeparcel/SafeParcelWriter;->writeString(Landroid/os/Parcel;ILjava/lang/String;Z)V

    const/4 v0, 0x5

    invoke-virtual {p0}, Lz1;->E()Z

    move-result v1

    invoke-static {p1, v0, v1}, Lcom/google/android/gms/common/internal/safeparcel/SafeParcelWriter;->writeBoolean(Landroid/os/Parcel;IZ)V

    const/4 v0, 0x6

    invoke-virtual {p0}, Lz1;->H()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1, v2}, Lcom/google/android/gms/common/internal/safeparcel/SafeParcelWriter;->writeString(Landroid/os/Parcel;ILjava/lang/String;Z)V

    const/4 v0, 0x7

    invoke-virtual {p0}, Lz1;->i()Z

    move-result v1

    invoke-static {p1, v0, v1}, Lcom/google/android/gms/common/internal/safeparcel/SafeParcelWriter;->writeBoolean(Landroid/os/Parcel;IZ)V

    const/16 v0, 0x8

    iget-object v1, p0, Lz1;->h:Ljava/lang/String;

    invoke-static {p1, v0, v1, v2}, Lcom/google/android/gms/common/internal/safeparcel/SafeParcelWriter;->writeString(Landroid/os/Parcel;ILjava/lang/String;Z)V

    const/16 v0, 0x9

    iget v1, p0, Lz1;->i:I

    invoke-static {p1, v0, v1}, Lcom/google/android/gms/common/internal/safeparcel/SafeParcelWriter;->writeInt(Landroid/os/Parcel;II)V

    const/16 v0, 0xa

    iget-object v1, p0, Lz1;->j:Ljava/lang/String;

    invoke-static {p1, v0, v1, v2}, Lcom/google/android/gms/common/internal/safeparcel/SafeParcelWriter;->writeString(Landroid/os/Parcel;ILjava/lang/String;Z)V

    invoke-static {p1, p2}, Lcom/google/android/gms/common/internal/safeparcel/SafeParcelWriter;->finishObjectHeader(Landroid/os/Parcel;I)V

    return-void
.end method

.method public final zza()I
    .locals 1

    .line 1
    iget v0, p0, Lz1;->i:I

    return v0
.end method

.method public final zza(I)V
    .locals 0

    .line 2
    iput p1, p0, Lz1;->i:I

    return-void
.end method

.method public final zzc()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lz1;->j:Ljava/lang/String;

    return-object v0
.end method

.method public final zzd()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lz1;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final zze()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lz1;->h:Ljava/lang/String;

    return-object v0
.end method

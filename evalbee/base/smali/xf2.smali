.class public final Lxf2;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# instance fields
.field public final a:Lca1;

.field public final b:Ldd2;

.field public c:Z

.field public final synthetic d:Lzf2;


# direct methods
.method public synthetic constructor <init>(Lzf2;Lca1;Ld4;Ldd2;Lmf2;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lxf2;->d:Lzf2;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    iput-object p2, p0, Lxf2;->a:Lca1;

    iput-object p4, p0, Lxf2;->b:Ldd2;

    return-void
.end method

.method public synthetic constructor <init>(Lzf2;Lsd2;Ldd2;Lmf2;)V
    .locals 0

    .line 2
    iput-object p1, p0, Lxf2;->d:Lzf2;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    const/4 p1, 0x0

    iput-object p1, p0, Lxf2;->a:Lca1;

    iput-object p3, p0, Lxf2;->b:Ldd2;

    return-void
.end method

.method public static bridge synthetic a(Lxf2;)Lsd2;
    .locals 0

    .line 1
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 p0, 0x0

    return-object p0
.end method

.method public static bridge synthetic b(Lxf2;)Lca1;
    .locals 0

    .line 1
    iget-object p0, p0, Lxf2;->a:Lca1;

    return-object p0
.end method


# virtual methods
.method public final declared-synchronized c(Landroid/content/Context;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/content/IntentFilter;)V
    .locals 6

    .line 1
    monitor-enter p0

    :try_start_0
    iget-boolean p3, p0, Lxf2;->c:Z

    if-nez p3, :cond_1

    sget p3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 p4, 0x21

    if-lt p3, p4, :cond_0

    iget-object p3, p0, Lxf2;->d:Lzf2;

    invoke-static {p3}, Lzf2;->b(Lzf2;)Lxf2;

    move-result-object v1

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x2

    move-object v0, p1

    move-object v2, p2

    invoke-static/range {v0 .. v5}, Ltf2;->a(Landroid/content/Context;Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/os/Handler;I)Landroid/content/Intent;

    goto :goto_0

    :cond_0
    iget-object p3, p0, Lxf2;->d:Lzf2;

    invoke-static {p3}, Lzf2;->a(Lzf2;)Landroid/content/Context;

    move-result-object p3

    invoke-virtual {p3}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p3

    invoke-virtual {p3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    iget-object p3, p0, Lxf2;->d:Lzf2;

    invoke-static {p3}, Lzf2;->b(Lzf2;)Lxf2;

    move-result-object p3

    invoke-virtual {p1, p3, p2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    :goto_0
    const/4 p1, 0x1

    iput-boolean p1, p0, Lxf2;->c:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :cond_1
    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public final declared-synchronized d(Landroid/content/Context;)V
    .locals 1

    .line 1
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lxf2;->c:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lxf2;->d:Lzf2;

    invoke-static {v0}, Lzf2;->b(Lzf2;)Lxf2;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    const/4 p1, 0x0

    iput-boolean p1, p0, Lxf2;->c:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    const-string p1, "BillingBroadcastManager"

    const-string v0, "Receiver is not registered."

    invoke-static {p1, v0}, Lcom/google/android/gms/internal/play_billing/zzb;->zzk(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public final e(Landroid/os/Bundle;Lcom/android/billingclient/api/a;I)V
    .locals 2

    .line 1
    const-string v0, "FAILURE_LOGGING_PAYLOAD"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v1

    if-eqz v1, :cond_0

    :try_start_0
    iget-object p2, p0, Lxf2;->b:Ldd2;

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object p1

    invoke-static {}, Lcom/google/android/gms/internal/play_billing/zzej;->zza()Lcom/google/android/gms/internal/play_billing/zzej;

    move-result-object p3

    invoke-static {p1, p3}, Lcom/google/android/gms/internal/play_billing/zzhy;->zzx([BLcom/google/android/gms/internal/play_billing/zzej;)Lcom/google/android/gms/internal/play_billing/zzhy;

    move-result-object p1

    invoke-interface {p2, p1}, Ldd2;->a(Lcom/google/android/gms/internal/play_billing/zzhy;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    const-string p1, "BillingBroadcastManager"

    const-string p2, "Failed parsing Api failure."

    invoke-static {p1, p2}, Lcom/google/android/gms/internal/play_billing/zzb;->zzk(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_0
    iget-object p1, p0, Lxf2;->b:Ldd2;

    const/16 v0, 0x17

    invoke-static {v0, p3, p2}, Lbd2;->a(IILcom/android/billingclient/api/a;)Lcom/google/android/gms/internal/play_billing/zzhy;

    move-result-object p2

    invoke-interface {p1, p2}, Ldd2;->a(Lcom/google/android/gms/internal/play_billing/zzhy;)V

    return-void
.end method

.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6

    .line 1
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object p1

    const/4 v0, 0x1

    const-string v1, "BillingBroadcastManager"

    if-nez p1, :cond_0

    const-string p1, "Bundle is null."

    invoke-static {v1, p1}, Lcom/google/android/gms/internal/play_billing/zzb;->zzk(Ljava/lang/String;Ljava/lang/String;)V

    iget-object p1, p0, Lxf2;->b:Ldd2;

    sget-object p2, Lcom/android/billingclient/api/b;->j:Lcom/android/billingclient/api/a;

    const/16 v1, 0xb

    invoke-static {v1, v0, p2}, Lbd2;->a(IILcom/android/billingclient/api/a;)Lcom/google/android/gms/internal/play_billing/zzhy;

    move-result-object v0

    invoke-interface {p1, v0}, Ldd2;->a(Lcom/google/android/gms/internal/play_billing/zzhy;)V

    iget-object p1, p0, Lxf2;->a:Lca1;

    if-eqz p1, :cond_5

    const/4 v0, 0x0

    invoke-interface {p1, p2, v0}, Lca1;->a(Lcom/android/billingclient/api/a;Ljava/util/List;)V

    return-void

    :cond_0
    invoke-static {p2, v1}, Lcom/google/android/gms/internal/play_billing/zzb;->zze(Landroid/content/Intent;Ljava/lang/String;)Lcom/android/billingclient/api/a;

    move-result-object v2

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object p2

    const-string v3, "INTENT_SOURCE"

    invoke-virtual {p1, v3}, Landroid/os/BaseBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x2

    const-string v5, "LAUNCH_BILLING_FLOW"

    if-eq v3, v5, :cond_1

    if-eqz v3, :cond_2

    invoke-virtual {v3, v5}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    :cond_1
    move v0, v4

    :cond_2
    const-string v3, "com.android.vending.billing.PURCHASES_UPDATED"

    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_6

    const-string v3, "com.android.vending.billing.LOCAL_BROADCAST_PURCHASES_UPDATED"

    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    goto :goto_0

    :cond_3
    const-string v3, "com.android.vending.billing.ALTERNATIVE_BILLING"

    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_5

    invoke-virtual {v2}, Lcom/android/billingclient/api/a;->b()I

    move-result p2

    if-eqz p2, :cond_4

    invoke-virtual {p0, p1, v2, v0}, Lxf2;->e(Landroid/os/Bundle;Lcom/android/billingclient/api/a;I)V

    iget-object p1, p0, Lxf2;->a:Lca1;

    invoke-static {}, Lcom/google/android/gms/internal/play_billing/zzaf;->zzk()Lcom/google/android/gms/internal/play_billing/zzaf;

    move-result-object p2

    invoke-interface {p1, v2, p2}, Lca1;->a(Lcom/android/billingclient/api/a;Ljava/util/List;)V

    return-void

    :cond_4
    const-string p1, "AlternativeBillingListener and UserChoiceBillingListener is null."

    invoke-static {v1, p1}, Lcom/google/android/gms/internal/play_billing/zzb;->zzk(Ljava/lang/String;Ljava/lang/String;)V

    iget-object p1, p0, Lxf2;->b:Ldd2;

    sget-object p2, Lcom/android/billingclient/api/b;->j:Lcom/android/billingclient/api/a;

    const/16 v1, 0x4d

    invoke-static {v1, v0, p2}, Lbd2;->a(IILcom/android/billingclient/api/a;)Lcom/google/android/gms/internal/play_billing/zzhy;

    move-result-object v0

    invoke-interface {p1, v0}, Ldd2;->a(Lcom/google/android/gms/internal/play_billing/zzhy;)V

    iget-object p1, p0, Lxf2;->a:Lca1;

    invoke-static {}, Lcom/google/android/gms/internal/play_billing/zzaf;->zzk()Lcom/google/android/gms/internal/play_billing/zzaf;

    move-result-object v0

    invoke-interface {p1, p2, v0}, Lca1;->a(Lcom/android/billingclient/api/a;Ljava/util/List;)V

    :cond_5
    return-void

    :cond_6
    :goto_0
    invoke-static {p1}, Lcom/google/android/gms/internal/play_billing/zzb;->zzi(Landroid/os/Bundle;)Ljava/util/List;

    move-result-object p2

    invoke-virtual {v2}, Lcom/android/billingclient/api/a;->b()I

    move-result v1

    if-nez v1, :cond_7

    iget-object p1, p0, Lxf2;->b:Ldd2;

    invoke-static {v0}, Lbd2;->b(I)Lcom/google/android/gms/internal/play_billing/zzic;

    move-result-object v0

    invoke-interface {p1, v0}, Ldd2;->c(Lcom/google/android/gms/internal/play_billing/zzic;)V

    goto :goto_1

    :cond_7
    invoke-virtual {p0, p1, v2, v0}, Lxf2;->e(Landroid/os/Bundle;Lcom/android/billingclient/api/a;I)V

    :goto_1
    iget-object p1, p0, Lxf2;->a:Lca1;

    invoke-interface {p1, v2, p2}, Lca1;->a(Lcom/android/billingclient/api/a;Ljava/util/List;)V

    return-void
.end method

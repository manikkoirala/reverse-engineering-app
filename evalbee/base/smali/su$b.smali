.class public Lsu$b;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lsu;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "b"
.end annotation


# instance fields
.field public a:[[Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Landroid/app/ProgressDialog;

.field public final synthetic e:Lsu;


# direct methods
.method public constructor <init>(Lsu;)V
    .locals 3

    .line 1
    iput-object p1, p0, Lsu$b;->e:Lsu;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lsu$b;->b:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p1}, Lsu;->a(Lsu;)Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    move-result-object p1

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;->getExamName()Ljava/lang/String;

    move-result-object p1

    const-string v1, "[^a-zA-Z0-9_-]"

    const-string v2, "_"

    invoke-virtual {p1, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, ".xls"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lsu$b;->c:Ljava/lang/String;

    return-void
.end method

.method public synthetic constructor <init>(Lsu;Lsu$a;)V
    .locals 0

    .line 2
    invoke-direct {p0, p1}, Lsu$b;-><init>(Lsu;)V

    return-void
.end method


# virtual methods
.method public varargs a([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 9

    .line 1
    iget-object p1, p0, Lsu$b;->e:Lsu;

    invoke-static {p1}, Lsu;->b(Lsu;)Landroid/content/Context;

    move-result-object p1

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;

    move-result-object p1

    iget-object v0, p0, Lsu$b;->e:Lsu;

    invoke-static {v0}, Lsu;->a(Lsu;)Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getTemplateJson(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;

    move-result-object p1

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->getSheetTemplate()Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    move-result-object v4

    iget-object p1, p0, Lsu$b;->e:Lsu;

    invoke-static {p1}, Lsu;->b(Lsu;)Landroid/content/Context;

    move-result-object v0

    invoke-static {p1, v0, v4}, Lsu;->c(Lsu;Landroid/content/Context;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)Ljava/util/ArrayList;

    move-result-object v3

    new-instance v0, Lwe1;

    invoke-direct {v0}, Lwe1;-><init>()V

    iget-object p1, p0, Lsu$b;->e:Lsu;

    invoke-static {p1}, Lsu;->b(Lsu;)Landroid/content/Context;

    move-result-object v1

    iget-object p1, p0, Lsu$b;->e:Lsu;

    invoke-static {p1}, Lsu;->a(Lsu;)Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    move-result-object v2

    iget-object p1, p0, Lsu$b;->e:Lsu;

    iget-boolean v5, p1, Lsu;->c:Z

    invoke-virtual/range {v0 .. v5}, Lwe1;->d(Landroid/content/Context;Lcom/ekodroid/omrevaluator/templateui/models/ExamId;Ljava/util/ArrayList;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;Z)[[Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lsu$b;->a:[[Ljava/lang/String;

    new-instance p1, Lorg/apache/poi/xssf/usermodel/XSSFWorkbook;

    invoke-direct {p1}, Lorg/apache/poi/xssf/usermodel/XSSFWorkbook;-><init>()V

    const-string v0, "Reports"

    invoke-virtual {p1, v0}, Lorg/apache/poi/xssf/usermodel/XSSFWorkbook;->createSheet(Ljava/lang/String;)Lorg/apache/poi/xssf/usermodel/XSSFSheet;

    move-result-object v0

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    iget-object v3, p0, Lsu$b;->a:[[Ljava/lang/String;

    array-length v4, v3

    if-ge v2, v4, :cond_1

    aget-object v3, v3, v2

    invoke-virtual {v0, v2}, Lorg/apache/poi/xssf/usermodel/XSSFSheet;->createRow(I)Lorg/apache/poi/xssf/usermodel/XSSFRow;

    move-result-object v4

    move v5, v1

    :goto_1
    array-length v6, v3

    if-ge v5, v6, :cond_0

    invoke-virtual {v4, v5}, Lorg/apache/poi/xssf/usermodel/XSSFRow;->createCell(I)Lorg/apache/poi/xssf/usermodel/XSSFCell;

    move-result-object v6

    new-instance v7, Lorg/apache/poi/xssf/usermodel/XSSFRichTextString;

    aget-object v8, v3, v5

    invoke-direct {v7, v8}, Lorg/apache/poi/xssf/usermodel/XSSFRichTextString;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v7}, Lorg/apache/poi/xssf/usermodel/XSSFCell;->setCellValue(Lorg/apache/poi/ss/usermodel/RichTextString;)V

    add-int/lit8 v5, v5, 0x1

    goto :goto_1

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lsu$b;->e:Lsu;

    invoke-static {v0}, Lsu;->b(Lsu;)Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lok;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/Context;->getExternalFilesDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    :cond_2
    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lsu$b;->c:Ljava/lang/String;

    invoke-direct {v1, v0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lsu$b;->b:Ljava/lang/String;

    const/4 v0, 0x0

    :try_start_0
    new-instance v2, Ljava/io/FileOutputStream;

    invoke-direct {v2, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-virtual {p1, v2}, Lorg/apache/poi/POIXMLDocument;->write(Ljava/io/OutputStream;)V

    invoke-virtual {p1}, Lorg/apache/poi/POIXMLDocument;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    invoke-virtual {v2}, Ljava/io/OutputStream;->flush()V

    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_3

    :catch_0
    move-exception p1

    goto :goto_2

    :catchall_0
    move-exception p1

    goto :goto_4

    :catch_1
    move-exception p1

    move-object v2, v0

    :goto_2
    :try_start_3
    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V

    invoke-virtual {v1}, Ljava/io/File;->deleteOnExit()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    if-eqz v2, :cond_3

    :try_start_4
    invoke-virtual {v2}, Ljava/io/OutputStream;->flush()V

    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_3

    :catch_2
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V

    :cond_3
    :goto_3
    return-object v0

    :catchall_1
    move-exception p1

    move-object v0, v2

    :goto_4
    if-eqz v0, :cond_4

    :try_start_5
    invoke-virtual {v0}, Ljava/io/OutputStream;->flush()V

    invoke-virtual {v0}, Ljava/io/FileOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    goto :goto_5

    :catch_3
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    :cond_4
    :goto_5
    throw p1
.end method

.method public b(Ljava/lang/Void;)V
    .locals 3

    .line 1
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    iget-object p1, p0, Lsu$b;->d:Landroid/app/ProgressDialog;

    invoke-virtual {p1}, Landroid/app/Dialog;->dismiss()V

    new-instance p1, Lhn1;

    iget-object v0, p0, Lsu$b;->e:Lsu;

    invoke-static {v0}, Lsu;->b(Lsu;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lsu$b;->b:Ljava/lang/String;

    iget-object v2, p0, Lsu$b;->c:Ljava/lang/String;

    invoke-direct {p1, v0, v1, v2}, Lhn1;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 1
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lsu$b;->a([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 1
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lsu$b;->b(Ljava/lang/Void;)V

    return-void
.end method

.method public onPreExecute()V
    .locals 3

    .line 1
    new-instance v0, Landroid/app/ProgressDialog;

    iget-object v1, p0, Lsu$b;->e:Lsu;

    invoke-static {v1}, Lsu;->b(Lsu;)Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lsu$b;->d:Landroid/app/ProgressDialog;

    iget-object v1, p0, Lsu$b;->e:Lsu;

    invoke-static {v1}, Lsu;->b(Lsu;)Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f120183

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lsu$b;->d:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    return-void
.end method

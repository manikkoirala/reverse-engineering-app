.class public Lgr;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Lpq;

.field public final b:Ljava/util/concurrent/Executor;

.field public final c:Ljava/util/concurrent/ScheduledExecutorService;

.field public volatile d:Ljava/util/concurrent/ScheduledFuture;

.field public volatile e:J


# direct methods
.method public constructor <init>(Lpq;Ljava/util/concurrent/Executor;Ljava/util/concurrent/ScheduledExecutorService;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/gms/common/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lpq;

    iput-object p1, p0, Lgr;->a:Lpq;

    iput-object p2, p0, Lgr;->b:Ljava/util/concurrent/Executor;

    iput-object p3, p0, Lgr;->c:Ljava/util/concurrent/ScheduledExecutorService;

    const-wide/16 p1, -0x1

    iput-wide p1, p0, Lgr;->e:J

    return-void
.end method

.method public static synthetic a(Lgr;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lgr;->f()V

    return-void
.end method

.method public static synthetic b(Lgr;Ljava/lang/Exception;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lgr;->e(Ljava/lang/Exception;)V

    return-void
.end method

.method private synthetic e(Ljava/lang/Exception;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lgr;->h()V

    return-void
.end method


# virtual methods
.method public c()V
    .locals 2

    .line 1
    iget-object v0, p0, Lgr;->d:Ljava/util/concurrent/ScheduledFuture;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgr;->d:Ljava/util/concurrent/ScheduledFuture;

    invoke-interface {v0}, Ljava/util/concurrent/Future;->isDone()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lgr;->d:Ljava/util/concurrent/ScheduledFuture;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/concurrent/Future;->cancel(Z)Z

    :cond_0
    return-void
.end method

.method public final d()J
    .locals 6

    .line 1
    iget-wide v0, p0, Lgr;->e:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const-wide/16 v0, 0x1e

    return-wide v0

    :cond_0
    iget-wide v0, p0, Lgr;->e:J

    const-wide/16 v2, 0x2

    mul-long/2addr v0, v2

    const-wide/16 v4, 0x3c0

    cmp-long v0, v0, v4

    if-gez v0, :cond_1

    iget-wide v0, p0, Lgr;->e:J

    mul-long/2addr v0, v2

    return-wide v0

    :cond_1
    return-wide v4
.end method

.method public final f()V
    .locals 3

    .line 1
    iget-object v0, p0, Lgr;->a:Lpq;

    invoke-virtual {v0}, Lpq;->e()Lcom/google/android/gms/tasks/Task;

    move-result-object v0

    iget-object v1, p0, Lgr;->b:Ljava/util/concurrent/Executor;

    new-instance v2, Lfr;

    invoke-direct {v2, p0}, Lfr;-><init>(Lgr;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/tasks/Task;->addOnFailureListener(Ljava/util/concurrent/Executor;Lcom/google/android/gms/tasks/OnFailureListener;)Lcom/google/android/gms/tasks/Task;

    return-void
.end method

.method public g(J)V
    .locals 4

    .line 1
    invoke-virtual {p0}, Lgr;->c()V

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lgr;->e:J

    iget-object v0, p0, Lgr;->c:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v1, Ler;

    invoke-direct {v1, p0}, Ler;-><init>(Lgr;)V

    const-wide/16 v2, 0x0

    invoke-static {v2, v3, p1, p2}, Ljava/lang/Math;->max(JJ)J

    move-result-wide p1

    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v1, p1, p2, v2}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object p1

    iput-object p1, p0, Lgr;->d:Ljava/util/concurrent/ScheduledFuture;

    return-void
.end method

.method public final h()V
    .locals 5

    .line 1
    invoke-virtual {p0}, Lgr;->c()V

    invoke-virtual {p0}, Lgr;->d()J

    move-result-wide v0

    iput-wide v0, p0, Lgr;->e:J

    iget-object v0, p0, Lgr;->c:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v1, Ler;

    invoke-direct {v1, p0}, Ler;-><init>(Lgr;)V

    iget-wide v2, p0, Lgr;->e:J

    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v1, v2, v3, v4}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    move-result-object v0

    iput-object v0, p0, Lgr;->d:Ljava/util/concurrent/ScheduledFuture;

    return-void
.end method

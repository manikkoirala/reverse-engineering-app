.class public final Lu00;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Ls00;

.field public final b:Loy1;


# direct methods
.method public constructor <init>(Ls00;Loy1;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lu00;->a:Ls00;

    iput-object p2, p0, Lu00;->b:Loy1;

    return-void
.end method


# virtual methods
.method public a()Ls00;
    .locals 1

    .line 1
    iget-object v0, p0, Lu00;->a:Ls00;

    return-object v0
.end method

.method public b()Loy1;
    .locals 1

    .line 1
    iget-object v0, p0, Lu00;->b:Loy1;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .line 1
    if-ne p0, p1, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 v0, 0x0

    if-eqz p1, :cond_3

    const-class v1, Lu00;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    if-eq v1, v2, :cond_1

    goto :goto_0

    :cond_1
    check-cast p1, Lu00;

    iget-object v1, p0, Lu00;->a:Ls00;

    iget-object v2, p1, Lu00;->a:Ls00;

    invoke-virtual {v1, v2}, Ljb;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    return v0

    :cond_2
    iget-object v0, p0, Lu00;->b:Loy1;

    iget-object p1, p1, Lu00;->b:Loy1;

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result p1

    return p1

    :cond_3
    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 2

    .line 1
    iget-object v0, p0, Lu00;->a:Ls00;

    invoke-virtual {v0}, Ljb;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lu00;->b:Loy1;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

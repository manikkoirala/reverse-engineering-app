.class public Lf61;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:Lee1;

.field public b:Lzg;

.field public c:Lcom/ekodroid/omrevaluator/serializable/ResultData;

.field public d:Landroid/content/Context;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/ekodroid/omrevaluator/serializable/ResultData;Lee1;Ljava/lang/String;Lzg;)V
    .locals 2

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "https://"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, La91;->w()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ":8759/excelDownload3"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lf61;->e:Ljava/lang/String;

    iput-object p1, p0, Lf61;->d:Landroid/content/Context;

    iput-object p3, p0, Lf61;->a:Lee1;

    iput-object p5, p0, Lf61;->b:Lzg;

    iput-object p2, p0, Lf61;->c:Lcom/ekodroid/omrevaluator/serializable/ResultData;

    iput-object p4, p0, Lf61;->f:Ljava/lang/String;

    invoke-virtual {p0}, Lf61;->g()V

    return-void
.end method

.method public static synthetic a(Lf61;Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lf61;->e(Ljava/lang/String;)V

    return-void
.end method

.method public static synthetic b(Lf61;ZILjava/lang/Object;)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1, p2, p3}, Lf61;->f(ZILjava/lang/Object;)V

    return-void
.end method

.method public static synthetic c(Lf61;)Lcom/ekodroid/omrevaluator/serializable/ResultData;
    .locals 0

    .line 1
    iget-object p0, p0, Lf61;->c:Lcom/ekodroid/omrevaluator/serializable/ResultData;

    return-object p0
.end method


# virtual methods
.method public d([B)V
    .locals 6

    .line 1
    const/4 v0, 0x0

    if-eqz p1, :cond_2

    iget-object v1, p0, Lf61;->d:Landroid/content/Context;

    sget-object v2, Lok;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/content/Context;->getExternalFilesDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lf61;->c:Lcom/ekodroid/omrevaluator/serializable/ResultData;

    invoke-virtual {v3}, Lcom/ekodroid/omrevaluator/serializable/ResultData;->getSubjectName()Ljava/lang/String;

    move-result-object v3

    const-string v4, "[^a-zA-Z0-9_-]"

    const-string v5, "_"

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, ".xlsx"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    :try_start_0
    new-instance v1, Ljava/io/FileOutputStream;

    invoke-direct {v1, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-virtual {v1, p1}, Ljava/io/FileOutputStream;->write([B)V

    invoke-virtual {v1}, Ljava/io/OutputStream;->flush()V

    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object p1

    const/4 v1, 0x1

    const/16 v2, 0xc8

    invoke-virtual {p0, v1, v2, p1}, Lf61;->f(ZILjava/lang/Object;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    return-void

    :catch_0
    move-exception p1

    goto :goto_0

    :catchall_0
    move-exception p1

    goto :goto_1

    :catch_1
    move-exception p1

    move-object v1, v0

    :goto_0
    :try_start_3
    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    if-eqz v1, :cond_2

    :try_start_4
    invoke-virtual {v1}, Ljava/io/OutputStream;->flush()V

    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_3

    :catch_2
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_3

    :catchall_1
    move-exception p1

    move-object v0, v1

    :goto_1
    if-eqz v0, :cond_1

    :try_start_5
    invoke-virtual {v0}, Ljava/io/OutputStream;->flush()V

    invoke-virtual {v0}, Ljava/io/FileOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    goto :goto_2

    :catch_3
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    :cond_1
    :goto_2
    throw p1

    :cond_2
    :goto_3
    const/4 p1, 0x0

    const/16 v1, 0x190

    invoke-virtual {p0, p1, v1, v0}, Lf61;->f(ZILjava/lang/Object;)V

    return-void
.end method

.method public final e(Ljava/lang/String;)V
    .locals 7

    .line 1
    new-instance v6, Lf61$c;

    const/4 v2, 0x1

    iget-object v3, p0, Lf61;->e:Ljava/lang/String;

    new-instance v4, Lf61$b;

    invoke-direct {v4, p0}, Lf61$b;-><init>(Lf61;)V

    move-object v0, v6

    move-object v1, p0

    move-object v5, p1

    invoke-direct/range {v0 .. v5}, Lf61$c;-><init>(Lf61;ILjava/lang/String;Lcom/android/volley/d$a;Ljava/lang/String;)V

    new-instance p1, Lwq;

    const/4 v0, 0x0

    const/high16 v1, 0x3f800000    # 1.0f

    const v2, 0x186a0

    invoke-direct {p1, v2, v0, v1}, Lwq;-><init>(IIF)V

    invoke-virtual {v6, p1}, Lcom/android/volley/Request;->L(Ljf1;)Lcom/android/volley/Request;

    iget-object p1, p0, Lf61;->a:Lee1;

    invoke-virtual {p1, v6}, Lee1;->a(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    return-void
.end method

.method public final f(ZILjava/lang/Object;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lf61;->b:Lzg;

    if-eqz v0, :cond_0

    invoke-interface {v0, p1, p2, p3}, Lzg;->a(ZILjava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public final g()V
    .locals 2

    .line 1
    invoke-static {}, Lcom/google/firebase/auth/FirebaseAuth;->getInstance()Lcom/google/firebase/auth/FirebaseAuth;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/firebase/auth/FirebaseAuth;->e()Lr30;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lr30;->i(Z)Lcom/google/android/gms/tasks/Task;

    move-result-object v0

    new-instance v1, Lf61$a;

    invoke-direct {v1, p0}, Lf61$a;-><init>(Lf61;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/tasks/Task;->addOnCompleteListener(Lcom/google/android/gms/tasks/OnCompleteListener;)Lcom/google/android/gms/tasks/Task;

    return-void
.end method

.method public h()V
    .locals 1

    .line 1
    const/4 v0, 0x0

    iput-object v0, p0, Lf61;->b:Lzg;

    return-void
.end method

.class public Luy1;
.super Lqy1;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Luy1$b;
    }
.end annotation


# instance fields
.field public a:Ljava/util/ArrayList;

.field public b:Z

.field public c:I

.field public d:Z

.field public e:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Lqy1;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Luy1;->a:Ljava/util/ArrayList;

    const/4 v0, 0x1

    iput-boolean v0, p0, Luy1;->b:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Luy1;->d:Z

    iput v0, p0, Luy1;->e:I

    return-void
.end method


# virtual methods
.method public final A(Lqy1;)V
    .locals 1

    .line 1
    iget-object v0, p0, Luy1;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iput-object p0, p1, Lqy1;->mParent:Luy1;

    return-void
.end method

.method public B(I)Lqy1;
    .locals 1

    .line 1
    if-ltz p1, :cond_1

    iget-object v0, p0, Luy1;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt p1, v0, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Luy1;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lqy1;

    return-object p1

    :cond_1
    :goto_0
    const/4 p1, 0x0

    return-object p1
.end method

.method public C()I
    .locals 1

    .line 1
    iget-object v0, p0, Luy1;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public D(Lqy1$g;)Luy1;
    .locals 0

    .line 1
    invoke-super {p0, p1}, Lqy1;->removeListener(Lqy1$g;)Lqy1;

    move-result-object p1

    check-cast p1, Luy1;

    return-object p1
.end method

.method public E(I)Luy1;
    .locals 2

    .line 1
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Luy1;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Luy1;->a:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lqy1;

    invoke-virtual {v1, p1}, Lqy1;->removeTarget(I)Lqy1;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-super {p0, p1}, Lqy1;->removeTarget(I)Lqy1;

    move-result-object p1

    check-cast p1, Luy1;

    return-object p1
.end method

.method public F(Landroid/view/View;)Luy1;
    .locals 2

    .line 1
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Luy1;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Luy1;->a:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lqy1;

    invoke-virtual {v1, p1}, Lqy1;->removeTarget(Landroid/view/View;)Lqy1;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-super {p0, p1}, Lqy1;->removeTarget(Landroid/view/View;)Lqy1;

    move-result-object p1

    check-cast p1, Luy1;

    return-object p1
.end method

.method public G(Ljava/lang/Class;)Luy1;
    .locals 2

    .line 1
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Luy1;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Luy1;->a:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lqy1;

    invoke-virtual {v1, p1}, Lqy1;->removeTarget(Ljava/lang/Class;)Lqy1;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-super {p0, p1}, Lqy1;->removeTarget(Ljava/lang/Class;)Lqy1;

    move-result-object p1

    check-cast p1, Luy1;

    return-object p1
.end method

.method public H(Ljava/lang/String;)Luy1;
    .locals 2

    .line 1
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Luy1;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Luy1;->a:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lqy1;

    invoke-virtual {v1, p1}, Lqy1;->removeTarget(Ljava/lang/String;)Lqy1;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-super {p0, p1}, Lqy1;->removeTarget(Ljava/lang/String;)Lqy1;

    move-result-object p1

    check-cast p1, Luy1;

    return-object p1
.end method

.method public I(Lqy1;)Luy1;
    .locals 1

    .line 1
    iget-object v0, p0, Luy1;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    const/4 v0, 0x0

    iput-object v0, p1, Lqy1;->mParent:Luy1;

    return-object p0
.end method

.method public J(J)Luy1;
    .locals 4

    .line 1
    invoke-super {p0, p1, p2}, Lqy1;->setDuration(J)Lqy1;

    iget-wide v0, p0, Lqy1;->mDuration:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    iget-object v0, p0, Luy1;->a:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    iget-object v2, p0, Luy1;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lqy1;

    invoke-virtual {v2, p1, p2}, Lqy1;->setDuration(J)Lqy1;

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-object p0
.end method

.method public K(Landroid/animation/TimeInterpolator;)Luy1;
    .locals 3

    .line 1
    iget v0, p0, Luy1;->e:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Luy1;->e:I

    iget-object v0, p0, Luy1;->a:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    iget-object v2, p0, Luy1;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lqy1;

    invoke-virtual {v2, p1}, Lqy1;->setInterpolator(Landroid/animation/TimeInterpolator;)Lqy1;

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    invoke-super {p0, p1}, Lqy1;->setInterpolator(Landroid/animation/TimeInterpolator;)Lqy1;

    move-result-object p1

    check-cast p1, Luy1;

    return-object p1
.end method

.method public L(I)Luy1;
    .locals 3

    .line 1
    const/4 v0, 0x1

    if-eqz p1, :cond_1

    if-ne p1, v0, :cond_0

    const/4 p1, 0x0

    iput-boolean p1, p0, Luy1;->b:Z

    goto :goto_0

    :cond_0
    new-instance v0, Landroid/util/AndroidRuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid parameter for TransitionSet ordering: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Landroid/util/AndroidRuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iput-boolean v0, p0, Luy1;->b:Z

    :goto_0
    return-object p0
.end method

.method public M(Landroid/view/ViewGroup;)Luy1;
    .locals 3

    .line 1
    invoke-super {p0, p1}, Lqy1;->setSceneRoot(Landroid/view/ViewGroup;)Lqy1;

    iget-object v0, p0, Luy1;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    iget-object v2, p0, Luy1;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lqy1;

    invoke-virtual {v2, p1}, Lqy1;->setSceneRoot(Landroid/view/ViewGroup;)Lqy1;

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-object p0
.end method

.method public N(J)Luy1;
    .locals 0

    .line 1
    invoke-super {p0, p1, p2}, Lqy1;->setStartDelay(J)Lqy1;

    move-result-object p1

    check-cast p1, Luy1;

    return-object p1
.end method

.method public final O()V
    .locals 3

    .line 1
    new-instance v0, Luy1$b;

    invoke-direct {v0, p0}, Luy1$b;-><init>(Luy1;)V

    iget-object v1, p0, Luy1;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lqy1;

    invoke-virtual {v2, v0}, Lqy1;->addListener(Lqy1$g;)Lqy1;

    goto :goto_0

    :cond_0
    iget-object v0, p0, Luy1;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iput v0, p0, Luy1;->c:I

    return-void
.end method

.method public bridge synthetic addListener(Lqy1$g;)Lqy1;
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Luy1;->u(Lqy1$g;)Luy1;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic addTarget(I)Lqy1;
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Luy1;->v(I)Luy1;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic addTarget(Landroid/view/View;)Lqy1;
    .locals 0

    .line 2
    invoke-virtual {p0, p1}, Luy1;->w(Landroid/view/View;)Luy1;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic addTarget(Ljava/lang/Class;)Lqy1;
    .locals 0

    .line 3
    invoke-virtual {p0, p1}, Luy1;->x(Ljava/lang/Class;)Luy1;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic addTarget(Ljava/lang/String;)Lqy1;
    .locals 0

    .line 4
    invoke-virtual {p0, p1}, Luy1;->y(Ljava/lang/String;)Luy1;

    move-result-object p1

    return-object p1
.end method

.method public cancel()V
    .locals 3

    .line 1
    invoke-super {p0}, Lqy1;->cancel()V

    iget-object v0, p0, Luy1;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    iget-object v2, p0, Luy1;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lqy1;

    invoke-virtual {v2}, Lqy1;->cancel()V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public captureEndValues(Lxy1;)V
    .locals 3

    .line 1
    iget-object v0, p1, Lxy1;->b:Landroid/view/View;

    invoke-virtual {p0, v0}, Lqy1;->isValidTarget(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Luy1;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lqy1;

    iget-object v2, p1, Lxy1;->b:Landroid/view/View;

    invoke-virtual {v1, v2}, Lqy1;->isValidTarget(Landroid/view/View;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1, p1}, Lqy1;->captureEndValues(Lxy1;)V

    iget-object v2, p1, Lxy1;->c:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-void
.end method

.method public capturePropagationValues(Lxy1;)V
    .locals 3

    .line 1
    invoke-super {p0, p1}, Lqy1;->capturePropagationValues(Lxy1;)V

    iget-object v0, p0, Luy1;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    iget-object v2, p0, Luy1;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lqy1;

    invoke-virtual {v2, p1}, Lqy1;->capturePropagationValues(Lxy1;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public captureStartValues(Lxy1;)V
    .locals 3

    .line 1
    iget-object v0, p1, Lxy1;->b:Landroid/view/View;

    invoke-virtual {p0, v0}, Lqy1;->isValidTarget(Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Luy1;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lqy1;

    iget-object v2, p1, Lxy1;->b:Landroid/view/View;

    invoke-virtual {v1, v2}, Lqy1;->isValidTarget(Landroid/view/View;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1, p1}, Lqy1;->captureStartValues(Lxy1;)V

    iget-object v2, p1, Lxy1;->c:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-void
.end method

.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1

    .line 2
    invoke-virtual {p0}, Luy1;->clone()Lqy1;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lqy1;
    .locals 4

    .line 1
    invoke-super {p0}, Lqy1;->clone()Lqy1;

    move-result-object v0

    check-cast v0, Luy1;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, Luy1;->a:Ljava/util/ArrayList;

    iget-object v1, p0, Luy1;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    iget-object v3, p0, Luy1;->a:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lqy1;

    invoke-virtual {v3}, Lqy1;->clone()Lqy1;

    move-result-object v3

    invoke-virtual {v0, v3}, Luy1;->A(Lqy1;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method public createAnimators(Landroid/view/ViewGroup;Lyy1;Lyy1;Ljava/util/ArrayList;Ljava/util/ArrayList;)V
    .locals 12

    .line 1
    move-object v0, p0

    invoke-virtual {p0}, Lqy1;->getStartDelay()J

    move-result-wide v1

    iget-object v3, v0, Luy1;->a:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v3, :cond_3

    iget-object v5, v0, Luy1;->a:Ljava/util/ArrayList;

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    move-object v6, v5

    check-cast v6, Lqy1;

    const-wide/16 v7, 0x0

    cmp-long v5, v1, v7

    if-lez v5, :cond_2

    iget-boolean v5, v0, Luy1;->b:Z

    if-nez v5, :cond_0

    if-nez v4, :cond_2

    :cond_0
    invoke-virtual {v6}, Lqy1;->getStartDelay()J

    move-result-wide v9

    cmp-long v5, v9, v7

    if-lez v5, :cond_1

    add-long/2addr v9, v1

    invoke-virtual {v6, v9, v10}, Lqy1;->setStartDelay(J)Lqy1;

    goto :goto_1

    :cond_1
    invoke-virtual {v6, v1, v2}, Lqy1;->setStartDelay(J)Lqy1;

    :cond_2
    :goto_1
    move-object v7, p1

    move-object v8, p2

    move-object v9, p3

    move-object/from16 v10, p4

    move-object/from16 v11, p5

    invoke-virtual/range {v6 .. v11}, Lqy1;->createAnimators(Landroid/view/ViewGroup;Lyy1;Lyy1;Ljava/util/ArrayList;Ljava/util/ArrayList;)V

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_3
    return-void
.end method

.method public excludeTarget(IZ)Lqy1;
    .locals 2

    .line 1
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Luy1;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Luy1;->a:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lqy1;

    invoke-virtual {v1, p1, p2}, Lqy1;->excludeTarget(IZ)Lqy1;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-super {p0, p1, p2}, Lqy1;->excludeTarget(IZ)Lqy1;

    move-result-object p1

    return-object p1
.end method

.method public excludeTarget(Landroid/view/View;Z)Lqy1;
    .locals 2

    .line 2
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Luy1;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Luy1;->a:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lqy1;

    invoke-virtual {v1, p1, p2}, Lqy1;->excludeTarget(Landroid/view/View;Z)Lqy1;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-super {p0, p1, p2}, Lqy1;->excludeTarget(Landroid/view/View;Z)Lqy1;

    move-result-object p1

    return-object p1
.end method

.method public excludeTarget(Ljava/lang/Class;Z)Lqy1;
    .locals 2

    .line 3
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Luy1;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Luy1;->a:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lqy1;

    invoke-virtual {v1, p1, p2}, Lqy1;->excludeTarget(Ljava/lang/Class;Z)Lqy1;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-super {p0, p1, p2}, Lqy1;->excludeTarget(Ljava/lang/Class;Z)Lqy1;

    move-result-object p1

    return-object p1
.end method

.method public excludeTarget(Ljava/lang/String;Z)Lqy1;
    .locals 2

    .line 4
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Luy1;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Luy1;->a:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lqy1;

    invoke-virtual {v1, p1, p2}, Lqy1;->excludeTarget(Ljava/lang/String;Z)Lqy1;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-super {p0, p1, p2}, Lqy1;->excludeTarget(Ljava/lang/String;Z)Lqy1;

    move-result-object p1

    return-object p1
.end method

.method public forceToEnd(Landroid/view/ViewGroup;)V
    .locals 3

    .line 1
    invoke-super {p0, p1}, Lqy1;->forceToEnd(Landroid/view/ViewGroup;)V

    iget-object v0, p0, Luy1;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    iget-object v2, p0, Luy1;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lqy1;

    invoke-virtual {v2, p1}, Lqy1;->forceToEnd(Landroid/view/ViewGroup;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public pause(Landroid/view/View;)V
    .locals 3

    .line 1
    invoke-super {p0, p1}, Lqy1;->pause(Landroid/view/View;)V

    iget-object v0, p0, Luy1;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    iget-object v2, p0, Luy1;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lqy1;

    invoke-virtual {v2, p1}, Lqy1;->pause(Landroid/view/View;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public bridge synthetic removeListener(Lqy1$g;)Lqy1;
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Luy1;->D(Lqy1$g;)Luy1;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic removeTarget(I)Lqy1;
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Luy1;->E(I)Luy1;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic removeTarget(Landroid/view/View;)Lqy1;
    .locals 0

    .line 2
    invoke-virtual {p0, p1}, Luy1;->F(Landroid/view/View;)Luy1;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic removeTarget(Ljava/lang/Class;)Lqy1;
    .locals 0

    .line 3
    invoke-virtual {p0, p1}, Luy1;->G(Ljava/lang/Class;)Luy1;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic removeTarget(Ljava/lang/String;)Lqy1;
    .locals 0

    .line 4
    invoke-virtual {p0, p1}, Luy1;->H(Ljava/lang/String;)Luy1;

    move-result-object p1

    return-object p1
.end method

.method public resume(Landroid/view/View;)V
    .locals 3

    .line 1
    invoke-super {p0, p1}, Lqy1;->resume(Landroid/view/View;)V

    iget-object v0, p0, Luy1;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    iget-object v2, p0, Luy1;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lqy1;

    invoke-virtual {v2, p1}, Lqy1;->resume(Landroid/view/View;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public runAnimators()V
    .locals 4

    .line 1
    iget-object v0, p0, Luy1;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lqy1;->start()V

    invoke-virtual {p0}, Lqy1;->end()V

    return-void

    :cond_0
    invoke-virtual {p0}, Luy1;->O()V

    iget-boolean v0, p0, Luy1;->b:Z

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    iget-object v1, p0, Luy1;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Luy1;->a:Ljava/util/ArrayList;

    add-int/lit8 v2, v0, -0x1

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lqy1;

    iget-object v2, p0, Luy1;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lqy1;

    new-instance v3, Luy1$a;

    invoke-direct {v3, p0, v2}, Luy1$a;-><init>(Luy1;Lqy1;)V

    invoke-virtual {v1, v3}, Lqy1;->addListener(Lqy1$g;)Lqy1;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Luy1;->a:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lqy1;

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lqy1;->runAnimators()V

    goto :goto_2

    :cond_2
    iget-object v0, p0, Luy1;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lqy1;

    invoke-virtual {v1}, Lqy1;->runAnimators()V

    goto :goto_1

    :cond_3
    :goto_2
    return-void
.end method

.method public setCanRemoveViews(Z)V
    .locals 3

    .line 1
    invoke-super {p0, p1}, Lqy1;->setCanRemoveViews(Z)V

    iget-object v0, p0, Luy1;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    iget-object v2, p0, Luy1;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lqy1;

    invoke-virtual {v2, p1}, Lqy1;->setCanRemoveViews(Z)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public bridge synthetic setDuration(J)Lqy1;
    .locals 0

    .line 1
    invoke-virtual {p0, p1, p2}, Luy1;->J(J)Luy1;

    move-result-object p1

    return-object p1
.end method

.method public setEpicenterCallback(Lqy1$f;)V
    .locals 3

    .line 1
    invoke-super {p0, p1}, Lqy1;->setEpicenterCallback(Lqy1$f;)V

    iget v0, p0, Luy1;->e:I

    or-int/lit8 v0, v0, 0x8

    iput v0, p0, Luy1;->e:I

    iget-object v0, p0, Luy1;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    iget-object v2, p0, Luy1;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lqy1;

    invoke-virtual {v2, p1}, Lqy1;->setEpicenterCallback(Lqy1$f;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public bridge synthetic setInterpolator(Landroid/animation/TimeInterpolator;)Lqy1;
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Luy1;->K(Landroid/animation/TimeInterpolator;)Luy1;

    move-result-object p1

    return-object p1
.end method

.method public setPathMotion(Lg31;)V
    .locals 2

    .line 1
    invoke-super {p0, p1}, Lqy1;->setPathMotion(Lg31;)V

    iget v0, p0, Luy1;->e:I

    or-int/lit8 v0, v0, 0x4

    iput v0, p0, Luy1;->e:I

    iget-object v0, p0, Luy1;->a:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Luy1;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Luy1;->a:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lqy1;

    invoke-virtual {v1, p1}, Lqy1;->setPathMotion(Lg31;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public setPropagation(Lty1;)V
    .locals 3

    .line 1
    invoke-super {p0, p1}, Lqy1;->setPropagation(Lty1;)V

    iget v0, p0, Luy1;->e:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p0, Luy1;->e:I

    iget-object v0, p0, Luy1;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    iget-object v2, p0, Luy1;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lqy1;

    invoke-virtual {v2, p1}, Lqy1;->setPropagation(Lty1;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public bridge synthetic setSceneRoot(Landroid/view/ViewGroup;)Lqy1;
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Luy1;->M(Landroid/view/ViewGroup;)Luy1;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic setStartDelay(J)Lqy1;
    .locals 0

    .line 1
    invoke-virtual {p0, p1, p2}, Luy1;->N(J)Luy1;

    move-result-object p1

    return-object p1
.end method

.method public toString(Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    .line 1
    invoke-super {p0, p1}, Lqy1;->toString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    :goto_0
    iget-object v2, p0, Luy1;->a:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v1, v2, :cond_0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "\n"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Luy1;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lqy1;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "  "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lqy1;->toString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method public u(Lqy1$g;)Luy1;
    .locals 0

    .line 1
    invoke-super {p0, p1}, Lqy1;->addListener(Lqy1$g;)Lqy1;

    move-result-object p1

    check-cast p1, Luy1;

    return-object p1
.end method

.method public v(I)Luy1;
    .locals 2

    .line 1
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Luy1;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Luy1;->a:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lqy1;

    invoke-virtual {v1, p1}, Lqy1;->addTarget(I)Lqy1;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-super {p0, p1}, Lqy1;->addTarget(I)Lqy1;

    move-result-object p1

    check-cast p1, Luy1;

    return-object p1
.end method

.method public w(Landroid/view/View;)Luy1;
    .locals 2

    .line 1
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Luy1;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Luy1;->a:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lqy1;

    invoke-virtual {v1, p1}, Lqy1;->addTarget(Landroid/view/View;)Lqy1;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-super {p0, p1}, Lqy1;->addTarget(Landroid/view/View;)Lqy1;

    move-result-object p1

    check-cast p1, Luy1;

    return-object p1
.end method

.method public x(Ljava/lang/Class;)Luy1;
    .locals 2

    .line 1
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Luy1;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Luy1;->a:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lqy1;

    invoke-virtual {v1, p1}, Lqy1;->addTarget(Ljava/lang/Class;)Lqy1;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-super {p0, p1}, Lqy1;->addTarget(Ljava/lang/Class;)Lqy1;

    move-result-object p1

    check-cast p1, Luy1;

    return-object p1
.end method

.method public y(Ljava/lang/String;)Luy1;
    .locals 2

    .line 1
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Luy1;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Luy1;->a:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lqy1;

    invoke-virtual {v1, p1}, Lqy1;->addTarget(Ljava/lang/String;)Lqy1;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-super {p0, p1}, Lqy1;->addTarget(Ljava/lang/String;)Lqy1;

    move-result-object p1

    check-cast p1, Luy1;

    return-object p1
.end method

.method public z(Lqy1;)Luy1;
    .locals 4

    .line 1
    invoke-virtual {p0, p1}, Luy1;->A(Lqy1;)V

    iget-wide v0, p0, Lqy1;->mDuration:J

    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-ltz v2, :cond_0

    invoke-virtual {p1, v0, v1}, Lqy1;->setDuration(J)Lqy1;

    :cond_0
    iget v0, p0, Luy1;->e:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lqy1;->getInterpolator()Landroid/animation/TimeInterpolator;

    move-result-object v0

    invoke-virtual {p1, v0}, Lqy1;->setInterpolator(Landroid/animation/TimeInterpolator;)Lqy1;

    :cond_1
    iget v0, p0, Luy1;->e:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lqy1;->getPropagation()Lty1;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lqy1;->setPropagation(Lty1;)V

    :cond_2
    iget v0, p0, Luy1;->e:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lqy1;->getPathMotion()Lg31;

    move-result-object v0

    invoke-virtual {p1, v0}, Lqy1;->setPathMotion(Lg31;)V

    :cond_3
    iget v0, p0, Luy1;->e:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_4

    invoke-virtual {p0}, Lqy1;->getEpicenterCallback()Lqy1$f;

    move-result-object v0

    invoke-virtual {p1, v0}, Lqy1;->setEpicenterCallback(Lqy1$f;)V

    :cond_4
    return-object p0
.end method

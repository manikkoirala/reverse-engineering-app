.class public Lve1$a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lve1;->v(IILcom/ekodroid/omrevaluator/templateui/models/Section2;Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;)I
    .locals 2

    .line 1
    invoke-virtual {p2}, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->getMarksForAnswer()D

    move-result-wide v0

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->getMarksForAnswer()D

    move-result-wide p1

    invoke-static {v0, v1, p1, p2}, Ljava/lang/Double;->compare(DD)I

    move-result p1

    return p1
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 0

    .line 1
    check-cast p1, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;

    check-cast p2, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;

    invoke-virtual {p0, p1, p2}, Lve1$a;->a(Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;)I

    move-result p1

    return p1
.end method

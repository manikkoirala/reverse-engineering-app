.class public Lk10$c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lk10;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Lk10;


# direct methods
.method public constructor <init>(Lk10;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lk10$c;->a:Lk10;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .line 1
    iget-object p1, p0, Lk10$c;->a:Lk10;

    invoke-static {p1}, Lk10;->a(Lk10;)Lj10;

    move-result-object p1

    if-eqz p1, :cond_2

    iget-object p1, p0, Lk10$c;->a:Lk10;

    invoke-static {p1}, Lk10;->a(Lk10;)Lj10;

    move-result-object p1

    new-instance v0, Lzx;

    iget-object v1, p0, Lk10$c;->a:Lk10;

    iget-object v1, v1, Lk10;->e:Landroid/widget/RadioButton;

    invoke-virtual {v1}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/ekodroid/omrevaluator/util/DataModels/SortArchiveExamList$SortBy;->NAME:Lcom/ekodroid/omrevaluator/util/DataModels/SortArchiveExamList$SortBy;

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lk10$c;->a:Lk10;

    iget-object v1, v1, Lk10;->d:Landroid/widget/RadioButton;

    invoke-virtual {v1}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_1

    sget-object v1, Lcom/ekodroid/omrevaluator/util/DataModels/SortArchiveExamList$SortBy;->DATE:Lcom/ekodroid/omrevaluator/util/DataModels/SortArchiveExamList$SortBy;

    goto :goto_0

    :cond_1
    sget-object v1, Lcom/ekodroid/omrevaluator/util/DataModels/SortArchiveExamList$SortBy;->CLASS:Lcom/ekodroid/omrevaluator/util/DataModels/SortArchiveExamList$SortBy;

    :goto_0
    iget-object v2, p0, Lk10$c;->a:Lk10;

    iget-object v2, v2, Lk10;->f:Landroid/widget/RadioButton;

    invoke-virtual {v2}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v2

    invoke-direct {v0, v1, v2}, Lzx;-><init>(Lcom/ekodroid/omrevaluator/util/DataModels/SortArchiveExamList$SortBy;Z)V

    invoke-interface {p1, v0}, Lj10;->a(Ljava/lang/Object;)V

    :cond_2
    iget-object p1, p0, Lk10$c;->a:Lk10;

    invoke-virtual {p1}, Landroid/app/Dialog;->dismiss()V

    return-void
.end method

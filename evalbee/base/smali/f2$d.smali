.class public Lf2$d;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ly01;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lf2;->r(Ljava/util/ArrayList;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Ljava/util/ArrayList;

.field public final synthetic b:Lf2;


# direct methods
.method public constructor <init>(Lf2;Ljava/util/ArrayList;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lf2$d;->b:Lf2;

    iput-object p2, p0, Lf2$d;->a:Ljava/util/ArrayList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Object;)V
    .locals 7

    .line 1
    iget-object p1, p0, Lf2$d;->b:Lf2;

    iget-object p1, p1, Lf2;->a:Landroid/content/Context;

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;

    move-result-object p1

    invoke-static {}, Lcom/google/firebase/auth/FirebaseAuth;->getInstance()Lcom/google/firebase/auth/FirebaseAuth;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/firebase/auth/FirebaseAuth;->e()Lr30;

    move-result-object v0

    invoke-virtual {v0}, Lr30;->O()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    :goto_0
    iget-object v2, p0, Lf2$d;->a:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v1, v2, :cond_2

    iget-object v2, p0, Lf2$d;->b:Lf2;

    iget-object v2, v2, Lf2;->a:Landroid/content/Context;

    invoke-static {v2}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;

    move-result-object v2

    iget-object v3, p0, Lf2$d;->a:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v2, v3}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getTemplateJson(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;->getOrgId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-virtual {v3}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->isCloudSyncOn()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v3}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->getUserId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    iget-object v2, p0, Lf2$d;->b:Lf2;

    iget-object v2, v2, Lf2;->a:Landroid/content/Context;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, " exam is not owned by you"

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f0800bd

    const v5, 0x7f08016e

    invoke-static {v2, v3, v4, v5}, La91;->H(Landroid/content/Context;Ljava/lang/String;II)V

    goto :goto_1

    :cond_0
    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->setArchiving(Z)V

    invoke-virtual {v3}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->getArchiveId()Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->setArchiveId(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v2, v3}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->saveOrUpdateTemplateJson(Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;)Z

    iget-object v2, p0, Lf2$d;->b:Lf2;

    iget-object v2, v2, Lf2;->a:Landroid/content/Context;

    invoke-static {v2}, Lcom/ekodroid/omrevaluator/activities/Services/ArchiveSyncService;->n(Landroid/content/Context;)V

    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0

    :cond_2
    iget-object p1, p0, Lf2$d;->b:Lf2;

    invoke-static {p1}, Lf2;->k(Lf2;)V

    return-void
.end method

.class public abstract Ly3;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Ly3$c;
    }
.end annotation


# static fields
.field public static a:Lcom/google/android/gms/ads/rewarded/RewardedAd; = null

.field public static b:Ljava/lang/String; = "ca-app-pub-8698273815250571/8966728877"


# direct methods
.method public static constructor <clinit>()V
    .locals 0

    .line 1
    return-void
.end method

.method public static synthetic a()Lcom/google/android/gms/ads/rewarded/RewardedAd;
    .locals 1

    .line 1
    sget-object v0, Ly3;->a:Lcom/google/android/gms/ads/rewarded/RewardedAd;

    return-object v0
.end method

.method public static synthetic b(Lcom/google/android/gms/ads/rewarded/RewardedAd;)Lcom/google/android/gms/ads/rewarded/RewardedAd;
    .locals 0

    .line 1
    sput-object p0, Ly3;->a:Lcom/google/android/gms/ads/rewarded/RewardedAd;

    return-object p0
.end method

.method public static c(Landroid/content/Context;)V
    .locals 1

    .line 1
    new-instance v0, Ly3$a;

    invoke-direct {v0}, Ly3$a;-><init>()V

    invoke-static {p0, v0}, Lcom/google/android/gms/ads/MobileAds;->initialize(Landroid/content/Context;Lcom/google/android/gms/ads/initialization/OnInitializationCompleteListener;)V

    return-void
.end method

.method public static d(Landroid/app/Activity;Lcom/google/android/gms/ads/OnUserEarnedRewardListener;Ly3$c;)V
    .locals 4

    .line 1
    new-instance v0, Lcom/google/android/gms/ads/AdRequest$Builder;

    invoke-direct {v0}, Lcom/google/android/gms/ads/AdRequest$Builder;-><init>()V

    invoke-virtual {v0}, Lcom/google/android/gms/ads/AdRequest$Builder;->build()Lcom/google/android/gms/ads/AdRequest;

    move-result-object v0

    new-instance v1, Landroid/app/ProgressDialog;

    invoke-direct {v1, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    const-string v2, "Please wait, loading video..."

    invoke-virtual {v1, v2}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    invoke-virtual {v1}, Landroid/app/Dialog;->show()V

    sget-object v2, Ly3;->b:Ljava/lang/String;

    new-instance v3, Ly3$b;

    invoke-direct {v3, v1, p2, p0, p1}, Ly3$b;-><init>(Landroid/app/ProgressDialog;Ly3$c;Landroid/app/Activity;Lcom/google/android/gms/ads/OnUserEarnedRewardListener;)V

    invoke-static {p0, v2, v0, v3}, Lcom/google/android/gms/ads/rewarded/RewardedAd;->load(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/ads/AdRequest;Lcom/google/android/gms/ads/rewarded/RewardedAdLoadCallback;)V

    return-void
.end method

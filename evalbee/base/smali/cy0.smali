.class public Lcy0;
.super Lwa;
.source "SourceFile"


# static fields
.field public static p:[D

.field public static q:[D


# instance fields
.field public n:Lz22;

.field public o:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .line 1
    const/4 v0, 0x0

    new-array v1, v0, [D

    sput-object v1, Lcy0;->p:[D

    new-array v0, v0, [D

    sput-object v0, Lcy0;->q:[D

    return-void
.end method

.method public constructor <init>(Lzl;Lub0;)V
    .locals 1

    .line 1
    invoke-direct {p0, p1, p2}, Lwa;-><init>(Lzl;Lub0;)V

    new-instance p1, Lz22;

    const/4 p2, 0x4

    new-array v0, p2, [D

    fill-array-data v0, :array_0

    invoke-direct {p1, v0, p2}, Lz22;-><init>([DI)V

    iput-object p1, p0, Lcy0;->n:Lz22;

    const/4 p1, 0x1

    iput-boolean p1, p0, Lcy0;->o:Z

    return-void

    :array_0
    .array-data 8
        0x3ff0000000000000L    # 1.0
        0x3ff0000000000000L    # 1.0
        0x3ff0000000000000L    # 1.0
        0x3ff0000000000000L    # 1.0
    .end array-data
.end method


# virtual methods
.method public c([D)V
    .locals 20

    .line 1
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    array-length v2, v1

    add-int/lit8 v2, v2, -0x1

    aget-wide v3, v1, v2

    iget-object v5, v0, Lmo;->b:Lub0;

    invoke-virtual {v5}, Lub0;->a()I

    move-result v5

    const-wide/16 v6, 0x0

    const/4 v8, 0x0

    move-wide v10, v6

    move v9, v8

    :goto_0
    if-ge v9, v5, :cond_0

    sget-object v12, Lcy0;->p:[D

    invoke-virtual {v0, v3, v4, v9}, Lwa;->e(DI)D

    move-result-wide v13

    sget-object v15, Lcy0;->q:[D

    aget-wide v16, v15, v9

    mul-double v13, v13, v16

    aput-wide v13, v12, v9

    sget-object v12, Lcy0;->p:[D

    aget-wide v13, v12, v9

    add-double/2addr v10, v13

    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    :cond_0
    cmpl-double v3, v10, v6

    if-nez v3, :cond_1

    const-wide/high16 v10, 0x3ff0000000000000L    # 1.0

    :cond_1
    move v3, v8

    :goto_1
    if-ge v3, v2, :cond_3

    iget-object v4, v0, Lmo;->b:Lub0;

    invoke-virtual {v4, v8, v8}, Lub0;->e(II)V

    move-wide v12, v6

    move v4, v8

    :goto_2
    if-ge v4, v5, :cond_2

    sget-object v9, Lcy0;->p:[D

    aget-wide v14, v9, v4

    iget-object v9, v0, Lmo;->a:Lzl;

    iget-object v6, v0, Lmo;->b:Lub0;

    invoke-virtual {v6}, Lub0;->c()I

    move-result v6

    invoke-virtual {v9, v6}, Lzl;->b(I)Lr51;

    move-result-object v6

    invoke-interface {v6}, Lr51;->getLocation()[D

    move-result-object v6

    aget-wide v18, v6, v3

    mul-double v14, v14, v18

    add-double/2addr v12, v14

    add-int/lit8 v4, v4, 0x1

    const-wide/16 v6, 0x0

    goto :goto_2

    :cond_2
    div-double/2addr v12, v10

    aput-wide v12, v1, v3

    add-int/lit8 v3, v3, 0x1

    const-wide/16 v6, 0x0

    goto :goto_1

    :cond_3
    return-void
.end method

.method public f(Lpx0;)V
    .locals 7

    .line 1
    iget-object v0, p0, Lmo;->b:Lub0;

    iget-object v1, p0, Lmo;->a:Lzl;

    invoke-virtual {v1}, Lzl;->c()I

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Lub0;->b(II)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lmo;->b:Lub0;

    invoke-virtual {v0}, Lub0;->a()I

    move-result v0

    sget-object v1, Lcy0;->p:[D

    array-length v1, v1

    if-ge v1, v0, :cond_0

    mul-int/lit8 v1, v0, 0x2

    new-array v3, v1, [D

    sput-object v3, Lcy0;->p:[D

    new-array v1, v1, [D

    sput-object v1, Lcy0;->q:[D

    :cond_0
    iget-boolean v1, p0, Lcy0;->o:Z

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcy0;->n:Lz22;

    invoke-virtual {v1}, Lz22;->f()I

    move-result v1

    if-ne v1, v0, :cond_2

    :goto_0
    if-ge v2, v0, :cond_4

    sget-object v1, Lcy0;->q:[D

    iget-object v3, p0, Lcy0;->n:Lz22;

    invoke-virtual {v3, v2}, Lz22;->c(I)D

    move-result-wide v3

    aput-wide v3, v1, v2

    sget-object v1, Lcy0;->q:[D

    aget-wide v3, v1, v2

    const-wide/16 v5, 0x0

    cmpg-double v1, v3, v5

    if-ltz v1, :cond_1

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Negative weight not allowed"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_2
    new-instance p1, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "weightVector.size("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcy0;->n:Lz22;

    invoke-virtual {v2}, Lz22;->f()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, ") != group iterator size("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, ")"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_3
    :goto_1
    if-ge v2, v0, :cond_4

    sget-object v1, Lcy0;->q:[D

    const-wide/high16 v3, 0x3ff0000000000000L    # 1.0

    aput-wide v3, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_4
    invoke-super {p0, p1}, Lwa;->f(Lpx0;)V

    return-void

    :cond_5
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Group iterator not in range"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public j(Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lcy0;->o:Z

    return-void
.end method

.method public k(Lz22;)V
    .locals 1

    .line 1
    if-eqz p1, :cond_0

    iput-object p1, p0, Lcy0;->n:Lz22;

    return-void

    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Weight-vector cannot be null."

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

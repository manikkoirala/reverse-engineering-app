.class public Lxc1;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/firebase/database/collection/c;

.field public b:Lcom/google/firebase/database/collection/c;


# direct methods
.method public constructor <init>()V
    .locals 3

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/google/firebase/database/collection/c;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    sget-object v2, Lku;->c:Ljava/util/Comparator;

    invoke-direct {v0, v1, v2}, Lcom/google/firebase/database/collection/c;-><init>(Ljava/util/List;Ljava/util/Comparator;)V

    iput-object v0, p0, Lxc1;->a:Lcom/google/firebase/database/collection/c;

    new-instance v0, Lcom/google/firebase/database/collection/c;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    sget-object v2, Lku;->d:Ljava/util/Comparator;

    invoke-direct {v0, v1, v2}, Lcom/google/firebase/database/collection/c;-><init>(Ljava/util/List;Ljava/util/Comparator;)V

    iput-object v0, p0, Lxc1;->b:Lcom/google/firebase/database/collection/c;

    return-void
.end method


# virtual methods
.method public a(Ldu;I)V
    .locals 1

    .line 1
    new-instance v0, Lku;

    invoke-direct {v0, p1, p2}, Lku;-><init>(Ldu;I)V

    iget-object p1, p0, Lxc1;->a:Lcom/google/firebase/database/collection/c;

    invoke-virtual {p1, v0}, Lcom/google/firebase/database/collection/c;->c(Ljava/lang/Object;)Lcom/google/firebase/database/collection/c;

    move-result-object p1

    iput-object p1, p0, Lxc1;->a:Lcom/google/firebase/database/collection/c;

    iget-object p1, p0, Lxc1;->b:Lcom/google/firebase/database/collection/c;

    invoke-virtual {p1, v0}, Lcom/google/firebase/database/collection/c;->c(Ljava/lang/Object;)Lcom/google/firebase/database/collection/c;

    move-result-object p1

    iput-object p1, p0, Lxc1;->b:Lcom/google/firebase/database/collection/c;

    return-void
.end method

.method public b(Lcom/google/firebase/database/collection/c;I)V
    .locals 1

    .line 1
    invoke-virtual {p1}, Lcom/google/firebase/database/collection/c;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldu;

    invoke-virtual {p0, v0, p2}, Lxc1;->a(Ldu;I)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public c(Ldu;)Z
    .locals 3

    .line 1
    new-instance v0, Lku;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lku;-><init>(Ldu;I)V

    iget-object v2, p0, Lxc1;->a:Lcom/google/firebase/database/collection/c;

    invoke-virtual {v2, v0}, Lcom/google/firebase/database/collection/c;->g(Ljava/lang/Object;)Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    return v1

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lku;

    invoke-virtual {v0}, Lku;->d()Ldu;

    move-result-object v0

    invoke-virtual {v0, p1}, Ldu;->equals(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public d(I)Lcom/google/firebase/database/collection/c;
    .locals 4

    .line 1
    invoke-static {}, Ldu;->d()Ldu;

    move-result-object v0

    new-instance v1, Lku;

    invoke-direct {v1, v0, p1}, Lku;-><init>(Ldu;I)V

    iget-object v0, p0, Lxc1;->b:Lcom/google/firebase/database/collection/c;

    invoke-virtual {v0, v1}, Lcom/google/firebase/database/collection/c;->g(Ljava/lang/Object;)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {}, Ldu;->e()Lcom/google/firebase/database/collection/c;

    move-result-object v1

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lku;

    invoke-virtual {v2}, Lku;->c()I

    move-result v3

    if-ne v3, p1, :cond_0

    invoke-virtual {v2}, Lku;->d()Ldu;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/firebase/database/collection/c;->c(Ljava/lang/Object;)Lcom/google/firebase/database/collection/c;

    move-result-object v1

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method public e(Ldu;I)V
    .locals 1

    .line 1
    new-instance v0, Lku;

    invoke-direct {v0, p1, p2}, Lku;-><init>(Ldu;I)V

    invoke-virtual {p0, v0}, Lxc1;->f(Lku;)V

    return-void
.end method

.method public final f(Lku;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lxc1;->a:Lcom/google/firebase/database/collection/c;

    invoke-virtual {v0, p1}, Lcom/google/firebase/database/collection/c;->i(Ljava/lang/Object;)Lcom/google/firebase/database/collection/c;

    move-result-object v0

    iput-object v0, p0, Lxc1;->a:Lcom/google/firebase/database/collection/c;

    iget-object v0, p0, Lxc1;->b:Lcom/google/firebase/database/collection/c;

    invoke-virtual {v0, p1}, Lcom/google/firebase/database/collection/c;->i(Ljava/lang/Object;)Lcom/google/firebase/database/collection/c;

    move-result-object p1

    iput-object p1, p0, Lxc1;->b:Lcom/google/firebase/database/collection/c;

    return-void
.end method

.method public g(Lcom/google/firebase/database/collection/c;I)V
    .locals 1

    .line 1
    invoke-virtual {p1}, Lcom/google/firebase/database/collection/c;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldu;

    invoke-virtual {p0, v0, p2}, Lxc1;->e(Ldu;I)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public h(I)Lcom/google/firebase/database/collection/c;
    .locals 4

    .line 1
    invoke-static {}, Ldu;->d()Ldu;

    move-result-object v0

    new-instance v1, Lku;

    invoke-direct {v1, v0, p1}, Lku;-><init>(Ldu;I)V

    iget-object v0, p0, Lxc1;->b:Lcom/google/firebase/database/collection/c;

    invoke-virtual {v0, v1}, Lcom/google/firebase/database/collection/c;->g(Ljava/lang/Object;)Ljava/util/Iterator;

    move-result-object v0

    invoke-static {}, Ldu;->e()Lcom/google/firebase/database/collection/c;

    move-result-object v1

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lku;

    invoke-virtual {v2}, Lku;->c()I

    move-result v3

    if-ne v3, p1, :cond_0

    invoke-virtual {v2}, Lku;->d()Ldu;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/google/firebase/database/collection/c;->c(Ljava/lang/Object;)Lcom/google/firebase/database/collection/c;

    move-result-object v1

    invoke-virtual {p0, v2}, Lxc1;->f(Lku;)V

    goto :goto_0

    :cond_0
    return-object v1
.end method

.class public abstract Ln92$a;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ln92;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "a"
.end annotation


# instance fields
.field public final a:Ljava/lang/Class;

.field public b:Z

.field public c:Ljava/util/UUID;

.field public d:Lp92;

.field public final e:Ljava/util/Set;


# direct methods
.method public constructor <init>(Ljava/lang/Class;)V
    .locals 4

    .line 1
    const-string v0, "workerClass"

    invoke-static {p1, v0}, Lfg0;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Ln92$a;->a:Ljava/lang/Class;

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    const-string v1, "randomUUID()"

    invoke-static {v0, v1}, Lfg0;->d(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object v0, p0, Ln92$a;->c:Ljava/util/UUID;

    new-instance v0, Lp92;

    iget-object v1, p0, Ln92$a;->c:Ljava/util/UUID;

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "id.toString()"

    invoke-static {v1, v2}, Lfg0;->d(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    const-string v3, "workerClass.name"

    invoke-static {v2, v3}, Lfg0;->d(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {v0, v1, v2}, Lp92;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Ln92$a;->d:Lp92;

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1, v3}, Lfg0;->d(Ljava/lang/Object;Ljava/lang/String;)V

    filled-new-array {p1}, [Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Ltm1;->f([Ljava/lang/Object;)Ljava/util/Set;

    move-result-object p1

    iput-object p1, p0, Ln92$a;->e:Ljava/util/Set;

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ln92$a;
    .locals 1

    .line 1
    const-string v0, "tag"

    invoke-static {p1, v0}, Lfg0;->e(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Ln92$a;->e:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0}, Ln92$a;->g()Ln92$a;

    move-result-object p1

    return-object p1
.end method

.method public final b()Ln92;
    .locals 7

    .line 1
    invoke-virtual {p0}, Ln92$a;->c()Ln92;

    move-result-object v0

    iget-object v1, p0, Ln92$a;->d:Lp92;

    iget-object v1, v1, Lp92;->j:Lzk;

    invoke-virtual {v1}, Lzk;->e()Z

    move-result v2

    const/4 v3, 0x0

    const/4 v4, 0x1

    if-nez v2, :cond_1

    invoke-virtual {v1}, Lzk;->f()Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {v1}, Lzk;->g()Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {v1}, Lzk;->h()Z

    move-result v1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    move v1, v3

    goto :goto_1

    :cond_1
    :goto_0
    move v1, v4

    :goto_1
    iget-object v2, p0, Ln92$a;->d:Lp92;

    iget-boolean v5, v2, Lp92;->q:Z

    if-eqz v5, :cond_5

    xor-int/2addr v1, v4

    if-eqz v1, :cond_4

    iget-wide v1, v2, Lp92;->g:J

    const-wide/16 v5, 0x0

    cmp-long v1, v1, v5

    if-gtz v1, :cond_2

    move v3, v4

    :cond_2
    if-eqz v3, :cond_3

    goto :goto_2

    :cond_3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Expedited jobs cannot be delayed"

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Expedited jobs only support network and storage constraints"

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_5
    :goto_2
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v1

    const-string v2, "randomUUID()"

    invoke-static {v1, v2}, Lfg0;->d(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p0, v1}, Ln92$a;->j(Ljava/util/UUID;)Ln92$a;

    return-object v0
.end method

.method public abstract c()Ln92;
.end method

.method public final d()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Ln92$a;->b:Z

    return v0
.end method

.method public final e()Ljava/util/UUID;
    .locals 1

    .line 1
    iget-object v0, p0, Ln92$a;->c:Ljava/util/UUID;

    return-object v0
.end method

.method public final f()Ljava/util/Set;
    .locals 1

    .line 1
    iget-object v0, p0, Ln92$a;->e:Ljava/util/Set;

    return-object v0
.end method

.method public abstract g()Ln92$a;
.end method

.method public final h()Lp92;
    .locals 1

    .line 1
    iget-object v0, p0, Ln92$a;->d:Lp92;

    return-object v0
.end method

.method public final i(Lzk;)Ln92$a;
    .locals 1

    .line 1
    const-string v0, "constraints"

    invoke-static {p1, v0}, Lfg0;->e(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Ln92$a;->d:Lp92;

    iput-object p1, v0, Lp92;->j:Lzk;

    invoke-virtual {p0}, Ln92$a;->g()Ln92$a;

    move-result-object p1

    return-object p1
.end method

.method public final j(Ljava/util/UUID;)Ln92$a;
    .locals 2

    .line 1
    const-string v0, "id"

    invoke-static {p1, v0}, Lfg0;->e(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Ln92$a;->c:Ljava/util/UUID;

    new-instance v0, Lp92;

    invoke-virtual {p1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v1, "id.toString()"

    invoke-static {p1, v1}, Lfg0;->d(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, p0, Ln92$a;->d:Lp92;

    invoke-direct {v0, p1, v1}, Lp92;-><init>(Ljava/lang/String;Lp92;)V

    iput-object v0, p0, Ln92$a;->d:Lp92;

    invoke-virtual {p0}, Ln92$a;->g()Ln92$a;

    move-result-object p1

    return-object p1
.end method

.method public final k(Landroidx/work/b;)Ln92$a;
    .locals 1

    .line 1
    const-string v0, "inputData"

    invoke-static {p1, v0}, Lfg0;->e(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Ln92$a;->d:Lp92;

    iput-object p1, v0, Lp92;->e:Landroidx/work/b;

    invoke-virtual {p0}, Ln92$a;->g()Ln92$a;

    move-result-object p1

    return-object p1
.end method

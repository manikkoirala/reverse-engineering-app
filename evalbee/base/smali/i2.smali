.class public Li2;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Li2$b;,
        Li2$a;
    }
.end annotation


# static fields
.field public static final c:Li2;


# instance fields
.field public final a:Ljava/util/Map;

.field public final b:Ljava/lang/Object;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .line 1
    new-instance v0, Li2;

    invoke-direct {v0}, Li2;-><init>()V

    sput-object v0, Li2;->c:Li2;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Li2;->a:Ljava/util/Map;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Li2;->b:Ljava/lang/Object;

    return-void
.end method

.method public static a()Li2;
    .locals 1

    .line 1
    sget-object v0, Li2;->c:Li2;

    return-object v0
.end method


# virtual methods
.method public b(Ljava/lang/Object;)V
    .locals 2

    .line 1
    iget-object v0, p0, Li2;->b:Ljava/lang/Object;

    monitor-enter v0

    :try_start_0
    iget-object v1, p0, Li2;->a:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Li2$a;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Li2$a;->a()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Li2$b;->b(Landroid/app/Activity;)Li2$b;

    move-result-object v1

    invoke-virtual {v1, p1}, Li2$b;->c(Li2$a;)V

    :cond_0
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method public c(Landroid/app/Activity;Ljava/lang/Object;Ljava/lang/Runnable;)V
    .locals 2

    .line 1
    iget-object v0, p0, Li2;->b:Ljava/lang/Object;

    monitor-enter v0

    :try_start_0
    new-instance v1, Li2$a;

    invoke-direct {v1, p1, p3, p2}, Li2$a;-><init>(Landroid/app/Activity;Ljava/lang/Runnable;Ljava/lang/Object;)V

    invoke-static {p1}, Li2$b;->b(Landroid/app/Activity;)Li2$b;

    move-result-object p1

    invoke-virtual {p1, v1}, Li2$b;->a(Li2$a;)V

    iget-object p1, p0, Li2;->a:Ljava/util/Map;

    invoke-interface {p1, p2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.class public final Lga$b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lw01;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lga;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "b"
.end annotation


# static fields
.field public static final a:Lga$b;

.field public static final b:Ln00;

.field public static final c:Ln00;

.field public static final d:Ln00;

.field public static final e:Ln00;

.field public static final f:Ln00;

.field public static final g:Ln00;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .line 1
    new-instance v0, Lga$b;

    invoke-direct {v0}, Lga$b;-><init>()V

    sput-object v0, Lga$b;->a:Lga$b;

    const-string v0, "appId"

    invoke-static {v0}, Ln00;->d(Ljava/lang/String;)Ln00;

    move-result-object v0

    sput-object v0, Lga$b;->b:Ln00;

    const-string v0, "deviceModel"

    invoke-static {v0}, Ln00;->d(Ljava/lang/String;)Ln00;

    move-result-object v0

    sput-object v0, Lga$b;->c:Ln00;

    const-string v0, "sessionSdkVersion"

    invoke-static {v0}, Ln00;->d(Ljava/lang/String;)Ln00;

    move-result-object v0

    sput-object v0, Lga$b;->d:Ln00;

    const-string v0, "osVersion"

    invoke-static {v0}, Ln00;->d(Ljava/lang/String;)Ln00;

    move-result-object v0

    sput-object v0, Lga$b;->e:Ln00;

    const-string v0, "logEnvironment"

    invoke-static {v0}, Ln00;->d(Ljava/lang/String;)Ln00;

    move-result-object v0

    sput-object v0, Lga$b;->f:Ln00;

    const-string v0, "androidAppInfo"

    invoke-static {v0}, Ln00;->d(Ljava/lang/String;)Ln00;

    move-result-object v0

    sput-object v0, Lga$b;->g:Ln00;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ly7;Lx01;)V
    .locals 2

    .line 1
    sget-object v0, Lga$b;->b:Ln00;

    invoke-virtual {p1}, Ly7;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2, v0, v1}, Lx01;->f(Ln00;Ljava/lang/Object;)Lx01;

    sget-object v0, Lga$b;->c:Ln00;

    invoke-virtual {p1}, Ly7;->c()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2, v0, v1}, Lx01;->f(Ln00;Ljava/lang/Object;)Lx01;

    sget-object v0, Lga$b;->d:Ln00;

    invoke-virtual {p1}, Ly7;->f()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2, v0, v1}, Lx01;->f(Ln00;Ljava/lang/Object;)Lx01;

    sget-object v0, Lga$b;->e:Ln00;

    invoke-virtual {p1}, Ly7;->e()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2, v0, v1}, Lx01;->f(Ln00;Ljava/lang/Object;)Lx01;

    sget-object v0, Lga$b;->f:Ln00;

    invoke-virtual {p1}, Ly7;->d()Lcom/google/firebase/sessions/LogEnvironment;

    move-result-object v1

    invoke-interface {p2, v0, v1}, Lx01;->f(Ln00;Ljava/lang/Object;)Lx01;

    sget-object v0, Lga$b;->g:Ln00;

    invoke-virtual {p1}, Ly7;->a()Lr4;

    move-result-object p1

    invoke-interface {p2, v0, p1}, Lx01;->f(Ln00;Ljava/lang/Object;)Lx01;

    return-void
.end method

.method public bridge synthetic encode(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .line 1
    check-cast p1, Ly7;

    check-cast p2, Lx01;

    invoke-virtual {p0, p1, p2}, Lga$b;->a(Ly7;Lx01;)V

    return-void
.end method

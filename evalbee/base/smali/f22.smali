.class public Lf22;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf22$a;
    }
.end annotation


# instance fields
.field public final a:Lbw0;

.field public final b:Lxm;

.field public c:Ljava/lang/String;

.field public final d:Lf22$a;

.field public final e:Lf22$a;

.field public final f:Lof1;

.field public final g:Ljava/util/concurrent/atomic/AtomicMarkableReference;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lz00;Lxm;)V
    .locals 3

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lf22$a;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lf22$a;-><init>(Lf22;Z)V

    iput-object v0, p0, Lf22;->d:Lf22$a;

    new-instance v0, Lf22$a;

    const/4 v2, 0x1

    invoke-direct {v0, p0, v2}, Lf22$a;-><init>(Lf22;Z)V

    iput-object v0, p0, Lf22;->e:Lf22$a;

    new-instance v0, Lof1;

    const/16 v2, 0x80

    invoke-direct {v0, v2}, Lof1;-><init>(I)V

    iput-object v0, p0, Lf22;->f:Lof1;

    new-instance v0, Ljava/util/concurrent/atomic/AtomicMarkableReference;

    const/4 v2, 0x0

    invoke-direct {v0, v2, v1}, Ljava/util/concurrent/atomic/AtomicMarkableReference;-><init>(Ljava/lang/Object;Z)V

    iput-object v0, p0, Lf22;->g:Ljava/util/concurrent/atomic/AtomicMarkableReference;

    iput-object p1, p0, Lf22;->c:Ljava/lang/String;

    new-instance p1, Lbw0;

    invoke-direct {p1, p2}, Lbw0;-><init>(Lz00;)V

    iput-object p1, p0, Lf22;->a:Lbw0;

    iput-object p3, p0, Lf22;->b:Lxm;

    return-void
.end method

.method public static synthetic a(Lf22;)Lxm;
    .locals 0

    .line 1
    iget-object p0, p0, Lf22;->b:Lxm;

    return-object p0
.end method

.method public static synthetic b(Lf22;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lf22;->c:Ljava/lang/String;

    return-object p0
.end method

.method public static synthetic c(Lf22;)Lbw0;
    .locals 0

    .line 1
    iget-object p0, p0, Lf22;->a:Lbw0;

    return-object p0
.end method

.method public static h(Ljava/lang/String;Lz00;Lxm;)Lf22;
    .locals 3

    .line 1
    new-instance v0, Lbw0;

    invoke-direct {v0, p1}, Lbw0;-><init>(Lz00;)V

    new-instance v1, Lf22;

    invoke-direct {v1, p0, p1, p2}, Lf22;-><init>(Ljava/lang/String;Lz00;Lxm;)V

    iget-object p1, v1, Lf22;->d:Lf22$a;

    iget-object p1, p1, Lf22$a;->a:Ljava/util/concurrent/atomic/AtomicMarkableReference;

    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicMarkableReference;->getReference()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Loi0;

    const/4 p2, 0x0

    invoke-virtual {v0, p0, p2}, Lbw0;->i(Ljava/lang/String;Z)Ljava/util/Map;

    move-result-object v2

    invoke-virtual {p1, v2}, Loi0;->e(Ljava/util/Map;)V

    iget-object p1, v1, Lf22;->e:Lf22$a;

    iget-object p1, p1, Lf22$a;->a:Ljava/util/concurrent/atomic/AtomicMarkableReference;

    invoke-virtual {p1}, Ljava/util/concurrent/atomic/AtomicMarkableReference;->getReference()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Loi0;

    const/4 v2, 0x1

    invoke-virtual {v0, p0, v2}, Lbw0;->i(Ljava/lang/String;Z)Ljava/util/Map;

    move-result-object v2

    invoke-virtual {p1, v2}, Loi0;->e(Ljava/util/Map;)V

    iget-object p1, v1, Lf22;->g:Ljava/util/concurrent/atomic/AtomicMarkableReference;

    invoke-virtual {v0, p0}, Lbw0;->k(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2, p2}, Ljava/util/concurrent/atomic/AtomicMarkableReference;->set(Ljava/lang/Object;Z)V

    iget-object p1, v1, Lf22;->f:Lof1;

    invoke-virtual {v0, p0}, Lbw0;->j(Ljava/lang/String;)Ljava/util/List;

    move-result-object p0

    invoke-virtual {p1, p0}, Lof1;->c(Ljava/util/List;)Z

    return-object v1
.end method

.method public static i(Ljava/lang/String;Lz00;)Ljava/lang/String;
    .locals 1

    .line 1
    new-instance v0, Lbw0;

    invoke-direct {v0, p1}, Lbw0;-><init>(Lz00;)V

    invoke-virtual {v0, p0}, Lbw0;->k(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public d()Ljava/util/Map;
    .locals 1

    .line 1
    iget-object v0, p0, Lf22;->d:Lf22$a;

    invoke-virtual {v0}, Lf22$a;->b()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public e()Ljava/util/Map;
    .locals 1

    .line 1
    iget-object v0, p0, Lf22;->e:Lf22$a;

    invoke-virtual {v0}, Lf22$a;->b()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public f()Ljava/util/List;
    .locals 1

    .line 1
    iget-object v0, p0, Lf22;->f:Lof1;

    invoke-virtual {v0}, Lof1;->a()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public g()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lf22;->g:Ljava/util/concurrent/atomic/AtomicMarkableReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicMarkableReference;->getReference()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public j(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1

    .line 1
    iget-object v0, p0, Lf22;->e:Lf22$a;

    invoke-virtual {v0, p1, p2}, Lf22$a;->f(Ljava/lang/String;Ljava/lang/String;)Z

    move-result p1

    return p1
.end method

.method public k(Ljava/lang/String;)V
    .locals 5

    .line 1
    iget-object v0, p0, Lf22;->c:Ljava/lang/String;

    monitor-enter v0

    :try_start_0
    iput-object p1, p0, Lf22;->c:Ljava/lang/String;

    iget-object v1, p0, Lf22;->d:Lf22$a;

    invoke-virtual {v1}, Lf22$a;->b()Ljava/util/Map;

    move-result-object v1

    iget-object v2, p0, Lf22;->f:Lof1;

    invoke-virtual {v2}, Lof1;->b()Ljava/util/List;

    move-result-object v2

    invoke-virtual {p0}, Lf22;->g()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lf22;->a:Lbw0;

    invoke-virtual {p0}, Lf22;->g()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, p1, v4}, Lbw0;->s(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    invoke-interface {v1}, Ljava/util/Map;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1

    iget-object v3, p0, Lf22;->a:Lbw0;

    invoke-virtual {v3, p1, v1}, Lbw0;->p(Ljava/lang/String;Ljava/util/Map;)V

    :cond_1
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lf22;->a:Lbw0;

    invoke-virtual {v1, p1, v2}, Lbw0;->r(Ljava/lang/String;Ljava/util/List;)V

    :cond_2
    monitor-exit v0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

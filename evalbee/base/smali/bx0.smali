.class public Lbx0;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lbx0$f;
    }
.end annotation


# instance fields
.field public a:Ljava/lang/String;

.field public b:Landroid/content/Context;

.field public c:Landroid/widget/LinearLayout;

.field public d:Landroid/widget/LinearLayout;

.field public e:Landroid/widget/TextView;

.field public f:Landroid/widget/Button;

.field public g:Landroid/widget/Button;

.field public h:Landroidx/appcompat/app/a;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lbx0;->b:Landroid/content/Context;

    iput-object p2, p0, Lbx0;->a:Ljava/lang/String;

    invoke-virtual {p0}, Lbx0;->j()V

    return-void
.end method

.method public static synthetic a(Lbx0;)Landroidx/appcompat/app/a;
    .locals 0

    .line 1
    iget-object p0, p0, Lbx0;->h:Landroidx/appcompat/app/a;

    return-object p0
.end method

.method public static synthetic b(Lbx0;)Ljava/lang/String;
    .locals 0

    .line 1
    iget-object p0, p0, Lbx0;->a:Ljava/lang/String;

    return-object p0
.end method

.method public static synthetic c(Lbx0;)Landroid/content/Context;
    .locals 0

    .line 1
    iget-object p0, p0, Lbx0;->b:Landroid/content/Context;

    return-object p0
.end method

.method public static synthetic d(Lbx0;Ljava/lang/String;)Z
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lbx0;->f(Ljava/lang/String;)Z

    move-result p0

    return p0
.end method

.method public static synthetic e(Lbx0;Ljava/io/File;)Z
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lbx0;->g(Ljava/io/File;)Z

    move-result p0

    return p0
.end method


# virtual methods
.method public final f(Ljava/lang/String;)Z
    .locals 5

    .line 1
    invoke-static {}, Lo30;->f()Lo30;

    move-result-object v0

    invoke-static {}, Lcom/google/firebase/auth/FirebaseAuth;->getInstance()Lcom/google/firebase/auth/FirebaseAuth;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/firebase/auth/FirebaseAuth;->e()Lr30;

    move-result-object v1

    if-eqz v1, :cond_3

    if-nez v0, :cond_0

    goto/16 :goto_1

    :cond_0
    new-instance v2, Ljava/io/File;

    iget-object v3, p0, Lbx0;->b:Landroid/content/Context;

    sget-object v4, Lok;->c:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/content/Context;->getExternalFilesDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object v3

    sget-object v4, Lok;->d:Ljava/lang/String;

    invoke-direct {v2, v3, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_1

    invoke-virtual {v2}, Ljava/io/File;->mkdirs()Z

    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, ".exm"

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v2, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v0}, Lo30;->n()Ljq1;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lok;->d:Ljava/lang/String;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "/"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Lr30;->O()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljq1;->a(Ljava/lang/String;)Ljq1;

    move-result-object p1

    invoke-virtual {p1, v3}, Ljq1;->g(Ljava/io/File;)Lx00;

    move-result-object p1

    new-instance v0, Lzs1;

    invoke-direct {v0}, Lzs1;-><init>()V

    new-instance v1, Lbx0$e;

    invoke-direct {v1, p0}, Lbx0$e;-><init>(Lbx0;)V

    invoke-virtual {p1, v1}, Lzq1;->p(Lcom/google/android/gms/tasks/OnFailureListener;)Lzq1;

    move-result-object p1

    new-instance v1, Lbx0$d;

    invoke-direct {v1, p0, v0}, Lbx0$d;-><init>(Lbx0;Lzs1;)V

    invoke-virtual {p1, v1}, Lzq1;->u(Lcom/google/android/gms/tasks/OnSuccessListener;)Lzq1;

    move-result-object p1

    new-instance v1, Lbx0$c;

    invoke-direct {v1, p0, v0}, Lbx0$c;-><init>(Lbx0;Lzs1;)V

    invoke-virtual {p1, v1}, Lzq1;->s(Lo11;)Lzq1;

    :goto_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iget-wide v3, v0, Lzs1;->c:J

    sub-long/2addr v1, v3

    const-wide/32 v3, 0x927c0

    cmp-long p1, v1, v3

    if-gez p1, :cond_2

    iget-boolean p1, v0, Lzs1;->b:Z

    if-nez p1, :cond_2

    const-wide/16 v1, 0x1f4

    :try_start_0
    invoke-static {v1, v2}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0

    :cond_2
    iget-boolean p1, v0, Lzs1;->a:Z

    return p1

    :cond_3
    :goto_1
    const/4 p1, 0x0

    return p1
.end method

.method public final g(Ljava/io/File;)Z
    .locals 5

    .line 1
    const/4 v0, 0x0

    :try_start_0
    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-static {v1}, Lcom/google/android/gms/common/util/IOUtils;->toByteArray(Ljava/io/InputStream;)[B

    move-result-object p1

    invoke-static {p1}, Lyx;->a([B)Lcom/ekodroid/omrevaluator/serializable/ExmVersions/ExamDataV1;

    move-result-object p1

    if-nez p1, :cond_0

    return v0

    :cond_0
    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/serializable/ExmVersions/ExamDataV1;->getExamId()Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    move-result-object v1

    iget-object v2, p0, Lbx0;->b:Landroid/content/Context;

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/serializable/ExmVersions/ExamDataV1;->getSheetTemplate2()Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    move-result-object v3

    iget-object v4, p0, Lbx0;->a:Ljava/lang/String;

    invoke-static {v2, v1, v3, v4}, Lxu1;->d(Landroid/content/Context;Lcom/ekodroid/omrevaluator/templateui/models/ExamId;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    return v0

    :cond_1
    iget-object v2, p0, Lbx0;->b:Landroid/content/Context;

    invoke-static {v2, v1, p1}, Lxu1;->c(Landroid/content/Context;Lcom/ekodroid/omrevaluator/templateui/models/ExamId;Lcom/ekodroid/omrevaluator/serializable/ExmVersions/ExamDataV1;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const/4 p1, 0x1

    return p1

    :catch_0
    return v0
.end method

.method public final h()V
    .locals 2

    .line 1
    iget-object v0, p0, Lbx0;->g:Landroid/widget/Button;

    new-instance v1, Lbx0$b;

    invoke-direct {v1, p0}, Lbx0$b;-><init>(Lbx0;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final i()V
    .locals 2

    .line 1
    iget-object v0, p0, Lbx0;->f:Landroid/widget/Button;

    new-instance v1, Lbx0$a;

    invoke-direct {v1, p0}, Lbx0$a;-><init>(Lbx0;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final j()V
    .locals 4

    .line 1
    new-instance v0, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    iget-object v1, p0, Lbx0;->b:Landroid/content/Context;

    const v2, 0x7f130151

    invoke-direct {v0, v1, v2}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;-><init>(Landroid/content/Context;I)V

    iget-object v1, p0, Lbx0;->b:Landroid/content/Context;

    const-string v2, "layout_inflater"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    const v2, 0x7f0c0070

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->setView(Landroid/view/View;)Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    const v2, 0x7f120080

    invoke-virtual {v0, v2}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->setTitle(I)Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    const v2, 0x7f0901e5

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lbx0;->d:Landroid/widget/LinearLayout;

    const v2, 0x7f0901e4

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lbx0;->c:Landroid/widget/LinearLayout;

    const v2, 0x7f0900ba

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lbx0;->f:Landroid/widget/Button;

    const v2, 0x7f0900a9

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lbx0;->g:Landroid/widget/Button;

    const v2, 0x7f0903bf

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lbx0;->e:Landroid/widget/TextView;

    iget-object v1, p0, Lbx0;->c:Landroid/widget/LinearLayout;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lbx0;->e:Landroid/widget/TextView;

    const v2, 0x7f120182

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    invoke-virtual {p0}, Lbx0;->i()V

    invoke-virtual {p0}, Lbx0;->h()V

    invoke-virtual {v0}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->create()Landroidx/appcompat/app/a;

    move-result-object v0

    iput-object v0, p0, Lbx0;->h:Landroidx/appcompat/app/a;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    return-void
.end method

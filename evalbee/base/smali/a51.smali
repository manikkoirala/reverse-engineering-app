.class public final La51;
.super Lkotlinx/coroutines/CoroutineDispatcher;
.source "SourceFile"


# instance fields
.field public final c:Llt;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Lkotlinx/coroutines/CoroutineDispatcher;-><init>()V

    new-instance v0, Llt;

    invoke-direct {v0}, Llt;-><init>()V

    iput-object v0, p0, La51;->c:Llt;

    return-void
.end method


# virtual methods
.method public f(Lkotlin/coroutines/CoroutineContext;Ljava/lang/Runnable;)V
    .locals 1

    .line 1
    const-string v0, "context"

    invoke-static {p1, v0}, Lfg0;->e(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "block"

    invoke-static {p2, v0}, Lfg0;->e(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, La51;->c:Llt;

    invoke-virtual {v0, p1, p2}, Llt;->c(Lkotlin/coroutines/CoroutineContext;Ljava/lang/Runnable;)V

    return-void
.end method

.method public q0(Lkotlin/coroutines/CoroutineContext;)Z
    .locals 1

    .line 1
    const-string v0, "context"

    invoke-static {p1, v0}, Lfg0;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-static {}, Lpt;->c()Lpm0;

    move-result-object v0

    invoke-virtual {v0}, Lpm0;->s0()Lpm0;

    move-result-object v0

    invoke-virtual {v0, p1}, Lkotlinx/coroutines/CoroutineDispatcher;->q0(Lkotlin/coroutines/CoroutineContext;)Z

    move-result p1

    const/4 v0, 0x1

    if-eqz p1, :cond_0

    return v0

    :cond_0
    iget-object p1, p0, La51;->c:Llt;

    invoke-virtual {p1}, Llt;->b()Z

    move-result p1

    xor-int/2addr p1, v0

    return p1
.end method

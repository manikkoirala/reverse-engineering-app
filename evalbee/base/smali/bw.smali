.class public Lbw;
.super Lu80;
.source "SourceFile"


# instance fields
.field public a:Landroid/content/Context;

.field public b:Ly01;

.field public c:Landroid/content/SharedPreferences;

.field public d:Z

.field public e:Lcom/toptoche/searchablespinnerlibrary/SearchableSpinner;

.field public f:Landroid/widget/Button;

.field public g:Landroid/widget/EditText;

.field public h:Landroid/widget/EditText;

.field public i:Landroid/widget/EditText;

.field public j:Lcom/google/android/material/textfield/TextInputLayout;

.field public k:Lcom/google/android/material/textfield/TextInputLayout;

.field public l:Lcom/google/android/material/textfield/TextInputLayout;

.field public m:Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

.field public n:Lbw;


# direct methods
.method public constructor <init>(Landroid/content/Context;ZLy01;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lu80;-><init>(Landroid/content/Context;)V

    iput-object p0, p0, Lbw;->n:Lbw;

    iput-object p1, p0, Lbw;->a:Landroid/content/Context;

    iput-boolean p2, p0, Lbw;->d:Z

    iput-object p3, p0, Lbw;->b:Ly01;

    return-void
.end method

.method public static synthetic a(Lbw;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lbw;->f()V

    return-void
.end method

.method public static synthetic b(Lbw;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lbw;->g()V

    return-void
.end method

.method public static synthetic c(Lbw;Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lbw;->h(Ljava/lang/String;)V

    return-void
.end method

.method public static synthetic d(Lbw;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1, p2}, Lbw;->k(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final e(Ljava/util/ArrayList;Ljava/lang/String;)I
    .locals 3

    .line 1
    invoke-virtual {p2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object p2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    return v1

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return v0
.end method

.method public final f()V
    .locals 3

    .line 1
    iget-object v0, p0, Lbw;->m:Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    new-instance v0, Lpa0;

    iget-object v1, p0, Lbw;->a:Landroid/content/Context;

    new-instance v2, Lbw$c;

    invoke-direct {v2, p0}, Lbw$c;-><init>(Lbw;)V

    invoke-direct {v0, v1, v2}, Lpa0;-><init>(Landroid/content/Context;Lzg;)V

    return-void
.end method

.method public final g()V
    .locals 3

    .line 1
    iget-object v0, p0, Lbw;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lbw;->g:Landroid/widget/EditText;

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;->getDisplayName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lbw;->h:Landroid/widget/EditText;

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;->getOrganization()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;->getDisplayName()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;->getDisplayName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x1

    if-ge v0, v1, :cond_1

    :cond_0
    invoke-static {}, Lcom/google/firebase/auth/FirebaseAuth;->getInstance()Lcom/google/firebase/auth/FirebaseAuth;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/firebase/auth/FirebaseAuth;->e()Lr30;

    move-result-object v0

    iget-object v1, p0, Lbw;->h:Landroid/widget/EditText;

    invoke-virtual {v0}, Lr30;->getDisplayName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    return-void
.end method

.method public final h(Ljava/lang/String;)V
    .locals 2

    .line 1
    invoke-static {}, Lcom/google/firebase/auth/FirebaseAuth;->getInstance()Lcom/google/firebase/auth/FirebaseAuth;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/firebase/auth/FirebaseAuth;->e()Lr30;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v1, Lg22$a;

    invoke-direct {v1}, Lg22$a;-><init>()V

    invoke-virtual {v1, p1}, Lg22$a;->b(Ljava/lang/String;)Lg22$a;

    move-result-object p1

    invoke-virtual {p1}, Lg22$a;->a()Lg22;

    move-result-object p1

    invoke-virtual {v0, p1}, Lr30;->Z(Lg22;)Lcom/google/android/gms/tasks/Task;

    :cond_0
    return-void
.end method

.method public final i()V
    .locals 2

    .line 1
    iget-object v0, p0, Lbw;->f:Landroid/widget/Button;

    new-instance v1, Lbw$d;

    invoke-direct {v1, p0}, Lbw$d;-><init>(Lbw;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final j()V
    .locals 6

    .line 1
    invoke-static {}, Ljava/util/Locale;->getAvailableLocales()[Ljava/util/Locale;

    move-result-object v0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    array-length v2, v0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_1

    aget-object v4, v0, v3

    invoke-virtual {v4}, Ljava/util/Locale;->getDisplayCountry()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v5

    if-lez v5, :cond_0

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    sget-object v0, Ljava/lang/String;->CASE_INSENSITIVE_ORDER:Ljava/util/Comparator;

    invoke-static {v1, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    new-instance v0, Landroid/widget/ArrayAdapter;

    iget-object v2, p0, Lbw;->a:Landroid/content/Context;

    const v3, 0x7f0c00f0

    invoke-direct {v0, v2, v3, v1}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    iget-object v2, p0, Lbw;->e:Lcom/toptoche/searchablespinnerlibrary/SearchableSpinner;

    invoke-virtual {v2, v0}, Lcom/toptoche/searchablespinnerlibrary/SearchableSpinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    iget-object v0, p0, Lbw;->e:Lcom/toptoche/searchablespinnerlibrary/SearchableSpinner;

    iget-object v2, p0, Lbw;->c:Landroid/content/SharedPreferences;

    const-string v3, "user_country"

    const-string v4, "India"

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lbw;->e(Ljava/util/ArrayList;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/AdapterView;->setSelection(I)V

    iget-object v0, p0, Lbw;->e:Lcom/toptoche/searchablespinnerlibrary/SearchableSpinner;

    iget-object v1, p0, Lbw;->a:Landroid/content/Context;

    const v2, 0x7f1202be

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/toptoche/searchablespinnerlibrary/SearchableSpinner;->setTitle(Ljava/lang/String;)V

    return-void
.end method

.method public final k(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5

    .line 1
    new-instance v0, Landroid/app/ProgressDialog;

    iget-object v1, p0, Lbw;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    const-string v1, "Please wait, updating profile..."

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    new-instance v1, Ls61;

    new-instance v2, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserProfileDto;

    invoke-direct {v2, p1, p2}, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserProfileDto;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v3, p0, Lbw;->a:Landroid/content/Context;

    new-instance v4, Lbw$e;

    invoke-direct {v4, p0, v0, p1, p2}, Lbw$e;-><init>(Lbw;Landroid/app/ProgressDialog;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v1, v2, v3, v4}, Ls61;-><init>(Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserProfileDto;Landroid/content/Context;Lzg;)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    .line 1
    invoke-super {p0, p1}, Lu80;->onCreate(Landroid/os/Bundle;)V

    const p1, 0x7f0c0088

    invoke-virtual {p0, p1}, Landroid/app/Dialog;->setContentView(I)V

    const p1, 0x7f090436

    invoke-virtual {p0, p1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroidx/appcompat/widget/Toolbar;

    new-instance v0, Lbw$a;

    invoke-direct {v0, p0}, Lbw$a;-><init>(Lbw;)V

    invoke-virtual {p1, v0}, Landroidx/appcompat/widget/Toolbar;->setNavigationOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object p1, p0, Lbw;->a:Landroid/content/Context;

    const-string v0, "MyPref"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object p1

    iput-object p1, p0, Lbw;->c:Landroid/content/SharedPreferences;

    const p1, 0x7f090345

    invoke-virtual {p0, p1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/toptoche/searchablespinnerlibrary/SearchableSpinner;

    iput-object p1, p0, Lbw;->e:Lcom/toptoche/searchablespinnerlibrary/SearchableSpinner;

    const p1, 0x7f09039f

    invoke-virtual {p0, p1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/google/android/material/textfield/TextInputLayout;

    iput-object p1, p0, Lbw;->k:Lcom/google/android/material/textfield/TextInputLayout;

    const p1, 0x7f09039a

    invoke-virtual {p0, p1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/google/android/material/textfield/TextInputLayout;

    iput-object p1, p0, Lbw;->l:Lcom/google/android/material/textfield/TextInputLayout;

    const p1, 0x7f0903a0

    invoke-virtual {p0, p1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Lcom/google/android/material/textfield/TextInputLayout;

    iput-object p1, p0, Lbw;->j:Lcom/google/android/material/textfield/TextInputLayout;

    invoke-virtual {p1}, Lcom/google/android/material/textfield/TextInputLayout;->getEditText()Landroid/widget/EditText;

    move-result-object p1

    iput-object p1, p0, Lbw;->g:Landroid/widget/EditText;

    iget-object p1, p0, Lbw;->l:Lcom/google/android/material/textfield/TextInputLayout;

    invoke-virtual {p1}, Lcom/google/android/material/textfield/TextInputLayout;->getEditText()Landroid/widget/EditText;

    move-result-object p1

    iput-object p1, p0, Lbw;->h:Landroid/widget/EditText;

    iget-object p1, p0, Lbw;->k:Lcom/google/android/material/textfield/TextInputLayout;

    invoke-virtual {p1}, Lcom/google/android/material/textfield/TextInputLayout;->getEditText()Landroid/widget/EditText;

    move-result-object p1

    iput-object p1, p0, Lbw;->i:Landroid/widget/EditText;

    const p1, 0x7f0900d2

    invoke-virtual {p0, p1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/Button;

    iput-object p1, p0, Lbw;->f:Landroid/widget/Button;

    const p1, 0x7f09036e

    invoke-virtual {p0, p1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

    iput-object p1, p0, Lbw;->m:Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

    invoke-static {}, Lcom/google/firebase/auth/FirebaseAuth;->getInstance()Lcom/google/firebase/auth/FirebaseAuth;

    move-result-object p1

    invoke-virtual {p1}, Lcom/google/firebase/auth/FirebaseAuth;->e()Lr30;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lr30;->getDisplayName()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lbw;->g:Landroid/widget/EditText;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lbw;->g:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/TextView;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSelection(I)V

    iget-object v0, p0, Lbw;->i:Landroid/widget/EditText;

    invoke-virtual {p1}, Lr30;->getEmail()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    iget-object p1, p0, Lbw;->h:Landroid/widget/EditText;

    iget-object v0, p0, Lbw;->c:Landroid/content/SharedPreferences;

    const-string v1, "user_organization"

    const-string v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object p1, p0, Lbw;->m:Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

    new-instance v0, Lbw$b;

    invoke-direct {v0, p0}, Lbw$b;-><init>(Lbw;)V

    invoke-virtual {p1, v0}, Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;->setOnRefreshListener(Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout$j;)V

    invoke-virtual {p0}, Lbw;->j()V

    invoke-virtual {p0}, Lbw;->i()V

    invoke-virtual {p0}, Lbw;->f()V

    invoke-virtual {p0}, Lbw;->g()V

    return-void
.end method

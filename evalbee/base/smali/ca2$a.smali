.class public Lca2$a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lca2;->o()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Lik0;

.field public final synthetic b:Lca2;


# direct methods
.method public constructor <init>(Lca2;Lik0;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lca2$a;->b:Lca2;

    iput-object p2, p0, Lca2$a;->a:Lik0;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .line 1
    iget-object v0, p0, Lca2$a;->b:Lca2;

    iget-object v0, v0, Lca2;->t:Lum1;

    invoke-virtual {v0}, Landroidx/work/impl/utils/futures/AbstractFuture;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    :try_start_0
    iget-object v0, p0, Lca2$a;->a:Lik0;

    invoke-interface {v0}, Ljava/util/concurrent/Future;->get()Ljava/lang/Object;

    invoke-static {}, Lxl0;->e()Lxl0;

    move-result-object v0

    sget-object v1, Lca2;->w:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Starting work for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lca2$a;->b:Lca2;

    iget-object v3, v3, Lca2;->d:Lp92;

    iget-object v3, v3, Lp92;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lxl0;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lca2$a;->b:Lca2;

    iget-object v1, v0, Lca2;->t:Lum1;

    iget-object v0, v0, Lca2;->e:Landroidx/work/c;

    invoke-virtual {v0}, Landroidx/work/c;->startWork()Lik0;

    move-result-object v0

    invoke-virtual {v1, v0}, Lum1;->q(Lik0;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lca2$a;->b:Lca2;

    iget-object v1, v1, Lca2;->t:Lum1;

    invoke-virtual {v1, v0}, Lum1;->p(Ljava/lang/Throwable;)Z

    :goto_0
    return-void
.end method

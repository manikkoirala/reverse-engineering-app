.class public Lwe0$a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lhj1;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lwe0;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "a"
.end annotation


# instance fields
.field public a:Lcom/google/firebase/firestore/util/AsyncQueue$b;

.field public final b:Lcom/google/firebase/firestore/util/AsyncQueue;

.field public final synthetic c:Lwe0;


# direct methods
.method public constructor <init>(Lwe0;Lcom/google/firebase/firestore/util/AsyncQueue;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lwe0$a;->c:Lwe0;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lwe0$a;->b:Lcom/google/firebase/firestore/util/AsyncQueue;

    return-void
.end method

.method public static synthetic a(Lwe0$a;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lwe0$a;->b()V

    return-void
.end method

.method private synthetic b()V
    .locals 3

    .line 1
    iget-object v0, p0, Lwe0$a;->c:Lwe0;

    invoke-virtual {v0}, Lwe0;->d()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    filled-new-array {v0}, [Ljava/lang/Object;

    move-result-object v0

    const-string v1, "IndexBackfiller"

    const-string v2, "Documents written: %s"

    invoke-static {v1, v2, v0}, Lcom/google/firebase/firestore/util/Logger;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-static {}, Lwe0;->c()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lwe0$a;->c(J)V

    return-void
.end method


# virtual methods
.method public final c(J)V
    .locals 3

    .line 1
    iget-object v0, p0, Lwe0$a;->b:Lcom/google/firebase/firestore/util/AsyncQueue;

    sget-object v1, Lcom/google/firebase/firestore/util/AsyncQueue$TimerId;->INDEX_BACKFILL:Lcom/google/firebase/firestore/util/AsyncQueue$TimerId;

    new-instance v2, Lve0;

    invoke-direct {v2, p0}, Lve0;-><init>(Lwe0$a;)V

    invoke-virtual {v0, v1, p1, p2, v2}, Lcom/google/firebase/firestore/util/AsyncQueue;->h(Lcom/google/firebase/firestore/util/AsyncQueue$TimerId;JLjava/lang/Runnable;)Lcom/google/firebase/firestore/util/AsyncQueue$b;

    move-result-object p1

    iput-object p1, p0, Lwe0$a;->a:Lcom/google/firebase/firestore/util/AsyncQueue$b;

    return-void
.end method

.method public start()V
    .locals 2

    .line 1
    invoke-static {}, Lwe0;->b()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lwe0$a;->c(J)V

    return-void
.end method

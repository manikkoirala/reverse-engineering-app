.class public Lo91;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lo91$a;
    }
.end annotation


# instance fields
.field public final a:Ljava/util/Map;

.field public final b:Ljava/util/Map;

.field public final c:Lw01;


# direct methods
.method public constructor <init>(Ljava/util/Map;Ljava/util/Map;Lw01;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lo91;->a:Ljava/util/Map;

    iput-object p2, p0, Lo91;->b:Ljava/util/Map;

    iput-object p3, p0, Lo91;->c:Lw01;

    return-void
.end method

.method public static a()Lo91$a;
    .locals 1

    .line 1
    new-instance v0, Lo91$a;

    invoke-direct {v0}, Lo91$a;-><init>()V

    return-object v0
.end method


# virtual methods
.method public b(Ljava/lang/Object;Ljava/io/OutputStream;)V
    .locals 4

    .line 1
    new-instance v0, Lcom/google/firebase/encoders/proto/b;

    iget-object v1, p0, Lo91;->a:Ljava/util/Map;

    iget-object v2, p0, Lo91;->b:Ljava/util/Map;

    iget-object v3, p0, Lo91;->c:Lw01;

    invoke-direct {v0, p2, v1, v2, v3}, Lcom/google/firebase/encoders/proto/b;-><init>(Ljava/io/OutputStream;Ljava/util/Map;Ljava/util/Map;Lw01;)V

    invoke-virtual {v0, p1}, Lcom/google/firebase/encoders/proto/b;->t(Ljava/lang/Object;)Lcom/google/firebase/encoders/proto/b;

    return-void
.end method

.method public c(Ljava/lang/Object;)[B
    .locals 1

    .line 1
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    :try_start_0
    invoke-virtual {p0, p1, v0}, Lo91;->b(Ljava/lang/Object;Ljava/io/OutputStream;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object p1

    return-object p1
.end method

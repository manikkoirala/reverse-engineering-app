.class public Lvi1;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:Ly01;

.field public b:Landroid/content/Context;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ly01;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lvi1;->b:Landroid/content/Context;

    iput-object p2, p0, Lvi1;->a:Ly01;

    iput-object p4, p0, Lvi1;->c:Ljava/lang/String;

    iput-object p3, p0, Lvi1;->d:Ljava/lang/String;

    invoke-virtual {p0}, Lvi1;->g()V

    return-void
.end method

.method public static synthetic a(Lvi1;Ljava/lang/String;)Z
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lvi1;->d(Ljava/lang/String;)Z

    move-result p0

    return p0
.end method

.method public static synthetic b(Lvi1;Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lvi1;->f(Ljava/lang/String;)V

    return-void
.end method

.method public static synthetic c(Lvi1;Ljava/lang/String;)Z
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lvi1;->e(Ljava/lang/String;)Z

    move-result p0

    return p0
.end method


# virtual methods
.method public final d(Ljava/lang/String;)Z
    .locals 1

    .line 1
    iget-object v0, p0, Lvi1;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/ekodroid/omrevaluator/database/repositories/SmsTemplateRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/SmsTemplateRepository;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/ekodroid/omrevaluator/database/repositories/SmsTemplateRepository;->getSmsTemplate(Ljava/lang/String;)Lcom/ekodroid/omrevaluator/database/SmsTemplateDataModel;

    move-result-object p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method public final e(Ljava/lang/String;)Z
    .locals 2

    .line 1
    iget-object v0, p0, Lvi1;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/ekodroid/omrevaluator/database/repositories/SmsTemplateRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/SmsTemplateRepository;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/ekodroid/omrevaluator/database/repositories/SmsTemplateRepository;->deleteSmsTemplate(Ljava/lang/String;)Z

    new-instance v0, Lcom/ekodroid/omrevaluator/database/SmsTemplateDataModel;

    iget-object v1, p0, Lvi1;->c:Ljava/lang/String;

    invoke-direct {v0, p1, v1}, Lcom/ekodroid/omrevaluator/database/SmsTemplateDataModel;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iget-object p1, p0, Lvi1;->b:Landroid/content/Context;

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/database/repositories/SmsTemplateRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/SmsTemplateRepository;

    move-result-object p1

    invoke-virtual {p1, v0}, Lcom/ekodroid/omrevaluator/database/repositories/SmsTemplateRepository;->saveOrUpdateSmsTemplate(Lcom/ekodroid/omrevaluator/database/SmsTemplateDataModel;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method public final f(Ljava/lang/String;)V
    .locals 8

    .line 1
    new-instance v1, Lvi1$c;

    invoke-direct {v1, p0, p1}, Lvi1$c;-><init>(Lvi1;Ljava/lang/String;)V

    iget-object v0, p0, Lvi1;->b:Landroid/content/Context;

    const/4 v2, 0x0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, p0, Lvi1;->b:Landroid/content/Context;

    const v4, 0x7f120173

    invoke-virtual {p1, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f120337

    const v5, 0x7f120241

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static/range {v0 .. v7}, Lxs;->d(Landroid/content/Context;Ly01;ILjava/lang/String;IIII)V

    return-void
.end method

.method public final g()V
    .locals 4

    .line 1
    new-instance v0, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    iget-object v1, p0, Lvi1;->b:Landroid/content/Context;

    const v2, 0x7f130151

    invoke-direct {v0, v1, v2}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;-><init>(Landroid/content/Context;I)V

    iget-object v1, p0, Lvi1;->b:Landroid/content/Context;

    const-string v2, "layout_inflater"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    const v2, 0x7f0c007a

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->setView(Landroid/view/View;)Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    const v2, 0x7f1202ac

    invoke-virtual {v0, v2}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->setTitle(I)Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    const v2, 0x7f090153

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iget-object v2, p0, Lvi1;->d:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    const-string v3, "DEFAULT"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lvi1;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lvi1;->d:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setSelection(I)V

    :cond_0
    new-instance v2, Lvi1$a;

    invoke-direct {v2, p0, v1}, Lvi1$a;-><init>(Lvi1;Landroid/widget/EditText;)V

    const v1, 0x7f1202a7

    invoke-virtual {v0, v1, v2}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    new-instance v1, Lvi1$b;

    invoke-direct {v1, p0}, Lvi1$b;-><init>(Lvi1;)V

    const v2, 0x7f120053

    invoke-virtual {v0, v2, v1}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    invoke-virtual {v0}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->create()Landroidx/appcompat/app/a;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    return-void
.end method

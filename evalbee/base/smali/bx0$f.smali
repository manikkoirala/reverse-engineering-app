.class public Lbx0$f;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lbx0;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "f"
.end annotation


# instance fields
.field public a:Z

.field public b:Ljava/lang/String;

.field public final synthetic c:Lbx0;


# direct methods
.method public constructor <init>(Lbx0;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lbx0$f;->c:Lbx0;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lbx0;Lbx0$a;)V
    .locals 0

    .line 2
    invoke-direct {p0, p1}, Lbx0$f;-><init>(Lbx0;)V

    return-void
.end method


# virtual methods
.method public varargs a([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 3

    .line 1
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, p0, Lbx0$f;->c:Lbx0;

    invoke-static {v0}, Lbx0;->b(Lbx0;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, ".exm"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lbx0$f;->c:Lbx0;

    invoke-static {v1}, Lbx0;->c(Lbx0;)Landroid/content/Context;

    move-result-object v1

    sget-object v2, Lok;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/content/Context;->getExternalFilesDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    sget-object v2, Lok;->d:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    :cond_0
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0, p1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result p1

    if-nez p1, :cond_1

    iget-object p1, p0, Lbx0$f;->c:Lbx0;

    invoke-static {p1}, Lbx0;->b(Lbx0;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lbx0;->d(Lbx0;Ljava/lang/String;)Z

    :cond_1
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result p1

    const/4 v0, 0x1

    const/4 v2, 0x0

    if-nez p1, :cond_2

    iput-boolean v0, p0, Lbx0$f;->a:Z

    iget-object p1, p0, Lbx0$f;->c:Lbx0;

    invoke-static {p1}, Lbx0;->c(Lbx0;)Landroid/content/Context;

    move-result-object p1

    const v0, 0x7f12019f

    :goto_0
    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lbx0$f;->b:Ljava/lang/String;

    return-object v2

    :cond_2
    iget-object p1, p0, Lbx0$f;->c:Lbx0;

    invoke-static {p1, v1}, Lbx0;->e(Lbx0;Ljava/io/File;)Z

    move-result p1

    if-nez p1, :cond_3

    iput-boolean v0, p0, Lbx0$f;->a:Z

    iget-object p1, p0, Lbx0$f;->c:Lbx0;

    invoke-static {p1}, Lbx0;->c(Lbx0;)Landroid/content/Context;

    move-result-object p1

    const v0, 0x7f1201f2

    goto :goto_0

    :cond_3
    return-object v2
.end method

.method public b(Ljava/lang/Void;)V
    .locals 3

    .line 1
    iget-boolean p1, p0, Lbx0$f;->a:Z

    if-eqz p1, :cond_0

    iget-object p1, p0, Lbx0$f;->c:Lbx0;

    iget-object p1, p1, Lbx0;->d:Landroid/widget/LinearLayout;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    iget-object p1, p0, Lbx0$f;->c:Lbx0;

    iget-object p1, p1, Lbx0;->c:Landroid/widget/LinearLayout;

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    iget-object p1, p0, Lbx0$f;->c:Lbx0;

    iget-object p1, p1, Lbx0;->e:Landroid/widget/TextView;

    iget-object v0, p0, Lbx0$f;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object p1, p0, Lbx0$f;->c:Lbx0;

    iget-object p1, p1, Lbx0;->g:Landroid/widget/Button;

    const v0, 0x7f120060

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(I)V

    iget-object p1, p0, Lbx0$f;->c:Lbx0;

    iget-object p1, p1, Lbx0;->f:Landroid/widget/Button;

    const v0, 0x7f12029a

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lbx0$f;->c:Lbx0;

    invoke-static {p1}, Lbx0;->a(Lbx0;)Landroidx/appcompat/app/a;

    move-result-object p1

    invoke-virtual {p1}, Lq6;->dismiss()V

    iget-object p1, p0, Lbx0$f;->c:Lbx0;

    invoke-static {p1}, Lbx0;->c(Lbx0;)Landroid/content/Context;

    move-result-object p1

    const v0, 0x7f0800cf

    const v1, 0x7f08016d

    const v2, 0x7f12007d

    invoke-static {p1, v2, v0, v1}, La91;->G(Landroid/content/Context;III)V

    :goto_0
    return-void
.end method

.method public bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 1
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lbx0$f;->a([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 1
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lbx0$f;->b(Ljava/lang/Void;)V

    return-void
.end method

.method public onPreExecute()V
    .locals 0

    .line 1
    return-void
.end method

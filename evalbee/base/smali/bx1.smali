.class public Lbx1;
.super Lt1;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lbx1$d;,
        Lbx1$c;,
        Lbx1$e;
    }
.end annotation


# instance fields
.field public final a:Ldq;

.field public final b:Landroid/view/Window$Callback;

.field public final c:Lg6$f;

.field public d:Z

.field public e:Z

.field public f:Z

.field public g:Ljava/util/ArrayList;

.field public final h:Ljava/lang/Runnable;

.field public final i:Landroidx/appcompat/widget/Toolbar$h;


# direct methods
.method public constructor <init>(Landroidx/appcompat/widget/Toolbar;Ljava/lang/CharSequence;Landroid/view/Window$Callback;)V
    .locals 3

    .line 1
    invoke-direct {p0}, Lt1;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lbx1;->g:Ljava/util/ArrayList;

    new-instance v0, Lbx1$a;

    invoke-direct {v0, p0}, Lbx1$a;-><init>(Lbx1;)V

    iput-object v0, p0, Lbx1;->h:Ljava/lang/Runnable;

    new-instance v0, Lbx1$b;

    invoke-direct {v0, p0}, Lbx1$b;-><init>(Lbx1;)V

    iput-object v0, p0, Lbx1;->i:Landroidx/appcompat/widget/Toolbar$h;

    invoke-static {p1}, Ll71;->g(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Landroidx/appcompat/widget/d;

    const/4 v2, 0x0

    invoke-direct {v1, p1, v2}, Landroidx/appcompat/widget/d;-><init>(Landroidx/appcompat/widget/Toolbar;Z)V

    iput-object v1, p0, Lbx1;->a:Ldq;

    invoke-static {p3}, Ll71;->g(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/Window$Callback;

    iput-object v2, p0, Lbx1;->b:Landroid/view/Window$Callback;

    invoke-interface {v1, p3}, Ldq;->setWindowCallback(Landroid/view/Window$Callback;)V

    invoke-virtual {p1, v0}, Landroidx/appcompat/widget/Toolbar;->setOnMenuItemClickListener(Landroidx/appcompat/widget/Toolbar$h;)V

    invoke-interface {v1, p2}, Ldq;->setWindowTitle(Ljava/lang/CharSequence;)V

    new-instance p1, Lbx1$e;

    invoke-direct {p1, p0}, Lbx1$e;-><init>(Lbx1;)V

    iput-object p1, p0, Lbx1;->c:Lg6$f;

    return-void
.end method


# virtual methods
.method public f()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lbx1;->a:Ldq;

    invoke-interface {v0}, Ldq;->d()Z

    move-result v0

    return v0
.end method

.method public g()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lbx1;->a:Ldq;

    invoke-interface {v0}, Ldq;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbx1;->a:Ldq;

    invoke-interface {v0}, Ldq;->collapseActionView()V

    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public h(Z)V
    .locals 1

    .line 1
    iget-boolean v0, p0, Lbx1;->f:Z

    if-ne p1, v0, :cond_0

    return-void

    :cond_0
    iput-boolean p1, p0, Lbx1;->f:Z

    iget-object p1, p0, Lbx1;->g:Ljava/util/ArrayList;

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result p1

    if-gtz p1, :cond_1

    return-void

    :cond_1
    iget-object p1, p0, Lbx1;->g:Ljava/util/ArrayList;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    invoke-static {p1}, Lzu0;->a(Ljava/lang/Object;)V

    const/4 p1, 0x0

    throw p1
.end method

.method public i()I
    .locals 1

    .line 1
    iget-object v0, p0, Lbx1;->a:Ldq;

    invoke-interface {v0}, Ldq;->o()I

    move-result v0

    return v0
.end method

.method public j()Landroid/content/Context;
    .locals 1

    .line 1
    iget-object v0, p0, Lbx1;->a:Ldq;

    invoke-interface {v0}, Ldq;->getContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method public k()Z
    .locals 2

    .line 1
    iget-object v0, p0, Lbx1;->a:Ldq;

    invoke-interface {v0}, Ldq;->t()Landroid/view/ViewGroup;

    move-result-object v0

    iget-object v1, p0, Lbx1;->h:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/view/View;->removeCallbacks(Ljava/lang/Runnable;)Z

    iget-object v0, p0, Lbx1;->a:Ldq;

    invoke-interface {v0}, Ldq;->t()Landroid/view/ViewGroup;

    move-result-object v0

    iget-object v1, p0, Lbx1;->h:Ljava/lang/Runnable;

    invoke-static {v0, v1}, Lo32;->i0(Landroid/view/View;Ljava/lang/Runnable;)V

    const/4 v0, 0x1

    return v0
.end method

.method public l(Landroid/content/res/Configuration;)V
    .locals 0

    .line 1
    invoke-super {p0, p1}, Lt1;->l(Landroid/content/res/Configuration;)V

    return-void
.end method

.method public m()V
    .locals 2

    .line 1
    iget-object v0, p0, Lbx1;->a:Ldq;

    invoke-interface {v0}, Ldq;->t()Landroid/view/ViewGroup;

    move-result-object v0

    iget-object v1, p0, Lbx1;->h:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/view/View;->removeCallbacks(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public n(ILandroid/view/KeyEvent;)Z
    .locals 4

    .line 1
    invoke-virtual {p0}, Lbx1;->w()Landroid/view/Menu;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_2

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getDeviceId()I

    move-result v2

    goto :goto_0

    :cond_0
    const/4 v2, -0x1

    :goto_0
    invoke-static {v2}, Landroid/view/KeyCharacterMap;->load(I)Landroid/view/KeyCharacterMap;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/KeyCharacterMap;->getKeyboardType()I

    move-result v2

    const/4 v3, 0x1

    if-eq v2, v3, :cond_1

    goto :goto_1

    :cond_1
    move v3, v1

    :goto_1
    invoke-interface {v0, v3}, Landroid/view/Menu;->setQwertyMode(Z)V

    invoke-interface {v0, p1, p2, v1}, Landroid/view/Menu;->performShortcut(ILandroid/view/KeyEvent;I)Z

    move-result p1

    return p1

    :cond_2
    return v1
.end method

.method public o(Landroid/view/KeyEvent;)Z
    .locals 1

    .line 1
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result p1

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    invoke-virtual {p0}, Lbx1;->p()Z

    :cond_0
    return v0
.end method

.method public p()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lbx1;->a:Ldq;

    invoke-interface {v0}, Ldq;->b()Z

    move-result v0

    return v0
.end method

.method public q(Z)V
    .locals 0

    .line 1
    return-void
.end method

.method public r(Z)V
    .locals 0

    .line 1
    return-void
.end method

.method public s(Ljava/lang/CharSequence;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lbx1;->a:Ldq;

    invoke-interface {v0, p1}, Ldq;->q(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public t(Ljava/lang/CharSequence;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lbx1;->a:Ldq;

    invoke-interface {v0, p1}, Ldq;->setTitle(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public u(Ljava/lang/CharSequence;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lbx1;->a:Ldq;

    invoke-interface {v0, p1}, Ldq;->setWindowTitle(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public final w()Landroid/view/Menu;
    .locals 3

    .line 1
    iget-boolean v0, p0, Lbx1;->e:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lbx1;->a:Ldq;

    new-instance v1, Lbx1$c;

    invoke-direct {v1, p0}, Lbx1$c;-><init>(Lbx1;)V

    new-instance v2, Lbx1$d;

    invoke-direct {v2, p0}, Lbx1$d;-><init>(Lbx1;)V

    invoke-interface {v0, v1, v2}, Ldq;->x(Landroidx/appcompat/view/menu/i$a;Landroidx/appcompat/view/menu/e$a;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lbx1;->e:Z

    :cond_0
    iget-object v0, p0, Lbx1;->a:Ldq;

    invoke-interface {v0}, Ldq;->r()Landroid/view/Menu;

    move-result-object v0

    return-object v0
.end method

.method public x()V
    .locals 5

    .line 1
    invoke-virtual {p0}, Lbx1;->w()Landroid/view/Menu;

    move-result-object v0

    instance-of v1, v0, Landroidx/appcompat/view/menu/e;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    move-object v1, v0

    check-cast v1, Landroidx/appcompat/view/menu/e;

    goto :goto_0

    :cond_0
    move-object v1, v2

    :goto_0
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Landroidx/appcompat/view/menu/e;->stopDispatchingItemsChanged()V

    :cond_1
    :try_start_0
    invoke-interface {v0}, Landroid/view/Menu;->clear()V

    iget-object v3, p0, Lbx1;->b:Landroid/view/Window$Callback;

    const/4 v4, 0x0

    invoke-interface {v3, v4, v0}, Landroid/view/Window$Callback;->onCreatePanelMenu(ILandroid/view/Menu;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lbx1;->b:Landroid/view/Window$Callback;

    invoke-interface {v3, v4, v2, v0}, Landroid/view/Window$Callback;->onPreparePanel(ILandroid/view/View;Landroid/view/Menu;)Z

    move-result v2

    if-nez v2, :cond_3

    :cond_2
    invoke-interface {v0}, Landroid/view/Menu;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_3
    if-eqz v1, :cond_4

    invoke-virtual {v1}, Landroidx/appcompat/view/menu/e;->startDispatchingItemsChanged()V

    :cond_4
    return-void

    :catchall_0
    move-exception v0

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Landroidx/appcompat/view/menu/e;->startDispatchingItemsChanged()V

    :cond_5
    throw v0
.end method

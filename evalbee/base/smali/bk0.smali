.class public abstract Lbk0;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lbk0$a;
    }
.end annotation


# instance fields
.field public a:Lbk0$a;

.field public b:Lbk0$a;

.field public c:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lbk0;->a:Lbk0$a;

    iput-object v0, p0, Lbk0;->b:Lbk0$a;

    const/4 v0, 0x0

    iput v0, p0, Lbk0;->c:I

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Object;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lbk0;->a:Lbk0$a;

    invoke-virtual {p0, v0, p1}, Lbk0;->e(Lbk0$a;Ljava/lang/Object;)V

    return-void
.end method

.method public b(Ljava/lang/Object;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lbk0;->b:Lbk0$a;

    invoke-virtual {p0, v0, p1}, Lbk0;->d(Lbk0$a;Ljava/lang/Object;)V

    return-void
.end method

.method public c(Ljava/lang/Object;)Lbk0$a;
    .locals 1

    .line 1
    new-instance v0, Lbk0$a;

    invoke-direct {v0, p0, p1}, Lbk0$a;-><init>(Lbk0;Ljava/lang/Object;)V

    return-object v0
.end method

.method public d(Lbk0$a;Ljava/lang/Object;)V
    .locals 2

    .line 1
    invoke-virtual {p0, p2}, Lbk0;->c(Ljava/lang/Object;)Lbk0$a;

    move-result-object p2

    iget v0, p0, Lbk0;->c:I

    if-nez v0, :cond_0

    iput-object p2, p0, Lbk0;->a:Lbk0$a;

    :goto_0
    iput-object p2, p0, Lbk0;->b:Lbk0$a;

    goto :goto_1

    :cond_0
    iget-object v1, p0, Lbk0;->b:Lbk0$a;

    if-ne p1, v1, :cond_1

    iput-object v1, p2, Lbk0$a;->c:Lbk0$a;

    iput-object p2, v1, Lbk0$a;->b:Lbk0$a;

    goto :goto_0

    :cond_1
    iget-object v1, p1, Lbk0$a;->b:Lbk0$a;

    iput-object p2, v1, Lbk0$a;->c:Lbk0$a;

    iput-object v1, p2, Lbk0$a;->b:Lbk0$a;

    iput-object p2, p1, Lbk0$a;->b:Lbk0$a;

    iput-object p1, p2, Lbk0$a;->c:Lbk0$a;

    :goto_1
    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lbk0;->c:I

    return-void
.end method

.method public e(Lbk0$a;Ljava/lang/Object;)V
    .locals 2

    .line 1
    invoke-virtual {p0, p2}, Lbk0;->c(Ljava/lang/Object;)Lbk0$a;

    move-result-object p2

    iget v0, p0, Lbk0;->c:I

    if-nez v0, :cond_0

    iput-object p2, p0, Lbk0;->a:Lbk0$a;

    iput-object p2, p0, Lbk0;->b:Lbk0$a;

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lbk0;->a:Lbk0$a;

    if-ne p1, v1, :cond_1

    iput-object v1, p2, Lbk0$a;->b:Lbk0$a;

    iput-object p2, v1, Lbk0$a;->c:Lbk0$a;

    iput-object p2, p0, Lbk0;->a:Lbk0$a;

    goto :goto_0

    :cond_1
    iget-object v1, p1, Lbk0$a;->c:Lbk0$a;

    iput-object p2, v1, Lbk0$a;->b:Lbk0$a;

    iput-object v1, p2, Lbk0$a;->c:Lbk0$a;

    iput-object p2, p1, Lbk0$a;->c:Lbk0$a;

    iput-object p1, p2, Lbk0$a;->b:Lbk0$a;

    :goto_0
    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lbk0;->c:I

    return-void
.end method

.method public f()Z
    .locals 1

    .line 1
    iget v0, p0, Lbk0;->c:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public g()Ljava/lang/Object;
    .locals 1

    .line 1
    iget-object v0, p0, Lbk0;->a:Lbk0$a;

    invoke-virtual {p0, v0}, Lbk0;->h(Lbk0$a;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public h(Lbk0$a;)Ljava/lang/Object;
    .locals 5

    .line 1
    iget v0, p0, Lbk0;->c:I

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return-object v1

    :cond_0
    iget-object v2, p1, Lbk0$a;->d:Ljava/lang/Object;

    iget-object v3, p0, Lbk0;->a:Lbk0$a;

    if-ne p1, v3, :cond_2

    iget-object v3, v3, Lbk0$a;->b:Lbk0$a;

    iput-object v3, p0, Lbk0;->a:Lbk0$a;

    if-nez v3, :cond_1

    iput-object v1, p0, Lbk0;->b:Lbk0$a;

    goto :goto_0

    :cond_1
    iput-object v1, v3, Lbk0$a;->c:Lbk0$a;

    goto :goto_0

    :cond_2
    iget-object v3, p0, Lbk0;->b:Lbk0$a;

    if-ne p1, v3, :cond_3

    iget-object v3, v3, Lbk0$a;->c:Lbk0$a;

    iput-object v3, p0, Lbk0;->b:Lbk0$a;

    iput-object v1, v3, Lbk0$a;->b:Lbk0$a;

    goto :goto_0

    :cond_3
    iget-object v3, p1, Lbk0$a;->c:Lbk0$a;

    iget-object v4, p1, Lbk0$a;->b:Lbk0$a;

    iput-object v4, v3, Lbk0$a;->b:Lbk0$a;

    iget-object v4, p1, Lbk0$a;->b:Lbk0$a;

    iput-object v3, v4, Lbk0$a;->c:Lbk0$a;

    :goto_0
    iput-object v1, p1, Lbk0$a;->a:Lbk0;

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lbk0;->c:I

    return-object v2
.end method

.method public i()Ljava/lang/Object;
    .locals 1

    .line 1
    iget-object v0, p0, Lbk0;->b:Lbk0$a;

    invoke-virtual {p0, v0}, Lbk0;->h(Lbk0$a;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public j()I
    .locals 1

    .line 1
    iget v0, p0, Lbk0;->c:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 1
    new-instance v0, Ljava/lang/StringBuffer;

    iget v1, p0, Lbk0;->c:I

    mul-int/lit8 v1, v1, 0x6

    invoke-direct {v0, v1}, Ljava/lang/StringBuffer;-><init>(I)V

    const-string v1, "["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget-object v1, p0, Lbk0;->a:Lbk0$a;

    if-eqz v1, :cond_0

    :goto_0
    iget-object v2, v1, Lbk0$a;->d:Ljava/lang/Object;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/Object;)Ljava/lang/StringBuffer;

    iget-object v1, v1, Lbk0$a;->b:Lbk0$a;

    :cond_0
    if-eqz v1, :cond_1

    const-string v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_0

    :cond_1
    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

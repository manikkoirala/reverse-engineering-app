.class public Lzv$a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lzv;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Landroid/widget/EditText;

.field public final synthetic b:Landroid/widget/EditText;

.field public final synthetic c:Landroid/widget/Spinner;

.field public final synthetic d:Landroid/widget/Spinner;

.field public final synthetic e:Landroid/widget/CheckBox;

.field public final synthetic f:Landroid/widget/CheckBox;

.field public final synthetic g:Lzv;


# direct methods
.method public constructor <init>(Lzv;Landroid/widget/EditText;Landroid/widget/EditText;Landroid/widget/Spinner;Landroid/widget/Spinner;Landroid/widget/CheckBox;Landroid/widget/CheckBox;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lzv$a;->g:Lzv;

    iput-object p2, p0, Lzv$a;->a:Landroid/widget/EditText;

    iput-object p3, p0, Lzv$a;->b:Landroid/widget/EditText;

    iput-object p4, p0, Lzv$a;->c:Landroid/widget/Spinner;

    iput-object p5, p0, Lzv$a;->d:Landroid/widget/Spinner;

    iput-object p6, p0, Lzv$a;->e:Landroid/widget/CheckBox;

    iput-object p7, p0, Lzv$a;->f:Landroid/widget/CheckBox;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 17

    .line 1
    move-object/from16 v0, p0

    iget-object v1, v0, Lzv$a;->g:Lzv;

    invoke-static {v1}, Lzv;->a(Lzv;)Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel;->i()Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel$ViewType;

    move-result-object v1

    sget-object v2, Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel$ViewType;->LABEL:Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel$ViewType;

    if-ne v1, v2, :cond_0

    new-instance v1, Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel;

    iget-object v2, v0, Lzv$a;->g:Lzv;

    invoke-static {v2}, Lzv;->a(Lzv;)Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel;->i()Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel$ViewType;

    move-result-object v4

    iget-object v2, v0, Lzv$a;->a:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    iget-object v2, v0, Lzv$a;->b:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    iget-object v2, v0, Lzv$a;->g:Lzv;

    invoke-static {v2}, Lzv;->a(Lzv;)Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel;->d()I

    move-result v7

    iget-object v2, v0, Lzv$a;->g:Lzv;

    invoke-static {v2}, Lzv;->a(Lzv;)Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel;->g()I

    move-result v8

    iget-object v2, v0, Lzv$a;->g:Lzv;

    invoke-static {v2}, Lzv;->a(Lzv;)Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel;->e()I

    move-result v9

    iget-object v2, v0, Lzv$a;->g:Lzv;

    invoke-static {v2}, Lzv;->a(Lzv;)Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel;->a()D

    move-result-wide v10

    iget-object v2, v0, Lzv$a;->g:Lzv;

    invoke-static {v2}, Lzv;->a(Lzv;)Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel;->b()D

    move-result-wide v12

    iget-object v2, v0, Lzv$a;->g:Lzv;

    invoke-static {v2}, Lzv;->a(Lzv;)Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel;->j()Z

    move-result v14

    iget-object v2, v0, Lzv$a;->g:Lzv;

    invoke-static {v2}, Lzv;->a(Lzv;)Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel;->c()Ljava/lang/String;

    move-result-object v15

    move-object v3, v1

    invoke-direct/range {v3 .. v15}, Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel;-><init>(Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel$ViewType;Ljava/lang/String;Ljava/lang/String;IIIDDZLjava/lang/String;)V

    iget-object v2, v0, Lzv$a;->g:Lzv;

    invoke-static {v2}, Lzv;->b(Lzv;)Lzv$d;

    move-result-object v2

    const/4 v3, 0x1

    goto/16 :goto_0

    :cond_0
    new-instance v1, Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel;

    iget-object v2, v0, Lzv$a;->g:Lzv;

    invoke-static {v2}, Lzv;->a(Lzv;)Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel;->i()Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel$ViewType;

    move-result-object v5

    iget-object v2, v0, Lzv$a;->g:Lzv;

    invoke-static {v2}, Lzv;->a(Lzv;)Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel;->h()Ljava/lang/String;

    move-result-object v6

    iget-object v2, v0, Lzv$a;->g:Lzv;

    invoke-static {v2}, Lzv;->a(Lzv;)Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel;->f()Ljava/lang/String;

    move-result-object v7

    iget-object v2, v0, Lzv$a;->g:Lzv;

    invoke-static {v2}, Lzv;->a(Lzv;)Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel;->d()I

    move-result v8

    iget-object v2, v0, Lzv$a;->g:Lzv;

    invoke-static {v2}, Lzv;->a(Lzv;)Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel;->g()I

    move-result v9

    iget-object v2, v0, Lzv$a;->g:Lzv;

    invoke-static {v2}, Lzv;->a(Lzv;)Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel;->e()I

    move-result v10

    iget-object v2, v0, Lzv$a;->g:Lzv;

    iget-object v2, v2, Lzv;->a:Ljava/util/ArrayList;

    iget-object v3, v0, Lzv$a;->c:Landroid/widget/Spinner;

    invoke-virtual {v3}, Landroid/widget/AdapterView;->getSelectedItemPosition()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Double;

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v11

    iget-object v2, v0, Lzv$a;->g:Lzv;

    iget-object v2, v2, Lzv;->b:Ljava/util/ArrayList;

    iget-object v3, v0, Lzv$a;->d:Landroid/widget/Spinner;

    invoke-virtual {v3}, Landroid/widget/AdapterView;->getSelectedItemPosition()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Double;

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v13

    iget-object v2, v0, Lzv$a;->e:Landroid/widget/CheckBox;

    invoke-virtual {v2}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v15

    iget-object v2, v0, Lzv$a;->g:Lzv;

    invoke-static {v2}, Lzv;->a(Lzv;)Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel;->c()Ljava/lang/String;

    move-result-object v16

    move-object v4, v1

    invoke-direct/range {v4 .. v16}, Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel;-><init>(Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel$ViewType;Ljava/lang/String;Ljava/lang/String;IIIDDZLjava/lang/String;)V

    iget-object v2, v0, Lzv$a;->g:Lzv;

    invoke-static {v2}, Lzv;->b(Lzv;)Lzv$d;

    move-result-object v2

    iget-object v3, v0, Lzv$a;->f:Landroid/widget/CheckBox;

    invoke-virtual {v3}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v3

    :goto_0
    invoke-interface {v2, v1, v3}, Lzv$d;->a(Lcom/ekodroid/omrevaluator/templateui/models/QueEditDataModel;Z)V

    iget-object v1, v0, Lzv$a;->g:Lzv;

    invoke-virtual {v1}, Landroid/app/Dialog;->dismiss()V

    return-void
.end method

.class public Lty;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lre1;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lty$b;
    }
.end annotation


# instance fields
.field public final a:Ljava/util/concurrent/Executor;


# direct methods
.method public constructor <init>(Landroid/os/Handler;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lty$a;

    invoke-direct {v0, p0, p1}, Lty$a;-><init>(Lty;Landroid/os/Handler;)V

    iput-object v0, p0, Lty;->a:Ljava/util/concurrent/Executor;

    return-void
.end method


# virtual methods
.method public a(Lcom/android/volley/Request;Lcom/android/volley/d;)V
    .locals 1

    .line 1
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lty;->b(Lcom/android/volley/Request;Lcom/android/volley/d;Ljava/lang/Runnable;)V

    return-void
.end method

.method public b(Lcom/android/volley/Request;Lcom/android/volley/d;Ljava/lang/Runnable;)V
    .locals 2

    .line 1
    invoke-virtual {p1}, Lcom/android/volley/Request;->C()V

    const-string v0, "post-response"

    invoke-virtual {p1, v0}, Lcom/android/volley/Request;->c(Ljava/lang/String;)V

    iget-object v0, p0, Lty;->a:Ljava/util/concurrent/Executor;

    new-instance v1, Lty$b;

    invoke-direct {v1, p1, p2, p3}, Lty$b;-><init>(Lcom/android/volley/Request;Lcom/android/volley/d;Ljava/lang/Runnable;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.method public c(Lcom/android/volley/Request;Lcom/android/volley/VolleyError;)V
    .locals 3

    .line 1
    const-string v0, "post-error"

    invoke-virtual {p1, v0}, Lcom/android/volley/Request;->c(Ljava/lang/String;)V

    invoke-static {p2}, Lcom/android/volley/d;->a(Lcom/android/volley/VolleyError;)Lcom/android/volley/d;

    move-result-object p2

    iget-object v0, p0, Lty;->a:Ljava/util/concurrent/Executor;

    new-instance v1, Lty$b;

    const/4 v2, 0x0

    invoke-direct {v1, p1, p2, v2}, Lty$b;-><init>(Lcom/android/volley/Request;Lcom/android/volley/d;Ljava/lang/Runnable;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    return-void
.end method

.class public Lu62$h;
.super Lu62$g;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lu62;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "h"
.end annotation


# instance fields
.field public m:Lnf0;


# direct methods
.method public constructor <init>(Lu62;Landroid/view/WindowInsets;)V
    .locals 0

    .line 2
    invoke-direct {p0, p1, p2}, Lu62$g;-><init>(Lu62;Landroid/view/WindowInsets;)V

    const/4 p1, 0x0

    iput-object p1, p0, Lu62$h;->m:Lnf0;

    return-void
.end method

.method public constructor <init>(Lu62;Lu62$h;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lu62$g;-><init>(Lu62;Lu62$g;)V

    const/4 p1, 0x0

    iput-object p1, p0, Lu62$h;->m:Lnf0;

    iget-object p1, p2, Lu62$h;->m:Lnf0;

    iput-object p1, p0, Lu62$h;->m:Lnf0;

    return-void
.end method


# virtual methods
.method public b()Lu62;
    .locals 1

    .line 1
    iget-object v0, p0, Lu62$g;->c:Landroid/view/WindowInsets;

    invoke-virtual {v0}, Landroid/view/WindowInsets;->consumeStableInsets()Landroid/view/WindowInsets;

    move-result-object v0

    invoke-static {v0}, Lu62;->v(Landroid/view/WindowInsets;)Lu62;

    move-result-object v0

    return-object v0
.end method

.method public c()Lu62;
    .locals 1

    .line 1
    iget-object v0, p0, Lu62$g;->c:Landroid/view/WindowInsets;

    invoke-virtual {v0}, Landroid/view/WindowInsets;->consumeSystemWindowInsets()Landroid/view/WindowInsets;

    move-result-object v0

    invoke-static {v0}, Lu62;->v(Landroid/view/WindowInsets;)Lu62;

    move-result-object v0

    return-object v0
.end method

.method public final i()Lnf0;
    .locals 4

    .line 1
    iget-object v0, p0, Lu62$h;->m:Lnf0;

    if-nez v0, :cond_0

    iget-object v0, p0, Lu62$g;->c:Landroid/view/WindowInsets;

    invoke-virtual {v0}, Landroid/view/WindowInsets;->getStableInsetLeft()I

    move-result v0

    iget-object v1, p0, Lu62$g;->c:Landroid/view/WindowInsets;

    invoke-virtual {v1}, Landroid/view/WindowInsets;->getStableInsetTop()I

    move-result v1

    iget-object v2, p0, Lu62$g;->c:Landroid/view/WindowInsets;

    invoke-virtual {v2}, Landroid/view/WindowInsets;->getStableInsetRight()I

    move-result v2

    iget-object v3, p0, Lu62$g;->c:Landroid/view/WindowInsets;

    invoke-virtual {v3}, Landroid/view/WindowInsets;->getStableInsetBottom()I

    move-result v3

    invoke-static {v0, v1, v2, v3}, Lnf0;->b(IIII)Lnf0;

    move-result-object v0

    iput-object v0, p0, Lu62$h;->m:Lnf0;

    :cond_0
    iget-object v0, p0, Lu62$h;->m:Lnf0;

    return-object v0
.end method

.method public n()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lu62$g;->c:Landroid/view/WindowInsets;

    invoke-virtual {v0}, Landroid/view/WindowInsets;->isConsumed()Z

    move-result v0

    return v0
.end method

.method public s(Lnf0;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lu62$h;->m:Lnf0;

    return-void
.end method

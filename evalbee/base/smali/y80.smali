.class public Ly80;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:[Ljava/lang/String;

.field public b:[Lt90;

.field public c:I

.field public d:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0x32

    new-array v1, v0, [Ljava/lang/String;

    iput-object v1, p0, Ly80;->a:[Ljava/lang/String;

    new-array v0, v0, [Lt90;

    iput-object v0, p0, Ly80;->b:[Lt90;

    const/4 v0, 0x0

    iput v0, p0, Ly80;->c:I

    iput-boolean v0, p0, Ly80;->d:Z

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;I)Lt90;
    .locals 3

    .line 1
    const/4 v0, 0x0

    :goto_0
    iget v1, p0, Ly80;->c:I

    if-ge v0, v1, :cond_3

    iget-object v1, p0, Ly80;->b:[Lt90;

    aget-object v1, v1, v0

    invoke-interface {v1, p2}, Lt90;->b(I)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-boolean v1, p0, Ly80;->d:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Ly80;->a:[Ljava/lang/String;

    aget-object v1, v1, v0

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    iget-boolean v1, p0, Ly80;->d:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Ly80;->a:[Ljava/lang/String;

    aget-object v1, v1, v0

    invoke-virtual {v1, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_1
    iget-object p1, p0, Ly80;->b:[Lt90;

    aget-object p1, p1, v0

    return-object p1

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "function not found: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, " "

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public b()V
    .locals 2

    .line 1
    new-instance v0, Lvw0;

    invoke-direct {v0}, Lvw0;-><init>()V

    const-string v1, "min"

    invoke-virtual {p0, v1, v0}, Ly80;->c(Ljava/lang/String;Lt90;)V

    new-instance v0, Llu0;

    invoke-direct {v0}, Llu0;-><init>()V

    const-string v1, "max"

    invoke-virtual {p0, v1, v0}, Ly80;->c(Ljava/lang/String;Lt90;)V

    new-instance v0, Les1;

    invoke-direct {v0}, Les1;-><init>()V

    const-string v1, "sum"

    invoke-virtual {p0, v1, v0}, Ly80;->c(Ljava/lang/String;Lt90;)V

    new-instance v0, Lva;

    invoke-direct {v0}, Lva;-><init>()V

    const-string v1, "avg"

    invoke-virtual {p0, v1, v0}, Ly80;->c(Ljava/lang/String;Lt90;)V

    new-instance v0, Lj51;

    invoke-direct {v0}, Lj51;-><init>()V

    const-string v1, "pi"

    invoke-virtual {p0, v1, v0}, Ly80;->c(Ljava/lang/String;Lt90;)V

    new-instance v0, Lwv;

    invoke-direct {v0}, Lwv;-><init>()V

    const-string v1, "e"

    invoke-virtual {p0, v1, v0}, Ly80;->c(Ljava/lang/String;Lt90;)V

    new-instance v0, Lcc1;

    invoke-direct {v0}, Lcc1;-><init>()V

    const-string v1, "rand"

    invoke-virtual {p0, v1, v0}, Ly80;->c(Ljava/lang/String;Lt90;)V

    new-instance v0, Leo1;

    invoke-direct {v0}, Leo1;-><init>()V

    const-string v1, "sin"

    invoke-virtual {p0, v1, v0}, Ly80;->c(Ljava/lang/String;Lt90;)V

    new-instance v0, Lqm;

    invoke-direct {v0}, Lqm;-><init>()V

    const-string v1, "cos"

    invoke-virtual {p0, v1, v0}, Ly80;->c(Ljava/lang/String;Lt90;)V

    new-instance v0, Lwt1;

    invoke-direct {v0}, Lwt1;-><init>()V

    const-string v1, "tan"

    invoke-virtual {p0, v1, v0}, Ly80;->c(Ljava/lang/String;Lt90;)V

    new-instance v0, Lgp1;

    invoke-direct {v0}, Lgp1;-><init>()V

    const-string v1, "sqrt"

    invoke-virtual {p0, v1, v0}, Ly80;->c(Ljava/lang/String;Lt90;)V

    new-instance v0, Ld;

    invoke-direct {v0}, Ld;-><init>()V

    const-string v1, "abs"

    invoke-virtual {p0, v1, v0}, Ly80;->c(Ljava/lang/String;Lt90;)V

    new-instance v0, Lqf;

    invoke-direct {v0}, Lqf;-><init>()V

    const-string v1, "ceil"

    invoke-virtual {p0, v1, v0}, Ly80;->c(Ljava/lang/String;Lt90;)V

    new-instance v0, Lx40;

    invoke-direct {v0}, Lx40;-><init>()V

    const-string v1, "floor"

    invoke-virtual {p0, v1, v0}, Ly80;->c(Ljava/lang/String;Lt90;)V

    new-instance v0, Liz;

    invoke-direct {v0}, Liz;-><init>()V

    const-string v1, "exp"

    invoke-virtual {p0, v1, v0}, Ly80;->c(Ljava/lang/String;Lt90;)V

    new-instance v0, Ljj0;

    invoke-direct {v0}, Ljj0;-><init>()V

    const-string v1, "lg"

    invoke-virtual {p0, v1, v0}, Ly80;->c(Ljava/lang/String;Lt90;)V

    new-instance v0, Lpk0;

    invoke-direct {v0}, Lpk0;-><init>()V

    const-string v1, "ln"

    invoke-virtual {p0, v1, v0}, Ly80;->c(Ljava/lang/String;Lt90;)V

    new-instance v0, Lao1;

    invoke-direct {v0}, Lao1;-><init>()V

    const-string v1, "sign"

    invoke-virtual {p0, v1, v0}, Ly80;->c(Ljava/lang/String;Lt90;)V

    new-instance v0, Ltf1;

    invoke-direct {v0}, Ltf1;-><init>()V

    const-string v1, "round"

    invoke-virtual {p0, v1, v0}, Ly80;->c(Ljava/lang/String;Lt90;)V

    new-instance v0, Lwz;

    invoke-direct {v0}, Lwz;-><init>()V

    const-string v1, "fact"

    invoke-virtual {p0, v1, v0}, Ly80;->c(Ljava/lang/String;Lt90;)V

    new-instance v0, Lrm;

    invoke-direct {v0}, Lrm;-><init>()V

    const-string v1, "cosh"

    invoke-virtual {p0, v1, v0}, Ly80;->c(Ljava/lang/String;Lt90;)V

    new-instance v0, Lgo1;

    invoke-direct {v0}, Lgo1;-><init>()V

    const-string v1, "sinh"

    invoke-virtual {p0, v1, v0}, Ly80;->c(Ljava/lang/String;Lt90;)V

    new-instance v0, Lxt1;

    invoke-direct {v0}, Lxt1;-><init>()V

    const-string v1, "tanh"

    invoke-virtual {p0, v1, v0}, Ly80;->c(Ljava/lang/String;Lt90;)V

    new-instance v0, Lr1;

    invoke-direct {v0}, Lr1;-><init>()V

    const-string v1, "acos"

    invoke-virtual {p0, v1, v0}, Ly80;->c(Ljava/lang/String;Lt90;)V

    new-instance v0, Le9;

    invoke-direct {v0}, Le9;-><init>()V

    const-string v1, "asin"

    invoke-virtual {p0, v1, v0}, Ly80;->c(Ljava/lang/String;Lt90;)V

    new-instance v0, Lr9;

    invoke-direct {v0}, Lr9;-><init>()V

    const-string v1, "atan"

    invoke-virtual {p0, v1, v0}, Ly80;->c(Ljava/lang/String;Lt90;)V

    new-instance v0, Ls1;

    invoke-direct {v0}, Ls1;-><init>()V

    const-string v1, "acosh"

    invoke-virtual {p0, v1, v0}, Ly80;->c(Ljava/lang/String;Lt90;)V

    new-instance v0, Lf9;

    invoke-direct {v0}, Lf9;-><init>()V

    const-string v1, "asinh"

    invoke-virtual {p0, v1, v0}, Ly80;->c(Ljava/lang/String;Lt90;)V

    new-instance v0, Ls9;

    invoke-direct {v0}, Ls9;-><init>()V

    const-string v1, "atanh"

    invoke-virtual {p0, v1, v0}, Ly80;->c(Ljava/lang/String;Lt90;)V

    new-instance v0, Lt61;

    invoke-direct {v0}, Lt61;-><init>()V

    const-string v1, "pow"

    invoke-virtual {p0, v1, v0}, Ly80;->c(Ljava/lang/String;Lt90;)V

    new-instance v0, Lxw0;

    invoke-direct {v0}, Lxw0;-><init>()V

    const-string v1, "mod"

    invoke-virtual {p0, v1, v0}, Ly80;->c(Ljava/lang/String;Lt90;)V

    new-instance v0, Ldi;

    invoke-direct {v0}, Ldi;-><init>()V

    const-string v1, "combin"

    invoke-virtual {p0, v1, v0}, Ly80;->c(Ljava/lang/String;Lt90;)V

    new-instance v0, Lvl0;

    invoke-direct {v0}, Lvl0;-><init>()V

    const-string v1, "log"

    invoke-virtual {p0, v1, v0}, Ly80;->c(Ljava/lang/String;Lt90;)V

    return-void
.end method

.method public c(Ljava/lang/String;Lt90;)V
    .locals 4

    .line 1
    if-eqz p1, :cond_7

    if-eqz p2, :cond_6

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget v2, p0, Ly80;->c:I

    if-ge v1, v2, :cond_3

    iget-boolean v2, p0, Ly80;->d:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Ly80;->a:[Ljava/lang/String;

    aget-object v2, v2, v1

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    iget-boolean v2, p0, Ly80;->d:Z

    if-nez v2, :cond_2

    iget-object v2, p0, Ly80;->a:[Ljava/lang/String;

    aget-object v2, v2, v1

    invoke-virtual {v2, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_1
    iget-object p1, p0, Ly80;->b:[Lt90;

    aput-object p2, p1, v1

    return-void

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_3
    iget-object v1, p0, Ly80;->a:[Ljava/lang/String;

    array-length v1, v1

    if-ne v2, v1, :cond_5

    mul-int/lit8 v2, v2, 0x2

    new-array v1, v2, [Ljava/lang/String;

    new-array v2, v2, [Lt90;

    :goto_1
    iget v3, p0, Ly80;->c:I

    if-ge v0, v3, :cond_4

    iget-object v3, p0, Ly80;->a:[Ljava/lang/String;

    aget-object v3, v3, v0

    aput-object v3, v1, v0

    iget-object v3, p0, Ly80;->b:[Lt90;

    aget-object v3, v3, v0

    aput-object v3, v2, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_4
    iput-object v1, p0, Ly80;->a:[Ljava/lang/String;

    iput-object v2, p0, Ly80;->b:[Lt90;

    :cond_5
    iget-object v0, p0, Ly80;->a:[Ljava/lang/String;

    iget v1, p0, Ly80;->c:I

    aput-object p1, v0, v1

    iget-object p1, p0, Ly80;->b:[Lt90;

    aput-object p2, p1, v1

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Ly80;->c:I

    return-void

    :cond_6
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "function cannot be null"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_7
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "function name cannot be null"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

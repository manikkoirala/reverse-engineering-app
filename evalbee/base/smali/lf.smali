.class public Llf;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lnf;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lmf;)V
    .locals 1

    .line 1
    invoke-virtual {p0, p1}, Llf;->n(Lmf;)F

    move-result v0

    invoke-virtual {p0, p1, v0}, Llf;->c(Lmf;F)V

    return-void
.end method

.method public b(Lmf;)F
    .locals 0

    .line 1
    invoke-interface {p1}, Lmf;->e()Landroid/view/View;

    move-result-object p1

    invoke-virtual {p1}, Landroid/view/View;->getElevation()F

    move-result p1

    return p1
.end method

.method public c(Lmf;F)V
    .locals 3

    .line 1
    invoke-virtual {p0, p1}, Llf;->p(Lmf;)Luf1;

    move-result-object v0

    invoke-interface {p1}, Lmf;->a()Z

    move-result v1

    invoke-interface {p1}, Lmf;->d()Z

    move-result v2

    invoke-virtual {v0, p2, v1, v2}, Luf1;->g(FZZ)V

    invoke-virtual {p0, p1}, Llf;->g(Lmf;)V

    return-void
.end method

.method public d(Lmf;F)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Llf;->p(Lmf;)Luf1;

    move-result-object p1

    invoke-virtual {p1, p2}, Luf1;->h(F)V

    return-void
.end method

.method public e(Lmf;)F
    .locals 1

    .line 1
    invoke-virtual {p0, p1}, Llf;->k(Lmf;)F

    move-result p1

    const/high16 v0, 0x40000000    # 2.0f

    mul-float/2addr p1, v0

    return p1
.end method

.method public f(Lmf;)V
    .locals 1

    .line 1
    invoke-virtual {p0, p1}, Llf;->n(Lmf;)F

    move-result v0

    invoke-virtual {p0, p1, v0}, Llf;->c(Lmf;F)V

    return-void
.end method

.method public g(Lmf;)V
    .locals 4

    .line 1
    invoke-interface {p1}, Lmf;->a()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    invoke-interface {p1, v0, v0, v0, v0}, Lmf;->setShadowPadding(IIII)V

    return-void

    :cond_0
    invoke-virtual {p0, p1}, Llf;->n(Lmf;)F

    move-result v0

    invoke-virtual {p0, p1}, Llf;->k(Lmf;)F

    move-result v1

    invoke-interface {p1}, Lmf;->d()Z

    move-result v2

    invoke-static {v0, v1, v2}, Lvf1;->a(FFZ)F

    move-result v2

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v2, v2

    invoke-interface {p1}, Lmf;->d()Z

    move-result v3

    invoke-static {v0, v1, v3}, Lvf1;->b(FFZ)F

    move-result v0

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    invoke-interface {p1, v2, v0, v2, v0}, Lmf;->setShadowPadding(IIII)V

    return-void
.end method

.method public h(Lmf;)F
    .locals 1

    .line 1
    invoke-virtual {p0, p1}, Llf;->k(Lmf;)F

    move-result p1

    const/high16 v0, 0x40000000    # 2.0f

    mul-float/2addr p1, v0

    return p1
.end method

.method public i(Lmf;Landroid/content/Context;Landroid/content/res/ColorStateList;FFF)V
    .locals 0

    .line 1
    new-instance p2, Luf1;

    invoke-direct {p2, p3, p4}, Luf1;-><init>(Landroid/content/res/ColorStateList;F)V

    invoke-interface {p1, p2}, Lmf;->b(Landroid/graphics/drawable/Drawable;)V

    invoke-interface {p1}, Lmf;->e()Landroid/view/View;

    move-result-object p2

    const/4 p3, 0x1

    invoke-virtual {p2, p3}, Landroid/view/View;->setClipToOutline(Z)V

    invoke-virtual {p2, p5}, Landroid/view/View;->setElevation(F)V

    invoke-virtual {p0, p1, p6}, Llf;->c(Lmf;F)V

    return-void
.end method

.method public j(Lmf;Landroid/content/res/ColorStateList;)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Llf;->p(Lmf;)Luf1;

    move-result-object p1

    invoke-virtual {p1, p2}, Luf1;->f(Landroid/content/res/ColorStateList;)V

    return-void
.end method

.method public k(Lmf;)F
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Llf;->p(Lmf;)Luf1;

    move-result-object p1

    invoke-virtual {p1}, Luf1;->d()F

    move-result p1

    return p1
.end method

.method public l(Lmf;)Landroid/content/res/ColorStateList;
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Llf;->p(Lmf;)Luf1;

    move-result-object p1

    invoke-virtual {p1}, Luf1;->b()Landroid/content/res/ColorStateList;

    move-result-object p1

    return-object p1
.end method

.method public m(Lmf;F)V
    .locals 0

    .line 1
    invoke-interface {p1}, Lmf;->e()Landroid/view/View;

    move-result-object p1

    invoke-virtual {p1, p2}, Landroid/view/View;->setElevation(F)V

    return-void
.end method

.method public n(Lmf;)F
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Llf;->p(Lmf;)Luf1;

    move-result-object p1

    invoke-virtual {p1}, Luf1;->c()F

    move-result p1

    return p1
.end method

.method public o()V
    .locals 0

    .line 1
    return-void
.end method

.method public final p(Lmf;)Luf1;
    .locals 0

    .line 1
    invoke-interface {p1}, Lmf;->c()Landroid/graphics/drawable/Drawable;

    move-result-object p1

    check-cast p1, Luf1;

    return-object p1
.end method

.class public abstract Lz70;
.super La80;
.source "SourceFile"

# interfaces
.implements Lrx0;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, La80;-><init>()V

    return-void
.end method


# virtual methods
.method public containsEntry(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1

    .line 1
    invoke-virtual {p0}, Lz70;->delegate()Lrx0;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lrx0;->containsEntry(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public containsKey(Ljava/lang/Object;)Z
    .locals 1

    .line 1
    invoke-virtual {p0}, Lz70;->delegate()Lrx0;

    move-result-object v0

    invoke-interface {v0, p1}, Lrx0;->containsKey(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public containsValue(Ljava/lang/Object;)Z
    .locals 1

    .line 1
    invoke-virtual {p0}, Lz70;->delegate()Lrx0;

    move-result-object v0

    invoke-interface {v0, p1}, Lrx0;->containsValue(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public abstract delegate()Lrx0;
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    .line 1
    if-eq p1, p0, :cond_1

    invoke-virtual {p0}, Lz70;->delegate()Lrx0;

    move-result-object v0

    invoke-interface {v0, p1}, Lrx0;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method

.method public hashCode()I
    .locals 1

    .line 1
    invoke-virtual {p0}, Lz70;->delegate()Lrx0;

    move-result-object v0

    invoke-interface {v0}, Lrx0;->hashCode()I

    move-result v0

    return v0
.end method

.method public isEmpty()Z
    .locals 1

    .line 1
    invoke-virtual {p0}, Lz70;->delegate()Lrx0;

    move-result-object v0

    invoke-interface {v0}, Lrx0;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public size()I
    .locals 1

    .line 1
    invoke-virtual {p0}, Lz70;->delegate()Lrx0;

    move-result-object v0

    invoke-interface {v0}, Lrx0;->size()I

    move-result v0

    return v0
.end method

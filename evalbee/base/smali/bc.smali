.class public abstract Lbc;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(Landroid/graphics/Bitmap;)[B
    .locals 3

    .line 1
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    sget-object v1, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v2, 0x64

    invoke-virtual {p0, v1, v2, v0}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object p0

    return-object p0
.end method

.method public static b(Ljava/lang/String;)Lcom/ekodroid/omrevaluator/templateui/models/DecimalOptionPayload;
    .locals 2

    .line 1
    if-nez p0, :cond_0

    new-instance p0, Lcom/ekodroid/omrevaluator/templateui/models/DecimalOptionPayload;

    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-direct {p0, v1, v0, v1}, Lcom/ekodroid/omrevaluator/templateui/models/DecimalOptionPayload;-><init>(ZIZ)V

    return-object p0

    :cond_0
    new-instance v0, Lgc0;

    invoke-direct {v0}, Lgc0;-><init>()V

    const-class v1, Lcom/ekodroid/omrevaluator/templateui/models/DecimalOptionPayload;

    invoke-virtual {v0, p0, v1}, Lgc0;->j(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/ekodroid/omrevaluator/templateui/models/DecimalOptionPayload;

    return-object p0
.end method

.method public static c(Ljava/lang/String;)Lcom/ekodroid/omrevaluator/templateui/models/MatrixOptionPayload;
    .locals 2

    .line 1
    if-nez p0, :cond_0

    new-instance p0, Lcom/ekodroid/omrevaluator/templateui/models/MatrixOptionPayload;

    const/4 v0, 0x4

    const/4 v1, 0x5

    invoke-direct {p0, v0, v1}, Lcom/ekodroid/omrevaluator/templateui/models/MatrixOptionPayload;-><init>(II)V

    return-object p0

    :cond_0
    new-instance v0, Lgc0;

    invoke-direct {v0}, Lgc0;-><init>()V

    const-class v1, Lcom/ekodroid/omrevaluator/templateui/models/MatrixOptionPayload;

    invoke-virtual {v0, p0, v1}, Lgc0;->j(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/ekodroid/omrevaluator/templateui/models/MatrixOptionPayload;

    return-object p0
.end method

.method public static d(Ljava/lang/String;)Lcom/ekodroid/omrevaluator/templateui/models/NumericalOptionPayload;
    .locals 2

    .line 1
    if-nez p0, :cond_0

    new-instance p0, Lcom/ekodroid/omrevaluator/templateui/models/NumericalOptionPayload;

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-direct {p0, v1, v0, v1}, Lcom/ekodroid/omrevaluator/templateui/models/NumericalOptionPayload;-><init>(ZII)V

    return-object p0

    :cond_0
    new-instance v0, Lgc0;

    invoke-direct {v0}, Lgc0;-><init>()V

    const-class v1, Lcom/ekodroid/omrevaluator/templateui/models/NumericalOptionPayload;

    invoke-virtual {v0, p0, v1}, Lgc0;->j(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/ekodroid/omrevaluator/templateui/models/NumericalOptionPayload;

    return-object p0
.end method

.method public static e(Landroid/graphics/Bitmap;F)Landroid/graphics/Bitmap;
    .locals 7

    .line 1
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    invoke-virtual {v5, p1, p1}, Landroid/graphics/Matrix;->postScale(FF)Z

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v6, 0x0

    move-object v0, p0

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object p1

    invoke-virtual {p0}, Landroid/graphics/Bitmap;->recycle()V

    return-object p1
.end method

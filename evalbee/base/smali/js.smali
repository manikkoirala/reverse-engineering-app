.class public Ljs;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public a:Ljq1;

.field public b:Lcom/google/android/gms/tasks/TaskCompletionSource;

.field public c:Lmz;


# direct methods
.method public constructor <init>(Ljq1;Lcom/google/android/gms/tasks/TaskCompletionSource;)V
    .locals 6

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/gms/common/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2}, Lcom/google/android/gms/common/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    iput-object p1, p0, Ljs;->a:Ljq1;

    iput-object p2, p0, Ljs;->b:Lcom/google/android/gms/tasks/TaskCompletionSource;

    invoke-virtual {p1}, Ljq1;->j()Lo30;

    move-result-object p1

    new-instance p2, Lmz;

    invoke-virtual {p1}, Lo30;->a()Lr10;

    move-result-object v0

    invoke-virtual {v0}, Lr10;->l()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p1}, Lo30;->c()Lzf0;

    move-result-object v2

    invoke-virtual {p1}, Lo30;->b()Ldg0;

    move-result-object v3

    invoke-virtual {p1}, Lo30;->l()J

    move-result-wide v4

    move-object v0, p2

    invoke-direct/range {v0 .. v5}, Lmz;-><init>(Landroid/content/Context;Lzf0;Ldg0;J)V

    iput-object p2, p0, Ljs;->c:Lmz;

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .line 1
    new-instance v0, Lis;

    iget-object v1, p0, Ljs;->a:Ljq1;

    invoke-virtual {v1}, Ljq1;->k()Lkq1;

    move-result-object v1

    iget-object v2, p0, Ljs;->a:Ljq1;

    invoke-virtual {v2}, Ljq1;->e()Lr10;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lis;-><init>(Lkq1;Lr10;)V

    iget-object v1, p0, Ljs;->c:Lmz;

    invoke-virtual {v1, v0}, Lmz;->d(Lxy0;)V

    iget-object v1, p0, Ljs;->b:Lcom/google/android/gms/tasks/TaskCompletionSource;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lxy0;->a(Lcom/google/android/gms/tasks/TaskCompletionSource;Ljava/lang/Object;)V

    return-void
.end method

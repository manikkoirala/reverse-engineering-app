.class public Lea1$d;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lea1;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "d"
.end annotation


# instance fields
.field public final synthetic a:Lea1;


# direct methods
.method public constructor <init>(Lea1;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lea1$d;->a:Lea1;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lea1;Lea1$a;)V
    .locals 0

    .line 2
    invoke-direct {p0, p1}, Lea1$d;-><init>(Lea1;)V

    return-void
.end method


# virtual methods
.method public varargs a([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 8

    .line 1
    iget-object p1, p0, Lea1$d;->a:Lea1;

    iget-object v0, p1, Lea1;->d:[Ljava/lang/String;

    array-length v0, v0

    new-array v0, v0, [Landroid/widget/EditText;

    iput-object v0, p1, Lea1;->e:[Landroid/widget/EditText;

    new-instance p1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v0, -0x2

    invoke-direct {p1, v0, v0}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    iget-object v1, p0, Lea1$d;->a:Lea1;

    iget-object v1, v1, Lea1;->a:Landroid/content/Context;

    const/16 v2, 0x10

    invoke-static {v2, v1}, La91;->d(ILandroid/content/Context;)I

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {p1, v2, v2, v1, v2}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    iget-object v3, p0, Lea1$d;->a:Lea1;

    iget-object v3, v3, Lea1;->a:Landroid/content/Context;

    const/16 v4, 0x28

    invoke-static {v4, v3}, La91;->d(ILandroid/content/Context;)I

    move-result v3

    invoke-direct {v1, v0, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    iget-object v0, p0, Lea1$d;->a:Lea1;

    iget-object v3, v0, Lea1;->d:[Ljava/lang/String;

    array-length v3, v3

    new-array v3, v3, [Landroid/widget/LinearLayout;

    iput-object v3, v0, Lea1;->g:[Landroid/widget/LinearLayout;

    :goto_0
    iget-object v0, p0, Lea1$d;->a:Lea1;

    iget-object v3, v0, Lea1;->e:[Landroid/widget/EditText;

    array-length v3, v3

    if-ge v2, v3, :cond_0

    iget-object v3, v0, Lea1;->g:[Landroid/widget/LinearLayout;

    iget-object v0, v0, Lea1;->a:Landroid/content/Context;

    invoke-static {v0}, Lyo;->b(Landroid/content/Context;)Landroid/widget/LinearLayout;

    move-result-object v0

    aput-object v0, v3, v2

    iget-object v0, p0, Lea1$d;->a:Lea1;

    iget-object v3, v0, Lea1;->a:Landroid/content/Context;

    iget-object v0, v0, Lea1;->d:[Ljava/lang/String;

    aget-object v0, v0, v2

    const/16 v4, 0x32

    invoke-static {v3, v0, v4}, Lyo;->c(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/TextView;

    move-result-object v0

    iget-object v3, p0, Lea1$d;->a:Lea1;

    iget-object v4, v3, Lea1;->e:[Landroid/widget/EditText;

    iget-object v5, v3, Lea1;->a:Landroid/content/Context;

    iget-object v6, v3, Lea1;->d:[Ljava/lang/String;

    aget-object v6, v6, v2

    iget v3, v3, Lea1;->f:I

    const/16 v7, 0x50

    invoke-static {v5, v6, v3, v7}, Lyo;->a(Landroid/content/Context;Ljava/lang/String;II)Landroid/widget/EditText;

    move-result-object v3

    aput-object v3, v4, v2

    iget-object v3, p0, Lea1$d;->a:Lea1;

    iget-object v3, v3, Lea1;->g:[Landroid/widget/LinearLayout;

    aget-object v3, v3, v2

    invoke-virtual {v3, v0, p1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lea1$d;->a:Lea1;

    iget-object v3, v0, Lea1;->g:[Landroid/widget/LinearLayout;

    aget-object v3, v3, v2

    iget-object v0, v0, Lea1;->e:[Landroid/widget/EditText;

    aget-object v0, v0, v2

    invoke-virtual {v3, v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    return-object p1
.end method

.method public b(Ljava/lang/Void;)V
    .locals 4

    .line 1
    iget-object p1, p0, Lea1$d;->a:Lea1;

    iget-object p1, p1, Lea1;->h:Landroid/widget/LinearLayout;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->removeAllViews()V

    iget-object p1, p0, Lea1$d;->a:Lea1;

    iget-object p1, p1, Lea1;->g:[Landroid/widget/LinearLayout;

    array-length v0, p1

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    aget-object v2, p1, v1

    iget-object v3, p0, Lea1$d;->a:Lea1;

    iget-object v3, v3, Lea1;->h:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lea1$d;->a:Lea1;

    iget-object p1, p1, Lea1;->i:Landroid/widget/ProgressBar;

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 1
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lea1$d;->a([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 1
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lea1$d;->b(Ljava/lang/Void;)V

    return-void
.end method

.method public onPreExecute()V
    .locals 2

    .line 1
    iget-object v0, p0, Lea1$d;->a:Lea1;

    iget-object v0, v0, Lea1;->i:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

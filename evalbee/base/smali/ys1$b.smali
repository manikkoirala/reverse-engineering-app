.class public Lys1$b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/android/volley/d$b;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lys1;->d(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Lys1;


# direct methods
.method public constructor <init>(Lys1;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lys1$b;->a:Lys1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .line 1
    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1}, Lys1$b;->b(Ljava/lang/String;)V

    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 5

    .line 1
    const/4 p1, 0x0

    :try_start_0
    iget-object v0, p0, Lys1$b;->a:Lys1;

    iget-object v0, v0, Lys1;->c:Landroid/content/Context;

    invoke-static {v0}, Lcom/ekodroid/omrevaluator/database/repositories/ClassRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/ClassRepository;

    move-result-object v0

    iget-object v1, p0, Lys1$b;->a:Lys1;

    invoke-static {v1}, Lys1;->c(Lys1;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    const/4 v3, 0x1

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/ekodroid/omrevaluator/students/clients/models/ClassDto;

    new-instance v4, Lcom/ekodroid/omrevaluator/database/ClassDataModel;

    iget-object v2, v2, Lcom/ekodroid/omrevaluator/students/clients/models/ClassDto;->className:Ljava/lang/String;

    invoke-direct {v4, v2, p1, v3}, Lcom/ekodroid/omrevaluator/database/ClassDataModel;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-virtual {v0, v4}, Lcom/ekodroid/omrevaluator/database/repositories/ClassRepository;->saveOrUpdateClass(Lcom/ekodroid/omrevaluator/database/ClassDataModel;)Z

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lys1$b;->a:Lys1;

    invoke-static {v0}, Lys1;->c(Lys1;)Ljava/util/ArrayList;

    move-result-object v1

    const/16 v2, 0xc8

    invoke-static {v0, v3, v2, v1}, Lys1;->b(Lys1;ZILjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    iget-object v0, p0, Lys1$b;->a:Lys1;

    const/4 v1, 0x0

    const/16 v2, 0x190

    invoke-static {v0, v1, v2, p1}, Lys1;->b(Lys1;ZILjava/lang/Object;)V

    :goto_1
    return-void
.end method

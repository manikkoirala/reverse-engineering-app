.class public Lv3;
.super Lu80;
.source "SourceFile"


# instance fields
.field public a:Landroid/content/Context;

.field public b:Lk5$f;

.field public c:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;

.field public d:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;

.field public e:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

.field public f:Landroid/widget/ArrayAdapter;

.field public g:[Ljava/lang/Double;

.field public h:[Ljava/lang/Double;

.field public i:Lk5;

.field public j:Ljava/util/ArrayList;

.field public k:Landroid/widget/LinearLayout;

.field public l:Landroid/widget/Button;

.field public m:Ly01;

.field public n:Landroid/view/View$OnClickListener;

.field public p:Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lk5$f;Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;Ly01;)V
    .locals 21

    .line 1
    move-object/from16 v0, p0

    invoke-direct/range {p0 .. p1}, Lu80;-><init>(Landroid/content/Context;)V

    const/16 v1, 0x1f

    new-array v1, v1, [Ljava/lang/Double;

    const-wide/high16 v2, 0x3fd0000000000000L    # 0.25

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const-wide v4, 0x3fd51eb851eb851fL    # 0.33

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    const/4 v4, 0x1

    aput-object v2, v1, v4

    const-wide/high16 v5, 0x3fe0000000000000L    # 0.5

    invoke-static {v5, v6}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    const/4 v5, 0x2

    aput-object v2, v1, v5

    const-wide v6, 0x3fe570a3d70a3d71L    # 0.67

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    const/4 v6, 0x3

    aput-object v2, v1, v6

    const-wide/high16 v7, 0x3fe8000000000000L    # 0.75

    invoke-static {v7, v8}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    const/4 v7, 0x4

    aput-object v2, v1, v7

    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    invoke-static {v8, v9}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    const/4 v8, 0x5

    aput-object v2, v1, v8

    const-wide/high16 v9, 0x3ff4000000000000L    # 1.25

    invoke-static {v9, v10}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    const/4 v9, 0x6

    aput-object v2, v1, v9

    const-wide/high16 v10, 0x3ff8000000000000L    # 1.5

    invoke-static {v10, v11}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    const/4 v10, 0x7

    aput-object v2, v1, v10

    const-wide/high16 v11, 0x3ffc000000000000L    # 1.75

    invoke-static {v11, v12}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    const/16 v11, 0x8

    aput-object v2, v1, v11

    const-wide/high16 v12, 0x4000000000000000L    # 2.0

    invoke-static {v12, v13}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    const/16 v12, 0x9

    aput-object v2, v1, v12

    const-wide/high16 v13, 0x4004000000000000L    # 2.5

    invoke-static {v13, v14}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    const/16 v13, 0xa

    aput-object v2, v1, v13

    const-wide/high16 v14, 0x4008000000000000L    # 3.0

    invoke-static {v14, v15}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    const/16 v14, 0xb

    aput-object v2, v1, v14

    const-wide/high16 v15, 0x400c000000000000L    # 3.5

    invoke-static/range {v15 .. v16}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    const/16 v15, 0xc

    aput-object v2, v1, v15

    const-wide/high16 v16, 0x4010000000000000L    # 4.0

    invoke-static/range {v16 .. v17}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    const/16 v16, 0xd

    aput-object v2, v1, v16

    const-wide/high16 v17, 0x4012000000000000L    # 4.5

    invoke-static/range {v17 .. v18}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    const/16 v17, 0xe

    aput-object v2, v1, v17

    const-wide/high16 v18, 0x4014000000000000L    # 5.0

    invoke-static/range {v18 .. v19}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    const/16 v15, 0xf

    aput-object v2, v1, v15

    const-wide/high16 v19, 0x4018000000000000L    # 6.0

    invoke-static/range {v19 .. v20}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    const/16 v19, 0x10

    aput-object v2, v1, v19

    const-wide/high16 v19, 0x401c000000000000L    # 7.0

    invoke-static/range {v19 .. v20}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    const/16 v19, 0x11

    aput-object v2, v1, v19

    const-wide/high16 v19, 0x4020000000000000L    # 8.0

    invoke-static/range {v19 .. v20}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    const/16 v19, 0x12

    aput-object v2, v1, v19

    const-wide/high16 v19, 0x4022000000000000L    # 9.0

    invoke-static/range {v19 .. v20}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    const/16 v19, 0x13

    aput-object v2, v1, v19

    const-wide/high16 v19, 0x4024000000000000L    # 10.0

    invoke-static/range {v19 .. v20}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    const/16 v19, 0x14

    aput-object v2, v1, v19

    const-wide/high16 v19, 0x4026000000000000L    # 11.0

    invoke-static/range {v19 .. v20}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    const/16 v19, 0x15

    aput-object v2, v1, v19

    const-wide/high16 v19, 0x4028000000000000L    # 12.0

    invoke-static/range {v19 .. v20}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    const/16 v19, 0x16

    aput-object v2, v1, v19

    const-wide/high16 v19, 0x402a000000000000L    # 13.0

    invoke-static/range {v19 .. v20}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    const/16 v19, 0x17

    aput-object v2, v1, v19

    const-wide/high16 v19, 0x402c000000000000L    # 14.0

    invoke-static/range {v19 .. v20}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    const/16 v19, 0x18

    aput-object v2, v1, v19

    const-wide/high16 v19, 0x402e000000000000L    # 15.0

    invoke-static/range {v19 .. v20}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    const/16 v19, 0x19

    aput-object v2, v1, v19

    const-wide/high16 v19, 0x4030000000000000L    # 16.0

    invoke-static/range {v19 .. v20}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    const/16 v19, 0x1a

    aput-object v2, v1, v19

    const-wide/high16 v19, 0x4031000000000000L    # 17.0

    invoke-static/range {v19 .. v20}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    const/16 v19, 0x1b

    aput-object v2, v1, v19

    const-wide/high16 v19, 0x4032000000000000L    # 18.0

    invoke-static/range {v19 .. v20}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    const/16 v19, 0x1c

    aput-object v2, v1, v19

    const-wide/high16 v19, 0x4033000000000000L    # 19.0

    invoke-static/range {v19 .. v20}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    const/16 v19, 0x1d

    aput-object v2, v1, v19

    const-wide/high16 v19, 0x4034000000000000L    # 20.0

    invoke-static/range {v19 .. v20}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    const/16 v19, 0x1e

    aput-object v2, v1, v19

    iput-object v1, v0, Lv3;->g:[Ljava/lang/Double;

    new-array v1, v15, [Ljava/lang/Double;

    const-wide/high16 v19, -0x3fec000000000000L    # -5.0

    invoke-static/range {v19 .. v20}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    aput-object v2, v1, v3

    const-wide/high16 v2, -0x3ff0000000000000L    # -4.0

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    aput-object v2, v1, v4

    const-wide/high16 v2, -0x3ff8000000000000L    # -3.0

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    aput-object v2, v1, v5

    const-wide/high16 v2, -0x3ffc000000000000L    # -2.5

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    aput-object v2, v1, v6

    const-wide/high16 v2, -0x4000000000000000L    # -2.0

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    aput-object v2, v1, v7

    const-wide v2, -0x400570a3d70a3d71L    # -1.66

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    aput-object v2, v1, v8

    const-wide/high16 v2, -0x4008000000000000L    # -1.5

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    aput-object v2, v1, v9

    const-wide v2, -0x400ab851eb851eb8L    # -1.33

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    aput-object v2, v1, v10

    const-wide/high16 v2, -0x4010000000000000L    # -1.0

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    aput-object v2, v1, v11

    const-wide v2, -0x401570a3d70a3d71L    # -0.83

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    aput-object v2, v1, v12

    const-wide v2, -0x401ae147ae147ae1L    # -0.66

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    aput-object v2, v1, v13

    const-wide/high16 v2, -0x4020000000000000L    # -0.5

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    aput-object v2, v1, v14

    const-wide v2, -0x402ae147ae147ae1L    # -0.33

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    const/16 v3, 0xc

    aput-object v2, v1, v3

    const-wide/high16 v2, -0x4030000000000000L    # -0.25

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    aput-object v2, v1, v16

    const-wide/16 v2, 0x0

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    aput-object v2, v1, v17

    iput-object v1, v0, Lv3;->h:[Ljava/lang/Double;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, Lv3;->j:Ljava/util/ArrayList;

    new-instance v1, Lv3$a;

    invoke-direct {v1, v0}, Lv3$a;-><init>(Lv3;)V

    iput-object v1, v0, Lv3;->n:Landroid/view/View$OnClickListener;

    move-object/from16 v1, p1

    iput-object v1, v0, Lv3;->a:Landroid/content/Context;

    move-object/from16 v1, p2

    iput-object v1, v0, Lv3;->b:Lk5$f;

    move-object/from16 v1, p3

    iput-object v1, v0, Lv3;->c:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;

    move-object/from16 v1, p4

    iput-object v1, v0, Lv3;->d:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;

    move-object/from16 v2, p5

    iput-object v2, v0, Lv3;->e:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    move-object/from16 v2, p6

    iput-object v2, v0, Lv3;->m:Ly01;

    invoke-virtual/range {p4 .. p4}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;->getPartialAnswerOptionKeys()Ljava/util/ArrayList;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual/range {p4 .. p4}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;->getPartialAnswerOptionKeys()Ljava/util/ArrayList;

    move-result-object v1

    iput-object v1, v0, Lv3;->j:Ljava/util/ArrayList;

    :cond_0
    return-void
.end method

.method public static synthetic a(Lv3;Ljava/util/ArrayList;Landroid/widget/LinearLayout;)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1, p2}, Lv3;->d(Ljava/util/ArrayList;Landroid/widget/LinearLayout;)V

    return-void
.end method


# virtual methods
.method public final b(Ljava/lang/String;[Ljava/lang/Double;)Ljava/util/ArrayList;
    .locals 2

    .line 1
    invoke-static {p1}, La91;->h(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object p1

    invoke-static {p2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    invoke-static {p1}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    new-instance p2, Ljava/util/ArrayList;

    invoke-direct {p2}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Double;

    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-object p2
.end method

.method public final c(Lcom/ekodroid/omrevaluator/templateui/models/PartialAnswerOptionKey;Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;)Ljava/lang/String;
    .locals 2

    .line 1
    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/PartialAnswerOptionKey;->getPartialType()Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/PartialAnswerOptionKey;->getPartialType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " - "

    goto :goto_0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "Exact - "

    :goto_0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, p2}, Le5;->t(Lcom/ekodroid/omrevaluator/templateui/models/PartialAnswerOptionKey;Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;)Ljava/lang/String;

    move-result-object p2

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, " : "

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/PartialAnswerOptionKey;->getPartialMarks()D

    move-result-wide p1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public final d(Ljava/util/ArrayList;Landroid/widget/LinearLayout;)V
    .locals 10

    .line 1
    if-eqz p1, :cond_0

    invoke-virtual {p2}, Landroid/view/ViewGroup;->removeAllViews()V

    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, -0x2

    invoke-direct {v0, v1, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    const/16 v1, 0x10

    iget-object v2, p0, Lv3;->a:Landroid/content/Context;

    invoke-static {v1, v2}, La91;->d(ILandroid/content/Context;)I

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, v2, v2}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    const/high16 v1, 0x3f800000    # 1.0f

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    iget-object v3, p0, Lv3;->a:Landroid/content/Context;

    const/16 v4, 0x20

    invoke-static {v4, v3}, La91;->d(ILandroid/content/Context;)I

    move-result v3

    iget-object v5, p0, Lv3;->a:Landroid/content/Context;

    invoke-static {v4, v5}, La91;->d(ILandroid/content/Context;)I

    move-result v4

    invoke-direct {v1, v3, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    const/16 v3, 0xc

    iget-object v4, p0, Lv3;->a:Landroid/content/Context;

    invoke-static {v3, v4}, La91;->d(ILandroid/content/Context;)I

    move-result v3

    invoke-virtual {v1, v2, v2, v3, v2}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    :goto_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v2, v3, :cond_0

    iget-object v3, p0, Lv3;->a:Landroid/content/Context;

    invoke-static {v3}, Lyo;->b(Landroid/content/Context;)Landroid/widget/LinearLayout;

    move-result-object v3

    iget-object v4, p0, Lv3;->a:Landroid/content/Context;

    const v5, 0x7f080080

    invoke-static {v4, v5}, Lg7;->b(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    iget-object v4, p0, Lv3;->a:Landroid/content/Context;

    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/ekodroid/omrevaluator/templateui/models/PartialAnswerOptionKey;

    iget-object v6, p0, Lv3;->e:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    invoke-virtual {p0, v5, v6}, Lv3;->c(Lcom/ekodroid/omrevaluator/templateui/models/PartialAnswerOptionKey;Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;)Ljava/lang/String;

    move-result-object v5

    const/16 v6, 0x96

    invoke-static {v4, v5, v6}, Lyo;->c(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/TextView;

    move-result-object v4

    invoke-virtual {v3, v4, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v4, Lpo;

    iget-object v5, p0, Lv3;->a:Landroid/content/Context;

    invoke-direct {v4, v5}, Lpo;-><init>(Landroid/content/Context;)V

    invoke-virtual {v4, v2}, Lpo;->setIndex(I)V

    iget-object v5, p0, Lv3;->a:Landroid/content/Context;

    const v6, 0x7f08008e

    invoke-static {v5, v6}, Lg7;->b(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    iget-object v5, p0, Lv3;->n:Landroid/view/View$OnClickListener;

    invoke-virtual {v4, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v5, p0, Lv3;->a:Landroid/content/Context;

    const/4 v6, 0x5

    invoke-static {v6, v5}, La91;->d(ILandroid/content/Context;)I

    move-result v5

    iget-object v7, p0, Lv3;->a:Landroid/content/Context;

    invoke-static {v6, v7}, La91;->d(ILandroid/content/Context;)I

    move-result v7

    iget-object v8, p0, Lv3;->a:Landroid/content/Context;

    invoke-static {v6, v8}, La91;->d(ILandroid/content/Context;)I

    move-result v8

    iget-object v9, p0, Lv3;->a:Landroid/content/Context;

    invoke-static {v6, v9}, La91;->d(ILandroid/content/Context;)I

    move-result v6

    invoke-virtual {v4, v5, v7, v8, v6}, Landroid/view/View;->setPadding(IIII)V

    invoke-virtual {v3, v4, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {p2, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final e()V
    .locals 2

    .line 1
    const v0, 0x7f090448

    invoke-virtual {p0, v0}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/appcompat/widget/Toolbar;

    new-instance v1, Lv3$e;

    invoke-direct {v1, p0}, Lv3$e;-><init>(Lv3;)V

    invoke-virtual {v0, v1}, Landroidx/appcompat/widget/Toolbar;->setNavigationOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 11

    .line 1
    invoke-super {p0, p1}, Lu80;->onCreate(Landroid/os/Bundle;)V

    const p1, 0x7f0c0060

    invoke-virtual {p0, p1}, Landroid/app/Dialog;->setContentView(I)V

    invoke-virtual {p0}, Lv3;->e()V

    iget-object p1, p0, Lv3;->a:Landroid/content/Context;

    const-string v0, "MyPref"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object p1

    iput-object p1, p0, Lv3;->p:Landroid/content/SharedPreferences;

    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lv3;->p:Landroid/content/SharedPreferences;

    const-string v2, "minus_mark_json"

    const-string v3, ""

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lv3;->h:[Ljava/lang/Double;

    invoke-virtual {p0, v0, v2}, Lv3;->b(Ljava/lang/String;[Ljava/lang/Double;)Ljava/util/ArrayList;

    move-result-object v0

    move v2, v1

    :goto_0
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v2, v4, :cond_1

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Double;

    invoke-virtual {v4}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    iget-object v6, p0, Lv3;->c:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;

    iget-wide v6, v6, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->incorrectMarks:D

    cmpl-double v4, v4, v6

    if-ltz v4, :cond_0

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Double;

    invoke-virtual {p1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lv3;->p:Landroid/content/SharedPreferences;

    const-string v2, "plus_mark_json"

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lv3;->g:[Ljava/lang/Double;

    invoke-virtual {p0, v0, v2}, Lv3;->b(Ljava/lang/String;[Ljava/lang/Double;)Ljava/util/ArrayList;

    move-result-object v0

    move v2, v1

    :goto_1
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v2, v3, :cond_3

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Double;

    invoke-virtual {v3}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v3

    iget-object v5, p0, Lv3;->c:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;

    iget-wide v5, v5, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->correctMarks:D

    cmpg-double v3, v3, v5

    if-gtz v3, :cond_2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Double;

    invoke-virtual {p1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_3
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget-object v2, p0, Lv3;->c:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;

    iget-object v2, v2, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->type:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    sget-object v3, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;->DECIMAL:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    if-ne v2, v3, :cond_4

    sget-object v2, Lok;->N:Ljava/lang/String;

    :goto_2
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_4
    sget-object v2, Lok;->M:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v2, Lok;->O:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v2, p0, Lv3;->c:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;

    iget-object v2, v2, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->type:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    sget-object v3, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;->MATRIX:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    if-ne v2, v3, :cond_5

    sget-object v2, Lok;->P:Ljava/lang/String;

    goto :goto_2

    :cond_5
    :goto_3
    new-instance v2, Landroid/widget/ArrayAdapter;

    iget-object v3, p0, Lv3;->a:Landroid/content/Context;

    const v4, 0x7f0c00f0

    invoke-direct {v2, v3, v4, p1}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    iput-object v2, p0, Lv3;->f:Landroid/widget/ArrayAdapter;

    new-instance p1, Landroid/widget/ArrayAdapter;

    iget-object v2, p0, Lv3;->a:Landroid/content/Context;

    invoke-direct {p1, v2, v4, v0}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    const v0, 0x7f090342

    invoke-virtual {p0, v0}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    const v2, 0x7f090343

    invoke-virtual {p0, v2}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Spinner;

    invoke-virtual {v2, p1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    const p1, 0x7f0903cb

    invoke-virtual {p0, p1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    const v3, 0x7f09037b

    invoke-virtual {p0, v3}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TableLayout;

    const v4, 0x7f09020a

    invoke-virtual {p0, v4}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    iput-object v4, p0, Lv3;->k:Landroid/widget/LinearLayout;

    iget-object v5, p0, Lv3;->j:Ljava/util/ArrayList;

    invoke-virtual {p0, v5, v4}, Lv3;->d(Ljava/util/ArrayList;Landroid/widget/LinearLayout;)V

    const v4, 0x7f090093

    invoke-virtual {p0, v4}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Button;

    iput-object v4, p0, Lv3;->l:Landroid/widget/Button;

    invoke-virtual {v4, v1}, Landroid/view/View;->setEnabled(Z)V

    iget-object v1, p0, Lv3;->f:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    iget-object v1, p0, Lv3;->c:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;

    iget-object v1, v1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->type:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    sget-object v4, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;->NUMARICAL:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    if-ne v1, v4, :cond_6

    const v1, 0x7f09021e

    invoke-virtual {p0, v1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/16 v4, 0x8

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    :cond_6
    new-instance v1, Lk5;

    iget-object v6, p0, Lv3;->a:Landroid/content/Context;

    iget-object v7, p0, Lv3;->d:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;

    const/4 v8, 0x0

    const/4 v9, 0x0

    iget-object v10, p0, Lv3;->e:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    move-object v5, v1

    invoke-direct/range {v5 .. v10}, Lk5;-><init>(Landroid/content/Context;Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;Lk5$f;Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;)V

    iput-object v1, p0, Lv3;->i:Lk5;

    invoke-virtual {v1}, Lk5;->p()V

    iget-object v1, p0, Lv3;->i:Lk5;

    invoke-virtual {v1}, Lk5;->m()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    invoke-virtual {v3, v4}, Landroid/widget/TableLayout;->addView(Landroid/view/View;)V

    goto :goto_4

    :cond_7
    const v1, 0x7f090097

    invoke-virtual {p0, v1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    new-instance v3, Lv3$b;

    invoke-direct {v3, p0, p1, v2, v0}, Lv3$b;-><init>(Lv3;Landroid/widget/TextView;Landroid/widget/Spinner;Landroid/widget/Spinner;)V

    invoke-virtual {v1, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const p1, 0x7f090087

    invoke-virtual {p0, p1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object p1

    new-instance v0, Lv3$c;

    invoke-direct {v0, p0}, Lv3$c;-><init>(Lv3;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object p1, p0, Lv3;->l:Landroid/widget/Button;

    new-instance v0, Lv3$d;

    invoke-direct {v0, p0}, Lv3$d;-><init>(Lv3;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

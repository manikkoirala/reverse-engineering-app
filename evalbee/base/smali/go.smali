.class public Lgo;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lgo$b;
    }
.end annotation


# instance fields
.field public a:Landroid/content/Context;

.field public b:Ljava/util/ArrayList;

.field public c:Ly01;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/ArrayList;Ly01;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lgo;->a:Landroid/content/Context;

    iput-object p2, p0, Lgo;->b:Ljava/util/ArrayList;

    iput-object p3, p0, Lgo;->c:Ly01;

    new-instance p1, Lgo$b;

    const/4 p2, 0x0

    invoke-direct {p1, p0, p2}, Lgo$b;-><init>(Lgo;Lgo$a;)V

    const/4 p2, 0x0

    new-array p2, p2, [Ljava/lang/Void;

    invoke-virtual {p1, p2}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method public static synthetic a(Lgo;)Landroid/content/Context;
    .locals 0

    .line 1
    iget-object p0, p0, Lgo;->a:Landroid/content/Context;

    return-object p0
.end method

.method public static synthetic b(Lgo;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lgo;->c()V

    return-void
.end method


# virtual methods
.method public final c()V
    .locals 11

    .line 1
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lgo;->a:Landroid/content/Context;

    sget-object v2, Lok;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/content/Context;->getExternalFilesDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    const-string v2, "student_export.csv"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    new-instance v4, Lgo$a;

    invoke-direct {v4, p0, v0}, Lgo$a;-><init>(Lgo;Ljava/io/File;)V

    iget-object v3, p0, Lgo;->a:Landroid/content/Context;

    const v5, 0x7f1200e8

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lgo;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f1200e7

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v1, Lok;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "/student_export.csv"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const v7, 0x7f1202e8

    const v8, 0x7f120060

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-static/range {v3 .. v10}, Lxs;->d(Landroid/content/Context;Ly01;ILjava/lang/String;IIII)V

    return-void
.end method

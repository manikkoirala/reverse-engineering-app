.class public final Lyo1;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lyo1$c;,
        Lyo1$d;
    }
.end annotation


# instance fields
.field public final a:Lgg;

.field public final b:Z

.field public final c:Lyo1$d;

.field public final d:I


# direct methods
.method public constructor <init>(Lyo1$d;)V
    .locals 3

    .line 1
    invoke-static {}, Lgg;->i()Lgg;

    move-result-object v0

    const v1, 0x7fffffff

    const/4 v2, 0x0

    invoke-direct {p0, p1, v2, v0, v1}, Lyo1;-><init>(Lyo1$d;ZLgg;I)V

    return-void
.end method

.method public constructor <init>(Lyo1$d;ZLgg;I)V
    .locals 0

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lyo1;->c:Lyo1$d;

    iput-boolean p2, p0, Lyo1;->b:Z

    iput-object p3, p0, Lyo1;->a:Lgg;

    iput p4, p0, Lyo1;->d:I

    return-void
.end method

.method public static synthetic a(Lyo1;Ljava/lang/CharSequence;)Ljava/util/Iterator;
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lyo1;->h(Ljava/lang/CharSequence;)Ljava/util/Iterator;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic b(Lyo1;)Lgg;
    .locals 0

    .line 1
    iget-object p0, p0, Lyo1;->a:Lgg;

    return-object p0
.end method

.method public static synthetic c(Lyo1;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lyo1;->b:Z

    return p0
.end method

.method public static synthetic d(Lyo1;)I
    .locals 0

    .line 1
    iget p0, p0, Lyo1;->d:I

    return p0
.end method

.method public static e(C)Lyo1;
    .locals 0

    .line 1
    invoke-static {p0}, Lgg;->f(C)Lgg;

    move-result-object p0

    invoke-static {p0}, Lyo1;->f(Lgg;)Lyo1;

    move-result-object p0

    return-object p0
.end method

.method public static f(Lgg;)Lyo1;
    .locals 2

    .line 1
    invoke-static {p0}, Li71;->r(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lyo1;

    new-instance v1, Lyo1$a;

    invoke-direct {v1, p0}, Lyo1$a;-><init>(Lgg;)V

    invoke-direct {v0, v1}, Lyo1;-><init>(Lyo1$d;)V

    return-object v0
.end method


# virtual methods
.method public g(Ljava/lang/CharSequence;)Ljava/lang/Iterable;
    .locals 1

    .line 1
    invoke-static {p1}, Li71;->r(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lyo1$b;

    invoke-direct {v0, p0, p1}, Lyo1$b;-><init>(Lyo1;Ljava/lang/CharSequence;)V

    return-object v0
.end method

.method public final h(Ljava/lang/CharSequence;)Ljava/util/Iterator;
    .locals 1

    .line 1
    iget-object v0, p0, Lyo1;->c:Lyo1$d;

    invoke-interface {v0, p0, p1}, Lyo1$d;->a(Lyo1;Ljava/lang/CharSequence;)Ljava/util/Iterator;

    move-result-object p1

    return-object p1
.end method

.method public i()Lyo1;
    .locals 1

    .line 1
    invoke-static {}, Lgg;->k()Lgg;

    move-result-object v0

    invoke-virtual {p0, v0}, Lyo1;->j(Lgg;)Lyo1;

    move-result-object v0

    return-object v0
.end method

.method public j(Lgg;)Lyo1;
    .locals 4

    .line 1
    invoke-static {p1}, Li71;->r(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lyo1;

    iget-object v1, p0, Lyo1;->c:Lyo1$d;

    iget-boolean v2, p0, Lyo1;->b:Z

    iget v3, p0, Lyo1;->d:I

    invoke-direct {v0, v1, v2, p1, v3}, Lyo1;-><init>(Lyo1$d;ZLgg;I)V

    return-object v0
.end method

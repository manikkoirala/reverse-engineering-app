.class public Lep1;
.super Lhz1;
.source "SourceFile"


# static fields
.field public static final b:Liz1;


# instance fields
.field public final a:Lhz1;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .line 1
    new-instance v0, Lep1$a;

    invoke-direct {v0}, Lep1$a;-><init>()V

    sput-object v0, Lep1;->b:Liz1;

    return-void
.end method

.method public constructor <init>(Lhz1;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lhz1;-><init>()V

    iput-object p1, p0, Lep1;->a:Lhz1;

    return-void
.end method

.method public synthetic constructor <init>(Lhz1;Lep1$a;)V
    .locals 0

    .line 2
    invoke-direct {p0, p1}, Lep1;-><init>(Lhz1;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic b(Lrh0;)Ljava/lang/Object;
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lep1;->e(Lrh0;)Ljava/sql/Timestamp;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic d(Lvh0;Ljava/lang/Object;)V
    .locals 0

    .line 1
    check-cast p2, Ljava/sql/Timestamp;

    invoke-virtual {p0, p1, p2}, Lep1;->f(Lvh0;Ljava/sql/Timestamp;)V

    return-void
.end method

.method public e(Lrh0;)Ljava/sql/Timestamp;
    .locals 3

    .line 1
    iget-object v0, p0, Lep1;->a:Lhz1;

    invoke-virtual {v0, p1}, Lhz1;->b(Lrh0;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Date;

    if-eqz p1, :cond_0

    new-instance v0, Ljava/sql/Timestamp;

    invoke-virtual {p1}, Ljava/util/Date;->getTime()J

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Ljava/sql/Timestamp;-><init>(J)V

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public f(Lvh0;Ljava/sql/Timestamp;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lep1;->a:Lhz1;

    invoke-virtual {v0, p1, p2}, Lhz1;->d(Lvh0;Ljava/lang/Object;)V

    return-void
.end method

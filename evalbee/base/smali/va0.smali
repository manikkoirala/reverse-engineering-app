.class public Lva0;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:Lee1;

.field public b:Ly01;

.field public c:Landroid/content/Context;

.field public d:Ljava/lang/String;

.field public e:Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lee1;Ly01;)V
    .locals 2

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "https://"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, La91;->u()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ":8759/smscredit"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lva0;->d:Ljava/lang/String;

    iput-object p2, p0, Lva0;->a:Lee1;

    iput-object p3, p0, Lva0;->b:Ly01;

    iput-object p1, p0, Lva0;->c:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p1

    const-string p2, "MyPref"

    const/4 p3, 0x0

    invoke-virtual {p1, p2, p3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object p1

    iput-object p1, p0, Lva0;->e:Landroid/content/SharedPreferences;

    invoke-virtual {p0}, Lva0;->c()V

    return-void
.end method

.method public static synthetic a(Lva0;Ljava/lang/Object;)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lva0;->d(Ljava/lang/Object;)V

    return-void
.end method

.method public static synthetic b(Lva0;)Landroid/content/Context;
    .locals 0

    .line 1
    iget-object p0, p0, Lva0;->c:Landroid/content/Context;

    return-object p0
.end method


# virtual methods
.method public final c()V
    .locals 7

    .line 1
    new-instance v6, Lva0$c;

    const/4 v2, 0x1

    iget-object v3, p0, Lva0;->d:Ljava/lang/String;

    new-instance v4, Lva0$a;

    invoke-direct {v4, p0}, Lva0$a;-><init>(Lva0;)V

    new-instance v5, Lva0$b;

    invoke-direct {v5, p0}, Lva0$b;-><init>(Lva0;)V

    move-object v0, v6

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lva0$c;-><init>(Lva0;ILjava/lang/String;Lcom/android/volley/d$b;Lcom/android/volley/d$a;)V

    new-instance v0, Lwq;

    const/4 v1, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    const/16 v3, 0x4e20

    invoke-direct {v0, v3, v1, v2}, Lwq;-><init>(IIF)V

    invoke-virtual {v6, v0}, Lcom/android/volley/Request;->L(Ljf1;)Lcom/android/volley/Request;

    iget-object v0, p0, Lva0;->a:Lee1;

    invoke-virtual {v0, v6}, Lee1;->a(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    return-void
.end method

.method public final d(Ljava/lang/Object;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lva0;->b:Ly01;

    if-eqz v0, :cond_0

    invoke-interface {v0, p1}, Ly01;->a(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

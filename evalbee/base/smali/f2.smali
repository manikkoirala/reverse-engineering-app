.class public Lf2;
.super Landroidx/fragment/app/Fragment;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf2$q;
    }
.end annotation


# instance fields
.field public a:Landroid/content/Context;

.field public b:Landroid/view/View;

.field public c:Landroid/view/ViewGroup;

.field public d:Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

.field public e:Landroid/widget/ListView;

.field public f:Ljava/util/ArrayList;

.field public g:Ld3;

.field public h:Landroid/widget/TextView;

.field public i:Landroid/content/BroadcastReceiver;

.field public j:Landroid/content/BroadcastReceiver;

.field public k:Landroid/view/ActionMode;

.field public l:Z

.field public m:Ljava/lang/String;

.field public n:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Landroidx/fragment/app/Fragment;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lf2;->f:Ljava/util/ArrayList;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lf2;->l:Z

    const-string v0, "All"

    iput-object v0, p0, Lf2;->m:Ljava/lang/String;

    return-void
.end method

.method public static synthetic g(Lf2;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lf2;->G()V

    return-void
.end method

.method public static synthetic h(Lf2;Ljava/util/ArrayList;)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lf2;->r(Ljava/util/ArrayList;)V

    return-void
.end method

.method public static synthetic i(Lf2;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 1
    iput-object p1, p0, Lf2;->m:Ljava/lang/String;

    return-object p1
.end method

.method public static synthetic j(Lf2;Z)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lf2;->H(Z)V

    return-void
.end method

.method public static synthetic k(Lf2;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lf2;->B()V

    return-void
.end method

.method public static synthetic l(Lf2;Z)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lf2;->v(Z)V

    return-void
.end method

.method public static synthetic m(Lf2;Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/SyncExamsFile;Z)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1, p2}, Lf2;->s(Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/SyncExamsFile;Z)V

    return-void
.end method

.method public static synthetic n(Lf2;I)I
    .locals 0

    .line 1
    iput p1, p0, Lf2;->n:I

    return p1
.end method

.method public static synthetic o(Lf2;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lf2;->C()V

    return-void
.end method

.method public static synthetic p(Lf2;Ljava/util/ArrayList;)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lf2;->u(Ljava/util/ArrayList;)V

    return-void
.end method

.method public static synthetic q(Lf2;Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lf2;->t(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)V

    return-void
.end method


# virtual methods
.method public final A()V
    .locals 1

    .line 1
    new-instance v0, Lf2$l;

    invoke-direct {v0, p0}, Lf2$l;-><init>(Lf2;)V

    iput-object v0, p0, Lf2;->j:Landroid/content/BroadcastReceiver;

    new-instance v0, Lf2$m;

    invoke-direct {v0, p0}, Lf2$m;-><init>(Lf2;)V

    iput-object v0, p0, Lf2;->i:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method public final B()V
    .locals 8

    .line 1
    iget-boolean v0, p0, Lf2;->l:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-object v0, p0, Lf2;->a:Landroid/content/Context;

    invoke-static {v0}, La91;->j(Landroid/content/Context;)J

    move-result-wide v4

    const-wide/32 v6, 0x2bf20

    add-long/2addr v4, v6

    cmp-long v0, v2, v4

    if-gtz v0, :cond_0

    iget-object v0, p0, Lf2;->a:Landroid/content/Context;

    invoke-static {v0}, La91;->i(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/ExamsPendingActionFile;

    move-result-object v0

    if-eqz v0, :cond_1

    :cond_0
    invoke-virtual {p0, v1}, Lf2;->H(Z)V

    :cond_1
    new-instance v0, Lf2$q;

    const/4 v2, 0x0

    invoke-direct {v0, p0, v2}, Lf2$q;-><init>(Lf2;Lf2$h;)V

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method public final C()V
    .locals 9

    .line 1
    iget-object v0, p0, Lf2;->a:Landroid/content/Context;

    const-string v1, "MyPref"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v3, "ARCHIVE_COUNT"

    invoke-interface {v0, v3, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    const-string v3, ""

    if-lez v0, :cond_0

    iget-object v4, p0, Lf2;->h:Landroid/widget/TextView;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lf2;->h:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    iget-object v0, p0, Lf2;->f:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lf2;->e:Landroid/widget/ListView;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    new-instance v0, Ld3;

    iget-object v3, p0, Lf2;->a:Landroid/content/Context;

    iget-object v4, p0, Lf2;->f:Ljava/util/ArrayList;

    const/4 v5, 0x1

    invoke-direct {v0, v3, v4, v5}, Ld3;-><init>(Landroid/content/Context;Ljava/util/ArrayList;Z)V

    iput-object v0, p0, Lf2;->g:Ld3;

    iget-object v3, p0, Lf2;->e:Landroid/widget/ListView;

    invoke-virtual {v3, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget v0, p0, Lf2;->n:I

    if-lez v0, :cond_2

    iget-object v0, p0, Lf2;->a:Landroid/content/Context;

    invoke-virtual {p0, v0}, Lf2;->w(Landroid/content/Context;)I

    move-result v0

    add-int/lit16 v0, v0, 0xb40

    invoke-static {}, La91;->n()I

    move-result v3

    if-ge v0, v3, :cond_2

    iget-object v0, p0, Lf2;->a:Landroid/content/Context;

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "LAST_ARCHIVE_ALERT"

    invoke-static {}, La91;->n()I

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    new-instance v2, Lf2$k;

    invoke-direct {v2, p0}, Lf2$k;-><init>(Lf2;)V

    iget-object v1, p0, Lf2;->a:Landroid/content/Context;

    const v3, 0x7f12016b

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget v4, p0, Lf2;->n:I

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v4, " "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const v4, 0x7f1201ba

    invoke-virtual {p0, v4}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const v5, 0x7f120257

    const/4 v6, 0x0

    const/4 v7, 0x0

    const v8, 0x7f0800d4

    invoke-static/range {v1 .. v8}, Lxs;->d(Landroid/content/Context;Ly01;ILjava/lang/String;IIII)V

    goto :goto_1

    :cond_1
    iget-object v0, p0, Lf2;->e:Landroid/widget/ListView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_2
    :goto_1
    return-void
.end method

.method public final D(Ljava/io/InputStream;Landroid/net/Uri;)V
    .locals 0

    .line 1
    :try_start_0
    invoke-static {p1}, Lcom/google/android/gms/common/util/IOUtils;->toByteArray(Ljava/io/InputStream;)[B

    move-result-object p1

    invoke-static {p1}, Lyx;->a([B)Lcom/ekodroid/omrevaluator/serializable/ExmVersions/ExamDataV1;

    move-result-object p1

    if-nez p1, :cond_0

    const p1, 0x7f1200ca

    const p2, 0x7f1201b3

    invoke-virtual {p0, p1, p2}, Lf2;->E(II)V

    return-void

    :cond_0
    invoke-virtual {p0, p1}, Lf2;->F(Lcom/ekodroid/omrevaluator/serializable/ExmVersions/ExamDataV1;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception p1

    goto :goto_0

    :catch_1
    move-exception p1

    :goto_0
    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V

    :goto_1
    return-void
.end method

.method public final E(II)V
    .locals 8

    .line 1
    iget-object v0, p0, Lf2;->a:Landroid/content/Context;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const v4, 0x7f120060

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move v3, p2

    invoke-static/range {v0 .. v7}, Lxs;->c(Landroid/content/Context;Ly01;IIIIII)V

    return-void
.end method

.method public final F(Lcom/ekodroid/omrevaluator/serializable/ExmVersions/ExamDataV1;)V
    .locals 4

    .line 1
    new-instance v0, Lf2$g;

    invoke-direct {v0, p0, p1}, Lf2$g;-><init>(Lf2;Lcom/ekodroid/omrevaluator/serializable/ExmVersions/ExamDataV1;)V

    new-instance v1, Lti1;

    iget-object v2, p0, Lf2;->a:Landroid/content/Context;

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/serializable/ExmVersions/ExamDataV1;->getSheetTemplate2()Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    move-result-object v3

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/serializable/ExmVersions/ExamDataV1;->getExamId()Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    move-result-object p1

    invoke-direct {v1, v2, v0, v3, p1}, Lti1;-><init>(Landroid/content/Context;Ly01;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)V

    invoke-virtual {v1}, Landroid/app/Dialog;->show()V

    return-void
.end method

.method public final G()V
    .locals 8

    .line 1
    new-instance v1, Lf2$n;

    invoke-direct {v1, p0}, Lf2$n;-><init>(Lf2;)V

    iget-object v0, p0, Lf2;->a:Landroid/content/Context;

    const v2, 0x7f120110

    const v3, 0x7f1201ac

    const v4, 0x7f120114

    const v5, 0x7f120053

    const/4 v6, 0x0

    const v7, 0x7f0800f4

    invoke-static/range {v0 .. v7}, Lxs;->c(Landroid/content/Context;Ly01;IIIIII)V

    return-void
.end method

.method public final H(Z)V
    .locals 4

    .line 1
    iget-object v0, p0, Lf2;->a:Landroid/content/Context;

    invoke-static {v0}, La91;->i(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/ExamsPendingActionFile;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-virtual {p0, p1}, Lf2;->v(Z)V

    goto :goto_0

    :cond_0
    new-instance v1, Lcom/ekodroid/omrevaluator/clients/SyncClients/DeleteSyncExams;

    iget-object v2, p0, Lf2;->a:Landroid/content/Context;

    new-instance v3, Lf2$a;

    invoke-direct {v3, p0, p1}, Lf2$a;-><init>(Lf2;Z)V

    invoke-direct {v1, v0, v2, v3}, Lcom/ekodroid/omrevaluator/clients/SyncClients/DeleteSyncExams;-><init>(Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/ExamsPendingActionFile;Landroid/content/Context;Lzg;)V

    :goto_0
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .line 1
    invoke-super {p0, p1, p2, p3}, Landroidx/fragment/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    const/4 p1, -0x1

    if-ne p2, p1, :cond_0

    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object p1

    :try_start_0
    iget-object p2, p0, Lf2;->a:Landroid/content/Context;

    invoke-virtual {p2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object p2

    invoke-virtual {p2, p1}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;

    move-result-object p2

    invoke-virtual {p0, p2, p1}, Lf2;->D(Ljava/io/InputStream;Landroid/net/Uri;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V

    :cond_0
    :goto_0
    return-void
.end method

.method public onAttach(Landroid/content/Context;)V
    .locals 0

    .line 1
    invoke-super {p0, p1}, Landroidx/fragment/app/Fragment;->onAttach(Landroid/content/Context;)V

    iput-object p1, p0, Lf2;->a:Landroid/content/Context;

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 0

    .line 1
    invoke-super {p0, p1}, Landroidx/fragment/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1

    .line 1
    const p3, 0x7f0c008c

    const/4 v0, 0x0

    invoke-virtual {p1, p3, p2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    iput-object p1, p0, Lf2;->b:Landroid/view/View;

    invoke-virtual {p0}, Lf2;->z()V

    invoke-virtual {p0}, Lf2;->y()V

    iget-object p1, p0, Lf2;->b:Landroid/view/View;

    const p2, 0x7f090421

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/TextView;

    iput-object p1, p0, Lf2;->h:Landroid/widget/TextView;

    iget-object p1, p0, Lf2;->b:Landroid/view/View;

    const p2, 0x7f09009b

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    new-instance p2, Lf2$h;

    invoke-direct {p2, p0}, Lf2$h;-><init>(Lf2;)V

    invoke-virtual {p1, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object p1, p0, Lf2;->b:Landroid/view/View;

    const p2, 0x7f0900c5

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    new-instance p2, Lf2$i;

    invoke-direct {p2, p0}, Lf2$i;-><init>(Lf2;)V

    invoke-virtual {p1, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object p1, p0, Lf2;->b:Landroid/view/View;

    const p2, 0x7f0901df

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    new-instance p2, Lf2$j;

    invoke-direct {p2, p0}, Lf2$j;-><init>(Lf2;)V

    invoke-virtual {p1, p2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0}, Lf2;->A()V

    iget-object p1, p0, Lf2;->a:Landroid/content/Context;

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;->getRequest()Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/PendingJoinRequest;

    move-result-object p1

    if-eqz p1, :cond_0

    new-instance p1, Lmg0;

    iget-object p2, p0, Lf2;->a:Landroid/content/Context;

    const/4 p3, 0x0

    invoke-direct {p1, p2, p3}, Lmg0;-><init>(Landroid/content/Context;Ly01;)V

    :cond_0
    iget-object p1, p0, Lf2;->b:Landroid/view/View;

    return-object p1
.end method

.method public onPause()V
    .locals 2

    .line 1
    invoke-super {p0}, Landroidx/fragment/app/Fragment;->onPause()V

    iget-object v0, p0, Lf2;->a:Landroid/content/Context;

    iget-object v1, p0, Lf2;->i:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iget-object v0, p0, Lf2;->a:Landroid/content/Context;

    iget-object v1, p0, Lf2;->j:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iget-object v0, p0, Lf2;->k:Landroid/view/ActionMode;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/ActionMode;->finish()V

    const/4 v0, 0x0

    iput-object v0, p0, Lf2;->k:Landroid/view/ActionMode;

    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 5

    .line 1
    invoke-super {p0}, Landroidx/fragment/app/Fragment;->onResume()V

    iget-object v0, p0, Lf2;->a:Landroid/content/Context;

    invoke-static {v0}, La91;->c(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lf2;->l:Z

    invoke-virtual {p0}, Lf2;->B()V

    iget-object v0, p0, Lf2;->a:Landroid/content/Context;

    iget-object v1, p0, Lf2;->i:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "UPDATE_EXAM_LIST"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    const/4 v3, 0x4

    invoke-static {v0, v1, v2, v3}, Lsl;->registerReceiver(Landroid/content/Context;Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;I)Landroid/content/Intent;

    iget-object v0, p0, Lf2;->a:Landroid/content/Context;

    iget-object v1, p0, Lf2;->j:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v4, "UPDATE_PURCHASE"

    invoke-direct {v2, v4}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1, v2, v3}, Lsl;->registerReceiver(Landroid/content/Context;Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;I)Landroid/content/Intent;

    return-void
.end method

.method public final r(Ljava/util/ArrayList;)V
    .locals 8

    .line 1
    new-instance v1, Lf2$d;

    invoke-direct {v1, p0, p1}, Lf2$d;-><init>(Lf2;Ljava/util/ArrayList;)V

    iget-object v0, p0, Lf2;->a:Landroid/content/Context;

    const v2, 0x7f12003f

    const v3, 0x7f120175

    const v4, 0x7f12003e

    const v5, 0x7f120053

    const/4 v6, 0x0

    const v7, 0x7f0800d4

    invoke-static/range {v0 .. v7}, Lxs;->c(Landroid/content/Context;Ly01;IIIIII)V

    return-void
.end method

.method public final s(Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/SyncExamsFile;Z)V
    .locals 8

    .line 1
    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/SyncExamsFile;->getExamIds()Ljava/util/Set;

    move-result-object v0

    iget-object v1, p0, Lf2;->a:Landroid/content/Context;

    invoke-static {v1}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;

    move-result-object v1

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getAllSyncedExams()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v4

    if-ne v3, v4, :cond_1

    :cond_0
    iget-object p2, p0, Lf2;->a:Landroid/content/Context;

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/SyncExamsFile;->getLastSyncedOn()Ljava/lang/Long;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-static {p2, v0, v1}, La91;->J(Landroid/content/Context;J)V

    return-void

    :cond_1
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v4

    if-le v3, v4, :cond_4

    iget-object p2, p0, Lf2;->a:Landroid/content/Context;

    invoke-static {p2}, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;

    move-result-object p2

    invoke-static {}, Lcom/google/firebase/auth/FirebaseAuth;->getInstance()Lcom/google/firebase/auth/FirebaseAuth;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/firebase/auth/FirebaseAuth;->e()Lr30;

    move-result-object v3

    invoke-virtual {v3}, Lr30;->O()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lf2;->a:Landroid/content/Context;

    invoke-static {v4}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;

    move-result-object v4

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;

    invoke-virtual {v5}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->getCloudId()Ljava/lang/Long;

    move-result-object v6

    invoke-interface {v0, v6}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_2

    invoke-virtual {p2}, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;->getOrgId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_3

    invoke-virtual {v5}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->getUserId()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_3

    iget-object v6, p0, Lf2;->a:Landroid/content/Context;

    invoke-virtual {v5}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->getExamId()Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    move-result-object v5

    invoke-static {v6, v5}, Lxu1;->b(Landroid/content/Context;Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)V

    goto :goto_0

    :cond_3
    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->setCloudSyncOn(Z)V

    invoke-virtual {v5, v6}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->setSynced(Z)V

    const/4 v7, 0x0

    invoke-virtual {v5, v7}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->setSharedType(Lcom/ekodroid/omrevaluator/serializable/SharedType;)V

    invoke-virtual {v5, v7}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->setUserId(Ljava/lang/String;)V

    invoke-virtual {v5, v6}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->setSyncImages(Z)V

    invoke-virtual {v1, v5}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->saveOrUpdateTemplateJsonAsSynced(Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;)Z

    invoke-virtual {v5}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->getExamId()Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->getAllResultJson(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;

    invoke-virtual {v7, v6}, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;->setSynced(Z)V

    invoke-virtual {v4, v7}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->saveOrUpdateResultJsonAsNotSynced(Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;)Z

    goto :goto_1

    :cond_4
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result p1

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    if-ge p1, v0, :cond_5

    if-nez p2, :cond_5

    iget-object p1, p0, Lf2;->a:Landroid/content/Context;

    const-wide/16 v0, 0x0

    invoke-static {p1, v0, v1}, La91;->J(Landroid/content/Context;J)V

    const/4 p1, 0x1

    invoke-virtual {p0, p1}, Lf2;->H(Z)V

    :cond_5
    return-void
.end method

.method public final t(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)V
    .locals 3

    .line 1
    new-instance v0, Lf2$e;

    invoke-direct {v0, p0}, Lf2$e;-><init>(Lf2;)V

    new-instance v1, Ldm;

    iget-object v2, p0, Lf2;->a:Landroid/content/Context;

    invoke-direct {v1, v2, p1, v0}, Ldm;-><init>(Landroid/content/Context;Lcom/ekodroid/omrevaluator/templateui/models/ExamId;Ly01;)V

    invoke-virtual {v1}, Landroid/app/Dialog;->show()V

    return-void
.end method

.method public final u(Ljava/util/ArrayList;)V
    .locals 3

    .line 1
    new-instance v0, Lf2$f;

    invoke-direct {v0, p0}, Lf2$f;-><init>(Lf2;)V

    new-instance v1, Lgs;

    iget-object v2, p0, Lf2;->a:Landroid/content/Context;

    invoke-direct {v1, v2, v0, p1}, Lgs;-><init>(Landroid/content/Context;Ly01;Ljava/util/ArrayList;)V

    return-void
.end method

.method public final v(Z)V
    .locals 5

    .line 1
    iget-object v0, p0, Lf2;->a:Landroid/content/Context;

    invoke-static {v0}, La91;->j(Landroid/content/Context;)J

    move-result-wide v0

    new-instance v2, Lxa0;

    iget-object v3, p0, Lf2;->a:Landroid/content/Context;

    new-instance v4, Lf2$b;

    invoke-direct {v4, p0, p1}, Lf2$b;-><init>(Lf2;Z)V

    invoke-direct {v2, v0, v1, v3, v4}, Lxa0;-><init>(JLandroid/content/Context;Lzg;)V

    return-void
.end method

.method public final w(Landroid/content/Context;)I
    .locals 2

    .line 1
    const-string v0, "MyPref"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object p1

    const-string v0, "LAST_ARCHIVE_ALERT"

    invoke-interface {p1, v0, v1}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result p1

    return p1
.end method

.method public x()V
    .locals 3

    .line 1
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.GET_CONTENT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "*/*"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "android.intent.category.OPENABLE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "android.intent.extra.LOCAL_ONLY"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string v1, "Choose a file"

    invoke-static {v0, v1}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Landroidx/fragment/app/Fragment;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method public final y()V
    .locals 2

    .line 1
    iget-object v0, p0, Lf2;->e:Landroid/widget/ListView;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->setChoiceMode(I)V

    iget-object v0, p0, Lf2;->e:Landroid/widget/ListView;

    new-instance v1, Lf2$c;

    invoke-direct {v1, p0}, Lf2$c;-><init>(Lf2;)V

    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->setMultiChoiceModeListener(Landroid/widget/AbsListView$MultiChoiceModeListener;)V

    return-void
.end method

.method public final z()V
    .locals 3

    .line 1
    iget-object v0, p0, Lf2;->b:Landroid/view/View;

    const v1, 0x7f090243

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lf2;->e:Landroid/widget/ListView;

    iget-object v0, p0, Lf2;->b:Landroid/view/View;

    const v1, 0x7f09036f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

    iput-object v0, p0, Lf2;->d:Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f030003

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getIntArray(I)[I

    move-result-object v1

    invoke-virtual {v0, v1}, Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;->setColorSchemeColors([I)V

    iget-object v0, p0, Lf2;->d:Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;

    new-instance v1, Lf2$o;

    invoke-direct {v1, p0}, Lf2$o;-><init>(Lf2;)V

    invoke-virtual {v0, v1}, Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout;->setOnRefreshListener(Landroidx/swiperefreshlayout/widget/SwipeRefreshLayout$j;)V

    iget-object v0, p0, Lf2;->e:Landroid/widget/ListView;

    new-instance v1, Lf2$p;

    invoke-direct {v1, p0}, Lf2$p;-><init>(Lf2;)V

    invoke-virtual {v0, v1}, Landroid/widget/AdapterView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0c0092

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lf2;->c:Landroid/view/ViewGroup;

    iget-object v1, p0, Lf2;->e:Landroid/widget/ListView;

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;)V

    return-void
.end method

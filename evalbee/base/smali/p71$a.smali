.class public Lp71$a;
.super Lix;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lp71;-><init>(Landroidx/room/RoomDatabase;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic d:Lp71;


# direct methods
.method public constructor <init>(Lp71;Landroidx/room/RoomDatabase;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lp71$a;->d:Lp71;

    invoke-direct {p0, p2}, Lix;-><init>(Landroidx/room/RoomDatabase;)V

    return-void
.end method


# virtual methods
.method public e()Ljava/lang/String;
    .locals 1

    .line 1
    const-string v0, "INSERT OR REPLACE INTO `Preference` (`key`,`long_value`) VALUES (?,?)"

    return-object v0
.end method

.method public bridge synthetic i(Lws1;Ljava/lang/Object;)V
    .locals 0

    .line 1
    check-cast p2, Ln71;

    invoke-virtual {p0, p1, p2}, Lp71$a;->k(Lws1;Ln71;)V

    return-void
.end method

.method public k(Lws1;Ln71;)V
    .locals 4

    .line 1
    invoke-virtual {p2}, Ln71;->a()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    if-nez v0, :cond_0

    invoke-interface {p1, v1}, Lus1;->I(I)V

    goto :goto_0

    :cond_0
    invoke-virtual {p2}, Ln71;->a()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v1, v0}, Lus1;->y(ILjava/lang/String;)V

    :goto_0
    invoke-virtual {p2}, Ln71;->b()Ljava/lang/Long;

    move-result-object v0

    const/4 v1, 0x2

    if-nez v0, :cond_1

    invoke-interface {p1, v1}, Lus1;->I(I)V

    goto :goto_1

    :cond_1
    invoke-virtual {p2}, Ln71;->b()Ljava/lang/Long;

    move-result-object p2

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-interface {p1, v1, v2, v3}, Lus1;->A(IJ)V

    :goto_1
    return-void
.end method

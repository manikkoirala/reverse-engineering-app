.class public abstract Ls70;
.super La80;
.source "SourceFile"

# interfaces
.implements Lne;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, La80;-><init>()V

    return-void
.end method


# virtual methods
.method public asMap()Ljava/util/concurrent/ConcurrentMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/concurrent/ConcurrentMap<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .line 1
    invoke-virtual {p0}, Ls70;->delegate()Lne;

    move-result-object v0

    invoke-interface {v0}, Lne;->asMap()Ljava/util/concurrent/ConcurrentMap;

    move-result-object v0

    return-object v0
.end method

.method public cleanUp()V
    .locals 1

    .line 1
    invoke-virtual {p0}, Ls70;->delegate()Lne;

    move-result-object v0

    invoke-interface {v0}, Lne;->cleanUp()V

    return-void
.end method

.method public abstract delegate()Lne;
.end method

.method public get(Ljava/lang/Object;Ljava/util/concurrent/Callable;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Ljava/util/concurrent/Callable<",
            "Ljava/lang/Object;",
            ">;)",
            "Ljava/lang/Object;"
        }
    .end annotation

    .line 1
    invoke-virtual {p0}, Ls70;->delegate()Lne;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lne;->get(Ljava/lang/Object;Ljava/util/concurrent/Callable;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public getAllPresent(Ljava/lang/Iterable;)Lcom/google/common/collect/ImmutableMap;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable<",
            "+",
            "Ljava/lang/Object;",
            ">;)",
            "Lcom/google/common/collect/ImmutableMap<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .line 1
    invoke-virtual {p0}, Ls70;->delegate()Lne;

    move-result-object v0

    invoke-interface {v0, p1}, Lne;->getAllPresent(Ljava/lang/Iterable;)Lcom/google/common/collect/ImmutableMap;

    move-result-object p1

    return-object p1
.end method

.method public getIfPresent(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")",
            "Ljava/lang/Object;"
        }
    .end annotation

    .line 1
    invoke-virtual {p0}, Ls70;->delegate()Lne;

    move-result-object v0

    invoke-interface {v0, p1}, Lne;->getIfPresent(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public invalidate(Ljava/lang/Object;)V
    .locals 1

    .line 1
    invoke-virtual {p0}, Ls70;->delegate()Lne;

    move-result-object v0

    invoke-interface {v0, p1}, Lne;->invalidate(Ljava/lang/Object;)V

    return-void
.end method

.method public invalidateAll()V
    .locals 1

    .line 1
    invoke-virtual {p0}, Ls70;->delegate()Lne;

    move-result-object v0

    invoke-interface {v0}, Lne;->invalidateAll()V

    return-void
.end method

.method public invalidateAll(Ljava/lang/Iterable;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Iterable<",
            "+",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 2
    invoke-virtual {p0}, Ls70;->delegate()Lne;

    move-result-object v0

    invoke-interface {v0, p1}, Lne;->invalidateAll(Ljava/lang/Iterable;)V

    return-void
.end method

.method public put(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    .line 1
    invoke-virtual {p0}, Ls70;->delegate()Lne;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lne;->put(Ljava/lang/Object;Ljava/lang/Object;)V

    return-void
.end method

.method public putAll(Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/Object;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 1
    invoke-virtual {p0}, Ls70;->delegate()Lne;

    move-result-object v0

    invoke-interface {v0, p1}, Lne;->putAll(Ljava/util/Map;)V

    return-void
.end method

.method public size()J
    .locals 2

    .line 1
    invoke-virtual {p0}, Ls70;->delegate()Lne;

    move-result-object v0

    invoke-interface {v0}, Lne;->size()J

    move-result-wide v0

    return-wide v0
.end method

.method public stats()Lpe;
    .locals 1

    .line 1
    invoke-virtual {p0}, Ls70;->delegate()Lne;

    move-result-object v0

    invoke-interface {v0}, Lne;->stats()Lpe;

    move-result-object v0

    return-object v0
.end method

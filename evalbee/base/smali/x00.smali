.class public Lx00;
.super Lzq1;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lx00$a;
    }
.end annotation


# instance fields
.field public final l:Landroid/net/Uri;

.field public m:J

.field public n:Ljq1;

.field public o:Lmz;

.field public p:J

.field public q:Ljava/lang/String;

.field public volatile r:Ljava/lang/Exception;

.field public s:J

.field public t:I


# direct methods
.method public constructor <init>(Ljq1;Landroid/net/Uri;)V
    .locals 6

    .line 1
    invoke-direct {p0}, Lzq1;-><init>()V

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lx00;->p:J

    const/4 v0, 0x0

    iput-object v0, p0, Lx00;->q:Ljava/lang/String;

    iput-object v0, p0, Lx00;->r:Ljava/lang/Exception;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lx00;->s:J

    iput-object p1, p0, Lx00;->n:Ljq1;

    iput-object p2, p0, Lx00;->l:Landroid/net/Uri;

    invoke-virtual {p1}, Ljq1;->j()Lo30;

    move-result-object p1

    new-instance p2, Lmz;

    invoke-virtual {p1}, Lo30;->a()Lr10;

    move-result-object v0

    invoke-virtual {v0}, Lr10;->l()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p1}, Lo30;->c()Lzf0;

    move-result-object v2

    invoke-virtual {p1}, Lo30;->b()Ldg0;

    move-result-object v3

    invoke-virtual {p1}, Lo30;->l()J

    move-result-wide v4

    move-object v0, p2

    invoke-direct/range {v0 .. v5}, Lmz;-><init>(Landroid/content/Context;Lzf0;Ldg0;J)V

    iput-object p2, p0, Lx00;->o:Lmz;

    return-void
.end method


# virtual methods
.method public G()Ljq1;
    .locals 1

    .line 1
    iget-object v0, p0, Lx00;->n:Ljq1;

    return-object v0
.end method

.method public R()V
    .locals 1

    .line 1
    iget-object v0, p0, Lx00;->o:Lmz;

    invoke-virtual {v0}, Lmz;->a()V

    sget-object v0, Lcom/google/android/gms/common/api/Status;->RESULT_CANCELED:Lcom/google/android/gms/common/api/Status;

    invoke-static {v0}, Lcom/google/firebase/storage/StorageException;->fromErrorStatus(Lcom/google/android/gms/common/api/Status;)Lcom/google/firebase/storage/StorageException;

    move-result-object v0

    iput-object v0, p0, Lx00;->r:Ljava/lang/Exception;

    return-void
.end method

.method public b0()V
    .locals 14

    .line 1
    iget-object v0, p0, Lx00;->r:Ljava/lang/Exception;

    const/16 v1, 0x40

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {p0, v1, v2}, Lzq1;->g0(IZ)Z

    return-void

    :cond_0
    const/4 v0, 0x4

    invoke-virtual {p0, v0, v2}, Lzq1;->g0(IZ)Z

    move-result v3

    if-nez v3, :cond_1

    return-void

    :cond_1
    const-wide/16 v3, 0x0

    iput-wide v3, p0, Lx00;->m:J

    const/4 v5, 0x0

    iput-object v5, p0, Lx00;->r:Ljava/lang/Exception;

    iget-object v6, p0, Lx00;->o:Lmz;

    invoke-virtual {v6}, Lmz;->c()V

    new-instance v6, Loa0;

    iget-object v7, p0, Lx00;->n:Ljq1;

    invoke-virtual {v7}, Ljq1;->k()Lkq1;

    move-result-object v7

    iget-object v8, p0, Lx00;->n:Ljq1;

    invoke-virtual {v8}, Ljq1;->e()Lr10;

    move-result-object v8

    iget-wide v9, p0, Lx00;->s:J

    invoke-direct {v6, v7, v8, v9, v10}, Loa0;-><init>(Lkq1;Lr10;J)V

    iget-object v7, p0, Lx00;->o:Lmz;

    invoke-virtual {v7, v6, v2}, Lmz;->e(Lxy0;Z)V

    invoke-virtual {v6}, Lxy0;->o()I

    move-result v7

    iput v7, p0, Lx00;->t:I

    invoke-virtual {v6}, Lxy0;->f()Ljava/lang/Exception;

    move-result-object v7

    if-eqz v7, :cond_2

    invoke-virtual {v6}, Lxy0;->f()Ljava/lang/Exception;

    move-result-object v7

    goto :goto_0

    :cond_2
    iget-object v7, p0, Lx00;->r:Ljava/lang/Exception;

    :goto_0
    iput-object v7, p0, Lx00;->r:Ljava/lang/Exception;

    iget v7, p0, Lx00;->t:I

    invoke-virtual {p0, v7}, Lx00;->j0(I)Z

    move-result v7

    const/4 v8, 0x1

    if-eqz v7, :cond_3

    iget-object v7, p0, Lx00;->r:Ljava/lang/Exception;

    if-nez v7, :cond_3

    invoke-virtual {p0}, Lzq1;->A()I

    move-result v7

    if-ne v7, v0, :cond_3

    move v7, v8

    goto :goto_1

    :cond_3
    move v7, v2

    :goto_1
    const-string v9, "FileDownloadTask"

    if-eqz v7, :cond_5

    invoke-virtual {v6}, Lxy0;->r()I

    move-result v10

    int-to-long v10, v10

    iget-wide v12, p0, Lx00;->s:J

    add-long/2addr v10, v12

    iput-wide v10, p0, Lx00;->p:J

    const-string v10, "ETag"

    invoke-virtual {v6, v10}, Lxy0;->q(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v11

    if-nez v11, :cond_4

    iget-object v11, p0, Lx00;->q:Ljava/lang/String;

    if-eqz v11, :cond_4

    invoke-virtual {v11, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_4

    const-string v0, "The file at the server has changed.  Restarting from the beginning."

    invoke-static {v9, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iput-wide v3, p0, Lx00;->s:J

    iput-object v5, p0, Lx00;->q:Ljava/lang/String;

    invoke-virtual {v6}, Lxy0;->C()V

    invoke-virtual {p0}, Lx00;->c0()V

    return-void

    :cond_4
    iput-object v10, p0, Lx00;->q:Ljava/lang/String;

    :try_start_0
    invoke-virtual {p0, v6}, Lx00;->k0(Lxy0;)Z

    move-result v7
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    :catch_0
    move-exception v5

    const-string v10, "Exception occurred during file write.  Aborting."

    invoke-static {v9, v10, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    iput-object v5, p0, Lx00;->r:Ljava/lang/Exception;

    :cond_5
    :goto_2
    invoke-virtual {v6}, Lxy0;->C()V

    if-eqz v7, :cond_6

    iget-object v5, p0, Lx00;->r:Ljava/lang/Exception;

    if-nez v5, :cond_6

    invoke-virtual {p0}, Lzq1;->A()I

    move-result v5

    if-ne v5, v0, :cond_6

    goto :goto_3

    :cond_6
    move v8, v2

    :goto_3
    if-eqz v8, :cond_7

    const/16 v0, 0x80

    invoke-virtual {p0, v0, v2}, Lzq1;->g0(IZ)Z

    return-void

    :cond_7
    new-instance v5, Ljava/io/File;

    iget-object v6, p0, Lx00;->l:Landroid/net/Uri;

    invoke-virtual {v6}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v6

    if-eqz v6, :cond_8

    invoke-virtual {v5}, Ljava/io/File;->length()J

    move-result-wide v5

    iput-wide v5, p0, Lx00;->s:J

    goto :goto_4

    :cond_8
    iput-wide v3, p0, Lx00;->s:J

    :goto_4
    invoke-virtual {p0}, Lzq1;->A()I

    move-result v5

    const/16 v6, 0x8

    if-ne v5, v6, :cond_9

    const/16 v0, 0x10

    invoke-virtual {p0, v0, v2}, Lzq1;->g0(IZ)Z

    return-void

    :cond_9
    invoke-virtual {p0}, Lzq1;->A()I

    move-result v5

    const/16 v6, 0x20

    if-ne v5, v6, :cond_b

    const/16 v0, 0x100

    invoke-virtual {p0, v0, v2}, Lzq1;->g0(IZ)Z

    move-result v0

    if-nez v0, :cond_a

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unable to change download task to final state from "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lzq1;->A()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v9, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_a
    return-void

    :cond_b
    iget-wide v5, p0, Lx00;->m:J

    cmp-long v3, v5, v3

    if-gtz v3, :cond_1

    invoke-virtual {p0, v1, v2}, Lzq1;->g0(IZ)Z

    return-void
.end method

.method public c0()V
    .locals 2

    .line 1
    invoke-static {}, Lbr1;->a()Lbr1;

    move-result-object v0

    invoke-virtual {p0}, Lzq1;->D()Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbr1;->f(Ljava/lang/Runnable;)V

    return-void
.end method

.method public bridge synthetic e0()Lzq1$a;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lx00;->l0()Lx00$a;

    move-result-object v0

    return-object v0
.end method

.method public final i0(Ljava/io/InputStream;[B)I
    .locals 4

    .line 1
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    const/4 v2, -0x1

    :try_start_0
    array-length v3, p2

    if-eq v0, v3, :cond_0

    array-length v3, p2

    sub-int/2addr v3, v0

    invoke-virtual {p1, p2, v0, v3}, Ljava/io/InputStream;->read([BII)I

    move-result v3
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    if-eq v3, v2, :cond_0

    add-int/2addr v0, v3

    const/4 v1, 0x1

    goto :goto_0

    :catch_0
    move-exception p1

    iput-object p1, p0, Lx00;->r:Ljava/lang/Exception;

    :cond_0
    if-eqz v1, :cond_1

    goto :goto_1

    :cond_1
    move v0, v2

    :goto_1
    return v0
.end method

.method public final j0(I)Z
    .locals 1

    .line 1
    const/16 v0, 0x134

    if-eq p1, v0, :cond_1

    const/16 v0, 0xc8

    if-lt p1, v0, :cond_0

    const/16 v0, 0x12c

    if-ge p1, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method

.method public final k0(Lxy0;)Z
    .locals 10

    .line 1
    invoke-virtual {p1}, Lxy0;->t()Ljava/io/InputStream;

    move-result-object p1

    const/4 v0, 0x0

    if-eqz p1, :cond_6

    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lx00;->l:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    const-wide/16 v3, 0x0

    const-string v5, "FileDownloadTask"

    if-nez v2, :cond_1

    iget-wide v6, p0, Lx00;->s:J

    cmp-long v2, v6, v3

    if-gtz v2, :cond_0

    invoke-virtual {v1}, Ljava/io/File;->createNewFile()Z

    move-result v2

    if-nez v2, :cond_1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "unable to create file:"

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v5, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_0
    new-instance p1, Ljava/io/IOException;

    const-string v0, "The file to download to has been deleted."

    invoke-direct {p1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    :goto_0
    iget-wide v6, p0, Lx00;->s:J

    cmp-long v2, v6, v3

    const/4 v3, 0x1

    if-lez v2, :cond_2

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Resuming download file "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, " at "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v6, p0, Lx00;->s:J

    invoke-virtual {v2, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v5, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v2, Ljava/io/FileOutputStream;

    invoke-direct {v2, v1, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;Z)V

    goto :goto_1

    :cond_2
    new-instance v2, Ljava/io/FileOutputStream;

    invoke-direct {v2, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    :goto_1
    const/high16 v1, 0x40000

    :try_start_0
    new-array v1, v1, [B

    :cond_3
    :goto_2
    if-eqz v3, :cond_5

    invoke-virtual {p0, p1, v1}, Lx00;->i0(Ljava/io/InputStream;[B)I

    move-result v4

    const/4 v6, -0x1

    if-eq v4, v6, :cond_5

    invoke-virtual {v2, v1, v0, v4}, Ljava/io/OutputStream;->write([BII)V

    iget-wide v6, p0, Lx00;->m:J

    int-to-long v8, v4

    add-long/2addr v6, v8

    iput-wide v6, p0, Lx00;->m:J

    iget-object v4, p0, Lx00;->r:Ljava/lang/Exception;

    if-eqz v4, :cond_4

    const-string v3, "Exception occurred during file download. Retrying."

    iget-object v4, p0, Lx00;->r:Ljava/lang/Exception;

    invoke-static {v5, v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/4 v3, 0x0

    iput-object v3, p0, Lx00;->r:Ljava/lang/Exception;

    move v3, v0

    :cond_4
    const/4 v4, 0x4

    invoke-virtual {p0, v4, v0}, Lzq1;->g0(IZ)Z

    move-result v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v4, :cond_3

    move v3, v0

    goto :goto_2

    :cond_5
    invoke-virtual {v2}, Ljava/io/OutputStream;->flush()V

    invoke-virtual {v2}, Ljava/io/OutputStream;->close()V

    invoke-virtual {p1}, Ljava/io/InputStream;->close()V

    move v0, v3

    goto :goto_3

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Ljava/io/OutputStream;->flush()V

    invoke-virtual {v2}, Ljava/io/OutputStream;->close()V

    invoke-virtual {p1}, Ljava/io/InputStream;->close()V

    throw v0

    :cond_6
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v1, "Unable to open Firebase Storage stream."

    invoke-direct {p1, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    iput-object p1, p0, Lx00;->r:Ljava/lang/Exception;

    :goto_3
    return v0
.end method

.method public l0()Lx00$a;
    .locals 6

    .line 1
    new-instance v0, Lx00$a;

    iget-object v1, p0, Lx00;->r:Ljava/lang/Exception;

    iget v2, p0, Lx00;->t:I

    invoke-static {v1, v2}, Lcom/google/firebase/storage/StorageException;->fromExceptionAndHttpCode(Ljava/lang/Throwable;I)Lcom/google/firebase/storage/StorageException;

    move-result-object v1

    iget-wide v2, p0, Lx00;->m:J

    iget-wide v4, p0, Lx00;->s:J

    add-long/2addr v2, v4

    invoke-direct {v0, p0, v1, v2, v3}, Lx00$a;-><init>(Lx00;Ljava/lang/Exception;J)V

    return-object v0
.end method

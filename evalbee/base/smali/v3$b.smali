.class public Lv3$b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lv3;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Landroid/widget/TextView;

.field public final synthetic b:Landroid/widget/Spinner;

.field public final synthetic c:Landroid/widget/Spinner;

.field public final synthetic d:Lv3;


# direct methods
.method public constructor <init>(Lv3;Landroid/widget/TextView;Landroid/widget/Spinner;Landroid/widget/Spinner;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lv3$b;->d:Lv3;

    iput-object p2, p0, Lv3$b;->a:Landroid/widget/TextView;

    iput-object p3, p0, Lv3$b;->b:Landroid/widget/Spinner;

    iput-object p4, p0, Lv3$b;->c:Landroid/widget/Spinner;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 14

    .line 1
    iget-object p1, p0, Lv3$b;->d:Lv3;

    iget-object p1, p1, Lv3;->i:Lk5;

    invoke-virtual {p1}, Lk5;->k()[Z

    move-result-object v3

    iget-object p1, p0, Lv3$b;->a:Landroid/widget/TextView;

    const-string v0, ""

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object p1, p0, Lv3$b;->b:Landroid/widget/Spinner;

    invoke-virtual {p1}, Landroid/widget/AdapterView;->getSelectedItem()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    sget-object v0, Lok;->O:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    const v0, 0x7f08016e

    const v1, 0x7f0800bd

    const/4 v11, 0x1

    const/4 v2, 0x0

    if-eqz p1, :cond_1

    iget-object p1, p0, Lv3$b;->d:Lv3;

    iget-object p1, p1, Lv3;->j:Ljava/util/ArrayList;

    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/ekodroid/omrevaluator/templateui/models/PartialAnswerOptionKey;

    invoke-virtual {v4}, Lcom/ekodroid/omrevaluator/templateui/models/PartialAnswerOptionKey;->getPartialType()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_0

    invoke-virtual {v4}, Lcom/ekodroid/omrevaluator/templateui/models/PartialAnswerOptionKey;->getPartialType()Ljava/lang/String;

    move-result-object v5

    sget-object v6, Lok;->O:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-virtual {v4, v3}, Lcom/ekodroid/omrevaluator/templateui/models/PartialAnswerOptionKey;->setMarkedValues([Z)V

    iget-object v2, p0, Lv3$b;->c:Landroid/widget/Spinner;

    invoke-virtual {v2}, Landroid/widget/AdapterView;->getSelectedItem()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Double;

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v5

    invoke-virtual {v4, v5, v6}, Lcom/ekodroid/omrevaluator/templateui/models/PartialAnswerOptionKey;->setPartialMarks(D)V

    iget-object v2, p0, Lv3$b;->d:Lv3;

    iget-object v2, v2, Lv3;->a:Landroid/content/Context;

    const v4, 0x7f1201c3

    invoke-static {v2, v4, v1, v0}, La91;->G(Landroid/content/Context;III)V

    move v2, v11

    goto :goto_0

    :cond_1
    iget-object p1, p0, Lv3$b;->b:Landroid/widget/Spinner;

    invoke-virtual {p1}, Landroid/widget/AdapterView;->getSelectedItem()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    sget-object v4, Lok;->P:Ljava/lang/String;

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    iget-object p1, p0, Lv3$b;->d:Lv3;

    iget-object p1, p1, Lv3;->j:Ljava/util/ArrayList;

    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_2
    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/ekodroid/omrevaluator/templateui/models/PartialAnswerOptionKey;

    invoke-virtual {v4}, Lcom/ekodroid/omrevaluator/templateui/models/PartialAnswerOptionKey;->getPartialType()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_2

    invoke-virtual {v4}, Lcom/ekodroid/omrevaluator/templateui/models/PartialAnswerOptionKey;->getPartialType()Ljava/lang/String;

    move-result-object v5

    sget-object v6, Lok;->P:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-virtual {v4, v3}, Lcom/ekodroid/omrevaluator/templateui/models/PartialAnswerOptionKey;->setMarkedValues([Z)V

    iget-object v2, p0, Lv3$b;->c:Landroid/widget/Spinner;

    invoke-virtual {v2}, Landroid/widget/AdapterView;->getSelectedItem()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Double;

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v5

    invoke-virtual {v4, v5, v6}, Lcom/ekodroid/omrevaluator/templateui/models/PartialAnswerOptionKey;->setPartialMarks(D)V

    iget-object v2, p0, Lv3$b;->d:Lv3;

    iget-object v2, v2, Lv3;->a:Landroid/content/Context;

    const v4, 0x7f1201c4

    invoke-static {v2, v4, v1, v0}, La91;->G(Landroid/content/Context;III)V

    move v2, v11

    goto :goto_1

    :cond_3
    if-nez v2, :cond_5

    iget-object p1, p0, Lv3$b;->d:Lv3;

    iget-object p1, p1, Lv3;->i:Lk5;

    invoke-virtual {p1}, Lk5;->l()Ljava/lang/String;

    move-result-object v9

    iget-object p1, p0, Lv3$b;->d:Lv3;

    iget-object v2, p1, Lv3;->c:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;

    iget-object v4, v2, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->type:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    sget-object v5, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;->DECIMAL:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    if-ne v4, v5, :cond_4

    if-nez v9, :cond_4

    iget-object p1, p1, Lv3;->a:Landroid/content/Context;

    const-string v2, "Please enter valid range"

    invoke-static {p1, v2, v1, v0}, La91;->H(Landroid/content/Context;Ljava/lang/String;II)V

    return-void

    :cond_4
    new-instance p1, Lcom/ekodroid/omrevaluator/templateui/models/PartialAnswerOptionKey;

    iget v5, v2, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->questionNumber:I

    iget v6, v2, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->subjectId:I

    iget v7, v2, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->sectionId:I

    iget-object v0, p0, Lv3$b;->c:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/AdapterView;->getSelectedItem()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v12

    iget-object v0, p0, Lv3$b;->d:Lv3;

    iget-object v0, v0, Lv3;->c:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;

    iget-object v8, v0, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->payload:Ljava/lang/String;

    iget-object v0, p0, Lv3$b;->b:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/AdapterView;->getSelectedItem()Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Ljava/lang/String;

    move-object v0, p1

    move-object v1, v4

    move v2, v5

    move v4, v6

    move v5, v7

    move-wide v6, v12

    invoke-direct/range {v0 .. v10}, Lcom/ekodroid/omrevaluator/templateui/models/PartialAnswerOptionKey;-><init>(Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;I[ZIIDLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lv3$b;->d:Lv3;

    iget-object v0, v0, Lv3;->j:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_5
    iget-object p1, p0, Lv3$b;->d:Lv3;

    iget-object p1, p1, Lv3;->l:Landroid/widget/Button;

    invoke-virtual {p1, v11}, Landroid/view/View;->setEnabled(Z)V

    iget-object p1, p0, Lv3$b;->d:Lv3;

    iget-object p1, p1, Lv3;->i:Lk5;

    invoke-virtual {p1}, Lk5;->p()V

    iget-object p1, p0, Lv3$b;->d:Lv3;

    iget-object v0, p1, Lv3;->j:Ljava/util/ArrayList;

    iget-object v1, p1, Lv3;->k:Landroid/widget/LinearLayout;

    invoke-static {p1, v0, v1}, Lv3;->a(Lv3;Ljava/util/ArrayList;Landroid/widget/LinearLayout;)V

    return-void
.end method

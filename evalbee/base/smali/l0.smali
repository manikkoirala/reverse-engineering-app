.class public abstract Ll0;
.super Ln;
.source "SourceFile"


# instance fields
.field public final a:Ljava/nio/ByteBuffer;

.field public final b:I

.field public final c:I


# direct methods
.method public constructor <init>(I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p1}, Ll0;-><init>(II)V

    return-void
.end method

.method public constructor <init>(II)V
    .locals 2

    .line 2
    invoke-direct {p0}, Ln;-><init>()V

    rem-int v0, p2, p1

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-static {v0}, Li71;->d(Z)V

    add-int/lit8 v0, p2, 0x7

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    sget-object v1, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, Ll0;->a:Ljava/nio/ByteBuffer;

    iput p2, p0, Ll0;->b:I

    iput p1, p0, Ll0;->c:I

    return-void
.end method


# virtual methods
.method public bridge synthetic a(I)Lc81;
    .locals 0

    .line 2
    invoke-virtual {p0, p1}, Ll0;->a(I)Lqc0;

    move-result-object p1

    return-object p1
.end method

.method public final a(I)Lqc0;
    .locals 1

    .line 1
    iget-object v0, p0, Ll0;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, p1}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    invoke-virtual {p0}, Ll0;->n()V

    return-object p0
.end method

.method public bridge synthetic b(J)Lc81;
    .locals 0

    .line 2
    invoke-virtual {p0, p1, p2}, Ll0;->b(J)Lqc0;

    move-result-object p1

    return-object p1
.end method

.method public final b(J)Lqc0;
    .locals 1

    .line 1
    iget-object v0, p0, Ll0;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, p1, p2}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    invoke-virtual {p0}, Ll0;->n()V

    return-object p0
.end method

.method public final e()Lcom/google/common/hash/HashCode;
    .locals 2

    .line 1
    invoke-virtual {p0}, Ll0;->m()V

    iget-object v0, p0, Ll0;->a:Ljava/nio/ByteBuffer;

    invoke-static {v0}, Lvg0;->b(Ljava/nio/Buffer;)V

    iget-object v0, p0, Ll0;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/Buffer;->remaining()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Ll0;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {p0, v0}, Ll0;->p(Ljava/nio/ByteBuffer;)V

    iget-object v0, p0, Ll0;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/Buffer;->limit()I

    move-result v1

    invoke-static {v0, v1}, Lvg0;->c(Ljava/nio/Buffer;I)V

    :cond_0
    invoke-virtual {p0}, Ll0;->l()Lcom/google/common/hash/HashCode;

    move-result-object v0

    return-object v0
.end method

.method public final h([BII)Lqc0;
    .locals 0

    .line 1
    invoke-static {p1, p2, p3}, Ljava/nio/ByteBuffer;->wrap([BII)Ljava/nio/ByteBuffer;

    move-result-object p1

    sget-object p2, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {p1, p2}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object p1

    invoke-virtual {p0, p1}, Ll0;->q(Ljava/nio/ByteBuffer;)Lqc0;

    move-result-object p1

    return-object p1
.end method

.method public final i(Ljava/nio/ByteBuffer;)Lqc0;
    .locals 2

    .line 1
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->order()Ljava/nio/ByteOrder;

    move-result-object v0

    :try_start_0
    sget-object v1, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {p1, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    invoke-virtual {p0, p1}, Ll0;->q(Ljava/nio/ByteBuffer;)Lqc0;

    move-result-object v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    return-object v1

    :catchall_0
    move-exception v1

    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    throw v1
.end method

.method public final k(C)Lqc0;
    .locals 1

    .line 1
    iget-object v0, p0, Ll0;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, p1}, Ljava/nio/ByteBuffer;->putChar(C)Ljava/nio/ByteBuffer;

    invoke-virtual {p0}, Ll0;->n()V

    return-object p0
.end method

.method public abstract l()Lcom/google/common/hash/HashCode;
.end method

.method public final m()V
    .locals 2

    .line 1
    iget-object v0, p0, Ll0;->a:Ljava/nio/ByteBuffer;

    invoke-static {v0}, Lvg0;->b(Ljava/nio/Buffer;)V

    :goto_0
    iget-object v0, p0, Ll0;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/Buffer;->remaining()I

    move-result v0

    iget v1, p0, Ll0;->c:I

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Ll0;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {p0, v0}, Ll0;->o(Ljava/nio/ByteBuffer;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Ll0;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->compact()Ljava/nio/ByteBuffer;

    return-void
.end method

.method public final n()V
    .locals 2

    .line 1
    iget-object v0, p0, Ll0;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/Buffer;->remaining()I

    move-result v0

    const/16 v1, 0x8

    if-ge v0, v1, :cond_0

    invoke-virtual {p0}, Ll0;->m()V

    :cond_0
    return-void
.end method

.method public abstract o(Ljava/nio/ByteBuffer;)V
.end method

.method public abstract p(Ljava/nio/ByteBuffer;)V
.end method

.method public final q(Ljava/nio/ByteBuffer;)Lqc0;
    .locals 4

    .line 1
    invoke-virtual {p1}, Ljava/nio/Buffer;->remaining()I

    move-result v0

    iget-object v1, p0, Ll0;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/Buffer;->remaining()I

    move-result v1

    if-gt v0, v1, :cond_0

    iget-object v0, p0, Ll0;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, p1}, Ljava/nio/ByteBuffer;->put(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;

    invoke-virtual {p0}, Ll0;->n()V

    return-object p0

    :cond_0
    iget v0, p0, Ll0;->b:I

    iget-object v1, p0, Ll0;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/Buffer;->position()I

    move-result v1

    sub-int/2addr v0, v1

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_1

    iget-object v2, p0, Ll0;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->get()B

    move-result v3

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Ll0;->m()V

    :goto_1
    invoke-virtual {p1}, Ljava/nio/Buffer;->remaining()I

    move-result v0

    iget v1, p0, Ll0;->c:I

    if-lt v0, v1, :cond_2

    invoke-virtual {p0, p1}, Ll0;->o(Ljava/nio/ByteBuffer;)V

    goto :goto_1

    :cond_2
    iget-object v0, p0, Ll0;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, p1}, Ljava/nio/ByteBuffer;->put(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;

    return-object p0
.end method

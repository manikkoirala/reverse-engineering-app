.class public Lg61$c;
.super Lcom/android/volley/Request;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lg61;->d(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic t:Ljava/lang/String;

.field public final synthetic v:Lg61;


# direct methods
.method public constructor <init>(Lg61;ILjava/lang/String;Lcom/android/volley/d$a;Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lg61$c;->v:Lg61;

    iput-object p5, p0, Lg61$c;->t:Ljava/lang/String;

    invoke-direct {p0, p2, p3, p4}, Lcom/android/volley/Request;-><init>(ILjava/lang/String;Lcom/android/volley/d$a;)V

    return-void
.end method


# virtual methods
.method public G(Lyy0;)Lcom/android/volley/d;
    .locals 1

    .line 1
    iget-object v0, p1, Lyy0;->b:[B

    invoke-static {p1}, Lid0;->e(Lyy0;)Lcom/android/volley/a$a;

    move-result-object p1

    invoke-static {v0, p1}, Lcom/android/volley/d;->c(Ljava/lang/Object;Lcom/android/volley/a$a;)Lcom/android/volley/d;

    move-result-object p1

    return-object p1
.end method

.method public Q([B)V
    .locals 4

    .line 1
    array-length v0, p1

    const/4 v1, 0x5

    const/4 v2, 0x1

    if-le v0, v1, :cond_0

    iget-object v0, p0, Lg61$c;->v:Lg61;

    const/16 v1, 0xc8

    invoke-static {v0, v2, v1, p1}, Lg61;->b(Lg61;ZILjava/lang/Object;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lg61$c;->v:Lg61;

    new-instance v1, Ljava/lang/String;

    sget-object v3, Ljava/nio/charset/StandardCharsets;->UTF_8:Ljava/nio/charset/Charset;

    invoke-direct {v1, p1, v3}, Ljava/lang/String;-><init>([BLjava/nio/charset/Charset;)V

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    const/16 v1, 0xda

    invoke-static {v0, v2, v1, p1}, Lg61;->b(Lg61;ZILjava/lang/Object;)V

    :goto_0
    return-void
.end method

.method public bridge synthetic f(Ljava/lang/Object;)V
    .locals 0

    .line 1
    check-cast p1, [B

    invoke-virtual {p0, p1}, Lg61$c;->Q([B)V

    return-void
.end method

.method public k()[B
    .locals 2

    .line 1
    :try_start_0
    new-instance v0, Lgc0;

    invoke-direct {v0}, Lgc0;-><init>()V

    iget-object v1, p0, Lg61$c;->v:Lg61;

    invoke-static {v1}, Lg61;->c(Lg61;)Lcom/ekodroid/omrevaluator/serializable/ResultData;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgc0;->s(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "UTF-8"

    invoke-virtual {v0, v1}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    const/4 v0, 0x0

    return-object v0
.end method

.method public l()Ljava/lang/String;
    .locals 1

    .line 1
    const-string v0, "application/json"

    return-object v0
.end method

.method public o()Ljava/util/Map;
    .locals 3

    .line 1
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    const-string v1, "x-user-token"

    iget-object v2, p0, Lg61$c;->t:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object v0
.end method

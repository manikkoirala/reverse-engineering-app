.class public Lb6;
.super Landroid/widget/CheckedTextView;
.source "SourceFile"


# instance fields
.field public final a:Lc6;

.field public final b:Lx5;

.field public final c:Ll7;

.field public d:Lv6;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .line 1
    sget v0, Lsa1;->p:I

    invoke-direct {p0, p1, p2, v0}, Lb6;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .line 2
    invoke-static {p1}, Lqw1;->b(Landroid/content/Context;)Landroid/content/Context;

    move-result-object p1

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/CheckedTextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p0, p1}, Lpv1;->a(Landroid/view/View;Landroid/content/Context;)V

    new-instance p1, Ll7;

    invoke-direct {p1, p0}, Ll7;-><init>(Landroid/widget/TextView;)V

    iput-object p1, p0, Lb6;->c:Ll7;

    invoke-virtual {p1, p2, p3}, Ll7;->m(Landroid/util/AttributeSet;I)V

    invoke-virtual {p1}, Ll7;->b()V

    new-instance p1, Lx5;

    invoke-direct {p1, p0}, Lx5;-><init>(Landroid/view/View;)V

    iput-object p1, p0, Lb6;->b:Lx5;

    invoke-virtual {p1, p2, p3}, Lx5;->e(Landroid/util/AttributeSet;I)V

    new-instance p1, Lc6;

    invoke-direct {p1, p0}, Lc6;-><init>(Landroid/widget/CheckedTextView;)V

    iput-object p1, p0, Lb6;->a:Lc6;

    invoke-virtual {p1, p2, p3}, Lc6;->d(Landroid/util/AttributeSet;I)V

    invoke-direct {p0}, Lb6;->getEmojiTextViewHelper()Lv6;

    move-result-object p1

    invoke-virtual {p1, p2, p3}, Lv6;->c(Landroid/util/AttributeSet;I)V

    return-void
.end method

.method private getEmojiTextViewHelper()Lv6;
    .locals 1

    .line 1
    iget-object v0, p0, Lb6;->d:Lv6;

    if-nez v0, :cond_0

    new-instance v0, Lv6;

    invoke-direct {v0, p0}, Lv6;-><init>(Landroid/widget/TextView;)V

    iput-object v0, p0, Lb6;->d:Lv6;

    :cond_0
    iget-object v0, p0, Lb6;->d:Lv6;

    return-object v0
.end method


# virtual methods
.method public drawableStateChanged()V
    .locals 1

    .line 1
    invoke-super {p0}, Landroid/widget/CheckedTextView;->drawableStateChanged()V

    iget-object v0, p0, Lb6;->c:Ll7;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ll7;->b()V

    :cond_0
    iget-object v0, p0, Lb6;->b:Lx5;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lx5;->b()V

    :cond_1
    iget-object v0, p0, Lb6;->a:Lc6;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lc6;->a()V

    :cond_2
    return-void
.end method

.method public getCustomSelectionActionModeCallback()Landroid/view/ActionMode$Callback;
    .locals 1

    .line 1
    invoke-super {p0}, Landroid/widget/TextView;->getCustomSelectionActionModeCallback()Landroid/view/ActionMode$Callback;

    move-result-object v0

    invoke-static {v0}, Lnv1;->q(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode$Callback;

    move-result-object v0

    return-object v0
.end method

.method public getSupportBackgroundTintList()Landroid/content/res/ColorStateList;
    .locals 1

    .line 1
    iget-object v0, p0, Lb6;->b:Lx5;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lx5;->c()Landroid/content/res/ColorStateList;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public getSupportBackgroundTintMode()Landroid/graphics/PorterDuff$Mode;
    .locals 1

    .line 1
    iget-object v0, p0, Lb6;->b:Lx5;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lx5;->d()Landroid/graphics/PorterDuff$Mode;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public getSupportCheckMarkTintList()Landroid/content/res/ColorStateList;
    .locals 1

    .line 1
    iget-object v0, p0, Lb6;->a:Lc6;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lc6;->b()Landroid/content/res/ColorStateList;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public getSupportCheckMarkTintMode()Landroid/graphics/PorterDuff$Mode;
    .locals 1

    .line 1
    iget-object v0, p0, Lb6;->a:Lc6;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lc6;->c()Landroid/graphics/PorterDuff$Mode;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public getSupportCompoundDrawablesTintList()Landroid/content/res/ColorStateList;
    .locals 1

    .line 1
    iget-object v0, p0, Lb6;->c:Ll7;

    invoke-virtual {v0}, Ll7;->j()Landroid/content/res/ColorStateList;

    move-result-object v0

    return-object v0
.end method

.method public getSupportCompoundDrawablesTintMode()Landroid/graphics/PorterDuff$Mode;
    .locals 1

    .line 1
    iget-object v0, p0, Lb6;->c:Ll7;

    invoke-virtual {v0}, Ll7;->k()Landroid/graphics/PorterDuff$Mode;

    move-result-object v0

    return-object v0
.end method

.method public onCreateInputConnection(Landroid/view/inputmethod/EditorInfo;)Landroid/view/inputmethod/InputConnection;
    .locals 1

    .line 1
    invoke-super {p0, p1}, Landroid/view/View;->onCreateInputConnection(Landroid/view/inputmethod/EditorInfo;)Landroid/view/inputmethod/InputConnection;

    move-result-object v0

    invoke-static {v0, p1, p0}, Lw6;->a(Landroid/view/inputmethod/InputConnection;Landroid/view/inputmethod/EditorInfo;Landroid/view/View;)Landroid/view/inputmethod/InputConnection;

    move-result-object p1

    return-object p1
.end method

.method public setAllCaps(Z)V
    .locals 1

    .line 1
    invoke-super {p0, p1}, Landroid/widget/TextView;->setAllCaps(Z)V

    invoke-direct {p0}, Lb6;->getEmojiTextViewHelper()Lv6;

    move-result-object v0

    invoke-virtual {v0, p1}, Lv6;->d(Z)V

    return-void
.end method

.method public setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .line 1
    invoke-super {p0, p1}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lb6;->b:Lx5;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lx5;->f(Landroid/graphics/drawable/Drawable;)V

    :cond_0
    return-void
.end method

.method public setBackgroundResource(I)V
    .locals 1

    .line 1
    invoke-super {p0, p1}, Landroid/view/View;->setBackgroundResource(I)V

    iget-object v0, p0, Lb6;->b:Lx5;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lx5;->g(I)V

    :cond_0
    return-void
.end method

.method public setCheckMarkDrawable(I)V
    .locals 1

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, Lg7;->b(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    invoke-virtual {p0, p1}, Lb6;->setCheckMarkDrawable(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method public setCheckMarkDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .line 2
    invoke-super {p0, p1}, Landroid/widget/CheckedTextView;->setCheckMarkDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object p1, p0, Lb6;->a:Lc6;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lc6;->e()V

    :cond_0
    return-void
.end method

.method public setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .line 1
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/TextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    iget-object p1, p0, Lb6;->c:Ll7;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ll7;->p()V

    :cond_0
    return-void
.end method

.method public setCompoundDrawablesRelative(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .line 1
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/TextView;->setCompoundDrawablesRelative(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    iget-object p1, p0, Lb6;->c:Ll7;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ll7;->p()V

    :cond_0
    return-void
.end method

.method public setCustomSelectionActionModeCallback(Landroid/view/ActionMode$Callback;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lnv1;->r(Landroid/widget/TextView;Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode$Callback;

    move-result-object p1

    invoke-super {p0, p1}, Landroid/widget/TextView;->setCustomSelectionActionModeCallback(Landroid/view/ActionMode$Callback;)V

    return-void
.end method

.method public setEmojiCompatEnabled(Z)V
    .locals 1

    .line 1
    invoke-direct {p0}, Lb6;->getEmojiTextViewHelper()Lv6;

    move-result-object v0

    invoke-virtual {v0, p1}, Lv6;->e(Z)V

    return-void
.end method

.method public setSupportBackgroundTintList(Landroid/content/res/ColorStateList;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lb6;->b:Lx5;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lx5;->i(Landroid/content/res/ColorStateList;)V

    :cond_0
    return-void
.end method

.method public setSupportBackgroundTintMode(Landroid/graphics/PorterDuff$Mode;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lb6;->b:Lx5;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lx5;->j(Landroid/graphics/PorterDuff$Mode;)V

    :cond_0
    return-void
.end method

.method public setSupportCheckMarkTintList(Landroid/content/res/ColorStateList;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lb6;->a:Lc6;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lc6;->f(Landroid/content/res/ColorStateList;)V

    :cond_0
    return-void
.end method

.method public setSupportCheckMarkTintMode(Landroid/graphics/PorterDuff$Mode;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lb6;->a:Lc6;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lc6;->g(Landroid/graphics/PorterDuff$Mode;)V

    :cond_0
    return-void
.end method

.method public setSupportCompoundDrawablesTintList(Landroid/content/res/ColorStateList;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lb6;->c:Ll7;

    invoke-virtual {v0, p1}, Ll7;->w(Landroid/content/res/ColorStateList;)V

    iget-object p1, p0, Lb6;->c:Ll7;

    invoke-virtual {p1}, Ll7;->b()V

    return-void
.end method

.method public setSupportCompoundDrawablesTintMode(Landroid/graphics/PorterDuff$Mode;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lb6;->c:Ll7;

    invoke-virtual {v0, p1}, Ll7;->x(Landroid/graphics/PorterDuff$Mode;)V

    iget-object p1, p0, Lb6;->c:Ll7;

    invoke-virtual {p1}, Ll7;->b()V

    return-void
.end method

.method public setTextAppearance(Landroid/content/Context;I)V
    .locals 1

    .line 1
    invoke-super {p0, p1, p2}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    iget-object v0, p0, Lb6;->c:Ll7;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1, p2}, Ll7;->q(Landroid/content/Context;I)V

    :cond_0
    return-void
.end method

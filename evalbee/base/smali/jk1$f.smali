.class public Ljk1$f;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Ljk1;->x()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Ljk1;


# direct methods
.method public constructor <init>(Ljk1;)V
    .locals 0

    .line 1
    iput-object p1, p0, Ljk1$f;->a:Ljk1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 0

    .line 1
    const/4 p1, 0x1

    iget-object p2, p0, Ljk1$f;->a:Ljk1;

    if-nez p3, :cond_0

    invoke-static {p2}, Ljk1;->e(Ljk1;)V

    iget-object p2, p0, Ljk1$f;->a:Ljk1;

    invoke-static {p2}, Ljk1;->f(Ljk1;)Landroid/widget/Spinner;

    move-result-object p2

    invoke-virtual {p2, p1}, Landroid/widget/AdapterView;->setSelection(I)V

    iget-object p1, p0, Ljk1$f;->a:Ljk1;

    iget-object p2, p1, Ljk1;->l:Ljava/util/ArrayList;

    const/4 p3, 0x0

    invoke-virtual {p2, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/lang/Double;

    invoke-virtual {p2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide p2

    iput-wide p2, p1, Ljk1;->n:D

    goto :goto_0

    :cond_0
    iget-object p4, p2, Ljk1;->l:Ljava/util/ArrayList;

    sub-int/2addr p3, p1

    invoke-virtual {p4, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Double;

    invoke-virtual {p1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide p3

    iput-wide p3, p2, Ljk1;->n:D

    :goto_0
    return-void
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0

    .line 1
    return-void
.end method

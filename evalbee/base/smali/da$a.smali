.class public final Lda$a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lw01;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lda;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# static fields
.field public static final a:Lda$a;

.field public static final b:Ln00;

.field public static final c:Ln00;

.field public static final d:Ln00;

.field public static final e:Ln00;

.field public static final f:Ln00;

.field public static final g:Ln00;

.field public static final h:Ln00;

.field public static final i:Ln00;

.field public static final j:Ln00;

.field public static final k:Ln00;

.field public static final l:Ln00;

.field public static final m:Ln00;

.field public static final n:Ln00;

.field public static final o:Ln00;

.field public static final p:Ln00;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .line 1
    new-instance v0, Lda$a;

    invoke-direct {v0}, Lda$a;-><init>()V

    sput-object v0, Lda$a;->a:Lda$a;

    const-string v0, "projectNumber"

    invoke-static {v0}, Ln00;->a(Ljava/lang/String;)Ln00$b;

    move-result-object v0

    invoke-static {}, Lcom/google/firebase/encoders/proto/a;->b()Lcom/google/firebase/encoders/proto/a;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/google/firebase/encoders/proto/a;->c(I)Lcom/google/firebase/encoders/proto/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/firebase/encoders/proto/a;->a()Lcom/google/firebase/encoders/proto/Protobuf;

    move-result-object v1

    invoke-virtual {v0, v1}, Ln00$b;->b(Ljava/lang/annotation/Annotation;)Ln00$b;

    move-result-object v0

    invoke-virtual {v0}, Ln00$b;->a()Ln00;

    move-result-object v0

    sput-object v0, Lda$a;->b:Ln00;

    const-string v0, "messageId"

    invoke-static {v0}, Ln00;->a(Ljava/lang/String;)Ln00$b;

    move-result-object v0

    invoke-static {}, Lcom/google/firebase/encoders/proto/a;->b()Lcom/google/firebase/encoders/proto/a;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/google/firebase/encoders/proto/a;->c(I)Lcom/google/firebase/encoders/proto/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/firebase/encoders/proto/a;->a()Lcom/google/firebase/encoders/proto/Protobuf;

    move-result-object v1

    invoke-virtual {v0, v1}, Ln00$b;->b(Ljava/lang/annotation/Annotation;)Ln00$b;

    move-result-object v0

    invoke-virtual {v0}, Ln00$b;->a()Ln00;

    move-result-object v0

    sput-object v0, Lda$a;->c:Ln00;

    const-string v0, "instanceId"

    invoke-static {v0}, Ln00;->a(Ljava/lang/String;)Ln00$b;

    move-result-object v0

    invoke-static {}, Lcom/google/firebase/encoders/proto/a;->b()Lcom/google/firebase/encoders/proto/a;

    move-result-object v1

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Lcom/google/firebase/encoders/proto/a;->c(I)Lcom/google/firebase/encoders/proto/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/firebase/encoders/proto/a;->a()Lcom/google/firebase/encoders/proto/Protobuf;

    move-result-object v1

    invoke-virtual {v0, v1}, Ln00$b;->b(Ljava/lang/annotation/Annotation;)Ln00$b;

    move-result-object v0

    invoke-virtual {v0}, Ln00$b;->a()Ln00;

    move-result-object v0

    sput-object v0, Lda$a;->d:Ln00;

    const-string v0, "messageType"

    invoke-static {v0}, Ln00;->a(Ljava/lang/String;)Ln00$b;

    move-result-object v0

    invoke-static {}, Lcom/google/firebase/encoders/proto/a;->b()Lcom/google/firebase/encoders/proto/a;

    move-result-object v1

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Lcom/google/firebase/encoders/proto/a;->c(I)Lcom/google/firebase/encoders/proto/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/firebase/encoders/proto/a;->a()Lcom/google/firebase/encoders/proto/Protobuf;

    move-result-object v1

    invoke-virtual {v0, v1}, Ln00$b;->b(Ljava/lang/annotation/Annotation;)Ln00$b;

    move-result-object v0

    invoke-virtual {v0}, Ln00$b;->a()Ln00;

    move-result-object v0

    sput-object v0, Lda$a;->e:Ln00;

    const-string v0, "sdkPlatform"

    invoke-static {v0}, Ln00;->a(Ljava/lang/String;)Ln00$b;

    move-result-object v0

    invoke-static {}, Lcom/google/firebase/encoders/proto/a;->b()Lcom/google/firebase/encoders/proto/a;

    move-result-object v1

    const/4 v2, 0x5

    invoke-virtual {v1, v2}, Lcom/google/firebase/encoders/proto/a;->c(I)Lcom/google/firebase/encoders/proto/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/firebase/encoders/proto/a;->a()Lcom/google/firebase/encoders/proto/Protobuf;

    move-result-object v1

    invoke-virtual {v0, v1}, Ln00$b;->b(Ljava/lang/annotation/Annotation;)Ln00$b;

    move-result-object v0

    invoke-virtual {v0}, Ln00$b;->a()Ln00;

    move-result-object v0

    sput-object v0, Lda$a;->f:Ln00;

    const-string v0, "packageName"

    invoke-static {v0}, Ln00;->a(Ljava/lang/String;)Ln00$b;

    move-result-object v0

    invoke-static {}, Lcom/google/firebase/encoders/proto/a;->b()Lcom/google/firebase/encoders/proto/a;

    move-result-object v1

    const/4 v2, 0x6

    invoke-virtual {v1, v2}, Lcom/google/firebase/encoders/proto/a;->c(I)Lcom/google/firebase/encoders/proto/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/firebase/encoders/proto/a;->a()Lcom/google/firebase/encoders/proto/Protobuf;

    move-result-object v1

    invoke-virtual {v0, v1}, Ln00$b;->b(Ljava/lang/annotation/Annotation;)Ln00$b;

    move-result-object v0

    invoke-virtual {v0}, Ln00$b;->a()Ln00;

    move-result-object v0

    sput-object v0, Lda$a;->g:Ln00;

    const-string v0, "collapseKey"

    invoke-static {v0}, Ln00;->a(Ljava/lang/String;)Ln00$b;

    move-result-object v0

    invoke-static {}, Lcom/google/firebase/encoders/proto/a;->b()Lcom/google/firebase/encoders/proto/a;

    move-result-object v1

    const/4 v2, 0x7

    invoke-virtual {v1, v2}, Lcom/google/firebase/encoders/proto/a;->c(I)Lcom/google/firebase/encoders/proto/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/firebase/encoders/proto/a;->a()Lcom/google/firebase/encoders/proto/Protobuf;

    move-result-object v1

    invoke-virtual {v0, v1}, Ln00$b;->b(Ljava/lang/annotation/Annotation;)Ln00$b;

    move-result-object v0

    invoke-virtual {v0}, Ln00$b;->a()Ln00;

    move-result-object v0

    sput-object v0, Lda$a;->h:Ln00;

    const-string v0, "priority"

    invoke-static {v0}, Ln00;->a(Ljava/lang/String;)Ln00$b;

    move-result-object v0

    invoke-static {}, Lcom/google/firebase/encoders/proto/a;->b()Lcom/google/firebase/encoders/proto/a;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/google/firebase/encoders/proto/a;->c(I)Lcom/google/firebase/encoders/proto/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/firebase/encoders/proto/a;->a()Lcom/google/firebase/encoders/proto/Protobuf;

    move-result-object v1

    invoke-virtual {v0, v1}, Ln00$b;->b(Ljava/lang/annotation/Annotation;)Ln00$b;

    move-result-object v0

    invoke-virtual {v0}, Ln00$b;->a()Ln00;

    move-result-object v0

    sput-object v0, Lda$a;->i:Ln00;

    const-string v0, "ttl"

    invoke-static {v0}, Ln00;->a(Ljava/lang/String;)Ln00$b;

    move-result-object v0

    invoke-static {}, Lcom/google/firebase/encoders/proto/a;->b()Lcom/google/firebase/encoders/proto/a;

    move-result-object v1

    const/16 v2, 0x9

    invoke-virtual {v1, v2}, Lcom/google/firebase/encoders/proto/a;->c(I)Lcom/google/firebase/encoders/proto/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/firebase/encoders/proto/a;->a()Lcom/google/firebase/encoders/proto/Protobuf;

    move-result-object v1

    invoke-virtual {v0, v1}, Ln00$b;->b(Ljava/lang/annotation/Annotation;)Ln00$b;

    move-result-object v0

    invoke-virtual {v0}, Ln00$b;->a()Ln00;

    move-result-object v0

    sput-object v0, Lda$a;->j:Ln00;

    const-string v0, "topic"

    invoke-static {v0}, Ln00;->a(Ljava/lang/String;)Ln00$b;

    move-result-object v0

    invoke-static {}, Lcom/google/firebase/encoders/proto/a;->b()Lcom/google/firebase/encoders/proto/a;

    move-result-object v1

    const/16 v2, 0xa

    invoke-virtual {v1, v2}, Lcom/google/firebase/encoders/proto/a;->c(I)Lcom/google/firebase/encoders/proto/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/firebase/encoders/proto/a;->a()Lcom/google/firebase/encoders/proto/Protobuf;

    move-result-object v1

    invoke-virtual {v0, v1}, Ln00$b;->b(Ljava/lang/annotation/Annotation;)Ln00$b;

    move-result-object v0

    invoke-virtual {v0}, Ln00$b;->a()Ln00;

    move-result-object v0

    sput-object v0, Lda$a;->k:Ln00;

    const-string v0, "bulkId"

    invoke-static {v0}, Ln00;->a(Ljava/lang/String;)Ln00$b;

    move-result-object v0

    invoke-static {}, Lcom/google/firebase/encoders/proto/a;->b()Lcom/google/firebase/encoders/proto/a;

    move-result-object v1

    const/16 v2, 0xb

    invoke-virtual {v1, v2}, Lcom/google/firebase/encoders/proto/a;->c(I)Lcom/google/firebase/encoders/proto/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/firebase/encoders/proto/a;->a()Lcom/google/firebase/encoders/proto/Protobuf;

    move-result-object v1

    invoke-virtual {v0, v1}, Ln00$b;->b(Ljava/lang/annotation/Annotation;)Ln00$b;

    move-result-object v0

    invoke-virtual {v0}, Ln00$b;->a()Ln00;

    move-result-object v0

    sput-object v0, Lda$a;->l:Ln00;

    const-string v0, "event"

    invoke-static {v0}, Ln00;->a(Ljava/lang/String;)Ln00$b;

    move-result-object v0

    invoke-static {}, Lcom/google/firebase/encoders/proto/a;->b()Lcom/google/firebase/encoders/proto/a;

    move-result-object v1

    const/16 v2, 0xc

    invoke-virtual {v1, v2}, Lcom/google/firebase/encoders/proto/a;->c(I)Lcom/google/firebase/encoders/proto/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/firebase/encoders/proto/a;->a()Lcom/google/firebase/encoders/proto/Protobuf;

    move-result-object v1

    invoke-virtual {v0, v1}, Ln00$b;->b(Ljava/lang/annotation/Annotation;)Ln00$b;

    move-result-object v0

    invoke-virtual {v0}, Ln00$b;->a()Ln00;

    move-result-object v0

    sput-object v0, Lda$a;->m:Ln00;

    const-string v0, "analyticsLabel"

    invoke-static {v0}, Ln00;->a(Ljava/lang/String;)Ln00$b;

    move-result-object v0

    invoke-static {}, Lcom/google/firebase/encoders/proto/a;->b()Lcom/google/firebase/encoders/proto/a;

    move-result-object v1

    const/16 v2, 0xd

    invoke-virtual {v1, v2}, Lcom/google/firebase/encoders/proto/a;->c(I)Lcom/google/firebase/encoders/proto/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/firebase/encoders/proto/a;->a()Lcom/google/firebase/encoders/proto/Protobuf;

    move-result-object v1

    invoke-virtual {v0, v1}, Ln00$b;->b(Ljava/lang/annotation/Annotation;)Ln00$b;

    move-result-object v0

    invoke-virtual {v0}, Ln00$b;->a()Ln00;

    move-result-object v0

    sput-object v0, Lda$a;->n:Ln00;

    const-string v0, "campaignId"

    invoke-static {v0}, Ln00;->a(Ljava/lang/String;)Ln00$b;

    move-result-object v0

    invoke-static {}, Lcom/google/firebase/encoders/proto/a;->b()Lcom/google/firebase/encoders/proto/a;

    move-result-object v1

    const/16 v2, 0xe

    invoke-virtual {v1, v2}, Lcom/google/firebase/encoders/proto/a;->c(I)Lcom/google/firebase/encoders/proto/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/firebase/encoders/proto/a;->a()Lcom/google/firebase/encoders/proto/Protobuf;

    move-result-object v1

    invoke-virtual {v0, v1}, Ln00$b;->b(Ljava/lang/annotation/Annotation;)Ln00$b;

    move-result-object v0

    invoke-virtual {v0}, Ln00$b;->a()Ln00;

    move-result-object v0

    sput-object v0, Lda$a;->o:Ln00;

    const-string v0, "composerLabel"

    invoke-static {v0}, Ln00;->a(Ljava/lang/String;)Ln00$b;

    move-result-object v0

    invoke-static {}, Lcom/google/firebase/encoders/proto/a;->b()Lcom/google/firebase/encoders/proto/a;

    move-result-object v1

    const/16 v2, 0xf

    invoke-virtual {v1, v2}, Lcom/google/firebase/encoders/proto/a;->c(I)Lcom/google/firebase/encoders/proto/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/firebase/encoders/proto/a;->a()Lcom/google/firebase/encoders/proto/Protobuf;

    move-result-object v1

    invoke-virtual {v0, v1}, Ln00$b;->b(Ljava/lang/annotation/Annotation;)Ln00$b;

    move-result-object v0

    invoke-virtual {v0}, Ln00$b;->a()Ln00;

    move-result-object v0

    sput-object v0, Lda$a;->p:Ln00;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/google/firebase/messaging/reporting/MessagingClientEvent;Lx01;)V
    .locals 3

    .line 1
    sget-object v0, Lda$a;->b:Ln00;

    invoke-virtual {p1}, Lcom/google/firebase/messaging/reporting/MessagingClientEvent;->l()J

    move-result-wide v1

    invoke-interface {p2, v0, v1, v2}, Lx01;->e(Ln00;J)Lx01;

    sget-object v0, Lda$a;->c:Ln00;

    invoke-virtual {p1}, Lcom/google/firebase/messaging/reporting/MessagingClientEvent;->h()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2, v0, v1}, Lx01;->f(Ln00;Ljava/lang/Object;)Lx01;

    sget-object v0, Lda$a;->d:Ln00;

    invoke-virtual {p1}, Lcom/google/firebase/messaging/reporting/MessagingClientEvent;->g()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2, v0, v1}, Lx01;->f(Ln00;Ljava/lang/Object;)Lx01;

    sget-object v0, Lda$a;->e:Ln00;

    invoke-virtual {p1}, Lcom/google/firebase/messaging/reporting/MessagingClientEvent;->i()Lcom/google/firebase/messaging/reporting/MessagingClientEvent$MessageType;

    move-result-object v1

    invoke-interface {p2, v0, v1}, Lx01;->f(Ln00;Ljava/lang/Object;)Lx01;

    sget-object v0, Lda$a;->f:Ln00;

    invoke-virtual {p1}, Lcom/google/firebase/messaging/reporting/MessagingClientEvent;->m()Lcom/google/firebase/messaging/reporting/MessagingClientEvent$SDKPlatform;

    move-result-object v1

    invoke-interface {p2, v0, v1}, Lx01;->f(Ln00;Ljava/lang/Object;)Lx01;

    sget-object v0, Lda$a;->g:Ln00;

    invoke-virtual {p1}, Lcom/google/firebase/messaging/reporting/MessagingClientEvent;->j()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2, v0, v1}, Lx01;->f(Ln00;Ljava/lang/Object;)Lx01;

    sget-object v0, Lda$a;->h:Ln00;

    invoke-virtual {p1}, Lcom/google/firebase/messaging/reporting/MessagingClientEvent;->d()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2, v0, v1}, Lx01;->f(Ln00;Ljava/lang/Object;)Lx01;

    sget-object v0, Lda$a;->i:Ln00;

    invoke-virtual {p1}, Lcom/google/firebase/messaging/reporting/MessagingClientEvent;->k()I

    move-result v1

    invoke-interface {p2, v0, v1}, Lx01;->c(Ln00;I)Lx01;

    sget-object v0, Lda$a;->j:Ln00;

    invoke-virtual {p1}, Lcom/google/firebase/messaging/reporting/MessagingClientEvent;->o()I

    move-result v1

    invoke-interface {p2, v0, v1}, Lx01;->c(Ln00;I)Lx01;

    sget-object v0, Lda$a;->k:Ln00;

    invoke-virtual {p1}, Lcom/google/firebase/messaging/reporting/MessagingClientEvent;->n()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2, v0, v1}, Lx01;->f(Ln00;Ljava/lang/Object;)Lx01;

    sget-object v0, Lda$a;->l:Ln00;

    invoke-virtual {p1}, Lcom/google/firebase/messaging/reporting/MessagingClientEvent;->b()J

    move-result-wide v1

    invoke-interface {p2, v0, v1, v2}, Lx01;->e(Ln00;J)Lx01;

    sget-object v0, Lda$a;->m:Ln00;

    invoke-virtual {p1}, Lcom/google/firebase/messaging/reporting/MessagingClientEvent;->f()Lcom/google/firebase/messaging/reporting/MessagingClientEvent$Event;

    move-result-object v1

    invoke-interface {p2, v0, v1}, Lx01;->f(Ln00;Ljava/lang/Object;)Lx01;

    sget-object v0, Lda$a;->n:Ln00;

    invoke-virtual {p1}, Lcom/google/firebase/messaging/reporting/MessagingClientEvent;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2, v0, v1}, Lx01;->f(Ln00;Ljava/lang/Object;)Lx01;

    sget-object v0, Lda$a;->o:Ln00;

    invoke-virtual {p1}, Lcom/google/firebase/messaging/reporting/MessagingClientEvent;->c()J

    move-result-wide v1

    invoke-interface {p2, v0, v1, v2}, Lx01;->e(Ln00;J)Lx01;

    sget-object v0, Lda$a;->p:Ln00;

    invoke-virtual {p1}, Lcom/google/firebase/messaging/reporting/MessagingClientEvent;->e()Ljava/lang/String;

    move-result-object p1

    invoke-interface {p2, v0, p1}, Lx01;->f(Ln00;Ljava/lang/Object;)Lx01;

    return-void
.end method

.method public bridge synthetic encode(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .line 1
    check-cast p1, Lcom/google/firebase/messaging/reporting/MessagingClientEvent;

    check-cast p2, Lx01;

    invoke-virtual {p0, p1, p2}, Lda$a;->a(Lcom/google/firebase/messaging/reporting/MessagingClientEvent;Lx01;)V

    return-void
.end method

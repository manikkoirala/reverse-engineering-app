.class public Lt3$a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lt3;->b()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Lcom/google/android/material/textfield/TextInputLayout;

.field public final synthetic b:Landroidx/appcompat/app/a;

.field public final synthetic c:Lt3;


# direct methods
.method public constructor <init>(Lt3;Lcom/google/android/material/textfield/TextInputLayout;Landroidx/appcompat/app/a;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lt3$a;->c:Lt3;

    iput-object p2, p0, Lt3$a;->a:Lcom/google/android/material/textfield/TextInputLayout;

    iput-object p3, p0, Lt3$a;->b:Landroidx/appcompat/app/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .line 1
    iget-object p1, p0, Lt3$a;->a:Lcom/google/android/material/textfield/TextInputLayout;

    invoke-virtual {p1}, Lcom/google/android/material/textfield/TextInputLayout;->getEditText()Landroid/widget/EditText;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p1

    const-string v0, ""

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lt3$a;->c:Lt3;

    iget-object p1, p1, Lt3;->a:Landroid/content/Context;

    const v0, 0x7f0800bd

    const v1, 0x7f08016e

    const v2, 0x7f1200b6

    invoke-static {p1, v2, v0, v1}, La91;->G(Landroid/content/Context;III)V

    return-void

    :cond_0
    new-instance p1, Lcom/ekodroid/omrevaluator/database/ClassDataModel;

    iget-object v0, p0, Lt3$a;->a:Lcom/google/android/material/textfield/TextInputLayout;

    invoke-virtual {v0}, Lcom/google/android/material/textfield/TextInputLayout;->getEditText()Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-direct {p1, v0, v1, v2}, Lcom/ekodroid/omrevaluator/database/ClassDataModel;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    iget-object v0, p0, Lt3$a;->c:Lt3;

    iget-object v0, v0, Lt3;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/ekodroid/omrevaluator/database/repositories/ClassRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/ClassRepository;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/ekodroid/omrevaluator/database/repositories/ClassRepository;->saveOrUpdateClass(Lcom/ekodroid/omrevaluator/database/ClassDataModel;)Z

    iget-object p1, p0, Lt3$a;->c:Lt3;

    iget-object p1, p1, Lt3;->a:Landroid/content/Context;

    invoke-static {p1, v2}, Lcom/ekodroid/omrevaluator/students/services/SyncClassListService;->c(Landroid/content/Context;Z)V

    iget-object p1, p0, Lt3$a;->c:Lt3;

    invoke-static {p1}, Lt3;->a(Lt3;)Ly01;

    move-result-object p1

    invoke-interface {p1, v1}, Ly01;->a(Ljava/lang/Object;)V

    iget-object p1, p0, Lt3$a;->b:Landroidx/appcompat/app/a;

    invoke-virtual {p1}, Lq6;->dismiss()V

    return-void
.end method

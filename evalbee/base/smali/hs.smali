.class public final Lhs;
.super Lwx0;
.source "SourceFile"


# direct methods
.method public constructor <init>(Ldu;Lh71;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lwx0;-><init>(Ldu;Lh71;)V

    return-void
.end method


# virtual methods
.method public a(Lcom/google/firebase/firestore/model/MutableDocument;Lq00;Lpw1;)Lq00;
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lwx0;->n(Lcom/google/firebase/firestore/model/MutableDocument;)V

    invoke-virtual {p0}, Lwx0;->h()Lh71;

    move-result-object p3

    invoke-virtual {p3, p1}, Lh71;->e(Lcom/google/firebase/firestore/model/MutableDocument;)Z

    move-result p3

    if-eqz p3, :cond_0

    invoke-virtual {p1}, Lcom/google/firebase/firestore/model/MutableDocument;->getVersion()Lqo1;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/google/firebase/firestore/model/MutableDocument;->m(Lqo1;)Lcom/google/firebase/firestore/model/MutableDocument;

    move-result-object p1

    invoke-virtual {p1}, Lcom/google/firebase/firestore/model/MutableDocument;->u()Lcom/google/firebase/firestore/model/MutableDocument;

    const/4 p1, 0x0

    return-object p1

    :cond_0
    return-object p2
.end method

.method public b(Lcom/google/firebase/firestore/model/MutableDocument;Lay0;)V
    .locals 3

    .line 1
    invoke-virtual {p0, p1}, Lwx0;->n(Lcom/google/firebase/firestore/model/MutableDocument;)V

    invoke-virtual {p2}, Lay0;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "Transform results received by DeleteMutation."

    invoke-static {v0, v2, v1}, Lg9;->d(ZLjava/lang/String;[Ljava/lang/Object;)V

    invoke-virtual {p2}, Lay0;->b()Lqo1;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/google/firebase/firestore/model/MutableDocument;->m(Lqo1;)Lcom/google/firebase/firestore/model/MutableDocument;

    move-result-object p1

    invoke-virtual {p1}, Lcom/google/firebase/firestore/model/MutableDocument;->t()Lcom/google/firebase/firestore/model/MutableDocument;

    return-void
.end method

.method public e()Lq00;
    .locals 1

    .line 1
    const/4 v0, 0x0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .line 1
    if-ne p0, p1, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_0
    if-eqz p1, :cond_2

    const-class v0, Lhs;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    if-eq v0, v1, :cond_1

    goto :goto_0

    :cond_1
    check-cast p1, Lhs;

    invoke-virtual {p0, p1}, Lwx0;->i(Lwx0;)Z

    move-result p1

    return p1

    :cond_2
    :goto_0
    const/4 p1, 0x0

    return p1
.end method

.method public hashCode()I
    .locals 1

    .line 1
    invoke-virtual {p0}, Lwx0;->j()I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "DeleteMutation{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lwx0;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

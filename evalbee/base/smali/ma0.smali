.class public Lma0;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:Lee1;

.field public b:Lzg;

.field public c:Landroid/content/Context;

.field public d:I

.field public e:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lzg;)V
    .locals 2

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "https://"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, La91;->w()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ":8759/excel_count"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lma0;->e:Ljava/lang/String;

    iput-object p1, p0, Lma0;->c:Landroid/content/Context;

    new-instance v0, Ln52;

    invoke-static {}, La91;->u()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Ln52;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {v0}, Ln52;->b()Lee1;

    move-result-object p1

    iput-object p1, p0, Lma0;->a:Lee1;

    iput-object p2, p0, Lma0;->b:Lzg;

    invoke-virtual {p0}, Lma0;->e()V

    return-void
.end method

.method public static synthetic a(Lma0;Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lma0;->c(Ljava/lang/String;)V

    return-void
.end method

.method public static synthetic b(Lma0;ZILjava/lang/Object;)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1, p2, p3}, Lma0;->d(ZILjava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public final c(Ljava/lang/String;)V
    .locals 8

    .line 1
    new-instance v7, Lma0$d;

    const/4 v2, 0x0

    iget-object v3, p0, Lma0;->e:Ljava/lang/String;

    new-instance v4, Lma0$b;

    invoke-direct {v4, p0}, Lma0$b;-><init>(Lma0;)V

    new-instance v5, Lma0$c;

    invoke-direct {v5, p0}, Lma0$c;-><init>(Lma0;)V

    move-object v0, v7

    move-object v1, p0

    move-object v6, p1

    invoke-direct/range {v0 .. v6}, Lma0$d;-><init>(Lma0;ILjava/lang/String;Lcom/android/volley/d$b;Lcom/android/volley/d$a;Ljava/lang/String;)V

    new-instance p1, Lwq;

    const/4 v0, 0x0

    const/high16 v1, 0x3f800000    # 1.0f

    const v2, 0x9c40

    invoke-direct {p1, v2, v0, v1}, Lwq;-><init>(IIF)V

    invoke-virtual {v7, p1}, Lcom/android/volley/Request;->L(Ljf1;)Lcom/android/volley/Request;

    iget-object p1, p0, Lma0;->a:Lee1;

    invoke-virtual {p1, v7}, Lee1;->a(Lcom/android/volley/Request;)Lcom/android/volley/Request;

    return-void
.end method

.method public final d(ZILjava/lang/Object;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lma0;->b:Lzg;

    if-eqz v0, :cond_0

    invoke-interface {v0, p1, p2, p3}, Lzg;->a(ZILjava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public final e()V
    .locals 2

    .line 1
    invoke-static {}, Lcom/google/firebase/auth/FirebaseAuth;->getInstance()Lcom/google/firebase/auth/FirebaseAuth;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/firebase/auth/FirebaseAuth;->e()Lr30;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lr30;->i(Z)Lcom/google/android/gms/tasks/Task;

    move-result-object v0

    new-instance v1, Lma0$a;

    invoke-direct {v1, p0}, Lma0$a;-><init>(Lma0;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/tasks/Task;->addOnCompleteListener(Lcom/google/android/gms/tasks/OnCompleteListener;)Lcom/google/android/gms/tasks/Task;

    return-void
.end method

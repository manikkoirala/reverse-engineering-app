.class public final Lz01;
.super Lhz1;
.source "SourceFile"


# static fields
.field public static final c:Liz1;


# instance fields
.field public final a:Lgc0;

.field public final b:Lvw1;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .line 1
    sget-object v0, Lcom/google/gson/ToNumberPolicy;->DOUBLE:Lcom/google/gson/ToNumberPolicy;

    invoke-static {v0}, Lz01;->f(Lvw1;)Liz1;

    move-result-object v0

    sput-object v0, Lz01;->c:Liz1;

    return-void
.end method

.method public constructor <init>(Lgc0;Lvw1;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lhz1;-><init>()V

    iput-object p1, p0, Lz01;->a:Lgc0;

    iput-object p2, p0, Lz01;->b:Lvw1;

    return-void
.end method

.method public synthetic constructor <init>(Lgc0;Lvw1;Lz01$a;)V
    .locals 0

    .line 2
    invoke-direct {p0, p1, p2}, Lz01;-><init>(Lgc0;Lvw1;)V

    return-void
.end method

.method public static e(Lvw1;)Liz1;
    .locals 1

    .line 1
    sget-object v0, Lcom/google/gson/ToNumberPolicy;->DOUBLE:Lcom/google/gson/ToNumberPolicy;

    if-ne p0, v0, :cond_0

    sget-object p0, Lz01;->c:Liz1;

    return-object p0

    :cond_0
    invoke-static {p0}, Lz01;->f(Lvw1;)Liz1;

    move-result-object p0

    return-object p0
.end method

.method public static f(Lvw1;)Liz1;
    .locals 1

    .line 1
    new-instance v0, Lz01$a;

    invoke-direct {v0, p0}, Lz01$a;-><init>(Lvw1;)V

    return-object v0
.end method


# virtual methods
.method public b(Lrh0;)Ljava/lang/Object;
    .locals 6

    .line 1
    invoke-virtual {p1}, Lrh0;->o0()Lcom/google/gson/stream/JsonToken;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lz01;->h(Lrh0;Lcom/google/gson/stream/JsonToken;)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-virtual {p0, p1, v0}, Lz01;->g(Lrh0;Lcom/google/gson/stream/JsonToken;)Ljava/lang/Object;

    move-result-object p1

    return-object p1

    :cond_0
    new-instance v0, Ljava/util/ArrayDeque;

    invoke-direct {v0}, Ljava/util/ArrayDeque;-><init>()V

    :cond_1
    :goto_0
    invoke-virtual {p1}, Lrh0;->k()Z

    move-result v2

    if-eqz v2, :cond_6

    instance-of v2, v1, Ljava/util/Map;

    if-eqz v2, :cond_2

    invoke-virtual {p1}, Lrh0;->K()Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    :cond_2
    const/4 v2, 0x0

    :goto_1
    invoke-virtual {p1}, Lrh0;->o0()Lcom/google/gson/stream/JsonToken;

    move-result-object v3

    invoke-virtual {p0, p1, v3}, Lz01;->h(Lrh0;Lcom/google/gson/stream/JsonToken;)Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_3

    const/4 v5, 0x1

    goto :goto_2

    :cond_3
    const/4 v5, 0x0

    :goto_2
    if-nez v4, :cond_4

    invoke-virtual {p0, p1, v3}, Lz01;->g(Lrh0;Lcom/google/gson/stream/JsonToken;)Ljava/lang/Object;

    move-result-object v4

    :cond_4
    instance-of v3, v1, Ljava/util/List;

    if-eqz v3, :cond_5

    move-object v2, v1

    check-cast v2, Ljava/util/List;

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_5
    move-object v3, v1

    check-cast v3, Ljava/util/Map;

    invoke-interface {v3, v2, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_3
    if-eqz v5, :cond_1

    invoke-interface {v0, v1}, Ljava/util/Deque;->addLast(Ljava/lang/Object;)V

    move-object v1, v4

    goto :goto_0

    :cond_6
    instance-of v2, v1, Ljava/util/List;

    if-eqz v2, :cond_7

    invoke-virtual {p1}, Lrh0;->f()V

    goto :goto_4

    :cond_7
    invoke-virtual {p1}, Lrh0;->g()V

    :goto_4
    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_8

    return-object v1

    :cond_8
    invoke-interface {v0}, Ljava/util/Deque;->removeLast()Ljava/lang/Object;

    move-result-object v1

    goto :goto_0
.end method

.method public d(Lvh0;Ljava/lang/Object;)V
    .locals 2

    .line 1
    if-nez p2, :cond_0

    invoke-virtual {p1}, Lvh0;->u()Lvh0;

    return-void

    :cond_0
    iget-object v0, p0, Lz01;->a:Lgc0;

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgc0;->m(Ljava/lang/Class;)Lhz1;

    move-result-object v0

    instance-of v1, v0, Lz01;

    if-eqz v1, :cond_1

    invoke-virtual {p1}, Lvh0;->d()Lvh0;

    invoke-virtual {p1}, Lvh0;->g()Lvh0;

    return-void

    :cond_1
    invoke-virtual {v0, p1, p2}, Lhz1;->d(Lvh0;Ljava/lang/Object;)V

    return-void
.end method

.method public final g(Lrh0;Lcom/google/gson/stream/JsonToken;)Ljava/lang/Object;
    .locals 2

    .line 1
    sget-object v0, Lz01$b;->a:[I

    invoke-virtual {p2}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x3

    if-eq v0, v1, :cond_3

    const/4 v1, 0x4

    if-eq v0, v1, :cond_2

    const/4 v1, 0x5

    if-eq v0, v1, :cond_1

    const/4 v1, 0x6

    if-ne v0, v1, :cond_0

    invoke-virtual {p1}, Lrh0;->R()V

    const/4 p1, 0x0

    return-object p1

    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unexpected token: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    invoke-virtual {p1}, Lrh0;->x()Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    return-object p1

    :cond_2
    iget-object p2, p0, Lz01;->b:Lvw1;

    invoke-interface {p2, p1}, Lvw1;->readNumber(Lrh0;)Ljava/lang/Number;

    move-result-object p1

    return-object p1

    :cond_3
    invoke-virtual {p1}, Lrh0;->i0()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public final h(Lrh0;Lcom/google/gson/stream/JsonToken;)Ljava/lang/Object;
    .locals 1

    .line 1
    sget-object v0, Lz01$b;->a:[I

    invoke-virtual {p2}, Ljava/lang/Enum;->ordinal()I

    move-result p2

    aget p2, v0, p2

    const/4 v0, 0x1

    if-eq p2, v0, :cond_1

    const/4 v0, 0x2

    if-eq p2, v0, :cond_0

    const/4 p1, 0x0

    return-object p1

    :cond_0
    invoke-virtual {p1}, Lrh0;->b()V

    new-instance p1, Lcom/google/gson/internal/LinkedTreeMap;

    invoke-direct {p1}, Lcom/google/gson/internal/LinkedTreeMap;-><init>()V

    return-object p1

    :cond_1
    invoke-virtual {p1}, Lrh0;->a()V

    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    return-object p1
.end method

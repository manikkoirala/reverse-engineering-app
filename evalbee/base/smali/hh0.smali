.class public final Lhh0;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Liz1;


# instance fields
.field public final a:Lbl;


# direct methods
.method public constructor <init>(Lbl;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lhh0;->a:Lbl;

    return-void
.end method


# virtual methods
.method public a(Lgc0;Lcom/google/gson/reflect/TypeToken;)Lhz1;
    .locals 2

    .line 1
    invoke-virtual {p2}, Lcom/google/gson/reflect/TypeToken;->getRawType()Ljava/lang/Class;

    move-result-object v0

    const-class v1, Lgh0;

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getAnnotation(Ljava/lang/Class;)Ljava/lang/annotation/Annotation;

    move-result-object v0

    check-cast v0, Lgh0;

    if-nez v0, :cond_0

    const/4 p1, 0x0

    return-object p1

    :cond_0
    iget-object v1, p0, Lhh0;->a:Lbl;

    invoke-virtual {p0, v1, p1, p2, v0}, Lhh0;->b(Lbl;Lgc0;Lcom/google/gson/reflect/TypeToken;Lgh0;)Lhz1;

    move-result-object p1

    return-object p1
.end method

.method public b(Lbl;Lgc0;Lcom/google/gson/reflect/TypeToken;Lgh0;)Lhz1;
    .locals 1

    .line 1
    invoke-interface {p4}, Lgh0;->value()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0}, Lcom/google/gson/reflect/TypeToken;->get(Ljava/lang/Class;)Lcom/google/gson/reflect/TypeToken;

    move-result-object v0

    invoke-virtual {p1, v0}, Lbl;->b(Lcom/google/gson/reflect/TypeToken;)Lu01;

    move-result-object p1

    invoke-interface {p1}, Lu01;->a()Ljava/lang/Object;

    move-result-object p1

    invoke-interface {p4}, Lgh0;->nullSafe()Z

    move-result p4

    instance-of v0, p1, Lhz1;

    if-eqz v0, :cond_0

    check-cast p1, Lhz1;

    goto :goto_0

    :cond_0
    instance-of v0, p1, Liz1;

    if-eqz v0, :cond_2

    check-cast p1, Liz1;

    invoke-interface {p1, p2, p3}, Liz1;->a(Lgc0;Lcom/google/gson/reflect/TypeToken;)Lhz1;

    move-result-object p1

    :goto_0
    if-eqz p1, :cond_1

    if-eqz p4, :cond_1

    invoke-virtual {p1}, Lhz1;->a()Lhz1;

    move-result-object p1

    :cond_1
    return-object p1

    :cond_2
    new-instance p2, Ljava/lang/IllegalArgumentException;

    new-instance p4, Ljava/lang/StringBuilder;

    invoke-direct {p4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Invalid attempt to bind an instance of "

    invoke-virtual {p4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, " as a @JsonAdapter for "

    invoke-virtual {p4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Lcom/google/gson/reflect/TypeToken;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, ". @JsonAdapter value must be a TypeAdapter, TypeAdapterFactory, JsonSerializer or JsonDeserializer."

    invoke-virtual {p4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p2
.end method

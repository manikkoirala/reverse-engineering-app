.class public Lw30$b;
.super Lio/grpc/ForwardingClientCall;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lw30;->g(Lio/grpc/MethodDescriptor;Lpe0;)Lio/grpc/ClientCall;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:[Lio/grpc/ClientCall;

.field public final synthetic b:Lcom/google/android/gms/tasks/Task;

.field public final synthetic c:Lw30;


# direct methods
.method public constructor <init>(Lw30;[Lio/grpc/ClientCall;Lcom/google/android/gms/tasks/Task;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lw30$b;->c:Lw30;

    iput-object p2, p0, Lw30$b;->a:[Lio/grpc/ClientCall;

    iput-object p3, p0, Lw30$b;->b:Lcom/google/android/gms/tasks/Task;

    invoke-direct {p0}, Lio/grpc/ForwardingClientCall;-><init>()V

    return-void
.end method


# virtual methods
.method public delegate()Lio/grpc/ClientCall;
    .locals 4

    .line 1
    iget-object v0, p0, Lw30$b;->a:[Lio/grpc/ClientCall;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    move v0, v1

    :goto_0
    const-string v2, "ClientCall used before onOpen() callback"

    new-array v3, v1, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Lg9;->d(ZLjava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lw30$b;->a:[Lio/grpc/ClientCall;

    aget-object v0, v0, v1

    return-object v0
.end method

.method public halfClose()V
    .locals 3

    .line 1
    iget-object v0, p0, Lw30$b;->a:[Lio/grpc/ClientCall;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    if-nez v0, :cond_0

    iget-object v0, p0, Lw30$b;->b:Lcom/google/android/gms/tasks/Task;

    iget-object v1, p0, Lw30$b;->c:Lw30;

    invoke-static {v1}, Lw30;->b(Lw30;)Lcom/google/firebase/firestore/util/AsyncQueue;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/firebase/firestore/util/AsyncQueue;->j()Ljava/util/concurrent/Executor;

    move-result-object v1

    new-instance v2, Lx30;

    invoke-direct {v2}, Lx30;-><init>()V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/tasks/Task;->addOnSuccessListener(Ljava/util/concurrent/Executor;Lcom/google/android/gms/tasks/OnSuccessListener;)Lcom/google/android/gms/tasks/Task;

    goto :goto_0

    :cond_0
    invoke-super {p0}, Lio/grpc/ForwardingClientCall;->halfClose()V

    :goto_0
    return-void
.end method

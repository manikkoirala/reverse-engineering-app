.class public final Lmj2;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator<",
            "Lmj2;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:Lna2;

.field public b:Laj2;

.field public c:Llf2;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .line 1
    new-instance v0, Lvj2;

    invoke-direct {v0}, Lvj2;-><init>()V

    sput-object v0, Lmj2;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Lna2;)V
    .locals 6

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/google/android/gms/common/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lna2;

    iput-object v0, p0, Lmj2;->a:Lna2;

    invoke-virtual {v0}, Lna2;->z0()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    iput-object v1, p0, Lmj2;->b:Laj2;

    const/4 v1, 0x0

    :goto_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ltj2;

    invoke-virtual {v2}, Ltj2;->zza()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    new-instance v2, Laj2;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ltj2;

    invoke-virtual {v3}, Ltj2;->b()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ltj2;

    invoke-virtual {v4}, Ltj2;->zza()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1}, Lna2;->A0()Z

    move-result v5

    invoke-direct {v2, v3, v4, v5}, Laj2;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    iput-object v2, p0, Lmj2;->b:Laj2;

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lmj2;->b:Laj2;

    if-nez v0, :cond_2

    new-instance v0, Laj2;

    invoke-virtual {p1}, Lna2;->A0()Z

    move-result v1

    invoke-direct {v0, v1}, Laj2;-><init>(Z)V

    iput-object v0, p0, Lmj2;->b:Laj2;

    :cond_2
    invoke-virtual {p1}, Lna2;->y0()Llf2;

    move-result-object p1

    iput-object p1, p0, Lmj2;->c:Llf2;

    return-void
.end method

.method public constructor <init>(Lna2;Laj2;Llf2;)V
    .locals 0

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lmj2;->a:Lna2;

    iput-object p2, p0, Lmj2;->b:Laj2;

    iput-object p3, p0, Lmj2;->c:Llf2;

    return-void
.end method


# virtual methods
.method public final b()Lx3;
    .locals 1

    .line 1
    iget-object v0, p0, Lmj2;->b:Laj2;

    return-object v0
.end method

.method public final c()Lr30;
    .locals 1

    .line 1
    iget-object v0, p0, Lmj2;->a:Lna2;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .line 1
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 4

    .line 1
    invoke-static {p1}, Lcom/google/android/gms/common/internal/safeparcel/SafeParcelWriter;->beginObjectHeader(Landroid/os/Parcel;)I

    move-result v0

    invoke-virtual {p0}, Lmj2;->c()Lr30;

    move-result-object v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-static {p1, v2, v1, p2, v3}, Lcom/google/android/gms/common/internal/safeparcel/SafeParcelWriter;->writeParcelable(Landroid/os/Parcel;ILandroid/os/Parcelable;IZ)V

    const/4 v1, 0x2

    invoke-virtual {p0}, Lmj2;->b()Lx3;

    move-result-object v2

    invoke-static {p1, v1, v2, p2, v3}, Lcom/google/android/gms/common/internal/safeparcel/SafeParcelWriter;->writeParcelable(Landroid/os/Parcel;ILandroid/os/Parcelable;IZ)V

    const/4 v1, 0x3

    iget-object v2, p0, Lmj2;->c:Llf2;

    invoke-static {p1, v1, v2, p2, v3}, Lcom/google/android/gms/common/internal/safeparcel/SafeParcelWriter;->writeParcelable(Landroid/os/Parcel;ILandroid/os/Parcelable;IZ)V

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/safeparcel/SafeParcelWriter;->finishObjectHeader(Landroid/os/Parcel;I)V

    return-void
.end method

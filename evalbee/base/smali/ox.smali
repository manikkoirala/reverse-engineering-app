.class public final Lox;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lpx;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lox$a;
    }
.end annotation


# static fields
.field public static final b:Lox$a;


# instance fields
.field public final a:Lr91;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lox$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lox$a;-><init>(Lgq;)V

    sput-object v0, Lox;->b:Lox$a;

    return-void
.end method

.method public constructor <init>(Lr91;)V
    .locals 1

    .line 1
    const-string v0, "transportFactoryProvider"

    invoke-static {p1, v0}, Lfg0;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lox;->a:Lr91;

    return-void
.end method

.method public static synthetic b(Lox;Lxl1;)[B
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lox;->c(Lxl1;)[B

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public a(Lxl1;)V
    .locals 5

    .line 1
    const-string v0, "sessionEvent"

    invoke-static {p1, v0}, Lfg0;->e(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Lox;->a:Lr91;

    invoke-interface {v0}, Lr91;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/datatransport/TransportFactory;

    const-string v1, "json"

    invoke-static {v1}, Lcom/google/android/datatransport/Encoding;->of(Ljava/lang/String;)Lcom/google/android/datatransport/Encoding;

    move-result-object v1

    new-instance v2, Lnx;

    invoke-direct {v2, p0}, Lnx;-><init>(Lox;)V

    const-string v3, "FIREBASE_APPQUALITY_SESSION"

    const-class v4, Lxl1;

    invoke-interface {v0, v3, v4, v1, v2}, Lcom/google/android/datatransport/TransportFactory;->getTransport(Ljava/lang/String;Ljava/lang/Class;Lcom/google/android/datatransport/Encoding;Lcom/google/android/datatransport/Transformer;)Lcom/google/android/datatransport/Transport;

    move-result-object v0

    invoke-static {p1}, Lcom/google/android/datatransport/Event;->ofData(Ljava/lang/Object;)Lcom/google/android/datatransport/Event;

    move-result-object p1

    invoke-interface {v0, p1}, Lcom/google/android/datatransport/Transport;->send(Lcom/google/android/datatransport/Event;)V

    return-void
.end method

.method public final c(Lxl1;)[B
    .locals 2

    .line 1
    sget-object v0, Lyl1;->a:Lyl1;

    invoke-virtual {v0}, Lyl1;->c()Lhp;

    move-result-object v0

    invoke-interface {v0, p1}, Lhp;->encode(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    const-string v0, "SessionEvents.SESSION_EVENT_ENCODER.encode(value)"

    invoke-static {p1, v0}, Lfg0;->d(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Session Event: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "EventGDTLogger"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v0, Ljg;->b:Ljava/nio/charset/Charset;

    invoke-virtual {p1, v0}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object p1

    const-string v0, "this as java.lang.String).getBytes(charset)"

    invoke-static {p1, v0}, Lfg0;->d(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

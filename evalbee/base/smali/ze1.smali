.class public Lze1;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lze1$b;
    }
.end annotation


# instance fields
.field public a:Ly01;

.field public b:Ltd1;

.field public c:Lcom/ekodroid/omrevaluator/exams/models/MessageReport;

.field public d:Landroid/content/Context;

.field public e:Ly01;

.field public f:[B


# direct methods
.method public constructor <init>(Landroid/content/Context;Ltd1;Ly01;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lze1$a;

    invoke-direct {v0, p0}, Lze1$a;-><init>(Lze1;)V

    iput-object v0, p0, Lze1;->e:Ly01;

    iput-object p1, p0, Lze1;->d:Landroid/content/Context;

    iput-object p2, p0, Lze1;->b:Ltd1;

    iput-object p3, p0, Lze1;->a:Ly01;

    iget-boolean p1, p2, Ltd1;->h:Z

    const/4 p3, 0x0

    if-eqz p1, :cond_0

    iget-object p1, p2, Ltd1;->b:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result p1

    const/4 p2, 0x3

    if-le p1, p2, :cond_0

    new-instance p1, Lze1$b;

    const/4 p2, 0x0

    invoke-direct {p1, p0, p2}, Lze1$b;-><init>(Lze1;Lze1$a;)V

    new-array p2, p3, [Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    :cond_0
    invoke-virtual {p0, p3}, Lze1;->p(Z)V

    :goto_0
    return-void
.end method

.method public static synthetic a(Lze1;)Landroid/content/Context;
    .locals 0

    .line 1
    iget-object p0, p0, Lze1;->d:Landroid/content/Context;

    return-object p0
.end method

.method public static synthetic b(Lze1;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lze1;->o()V

    return-void
.end method

.method public static synthetic c(Lze1;Z)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lze1;->p(Z)V

    return-void
.end method

.method public static synthetic d(Lze1;)[B
    .locals 0

    .line 1
    iget-object p0, p0, Lze1;->f:[B

    return-object p0
.end method

.method public static synthetic e(Lze1;[B)[B
    .locals 0

    .line 1
    iput-object p1, p0, Lze1;->f:[B

    return-object p1
.end method

.method public static synthetic f(Lze1;Ltd1;)[B
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lze1;->m(Ltd1;)[B

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final g(Lwd1;Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;ILcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;)V
    .locals 9

    .line 1
    invoke-virtual {p2}, Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;->getExamSet()I

    move-result p2

    invoke-static {p3, p2}, Lve1;->b(Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;I)Ljava/util/ArrayList;

    move-result-object p2

    invoke-virtual {p3}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getLabelProfile()Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    move-result-object v0

    invoke-virtual {p5}, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->getQuestionNumber()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getQuestionNumberLabel(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p3}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getLabelProfile()Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    move-result-object v0

    invoke-static {p5, v0}, Le5;->r(Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2, p4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;

    invoke-virtual {p3}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getLabelProfile()Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    move-result-object p3

    invoke-static {p2, p3}, Le5;->q(Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;)Ljava/lang/String;

    move-result-object v5

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string p3, ""

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p5}, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->getMarksForAnswer()D

    move-result-wide p3

    invoke-static {p3, p4}, Lve1;->o(D)D

    move-result-wide p3

    invoke-virtual {p2, p3, p4}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {p5}, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->getMarkedState()Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;

    move-result-object v8

    move-object v2, p1

    invoke-virtual/range {v2 .. v8}, Lwd1;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;)V

    return-void
.end method

.method public final h(Lwd1;Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)V
    .locals 9

    .line 1
    invoke-virtual {p3}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getLabelProfile()Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    move-result-object v0

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getQueNoString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getAttemptedString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getCorrectString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getMarksString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    move-object v1, p1

    invoke-virtual/range {v1 .. v6}, Lwd1;->f(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-virtual {p3}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getTemplateParams()Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;

    move-result-object v0

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;->getSubjects()[Lcom/ekodroid/omrevaluator/templateui/models/Subject2;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v1, v1}, Lze1;->j(Lwd1;[Lcom/ekodroid/omrevaluator/templateui/models/Subject2;II)V

    move v0, v1

    move v2, v0

    :goto_0
    invoke-virtual {p2}, Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;->getAnswerValue2s()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v1, v3, :cond_2

    invoke-virtual {p2}, Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;->getAnswerValue2s()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    move-object v7, v3

    check-cast v7, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;

    invoke-virtual {v7}, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->getSubjectId()I

    move-result v3

    if-ne v0, v3, :cond_0

    invoke-virtual {v7}, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->getSectionId()I

    move-result v3

    if-eq v2, v3, :cond_1

    :cond_0
    invoke-virtual {v7}, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->getSubjectId()I

    move-result v0

    invoke-virtual {v7}, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->getSectionId()I

    move-result v2

    invoke-virtual {p3}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getTemplateParams()Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;

    move-result-object v3

    invoke-virtual {v3}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;->getSubjects()[Lcom/ekodroid/omrevaluator/templateui/models/Subject2;

    move-result-object v3

    invoke-virtual {p0, p1, v3, v0, v2}, Lze1;->j(Lwd1;[Lcom/ekodroid/omrevaluator/templateui/models/Subject2;II)V

    :cond_1
    move v8, v2

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move v6, v1

    invoke-virtual/range {v2 .. v7}, Lze1;->g(Lwd1;Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;ILcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;)V

    add-int/lit8 v1, v1, 0x1

    move v2, v8

    goto :goto_0

    :cond_2
    return-void
.end method

.method public final i(Lorg/apache/pdfbox/pdmodel/graphics/image/PDImageXObject;Lorg/apache/pdfbox/pdmodel/PDPage;Lorg/apache/pdfbox/pdmodel/PDPageContentStream;)V
    .locals 10

    .line 1
    invoke-virtual {p2}, Lorg/apache/pdfbox/pdmodel/PDPage;->getBBox()Lorg/apache/pdfbox/pdmodel/common/PDRectangle;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->getWidth()F

    move-result v0

    const/high16 v1, 0x41a00000    # 20.0f

    sub-float/2addr v0, v1

    invoke-virtual {p2}, Lorg/apache/pdfbox/pdmodel/PDPage;->getBBox()Lorg/apache/pdfbox/pdmodel/common/PDRectangle;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->getHeight()F

    move-result v2

    sub-float/2addr v2, v1

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/graphics/image/PDImageXObject;->getWidth()I

    move-result v1

    const/16 v3, 0x320

    if-le v1, v3, :cond_0

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/graphics/image/PDImageXObject;->getWidth()I

    move-result v3

    :cond_0
    div-float v1, v0, v2

    float-to-double v4, v1

    :try_start_0
    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/graphics/image/PDImageXObject;->getWidth()I

    move-result v1

    int-to-double v6, v1

    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    mul-double/2addr v6, v8

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/graphics/image/PDImageXObject;->getHeight()I

    move-result v1

    int-to-double v8, v1

    div-double/2addr v6, v8

    cmpl-double v1, v4, v6

    if-lez v1, :cond_1

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/graphics/image/PDImageXObject;->getHeight()I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v2, v0

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/graphics/image/PDImageXObject;->getWidth()I

    move-result v1

    int-to-float v1, v1

    div-float v2, v0, v1

    :goto_0
    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/graphics/image/PDImageXObject;->getWidth()I

    move-result v0

    int-to-float v0, v0

    mul-float/2addr v2, v0

    int-to-float v0, v3

    div-float/2addr v2, v0

    invoke-virtual {p2}, Lorg/apache/pdfbox/pdmodel/PDPage;->getBBox()Lorg/apache/pdfbox/pdmodel/common/PDRectangle;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->getWidth()F

    move-result v0

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/graphics/image/PDImageXObject;->getWidth()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v1, v2

    sub-float/2addr v0, v1

    const/high16 v1, 0x40000000    # 2.0f

    div-float v5, v0, v1

    invoke-virtual {p2}, Lorg/apache/pdfbox/pdmodel/PDPage;->getBBox()Lorg/apache/pdfbox/pdmodel/common/PDRectangle;

    move-result-object p2

    invoke-virtual {p2}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->getHeight()F

    move-result p2

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/graphics/image/PDImageXObject;->getHeight()I

    move-result v0

    int-to-float v0, v0

    mul-float/2addr v0, v2

    sub-float/2addr p2, v0

    div-float v6, p2, v1

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/graphics/image/PDImageXObject;->getWidth()I

    move-result p2

    int-to-float p2, p2

    mul-float v7, p2, v2

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/graphics/image/PDImageXObject;->getHeight()I

    move-result p2

    int-to-float p2, p2

    mul-float v8, p2, v2

    move-object v3, p3

    move-object v4, p1

    invoke-virtual/range {v3 .. v8}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->drawImage(Lorg/apache/pdfbox/pdmodel/graphics/image/PDImageXObject;FFFF)V

    const-string p1, "TAG"

    const-string p2, "image converted to pdf"

    invoke-static {p1, p2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V

    :goto_1
    return-void
.end method

.method public final j(Lwd1;[Lcom/ekodroid/omrevaluator/templateui/models/Subject2;II)V
    .locals 1

    .line 1
    aget-object p2, p2, p3

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2}, Lcom/ekodroid/omrevaluator/templateui/models/Subject2;->getSubName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " - "

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Lcom/ekodroid/omrevaluator/templateui/models/Subject2;->getSections()[Lcom/ekodroid/omrevaluator/templateui/models/Section2;

    move-result-object p2

    aget-object p2, p2, p4

    invoke-virtual {p2}, Lcom/ekodroid/omrevaluator/templateui/models/Section2;->getName()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    const/4 p3, 0x0

    invoke-virtual {p1, p2, p3}, Lwd1;->g(Ljava/lang/String;Z)V

    return-void
.end method

.method public final k(Lwd1;Ltd1;Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)V
    .locals 29

    .line 1
    move-object/from16 v7, p1

    move-object/from16 v0, p2

    invoke-virtual/range {p4 .. p4}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getLabelProfile()Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    move-result-object v8

    invoke-virtual/range {p4 .. p4}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getTemplateParams()Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;

    move-result-object v9

    invoke-virtual/range {p3 .. p3}, Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;->getMarksForEachSubject()[D

    move-result-object v10

    invoke-static/range {p3 .. p4}, Lve1;->k(Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)[[D

    move-result-object v11

    invoke-static/range {p3 .. p4}, Lve1;->c(Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)[[I

    move-result-object v12

    invoke-static/range {p3 .. p4}, Lve1;->j(Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)[[I

    move-result-object v13

    invoke-static {v12}, Lve1;->h([[I)[I

    move-result-object v14

    invoke-static {v13}, Lve1;->h([[I)[I

    move-result-object v15

    invoke-static/range {p4 .. p4}, Lve1;->r(Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)[[D

    move-result-object v1

    invoke-static {v11, v1}, Lve1;->n([[D[[D)[[D

    move-result-object v16

    invoke-static {v1}, Lve1;->g([[D)[D

    move-result-object v1

    invoke-static {v10, v1}, Lve1;->m([D[D)[D

    move-result-object v17

    invoke-virtual/range {p3 .. p3}, Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;->getTotalMarks()D

    move-result-wide v5

    invoke-static {v1}, Lve1;->d([D)D

    move-result-wide v1

    div-double v3, v5, v1

    const-wide/high16 v18, 0x4059000000000000L    # 100.0

    mul-double v18, v18, v3

    invoke-virtual {v8}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getExamSetLabels()[Ljava/lang/String;

    move-result-object v3

    invoke-virtual/range {p4 .. p4}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getGradeLevels()[Lcom/ekodroid/omrevaluator/templateui/models/GradeLevel;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual/range {p4 .. p4}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getGradeLevels()[Lcom/ekodroid/omrevaluator/templateui/models/GradeLevel;

    move-result-object v4

    invoke-static {v5, v6, v4}, Lve1;->i(D[Lcom/ekodroid/omrevaluator/templateui/models/GradeLevel;)Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    :cond_0
    const/4 v4, 0x0

    :goto_0
    move-wide/from16 v20, v5

    invoke-virtual {v8}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getReportCardString()Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v22, v13

    iget v13, v0, Ltd1;->l:I

    invoke-virtual {v6, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v13, ""

    invoke-virtual {v6, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v7, v5, v6}, Lwd1;->a(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getNameString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, " : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object/from16 v23, v12

    iget-object v12, v0, Ltd1;->a:Ljava/lang/String;

    invoke-virtual {v5, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v7, v5}, Lwd1;->b(Ljava/lang/String;)V

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getExamNameString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v5, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v12, v0, Ltd1;->d:Ljava/lang/String;

    invoke-virtual {v5, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v7, v5}, Lwd1;->b(Ljava/lang/String;)V

    invoke-virtual/range {p3 .. p3}, Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;->getExamSet()I

    move-result v5

    if-lez v5, :cond_1

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getExamSetString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v5, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual/range {p3 .. p3}, Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;->getExamSet()I

    move-result v12

    const/16 v24, 0x1

    add-int/lit8 v12, v12, -0x1

    aget-object v3, v3, v12

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v7, v3}, Lwd1;->b(Ljava/lang/String;)V

    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getDateString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v5, v0, Ltd1;->g:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v7, v3}, Lwd1;->b(Ljava/lang/String;)V

    iget-boolean v3, v0, Ltd1;->k:Z

    if-eqz v3, :cond_2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getRankString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v0, v0, Ltd1;->m:I

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Lwd1;->b(Ljava/lang/String;)V

    :cond_2
    if-eqz v4, :cond_3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getGradeString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Lwd1;->b(Ljava/lang/String;)V

    :cond_3
    const/16 v12, 0x32

    invoke-virtual {v7, v12}, Lwd1;->h(I)V

    invoke-virtual {v8}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getSubjectString()Ljava/lang/String;

    move-result-object v3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getMarksString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, " ("

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v8}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getPercentageString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v8}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getCorrectAnswerString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v8}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getIncorrectAnswerString()Ljava/lang/String;

    move-result-object v6

    const/16 v24, 0x0

    move-object/from16 v0, p1

    move-object v1, v3

    move-object v3, v4

    move-object v4, v5

    move-object v5, v6

    move/from16 v6, v24

    invoke-virtual/range {v0 .. v6}, Lwd1;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    :goto_1
    array-length v0, v10

    const-string v5, "%"

    if-ge v6, v0, :cond_5

    invoke-virtual {v9}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;->getSubjects()[Lcom/ekodroid/omrevaluator/templateui/models/Subject2;

    move-result-object v0

    aget-object v0, v0, v6

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/templateui/models/Subject2;->getSubName()Ljava/lang/String;

    move-result-object v1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-wide v2, v10, v6

    invoke-static {v2, v3}, Lve1;->o(D)D

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    aget-wide v3, v17, v6

    invoke-static {v3, v4}, Lve1;->o(D)D

    move-result-wide v3

    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    aget v4, v14, v6

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    aget v12, v15, v6

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    const/16 v25, 0x0

    move-object/from16 v0, p1

    move-object/from16 v26, v10

    move-object v10, v5

    move-object v5, v12

    move v12, v6

    move/from16 v6, v25

    invoke-virtual/range {v0 .. v6}, Lwd1;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    aget-object v0, v11, v12

    array-length v0, v0

    const/4 v6, 0x1

    if-le v0, v6, :cond_4

    move/from16 v5, v24

    :goto_2
    aget-object v0, v11, v12

    array-length v0, v0

    if-ge v5, v0, :cond_4

    invoke-virtual {v9}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;->getSubjects()[Lcom/ekodroid/omrevaluator/templateui/models/Subject2;

    move-result-object v0

    aget-object v0, v0, v12

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/templateui/models/Subject2;->getSections()[Lcom/ekodroid/omrevaluator/templateui/models/Section2;

    move-result-object v0

    aget-object v0, v0, v5

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/templateui/models/Section2;->getName()Ljava/lang/String;

    move-result-object v1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-object v2, v11, v12

    aget-wide v3, v2, v5

    invoke-static {v3, v4}, Lve1;->o(D)D

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    aget-object v3, v16, v12

    aget-wide v27, v3, v5

    invoke-static/range {v27 .. v28}, Lve1;->o(D)D

    move-result-wide v3

    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    aget-object v4, v23, v12

    aget v4, v4, v5

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    aget-object v25, v22, v12

    aget v6, v25, v5

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const/16 v25, 0x0

    move-object/from16 v0, p1

    move/from16 v27, v5

    move-object v5, v6

    const/16 v28, 0x1

    move/from16 v6, v25

    invoke-virtual/range {v0 .. v6}, Lwd1;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    add-int/lit8 v5, v27, 0x1

    move/from16 v6, v28

    goto :goto_2

    :cond_4
    move/from16 v28, v6

    add-int/lit8 v6, v12, 0x1

    move-object/from16 v10, v26

    const/16 v12, 0x32

    goto/16 :goto_1

    :cond_5
    move-object v10, v5

    invoke-virtual {v8}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getTotalMarksString()Ljava/lang/String;

    move-result-object v1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static/range {v20 .. v21}, Lve1;->o(D)D

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static/range {v18 .. v19}, Lve1;->o(D)D

    move-result-wide v3

    invoke-static {v3, v4}, Lve1;->o(D)D

    move-result-wide v3

    invoke-virtual {v0, v3, v4}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v14}, Lve1;->e([I)I

    move-result v4

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v15}, Lve1;->e([I)I

    move-result v5

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x1

    move-object/from16 v0, p1

    invoke-virtual/range {v0 .. v6}, Lwd1;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    const/16 v0, 0x32

    invoke-virtual {v7, v0}, Lwd1;->h(I)V

    return-void
.end method

.method public l()V
    .locals 2

    .line 1
    iget-object v0, p0, Lze1;->a:Ly01;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ly01;->a(Ljava/lang/Object;)V

    iput-object v1, p0, Lze1;->a:Ly01;

    return-void
.end method

.method public final m(Ltd1;)[B
    .locals 20

    .line 1
    move-object/from16 v1, p0

    move-object/from16 v0, p1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, v1, Lze1;->d:Landroid/content/Context;

    sget-object v4, Landroid/os/Environment;->DIRECTORY_DOCUMENTS:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/content/Context;->getExternalFilesDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v3, "/temp_report.pdf"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lorg/apache/pdfbox/pdmodel/PDDocument;

    invoke-direct {v3}, Lorg/apache/pdfbox/pdmodel/PDDocument;-><init>()V

    new-instance v4, Lwd1;

    invoke-direct {v4}, Lwd1;-><init>()V

    const/4 v5, 0x0

    :try_start_0
    iget-object v6, v1, Lze1;->d:Landroid/content/Context;

    invoke-static {v6}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;

    move-result-object v6

    new-instance v7, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    iget-object v8, v0, Ltd1;->d:Ljava/lang/String;

    iget-object v9, v0, Ltd1;->e:Ljava/lang/String;

    iget-object v10, v0, Ltd1;->g:Ljava/lang/String;

    invoke-direct {v7, v8, v9, v10}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v6, v7}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getTemplateJson(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;

    move-result-object v6

    invoke-virtual {v6}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->getSheetTemplate()Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    move-result-object v6

    iget-object v7, v1, Lze1;->d:Landroid/content/Context;

    invoke-static {v7}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;

    move-result-object v7

    new-instance v8, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    iget-object v9, v0, Ltd1;->d:Ljava/lang/String;

    iget-object v10, v0, Ltd1;->e:Ljava/lang/String;

    iget-object v11, v0, Ltd1;->g:Ljava/lang/String;

    invoke-direct {v8, v9, v10, v11}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget v9, v0, Ltd1;->l:I

    invoke-virtual {v7, v8, v9}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->getResult(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;I)Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;

    move-result-object v8

    invoke-virtual {v8, v6}, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;->getResultItem(Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;

    move-result-object v14

    invoke-virtual {v1, v4, v0, v14, v6}, Lze1;->k(Lwd1;Ltd1;Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)V

    invoke-virtual {v1, v4, v14, v6}, Lze1;->h(Lwd1;Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)V

    invoke-virtual {v4}, Lwd1;->j()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/graphics/Bitmap;

    new-instance v9, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v9}, Ljava/io/ByteArrayOutputStream;-><init>()V

    sget-object v10, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v11, 0x64

    invoke-virtual {v8, v10, v11, v9}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    invoke-virtual {v9}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v9

    new-instance v10, Ljava/io/ByteArrayInputStream;

    invoke-direct {v10, v9}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-static {v3, v10}, Lorg/apache/pdfbox/pdmodel/graphics/image/JPEGFactory;->createFromStream(Lorg/apache/pdfbox/pdmodel/PDDocument;Ljava/io/InputStream;)Lorg/apache/pdfbox/pdmodel/graphics/image/PDImageXObject;

    move-result-object v9

    new-instance v10, Lorg/apache/pdfbox/pdmodel/PDPage;

    sget-object v11, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->A4:Lorg/apache/pdfbox/pdmodel/common/PDRectangle;

    invoke-direct {v10, v11}, Lorg/apache/pdfbox/pdmodel/PDPage;-><init>(Lorg/apache/pdfbox/pdmodel/common/PDRectangle;)V

    invoke-virtual {v3, v10}, Lorg/apache/pdfbox/pdmodel/PDDocument;->addPage(Lorg/apache/pdfbox/pdmodel/PDPage;)V

    new-instance v11, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;

    invoke-direct {v11, v3, v10}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;-><init>(Lorg/apache/pdfbox/pdmodel/PDDocument;Lorg/apache/pdfbox/pdmodel/PDPage;)V

    invoke-virtual {v1, v9, v10, v11}, Lze1;->i(Lorg/apache/pdfbox/pdmodel/graphics/image/PDImageXObject;Lorg/apache/pdfbox/pdmodel/PDPage;Lorg/apache/pdfbox/pdmodel/PDPageContentStream;)V

    invoke-virtual {v11}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->close()V

    invoke-virtual {v8}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_0

    :cond_0
    iget-boolean v4, v0, Ltd1;->j:Z

    if-eqz v4, :cond_6

    new-instance v4, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    iget-object v8, v0, Ltd1;->d:Ljava/lang/String;

    iget-object v9, v0, Ltd1;->e:Ljava/lang/String;

    iget-object v10, v0, Ltd1;->g:Ljava/lang/String;

    invoke-direct {v4, v8, v9, v10}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget v8, v0, Ltd1;->l:I

    invoke-virtual {v7, v4, v8}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->getSheetImages(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;I)Ljava/util/ArrayList;

    move-result-object v4
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    :try_start_1
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-lez v7, :cond_4

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v7

    invoke-virtual {v6}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getPageLayouts()[Lcom/ekodroid/omrevaluator/templateui/models/PageLayout;

    move-result-object v8

    array-length v8, v8

    if-ne v7, v8, :cond_4

    invoke-static {v4}, Le5;->E(Ljava/util/ArrayList;)V

    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :try_start_2
    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_3

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/ekodroid/omrevaluator/database/SheetImageModel;

    invoke-virtual {v8}, Lcom/ekodroid/omrevaluator/database/SheetImageModel;->getImagePath()Ljava/lang/String;

    invoke-virtual {v8}, Lcom/ekodroid/omrevaluator/database/SheetImageModel;->getImagePath()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v9
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    const-string v10, "Image Missing"

    if-eqz v9, :cond_2

    :try_start_3
    invoke-virtual {v15, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v8}, Lcom/ekodroid/omrevaluator/database/SheetImageModel;->getSheetDimension()Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;

    move-result-object v8

    if-eqz v8, :cond_1

    invoke-virtual {v13, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    new-instance v0, Ljava/lang/Exception;

    invoke-direct {v0, v10}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    new-instance v0, Ljava/lang/Exception;

    invoke-direct {v0, v10}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    new-instance v16, Lcom/ekodroid/omrevaluator/templateui/scanner/b;

    invoke-direct/range {v16 .. v16}, Lcom/ekodroid/omrevaluator/templateui/scanner/b;-><init>()V

    const/4 v7, 0x0

    move v12, v7

    :goto_2
    invoke-virtual {v15}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-ge v12, v7, :cond_5

    invoke-virtual {v15, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    move-object v8, v7

    check-cast v8, Landroid/graphics/Bitmap;

    invoke-virtual {v13, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    move-object v11, v7

    check-cast v11, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;

    iget v10, v0, Ltd1;->l:I

    invoke-virtual {v4, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/ekodroid/omrevaluator/database/SheetImageModel;

    invoke-virtual {v7}, Lcom/ekodroid/omrevaluator/database/SheetImageModel;->getPageIndex()I

    move-result v17

    move-object/from16 v7, v16

    move-object v9, v6

    move/from16 v18, v10

    move-object v10, v14

    move/from16 v19, v12

    move/from16 v12, v18

    move-object/from16 v18, v13

    move/from16 v13, v17

    invoke-virtual/range {v7 .. v13}, Lcom/ekodroid/omrevaluator/templateui/scanner/b;->i(Landroid/graphics/Bitmap;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;II)Landroid/graphics/Bitmap;

    move-result-object v7

    new-instance v8, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v8}, Ljava/io/ByteArrayOutputStream;-><init>()V

    sget-object v9, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v10, 0x32

    invoke-virtual {v7, v9, v10, v8}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    invoke-virtual {v8}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v8

    new-instance v9, Ljava/io/ByteArrayInputStream;

    invoke-direct {v9, v8}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-static {v3, v9}, Lorg/apache/pdfbox/pdmodel/graphics/image/JPEGFactory;->createFromStream(Lorg/apache/pdfbox/pdmodel/PDDocument;Ljava/io/InputStream;)Lorg/apache/pdfbox/pdmodel/graphics/image/PDImageXObject;

    move-result-object v8

    new-instance v9, Lorg/apache/pdfbox/pdmodel/PDPage;

    sget-object v10, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->A4:Lorg/apache/pdfbox/pdmodel/common/PDRectangle;

    invoke-direct {v9, v10}, Lorg/apache/pdfbox/pdmodel/PDPage;-><init>(Lorg/apache/pdfbox/pdmodel/common/PDRectangle;)V

    invoke-virtual {v3, v9}, Lorg/apache/pdfbox/pdmodel/PDDocument;->addPage(Lorg/apache/pdfbox/pdmodel/PDPage;)V

    new-instance v10, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;

    invoke-direct {v10, v3, v9}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;-><init>(Lorg/apache/pdfbox/pdmodel/PDDocument;Lorg/apache/pdfbox/pdmodel/PDPage;)V

    invoke-virtual {v1, v8, v9, v10}, Lze1;->i(Lorg/apache/pdfbox/pdmodel/graphics/image/PDImageXObject;Lorg/apache/pdfbox/pdmodel/PDPage;Lorg/apache/pdfbox/pdmodel/PDPageContentStream;)V

    invoke-virtual {v10}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->close()V

    invoke-virtual {v7}, Landroid/graphics/Bitmap;->recycle()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    add-int/lit8 v12, v19, 0x1

    move-object/from16 v13, v18

    goto :goto_2

    :catch_0
    move-exception v0

    goto :goto_3

    :cond_4
    move-object v15, v5

    goto :goto_4

    :catch_1
    move-exception v0

    move-object v15, v5

    :goto_3
    :try_start_4
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    :cond_5
    :goto_4
    if-eqz v15, :cond_6

    invoke-virtual {v15}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_6

    invoke-virtual {v15}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_5
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_6

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_5

    :cond_6
    invoke-virtual {v3, v2}, Lorg/apache/pdfbox/pdmodel/PDDocument;->save(Ljava/lang/String;)V

    invoke-virtual {v3}, Lorg/apache/pdfbox/pdmodel/PDDocument;->close()V

    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Lze1;->n(Ljava/io/File;)[B

    move-result-object v0
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    return-object v0

    :catch_2
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    return-object v5
.end method

.method public final n(Ljava/io/File;)[B
    .locals 2

    .line 1
    :try_start_0
    invoke-virtual {p1}, Ljava/io/File;->length()J

    move-result-wide v0

    long-to-int v0, v0

    new-array v0, v0, [B

    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-virtual {v1, v0}, Ljava/io/FileInputStream;->read([B)I
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception p1

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "Error Reading The File."

    goto :goto_0

    :catch_1
    move-exception p1

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "File Not Found."

    :goto_0
    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V

    const/4 p1, 0x0

    return-object p1
.end method

.method public final o()V
    .locals 3

    .line 1
    iget-object v0, p0, Lze1;->d:Landroid/content/Context;

    const-string v1, "MyPref"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "count_email"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    return-void
.end method

.method public final p(Z)V
    .locals 2

    .line 1
    if-eqz p1, :cond_0

    iget-object p1, p0, Lze1;->a:Ly01;

    if-eqz p1, :cond_1

    new-instance v0, Ljava/lang/Boolean;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/lang/Boolean;-><init>(Z)V

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lze1;->a:Ly01;

    if-eqz p1, :cond_1

    new-instance v0, Ljava/lang/Boolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/lang/Boolean;-><init>(Z)V

    :goto_0
    invoke-interface {p1, v0}, Ly01;->a(Ljava/lang/Object;)V

    :cond_1
    return-void
.end method

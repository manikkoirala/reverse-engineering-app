.class public Lwz;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lt90;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a([DI)D
    .locals 4

    .line 1
    const/4 p2, 0x0

    aget-wide v0, p1, p2

    double-to-int p1, v0

    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    :goto_0
    const/4 p2, 0x1

    if-le p1, p2, :cond_0

    int-to-double v2, p1

    mul-double/2addr v0, v2

    add-int/lit8 p1, p1, -0x1

    goto :goto_0

    :cond_0
    return-wide v0
.end method

.method public b(I)Z
    .locals 1

    .line 1
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .line 1
    const-string v0, "fact(n)"

    return-object v0
.end method

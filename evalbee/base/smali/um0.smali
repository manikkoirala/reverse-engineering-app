.class public final Lum0;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Loj1;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lum0$b;
    }
.end annotation


# static fields
.field public static final b:Lvv0;


# instance fields
.field public final a:Lvv0;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .line 1
    new-instance v0, Lum0$a;

    invoke-direct {v0}, Lum0$a;-><init>()V

    sput-object v0, Lum0;->b:Lvv0;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-static {}, Lum0;->b()Lvv0;

    move-result-object v0

    invoke-direct {p0, v0}, Lum0;-><init>(Lvv0;)V

    return-void
.end method

.method public constructor <init>(Lvv0;)V
    .locals 1

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "messageInfoFactory"

    invoke-static {p1, v0}, Lcom/google/protobuf/t;->b(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lvv0;

    iput-object p1, p0, Lum0;->a:Lvv0;

    return-void
.end method

.method public static b()Lvv0;
    .locals 4

    .line 1
    new-instance v0, Lum0$b;

    const/4 v1, 0x2

    new-array v1, v1, [Lvv0;

    const/4 v2, 0x0

    invoke-static {}, Lcom/google/protobuf/q;->c()Lcom/google/protobuf/q;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-static {}, Lum0;->c()Lvv0;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-direct {v0, v1}, Lum0$b;-><init>([Lvv0;)V

    return-object v0
.end method

.method public static c()Lvv0;
    .locals 4

    .line 1
    :try_start_0
    const-string v0, "com.google.protobuf.DescriptorMessageInfoFactory"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    const-string v1, "getInstance"

    const/4 v2, 0x0

    new-array v3, v2, [Ljava/lang/Class;

    invoke-virtual {v0, v1, v3}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    new-array v1, v2, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lvv0;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    sget-object v0, Lum0;->b:Lvv0;

    return-object v0
.end method

.method public static d(Ltv0;)Z
    .locals 1

    .line 1
    invoke-interface {p0}, Ltv0;->c()Lcom/google/protobuf/ProtoSyntax;

    move-result-object p0

    sget-object v0, Lcom/google/protobuf/ProtoSyntax;->PROTO2:Lcom/google/protobuf/ProtoSyntax;

    if-ne p0, v0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method public static e(Ljava/lang/Class;Ltv0;)Lcom/google/protobuf/g0;
    .locals 8

    .line 1
    const-class v0, Lcom/google/protobuf/GeneratedMessageLite;

    invoke-virtual {v0, p0}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {p1}, Lum0;->d(Ltv0;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lhz0;->b()Lez0;

    move-result-object v3

    invoke-static {}, Lcom/google/protobuf/v;->b()Lcom/google/protobuf/v;

    move-result-object v4

    invoke-static {}, Lcom/google/protobuf/h0;->L()Lcom/google/protobuf/k0;

    move-result-object v5

    invoke-static {}, Lsz;->b()Lcom/google/protobuf/m;

    move-result-object v6

    invoke-static {}, Lzm0;->b()Lcom/google/protobuf/y;

    move-result-object v7

    move-object v1, p0

    move-object v2, p1

    invoke-static/range {v1 .. v7}, Lcom/google/protobuf/c0;->U(Ljava/lang/Class;Ltv0;Lez0;Lcom/google/protobuf/v;Lcom/google/protobuf/k0;Lcom/google/protobuf/m;Lcom/google/protobuf/y;)Lcom/google/protobuf/c0;

    move-result-object p0

    goto :goto_0

    :cond_0
    invoke-static {}, Lhz0;->b()Lez0;

    move-result-object v2

    invoke-static {}, Lcom/google/protobuf/v;->b()Lcom/google/protobuf/v;

    move-result-object v3

    invoke-static {}, Lcom/google/protobuf/h0;->L()Lcom/google/protobuf/k0;

    move-result-object v4

    const/4 v5, 0x0

    invoke-static {}, Lzm0;->b()Lcom/google/protobuf/y;

    move-result-object v6

    move-object v0, p0

    move-object v1, p1

    invoke-static/range {v0 .. v6}, Lcom/google/protobuf/c0;->U(Ljava/lang/Class;Ltv0;Lez0;Lcom/google/protobuf/v;Lcom/google/protobuf/k0;Lcom/google/protobuf/m;Lcom/google/protobuf/y;)Lcom/google/protobuf/c0;

    move-result-object p0

    :goto_0
    return-object p0

    :cond_1
    invoke-static {p1}, Lum0;->d(Ltv0;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {}, Lhz0;->a()Lez0;

    move-result-object v3

    invoke-static {}, Lcom/google/protobuf/v;->a()Lcom/google/protobuf/v;

    move-result-object v4

    invoke-static {}, Lcom/google/protobuf/h0;->G()Lcom/google/protobuf/k0;

    move-result-object v5

    invoke-static {}, Lsz;->a()Lcom/google/protobuf/m;

    move-result-object v6

    invoke-static {}, Lzm0;->a()Lcom/google/protobuf/y;

    move-result-object v7

    move-object v1, p0

    move-object v2, p1

    invoke-static/range {v1 .. v7}, Lcom/google/protobuf/c0;->U(Ljava/lang/Class;Ltv0;Lez0;Lcom/google/protobuf/v;Lcom/google/protobuf/k0;Lcom/google/protobuf/m;Lcom/google/protobuf/y;)Lcom/google/protobuf/c0;

    move-result-object p0

    goto :goto_1

    :cond_2
    invoke-static {}, Lhz0;->a()Lez0;

    move-result-object v2

    invoke-static {}, Lcom/google/protobuf/v;->a()Lcom/google/protobuf/v;

    move-result-object v3

    invoke-static {}, Lcom/google/protobuf/h0;->H()Lcom/google/protobuf/k0;

    move-result-object v4

    const/4 v5, 0x0

    invoke-static {}, Lzm0;->a()Lcom/google/protobuf/y;

    move-result-object v6

    move-object v0, p0

    move-object v1, p1

    invoke-static/range {v0 .. v6}, Lcom/google/protobuf/c0;->U(Ljava/lang/Class;Ltv0;Lez0;Lcom/google/protobuf/v;Lcom/google/protobuf/k0;Lcom/google/protobuf/m;Lcom/google/protobuf/y;)Lcom/google/protobuf/c0;

    move-result-object p0

    :goto_1
    return-object p0
.end method


# virtual methods
.method public a(Ljava/lang/Class;)Lcom/google/protobuf/g0;
    .locals 2

    .line 1
    invoke-static {p1}, Lcom/google/protobuf/h0;->I(Ljava/lang/Class;)V

    iget-object v0, p0, Lum0;->a:Lvv0;

    invoke-interface {v0, p1}, Lvv0;->a(Ljava/lang/Class;)Ltv0;

    move-result-object v0

    invoke-interface {v0}, Ltv0;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    const-class v1, Lcom/google/protobuf/GeneratedMessageLite;

    invoke-virtual {v1, p1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result p1

    if-eqz p1, :cond_0

    invoke-static {}, Lcom/google/protobuf/h0;->L()Lcom/google/protobuf/k0;

    move-result-object p1

    invoke-static {}, Lsz;->b()Lcom/google/protobuf/m;

    move-result-object v1

    :goto_0
    invoke-interface {v0}, Ltv0;->b()Lcom/google/protobuf/a0;

    move-result-object v0

    invoke-static {p1, v1, v0}, Lcom/google/protobuf/d0;->l(Lcom/google/protobuf/k0;Lcom/google/protobuf/m;Lcom/google/protobuf/a0;)Lcom/google/protobuf/d0;

    move-result-object p1

    return-object p1

    :cond_0
    invoke-static {}, Lcom/google/protobuf/h0;->G()Lcom/google/protobuf/k0;

    move-result-object p1

    invoke-static {}, Lsz;->a()Lcom/google/protobuf/m;

    move-result-object v1

    goto :goto_0

    :cond_1
    invoke-static {p1, v0}, Lum0;->e(Ljava/lang/Class;Ltv0;)Lcom/google/protobuf/g0;

    move-result-object p1

    return-object p1
.end method

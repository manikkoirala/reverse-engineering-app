.class public Lmf1$c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lmf1;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Lmf1;


# direct methods
.method public constructor <init>(Lmf1;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lmf1$c;->a:Lmf1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .line 1
    iget-object p1, p0, Lmf1$c;->a:Lmf1;

    iget-object p1, p1, Lmf1;->e:[Landroid/widget/EditText;

    array-length p1, p1

    new-array p1, p1, [Ljava/lang/String;

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lmf1$c;->a:Lmf1;

    iget-object v2, v1, Lmf1;->e:[Landroid/widget/EditText;

    array-length v3, v2

    if-ge v0, v3, :cond_1

    aget-object v1, v2, v0

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, p1, v0

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x1

    if-ge v1, v2, :cond_0

    iget-object p1, p0, Lmf1$c;->a:Lmf1;

    iget-object p1, p1, Lmf1;->a:Landroid/content/Context;

    const v0, 0x7f0800bd

    const v1, 0x7f08016e

    const v2, 0x7f1200b5

    invoke-static {p1, v2, v0, v1}, La91;->G(Landroid/content/Context;III)V

    return-void

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iget-object v0, v1, Lmf1;->c:Lmf1$d;

    if-eqz v0, :cond_2

    invoke-static {v1}, Lmf1;->a(Lmf1;)Landroid/widget/Spinner;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/AdapterView;->getSelectedItemPosition()I

    move-result v1

    invoke-interface {v0, v1, p1}, Lmf1$d;->a(I[Ljava/lang/String;)V

    :cond_2
    iget-object p1, p0, Lmf1$c;->a:Lmf1;

    invoke-virtual {p1}, Landroid/app/Dialog;->dismiss()V

    return-void
.end method

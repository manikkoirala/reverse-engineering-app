.class public Lig0$d;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lig0;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Lig0;


# direct methods
.method public constructor <init>(Lig0;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lig0$d;->a:Lig0;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8

    .line 1
    iget-object p1, p0, Lig0$d;->a:Lig0;

    invoke-static {p1}, Lig0;->d(Lig0;)Landroid/widget/Spinner;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/AdapterView;->getSelectedItemPosition()I

    move-result p1

    if-nez p1, :cond_0

    iget-object p1, p0, Lig0$d;->a:Lig0;

    const-string v0, "None"

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lig0$d;->a:Lig0;

    const-string v0, "Max"

    :goto_0
    iput-object v0, p1, Lig0;->c:Ljava/lang/String;

    iget-object p1, p0, Lig0$d;->a:Lig0;

    invoke-static {p1}, Lig0;->a(Lig0;)[[Z

    move-result-object p1

    array-length p1, p1

    new-array v0, p1, [Lcom/ekodroid/omrevaluator/templateui/models/InvalidQuestionSet;

    const/4 v1, 0x0

    move v2, v1

    :goto_1
    if-ge v2, p1, :cond_6

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    move v4, v1

    :goto_2
    iget-object v5, p0, Lig0$d;->a:Lig0;

    invoke-static {v5}, Lig0;->a(Lig0;)[[Z

    move-result-object v5

    aget-object v5, v5, v2

    array-length v5, v5

    if-ge v4, v5, :cond_2

    iget-object v5, p0, Lig0$d;->a:Lig0;

    invoke-static {v5}, Lig0;->a(Lig0;)[[Z

    move-result-object v5

    aget-object v5, v5, v2

    aget-boolean v5, v5, v4

    if-eqz v5, :cond_1

    add-int/lit8 v5, v4, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    :cond_2
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-lez v4, :cond_3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v4

    new-array v5, v4, [I

    move v6, v1

    :goto_3
    if-ge v6, v4, :cond_4

    invoke-virtual {v3, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    aput v7, v5, v6

    add-int/lit8 v6, v6, 0x1

    goto :goto_3

    :cond_3
    const/4 v5, 0x0

    :cond_4
    add-int/lit8 v3, v2, 0x1

    const/4 v4, 0x1

    if-ne p1, v4, :cond_5

    move v4, v1

    goto :goto_4

    :cond_5
    move v4, v3

    :goto_4
    new-instance v6, Lcom/ekodroid/omrevaluator/templateui/models/InvalidQuestionSet;

    iget-object v7, p0, Lig0$d;->a:Lig0;

    iget-object v7, v7, Lig0;->c:Ljava/lang/String;

    invoke-direct {v6, v4, v5, v7}, Lcom/ekodroid/omrevaluator/templateui/models/InvalidQuestionSet;-><init>(I[ILjava/lang/String;)V

    aput-object v6, v0, v2

    move v2, v3

    goto :goto_1

    :cond_6
    iget-object p1, p0, Lig0$d;->a:Lig0;

    invoke-static {p1}, Lig0;->e(Lig0;)Lig0$f;

    move-result-object p1

    invoke-interface {p1, v0}, Lig0$f;->a([Lcom/ekodroid/omrevaluator/templateui/models/InvalidQuestionSet;)V

    iget-object p1, p0, Lig0$d;->a:Lig0;

    invoke-virtual {p1}, Landroid/app/Dialog;->dismiss()V

    return-void
.end method

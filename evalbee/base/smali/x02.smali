.class public Lx02;
.super Ljava/util/AbstractList;
.source "SourceFile"

# interfaces
.implements Lfj0;
.implements Ljava/util/RandomAccess;


# instance fields
.field public final a:Lfj0;


# direct methods
.method public constructor <init>(Lfj0;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/util/AbstractList;-><init>()V

    iput-object p1, p0, Lx02;->a:Lfj0;

    return-void
.end method

.method public static synthetic a(Lx02;)Lfj0;
    .locals 0

    .line 1
    iget-object p0, p0, Lx02;->a:Lfj0;

    return-object p0
.end method


# virtual methods
.method public b(I)Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lx02;->a:Lfj0;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    return-object p1
.end method

.method public d()Lfj0;
    .locals 0

    .line 1
    return-object p0
.end method

.method public f()Ljava/util/List;
    .locals 1

    .line 1
    iget-object v0, p0, Lx02;->a:Lfj0;

    invoke-interface {v0}, Lfj0;->f()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic get(I)Ljava/lang/Object;
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lx02;->b(I)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 1

    .line 1
    new-instance v0, Lx02$b;

    invoke-direct {v0, p0}, Lx02$b;-><init>(Lx02;)V

    return-object v0
.end method

.method public k(I)Ljava/lang/Object;
    .locals 1

    .line 1
    iget-object v0, p0, Lx02;->a:Lfj0;

    invoke-interface {v0, p1}, Lfj0;->k(I)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public listIterator(I)Ljava/util/ListIterator;
    .locals 1

    .line 1
    new-instance v0, Lx02$a;

    invoke-direct {v0, p0, p1}, Lx02$a;-><init>(Lx02;I)V

    return-object v0
.end method

.method public q(Lcom/google/protobuf/ByteString;)V
    .locals 0

    .line 1
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    invoke-direct {p1}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw p1
.end method

.method public size()I
    .locals 1

    .line 1
    iget-object v0, p0, Lx02;->a:Lfj0;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

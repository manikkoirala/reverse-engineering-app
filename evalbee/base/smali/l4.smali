.class public Ll4;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Ljr;

.field public volatile b:Lm4;

.field public volatile c:Lsc;

.field public final d:Ljava/util/List;


# direct methods
.method public constructor <init>(Ljr;)V
    .locals 2

    .line 1
    new-instance v0, Lht;

    invoke-direct {v0}, Lht;-><init>()V

    new-instance v1, Lp02;

    invoke-direct {v1}, Lp02;-><init>()V

    invoke-direct {p0, p1, v0, v1}, Ll4;-><init>(Ljr;Lsc;Lm4;)V

    return-void
.end method

.method public constructor <init>(Ljr;Lsc;Lm4;)V
    .locals 0

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Ll4;->a:Ljr;

    iput-object p2, p0, Ll4;->c:Lsc;

    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Ll4;->d:Ljava/util/List;

    iput-object p3, p0, Ll4;->b:Lm4;

    invoke-virtual {p0}, Ll4;->f()V

    return-void
.end method

.method public static synthetic a(Ll4;Lr91;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Ll4;->i(Lr91;)V

    return-void
.end method

.method public static synthetic b(Ll4;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Ll4;->g(Ljava/lang/String;Landroid/os/Bundle;)V

    return-void
.end method

.method public static synthetic c(Ll4;Lrc;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Ll4;->h(Lrc;)V

    return-void
.end method

.method private synthetic g(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 1

    .line 1
    iget-object v0, p0, Ll4;->b:Lm4;

    invoke-interface {v0, p1, p2}, Lm4;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    return-void
.end method

.method private synthetic h(Lrc;)V
    .locals 1

    .line 1
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Ll4;->c:Lsc;

    instance-of v0, v0, Lht;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ll4;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    iget-object v0, p0, Ll4;->c:Lsc;

    invoke-interface {v0, p1}, Lsc;->a(Lrc;)V

    monitor-exit p0

    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1
.end method

.method private synthetic i(Lr91;)V
    .locals 5

    .line 1
    invoke-static {}, Lzl0;->f()Lzl0;

    move-result-object v0

    const-string v1, "AnalyticsConnector now available."

    invoke-virtual {v0, v1}, Lzl0;->b(Ljava/lang/String;)V

    invoke-interface {p1}, Lr91;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lg4;

    new-instance v0, Lhn;

    invoke-direct {v0, p1}, Lhn;-><init>(Lg4;)V

    new-instance v1, Lsm;

    invoke-direct {v1}, Lsm;-><init>()V

    invoke-static {p1, v1}, Ll4;->j(Lg4;Lsm;)Lg4$a;

    move-result-object p1

    if-eqz p1, :cond_1

    invoke-static {}, Lzl0;->f()Lzl0;

    move-result-object p1

    const-string v2, "Registered Firebase Analytics listener."

    invoke-virtual {p1, v2}, Lzl0;->b(Ljava/lang/String;)V

    new-instance p1, Lqc;

    invoke-direct {p1}, Lqc;-><init>()V

    new-instance v2, Lgc;

    const/16 v3, 0x1f4

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-direct {v2, v0, v3, v4}, Lgc;-><init>(Lhn;ILjava/util/concurrent/TimeUnit;)V

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Ll4;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lrc;

    invoke-virtual {p1, v3}, Lqc;->a(Lrc;)V

    goto :goto_0

    :cond_0
    invoke-virtual {v1, p1}, Lsm;->d(Ln4;)V

    invoke-virtual {v1, v2}, Lsm;->e(Ln4;)V

    iput-object p1, p0, Ll4;->c:Lsc;

    iput-object v2, p0, Ll4;->b:Lm4;

    monitor-exit p0

    goto :goto_1

    :catchall_0
    move-exception p1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1

    :cond_1
    invoke-static {}, Lzl0;->f()Lzl0;

    move-result-object p1

    const-string v0, "Could not register Firebase Analytics listener; a listener is already registered."

    invoke-virtual {p1, v0}, Lzl0;->k(Ljava/lang/String;)V

    :goto_1
    return-void
.end method

.method public static j(Lg4;Lsm;)Lg4$a;
    .locals 2

    .line 1
    const-string v0, "clx"

    invoke-interface {p0, v0, p1}, Lg4;->c(Ljava/lang/String;Lg4$b;)Lg4$a;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-static {}, Lzl0;->f()Lzl0;

    move-result-object v0

    const-string v1, "Could not register AnalyticsConnectorListener with Crashlytics origin."

    invoke-virtual {v0, v1}, Lzl0;->b(Ljava/lang/String;)V

    const-string v0, "crash"

    invoke-interface {p0, v0, p1}, Lg4;->c(Ljava/lang/String;Lg4$b;)Lg4$a;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lzl0;->f()Lzl0;

    move-result-object p0

    const-string p1, "A new version of the Google Analytics for Firebase SDK is now available. For improved performance and compatibility with Crashlytics, please update to the latest version."

    invoke-virtual {p0, p1}, Lzl0;->k(Ljava/lang/String;)V

    :cond_0
    return-object v0
.end method


# virtual methods
.method public d()Lm4;
    .locals 1

    .line 1
    new-instance v0, Lj4;

    invoke-direct {v0, p0}, Lj4;-><init>(Ll4;)V

    return-object v0
.end method

.method public e()Lsc;
    .locals 1

    .line 1
    new-instance v0, Li4;

    invoke-direct {v0, p0}, Li4;-><init>(Ll4;)V

    return-object v0
.end method

.method public final f()V
    .locals 2

    .line 1
    iget-object v0, p0, Ll4;->a:Ljr;

    new-instance v1, Lk4;

    invoke-direct {v1, p0}, Lk4;-><init>(Ll4;)V

    invoke-interface {v0, v1}, Ljr;->a(Ljr$a;)V

    return-void
.end method

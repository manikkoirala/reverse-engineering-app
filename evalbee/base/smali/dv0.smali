.class public Ldv0;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lwc1;


# instance fields
.field public a:Lxc1;

.field public final b:Lcom/google/firebase/firestore/local/e;

.field public c:Ljava/util/Set;


# direct methods
.method public constructor <init>(Lcom/google/firebase/firestore/local/e;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Ldv0;->b:Lcom/google/firebase/firestore/local/e;

    return-void
.end method


# virtual methods
.method public a(Ldu;)V
    .locals 1

    .line 1
    iget-object v0, p0, Ldv0;->c:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public final b(Ldu;)Z
    .locals 2

    .line 1
    iget-object v0, p0, Ldv0;->b:Lcom/google/firebase/firestore/local/e;

    invoke-virtual {v0}, Lcom/google/firebase/firestore/local/e;->r()Liv0;

    move-result-object v0

    invoke-virtual {v0, p1}, Liv0;->j(Ldu;)Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    return v1

    :cond_0
    invoke-virtual {p0, p1}, Ldv0;->i(Ldu;)Z

    move-result v0

    if-eqz v0, :cond_1

    return v1

    :cond_1
    iget-object v0, p0, Ldv0;->a:Lxc1;

    if-eqz v0, :cond_2

    invoke-virtual {v0, p1}, Lxc1;->c(Ldu;)Z

    move-result p1

    if-eqz p1, :cond_2

    return v1

    :cond_2
    const/4 p1, 0x0

    return p1
.end method

.method public c(Ldu;)V
    .locals 1

    .line 1
    iget-object v0, p0, Ldv0;->c:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method public d(Ldu;)V
    .locals 1

    .line 1
    iget-object v0, p0, Ldv0;->c:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public e()V
    .locals 5

    .line 1
    iget-object v0, p0, Ldv0;->b:Lcom/google/firebase/firestore/local/e;

    invoke-virtual {v0}, Lcom/google/firebase/firestore/local/e;->q()Lhv0;

    move-result-object v0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iget-object v2, p0, Ldv0;->c:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ldu;

    invoke-virtual {p0, v3}, Ldv0;->b(Ldu;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    invoke-virtual {v0, v1}, Lhv0;->removeAll(Ljava/util/Collection;)V

    const/4 v0, 0x0

    iput-object v0, p0, Ldv0;->c:Ljava/util/Set;

    return-void
.end method

.method public f()V
    .locals 1

    .line 1
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Ldv0;->c:Ljava/util/Set;

    return-void
.end method

.method public g(Lau1;)V
    .locals 4

    .line 1
    iget-object v0, p0, Ldv0;->b:Lcom/google/firebase/firestore/local/e;

    invoke-virtual {v0}, Lcom/google/firebase/firestore/local/e;->r()Liv0;

    move-result-object v0

    invoke-virtual {p1}, Lau1;->h()I

    move-result v1

    invoke-virtual {v0, v1}, Liv0;->h(I)Lcom/google/firebase/database/collection/c;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/firebase/database/collection/c;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ldu;

    iget-object v3, p0, Ldv0;->c:Ljava/util/Set;

    invoke-interface {v3, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    invoke-virtual {v0, p1}, Liv0;->q(Lau1;)V

    return-void
.end method

.method public h(Ldu;)V
    .locals 1

    .line 1
    invoke-virtual {p0, p1}, Ldv0;->b(Ldu;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ldv0;->c:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    iget-object v0, p0, Ldv0;->c:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :goto_0
    return-void
.end method

.method public final i(Ldu;)Z
    .locals 2

    .line 1
    iget-object v0, p0, Ldv0;->b:Lcom/google/firebase/firestore/local/e;

    invoke-virtual {v0}, Lcom/google/firebase/firestore/local/e;->p()Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lfv0;

    invoke-virtual {v1, p1}, Lfv0;->k(Ldu;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_1
    const/4 p1, 0x0

    return p1
.end method

.method public l()J
    .locals 2

    .line 1
    const-wide/16 v0, -0x1

    return-wide v0
.end method

.method public o(Lxc1;)V
    .locals 0

    .line 1
    iput-object p1, p0, Ldv0;->a:Lxc1;

    return-void
.end method

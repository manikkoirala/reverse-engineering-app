.class public final Lbn0;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Liz1;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lbn0$a;
    }
.end annotation


# instance fields
.field public final a:Lbl;

.field public final b:Z


# direct methods
.method public constructor <init>(Lbl;Z)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lbn0;->a:Lbl;

    iput-boolean p2, p0, Lbn0;->b:Z

    return-void
.end method


# virtual methods
.method public a(Lgc0;Lcom/google/gson/reflect/TypeToken;)Lhz1;
    .locals 11

    .line 1
    invoke-virtual {p2}, Lcom/google/gson/reflect/TypeToken;->getType()Ljava/lang/reflect/Type;

    move-result-object v0

    invoke-virtual {p2}, Lcom/google/gson/reflect/TypeToken;->getRawType()Ljava/lang/Class;

    move-result-object v1

    const-class v2, Ljava/util/Map;

    invoke-virtual {v2, v1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v2

    if-nez v2, :cond_0

    const/4 p1, 0x0

    return-object p1

    :cond_0
    invoke-static {v0, v1}, Lcom/google/gson/internal/$Gson$Types;->j(Ljava/lang/reflect/Type;Ljava/lang/Class;)[Ljava/lang/reflect/Type;

    move-result-object v0

    const/4 v1, 0x0

    aget-object v2, v0, v1

    invoke-virtual {p0, p1, v2}, Lbn0;->b(Lgc0;Ljava/lang/reflect/Type;)Lhz1;

    move-result-object v7

    const/4 v2, 0x1

    aget-object v3, v0, v2

    invoke-static {v3}, Lcom/google/gson/reflect/TypeToken;->get(Ljava/lang/reflect/Type;)Lcom/google/gson/reflect/TypeToken;

    move-result-object v3

    invoke-virtual {p1, v3}, Lgc0;->l(Lcom/google/gson/reflect/TypeToken;)Lhz1;

    move-result-object v9

    iget-object v3, p0, Lbn0;->a:Lbl;

    invoke-virtual {v3, p2}, Lbl;->b(Lcom/google/gson/reflect/TypeToken;)Lu01;

    move-result-object v10

    new-instance p2, Lbn0$a;

    aget-object v6, v0, v1

    aget-object v8, v0, v2

    move-object v3, p2

    move-object v4, p0

    move-object v5, p1

    invoke-direct/range {v3 .. v10}, Lbn0$a;-><init>(Lbn0;Lgc0;Ljava/lang/reflect/Type;Lhz1;Ljava/lang/reflect/Type;Lhz1;Lu01;)V

    return-object p2
.end method

.method public final b(Lgc0;Ljava/lang/reflect/Type;)Lhz1;
    .locals 1

    .line 1
    sget-object v0, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    if-eq p2, v0, :cond_1

    const-class v0, Ljava/lang/Boolean;

    if-ne p2, v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-static {p2}, Lcom/google/gson/reflect/TypeToken;->get(Ljava/lang/reflect/Type;)Lcom/google/gson/reflect/TypeToken;

    move-result-object p2

    invoke-virtual {p1, p2}, Lgc0;->l(Lcom/google/gson/reflect/TypeToken;)Lhz1;

    move-result-object p1

    goto :goto_1

    :cond_1
    :goto_0
    sget-object p1, Lkz1;->f:Lhz1;

    :goto_1
    return-object p1
.end method

.class public abstract Lql1;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(Lcom/google/firestore/v1/Value;)Lcom/google/protobuf/j0;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lcom/google/firestore/v1/Value;->s0()Lcom/google/firestore/v1/k;

    move-result-object p0

    const-string v0, "__local_write_time__"

    invoke-virtual {p0, v0}, Lcom/google/firestore/v1/k;->f0(Ljava/lang/String;)Lcom/google/firestore/v1/Value;

    move-result-object p0

    invoke-virtual {p0}, Lcom/google/firestore/v1/Value;->v0()Lcom/google/protobuf/j0;

    move-result-object p0

    return-object p0
.end method

.method public static b(Lcom/google/firestore/v1/Value;)Lcom/google/firestore/v1/Value;
    .locals 2

    .line 1
    invoke-virtual {p0}, Lcom/google/firestore/v1/Value;->s0()Lcom/google/firestore/v1/k;

    move-result-object p0

    const-string v0, "__previous_value__"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/firestore/v1/k;->e0(Ljava/lang/String;Lcom/google/firestore/v1/Value;)Lcom/google/firestore/v1/Value;

    move-result-object p0

    invoke-static {p0}, Lql1;->c(Lcom/google/firestore/v1/Value;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Lql1;->b(Lcom/google/firestore/v1/Value;)Lcom/google/firestore/v1/Value;

    move-result-object p0

    :cond_0
    return-object p0
.end method

.method public static c(Lcom/google/firestore/v1/Value;)Z
    .locals 2

    .line 1
    const/4 v0, 0x0

    if-nez p0, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/google/firestore/v1/Value;->s0()Lcom/google/firestore/v1/k;

    move-result-object p0

    const-string v1, "__type__"

    invoke-virtual {p0, v1, v0}, Lcom/google/firestore/v1/k;->e0(Ljava/lang/String;Lcom/google/firestore/v1/Value;)Lcom/google/firestore/v1/Value;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_1

    const-string p0, "server_timestamp"

    invoke-virtual {v0}, Lcom/google/firestore/v1/Value;->u0()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_1

    const/4 p0, 0x1

    goto :goto_1

    :cond_1
    const/4 p0, 0x0

    :goto_1
    return p0
.end method

.method public static d(Lpw1;Lcom/google/firestore/v1/Value;)Lcom/google/firestore/v1/Value;
    .locals 5

    .line 1
    invoke-static {}, Lcom/google/firestore/v1/Value;->x0()Lcom/google/firestore/v1/Value$b;

    move-result-object v0

    const-string v1, "server_timestamp"

    invoke-virtual {v0, v1}, Lcom/google/firestore/v1/Value$b;->L(Ljava/lang/String;)Lcom/google/firestore/v1/Value$b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/protobuf/GeneratedMessageLite$a;->p()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v0

    check-cast v0, Lcom/google/firestore/v1/Value;

    invoke-static {}, Lcom/google/firestore/v1/Value;->x0()Lcom/google/firestore/v1/Value$b;

    move-result-object v1

    invoke-static {}, Lcom/google/protobuf/j0;->f0()Lcom/google/protobuf/j0$b;

    move-result-object v2

    invoke-virtual {p0}, Lpw1;->e()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Lcom/google/protobuf/j0$b;->B(J)Lcom/google/protobuf/j0$b;

    move-result-object v2

    invoke-virtual {p0}, Lpw1;->d()I

    move-result p0

    invoke-virtual {v2, p0}, Lcom/google/protobuf/j0$b;->A(I)Lcom/google/protobuf/j0$b;

    move-result-object p0

    invoke-virtual {v1, p0}, Lcom/google/firestore/v1/Value$b;->M(Lcom/google/protobuf/j0$b;)Lcom/google/firestore/v1/Value$b;

    move-result-object p0

    invoke-virtual {p0}, Lcom/google/protobuf/GeneratedMessageLite$a;->p()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object p0

    check-cast p0, Lcom/google/firestore/v1/Value;

    invoke-static {}, Lcom/google/firestore/v1/k;->j0()Lcom/google/firestore/v1/k$b;

    move-result-object v1

    const-string v2, "__type__"

    invoke-virtual {v1, v2, v0}, Lcom/google/firestore/v1/k$b;->C(Ljava/lang/String;Lcom/google/firestore/v1/Value;)Lcom/google/firestore/v1/k$b;

    move-result-object v0

    const-string v1, "__local_write_time__"

    invoke-virtual {v0, v1, p0}, Lcom/google/firestore/v1/k$b;->C(Ljava/lang/String;Lcom/google/firestore/v1/Value;)Lcom/google/firestore/v1/k$b;

    move-result-object p0

    invoke-static {p1}, Lql1;->c(Lcom/google/firestore/v1/Value;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p1}, Lql1;->b(Lcom/google/firestore/v1/Value;)Lcom/google/firestore/v1/Value;

    move-result-object p1

    :cond_0
    if-eqz p1, :cond_1

    const-string v0, "__previous_value__"

    invoke-virtual {p0, v0, p1}, Lcom/google/firestore/v1/k$b;->C(Ljava/lang/String;Lcom/google/firestore/v1/Value;)Lcom/google/firestore/v1/k$b;

    :cond_1
    invoke-static {}, Lcom/google/firestore/v1/Value;->x0()Lcom/google/firestore/v1/Value$b;

    move-result-object p1

    invoke-virtual {p1, p0}, Lcom/google/firestore/v1/Value$b;->H(Lcom/google/firestore/v1/k$b;)Lcom/google/firestore/v1/Value$b;

    move-result-object p0

    invoke-virtual {p0}, Lcom/google/protobuf/GeneratedMessageLite$a;->p()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object p0

    check-cast p0, Lcom/google/firestore/v1/Value;

    return-object p0
.end method

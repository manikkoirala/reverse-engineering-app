.class public Lo32$m$a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnApplyWindowInsetsListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lo32$m;->u(Landroid/view/View;Lg11;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public a:Lu62;

.field public final synthetic b:Landroid/view/View;

.field public final synthetic c:Lg11;


# direct methods
.method public constructor <init>(Landroid/view/View;Lg11;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lo32$m$a;->b:Landroid/view/View;

    iput-object p2, p0, Lo32$m$a;->c:Lg11;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 p1, 0x0

    iput-object p1, p0, Lo32$m$a;->a:Lu62;

    return-void
.end method


# virtual methods
.method public onApplyWindowInsets(Landroid/view/View;Landroid/view/WindowInsets;)Landroid/view/WindowInsets;
    .locals 4

    .line 1
    invoke-static {p2, p1}, Lu62;->w(Landroid/view/WindowInsets;Landroid/view/View;)Lu62;

    move-result-object v0

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x1e

    if-ge v1, v2, :cond_0

    iget-object v3, p0, Lo32$m$a;->b:Landroid/view/View;

    invoke-static {p2, v3}, Lo32$m;->a(Landroid/view/WindowInsets;Landroid/view/View;)V

    iget-object p2, p0, Lo32$m$a;->a:Lu62;

    invoke-virtual {v0, p2}, Lu62;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_0

    iget-object p2, p0, Lo32$m$a;->c:Lg11;

    invoke-interface {p2, p1, v0}, Lg11;->onApplyWindowInsets(Landroid/view/View;Lu62;)Lu62;

    move-result-object p1

    invoke-virtual {p1}, Lu62;->u()Landroid/view/WindowInsets;

    move-result-object p1

    return-object p1

    :cond_0
    iput-object v0, p0, Lo32$m$a;->a:Lu62;

    iget-object p2, p0, Lo32$m$a;->c:Lg11;

    invoke-interface {p2, p1, v0}, Lg11;->onApplyWindowInsets(Landroid/view/View;Lu62;)Lu62;

    move-result-object p2

    if-lt v1, v2, :cond_1

    invoke-virtual {p2}, Lu62;->u()Landroid/view/WindowInsets;

    move-result-object p1

    return-object p1

    :cond_1
    invoke-static {p1}, Lo32;->n0(Landroid/view/View;)V

    invoke-virtual {p2}, Lu62;->u()Landroid/view/WindowInsets;

    move-result-object p1

    return-object p1
.end method

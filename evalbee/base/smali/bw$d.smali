.class public Lbw$d;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lbw;->i()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Lbw;


# direct methods
.method public constructor <init>(Lbw;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lbw$d;->a:Lbw;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .line 1
    iget-object p1, p0, Lbw$d;->a:Lbw;

    iget-object p1, p1, Lbw;->g:Landroid/widget/EditText;

    invoke-virtual {p1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p1

    iget-object v0, p0, Lbw$d;->a:Lbw;

    iget-object v0, v0, Lbw;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    const v3, 0x7f08016e

    const v4, 0x7f0800bd

    if-eqz v2, :cond_0

    iget-object p1, p0, Lbw$d;->a:Lbw;

    iget-object p1, p1, Lbw;->a:Landroid/content/Context;

    const v0, 0x7f1200c2

    invoke-static {p1, v0, v4, v3}, La91;->G(Landroid/content/Context;III)V

    return-void

    :cond_0
    iget-object v2, p0, Lbw$d;->a:Lbw;

    iget-object v2, v2, Lbw;->h:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;->getRole()Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserRole;

    move-result-object v0

    sget-object v5, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserRole;->OWNER:Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserRole;

    if-ne v0, v5, :cond_1

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object p1, p0, Lbw$d;->a:Lbw;

    iget-object p1, p1, Lbw;->a:Landroid/content/Context;

    const v0, 0x7f1200c4

    invoke-static {p1, v0, v4, v3}, La91;->G(Landroid/content/Context;III)V

    return-void

    :cond_1
    iget-object v0, p0, Lbw$d;->a:Lbw;

    iget-object v0, v0, Lbw;->e:Lcom/toptoche/searchablespinnerlibrary/SearchableSpinner;

    invoke-virtual {v0}, Lcom/toptoche/searchablespinnerlibrary/SearchableSpinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_2

    iget-object p1, p0, Lbw$d;->a:Lbw;

    iget-object p1, p1, Lbw;->a:Landroid/content/Context;

    const v0, 0x7f1202be

    invoke-static {p1, v0, v4, v3}, La91;->G(Landroid/content/Context;III)V

    return-void

    :cond_2
    iget-object v0, p0, Lbw$d;->a:Lbw;

    iget-object v0, v0, Lbw;->c:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iget-object v1, p0, Lbw$d;->a:Lbw;

    iget-object v1, v1, Lbw;->e:Lcom/toptoche/searchablespinnerlibrary/SearchableSpinner;

    invoke-virtual {v1}, Lcom/toptoche/searchablespinnerlibrary/SearchableSpinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const-string v3, "user_country"

    invoke-interface {v0, v3, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    iget-object v0, p0, Lbw$d;->a:Lbw;

    iget-object v0, v0, Lbw;->c:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "user_organization"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    iget-object v0, p0, Lbw$d;->a:Lbw;

    iget-object v1, v0, Lbw;->g:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbw;->c(Lbw;Ljava/lang/String;)V

    iget-object v0, p0, Lbw$d;->a:Lbw;

    invoke-static {v0, p1, v2}, Lbw;->d(Lbw;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

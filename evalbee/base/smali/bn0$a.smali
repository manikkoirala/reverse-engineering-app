.class public final Lbn0$a;
.super Lhz1;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lbn0;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "a"
.end annotation


# instance fields
.field public final a:Lhz1;

.field public final b:Lhz1;

.field public final c:Lu01;

.field public final synthetic d:Lbn0;


# direct methods
.method public constructor <init>(Lbn0;Lgc0;Ljava/lang/reflect/Type;Lhz1;Ljava/lang/reflect/Type;Lhz1;Lu01;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lbn0$a;->d:Lbn0;

    invoke-direct {p0}, Lhz1;-><init>()V

    new-instance p1, Ljz1;

    invoke-direct {p1, p2, p4, p3}, Ljz1;-><init>(Lgc0;Lhz1;Ljava/lang/reflect/Type;)V

    iput-object p1, p0, Lbn0$a;->a:Lhz1;

    new-instance p1, Ljz1;

    invoke-direct {p1, p2, p6, p5}, Ljz1;-><init>(Lgc0;Lhz1;Ljava/lang/reflect/Type;)V

    iput-object p1, p0, Lbn0$a;->b:Lhz1;

    iput-object p7, p0, Lbn0$a;->c:Lu01;

    return-void
.end method


# virtual methods
.method public bridge synthetic b(Lrh0;)Ljava/lang/Object;
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lbn0$a;->f(Lrh0;)Ljava/util/Map;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic d(Lvh0;Ljava/lang/Object;)V
    .locals 0

    .line 1
    check-cast p2, Ljava/util/Map;

    invoke-virtual {p0, p1, p2}, Lbn0$a;->g(Lvh0;Ljava/util/Map;)V

    return-void
.end method

.method public final e(Lnh0;)Ljava/lang/String;
    .locals 1

    .line 1
    invoke-virtual {p1}, Lnh0;->m()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lnh0;->c()Lqh0;

    move-result-object p1

    invoke-virtual {p1}, Lqh0;->t()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lqh0;->o()Ljava/lang/Number;

    move-result-object p1

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_0
    invoke-virtual {p1}, Lqh0;->r()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lqh0;->n()Z

    move-result p1

    invoke-static {p1}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_1
    invoke-virtual {p1}, Lqh0;->u()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lqh0;->p()Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_2
    new-instance p1, Ljava/lang/AssertionError;

    invoke-direct {p1}, Ljava/lang/AssertionError;-><init>()V

    throw p1

    :cond_3
    invoke-virtual {p1}, Lnh0;->i()Z

    move-result p1

    if-eqz p1, :cond_4

    const-string p1, "null"

    return-object p1

    :cond_4
    new-instance p1, Ljava/lang/AssertionError;

    invoke-direct {p1}, Ljava/lang/AssertionError;-><init>()V

    throw p1
.end method

.method public f(Lrh0;)Ljava/util/Map;
    .locals 4

    .line 1
    invoke-virtual {p1}, Lrh0;->o0()Lcom/google/gson/stream/JsonToken;

    move-result-object v0

    sget-object v1, Lcom/google/gson/stream/JsonToken;->NULL:Lcom/google/gson/stream/JsonToken;

    if-ne v0, v1, :cond_0

    invoke-virtual {p1}, Lrh0;->R()V

    const/4 p1, 0x0

    return-object p1

    :cond_0
    iget-object v1, p0, Lbn0$a;->c:Lu01;

    invoke-interface {v1}, Lu01;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map;

    sget-object v2, Lcom/google/gson/stream/JsonToken;->BEGIN_ARRAY:Lcom/google/gson/stream/JsonToken;

    const-string v3, "duplicate key: "

    if-ne v0, v2, :cond_3

    invoke-virtual {p1}, Lrh0;->a()V

    :goto_0
    invoke-virtual {p1}, Lrh0;->k()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lrh0;->a()V

    iget-object v0, p0, Lbn0$a;->a:Lhz1;

    invoke-virtual {v0, p1}, Lhz1;->b(Lrh0;)Ljava/lang/Object;

    move-result-object v0

    iget-object v2, p0, Lbn0$a;->b:Lhz1;

    invoke-virtual {v2, p1}, Lhz1;->b(Lrh0;)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_1

    invoke-virtual {p1}, Lrh0;->f()V

    goto :goto_0

    :cond_1
    new-instance p1, Lcom/google/gson/JsonSyntaxException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Lcom/google/gson/JsonSyntaxException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_2
    invoke-virtual {p1}, Lrh0;->f()V

    goto :goto_2

    :cond_3
    invoke-virtual {p1}, Lrh0;->b()V

    :goto_1
    invoke-virtual {p1}, Lrh0;->k()Z

    move-result v0

    if-eqz v0, :cond_5

    sget-object v0, Lsh0;->a:Lsh0;

    invoke-virtual {v0, p1}, Lsh0;->a(Lrh0;)V

    iget-object v0, p0, Lbn0$a;->a:Lhz1;

    invoke-virtual {v0, p1}, Lhz1;->b(Lrh0;)Ljava/lang/Object;

    move-result-object v0

    iget-object v2, p0, Lbn0$a;->b:Lhz1;

    invoke-virtual {v2, p1}, Lhz1;->b(Lrh0;)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_4

    goto :goto_1

    :cond_4
    new-instance p1, Lcom/google/gson/JsonSyntaxException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Lcom/google/gson/JsonSyntaxException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_5
    invoke-virtual {p1}, Lrh0;->g()V

    :goto_2
    return-object v1
.end method

.method public g(Lvh0;Ljava/util/Map;)V
    .locals 7

    .line 1
    if-nez p2, :cond_0

    invoke-virtual {p1}, Lvh0;->u()Lvh0;

    return-void

    :cond_0
    iget-object v0, p0, Lbn0$a;->d:Lbn0;

    iget-boolean v0, v0, Lbn0;->b:Z

    if-nez v0, :cond_2

    invoke-virtual {p1}, Lvh0;->d()Lvh0;

    invoke-interface {p2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p2

    invoke-interface {p2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lvh0;->o(Ljava/lang/String;)Lvh0;

    iget-object v1, p0, Lbn0$a;->b:Lhz1;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v1, p1, v0}, Lhz1;->d(Lvh0;Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Lvh0;->g()Lvh0;

    return-void

    :cond_2
    new-instance v0, Ljava/util/ArrayList;

    invoke-interface {p2}, Ljava/util/Map;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {p2}, Ljava/util/Map;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {p2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p2

    invoke-interface {p2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p2

    const/4 v2, 0x0

    move v3, v2

    :goto_1
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Map$Entry;

    iget-object v5, p0, Lbn0$a;->a:Lhz1;

    invoke-interface {v4}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v5, v6}, Lhz1;->c(Ljava/lang/Object;)Lnh0;

    move-result-object v5

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {v5}, Lnh0;->g()Z

    move-result v4

    if-nez v4, :cond_4

    invoke-virtual {v5}, Lnh0;->l()Z

    move-result v4

    if-eqz v4, :cond_3

    goto :goto_2

    :cond_3
    move v4, v2

    goto :goto_3

    :cond_4
    :goto_2
    const/4 v4, 0x1

    :goto_3
    or-int/2addr v3, v4

    goto :goto_1

    :cond_5
    if-eqz v3, :cond_7

    invoke-virtual {p1}, Lvh0;->c()Lvh0;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result p2

    :goto_4
    if-ge v2, p2, :cond_6

    invoke-virtual {p1}, Lvh0;->c()Lvh0;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lnh0;

    invoke-static {v3, p1}, Ler1;->a(Lnh0;Lvh0;)V

    iget-object v3, p0, Lbn0$a;->b:Lhz1;

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, p1, v4}, Lhz1;->d(Lvh0;Ljava/lang/Object;)V

    invoke-virtual {p1}, Lvh0;->f()Lvh0;

    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    :cond_6
    invoke-virtual {p1}, Lvh0;->f()Lvh0;

    goto :goto_6

    :cond_7
    invoke-virtual {p1}, Lvh0;->d()Lvh0;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result p2

    :goto_5
    if-ge v2, p2, :cond_8

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lnh0;

    invoke-virtual {p0, v3}, Lbn0$a;->e(Lnh0;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Lvh0;->o(Ljava/lang/String;)Lvh0;

    iget-object v3, p0, Lbn0$a;->b:Lhz1;

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, p1, v4}, Lhz1;->d(Lvh0;Ljava/lang/Object;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    :cond_8
    invoke-virtual {p1}, Lvh0;->g()Lvh0;

    :goto_6
    return-void
.end method

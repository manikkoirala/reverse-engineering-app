.class public final Lzf2;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:Lxf2;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lca1;Ld4;Ldd2;)V
    .locals 6

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lzf2;->a:Landroid/content/Context;

    new-instance p1, Lxf2;

    const/4 v5, 0x0

    move-object v0, p1

    move-object v1, p0

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lxf2;-><init>(Lzf2;Lca1;Ld4;Ldd2;Lmf2;)V

    iput-object p1, p0, Lzf2;->b:Lxf2;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lsd2;Ldd2;)V
    .locals 0

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lzf2;->a:Landroid/content/Context;

    new-instance p1, Lxf2;

    const/4 p2, 0x0

    invoke-direct {p1, p0, p2, p3, p2}, Lxf2;-><init>(Lzf2;Lsd2;Ldd2;Lmf2;)V

    iput-object p1, p0, Lzf2;->b:Lxf2;

    return-void
.end method

.method public static bridge synthetic a(Lzf2;)Landroid/content/Context;
    .locals 0

    .line 1
    iget-object p0, p0, Lzf2;->a:Landroid/content/Context;

    return-object p0
.end method

.method public static bridge synthetic b(Lzf2;)Lxf2;
    .locals 0

    .line 1
    iget-object p0, p0, Lzf2;->b:Lxf2;

    return-object p0
.end method


# virtual methods
.method public final c()Lsd2;
    .locals 1

    .line 1
    iget-object v0, p0, Lzf2;->b:Lxf2;

    invoke-static {v0}, Lxf2;->a(Lxf2;)Lsd2;

    const/4 v0, 0x0

    return-object v0
.end method

.method public final d()Lca1;
    .locals 1

    .line 1
    iget-object v0, p0, Lzf2;->b:Lxf2;

    invoke-static {v0}, Lxf2;->b(Lxf2;)Lca1;

    move-result-object v0

    return-object v0
.end method

.method public final e()V
    .locals 2

    .line 1
    iget-object v0, p0, Lzf2;->b:Lxf2;

    iget-object v1, p0, Lzf2;->a:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lxf2;->d(Landroid/content/Context;)V

    return-void
.end method

.method public final f(Z)V
    .locals 3

    .line 1
    new-instance p1, Landroid/content/IntentFilter;

    const-string v0, "com.android.vending.billing.PURCHASES_UPDATED"

    invoke-direct {p1, v0}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iget-object v0, p0, Lzf2;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    const-string v0, "com.android.vending.billing.ALTERNATIVE_BILLING"

    invoke-virtual {p1, v0}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v0, p0, Lzf2;->b:Lxf2;

    iget-object v1, p0, Lzf2;->a:Landroid/content/Context;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2, v2}, Lxf2;->c(Landroid/content/Context;Landroid/content/IntentFilter;Ljava/lang/String;Landroid/content/IntentFilter;)V

    return-void
.end method

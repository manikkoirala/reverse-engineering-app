.class public Lzi$b;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lzi;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "b"
.end annotation


# instance fields
.field public a:Ljava/lang/String;

.field public final b:Ljava/util/Set;

.field public final c:Ljava/util/Set;

.field public d:I

.field public e:I

.field public f:Lmj;

.field public final g:Ljava/util/Set;


# direct methods
.method public varargs constructor <init>(Lda1;[Lda1;)V
    .locals 3

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lzi$b;->a:Ljava/lang/String;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lzi$b;->b:Ljava/util/Set;

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lzi$b;->c:Ljava/util/Set;

    const/4 v1, 0x0

    iput v1, p0, Lzi$b;->d:I

    iput v1, p0, Lzi$b;->e:I

    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    iput-object v2, p0, Lzi$b;->g:Ljava/util/Set;

    const-string v2, "Null interface"

    invoke-static {p1, v2}, Lj71;->c(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    array-length p1, p2

    :goto_0
    if-ge v1, p1, :cond_0

    aget-object v0, p2, v1

    invoke-static {v0, v2}, Lj71;->c(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lzi$b;->b:Ljava/util/Set;

    invoke-static {p1, p2}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    return-void
.end method

.method public synthetic constructor <init>(Lda1;[Lda1;Lzi$a;)V
    .locals 0

    .line 2
    invoke-direct {p0, p1, p2}, Lzi$b;-><init>(Lda1;[Lda1;)V

    return-void
.end method

.method public varargs constructor <init>(Ljava/lang/Class;[Ljava/lang/Class;)V
    .locals 4

    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lzi$b;->a:Ljava/lang/String;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lzi$b;->b:Ljava/util/Set;

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lzi$b;->c:Ljava/util/Set;

    const/4 v1, 0x0

    iput v1, p0, Lzi$b;->d:I

    iput v1, p0, Lzi$b;->e:I

    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    iput-object v2, p0, Lzi$b;->g:Ljava/util/Set;

    const-string v2, "Null interface"

    invoke-static {p1, v2}, Lj71;->c(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    invoke-static {p1}, Lda1;->b(Ljava/lang/Class;)Lda1;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    array-length p1, p2

    :goto_0
    if-ge v1, p1, :cond_0

    aget-object v0, p2, v1

    invoke-static {v0, v2}, Lj71;->c(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    iget-object v3, p0, Lzi$b;->b:Ljava/util/Set;

    invoke-static {v0}, Lda1;->b(Ljava/lang/Class;)Lda1;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/Class;[Ljava/lang/Class;Lzi$a;)V
    .locals 0

    .line 4
    invoke-direct {p0, p1, p2}, Lzi$b;-><init>(Ljava/lang/Class;[Ljava/lang/Class;)V

    return-void
.end method

.method public static synthetic a(Lzi$b;)Lzi$b;
    .locals 0

    .line 1
    invoke-virtual {p0}, Lzi$b;->g()Lzi$b;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public b(Los;)Lzi$b;
    .locals 1

    .line 1
    const-string v0, "Null dependency"

    invoke-static {p1, v0}, Lj71;->c(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    invoke-virtual {p1}, Los;->c()Lda1;

    move-result-object v0

    invoke-virtual {p0, v0}, Lzi$b;->j(Lda1;)V

    iget-object v0, p0, Lzi$b;->c:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public c()Lzi$b;
    .locals 1

    .line 1
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lzi$b;->i(I)Lzi$b;

    move-result-object v0

    return-object v0
.end method

.method public d()Lzi;
    .locals 11

    .line 1
    iget-object v0, p0, Lzi$b;->f:Lmj;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    const-string v1, "Missing required property: factory."

    invoke-static {v0, v1}, Lj71;->d(ZLjava/lang/String;)V

    new-instance v0, Lzi;

    iget-object v3, p0, Lzi$b;->a:Ljava/lang/String;

    new-instance v4, Ljava/util/HashSet;

    iget-object v1, p0, Lzi$b;->b:Ljava/util/Set;

    invoke-direct {v4, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    new-instance v5, Ljava/util/HashSet;

    iget-object v1, p0, Lzi$b;->c:Ljava/util/Set;

    invoke-direct {v5, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    iget v6, p0, Lzi$b;->d:I

    iget v7, p0, Lzi$b;->e:I

    iget-object v8, p0, Lzi$b;->f:Lmj;

    iget-object v9, p0, Lzi$b;->g:Ljava/util/Set;

    const/4 v10, 0x0

    move-object v2, v0

    invoke-direct/range {v2 .. v10}, Lzi;-><init>(Ljava/lang/String;Ljava/util/Set;Ljava/util/Set;IILmj;Ljava/util/Set;Lzi$a;)V

    return-object v0
.end method

.method public e()Lzi$b;
    .locals 1

    .line 1
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lzi$b;->i(I)Lzi$b;

    move-result-object v0

    return-object v0
.end method

.method public f(Lmj;)Lzi$b;
    .locals 1

    .line 1
    const-string v0, "Null factory"

    invoke-static {p1, v0}, Lj71;->c(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lmj;

    iput-object p1, p0, Lzi$b;->f:Lmj;

    return-object p0
.end method

.method public final g()Lzi$b;
    .locals 1

    .line 1
    const/4 v0, 0x1

    iput v0, p0, Lzi$b;->e:I

    return-object p0
.end method

.method public h(Ljava/lang/String;)Lzi$b;
    .locals 0

    .line 1
    iput-object p1, p0, Lzi$b;->a:Ljava/lang/String;

    return-object p0
.end method

.method public final i(I)Lzi$b;
    .locals 2

    .line 1
    iget v0, p0, Lzi$b;->d:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    const-string v1, "Instantiation type has already been set."

    invoke-static {v0, v1}, Lj71;->d(ZLjava/lang/String;)V

    iput p1, p0, Lzi$b;->d:I

    return-object p0
.end method

.method public final j(Lda1;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lzi$b;->b:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result p1

    xor-int/lit8 p1, p1, 0x1

    const-string v0, "Components are not allowed to depend on interfaces they themselves provide."

    invoke-static {p1, v0}, Lj71;->a(ZLjava/lang/String;)V

    return-void
.end method

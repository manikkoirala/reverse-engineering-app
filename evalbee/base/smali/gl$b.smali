.class public final Lgl$b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lgl$c;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lgl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "b"
.end annotation


# instance fields
.field public final a:Landroid/view/ContentInfo$Builder;


# direct methods
.method public constructor <init>(Landroid/content/ClipData;I)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1, p2}, Ljl;->a(Landroid/content/ClipData;I)Landroid/view/ContentInfo$Builder;

    move-result-object p1

    iput-object p1, p0, Lgl$b;->a:Landroid/view/ContentInfo$Builder;

    return-void
.end method


# virtual methods
.method public a(Landroid/net/Uri;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lgl$b;->a:Landroid/view/ContentInfo$Builder;

    invoke-static {v0, p1}, Lll;->a(Landroid/view/ContentInfo$Builder;Landroid/net/Uri;)Landroid/view/ContentInfo$Builder;

    return-void
.end method

.method public b(I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lgl$b;->a:Landroid/view/ContentInfo$Builder;

    invoke-static {v0, p1}, Lhl;->a(Landroid/view/ContentInfo$Builder;I)Landroid/view/ContentInfo$Builder;

    return-void
.end method

.method public build()Lgl;
    .locals 3

    .line 1
    new-instance v0, Lgl;

    new-instance v1, Lgl$e;

    iget-object v2, p0, Lgl$b;->a:Landroid/view/ContentInfo$Builder;

    invoke-static {v2}, Lil;->a(Landroid/view/ContentInfo$Builder;)Landroid/view/ContentInfo;

    move-result-object v2

    invoke-direct {v1, v2}, Lgl$e;-><init>(Landroid/view/ContentInfo;)V

    invoke-direct {v0, v1}, Lgl;-><init>(Lgl$f;)V

    return-object v0
.end method

.method public setExtras(Landroid/os/Bundle;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lgl$b;->a:Landroid/view/ContentInfo$Builder;

    invoke-static {v0, p1}, Lkl;->a(Landroid/view/ContentInfo$Builder;Landroid/os/Bundle;)Landroid/view/ContentInfo$Builder;

    return-void
.end method

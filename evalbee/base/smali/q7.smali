.class public Lq7;
.super Landroid/widget/ToggleButton;
.source "SourceFile"


# instance fields
.field public final a:Lx5;

.field public final b:Ll7;

.field public c:Lv6;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .line 1
    const v0, 0x101004b

    invoke-direct {p0, p1, p2, v0}, Lq7;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .line 2
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ToggleButton;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p0, p1}, Lpv1;->a(Landroid/view/View;Landroid/content/Context;)V

    new-instance p1, Lx5;

    invoke-direct {p1, p0}, Lx5;-><init>(Landroid/view/View;)V

    iput-object p1, p0, Lq7;->a:Lx5;

    invoke-virtual {p1, p2, p3}, Lx5;->e(Landroid/util/AttributeSet;I)V

    new-instance p1, Ll7;

    invoke-direct {p1, p0}, Ll7;-><init>(Landroid/widget/TextView;)V

    iput-object p1, p0, Lq7;->b:Ll7;

    invoke-virtual {p1, p2, p3}, Ll7;->m(Landroid/util/AttributeSet;I)V

    invoke-direct {p0}, Lq7;->getEmojiTextViewHelper()Lv6;

    move-result-object p1

    invoke-virtual {p1, p2, p3}, Lv6;->c(Landroid/util/AttributeSet;I)V

    return-void
.end method

.method private getEmojiTextViewHelper()Lv6;
    .locals 1

    .line 1
    iget-object v0, p0, Lq7;->c:Lv6;

    if-nez v0, :cond_0

    new-instance v0, Lv6;

    invoke-direct {v0, p0}, Lv6;-><init>(Landroid/widget/TextView;)V

    iput-object v0, p0, Lq7;->c:Lv6;

    :cond_0
    iget-object v0, p0, Lq7;->c:Lv6;

    return-object v0
.end method


# virtual methods
.method public drawableStateChanged()V
    .locals 1

    .line 1
    invoke-super {p0}, Landroid/widget/ToggleButton;->drawableStateChanged()V

    iget-object v0, p0, Lq7;->a:Lx5;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lx5;->b()V

    :cond_0
    iget-object v0, p0, Lq7;->b:Ll7;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ll7;->b()V

    :cond_1
    return-void
.end method

.method public getSupportBackgroundTintList()Landroid/content/res/ColorStateList;
    .locals 1

    .line 1
    iget-object v0, p0, Lq7;->a:Lx5;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lx5;->c()Landroid/content/res/ColorStateList;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public getSupportBackgroundTintMode()Landroid/graphics/PorterDuff$Mode;
    .locals 1

    .line 1
    iget-object v0, p0, Lq7;->a:Lx5;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lx5;->d()Landroid/graphics/PorterDuff$Mode;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public getSupportCompoundDrawablesTintList()Landroid/content/res/ColorStateList;
    .locals 1

    .line 1
    iget-object v0, p0, Lq7;->b:Ll7;

    invoke-virtual {v0}, Ll7;->j()Landroid/content/res/ColorStateList;

    move-result-object v0

    return-object v0
.end method

.method public getSupportCompoundDrawablesTintMode()Landroid/graphics/PorterDuff$Mode;
    .locals 1

    .line 1
    iget-object v0, p0, Lq7;->b:Ll7;

    invoke-virtual {v0}, Ll7;->k()Landroid/graphics/PorterDuff$Mode;

    move-result-object v0

    return-object v0
.end method

.method public setAllCaps(Z)V
    .locals 1

    .line 1
    invoke-super {p0, p1}, Landroid/widget/TextView;->setAllCaps(Z)V

    invoke-direct {p0}, Lq7;->getEmojiTextViewHelper()Lv6;

    move-result-object v0

    invoke-virtual {v0, p1}, Lv6;->d(Z)V

    return-void
.end method

.method public setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .line 1
    invoke-super {p0, p1}, Landroid/widget/ToggleButton;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lq7;->a:Lx5;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lx5;->f(Landroid/graphics/drawable/Drawable;)V

    :cond_0
    return-void
.end method

.method public setBackgroundResource(I)V
    .locals 1

    .line 1
    invoke-super {p0, p1}, Landroid/view/View;->setBackgroundResource(I)V

    iget-object v0, p0, Lq7;->a:Lx5;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lx5;->g(I)V

    :cond_0
    return-void
.end method

.method public setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .line 1
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/TextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    iget-object p1, p0, Lq7;->b:Ll7;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ll7;->p()V

    :cond_0
    return-void
.end method

.method public setCompoundDrawablesRelative(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .line 1
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/TextView;->setCompoundDrawablesRelative(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    iget-object p1, p0, Lq7;->b:Ll7;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ll7;->p()V

    :cond_0
    return-void
.end method

.method public setEmojiCompatEnabled(Z)V
    .locals 1

    .line 1
    invoke-direct {p0}, Lq7;->getEmojiTextViewHelper()Lv6;

    move-result-object v0

    invoke-virtual {v0, p1}, Lv6;->e(Z)V

    return-void
.end method

.method public setFilters([Landroid/text/InputFilter;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Lq7;->getEmojiTextViewHelper()Lv6;

    move-result-object v0

    invoke-virtual {v0, p1}, Lv6;->a([Landroid/text/InputFilter;)[Landroid/text/InputFilter;

    move-result-object p1

    invoke-super {p0, p1}, Landroid/widget/TextView;->setFilters([Landroid/text/InputFilter;)V

    return-void
.end method

.method public setSupportBackgroundTintList(Landroid/content/res/ColorStateList;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lq7;->a:Lx5;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lx5;->i(Landroid/content/res/ColorStateList;)V

    :cond_0
    return-void
.end method

.method public setSupportBackgroundTintMode(Landroid/graphics/PorterDuff$Mode;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lq7;->a:Lx5;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lx5;->j(Landroid/graphics/PorterDuff$Mode;)V

    :cond_0
    return-void
.end method

.method public setSupportCompoundDrawablesTintList(Landroid/content/res/ColorStateList;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lq7;->b:Ll7;

    invoke-virtual {v0, p1}, Ll7;->w(Landroid/content/res/ColorStateList;)V

    iget-object p1, p0, Lq7;->b:Ll7;

    invoke-virtual {p1}, Ll7;->b()V

    return-void
.end method

.method public setSupportCompoundDrawablesTintMode(Landroid/graphics/PorterDuff$Mode;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lq7;->b:Ll7;

    invoke-virtual {v0, p1}, Ll7;->x(Landroid/graphics/PorterDuff$Mode;)V

    iget-object p1, p0, Lq7;->b:Ll7;

    invoke-virtual {p1}, Ll7;->b()V

    return-void
.end method

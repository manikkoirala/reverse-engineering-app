.class public Lve$c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/TextureView$SurfaceTextureListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lve;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Lve;


# direct methods
.method public constructor <init>(Lve;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lve$c;->a:Lve;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSurfaceTextureAvailable(Landroid/graphics/SurfaceTexture;II)V
    .locals 3

    .line 1
    :try_start_0
    iget-object p2, p0, Lve$c;->a:Lve;

    invoke-static {}, Landroid/hardware/Camera;->open()Landroid/hardware/Camera;

    move-result-object p3

    invoke-static {p2, p3}, Lve;->r(Lve;Landroid/hardware/Camera;)Landroid/hardware/Camera;

    iget-object p2, p0, Lve$c;->a:Lve;

    invoke-static {p2}, Lve;->q(Lve;)Landroid/hardware/Camera;

    move-result-object p2

    iget-object p3, p0, Lve$c;->a:Lve;

    invoke-static {p3}, Lve;->t(Lve;)Landroid/hardware/Camera$PreviewCallback;

    move-result-object p3

    invoke-virtual {p2, p3}, Landroid/hardware/Camera;->setPreviewCallback(Landroid/hardware/Camera$PreviewCallback;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1

    iget-object p2, p0, Lve$c;->a:Lve;

    invoke-static {p2}, Lve;->q(Lve;)Landroid/hardware/Camera;

    move-result-object p2

    invoke-virtual {p2}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object p2

    iget-object p3, p0, Lve$c;->a:Lve;

    invoke-static {p3}, Lve;->u(Lve;)Z

    move-result p3

    if-eqz p3, :cond_0

    iget-object p3, p0, Lve$c;->a:Lve;

    invoke-static {p3, p2}, Lve;->x(Lve;Landroid/hardware/Camera$Parameters;)Landroid/hardware/Camera$Size;

    move-result-object v0

    invoke-static {p3, v0}, Lve;->w(Lve;Landroid/hardware/Camera$Size;)Landroid/hardware/Camera$Size;

    iget-object p3, p0, Lve$c;->a:Lve;

    invoke-static {p3}, Lve;->v(Lve;)Landroid/hardware/Camera$Size;

    move-result-object v0

    invoke-static {p3, p2, v0}, Lve;->h(Lve;Landroid/hardware/Camera$Parameters;Landroid/hardware/Camera$Size;)Landroid/hardware/Camera$Size;

    move-result-object v0

    invoke-static {p3, v0}, Lve;->f(Lve;Landroid/hardware/Camera$Size;)Landroid/hardware/Camera$Size;

    goto :goto_0

    :cond_0
    iget-object p3, p0, Lve$c;->a:Lve;

    invoke-static {p3}, Lve;->i(Lve;)I

    move-result v0

    invoke-static {p3, p2, v0}, Lve;->j(Lve;Landroid/hardware/Camera$Parameters;I)Landroid/hardware/Camera$Size;

    move-result-object v0

    invoke-static {p3, v0}, Lve;->w(Lve;Landroid/hardware/Camera$Size;)Landroid/hardware/Camera$Size;

    :goto_0
    iget-object p3, p0, Lve$c;->a:Lve;

    invoke-static {p3}, Lve;->v(Lve;)Landroid/hardware/Camera$Size;

    move-result-object p3

    iget p3, p3, Landroid/hardware/Camera$Size;->width:I

    iget-object v0, p0, Lve$c;->a:Lve;

    invoke-static {v0}, Lve;->v(Lve;)Landroid/hardware/Camera$Size;

    move-result-object v0

    iget v0, v0, Landroid/hardware/Camera$Size;->height:I

    invoke-virtual {p2, p3, v0}, Landroid/hardware/Camera$Parameters;->setPreviewSize(II)V

    iget-object p3, p0, Lve$c;->a:Lve;

    invoke-static {p3}, Lve;->e(Lve;)Landroid/hardware/Camera$Size;

    move-result-object p3

    if-eqz p3, :cond_1

    iget-object p3, p0, Lve$c;->a:Lve;

    invoke-static {p3}, Lve;->e(Lve;)Landroid/hardware/Camera$Size;

    move-result-object p3

    iget p3, p3, Landroid/hardware/Camera$Size;->width:I

    iget-object v0, p0, Lve$c;->a:Lve;

    invoke-static {v0}, Lve;->e(Lve;)Landroid/hardware/Camera$Size;

    move-result-object v0

    iget v0, v0, Landroid/hardware/Camera$Size;->height:I

    invoke-virtual {p2, p3, v0}, Landroid/hardware/Camera$Parameters;->setPictureSize(II)V

    :cond_1
    const/16 p3, 0x5a

    invoke-virtual {p2, p3}, Landroid/hardware/Camera$Parameters;->setRotation(I)V

    invoke-virtual {p2}, Landroid/hardware/Camera$Parameters;->getSupportedFocusModes()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_2

    const-string v1, "continuous-picture"

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v0, p0, Lve$c;->a:Lve;

    const/4 v2, 0x0

    invoke-static {v0, v2}, Lve;->l(Lve;Z)Z

    invoke-virtual {p2, v1}, Landroid/hardware/Camera$Parameters;->setFocusMode(Ljava/lang/String;)V

    goto :goto_1

    :cond_2
    if-eqz v0, :cond_3

    const-string v1, "auto"

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p2, v1}, Landroid/hardware/Camera$Parameters;->setFocusMode(Ljava/lang/String;)V

    iget-object v0, p0, Lve$c;->a:Lve;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lve;->l(Lve;Z)Z

    :cond_3
    :goto_1
    iget-object v0, p0, Lve$c;->a:Lve;

    invoke-static {v0}, Lve;->q(Lve;)Landroid/hardware/Camera;

    move-result-object v0

    invoke-virtual {v0, p2}, Landroid/hardware/Camera;->setParameters(Landroid/hardware/Camera$Parameters;)V

    iget-object p2, p0, Lve$c;->a:Lve;

    invoke-static {p2}, Lve;->q(Lve;)Landroid/hardware/Camera;

    move-result-object p2

    invoke-virtual {p2, p3}, Landroid/hardware/Camera;->setDisplayOrientation(I)V

    :try_start_1
    iget-object p2, p0, Lve$c;->a:Lve;

    invoke-static {p2}, Lve;->m(Lve;)Lcom/ekodroid/omrevaluator/components/AutofitPreview;

    move-result-object p2

    iget-object p3, p0, Lve$c;->a:Lve;

    invoke-static {p3}, Lve;->v(Lve;)Landroid/hardware/Camera$Size;

    move-result-object p3

    iget p3, p3, Landroid/hardware/Camera$Size;->height:I

    iget-object v0, p0, Lve$c;->a:Lve;

    invoke-static {v0}, Lve;->v(Lve;)Landroid/hardware/Camera$Size;

    move-result-object v0

    iget v0, v0, Landroid/hardware/Camera$Size;->width:I

    invoke-virtual {p2, p3, v0}, Lcom/ekodroid/omrevaluator/components/AutofitPreview;->a(II)V

    iget-object p2, p0, Lve$c;->a:Lve;

    invoke-static {p2}, Lve;->q(Lve;)Landroid/hardware/Camera;

    move-result-object p2

    invoke-virtual {p2, p1}, Landroid/hardware/Camera;->setPreviewTexture(Landroid/graphics/SurfaceTexture;)V

    iget-object p1, p0, Lve$c;->a:Lve;

    invoke-static {p1}, Lve;->q(Lve;)Landroid/hardware/Camera;

    move-result-object p1

    invoke-virtual {p1}, Landroid/hardware/Camera;->startPreview()V

    iget-object p1, p0, Lve$c;->a:Lve;

    invoke-static {p1}, Lve;->k(Lve;)Z

    move-result p1

    if-eqz p1, :cond_4

    iget-object p1, p0, Lve$c;->a:Lve;

    invoke-static {p1}, Lve;->q(Lve;)Landroid/hardware/Camera;

    move-result-object p1

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Landroid/hardware/Camera;->autoFocus(Landroid/hardware/Camera$AutoFocusCallback;)V

    :cond_4
    iget-object p1, p0, Lve$c;->a:Lve;

    invoke-static {p1}, Lve;->n(Lve;)Lxe;

    move-result-object p1

    if-eqz p1, :cond_5

    iget-object p1, p0, Lve$c;->a:Lve;

    invoke-static {p1}, Lve;->n(Lve;)Lxe;

    move-result-object p1

    iget-object p2, p0, Lve$c;->a:Lve;

    invoke-static {p2}, Lve;->v(Lve;)Landroid/hardware/Camera$Size;

    move-result-object p2

    iget-object p3, p0, Lve$c;->a:Lve;

    invoke-static {p3}, Lve;->e(Lve;)Landroid/hardware/Camera$Size;

    move-result-object p3

    invoke-interface {p1, p2, p3}, Lxe;->a(Landroid/hardware/Camera$Size;Landroid/hardware/Camera$Size;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    :cond_5
    return-void

    :catch_0
    move-exception p1

    :goto_2
    sget-object p2, Ljava/lang/System;->err:Ljava/io/PrintStream;

    invoke-virtual {p2, p1}, Ljava/io/PrintStream;->println(Ljava/lang/Object;)V

    return-void

    :catch_1
    move-exception p1

    goto :goto_2
.end method

.method public onSurfaceTextureDestroyed(Landroid/graphics/SurfaceTexture;)Z
    .locals 0

    .line 1
    const/4 p1, 0x0

    return p1
.end method

.method public onSurfaceTextureSizeChanged(Landroid/graphics/SurfaceTexture;II)V
    .locals 0

    .line 1
    return-void
.end method

.method public onSurfaceTextureUpdated(Landroid/graphics/SurfaceTexture;)V
    .locals 0

    .line 1
    return-void
.end method

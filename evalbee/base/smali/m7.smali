.class public Lm7;
.super Landroid/widget/TextView;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lm7$c;,
        Lm7$b;,
        Lm7$a;
    }
.end annotation


# instance fields
.field private final mBackgroundTintHelper:Lx5;

.field private mEmojiTextViewHelper:Lv6;

.field private mIsSetTypefaceProcessing:Z

.field private mPrecomputedTextFuture:Ljava/util/concurrent/Future;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/Future<",
            "Lg71;",
            ">;"
        }
    .end annotation
.end field

.field private mSuperCaller:Lm7$a;

.field private final mTextClassifierHelper:Lk7;

.field private final mTextHelper:Ll7;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .line 1
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lm7;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .line 2
    const v0, 0x1010084

    invoke-direct {p0, p1, p2, v0}, Lm7;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .line 3
    invoke-static {p1}, Lqw1;->b(Landroid/content/Context;)Landroid/content/Context;

    move-result-object p1

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 p1, 0x0

    iput-boolean p1, p0, Lm7;->mIsSetTypefaceProcessing:Z

    const/4 p1, 0x0

    iput-object p1, p0, Lm7;->mSuperCaller:Lm7$a;

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p0, p1}, Lpv1;->a(Landroid/view/View;Landroid/content/Context;)V

    new-instance p1, Lx5;

    invoke-direct {p1, p0}, Lx5;-><init>(Landroid/view/View;)V

    iput-object p1, p0, Lm7;->mBackgroundTintHelper:Lx5;

    invoke-virtual {p1, p2, p3}, Lx5;->e(Landroid/util/AttributeSet;I)V

    new-instance p1, Ll7;

    invoke-direct {p1, p0}, Ll7;-><init>(Landroid/widget/TextView;)V

    iput-object p1, p0, Lm7;->mTextHelper:Ll7;

    invoke-virtual {p1, p2, p3}, Ll7;->m(Landroid/util/AttributeSet;I)V

    invoke-virtual {p1}, Ll7;->b()V

    new-instance p1, Lk7;

    invoke-direct {p1, p0}, Lk7;-><init>(Landroid/widget/TextView;)V

    iput-object p1, p0, Lm7;->mTextClassifierHelper:Lk7;

    invoke-direct {p0}, Lm7;->getEmojiTextViewHelper()Lv6;

    move-result-object p1

    invoke-virtual {p1, p2, p3}, Lv6;->c(Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public static synthetic access$001(Lm7;)I
    .locals 0

    .line 1
    invoke-super {p0}, Landroid/widget/TextView;->getAutoSizeMaxTextSize()I

    move-result p0

    return p0
.end method

.method public static synthetic access$1001(Lm7;I)V
    .locals 0

    .line 1
    invoke-super {p0, p1}, Landroid/widget/TextView;->setFirstBaselineToTopHeight(I)V

    return-void
.end method

.method public static synthetic access$101(Lm7;)I
    .locals 0

    .line 1
    invoke-super {p0}, Landroid/widget/TextView;->getAutoSizeMinTextSize()I

    move-result p0

    return p0
.end method

.method public static synthetic access$1101(Lm7;I)V
    .locals 0

    .line 1
    invoke-super {p0, p1}, Landroid/widget/TextView;->setLastBaselineToBottomHeight(I)V

    return-void
.end method

.method public static synthetic access$201(Lm7;)I
    .locals 0

    .line 1
    invoke-super {p0}, Landroid/widget/TextView;->getAutoSizeStepGranularity()I

    move-result p0

    return p0
.end method

.method public static synthetic access$301(Lm7;)[I
    .locals 0

    .line 1
    invoke-super {p0}, Landroid/widget/TextView;->getAutoSizeTextAvailableSizes()[I

    move-result-object p0

    return-object p0
.end method

.method public static synthetic access$401(Lm7;)I
    .locals 0

    .line 1
    invoke-super {p0}, Landroid/widget/TextView;->getAutoSizeTextType()I

    move-result p0

    return p0
.end method

.method public static synthetic access$501(Lm7;)Landroid/view/textclassifier/TextClassifier;
    .locals 0

    .line 1
    invoke-super {p0}, Landroid/widget/TextView;->getTextClassifier()Landroid/view/textclassifier/TextClassifier;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic access$601(Lm7;IIII)V
    .locals 0

    .line 1
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/TextView;->setAutoSizeTextTypeUniformWithConfiguration(IIII)V

    return-void
.end method

.method public static synthetic access$701(Lm7;[II)V
    .locals 0

    .line 1
    invoke-super {p0, p1, p2}, Landroid/widget/TextView;->setAutoSizeTextTypeUniformWithPresetSizes([II)V

    return-void
.end method

.method public static synthetic access$801(Lm7;I)V
    .locals 0

    .line 1
    invoke-super {p0, p1}, Landroid/widget/TextView;->setAutoSizeTextTypeWithDefaults(I)V

    return-void
.end method

.method public static synthetic access$901(Lm7;Landroid/view/textclassifier/TextClassifier;)V
    .locals 0

    .line 1
    invoke-super {p0, p1}, Landroid/widget/TextView;->setTextClassifier(Landroid/view/textclassifier/TextClassifier;)V

    return-void
.end method

.method private getEmojiTextViewHelper()Lv6;
    .locals 1

    .line 1
    iget-object v0, p0, Lm7;->mEmojiTextViewHelper:Lv6;

    if-nez v0, :cond_0

    new-instance v0, Lv6;

    invoke-direct {v0, p0}, Lv6;-><init>(Landroid/widget/TextView;)V

    iput-object v0, p0, Lm7;->mEmojiTextViewHelper:Lv6;

    :cond_0
    iget-object v0, p0, Lm7;->mEmojiTextViewHelper:Lv6;

    return-object v0
.end method


# virtual methods
.method public final c()V
    .locals 2

    .line 1
    iget-object v0, p0, Lm7;->mPrecomputedTextFuture:Ljava/util/concurrent/Future;

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    :try_start_0
    iput-object v1, p0, Lm7;->mPrecomputedTextFuture:Ljava/util/concurrent/Future;

    invoke-interface {v0}, Ljava/util/concurrent/Future;->get()Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lzu0;->a(Ljava/lang/Object;)V

    invoke-static {p0, v1}, Lnv1;->n(Landroid/widget/TextView;Lg71;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    :cond_0
    return-void
.end method

.method public drawableStateChanged()V
    .locals 1

    .line 1
    invoke-super {p0}, Landroid/widget/TextView;->drawableStateChanged()V

    iget-object v0, p0, Lm7;->mBackgroundTintHelper:Lx5;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lx5;->b()V

    :cond_0
    iget-object v0, p0, Lm7;->mTextHelper:Ll7;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ll7;->b()V

    :cond_1
    return-void
.end method

.method public getAutoSizeMaxTextSize()I
    .locals 1

    .line 1
    sget-boolean v0, Lu42;->b:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lm7;->getSuperCaller()Lm7$a;

    move-result-object v0

    invoke-interface {v0}, Lm7$a;->c()I

    move-result v0

    return v0

    :cond_0
    iget-object v0, p0, Lm7;->mTextHelper:Ll7;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ll7;->e()I

    move-result v0

    return v0

    :cond_1
    const/4 v0, -0x1

    return v0
.end method

.method public getAutoSizeMinTextSize()I
    .locals 1

    .line 1
    sget-boolean v0, Lu42;->b:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lm7;->getSuperCaller()Lm7$a;

    move-result-object v0

    invoke-interface {v0}, Lm7$a;->a()I

    move-result v0

    return v0

    :cond_0
    iget-object v0, p0, Lm7;->mTextHelper:Ll7;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ll7;->f()I

    move-result v0

    return v0

    :cond_1
    const/4 v0, -0x1

    return v0
.end method

.method public getAutoSizeStepGranularity()I
    .locals 1

    .line 1
    sget-boolean v0, Lu42;->b:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lm7;->getSuperCaller()Lm7$a;

    move-result-object v0

    invoke-interface {v0}, Lm7$a;->f()I

    move-result v0

    return v0

    :cond_0
    iget-object v0, p0, Lm7;->mTextHelper:Ll7;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ll7;->g()I

    move-result v0

    return v0

    :cond_1
    const/4 v0, -0x1

    return v0
.end method

.method public getAutoSizeTextAvailableSizes()[I
    .locals 1

    .line 1
    sget-boolean v0, Lu42;->b:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lm7;->getSuperCaller()Lm7$a;

    move-result-object v0

    invoke-interface {v0}, Lm7$a;->k()[I

    move-result-object v0

    return-object v0

    :cond_0
    iget-object v0, p0, Lm7;->mTextHelper:Ll7;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ll7;->h()[I

    move-result-object v0

    return-object v0

    :cond_1
    const/4 v0, 0x0

    new-array v0, v0, [I

    return-object v0
.end method

.method public getAutoSizeTextType()I
    .locals 3

    .line 1
    sget-boolean v0, Lu42;->b:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lm7;->getSuperCaller()Lm7$a;

    move-result-object v0

    invoke-interface {v0}, Lm7$a;->j()I

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    move v1, v2

    :cond_0
    return v1

    :cond_1
    iget-object v0, p0, Lm7;->mTextHelper:Ll7;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ll7;->i()I

    move-result v0

    return v0

    :cond_2
    return v1
.end method

.method public getCustomSelectionActionModeCallback()Landroid/view/ActionMode$Callback;
    .locals 1

    .line 1
    invoke-super {p0}, Landroid/widget/TextView;->getCustomSelectionActionModeCallback()Landroid/view/ActionMode$Callback;

    move-result-object v0

    invoke-static {v0}, Lnv1;->q(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode$Callback;

    move-result-object v0

    return-object v0
.end method

.method public getFirstBaselineToTopHeight()I
    .locals 1

    .line 1
    invoke-static {p0}, Lnv1;->b(Landroid/widget/TextView;)I

    move-result v0

    return v0
.end method

.method public getLastBaselineToBottomHeight()I
    .locals 1

    .line 1
    invoke-static {p0}, Lnv1;->c(Landroid/widget/TextView;)I

    move-result v0

    return v0
.end method

.method public getSuperCaller()Lm7$a;
    .locals 2

    .line 1
    iget-object v0, p0, Lm7;->mSuperCaller:Lm7$a;

    if-nez v0, :cond_1

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x1c

    if-lt v0, v1, :cond_0

    new-instance v0, Lm7$c;

    invoke-direct {v0, p0}, Lm7$c;-><init>(Lm7;)V

    :goto_0
    iput-object v0, p0, Lm7;->mSuperCaller:Lm7$a;

    goto :goto_1

    :cond_0
    const/16 v1, 0x1a

    if-lt v0, v1, :cond_1

    new-instance v0, Lm7$b;

    invoke-direct {v0, p0}, Lm7$b;-><init>(Lm7;)V

    goto :goto_0

    :cond_1
    :goto_1
    iget-object v0, p0, Lm7;->mSuperCaller:Lm7$a;

    return-object v0
.end method

.method public getSupportBackgroundTintList()Landroid/content/res/ColorStateList;
    .locals 1

    .line 1
    iget-object v0, p0, Lm7;->mBackgroundTintHelper:Lx5;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lx5;->c()Landroid/content/res/ColorStateList;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public getSupportBackgroundTintMode()Landroid/graphics/PorterDuff$Mode;
    .locals 1

    .line 1
    iget-object v0, p0, Lm7;->mBackgroundTintHelper:Lx5;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lx5;->d()Landroid/graphics/PorterDuff$Mode;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public getSupportCompoundDrawablesTintList()Landroid/content/res/ColorStateList;
    .locals 1

    .line 1
    iget-object v0, p0, Lm7;->mTextHelper:Ll7;

    invoke-virtual {v0}, Ll7;->j()Landroid/content/res/ColorStateList;

    move-result-object v0

    return-object v0
.end method

.method public getSupportCompoundDrawablesTintMode()Landroid/graphics/PorterDuff$Mode;
    .locals 1

    .line 1
    iget-object v0, p0, Lm7;->mTextHelper:Ll7;

    invoke-virtual {v0}, Ll7;->k()Landroid/graphics/PorterDuff$Mode;

    move-result-object v0

    return-object v0
.end method

.method public getText()Ljava/lang/CharSequence;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lm7;->c()V

    invoke-super {p0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public getTextClassifier()Landroid/view/textclassifier/TextClassifier;
    .locals 2

    .line 1
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x1c

    if-ge v0, v1, :cond_1

    iget-object v0, p0, Lm7;->mTextClassifierHelper:Lk7;

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Lk7;->a()Landroid/view/textclassifier/TextClassifier;

    move-result-object v0

    return-object v0

    :cond_1
    :goto_0
    invoke-virtual {p0}, Lm7;->getSuperCaller()Lm7$a;

    move-result-object v0

    invoke-interface {v0}, Lm7$a;->b()Landroid/view/textclassifier/TextClassifier;

    move-result-object v0

    return-object v0
.end method

.method public getTextMetricsParamsCompat()Lg71$a;
    .locals 1

    .line 1
    invoke-static {p0}, Lnv1;->g(Landroid/widget/TextView;)Lg71$a;

    move-result-object v0

    return-object v0
.end method

.method public isEmojiCompatEnabled()Z
    .locals 1

    .line 1
    invoke-direct {p0}, Lm7;->getEmojiTextViewHelper()Lv6;

    move-result-object v0

    invoke-virtual {v0}, Lv6;->b()Z

    move-result v0

    return v0
.end method

.method public onCreateInputConnection(Landroid/view/inputmethod/EditorInfo;)Landroid/view/inputmethod/InputConnection;
    .locals 2

    .line 1
    invoke-super {p0, p1}, Landroid/widget/TextView;->onCreateInputConnection(Landroid/view/inputmethod/EditorInfo;)Landroid/view/inputmethod/InputConnection;

    move-result-object v0

    iget-object v1, p0, Lm7;->mTextHelper:Ll7;

    invoke-virtual {v1, p0, v0, p1}, Ll7;->r(Landroid/widget/TextView;Landroid/view/inputmethod/InputConnection;Landroid/view/inputmethod/EditorInfo;)V

    invoke-static {v0, p1, p0}, Lw6;->a(Landroid/view/inputmethod/InputConnection;Landroid/view/inputmethod/EditorInfo;Landroid/view/View;)Landroid/view/inputmethod/InputConnection;

    move-result-object p1

    return-object p1
.end method

.method public onLayout(ZIIII)V
    .locals 6

    .line 1
    invoke-super/range {p0 .. p5}, Landroid/widget/TextView;->onLayout(ZIIII)V

    iget-object v0, p0, Lm7;->mTextHelper:Ll7;

    if-eqz v0, :cond_0

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Ll7;->o(ZIIII)V

    :cond_0
    return-void
.end method

.method public onMeasure(II)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lm7;->c()V

    invoke-super {p0, p1, p2}, Landroid/widget/TextView;->onMeasure(II)V

    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .line 1
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/TextView;->onTextChanged(Ljava/lang/CharSequence;III)V

    iget-object p1, p0, Lm7;->mTextHelper:Ll7;

    if-eqz p1, :cond_0

    sget-boolean p2, Lu42;->b:Z

    if-nez p2, :cond_0

    invoke-virtual {p1}, Ll7;->l()Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    if-eqz p1, :cond_1

    iget-object p1, p0, Lm7;->mTextHelper:Ll7;

    invoke-virtual {p1}, Ll7;->c()V

    :cond_1
    return-void
.end method

.method public setAllCaps(Z)V
    .locals 1

    .line 1
    invoke-super {p0, p1}, Landroid/widget/TextView;->setAllCaps(Z)V

    invoke-direct {p0}, Lm7;->getEmojiTextViewHelper()Lv6;

    move-result-object v0

    invoke-virtual {v0, p1}, Lv6;->d(Z)V

    return-void
.end method

.method public setAutoSizeTextTypeUniformWithConfiguration(IIII)V
    .locals 1

    .line 1
    sget-boolean v0, Lu42;->b:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lm7;->getSuperCaller()Lm7$a;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3, p4}, Lm7$a;->d(IIII)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lm7;->mTextHelper:Ll7;

    if-eqz v0, :cond_1

    invoke-virtual {v0, p1, p2, p3, p4}, Ll7;->t(IIII)V

    :cond_1
    :goto_0
    return-void
.end method

.method public setAutoSizeTextTypeUniformWithPresetSizes([II)V
    .locals 1

    .line 1
    sget-boolean v0, Lu42;->b:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lm7;->getSuperCaller()Lm7$a;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lm7$a;->h([II)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lm7;->mTextHelper:Ll7;

    if-eqz v0, :cond_1

    invoke-virtual {v0, p1, p2}, Ll7;->u([II)V

    :cond_1
    :goto_0
    return-void
.end method

.method public setAutoSizeTextTypeWithDefaults(I)V
    .locals 1

    .line 1
    sget-boolean v0, Lu42;->b:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lm7;->getSuperCaller()Lm7$a;

    move-result-object v0

    invoke-interface {v0, p1}, Lm7$a;->g(I)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lm7;->mTextHelper:Ll7;

    if-eqz v0, :cond_1

    invoke-virtual {v0, p1}, Ll7;->v(I)V

    :cond_1
    :goto_0
    return-void
.end method

.method public setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .line 1
    invoke-super {p0, p1}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lm7;->mBackgroundTintHelper:Lx5;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lx5;->f(Landroid/graphics/drawable/Drawable;)V

    :cond_0
    return-void
.end method

.method public setBackgroundResource(I)V
    .locals 1

    .line 1
    invoke-super {p0, p1}, Landroid/view/View;->setBackgroundResource(I)V

    iget-object v0, p0, Lm7;->mBackgroundTintHelper:Lx5;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lx5;->g(I)V

    :cond_0
    return-void
.end method

.method public setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .line 1
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/TextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    iget-object p1, p0, Lm7;->mTextHelper:Ll7;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ll7;->p()V

    :cond_0
    return-void
.end method

.method public setCompoundDrawablesRelative(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .line 1
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/TextView;->setCompoundDrawablesRelative(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    iget-object p1, p0, Lm7;->mTextHelper:Ll7;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ll7;->p()V

    :cond_0
    return-void
.end method

.method public setCompoundDrawablesRelativeWithIntrinsicBounds(IIII)V
    .locals 2

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    invoke-static {v0, p1}, Lg7;->b(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    goto :goto_0

    :cond_0
    move-object p1, v1

    :goto_0
    if-eqz p2, :cond_1

    invoke-static {v0, p2}, Lg7;->b(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object p2

    goto :goto_1

    :cond_1
    move-object p2, v1

    :goto_1
    if-eqz p3, :cond_2

    invoke-static {v0, p3}, Lg7;->b(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object p3

    goto :goto_2

    :cond_2
    move-object p3, v1

    :goto_2
    if-eqz p4, :cond_3

    invoke-static {v0, p4}, Lg7;->b(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    :cond_3
    invoke-virtual {p0, p1, p2, p3, v1}, Lm7;->setCompoundDrawablesRelativeWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    iget-object p1, p0, Lm7;->mTextHelper:Ll7;

    if-eqz p1, :cond_4

    invoke-virtual {p1}, Ll7;->p()V

    :cond_4
    return-void
.end method

.method public setCompoundDrawablesRelativeWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .line 2
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/TextView;->setCompoundDrawablesRelativeWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    iget-object p1, p0, Lm7;->mTextHelper:Ll7;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ll7;->p()V

    :cond_0
    return-void
.end method

.method public setCompoundDrawablesWithIntrinsicBounds(IIII)V
    .locals 2

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    invoke-static {v0, p1}, Lg7;->b(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    goto :goto_0

    :cond_0
    move-object p1, v1

    :goto_0
    if-eqz p2, :cond_1

    invoke-static {v0, p2}, Lg7;->b(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object p2

    goto :goto_1

    :cond_1
    move-object p2, v1

    :goto_1
    if-eqz p3, :cond_2

    invoke-static {v0, p3}, Lg7;->b(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object p3

    goto :goto_2

    :cond_2
    move-object p3, v1

    :goto_2
    if-eqz p4, :cond_3

    invoke-static {v0, p4}, Lg7;->b(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    :cond_3
    invoke-virtual {p0, p1, p2, p3, v1}, Lm7;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    iget-object p1, p0, Lm7;->mTextHelper:Ll7;

    if-eqz p1, :cond_4

    invoke-virtual {p1}, Ll7;->p()V

    :cond_4
    return-void
.end method

.method public setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .line 2
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    iget-object p1, p0, Lm7;->mTextHelper:Ll7;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ll7;->p()V

    :cond_0
    return-void
.end method

.method public setCustomSelectionActionModeCallback(Landroid/view/ActionMode$Callback;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lnv1;->r(Landroid/widget/TextView;Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode$Callback;

    move-result-object p1

    invoke-super {p0, p1}, Landroid/widget/TextView;->setCustomSelectionActionModeCallback(Landroid/view/ActionMode$Callback;)V

    return-void
.end method

.method public setEmojiCompatEnabled(Z)V
    .locals 1

    .line 1
    invoke-direct {p0}, Lm7;->getEmojiTextViewHelper()Lv6;

    move-result-object v0

    invoke-virtual {v0, p1}, Lv6;->e(Z)V

    return-void
.end method

.method public setFilters([Landroid/text/InputFilter;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Lm7;->getEmojiTextViewHelper()Lv6;

    move-result-object v0

    invoke-virtual {v0, p1}, Lv6;->a([Landroid/text/InputFilter;)[Landroid/text/InputFilter;

    move-result-object p1

    invoke-super {p0, p1}, Landroid/widget/TextView;->setFilters([Landroid/text/InputFilter;)V

    return-void
.end method

.method public setFirstBaselineToTopHeight(I)V
    .locals 2

    .line 1
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x1c

    if-lt v0, v1, :cond_0

    invoke-virtual {p0}, Lm7;->getSuperCaller()Lm7$a;

    move-result-object v0

    invoke-interface {v0, p1}, Lm7$a;->e(I)V

    goto :goto_0

    :cond_0
    invoke-static {p0, p1}, Lnv1;->k(Landroid/widget/TextView;I)V

    :goto_0
    return-void
.end method

.method public setLastBaselineToBottomHeight(I)V
    .locals 2

    .line 1
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x1c

    if-lt v0, v1, :cond_0

    invoke-virtual {p0}, Lm7;->getSuperCaller()Lm7$a;

    move-result-object v0

    invoke-interface {v0, p1}, Lm7$a;->i(I)V

    goto :goto_0

    :cond_0
    invoke-static {p0, p1}, Lnv1;->l(Landroid/widget/TextView;I)V

    :goto_0
    return-void
.end method

.method public setLineHeight(I)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lnv1;->m(Landroid/widget/TextView;I)V

    return-void
.end method

.method public setPrecomputedText(Lg71;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lnv1;->n(Landroid/widget/TextView;Lg71;)V

    return-void
.end method

.method public setSupportBackgroundTintList(Landroid/content/res/ColorStateList;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lm7;->mBackgroundTintHelper:Lx5;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lx5;->i(Landroid/content/res/ColorStateList;)V

    :cond_0
    return-void
.end method

.method public setSupportBackgroundTintMode(Landroid/graphics/PorterDuff$Mode;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lm7;->mBackgroundTintHelper:Lx5;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lx5;->j(Landroid/graphics/PorterDuff$Mode;)V

    :cond_0
    return-void
.end method

.method public setSupportCompoundDrawablesTintList(Landroid/content/res/ColorStateList;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lm7;->mTextHelper:Ll7;

    invoke-virtual {v0, p1}, Ll7;->w(Landroid/content/res/ColorStateList;)V

    iget-object p1, p0, Lm7;->mTextHelper:Ll7;

    invoke-virtual {p1}, Ll7;->b()V

    return-void
.end method

.method public setSupportCompoundDrawablesTintMode(Landroid/graphics/PorterDuff$Mode;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lm7;->mTextHelper:Ll7;

    invoke-virtual {v0, p1}, Ll7;->x(Landroid/graphics/PorterDuff$Mode;)V

    iget-object p1, p0, Lm7;->mTextHelper:Ll7;

    invoke-virtual {p1}, Ll7;->b()V

    return-void
.end method

.method public setTextAppearance(Landroid/content/Context;I)V
    .locals 1

    .line 1
    invoke-super {p0, p1, p2}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    iget-object v0, p0, Lm7;->mTextHelper:Ll7;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1, p2}, Ll7;->q(Landroid/content/Context;I)V

    :cond_0
    return-void
.end method

.method public setTextClassifier(Landroid/view/textclassifier/TextClassifier;)V
    .locals 2

    .line 1
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x1c

    if-ge v0, v1, :cond_1

    iget-object v0, p0, Lm7;->mTextClassifierHelper:Lk7;

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {v0, p1}, Lk7;->b(Landroid/view/textclassifier/TextClassifier;)V

    return-void

    :cond_1
    :goto_0
    invoke-virtual {p0}, Lm7;->getSuperCaller()Lm7$a;

    move-result-object v0

    invoke-interface {v0, p1}, Lm7$a;->l(Landroid/view/textclassifier/TextClassifier;)V

    return-void
.end method

.method public setTextFuture(Ljava/util/concurrent/Future;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/Future<",
            "Lg71;",
            ">;)V"
        }
    .end annotation

    .line 1
    iput-object p1, p0, Lm7;->mPrecomputedTextFuture:Ljava/util/concurrent/Future;

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Landroid/view/View;->requestLayout()V

    :cond_0
    return-void
.end method

.method public setTextMetricsParamsCompat(Lg71$a;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lnv1;->p(Landroid/widget/TextView;Lg71$a;)V

    return-void
.end method

.method public setTextSize(IF)V
    .locals 1

    .line 1
    sget-boolean v0, Lu42;->b:Z

    if-eqz v0, :cond_0

    invoke-super {p0, p1, p2}, Landroid/widget/TextView;->setTextSize(IF)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lm7;->mTextHelper:Ll7;

    if-eqz v0, :cond_1

    invoke-virtual {v0, p1, p2}, Ll7;->A(IF)V

    :cond_1
    :goto_0
    return-void
.end method

.method public setTypeface(Landroid/graphics/Typeface;I)V
    .locals 2

    .line 1
    iget-boolean v0, p0, Lm7;->mIsSetTypefaceProcessing:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    if-eqz p1, :cond_1

    if-lez p2, :cond_1

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1, p2}, Lsz1;->a(Landroid/content/Context;Landroid/graphics/Typeface;I)Landroid/graphics/Typeface;

    move-result-object v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    const/4 v1, 0x1

    iput-boolean v1, p0, Lm7;->mIsSetTypefaceProcessing:Z

    if-eqz v0, :cond_2

    move-object p1, v0

    :cond_2
    const/4 v0, 0x0

    :try_start_0
    invoke-super {p0, p1, p2}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iput-boolean v0, p0, Lm7;->mIsSetTypefaceProcessing:Z

    return-void

    :catchall_0
    move-exception p1

    iput-boolean v0, p0, Lm7;->mIsSetTypefaceProcessing:Z

    throw p1
.end method

.class public final Lga$f;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lw01;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lga;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "f"
.end annotation


# static fields
.field public static final a:Lga$f;

.field public static final b:Ln00;

.field public static final c:Ln00;

.field public static final d:Ln00;

.field public static final e:Ln00;

.field public static final f:Ln00;

.field public static final g:Ln00;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .line 1
    new-instance v0, Lga$f;

    invoke-direct {v0}, Lga$f;-><init>()V

    sput-object v0, Lga$f;->a:Lga$f;

    const-string v0, "sessionId"

    invoke-static {v0}, Ln00;->d(Ljava/lang/String;)Ln00;

    move-result-object v0

    sput-object v0, Lga$f;->b:Ln00;

    const-string v0, "firstSessionId"

    invoke-static {v0}, Ln00;->d(Ljava/lang/String;)Ln00;

    move-result-object v0

    sput-object v0, Lga$f;->c:Ln00;

    const-string v0, "sessionIndex"

    invoke-static {v0}, Ln00;->d(Ljava/lang/String;)Ln00;

    move-result-object v0

    sput-object v0, Lga$f;->d:Ln00;

    const-string v0, "eventTimestampUs"

    invoke-static {v0}, Ln00;->d(Ljava/lang/String;)Ln00;

    move-result-object v0

    sput-object v0, Lga$f;->e:Ln00;

    const-string v0, "dataCollectionStatus"

    invoke-static {v0}, Ln00;->d(Ljava/lang/String;)Ln00;

    move-result-object v0

    sput-object v0, Lga$f;->f:Ln00;

    const-string v0, "firebaseInstallationId"

    invoke-static {v0}, Ln00;->d(Ljava/lang/String;)Ln00;

    move-result-object v0

    sput-object v0, Lga$f;->g:Ln00;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lam1;Lx01;)V
    .locals 3

    .line 1
    sget-object v0, Lga$f;->b:Ln00;

    invoke-virtual {p1}, Lam1;->e()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2, v0, v1}, Lx01;->f(Ln00;Ljava/lang/Object;)Lx01;

    sget-object v0, Lga$f;->c:Ln00;

    invoke-virtual {p1}, Lam1;->d()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2, v0, v1}, Lx01;->f(Ln00;Ljava/lang/Object;)Lx01;

    sget-object v0, Lga$f;->d:Ln00;

    invoke-virtual {p1}, Lam1;->f()I

    move-result v1

    invoke-interface {p2, v0, v1}, Lx01;->c(Ln00;I)Lx01;

    sget-object v0, Lga$f;->e:Ln00;

    invoke-virtual {p1}, Lam1;->b()J

    move-result-wide v1

    invoke-interface {p2, v0, v1, v2}, Lx01;->e(Ln00;J)Lx01;

    sget-object v0, Lga$f;->f:Ln00;

    invoke-virtual {p1}, Lam1;->a()Lgp;

    move-result-object v1

    invoke-interface {p2, v0, v1}, Lx01;->f(Ln00;Ljava/lang/Object;)Lx01;

    sget-object v0, Lga$f;->g:Ln00;

    invoke-virtual {p1}, Lam1;->c()Ljava/lang/String;

    move-result-object p1

    invoke-interface {p2, v0, p1}, Lx01;->f(Ln00;Ljava/lang/Object;)Lx01;

    return-void
.end method

.method public bridge synthetic encode(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .line 1
    check-cast p1, Lam1;

    check-cast p2, Lx01;

    invoke-virtual {p0, p1, p2}, Lga$f;->a(Lam1;Lx01;)V

    return-void
.end method

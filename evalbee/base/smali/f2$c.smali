.class public Lf2$c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/widget/AbsListView$MultiChoiceModeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lf2;->y()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Lf2;


# direct methods
.method public constructor <init>(Lf2;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lf2$c;->a:Lf2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onActionItemClicked(Landroid/view/ActionMode;Landroid/view/MenuItem;)Z
    .locals 8

    .line 1
    iget-object v0, p0, Lf2$c;->a:Lf2;

    iget-object v0, v0, Lf2;->g:Ld3;

    invoke-virtual {v0}, Ld3;->a()Landroid/util/SparseBooleanArray;

    move-result-object v0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result p2

    const/4 v2, 0x0

    const/4 v3, 0x1

    packed-switch p2, :pswitch_data_0

    return v2

    :pswitch_0
    invoke-virtual {v0}, Landroid/util/SparseBooleanArray;->size()I

    move-result p2

    sub-int/2addr p2, v3

    :goto_0
    if-ltz p2, :cond_1

    invoke-virtual {v0, p2}, Landroid/util/SparseBooleanArray;->valueAt(I)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lf2$c;->a:Lf2;

    iget-object v2, v2, Lf2;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Landroid/util/SparseBooleanArray;->keyAt(I)I

    move-result v4

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lay;

    invoke-virtual {v2}, Lay;->b()Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 p2, p2, -0x1

    goto :goto_0

    :cond_1
    iget-object p2, p0, Lf2$c;->a:Lf2;

    invoke-static {p2, v1}, Lf2;->p(Lf2;Ljava/util/ArrayList;)V

    invoke-virtual {p1}, Landroid/view/ActionMode;->finish()V

    return v3

    :pswitch_1
    invoke-virtual {v0}, Landroid/util/SparseBooleanArray;->size()I

    move-result p2

    if-ne p2, v3, :cond_2

    invoke-virtual {v0, v2}, Landroid/util/SparseBooleanArray;->valueAt(I)Z

    move-result p2

    if-eqz p2, :cond_2

    iget-object p2, p0, Lf2$c;->a:Lf2;

    iget-object v1, p2, Lf2;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Landroid/util/SparseBooleanArray;->keyAt(I)I

    move-result v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lay;

    invoke-virtual {v0}, Lay;->b()Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    move-result-object v0

    invoke-static {p2, v0}, Lf2;->q(Lf2;Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)V

    :cond_2
    invoke-virtual {p1}, Landroid/view/ActionMode;->finish()V

    return v3

    :pswitch_2
    invoke-virtual {v0}, Landroid/util/SparseBooleanArray;->size()I

    move-result p2

    sub-int/2addr p2, v3

    :goto_1
    if-ltz p2, :cond_5

    invoke-virtual {v0, p2}, Landroid/util/SparseBooleanArray;->valueAt(I)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lf2$c;->a:Lf2;

    iget-object v2, v2, Lf2;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Landroid/util/SparseBooleanArray;->keyAt(I)I

    move-result v4

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lay;

    invoke-virtual {v2}, Lay;->d()I

    move-result v4

    if-lez v4, :cond_3

    invoke-virtual {v2}, Lay;->b()Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_3
    iget-object v4, p0, Lf2$c;->a:Lf2;

    iget-object v4, v4, Lf2;->a:Landroid/content/Context;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lf2$c;->a:Lf2;

    iget-object v6, v6, Lf2;->a:Landroid/content/Context;

    const v7, 0x7f1201bc

    invoke-virtual {v6, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Lay;->b()Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    move-result-object v2

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;->getExamName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const v5, 0x7f0800bd

    const v6, 0x7f08016e

    invoke-static {v4, v2, v5, v6}, La91;->H(Landroid/content/Context;Ljava/lang/String;II)V

    :cond_4
    :goto_2
    add-int/lit8 p2, p2, -0x1

    goto :goto_1

    :cond_5
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result p2

    if-lez p2, :cond_6

    iget-object p2, p0, Lf2$c;->a:Lf2;

    invoke-static {p2, v1}, Lf2;->h(Lf2;Ljava/util/ArrayList;)V

    :cond_6
    invoke-virtual {p1}, Landroid/view/ActionMode;->finish()V

    return v3

    nop

    :pswitch_data_0
    .packed-switch 0x7f0901c2
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onCreateActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z
    .locals 2

    .line 1
    iget-object v0, p0, Lf2$c;->a:Lf2;

    invoke-virtual {v0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/e;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f0e0002

    invoke-virtual {v0, v1, p2}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    iget-object p2, p0, Lf2$c;->a:Lf2;

    iput-object p1, p2, Lf2;->k:Landroid/view/ActionMode;

    iget-object p1, p2, Lf2;->b:Landroid/view/View;

    const p2, 0x7f0901df

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p1

    const/16 p2, 0x8

    invoke-virtual {p1, p2}, Landroid/view/View;->setVisibility(I)V

    const/4 p1, 0x1

    return p1
.end method

.method public onDestroyActionMode(Landroid/view/ActionMode;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lf2$c;->a:Lf2;

    iget-object v0, v0, Lf2;->b:Landroid/view/View;

    const v1, 0x7f0901df

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lf2$c;->a:Lf2;

    iget-object v0, v0, Lf2;->g:Ld3;

    invoke-virtual {v0}, Ld3;->b()V

    invoke-virtual {p1}, Landroid/view/ActionMode;->finish()V

    iget-object p1, p0, Lf2$c;->a:Lf2;

    const/4 v0, 0x0

    iput-object v0, p1, Lf2;->k:Landroid/view/ActionMode;

    return-void
.end method

.method public onItemCheckedStateChanged(Landroid/view/ActionMode;IJZ)V
    .locals 1

    .line 1
    const/4 p3, 0x1

    sub-int/2addr p2, p3

    iget-object p4, p0, Lf2$c;->a:Lf2;

    iget-object p4, p4, Lf2;->e:Landroid/widget/ListView;

    invoke-virtual {p4}, Landroid/widget/AbsListView;->getCheckedItemCount()I

    move-result p4

    new-instance p5, Ljava/lang/StringBuilder;

    invoke-direct {p5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p5, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, " Selected"

    invoke-virtual {p5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p5

    invoke-virtual {p1, p5}, Landroid/view/ActionMode;->setTitle(Ljava/lang/CharSequence;)V

    invoke-virtual {p1}, Landroid/view/ActionMode;->getMenu()Landroid/view/Menu;

    move-result-object p1

    const p5, 0x7f0901c3

    invoke-interface {p1, p5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object p1

    if-ne p4, p3, :cond_0

    goto :goto_0

    :cond_0
    const/4 p3, 0x0

    :goto_0
    invoke-interface {p1, p3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    iget-object p1, p0, Lf2$c;->a:Lf2;

    iget-object p1, p1, Lf2;->g:Ld3;

    invoke-virtual {p1, p2}, Ld3;->d(I)V

    iget-object p1, p0, Lf2$c;->a:Lf2;

    iget-object p1, p1, Lf2;->g:Ld3;

    invoke-virtual {p1}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    return-void
.end method

.method public onPrepareActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z
    .locals 0

    .line 1
    const/4 p1, 0x0

    return p1
.end method

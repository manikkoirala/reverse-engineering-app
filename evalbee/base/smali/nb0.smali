.class public Lnb0;
.super Lu80;
.source "SourceFile"


# instance fields
.field public a:Landroid/content/Context;

.field public b:Ly01;

.field public c:Ljava/util/ArrayList;

.field public d:Landroid/widget/LinearLayout;

.field public e:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ly01;[Lcom/ekodroid/omrevaluator/templateui/models/GradeLevel;)V
    .locals 1

    .line 1
    invoke-direct {p0, p1}, Lu80;-><init>(Landroid/content/Context;)V

    new-instance v0, Lnb0$a;

    invoke-direct {v0, p0}, Lnb0$a;-><init>(Lnb0;)V

    iput-object v0, p0, Lnb0;->e:Landroid/view/View$OnClickListener;

    iput-object p1, p0, Lnb0;->a:Landroid/content/Context;

    iput-object p2, p0, Lnb0;->b:Ly01;

    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lnb0;->c:Ljava/util/ArrayList;

    if-eqz p3, :cond_0

    array-length p2, p3

    if-lez p2, :cond_0

    invoke-static {p3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    :cond_0
    return-void
.end method

.method public static synthetic a(Lnb0;Ljava/util/ArrayList;Landroid/widget/LinearLayout;)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1, p2}, Lnb0;->b(Ljava/util/ArrayList;Landroid/widget/LinearLayout;)V

    return-void
.end method


# virtual methods
.method public final b(Ljava/util/ArrayList;Landroid/widget/LinearLayout;)V
    .locals 7

    .line 1
    if-eqz p1, :cond_0

    invoke-virtual {p2}, Landroid/view/ViewGroup;->removeAllViews()V

    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, -0x2

    invoke-direct {v0, v1, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    const/16 v1, 0x10

    iget-object v2, p0, Lnb0;->a:Landroid/content/Context;

    invoke-static {v1, v2}, La91;->d(ILandroid/content/Context;)I

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, v2, v2}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    const/high16 v1, 0x3f800000    # 1.0f

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    iget-object v3, p0, Lnb0;->a:Landroid/content/Context;

    const/16 v4, 0x20

    invoke-static {v4, v3}, La91;->d(ILandroid/content/Context;)I

    move-result v3

    iget-object v5, p0, Lnb0;->a:Landroid/content/Context;

    invoke-static {v4, v5}, La91;->d(ILandroid/content/Context;)I

    move-result v4

    invoke-direct {v1, v3, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    const/16 v3, 0xc

    iget-object v4, p0, Lnb0;->a:Landroid/content/Context;

    invoke-static {v3, v4}, La91;->d(ILandroid/content/Context;)I

    move-result v3

    invoke-virtual {v1, v2, v2, v3, v2}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    :goto_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v2, v3, :cond_0

    iget-object v3, p0, Lnb0;->a:Landroid/content/Context;

    invoke-static {v3}, Lyo;->b(Landroid/content/Context;)Landroid/widget/LinearLayout;

    move-result-object v3

    iget-object v4, p0, Lnb0;->a:Landroid/content/Context;

    const v5, 0x7f080080

    invoke-static {v4, v5}, Lg7;->b(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    iget-object v4, p0, Lnb0;->a:Landroid/content/Context;

    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/ekodroid/omrevaluator/templateui/models/GradeLevel;

    invoke-virtual {v5}, Lcom/ekodroid/omrevaluator/templateui/models/GradeLevel;->getStringLabel()Ljava/lang/String;

    move-result-object v5

    const/16 v6, 0x96

    invoke-static {v4, v5, v6}, Lyo;->c(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/TextView;

    move-result-object v4

    invoke-virtual {v3, v4, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v4, Lpo;

    iget-object v5, p0, Lnb0;->a:Landroid/content/Context;

    invoke-direct {v4, v5}, Lpo;-><init>(Landroid/content/Context;)V

    invoke-virtual {v4, v2}, Lpo;->setIndex(I)V

    iget-object v5, p0, Lnb0;->a:Landroid/content/Context;

    const v6, 0x7f08008e

    invoke-static {v5, v6}, Lg7;->b(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    iget-object v5, p0, Lnb0;->e:Landroid/view/View$OnClickListener;

    invoke-virtual {v4, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v3, v4, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {p2, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final c()V
    .locals 2

    .line 1
    const v0, 0x7f090442

    invoke-virtual {p0, v0}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/appcompat/widget/Toolbar;

    new-instance v1, Lnb0$e;

    invoke-direct {v1, p0}, Lnb0$e;-><init>(Lnb0;)V

    invoke-virtual {v0, v1}, Landroidx/appcompat/widget/Toolbar;->setNavigationOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    .line 1
    invoke-super {p0, p1}, Lu80;->onCreate(Landroid/os/Bundle;)V

    const p1, 0x7f0c006c

    invoke-virtual {p0, p1}, Landroid/app/Dialog;->setContentView(I)V

    const p1, 0x7f1202dd

    invoke-virtual {p0, p1}, Landroid/app/Dialog;->setTitle(I)V

    invoke-virtual {p0}, Lnb0;->c()V

    const p1, 0x7f09022f

    invoke-virtual {p0, p1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/LinearLayout;

    iput-object p1, p0, Lnb0;->d:Landroid/widget/LinearLayout;

    const p1, 0x7f090144

    invoke-virtual {p0, p1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/EditText;

    const v0, 0x7f090149

    invoke-virtual {p0, v0}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    const v1, 0x7f090147

    invoke-virtual {p0, v1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iget-object v2, p0, Lnb0;->c:Ljava/util/ArrayList;

    iget-object v3, p0, Lnb0;->d:Landroid/widget/LinearLayout;

    invoke-virtual {p0, v2, v3}, Lnb0;->b(Ljava/util/ArrayList;Landroid/widget/LinearLayout;)V

    const v2, 0x7f090096

    invoke-virtual {p0, v2}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v2

    new-instance v3, Lnb0$b;

    invoke-direct {v3, p0, p1, v0, v1}, Lnb0$b;-><init>(Lnb0;Landroid/widget/EditText;Landroid/widget/EditText;Landroid/widget/EditText;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const p1, 0x7f090085

    invoke-virtual {p0, p1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object p1

    new-instance v0, Lnb0$c;

    invoke-direct {v0, p0}, Lnb0$c;-><init>(Lnb0;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const p1, 0x7f090091

    invoke-virtual {p0, p1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object p1

    new-instance v0, Lnb0$d;

    invoke-direct {v0, p0}, Lnb0$d;-><init>(Lnb0;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

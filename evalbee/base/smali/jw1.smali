.class public final Ljw1;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Lag1;

.field public final b:La92;

.field public final c:J

.field public final d:Ljava/lang/Object;

.field public final e:Ljava/util/Map;


# direct methods
.method public constructor <init>(Lag1;La92;)V
    .locals 8

    .line 1
    const-string v0, "runnableScheduler"

    invoke-static {p1, v0}, Lfg0;->e(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "launcher"

    invoke-static {p2, v0}, Lfg0;->e(Ljava/lang/Object;Ljava/lang/String;)V

    const-wide/16 v4, 0x0

    const/4 v6, 0x4

    const/4 v7, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    invoke-direct/range {v1 .. v7}, Ljw1;-><init>(Lag1;La92;JILgq;)V

    return-void
.end method

.method public constructor <init>(Lag1;La92;J)V
    .locals 1

    .line 2
    const-string v0, "runnableScheduler"

    invoke-static {p1, v0}, Lfg0;->e(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "launcher"

    invoke-static {p2, v0}, Lfg0;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Ljw1;->a:Lag1;

    iput-object p2, p0, Ljw1;->b:La92;

    iput-wide p3, p0, Ljw1;->c:J

    new-instance p1, Ljava/lang/Object;

    invoke-direct {p1}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Ljw1;->d:Ljava/lang/Object;

    new-instance p1, Ljava/util/LinkedHashMap;

    invoke-direct {p1}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object p1, p0, Ljw1;->e:Ljava/util/Map;

    return-void
.end method

.method public synthetic constructor <init>(Lag1;La92;JILgq;)V
    .locals 0

    .line 3
    and-int/lit8 p5, p5, 0x4

    if-eqz p5, :cond_0

    sget-object p3, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 p4, 0x5a

    invoke-virtual {p3, p4, p5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide p3

    :cond_0
    invoke-direct {p0, p1, p2, p3, p4}, Ljw1;-><init>(Lag1;La92;J)V

    return-void
.end method

.method public static synthetic a(Ljw1;Lop1;)V
    .locals 0

    .line 1
    invoke-static {p0, p1}, Ljw1;->d(Ljw1;Lop1;)V

    return-void
.end method

.method public static final d(Ljw1;Lop1;)V
    .locals 1

    .line 1
    const-string v0, "this$0"

    invoke-static {p0, v0}, Lfg0;->e(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "$token"

    invoke-static {p1, v0}, Lfg0;->e(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object p0, p0, Ljw1;->b:La92;

    const/4 v0, 0x3

    invoke-interface {p0, p1, v0}, La92;->d(Lop1;I)V

    return-void
.end method


# virtual methods
.method public final b(Lop1;)V
    .locals 2

    .line 1
    const-string v0, "token"

    invoke-static {p1, v0}, Lfg0;->e(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, p0, Ljw1;->d:Ljava/lang/Object;

    monitor-enter v0

    :try_start_0
    iget-object v1, p0, Ljw1;->e:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Runnable;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    if-eqz p1, :cond_0

    iget-object v0, p0, Ljw1;->a:Lag1;

    invoke-interface {v0, p1}, Lag1;->a(Ljava/lang/Runnable;)V

    :cond_0
    return-void

    :catchall_0
    move-exception p1

    monitor-exit v0

    throw p1
.end method

.method public final c(Lop1;)V
    .locals 3

    .line 1
    const-string v0, "token"

    invoke-static {p1, v0}, Lfg0;->e(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Liw1;

    invoke-direct {v0, p0, p1}, Liw1;-><init>(Ljw1;Lop1;)V

    iget-object v1, p0, Ljw1;->d:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v2, p0, Ljw1;->e:Ljava/util/Map;

    invoke-interface {v2, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Runnable;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    iget-object p1, p0, Ljw1;->a:Lag1;

    iget-wide v1, p0, Ljw1;->c:J

    invoke-interface {p1, v1, v2, v0}, Lag1;->b(JLjava/lang/Runnable;)V

    return-void

    :catchall_0
    move-exception p1

    monitor-exit v1

    throw p1
.end method

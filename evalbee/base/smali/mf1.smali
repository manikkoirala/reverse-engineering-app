.class public Lmf1;
.super Lu80;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lmf1$d;
    }
.end annotation


# instance fields
.field public a:Landroid/content/Context;

.field public b:Ljava/lang/String;

.field public c:Lmf1$d;

.field public d:[Ljava/lang/String;

.field public e:[Landroid/widget/EditText;

.field public f:I

.field public g:I

.field public h:Landroid/widget/Spinner;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lmf1$d;[Ljava/lang/String;II)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lu80;-><init>(Landroid/content/Context;)V

    iput-object p1, p0, Lmf1;->a:Landroid/content/Context;

    iput-object p2, p0, Lmf1;->b:Ljava/lang/String;

    iput-object p3, p0, Lmf1;->c:Lmf1$d;

    iput-object p4, p0, Lmf1;->d:[Ljava/lang/String;

    iput p6, p0, Lmf1;->f:I

    iput p5, p0, Lmf1;->g:I

    return-void
.end method

.method public static synthetic a(Lmf1;)Landroid/widget/Spinner;
    .locals 0

    .line 1
    iget-object p0, p0, Lmf1;->h:Landroid/widget/Spinner;

    return-object p0
.end method


# virtual methods
.method public final b()V
    .locals 2

    .line 1
    const v0, 0x7f09044c

    invoke-virtual {p0, v0}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/appcompat/widget/Toolbar;

    iget-object v1, p0, Lmf1;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroidx/appcompat/widget/Toolbar;->setTitle(Ljava/lang/CharSequence;)V

    new-instance v1, Lmf1$a;

    invoke-direct {v1, p0}, Lmf1$a;-><init>(Lmf1;)V

    invoke-virtual {v0, v1}, Landroidx/appcompat/widget/Toolbar;->setNavigationOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 10

    .line 1
    invoke-super {p0, p1}, Lu80;->onCreate(Landroid/os/Bundle;)V

    const p1, 0x7f0c0079

    invoke-virtual {p0, p1}, Landroid/app/Dialog;->setContentView(I)V

    invoke-virtual {p0}, Lmf1;->b()V

    const p1, 0x7f090344

    invoke-virtual {p0, p1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/Spinner;

    iput-object p1, p0, Lmf1;->h:Landroid/widget/Spinner;

    const-string p1, "0, 1...8, 9"

    const-string v0, "1, 2...9, 0"

    filled-new-array {p1, v0}, [Ljava/lang/String;

    move-result-object p1

    new-instance v0, Landroid/widget/ArrayAdapter;

    iget-object v1, p0, Lmf1;->a:Landroid/content/Context;

    const v2, 0x7f0c00f0

    invoke-direct {v0, v1, v2, p1}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    iget-object p1, p0, Lmf1;->h:Landroid/widget/Spinner;

    invoke-virtual {p1, v0}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    iget-object p1, p0, Lmf1;->h:Landroid/widget/Spinner;

    iget v0, p0, Lmf1;->g:I

    invoke-virtual {p1, v0}, Landroid/widget/AdapterView;->setSelection(I)V

    const p1, 0x7f09021a

    invoke-virtual {p0, p1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/LinearLayout;

    iget-object v0, p0, Lmf1;->d:[Ljava/lang/String;

    array-length v0, v0

    new-array v0, v0, [Landroid/widget/EditText;

    iput-object v0, p0, Lmf1;->e:[Landroid/widget/EditText;

    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, -0x2

    invoke-direct {v0, v1, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    const/16 v2, 0x10

    iget-object v3, p0, Lmf1;->a:Landroid/content/Context;

    invoke-static {v2, v3}, La91;->d(ILandroid/content/Context;)I

    move-result v2

    const/4 v3, 0x0

    invoke-virtual {v0, v3, v3, v2, v3}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    const/16 v4, 0x28

    iget-object v5, p0, Lmf1;->a:Landroid/content/Context;

    invoke-static {v4, v5}, La91;->d(ILandroid/content/Context;)I

    move-result v4

    invoke-direct {v2, v1, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    :goto_0
    iget-object v1, p0, Lmf1;->e:[Landroid/widget/EditText;

    array-length v1, v1

    if-ge v3, v1, :cond_0

    iget-object v1, p0, Lmf1;->a:Landroid/content/Context;

    invoke-static {v1}, Lyo;->b(Landroid/content/Context;)Landroid/widget/LinearLayout;

    move-result-object v1

    iget-object v4, p0, Lmf1;->a:Landroid/content/Context;

    iget-object v5, p0, Lmf1;->d:[Ljava/lang/String;

    aget-object v5, v5, v3

    const/16 v6, 0x32

    invoke-static {v4, v5, v6}, Lyo;->c(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/TextView;

    move-result-object v4

    iget-object v5, p0, Lmf1;->e:[Landroid/widget/EditText;

    iget-object v6, p0, Lmf1;->a:Landroid/content/Context;

    iget-object v7, p0, Lmf1;->d:[Ljava/lang/String;

    aget-object v7, v7, v3

    iget v8, p0, Lmf1;->f:I

    const/16 v9, 0x50

    invoke-static {v6, v7, v8, v9}, Lyo;->a(Landroid/content/Context;Ljava/lang/String;II)Landroid/widget/EditText;

    move-result-object v6

    aput-object v6, v5, v3

    invoke-virtual {v1, v4, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v4, p0, Lmf1;->e:[Landroid/widget/EditText;

    aget-object v4, v4, v3

    invoke-virtual {v1, v4, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_0
    const p1, 0x7f0900b2

    invoke-virtual {p0, p1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object p1

    new-instance v0, Lmf1$b;

    invoke-direct {v0, p0}, Lmf1$b;-><init>(Lmf1;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const p1, 0x7f0900d4

    invoke-virtual {p0, p1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object p1

    new-instance v0, Lmf1$c;

    invoke-direct {v0, p0}, Lmf1$c;-><init>(Lmf1;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

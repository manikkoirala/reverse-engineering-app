.class public Lk92$a;
.super Lix;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lk92;-><init>(Landroidx/room/RoomDatabase;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic d:Lk92;


# direct methods
.method public constructor <init>(Lk92;Landroidx/room/RoomDatabase;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lk92$a;->d:Lk92;

    invoke-direct {p0, p2}, Lix;-><init>(Landroidx/room/RoomDatabase;)V

    return-void
.end method


# virtual methods
.method public e()Ljava/lang/String;
    .locals 1

    .line 1
    const-string v0, "INSERT OR REPLACE INTO `WorkProgress` (`work_spec_id`,`progress`) VALUES (?,?)"

    return-object v0
.end method

.method public bridge synthetic i(Lws1;Ljava/lang/Object;)V
    .locals 0

    .line 1
    check-cast p2, Li92;

    invoke-virtual {p0, p1, p2}, Lk92$a;->k(Lws1;Li92;)V

    return-void
.end method

.method public k(Lws1;Li92;)V
    .locals 2

    .line 1
    invoke-virtual {p2}, Li92;->b()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    if-nez v0, :cond_0

    invoke-interface {p1, v1}, Lus1;->I(I)V

    goto :goto_0

    :cond_0
    invoke-virtual {p2}, Li92;->b()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v1, v0}, Lus1;->y(ILjava/lang/String;)V

    :goto_0
    invoke-virtual {p2}, Li92;->a()Landroidx/work/b;

    move-result-object p2

    invoke-static {p2}, Landroidx/work/b;->k(Landroidx/work/b;)[B

    move-result-object p2

    const/4 v0, 0x2

    if-nez p2, :cond_1

    invoke-interface {p1, v0}, Lus1;->I(I)V

    goto :goto_1

    :cond_1
    invoke-interface {p1, v0, p2}, Lus1;->D(I[B)V

    :goto_1
    return-void
.end method

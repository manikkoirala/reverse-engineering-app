.class public final Lkh;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Liz1;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lkh$a;
    }
.end annotation


# instance fields
.field public final a:Lbl;


# direct methods
.method public constructor <init>(Lbl;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lkh;->a:Lbl;

    return-void
.end method


# virtual methods
.method public a(Lgc0;Lcom/google/gson/reflect/TypeToken;)Lhz1;
    .locals 3

    .line 1
    invoke-virtual {p2}, Lcom/google/gson/reflect/TypeToken;->getType()Ljava/lang/reflect/Type;

    move-result-object v0

    invoke-virtual {p2}, Lcom/google/gson/reflect/TypeToken;->getRawType()Ljava/lang/Class;

    move-result-object v1

    const-class v2, Ljava/util/Collection;

    invoke-virtual {v2, v1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v2

    if-nez v2, :cond_0

    const/4 p1, 0x0

    return-object p1

    :cond_0
    invoke-static {v0, v1}, Lcom/google/gson/internal/$Gson$Types;->h(Ljava/lang/reflect/Type;Ljava/lang/Class;)Ljava/lang/reflect/Type;

    move-result-object v0

    invoke-static {v0}, Lcom/google/gson/reflect/TypeToken;->get(Ljava/lang/reflect/Type;)Lcom/google/gson/reflect/TypeToken;

    move-result-object v1

    invoke-virtual {p1, v1}, Lgc0;->l(Lcom/google/gson/reflect/TypeToken;)Lhz1;

    move-result-object v1

    iget-object v2, p0, Lkh;->a:Lbl;

    invoke-virtual {v2, p2}, Lbl;->b(Lcom/google/gson/reflect/TypeToken;)Lu01;

    move-result-object p2

    new-instance v2, Lkh$a;

    invoke-direct {v2, p1, v0, v1, p2}, Lkh$a;-><init>(Lgc0;Ljava/lang/reflect/Type;Lhz1;Lu01;)V

    return-object v2
.end method

.class public Lea1;
.super Lu80;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lea1$d;
    }
.end annotation


# instance fields
.field public a:Landroid/content/Context;

.field public b:Ljava/lang/String;

.field public c:Ly01;

.field public d:[Ljava/lang/String;

.field public e:[Landroid/widget/EditText;

.field public f:I

.field public g:[Landroid/widget/LinearLayout;

.field public h:Landroid/widget/LinearLayout;

.field public i:Landroid/widget/ProgressBar;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ly01;[Ljava/lang/String;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lu80;-><init>(Landroid/content/Context;)V

    iput-object p1, p0, Lea1;->a:Landroid/content/Context;

    iput-object p2, p0, Lea1;->b:Ljava/lang/String;

    iput-object p3, p0, Lea1;->c:Ly01;

    iput-object p4, p0, Lea1;->d:[Ljava/lang/String;

    iput p5, p0, Lea1;->f:I

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .line 1
    const v0, 0x7f09044a

    invoke-virtual {p0, v0}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/appcompat/widget/Toolbar;

    iget-object v1, p0, Lea1;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroidx/appcompat/widget/Toolbar;->setTitle(Ljava/lang/CharSequence;)V

    new-instance v1, Lea1$a;

    invoke-direct {v1, p0}, Lea1$a;-><init>(Lea1;)V

    invoke-virtual {v0, v1}, Landroidx/appcompat/widget/Toolbar;->setNavigationOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    .line 1
    invoke-super {p0, p1}, Lu80;->onCreate(Landroid/os/Bundle;)V

    const p1, 0x7f0c0075

    invoke-virtual {p0, p1}, Landroid/app/Dialog;->setContentView(I)V

    invoke-virtual {p0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object p1

    const/4 v0, -0x1

    invoke-virtual {p1, v0, v0}, Landroid/view/Window;->setLayout(II)V

    invoke-virtual {p0}, Lea1;->a()V

    const p1, 0x7f090209

    invoke-virtual {p0, p1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/LinearLayout;

    iput-object p1, p0, Lea1;->h:Landroid/widget/LinearLayout;

    const p1, 0x7f0902cc

    invoke-virtual {p0, p1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/ProgressBar;

    iput-object p1, p0, Lea1;->i:Landroid/widget/ProgressBar;

    new-instance p1, Lea1$d;

    const/4 v0, 0x0

    invoke-direct {p1, p0, v0}, Lea1$d;-><init>(Lea1;Lea1$a;)V

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Void;

    invoke-virtual {p1, v0}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    const p1, 0x7f0900b1

    invoke-virtual {p0, p1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object p1

    new-instance v0, Lea1$b;

    invoke-direct {v0, p0}, Lea1$b;-><init>(Lea1;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const p1, 0x7f0900d3

    invoke-virtual {p0, p1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object p1

    new-instance v0, Lea1$c;

    invoke-direct {v0, p0}, Lea1$c;-><init>(Lea1;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

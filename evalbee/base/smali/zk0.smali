.class public final Lzk0;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Lcom/google/firebase/firestore/remote/f;


# direct methods
.method public constructor <init>(Lcom/google/firebase/firestore/remote/f;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lzk0;->a:Lcom/google/firebase/firestore/remote/f;

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/firestore/v1/e;Z)Lcom/google/firebase/firestore/model/MutableDocument;
    .locals 3

    .line 1
    iget-object v0, p0, Lzk0;->a:Lcom/google/firebase/firestore/remote/f;

    invoke-virtual {p1}, Lcom/google/firestore/v1/e;->g0()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/firebase/firestore/remote/f;->k(Ljava/lang/String;)Ldu;

    move-result-object v0

    iget-object v1, p0, Lzk0;->a:Lcom/google/firebase/firestore/remote/f;

    invoke-virtual {p1}, Lcom/google/firestore/v1/e;->h0()Lcom/google/protobuf/j0;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/firebase/firestore/remote/f;->v(Lcom/google/protobuf/j0;)Lqo1;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/firestore/v1/e;->e0()Ljava/util/Map;

    move-result-object p1

    invoke-static {p1}, La11;->i(Ljava/util/Map;)La11;

    move-result-object p1

    invoke-static {v0, v1, p1}, Lcom/google/firebase/firestore/model/MutableDocument;->p(Ldu;Lqo1;La11;)Lcom/google/firebase/firestore/model/MutableDocument;

    move-result-object p1

    if-eqz p2, :cond_0

    invoke-virtual {p1}, Lcom/google/firebase/firestore/model/MutableDocument;->t()Lcom/google/firebase/firestore/model/MutableDocument;

    move-result-object p1

    :cond_0
    return-object p1
.end method

.method public b(Lcom/google/firestore/admin/v1/Index;)Ljava/util/List;
    .locals 5

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p1}, Lcom/google/firestore/admin/v1/Index;->e0()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/firestore/admin/v1/Index$IndexField;

    invoke-virtual {v1}, Lcom/google/firestore/admin/v1/Index$IndexField;->d0()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ls00;->q(Ljava/lang/String;)Ls00;

    move-result-object v2

    invoke-virtual {v1}, Lcom/google/firestore/admin/v1/Index$IndexField;->f0()Lcom/google/firestore/admin/v1/Index$IndexField$ValueModeCase;

    move-result-object v3

    sget-object v4, Lcom/google/firestore/admin/v1/Index$IndexField$ValueModeCase;->ARRAY_CONFIG:Lcom/google/firestore/admin/v1/Index$IndexField$ValueModeCase;

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    sget-object v1, Lcom/google/firebase/firestore/model/FieldIndex$Segment$Kind;->CONTAINS:Lcom/google/firebase/firestore/model/FieldIndex$Segment$Kind;

    goto :goto_1

    :cond_0
    invoke-virtual {v1}, Lcom/google/firestore/admin/v1/Index$IndexField;->e0()Lcom/google/firestore/admin/v1/Index$IndexField$Order;

    move-result-object v1

    sget-object v3, Lcom/google/firestore/admin/v1/Index$IndexField$Order;->ASCENDING:Lcom/google/firestore/admin/v1/Index$IndexField$Order;

    invoke-virtual {v1, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    sget-object v1, Lcom/google/firebase/firestore/model/FieldIndex$Segment$Kind;->ASCENDING:Lcom/google/firebase/firestore/model/FieldIndex$Segment$Kind;

    goto :goto_1

    :cond_1
    sget-object v1, Lcom/google/firebase/firestore/model/FieldIndex$Segment$Kind;->DESCENDING:Lcom/google/firebase/firestore/model/FieldIndex$Segment$Kind;

    :goto_1
    invoke-static {v2, v1}, Lcom/google/firebase/firestore/model/FieldIndex$Segment;->c(Ls00;Lcom/google/firebase/firestore/model/FieldIndex$Segment$Kind;)Lcom/google/firebase/firestore/model/FieldIndex$Segment;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    return-object v0
.end method

.method public c(Lcom/google/firebase/firestore/proto/MaybeDocument;)Lcom/google/firebase/firestore/model/MutableDocument;
    .locals 2

    .line 1
    sget-object v0, Lzk0$a;->a:[I

    invoke-virtual {p1}, Lcom/google/firebase/firestore/proto/MaybeDocument;->f0()Lcom/google/firebase/firestore/proto/MaybeDocument$DocumentTypeCase;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    invoke-virtual {p1}, Lcom/google/firebase/firestore/proto/MaybeDocument;->i0()Lcom/google/firebase/firestore/proto/b;

    move-result-object p1

    invoke-virtual {p0, p1}, Lzk0;->h(Lcom/google/firebase/firestore/proto/b;)Lcom/google/firebase/firestore/model/MutableDocument;

    move-result-object p1

    return-object p1

    :cond_0
    const-string v0, "Unknown MaybeDocument %s"

    filled-new-array {p1}, [Ljava/lang/Object;

    move-result-object p1

    invoke-static {v0, p1}, Lg9;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/AssertionError;

    move-result-object p1

    throw p1

    :cond_1
    invoke-virtual {p1}, Lcom/google/firebase/firestore/proto/MaybeDocument;->h0()Lcom/google/firebase/firestore/proto/a;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/firebase/firestore/proto/MaybeDocument;->g0()Z

    move-result p1

    invoke-virtual {p0, v0, p1}, Lzk0;->f(Lcom/google/firebase/firestore/proto/a;Z)Lcom/google/firebase/firestore/model/MutableDocument;

    move-result-object p1

    return-object p1

    :cond_2
    invoke-virtual {p1}, Lcom/google/firebase/firestore/proto/MaybeDocument;->e0()Lcom/google/firestore/v1/e;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/firebase/firestore/proto/MaybeDocument;->g0()Z

    move-result p1

    invoke-virtual {p0, v0, p1}, Lzk0;->a(Lcom/google/firestore/v1/e;Z)Lcom/google/firebase/firestore/model/MutableDocument;

    move-result-object p1

    return-object p1
.end method

.method public d(Lcom/google/firestore/v1/Write;)Lwx0;
    .locals 1

    .line 1
    iget-object v0, p0, Lzk0;->a:Lcom/google/firebase/firestore/remote/f;

    invoke-virtual {v0, p1}, Lcom/google/firebase/firestore/remote/f;->l(Lcom/google/firestore/v1/Write;)Lwx0;

    move-result-object p1

    return-object p1
.end method

.method public e(Lea2;)Lxx0;
    .locals 11

    .line 1
    invoke-virtual {p1}, Lea2;->k0()I

    move-result v0

    iget-object v1, p0, Lzk0;->a:Lcom/google/firebase/firestore/remote/f;

    invoke-virtual {p1}, Lea2;->l0()Lcom/google/protobuf/j0;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/firebase/firestore/remote/f;->t(Lcom/google/protobuf/j0;)Lpw1;

    move-result-object v1

    invoke-virtual {p1}, Lea2;->j0()I

    move-result v2

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3, v2}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v4, 0x0

    move v5, v4

    :goto_0
    if-ge v5, v2, :cond_0

    iget-object v6, p0, Lzk0;->a:Lcom/google/firebase/firestore/remote/f;

    invoke-virtual {p1, v5}, Lea2;->i0(I)Lcom/google/firestore/v1/Write;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/google/firebase/firestore/remote/f;->l(Lcom/google/firestore/v1/Write;)Lwx0;

    move-result-object v6

    invoke-interface {v3, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    :cond_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-virtual {p1}, Lea2;->n0()I

    move-result v5

    invoke-direct {v2, v5}, Ljava/util/ArrayList;-><init>(I)V

    move v5, v4

    :goto_1
    invoke-virtual {p1}, Lea2;->n0()I

    move-result v6

    if-ge v5, v6, :cond_4

    invoke-virtual {p1, v5}, Lea2;->m0(I)Lcom/google/firestore/v1/Write;

    move-result-object v6

    add-int/lit8 v7, v5, 0x1

    invoke-virtual {p1}, Lea2;->n0()I

    move-result v8

    const/4 v9, 0x1

    if-ge v7, v8, :cond_1

    invoke-virtual {p1, v7}, Lea2;->m0(I)Lcom/google/firestore/v1/Write;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/firestore/v1/Write;->r0()Z

    move-result v8

    if-eqz v8, :cond_1

    move v8, v9

    goto :goto_2

    :cond_1
    move v8, v4

    :goto_2
    if-eqz v8, :cond_3

    invoke-virtual {p1, v5}, Lea2;->m0(I)Lcom/google/firestore/v1/Write;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/firestore/v1/Write;->s0()Z

    move-result v5

    const-string v8, "TransformMutation should be preceded by a patch or set mutation"

    new-array v10, v4, [Ljava/lang/Object;

    invoke-static {v5, v8, v10}, Lg9;->d(ZLjava/lang/String;[Ljava/lang/Object;)V

    invoke-static {v6}, Lcom/google/firestore/v1/Write;->v0(Lcom/google/firestore/v1/Write;)Lcom/google/firestore/v1/Write$b;

    move-result-object v5

    invoke-virtual {p1, v7}, Lea2;->m0(I)Lcom/google/firestore/v1/Write;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/firestore/v1/Write;->l0()Lcom/google/firestore/v1/DocumentTransform;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/firestore/v1/DocumentTransform;->b0()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_3
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/google/firestore/v1/DocumentTransform$FieldTransform;

    invoke-virtual {v5, v8}, Lcom/google/firestore/v1/Write$b;->A(Lcom/google/firestore/v1/DocumentTransform$FieldTransform;)Lcom/google/firestore/v1/Write$b;

    goto :goto_3

    :cond_2
    iget-object v6, p0, Lzk0;->a:Lcom/google/firebase/firestore/remote/f;

    invoke-virtual {v5}, Lcom/google/protobuf/GeneratedMessageLite$a;->p()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v5

    check-cast v5, Lcom/google/firestore/v1/Write;

    invoke-virtual {v6, v5}, Lcom/google/firebase/firestore/remote/f;->l(Lcom/google/firestore/v1/Write;)Lwx0;

    move-result-object v5

    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v5, v7

    goto :goto_4

    :cond_3
    iget-object v7, p0, Lzk0;->a:Lcom/google/firebase/firestore/remote/f;

    invoke-virtual {v7, v6}, Lcom/google/firebase/firestore/remote/f;->l(Lcom/google/firestore/v1/Write;)Lwx0;

    move-result-object v6

    invoke-interface {v2, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_4
    add-int/2addr v5, v9

    goto :goto_1

    :cond_4
    new-instance p1, Lxx0;

    invoke-direct {p1, v0, v1, v3, v2}, Lxx0;-><init>(ILpw1;Ljava/util/List;Ljava/util/List;)V

    return-object p1
.end method

.method public final f(Lcom/google/firebase/firestore/proto/a;Z)Lcom/google/firebase/firestore/model/MutableDocument;
    .locals 2

    .line 1
    iget-object v0, p0, Lzk0;->a:Lcom/google/firebase/firestore/remote/f;

    invoke-virtual {p1}, Lcom/google/firebase/firestore/proto/a;->d0()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/firebase/firestore/remote/f;->k(Ljava/lang/String;)Ldu;

    move-result-object v0

    iget-object v1, p0, Lzk0;->a:Lcom/google/firebase/firestore/remote/f;

    invoke-virtual {p1}, Lcom/google/firebase/firestore/proto/a;->e0()Lcom/google/protobuf/j0;

    move-result-object p1

    invoke-virtual {v1, p1}, Lcom/google/firebase/firestore/remote/f;->v(Lcom/google/protobuf/j0;)Lqo1;

    move-result-object p1

    invoke-static {v0, p1}, Lcom/google/firebase/firestore/model/MutableDocument;->r(Ldu;Lqo1;)Lcom/google/firebase/firestore/model/MutableDocument;

    move-result-object p1

    if-eqz p2, :cond_0

    invoke-virtual {p1}, Lcom/google/firebase/firestore/model/MutableDocument;->t()Lcom/google/firebase/firestore/model/MutableDocument;

    move-result-object p1

    :cond_0
    return-object p1
.end method

.method public g(Lcom/google/firebase/firestore/proto/Target;)Lau1;
    .locals 10

    .line 1
    invoke-virtual {p1}, Lcom/google/firebase/firestore/proto/Target;->p0()I

    move-result v2

    iget-object v0, p0, Lzk0;->a:Lcom/google/firebase/firestore/remote/f;

    invoke-virtual {p1}, Lcom/google/firebase/firestore/proto/Target;->o0()Lcom/google/protobuf/j0;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/firebase/firestore/remote/f;->v(Lcom/google/protobuf/j0;)Lqo1;

    move-result-object v6

    iget-object v0, p0, Lzk0;->a:Lcom/google/firebase/firestore/remote/f;

    invoke-virtual {p1}, Lcom/google/firebase/firestore/proto/Target;->k0()Lcom/google/protobuf/j0;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/firebase/firestore/remote/f;->v(Lcom/google/protobuf/j0;)Lqo1;

    move-result-object v7

    invoke-virtual {p1}, Lcom/google/firebase/firestore/proto/Target;->n0()Lcom/google/protobuf/ByteString;

    move-result-object v8

    invoke-virtual {p1}, Lcom/google/firebase/firestore/proto/Target;->l0()J

    move-result-wide v3

    sget-object v0, Lzk0$a;->b:[I

    invoke-virtual {p1}, Lcom/google/firebase/firestore/proto/Target;->q0()Lcom/google/firebase/firestore/proto/Target$TargetTypeCase;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lzk0;->a:Lcom/google/firebase/firestore/remote/f;

    invoke-virtual {p1}, Lcom/google/firebase/firestore/proto/Target;->m0()Lcom/google/firestore/v1/Target$QueryTarget;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/google/firebase/firestore/remote/f;->q(Lcom/google/firestore/v1/Target$QueryTarget;)Lcom/google/firebase/firestore/core/q;

    move-result-object p1

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Lcom/google/firebase/firestore/proto/Target;->q0()Lcom/google/firebase/firestore/proto/Target$TargetTypeCase;

    move-result-object p1

    filled-new-array {p1}, [Ljava/lang/Object;

    move-result-object p1

    const-string v0, "Unknown targetType %d"

    invoke-static {v0, p1}, Lg9;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/AssertionError;

    move-result-object p1

    throw p1

    :cond_1
    iget-object v0, p0, Lzk0;->a:Lcom/google/firebase/firestore/remote/f;

    invoke-virtual {p1}, Lcom/google/firebase/firestore/proto/Target;->j0()Lcom/google/firestore/v1/Target$c;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/google/firebase/firestore/remote/f;->e(Lcom/google/firestore/v1/Target$c;)Lcom/google/firebase/firestore/core/q;

    move-result-object p1

    :goto_0
    move-object v1, p1

    new-instance p1, Lau1;

    sget-object v5, Lcom/google/firebase/firestore/local/QueryPurpose;->LISTEN:Lcom/google/firebase/firestore/local/QueryPurpose;

    const/4 v9, 0x0

    move-object v0, p1

    invoke-direct/range {v0 .. v9}, Lau1;-><init>(Lcom/google/firebase/firestore/core/q;IJLcom/google/firebase/firestore/local/QueryPurpose;Lqo1;Lqo1;Lcom/google/protobuf/ByteString;Ljava/lang/Integer;)V

    return-object p1
.end method

.method public final h(Lcom/google/firebase/firestore/proto/b;)Lcom/google/firebase/firestore/model/MutableDocument;
    .locals 2

    .line 1
    iget-object v0, p0, Lzk0;->a:Lcom/google/firebase/firestore/remote/f;

    invoke-virtual {p1}, Lcom/google/firebase/firestore/proto/b;->d0()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/firebase/firestore/remote/f;->k(Ljava/lang/String;)Ldu;

    move-result-object v0

    iget-object v1, p0, Lzk0;->a:Lcom/google/firebase/firestore/remote/f;

    invoke-virtual {p1}, Lcom/google/firebase/firestore/proto/b;->e0()Lcom/google/protobuf/j0;

    move-result-object p1

    invoke-virtual {v1, p1}, Lcom/google/firebase/firestore/remote/f;->v(Lcom/google/protobuf/j0;)Lqo1;

    move-result-object p1

    invoke-static {v0, p1}, Lcom/google/firebase/firestore/model/MutableDocument;->s(Ldu;Lqo1;)Lcom/google/firebase/firestore/model/MutableDocument;

    move-result-object p1

    return-object p1
.end method

.method public final i(Lzt;)Lcom/google/firestore/v1/e;
    .locals 3

    .line 1
    invoke-static {}, Lcom/google/firestore/v1/e;->k0()Lcom/google/firestore/v1/e$b;

    move-result-object v0

    iget-object v1, p0, Lzk0;->a:Lcom/google/firebase/firestore/remote/f;

    invoke-interface {p1}, Lzt;->getKey()Ldu;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/firebase/firestore/remote/f;->I(Ldu;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/firestore/v1/e$b;->B(Ljava/lang/String;)Lcom/google/firestore/v1/e$b;

    invoke-interface {p1}, Lzt;->getData()La11;

    move-result-object v1

    invoke-virtual {v1}, La11;->l()Ljava/util/Map;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/firestore/v1/e$b;->A(Ljava/util/Map;)Lcom/google/firestore/v1/e$b;

    invoke-interface {p1}, Lzt;->getVersion()Lqo1;

    move-result-object p1

    invoke-virtual {p1}, Lqo1;->c()Lpw1;

    move-result-object p1

    iget-object v1, p0, Lzk0;->a:Lcom/google/firebase/firestore/remote/f;

    invoke-virtual {v1, p1}, Lcom/google/firebase/firestore/remote/f;->S(Lpw1;)Lcom/google/protobuf/j0;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/google/firestore/v1/e$b;->C(Lcom/google/protobuf/j0;)Lcom/google/firestore/v1/e$b;

    invoke-virtual {v0}, Lcom/google/protobuf/GeneratedMessageLite$a;->p()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object p1

    check-cast p1, Lcom/google/firestore/v1/e;

    return-object p1
.end method

.method public j(Ljava/util/List;)Lcom/google/firestore/admin/v1/Index;
    .locals 5

    .line 1
    invoke-static {}, Lcom/google/firestore/admin/v1/Index;->f0()Lcom/google/firestore/admin/v1/Index$b;

    move-result-object v0

    sget-object v1, Lcom/google/firestore/admin/v1/Index$QueryScope;->COLLECTION_GROUP:Lcom/google/firestore/admin/v1/Index$QueryScope;

    invoke-virtual {v0, v1}, Lcom/google/firestore/admin/v1/Index$b;->B(Lcom/google/firestore/admin/v1/Index$QueryScope;)Lcom/google/firestore/admin/v1/Index$b;

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/firebase/firestore/model/FieldIndex$Segment;

    invoke-static {}, Lcom/google/firestore/admin/v1/Index$IndexField;->g0()Lcom/google/firestore/admin/v1/Index$IndexField$a;

    move-result-object v2

    invoke-virtual {v1}, Lcom/google/firebase/firestore/model/FieldIndex$Segment;->d()Ls00;

    move-result-object v3

    invoke-virtual {v3}, Ls00;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/firestore/admin/v1/Index$IndexField$a;->B(Ljava/lang/String;)Lcom/google/firestore/admin/v1/Index$IndexField$a;

    invoke-virtual {v1}, Lcom/google/firebase/firestore/model/FieldIndex$Segment;->e()Lcom/google/firebase/firestore/model/FieldIndex$Segment$Kind;

    move-result-object v3

    sget-object v4, Lcom/google/firebase/firestore/model/FieldIndex$Segment$Kind;->CONTAINS:Lcom/google/firebase/firestore/model/FieldIndex$Segment$Kind;

    if-ne v3, v4, :cond_0

    sget-object v1, Lcom/google/firestore/admin/v1/Index$IndexField$ArrayConfig;->CONTAINS:Lcom/google/firestore/admin/v1/Index$IndexField$ArrayConfig;

    invoke-virtual {v2, v1}, Lcom/google/firestore/admin/v1/Index$IndexField$a;->A(Lcom/google/firestore/admin/v1/Index$IndexField$ArrayConfig;)Lcom/google/firestore/admin/v1/Index$IndexField$a;

    goto :goto_2

    :cond_0
    invoke-virtual {v1}, Lcom/google/firebase/firestore/model/FieldIndex$Segment;->e()Lcom/google/firebase/firestore/model/FieldIndex$Segment$Kind;

    move-result-object v1

    sget-object v3, Lcom/google/firebase/firestore/model/FieldIndex$Segment$Kind;->ASCENDING:Lcom/google/firebase/firestore/model/FieldIndex$Segment$Kind;

    if-ne v1, v3, :cond_1

    sget-object v1, Lcom/google/firestore/admin/v1/Index$IndexField$Order;->ASCENDING:Lcom/google/firestore/admin/v1/Index$IndexField$Order;

    goto :goto_1

    :cond_1
    sget-object v1, Lcom/google/firestore/admin/v1/Index$IndexField$Order;->DESCENDING:Lcom/google/firestore/admin/v1/Index$IndexField$Order;

    :goto_1
    invoke-virtual {v2, v1}, Lcom/google/firestore/admin/v1/Index$IndexField$a;->C(Lcom/google/firestore/admin/v1/Index$IndexField$Order;)Lcom/google/firestore/admin/v1/Index$IndexField$a;

    :goto_2
    invoke-virtual {v0, v2}, Lcom/google/firestore/admin/v1/Index$b;->A(Lcom/google/firestore/admin/v1/Index$IndexField$a;)Lcom/google/firestore/admin/v1/Index$b;

    goto :goto_0

    :cond_2
    invoke-virtual {v0}, Lcom/google/protobuf/GeneratedMessageLite$a;->p()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object p1

    check-cast p1, Lcom/google/firestore/admin/v1/Index;

    return-object p1
.end method

.method public k(Lzt;)Lcom/google/firebase/firestore/proto/MaybeDocument;
    .locals 2

    .line 1
    invoke-static {}, Lcom/google/firebase/firestore/proto/MaybeDocument;->j0()Lcom/google/firebase/firestore/proto/MaybeDocument$b;

    move-result-object v0

    invoke-interface {p1}, Lzt;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0, p1}, Lzk0;->n(Lzt;)Lcom/google/firebase/firestore/proto/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/firebase/firestore/proto/MaybeDocument$b;->C(Lcom/google/firebase/firestore/proto/a;)Lcom/google/firebase/firestore/proto/MaybeDocument$b;

    goto :goto_0

    :cond_0
    invoke-interface {p1}, Lzt;->d()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0, p1}, Lzk0;->i(Lzt;)Lcom/google/firestore/v1/e;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/firebase/firestore/proto/MaybeDocument$b;->A(Lcom/google/firestore/v1/e;)Lcom/google/firebase/firestore/proto/MaybeDocument$b;

    goto :goto_0

    :cond_1
    invoke-interface {p1}, Lzt;->i()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {p0, p1}, Lzk0;->p(Lzt;)Lcom/google/firebase/firestore/proto/b;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/firebase/firestore/proto/MaybeDocument$b;->D(Lcom/google/firebase/firestore/proto/b;)Lcom/google/firebase/firestore/proto/MaybeDocument$b;

    :goto_0
    invoke-interface {p1}, Lzt;->f()Z

    move-result p1

    invoke-virtual {v0, p1}, Lcom/google/firebase/firestore/proto/MaybeDocument$b;->B(Z)Lcom/google/firebase/firestore/proto/MaybeDocument$b;

    invoke-virtual {v0}, Lcom/google/protobuf/GeneratedMessageLite$a;->p()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object p1

    check-cast p1, Lcom/google/firebase/firestore/proto/MaybeDocument;

    return-object p1

    :cond_2
    const-string v0, "Cannot encode invalid document %s"

    filled-new-array {p1}, [Ljava/lang/Object;

    move-result-object p1

    invoke-static {v0, p1}, Lg9;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/AssertionError;

    move-result-object p1

    throw p1
.end method

.method public l(Lwx0;)Lcom/google/firestore/v1/Write;
    .locals 1

    .line 1
    iget-object v0, p0, Lzk0;->a:Lcom/google/firebase/firestore/remote/f;

    invoke-virtual {v0, p1}, Lcom/google/firebase/firestore/remote/f;->L(Lwx0;)Lcom/google/firestore/v1/Write;

    move-result-object p1

    return-object p1
.end method

.method public m(Lxx0;)Lea2;
    .locals 4

    .line 1
    invoke-static {}, Lea2;->o0()Lea2$b;

    move-result-object v0

    invoke-virtual {p1}, Lxx0;->e()I

    move-result v1

    invoke-virtual {v0, v1}, Lea2$b;->C(I)Lea2$b;

    iget-object v1, p0, Lzk0;->a:Lcom/google/firebase/firestore/remote/f;

    invoke-virtual {p1}, Lxx0;->g()Lpw1;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/firebase/firestore/remote/f;->S(Lpw1;)Lcom/google/protobuf/j0;

    move-result-object v1

    invoke-virtual {v0, v1}, Lea2$b;->D(Lcom/google/protobuf/j0;)Lea2$b;

    invoke-virtual {p1}, Lxx0;->d()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lwx0;

    iget-object v3, p0, Lzk0;->a:Lcom/google/firebase/firestore/remote/f;

    invoke-virtual {v3, v2}, Lcom/google/firebase/firestore/remote/f;->L(Lwx0;)Lcom/google/firestore/v1/Write;

    move-result-object v2

    invoke-virtual {v0, v2}, Lea2$b;->A(Lcom/google/firestore/v1/Write;)Lea2$b;

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Lxx0;->h()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lwx0;

    iget-object v2, p0, Lzk0;->a:Lcom/google/firebase/firestore/remote/f;

    invoke-virtual {v2, v1}, Lcom/google/firebase/firestore/remote/f;->L(Lwx0;)Lcom/google/firestore/v1/Write;

    move-result-object v1

    invoke-virtual {v0, v1}, Lea2$b;->B(Lcom/google/firestore/v1/Write;)Lea2$b;

    goto :goto_1

    :cond_1
    invoke-virtual {v0}, Lcom/google/protobuf/GeneratedMessageLite$a;->p()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object p1

    check-cast p1, Lea2;

    return-object p1
.end method

.method public final n(Lzt;)Lcom/google/firebase/firestore/proto/a;
    .locals 3

    .line 1
    invoke-static {}, Lcom/google/firebase/firestore/proto/a;->f0()Lcom/google/firebase/firestore/proto/a$b;

    move-result-object v0

    iget-object v1, p0, Lzk0;->a:Lcom/google/firebase/firestore/remote/f;

    invoke-interface {p1}, Lzt;->getKey()Ldu;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/firebase/firestore/remote/f;->I(Ldu;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/firebase/firestore/proto/a$b;->A(Ljava/lang/String;)Lcom/google/firebase/firestore/proto/a$b;

    iget-object v1, p0, Lzk0;->a:Lcom/google/firebase/firestore/remote/f;

    invoke-interface {p1}, Lzt;->getVersion()Lqo1;

    move-result-object p1

    invoke-virtual {p1}, Lqo1;->c()Lpw1;

    move-result-object p1

    invoke-virtual {v1, p1}, Lcom/google/firebase/firestore/remote/f;->S(Lpw1;)Lcom/google/protobuf/j0;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/google/firebase/firestore/proto/a$b;->B(Lcom/google/protobuf/j0;)Lcom/google/firebase/firestore/proto/a$b;

    invoke-virtual {v0}, Lcom/google/protobuf/GeneratedMessageLite$a;->p()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object p1

    check-cast p1, Lcom/google/firebase/firestore/proto/a;

    return-object p1
.end method

.method public o(Lau1;)Lcom/google/firebase/firestore/proto/Target;
    .locals 4

    .line 1
    sget-object v0, Lcom/google/firebase/firestore/local/QueryPurpose;->LISTEN:Lcom/google/firebase/firestore/local/QueryPurpose;

    invoke-virtual {p1}, Lau1;->c()Lcom/google/firebase/firestore/local/QueryPurpose;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    invoke-virtual {p1}, Lau1;->c()Lcom/google/firebase/firestore/local/QueryPurpose;

    move-result-object v2

    filled-new-array {v0, v2}, [Ljava/lang/Object;

    move-result-object v0

    const-string v2, "Only queries with purpose %s may be stored, got %s"

    invoke-static {v1, v2, v0}, Lg9;->d(ZLjava/lang/String;[Ljava/lang/Object;)V

    invoke-static {}, Lcom/google/firebase/firestore/proto/Target;->r0()Lcom/google/firebase/firestore/proto/Target$b;

    move-result-object v0

    invoke-virtual {p1}, Lau1;->h()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/firebase/firestore/proto/Target$b;->H(I)Lcom/google/firebase/firestore/proto/Target$b;

    move-result-object v1

    invoke-virtual {p1}, Lau1;->e()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/google/firebase/firestore/proto/Target$b;->D(J)Lcom/google/firebase/firestore/proto/Target$b;

    move-result-object v1

    iget-object v2, p0, Lzk0;->a:Lcom/google/firebase/firestore/remote/f;

    invoke-virtual {p1}, Lau1;->b()Lqo1;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/firebase/firestore/remote/f;->U(Lqo1;)Lcom/google/protobuf/j0;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/firebase/firestore/proto/Target$b;->C(Lcom/google/protobuf/j0;)Lcom/google/firebase/firestore/proto/Target$b;

    move-result-object v1

    iget-object v2, p0, Lzk0;->a:Lcom/google/firebase/firestore/remote/f;

    invoke-virtual {p1}, Lau1;->f()Lqo1;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/firebase/firestore/remote/f;->U(Lqo1;)Lcom/google/protobuf/j0;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/firebase/firestore/proto/Target$b;->G(Lcom/google/protobuf/j0;)Lcom/google/firebase/firestore/proto/Target$b;

    move-result-object v1

    invoke-virtual {p1}, Lau1;->d()Lcom/google/protobuf/ByteString;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/firebase/firestore/proto/Target$b;->F(Lcom/google/protobuf/ByteString;)Lcom/google/firebase/firestore/proto/Target$b;

    invoke-virtual {p1}, Lau1;->g()Lcom/google/firebase/firestore/core/q;

    move-result-object p1

    invoke-virtual {p1}, Lcom/google/firebase/firestore/core/q;->s()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lzk0;->a:Lcom/google/firebase/firestore/remote/f;

    invoke-virtual {v1, p1}, Lcom/google/firebase/firestore/remote/f;->C(Lcom/google/firebase/firestore/core/q;)Lcom/google/firestore/v1/Target$c;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/google/firebase/firestore/proto/Target$b;->B(Lcom/google/firestore/v1/Target$c;)Lcom/google/firebase/firestore/proto/Target$b;

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lzk0;->a:Lcom/google/firebase/firestore/remote/f;

    invoke-virtual {v1, p1}, Lcom/google/firebase/firestore/remote/f;->P(Lcom/google/firebase/firestore/core/q;)Lcom/google/firestore/v1/Target$QueryTarget;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/google/firebase/firestore/proto/Target$b;->E(Lcom/google/firestore/v1/Target$QueryTarget;)Lcom/google/firebase/firestore/proto/Target$b;

    :goto_0
    invoke-virtual {v0}, Lcom/google/protobuf/GeneratedMessageLite$a;->p()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object p1

    check-cast p1, Lcom/google/firebase/firestore/proto/Target;

    return-object p1
.end method

.method public final p(Lzt;)Lcom/google/firebase/firestore/proto/b;
    .locals 3

    .line 1
    invoke-static {}, Lcom/google/firebase/firestore/proto/b;->f0()Lcom/google/firebase/firestore/proto/b$b;

    move-result-object v0

    iget-object v1, p0, Lzk0;->a:Lcom/google/firebase/firestore/remote/f;

    invoke-interface {p1}, Lzt;->getKey()Ldu;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/firebase/firestore/remote/f;->I(Ldu;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/firebase/firestore/proto/b$b;->A(Ljava/lang/String;)Lcom/google/firebase/firestore/proto/b$b;

    iget-object v1, p0, Lzk0;->a:Lcom/google/firebase/firestore/remote/f;

    invoke-interface {p1}, Lzt;->getVersion()Lqo1;

    move-result-object p1

    invoke-virtual {p1}, Lqo1;->c()Lpw1;

    move-result-object p1

    invoke-virtual {v1, p1}, Lcom/google/firebase/firestore/remote/f;->S(Lpw1;)Lcom/google/protobuf/j0;

    move-result-object p1

    invoke-virtual {v0, p1}, Lcom/google/firebase/firestore/proto/b$b;->B(Lcom/google/protobuf/j0;)Lcom/google/firebase/firestore/proto/b$b;

    invoke-virtual {v0}, Lcom/google/protobuf/GeneratedMessageLite$a;->p()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object p1

    check-cast p1, Lcom/google/firebase/firestore/proto/b;

    return-object p1
.end method

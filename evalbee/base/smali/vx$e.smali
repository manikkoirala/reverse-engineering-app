.class public Lvx$e;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lvx;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Landroid/widget/TextView;

.field public final synthetic b:Landroid/widget/Spinner;

.field public final synthetic c:Landroid/widget/TextView;

.field public final synthetic d:Landroid/widget/LinearLayout;

.field public final synthetic e:Ljava/util/ArrayList;

.field public final synthetic f:Lvx;


# direct methods
.method public constructor <init>(Lvx;Landroid/widget/TextView;Landroid/widget/Spinner;Landroid/widget/TextView;Landroid/widget/LinearLayout;Ljava/util/ArrayList;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lvx$e;->f:Lvx;

    iput-object p2, p0, Lvx$e;->a:Landroid/widget/TextView;

    iput-object p3, p0, Lvx$e;->b:Landroid/widget/Spinner;

    iput-object p4, p0, Lvx$e;->c:Landroid/widget/TextView;

    iput-object p5, p0, Lvx$e;->d:Landroid/widget/LinearLayout;

    iput-object p6, p0, Lvx$e;->e:Ljava/util/ArrayList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 0

    .line 1
    iget-object p1, p0, Lvx$e;->a:Landroid/widget/TextView;

    const/16 p2, 0x8

    invoke-virtual {p1, p2}, Landroid/view/View;->setVisibility(I)V

    iget-object p1, p0, Lvx$e;->b:Landroid/widget/Spinner;

    invoke-virtual {p1}, Landroid/widget/AdapterView;->getSelectedItemPosition()I

    move-result p1

    const/4 p3, 0x0

    if-eqz p1, :cond_2

    const/4 p4, 0x1

    if-eq p1, p4, :cond_1

    const/4 p5, 0x2

    if-eq p1, p5, :cond_0

    iget-object p1, p0, Lvx$e;->c:Landroid/widget/TextView;

    const-string p4, "Only you have access to this exam"

    goto :goto_1

    :cond_0
    iget-object p1, p0, Lvx$e;->c:Landroid/widget/TextView;

    invoke-virtual {p1, p2}, Landroid/view/View;->setVisibility(I)V

    iget-object p1, p0, Lvx$e;->d:Landroid/widget/LinearLayout;

    invoke-virtual {p1, p3}, Landroid/view/View;->setVisibility(I)V

    iget-object p1, p0, Lvx$e;->f:Lvx;

    sget-object p2, Lcom/ekodroid/omrevaluator/serializable/SharedType;->SHARED:Lcom/ekodroid/omrevaluator/serializable/SharedType;

    invoke-static {p1, p2}, Lvx;->e(Lvx;Lcom/ekodroid/omrevaluator/serializable/SharedType;)Lcom/ekodroid/omrevaluator/serializable/SharedType;

    iget-object p1, p0, Lvx$e;->e:Ljava/util/ArrayList;

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result p1

    if-ge p1, p4, :cond_3

    iget-object p1, p0, Lvx$e;->a:Landroid/widget/TextView;

    invoke-virtual {p1, p3}, Landroid/view/View;->setVisibility(I)V

    iget-object p1, p0, Lvx$e;->a:Landroid/widget/TextView;

    const-string p2, "No teachers found, please add teachers to your account"

    goto :goto_0

    :cond_1
    iget-object p1, p0, Lvx$e;->c:Landroid/widget/TextView;

    invoke-virtual {p1, p3}, Landroid/view/View;->setVisibility(I)V

    iget-object p1, p0, Lvx$e;->d:Landroid/widget/LinearLayout;

    invoke-virtual {p1, p2}, Landroid/view/View;->setVisibility(I)V

    iget-object p1, p0, Lvx$e;->f:Lvx;

    sget-object p2, Lcom/ekodroid/omrevaluator/serializable/SharedType;->PUBLIC:Lcom/ekodroid/omrevaluator/serializable/SharedType;

    invoke-static {p1, p2}, Lvx;->e(Lvx;Lcom/ekodroid/omrevaluator/serializable/SharedType;)Lcom/ekodroid/omrevaluator/serializable/SharedType;

    iget-object p1, p0, Lvx$e;->c:Landroid/widget/TextView;

    const-string p2, "All teachers in your organization account will have access to the exam"

    :goto_0
    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    :cond_2
    iget-object p1, p0, Lvx$e;->c:Landroid/widget/TextView;

    const-string p4, "Only you have access to the exam"

    :goto_1
    invoke-virtual {p1, p4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object p1, p0, Lvx$e;->c:Landroid/widget/TextView;

    invoke-virtual {p1, p3}, Landroid/view/View;->setVisibility(I)V

    iget-object p1, p0, Lvx$e;->d:Landroid/widget/LinearLayout;

    invoke-virtual {p1, p2}, Landroid/view/View;->setVisibility(I)V

    iget-object p1, p0, Lvx$e;->f:Lvx;

    sget-object p2, Lcom/ekodroid/omrevaluator/serializable/SharedType;->PRIVATE:Lcom/ekodroid/omrevaluator/serializable/SharedType;

    invoke-static {p1, p2}, Lvx;->e(Lvx;Lcom/ekodroid/omrevaluator/serializable/SharedType;)Lcom/ekodroid/omrevaluator/serializable/SharedType;

    :cond_3
    :goto_2
    return-void
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0

    .line 1
    return-void
.end method

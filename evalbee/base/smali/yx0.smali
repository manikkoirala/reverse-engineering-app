.class public final Lyx0;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Lxx0;

.field public final b:Lqo1;

.field public final c:Ljava/util/List;

.field public final d:Lcom/google/protobuf/ByteString;

.field public final e:Lcom/google/firebase/database/collection/b;


# direct methods
.method public constructor <init>(Lxx0;Lqo1;Ljava/util/List;Lcom/google/protobuf/ByteString;Lcom/google/firebase/database/collection/b;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lyx0;->a:Lxx0;

    iput-object p2, p0, Lyx0;->b:Lqo1;

    iput-object p3, p0, Lyx0;->c:Ljava/util/List;

    iput-object p4, p0, Lyx0;->d:Lcom/google/protobuf/ByteString;

    iput-object p5, p0, Lyx0;->e:Lcom/google/firebase/database/collection/b;

    return-void
.end method

.method public static a(Lxx0;Lqo1;Ljava/util/List;Lcom/google/protobuf/ByteString;)Lyx0;
    .locals 9

    .line 1
    invoke-virtual {p0}, Lxx0;->h()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x0

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    move v0, v2

    :goto_0
    invoke-virtual {p0}, Lxx0;->h()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    filled-new-array {v1, v3}, [Ljava/lang/Object;

    move-result-object v1

    const-string v3, "Mutations sent %d must equal results received %d"

    invoke-static {v0, v3, v1}, Lg9;->d(ZLjava/lang/String;[Ljava/lang/Object;)V

    invoke-static {}, Lau;->b()Lcom/google/firebase/database/collection/b;

    move-result-object v0

    invoke-virtual {p0}, Lxx0;->h()Ljava/util/List;

    move-result-object v1

    move-object v8, v0

    :goto_1
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_1

    invoke-interface {p2, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lay0;

    invoke-virtual {v0}, Lay0;->b()Lqo1;

    move-result-object v0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lwx0;

    invoke-virtual {v3}, Lwx0;->g()Ldu;

    move-result-object v3

    invoke-virtual {v8, v3, v0}, Lcom/google/firebase/database/collection/b;->l(Ljava/lang/Object;Ljava/lang/Object;)Lcom/google/firebase/database/collection/b;

    move-result-object v8

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_1
    new-instance v0, Lyx0;

    move-object v3, v0

    move-object v4, p0

    move-object v5, p1

    move-object v6, p2

    move-object v7, p3

    invoke-direct/range {v3 .. v8}, Lyx0;-><init>(Lxx0;Lqo1;Ljava/util/List;Lcom/google/protobuf/ByteString;Lcom/google/firebase/database/collection/b;)V

    return-object v0
.end method


# virtual methods
.method public b()Lxx0;
    .locals 1

    .line 1
    iget-object v0, p0, Lyx0;->a:Lxx0;

    return-object v0
.end method

.method public c()Lqo1;
    .locals 1

    .line 1
    iget-object v0, p0, Lyx0;->b:Lqo1;

    return-object v0
.end method

.method public d()Lcom/google/firebase/database/collection/b;
    .locals 1

    .line 1
    iget-object v0, p0, Lyx0;->e:Lcom/google/firebase/database/collection/b;

    return-object v0
.end method

.method public e()Ljava/util/List;
    .locals 1

    .line 1
    iget-object v0, p0, Lyx0;->c:Ljava/util/List;

    return-object v0
.end method

.method public f()Lcom/google/protobuf/ByteString;
    .locals 1

    .line 1
    iget-object v0, p0, Lyx0;->d:Lcom/google/protobuf/ByteString;

    return-object v0
.end method

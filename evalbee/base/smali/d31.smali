.class public abstract Ld31;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static a:[[D


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .line 1
    const/4 v0, 0x1

    new-array v1, v0, [[D

    new-array v0, v0, [D

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    const/4 v4, 0x0

    aput-wide v2, v0, v4

    aput-object v0, v1, v4

    sput-object v1, Ld31;->a:[[D

    return-void
.end method

.method public static declared-synchronized a(II)D
    .locals 12

    .line 1
    const-class v0, Ld31;

    monitor-enter v0

    if-ltz p0, :cond_8

    if-ltz p1, :cond_8

    if-le p1, p0, :cond_0

    goto :goto_5

    :cond_0
    :try_start_0
    sget-object v1, Ld31;->a:[[D

    array-length v2, v1

    if-lt p0, v2, :cond_6

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x2

    const/4 v2, 0x1

    if-le p0, v1, :cond_1

    add-int/lit8 v1, p0, 0x1

    new-array v1, v1, [[D

    goto :goto_0

    :cond_1
    add-int/2addr v1, v2

    new-array v1, v1, [[D

    :goto_0
    const/4 v3, 0x0

    move v4, v3

    :goto_1
    sget-object v5, Ld31;->a:[[D

    array-length v6, v5

    if-ge v4, v6, :cond_2

    aget-object v5, v5, v4

    aput-object v5, v1, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :cond_2
    array-length v4, v5

    :goto_2
    array-length v5, v1

    if-ge v4, v5, :cond_5

    div-int/lit8 v5, v4, 0x2

    add-int/2addr v5, v2

    new-array v5, v5, [D

    aput-object v5, v1, v4

    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    aput-wide v6, v5, v3

    move v5, v2

    :goto_3
    aget-object v6, v1, v4

    array-length v7, v6

    if-ge v5, v7, :cond_4

    add-int/lit8 v7, v4, -0x1

    aget-object v7, v1, v7

    add-int/lit8 v8, v5, -0x1

    aget-wide v8, v7, v8

    array-length v10, v7

    if-ge v5, v10, :cond_3

    aget-wide v10, v7, v5

    add-double/2addr v8, v10

    goto :goto_4

    :cond_3
    const-wide/high16 v10, 0x4000000000000000L    # 2.0

    mul-double/2addr v8, v10

    :goto_4
    aput-wide v8, v6, v5

    add-int/lit8 v5, v5, 0x1

    goto :goto_3

    :cond_4
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    :cond_5
    sput-object v1, Ld31;->a:[[D

    :cond_6
    mul-int/lit8 v1, p1, 0x2

    if-le v1, p0, :cond_7

    sub-int p1, p0, p1

    :cond_7
    sget-object v1, Ld31;->a:[[D

    aget-object p0, v1, p0

    aget-wide v1, p0, p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-wide v1

    :catchall_0
    move-exception p0

    monitor-exit v0

    throw p0

    :cond_8
    :goto_5
    monitor-exit v0

    const-wide/16 p0, 0x0

    return-wide p0
.end method

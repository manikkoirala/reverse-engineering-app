.class public Lmo1;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:I

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

.field public e:D

.field public f:D

.field public g:I

.field public h:Ljava/lang/String;

.field public i:[Ljava/lang/String;

.field public j:[D

.field public k:[D

.field public l:I

.field public m:I

.field public n:I

.field public o:Z


# direct methods
.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;Lcom/ekodroid/omrevaluator/templateui/models/ExamId;DDILjava/lang/String;[Ljava/lang/String;[D[DIIIZ)V
    .locals 3

    .line 1
    move-object v0, p0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    move v1, p1

    iput v1, v0, Lmo1;->a:I

    move-object v1, p2

    iput-object v1, v0, Lmo1;->b:Ljava/lang/String;

    move-object v1, p3

    iput-object v1, v0, Lmo1;->c:Ljava/lang/String;

    move-object v1, p4

    iput-object v1, v0, Lmo1;->d:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    move-wide v1, p5

    iput-wide v1, v0, Lmo1;->e:D

    move-wide v1, p7

    iput-wide v1, v0, Lmo1;->f:D

    move v1, p9

    iput v1, v0, Lmo1;->g:I

    move-object v1, p10

    iput-object v1, v0, Lmo1;->h:Ljava/lang/String;

    move-object v1, p11

    iput-object v1, v0, Lmo1;->i:[Ljava/lang/String;

    move-object v1, p12

    iput-object v1, v0, Lmo1;->j:[D

    move-object/from16 v1, p13

    iput-object v1, v0, Lmo1;->k:[D

    move/from16 v1, p14

    iput v1, v0, Lmo1;->l:I

    move/from16 v1, p15

    iput v1, v0, Lmo1;->m:I

    move/from16 v1, p16

    iput v1, v0, Lmo1;->n:I

    move/from16 v1, p17

    iput-boolean v1, v0, Lmo1;->o:Z

    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .line 1
    iget v0, p0, Lmo1;->l:I

    return v0
.end method

.method public b()Lcom/ekodroid/omrevaluator/templateui/models/ExamId;
    .locals 1

    .line 1
    iget-object v0, p0, Lmo1;->d:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lmo1;->h:Ljava/lang/String;

    return-object v0
.end method

.method public d()I
    .locals 1

    .line 1
    iget v0, p0, Lmo1;->m:I

    return v0
.end method

.method public e()D
    .locals 2

    .line 1
    iget-wide v0, p0, Lmo1;->f:D

    return-wide v0
.end method

.method public f()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lmo1;->c:Ljava/lang/String;

    return-object v0
.end method

.method public g()I
    .locals 1

    .line 1
    iget v0, p0, Lmo1;->g:I

    return v0
.end method

.method public h()I
    .locals 1

    .line 1
    iget v0, p0, Lmo1;->a:I

    return v0
.end method

.method public i()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lmo1;->b:Ljava/lang/String;

    return-object v0
.end method

.method public j()[Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lmo1;->i:[Ljava/lang/String;

    return-object v0
.end method

.method public k()[D
    .locals 1

    .line 1
    iget-object v0, p0, Lmo1;->j:[D

    return-object v0
.end method

.method public l()[D
    .locals 1

    .line 1
    iget-object v0, p0, Lmo1;->k:[D

    return-object v0
.end method

.method public m()D
    .locals 2

    .line 1
    iget-wide v0, p0, Lmo1;->e:D

    return-wide v0
.end method

.method public n()I
    .locals 1

    .line 1
    iget v0, p0, Lmo1;->n:I

    return v0
.end method

.method public o()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lmo1;->o:Z

    return v0
.end method

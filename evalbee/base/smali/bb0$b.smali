.class public Lbb0$b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/tasks/OnSuccessListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lbb0;->d()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Lbb0;


# direct methods
.method public constructor <init>(Lbb0;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lbb0$b;->a:Lbb0;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lya0;)V
    .locals 2

    .line 1
    invoke-virtual {p1}, Lya0;->a()Ljava/util/Map;

    move-result-object v0

    const-string v1, "orgId"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v1, p0, Lbb0$b;->a:Lbb0;

    iget-object v1, v1, Lbb0;->e:Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;->getOrgId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lbb0$b;->a:Lbb0;

    invoke-virtual {p1}, Lya0;->c()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Lbb0;->b(Lbb0;Ljava/lang/String;)V

    iget-object p1, p0, Lbb0$b;->a:Lbb0;

    iget-object p1, p1, Lbb0;->d:Landroid/content/Context;

    invoke-static {p1}, Lcom/google/firebase/analytics/FirebaseAnalytics;->getInstance(Landroid/content/Context;)Lcom/google/firebase/analytics/FirebaseAnalytics;

    move-result-object p1

    const-string v0, "FORCE_ORG_UPDATE"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Lcom/google/firebase/analytics/FirebaseAnalytics;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    :cond_0
    return-void
.end method

.method public bridge synthetic onSuccess(Ljava/lang/Object;)V
    .locals 0

    .line 1
    check-cast p1, Lya0;

    invoke-virtual {p0, p1}, Lbb0$b;->a(Lya0;)V

    return-void
.end method

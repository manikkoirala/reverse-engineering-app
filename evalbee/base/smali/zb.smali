.class public Lzb;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lzb$c;,
        Lzb$a;,
        Lzb$b;
    }
.end annotation


# instance fields
.field public a:Z

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Lzb$c;

.field public e:Lcom/google/android/gms/internal/play_billing/zzaf;

.field public f:Ljava/util/ArrayList;

.field public g:Z


# direct methods
.method public synthetic constructor <init>(Lzc2;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a()Lzb$a;
    .locals 2

    .line 1
    new-instance v0, Lzb$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lzb$a;-><init>(Lpc2;)V

    return-object v0
.end method

.method public static bridge synthetic j(Lzb;Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lzb;->a:Z

    return-void
.end method

.method public static bridge synthetic k(Lzb;Z)V
    .locals 0

    .line 1
    iput-boolean p1, p0, Lzb;->g:Z

    return-void
.end method

.method public static bridge synthetic l(Lzb;Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lzb;->b:Ljava/lang/String;

    return-void
.end method

.method public static bridge synthetic m(Lzb;Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lzb;->c:Ljava/lang/String;

    return-void
.end method

.method public static bridge synthetic n(Lzb;Lcom/google/android/gms/internal/play_billing/zzaf;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lzb;->e:Lcom/google/android/gms/internal/play_billing/zzaf;

    return-void
.end method

.method public static bridge synthetic o(Lzb;Ljava/util/ArrayList;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lzb;->f:Ljava/util/ArrayList;

    return-void
.end method

.method public static bridge synthetic p(Lzb;Lzb$c;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lzb;->d:Lzb$c;

    return-void
.end method


# virtual methods
.method public final b()I
    .locals 1

    .line 1
    iget-object v0, p0, Lzb;->d:Lzb$c;

    invoke-virtual {v0}, Lzb$c;->b()I

    move-result v0

    return v0
.end method

.method public final c()I
    .locals 1

    .line 1
    iget-object v0, p0, Lzb;->d:Lzb$c;

    invoke-virtual {v0}, Lzb$c;->c()I

    move-result v0

    return v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lzb;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lzb;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lzb;->d:Lzb$c;

    invoke-virtual {v0}, Lzb$c;->d()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final g()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lzb;->d:Lzb$c;

    invoke-virtual {v0}, Lzb$c;->e()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final h()Ljava/util/ArrayList;
    .locals 2

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget-object v1, p0, Lzb;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    return-object v0
.end method

.method public final i()Ljava/util/List;
    .locals 1

    .line 1
    iget-object v0, p0, Lzb;->e:Lcom/google/android/gms/internal/play_billing/zzaf;

    return-object v0
.end method

.method public final q()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lzb;->g:Z

    return v0
.end method

.method public final r()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lzb;->b:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p0, Lzb;->c:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p0, Lzb;->d:Lzb$c;

    invoke-virtual {v0}, Lzb$c;->e()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lzb;->d:Lzb$c;

    invoke-virtual {v0}, Lzb$c;->b()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lzb;->d:Lzb$c;

    invoke-virtual {v0}, Lzb$c;->c()I

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lzb;->a:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lzb;->g:Z

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return v0

    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.class public final Ljz1;
.super Lhz1;
.source "SourceFile"


# instance fields
.field public final a:Lgc0;

.field public final b:Lhz1;

.field public final c:Ljava/lang/reflect/Type;


# direct methods
.method public constructor <init>(Lgc0;Lhz1;Ljava/lang/reflect/Type;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lhz1;-><init>()V

    iput-object p1, p0, Ljz1;->a:Lgc0;

    iput-object p2, p0, Ljz1;->b:Lhz1;

    iput-object p3, p0, Ljz1;->c:Ljava/lang/reflect/Type;

    return-void
.end method

.method public static e(Ljava/lang/reflect/Type;Ljava/lang/Object;)Ljava/lang/reflect/Type;
    .locals 1

    .line 1
    if-eqz p1, :cond_1

    instance-of v0, p0, Ljava/lang/Class;

    if-nez v0, :cond_0

    instance-of v0, p0, Ljava/lang/reflect/TypeVariable;

    if-eqz v0, :cond_1

    :cond_0
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p0

    :cond_1
    return-object p0
.end method

.method public static f(Lhz1;)Z
    .locals 1

    .line 1
    :goto_0
    instance-of v0, p0, Lll1;

    if-eqz v0, :cond_1

    move-object v0, p0

    check-cast v0, Lll1;

    invoke-virtual {v0}, Lll1;->e()Lhz1;

    move-result-object v0

    if-ne v0, p0, :cond_0

    goto :goto_1

    :cond_0
    move-object p0, v0

    goto :goto_0

    :cond_1
    :goto_1
    instance-of p0, p0, Ldd1$b;

    return p0
.end method


# virtual methods
.method public b(Lrh0;)Ljava/lang/Object;
    .locals 1

    .line 1
    iget-object v0, p0, Ljz1;->b:Lhz1;

    invoke-virtual {v0, p1}, Lhz1;->b(Lrh0;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public d(Lvh0;Ljava/lang/Object;)V
    .locals 3

    .line 1
    iget-object v0, p0, Ljz1;->b:Lhz1;

    iget-object v1, p0, Ljz1;->c:Ljava/lang/reflect/Type;

    invoke-static {v1, p2}, Ljz1;->e(Ljava/lang/reflect/Type;Ljava/lang/Object;)Ljava/lang/reflect/Type;

    move-result-object v1

    iget-object v2, p0, Ljz1;->c:Ljava/lang/reflect/Type;

    if-eq v1, v2, :cond_1

    iget-object v0, p0, Ljz1;->a:Lgc0;

    invoke-static {v1}, Lcom/google/gson/reflect/TypeToken;->get(Ljava/lang/reflect/Type;)Lcom/google/gson/reflect/TypeToken;

    move-result-object v1

    invoke-virtual {v0, v1}, Lgc0;->l(Lcom/google/gson/reflect/TypeToken;)Lhz1;

    move-result-object v0

    instance-of v1, v0, Ldd1$b;

    if-nez v1, :cond_0

    goto :goto_0

    :cond_0
    iget-object v1, p0, Ljz1;->b:Lhz1;

    invoke-static {v1}, Ljz1;->f(Lhz1;)Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v0, p0, Ljz1;->b:Lhz1;

    :cond_1
    :goto_0
    invoke-virtual {v0, p1, p2}, Lhz1;->d(Lvh0;Ljava/lang/Object;)V

    return-void
.end method

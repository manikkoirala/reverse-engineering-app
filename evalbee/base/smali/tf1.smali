.class public Ltf1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lt90;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a([DI)D
    .locals 2

    .line 1
    const/4 p2, 0x0

    aget-wide v0, p1, p2

    const-wide/high16 p1, 0x43e0000000000000L    # 9.223372036854776E18

    cmpl-double p1, v0, p1

    if-gez p1, :cond_1

    const-wide/high16 p1, -0x3c20000000000000L    # -9.223372036854776E18

    cmpg-double p1, v0, p1

    if-gtz p1, :cond_0

    goto :goto_0

    :cond_0
    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    move-result-wide p1

    long-to-double p1, p1

    return-wide p1

    :cond_1
    :goto_0
    return-wide v0
.end method

.method public b(I)Z
    .locals 1

    .line 1
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .line 1
    const-string v0, "round(x)"

    return-object v0
.end method

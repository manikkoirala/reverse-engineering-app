.class public Lz80;
.super Lyu1;
.source "SourceFile"


# instance fields
.field public d:Leb;

.field public e:[D


# direct methods
.method public constructor <init>(Ljava/lang/String;Z)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2}, Lyu1;-><init>(Ljava/lang/String;Z)V

    new-instance p1, Leb;

    const/4 p2, 0x1

    invoke-direct {p1, p2}, Leb;-><init>(I)V

    iput-object p1, p0, Lz80;->d:Leb;

    new-array p1, p2, [D

    iput-object p1, p0, Lz80;->e:[D

    return-void
.end method


# virtual methods
.method public b(Lb32;Ly80;)D
    .locals 5

    .line 1
    iget-object v0, p0, Lz80;->d:Leb;

    invoke-virtual {v0}, Leb;->h()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    iget-object v2, p0, Lz80;->e:[D

    invoke-virtual {p0, v1}, Lz80;->n(I)Loz;

    move-result-object v3

    invoke-virtual {v3, p1, p2}, Loz;->b(Lb32;Ly80;)D

    move-result-wide v3

    aput-wide v3, v2, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lyu1;->b:Ljava/lang/String;

    invoke-virtual {p2, p1, v0}, Ly80;->a(Ljava/lang/String;I)Lt90;

    move-result-object p1

    iget-object p2, p0, Lz80;->e:[D

    invoke-interface {p1, p2, v0}, Lt90;->a([DI)D

    move-result-wide p1

    iget-boolean v0, p0, Lyu1;->c:Z

    if-eqz v0, :cond_1

    neg-double p1, p1

    :cond_1
    return-wide p1
.end method

.method public m(Loz;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lz80;->d:Leb;

    invoke-virtual {v0}, Leb;->h()I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lz80;->o(Loz;I)V

    return-void
.end method

.method public n(I)Loz;
    .locals 1

    .line 1
    iget-object v0, p0, Lz80;->d:Leb;

    invoke-virtual {v0, p1}, Leb;->e(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Loz;

    return-object p1
.end method

.method public o(Loz;I)V
    .locals 2

    .line 1
    invoke-virtual {p0, p1}, Loz;->a(Loz;)V

    iget-object v0, p0, Lz80;->d:Leb;

    invoke-virtual {v0}, Leb;->f()I

    move-result v0

    iget-object v1, p0, Lz80;->d:Leb;

    invoke-virtual {v1, p1, p2}, Leb;->g(Ljava/lang/Object;I)V

    iget-object p2, p0, Lz80;->d:Leb;

    invoke-virtual {p2}, Leb;->f()I

    move-result p2

    if-eq v0, p2, :cond_0

    new-array p2, p2, [D

    iput-object p2, p0, Lz80;->e:[D

    :cond_0
    iput-object p0, p1, Loz;->a:Loz;

    return-void
.end method

.method public p()I
    .locals 1

    .line 1
    iget-object v0, p0, Lz80;->d:Leb;

    invoke-virtual {v0}, Leb;->h()I

    move-result v0

    return v0
.end method

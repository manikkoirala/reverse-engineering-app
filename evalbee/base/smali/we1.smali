.class public Lwe1;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/ArrayList;Ljava/util/ArrayList;IILjava/lang/String;)V
    .locals 5

    .line 1
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ge v0, v1, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/ekodroid/omrevaluator/database/StudentDataModel;

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/database/StudentDataModel;->getRollNO()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ne v1, p3, :cond_1

    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    return-void

    :cond_1
    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/ekodroid/omrevaluator/database/StudentDataModel;

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/database/StudentDataModel;->getRollNO()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ge v1, p3, :cond_3

    new-array v1, p4, [Ljava/lang/String;

    move v2, v0

    :goto_0
    const-string v3, ""

    if-ge v2, p4, :cond_2

    aput-object v3, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    aput-object p5, v1, v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/ekodroid/omrevaluator/database/StudentDataModel;

    invoke-virtual {v4}, Lcom/ekodroid/omrevaluator/database/StudentDataModel;->getRollNO()Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    aput-object v2, v1, v3

    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/ekodroid/omrevaluator/database/StudentDataModel;

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/database/StudentDataModel;->getStudentName()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x3

    aput-object v2, v1, v3

    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    invoke-virtual/range {p0 .. p5}, Lwe1;->a(Ljava/util/ArrayList;Ljava/util/ArrayList;IILjava/lang/String;)V

    :cond_3
    return-void
.end method

.method public final b(Ljava/util/ArrayList;Ljava/util/ArrayList;ILjava/lang/String;)V
    .locals 6

    .line 1
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    new-array v2, p3, [Ljava/lang/String;

    move v3, v0

    :goto_1
    const-string v4, ""

    if-ge v3, p3, :cond_0

    aput-object v4, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_0
    aput-object p4, v2, v0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/ekodroid/omrevaluator/database/StudentDataModel;

    invoke-virtual {v5}, Lcom/ekodroid/omrevaluator/database/StudentDataModel;->getRollNO()Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x2

    aput-object v3, v2, v4

    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/ekodroid/omrevaluator/database/StudentDataModel;

    invoke-virtual {v3}, Lcom/ekodroid/omrevaluator/database/StudentDataModel;->getStudentName()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x3

    aput-object v3, v2, v4

    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method public c(Landroid/content/Context;Lcom/ekodroid/omrevaluator/templateui/models/ExamId;Ljava/util/ArrayList;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;Z)[[Ljava/lang/String;
    .locals 22

    .line 1
    move-object/from16 v0, p0

    move-object/from16 v1, p3

    move-object/from16 v2, p4

    if-eqz p5, :cond_0

    invoke-virtual/range {p0 .. p4}, Lwe1;->e(Landroid/content/Context;Lcom/ekodroid/omrevaluator/templateui/models/ExamId;Ljava/util/ArrayList;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)[[Ljava/lang/String;

    move-result-object v1

    return-object v1

    :cond_0
    invoke-virtual/range {p4 .. p4}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getLabelProfile()Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    move-result-object v3

    invoke-virtual/range {p4 .. p4}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getTemplateParams()Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;

    move-result-object v4

    invoke-virtual {v4}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;->getRollDigits()I

    move-result v4

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    const/4 v6, 0x0

    invoke-virtual {v1, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ld8;

    invoke-virtual {v7}, Ld8;->a()Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;

    move-result-object v7

    invoke-virtual {v0, v7, v2}, Lwe1;->f(Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)[Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual/range {p4 .. p4}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getAnswerKeys()[Lcom/ekodroid/omrevaluator/templateui/models/AnswerSetKey;

    move-result-object v7

    move v8, v6

    :goto_0
    invoke-virtual/range {p3 .. p3}, Ljava/util/ArrayList;->size()I

    move-result v9

    if-ge v8, v9, :cond_8

    invoke-virtual {v1, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ld8;

    invoke-virtual {v9}, Ld8;->b()Lye1;

    move-result-object v10

    invoke-virtual {v10}, Lye1;->g()I

    move-result v10

    invoke-virtual {v9}, Ld8;->a()Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;

    move-result-object v11

    if-nez v11, :cond_1

    move-object/from16 v21, v3

    move/from16 v19, v4

    move-object v2, v5

    goto/16 :goto_6

    :cond_1
    invoke-virtual {v11, v2}, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;->getResultItem(Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;

    move-result-object v11

    invoke-virtual/range {p4 .. p4}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getTemplateParams()Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;

    move-result-object v12

    invoke-virtual {v12}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;->getSubjects()[Lcom/ekodroid/omrevaluator/templateui/models/Subject2;

    move-result-object v12

    invoke-virtual {v0, v12}, Lwe1;->g([Lcom/ekodroid/omrevaluator/templateui/models/Subject2;)I

    move-result v12

    invoke-virtual {v11}, Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;->getMarksForEachSubject()[D

    move-result-object v13

    invoke-static {v11, v2}, Lve1;->k(Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)[[D

    move-result-object v14

    array-length v15, v13

    add-int/lit8 v15, v15, 0xa

    add-int/2addr v15, v12

    invoke-virtual {v11}, Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;->getAnswerValue2s()Ljava/util/ArrayList;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/util/ArrayList;->size()I

    move-result v16

    const/16 v17, 0x3

    mul-int/lit8 v16, v16, 0x3

    add-int v15, v15, v16

    new-array v15, v15, [Ljava/lang/String;

    invoke-virtual/range {p4 .. p4}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getName()Ljava/lang/String;

    move-result-object v16

    aput-object v16, v15, v6

    const/4 v6, 0x1

    const-string v1, ""

    aput-object v1, v15, v6

    invoke-virtual {v11}, Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;->getExamSet()I

    move-result v16

    if-lez v16, :cond_2

    invoke-virtual {v3}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getExamSetLabels()[Ljava/lang/String;

    move-result-object v16

    invoke-virtual {v11}, Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;->getExamSet()I

    move-result v18

    add-int/lit8 v18, v18, -0x1

    aget-object v16, v16, v18

    aput-object v16, v15, v6

    :cond_2
    const/16 v16, 0x2

    invoke-virtual {v0, v10, v4}, Lwe1;->h(II)Ljava/lang/String;

    move-result-object v18

    aput-object v18, v15, v16

    invoke-virtual/range {p2 .. p2}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;->getClassName()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v2, p1

    invoke-virtual {v0, v2, v10, v6}, Lwe1;->i(Landroid/content/Context;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v15, v17

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9}, Ld8;->b()Lye1;

    move-result-object v10

    invoke-virtual {v10}, Lye1;->d()D

    move-result-wide v17

    move-object v10, v3

    invoke-static/range {v17 .. v18}, Lve1;->o(D)D

    move-result-wide v2

    invoke-virtual {v6, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x4

    aput-object v2, v15, v3

    invoke-virtual {v9}, Ld8;->b()Lye1;

    move-result-object v2

    invoke-virtual {v2}, Lye1;->b()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x5

    aput-object v2, v15, v3

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9}, Ld8;->b()Lye1;

    move-result-object v3

    invoke-virtual {v3}, Lye1;->f()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x6

    aput-object v2, v15, v3

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9}, Ld8;->b()Lye1;

    move-result-object v3

    invoke-virtual {v3}, Lye1;->a()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x7

    aput-object v2, v15, v3

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9}, Ld8;->b()Lye1;

    move-result-object v3

    invoke-virtual {v3}, Lye1;->c()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x8

    aput-object v2, v15, v3

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9}, Ld8;->b()Lye1;

    move-result-object v3

    invoke-virtual {v3}, Lye1;->h()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x9

    aput-object v2, v15, v3

    const/4 v2, 0x0

    const/4 v3, 0x0

    :goto_1
    array-length v6, v13

    if-ge v2, v6, :cond_4

    add-int/lit8 v6, v3, 0xa

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    aget-wide v17, v13, v2

    move/from16 v19, v4

    move-object/from16 v20, v5

    invoke-static/range {v17 .. v18}, Lve1;->o(D)D

    move-result-wide v4

    invoke-virtual {v9, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v15, v6

    const/4 v4, 0x1

    add-int/2addr v3, v4

    aget-object v5, v14, v2

    array-length v5, v5

    if-le v5, v4, :cond_3

    const/4 v4, 0x0

    :goto_2
    aget-object v5, v14, v2

    array-length v5, v5

    if-ge v4, v5, :cond_3

    add-int/lit8 v5, v3, 0xa

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    aget-object v9, v14, v2

    aget-wide v17, v9, v4

    move-object/from16 v21, v10

    invoke-static/range {v17 .. v18}, Lve1;->o(D)D

    move-result-wide v9

    invoke-virtual {v6, v9, v10}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v15, v5

    add-int/lit8 v3, v3, 0x1

    add-int/lit8 v4, v4, 0x1

    move-object/from16 v10, v21

    goto :goto_2

    :cond_3
    move-object/from16 v21, v10

    add-int/lit8 v2, v2, 0x1

    move/from16 v4, v19

    move-object/from16 v5, v20

    move-object/from16 v10, v21

    goto :goto_1

    :cond_4
    move/from16 v19, v4

    move-object/from16 v20, v5

    move-object/from16 v21, v10

    const/4 v2, 0x0

    :goto_3
    invoke-virtual {v11}, Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;->getAnswerValue2s()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v2, v3, :cond_7

    invoke-virtual {v11}, Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;->getAnswerValue2s()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;

    invoke-virtual/range {p4 .. p4}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getLabelProfile()Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    move-result-object v4

    invoke-static {v3, v4}, Le5;->r(Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;)Ljava/lang/String;

    move-result-object v4

    if-eqz v7, :cond_6

    array-length v5, v7

    if-lez v5, :cond_6

    invoke-virtual {v11}, Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;->getExamSet()I

    move-result v5

    if-lez v5, :cond_5

    invoke-virtual {v11}, Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;->getExamSet()I

    move-result v5

    const/4 v6, 0x1

    sub-int/2addr v5, v6

    goto :goto_4

    :cond_5
    const/4 v6, 0x1

    const/4 v5, 0x0

    :goto_4
    aget-object v5, v7, v5

    invoke-virtual {v5}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerSetKey;->getAnswerOptionKeys()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;

    invoke-virtual/range {p4 .. p4}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getLabelProfile()Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    move-result-object v9

    invoke-static {v5, v9}, Le5;->q(Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;)Ljava/lang/String;

    move-result-object v5

    goto :goto_5

    :cond_6
    const/4 v6, 0x1

    move-object v5, v1

    :goto_5
    array-length v9, v13

    add-int/lit8 v9, v9, 0xa

    add-int/2addr v9, v12

    mul-int/lit8 v10, v2, 0x3

    add-int/2addr v9, v10

    aput-object v4, v15, v9

    array-length v4, v13

    add-int/lit8 v4, v4, 0xb

    add-int/2addr v4, v12

    add-int/2addr v4, v10

    aput-object v5, v15, v4

    array-length v4, v13

    add-int/lit8 v4, v4, 0xc

    add-int/2addr v4, v12

    add-int/2addr v4, v10

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3}, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->getMarksForAnswer()D

    move-result-wide v9

    invoke-virtual {v5, v9, v10}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v15, v4

    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_7
    move-object/from16 v2, v20

    invoke-virtual {v2, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_6
    add-int/lit8 v8, v8, 0x1

    move-object/from16 v1, p3

    move-object v5, v2

    move/from16 v4, v19

    move-object/from16 v3, v21

    const/4 v6, 0x0

    move-object/from16 v2, p4

    goto/16 :goto_0

    :cond_8
    move-object v2, v5

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [[Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [[Ljava/lang/String;

    return-object v1
.end method

.method public d(Landroid/content/Context;Lcom/ekodroid/omrevaluator/templateui/models/ExamId;Ljava/util/ArrayList;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;Z)[[Ljava/lang/String;
    .locals 6

    .line 1
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;

    move-result-object v0

    invoke-virtual {p3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p3

    :goto_0
    invoke-interface {p3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lye1;

    invoke-virtual {v1}, Lye1;->g()I

    move-result v2

    invoke-virtual {v0, p2, v2}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->getResult(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;I)Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;

    move-result-object v2

    new-instance v4, Ld8;

    invoke-direct {v4, v2, v1}, Ld8;-><init>(Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;Lye1;)V

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lwe1;->c(Landroid/content/Context;Lcom/ekodroid/omrevaluator/templateui/models/ExamId;Ljava/util/ArrayList;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;Z)[[Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public final e(Landroid/content/Context;Lcom/ekodroid/omrevaluator/templateui/models/ExamId;Ljava/util/ArrayList;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)[[Ljava/lang/String;
    .locals 27

    .line 1
    move-object/from16 v6, p0

    move-object/from16 v7, p4

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual/range {p3 .. p3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ld8;

    invoke-virtual {v8, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    invoke-virtual/range {p4 .. p4}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getTemplateParams()Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;

    move-result-object v0

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;->getRollDigits()I

    move-result v9

    invoke-virtual/range {p4 .. p4}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getLabelProfile()Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    move-result-object v10

    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    const/4 v12, 0x0

    invoke-virtual {v8, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ld8;

    invoke-virtual {v0}, Ld8;->a()Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;

    move-result-object v0

    invoke-virtual {v6, v0, v7}, Lwe1;->f(Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-static/range {p1 .. p1}, Lcom/ekodroid/omrevaluator/database/repositories/ClassRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/ClassRepository;

    move-result-object v0

    invoke-virtual/range {p2 .. p2}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;->getClassName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/ekodroid/omrevaluator/database/repositories/ClassRepository;->getStudents(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v13

    new-instance v0, Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList;

    invoke-direct {v0}, Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList;-><init>()V

    sget-object v1, Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList$ResultSortBy;->ROLLNO:Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList$ResultSortBy;

    const/4 v14, 0x1

    invoke-virtual {v0, v1, v8, v14}, Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList;->a(Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList$ResultSortBy;Ljava/util/ArrayList;Z)V

    invoke-virtual/range {p4 .. p4}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getAnswerKeys()[Lcom/ekodroid/omrevaluator/templateui/models/AnswerSetKey;

    move-result-object v15

    move v0, v12

    move v5, v0

    :goto_1
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v5, v1, :cond_8

    invoke-virtual {v8, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    move-object/from16 v16, v1

    check-cast v16, Ld8;

    invoke-virtual/range {v16 .. v16}, Ld8;->b()Lye1;

    move-result-object v1

    invoke-virtual {v1}, Lye1;->g()I

    move-result v4

    invoke-virtual/range {v16 .. v16}, Ld8;->a()Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;

    move-result-object v1

    const-string v3, ""

    if-nez v1, :cond_1

    invoke-static/range {p1 .. p1}, Lcom/google/firebase/analytics/FirebaseAnalytics;->getInstance(Landroid/content/Context;)Lcom/google/firebase/analytics/FirebaseAnalytics;

    move-result-object v1

    const-string v2, "RA_EE_RESULT_NULL"

    invoke-static {v1, v2, v3}, Lo4;->c(Lcom/google/firebase/analytics/FirebaseAnalytics;Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v3, p1

    move/from16 v26, v5

    move-object/from16 v22, v8

    move v12, v9

    move-object/from16 v24, v10

    move v8, v14

    goto/16 :goto_7

    :cond_1
    invoke-virtual {v1, v7}, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;->getResultItem(Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;

    move-result-object v2

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;->getMarksForEachSubject()[D

    move-result-object v1

    invoke-static {v2, v7}, Lve1;->k(Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)[[D

    move-result-object v17

    invoke-virtual/range {p4 .. p4}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getTemplateParams()Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;

    move-result-object v0

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;->getSubjects()[Lcom/ekodroid/omrevaluator/templateui/models/Subject2;

    move-result-object v0

    invoke-virtual {v6, v0}, Lwe1;->g([Lcom/ekodroid/omrevaluator/templateui/models/Subject2;)I

    move-result v18

    array-length v0, v1

    add-int/lit8 v0, v0, 0xa

    add-int v0, v0, v18

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;->getAnswerValue2s()Ljava/util/ArrayList;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/util/ArrayList;->size()I

    move-result v19

    const/16 v20, 0x3

    mul-int/lit8 v19, v19, 0x3

    add-int v0, v0, v19

    new-array v14, v0, [Ljava/lang/String;

    invoke-virtual/range {p4 .. p4}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getName()Ljava/lang/String;

    move-result-object v19

    move/from16 v21, v0

    move-object/from16 v0, p0

    move-object/from16 v22, v1

    move-object v1, v11

    move-object/from16 v23, v2

    move-object v2, v13

    move-object/from16 v24, v3

    move v3, v4

    move/from16 v25, v4

    move/from16 v4, v21

    move/from16 v26, v5

    move-object/from16 v5, v19

    invoke-virtual/range {v0 .. v5}, Lwe1;->a(Ljava/util/ArrayList;Ljava/util/ArrayList;IILjava/lang/String;)V

    invoke-virtual/range {p4 .. p4}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getName()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v14, v12

    move-object/from16 v0, v24

    const/4 v1, 0x1

    aput-object v0, v14, v1

    invoke-virtual/range {v23 .. v23}, Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;->getExamSet()I

    move-result v2

    if-lez v2, :cond_2

    invoke-virtual {v10}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getExamSetLabels()[Ljava/lang/String;

    move-result-object v2

    invoke-virtual/range {v23 .. v23}, Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;->getExamSet()I

    move-result v3

    sub-int/2addr v3, v1

    aget-object v2, v2, v3

    aput-object v2, v14, v1

    :cond_2
    const/4 v1, 0x2

    move/from16 v2, v25

    invoke-virtual {v6, v2, v9}, Lwe1;->h(II)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v14, v1

    invoke-virtual/range {p2 .. p2}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;->getClassName()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v3, p1

    invoke-virtual {v6, v3, v2, v1}, Lwe1;->i(Landroid/content/Context;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v14, v20

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {v16 .. v16}, Ld8;->b()Lye1;

    move-result-object v2

    invoke-virtual {v2}, Lye1;->d()D

    move-result-wide v4

    invoke-static {v4, v5}, Lve1;->o(D)D

    move-result-wide v4

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x4

    aput-object v1, v14, v2

    invoke-virtual/range {v16 .. v16}, Ld8;->b()Lye1;

    move-result-object v1

    invoke-virtual {v1}, Lye1;->b()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x5

    aput-object v1, v14, v2

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {v16 .. v16}, Ld8;->b()Lye1;

    move-result-object v2

    invoke-virtual {v2}, Lye1;->f()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x6

    aput-object v1, v14, v2

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {v16 .. v16}, Ld8;->b()Lye1;

    move-result-object v2

    invoke-virtual {v2}, Lye1;->a()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x7

    aput-object v1, v14, v2

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {v16 .. v16}, Ld8;->b()Lye1;

    move-result-object v2

    invoke-virtual {v2}, Lye1;->c()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x8

    aput-object v1, v14, v2

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {v16 .. v16}, Ld8;->b()Lye1;

    move-result-object v2

    invoke-virtual {v2}, Lye1;->h()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x9

    aput-object v1, v14, v2

    move v2, v12

    move v4, v2

    move-object/from16 v1, v22

    :goto_2
    array-length v5, v1

    if-ge v2, v5, :cond_4

    add-int/lit8 v5, v4, 0xa

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    aget-wide v19, v1, v2

    move-object/from16 v22, v8

    invoke-static/range {v19 .. v20}, Lve1;->o(D)D

    move-result-wide v7

    invoke-virtual {v12, v7, v8}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v14, v5

    const/4 v5, 0x1

    add-int/2addr v4, v5

    aget-object v7, v17, v2

    array-length v7, v7

    if-le v7, v5, :cond_3

    const/4 v5, 0x0

    :goto_3
    aget-object v7, v17, v2

    array-length v7, v7

    if-ge v5, v7, :cond_3

    add-int/lit8 v7, v4, 0xa

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    aget-object v12, v17, v2

    aget-wide v19, v12, v5

    move v12, v9

    move-object/from16 v24, v10

    invoke-static/range {v19 .. v20}, Lve1;->o(D)D

    move-result-wide v9

    invoke-virtual {v8, v9, v10}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v14, v7

    add-int/lit8 v4, v4, 0x1

    add-int/lit8 v5, v5, 0x1

    move v9, v12

    move-object/from16 v10, v24

    goto :goto_3

    :cond_3
    move v12, v9

    move-object/from16 v24, v10

    add-int/lit8 v2, v2, 0x1

    move-object/from16 v7, p4

    move v9, v12

    move-object/from16 v8, v22

    move-object/from16 v10, v24

    const/4 v12, 0x0

    goto :goto_2

    :cond_4
    move-object/from16 v22, v8

    move v12, v9

    move-object/from16 v24, v10

    const/4 v2, 0x0

    :goto_4
    invoke-virtual/range {v23 .. v23}, Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;->getAnswerValue2s()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v2, v4, :cond_7

    invoke-virtual/range {v23 .. v23}, Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;->getAnswerValue2s()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;

    invoke-virtual/range {p4 .. p4}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getLabelProfile()Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    move-result-object v5

    invoke-static {v4, v5}, Le5;->r(Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;)Ljava/lang/String;

    move-result-object v5

    if-eqz v15, :cond_6

    array-length v7, v15

    if-lez v7, :cond_6

    invoke-virtual/range {v23 .. v23}, Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;->getExamSet()I

    move-result v7

    if-lez v7, :cond_5

    invoke-virtual/range {v23 .. v23}, Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;->getExamSet()I

    move-result v7

    const/4 v8, 0x1

    sub-int/2addr v7, v8

    goto :goto_5

    :cond_5
    const/4 v8, 0x1

    const/4 v7, 0x0

    :goto_5
    aget-object v7, v15, v7

    invoke-virtual {v7}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerSetKey;->getAnswerOptionKeys()Ljava/util/ArrayList;

    move-result-object v7

    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;

    invoke-virtual/range {p4 .. p4}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getLabelProfile()Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    move-result-object v9

    invoke-static {v7, v9}, Le5;->q(Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;)Ljava/lang/String;

    move-result-object v7

    goto :goto_6

    :cond_6
    const/4 v8, 0x1

    move-object v7, v0

    :goto_6
    array-length v9, v1

    add-int/lit8 v9, v9, 0xa

    add-int v9, v9, v18

    mul-int/lit8 v10, v2, 0x3

    add-int/2addr v9, v10

    aput-object v5, v14, v9

    array-length v5, v1

    add-int/lit8 v5, v5, 0xb

    add-int v5, v5, v18

    add-int/2addr v5, v10

    aput-object v7, v14, v5

    array-length v5, v1

    add-int/lit8 v5, v5, 0xc

    add-int v5, v5, v18

    add-int/2addr v5, v10

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4}, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->getMarksForAnswer()D

    move-result-wide v9

    invoke-virtual {v7, v9, v10}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v14, v5

    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    :cond_7
    const/4 v8, 0x1

    invoke-virtual {v11, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move/from16 v0, v21

    :goto_7
    add-int/lit8 v5, v26, 0x1

    move-object/from16 v7, p4

    move v14, v8

    move v9, v12

    move-object/from16 v8, v22

    move-object/from16 v10, v24

    const/4 v12, 0x0

    goto/16 :goto_1

    :cond_8
    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_9

    if-lez v0, :cond_9

    invoke-virtual/range {p4 .. p4}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v11, v13, v0, v1}, Lwe1;->b(Ljava/util/ArrayList;Ljava/util/ArrayList;ILjava/lang/String;)V

    :cond_9
    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [[Ljava/lang/String;

    invoke-virtual {v11, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[Ljava/lang/String;

    return-object v0
.end method

.method public final f(Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)[Ljava/lang/String;
    .locals 13

    .line 1
    invoke-virtual {p2}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getLabelProfile()Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    move-result-object v0

    invoke-virtual {p2}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getTemplateParams()Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;

    move-result-object v1

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;->getSubjects()[Lcom/ekodroid/omrevaluator/templateui/models/Subject2;

    move-result-object v2

    array-length v2, v2

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;->getSubjects()[Lcom/ekodroid/omrevaluator/templateui/models/Subject2;

    move-result-object v3

    invoke-virtual {p0, v3}, Lwe1;->g([Lcom/ekodroid/omrevaluator/templateui/models/Subject2;)I

    move-result v3

    add-int/lit8 v4, v2, 0xa

    add-int/2addr v4, v3

    invoke-virtual {p1, p2}, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;->getResultItem(Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;

    move-result-object v5

    invoke-virtual {v5}, Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;->getAnswerValue2s()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    const/4 v6, 0x3

    mul-int/2addr v5, v6

    add-int/2addr v5, v4

    new-array v5, v5, [Ljava/lang/String;

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getExamNameString()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    aput-object v7, v5, v8

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getExamSetString()Ljava/lang/String;

    move-result-object v7

    const/4 v9, 0x1

    aput-object v7, v5, v9

    const/4 v7, 0x2

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getRollNoString()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v5, v7

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getNameString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x4

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getTotalMarksString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x5

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getGradeString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x6

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getRankString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x7

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getCorrectAnswerString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/16 v6, 0x8

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getIncorrectAnswerString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v6

    const/16 v0, 0x9

    const-string v6, "Not attempted"

    aput-object v6, v5, v0

    move v0, v8

    move v6, v0

    :goto_0
    if-ge v0, v2, :cond_1

    add-int/lit8 v7, v6, 0xa

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;->getSubjects()[Lcom/ekodroid/omrevaluator/templateui/models/Subject2;

    move-result-object v10

    aget-object v10, v10, v0

    invoke-virtual {v10}, Lcom/ekodroid/omrevaluator/templateui/models/Subject2;->getSubName()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v5, v7

    add-int/2addr v6, v9

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;->getSubjects()[Lcom/ekodroid/omrevaluator/templateui/models/Subject2;

    move-result-object v7

    aget-object v7, v7, v0

    invoke-virtual {v7}, Lcom/ekodroid/omrevaluator/templateui/models/Subject2;->getSections()[Lcom/ekodroid/omrevaluator/templateui/models/Section2;

    move-result-object v7

    array-length v7, v7

    if-le v7, v9, :cond_0

    move v7, v8

    :goto_1
    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;->getSubjects()[Lcom/ekodroid/omrevaluator/templateui/models/Subject2;

    move-result-object v10

    aget-object v10, v10, v0

    invoke-virtual {v10}, Lcom/ekodroid/omrevaluator/templateui/models/Subject2;->getSections()[Lcom/ekodroid/omrevaluator/templateui/models/Section2;

    move-result-object v10

    array-length v10, v10

    if-ge v7, v10, :cond_0

    add-int/lit8 v10, v6, 0xa

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;->getSubjects()[Lcom/ekodroid/omrevaluator/templateui/models/Subject2;

    move-result-object v12

    aget-object v12, v12, v0

    invoke-virtual {v12}, Lcom/ekodroid/omrevaluator/templateui/models/Subject2;->getSubName()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v12, "-"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;->getSubjects()[Lcom/ekodroid/omrevaluator/templateui/models/Subject2;

    move-result-object v12

    aget-object v12, v12, v0

    invoke-virtual {v12}, Lcom/ekodroid/omrevaluator/templateui/models/Subject2;->getSections()[Lcom/ekodroid/omrevaluator/templateui/models/Section2;

    move-result-object v12

    aget-object v12, v12, v7

    invoke-virtual {v12}, Lcom/ekodroid/omrevaluator/templateui/models/Section2;->getName()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    aput-object v11, v5, v10

    add-int/lit8 v6, v6, 0x1

    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    :goto_2
    invoke-virtual {p1, p2}, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;->getResultItem(Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;

    move-result-object v0

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;->getAnswerValue2s()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v8, v0, :cond_2

    mul-int/lit8 v0, v8, 0x3

    add-int v1, v4, v0

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Q "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v8, v8, 0x1

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v9, " Options"

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v1

    add-int/lit8 v1, v2, 0xb

    add-int/2addr v1, v3

    add-int/2addr v1, v0

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v9, " Key"

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v1

    add-int/lit8 v1, v2, 0xc

    add-int/2addr v1, v3

    add-int/2addr v1, v0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v6, " Marks"

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v1

    goto :goto_2

    :cond_2
    return-object v5
.end method

.method public final g([Lcom/ekodroid/omrevaluator/templateui/models/Subject2;)I
    .locals 6

    .line 1
    array-length v0, p1

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v1, v0, :cond_1

    aget-object v3, p1, v1

    invoke-virtual {v3}, Lcom/ekodroid/omrevaluator/templateui/models/Subject2;->getSections()[Lcom/ekodroid/omrevaluator/templateui/models/Section2;

    move-result-object v4

    array-length v4, v4

    const/4 v5, 0x1

    if-le v4, v5, :cond_0

    invoke-virtual {v3}, Lcom/ekodroid/omrevaluator/templateui/models/Subject2;->getSections()[Lcom/ekodroid/omrevaluator/templateui/models/Section2;

    move-result-object v3

    array-length v3, v3

    add-int/2addr v2, v3

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return v2
.end method

.method public final h(II)Ljava/lang/String;
    .locals 3

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-le p2, v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    sub-int v2, p2, v2

    if-ge v0, v2, :cond_0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "0"

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    :cond_1
    return-object p1
.end method

.method public final i(Landroid/content/Context;ILjava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 1
    invoke-static {p1}, Lcom/ekodroid/omrevaluator/database/repositories/ClassRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/ClassRepository;

    move-result-object p1

    invoke-virtual {p1, p2, p3}, Lcom/ekodroid/omrevaluator/database/repositories/ClassRepository;->getStudent(ILjava/lang/String;)Lcom/ekodroid/omrevaluator/database/StudentDataModel;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/database/StudentDataModel;->getStudentName()Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_0
    const-string p1, " "

    return-object p1
.end method

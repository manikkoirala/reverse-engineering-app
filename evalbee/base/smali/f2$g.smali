.class public Lf2$g;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ly01;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lf2;->F(Lcom/ekodroid/omrevaluator/serializable/ExmVersions/ExamDataV1;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Lcom/ekodroid/omrevaluator/serializable/ExmVersions/ExamDataV1;

.field public final synthetic b:Lf2;


# direct methods
.method public constructor <init>(Lf2;Lcom/ekodroid/omrevaluator/serializable/ExmVersions/ExamDataV1;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lf2$g;->b:Lf2;

    iput-object p2, p0, Lf2$g;->a:Lcom/ekodroid/omrevaluator/serializable/ExmVersions/ExamDataV1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Object;)V
    .locals 4

    .line 1
    check-cast p1, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    iget-object v0, p0, Lf2$g;->b:Lf2;

    iget-object v0, v0, Lf2;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "android_id"

    invoke-static {v0, v1}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lf2$g;->b:Lf2;

    iget-object v1, v1, Lf2;->a:Landroid/content/Context;

    invoke-static {v1}, Lo4;->a(Landroid/content/Context;)Lcom/google/firebase/analytics/FirebaseAnalytics;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Q "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lf2$g;->a:Lcom/ekodroid/omrevaluator/serializable/ExmVersions/ExamDataV1;

    invoke-virtual {v3}, Lcom/ekodroid/omrevaluator/serializable/ExmVersions/ExamDataV1;->getSheetTemplate2()Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    move-result-object v3

    invoke-virtual {v3}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getAnswerOptions()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "EXAM_IMPORT"

    invoke-static {v1, v3, v0, v2}, Lo4;->b(Lcom/google/firebase/analytics/FirebaseAnalytics;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lf2$g;->b:Lf2;

    iget-object v0, v0, Lf2;->a:Landroid/content/Context;

    iget-object v1, p0, Lf2$g;->a:Lcom/ekodroid/omrevaluator/serializable/ExmVersions/ExamDataV1;

    invoke-static {v0, p1, v1}, Lxu1;->c(Landroid/content/Context;Lcom/ekodroid/omrevaluator/templateui/models/ExamId;Lcom/ekodroid/omrevaluator/serializable/ExmVersions/ExamDataV1;)Z

    iget-object p1, p0, Lf2$g;->b:Lf2;

    invoke-static {p1}, Lf2;->k(Lf2;)V

    return-void
.end method

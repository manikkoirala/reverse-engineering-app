.class public abstract Loz;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:Loz;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Loz;->a:Loz;

    return-void
.end method

.method public static c(Loz;Leb;Z)V
    .locals 2

    .line 1
    instance-of v0, p0, Lw11;

    if-eqz v0, :cond_0

    check-cast p0, Lw11;

    iget-object v0, p0, Lw11;->b:Loz;

    invoke-static {v0, p1, p2}, Loz;->c(Loz;Leb;Z)V

    iget-object p0, p0, Lw11;->c:Loz;

    invoke-static {p0, p1, p2}, Loz;->c(Loz;Leb;Z)V

    goto :goto_1

    :cond_0
    instance-of v0, p0, Lc32;

    if-eqz v0, :cond_1

    if-eqz p2, :cond_3

    check-cast p0, Lc32;

    iget-object p2, p0, Lyu1;->b:Ljava/lang/String;

    invoke-virtual {p1, p2}, Leb;->b(Ljava/lang/Object;)Z

    move-result p2

    if-nez p2, :cond_3

    iget-object p0, p0, Lyu1;->b:Ljava/lang/String;

    invoke-virtual {p1, p0}, Leb;->a(Ljava/lang/Object;)V

    goto :goto_1

    :cond_1
    instance-of v0, p0, Lz80;

    if-eqz v0, :cond_3

    check-cast p0, Lz80;

    if-nez p2, :cond_2

    iget-object v0, p0, Lyu1;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Leb;->b(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lyu1;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Leb;->a(Ljava/lang/Object;)V

    :cond_2
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0}, Lz80;->p()I

    move-result v1

    if-ge v0, v1, :cond_3

    invoke-virtual {p0, v0}, Lz80;->n(I)Loz;

    move-result-object v1

    invoke-static {v1, p1, p2}, Loz;->c(Loz;Leb;Z)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    :goto_1
    return-void
.end method

.method public static g(Loz;Ljava/lang/StringBuffer;)V
    .locals 4

    .line 1
    instance-of v0, p0, Lw11;

    const-string v1, ")"

    const-string v2, "("

    if-eqz v0, :cond_0

    check-cast p0, Lw11;

    invoke-virtual {p1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget-object v0, p0, Lw11;->b:Loz;

    invoke-static {v0, p1}, Loz;->g(Loz;Ljava/lang/StringBuffer;)V

    invoke-virtual {p0}, Lw11;->h()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    iget-object p0, p0, Lw11;->c:Loz;

    invoke-static {p0, p1}, Loz;->g(Loz;Ljava/lang/StringBuffer;)V

    :goto_0
    invoke-virtual {p1, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_2

    :cond_0
    instance-of v0, p0, Lyu1;

    if-eqz v0, :cond_5

    check-cast p0, Lyu1;

    invoke-virtual {p0}, Lyu1;->i()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string v0, "-"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :cond_1
    invoke-virtual {p0}, Lyu1;->h()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    instance-of v0, p0, Lz80;

    if-eqz v0, :cond_4

    move-object v0, p0

    check-cast v0, Lz80;

    invoke-virtual {p1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-virtual {v0}, Lz80;->p()I

    move-result v2

    if-lez v2, :cond_2

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lz80;->n(I)Loz;

    move-result-object v2

    invoke-static {v2, p1}, Loz;->g(Loz;Ljava/lang/StringBuffer;)V

    :cond_2
    const/4 v2, 0x1

    :goto_1
    invoke-virtual {v0}, Lz80;->p()I

    move-result v3

    if-ge v2, v3, :cond_3

    const-string v3, ", "

    invoke-virtual {p1, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-virtual {v0, v2}, Lz80;->n(I)Loz;

    move-result-object v3

    invoke-static {v3, p1}, Loz;->g(Loz;Ljava/lang/StringBuffer;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_3
    invoke-virtual {p1, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :cond_4
    invoke-virtual {p0}, Lyu1;->i()Z

    move-result p0

    if-eqz p0, :cond_6

    goto :goto_0

    :cond_5
    instance-of v0, p0, Lw22;

    if-eqz v0, :cond_6

    check-cast p0, Lw22;

    iget-wide v0, p0, Lw22;->b:D

    invoke-virtual {p1, v0, v1}, Ljava/lang/StringBuffer;->append(D)Ljava/lang/StringBuffer;

    :cond_6
    :goto_2
    return-void
.end method


# virtual methods
.method public a(Loz;)V
    .locals 1

    .line 1
    if-eqz p1, :cond_2

    iget-object v0, p1, Loz;->a:Loz;

    if-nez v0, :cond_1

    invoke-virtual {p0, p1}, Loz;->f(Loz;)Z

    move-result p1

    if-nez p1, :cond_0

    return-void

    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "cyclic reference"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "expression must be removed parent"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_2
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "expression cannot be null"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public abstract b(Lb32;Ly80;)D
.end method

.method public final d(Z)[Ljava/lang/String;
    .locals 4

    .line 1
    new-instance v0, Leb;

    invoke-direct {v0}, Leb;-><init>()V

    invoke-static {p0, v0, p1}, Loz;->c(Loz;Leb;Z)V

    invoke-virtual {v0}, Leb;->h()I

    move-result p1

    new-array v1, p1, [Ljava/lang/String;

    const/4 v2, 0x0

    :goto_0
    if-ge v2, p1, :cond_0

    invoke-virtual {v0, v2}, Leb;->e(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    aput-object v3, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method public e()[Ljava/lang/String;
    .locals 1

    .line 1
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Loz;->d(Z)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public f(Loz;)Z
    .locals 1

    .line 1
    move-object v0, p0

    :goto_0
    if-eqz v0, :cond_1

    if-ne v0, p1, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_0
    iget-object v0, v0, Loz;->a:Loz;

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    return p1
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .line 1
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    invoke-static {p0, v0}, Loz;->g(Loz;Ljava/lang/StringBuffer;)V

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

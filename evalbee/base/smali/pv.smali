.class public abstract Lpv;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements La5$b;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lpv$p;,
        Lpv$o;,
        Lpv$q;
    }
.end annotation


# static fields
.field public static final m:Lpv$q;

.field public static final n:Lpv$q;

.field public static final o:Lpv$q;

.field public static final p:Lpv$q;

.field public static final q:Lpv$q;

.field public static final r:Lpv$q;

.field public static final s:Lpv$q;

.field public static final t:Lpv$q;

.field public static final u:Lpv$q;

.field public static final v:Lpv$q;

.field public static final w:Lpv$q;

.field public static final x:Lpv$q;

.field public static final y:Lpv$q;

.field public static final z:Lpv$q;


# instance fields
.field public a:F

.field public b:F

.field public c:Z

.field public final d:Ljava/lang/Object;

.field public final e:Lv40;

.field public f:Z

.field public g:F

.field public h:F

.field public i:J

.field public j:F

.field public final k:Ljava/util/ArrayList;

.field public final l:Ljava/util/ArrayList;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lpv$f;

    const-string v1, "translationX"

    invoke-direct {v0, v1}, Lpv$f;-><init>(Ljava/lang/String;)V

    sput-object v0, Lpv;->m:Lpv$q;

    new-instance v0, Lpv$g;

    const-string v1, "translationY"

    invoke-direct {v0, v1}, Lpv$g;-><init>(Ljava/lang/String;)V

    sput-object v0, Lpv;->n:Lpv$q;

    new-instance v0, Lpv$h;

    const-string v1, "translationZ"

    invoke-direct {v0, v1}, Lpv$h;-><init>(Ljava/lang/String;)V

    sput-object v0, Lpv;->o:Lpv$q;

    new-instance v0, Lpv$i;

    const-string v1, "scaleX"

    invoke-direct {v0, v1}, Lpv$i;-><init>(Ljava/lang/String;)V

    sput-object v0, Lpv;->p:Lpv$q;

    new-instance v0, Lpv$j;

    const-string v1, "scaleY"

    invoke-direct {v0, v1}, Lpv$j;-><init>(Ljava/lang/String;)V

    sput-object v0, Lpv;->q:Lpv$q;

    new-instance v0, Lpv$k;

    const-string v1, "rotation"

    invoke-direct {v0, v1}, Lpv$k;-><init>(Ljava/lang/String;)V

    sput-object v0, Lpv;->r:Lpv$q;

    new-instance v0, Lpv$l;

    const-string v1, "rotationX"

    invoke-direct {v0, v1}, Lpv$l;-><init>(Ljava/lang/String;)V

    sput-object v0, Lpv;->s:Lpv$q;

    new-instance v0, Lpv$m;

    const-string v1, "rotationY"

    invoke-direct {v0, v1}, Lpv$m;-><init>(Ljava/lang/String;)V

    sput-object v0, Lpv;->t:Lpv$q;

    new-instance v0, Lpv$n;

    const-string v1, "x"

    invoke-direct {v0, v1}, Lpv$n;-><init>(Ljava/lang/String;)V

    sput-object v0, Lpv;->u:Lpv$q;

    new-instance v0, Lpv$a;

    const-string v1, "y"

    invoke-direct {v0, v1}, Lpv$a;-><init>(Ljava/lang/String;)V

    sput-object v0, Lpv;->v:Lpv$q;

    new-instance v0, Lpv$b;

    const-string v1, "z"

    invoke-direct {v0, v1}, Lpv$b;-><init>(Ljava/lang/String;)V

    sput-object v0, Lpv;->w:Lpv$q;

    new-instance v0, Lpv$c;

    const-string v1, "alpha"

    invoke-direct {v0, v1}, Lpv$c;-><init>(Ljava/lang/String;)V

    sput-object v0, Lpv;->x:Lpv$q;

    new-instance v0, Lpv$d;

    const-string v1, "scrollX"

    invoke-direct {v0, v1}, Lpv$d;-><init>(Ljava/lang/String;)V

    sput-object v0, Lpv;->y:Lpv$q;

    new-instance v0, Lpv$e;

    const-string v1, "scrollY"

    invoke-direct {v0, v1}, Lpv$e;-><init>(Ljava/lang/String;)V

    sput-object v0, Lpv;->z:Lpv$q;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Object;Lv40;)V
    .locals 2

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lpv;->a:F

    const v0, 0x7f7fffff    # Float.MAX_VALUE

    iput v0, p0, Lpv;->b:F

    const/4 v1, 0x0

    iput-boolean v1, p0, Lpv;->c:Z

    iput-boolean v1, p0, Lpv;->f:Z

    iput v0, p0, Lpv;->g:F

    neg-float v0, v0

    iput v0, p0, Lpv;->h:F

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lpv;->i:J

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lpv;->k:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lpv;->l:Ljava/util/ArrayList;

    iput-object p1, p0, Lpv;->d:Ljava/lang/Object;

    iput-object p2, p0, Lpv;->e:Lv40;

    sget-object p1, Lpv;->r:Lpv$q;

    if-eq p2, p1, :cond_4

    sget-object p1, Lpv;->s:Lpv$q;

    if-eq p2, p1, :cond_4

    sget-object p1, Lpv;->t:Lpv$q;

    if-ne p2, p1, :cond_0

    goto :goto_1

    :cond_0
    sget-object p1, Lpv;->x:Lpv$q;

    const/high16 v0, 0x3b800000    # 0.00390625f

    if-ne p2, p1, :cond_2

    :cond_1
    :goto_0
    iput v0, p0, Lpv;->j:F

    goto :goto_3

    :cond_2
    sget-object p1, Lpv;->p:Lpv$q;

    if-eq p2, p1, :cond_1

    sget-object p1, Lpv;->q:Lpv$q;

    if-ne p2, p1, :cond_3

    goto :goto_0

    :cond_3
    const/high16 p1, 0x3f800000    # 1.0f

    goto :goto_2

    :cond_4
    :goto_1
    const p1, 0x3dcccccd    # 0.1f

    :goto_2
    iput p1, p0, Lpv;->j:F

    :goto_3
    return-void
.end method

.method public static h(Ljava/util/ArrayList;Ljava/lang/Object;)V
    .locals 1

    .line 1
    invoke-virtual {p0, p1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result p1

    if-ltz p1, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-void
.end method

.method public static i(Ljava/util/ArrayList;)V
    .locals 2

    .line 1
    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_1

    invoke-virtual {p0, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-virtual {p0, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_1
    return-void
.end method


# virtual methods
.method public a(J)Z
    .locals 4

    .line 1
    iget-wide v0, p0, Lpv;->i:J

    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    const/4 v3, 0x0

    if-nez v2, :cond_0

    iput-wide p1, p0, Lpv;->i:J

    iget p1, p0, Lpv;->b:F

    invoke-virtual {p0, p1}, Lpv;->j(F)V

    return v3

    :cond_0
    sub-long v0, p1, v0

    iput-wide p1, p0, Lpv;->i:J

    invoke-virtual {p0, v0, v1}, Lpv;->n(J)Z

    move-result p1

    iget p2, p0, Lpv;->b:F

    iget v0, p0, Lpv;->g:F

    invoke-static {p2, v0}, Ljava/lang/Math;->min(FF)F

    move-result p2

    iput p2, p0, Lpv;->b:F

    iget v0, p0, Lpv;->h:F

    invoke-static {p2, v0}, Ljava/lang/Math;->max(FF)F

    move-result p2

    iput p2, p0, Lpv;->b:F

    invoke-virtual {p0, p2}, Lpv;->j(F)V

    if-eqz p1, :cond_1

    invoke-virtual {p0, v3}, Lpv;->c(Z)V

    :cond_1
    return p1
.end method

.method public b(Lpv$p;)Lpv;
    .locals 1

    .line 1
    iget-object v0, p0, Lpv;->k:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lpv;->k:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    return-object p0
.end method

.method public final c(Z)V
    .locals 2

    .line 1
    const/4 p1, 0x0

    iput-boolean p1, p0, Lpv;->f:Z

    invoke-static {}, La5;->d()La5;

    move-result-object v0

    invoke-virtual {v0, p0}, La5;->g(La5$b;)V

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lpv;->i:J

    iput-boolean p1, p0, Lpv;->c:Z

    :goto_0
    iget-object v0, p0, Lpv;->k:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge p1, v0, :cond_1

    iget-object v0, p0, Lpv;->k:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lpv;->k:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    invoke-static {p1}, Lzu0;->a(Ljava/lang/Object;)V

    const/4 p1, 0x0

    throw p1

    :cond_1
    iget-object p1, p0, Lpv;->k:Ljava/util/ArrayList;

    invoke-static {p1}, Lpv;->i(Ljava/util/ArrayList;)V

    return-void
.end method

.method public final d()F
    .locals 2

    .line 1
    iget-object v0, p0, Lpv;->e:Lv40;

    iget-object v1, p0, Lpv;->d:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lv40;->getValue(Ljava/lang/Object;)F

    move-result v0

    return v0
.end method

.method public e()F
    .locals 2

    .line 1
    iget v0, p0, Lpv;->j:F

    const/high16 v1, 0x3f400000    # 0.75f

    mul-float/2addr v0, v1

    return v0
.end method

.method public f()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lpv;->f:Z

    return v0
.end method

.method public g(Lpv$p;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lpv;->k:Ljava/util/ArrayList;

    invoke-static {v0, p1}, Lpv;->h(Ljava/util/ArrayList;Ljava/lang/Object;)V

    return-void
.end method

.method public j(F)V
    .locals 2

    .line 1
    iget-object v0, p0, Lpv;->e:Lv40;

    iget-object v1, p0, Lpv;->d:Ljava/lang/Object;

    invoke-virtual {v0, v1, p1}, Lv40;->setValue(Ljava/lang/Object;F)V

    const/4 p1, 0x0

    :goto_0
    iget-object v0, p0, Lpv;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge p1, v0, :cond_1

    iget-object v0, p0, Lpv;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lpv;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    invoke-static {p1}, Lzu0;->a(Ljava/lang/Object;)V

    const/4 p1, 0x0

    throw p1

    :cond_1
    iget-object p1, p0, Lpv;->l:Ljava/util/ArrayList;

    invoke-static {p1}, Lpv;->i(Ljava/util/ArrayList;)V

    return-void
.end method

.method public k(F)Lpv;
    .locals 0

    .line 1
    iput p1, p0, Lpv;->b:F

    const/4 p1, 0x1

    iput-boolean p1, p0, Lpv;->c:Z

    return-object p0
.end method

.method public l()V
    .locals 2

    .line 1
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    if-ne v0, v1, :cond_1

    iget-boolean v0, p0, Lpv;->f:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lpv;->m()V

    :cond_0
    return-void

    :cond_1
    new-instance v0, Landroid/util/AndroidRuntimeException;

    const-string v1, "Animations may only be started on the main thread"

    invoke-direct {v0, v1}, Landroid/util/AndroidRuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final m()V
    .locals 3

    .line 1
    iget-boolean v0, p0, Lpv;->f:Z

    if-nez v0, :cond_2

    const/4 v0, 0x1

    iput-boolean v0, p0, Lpv;->f:Z

    iget-boolean v0, p0, Lpv;->c:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lpv;->d()F

    move-result v0

    iput v0, p0, Lpv;->b:F

    :cond_0
    iget v0, p0, Lpv;->b:F

    iget v1, p0, Lpv;->g:F

    cmpl-float v1, v0, v1

    if-gtz v1, :cond_1

    iget v1, p0, Lpv;->h:F

    cmpg-float v0, v0, v1

    if-ltz v0, :cond_1

    invoke-static {}, La5;->d()La5;

    move-result-object v0

    const-wide/16 v1, 0x0

    invoke-virtual {v0, p0, v1, v2}, La5;->a(La5$b;J)V

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Starting value need to be in between min value and max value"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    :goto_0
    return-void
.end method

.method public abstract n(J)Z
.end method

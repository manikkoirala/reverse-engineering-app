.class public Lui1;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:Ly01;

.field public b:Landroid/content/Context;

.field public c:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ly01;Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lui1;->b:Landroid/content/Context;

    iput-object p2, p0, Lui1;->a:Ly01;

    iput-object p3, p0, Lui1;->c:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    invoke-virtual {p0}, Lui1;->g()V

    return-void
.end method

.method public static synthetic a(Lui1;Ljava/lang/String;)Z
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lui1;->d(Ljava/lang/String;)Z

    move-result p0

    return p0
.end method

.method public static synthetic b(Lui1;Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lui1;->f(Ljava/lang/String;)V

    return-void
.end method

.method public static synthetic c(Lui1;Ljava/lang/String;)Z
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lui1;->e(Ljava/lang/String;)Z

    move-result p0

    return p0
.end method


# virtual methods
.method public final d(Ljava/lang/String;)Z
    .locals 1

    .line 1
    iget-object v0, p0, Lui1;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getLabelProfileJsonModel(Ljava/lang/String;)Lcom/ekodroid/omrevaluator/database/LabelProfileJsonModel;

    move-result-object p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method public final e(Ljava/lang/String;)Z
    .locals 2

    .line 1
    iget-object v0, p0, Lui1;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->deleteLabelProfileJson(Ljava/lang/String;)Z

    iget-object v0, p0, Lui1;->c:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    invoke-virtual {v0, p1}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->setLabelProfileName(Ljava/lang/String;)V

    new-instance v0, Lcom/ekodroid/omrevaluator/database/LabelProfileJsonModel;

    iget-object v1, p0, Lui1;->c:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    invoke-direct {v0, p1, v1}, Lcom/ekodroid/omrevaluator/database/LabelProfileJsonModel;-><init>(Ljava/lang/String;Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;)V

    iget-object p1, p0, Lui1;->b:Landroid/content/Context;

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;

    move-result-object p1

    invoke-virtual {p1, v0}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->saveOrUpdateLabelProfileJson(Lcom/ekodroid/omrevaluator/database/LabelProfileJsonModel;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method public final f(Ljava/lang/String;)V
    .locals 4

    .line 1
    new-instance v0, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    iget-object v1, p0, Lui1;->b:Landroid/content/Context;

    const v2, 0x7f130151

    invoke-direct {v0, v1, v2}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;-><init>(Landroid/content/Context;I)V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lui1;->b:Landroid/content/Context;

    const v3, 0x7f120173

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->setMessage(Ljava/lang/CharSequence;)Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    move-result-object v1

    new-instance v2, Lui1$d;

    invoke-direct {v2, p0, p1}, Lui1$d;-><init>(Lui1;Ljava/lang/String;)V

    const p1, 0x7f120337

    invoke-virtual {v1, p1, v2}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    move-result-object p1

    new-instance v1, Lui1$c;

    invoke-direct {v1, p0}, Lui1$c;-><init>(Lui1;)V

    const v2, 0x7f120241

    invoke-virtual {p1, v2, v1}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    move-result-object p1

    invoke-virtual {p1}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->create()Landroidx/appcompat/app/a;

    invoke-virtual {v0}, Landroidx/appcompat/app/a$a;->show()Landroidx/appcompat/app/a;

    return-void
.end method

.method public final g()V
    .locals 4

    .line 1
    new-instance v0, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    iget-object v1, p0, Lui1;->b:Landroid/content/Context;

    const v2, 0x7f130151

    invoke-direct {v0, v1, v2}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;-><init>(Landroid/content/Context;I)V

    iget-object v1, p0, Lui1;->b:Landroid/content/Context;

    const-string v2, "layout_inflater"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    const v2, 0x7f0c007a

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->setView(Landroid/view/View;)Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    const v2, 0x7f1202ab

    invoke-virtual {v0, v2}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->setTitle(I)Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    const v2, 0x7f090153

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    new-instance v2, Lui1$a;

    invoke-direct {v2, p0, v1}, Lui1$a;-><init>(Lui1;Landroid/widget/EditText;)V

    const v1, 0x7f1202a7

    invoke-virtual {v0, v1, v2}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    new-instance v1, Lui1$b;

    invoke-direct {v1, p0}, Lui1$b;-><init>(Lui1;)V

    const v2, 0x7f120053

    invoke-virtual {v0, v2, v1}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    invoke-virtual {v0}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->create()Landroidx/appcompat/app/a;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    return-void
.end method

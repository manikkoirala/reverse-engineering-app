.class public Lve$a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/hardware/Camera$PreviewCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lve;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Lve;


# direct methods
.method public constructor <init>(Lve;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lve$a;->a:Lve;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreviewFrame([BLandroid/hardware/Camera;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lve$a;->a:Lve;

    invoke-static {v0}, Lve;->c(Lve;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lve$a;->a:Lve;

    invoke-virtual {p2}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v1

    invoke-virtual {v1}, Landroid/hardware/Camera$Parameters;->getPreviewSize()Landroid/hardware/Camera$Size;

    move-result-object v1

    iget v1, v1, Landroid/hardware/Camera$Size;->width:I

    invoke-virtual {p2}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object p2

    invoke-virtual {p2}, Landroid/hardware/Camera$Parameters;->getPreviewSize()Landroid/hardware/Camera$Size;

    move-result-object p2

    iget p2, p2, Landroid/hardware/Camera$Size;->height:I

    invoke-static {v0, v1, p2, p1}, Lve;->o(Lve;II[B)[B

    move-result-object p1

    invoke-static {v0, p1}, Lve;->g(Lve;[B)[B

    iget-object p1, p0, Lve$a;->a:Lve;

    const/4 p2, 0x0

    invoke-static {p1, p2}, Lve;->d(Lve;Z)Z

    :cond_0
    iget-object p1, p0, Lve$a;->a:Lve;

    const/4 p2, 0x1

    iput-boolean p2, p1, Lve;->c:Z

    return-void
.end method

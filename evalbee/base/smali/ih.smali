.class public Lih;
.super Lcom/google/firebase/firestore/Query;
.source "SourceFile"


# direct methods
.method public constructor <init>(Lke1;Lcom/google/firebase/firestore/FirebaseFirestore;)V
    .locals 2

    .line 1
    invoke-static {p1}, Lcom/google/firebase/firestore/core/Query;->b(Lke1;)Lcom/google/firebase/firestore/core/Query;

    move-result-object v0

    invoke-direct {p0, v0, p2}, Lcom/google/firebase/firestore/Query;-><init>(Lcom/google/firebase/firestore/core/Query;Lcom/google/firebase/firestore/FirebaseFirestore;)V

    invoke-virtual {p1}, Ljb;->l()I

    move-result p2

    rem-int/lit8 p2, p2, 0x2

    const/4 v0, 0x1

    if-ne p2, v0, :cond_0

    return-void

    :cond_0
    new-instance p2, Ljava/lang/IllegalArgumentException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Invalid collection reference. Collection references must have an odd number of segments, but "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lke1;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " has "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljb;->l()I

    move-result p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p2
.end method


# virtual methods
.method public g(Ljava/lang/String;)Lcom/google/firebase/firestore/a;
    .locals 1

    .line 1
    const-string v0, "Provided document path must not be null."

    invoke-static {p1, v0}, Lk71;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/firebase/firestore/Query;->a:Lcom/google/firebase/firestore/core/Query;

    invoke-virtual {v0}, Lcom/google/firebase/firestore/core/Query;->l()Lke1;

    move-result-object v0

    invoke-static {p1}, Lke1;->q(Ljava/lang/String;)Lke1;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljb;->a(Ljb;)Ljb;

    move-result-object p1

    check-cast p1, Lke1;

    iget-object v0, p0, Lcom/google/firebase/firestore/Query;->b:Lcom/google/firebase/firestore/FirebaseFirestore;

    invoke-static {p1, v0}, Lcom/google/firebase/firestore/a;->g(Lke1;Lcom/google/firebase/firestore/FirebaseFirestore;)Lcom/google/firebase/firestore/a;

    move-result-object p1

    return-object p1
.end method

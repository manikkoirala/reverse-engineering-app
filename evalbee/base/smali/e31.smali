.class public final Le31;
.super Lwx0;
.source "SourceFile"


# instance fields
.field public final d:La11;

.field public final e:Lq00;


# direct methods
.method public constructor <init>(Ldu;La11;Lq00;Lh71;)V
    .locals 6

    .line 1
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Le31;-><init>(Ldu;La11;Lq00;Lh71;Ljava/util/List;)V

    return-void
.end method

.method public constructor <init>(Ldu;La11;Lq00;Lh71;Ljava/util/List;)V
    .locals 0

    .line 2
    invoke-direct {p0, p1, p4, p5}, Lwx0;-><init>(Ldu;Lh71;Ljava/util/List;)V

    iput-object p2, p0, Le31;->d:La11;

    iput-object p3, p0, Le31;->e:Lq00;

    return-void
.end method


# virtual methods
.method public a(Lcom/google/firebase/firestore/model/MutableDocument;Lq00;Lpw1;)Lq00;
    .locals 2

    .line 1
    invoke-virtual {p0, p1}, Lwx0;->n(Lcom/google/firebase/firestore/model/MutableDocument;)V

    invoke-virtual {p0}, Lwx0;->h()Lh71;

    move-result-object v0

    invoke-virtual {v0, p1}, Lh71;->e(Lcom/google/firebase/firestore/model/MutableDocument;)Z

    move-result v0

    if-nez v0, :cond_0

    return-object p2

    :cond_0
    invoke-virtual {p0, p3, p1}, Lwx0;->l(Lpw1;Lcom/google/firebase/firestore/model/MutableDocument;)Ljava/util/Map;

    move-result-object p3

    invoke-virtual {p0}, Le31;->p()Ljava/util/Map;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/firebase/firestore/model/MutableDocument;->getData()La11;

    move-result-object v1

    invoke-virtual {v1, v0}, La11;->n(Ljava/util/Map;)V

    invoke-virtual {v1, p3}, La11;->n(Ljava/util/Map;)V

    invoke-virtual {p1}, Lcom/google/firebase/firestore/model/MutableDocument;->getVersion()Lqo1;

    move-result-object p3

    invoke-virtual {p1}, Lcom/google/firebase/firestore/model/MutableDocument;->getData()La11;

    move-result-object v0

    invoke-virtual {p1, p3, v0}, Lcom/google/firebase/firestore/model/MutableDocument;->l(Lqo1;La11;)Lcom/google/firebase/firestore/model/MutableDocument;

    move-result-object p1

    invoke-virtual {p1}, Lcom/google/firebase/firestore/model/MutableDocument;->u()Lcom/google/firebase/firestore/model/MutableDocument;

    if-nez p2, :cond_1

    const/4 p1, 0x0

    return-object p1

    :cond_1
    new-instance p1, Ljava/util/HashSet;

    invoke-virtual {p2}, Lq00;->c()Ljava/util/Set;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    iget-object p2, p0, Le31;->e:Lq00;

    invoke-virtual {p2}, Lq00;->c()Ljava/util/Set;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/util/AbstractCollection;->addAll(Ljava/util/Collection;)Z

    invoke-virtual {p0}, Le31;->o()Ljava/util/List;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/util/AbstractCollection;->addAll(Ljava/util/Collection;)Z

    invoke-static {p1}, Lq00;->b(Ljava/util/Set;)Lq00;

    move-result-object p1

    return-object p1
.end method

.method public b(Lcom/google/firebase/firestore/model/MutableDocument;Lay0;)V
    .locals 3

    .line 1
    invoke-virtual {p0, p1}, Lwx0;->n(Lcom/google/firebase/firestore/model/MutableDocument;)V

    invoke-virtual {p0}, Lwx0;->h()Lh71;

    move-result-object v0

    invoke-virtual {v0, p1}, Lh71;->e(Lcom/google/firebase/firestore/model/MutableDocument;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p2}, Lay0;->b()Lqo1;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/google/firebase/firestore/model/MutableDocument;->n(Lqo1;)Lcom/google/firebase/firestore/model/MutableDocument;

    return-void

    :cond_0
    invoke-virtual {p2}, Lay0;->a()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lwx0;->m(Lcom/google/firebase/firestore/model/MutableDocument;Ljava/util/List;)Ljava/util/Map;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/firebase/firestore/model/MutableDocument;->getData()La11;

    move-result-object v1

    invoke-virtual {p0}, Le31;->p()Ljava/util/Map;

    move-result-object v2

    invoke-virtual {v1, v2}, La11;->n(Ljava/util/Map;)V

    invoke-virtual {v1, v0}, La11;->n(Ljava/util/Map;)V

    invoke-virtual {p2}, Lay0;->b()Lqo1;

    move-result-object p2

    invoke-virtual {p1}, Lcom/google/firebase/firestore/model/MutableDocument;->getData()La11;

    move-result-object v0

    invoke-virtual {p1, p2, v0}, Lcom/google/firebase/firestore/model/MutableDocument;->l(Lqo1;La11;)Lcom/google/firebase/firestore/model/MutableDocument;

    move-result-object p1

    invoke-virtual {p1}, Lcom/google/firebase/firestore/model/MutableDocument;->t()Lcom/google/firebase/firestore/model/MutableDocument;

    return-void
.end method

.method public e()Lq00;
    .locals 1

    .line 1
    iget-object v0, p0, Le31;->e:Lq00;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .line 1
    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x0

    if-eqz p1, :cond_3

    const-class v2, Le31;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_1

    goto :goto_1

    :cond_1
    check-cast p1, Le31;

    invoke-virtual {p0, p1}, Lwx0;->i(Lwx0;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Le31;->d:La11;

    iget-object v3, p1, Le31;->d:La11;

    invoke-virtual {v2, v3}, La11;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {p0}, Lwx0;->f()Ljava/util/List;

    move-result-object v2

    invoke-virtual {p1}, Lwx0;->f()Ljava/util/List;

    move-result-object p1

    invoke-interface {v2, p1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    move v0, v1

    :goto_0
    return v0

    :cond_3
    :goto_1
    return v1
.end method

.method public hashCode()I
    .locals 2

    .line 1
    invoke-virtual {p0}, Lwx0;->j()I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Le31;->d:La11;

    invoke-virtual {v1}, La11;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public final o()Ljava/util/List;
    .locals 3

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0}, Lwx0;->f()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lu00;

    invoke-virtual {v2}, Lu00;->a()Ls00;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method public final p()Ljava/util/Map;
    .locals 4

    .line 1
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iget-object v1, p0, Le31;->e:Lq00;

    invoke-virtual {v1}, Lq00;->c()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ls00;

    invoke-virtual {v2}, Ljb;->j()Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Le31;->d:La11;

    invoke-virtual {v3, v2}, La11;->j(Ls00;)Lcom/google/firestore/v1/Value;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method public q()La11;
    .locals 1

    .line 1
    iget-object v0, p0, Le31;->d:La11;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "PatchMutation{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lwx0;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", mask="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Le31;->e:Lq00;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", value="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Le31;->d:La11;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.class public final Lth0;
.super Lvh0;
.source "SourceFile"


# static fields
.field public static final q:Ljava/io/Writer;

.field public static final t:Lqh0;


# instance fields
.field public final m:Ljava/util/List;

.field public n:Ljava/lang/String;

.field public p:Lnh0;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lth0$a;

    invoke-direct {v0}, Lth0$a;-><init>()V

    sput-object v0, Lth0;->q:Ljava/io/Writer;

    new-instance v0, Lqh0;

    const-string v1, "closed"

    invoke-direct {v0, v1}, Lqh0;-><init>(Ljava/lang/String;)V

    sput-object v0, Lth0;->t:Lqh0;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 1
    sget-object v0, Lth0;->q:Ljava/io/Writer;

    invoke-direct {p0, v0}, Lvh0;-><init>(Ljava/io/Writer;)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lth0;->m:Ljava/util/List;

    sget-object v0, Loh0;->a:Loh0;

    iput-object v0, p0, Lth0;->p:Lnh0;

    return-void
.end method


# virtual methods
.method public c()Lvh0;
    .locals 2

    .line 1
    new-instance v0, Lih0;

    invoke-direct {v0}, Lih0;-><init>()V

    invoke-virtual {p0, v0}, Lth0;->w0(Lnh0;)V

    iget-object v1, p0, Lth0;->m:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public close()V
    .locals 2

    .line 1
    iget-object v0, p0, Lth0;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lth0;->m:Ljava/util/List;

    sget-object v1, Lth0;->t:Lqh0;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void

    :cond_0
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Incomplete document"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public d()Lvh0;
    .locals 2

    .line 1
    new-instance v0, Lph0;

    invoke-direct {v0}, Lph0;-><init>()V

    invoke-virtual {p0, v0}, Lth0;->w0(Lnh0;)V

    iget-object v1, p0, Lth0;->m:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public f()Lvh0;
    .locals 2

    .line 1
    iget-object v0, p0, Lth0;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lth0;->n:Ljava/lang/String;

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lth0;->v0()Lnh0;

    move-result-object v0

    instance-of v0, v0, Lih0;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lth0;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    return-object p0

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0
.end method

.method public flush()V
    .locals 0

    .line 1
    return-void
.end method

.method public g()Lvh0;
    .locals 2

    .line 1
    iget-object v0, p0, Lth0;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lth0;->n:Ljava/lang/String;

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lth0;->v0()Lnh0;

    move-result-object v0

    instance-of v0, v0, Lph0;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lth0;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    return-object p0

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0
.end method

.method public m0(D)Lvh0;
    .locals 3

    .line 1
    invoke-virtual {p0}, Lvh0;->j()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {p1, p2}, Ljava/lang/Double;->isNaN(D)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p1, p2}, Ljava/lang/Double;->isInfinite(D)Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "JSON forbids NaN and infinities: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    :goto_0
    new-instance v0, Lqh0;

    invoke-static {p1, p2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object p1

    invoke-direct {v0, p1}, Lqh0;-><init>(Ljava/lang/Number;)V

    invoke-virtual {p0, v0}, Lth0;->w0(Lnh0;)V

    return-object p0
.end method

.method public o(Ljava/lang/String;)Lvh0;
    .locals 1

    .line 1
    const-string v0, "name == null"

    invoke-static {p1, v0}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    iget-object v0, p0, Lth0;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lth0;->n:Ljava/lang/String;

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lth0;->v0()Lnh0;

    move-result-object v0

    instance-of v0, v0, Lph0;

    if-eqz v0, :cond_0

    iput-object p1, p0, Lth0;->n:Ljava/lang/String;

    return-object p0

    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    invoke-direct {p1}, Ljava/lang/IllegalStateException;-><init>()V

    throw p1

    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    invoke-direct {p1}, Ljava/lang/IllegalStateException;-><init>()V

    throw p1
.end method

.method public o0(J)Lvh0;
    .locals 1

    .line 1
    new-instance v0, Lqh0;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-direct {v0, p1}, Lqh0;-><init>(Ljava/lang/Number;)V

    invoke-virtual {p0, v0}, Lth0;->w0(Lnh0;)V

    return-object p0
.end method

.method public p0(Ljava/lang/Boolean;)Lvh0;
    .locals 1

    .line 1
    if-nez p1, :cond_0

    invoke-virtual {p0}, Lth0;->u()Lvh0;

    move-result-object p1

    return-object p1

    :cond_0
    new-instance v0, Lqh0;

    invoke-direct {v0, p1}, Lqh0;-><init>(Ljava/lang/Boolean;)V

    invoke-virtual {p0, v0}, Lth0;->w0(Lnh0;)V

    return-object p0
.end method

.method public q0(Ljava/lang/Number;)Lvh0;
    .locals 3

    .line 1
    if-nez p1, :cond_0

    invoke-virtual {p0}, Lth0;->u()Lvh0;

    move-result-object p1

    return-object p1

    :cond_0
    invoke-virtual {p0}, Lvh0;->j()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p1}, Ljava/lang/Number;->doubleValue()D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->isNaN(D)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-static {v0, v1}, Ljava/lang/Double;->isInfinite(D)Z

    move-result v0

    if-nez v0, :cond_1

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "JSON forbids NaN and infinities: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    :goto_0
    new-instance v0, Lqh0;

    invoke-direct {v0, p1}, Lqh0;-><init>(Ljava/lang/Number;)V

    invoke-virtual {p0, v0}, Lth0;->w0(Lnh0;)V

    return-object p0
.end method

.method public r0(Ljava/lang/String;)Lvh0;
    .locals 1

    .line 1
    if-nez p1, :cond_0

    invoke-virtual {p0}, Lth0;->u()Lvh0;

    move-result-object p1

    return-object p1

    :cond_0
    new-instance v0, Lqh0;

    invoke-direct {v0, p1}, Lqh0;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lth0;->w0(Lnh0;)V

    return-object p0
.end method

.method public s0(Z)Lvh0;
    .locals 1

    .line 1
    new-instance v0, Lqh0;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-direct {v0, p1}, Lqh0;-><init>(Ljava/lang/Boolean;)V

    invoke-virtual {p0, v0}, Lth0;->w0(Lnh0;)V

    return-object p0
.end method

.method public u()Lvh0;
    .locals 1

    .line 1
    sget-object v0, Loh0;->a:Loh0;

    invoke-virtual {p0, v0}, Lth0;->w0(Lnh0;)V

    return-object p0
.end method

.method public u0()Lnh0;
    .locals 3

    .line 1
    iget-object v0, p0, Lth0;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lth0;->p:Lnh0;

    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expected one JSON element but was "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lth0;->m:Ljava/util/List;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final v0()Lnh0;
    .locals 2

    .line 1
    iget-object v0, p0, Lth0;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnh0;

    return-object v0
.end method

.method public final w0(Lnh0;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lth0;->n:Ljava/lang/String;

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lnh0;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lvh0;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    invoke-virtual {p0}, Lth0;->v0()Lnh0;

    move-result-object v0

    check-cast v0, Lph0;

    iget-object v1, p0, Lth0;->n:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Lph0;->n(Ljava/lang/String;Lnh0;)V

    :cond_1
    const/4 p1, 0x0

    iput-object p1, p0, Lth0;->n:Ljava/lang/String;

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lth0;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    iput-object p1, p0, Lth0;->p:Lnh0;

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lth0;->v0()Lnh0;

    move-result-object v0

    instance-of v1, v0, Lih0;

    if-eqz v1, :cond_4

    check-cast v0, Lih0;

    invoke-virtual {v0, p1}, Lih0;->n(Lnh0;)V

    :goto_0
    return-void

    :cond_4
    new-instance p1, Ljava/lang/IllegalStateException;

    invoke-direct {p1}, Ljava/lang/IllegalStateException;-><init>()V

    throw p1
.end method

.class public Lvx;
.super Lu80;
.source "SourceFile"


# instance fields
.field public a:Landroid/content/Context;

.field public b:Ly01;

.field public c:Lcom/ekodroid/omrevaluator/serializable/SharedType;

.field public d:Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/SharedExamDetailsDto;

.field public e:Landroid/widget/CheckBox;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/SharedExamDetailsDto;Ly01;)V
    .locals 1

    .line 1
    invoke-direct {p0, p1}, Lu80;-><init>(Landroid/content/Context;)V

    sget-object v0, Lcom/ekodroid/omrevaluator/serializable/SharedType;->PRIVATE:Lcom/ekodroid/omrevaluator/serializable/SharedType;

    iput-object v0, p0, Lvx;->c:Lcom/ekodroid/omrevaluator/serializable/SharedType;

    iput-object p1, p0, Lvx;->a:Landroid/content/Context;

    iput-object p3, p0, Lvx;->b:Ly01;

    iput-object p2, p0, Lvx;->d:Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/SharedExamDetailsDto;

    return-void
.end method

.method public static synthetic a(Lvx;)Ly01;
    .locals 0

    .line 1
    iget-object p0, p0, Lvx;->b:Ly01;

    return-object p0
.end method

.method public static synthetic b(Lvx;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lvx;->h()V

    return-void
.end method

.method public static synthetic c(Lvx;)Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/SharedExamDetailsDto;
    .locals 0

    .line 1
    iget-object p0, p0, Lvx;->d:Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/SharedExamDetailsDto;

    return-object p0
.end method

.method public static synthetic d(Lvx;)Lcom/ekodroid/omrevaluator/serializable/SharedType;
    .locals 0

    .line 1
    iget-object p0, p0, Lvx;->c:Lcom/ekodroid/omrevaluator/serializable/SharedType;

    return-object p0
.end method

.method public static synthetic e(Lvx;Lcom/ekodroid/omrevaluator/serializable/SharedType;)Lcom/ekodroid/omrevaluator/serializable/SharedType;
    .locals 0

    .line 1
    iput-object p1, p0, Lvx;->c:Lcom/ekodroid/omrevaluator/serializable/SharedType;

    return-object p1
.end method


# virtual methods
.method public final f(Lcom/ekodroid/omrevaluator/serializable/SharedType;)I
    .locals 2

    .line 1
    sget-object v0, Lvx$g;->a:[I

    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x2

    if-eq p1, v0, :cond_1

    const/4 v1, 0x3

    if-eq p1, v1, :cond_0

    const/4 p1, 0x0

    return p1

    :cond_0
    return v0

    :cond_1
    const/4 p1, 0x1

    return p1
.end method

.method public final g()V
    .locals 2

    .line 1
    const v0, 0x7f09043f

    invoke-virtual {p0, v0}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/appcompat/widget/Toolbar;

    new-instance v1, Lvx$a;

    invoke-direct {v1, p0}, Lvx$a;-><init>(Lvx;)V

    invoke-virtual {v0, v1}, Landroidx/appcompat/widget/Toolbar;->setNavigationOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final h()V
    .locals 10

    .line 1
    iget-object v0, p0, Lvx;->e:Landroid/widget/CheckBox;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/CompoundButton;->setChecked(Z)V

    new-instance v3, Lvx$f;

    invoke-direct {v3, p0}, Lvx$f;-><init>(Lvx;)V

    iget-object v2, p0, Lvx;->a:Landroid/content/Context;

    const v4, 0x7f1200c8

    const v5, 0x7f12019d

    const v6, 0x7f120326

    const v7, 0x7f120053

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-static/range {v2 .. v9}, Lxs;->c(Landroid/content/Context;Ly01;IIIIII)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 12

    .line 1
    invoke-super {p0, p1}, Lu80;->onCreate(Landroid/os/Bundle;)V

    const/4 p1, 0x0

    invoke-virtual {p0, p1}, Landroid/app/Dialog;->setCancelable(Z)V

    const v0, 0x7f0c006a

    invoke-virtual {p0, v0}, Landroid/app/Dialog;->setContentView(I)V

    invoke-virtual {p0}, Lvx;->g()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget-object v1, p0, Lvx;->a:Landroid/content/Context;

    invoke-static {v1}, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;->getTeacherList()Ljava/util/ArrayList;

    move-result-object v0

    :cond_0
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    invoke-static {}, Lcom/google/firebase/auth/FirebaseAuth;->getInstance()Lcom/google/firebase/auth/FirebaseAuth;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/firebase/auth/FirebaseAuth;->e()Lr30;

    move-result-object v1

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/ekodroid/omrevaluator/more/models/Teacher;

    invoke-virtual {v1}, Lr30;->O()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/more/models/Teacher;->getUserId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    new-instance v3, Lsu1;

    invoke-direct {v3, v2, p1}, Lsu1;-><init>(Lcom/ekodroid/omrevaluator/more/models/Teacher;Z)V

    invoke-virtual {v7, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    const v0, 0x7f0903e5

    invoke-virtual {p0, v0}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f090331

    invoke-virtual {p0, v1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    move-object v8, v1

    check-cast v8, Landroid/widget/Spinner;

    new-instance v1, Landroid/widget/ArrayAdapter;

    iget-object v2, p0, Lvx;->a:Landroid/content/Context;

    const v3, 0x7f0c00f0

    sget-object v4, Lgr1;->f:[Ljava/lang/String;

    invoke-direct {v1, v2, v3, v4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    invoke-virtual {v8, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    const v1, 0x7f09022b

    invoke-virtual {p0, v1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    move-object v9, v1

    check-cast v9, Landroid/widget/LinearLayout;

    const v1, 0x7f09024f

    invoke-virtual {p0, v1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    const v2, 0x7f090401

    invoke-virtual {p0, v2}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v2

    move-object v10, v2

    check-cast v10, Landroid/widget/TextView;

    const v2, 0x7f0900fe

    invoke-virtual {p0, v2}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckBox;

    iput-object v2, p0, Lvx;->e:Landroid/widget/CheckBox;

    iget-object v3, p0, Lvx;->d:Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/SharedExamDetailsDto;

    if-eqz v3, :cond_6

    iget-boolean v3, v3, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/SharedExamDetailsDto;->syncImages:Z

    invoke-virtual {v2, v3}, Landroid/widget/CompoundButton;->setChecked(Z)V

    iget-object v2, p0, Lvx;->d:Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/SharedExamDetailsDto;

    iget-boolean v2, v2, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/SharedExamDetailsDto;->syncImages:Z

    if-eqz v2, :cond_3

    iget-object v2, p0, Lvx;->e:Landroid/widget/CheckBox;

    invoke-virtual {v2, p1}, Landroid/view/View;->setEnabled(Z)V

    :cond_3
    iget-object p1, p0, Lvx;->d:Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/SharedExamDetailsDto;

    iget-object v2, p1, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/SharedExamDetailsDto;->sharedType:Lcom/ekodroid/omrevaluator/serializable/SharedType;

    iput-object v2, p0, Lvx;->c:Lcom/ekodroid/omrevaluator/serializable/SharedType;

    sget-object v3, Lcom/ekodroid/omrevaluator/serializable/SharedType;->SHARED:Lcom/ekodroid/omrevaluator/serializable/SharedType;

    if-ne v2, v3, :cond_6

    iget-object p1, p1, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/SharedExamDetailsDto;->teachersUid:Ljava/util/ArrayList;

    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_4
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v7}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_5
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lsu1;

    iget-object v5, v4, Lsu1;->a:Lcom/ekodroid/omrevaluator/more/models/Teacher;

    if-eqz v5, :cond_5

    invoke-virtual {v5}, Lcom/ekodroid/omrevaluator/more/models/Teacher;->getUserId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    const/4 v5, 0x1

    iput-boolean v5, v4, Lsu1;->b:Z

    goto :goto_1

    :cond_6
    const/16 p1, 0x8

    invoke-virtual {v10, p1}, Landroid/view/View;->setVisibility(I)V

    iget-object p1, p0, Lvx;->c:Lcom/ekodroid/omrevaluator/serializable/SharedType;

    invoke-virtual {p0, p1}, Lvx;->f(Lcom/ekodroid/omrevaluator/serializable/SharedType;)I

    move-result p1

    invoke-virtual {v8, p1}, Landroid/widget/AdapterView;->setSelection(I)V

    new-instance p1, Lc3;

    iget-object v2, p0, Lvx;->a:Landroid/content/Context;

    invoke-direct {p1, v7, v2}, Lc3;-><init>(Ljava/util/ArrayList;Landroid/content/Context;)V

    invoke-virtual {v1, p1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    const p1, 0x7f090090

    invoke-virtual {p0, p1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/Button;

    const v1, 0x7f090084

    invoke-virtual {p0, v1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    new-instance v2, Lvx$b;

    invoke-direct {v2, p0}, Lvx$b;-><init>(Lvx;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lvx;->e:Landroid/widget/CheckBox;

    new-instance v2, Lvx$c;

    invoke-direct {v2, p0}, Lvx$c;-><init>(Lvx;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v11, Lvx$d;

    move-object v1, v11

    move-object v2, p0

    move-object v3, p1

    move-object v4, v7

    move-object v5, v8

    move-object v6, v10

    invoke-direct/range {v1 .. v6}, Lvx$d;-><init>(Lvx;Landroid/widget/Button;Ljava/util/ArrayList;Landroid/widget/Spinner;Landroid/widget/TextView;)V

    invoke-virtual {p1, v11}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance p1, Lvx$e;

    move-object v1, p1

    move-object v3, v10

    move-object v4, v8

    move-object v5, v0

    move-object v6, v9

    invoke-direct/range {v1 .. v7}, Lvx$e;-><init>(Lvx;Landroid/widget/TextView;Landroid/widget/Spinner;Landroid/widget/TextView;Landroid/widget/LinearLayout;Ljava/util/ArrayList;)V

    invoke-virtual {v8, p1}, Landroid/widget/AdapterView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    return-void
.end method

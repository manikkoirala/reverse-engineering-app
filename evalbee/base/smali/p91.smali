.class public Lp91;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ly22;


# instance fields
.field public a:Z

.field public b:Z

.field public c:Ln00;

.field public final d:Lcom/google/firebase/encoders/proto/b;


# direct methods
.method public constructor <init>(Lcom/google/firebase/encoders/proto/b;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lp91;->a:Z

    iput-boolean v0, p0, Lp91;->b:Z

    iput-object p1, p0, Lp91;->d:Lcom/google/firebase/encoders/proto/b;

    return-void
.end method


# virtual methods
.method public a(Z)Ly22;
    .locals 3

    .line 1
    invoke-virtual {p0}, Lp91;->b()V

    iget-object v0, p0, Lp91;->d:Lcom/google/firebase/encoders/proto/b;

    iget-object v1, p0, Lp91;->c:Ln00;

    iget-boolean v2, p0, Lp91;->b:Z

    invoke-virtual {v0, v1, p1, v2}, Lcom/google/firebase/encoders/proto/b;->o(Ln00;ZZ)Lcom/google/firebase/encoders/proto/b;

    return-object p0
.end method

.method public add(Ljava/lang/String;)Ly22;
    .locals 3

    .line 1
    invoke-virtual {p0}, Lp91;->b()V

    iget-object v0, p0, Lp91;->d:Lcom/google/firebase/encoders/proto/b;

    iget-object v1, p0, Lp91;->c:Ln00;

    iget-boolean v2, p0, Lp91;->b:Z

    invoke-virtual {v0, v1, p1, v2}, Lcom/google/firebase/encoders/proto/b;->i(Ln00;Ljava/lang/Object;Z)Lx01;

    return-object p0
.end method

.method public final b()V
    .locals 2

    .line 1
    iget-boolean v0, p0, Lp91;->a:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lp91;->a:Z

    return-void

    :cond_0
    new-instance v0, Lcom/google/firebase/encoders/EncodingException;

    const-string v1, "Cannot encode a second value in the ValueEncoderContext"

    invoke-direct {v0, v1}, Lcom/google/firebase/encoders/EncodingException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public c(Ln00;Z)V
    .locals 1

    .line 1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lp91;->a:Z

    iput-object p1, p0, Lp91;->c:Ln00;

    iput-boolean p2, p0, Lp91;->b:Z

    return-void
.end method

.class public final Lgn;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ldn;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lgn$b;
    }
.end annotation


# static fields
.field public static final c:Lgy0;


# instance fields
.field public final a:Ljr;

.field public final b:Ljava/util/concurrent/atomic/AtomicReference;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lgn$b;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lgn$b;-><init>(Lgn$a;)V

    sput-object v0, Lgn;->c:Lgy0;

    return-void
.end method

.method public constructor <init>(Ljr;)V
    .locals 2

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lgn;->b:Ljava/util/concurrent/atomic/AtomicReference;

    iput-object p1, p0, Lgn;->a:Ljr;

    new-instance v0, Len;

    invoke-direct {v0, p0}, Len;-><init>(Lgn;)V

    invoke-interface {p1, v0}, Ljr;->a(Ljr$a;)V

    return-void
.end method

.method public static synthetic e(Lgn;Lr91;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lgn;->g(Lr91;)V

    return-void
.end method

.method public static synthetic f(Ljava/lang/String;Ljava/lang/String;JLzp1;Lr91;)V
    .locals 0

    .line 1
    invoke-static/range {p0 .. p5}, Lgn;->h(Ljava/lang/String;Ljava/lang/String;JLzp1;Lr91;)V

    return-void
.end method

.method private synthetic g(Lr91;)V
    .locals 2

    .line 1
    invoke-static {}, Lzl0;->f()Lzl0;

    move-result-object v0

    const-string v1, "Crashlytics native component now available."

    invoke-virtual {v0, v1}, Lzl0;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lgn;->b:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-interface {p1}, Lr91;->get()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ldn;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public static synthetic h(Ljava/lang/String;Ljava/lang/String;JLzp1;Lr91;)V
    .locals 6

    .line 1
    invoke-interface {p5}, Lr91;->get()Ljava/lang/Object;

    move-result-object p5

    move-object v0, p5

    check-cast v0, Ldn;

    move-object v1, p0

    move-object v2, p1

    move-wide v3, p2

    move-object v5, p4

    invoke-interface/range {v0 .. v5}, Ldn;->a(Ljava/lang/String;Ljava/lang/String;JLzp1;)V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;Ljava/lang/String;JLzp1;)V
    .locals 8

    .line 1
    invoke-static {}, Lzl0;->f()Lzl0;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Deferring native open session: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lzl0;->i(Ljava/lang/String;)V

    iget-object v0, p0, Lgn;->a:Ljr;

    new-instance v7, Lfn;

    move-object v1, v7

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    move-object v6, p5

    invoke-direct/range {v1 .. v6}, Lfn;-><init>(Ljava/lang/String;Ljava/lang/String;JLzp1;)V

    invoke-interface {v0, v7}, Ljr;->a(Ljr$a;)V

    return-void
.end method

.method public b(Ljava/lang/String;)Lgy0;
    .locals 1

    .line 1
    iget-object v0, p0, Lgn;->b:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldn;

    if-nez v0, :cond_0

    sget-object p1, Lgn;->c:Lgy0;

    goto :goto_0

    :cond_0
    invoke-interface {v0, p1}, Ldn;->b(Ljava/lang/String;)Lgy0;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public c()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lgn;->b:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldn;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ldn;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public d(Ljava/lang/String;)Z
    .locals 1

    .line 1
    iget-object v0, p0, Lgn;->b:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldn;

    if-eqz v0, :cond_0

    invoke-interface {v0, p1}, Ldn;->d(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

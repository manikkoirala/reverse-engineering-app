.class public Lvi1$a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lvi1;->g()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Landroid/widget/EditText;

.field public final synthetic b:Lvi1;


# direct methods
.method public constructor <init>(Lvi1;Landroid/widget/EditText;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lvi1$a;->b:Lvi1;

    iput-object p2, p0, Lvi1$a;->a:Landroid/widget/EditText;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 10

    .line 1
    iget-object p2, p0, Lvi1$a;->a:Landroid/widget/EditText;

    invoke-virtual {p2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object p2

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object p1, p0, Lvi1$a;->b:Lvi1;

    iget-object p1, p1, Lvi1;->b:Landroid/content/Context;

    const p2, 0x7f0800bd

    const v0, 0x7f08016e

    const v1, 0x7f1200c2

    invoke-static {p1, v1, p2, v0}, La91;->G(Landroid/content/Context;III)V

    return-void

    :cond_0
    invoke-virtual {p2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    const-string v2, "DEFAULT"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object p1, p0, Lvi1$a;->b:Lvi1;

    iget-object v2, p1, Lvi1;->b:Landroid/content/Context;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const v5, 0x7f120094

    const v6, 0x7f120257

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-static/range {v2 .. v9}, Lxs;->c(Landroid/content/Context;Ly01;IIIIII)V

    iget-object p1, p0, Lvi1$a;->a:Landroid/widget/EditText;

    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void

    :cond_1
    iget-object v0, p0, Lvi1$a;->b:Lvi1;

    invoke-static {v0, p2}, Lvi1;->a(Lvi1;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lvi1$a;->b:Lvi1;

    invoke-static {v0, p2}, Lvi1;->b(Lvi1;Ljava/lang/String;)V

    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    return-void

    :cond_2
    iget-object v0, p0, Lvi1$a;->b:Lvi1;

    invoke-static {v0, p2}, Lvi1;->c(Lvi1;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lvi1$a;->b:Lvi1;

    iget-object v0, v0, Lvi1;->b:Landroid/content/Context;

    const v1, 0x7f0800cf

    const v2, 0x7f08016d

    const v3, 0x7f1202ad

    invoke-static {v0, v3, v1, v2}, La91;->G(Landroid/content/Context;III)V

    iget-object v0, p0, Lvi1$a;->b:Lvi1;

    iget-object v0, v0, Lvi1;->a:Ly01;

    invoke-interface {v0, p2}, Ly01;->a(Ljava/lang/Object;)V

    :cond_3
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    return-void
.end method

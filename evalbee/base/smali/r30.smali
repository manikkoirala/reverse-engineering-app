.class public abstract Lr30;
.super Lcom/google/android/gms/common/internal/safeparcel/AbstractSafeParcelable;
.source "SourceFile"

# interfaces
.implements Lc22;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/common/internal/safeparcel/AbstractSafeParcelable;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract E()Ls30;
.end method

.method public abstract H()Lex0;
.end method

.method public abstract J()Ljava/util/List;
.end method

.method public abstract K()Ljava/lang/String;
.end method

.method public abstract O()Ljava/lang/String;
.end method

.method public abstract R()Z
.end method

.method public Z(Lg22;)Lcom/google/android/gms/tasks/Task;
    .locals 1

    .line 1
    invoke-static {p1}, Lcom/google/android/gms/common/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0}, Lr30;->i0()Lr10;

    move-result-object v0

    invoke-static {v0}, Lcom/google/firebase/auth/FirebaseAuth;->getInstance(Lr10;)Lcom/google/firebase/auth/FirebaseAuth;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/google/firebase/auth/FirebaseAuth;->r(Lr30;Lg22;)Lcom/google/android/gms/tasks/Task;

    move-result-object p1

    return-object p1
.end method

.method public abstract getDisplayName()Ljava/lang/String;
.end method

.method public abstract getEmail()Ljava/lang/String;
.end method

.method public i(Z)Lcom/google/android/gms/tasks/Task;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lr30;->i0()Lr10;

    move-result-object v0

    invoke-static {v0}, Lcom/google/firebase/auth/FirebaseAuth;->getInstance(Lr10;)Lcom/google/firebase/auth/FirebaseAuth;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Lcom/google/firebase/auth/FirebaseAuth;->s(Lr30;Z)Lcom/google/android/gms/tasks/Task;

    move-result-object p1

    return-object p1
.end method

.method public abstract i0()Lr10;
.end method

.method public abstract m0(Ljava/util/List;)Lr30;
.end method

.method public abstract o0(Lcom/google/android/gms/internal/firebase-auth-api/zzafn;)V
.end method

.method public abstract p0()Lr30;
.end method

.method public abstract q0(Ljava/util/List;)V
.end method

.method public abstract r0()Lcom/google/android/gms/internal/firebase-auth-api/zzafn;
.end method

.method public abstract s0()Ljava/util/List;
.end method

.method public abstract zzd()Ljava/lang/String;
.end method

.method public abstract zze()Ljava/lang/String;
.end method

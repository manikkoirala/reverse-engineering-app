.class public Lfq1;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lfq1$b;,
        Lfq1$c;
    }
.end annotation


# instance fields
.field public a:Ljava/lang/String;

.field public b:Lo30;

.field public c:Ljq1;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Lfq1$c;

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/String;

.field public i:Ljava/lang/String;

.field public j:J

.field public k:Ljava/lang/String;

.field public l:Lfq1$c;

.field public m:Lfq1$c;

.field public n:Lfq1$c;

.field public o:Lfq1$c;

.field public p:Lfq1$c;


# direct methods
.method public constructor <init>()V
    .locals 3

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lfq1;->a:Ljava/lang/String;

    iput-object v0, p0, Lfq1;->b:Lo30;

    iput-object v0, p0, Lfq1;->c:Ljq1;

    iput-object v0, p0, Lfq1;->d:Ljava/lang/String;

    iput-object v0, p0, Lfq1;->e:Ljava/lang/String;

    const-string v1, ""

    invoke-static {v1}, Lfq1$c;->c(Ljava/lang/Object;)Lfq1$c;

    move-result-object v2

    iput-object v2, p0, Lfq1;->f:Lfq1$c;

    iput-object v0, p0, Lfq1;->g:Ljava/lang/String;

    iput-object v0, p0, Lfq1;->h:Ljava/lang/String;

    iput-object v0, p0, Lfq1;->i:Ljava/lang/String;

    iput-object v0, p0, Lfq1;->k:Ljava/lang/String;

    invoke-static {v1}, Lfq1$c;->c(Ljava/lang/Object;)Lfq1$c;

    move-result-object v0

    iput-object v0, p0, Lfq1;->l:Lfq1$c;

    invoke-static {v1}, Lfq1$c;->c(Ljava/lang/Object;)Lfq1$c;

    move-result-object v0

    iput-object v0, p0, Lfq1;->m:Lfq1$c;

    invoke-static {v1}, Lfq1$c;->c(Ljava/lang/Object;)Lfq1$c;

    move-result-object v0

    iput-object v0, p0, Lfq1;->n:Lfq1$c;

    invoke-static {v1}, Lfq1$c;->c(Ljava/lang/Object;)Lfq1$c;

    move-result-object v0

    iput-object v0, p0, Lfq1;->o:Lfq1$c;

    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v0

    invoke-static {v0}, Lfq1$c;->c(Ljava/lang/Object;)Lfq1$c;

    move-result-object v0

    iput-object v0, p0, Lfq1;->p:Lfq1$c;

    return-void
.end method

.method public constructor <init>(Lfq1;Z)V
    .locals 3

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lfq1;->a:Ljava/lang/String;

    iput-object v0, p0, Lfq1;->b:Lo30;

    iput-object v0, p0, Lfq1;->c:Ljq1;

    iput-object v0, p0, Lfq1;->d:Ljava/lang/String;

    iput-object v0, p0, Lfq1;->e:Ljava/lang/String;

    const-string v1, ""

    invoke-static {v1}, Lfq1$c;->c(Ljava/lang/Object;)Lfq1$c;

    move-result-object v2

    iput-object v2, p0, Lfq1;->f:Lfq1$c;

    iput-object v0, p0, Lfq1;->g:Ljava/lang/String;

    iput-object v0, p0, Lfq1;->h:Ljava/lang/String;

    iput-object v0, p0, Lfq1;->i:Ljava/lang/String;

    iput-object v0, p0, Lfq1;->k:Ljava/lang/String;

    invoke-static {v1}, Lfq1$c;->c(Ljava/lang/Object;)Lfq1$c;

    move-result-object v0

    iput-object v0, p0, Lfq1;->l:Lfq1$c;

    invoke-static {v1}, Lfq1$c;->c(Ljava/lang/Object;)Lfq1$c;

    move-result-object v0

    iput-object v0, p0, Lfq1;->m:Lfq1$c;

    invoke-static {v1}, Lfq1$c;->c(Ljava/lang/Object;)Lfq1$c;

    move-result-object v0

    iput-object v0, p0, Lfq1;->n:Lfq1$c;

    invoke-static {v1}, Lfq1$c;->c(Ljava/lang/Object;)Lfq1$c;

    move-result-object v0

    iput-object v0, p0, Lfq1;->o:Lfq1$c;

    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v0

    invoke-static {v0}, Lfq1$c;->c(Ljava/lang/Object;)Lfq1$c;

    move-result-object v0

    iput-object v0, p0, Lfq1;->p:Lfq1$c;

    invoke-static {p1}, Lcom/google/android/gms/common/internal/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p1, Lfq1;->a:Ljava/lang/String;

    iput-object v0, p0, Lfq1;->a:Ljava/lang/String;

    iget-object v0, p1, Lfq1;->b:Lo30;

    iput-object v0, p0, Lfq1;->b:Lo30;

    iget-object v0, p1, Lfq1;->c:Ljq1;

    iput-object v0, p0, Lfq1;->c:Ljq1;

    iget-object v0, p1, Lfq1;->d:Ljava/lang/String;

    iput-object v0, p0, Lfq1;->d:Ljava/lang/String;

    iget-object v0, p1, Lfq1;->f:Lfq1$c;

    iput-object v0, p0, Lfq1;->f:Lfq1$c;

    iget-object v0, p1, Lfq1;->l:Lfq1$c;

    iput-object v0, p0, Lfq1;->l:Lfq1$c;

    iget-object v0, p1, Lfq1;->m:Lfq1$c;

    iput-object v0, p0, Lfq1;->m:Lfq1$c;

    iget-object v0, p1, Lfq1;->n:Lfq1$c;

    iput-object v0, p0, Lfq1;->n:Lfq1$c;

    iget-object v0, p1, Lfq1;->o:Lfq1$c;

    iput-object v0, p0, Lfq1;->o:Lfq1$c;

    iget-object v0, p1, Lfq1;->p:Lfq1$c;

    iput-object v0, p0, Lfq1;->p:Lfq1$c;

    if-eqz p2, :cond_0

    iget-object p2, p1, Lfq1;->k:Ljava/lang/String;

    iput-object p2, p0, Lfq1;->k:Ljava/lang/String;

    iget-wide v0, p1, Lfq1;->j:J

    iput-wide v0, p0, Lfq1;->j:J

    iget-object p2, p1, Lfq1;->i:Ljava/lang/String;

    iput-object p2, p0, Lfq1;->i:Ljava/lang/String;

    iget-object p2, p1, Lfq1;->h:Ljava/lang/String;

    iput-object p2, p0, Lfq1;->h:Ljava/lang/String;

    iget-object p2, p1, Lfq1;->g:Ljava/lang/String;

    iput-object p2, p0, Lfq1;->g:Ljava/lang/String;

    iget-object p1, p1, Lfq1;->e:Ljava/lang/String;

    iput-object p1, p0, Lfq1;->e:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method public synthetic constructor <init>(Lfq1;ZLfq1$a;)V
    .locals 0

    .line 3
    invoke-direct {p0, p1, p2}, Lfq1;-><init>(Lfq1;Z)V

    return-void
.end method

.method public static synthetic a(Lfq1;Lfq1$c;)Lfq1$c;
    .locals 0

    .line 1
    iput-object p1, p0, Lfq1;->o:Lfq1$c;

    return-object p1
.end method

.method public static synthetic b(Lfq1;Ljq1;)Ljq1;
    .locals 0

    .line 1
    iput-object p1, p0, Lfq1;->c:Ljq1;

    return-object p1
.end method

.method public static synthetic c(Lfq1;Lfq1$c;)Lfq1$c;
    .locals 0

    .line 1
    iput-object p1, p0, Lfq1;->n:Lfq1$c;

    return-object p1
.end method

.method public static synthetic d(Lfq1;Lfq1$c;)Lfq1$c;
    .locals 0

    .line 1
    iput-object p1, p0, Lfq1;->m:Lfq1$c;

    return-object p1
.end method

.method public static synthetic e(Lfq1;Lfq1$c;)Lfq1$c;
    .locals 0

    .line 1
    iput-object p1, p0, Lfq1;->l:Lfq1$c;

    return-object p1
.end method

.method public static synthetic f(Lfq1;)Lfq1$c;
    .locals 0

    .line 1
    iget-object p0, p0, Lfq1;->p:Lfq1$c;

    return-object p0
.end method

.method public static synthetic g(Lfq1;Lfq1$c;)Lfq1$c;
    .locals 0

    .line 1
    iput-object p1, p0, Lfq1;->p:Lfq1$c;

    return-object p1
.end method

.method public static synthetic h(Lfq1;Lfq1$c;)Lfq1$c;
    .locals 0

    .line 1
    iput-object p1, p0, Lfq1;->f:Lfq1$c;

    return-object p1
.end method

.method public static synthetic i(Lfq1;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 1
    iput-object p1, p0, Lfq1;->e:Ljava/lang/String;

    return-object p1
.end method

.method public static synthetic j(Lfq1;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 1
    iput-object p1, p0, Lfq1;->a:Ljava/lang/String;

    return-object p1
.end method

.method public static synthetic k(Lfq1;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 1
    iput-object p1, p0, Lfq1;->d:Ljava/lang/String;

    return-object p1
.end method

.method public static synthetic l(Lfq1;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 1
    iput-object p1, p0, Lfq1;->g:Ljava/lang/String;

    return-object p1
.end method

.method public static synthetic m(Lfq1;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 1
    iput-object p1, p0, Lfq1;->h:Ljava/lang/String;

    return-object p1
.end method

.method public static synthetic n(Lfq1;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 1
    iput-object p1, p0, Lfq1;->i:Ljava/lang/String;

    return-object p1
.end method

.method public static synthetic o(Lfq1;J)J
    .locals 0

    .line 1
    iput-wide p1, p0, Lfq1;->j:J

    return-wide p1
.end method

.method public static synthetic p(Lfq1;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 1
    iput-object p1, p0, Lfq1;->k:Ljava/lang/String;

    return-object p1
.end method


# virtual methods
.method public q()Lorg/json/JSONObject;
    .locals 3

    .line 1
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iget-object v1, p0, Lfq1;->f:Lfq1$c;

    invoke-virtual {v1}, Lfq1$c;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "contentType"

    invoke-virtual {p0}, Lfq1;->v()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    iget-object v1, p0, Lfq1;->p:Lfq1$c;

    invoke-virtual {v1}, Lfq1$c;->b()Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance v1, Lorg/json/JSONObject;

    iget-object v2, p0, Lfq1;->p:Lfq1$c;

    invoke-virtual {v2}, Lfq1$c;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map;

    invoke-direct {v1, v2}, Lorg/json/JSONObject;-><init>(Ljava/util/Map;)V

    const-string v2, "metadata"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    iget-object v1, p0, Lfq1;->l:Lfq1$c;

    invoke-virtual {v1}, Lfq1$c;->b()Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "cacheControl"

    invoke-virtual {p0}, Lfq1;->r()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_2
    iget-object v1, p0, Lfq1;->m:Lfq1$c;

    invoke-virtual {v1}, Lfq1$c;->b()Z

    move-result v1

    if-eqz v1, :cond_3

    const-string v1, "contentDisposition"

    invoke-virtual {p0}, Lfq1;->s()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_3
    iget-object v1, p0, Lfq1;->n:Lfq1$c;

    invoke-virtual {v1}, Lfq1$c;->b()Z

    move-result v1

    if-eqz v1, :cond_4

    const-string v1, "contentEncoding"

    invoke-virtual {p0}, Lfq1;->t()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_4
    iget-object v1, p0, Lfq1;->o:Lfq1$c;

    invoke-virtual {v1}, Lfq1$c;->b()Z

    move-result v1

    if-eqz v1, :cond_5

    const-string v1, "contentLanguage"

    invoke-virtual {p0}, Lfq1;->u()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_5
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1, v0}, Lorg/json/JSONObject;-><init>(Ljava/util/Map;)V

    return-object v1
.end method

.method public r()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lfq1;->l:Lfq1$c;

    invoke-virtual {v0}, Lfq1$c;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public s()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lfq1;->m:Lfq1$c;

    invoke-virtual {v0}, Lfq1$c;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public t()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lfq1;->n:Lfq1$c;

    invoke-virtual {v0}, Lfq1$c;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public u()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lfq1;->o:Lfq1$c;

    invoke-virtual {v0}, Lfq1$c;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public v()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lfq1;->f:Lfq1$c;

    invoke-virtual {v0}, Lfq1$c;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

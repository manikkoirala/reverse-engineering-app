.class public abstract Lgr1;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:[Ljava/lang/String;

.field public static final b:[Ljava/lang/String;

.field public static final c:[Ljava/lang/String;

.field public static final d:[Ljava/lang/String;

.field public static final e:[Ljava/lang/String;

.field public static final f:[Ljava/lang/String;

.field public static final g:[Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 11

    .line 1
    const-string v0, "TrueOrFalse"

    const-string v1, "3 option"

    const-string v2, "4 option"

    const-string v3, "5 option"

    const-string v4, "6 option"

    const-string v5, "8 option"

    const-string v6, "10 option"

    const-string v7, "Matrix"

    const-string v8, "Numerical"

    filled-new-array/range {v0 .. v8}, [Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lgr1;->a:[Ljava/lang/String;

    const-string v0, "Max Marks"

    const-string v1, "Min Marks"

    const-string v2, "Default"

    filled-new-array {v2, v0, v1}, [Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lgr1;->b:[Ljava/lang/String;

    const-string v0, "OPTION LABEL"

    const-string v1, "TEXT LABEL"

    const-string v2, "OPTION BLOCK"

    filled-new-array {v2, v0, v1}, [Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lgr1;->c:[Ljava/lang/String;

    const-string v1, "2"

    const-string v2, "3"

    const-string v3, "4"

    const-string v4, "5"

    const-string v5, "6"

    const-string v6, "7"

    filled-new-array/range {v1 .. v6}, [Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lgr1;->d:[Ljava/lang/String;

    const-string v1, "3"

    const-string v2, "4"

    const-string v3, "5"

    const-string v4, "6"

    const-string v5, "7"

    const-string v6, "8"

    const-string v7, "9"

    const-string v8, "10"

    const-string v9, "11"

    const-string v10, "12"

    filled-new-array/range {v1 .. v10}, [Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lgr1;->e:[Ljava/lang/String;

    const-string v0, "Public"

    const-string v1, "Shared"

    const-string v2, "Private"

    filled-new-array {v2, v0, v1}, [Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lgr1;->f:[Ljava/lang/String;

    const-string v0, "Large"

    const-string v1, "Full Width"

    const-string v2, "Small"

    const-string v3, "Medium"

    filled-new-array {v2, v3, v0, v1}, [Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lgr1;->g:[Ljava/lang/String;

    return-void
.end method

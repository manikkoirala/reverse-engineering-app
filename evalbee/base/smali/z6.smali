.class public Lz6;
.super Landroid/widget/ImageView;
.source "SourceFile"


# instance fields
.field private final mBackgroundTintHelper:Lx5;

.field private mHasLevel:Z

.field private final mImageHelper:Ly6;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .line 1
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lz6;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .line 2
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lz6;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .line 3
    invoke-static {p1}, Lqw1;->b(Landroid/content/Context;)Landroid/content/Context;

    move-result-object p1

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 p1, 0x0

    iput-boolean p1, p0, Lz6;->mHasLevel:Z

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p0, p1}, Lpv1;->a(Landroid/view/View;Landroid/content/Context;)V

    new-instance p1, Lx5;

    invoke-direct {p1, p0}, Lx5;-><init>(Landroid/view/View;)V

    iput-object p1, p0, Lz6;->mBackgroundTintHelper:Lx5;

    invoke-virtual {p1, p2, p3}, Lx5;->e(Landroid/util/AttributeSet;I)V

    new-instance p1, Ly6;

    invoke-direct {p1, p0}, Ly6;-><init>(Landroid/widget/ImageView;)V

    iput-object p1, p0, Lz6;->mImageHelper:Ly6;

    invoke-virtual {p1, p2, p3}, Ly6;->g(Landroid/util/AttributeSet;I)V

    return-void
.end method


# virtual methods
.method public drawableStateChanged()V
    .locals 1

    .line 1
    invoke-super {p0}, Landroid/widget/ImageView;->drawableStateChanged()V

    iget-object v0, p0, Lz6;->mBackgroundTintHelper:Lx5;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lx5;->b()V

    :cond_0
    iget-object v0, p0, Lz6;->mImageHelper:Ly6;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ly6;->c()V

    :cond_1
    return-void
.end method

.method public getSupportBackgroundTintList()Landroid/content/res/ColorStateList;
    .locals 1

    .line 1
    iget-object v0, p0, Lz6;->mBackgroundTintHelper:Lx5;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lx5;->c()Landroid/content/res/ColorStateList;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public getSupportBackgroundTintMode()Landroid/graphics/PorterDuff$Mode;
    .locals 1

    .line 1
    iget-object v0, p0, Lz6;->mBackgroundTintHelper:Lx5;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lx5;->d()Landroid/graphics/PorterDuff$Mode;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public getSupportImageTintList()Landroid/content/res/ColorStateList;
    .locals 1

    .line 1
    iget-object v0, p0, Lz6;->mImageHelper:Ly6;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ly6;->d()Landroid/content/res/ColorStateList;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public getSupportImageTintMode()Landroid/graphics/PorterDuff$Mode;
    .locals 1

    .line 1
    iget-object v0, p0, Lz6;->mImageHelper:Ly6;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ly6;->e()Landroid/graphics/PorterDuff$Mode;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public hasOverlappingRendering()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lz6;->mImageHelper:Ly6;

    invoke-virtual {v0}, Ly6;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-super {p0}, Landroid/widget/ImageView;->hasOverlappingRendering()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .line 1
    invoke-super {p0, p1}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lz6;->mBackgroundTintHelper:Lx5;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lx5;->f(Landroid/graphics/drawable/Drawable;)V

    :cond_0
    return-void
.end method

.method public setBackgroundResource(I)V
    .locals 1

    .line 1
    invoke-super {p0, p1}, Landroid/view/View;->setBackgroundResource(I)V

    iget-object v0, p0, Lz6;->mBackgroundTintHelper:Lx5;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lx5;->g(I)V

    :cond_0
    return-void
.end method

.method public setImageBitmap(Landroid/graphics/Bitmap;)V
    .locals 0

    .line 1
    invoke-super {p0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    iget-object p1, p0, Lz6;->mImageHelper:Ly6;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ly6;->c()V

    :cond_0
    return-void
.end method

.method public setImageDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lz6;->mImageHelper:Ly6;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    iget-boolean v1, p0, Lz6;->mHasLevel:Z

    if-nez v1, :cond_0

    invoke-virtual {v0, p1}, Ly6;->h(Landroid/graphics/drawable/Drawable;)V

    :cond_0
    invoke-super {p0, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object p1, p0, Lz6;->mImageHelper:Ly6;

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ly6;->c()V

    iget-boolean p1, p0, Lz6;->mHasLevel:Z

    if-nez p1, :cond_1

    iget-object p1, p0, Lz6;->mImageHelper:Ly6;

    invoke-virtual {p1}, Ly6;->b()V

    :cond_1
    return-void
.end method

.method public setImageLevel(I)V
    .locals 0

    .line 1
    invoke-super {p0, p1}, Landroid/widget/ImageView;->setImageLevel(I)V

    const/4 p1, 0x1

    iput-boolean p1, p0, Lz6;->mHasLevel:Z

    return-void
.end method

.method public setImageResource(I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lz6;->mImageHelper:Ly6;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Ly6;->i(I)V

    :cond_0
    return-void
.end method

.method public setImageURI(Landroid/net/Uri;)V
    .locals 0

    .line 1
    invoke-super {p0, p1}, Landroid/widget/ImageView;->setImageURI(Landroid/net/Uri;)V

    iget-object p1, p0, Lz6;->mImageHelper:Ly6;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ly6;->c()V

    :cond_0
    return-void
.end method

.method public setSupportBackgroundTintList(Landroid/content/res/ColorStateList;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lz6;->mBackgroundTintHelper:Lx5;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lx5;->i(Landroid/content/res/ColorStateList;)V

    :cond_0
    return-void
.end method

.method public setSupportBackgroundTintMode(Landroid/graphics/PorterDuff$Mode;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lz6;->mBackgroundTintHelper:Lx5;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lx5;->j(Landroid/graphics/PorterDuff$Mode;)V

    :cond_0
    return-void
.end method

.method public setSupportImageTintList(Landroid/content/res/ColorStateList;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lz6;->mImageHelper:Ly6;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Ly6;->j(Landroid/content/res/ColorStateList;)V

    :cond_0
    return-void
.end method

.method public setSupportImageTintMode(Landroid/graphics/PorterDuff$Mode;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lz6;->mImageHelper:Ly6;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Ly6;->k(Landroid/graphics/PorterDuff$Mode;)V

    :cond_0
    return-void
.end method

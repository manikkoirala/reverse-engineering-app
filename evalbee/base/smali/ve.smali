.class public Lve;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lwe;


# instance fields
.field public final a:Z

.field public b:Z

.field public c:Z

.field public d:Landroid/hardware/Camera;

.field public e:Lcom/ekodroid/omrevaluator/components/AutofitPreview;

.field public f:Z

.field public g:[B

.field public final h:Landroid/hardware/Camera$PreviewCallback;

.field public i:I

.field public j:[B

.field public k:Z

.field public l:Landroid/hardware/Camera$PictureCallback;

.field public m:Landroid/hardware/Camera$Size;

.field public n:Landroid/hardware/Camera$Size;

.field public o:Lxe;

.field public p:Z

.field public final q:Landroid/view/TextureView$SurfaceTextureListener;


# direct methods
.method public constructor <init>(Lcom/ekodroid/omrevaluator/components/AutofitPreview;Lxe;ZI)V
    .locals 2

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lve;->b:Z

    iput-boolean v0, p0, Lve;->c:Z

    iput-boolean v0, p0, Lve;->f:Z

    new-instance v1, Lve$a;

    invoke-direct {v1, p0}, Lve$a;-><init>(Lve;)V

    iput-object v1, p0, Lve;->h:Landroid/hardware/Camera$PreviewCallback;

    iput-boolean v0, p0, Lve;->k:Z

    new-instance v1, Lve$b;

    invoke-direct {v1, p0}, Lve$b;-><init>(Lve;)V

    iput-object v1, p0, Lve;->l:Landroid/hardware/Camera$PictureCallback;

    const/4 v1, 0x0

    iput-object v1, p0, Lve;->m:Landroid/hardware/Camera$Size;

    iput-object v1, p0, Lve;->n:Landroid/hardware/Camera$Size;

    iput-boolean v0, p0, Lve;->p:Z

    new-instance v0, Lve$c;

    invoke-direct {v0, p0}, Lve$c;-><init>(Lve;)V

    iput-object v0, p0, Lve;->q:Landroid/view/TextureView$SurfaceTextureListener;

    iput-object p1, p0, Lve;->e:Lcom/ekodroid/omrevaluator/components/AutofitPreview;

    invoke-virtual {p1, v0}, Landroid/view/TextureView;->setSurfaceTextureListener(Landroid/view/TextureView$SurfaceTextureListener;)V

    iput-object p2, p0, Lve;->o:Lxe;

    iput-boolean p3, p0, Lve;->a:Z

    iput p4, p0, Lve;->i:I

    return-void
.end method

.method public static synthetic c(Lve;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lve;->f:Z

    return p0
.end method

.method public static synthetic d(Lve;Z)Z
    .locals 0

    .line 1
    iput-boolean p1, p0, Lve;->f:Z

    return p1
.end method

.method public static synthetic e(Lve;)Landroid/hardware/Camera$Size;
    .locals 0

    .line 1
    iget-object p0, p0, Lve;->n:Landroid/hardware/Camera$Size;

    return-object p0
.end method

.method public static synthetic f(Lve;Landroid/hardware/Camera$Size;)Landroid/hardware/Camera$Size;
    .locals 0

    .line 1
    iput-object p1, p0, Lve;->n:Landroid/hardware/Camera$Size;

    return-object p1
.end method

.method public static synthetic g(Lve;[B)[B
    .locals 0

    .line 1
    iput-object p1, p0, Lve;->g:[B

    return-object p1
.end method

.method public static synthetic h(Lve;Landroid/hardware/Camera$Parameters;Landroid/hardware/Camera$Size;)Landroid/hardware/Camera$Size;
    .locals 0

    .line 1
    invoke-virtual {p0, p1, p2}, Lve;->z(Landroid/hardware/Camera$Parameters;Landroid/hardware/Camera$Size;)Landroid/hardware/Camera$Size;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic i(Lve;)I
    .locals 0

    .line 1
    iget p0, p0, Lve;->i:I

    return p0
.end method

.method public static synthetic j(Lve;Landroid/hardware/Camera$Parameters;I)Landroid/hardware/Camera$Size;
    .locals 0

    .line 1
    invoke-virtual {p0, p1, p2}, Lve;->A(Landroid/hardware/Camera$Parameters;I)Landroid/hardware/Camera$Size;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic k(Lve;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lve;->p:Z

    return p0
.end method

.method public static synthetic l(Lve;Z)Z
    .locals 0

    .line 1
    iput-boolean p1, p0, Lve;->p:Z

    return p1
.end method

.method public static synthetic m(Lve;)Lcom/ekodroid/omrevaluator/components/AutofitPreview;
    .locals 0

    .line 1
    iget-object p0, p0, Lve;->e:Lcom/ekodroid/omrevaluator/components/AutofitPreview;

    return-object p0
.end method

.method public static synthetic n(Lve;)Lxe;
    .locals 0

    .line 1
    iget-object p0, p0, Lve;->o:Lxe;

    return-object p0
.end method

.method public static synthetic o(Lve;II[B)[B
    .locals 0

    .line 1
    invoke-virtual {p0, p1, p2, p3}, Lve;->C(II[B)[B

    move-result-object p0

    return-object p0
.end method

.method public static synthetic p(Lve;[B)[B
    .locals 0

    .line 1
    iput-object p1, p0, Lve;->j:[B

    return-object p1
.end method

.method public static synthetic q(Lve;)Landroid/hardware/Camera;
    .locals 0

    .line 1
    iget-object p0, p0, Lve;->d:Landroid/hardware/Camera;

    return-object p0
.end method

.method public static synthetic r(Lve;Landroid/hardware/Camera;)Landroid/hardware/Camera;
    .locals 0

    .line 1
    iput-object p1, p0, Lve;->d:Landroid/hardware/Camera;

    return-object p1
.end method

.method public static synthetic s(Lve;Z)Z
    .locals 0

    .line 1
    iput-boolean p1, p0, Lve;->k:Z

    return p1
.end method

.method public static synthetic t(Lve;)Landroid/hardware/Camera$PreviewCallback;
    .locals 0

    .line 1
    iget-object p0, p0, Lve;->h:Landroid/hardware/Camera$PreviewCallback;

    return-object p0
.end method

.method public static synthetic u(Lve;)Z
    .locals 0

    .line 1
    iget-boolean p0, p0, Lve;->a:Z

    return p0
.end method

.method public static synthetic v(Lve;)Landroid/hardware/Camera$Size;
    .locals 0

    .line 1
    iget-object p0, p0, Lve;->m:Landroid/hardware/Camera$Size;

    return-object p0
.end method

.method public static synthetic w(Lve;Landroid/hardware/Camera$Size;)Landroid/hardware/Camera$Size;
    .locals 0

    .line 1
    iput-object p1, p0, Lve;->m:Landroid/hardware/Camera$Size;

    return-object p1
.end method

.method public static synthetic x(Lve;Landroid/hardware/Camera$Parameters;)Landroid/hardware/Camera$Size;
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lve;->B(Landroid/hardware/Camera$Parameters;)Landroid/hardware/Camera$Size;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final A(Landroid/hardware/Camera$Parameters;I)Landroid/hardware/Camera$Size;
    .locals 19

    .line 1
    invoke-virtual/range {p1 .. p1}, Landroid/hardware/Camera$Parameters;->getSupportedPreviewSizes()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    const-wide/16 v2, 0x0

    const-wide v4, 0x408f400000000000L    # 1000.0

    const/4 v6, 0x0

    move-wide v8, v4

    move v10, v6

    move-wide v4, v2

    move-wide v6, v4

    move-object v2, v1

    move-object v3, v2

    :goto_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v11

    if-ge v10, v11, :cond_5

    invoke-interface {v0, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/hardware/Camera$Size;

    iget v12, v11, Landroid/hardware/Camera$Size;->height:I

    iget v13, v11, Landroid/hardware/Camera$Size;->width:I

    mul-int v14, v12, v13

    if-ge v12, v13, :cond_1

    move-object/from16 p1, v2

    move-object v15, v3

    int-to-double v2, v12

    const-wide/high16 v16, 0x3ff0000000000000L    # 1.0

    mul-double v2, v2, v16

    move-object/from16 v16, v0

    move-wide/from16 v17, v6

    move/from16 v0, p2

    int-to-double v6, v0

    div-double/2addr v2, v6

    const-wide/high16 v6, 0x4040000000000000L    # 32.0

    cmpl-double v6, v2, v6

    if-lez v6, :cond_2

    cmpg-double v6, v2, v8

    if-gtz v6, :cond_2

    if-eqz v1, :cond_0

    iget v6, v1, Landroid/hardware/Camera$Size;->width:I

    iget v7, v1, Landroid/hardware/Camera$Size;->height:I

    mul-int/2addr v6, v7

    if-ge v14, v6, :cond_2

    :cond_0
    move-wide v8, v2

    move-object v1, v11

    goto :goto_1

    :cond_1
    move-object/from16 v16, v0

    move-object/from16 p1, v2

    move-object v15, v3

    move-wide/from16 v17, v6

    move/from16 v0, p2

    :cond_2
    :goto_1
    move-object/from16 v2, p0

    if-ge v12, v13, :cond_3

    invoke-virtual {v2, v11}, Lve;->D(Landroid/hardware/Camera$Size;)Z

    move-result v3

    if-eqz v3, :cond_3

    int-to-double v6, v14

    cmpl-double v3, v6, v4

    if-lez v3, :cond_4

    move-wide v4, v6

    goto :goto_2

    :cond_3
    int-to-double v6, v14

    cmpl-double v3, v6, v17

    if-lez v3, :cond_4

    move-object v3, v11

    move-object/from16 v11, p1

    goto :goto_3

    :cond_4
    move-object/from16 v11, p1

    :goto_2
    move-object v3, v15

    move-wide/from16 v6, v17

    :goto_3
    add-int/lit8 v10, v10, 0x1

    move-object v2, v11

    move-object/from16 v0, v16

    goto :goto_0

    :cond_5
    move-object/from16 p1, v2

    move-object v15, v3

    move-object/from16 v2, p0

    if-eqz v1, :cond_6

    return-object v1

    :cond_6
    if-eqz p1, :cond_7

    return-object p1

    :cond_7
    return-object v15
.end method

.method public final B(Landroid/hardware/Camera$Parameters;)Landroid/hardware/Camera$Size;
    .locals 11

    .line 1
    invoke-virtual {p1}, Landroid/hardware/Camera$Parameters;->getSupportedPreviewSizes()Ljava/util/List;

    move-result-object p1

    const/4 v0, 0x0

    const-wide/16 v1, 0x0

    const/4 v3, 0x0

    move-wide v4, v1

    move v6, v3

    move-object v1, v0

    move-wide v2, v4

    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v7

    if-ge v6, v7, :cond_2

    invoke-interface {p1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/hardware/Camera$Size;

    iget v8, v7, Landroid/hardware/Camera$Size;->height:I

    iget v9, v7, Landroid/hardware/Camera$Size;->width:I

    mul-int/2addr v8, v9

    invoke-virtual {p0, v7}, Lve;->D(Landroid/hardware/Camera$Size;)Z

    move-result v9

    if-eqz v9, :cond_0

    int-to-double v8, v8

    cmpl-double v10, v8, v4

    if-lez v10, :cond_1

    move-object v0, v7

    move-wide v4, v8

    goto :goto_1

    :cond_0
    int-to-double v8, v8

    cmpl-double v10, v8, v2

    if-lez v10, :cond_1

    move-object v1, v7

    move-wide v2, v8

    :cond_1
    :goto_1
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    :cond_2
    if-eqz v0, :cond_3

    return-object v0

    :cond_3
    return-object v1
.end method

.method public final C(II[B)[B
    .locals 7

    .line 1
    new-instance v6, Landroid/graphics/YuvImage;

    const/16 v2, 0x11

    const/4 v5, 0x0

    move-object v0, v6

    move-object v1, p3

    move v3, p1

    move v4, p2

    invoke-direct/range {v0 .. v5}, Landroid/graphics/YuvImage;-><init>([BIII[I)V

    new-instance p3, Ljava/io/ByteArrayOutputStream;

    invoke-direct {p3}, Ljava/io/ByteArrayOutputStream;-><init>()V

    new-instance v0, Landroid/graphics/Rect;

    const/4 v1, 0x0

    invoke-direct {v0, v1, v1, p1, p2}, Landroid/graphics/Rect;-><init>(IIII)V

    const/16 p1, 0x64

    invoke-virtual {v6, v0, p1, p3}, Landroid/graphics/YuvImage;->compressToJpeg(Landroid/graphics/Rect;ILjava/io/OutputStream;)Z

    invoke-virtual {p3}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object p1

    return-object p1
.end method

.method public final D(Landroid/hardware/Camera$Size;)Z
    .locals 5

    .line 1
    iget v0, p1, Landroid/hardware/Camera$Size;->height:I

    iget p1, p1, Landroid/hardware/Camera$Size;->width:I

    if-le v0, p1, :cond_0

    int-to-double v0, v0

    int-to-double v2, p1

    div-double/2addr v0, v2

    goto :goto_0

    :cond_0
    int-to-double v1, p1

    int-to-double v3, v0

    div-double v0, v1, v3

    :goto_0
    const-wide v2, 0x3ff6eeeeeeeeeeefL    # 1.4333333333333333

    cmpg-double p1, v0, v2

    if-gez p1, :cond_1

    const-wide v2, 0x3ff3bbbbbbbbbbbbL    # 1.2333333333333332

    cmpl-double p1, v0, v2

    if-lez p1, :cond_1

    const/4 p1, 0x1

    goto :goto_1

    :cond_1
    const/4 p1, 0x0

    :goto_1
    return p1
.end method

.method public a()V
    .locals 2

    .line 1
    iget-object v0, p0, Lve;->d:Landroid/hardware/Camera;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/hardware/Camera;->stopPreview()V

    iget-object v0, p0, Lve;->d:Landroid/hardware/Camera;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/hardware/Camera;->setPreviewCallback(Landroid/hardware/Camera$PreviewCallback;)V

    iget-object v0, p0, Lve;->d:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->release()V

    iput-object v1, p0, Lve;->d:Landroid/hardware/Camera;

    :cond_0
    return-void
.end method

.method public b()V
    .locals 6

    .line 1
    iget-object v0, p0, Lve;->d:Landroid/hardware/Camera;

    if-eqz v0, :cond_1

    iget-boolean v1, p0, Lve;->p:Z

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Landroid/hardware/Camera;->cancelAutoFocus()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lve;->b:Z

    iget-object v0, p0, Lve;->d:Landroid/hardware/Camera;

    new-instance v1, Lve$d;

    invoke-direct {v1, p0}, Lve$d;-><init>(Lve;)V

    invoke-virtual {v0, v1}, Landroid/hardware/Camera;->autoFocus(Landroid/hardware/Camera$AutoFocusCallback;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sub-long/2addr v2, v0

    const-wide/16 v4, 0xbb8

    cmp-long v2, v2, v4

    if-gez v2, :cond_1

    const-wide/16 v2, 0x32

    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v2

    invoke-virtual {v2}, Ljava/lang/Throwable;->printStackTrace()V

    :goto_0
    iget-boolean v2, p0, Lve;->b:Z

    if-eqz v2, :cond_0

    const-wide/16 v0, 0x64

    :try_start_1
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    :cond_1
    :goto_1
    return-void
.end method

.method public declared-synchronized getFrame()[B
    .locals 6

    .line 1
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lve;->f:Z

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    sub-long/2addr v2, v0

    const-wide/16 v4, 0x7d0

    cmp-long v2, v2, v4

    if-gez v2, :cond_1

    const-wide/16 v2, 0x32

    :try_start_1
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v2

    :try_start_2
    invoke-virtual {v2}, Ljava/lang/Throwable;->printStackTrace()V

    :goto_0
    iget-boolean v2, p0, Lve;->f:Z

    if-nez v2, :cond_0

    iget-object v0, p0, Lve;->g:[B
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    monitor-exit p0

    return-object v0

    :cond_1
    monitor-exit p0

    const/4 v0, 0x0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getImage()[B
    .locals 8

    .line 1
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lve;->k:Z

    iget-boolean v0, p0, Lve;->c:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    monitor-exit p0

    return-object v1

    :cond_0
    :try_start_1
    invoke-virtual {p0}, Lve;->y()V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    sub-long/2addr v4, v2

    const-wide/16 v6, 0xfa0

    cmp-long v0, v4, v6

    if-gez v0, :cond_2

    const-wide/16 v4, 0x32

    :try_start_2
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_3
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    :goto_0
    iget-boolean v0, p0, Lve;->k:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lve;->j:[B
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    monitor-exit p0

    return-object v0

    :cond_2
    monitor-exit p0

    return-object v1

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final y()V
    .locals 3

    .line 1
    iget-object v0, p0, Lve;->d:Landroid/hardware/Camera;

    const/4 v1, 0x0

    iget-object v2, p0, Lve;->l:Landroid/hardware/Camera$PictureCallback;

    invoke-virtual {v0, v1, v1, v2}, Landroid/hardware/Camera;->takePicture(Landroid/hardware/Camera$ShutterCallback;Landroid/hardware/Camera$PictureCallback;Landroid/hardware/Camera$PictureCallback;)V

    return-void
.end method

.method public final z(Landroid/hardware/Camera$Parameters;Landroid/hardware/Camera$Size;)Landroid/hardware/Camera$Size;
    .locals 7

    .line 1
    invoke-virtual {p0, p2}, Lve;->D(Landroid/hardware/Camera$Size;)Z

    move-result p2

    const/4 v0, 0x0

    if-nez p2, :cond_0

    return-object v0

    :cond_0
    invoke-virtual {p1}, Landroid/hardware/Camera$Parameters;->getSupportedPictureSizes()Ljava/util/List;

    move-result-object p1

    const/4 p2, 0x0

    move-object v1, v0

    move-object v2, v1

    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    if-ge p2, v3, :cond_5

    invoke-interface {p1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/hardware/Camera$Size;

    iget v4, v3, Landroid/hardware/Camera$Size;->width:I

    iget v5, v3, Landroid/hardware/Camera$Size;->height:I

    mul-int/2addr v4, v5

    invoke-virtual {p0, v3}, Lve;->D(Landroid/hardware/Camera$Size;)Z

    move-result v5

    if-eqz v5, :cond_4

    const v5, 0x2dc6c0

    if-ge v4, v5, :cond_2

    if-nez v2, :cond_1

    goto :goto_1

    :cond_1
    iget v5, v2, Landroid/hardware/Camera$Size;->width:I

    iget v6, v2, Landroid/hardware/Camera$Size;->height:I

    mul-int/2addr v5, v6

    if-ge v5, v4, :cond_4

    :goto_1
    move-object v2, v3

    goto :goto_3

    :cond_2
    if-nez v1, :cond_3

    goto :goto_2

    :cond_3
    iget v5, v1, Landroid/hardware/Camera$Size;->width:I

    iget v6, v1, Landroid/hardware/Camera$Size;->height:I

    mul-int/2addr v5, v6

    if-le v5, v4, :cond_4

    :goto_2
    move-object v1, v3

    :cond_4
    :goto_3
    add-int/lit8 p2, p2, 0x1

    goto :goto_0

    :cond_5
    if-eqz v1, :cond_6

    return-object v1

    :cond_6
    if-eqz v2, :cond_7

    return-object v2

    :cond_7
    return-object v0
.end method

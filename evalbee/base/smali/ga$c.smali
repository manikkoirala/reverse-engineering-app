.class public final Lga$c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lw01;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lga;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "c"
.end annotation


# static fields
.field public static final a:Lga$c;

.field public static final b:Ln00;

.field public static final c:Ln00;

.field public static final d:Ln00;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .line 1
    new-instance v0, Lga$c;

    invoke-direct {v0}, Lga$c;-><init>()V

    sput-object v0, Lga$c;->a:Lga$c;

    const-string v0, "performance"

    invoke-static {v0}, Ln00;->d(Ljava/lang/String;)Ln00;

    move-result-object v0

    sput-object v0, Lga$c;->b:Ln00;

    const-string v0, "crashlytics"

    invoke-static {v0}, Ln00;->d(Ljava/lang/String;)Ln00;

    move-result-object v0

    sput-object v0, Lga$c;->c:Ln00;

    const-string v0, "sessionSamplingRate"

    invoke-static {v0}, Ln00;->d(Ljava/lang/String;)Ln00;

    move-result-object v0

    sput-object v0, Lga$c;->d:Ln00;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lgp;Lx01;)V
    .locals 3

    .line 1
    sget-object v0, Lga$c;->b:Ln00;

    invoke-virtual {p1}, Lgp;->b()Lcom/google/firebase/sessions/DataCollectionState;

    move-result-object v1

    invoke-interface {p2, v0, v1}, Lx01;->f(Ln00;Ljava/lang/Object;)Lx01;

    sget-object v0, Lga$c;->c:Ln00;

    invoke-virtual {p1}, Lgp;->a()Lcom/google/firebase/sessions/DataCollectionState;

    move-result-object v1

    invoke-interface {p2, v0, v1}, Lx01;->f(Ln00;Ljava/lang/Object;)Lx01;

    sget-object v0, Lga$c;->d:Ln00;

    invoke-virtual {p1}, Lgp;->c()D

    move-result-wide v1

    invoke-interface {p2, v0, v1, v2}, Lx01;->d(Ln00;D)Lx01;

    return-void
.end method

.method public bridge synthetic encode(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .line 1
    check-cast p1, Lgp;

    check-cast p2, Lx01;

    invoke-virtual {p0, p1, p2}, Lga$c;->a(Lgp;Lx01;)V

    return-void
.end method

.class public Lhn0;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static c:I = 0x0

.field public static d:I = 0x0

.field public static e:I = 0x0

.field public static f:I = 0x0

.field public static g:I = 0x5

.field public static h:D = 0.3


# instance fields
.field public a:D

.field public b:D


# direct methods
.method public static constructor <clinit>()V
    .locals 0

    .line 1
    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/high16 v0, 0x3fe0000000000000L    # 0.5

    iput-wide v0, p0, Lhn0;->a:D

    const-wide v0, 0x3feccccccccccccdL    # 0.9

    iput-wide v0, p0, Lhn0;->b:D

    return-void
.end method

.method public static j()V
    .locals 2

    .line 1
    const/4 v0, 0x0

    sput v0, Lhn0;->c:I

    sput v0, Lhn0;->d:I

    sput v0, Lhn0;->e:I

    sput v0, Lhn0;->f:I

    const-wide v0, 0x3fd3333333333333L    # 0.3

    sput-wide v0, Lhn0;->h:D

    return-void
.end method


# virtual methods
.method public final a(Landroid/graphics/Bitmap;Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;DLcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;DLjava/util/ArrayList;)Z
    .locals 25

    .line 1
    move-object/from16 v0, p0

    move-object/from16 v8, p1

    move-object/from16 v9, p2

    move-object/from16 v10, p5

    move-wide/from16 v11, p6

    move-object/from16 v13, p8

    iget-wide v1, v0, Lhn0;->b:D

    cmpl-double v1, v11, v1

    const/4 v14, 0x0

    if-lez v1, :cond_0

    return v14

    :cond_0
    iget v15, v10, Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;->squarelength:I

    iget v1, v10, Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;->bottomCornerHeight:I

    iget v2, v10, Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;->topCornerHeight:I

    sub-int/2addr v1, v2

    sub-int/2addr v1, v15

    invoke-static {}, Lgk;->a()I

    move-result v2

    iget-object v3, v9, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;->sheetMarkers:[[Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    array-length v3, v3

    const/16 v16, 0x1

    add-int/lit8 v3, v3, -0x1

    mul-int/lit8 v3, v3, 0x5

    add-int/lit8 v3, v3, 0x1

    mul-int/2addr v2, v3

    div-int v7, v1, v2

    invoke-static {}, La91;->n()I

    move-result v1

    invoke-static {}, Lgk;->b()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    const v2, 0xc350

    if-le v1, v2, :cond_1

    return v14

    :cond_1
    move v5, v14

    move/from16 v17, v5

    :goto_0
    mul-int/lit8 v6, v15, 0x2

    if-ge v5, v6, :cond_9

    const-wide/high16 v18, 0x3ff8000000000000L    # 1.5

    if-ge v5, v15, :cond_6

    move v4, v5

    :goto_1
    const/4 v1, -0x1

    if-le v4, v1, :cond_5

    iget v1, v10, Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;->topCornerWidth:I

    add-int v3, v1, v4

    iget v1, v10, Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;->bottomCornerHeight:I

    add-int/lit8 v1, v1, -0x1

    sub-int/2addr v1, v5

    add-int v2, v1, v4

    invoke-virtual {v8, v3, v2}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v1

    invoke-static {v1, v11, v12}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->k(ID)Z

    move-result v1

    if-eqz v1, :cond_3

    move/from16 v23, v15

    iget-wide v14, v0, Lhn0;->a:D

    move-object/from16 v1, p1

    move/from16 v20, v2

    move v2, v3

    move/from16 v21, v3

    move/from16 v3, v20

    move v0, v4

    move v4, v7

    move/from16 v22, v5

    move/from16 v24, v6

    move-wide v5, v14

    invoke-static/range {v1 .. v6}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->j(Landroid/graphics/Bitmap;IIID)Z

    move-result v1

    if-eqz v1, :cond_2

    int-to-double v1, v7

    mul-double v1, v1, v18

    double-to-int v4, v1

    move-object/from16 v1, p1

    move/from16 v2, v21

    move/from16 v3, v20

    move-wide/from16 v5, p6

    invoke-static/range {v1 .. v6}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->i(Landroid/graphics/Bitmap;IIID)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v5, 0x0

    move-object/from16 v1, p1

    move v2, v7

    move/from16 v3, v21

    move/from16 v4, v20

    move v14, v7

    move-wide/from16 v6, p6

    invoke-static/range {v1 .. v7}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->c(Landroid/graphics/Bitmap;IIIZD)Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    move-result-object v1

    invoke-static {v8, v1, v14, v11, v12}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->l(Landroid/graphics/Bitmap;Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;ID)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v0, v9, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;->sheetMarkers:[[Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    array-length v2, v0

    add-int/lit8 v2, v2, -0x1

    aget-object v2, v0, v2

    const/4 v3, 0x0

    aput-object v1, v2, v3

    add-int/lit8 v6, v24, 0x1

    new-instance v1, Lem;

    array-length v2, v0

    add-int/lit8 v2, v2, -0x1

    aget-object v2, v0, v2

    aget-object v2, v2, v3

    iget-wide v4, v2, Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;->x:D

    double-to-int v2, v4

    array-length v4, v0

    add-int/lit8 v4, v4, -0x1

    aget-object v0, v0, v4

    aget-object v0, v0, v3

    iget-wide v3, v0, Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;->y:D

    double-to-int v0, v3

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v21

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v22

    move-object/from16 v17, v1

    move/from16 v18, v2

    move/from16 v19, v0

    move/from16 v20, v14

    invoke-direct/range {v17 .. v22}, Lem;-><init>(IIIII)V

    invoke-virtual {v13, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v5, v6

    move/from16 v17, v16

    goto :goto_3

    :cond_2
    move v14, v7

    goto :goto_2

    :cond_3
    move v0, v4

    move/from16 v22, v5

    move/from16 v24, v6

    move v14, v7

    move/from16 v23, v15

    :cond_4
    :goto_2
    int-to-double v0, v0

    sub-double v0, v0, p3

    double-to-int v4, v0

    move-object/from16 v0, p0

    move v7, v14

    move/from16 v5, v22

    move/from16 v15, v23

    move/from16 v6, v24

    const/4 v14, 0x0

    goto/16 :goto_1

    :cond_5
    move/from16 v22, v5

    move v14, v7

    move/from16 v23, v15

    :goto_3
    const/4 v3, 0x0

    goto/16 :goto_5

    :cond_6
    move/from16 v22, v5

    move/from16 v24, v6

    move v14, v7

    move/from16 v23, v15

    move/from16 v0, v23

    :goto_4
    sub-int v5, v22, v23

    if-le v0, v5, :cond_8

    iget v1, v10, Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;->topCornerWidth:I

    add-int v7, v1, v0

    iget v1, v10, Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;->bottomCornerHeight:I

    add-int/lit8 v1, v1, -0x1

    sub-int v1, v1, v22

    add-int v15, v1, v0

    invoke-virtual {v8, v7, v15}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v1

    invoke-static {v1, v11, v12}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->k(ID)Z

    move-result v1

    if-eqz v1, :cond_7

    move-object/from16 v5, p0

    iget-wide v3, v5, Lhn0;->a:D

    move-object/from16 v1, p1

    move v2, v7

    move-wide/from16 v20, v3

    move v3, v15

    move v4, v14

    move-wide/from16 v5, v20

    invoke-static/range {v1 .. v6}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->j(Landroid/graphics/Bitmap;IIID)Z

    move-result v1

    if-eqz v1, :cond_7

    int-to-double v1, v14

    mul-double v1, v1, v18

    double-to-int v4, v1

    move-object/from16 v1, p1

    move v2, v7

    move v3, v15

    move-wide/from16 v5, p6

    invoke-static/range {v1 .. v6}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->i(Landroid/graphics/Bitmap;IIID)Z

    move-result v1

    if-eqz v1, :cond_7

    const/4 v5, 0x0

    move-object/from16 v1, p1

    move v2, v14

    move v3, v7

    move v4, v15

    move-wide/from16 v6, p6

    invoke-static/range {v1 .. v7}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->c(Landroid/graphics/Bitmap;IIIZD)Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    move-result-object v1

    invoke-static {v8, v1, v14, v11, v12}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->l(Landroid/graphics/Bitmap;Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;ID)Z

    move-result v2

    if-eqz v2, :cond_7

    iget-object v0, v9, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;->sheetMarkers:[[Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    array-length v2, v0

    add-int/lit8 v2, v2, -0x1

    aget-object v2, v0, v2

    const/4 v3, 0x0

    aput-object v1, v2, v3

    add-int/lit8 v5, v24, 0x1

    new-instance v1, Lem;

    array-length v2, v0

    add-int/lit8 v2, v2, -0x1

    aget-object v2, v0, v2

    aget-object v2, v2, v3

    iget-wide v6, v2, Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;->x:D

    double-to-int v2, v6

    array-length v4, v0

    add-int/lit8 v4, v4, -0x1

    aget-object v0, v0, v4

    aget-object v0, v0, v3

    iget-wide v6, v0, Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;->y:D

    double-to-int v0, v6

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v21

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v22

    move-object/from16 v17, v1

    move/from16 v18, v2

    move/from16 v19, v0

    move/from16 v20, v14

    invoke-direct/range {v17 .. v22}, Lem;-><init>(IIIII)V

    invoke-virtual {v13, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move/from16 v17, v16

    goto :goto_5

    :cond_7
    const/4 v3, 0x0

    int-to-double v0, v0

    sub-double v0, v0, p3

    double-to-int v0, v0

    goto/16 :goto_4

    :cond_8
    const/4 v3, 0x0

    move/from16 v5, v22

    :goto_5
    int-to-double v0, v5

    add-double v0, v0, p3

    double-to-int v5, v0

    move-object/from16 v0, p0

    move v7, v14

    move/from16 v15, v23

    move v14, v3

    goto/16 :goto_0

    :cond_9
    return v17
.end method

.method public final b(Landroid/graphics/Bitmap;Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;DLcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;DLjava/util/ArrayList;)Z
    .locals 25

    .line 1
    move-object/from16 v0, p0

    move-object/from16 v8, p1

    move-object/from16 v9, p2

    move-object/from16 v10, p5

    move-wide/from16 v11, p6

    move-object/from16 v13, p8

    iget-wide v1, v0, Lhn0;->b:D

    cmpl-double v1, v11, v1

    const/4 v14, 0x0

    if-lez v1, :cond_0

    return v14

    :cond_0
    iget v15, v10, Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;->squarelength:I

    iget v1, v10, Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;->bottomCornerHeight:I

    iget v2, v10, Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;->topCornerHeight:I

    sub-int/2addr v1, v2

    sub-int/2addr v1, v15

    invoke-static {}, Lgk;->a()I

    move-result v2

    iget-object v3, v9, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;->sheetMarkers:[[Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    array-length v3, v3

    const/16 v16, 0x1

    add-int/lit8 v3, v3, -0x1

    mul-int/lit8 v3, v3, 0x5

    add-int/lit8 v3, v3, 0x1

    mul-int/2addr v2, v3

    div-int v7, v1, v2

    move v5, v14

    move/from16 v17, v5

    :goto_0
    mul-int/lit8 v6, v15, 0x2

    if-ge v5, v6, :cond_8

    const-wide/high16 v18, 0x3ff8000000000000L    # 1.5

    if-ge v5, v15, :cond_5

    move v4, v5

    :goto_1
    const/4 v1, -0x1

    if-le v4, v1, :cond_4

    iget v1, v10, Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;->topCornerWidth:I

    add-int v3, v1, v4

    iget v1, v10, Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;->topCornerHeight:I

    add-int/2addr v1, v5

    sub-int v2, v1, v4

    invoke-virtual {v8, v3, v2}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v1

    invoke-static {v1, v11, v12}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->k(ID)Z

    move-result v1

    if-eqz v1, :cond_2

    move/from16 v23, v15

    iget-wide v14, v0, Lhn0;->a:D

    move-object/from16 v1, p1

    move/from16 v20, v2

    move v2, v3

    move/from16 v21, v3

    move/from16 v3, v20

    move v0, v4

    move v4, v7

    move/from16 v22, v5

    move/from16 v24, v6

    move-wide v5, v14

    invoke-static/range {v1 .. v6}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->j(Landroid/graphics/Bitmap;IIID)Z

    move-result v1

    if-eqz v1, :cond_1

    int-to-double v1, v7

    mul-double v1, v1, v18

    double-to-int v4, v1

    move-object/from16 v1, p1

    move/from16 v2, v21

    move/from16 v3, v20

    move-wide/from16 v5, p6

    invoke-static/range {v1 .. v6}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->i(Landroid/graphics/Bitmap;IIID)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v5, 0x0

    move-object/from16 v1, p1

    move v2, v7

    move/from16 v3, v21

    move/from16 v4, v20

    move v14, v7

    move-wide/from16 v6, p6

    invoke-static/range {v1 .. v7}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->c(Landroid/graphics/Bitmap;IIIZD)Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    move-result-object v1

    invoke-static {v8, v1, v14, v11, v12}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->l(Landroid/graphics/Bitmap;Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;ID)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v0, v9, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;->sheetMarkers:[[Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    const/4 v2, 0x0

    aget-object v3, v0, v2

    aput-object v1, v3, v2

    add-int/lit8 v6, v24, 0x1

    new-instance v1, Lem;

    aget-object v0, v0, v2

    aget-object v0, v0, v2

    iget-wide v2, v0, Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;->x:D

    double-to-int v2, v2

    iget-wide v3, v0, Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;->y:D

    double-to-int v0, v3

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v21

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v22

    move-object/from16 v17, v1

    move/from16 v18, v2

    move/from16 v19, v0

    move/from16 v20, v14

    invoke-direct/range {v17 .. v22}, Lem;-><init>(IIIII)V

    invoke-virtual {v13, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v5, v6

    move/from16 v17, v16

    goto :goto_3

    :cond_1
    move v14, v7

    goto :goto_2

    :cond_2
    move v0, v4

    move/from16 v22, v5

    move/from16 v24, v6

    move v14, v7

    move/from16 v23, v15

    :cond_3
    :goto_2
    int-to-double v0, v0

    sub-double v0, v0, p3

    double-to-int v4, v0

    move-object/from16 v0, p0

    move v7, v14

    move/from16 v5, v22

    move/from16 v15, v23

    move/from16 v6, v24

    const/4 v14, 0x0

    goto/16 :goto_1

    :cond_4
    move/from16 v22, v5

    move v14, v7

    move/from16 v23, v15

    :goto_3
    const/4 v2, 0x0

    goto/16 :goto_5

    :cond_5
    move/from16 v22, v5

    move/from16 v24, v6

    move v14, v7

    move/from16 v23, v15

    move/from16 v0, v23

    :goto_4
    sub-int v5, v22, v23

    if-le v0, v5, :cond_7

    iget v1, v10, Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;->topCornerWidth:I

    add-int v7, v1, v0

    iget v1, v10, Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;->topCornerHeight:I

    add-int v1, v1, v22

    sub-int v15, v1, v0

    invoke-virtual {v8, v7, v15}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v1

    invoke-static {v1, v11, v12}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->k(ID)Z

    move-result v1

    if-eqz v1, :cond_6

    move-object/from16 v5, p0

    iget-wide v3, v5, Lhn0;->a:D

    move-object/from16 v1, p1

    move v2, v7

    move-wide/from16 v20, v3

    move v3, v15

    move v4, v14

    move-wide/from16 v5, v20

    invoke-static/range {v1 .. v6}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->j(Landroid/graphics/Bitmap;IIID)Z

    move-result v1

    if-eqz v1, :cond_6

    int-to-double v1, v14

    mul-double v1, v1, v18

    double-to-int v4, v1

    move-object/from16 v1, p1

    move v2, v7

    move v3, v15

    move-wide/from16 v5, p6

    invoke-static/range {v1 .. v6}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->i(Landroid/graphics/Bitmap;IIID)Z

    move-result v1

    if-eqz v1, :cond_6

    const/4 v5, 0x0

    move-object/from16 v1, p1

    move v2, v14

    move v3, v7

    move v4, v15

    move-wide/from16 v6, p6

    invoke-static/range {v1 .. v7}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->c(Landroid/graphics/Bitmap;IIIZD)Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    move-result-object v1

    invoke-static {v8, v1, v14, v11, v12}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->l(Landroid/graphics/Bitmap;Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;ID)Z

    move-result v2

    if-eqz v2, :cond_6

    iget-object v0, v9, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;->sheetMarkers:[[Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    const/4 v2, 0x0

    aget-object v3, v0, v2

    aput-object v1, v3, v2

    add-int/lit8 v5, v24, 0x1

    new-instance v1, Lem;

    aget-object v0, v0, v2

    aget-object v0, v0, v2

    iget-wide v3, v0, Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;->x:D

    double-to-int v3, v3

    iget-wide v6, v0, Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;->y:D

    double-to-int v0, v6

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v21

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v22

    move-object/from16 v17, v1

    move/from16 v18, v3

    move/from16 v19, v0

    move/from16 v20, v14

    invoke-direct/range {v17 .. v22}, Lem;-><init>(IIIII)V

    invoke-virtual {v13, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move/from16 v17, v16

    goto :goto_5

    :cond_6
    const/4 v2, 0x0

    int-to-double v0, v0

    sub-double v0, v0, p3

    double-to-int v0, v0

    goto/16 :goto_4

    :cond_7
    const/4 v2, 0x0

    move/from16 v5, v22

    :goto_5
    int-to-double v0, v5

    add-double v0, v0, p3

    double-to-int v5, v0

    move-object/from16 v0, p0

    move v7, v14

    move/from16 v15, v23

    move v14, v2

    goto/16 :goto_0

    :cond_8
    return v17
.end method

.method public final c(Landroid/graphics/Bitmap;Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;DLcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;DLjava/util/ArrayList;)Z
    .locals 27

    .line 1
    move-object/from16 v0, p0

    move-object/from16 v8, p1

    move-object/from16 v9, p2

    move-object/from16 v10, p5

    move-wide/from16 v11, p6

    move-object/from16 v13, p8

    iget-wide v1, v0, Lhn0;->b:D

    cmpl-double v1, v11, v1

    const/4 v14, 0x0

    if-lez v1, :cond_0

    return v14

    :cond_0
    iget v15, v10, Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;->squarelength:I

    invoke-static {}, Lgk;->a()I

    move-result v16

    iget v1, v10, Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;->bottomCornerHeight:I

    iget v2, v10, Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;->topCornerHeight:I

    sub-int/2addr v1, v2

    sub-int/2addr v1, v15

    invoke-static {}, Lgk;->a()I

    move-result v2

    iget-object v3, v9, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;->sheetMarkers:[[Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    array-length v3, v3

    const/16 v17, 0x1

    add-int/lit8 v3, v3, -0x1

    mul-int/lit8 v3, v3, 0x5

    add-int/lit8 v3, v3, 0x1

    mul-int/2addr v2, v3

    div-int v7, v1, v2

    move v5, v14

    move/from16 v18, v5

    :goto_0
    mul-int v1, v15, v16

    if-ge v5, v1, :cond_9

    const-wide/high16 v19, 0x3ff8000000000000L    # 1.5

    if-ge v5, v15, :cond_5

    move v6, v5

    :goto_1
    const/4 v1, -0x1

    if-le v6, v1, :cond_4

    iget v1, v10, Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;->bottomCornerWidth:I

    add-int/lit8 v1, v1, -0x1

    sub-int v4, v1, v6

    iget v1, v10, Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;->bottomCornerHeight:I

    add-int/lit8 v1, v1, -0x1

    sub-int/2addr v1, v5

    add-int v3, v1, v6

    invoke-virtual {v8, v4, v3}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v1

    invoke-static {v1, v11, v12}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->k(ID)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-wide v1, v0, Lhn0;->a:D

    move-wide/from16 v21, v1

    move-object/from16 v1, p1

    move v2, v4

    move/from16 v23, v3

    move/from16 v24, v4

    move v4, v7

    move/from16 v25, v5

    move/from16 v26, v6

    move-wide/from16 v5, v21

    invoke-static/range {v1 .. v6}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->j(Landroid/graphics/Bitmap;IIID)Z

    move-result v1

    if-eqz v1, :cond_1

    int-to-double v1, v7

    mul-double v1, v1, v19

    double-to-int v4, v1

    move-object/from16 v1, p1

    move/from16 v2, v24

    move/from16 v3, v23

    move-wide/from16 v5, p6

    invoke-static/range {v1 .. v6}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->i(Landroid/graphics/Bitmap;IIID)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v5, 0x0

    move-object/from16 v1, p1

    move v2, v7

    move/from16 v3, v24

    move/from16 v4, v23

    move v14, v7

    move-wide/from16 v6, p6

    invoke-static/range {v1 .. v7}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->c(Landroid/graphics/Bitmap;IIIZD)Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    move-result-object v1

    invoke-static {v8, v1, v14, v11, v12}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->l(Landroid/graphics/Bitmap;Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;ID)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, v9, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;->sheetMarkers:[[Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    aget-object v3, v2, v3

    const/4 v4, 0x0

    aget-object v5, v2, v4

    array-length v5, v5

    add-int/lit8 v5, v5, -0x1

    aput-object v1, v3, v5

    mul-int/lit8 v1, v15, 0x2

    add-int/lit8 v1, v1, 0x1

    new-instance v3, Lem;

    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    aget-object v5, v2, v5

    aget-object v6, v2, v4

    array-length v4, v6

    add-int/lit8 v4, v4, -0x1

    aget-object v4, v5, v4

    iget-wide v4, v4, Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;->x:D

    double-to-int v4, v4

    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    aget-object v2, v2, v5

    array-length v5, v6

    add-int/lit8 v5, v5, -0x1

    aget-object v2, v2, v5

    iget-wide v5, v2, Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;->y:D

    double-to-int v2, v5

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v22

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v23

    move-object/from16 v18, v3

    move/from16 v19, v4

    move/from16 v20, v2

    move/from16 v21, v14

    invoke-direct/range {v18 .. v23}, Lem;-><init>(IIIII)V

    invoke-virtual {v13, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v5, v1

    move/from16 v18, v17

    goto :goto_3

    :cond_1
    move v14, v7

    :cond_2
    move/from16 v5, v26

    goto :goto_2

    :cond_3
    move/from16 v25, v5

    move v14, v7

    move v5, v6

    :goto_2
    int-to-double v1, v5

    sub-double v1, v1, p3

    double-to-int v6, v1

    move v7, v14

    move/from16 v5, v25

    const/4 v14, 0x0

    goto/16 :goto_1

    :cond_4
    move/from16 v25, v5

    move v14, v7

    :goto_3
    const/4 v3, 0x0

    goto/16 :goto_5

    :cond_5
    move/from16 v25, v5

    move v14, v7

    move v7, v15

    :goto_4
    sub-int v5, v25, v15

    if-le v7, v5, :cond_8

    iget v1, v10, Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;->bottomCornerWidth:I

    add-int/lit8 v1, v1, -0x1

    sub-int v5, v1, v7

    iget v1, v10, Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;->bottomCornerHeight:I

    add-int/lit8 v1, v1, -0x1

    sub-int v1, v1, v25

    add-int v6, v1, v7

    invoke-virtual {v8, v5, v6}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v1

    invoke-static {v1, v11, v12}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->k(ID)Z

    move-result v1

    if-eqz v1, :cond_6

    iget-wide v3, v0, Lhn0;->a:D

    move-object/from16 v1, p1

    move v2, v5

    move-wide/from16 v21, v3

    move v3, v6

    move v4, v14

    move/from16 v23, v5

    move/from16 v26, v6

    move-wide/from16 v5, v21

    invoke-static/range {v1 .. v6}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->j(Landroid/graphics/Bitmap;IIID)Z

    move-result v1

    if-eqz v1, :cond_6

    int-to-double v1, v14

    mul-double v1, v1, v19

    double-to-int v4, v1

    move-object/from16 v1, p1

    move/from16 v2, v23

    move/from16 v3, v26

    move-wide/from16 v5, p6

    invoke-static/range {v1 .. v6}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->i(Landroid/graphics/Bitmap;IIID)Z

    move-result v1

    if-eqz v1, :cond_6

    const/4 v5, 0x0

    move-object/from16 v1, p1

    move v2, v14

    move/from16 v3, v23

    move/from16 v4, v26

    move v0, v7

    move-wide/from16 v6, p6

    invoke-static/range {v1 .. v7}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->c(Landroid/graphics/Bitmap;IIIZD)Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    move-result-object v1

    invoke-static {v8, v1, v14, v11, v12}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->l(Landroid/graphics/Bitmap;Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;ID)Z

    move-result v2

    if-eqz v2, :cond_7

    iget-object v0, v9, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;->sheetMarkers:[[Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    array-length v2, v0

    add-int/lit8 v2, v2, -0x1

    aget-object v2, v0, v2

    const/4 v3, 0x0

    aget-object v4, v0, v3

    array-length v4, v4

    add-int/lit8 v4, v4, -0x1

    aput-object v1, v2, v4

    mul-int/lit8 v1, v15, 0x2

    add-int/lit8 v5, v1, 0x1

    new-instance v1, Lem;

    array-length v2, v0

    add-int/lit8 v2, v2, -0x1

    aget-object v2, v0, v2

    aget-object v4, v0, v3

    array-length v6, v4

    add-int/lit8 v6, v6, -0x1

    aget-object v2, v2, v6

    iget-wide v6, v2, Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;->x:D

    double-to-int v2, v6

    array-length v6, v0

    add-int/lit8 v6, v6, -0x1

    aget-object v0, v0, v6

    array-length v4, v4

    add-int/lit8 v4, v4, -0x1

    aget-object v0, v0, v4

    iget-wide v6, v0, Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;->y:D

    double-to-int v0, v6

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v22

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v23

    move-object/from16 v18, v1

    move/from16 v19, v2

    move/from16 v20, v0

    move/from16 v21, v14

    invoke-direct/range {v18 .. v23}, Lem;-><init>(IIIII)V

    invoke-virtual {v13, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move/from16 v18, v17

    goto :goto_5

    :cond_6
    move v0, v7

    :cond_7
    const/4 v3, 0x0

    int-to-double v0, v0

    sub-double v0, v0, p3

    double-to-int v7, v0

    move-object/from16 v0, p0

    goto/16 :goto_4

    :cond_8
    const/4 v3, 0x0

    move/from16 v5, v25

    :goto_5
    int-to-double v0, v5

    add-double v0, v0, p3

    double-to-int v5, v0

    move-object/from16 v0, p0

    move v7, v14

    move v14, v3

    goto/16 :goto_0

    :cond_9
    return v18
.end method

.method public d(Lj5;Ld91;Lej1;ZI)Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;
    .locals 16

    .line 1
    move-object/from16 v0, p1

    move-object/from16 v1, p2

    move-object/from16 v10, p3

    move/from16 v11, p5

    iget-object v2, v0, Lj5;->c:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    invoke-virtual {v2, v11}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getRows(I)I

    move-result v2

    div-int/lit8 v2, v2, 0x5

    add-int/lit8 v2, v2, 0x1

    iget-object v3, v0, Lj5;->c:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    invoke-virtual {v3, v11}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getColumns(I)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    new-instance v12, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;

    invoke-direct {v12}, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;-><init>()V

    filled-new-array {v2, v3}, [I

    move-result-object v2

    const-class v3, Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    invoke-static {v3, v2}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [[Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    iput-object v2, v12, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;->sheetMarkers:[[Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    iget-object v13, v0, Lj5;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v13}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    invoke-virtual {v12, v2}, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;->setHeight(I)V

    invoke-virtual {v13}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    invoke-virtual {v12, v2}, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;->setWidth(I)V

    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    iget-object v2, v0, Lj5;->b:Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;

    iget v2, v2, Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;->imageHeight:I

    invoke-virtual {v13}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    if-ne v2, v3, :cond_0

    iget-object v2, v0, Lj5;->b:Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;

    :goto_0
    move-object v7, v2

    goto :goto_2

    :cond_0
    new-instance v2, Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;

    invoke-direct {v2}, Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;-><init>()V

    invoke-virtual {v13}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    iput v3, v2, Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;->imageHeight:I

    invoke-virtual {v13}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    iput v3, v2, Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;->imageWidth:I

    iget v4, v2, Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;->imageHeight:I

    iget-object v5, v0, Lj5;->b:Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;

    iget v5, v5, Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;->squarelengthPercent:I

    if-le v3, v4, :cond_1

    mul-int/2addr v5, v4

    goto :goto_1

    :cond_1
    mul-int/2addr v5, v3

    :goto_1
    div-int/lit8 v5, v5, 0x64

    iput v5, v2, Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;->squarelength:I

    iget-object v5, v0, Lj5;->b:Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;

    iget v6, v5, Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;->bottomCornerHeightPercent:I

    mul-int/2addr v6, v4

    div-int/lit8 v6, v6, 0x64

    iput v6, v2, Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;->bottomCornerHeight:I

    iget v6, v5, Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;->bottomCornerWidthPercent:I

    mul-int/2addr v6, v3

    div-int/lit8 v6, v6, 0x64

    iput v6, v2, Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;->bottomCornerWidth:I

    iget v6, v5, Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;->topCornerHeightPercent:I

    mul-int/2addr v6, v4

    div-int/lit8 v6, v6, 0x64

    iput v6, v2, Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;->topCornerHeight:I

    iget v4, v5, Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;->topCornerWidthPercent:I

    mul-int/2addr v4, v3

    div-int/lit8 v4, v4, 0x64

    iput v4, v2, Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;->topCornerWidth:I

    goto :goto_0

    :goto_2
    iget-object v2, v0, Lj5;->c:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    invoke-virtual {v2, v11}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getColumnWidthInBubbles(I)[I

    move-result-object v2

    invoke-static {v2}, Lve1;->e([I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v13}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    iget v4, v7, Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;->topCornerWidth:I

    const/4 v15, 0x2

    mul-int/2addr v4, v15

    sub-int/2addr v3, v4

    iget v4, v7, Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;->squarelength:I

    sub-int/2addr v3, v4

    int-to-double v3, v3

    const-wide/high16 v5, 0x3ff0000000000000L    # 1.0

    mul-double/2addr v3, v5

    int-to-double v5, v2

    const-wide/high16 v8, 0x4014000000000000L    # 5.0

    mul-double/2addr v5, v8

    div-double/2addr v3, v5

    invoke-static {}, Lgk;->a()I

    move-result v2

    div-int/2addr v2, v15

    int-to-double v5, v2

    mul-double/2addr v5, v3

    move-object/from16 v2, p0

    move-object v3, v13

    move-object v4, v12

    move-object/from16 v8, p3

    move-object v9, v14

    invoke-virtual/range {v2 .. v9}, Lhn0;->r(Landroid/graphics/Bitmap;Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;DLcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;Lej1;Ljava/util/ArrayList;)Z

    move-result v2

    if-eqz v1, :cond_2

    if-eqz v2, :cond_2

    const/16 v3, 0x3c

    invoke-interface {v1, v3}, Ld91;->a(I)V

    invoke-interface {v10, v14}, Lej1;->a(Ljava/util/ArrayList;)V

    goto :goto_3

    :cond_2
    invoke-virtual {v14}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-le v3, v15, :cond_3

    sget v3, Lhn0;->d:I

    add-int/lit8 v3, v3, 0x1

    sput v3, Lhn0;->d:I

    goto :goto_3

    :cond_3
    sget v3, Lhn0;->c:I

    add-int/lit8 v3, v3, 0x1

    sput v3, Lhn0;->c:I

    :goto_3
    if-eqz v2, :cond_8

    sget v2, Lhn0;->e:I

    sget v3, Lhn0;->g:I

    if-gt v2, v3, :cond_5

    if-eqz p4, :cond_4

    goto :goto_4

    :cond_4
    iget-object v5, v0, Lj5;->c:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    move-object/from16 v2, p0

    move-object v3, v13

    move-object v4, v12

    move-object/from16 v6, p3

    move-object v7, v14

    move/from16 v8, p5

    invoke-virtual/range {v2 .. v8}, Lhn0;->p(Landroid/graphics/Bitmap;Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;Lej1;Ljava/util/ArrayList;I)Z

    move-result v0

    goto :goto_5

    :cond_5
    :goto_4
    iget-object v5, v0, Lj5;->c:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    move-object/from16 v2, p0

    move-object v3, v13

    move-object v4, v12

    move-object/from16 v6, p3

    move-object v7, v14

    move/from16 v8, p5

    invoke-virtual/range {v2 .. v8}, Lhn0;->o(Landroid/graphics/Bitmap;Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;Lej1;Ljava/util/ArrayList;I)Z

    move-result v0

    :goto_5
    if-eqz v0, :cond_7

    move-object/from16 v0, p0

    invoke-virtual {v0, v12}, Lhn0;->i(Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;)Z

    move-result v2

    if-eqz v2, :cond_6

    const/16 v2, 0x5a

    invoke-interface {v1, v2}, Ld91;->a(I)V

    return-object v12

    :cond_6
    sget v1, Lhn0;->f:I

    add-int/lit8 v1, v1, 0x1

    sput v1, Lhn0;->f:I

    goto :goto_6

    :cond_7
    move-object/from16 v0, p0

    sget v1, Lhn0;->e:I

    add-int/lit8 v1, v1, 0x1

    sput v1, Lhn0;->e:I

    goto :goto_6

    :cond_8
    move-object/from16 v0, p0

    :goto_6
    invoke-interface {v10, v14}, Lej1;->a(Ljava/util/ArrayList;)V

    const/4 v1, 0x0

    return-object v1
.end method

.method public final e(Landroid/graphics/Bitmap;Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;DLcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;DLjava/util/ArrayList;)Z
    .locals 25

    .line 1
    move-object/from16 v0, p0

    move-object/from16 v8, p1

    move-object/from16 v9, p2

    move-object/from16 v10, p5

    move-wide/from16 v11, p6

    move-object/from16 v13, p8

    iget-wide v1, v0, Lhn0;->b:D

    cmpl-double v1, v11, v1

    const/4 v14, 0x0

    if-lez v1, :cond_0

    return v14

    :cond_0
    iget v15, v10, Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;->squarelength:I

    invoke-static {}, Lgk;->a()I

    iget v1, v10, Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;->bottomCornerHeight:I

    iget v2, v10, Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;->topCornerHeight:I

    sub-int/2addr v1, v2

    sub-int/2addr v1, v15

    invoke-static {}, Lgk;->a()I

    move-result v2

    iget-object v3, v9, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;->sheetMarkers:[[Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    array-length v3, v3

    const/16 v16, 0x1

    add-int/lit8 v3, v3, -0x1

    mul-int/lit8 v3, v3, 0x5

    add-int/lit8 v3, v3, 0x1

    mul-int/2addr v2, v3

    div-int v7, v1, v2

    move v5, v14

    move/from16 v17, v5

    :goto_0
    mul-int/lit8 v6, v15, 0x2

    if-ge v5, v6, :cond_8

    const-wide/high16 v18, 0x3ff8000000000000L    # 1.5

    if-ge v5, v15, :cond_5

    move v4, v5

    :goto_1
    const/4 v1, -0x1

    if-le v4, v1, :cond_4

    iget v1, v10, Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;->bottomCornerWidth:I

    add-int/lit8 v1, v1, -0x1

    sub-int v3, v1, v4

    iget v1, v10, Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;->topCornerHeight:I

    add-int/2addr v1, v5

    sub-int v2, v1, v4

    invoke-virtual {v8, v3, v2}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v1

    invoke-static {v1, v11, v12}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->k(ID)Z

    move-result v1

    if-eqz v1, :cond_2

    move/from16 v23, v15

    iget-wide v14, v0, Lhn0;->a:D

    move-object/from16 v1, p1

    move/from16 v20, v2

    move v2, v3

    move/from16 v21, v3

    move/from16 v3, v20

    move v0, v4

    move v4, v7

    move/from16 v22, v5

    move/from16 v24, v6

    move-wide v5, v14

    invoke-static/range {v1 .. v6}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->j(Landroid/graphics/Bitmap;IIID)Z

    move-result v1

    if-eqz v1, :cond_1

    int-to-double v1, v7

    mul-double v1, v1, v18

    double-to-int v4, v1

    move-object/from16 v1, p1

    move/from16 v2, v21

    move/from16 v3, v20

    move-wide/from16 v5, p6

    invoke-static/range {v1 .. v6}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->i(Landroid/graphics/Bitmap;IIID)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v5, 0x0

    move-object/from16 v1, p1

    move v2, v7

    move/from16 v3, v21

    move/from16 v4, v20

    move v14, v7

    move-wide/from16 v6, p6

    invoke-static/range {v1 .. v7}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->c(Landroid/graphics/Bitmap;IIIZD)Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    move-result-object v1

    invoke-static {v8, v1, v14, v11, v12}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->l(Landroid/graphics/Bitmap;Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;ID)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v0, v9, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;->sheetMarkers:[[Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    const/4 v2, 0x0

    aget-object v3, v0, v2

    array-length v4, v3

    add-int/lit8 v4, v4, -0x1

    aput-object v1, v3, v4

    add-int/lit8 v6, v24, 0x1

    new-instance v1, Lem;

    aget-object v0, v0, v2

    array-length v2, v0

    add-int/lit8 v2, v2, -0x1

    aget-object v2, v0, v2

    iget-wide v2, v2, Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;->x:D

    double-to-int v2, v2

    array-length v3, v0

    add-int/lit8 v3, v3, -0x1

    aget-object v0, v0, v3

    iget-wide v3, v0, Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;->y:D

    double-to-int v0, v3

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v21

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v22

    move-object/from16 v17, v1

    move/from16 v18, v2

    move/from16 v19, v0

    move/from16 v20, v14

    invoke-direct/range {v17 .. v22}, Lem;-><init>(IIIII)V

    invoke-virtual {v13, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v5, v6

    move/from16 v17, v16

    goto :goto_3

    :cond_1
    move v14, v7

    goto :goto_2

    :cond_2
    move v0, v4

    move/from16 v22, v5

    move/from16 v24, v6

    move v14, v7

    move/from16 v23, v15

    :cond_3
    :goto_2
    int-to-double v0, v0

    sub-double v0, v0, p3

    double-to-int v4, v0

    move-object/from16 v0, p0

    move v7, v14

    move/from16 v5, v22

    move/from16 v15, v23

    move/from16 v6, v24

    const/4 v14, 0x0

    goto/16 :goto_1

    :cond_4
    move/from16 v22, v5

    move v14, v7

    move/from16 v23, v15

    :goto_3
    const/4 v2, 0x0

    goto/16 :goto_5

    :cond_5
    move/from16 v22, v5

    move/from16 v24, v6

    move v14, v7

    move/from16 v23, v15

    move/from16 v0, v23

    :goto_4
    sub-int v5, v22, v23

    if-le v0, v5, :cond_7

    iget v1, v10, Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;->bottomCornerWidth:I

    add-int/lit8 v1, v1, -0x1

    sub-int v7, v1, v0

    iget v1, v10, Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;->topCornerHeight:I

    add-int v1, v1, v22

    sub-int v15, v1, v0

    invoke-virtual {v8, v7, v15}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v1

    invoke-static {v1, v11, v12}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->k(ID)Z

    move-result v1

    if-eqz v1, :cond_6

    move-object/from16 v5, p0

    iget-wide v3, v5, Lhn0;->a:D

    move-object/from16 v1, p1

    move v2, v7

    move-wide/from16 v20, v3

    move v3, v15

    move v4, v14

    move-wide/from16 v5, v20

    invoke-static/range {v1 .. v6}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->j(Landroid/graphics/Bitmap;IIID)Z

    move-result v1

    if-eqz v1, :cond_6

    int-to-double v1, v14

    mul-double v1, v1, v18

    double-to-int v4, v1

    move-object/from16 v1, p1

    move v2, v7

    move v3, v15

    move-wide/from16 v5, p6

    invoke-static/range {v1 .. v6}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->i(Landroid/graphics/Bitmap;IIID)Z

    move-result v1

    if-eqz v1, :cond_6

    const/4 v5, 0x0

    move-object/from16 v1, p1

    move v2, v14

    move v3, v7

    move v4, v15

    move-wide/from16 v6, p6

    invoke-static/range {v1 .. v7}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->c(Landroid/graphics/Bitmap;IIIZD)Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    move-result-object v1

    invoke-static {v8, v1, v14, v11, v12}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->l(Landroid/graphics/Bitmap;Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;ID)Z

    move-result v2

    if-eqz v2, :cond_6

    iget-object v0, v9, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;->sheetMarkers:[[Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    const/4 v2, 0x0

    aget-object v3, v0, v2

    array-length v4, v3

    add-int/lit8 v4, v4, -0x1

    aput-object v1, v3, v4

    add-int/lit8 v5, v24, 0x1

    new-instance v1, Lem;

    aget-object v0, v0, v2

    array-length v3, v0

    add-int/lit8 v3, v3, -0x1

    aget-object v3, v0, v3

    iget-wide v3, v3, Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;->x:D

    double-to-int v3, v3

    array-length v4, v0

    add-int/lit8 v4, v4, -0x1

    aget-object v0, v0, v4

    iget-wide v6, v0, Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;->y:D

    double-to-int v0, v6

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v21

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v22

    move-object/from16 v17, v1

    move/from16 v18, v3

    move/from16 v19, v0

    move/from16 v20, v14

    invoke-direct/range {v17 .. v22}, Lem;-><init>(IIIII)V

    invoke-virtual {v13, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move/from16 v17, v16

    goto :goto_5

    :cond_6
    const/4 v2, 0x0

    int-to-double v0, v0

    sub-double v0, v0, p3

    double-to-int v0, v0

    goto/16 :goto_4

    :cond_7
    const/4 v2, 0x0

    move/from16 v5, v22

    :goto_5
    int-to-double v0, v5

    add-double v0, v0, p3

    double-to-int v5, v0

    move-object/from16 v0, p0

    move v7, v14

    move/from16 v15, v23

    move v14, v2

    goto/16 :goto_0

    :cond_8
    return v17
.end method

.method public final f([[Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;)I
    .locals 6

    .line 1
    const/4 v0, 0x0

    move v1, v0

    move v2, v1

    :goto_0
    array-length v3, p1

    if-ge v1, v3, :cond_2

    move v3, v0

    :goto_1
    aget-object v4, p1, v1

    array-length v5, v4

    if-ge v3, v5, :cond_1

    aget-object v4, v4, v3

    if-nez v4, :cond_0

    add-int/lit8 v2, v2, 0x1

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    return v2
.end method

.method public final g(Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;I)D
    .locals 6

    .line 1
    iget-wide v0, p2, Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;->x:D

    iget-wide v2, p1, Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;->x:D

    sub-double/2addr v0, v2

    iget-wide v4, p2, Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;->y:D

    iget-wide p1, p1, Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;->y:D

    sub-double/2addr v4, p1

    div-double/2addr v0, v4

    int-to-double v4, p3

    sub-double/2addr p1, v4

    mul-double/2addr v0, p1

    sub-double/2addr v2, v0

    return-wide v2
.end method

.method public final h(Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;I)D
    .locals 6

    .line 1
    iget-wide v0, p2, Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;->y:D

    iget-wide v2, p1, Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;->y:D

    sub-double/2addr v0, v2

    iget-wide v4, p2, Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;->x:D

    iget-wide p1, p1, Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;->x:D

    sub-double/2addr v4, p1

    div-double/2addr v0, v4

    int-to-double v4, p3

    sub-double/2addr p1, v4

    mul-double/2addr v0, p1

    sub-double/2addr v2, v0

    return-wide v2
.end method

.method public final i(Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;)Z
    .locals 13

    .line 1
    iget-object p1, p1, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;->sheetMarkers:[[Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    array-length v2, p1

    add-int/lit8 v2, v2, -0x2

    const-wide v3, 0x3ff3333333333333L    # 1.2

    const-wide v5, 0x3fe999999999999aL    # 0.8

    if-ge v1, v2, :cond_2

    aget-object v2, p1, v1

    aget-object v2, v2, v0

    iget-wide v7, v2, Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;->y:D

    add-int/lit8 v2, v1, 0x1

    aget-object v9, p1, v2

    aget-object v9, v9, v0

    iget-wide v9, v9, Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;->y:D

    sub-double/2addr v7, v9

    add-int/lit8 v1, v1, 0x2

    aget-object v1, p1, v1

    aget-object v1, v1, v0

    iget-wide v11, v1, Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;->y:D

    sub-double/2addr v9, v11

    div-double/2addr v7, v9

    cmpl-double v1, v7, v3

    if-gtz v1, :cond_1

    cmpg-double v1, v7, v5

    if-gez v1, :cond_0

    goto :goto_1

    :cond_0
    move v1, v2

    goto :goto_0

    :cond_1
    :goto_1
    return v0

    :cond_2
    move v1, v0

    :goto_2
    array-length v2, p1

    add-int/lit8 v2, v2, -0x2

    const/4 v7, 0x1

    if-ge v1, v2, :cond_5

    aget-object v2, p1, v0

    array-length v2, v2

    sub-int/2addr v2, v7

    aget-object v7, p1, v1

    aget-object v7, v7, v2

    iget-wide v7, v7, Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;->y:D

    add-int/lit8 v9, v1, 0x1

    aget-object v10, p1, v9

    aget-object v10, v10, v2

    iget-wide v10, v10, Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;->y:D

    sub-double/2addr v7, v10

    add-int/lit8 v1, v1, 0x2

    aget-object v1, p1, v1

    aget-object v1, v1, v2

    iget-wide v1, v1, Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;->y:D

    sub-double/2addr v10, v1

    div-double/2addr v7, v10

    cmpl-double v1, v7, v3

    if-gtz v1, :cond_4

    cmpg-double v1, v7, v5

    if-gez v1, :cond_3

    goto :goto_3

    :cond_3
    move v1, v9

    goto :goto_2

    :cond_4
    :goto_3
    return v0

    :cond_5
    return v7
.end method

.method public final k(Landroid/graphics/Bitmap;Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;I)Z
    .locals 17

    .line 1
    move-object/from16 v0, p2

    iget-object v1, v0, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;->sheetMarkers:[[Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    array-length v1, v1

    const/4 v2, 0x1

    move v3, v2

    :goto_0
    add-int/lit8 v4, v1, -0x1

    if-ge v3, v4, :cond_1

    iget-object v5, v0, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;->sheetMarkers:[[Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    add-int/lit8 v6, v3, -0x1

    aget-object v7, v5, v6

    aget-object v7, v7, p4

    aget-object v5, v5, v4

    aget-object v5, v5, p4

    add-int/lit8 v8, v1, 0x1

    sub-int/2addr v8, v3

    invoke-static {v7, v5, v2, v8}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->g(Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;II)Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    move-result-object v11

    invoke-virtual/range {p2 .. p2}, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;->getBubbleSize()D

    move-result-wide v12

    iget-object v5, v0, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;->sheetMarkers:[[Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    aget-object v6, v5, v6

    aget-object v14, v6, p4

    aget-object v4, v5, v4

    aget-object v15, v4, p4

    const/16 v16, 0x1

    move-object/from16 v9, p0

    move-object/from16 v10, p1

    invoke-virtual/range {v9 .. v16}, Lhn0;->s(Landroid/graphics/Bitmap;Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;DLcom/ekodroid/omrevaluator/templateui/models/Point2Double;Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;Z)Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    move-result-object v4

    if-eqz v4, :cond_0

    iget-object v5, v0, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;->sheetMarkers:[[Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    aget-object v5, v5, v3

    aput-object v4, v5, p4

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    return v0

    :cond_1
    return v2
.end method

.method public final l(Landroid/graphics/Bitmap;Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;I)I
    .locals 19

    .line 1
    move-object/from16 v0, p2

    iget-object v1, v0, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;->sheetMarkers:[[Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    array-length v1, v1

    const/4 v2, 0x0

    const/4 v3, 0x1

    move v5, v2

    move v4, v3

    :goto_0
    add-int/lit8 v6, v1, -0x1

    if-ge v4, v6, :cond_3

    iget-object v7, v0, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;->sheetMarkers:[[Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    aget-object v8, v7, v4

    aget-object v8, v8, p4

    if-eqz v8, :cond_0

    goto :goto_1

    :cond_0
    add-int/lit8 v8, v4, -0x1

    aget-object v9, v7, v8

    aget-object v9, v9, p4

    if-eqz v9, :cond_1

    aget-object v7, v7, v6

    aget-object v7, v7, p4

    add-int/lit8 v10, v1, 0x1

    sub-int/2addr v10, v4

    invoke-static {v9, v7, v3, v10}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->g(Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;II)Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    move-result-object v13

    invoke-virtual/range {p2 .. p2}, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;->getBubbleSize()D

    move-result-wide v14

    iget-object v7, v0, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;->sheetMarkers:[[Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    aget-object v8, v7, v8

    aget-object v16, v8, p4

    aget-object v6, v7, v6

    aget-object v17, v6, p4

    const/16 v18, 0x1

    move-object/from16 v11, p0

    move-object/from16 v12, p1

    invoke-virtual/range {v11 .. v18}, Lhn0;->s(Landroid/graphics/Bitmap;Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;DLcom/ekodroid/omrevaluator/templateui/models/Point2Double;Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;Z)Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    move-result-object v6

    if-eqz v6, :cond_2

    iget-object v7, v0, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;->sheetMarkers:[[Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    aget-object v7, v7, v4

    aput-object v6, v7, p4

    goto :goto_1

    :cond_1
    aget-object v8, v7, v2

    aget-object v8, v8, p4

    aget-object v7, v7, v6

    aget-object v7, v7, p4

    invoke-static {v8, v7, v4, v1}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->g(Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;II)Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    move-result-object v11

    invoke-virtual/range {p2 .. p2}, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;->getBubbleSize()D

    move-result-wide v12

    iget-object v7, v0, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;->sheetMarkers:[[Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    aget-object v8, v7, v2

    aget-object v14, v8, p4

    aget-object v6, v7, v6

    aget-object v15, v6, p4

    const/16 v16, 0x1

    move-object/from16 v9, p0

    move-object/from16 v10, p1

    invoke-virtual/range {v9 .. v16}, Lhn0;->s(Landroid/graphics/Bitmap;Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;DLcom/ekodroid/omrevaluator/templateui/models/Point2Double;Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;Z)Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    move-result-object v6

    if-eqz v6, :cond_2

    iget-object v7, v0, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;->sheetMarkers:[[Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    aget-object v7, v7, v4

    aput-object v6, v7, p4

    goto :goto_1

    :cond_2
    add-int/lit8 v5, v5, 0x1

    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_3
    return v5
.end method

.method public final m(Landroid/graphics/Bitmap;Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;II)Z
    .locals 22

    .line 1
    move-object/from16 v0, p2

    move-object/from16 v1, p3

    move/from16 v2, p5

    iget-object v3, v0, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;->sheetMarkers:[[Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    const/4 v4, 0x0

    aget-object v3, v3, v4

    array-length v3, v3

    invoke-virtual {v1, v2}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getColumnWidthInBubbles(I)[I

    move-result-object v5

    invoke-static {v5}, Lve1;->e([I)I

    move-result v5

    const/4 v6, 0x1

    add-int/2addr v5, v6

    move v7, v6

    :goto_0
    add-int/lit8 v8, v3, -0x1

    if-ge v7, v8, :cond_1

    invoke-virtual {v1, v2}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getColumnWidthInBubbles(I)[I

    move-result-object v9

    add-int/lit8 v10, v7, -0x1

    invoke-static {v9, v10}, Lve1;->f([II)I

    move-result v9

    sub-int v9, v5, v9

    invoke-virtual {v1, v2}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getColumnWidthInBubbles(I)[I

    move-result-object v11

    aget v11, v11, v10

    iget-object v12, v0, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;->sheetMarkers:[[Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    aget-object v12, v12, p4

    aget-object v13, v12, v10

    aget-object v12, v12, v8

    invoke-static {v13, v12, v11, v9}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->g(Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;II)Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    move-result-object v16

    invoke-virtual/range {p2 .. p2}, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;->getBubbleSize()D

    move-result-wide v17

    iget-object v9, v0, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;->sheetMarkers:[[Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    aget-object v9, v9, p4

    aget-object v19, v9, v10

    aget-object v20, v9, v8

    const/16 v21, 0x0

    move-object/from16 v14, p0

    move-object/from16 v15, p1

    invoke-virtual/range {v14 .. v21}, Lhn0;->s(Landroid/graphics/Bitmap;Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;DLcom/ekodroid/omrevaluator/templateui/models/Point2Double;Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;Z)Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    move-result-object v8

    if-eqz v8, :cond_0

    iget-object v9, v0, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;->sheetMarkers:[[Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    aget-object v9, v9, p4

    aput-object v8, v9, v7

    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    :cond_0
    return v4

    :cond_1
    return v6
.end method

.method public final n(Landroid/graphics/Bitmap;Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;II)I
    .locals 23

    .line 1
    move-object/from16 v0, p2

    move-object/from16 v1, p3

    move/from16 v2, p5

    iget-object v3, v0, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;->sheetMarkers:[[Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    const/4 v4, 0x0

    aget-object v3, v3, v4

    array-length v3, v3

    invoke-virtual {v1, v2}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getColumnWidthInBubbles(I)[I

    move-result-object v5

    invoke-static {v5}, Lve1;->e([I)I

    move-result v5

    const/4 v6, 0x1

    add-int/2addr v5, v6

    move v8, v4

    move v7, v6

    :goto_0
    add-int/lit8 v9, v3, -0x1

    if-ge v7, v9, :cond_3

    iget-object v10, v0, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;->sheetMarkers:[[Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    aget-object v10, v10, p4

    aget-object v11, v10, v7

    if-eqz v11, :cond_0

    goto/16 :goto_1

    :cond_0
    add-int/lit8 v11, v7, -0x1

    aget-object v10, v10, v11

    if-eqz v10, :cond_1

    invoke-virtual {v1, v2}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getColumnWidthInBubbles(I)[I

    move-result-object v10

    invoke-static {v10, v11}, Lve1;->f([II)I

    move-result v10

    sub-int v10, v5, v10

    invoke-virtual {v1, v2}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getColumnWidthInBubbles(I)[I

    move-result-object v12

    aget v12, v12, v11

    iget-object v13, v0, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;->sheetMarkers:[[Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    aget-object v13, v13, p4

    aget-object v14, v13, v11

    aget-object v13, v13, v9

    invoke-static {v14, v13, v12, v10}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->g(Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;II)Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    move-result-object v17

    invoke-virtual/range {p2 .. p2}, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;->getBubbleSize()D

    move-result-wide v18

    iget-object v10, v0, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;->sheetMarkers:[[Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    aget-object v10, v10, p4

    aget-object v20, v10, v11

    aget-object v21, v10, v9

    const/16 v22, 0x0

    move-object/from16 v15, p0

    move-object/from16 v16, p1

    invoke-virtual/range {v15 .. v22}, Lhn0;->s(Landroid/graphics/Bitmap;Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;DLcom/ekodroid/omrevaluator/templateui/models/Point2Double;Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;Z)Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    move-result-object v9

    if-eqz v9, :cond_2

    iget-object v10, v0, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;->sheetMarkers:[[Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    aget-object v10, v10, p4

    aput-object v9, v10, v7

    goto :goto_1

    :cond_1
    invoke-virtual {v1, v2}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getColumnWidthInBubbles(I)[I

    move-result-object v10

    invoke-static {v10, v7}, Lve1;->f([II)I

    move-result v10

    iget-object v11, v0, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;->sheetMarkers:[[Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    aget-object v11, v11, p4

    aget-object v12, v11, v4

    aget-object v11, v11, v9

    invoke-static {v12, v11, v10, v5}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->g(Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;II)Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    move-result-object v15

    invoke-virtual/range {p2 .. p2}, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;->getBubbleSize()D

    move-result-wide v16

    iget-object v10, v0, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;->sheetMarkers:[[Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    aget-object v10, v10, p4

    aget-object v18, v10, v4

    aget-object v19, v10, v9

    const/16 v20, 0x0

    move-object/from16 v13, p0

    move-object/from16 v14, p1

    invoke-virtual/range {v13 .. v20}, Lhn0;->s(Landroid/graphics/Bitmap;Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;DLcom/ekodroid/omrevaluator/templateui/models/Point2Double;Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;Z)Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    move-result-object v9

    if-eqz v9, :cond_2

    iget-object v10, v0, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;->sheetMarkers:[[Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    aget-object v10, v10, p4

    aput-object v9, v10, v7

    goto :goto_1

    :cond_2
    add-int/lit8 v8, v8, 0x1

    :goto_1
    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_0

    :cond_3
    return v8
.end method

.method public final o(Landroid/graphics/Bitmap;Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;Lej1;Ljava/util/ArrayList;I)Z
    .locals 17

    .line 1
    move-object/from16 v6, p0

    move-object/from16 v7, p1

    move-object/from16 v8, p2

    move-object/from16 v9, p3

    move/from16 v10, p6

    iget-object v0, v8, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;->sheetMarkers:[[Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    array-length v11, v0

    const/4 v12, 0x0

    aget-object v0, v0, v12

    array-length v13, v0

    const/4 v4, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move/from16 v5, p6

    invoke-virtual/range {v0 .. v5}, Lhn0;->n(Landroid/graphics/Bitmap;Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;II)I

    add-int/lit8 v14, v11, -0x1

    move v4, v14

    invoke-virtual/range {v0 .. v5}, Lhn0;->n(Landroid/graphics/Bitmap;Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;II)I

    move v0, v12

    :goto_0
    if-ge v0, v13, :cond_1

    iget-object v1, v8, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;->sheetMarkers:[[Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    aget-object v2, v1, v12

    aget-object v2, v2, v0

    if-eqz v2, :cond_0

    aget-object v1, v1, v14

    aget-object v1, v1, v0

    if-eqz v1, :cond_0

    invoke-virtual {v6, v7, v8, v9, v0}, Lhn0;->l(Landroid/graphics/Bitmap;Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;I)I

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iget-object v0, v8, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;->sheetMarkers:[[Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    invoke-virtual {v6, v0}, Lhn0;->f([[Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;)I

    move-result v0

    const/4 v15, 0x1

    if-nez v0, :cond_2

    return v15

    :cond_2
    move v5, v15

    :goto_1
    iget-object v0, v8, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;->sheetMarkers:[[Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    if-ge v5, v14, :cond_4

    aget-object v0, v0, v5

    aget-object v1, v0, v12

    if-eqz v1, :cond_3

    add-int/lit8 v1, v13, -0x1

    aget-object v0, v0, v1

    if-eqz v0, :cond_3

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move v4, v5

    move/from16 v16, v5

    move/from16 v5, p6

    invoke-virtual/range {v0 .. v5}, Lhn0;->n(Landroid/graphics/Bitmap;Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;II)I

    goto :goto_2

    :cond_3
    move/from16 v16, v5

    :goto_2
    add-int/lit8 v5, v16, 0x1

    goto :goto_1

    :cond_4
    invoke-virtual {v6, v0}, Lhn0;->f([[Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;)I

    move-result v0

    if-nez v0, :cond_5

    return v15

    :cond_5
    int-to-double v0, v0

    mul-int/2addr v11, v13

    int-to-double v2, v11

    const-wide/high16 v4, 0x3fd0000000000000L    # 0.25

    mul-double/2addr v2, v4

    cmpl-double v0, v0, v2

    if-lez v0, :cond_6

    return v12

    :cond_6
    invoke-virtual {v6, v7, v8, v9, v10}, Lhn0;->q(Landroid/graphics/Bitmap;Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;I)I

    iget-object v0, v8, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;->sheetMarkers:[[Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    invoke-virtual {v6, v0}, Lhn0;->f([[Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;)I

    move-result v0

    if-nez v0, :cond_7

    return v15

    :cond_7
    invoke-virtual {v6, v7, v8, v9, v10}, Lhn0;->q(Landroid/graphics/Bitmap;Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;I)I

    iget-object v0, v8, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;->sheetMarkers:[[Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    invoke-virtual {v6, v0}, Lhn0;->f([[Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;)I

    move-result v0

    if-nez v0, :cond_8

    return v15

    :cond_8
    return v12
.end method

.method public final p(Landroid/graphics/Bitmap;Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;Lej1;Ljava/util/ArrayList;I)Z
    .locals 8

    .line 1
    iget-object p4, p2, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;->sheetMarkers:[[Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    array-length p5, p4

    const/4 v0, 0x0

    aget-object p4, p4, v0

    array-length p4, p4

    const/4 v5, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v6, p6

    invoke-virtual/range {v1 .. v6}, Lhn0;->m(Landroid/graphics/Bitmap;Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;II)Z

    move-result v1

    if-nez v1, :cond_0

    return v0

    :cond_0
    const/4 v1, 0x1

    add-int/lit8 v6, p5, -0x1

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move v7, p6

    invoke-virtual/range {v2 .. v7}, Lhn0;->m(Landroid/graphics/Bitmap;Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;II)Z

    move-result p5

    if-nez p5, :cond_1

    return v0

    :cond_1
    move p5, v0

    :goto_0
    if-ge p5, p4, :cond_3

    invoke-virtual {p0, p1, p2, p3, p5}, Lhn0;->k(Landroid/graphics/Bitmap;Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;I)Z

    move-result p6

    if-nez p6, :cond_2

    return v0

    :cond_2
    add-int/lit8 p5, p5, 0x1

    goto :goto_0

    :cond_3
    return v1
.end method

.method public final q(Landroid/graphics/Bitmap;Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;I)I
    .locals 28

    .line 1
    move-object/from16 v0, p2

    iget-object v1, v0, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;->sheetMarkers:[[Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    array-length v2, v1

    const/4 v3, 0x0

    aget-object v1, v1, v3

    array-length v1, v1

    invoke-virtual/range {p3 .. p4}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getColumnWidthInBubbles(I)[I

    move-result-object v4

    invoke-static {v4}, Lve1;->e([I)I

    move-result v4

    const/4 v5, 0x1

    add-int/2addr v4, v5

    move v6, v3

    move v7, v6

    :goto_0
    if-ge v6, v2, :cond_9

    move v8, v3

    :goto_1
    if-ge v8, v1, :cond_8

    iget-object v9, v0, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;->sheetMarkers:[[Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    aget-object v10, v9, v6

    aget-object v11, v10, v8

    if-nez v11, :cond_7

    aget-object v11, v9, v3

    aget-object v11, v11, v8

    if-eqz v11, :cond_3

    add-int/lit8 v12, v2, -0x1

    aget-object v13, v9, v12

    aget-object v13, v13, v8

    if-eqz v13, :cond_3

    add-int/lit8 v10, v6, -0x1

    aget-object v9, v9, v10

    aget-object v9, v9, v8

    if-eqz v9, :cond_1

    add-int/lit8 v11, v2, 0x1

    sub-int/2addr v11, v6

    invoke-static {v9, v13, v5, v11}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->g(Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;II)Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    move-result-object v9

    invoke-virtual/range {p2 .. p2}, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;->getBubbleSize()D

    move-result-wide v17

    iget-object v11, v0, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;->sheetMarkers:[[Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    aget-object v10, v11, v10

    aget-object v19, v10, v8

    aget-object v10, v11, v12

    aget-object v20, v10, v8

    const/16 v21, 0x1

    move-object/from16 v14, p0

    move-object/from16 v15, p1

    move-object/from16 v16, v9

    invoke-virtual/range {v14 .. v21}, Lhn0;->s(Landroid/graphics/Bitmap;Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;DLcom/ekodroid/omrevaluator/templateui/models/Point2Double;Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;Z)Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    move-result-object v10

    if-eqz v10, :cond_0

    iget-object v9, v0, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;->sheetMarkers:[[Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    aget-object v9, v9, v6

    aput-object v10, v9, v8

    goto/16 :goto_3

    :cond_0
    iget-object v10, v0, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;->sheetMarkers:[[Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    aget-object v10, v10, v6

    aput-object v9, v10, v8

    iget-object v10, v0, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;->predictedMarkers:Ljava/util/ArrayList;

    invoke-virtual {v10, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_2
    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_3

    :cond_1
    invoke-static {v11, v13, v6, v2}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->g(Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;II)Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    move-result-object v15

    invoke-virtual/range {p2 .. p2}, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;->getBubbleSize()D

    move-result-wide v13

    iget-object v9, v0, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;->sheetMarkers:[[Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    aget-object v10, v9, v3

    aget-object v16, v10, v8

    aget-object v9, v9, v12

    aget-object v17, v9, v8

    const/16 v18, 0x1

    move-object/from16 v9, p0

    move-object/from16 v10, p1

    move-object v11, v15

    move-wide v12, v13

    move-object/from16 v14, v16

    move-object v5, v15

    move-object/from16 v15, v17

    move/from16 v16, v18

    invoke-virtual/range {v9 .. v16}, Lhn0;->s(Landroid/graphics/Bitmap;Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;DLcom/ekodroid/omrevaluator/templateui/models/Point2Double;Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;Z)Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    move-result-object v9

    if-eqz v9, :cond_2

    iget-object v5, v0, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;->sheetMarkers:[[Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    aget-object v5, v5, v6

    aput-object v9, v5, v8

    goto/16 :goto_3

    :cond_2
    iget-object v9, v0, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;->sheetMarkers:[[Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    aget-object v9, v9, v6

    aput-object v5, v9, v8

    iget-object v9, v0, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;->predictedMarkers:Ljava/util/ArrayList;

    invoke-virtual {v9, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_3
    aget-object v5, v10, v3

    if-eqz v5, :cond_7

    add-int/lit8 v5, v1, -0x1

    aget-object v9, v10, v5

    if-eqz v9, :cond_7

    add-int/lit8 v9, v8, -0x1

    aget-object v10, v10, v9

    if-eqz v10, :cond_5

    invoke-virtual/range {p3 .. p4}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getColumnWidthInBubbles(I)[I

    move-result-object v10

    invoke-static {v10, v9}, Lve1;->f([II)I

    move-result v10

    sub-int v10, v4, v10

    invoke-virtual/range {p3 .. p4}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getColumnWidthInBubbles(I)[I

    move-result-object v11

    aget v11, v11, v9

    iget-object v12, v0, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;->sheetMarkers:[[Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    aget-object v12, v12, v6

    aget-object v13, v12, v9

    aget-object v12, v12, v5

    invoke-static {v13, v12, v11, v10}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->g(Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;II)Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    move-result-object v10

    invoke-virtual/range {p2 .. p2}, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;->getBubbleSize()D

    move-result-wide v23

    iget-object v11, v0, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;->sheetMarkers:[[Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    aget-object v11, v11, v6

    aget-object v25, v11, v9

    aget-object v26, v11, v5

    const/16 v27, 0x0

    move-object/from16 v20, p0

    move-object/from16 v21, p1

    move-object/from16 v22, v10

    invoke-virtual/range {v20 .. v27}, Lhn0;->s(Landroid/graphics/Bitmap;Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;DLcom/ekodroid/omrevaluator/templateui/models/Point2Double;Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;Z)Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    move-result-object v5

    if-eqz v5, :cond_4

    iget-object v9, v0, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;->sheetMarkers:[[Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    aget-object v9, v9, v6

    aput-object v5, v9, v8

    goto :goto_3

    :cond_4
    iget-object v5, v0, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;->sheetMarkers:[[Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    aget-object v5, v5, v6

    aput-object v10, v5, v8

    iget-object v5, v0, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;->predictedMarkers:Ljava/util/ArrayList;

    invoke-virtual {v5, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    :cond_5
    invoke-virtual/range {p3 .. p4}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getColumnWidthInBubbles(I)[I

    move-result-object v9

    invoke-static {v9, v8}, Lve1;->f([II)I

    move-result v9

    iget-object v10, v0, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;->sheetMarkers:[[Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    aget-object v10, v10, v6

    aget-object v11, v10, v3

    aget-object v10, v10, v5

    invoke-static {v11, v10, v9, v4}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->g(Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;II)Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    move-result-object v9

    invoke-virtual/range {p2 .. p2}, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;->getBubbleSize()D

    move-result-wide v23

    iget-object v10, v0, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;->sheetMarkers:[[Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    aget-object v10, v10, v6

    aget-object v25, v10, v3

    aget-object v26, v10, v5

    const/16 v27, 0x0

    move-object/from16 v20, p0

    move-object/from16 v21, p1

    move-object/from16 v22, v9

    invoke-virtual/range {v20 .. v27}, Lhn0;->s(Landroid/graphics/Bitmap;Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;DLcom/ekodroid/omrevaluator/templateui/models/Point2Double;Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;Z)Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    move-result-object v5

    if-eqz v5, :cond_6

    iget-object v9, v0, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;->sheetMarkers:[[Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    aget-object v9, v9, v6

    aput-object v5, v9, v8

    goto :goto_3

    :cond_6
    add-int/lit8 v7, v7, 0x1

    iget-object v5, v0, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;->sheetMarkers:[[Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    aget-object v5, v5, v6

    aput-object v9, v5, v8

    iget-object v5, v0, Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;->predictedMarkers:Ljava/util/ArrayList;

    invoke-virtual {v5, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_7
    :goto_3
    add-int/lit8 v8, v8, 0x1

    const/4 v5, 0x1

    goto/16 :goto_1

    :cond_8
    add-int/lit8 v6, v6, 0x1

    const/4 v5, 0x1

    goto/16 :goto_0

    :cond_9
    return v7
.end method

.method public final r(Landroid/graphics/Bitmap;Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;DLcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;Lej1;Ljava/util/ArrayList;)Z
    .locals 14

    .line 1
    move-object/from16 v9, p5

    iget v10, v9, Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;->squarelength:I

    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Ljava/util/Random;->nextInt(I)I

    move-result v0

    rem-int/2addr v0, v1

    const/4 v11, 0x1

    const/4 v12, 0x0

    if-eqz v0, :cond_c

    if-eq v0, v11, :cond_8

    const/4 v1, 0x2

    if-eq v0, v1, :cond_4

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    goto/16 :goto_0

    :cond_0
    iget v0, v9, Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;->topCornerWidth:I

    div-int/lit8 v13, v10, 0x2

    add-int v1, v0, v13

    iget v0, v9, Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;->bottomCornerHeight:I

    sub-int v2, v0, v13

    const/16 v5, 0x19

    sget-wide v6, Lhn0;->h:D

    move-object v0, p1

    move v3, v10

    move v4, v10

    invoke-static/range {v0 .. v7}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->b(Landroid/graphics/Bitmap;IIIIID)D

    move-result-wide v6

    move-object v0, p0

    move-object v1, p1

    move-object/from16 v2, p2

    move-wide/from16 v3, p3

    move-object/from16 v5, p5

    move-object/from16 v8, p7

    invoke-virtual/range {v0 .. v8}, Lhn0;->a(Landroid/graphics/Bitmap;Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;DLcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;DLjava/util/ArrayList;)Z

    move-result v0

    if-nez v0, :cond_1

    return v12

    :cond_1
    iget v0, v9, Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;->topCornerWidth:I

    add-int v1, v0, v13

    iget v0, v9, Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;->topCornerHeight:I

    add-int v2, v0, v13

    const/16 v5, 0x19

    sget-wide v6, Lhn0;->h:D

    move-object v0, p1

    move v3, v10

    move v4, v10

    invoke-static/range {v0 .. v7}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->b(Landroid/graphics/Bitmap;IIIIID)D

    move-result-wide v6

    move-object v0, p0

    move-object v1, p1

    move-object/from16 v2, p2

    move-wide/from16 v3, p3

    move-object/from16 v5, p5

    move-object/from16 v8, p7

    invoke-virtual/range {v0 .. v8}, Lhn0;->b(Landroid/graphics/Bitmap;Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;DLcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;DLjava/util/ArrayList;)Z

    move-result v0

    if-nez v0, :cond_2

    return v12

    :cond_2
    iget v0, v9, Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;->bottomCornerWidth:I

    sub-int v1, v0, v13

    iget v0, v9, Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;->topCornerHeight:I

    add-int v2, v0, v13

    const/16 v5, 0x19

    sget-wide v6, Lhn0;->h:D

    move-object v0, p1

    move v3, v10

    move v4, v10

    invoke-static/range {v0 .. v7}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->b(Landroid/graphics/Bitmap;IIIIID)D

    move-result-wide v6

    move-object v0, p0

    move-object v1, p1

    move-object/from16 v2, p2

    move-wide/from16 v3, p3

    move-object/from16 v5, p5

    move-object/from16 v8, p7

    invoke-virtual/range {v0 .. v8}, Lhn0;->e(Landroid/graphics/Bitmap;Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;DLcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;DLjava/util/ArrayList;)Z

    move-result v0

    if-nez v0, :cond_3

    return v12

    :cond_3
    iget v0, v9, Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;->bottomCornerWidth:I

    sub-int v1, v0, v13

    iget v0, v9, Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;->bottomCornerHeight:I

    sub-int v2, v0, v13

    const/16 v5, 0x19

    sget-wide v6, Lhn0;->h:D

    move-object v0, p1

    move v3, v10

    move v4, v10

    invoke-static/range {v0 .. v7}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->b(Landroid/graphics/Bitmap;IIIIID)D

    move-result-wide v6

    move-object v0, p0

    move-object v1, p1

    move-object/from16 v2, p2

    move-wide/from16 v3, p3

    move-object/from16 v5, p5

    move-object/from16 v8, p7

    invoke-virtual/range {v0 .. v8}, Lhn0;->c(Landroid/graphics/Bitmap;Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;DLcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;DLjava/util/ArrayList;)Z

    move-result v0

    if-nez v0, :cond_10

    return v12

    :cond_4
    iget v0, v9, Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;->bottomCornerWidth:I

    div-int/lit8 v13, v10, 0x2

    sub-int v1, v0, v13

    iget v0, v9, Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;->bottomCornerHeight:I

    sub-int v2, v0, v13

    const/16 v5, 0x19

    sget-wide v6, Lhn0;->h:D

    move-object v0, p1

    move v3, v10

    move v4, v10

    invoke-static/range {v0 .. v7}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->b(Landroid/graphics/Bitmap;IIIIID)D

    move-result-wide v6

    move-object v0, p0

    move-object v1, p1

    move-object/from16 v2, p2

    move-wide/from16 v3, p3

    move-object/from16 v5, p5

    move-object/from16 v8, p7

    invoke-virtual/range {v0 .. v8}, Lhn0;->c(Landroid/graphics/Bitmap;Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;DLcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;DLjava/util/ArrayList;)Z

    move-result v0

    if-nez v0, :cond_5

    return v12

    :cond_5
    iget v0, v9, Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;->topCornerWidth:I

    add-int v1, v0, v13

    iget v0, v9, Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;->bottomCornerHeight:I

    sub-int v2, v0, v13

    const/16 v5, 0x19

    sget-wide v6, Lhn0;->h:D

    move-object v0, p1

    move v3, v10

    move v4, v10

    invoke-static/range {v0 .. v7}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->b(Landroid/graphics/Bitmap;IIIIID)D

    move-result-wide v6

    move-object v0, p0

    move-object v1, p1

    move-object/from16 v2, p2

    move-wide/from16 v3, p3

    move-object/from16 v5, p5

    move-object/from16 v8, p7

    invoke-virtual/range {v0 .. v8}, Lhn0;->a(Landroid/graphics/Bitmap;Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;DLcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;DLjava/util/ArrayList;)Z

    move-result v0

    if-nez v0, :cond_6

    return v12

    :cond_6
    iget v0, v9, Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;->topCornerWidth:I

    add-int v1, v0, v13

    iget v0, v9, Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;->topCornerHeight:I

    add-int v2, v0, v13

    const/16 v5, 0x19

    sget-wide v6, Lhn0;->h:D

    move-object v0, p1

    move v3, v10

    move v4, v10

    invoke-static/range {v0 .. v7}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->b(Landroid/graphics/Bitmap;IIIIID)D

    move-result-wide v6

    move-object v0, p0

    move-object v1, p1

    move-object/from16 v2, p2

    move-wide/from16 v3, p3

    move-object/from16 v5, p5

    move-object/from16 v8, p7

    invoke-virtual/range {v0 .. v8}, Lhn0;->b(Landroid/graphics/Bitmap;Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;DLcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;DLjava/util/ArrayList;)Z

    move-result v0

    if-nez v0, :cond_7

    return v12

    :cond_7
    iget v0, v9, Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;->bottomCornerWidth:I

    sub-int v1, v0, v13

    iget v0, v9, Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;->topCornerHeight:I

    add-int v2, v0, v13

    const/16 v5, 0x19

    sget-wide v6, Lhn0;->h:D

    move-object v0, p1

    move v3, v10

    move v4, v10

    invoke-static/range {v0 .. v7}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->b(Landroid/graphics/Bitmap;IIIIID)D

    move-result-wide v6

    move-object v0, p0

    move-object v1, p1

    move-object/from16 v2, p2

    move-wide/from16 v3, p3

    move-object/from16 v5, p5

    move-object/from16 v8, p7

    invoke-virtual/range {v0 .. v8}, Lhn0;->e(Landroid/graphics/Bitmap;Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;DLcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;DLjava/util/ArrayList;)Z

    move-result v0

    if-nez v0, :cond_10

    return v12

    :cond_8
    iget v0, v9, Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;->bottomCornerWidth:I

    div-int/lit8 v13, v10, 0x2

    sub-int v1, v0, v13

    iget v0, v9, Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;->topCornerHeight:I

    add-int v2, v0, v13

    const/16 v5, 0x19

    sget-wide v6, Lhn0;->h:D

    move-object v0, p1

    move v3, v10

    move v4, v10

    invoke-static/range {v0 .. v7}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->b(Landroid/graphics/Bitmap;IIIIID)D

    move-result-wide v6

    move-object v0, p0

    move-object v1, p1

    move-object/from16 v2, p2

    move-wide/from16 v3, p3

    move-object/from16 v5, p5

    move-object/from16 v8, p7

    invoke-virtual/range {v0 .. v8}, Lhn0;->e(Landroid/graphics/Bitmap;Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;DLcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;DLjava/util/ArrayList;)Z

    move-result v0

    if-nez v0, :cond_9

    return v12

    :cond_9
    iget v0, v9, Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;->bottomCornerWidth:I

    sub-int v1, v0, v13

    iget v0, v9, Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;->bottomCornerHeight:I

    sub-int v2, v0, v13

    const/16 v5, 0x19

    sget-wide v6, Lhn0;->h:D

    move-object v0, p1

    move v3, v10

    move v4, v10

    invoke-static/range {v0 .. v7}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->b(Landroid/graphics/Bitmap;IIIIID)D

    move-result-wide v6

    move-object v0, p0

    move-object v1, p1

    move-object/from16 v2, p2

    move-wide/from16 v3, p3

    move-object/from16 v5, p5

    move-object/from16 v8, p7

    invoke-virtual/range {v0 .. v8}, Lhn0;->c(Landroid/graphics/Bitmap;Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;DLcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;DLjava/util/ArrayList;)Z

    move-result v0

    if-nez v0, :cond_a

    return v12

    :cond_a
    iget v0, v9, Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;->topCornerWidth:I

    add-int v1, v0, v13

    iget v0, v9, Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;->bottomCornerHeight:I

    sub-int v2, v0, v13

    const/16 v5, 0x19

    sget-wide v6, Lhn0;->h:D

    move-object v0, p1

    move v3, v10

    move v4, v10

    invoke-static/range {v0 .. v7}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->b(Landroid/graphics/Bitmap;IIIIID)D

    move-result-wide v6

    move-object v0, p0

    move-object v1, p1

    move-object/from16 v2, p2

    move-wide/from16 v3, p3

    move-object/from16 v5, p5

    move-object/from16 v8, p7

    invoke-virtual/range {v0 .. v8}, Lhn0;->a(Landroid/graphics/Bitmap;Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;DLcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;DLjava/util/ArrayList;)Z

    move-result v0

    if-nez v0, :cond_b

    return v12

    :cond_b
    iget v0, v9, Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;->topCornerWidth:I

    add-int v1, v0, v13

    iget v0, v9, Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;->topCornerHeight:I

    add-int v2, v0, v13

    const/16 v5, 0x19

    sget-wide v6, Lhn0;->h:D

    move-object v0, p1

    move v3, v10

    move v4, v10

    invoke-static/range {v0 .. v7}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->b(Landroid/graphics/Bitmap;IIIIID)D

    move-result-wide v6

    move-object v0, p0

    move-object v1, p1

    move-object/from16 v2, p2

    move-wide/from16 v3, p3

    move-object/from16 v5, p5

    move-object/from16 v8, p7

    invoke-virtual/range {v0 .. v8}, Lhn0;->b(Landroid/graphics/Bitmap;Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;DLcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;DLjava/util/ArrayList;)Z

    move-result v0

    if-nez v0, :cond_10

    return v12

    :cond_c
    iget v0, v9, Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;->topCornerWidth:I

    div-int/lit8 v13, v10, 0x2

    add-int v1, v0, v13

    iget v0, v9, Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;->topCornerHeight:I

    add-int v2, v0, v13

    const/16 v5, 0x19

    sget-wide v6, Lhn0;->h:D

    move-object v0, p1

    move v3, v10

    move v4, v10

    invoke-static/range {v0 .. v7}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->b(Landroid/graphics/Bitmap;IIIIID)D

    move-result-wide v6

    move-object v0, p0

    move-object v1, p1

    move-object/from16 v2, p2

    move-wide/from16 v3, p3

    move-object/from16 v5, p5

    move-object/from16 v8, p7

    invoke-virtual/range {v0 .. v8}, Lhn0;->b(Landroid/graphics/Bitmap;Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;DLcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;DLjava/util/ArrayList;)Z

    move-result v0

    if-nez v0, :cond_d

    return v12

    :cond_d
    iget v0, v9, Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;->bottomCornerWidth:I

    sub-int v1, v0, v13

    iget v0, v9, Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;->topCornerHeight:I

    add-int v2, v0, v13

    const/16 v5, 0x19

    sget-wide v6, Lhn0;->h:D

    move-object v0, p1

    move v3, v10

    move v4, v10

    invoke-static/range {v0 .. v7}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->b(Landroid/graphics/Bitmap;IIIIID)D

    move-result-wide v6

    move-object v0, p0

    move-object v1, p1

    move-object/from16 v2, p2

    move-wide/from16 v3, p3

    move-object/from16 v5, p5

    move-object/from16 v8, p7

    invoke-virtual/range {v0 .. v8}, Lhn0;->e(Landroid/graphics/Bitmap;Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;DLcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;DLjava/util/ArrayList;)Z

    move-result v0

    if-nez v0, :cond_e

    return v12

    :cond_e
    iget v0, v9, Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;->bottomCornerWidth:I

    sub-int v1, v0, v13

    iget v0, v9, Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;->bottomCornerHeight:I

    sub-int v2, v0, v13

    const/16 v5, 0x19

    sget-wide v6, Lhn0;->h:D

    move-object v0, p1

    move v3, v10

    move v4, v10

    invoke-static/range {v0 .. v7}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->b(Landroid/graphics/Bitmap;IIIIID)D

    move-result-wide v6

    move-object v0, p0

    move-object v1, p1

    move-object/from16 v2, p2

    move-wide/from16 v3, p3

    move-object/from16 v5, p5

    move-object/from16 v8, p7

    invoke-virtual/range {v0 .. v8}, Lhn0;->c(Landroid/graphics/Bitmap;Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;DLcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;DLjava/util/ArrayList;)Z

    move-result v0

    if-nez v0, :cond_f

    return v12

    :cond_f
    iget v0, v9, Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;->topCornerWidth:I

    add-int v1, v0, v13

    iget v0, v9, Lcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;->bottomCornerHeight:I

    sub-int v2, v0, v13

    const/16 v5, 0x19

    sget-wide v6, Lhn0;->h:D

    move-object v0, p1

    move v3, v10

    move v4, v10

    invoke-static/range {v0 .. v7}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->b(Landroid/graphics/Bitmap;IIIIID)D

    move-result-wide v6

    move-object v0, p0

    move-object v1, p1

    move-object/from16 v2, p2

    move-wide/from16 v3, p3

    move-object/from16 v5, p5

    move-object/from16 v8, p7

    invoke-virtual/range {v0 .. v8}, Lhn0;->a(Landroid/graphics/Bitmap;Lcom/ekodroid/omrevaluator/templateui/scanner/SheetDimension;DLcom/ekodroid/omrevaluator/serializable/SheetImageConfigure;DLjava/util/ArrayList;)Z

    move-result v0

    if-nez v0, :cond_10

    return v12

    :cond_10
    :goto_0
    return v11
.end method

.method public final s(Landroid/graphics/Bitmap;Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;DLcom/ekodroid/omrevaluator/templateui/models/Point2Double;Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;Z)Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;
    .locals 16

    .line 1
    move-object/from16 v0, p0

    move-object/from16 v9, p1

    move-object/from16 v10, p2

    move-object/from16 v11, p5

    move-object/from16 v12, p6

    const-wide/high16 v1, 0x3ff0000000000000L    # 1.0

    add-double v1, p3, v1

    double-to-int v13, v1

    iget-wide v1, v10, Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;->x:D

    double-to-int v2, v1

    iget-wide v3, v10, Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;->y:D

    double-to-int v3, v3

    mul-int/lit8 v5, v13, 0x2

    const/16 v6, 0x32

    sget-wide v7, Lhn0;->h:D

    move-object/from16 v1, p1

    move v4, v5

    invoke-static/range {v1 .. v8}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->b(Landroid/graphics/Bitmap;IIIIID)D

    move-result-wide v6

    iget-wide v1, v10, Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;->x:D

    double-to-int v1, v1

    iget-wide v2, v10, Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;->y:D

    double-to-int v2, v2

    invoke-virtual {v9, v1, v2}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v1

    invoke-static {v1, v6, v7}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->k(ID)Z

    move-result v1

    const/4 v8, 0x0

    if-eqz v1, :cond_1

    iget-wide v1, v10, Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;->x:D

    double-to-int v3, v1

    iget-wide v1, v10, Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;->y:D

    double-to-int v4, v1

    const/4 v5, 0x0

    move-object/from16 v1, p1

    move v2, v13

    invoke-static/range {v1 .. v7}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->c(Landroid/graphics/Bitmap;IIIZD)Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    move-result-object v1

    iget-wide v2, v1, Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;->x:D

    double-to-int v2, v2

    iget-wide v3, v1, Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;->y:D

    double-to-int v3, v3

    const-wide v4, 0x3fd3333333333333L    # 0.3

    move/from16 p2, v2

    move/from16 p3, v3

    move/from16 p4, v13

    move-wide/from16 p5, v4

    invoke-static/range {p1 .. p6}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->j(Landroid/graphics/Bitmap;IIID)Z

    move-result v2

    if-nez v2, :cond_0

    return-object v8

    :cond_0
    return-object v1

    :cond_1
    div-int/lit8 v1, v13, 0x3

    move v2, v1

    if-eqz p7, :cond_6

    :goto_0
    mul-int/lit8 v3, v1, 0x6

    if-ge v2, v3, :cond_b

    iget-wide v3, v10, Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;->y:D

    double-to-int v3, v3

    add-int v4, v3, v2

    invoke-virtual {v0, v11, v12, v4}, Lhn0;->g(Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;I)D

    move-result-wide v14

    double-to-int v3, v14

    invoke-virtual {v9, v3, v4}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v5

    invoke-static {v5, v6, v7}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->k(ID)Z

    move-result v5

    if-eqz v5, :cond_3

    const/4 v5, 0x0

    move-object/from16 v1, p1

    move v2, v13

    invoke-static/range {v1 .. v7}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->c(Landroid/graphics/Bitmap;IIIZD)Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    move-result-object v1

    iget-wide v2, v1, Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;->x:D

    double-to-int v2, v2

    iget-wide v3, v1, Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;->y:D

    double-to-int v3, v3

    const-wide v4, 0x3fd3333333333333L    # 0.3

    move/from16 p2, v2

    move/from16 p3, v3

    move/from16 p4, v13

    move-wide/from16 p5, v4

    invoke-static/range {p1 .. p6}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->j(Landroid/graphics/Bitmap;IIID)Z

    move-result v2

    if-nez v2, :cond_2

    return-object v8

    :cond_2
    return-object v1

    :cond_3
    iget-wide v3, v10, Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;->y:D

    double-to-int v3, v3

    sub-int v4, v3, v2

    invoke-virtual {v0, v11, v12, v4}, Lhn0;->g(Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;I)D

    move-result-wide v14

    double-to-int v3, v14

    invoke-virtual {v9, v3, v4}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v5

    invoke-static {v5, v6, v7}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->k(ID)Z

    move-result v5

    if-eqz v5, :cond_5

    const/4 v5, 0x0

    move-object/from16 v1, p1

    move v2, v13

    invoke-static/range {v1 .. v7}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->c(Landroid/graphics/Bitmap;IIIZD)Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    move-result-object v1

    iget-wide v2, v1, Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;->x:D

    double-to-int v2, v2

    iget-wide v3, v1, Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;->y:D

    double-to-int v3, v3

    const-wide v4, 0x3fd3333333333333L    # 0.3

    move/from16 p2, v2

    move/from16 p3, v3

    move/from16 p4, v13

    move-wide/from16 p5, v4

    invoke-static/range {p1 .. p6}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->j(Landroid/graphics/Bitmap;IIID)Z

    move-result v2

    if-nez v2, :cond_4

    return-object v8

    :cond_4
    return-object v1

    :cond_5
    add-int/2addr v2, v1

    goto :goto_0

    :cond_6
    :goto_1
    mul-int/lit8 v3, v1, 0x3

    if-ge v2, v3, :cond_b

    iget-wide v3, v10, Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;->x:D

    double-to-int v3, v3

    add-int/2addr v3, v2

    invoke-virtual {v0, v11, v12, v3}, Lhn0;->h(Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;I)D

    move-result-wide v4

    double-to-int v4, v4

    invoke-virtual {v9, v3, v4}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v5

    invoke-static {v5, v6, v7}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->k(ID)Z

    move-result v5

    if-eqz v5, :cond_8

    const/4 v5, 0x0

    move-object/from16 v1, p1

    move v2, v13

    invoke-static/range {v1 .. v7}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->c(Landroid/graphics/Bitmap;IIIZD)Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    move-result-object v1

    iget-wide v2, v1, Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;->x:D

    double-to-int v2, v2

    iget-wide v3, v1, Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;->y:D

    double-to-int v3, v3

    const-wide v4, 0x3fd3333333333333L    # 0.3

    move/from16 p2, v2

    move/from16 p3, v3

    move/from16 p4, v13

    move-wide/from16 p5, v4

    invoke-static/range {p1 .. p6}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->j(Landroid/graphics/Bitmap;IIID)Z

    move-result v2

    if-nez v2, :cond_7

    return-object v8

    :cond_7
    return-object v1

    :cond_8
    iget-wide v3, v10, Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;->x:D

    double-to-int v3, v3

    sub-int/2addr v3, v2

    invoke-virtual {v0, v11, v12, v3}, Lhn0;->h(Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;I)D

    move-result-wide v4

    double-to-int v4, v4

    invoke-virtual {v9, v3, v4}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v5

    invoke-static {v5, v6, v7}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->k(ID)Z

    move-result v5

    if-eqz v5, :cond_a

    const/4 v5, 0x0

    move-object/from16 v1, p1

    move v2, v13

    invoke-static/range {v1 .. v7}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->c(Landroid/graphics/Bitmap;IIIZD)Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;

    move-result-object v1

    iget-wide v2, v1, Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;->x:D

    double-to-int v2, v2

    iget-wide v3, v1, Lcom/ekodroid/omrevaluator/templateui/models/Point2Double;->y:D

    double-to-int v3, v3

    const-wide v4, 0x3fd3333333333333L    # 0.3

    move/from16 p2, v2

    move/from16 p3, v3

    move/from16 p4, v13

    move-wide/from16 p5, v4

    invoke-static/range {p1 .. p6}, Lcom/ekodroid/omrevaluator/templateui/scanner/ScannerUtil;->j(Landroid/graphics/Bitmap;IIID)Z

    move-result v2

    if-nez v2, :cond_9

    return-object v8

    :cond_9
    return-object v1

    :cond_a
    add-int/2addr v2, v1

    goto :goto_1

    :cond_b
    return-object v8
.end method

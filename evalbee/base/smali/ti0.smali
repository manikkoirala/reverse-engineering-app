.class public Lti0;
.super Lu80;
.source "SourceFile"


# instance fields
.field public a:Landroid/content/Context;

.field public b:Ljava/lang/String;

.field public c:Ly01;

.field public d:[Ljava/lang/String;

.field public e:[Landroid/widget/EditText;

.field public f:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ly01;[Ljava/lang/String;I)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lu80;-><init>(Landroid/content/Context;)V

    iput-object p1, p0, Lti0;->a:Landroid/content/Context;

    iput-object p2, p0, Lti0;->b:Ljava/lang/String;

    iput-object p3, p0, Lti0;->c:Ly01;

    iput-object p4, p0, Lti0;->d:[Ljava/lang/String;

    iput p5, p0, Lti0;->f:I

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .line 1
    const v0, 0x7f090447

    invoke-virtual {p0, v0}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroidx/appcompat/widget/Toolbar;

    iget-object v1, p0, Lti0;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroidx/appcompat/widget/Toolbar;->setTitle(Ljava/lang/CharSequence;)V

    new-instance v1, Lti0$a;

    invoke-direct {v1, p0}, Lti0$a;-><init>(Lti0;)V

    invoke-virtual {v0, v1}, Landroidx/appcompat/widget/Toolbar;->setNavigationOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 11

    .line 1
    invoke-super {p0, p1}, Lu80;->onCreate(Landroid/os/Bundle;)V

    const p1, 0x7f0c006f

    invoke-virtual {p0, p1}, Landroid/app/Dialog;->setContentView(I)V

    invoke-virtual {p0}, Lti0;->a()V

    const p1, 0x7f090209

    invoke-virtual {p0, p1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/LinearLayout;

    iget-object v0, p0, Lti0;->d:[Ljava/lang/String;

    array-length v0, v0

    new-array v0, v0, [Landroid/widget/EditText;

    iput-object v0, p0, Lti0;->e:[Landroid/widget/EditText;

    const/16 v0, 0xc

    iget-object v1, p0, Lti0;->a:Landroid/content/Context;

    invoke-static {v0, v1}, La91;->d(ILandroid/content/Context;)I

    move-result v0

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v2, -0x2

    invoke-direct {v1, v2, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v3, v2, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    const/16 v2, 0x10

    iget-object v4, p0, Lti0;->a:Landroid/content/Context;

    invoke-static {v2, v4}, La91;->d(ILandroid/content/Context;)I

    move-result v2

    const/4 v4, 0x0

    invoke-virtual {v3, v4, v4, v2, v4}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    :goto_0
    iget-object v2, p0, Lti0;->e:[Landroid/widget/EditText;

    array-length v2, v2

    if-ge v4, v2, :cond_0

    iget-object v2, p0, Lti0;->a:Landroid/content/Context;

    invoke-static {v2}, Lyo;->b(Landroid/content/Context;)Landroid/widget/LinearLayout;

    move-result-object v2

    iget-object v5, p0, Lti0;->a:Landroid/content/Context;

    iget-object v6, p0, Lti0;->d:[Ljava/lang/String;

    aget-object v6, v6, v4

    const/16 v7, 0x64

    invoke-static {v5, v6, v7}, Lyo;->c(Landroid/content/Context;Ljava/lang/String;I)Landroid/widget/TextView;

    move-result-object v5

    const/4 v6, 0x6

    invoke-virtual {v5, v6}, Landroid/view/View;->setTextAlignment(I)V

    invoke-virtual {v5, v0, v0, v0, v0}, Landroid/widget/TextView;->setPadding(IIII)V

    iget-object v6, p0, Lti0;->e:[Landroid/widget/EditText;

    iget-object v8, p0, Lti0;->a:Landroid/content/Context;

    iget-object v9, p0, Lti0;->d:[Ljava/lang/String;

    aget-object v9, v9, v4

    iget v10, p0, Lti0;->f:I

    invoke-static {v8, v9, v10, v7}, Lyo;->a(Landroid/content/Context;Ljava/lang/String;II)Landroid/widget/EditText;

    move-result-object v7

    aput-object v7, v6, v4

    iget-object v6, p0, Lti0;->e:[Landroid/widget/EditText;

    aget-object v6, v6, v4

    invoke-virtual {v6, v0, v0, v0, v0}, Landroid/view/View;->setPadding(IIII)V

    invoke-virtual {v2, v5, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v5, p0, Lti0;->e:[Landroid/widget/EditText;

    aget-object v5, v5, v4

    invoke-virtual {v2, v5, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {p1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_0
    const p1, 0x7f0900b1

    invoke-virtual {p0, p1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object p1

    new-instance v0, Lti0$b;

    invoke-direct {v0, p0}, Lti0$b;-><init>(Lti0;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const p1, 0x7f0900d3

    invoke-virtual {p0, p1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object p1

    new-instance v0, Lti0$c;

    invoke-direct {v0, p0}, Lti0$c;-><init>(Lti0;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

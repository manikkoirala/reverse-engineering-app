.class public Lsi0$d;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lsi0;->e()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Ljava/util/ArrayList;

.field public final synthetic b:Lsi0;


# direct methods
.method public constructor <init>(Lsi0;Ljava/util/ArrayList;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lsi0$d;->b:Lsi0;

    iput-object p2, p0, Lsi0$d;->a:Ljava/util/ArrayList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 0

    .line 1
    const/4 p1, 0x0

    if-nez p3, :cond_0

    iget-object p2, p0, Lsi0$d;->b:Lsi0;

    invoke-static {p2, p1}, Lsi0;->b(Lsi0;Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;)Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    return-void

    :cond_0
    const/4 p2, 0x1

    if-ne p3, p2, :cond_1

    iget-object p2, p0, Lsi0$d;->b:Lsi0;

    invoke-static {p2, p1}, Lsi0;->c(Lsi0;Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;)V

    return-void

    :cond_1
    iget-object p1, p0, Lsi0$d;->b:Lsi0;

    iget-object p1, p1, Lsi0;->a:Landroid/content/Context;

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;

    move-result-object p1

    iget-object p2, p0, Lsi0$d;->a:Ljava/util/ArrayList;

    invoke-virtual {p2, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/lang/String;

    invoke-virtual {p1, p2}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getLabelProfileJsonModel(Ljava/lang/String;)Lcom/ekodroid/omrevaluator/database/LabelProfileJsonModel;

    move-result-object p1

    iget-object p2, p0, Lsi0$d;->b:Lsi0;

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/database/LabelProfileJsonModel;->getLabelProfile()Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    move-result-object p1

    invoke-static {p2, p1}, Lsi0;->b(Lsi0;Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;)Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    return-void
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0

    .line 1
    return-void
.end method

.class public Lu62$d;
.super Lu62$f;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lu62;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "d"
.end annotation


# instance fields
.field public final c:Landroid/view/WindowInsets$Builder;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Lu62$f;-><init>()V

    invoke-static {}, Lb72;->a()Landroid/view/WindowInsets$Builder;

    move-result-object v0

    iput-object v0, p0, Lu62$d;->c:Landroid/view/WindowInsets$Builder;

    return-void
.end method

.method public constructor <init>(Lu62;)V
    .locals 0

    .line 2
    invoke-direct {p0, p1}, Lu62$f;-><init>(Lu62;)V

    invoke-virtual {p1}, Lu62;->u()Landroid/view/WindowInsets;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-static {p1}, Lc72;->a(Landroid/view/WindowInsets;)Landroid/view/WindowInsets$Builder;

    move-result-object p1

    goto :goto_0

    :cond_0
    invoke-static {}, Lb72;->a()Landroid/view/WindowInsets$Builder;

    move-result-object p1

    :goto_0
    iput-object p1, p0, Lu62$d;->c:Landroid/view/WindowInsets$Builder;

    return-void
.end method


# virtual methods
.method public b()Lu62;
    .locals 2

    .line 1
    invoke-virtual {p0}, Lu62$f;->a()V

    iget-object v0, p0, Lu62$d;->c:Landroid/view/WindowInsets$Builder;

    invoke-static {v0}, La72;->a(Landroid/view/WindowInsets$Builder;)Landroid/view/WindowInsets;

    move-result-object v0

    invoke-static {v0}, Lu62;->v(Landroid/view/WindowInsets;)Lu62;

    move-result-object v0

    iget-object v1, p0, Lu62$f;->b:[Lnf0;

    invoke-virtual {v0, v1}, Lu62;->q([Lnf0;)V

    return-object v0
.end method

.method public d(Lnf0;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lu62$d;->c:Landroid/view/WindowInsets$Builder;

    invoke-virtual {p1}, Lnf0;->e()Landroid/graphics/Insets;

    move-result-object p1

    invoke-static {v0, p1}, Ly62;->a(Landroid/view/WindowInsets$Builder;Landroid/graphics/Insets;)Landroid/view/WindowInsets$Builder;

    return-void
.end method

.method public e(Lnf0;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lu62$d;->c:Landroid/view/WindowInsets$Builder;

    invoke-virtual {p1}, Lnf0;->e()Landroid/graphics/Insets;

    move-result-object p1

    invoke-static {v0, p1}, Lx62;->a(Landroid/view/WindowInsets$Builder;Landroid/graphics/Insets;)Landroid/view/WindowInsets$Builder;

    return-void
.end method

.method public f(Lnf0;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lu62$d;->c:Landroid/view/WindowInsets$Builder;

    invoke-virtual {p1}, Lnf0;->e()Landroid/graphics/Insets;

    move-result-object p1

    invoke-static {v0, p1}, Lz62;->a(Landroid/view/WindowInsets$Builder;Landroid/graphics/Insets;)Landroid/view/WindowInsets$Builder;

    return-void
.end method

.method public g(Lnf0;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lu62$d;->c:Landroid/view/WindowInsets$Builder;

    invoke-virtual {p1}, Lnf0;->e()Landroid/graphics/Insets;

    move-result-object p1

    invoke-static {v0, p1}, Lw62;->a(Landroid/view/WindowInsets$Builder;Landroid/graphics/Insets;)Landroid/view/WindowInsets$Builder;

    return-void
.end method

.method public h(Lnf0;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lu62$d;->c:Landroid/view/WindowInsets$Builder;

    invoke-virtual {p1}, Lnf0;->e()Landroid/graphics/Insets;

    move-result-object p1

    invoke-static {v0, p1}, Lv62;->a(Landroid/view/WindowInsets$Builder;Landroid/graphics/Insets;)Landroid/view/WindowInsets$Builder;

    return-void
.end method

.class public interface abstract Lss1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Closeable;


# virtual methods
.method public abstract B(Lvs1;Landroid/os/CancellationSignal;)Landroid/database/Cursor;
.end method

.method public abstract C()J
.end method

.method public abstract G(Ljava/lang/String;ILandroid/content/ContentValues;)J
.end method

.method public abstract L(Lvs1;)Landroid/database/Cursor;
.end method

.method public abstract M(Ljava/lang/String;)V
.end method

.method public abstract N()Z
.end method

.method public abstract S()V
.end method

.method public abstract T(Ljava/lang/String;[Ljava/lang/Object;)V
.end method

.method public abstract U(J)J
.end method

.method public abstract X()V
.end method

.method public abstract c0(I)V
.end method

.method public abstract d0(Ljava/lang/String;)Lws1;
.end method

.method public abstract f0(Ljava/lang/String;ILandroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/Object;)I
.end method

.method public abstract g0()Z
.end method

.method public abstract getPageSize()J
.end method

.method public abstract getPath()Ljava/lang/String;
.end method

.method public abstract getVersion()I
.end method

.method public abstract h0(Ljava/lang/String;)Landroid/database/Cursor;
.end method

.method public abstract isOpen()Z
.end method

.method public abstract isReadOnly()Z
.end method

.method public abstract j0()Z
.end method

.method public abstract k0()Z
.end method

.method public abstract l(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I
.end method

.method public abstract l0(I)V
.end method

.method public abstract m()V
.end method

.method public abstract n()Ljava/util/List;
.end method

.method public abstract n0(J)V
.end method

.method public abstract r()V
.end method

.method public abstract s()Z
.end method

.method public abstract setLocale(Ljava/util/Locale;)V
.end method

.method public abstract t(I)Z
.end method

.method public abstract z(Z)V
.end method

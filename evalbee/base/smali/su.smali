.class public Lsu;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lsu$b;
    }
.end annotation


# instance fields
.field public a:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

.field public b:Landroid/content/Context;

.field public c:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/ekodroid/omrevaluator/templateui/models/ExamId;Z)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lsu;->a:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    iput-object p1, p0, Lsu;->b:Landroid/content/Context;

    iput-boolean p3, p0, Lsu;->c:Z

    new-instance p1, Lsu$b;

    const/4 p2, 0x0

    invoke-direct {p1, p0, p2}, Lsu$b;-><init>(Lsu;Lsu$a;)V

    const/4 p2, 0x0

    new-array p2, p2, [Ljava/lang/Void;

    invoke-virtual {p1, p2}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method public static synthetic a(Lsu;)Lcom/ekodroid/omrevaluator/templateui/models/ExamId;
    .locals 0

    .line 1
    iget-object p0, p0, Lsu;->a:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    return-object p0
.end method

.method public static synthetic b(Lsu;)Landroid/content/Context;
    .locals 0

    .line 1
    iget-object p0, p0, Lsu;->b:Landroid/content/Context;

    return-object p0
.end method

.method public static synthetic c(Lsu;Landroid/content/Context;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)Ljava/util/ArrayList;
    .locals 0

    .line 1
    invoke-virtual {p0, p1, p2}, Lsu;->e(Landroid/content/Context;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)Ljava/util/ArrayList;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public final d(Landroid/content/Context;Ljava/util/ArrayList;)V
    .locals 4

    .line 1
    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lye1;

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/database/repositories/ClassRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/ClassRepository;

    move-result-object v1

    invoke-virtual {v0}, Lye1;->g()I

    move-result v2

    iget-object v3, p0, Lsu;->a:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v3}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;->getClassName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/ekodroid/omrevaluator/database/repositories/ClassRepository;->getStudent(ILjava/lang/String;)Lcom/ekodroid/omrevaluator/database/StudentDataModel;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/database/StudentDataModel;->getStudentName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lye1;->m(Ljava/lang/String;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lye1;->p(Z)V

    goto :goto_0

    :cond_0
    const-string v1, " - - - "

    invoke-virtual {v0, v1}, Lye1;->m(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public final e(Landroid/content/Context;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)Ljava/util/ArrayList;
    .locals 17

    .line 1
    move-object/from16 v0, p0

    invoke-static/range {p1 .. p1}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;

    move-result-object v1

    iget-object v2, v0, Lsu;->a:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v1, v2}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->getAllResultJson(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)Ljava/util/ArrayList;

    move-result-object v1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;

    move-object/from16 v4, p2

    if-eqz v3, :cond_0

    invoke-virtual {v3, v4}, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;->getResultItem(Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;

    move-result-object v5

    invoke-static {v5}, Lve1;->p(Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;)I

    move-result v11

    invoke-static {v5}, Lve1;->q(Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;)I

    move-result v12

    invoke-static {v5}, Lve1;->s(Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;)I

    move-result v13

    invoke-virtual {v5}, Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;->getTotalMarks()D

    move-result-wide v8

    invoke-virtual/range {p2 .. p2}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getGradeLevels()[Lcom/ekodroid/omrevaluator/templateui/models/GradeLevel;

    move-result-object v5

    invoke-static {v8, v9, v5}, Lve1;->i(D[Lcom/ekodroid/omrevaluator/templateui/models/GradeLevel;)Ljava/lang/String;

    move-result-object v10

    new-instance v5, Lye1;

    invoke-virtual {v3}, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;->getRollNo()I

    move-result v7

    invoke-virtual {v3}, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;->isSmsSent()Z

    move-result v14

    invoke-virtual {v3}, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;->isSynced()Z

    move-result v15

    invoke-virtual {v3}, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;->isPublished()Z

    move-result v16

    move-object v6, v5

    invoke-direct/range {v6 .. v16}, Lye1;-><init>(IDLjava/lang/String;IIIZZZ)V

    invoke-virtual {v2, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    move-object/from16 v4, p2

    invoke-virtual/range {p2 .. p2}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getRankingMethod()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lve1;->a(Ljava/util/ArrayList;Ljava/lang/String;)V

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v2}, Lsu;->d(Landroid/content/Context;Ljava/util/ArrayList;)V

    return-object v2
.end method

.class public Lee1;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Ljava/util/concurrent/atomic/AtomicInteger;

.field public final b:Ljava/util/Set;

.field public final c:Ljava/util/concurrent/PriorityBlockingQueue;

.field public final d:Ljava/util/concurrent/PriorityBlockingQueue;

.field public final e:Lcom/android/volley/a;

.field public final f:Lqy0;

.field public final g:Lre1;

.field public final h:[Lcom/android/volley/c;

.field public i:Lcom/android/volley/b;

.field public final j:Ljava/util/List;

.field public final k:Ljava/util/List;


# direct methods
.method public constructor <init>(Lcom/android/volley/a;Lqy0;)V
    .locals 1

    .line 1
    const/4 v0, 0x4

    invoke-direct {p0, p1, p2, v0}, Lee1;-><init>(Lcom/android/volley/a;Lqy0;I)V

    return-void
.end method

.method public constructor <init>(Lcom/android/volley/a;Lqy0;I)V
    .locals 3

    .line 2
    new-instance v0, Lty;

    new-instance v1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    invoke-direct {v0, v1}, Lty;-><init>(Landroid/os/Handler;)V

    invoke-direct {p0, p1, p2, p3, v0}, Lee1;-><init>(Lcom/android/volley/a;Lqy0;ILre1;)V

    return-void
.end method

.method public constructor <init>(Lcom/android/volley/a;Lqy0;ILre1;)V
    .locals 1

    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    iput-object v0, p0, Lee1;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lee1;->b:Ljava/util/Set;

    new-instance v0, Ljava/util/concurrent/PriorityBlockingQueue;

    invoke-direct {v0}, Ljava/util/concurrent/PriorityBlockingQueue;-><init>()V

    iput-object v0, p0, Lee1;->c:Ljava/util/concurrent/PriorityBlockingQueue;

    new-instance v0, Ljava/util/concurrent/PriorityBlockingQueue;

    invoke-direct {v0}, Ljava/util/concurrent/PriorityBlockingQueue;-><init>()V

    iput-object v0, p0, Lee1;->d:Ljava/util/concurrent/PriorityBlockingQueue;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lee1;->j:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lee1;->k:Ljava/util/List;

    iput-object p1, p0, Lee1;->e:Lcom/android/volley/a;

    iput-object p2, p0, Lee1;->f:Lqy0;

    new-array p1, p3, [Lcom/android/volley/c;

    iput-object p1, p0, Lee1;->h:[Lcom/android/volley/c;

    iput-object p4, p0, Lee1;->g:Lre1;

    return-void
.end method


# virtual methods
.method public a(Lcom/android/volley/Request;)Lcom/android/volley/Request;
    .locals 2

    .line 1
    invoke-virtual {p1, p0}, Lcom/android/volley/Request;->K(Lee1;)Lcom/android/volley/Request;

    iget-object v0, p0, Lee1;->b:Ljava/util/Set;

    monitor-enter v0

    :try_start_0
    iget-object v1, p0, Lee1;->b:Ljava/util/Set;

    invoke-interface {v1, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p0}, Lee1;->e()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/android/volley/Request;->M(I)Lcom/android/volley/Request;

    const-string v0, "add-to-queue"

    invoke-virtual {p1, v0}, Lcom/android/volley/Request;->c(Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lee1;->f(Lcom/android/volley/Request;I)V

    invoke-virtual {p0, p1}, Lee1;->b(Lcom/android/volley/Request;)V

    return-object p1

    :catchall_0
    move-exception p1

    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw p1
.end method

.method public b(Lcom/android/volley/Request;)V
    .locals 1

    .line 1
    invoke-virtual {p1}, Lcom/android/volley/Request;->N()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0, p1}, Lee1;->g(Lcom/android/volley/Request;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lee1;->c:Ljava/util/concurrent/PriorityBlockingQueue;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/PriorityBlockingQueue;->add(Ljava/lang/Object;)Z

    :goto_0
    return-void
.end method

.method public c(Lcom/android/volley/Request;)V
    .locals 3

    .line 1
    iget-object v0, p0, Lee1;->b:Ljava/util/Set;

    monitor-enter v0

    :try_start_0
    iget-object v1, p0, Lee1;->b:Ljava/util/Set;

    invoke-interface {v1, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    iget-object v1, p0, Lee1;->j:Ljava/util/List;

    monitor-enter v1

    :try_start_1
    iget-object v0, p0, Lee1;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const/4 v0, 0x5

    invoke-virtual {p0, p1, v0}, Lee1;->f(Lcom/android/volley/Request;I)V

    return-void

    :cond_0
    :try_start_2
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p1

    invoke-static {p1}, Lzu0;->a(Ljava/lang/Object;)V

    const/4 p1, 0x0

    throw p1

    :catchall_0
    move-exception p1

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw p1

    :catchall_1
    move-exception p1

    :try_start_3
    monitor-exit v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw p1
.end method

.method public d()Lcom/android/volley/a;
    .locals 1

    .line 1
    iget-object v0, p0, Lee1;->e:Lcom/android/volley/a;

    return-object v0
.end method

.method public e()I
    .locals 1

    .line 1
    iget-object v0, p0, Lee1;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v0

    return v0
.end method

.method public f(Lcom/android/volley/Request;I)V
    .locals 1

    .line 1
    iget-object p1, p0, Lee1;->k:Ljava/util/List;

    monitor-enter p1

    :try_start_0
    iget-object p2, p0, Lee1;->k:Ljava/util/List;

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    monitor-exit p1

    return-void

    :cond_0
    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    invoke-static {p2}, Lzu0;->a(Ljava/lang/Object;)V

    const/4 p2, 0x0

    throw p2

    :catchall_0
    move-exception p2

    monitor-exit p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p2
.end method

.method public g(Lcom/android/volley/Request;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lee1;->d:Ljava/util/concurrent/PriorityBlockingQueue;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/PriorityBlockingQueue;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public h()V
    .locals 6

    .line 1
    invoke-virtual {p0}, Lee1;->i()V

    new-instance v0, Lcom/android/volley/b;

    iget-object v1, p0, Lee1;->c:Ljava/util/concurrent/PriorityBlockingQueue;

    iget-object v2, p0, Lee1;->d:Ljava/util/concurrent/PriorityBlockingQueue;

    iget-object v3, p0, Lee1;->e:Lcom/android/volley/a;

    iget-object v4, p0, Lee1;->g:Lre1;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/android/volley/b;-><init>(Ljava/util/concurrent/BlockingQueue;Ljava/util/concurrent/BlockingQueue;Lcom/android/volley/a;Lre1;)V

    iput-object v0, p0, Lee1;->i:Lcom/android/volley/b;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lee1;->h:[Lcom/android/volley/c;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    new-instance v1, Lcom/android/volley/c;

    iget-object v2, p0, Lee1;->d:Ljava/util/concurrent/PriorityBlockingQueue;

    iget-object v3, p0, Lee1;->f:Lqy0;

    iget-object v4, p0, Lee1;->e:Lcom/android/volley/a;

    iget-object v5, p0, Lee1;->g:Lre1;

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/android/volley/c;-><init>(Ljava/util/concurrent/BlockingQueue;Lqy0;Lcom/android/volley/a;Lre1;)V

    iget-object v2, p0, Lee1;->h:[Lcom/android/volley/c;

    aput-object v1, v2, v0

    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public i()V
    .locals 4

    .line 1
    iget-object v0, p0, Lee1;->i:Lcom/android/volley/b;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/android/volley/b;->d()V

    :cond_0
    iget-object v0, p0, Lee1;->h:[Lcom/android/volley/c;

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_2

    aget-object v3, v0, v2

    if-eqz v3, :cond_1

    invoke-virtual {v3}, Lcom/android/volley/c;->e()V

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    return-void
.end method

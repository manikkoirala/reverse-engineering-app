.class public Leb;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:[Ljava/lang/Object;

.field public b:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Leb;->b:I

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    iput-object v0, p0, Leb;->a:[Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(I)V
    .locals 1

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Leb;->a:[Ljava/lang/Object;

    const/4 v0, 0x0

    iput v0, p0, Leb;->b:I

    new-array p1, p1, [Ljava/lang/Object;

    iput-object p1, p0, Leb;->a:[Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Object;)V
    .locals 1

    .line 1
    iget v0, p0, Leb;->b:I

    invoke-virtual {p0, p1, v0}, Leb;->g(Ljava/lang/Object;I)V

    return-void
.end method

.method public b(Ljava/lang/Object;)Z
    .locals 2

    .line 1
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, p1, v0, v1}, Leb;->d(Ljava/lang/Object;IZ)I

    move-result p1

    if-ltz p1, :cond_0

    move v0, v1

    :cond_0
    return v0
.end method

.method public c(I)V
    .locals 2

    .line 1
    iget-object v0, p0, Leb;->a:[Ljava/lang/Object;

    array-length v1, v0

    if-le p1, v1, :cond_2

    array-length v0, v0

    mul-int/lit8 v0, v0, 0x2

    if-ge v0, p1, :cond_0

    goto :goto_0

    :cond_0
    move p1, v0

    :goto_0
    new-array p1, p1, [Ljava/lang/Object;

    const/4 v0, 0x0

    :goto_1
    iget v1, p0, Leb;->b:I

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Leb;->a:[Ljava/lang/Object;

    aget-object v1, v1, v0

    aput-object v1, p1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    iput-object p1, p0, Leb;->a:[Ljava/lang/Object;

    :cond_2
    return-void
.end method

.method public final d(Ljava/lang/Object;IZ)I
    .locals 2

    .line 1
    const/4 v0, -0x1

    if-ltz p2, :cond_8

    iget v1, p0, Leb;->b:I

    if-lt p2, v1, :cond_0

    goto :goto_4

    :cond_0
    if-eqz p3, :cond_4

    if-nez p1, :cond_2

    :goto_0
    iget p1, p0, Leb;->b:I

    if-ge p2, p1, :cond_8

    iget-object p1, p0, Leb;->a:[Ljava/lang/Object;

    aget-object p1, p1, p2

    if-nez p1, :cond_1

    return p2

    :cond_1
    add-int/lit8 p2, p2, 0x1

    goto :goto_0

    :cond_2
    :goto_1
    iget p3, p0, Leb;->b:I

    if-ge p2, p3, :cond_8

    iget-object p3, p0, Leb;->a:[Ljava/lang/Object;

    aget-object p3, p3, p2

    invoke-virtual {p1, p3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result p3

    if-eqz p3, :cond_3

    return p2

    :cond_3
    add-int/lit8 p2, p2, 0x1

    goto :goto_1

    :cond_4
    if-nez p1, :cond_6

    :goto_2
    if-ltz p2, :cond_8

    iget-object p1, p0, Leb;->a:[Ljava/lang/Object;

    aget-object p1, p1, p2

    if-nez p1, :cond_5

    return p2

    :cond_5
    add-int/lit8 p2, p2, -0x1

    goto :goto_2

    :cond_6
    :goto_3
    if-ltz p2, :cond_8

    iget-object p3, p0, Leb;->a:[Ljava/lang/Object;

    aget-object p3, p3, p2

    invoke-virtual {p1, p3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result p3

    if-eqz p3, :cond_7

    return p2

    :cond_7
    add-int/lit8 p2, p2, -0x1

    goto :goto_3

    :cond_8
    :goto_4
    return v0
.end method

.method public e(I)Ljava/lang/Object;
    .locals 3

    .line 1
    if-ltz p1, :cond_0

    iget v0, p0, Leb;->b:I

    if-ge p1, v0, :cond_0

    iget-object v0, p0, Leb;->a:[Ljava/lang/Object;

    aget-object p1, v0, p1

    return-object p1

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "required: (index >= 0 && index < size) but: (index = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, ", size = "

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget p1, p0, Leb;->b:I

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, ")"

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public f()I
    .locals 1

    .line 1
    iget-object v0, p0, Leb;->a:[Ljava/lang/Object;

    array-length v0, v0

    return v0
.end method

.method public g(Ljava/lang/Object;I)V
    .locals 3

    .line 1
    if-ltz p2, :cond_1

    iget v0, p0, Leb;->b:I

    if-gt p2, v0, :cond_1

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Leb;->c(I)V

    iget v0, p0, Leb;->b:I

    :goto_0
    if-le v0, p2, :cond_0

    iget-object v1, p0, Leb;->a:[Ljava/lang/Object;

    add-int/lit8 v2, v0, -0x1

    aget-object v2, v1, v2

    aput-object v2, v1, v0

    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Leb;->a:[Ljava/lang/Object;

    aput-object p1, v0, p2

    iget p1, p0, Leb;->b:I

    add-int/lit8 p1, p1, 0x1

    iput p1, p0, Leb;->b:I

    return-void

    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "required: (index >= 0 && index <= size) but: (index = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p2, ", size = "

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget p2, p0, Leb;->b:I

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p2, ")"

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public h()I
    .locals 1

    .line 1
    iget v0, p0, Leb;->b:I

    return v0
.end method

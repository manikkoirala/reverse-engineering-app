.class public abstract Lbf;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public final a:Lx11;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lx11;

    invoke-direct {v0}, Lx11;-><init>()V

    iput-object v0, p0, Lbf;->a:Lx11;

    return-void
.end method

.method public static b(Ljava/util/UUID;Lc92;)Lbf;
    .locals 1

    .line 1
    new-instance v0, Lbf$a;

    invoke-direct {v0, p1, p0}, Lbf$a;-><init>(Lc92;Ljava/util/UUID;)V

    return-object v0
.end method

.method public static c(Ljava/lang/String;Lc92;Z)Lbf;
    .locals 1

    .line 1
    new-instance v0, Lbf$c;

    invoke-direct {v0, p1, p0, p2}, Lbf$c;-><init>(Lc92;Ljava/lang/String;Z)V

    return-object v0
.end method

.method public static d(Ljava/lang/String;Lc92;)Lbf;
    .locals 1

    .line 1
    new-instance v0, Lbf$b;

    invoke-direct {v0, p1, p0}, Lbf$b;-><init>(Lc92;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public a(Lc92;Ljava/lang/String;)V
    .locals 2

    .line 1
    invoke-virtual {p1}, Lc92;->o()Landroidx/work/impl/WorkDatabase;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, Lbf;->f(Landroidx/work/impl/WorkDatabase;Ljava/lang/String;)V

    invoke-virtual {p1}, Lc92;->l()Lq81;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, p2, v1}, Lq81;->t(Ljava/lang/String;I)Z

    invoke-virtual {p1}, Lc92;->m()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lij1;

    invoke-interface {v0, p2}, Lij1;->e(Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public e()Landroidx/work/d;
    .locals 1

    .line 1
    iget-object v0, p0, Lbf;->a:Lx11;

    return-object v0
.end method

.method public final f(Landroidx/work/impl/WorkDatabase;Ljava/lang/String;)V
    .locals 4

    .line 1
    invoke-virtual {p1}, Landroidx/work/impl/WorkDatabase;->K()Lq92;

    move-result-object v0

    invoke-virtual {p1}, Landroidx/work/impl/WorkDatabase;->F()Lqs;

    move-result-object p1

    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    invoke-virtual {v1, p2}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    :goto_0
    invoke-virtual {v1}, Ljava/util/AbstractCollection;->isEmpty()Z

    move-result p2

    if-nez p2, :cond_1

    invoke-virtual {v1}, Ljava/util/LinkedList;->remove()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/lang/String;

    invoke-interface {v0, p2}, Lq92;->d(Ljava/lang/String;)Landroidx/work/WorkInfo$State;

    move-result-object v2

    sget-object v3, Landroidx/work/WorkInfo$State;->SUCCEEDED:Landroidx/work/WorkInfo$State;

    if-eq v2, v3, :cond_0

    sget-object v3, Landroidx/work/WorkInfo$State;->FAILED:Landroidx/work/WorkInfo$State;

    if-eq v2, v3, :cond_0

    invoke-interface {v0, p2}, Lq92;->e(Ljava/lang/String;)I

    :cond_0
    invoke-interface {p1, p2}, Lqs;->a(Ljava/lang/String;)Ljava/util/List;

    move-result-object p2

    invoke-virtual {v1, p2}, Ljava/util/LinkedList;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    :cond_1
    return-void
.end method

.method public g(Lc92;)V
    .locals 2

    .line 1
    invoke-virtual {p1}, Lc92;->h()Landroidx/work/a;

    move-result-object v0

    invoke-virtual {p1}, Lc92;->o()Landroidx/work/impl/WorkDatabase;

    move-result-object v1

    invoke-virtual {p1}, Lc92;->m()Ljava/util/List;

    move-result-object p1

    invoke-static {v0, v1, p1}, Lnj1;->h(Landroidx/work/a;Landroidx/work/impl/WorkDatabase;Ljava/util/List;)V

    return-void
.end method

.method public abstract h()V
.end method

.method public run()V
    .locals 3

    .line 1
    :try_start_0
    invoke-virtual {p0}, Lbf;->h()V

    iget-object v0, p0, Lbf;->a:Lx11;

    sget-object v1, Landroidx/work/d;->a:Landroidx/work/d$b$c;

    invoke-virtual {v0, v1}, Lx11;->a(Landroidx/work/d$b;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lbf;->a:Lx11;

    new-instance v2, Landroidx/work/d$b$a;

    invoke-direct {v2, v0}, Landroidx/work/d$b$a;-><init>(Ljava/lang/Throwable;)V

    invoke-virtual {v1, v2}, Lx11;->a(Landroidx/work/d$b;)V

    :goto_0
    return-void
.end method

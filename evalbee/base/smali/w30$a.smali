.class public Lw30$a;
.super Lio/grpc/ClientCall$Listener;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lw30;->e([Lio/grpc/ClientCall;Lpe0;Lcom/google/android/gms/tasks/Task;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Lpe0;

.field public final synthetic b:[Lio/grpc/ClientCall;

.field public final synthetic c:Lw30;


# direct methods
.method public constructor <init>(Lw30;Lpe0;[Lio/grpc/ClientCall;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lw30$a;->c:Lw30;

    iput-object p2, p0, Lw30$a;->a:Lpe0;

    iput-object p3, p0, Lw30$a;->b:[Lio/grpc/ClientCall;

    invoke-direct {p0}, Lio/grpc/ClientCall$Listener;-><init>()V

    return-void
.end method


# virtual methods
.method public onClose(Lio/grpc/Status;Lio/grpc/Metadata;)V
    .locals 0

    .line 1
    :try_start_0
    iget-object p2, p0, Lw30$a;->a:Lpe0;

    invoke-interface {p2, p1}, Lpe0;->a(Lio/grpc/Status;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception p1

    iget-object p2, p0, Lw30$a;->c:Lw30;

    invoke-static {p2}, Lw30;->b(Lw30;)Lcom/google/firebase/firestore/util/AsyncQueue;

    move-result-object p2

    invoke-virtual {p2, p1}, Lcom/google/firebase/firestore/util/AsyncQueue;->n(Ljava/lang/Throwable;)V

    :goto_0
    return-void
.end method

.method public onHeaders(Lio/grpc/Metadata;)V
    .locals 1

    .line 1
    :try_start_0
    iget-object v0, p0, Lw30$a;->a:Lpe0;

    invoke-interface {v0, p1}, Lpe0;->c(Lio/grpc/Metadata;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception p1

    iget-object v0, p0, Lw30$a;->c:Lw30;

    invoke-static {v0}, Lw30;->b(Lw30;)Lcom/google/firebase/firestore/util/AsyncQueue;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/firebase/firestore/util/AsyncQueue;->n(Ljava/lang/Throwable;)V

    :goto_0
    return-void
.end method

.method public onMessage(Ljava/lang/Object;)V
    .locals 1

    .line 1
    :try_start_0
    iget-object v0, p0, Lw30$a;->a:Lpe0;

    invoke-interface {v0, p1}, Lpe0;->onNext(Ljava/lang/Object;)V

    iget-object p1, p0, Lw30$a;->b:[Lio/grpc/ClientCall;

    const/4 v0, 0x0

    aget-object p1, p1, v0

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lio/grpc/ClientCall;->request(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception p1

    iget-object v0, p0, Lw30$a;->c:Lw30;

    invoke-static {v0}, Lw30;->b(Lw30;)Lcom/google/firebase/firestore/util/AsyncQueue;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/firebase/firestore/util/AsyncQueue;->n(Ljava/lang/Throwable;)V

    :goto_0
    return-void
.end method

.method public onReady()V
    .locals 0

    .line 1
    return-void
.end method

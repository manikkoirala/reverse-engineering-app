.class public abstract Ldd1$b;
.super Lhz1;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Ldd1;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "b"
.end annotation


# instance fields
.field public final a:Ljava/util/Map;


# direct methods
.method public constructor <init>(Ljava/util/Map;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lhz1;-><init>()V

    iput-object p1, p0, Ldd1$b;->a:Ljava/util/Map;

    return-void
.end method


# virtual methods
.method public b(Lrh0;)Ljava/lang/Object;
    .locals 3

    .line 1
    invoke-virtual {p1}, Lrh0;->o0()Lcom/google/gson/stream/JsonToken;

    move-result-object v0

    sget-object v1, Lcom/google/gson/stream/JsonToken;->NULL:Lcom/google/gson/stream/JsonToken;

    if-ne v0, v1, :cond_0

    invoke-virtual {p1}, Lrh0;->R()V

    const/4 p1, 0x0

    return-object p1

    :cond_0
    invoke-virtual {p0}, Ldd1$b;->e()Ljava/lang/Object;

    move-result-object v0

    :try_start_0
    invoke-virtual {p1}, Lrh0;->b()V

    :goto_0
    invoke-virtual {p1}, Lrh0;->k()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {p1}, Lrh0;->K()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Ldd1$b;->a:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ldd1$c;

    if-eqz v1, :cond_2

    iget-boolean v2, v1, Ldd1$c;->e:Z

    if-nez v2, :cond_1

    goto :goto_1

    :cond_1
    invoke-virtual {p0, v0, p1, v1}, Ldd1$b;->g(Ljava/lang/Object;Lrh0;Ldd1$c;)V

    goto :goto_0

    :cond_2
    :goto_1
    invoke-virtual {p1}, Lrh0;->y0()V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :cond_3
    invoke-virtual {p1}, Lrh0;->g()V

    invoke-virtual {p0, v0}, Ldd1$b;->f(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    return-object p1

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcd1;->e(Ljava/lang/IllegalAccessException;)Ljava/lang/RuntimeException;

    move-result-object p1

    throw p1

    :catch_1
    move-exception p1

    new-instance v0, Lcom/google/gson/JsonSyntaxException;

    invoke-direct {v0, p1}, Lcom/google/gson/JsonSyntaxException;-><init>(Ljava/lang/Throwable;)V

    throw v0
.end method

.method public d(Lvh0;Ljava/lang/Object;)V
    .locals 2

    .line 1
    if-nez p2, :cond_0

    invoke-virtual {p1}, Lvh0;->u()Lvh0;

    return-void

    :cond_0
    invoke-virtual {p1}, Lvh0;->d()Lvh0;

    :try_start_0
    iget-object v0, p0, Ldd1$b;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ldd1$c;

    invoke-virtual {v1, p1, p2}, Ldd1$c;->c(Lvh0;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Lvh0;->g()Lvh0;

    return-void

    :catch_0
    move-exception p1

    invoke-static {p1}, Lcd1;->e(Ljava/lang/IllegalAccessException;)Ljava/lang/RuntimeException;

    move-result-object p1

    throw p1
.end method

.method public abstract e()Ljava/lang/Object;
.end method

.method public abstract f(Ljava/lang/Object;)Ljava/lang/Object;
.end method

.method public abstract g(Ljava/lang/Object;Lrh0;Ldd1$c;)V
.end method

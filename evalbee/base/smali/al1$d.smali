.class public Lal1$d;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lal1;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Lal1;


# direct methods
.method public constructor <init>(Lal1;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lal1$d;->a:Lal1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .line 1
    iget-object p1, p0, Lal1$d;->a:Lal1;

    iget-object v0, p1, Lal1;->l:Ljava/util/ArrayList;

    invoke-static {p1, v0}, Lal1;->b(Lal1;Ljava/util/ArrayList;)Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lal1$d;->a:Lal1;

    iget-object p1, p1, Lal1;->a:Landroid/content/Context;

    const-string v0, "MyPref"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object p1

    invoke-interface {p1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lal1$d;->a:Lal1;

    iget-object v3, v3, Lal1;->b:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v3}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;->getExamName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "_sentSMSCount"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    invoke-interface {p1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lal1$d;->a:Lal1;

    iget-object v1, v1, Lal1;->b:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;->getExamName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "_lastSMSIndexSent"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, -0x1

    invoke-interface {p1, v0, v1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object p1

    invoke-interface {p1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    new-instance p1, Landroid/content/Intent;

    iget-object v0, p0, Lal1$d;->a:Lal1;

    iget-object v0, v0, Lal1;->a:Landroid/content/Context;

    const-class v1, Lcom/ekodroid/omrevaluator/exams/sms/SmsPublishService;

    invoke-direct {p1, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v0, p0, Lal1$d;->a:Lal1;

    iget-object v0, v0, Lal1;->b:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    const-string v1, "EXAM_ID"

    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    iget-object v0, p0, Lal1$d;->a:Lal1;

    iget-object v0, v0, Lal1;->m:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v0

    const-string v1, "SEND_SMS_SUMMARY"

    invoke-virtual {p1, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    iget-object v0, p0, Lal1$d;->a:Lal1;

    iget-object v0, v0, Lal1;->a:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    iget-object p1, p0, Lal1$d;->a:Lal1;

    invoke-static {p1}, Lal1;->c(Lal1;)Ly01;

    move-result-object p1

    const-string v0, "CLOSE_ACTIVITY"

    invoke-interface {p1, v0}, Ly01;->a(Ljava/lang/Object;)V

    iget-object p1, p0, Lal1$d;->a:Lal1;

    invoke-virtual {p1}, Landroid/app/Dialog;->dismiss()V

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lal1$d;->a:Lal1;

    iget-object p1, p1, Lal1;->a:Landroid/content/Context;

    const v0, 0x7f0800bd

    const v1, 0x7f08016e

    const v2, 0x7f1201a6

    invoke-static {p1, v2, v0, v1}, La91;->G(Landroid/content/Context;III)V

    :goto_0
    return-void
.end method

.class public Lg5;
.super Landroidx/fragment/app/Fragment;
.source "SourceFile"


# instance fields
.field public a:Landroid/content/Context;

.field public b:Landroid/widget/ListView;

.field public c:Landroid/widget/Spinner;

.field public d:Landroid/widget/ImageButton;

.field public e:Landroid/widget/ImageButton;

.field public f:Ljava/util/ArrayList;

.field public g:Li3;

.field public h:Lcom/ekodroid/omrevaluator/util/DataModels/SortAnswerResponseList$AnswerResponseSortBy;

.field public i:Z

.field public j:Lcom/google/firebase/analytics/FirebaseAnalytics;

.field public k:Ljava/util/ArrayList;

.field public l:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

.field public m:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

.field public n:I

.field public p:Lf5$b;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Landroidx/fragment/app/Fragment;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lg5;->f:Ljava/util/ArrayList;

    sget-object v0, Lcom/ekodroid/omrevaluator/util/DataModels/SortAnswerResponseList$AnswerResponseSortBy;->QUESTION:Lcom/ekodroid/omrevaluator/util/DataModels/SortAnswerResponseList$AnswerResponseSortBy;

    iput-object v0, p0, Lg5;->h:Lcom/ekodroid/omrevaluator/util/DataModels/SortAnswerResponseList$AnswerResponseSortBy;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lg5;->i:Z

    const/4 v0, 0x0

    iput v0, p0, Lg5;->n:I

    new-instance v0, Lg5$a;

    invoke-direct {v0, p0}, Lg5$a;-><init>(Lg5;)V

    iput-object v0, p0, Lg5;->p:Lf5$b;

    return-void
.end method

.method public static synthetic g(Lg5;)V
    .locals 0

    .line 1
    invoke-virtual {p0}, Lg5;->n()V

    return-void
.end method

.method public static synthetic h(Lg5;)I
    .locals 0

    .line 1
    iget p0, p0, Lg5;->n:I

    return p0
.end method

.method public static synthetic i(Lg5;I)I
    .locals 0

    .line 1
    iput p1, p0, Lg5;->n:I

    return p1
.end method

.method public static synthetic j(Lg5;)Lf5$b;
    .locals 0

    .line 1
    iget-object p0, p0, Lg5;->p:Lf5$b;

    return-object p0
.end method


# virtual methods
.method public final k()V
    .locals 2

    .line 1
    iget-object v0, p0, Lg5;->e:Landroid/widget/ImageButton;

    new-instance v1, Lg5$d;

    invoke-direct {v1, p0}, Lg5$d;-><init>(Lg5;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final l()V
    .locals 2

    .line 1
    iget-object v0, p0, Lg5;->d:Landroid/widget/ImageButton;

    new-instance v1, Lg5$c;

    invoke-direct {v1, p0}, Lg5$c;-><init>(Lg5;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method public final m(I[Ljava/lang/String;)V
    .locals 3

    .line 1
    new-array v0, p1, [Ljava/lang/String;

    const/4 v1, 0x0

    :goto_0
    if-ge v1, p1, :cond_0

    aget-object v2, p2, v1

    aput-object v2, v0, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    new-instance p1, Landroid/widget/ArrayAdapter;

    iget-object p2, p0, Lg5;->a:Landroid/content/Context;

    const v1, 0x7f0c00f0

    invoke-direct {p1, p2, v1, v0}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    iget-object p2, p0, Lg5;->c:Landroid/widget/Spinner;

    invoke-virtual {p2, p1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    iget-object p1, p0, Lg5;->c:Landroid/widget/Spinner;

    new-instance p2, Lg5$b;

    invoke-direct {p2, p0}, Lg5$b;-><init>(Lg5;)V

    invoke-virtual {p1, p2}, Landroid/widget/AdapterView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    return-void
.end method

.method public final n()V
    .locals 4

    .line 1
    new-instance v0, Lcom/ekodroid/omrevaluator/util/DataModels/SortAnswerResponseList;

    invoke-direct {v0}, Lcom/ekodroid/omrevaluator/util/DataModels/SortAnswerResponseList;-><init>()V

    iget-object v1, p0, Lg5;->h:Lcom/ekodroid/omrevaluator/util/DataModels/SortAnswerResponseList$AnswerResponseSortBy;

    iget-object v2, p0, Lg5;->f:Ljava/util/ArrayList;

    iget-boolean v3, p0, Lg5;->i:Z

    invoke-virtual {v0, v1, v2, v3}, Lcom/ekodroid/omrevaluator/util/DataModels/SortAnswerResponseList;->e(Lcom/ekodroid/omrevaluator/util/DataModels/SortAnswerResponseList$AnswerResponseSortBy;Ljava/util/ArrayList;Z)V

    new-instance v0, Li3;

    iget-object v1, p0, Lg5;->a:Landroid/content/Context;

    iget-object v2, p0, Lg5;->f:Ljava/util/ArrayList;

    iget-object v3, p0, Lg5;->h:Lcom/ekodroid/omrevaluator/util/DataModels/SortAnswerResponseList$AnswerResponseSortBy;

    invoke-direct {v0, v1, v2, v3}, Li3;-><init>(Landroid/content/Context;Ljava/util/ArrayList;Lcom/ekodroid/omrevaluator/util/DataModels/SortAnswerResponseList$AnswerResponseSortBy;)V

    iput-object v0, p0, Lg5;->g:Li3;

    iget-object v1, p0, Lg5;->b:Landroid/widget/ListView;

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void
.end method

.method public onAttach(Landroid/content/Context;)V
    .locals 0

    .line 1
    invoke-super {p0, p1}, Landroidx/fragment/app/Fragment;->onAttach(Landroid/content/Context;)V

    iput-object p1, p0, Lg5;->a:Landroid/content/Context;

    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    .line 1
    const p3, 0x7f0c0091

    const/4 v0, 0x0

    invoke-virtual {p1, p3, p2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    iget-object p2, p0, Lg5;->a:Landroid/content/Context;

    invoke-static {p2}, Lcom/google/firebase/analytics/FirebaseAnalytics;->getInstance(Landroid/content/Context;)Lcom/google/firebase/analytics/FirebaseAnalytics;

    move-result-object p2

    iput-object p2, p0, Lg5;->j:Lcom/google/firebase/analytics/FirebaseAnalytics;

    const p2, 0x7f090244

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/ListView;

    iput-object p2, p0, Lg5;->b:Landroid/widget/ListView;

    const p2, 0x7f090348

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/Spinner;

    iput-object p2, p0, Lg5;->c:Landroid/widget/Spinner;

    const p2, 0x7f0901a4

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/ImageButton;

    iput-object p2, p0, Lg5;->d:Landroid/widget/ImageButton;

    const p2, 0x7f0901a3

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    check-cast p2, Landroid/widget/ImageButton;

    iput-object p2, p0, Lg5;->e:Landroid/widget/ImageButton;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getArguments()Landroid/os/Bundle;

    move-result-object p2

    if-eqz p2, :cond_2

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getArguments()Landroid/os/Bundle;

    move-result-object p2

    const-string p3, "EXAM_ID"

    invoke-virtual {p2, p3}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object p2

    check-cast p2, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    iput-object p2, p0, Lg5;->m:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {p0}, Landroidx/fragment/app/Fragment;->getArguments()Landroid/os/Bundle;

    move-result-object p2

    const-string p3, "EXAM_TYPE"

    invoke-virtual {p2, p3}, Landroid/os/BaseBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    const-string p3, "ARCHIVED"

    invoke-virtual {p3, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_0

    sget-object p2, Le8;->a:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    iput-object p2, p0, Lg5;->l:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    invoke-static {}, Le8;->a()Ljava/util/ArrayList;

    move-result-object p2

    goto :goto_0

    :cond_0
    iget-object p2, p0, Lg5;->a:Landroid/content/Context;

    invoke-static {p2}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;

    move-result-object p2

    iget-object p3, p0, Lg5;->m:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {p2, p3}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getTemplateJson(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;

    move-result-object p2

    invoke-virtual {p2}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->getSheetTemplate()Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    move-result-object p2

    iput-object p2, p0, Lg5;->l:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    iget-object p2, p0, Lg5;->a:Landroid/content/Context;

    invoke-static {p2}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;

    move-result-object p2

    iget-object p3, p0, Lg5;->m:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {p2, p3}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->getAllResultJson(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)Ljava/util/ArrayList;

    move-result-object p2

    :goto_0
    iput-object p2, p0, Lg5;->k:Ljava/util/ArrayList;

    iget-object p2, p0, Lg5;->l:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    invoke-virtual {p2}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getTemplateParams()Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;

    move-result-object p2

    invoke-virtual {p2}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;->getExamSets()I

    move-result p2

    const/4 p3, 0x1

    if-ne p2, p3, :cond_1

    iput v0, p0, Lg5;->n:I

    const p2, 0x7f090400

    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object p2

    const/16 p3, 0x8

    invoke-virtual {p2, p3}, Landroid/view/View;->setVisibility(I)V

    iget-object p2, p0, Lg5;->c:Landroid/widget/Spinner;

    invoke-virtual {p2, p3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    :cond_1
    iget-object p3, p0, Lg5;->l:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    invoke-virtual {p3}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getLabelProfile()Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    move-result-object p3

    invoke-virtual {p3}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getExamSetLabels()[Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p0, p2, p3}, Lg5;->m(I[Ljava/lang/String;)V

    :goto_1
    new-instance p2, Lf5;

    iget p3, p0, Lg5;->n:I

    iget-object v0, p0, Lg5;->p:Lf5$b;

    iget-object v1, p0, Lg5;->l:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    iget-object v2, p0, Lg5;->k:Ljava/util/ArrayList;

    invoke-direct {p2, p3, v0, v1, v2}, Lf5;-><init>(ILf5$b;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;Ljava/util/ArrayList;)V

    :cond_2
    invoke-virtual {p0}, Lg5;->l()V

    invoke-virtual {p0}, Lg5;->k()V

    return-object p1
.end method

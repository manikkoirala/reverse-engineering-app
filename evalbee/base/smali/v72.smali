.class public final Lv72;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lv72$d;,
        Lv72$c;,
        Lv72$b;,
        Lv72$a;,
        Lv72$e;
    }
.end annotation


# instance fields
.field public final a:Lv72$e;


# direct methods
.method public constructor <init>(Landroid/view/Window;Landroid/view/View;)V
    .locals 2

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x1e

    if-lt v0, v1, :cond_0

    new-instance p2, Lv72$d;

    invoke-direct {p2, p1, p0}, Lv72$d;-><init>(Landroid/view/Window;Lv72;)V

    iput-object p2, p0, Lv72;->a:Lv72$e;

    goto :goto_1

    :cond_0
    const/16 v1, 0x1a

    if-lt v0, v1, :cond_1

    new-instance v0, Lv72$c;

    invoke-direct {v0, p1, p2}, Lv72$c;-><init>(Landroid/view/Window;Landroid/view/View;)V

    :goto_0
    iput-object v0, p0, Lv72;->a:Lv72$e;

    goto :goto_1

    :cond_1
    new-instance v0, Lv72$b;

    invoke-direct {v0, p1, p2}, Lv72$b;-><init>(Landroid/view/Window;Landroid/view/View;)V

    goto :goto_0

    :goto_1
    return-void
.end method

.method public constructor <init>(Landroid/view/WindowInsetsController;)V
    .locals 1

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lv72$d;

    invoke-direct {v0, p1, p0}, Lv72$d;-><init>(Landroid/view/WindowInsetsController;Lv72;)V

    iput-object v0, p0, Lv72;->a:Lv72$e;

    return-void
.end method

.method public static f(Landroid/view/WindowInsetsController;)Lv72;
    .locals 1

    .line 1
    new-instance v0, Lv72;

    invoke-direct {v0, p0}, Lv72;-><init>(Landroid/view/WindowInsetsController;)V

    return-object v0
.end method


# virtual methods
.method public a(I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lv72;->a:Lv72$e;

    invoke-virtual {v0, p1}, Lv72$e;->a(I)V

    return-void
.end method

.method public b()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lv72;->a:Lv72$e;

    invoke-virtual {v0}, Lv72$e;->b()Z

    move-result v0

    return v0
.end method

.method public c(Z)V
    .locals 1

    .line 1
    iget-object v0, p0, Lv72;->a:Lv72$e;

    invoke-virtual {v0, p1}, Lv72$e;->c(Z)V

    return-void
.end method

.method public d(Z)V
    .locals 1

    .line 1
    iget-object v0, p0, Lv72;->a:Lv72$e;

    invoke-virtual {v0, p1}, Lv72$e;->d(Z)V

    return-void
.end method

.method public e(I)V
    .locals 1

    .line 1
    iget-object v0, p0, Lv72;->a:Lv72$e;

    invoke-virtual {v0, p1}, Lv72$e;->e(I)V

    return-void
.end method

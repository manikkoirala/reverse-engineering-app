.class public final Lb22;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Lqp;


# direct methods
.method public constructor <init>(Lqp;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lb22;->a:Lqp;

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;Ly12;)La11;
    .locals 3

    .line 1
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->isArray()Z

    move-result v0

    const-string v1, "Invalid data. Data must be a Map<String, Object> or a suitable POJO object, but it was "

    if-nez v0, :cond_1

    invoke-static {p1}, Loo;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, Lb22;->c(Ljava/lang/Object;Ly12;)Lcom/google/firestore/v1/Value;

    move-result-object p2

    invoke-virtual {p2}, Lcom/google/firestore/v1/Value;->w0()Lcom/google/firestore/v1/Value$ValueTypeCase;

    move-result-object v0

    sget-object v2, Lcom/google/firestore/v1/Value$ValueTypeCase;->MAP_VALUE:Lcom/google/firestore/v1/Value$ValueTypeCase;

    if-ne v0, v2, :cond_0

    new-instance p1, La11;

    invoke-direct {p1, p2}, La11;-><init>(Lcom/google/firestore/v1/Value;)V

    return-object p1

    :cond_0
    new-instance p2, Ljava/lang/IllegalArgumentException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "of type: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {p1}, Lo22;->z(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p2

    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "an array"

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public b(Ljava/lang/Object;Ly12;)Lcom/google/firestore/v1/Value;
    .locals 0

    .line 1
    invoke-static {p1}, Loo;->c(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    invoke-virtual {p0, p1, p2}, Lb22;->c(Ljava/lang/Object;Ly12;)Lcom/google/firestore/v1/Value;

    move-result-object p1

    return-object p1
.end method

.method public final c(Ljava/lang/Object;Ly12;)Lcom/google/firestore/v1/Value;
    .locals 2

    .line 1
    instance-of v0, p1, Ljava/util/Map;

    if-eqz v0, :cond_0

    check-cast p1, Ljava/util/Map;

    invoke-virtual {p0, p1, p2}, Lb22;->e(Ljava/util/Map;Ly12;)Lcom/google/firestore/v1/Value;

    move-result-object p1

    return-object p1

    :cond_0
    instance-of v0, p1, Lv00;

    if-eqz v0, :cond_1

    check-cast p1, Lv00;

    invoke-virtual {p0, p1, p2}, Lb22;->h(Lv00;Ly12;)V

    const/4 p1, 0x0

    return-object p1

    :cond_1
    invoke-virtual {p2}, Ly12;->h()Ls00;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p2}, Ly12;->h()Ls00;

    move-result-object v0

    invoke-virtual {p2, v0}, Ly12;->a(Ls00;)V

    :cond_2
    instance-of v0, p1, Ljava/util/List;

    if-eqz v0, :cond_5

    invoke-virtual {p2}, Ly12;->i()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p2}, Ly12;->g()Lcom/google/firebase/firestore/core/UserData$Source;

    move-result-object v0

    sget-object v1, Lcom/google/firebase/firestore/core/UserData$Source;->ArrayArgument:Lcom/google/firebase/firestore/core/UserData$Source;

    if-ne v0, v1, :cond_3

    goto :goto_0

    :cond_3
    const-string p1, "Nested arrays are not supported"

    invoke-virtual {p2, p1}, Ly12;->f(Ljava/lang/String;)Ljava/lang/RuntimeException;

    move-result-object p1

    throw p1

    :cond_4
    :goto_0
    check-cast p1, Ljava/util/List;

    invoke-virtual {p0, p1, p2}, Lb22;->d(Ljava/util/List;Ly12;)Lcom/google/firestore/v1/Value;

    move-result-object p1

    return-object p1

    :cond_5
    invoke-virtual {p0, p1, p2}, Lb22;->g(Ljava/lang/Object;Ly12;)Lcom/google/firestore/v1/Value;

    move-result-object p1

    return-object p1
.end method

.method public final d(Ljava/util/List;Ly12;)Lcom/google/firestore/v1/Value;
    .locals 4

    .line 1
    invoke-static {}, Lcom/google/firestore/v1/a;->j0()Lcom/google/firestore/v1/a$b;

    move-result-object v0

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    const/4 v1, 0x0

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p2, v1}, Ly12;->c(I)Ly12;

    move-result-object v3

    invoke-virtual {p0, v2, v3}, Lb22;->c(Ljava/lang/Object;Ly12;)Lcom/google/firestore/v1/Value;

    move-result-object v2

    if-nez v2, :cond_0

    invoke-static {}, Lcom/google/firestore/v1/Value;->x0()Lcom/google/firestore/v1/Value$b;

    move-result-object v2

    sget-object v3, Lcom/google/protobuf/NullValue;->NULL_VALUE:Lcom/google/protobuf/NullValue;

    invoke-virtual {v2, v3}, Lcom/google/firestore/v1/Value$b;->J(Lcom/google/protobuf/NullValue;)Lcom/google/firestore/v1/Value$b;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/protobuf/GeneratedMessageLite$a;->p()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object v2

    check-cast v2, Lcom/google/firestore/v1/Value;

    :cond_0
    invoke-virtual {v0, v2}, Lcom/google/firestore/v1/a$b;->B(Lcom/google/firestore/v1/Value;)Lcom/google/firestore/v1/a$b;

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    invoke-static {}, Lcom/google/firestore/v1/Value;->x0()Lcom/google/firestore/v1/Value$b;

    move-result-object p1

    invoke-virtual {p1, v0}, Lcom/google/firestore/v1/Value$b;->A(Lcom/google/firestore/v1/a$b;)Lcom/google/firestore/v1/Value$b;

    move-result-object p1

    invoke-virtual {p1}, Lcom/google/protobuf/GeneratedMessageLite$a;->p()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object p1

    check-cast p1, Lcom/google/firestore/v1/Value;

    return-object p1
.end method

.method public final e(Ljava/util/Map;Ly12;)Lcom/google/firestore/v1/Value;
    .locals 4

    .line 1
    invoke-interface {p1}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p2}, Ly12;->h()Ls00;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-virtual {p2}, Ly12;->h()Ls00;

    move-result-object p1

    invoke-virtual {p1}, Ljb;->j()Z

    move-result p1

    if-nez p1, :cond_0

    invoke-virtual {p2}, Ly12;->h()Ls00;

    move-result-object p1

    invoke-virtual {p2, p1}, Ly12;->a(Ls00;)V

    :cond_0
    invoke-static {}, Lcom/google/firestore/v1/Value;->x0()Lcom/google/firestore/v1/Value$b;

    move-result-object p1

    invoke-static {}, Lcom/google/firestore/v1/k;->b0()Lcom/google/firestore/v1/k;

    move-result-object p2

    invoke-virtual {p1, p2}, Lcom/google/firestore/v1/Value$b;->I(Lcom/google/firestore/v1/k;)Lcom/google/firestore/v1/Value$b;

    move-result-object p1

    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/GeneratedMessageLite$a;->p()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object p1

    check-cast p1, Lcom/google/firestore/v1/Value;

    return-object p1

    :cond_1
    invoke-static {}, Lcom/google/firestore/v1/k;->j0()Lcom/google/firestore/v1/k$b;

    move-result-object v0

    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_2
    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    instance-of v2, v2, Ljava/lang/String;

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p2, v2}, Ly12;->e(Ljava/lang/String;)Ly12;

    move-result-object v3

    invoke-virtual {p0, v1, v3}, Lb22;->c(Ljava/lang/Object;Ly12;)Lcom/google/firestore/v1/Value;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v0, v2, v1}, Lcom/google/firestore/v1/k$b;->C(Ljava/lang/String;Lcom/google/firestore/v1/Value;)Lcom/google/firestore/v1/k$b;

    goto :goto_1

    :cond_3
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object p1

    filled-new-array {p1}, [Ljava/lang/Object;

    move-result-object p1

    const-string v0, "Non-String Map key (%s) is not allowed"

    invoke-static {v0, p1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Ly12;->f(Ljava/lang/String;)Ljava/lang/RuntimeException;

    move-result-object p1

    throw p1

    :cond_4
    invoke-static {}, Lcom/google/firestore/v1/Value;->x0()Lcom/google/firestore/v1/Value$b;

    move-result-object p1

    invoke-virtual {p1, v0}, Lcom/google/firestore/v1/Value$b;->H(Lcom/google/firestore/v1/k$b;)Lcom/google/firestore/v1/Value$b;

    move-result-object p1

    goto :goto_0
.end method

.method public f(Ljava/lang/Object;Lq00;)Lz12;
    .locals 4

    .line 1
    new-instance v0, Lx12;

    sget-object v1, Lcom/google/firebase/firestore/core/UserData$Source;->MergeSet:Lcom/google/firebase/firestore/core/UserData$Source;

    invoke-direct {v0, v1}, Lx12;-><init>(Lcom/google/firebase/firestore/core/UserData$Source;)V

    invoke-virtual {v0}, Lx12;->e()Ly12;

    move-result-object v1

    invoke-virtual {p0, p1, v1}, Lb22;->a(Ljava/lang/Object;Ly12;)La11;

    move-result-object p1

    if-eqz p2, :cond_2

    invoke-virtual {p2}, Lq00;->c()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ls00;

    invoke-virtual {v0, v2}, Lx12;->d(Ls00;)Z

    move-result v3

    if-eqz v3, :cond_0

    goto :goto_0

    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Field \'"

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljb;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "\' is specified in your field mask but not in your input data."

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    invoke-virtual {v0, p1, p2}, Lx12;->g(La11;Lq00;)Lz12;

    move-result-object p1

    return-object p1

    :cond_2
    invoke-virtual {v0, p1}, Lx12;->f(La11;)Lz12;

    move-result-object p1

    return-object p1
.end method

.method public final g(Ljava/lang/Object;Ly12;)Lcom/google/firestore/v1/Value;
    .locals 3

    .line 1
    if-nez p1, :cond_0

    invoke-static {}, Lcom/google/firestore/v1/Value;->x0()Lcom/google/firestore/v1/Value$b;

    move-result-object p1

    sget-object p2, Lcom/google/protobuf/NullValue;->NULL_VALUE:Lcom/google/protobuf/NullValue;

    invoke-virtual {p1, p2}, Lcom/google/firestore/v1/Value$b;->J(Lcom/google/protobuf/NullValue;)Lcom/google/firestore/v1/Value$b;

    move-result-object p1

    invoke-virtual {p1}, Lcom/google/protobuf/GeneratedMessageLite$a;->p()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object p1

    check-cast p1, Lcom/google/firestore/v1/Value;

    return-object p1

    :cond_0
    instance-of v0, p1, Ljava/lang/Integer;

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/google/firestore/v1/Value;->x0()Lcom/google/firestore/v1/Value$b;

    move-result-object p2

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    int-to-long v0, p1

    invoke-virtual {p2, v0, v1}, Lcom/google/firestore/v1/Value$b;->G(J)Lcom/google/firestore/v1/Value$b;

    move-result-object p1

    invoke-virtual {p1}, Lcom/google/protobuf/GeneratedMessageLite$a;->p()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object p1

    check-cast p1, Lcom/google/firestore/v1/Value;

    return-object p1

    :cond_1
    instance-of v0, p1, Ljava/lang/Long;

    if-eqz v0, :cond_2

    invoke-static {}, Lcom/google/firestore/v1/Value;->x0()Lcom/google/firestore/v1/Value$b;

    move-result-object p2

    check-cast p1, Ljava/lang/Long;

    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p2, v0, v1}, Lcom/google/firestore/v1/Value$b;->G(J)Lcom/google/firestore/v1/Value$b;

    move-result-object p1

    invoke-virtual {p1}, Lcom/google/protobuf/GeneratedMessageLite$a;->p()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object p1

    check-cast p1, Lcom/google/firestore/v1/Value;

    return-object p1

    :cond_2
    instance-of v0, p1, Ljava/lang/Float;

    if-eqz v0, :cond_3

    invoke-static {}, Lcom/google/firestore/v1/Value;->x0()Lcom/google/firestore/v1/Value$b;

    move-result-object p2

    check-cast p1, Ljava/lang/Float;

    invoke-virtual {p1}, Ljava/lang/Float;->doubleValue()D

    move-result-wide v0

    invoke-virtual {p2, v0, v1}, Lcom/google/firestore/v1/Value$b;->E(D)Lcom/google/firestore/v1/Value$b;

    move-result-object p1

    invoke-virtual {p1}, Lcom/google/protobuf/GeneratedMessageLite$a;->p()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object p1

    check-cast p1, Lcom/google/firestore/v1/Value;

    return-object p1

    :cond_3
    instance-of v0, p1, Ljava/lang/Double;

    if-eqz v0, :cond_4

    invoke-static {}, Lcom/google/firestore/v1/Value;->x0()Lcom/google/firestore/v1/Value$b;

    move-result-object p2

    check-cast p1, Ljava/lang/Double;

    invoke-virtual {p1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    invoke-virtual {p2, v0, v1}, Lcom/google/firestore/v1/Value$b;->E(D)Lcom/google/firestore/v1/Value$b;

    move-result-object p1

    invoke-virtual {p1}, Lcom/google/protobuf/GeneratedMessageLite$a;->p()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object p1

    check-cast p1, Lcom/google/firestore/v1/Value;

    return-object p1

    :cond_4
    instance-of v0, p1, Ljava/lang/Boolean;

    if-eqz v0, :cond_5

    invoke-static {}, Lcom/google/firestore/v1/Value;->x0()Lcom/google/firestore/v1/Value$b;

    move-result-object p2

    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    invoke-virtual {p2, p1}, Lcom/google/firestore/v1/Value$b;->C(Z)Lcom/google/firestore/v1/Value$b;

    move-result-object p1

    invoke-virtual {p1}, Lcom/google/protobuf/GeneratedMessageLite$a;->p()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object p1

    check-cast p1, Lcom/google/firestore/v1/Value;

    return-object p1

    :cond_5
    instance-of v0, p1, Ljava/lang/String;

    if-eqz v0, :cond_6

    invoke-static {}, Lcom/google/firestore/v1/Value;->x0()Lcom/google/firestore/v1/Value$b;

    move-result-object p2

    check-cast p1, Ljava/lang/String;

    invoke-virtual {p2, p1}, Lcom/google/firestore/v1/Value$b;->L(Ljava/lang/String;)Lcom/google/firestore/v1/Value$b;

    move-result-object p1

    invoke-virtual {p1}, Lcom/google/protobuf/GeneratedMessageLite$a;->p()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object p1

    check-cast p1, Lcom/google/firestore/v1/Value;

    return-object p1

    :cond_6
    instance-of v0, p1, Ljava/util/Date;

    if-eqz v0, :cond_7

    new-instance p2, Lpw1;

    check-cast p1, Ljava/util/Date;

    invoke-direct {p2, p1}, Lpw1;-><init>(Ljava/util/Date;)V

    invoke-virtual {p0, p2}, Lb22;->j(Lpw1;)Lcom/google/firestore/v1/Value;

    move-result-object p1

    return-object p1

    :cond_7
    instance-of v0, p1, Lpw1;

    if-eqz v0, :cond_8

    check-cast p1, Lpw1;

    invoke-virtual {p0, p1}, Lb22;->j(Lpw1;)Lcom/google/firestore/v1/Value;

    move-result-object p1

    return-object p1

    :cond_8
    instance-of v0, p1, Lia0;

    if-eqz v0, :cond_9

    check-cast p1, Lia0;

    invoke-static {}, Lcom/google/firestore/v1/Value;->x0()Lcom/google/firestore/v1/Value$b;

    move-result-object p2

    invoke-static {}, Lui0;->f0()Lui0$b;

    move-result-object v0

    invoke-virtual {p1}, Lia0;->c()D

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lui0$b;->A(D)Lui0$b;

    move-result-object v0

    invoke-virtual {p1}, Lia0;->d()D

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lui0$b;->B(D)Lui0$b;

    move-result-object p1

    invoke-virtual {p2, p1}, Lcom/google/firestore/v1/Value$b;->F(Lui0$b;)Lcom/google/firestore/v1/Value$b;

    move-result-object p1

    invoke-virtual {p1}, Lcom/google/protobuf/GeneratedMessageLite$a;->p()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object p1

    check-cast p1, Lcom/google/firestore/v1/Value;

    return-object p1

    :cond_9
    instance-of v0, p1, Lec;

    if-eqz v0, :cond_a

    invoke-static {}, Lcom/google/firestore/v1/Value;->x0()Lcom/google/firestore/v1/Value$b;

    move-result-object p2

    check-cast p1, Lec;

    invoke-virtual {p1}, Lec;->d()Lcom/google/protobuf/ByteString;

    move-result-object p1

    invoke-virtual {p2, p1}, Lcom/google/firestore/v1/Value$b;->D(Lcom/google/protobuf/ByteString;)Lcom/google/firestore/v1/Value$b;

    move-result-object p1

    invoke-virtual {p1}, Lcom/google/protobuf/GeneratedMessageLite$a;->p()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object p1

    check-cast p1, Lcom/google/firestore/v1/Value;

    return-object p1

    :cond_a
    instance-of v0, p1, Lcom/google/firebase/firestore/a;

    if-eqz v0, :cond_d

    check-cast p1, Lcom/google/firebase/firestore/a;

    invoke-virtual {p1}, Lcom/google/firebase/firestore/a;->j()Lcom/google/firebase/firestore/FirebaseFirestore;

    move-result-object v0

    if-eqz v0, :cond_c

    invoke-virtual {p1}, Lcom/google/firebase/firestore/a;->j()Lcom/google/firebase/firestore/FirebaseFirestore;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/firebase/firestore/FirebaseFirestore;->d()Lqp;

    move-result-object v0

    iget-object v1, p0, Lb22;->a:Lqp;

    invoke-virtual {v0, v1}, Lqp;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    goto :goto_0

    :cond_b
    invoke-virtual {v0}, Lqp;->f()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0}, Lqp;->e()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lb22;->a:Lqp;

    invoke-virtual {v1}, Lqp;->f()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lb22;->a:Lqp;

    invoke-virtual {v2}, Lqp;->e()Ljava/lang/String;

    move-result-object v2

    filled-new-array {p1, v0, v1, v2}, [Ljava/lang/Object;

    move-result-object p1

    const-string v0, "Document reference is for database %s/%s but should be for database %s/%s"

    invoke-static {v0, p1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Ly12;->f(Ljava/lang/String;)Ljava/lang/RuntimeException;

    move-result-object p1

    throw p1

    :cond_c
    :goto_0
    invoke-static {}, Lcom/google/firestore/v1/Value;->x0()Lcom/google/firestore/v1/Value$b;

    move-result-object p2

    iget-object v0, p0, Lb22;->a:Lqp;

    invoke-virtual {v0}, Lqp;->f()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lb22;->a:Lqp;

    invoke-virtual {v1}, Lqp;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/firebase/firestore/a;->k()Ljava/lang/String;

    move-result-object p1

    filled-new-array {v0, v1, p1}, [Ljava/lang/Object;

    move-result-object p1

    const-string v0, "projects/%s/databases/%s/documents/%s"

    invoke-static {v0, p1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Lcom/google/firestore/v1/Value$b;->K(Ljava/lang/String;)Lcom/google/firestore/v1/Value$b;

    move-result-object p1

    invoke-virtual {p1}, Lcom/google/protobuf/GeneratedMessageLite$a;->p()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object p1

    check-cast p1, Lcom/google/firestore/v1/Value;

    return-object p1

    :cond_d
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->isArray()Z

    move-result v0

    if-eqz v0, :cond_e

    const-string p1, "Arrays are not supported; use a List instead"

    invoke-virtual {p2, p1}, Ly12;->f(Ljava/lang/String;)Ljava/lang/RuntimeException;

    move-result-object p1

    throw p1

    :cond_e
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unsupported type: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {p1}, Lo22;->z(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Ly12;->f(Ljava/lang/String;)Ljava/lang/RuntimeException;

    move-result-object p1

    throw p1
.end method

.method public final h(Lv00;Ly12;)V
    .locals 2

    .line 1
    invoke-virtual {p2}, Ly12;->j()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-virtual {p2}, Ly12;->h()Ls00;

    move-result-object v0

    if-eqz v0, :cond_5

    instance-of v0, p1, Lv00$a;

    if-eqz v0, :cond_3

    invoke-virtual {p2}, Ly12;->g()Lcom/google/firebase/firestore/core/UserData$Source;

    move-result-object p1

    sget-object v0, Lcom/google/firebase/firestore/core/UserData$Source;->MergeSet:Lcom/google/firebase/firestore/core/UserData$Source;

    if-ne p1, v0, :cond_0

    invoke-virtual {p2}, Ly12;->h()Ls00;

    move-result-object p1

    invoke-virtual {p2, p1}, Ly12;->a(Ls00;)V

    goto :goto_1

    :cond_0
    invoke-virtual {p2}, Ly12;->g()Lcom/google/firebase/firestore/core/UserData$Source;

    move-result-object p1

    sget-object v0, Lcom/google/firebase/firestore/core/UserData$Source;->Update:Lcom/google/firebase/firestore/core/UserData$Source;

    if-ne p1, v0, :cond_2

    invoke-virtual {p2}, Ly12;->h()Ls00;

    move-result-object p1

    invoke-virtual {p1}, Ljb;->l()I

    move-result p1

    const/4 v0, 0x0

    if-lez p1, :cond_1

    const/4 p1, 0x1

    goto :goto_0

    :cond_1
    move p1, v0

    :goto_0
    const-string v1, "FieldValue.delete() at the top level should have already been handled."

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p1, v1, v0}, Lg9;->d(ZLjava/lang/String;[Ljava/lang/Object;)V

    const-string p1, "FieldValue.delete() can only appear at the top level of your update data"

    invoke-virtual {p2, p1}, Ly12;->f(Ljava/lang/String;)Ljava/lang/RuntimeException;

    move-result-object p1

    throw p1

    :cond_2
    const-string p1, "FieldValue.delete() can only be used with update() and set() with SetOptions.merge()"

    invoke-virtual {p2, p1}, Ly12;->f(Ljava/lang/String;)Ljava/lang/RuntimeException;

    move-result-object p1

    throw p1

    :cond_3
    instance-of v0, p1, Lv00$b;

    if-eqz v0, :cond_4

    invoke-virtual {p2}, Ly12;->h()Ls00;

    move-result-object p1

    invoke-static {}, Lpl1;->d()Lpl1;

    move-result-object v0

    invoke-virtual {p2, p1, v0}, Ly12;->b(Ls00;Loy1;)V

    :goto_1
    return-void

    :cond_4
    invoke-static {p1}, Lo22;->z(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    filled-new-array {p1}, [Ljava/lang/Object;

    move-result-object p1

    const-string p2, "Unknown FieldValue type: %s"

    invoke-static {p2, p1}, Lg9;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/AssertionError;

    move-result-object p1

    throw p1

    :cond_5
    invoke-virtual {p1}, Lv00;->b()Ljava/lang/String;

    move-result-object p1

    filled-new-array {p1}, [Ljava/lang/Object;

    move-result-object p1

    const-string v0, "%s() is not currently supported inside arrays"

    invoke-static {v0, p1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Ly12;->f(Ljava/lang/String;)Ljava/lang/RuntimeException;

    move-result-object p1

    throw p1

    :cond_6
    invoke-virtual {p1}, Lv00;->b()Ljava/lang/String;

    move-result-object p1

    filled-new-array {p1}, [Ljava/lang/Object;

    move-result-object p1

    const-string v0, "%s() can only be used with set() and update()"

    invoke-static {v0, p1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Ly12;->f(Ljava/lang/String;)Ljava/lang/RuntimeException;

    move-result-object p1

    throw p1
.end method

.method public i(Ljava/lang/Object;)Lz12;
    .locals 2

    .line 1
    new-instance v0, Lx12;

    sget-object v1, Lcom/google/firebase/firestore/core/UserData$Source;->Set:Lcom/google/firebase/firestore/core/UserData$Source;

    invoke-direct {v0, v1}, Lx12;-><init>(Lcom/google/firebase/firestore/core/UserData$Source;)V

    invoke-virtual {v0}, Lx12;->e()Ly12;

    move-result-object v1

    invoke-virtual {p0, p1, v1}, Lb22;->a(Ljava/lang/Object;Ly12;)La11;

    move-result-object p1

    invoke-virtual {v0, p1}, Lx12;->h(La11;)Lz12;

    move-result-object p1

    return-object p1
.end method

.method public final j(Lpw1;)Lcom/google/firestore/v1/Value;
    .locals 5

    .line 1
    invoke-virtual {p1}, Lpw1;->d()I

    move-result v0

    div-int/lit16 v0, v0, 0x3e8

    mul-int/lit16 v0, v0, 0x3e8

    invoke-static {}, Lcom/google/firestore/v1/Value;->x0()Lcom/google/firestore/v1/Value$b;

    move-result-object v1

    invoke-static {}, Lcom/google/protobuf/j0;->f0()Lcom/google/protobuf/j0$b;

    move-result-object v2

    invoke-virtual {p1}, Lpw1;->e()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Lcom/google/protobuf/j0$b;->B(J)Lcom/google/protobuf/j0$b;

    move-result-object p1

    invoke-virtual {p1, v0}, Lcom/google/protobuf/j0$b;->A(I)Lcom/google/protobuf/j0$b;

    move-result-object p1

    invoke-virtual {v1, p1}, Lcom/google/firestore/v1/Value$b;->M(Lcom/google/protobuf/j0$b;)Lcom/google/firestore/v1/Value$b;

    move-result-object p1

    invoke-virtual {p1}, Lcom/google/protobuf/GeneratedMessageLite$a;->p()Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object p1

    check-cast p1, Lcom/google/firestore/v1/Value;

    return-object p1
.end method

.method public k(Ljava/util/Map;)La22;
    .locals 6

    .line 1
    const-string v0, "Provided update data must not be null."

    invoke-static {p1, v0}, Lk71;->c(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lx12;

    sget-object v1, Lcom/google/firebase/firestore/core/UserData$Source;->Update:Lcom/google/firebase/firestore/core/UserData$Source;

    invoke-direct {v0, v1}, Lx12;-><init>(Lcom/google/firebase/firestore/core/UserData$Source;)V

    invoke-virtual {v0}, Lx12;->e()Ly12;

    move-result-object v1

    new-instance v2, La11;

    invoke-direct {v2}, La11;-><init>()V

    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-static {v4}, Lt00;->a(Ljava/lang/String;)Lt00;

    move-result-object v4

    invoke-virtual {v4}, Lt00;->b()Ls00;

    move-result-object v4

    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    instance-of v5, v3, Lv00$a;

    if-eqz v5, :cond_1

    invoke-virtual {v1, v4}, Ly12;->a(Ls00;)V

    goto :goto_0

    :cond_1
    invoke-virtual {v1, v4}, Ly12;->d(Ls00;)Ly12;

    move-result-object v5

    invoke-virtual {p0, v3, v5}, Lb22;->b(Ljava/lang/Object;Ly12;)Lcom/google/firestore/v1/Value;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {v1, v4}, Ly12;->a(Ls00;)V

    invoke-virtual {v2, v4, v3}, La11;->m(Ls00;Lcom/google/firestore/v1/Value;)V

    goto :goto_0

    :cond_2
    invoke-virtual {v0, v2}, Lx12;->i(La11;)La22;

    move-result-object p1

    return-object p1
.end method

.class public Lzm$d$a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lzm$d;->a(Ljava/lang/Boolean;)Lcom/google/android/gms/tasks/Task;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Ljava/lang/Boolean;

.field public final synthetic b:Lzm$d;


# direct methods
.method public constructor <init>(Lzm$d;Ljava/lang/Boolean;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lzm$d$a;->b:Lzm$d;

    iput-object p2, p0, Lzm$d$a;->a:Ljava/lang/Boolean;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Lcom/google/android/gms/tasks/Task;
    .locals 3

    .line 1
    iget-object v0, p0, Lzm$d$a;->a:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lzl0;->f()Lzl0;

    move-result-object v0

    const-string v1, "Deleting cached crash reports..."

    invoke-virtual {v0, v1}, Lzl0;->i(Ljava/lang/String;)V

    iget-object v0, p0, Lzm$d$a;->b:Lzm$d;

    iget-object v0, v0, Lzm$d;->b:Lzm;

    invoke-virtual {v0}, Lzm;->L()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lzm;->d(Ljava/util/List;)V

    iget-object v0, p0, Lzm$d$a;->b:Lzm$d;

    iget-object v0, v0, Lzm$d;->b:Lzm;

    invoke-static {v0}, Lzm;->h(Lzm;)Lnm1;

    move-result-object v0

    invoke-virtual {v0}, Lnm1;->v()V

    iget-object v0, p0, Lzm$d$a;->b:Lzm$d;

    iget-object v0, v0, Lzm$d;->b:Lzm;

    iget-object v0, v0, Lzm;->r:Lcom/google/android/gms/tasks/TaskCompletionSource;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/tasks/TaskCompletionSource;->trySetResult(Ljava/lang/Object;)Z

    invoke-static {v1}, Lcom/google/android/gms/tasks/Tasks;->forResult(Ljava/lang/Object;)Lcom/google/android/gms/tasks/Task;

    move-result-object v0

    return-object v0

    :cond_0
    invoke-static {}, Lzl0;->f()Lzl0;

    move-result-object v0

    const-string v1, "Sending cached crash reports..."

    invoke-virtual {v0, v1}, Lzl0;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lzm$d$a;->a:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iget-object v1, p0, Lzm$d$a;->b:Lzm$d;

    iget-object v1, v1, Lzm$d;->b:Lzm;

    invoke-static {v1}, Lzm;->l(Lzm;)Ldp;

    move-result-object v1

    invoke-virtual {v1, v0}, Ldp;->c(Z)V

    iget-object v0, p0, Lzm$d$a;->b:Lzm$d;

    iget-object v0, v0, Lzm$d;->b:Lzm;

    invoke-static {v0}, Lzm;->m(Lzm;)Lxm;

    move-result-object v0

    invoke-virtual {v0}, Lxm;->c()Ljava/util/concurrent/Executor;

    move-result-object v0

    iget-object v1, p0, Lzm$d$a;->b:Lzm$d;

    iget-object v1, v1, Lzm$d;->a:Lcom/google/android/gms/tasks/Task;

    new-instance v2, Lzm$d$a$a;

    invoke-direct {v2, p0, v0}, Lzm$d$a$a;-><init>(Lzm$d$a;Ljava/util/concurrent/Executor;)V

    invoke-virtual {v1, v0, v2}, Lcom/google/android/gms/tasks/Task;->onSuccessTask(Ljava/util/concurrent/Executor;Lcom/google/android/gms/tasks/SuccessContinuation;)Lcom/google/android/gms/tasks/Task;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lzm$d$a;->a()Lcom/google/android/gms/tasks/Task;

    move-result-object v0

    return-object v0
.end method

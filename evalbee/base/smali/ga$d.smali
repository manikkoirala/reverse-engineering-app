.class public final Lga$d;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lw01;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lga;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "d"
.end annotation


# static fields
.field public static final a:Lga$d;

.field public static final b:Ln00;

.field public static final c:Ln00;

.field public static final d:Ln00;

.field public static final e:Ln00;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .line 1
    new-instance v0, Lga$d;

    invoke-direct {v0}, Lga$d;-><init>()V

    sput-object v0, Lga$d;->a:Lga$d;

    const-string v0, "processName"

    invoke-static {v0}, Ln00;->d(Ljava/lang/String;)Ln00;

    move-result-object v0

    sput-object v0, Lga$d;->b:Ln00;

    const-string v0, "pid"

    invoke-static {v0}, Ln00;->d(Ljava/lang/String;)Ln00;

    move-result-object v0

    sput-object v0, Lga$d;->c:Ln00;

    const-string v0, "importance"

    invoke-static {v0}, Ln00;->d(Ljava/lang/String;)Ln00;

    move-result-object v0

    sput-object v0, Lga$d;->d:Ln00;

    const-string v0, "defaultProcess"

    invoke-static {v0}, Ln00;->d(Ljava/lang/String;)Ln00;

    move-result-object v0

    sput-object v0, Lga$d;->e:Ln00;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lf81;Lx01;)V
    .locals 2

    .line 1
    sget-object v0, Lga$d;->b:Ln00;

    invoke-virtual {p1}, Lf81;->c()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2, v0, v1}, Lx01;->f(Ln00;Ljava/lang/Object;)Lx01;

    sget-object v0, Lga$d;->c:Ln00;

    invoke-virtual {p1}, Lf81;->b()I

    move-result v1

    invoke-interface {p2, v0, v1}, Lx01;->c(Ln00;I)Lx01;

    sget-object v0, Lga$d;->d:Ln00;

    invoke-virtual {p1}, Lf81;->a()I

    move-result v1

    invoke-interface {p2, v0, v1}, Lx01;->c(Ln00;I)Lx01;

    sget-object v0, Lga$d;->e:Ln00;

    invoke-virtual {p1}, Lf81;->d()Z

    move-result p1

    invoke-interface {p2, v0, p1}, Lx01;->b(Ln00;Z)Lx01;

    return-void
.end method

.method public bridge synthetic encode(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .line 1
    check-cast p1, Lf81;

    check-cast p2, Lx01;

    invoke-virtual {p0, p1, p2}, Lga$d;->a(Lf81;Lx01;)V

    return-void
.end method

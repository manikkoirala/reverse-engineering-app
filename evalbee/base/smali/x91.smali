.class public abstract Lx91;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(Landroid/content/Context;Lcom/ekodroid/omrevaluator/templateui/models/ExamId;ZZLjava/lang/Long;Ljava/lang/String;Ljava/lang/String;)Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/PublishRequest;
    .locals 15

    .line 1
    move-object/from16 v10, p1

    new-instance v11, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/PublishRequest;

    invoke-direct {v11}, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/PublishRequest;-><init>()V

    const-wide v0, 0x757b12c00L

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, v11, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/PublishRequest;->expiresIn:Ljava/lang/Long;

    invoke-static {p0}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;

    move-result-object v12

    invoke-static {p0}, Lcom/ekodroid/omrevaluator/database/repositories/ClassRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/ClassRepository;

    move-result-object v13

    invoke-static {p0}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;

    move-result-object v0

    invoke-virtual {v0, v10}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getTemplateJson(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->getCloudId()Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v11, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/PublishRequest;->cloudId:Ljava/lang/Long;

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->isCloudSyncOn()Z

    move-result v1

    if-nez v1, :cond_3

    const-wide/32 v1, 0x240c8400

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v11, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/PublishRequest;->expiresIn:Ljava/lang/Long;

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->getSheetTemplate()Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    move-result-object v1

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->isPublished()Z

    move-result v2

    if-eqz v2, :cond_0

    sget-object v2, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/Published;->PUBLISHED:Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/Published;

    goto :goto_0

    :cond_0
    sget-object v2, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/Published;->NOT_PUBLISHED:Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/Published;

    :goto_0
    move-object v6, v2

    new-instance v14, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/ExamPartialResponse;

    const/4 v2, 0x0

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->getLastUpdatedOn()Ljava/lang/Long;

    move-result-object v3

    new-instance v4, Lgc0;

    invoke-direct {v4}, Lgc0;-><init>()V

    invoke-virtual {v4, v1}, Lgc0;->s(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getTemplateVersion()I

    move-result v5

    sget-object v7, Lcom/ekodroid/omrevaluator/serializable/SharedType;->PUBLIC:Lcom/ekodroid/omrevaluator/serializable/SharedType;

    const/4 v8, 0x0

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->isSyncImages()Z

    move-result v9

    move-object v0, v14

    move-object v1, v2

    move-object v2, v3

    move-object/from16 v3, p1

    invoke-direct/range {v0 .. v9}, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/ExamPartialResponse;-><init>(Ljava/lang/Long;Ljava/lang/Long;Lcom/ekodroid/omrevaluator/templateui/models/ExamId;Ljava/lang/String;ILcom/ekodroid/omrevaluator/clients/SyncClients/Models/Published;Lcom/ekodroid/omrevaluator/serializable/SharedType;Ljava/lang/String;Z)V

    iput-object v14, v11, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/PublishRequest;->examData:Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/ExamPartialResponse;

    invoke-virtual {v12, v10}, Lcom/ekodroid/omrevaluator/database/repositories/ResultRepository;->getAllNotSyncedResults(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)Ljava/util/ArrayList;

    move-result-object v0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;->isPublished()Z

    move-result v3

    if-eqz v3, :cond_1

    sget-object v3, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/Published;->PUBLISHED:Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/Published;

    goto :goto_2

    :cond_1
    sget-object v3, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/Published;->NOT_PUBLISHED:Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/Published;

    :goto_2
    move-object v9, v3

    new-instance v3, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/StudentResultPartialResponse;

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;->getRollNo()I

    move-result v5

    const-wide/16 v6, 0x0

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;->getResultItemString()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    move-object v4, v3

    invoke-direct/range {v4 .. v9}, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/StudentResultPartialResponse;-><init>(ILjava/lang/Long;Ljava/lang/String;ILcom/ekodroid/omrevaluator/clients/SyncClients/Models/Published;)V

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    iput-object v1, v11, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/PublishRequest;->studentResults:Ljava/util/ArrayList;

    :cond_3
    invoke-virtual/range {p1 .. p1}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;->getClassName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v13, v0}, Lcom/ekodroid/omrevaluator/database/repositories/ClassRepository;->getStudents(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v11, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/PublishRequest;->publishMetaDataDtos:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_4
    :goto_3
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/ekodroid/omrevaluator/database/StudentDataModel;

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/database/StudentDataModel;->getEmailId()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_4

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/database/StudentDataModel;->getEmailId()Ljava/lang/String;

    move-result-object v2

    const-string v3, "@"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/database/StudentDataModel;->getEmailId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    const/4 v3, 0x5

    if-le v2, v3, :cond_4

    iget-object v2, v11, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/PublishRequest;->publishMetaDataDtos:Ljava/util/ArrayList;

    new-instance v12, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/PublishMetaDataDto;

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/database/StudentDataModel;->getStudentName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/database/StudentDataModel;->getEmailId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/database/StudentDataModel;->getRollNO()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v6

    move-object v3, v12

    move-object/from16 v7, p5

    move-object/from16 v8, p6

    move/from16 v9, p3

    move/from16 v10, p2

    invoke-direct/range {v3 .. v10}, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/PublishMetaDataDto;-><init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;ZZ)V

    invoke-virtual {v2, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_5
    return-object v11
.end method

.method public static b(Landroid/content/Context;Lcom/ekodroid/omrevaluator/templateui/models/ExamId;ZZLjava/lang/Long;Ljava/lang/String;Ljava/lang/String;I)Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/PublishRequest;
    .locals 8

    .line 1
    new-instance p4, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/PublishRequest;

    invoke-direct {p4}, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/PublishRequest;-><init>()V

    const-wide v0, 0x757b12c00L

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p4, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/PublishRequest;->expiresIn:Ljava/lang/Long;

    invoke-static {p0}, Lcom/ekodroid/omrevaluator/database/repositories/ClassRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/ClassRepository;

    move-result-object v0

    invoke-static {p0}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;

    move-result-object p0

    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getTemplateJson(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;

    move-result-object p0

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->getCloudId()Ljava/lang/Long;

    move-result-object p0

    iput-object p0, p4, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/PublishRequest;->cloudId:Ljava/lang/Long;

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;->getClassName()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, p7, p0}, Lcom/ekodroid/omrevaluator/database/repositories/ClassRepository;->getStudent(ILjava/lang/String;)Lcom/ekodroid/omrevaluator/database/StudentDataModel;

    move-result-object p0

    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p4, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/PublishRequest;->publishMetaDataDtos:Ljava/util/ArrayList;

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/database/StudentDataModel;->getEmailId()Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/database/StudentDataModel;->getEmailId()Ljava/lang/String;

    move-result-object p1

    const-string p7, "@"

    invoke-virtual {p1, p7}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result p1

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/database/StudentDataModel;->getEmailId()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result p1

    const/4 p7, 0x5

    if-le p1, p7, :cond_0

    iget-object p1, p4, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/PublishRequest;->publishMetaDataDtos:Ljava/util/ArrayList;

    new-instance p7, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/PublishMetaDataDto;

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/database/StudentDataModel;->getStudentName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/database/StudentDataModel;->getEmailId()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/database/StudentDataModel;->getRollNO()Ljava/lang/Integer;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    move-object v0, p7

    move-object v4, p5

    move-object v5, p6

    move v6, p3

    move v7, p2

    invoke-direct/range {v0 .. v7}, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/PublishMetaDataDto;-><init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;ZZ)V

    invoke-virtual {p1, p7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    return-object p4
.end method

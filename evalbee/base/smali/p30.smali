.class public Lp30;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Ljava/util/Map;

.field public final b:Lr10;

.field public final c:Lr91;

.field public final d:Lr91;


# direct methods
.method public constructor <init>(Lr10;Lr91;Lr91;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lp30;->a:Ljava/util/Map;

    iput-object p1, p0, Lp30;->b:Lr10;

    iput-object p2, p0, Lp30;->c:Lr91;

    iput-object p3, p0, Lp30;->d:Lr91;

    invoke-static {p4, p5}, Lbr1;->c(Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;)V

    return-void
.end method


# virtual methods
.method public declared-synchronized a(Ljava/lang/String;)Lo30;
    .locals 4

    .line 1
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lp30;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lo30;

    if-nez v0, :cond_0

    new-instance v0, Lo30;

    iget-object v1, p0, Lp30;->b:Lr10;

    iget-object v2, p0, Lp30;->c:Lr91;

    iget-object v3, p0, Lp30;->d:Lr91;

    invoke-direct {v0, p1, v1, v2, v3}, Lo30;-><init>(Ljava/lang/String;Lr10;Lr91;Lr91;)V

    iget-object v1, p0, Lp30;->a:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

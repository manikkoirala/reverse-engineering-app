.class public Lvx$d;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lvx;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Landroid/widget/Button;

.field public final synthetic b:Ljava/util/ArrayList;

.field public final synthetic c:Landroid/widget/Spinner;

.field public final synthetic d:Landroid/widget/TextView;

.field public final synthetic e:Lvx;


# direct methods
.method public constructor <init>(Lvx;Landroid/widget/Button;Ljava/util/ArrayList;Landroid/widget/Spinner;Landroid/widget/TextView;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lvx$d;->e:Lvx;

    iput-object p2, p0, Lvx$d;->a:Landroid/widget/Button;

    iput-object p3, p0, Lvx$d;->b:Ljava/util/ArrayList;

    iput-object p4, p0, Lvx$d;->c:Landroid/widget/Spinner;

    iput-object p5, p0, Lvx$d;->d:Landroid/widget/TextView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4

    .line 1
    iget-object p1, p0, Lvx$d;->e:Lvx;

    invoke-static {p1}, Lvx;->c(Lvx;)Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/SharedExamDetailsDto;

    move-result-object p1

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    iget-object p1, p0, Lvx$d;->e:Lvx;

    iget-object p1, p1, Lvx;->a:Landroid/content/Context;

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;

    move-result-object p1

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;->getRole()Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserRole;

    move-result-object p1

    sget-object v1, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserRole;->OWNER:Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserRole;

    if-eq p1, v1, :cond_0

    invoke-static {}, Lcom/google/firebase/auth/FirebaseAuth;->getInstance()Lcom/google/firebase/auth/FirebaseAuth;

    move-result-object p1

    invoke-virtual {p1}, Lcom/google/firebase/auth/FirebaseAuth;->e()Lr30;

    move-result-object p1

    invoke-virtual {p1}, Lr30;->O()Ljava/lang/String;

    move-result-object p1

    iget-object v1, p0, Lvx$d;->e:Lvx;

    invoke-static {v1}, Lvx;->c(Lvx;)Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/SharedExamDetailsDto;

    move-result-object v1

    iget-object v1, v1, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/SharedExamDetailsDto;->ownerUid:Ljava/lang/String;

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_0

    iget-object p1, p0, Lvx$d;->a:Landroid/widget/Button;

    invoke-virtual {p1, v0}, Landroid/view/View;->setEnabled(Z)V

    iget-object p1, p0, Lvx$d;->e:Lvx;

    iget-object p1, p1, Lvx;->a:Landroid/content/Context;

    const v1, 0x7f0800bd

    const v2, 0x7f08016e

    const v3, 0x7f1201a2

    invoke-static {p1, v3, v1, v2}, La91;->G(Landroid/content/Context;III)V

    :cond_0
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iget-object v1, p0, Lvx$d;->b:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lsu1;

    iget-boolean v3, v2, Lsu1;->b:Z

    if-eqz v3, :cond_1

    iget-object v2, v2, Lsu1;->a:Lcom/ekodroid/omrevaluator/more/models/Teacher;

    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lvx$d;->c:Landroid/widget/Spinner;

    invoke-virtual {v1}, Landroid/widget/AdapterView;->getSelectedItemPosition()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_3

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-nez v1, :cond_3

    iget-object p1, p0, Lvx$d;->d:Landroid/widget/TextView;

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    iget-object p1, p0, Lvx$d;->d:Landroid/widget/TextView;

    const v0, 0x7f1201e4

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(I)V

    return-void

    :cond_3
    new-instance v0, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/SharedExamPartial;

    invoke-direct {v0}, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/SharedExamPartial;-><init>()V

    iget-object v1, p0, Lvx$d;->e:Lvx;

    invoke-static {v1}, Lvx;->c(Lvx;)Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/SharedExamDetailsDto;

    move-result-object v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lvx$d;->e:Lvx;

    invoke-static {v1}, Lvx;->c(Lvx;)Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/SharedExamDetailsDto;

    move-result-object v1

    iget-wide v1, v1, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/SharedExamDetailsDto;->examId:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/SharedExamPartial;->setExamId(Ljava/lang/Long;)V

    :cond_4
    iget-object v1, p0, Lvx$d;->e:Lvx;

    invoke-static {v1}, Lvx;->d(Lvx;)Lcom/ekodroid/omrevaluator/serializable/SharedType;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/SharedExamPartial;->setSharedType(Lcom/ekodroid/omrevaluator/serializable/SharedType;)V

    invoke-virtual {v0, p1}, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/SharedExamPartial;->setTeachers(Ljava/util/ArrayList;)V

    iget-object p1, p0, Lvx$d;->e:Lvx;

    iget-object p1, p1, Lvx;->e:Landroid/widget/CheckBox;

    invoke-virtual {p1}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result p1

    invoke-virtual {v0, p1}, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/SharedExamPartial;->setSyncImages(Z)V

    iget-object p1, p0, Lvx$d;->e:Lvx;

    invoke-static {p1}, Lvx;->a(Lvx;)Ly01;

    move-result-object p1

    invoke-interface {p1, v0}, Ly01;->a(Ljava/lang/Object;)V

    iget-object p1, p0, Lvx$d;->e:Lvx;

    invoke-virtual {p1}, Landroid/app/Dialog;->dismiss()V

    return-void
.end method

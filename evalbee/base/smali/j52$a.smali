.class public Lj52$a;
.super Lry1;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lj52;->onDisappear(Landroid/view/ViewGroup;Lxy1;ILxy1;I)Landroid/animation/Animator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Landroid/view/ViewGroup;

.field public final synthetic b:Landroid/view/View;

.field public final synthetic c:Landroid/view/View;

.field public final synthetic d:Lj52;


# direct methods
.method public constructor <init>(Lj52;Landroid/view/ViewGroup;Landroid/view/View;Landroid/view/View;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lj52$a;->d:Lj52;

    iput-object p2, p0, Lj52$a;->a:Landroid/view/ViewGroup;

    iput-object p3, p0, Lj52$a;->b:Landroid/view/View;

    iput-object p4, p0, Lj52$a;->c:Landroid/view/View;

    invoke-direct {p0}, Lry1;-><init>()V

    return-void
.end method


# virtual methods
.method public onTransitionEnd(Lqy1;)V
    .locals 3

    .line 1
    iget-object v0, p0, Lj52$a;->c:Landroid/view/View;

    sget v1, Lkb1;->a:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    iget-object v0, p0, Lj52$a;->a:Landroid/view/ViewGroup;

    invoke-static {v0}, Lw32;->a(Landroid/view/ViewGroup;)Lu32;

    move-result-object v0

    iget-object v1, p0, Lj52$a;->b:Landroid/view/View;

    invoke-interface {v0, v1}, Lu32;->remove(Landroid/view/View;)V

    invoke-virtual {p1, p0}, Lqy1;->removeListener(Lqy1$g;)Lqy1;

    return-void
.end method

.method public onTransitionPause(Lqy1;)V
    .locals 1

    .line 1
    iget-object p1, p0, Lj52$a;->a:Landroid/view/ViewGroup;

    invoke-static {p1}, Lw32;->a(Landroid/view/ViewGroup;)Lu32;

    move-result-object p1

    iget-object v0, p0, Lj52$a;->b:Landroid/view/View;

    invoke-interface {p1, v0}, Lu32;->remove(Landroid/view/View;)V

    return-void
.end method

.method public onTransitionResume(Lqy1;)V
    .locals 1

    .line 1
    iget-object p1, p0, Lj52$a;->b:Landroid/view/View;

    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object p1

    if-nez p1, :cond_0

    iget-object p1, p0, Lj52$a;->a:Landroid/view/ViewGroup;

    invoke-static {p1}, Lw32;->a(Landroid/view/ViewGroup;)Lu32;

    move-result-object p1

    iget-object v0, p0, Lj52$a;->b:Landroid/view/View;

    invoke-interface {p1, v0}, Lu32;->add(Landroid/view/View;)V

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lj52$a;->d:Lj52;

    invoke-virtual {p1}, Lqy1;->cancel()V

    :goto_0
    return-void
.end method

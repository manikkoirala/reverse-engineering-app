.class public La6;
.super Landroid/widget/CheckBox;
.source "SourceFile"

# interfaces
.implements Luw1;


# instance fields
.field private mAppCompatEmojiTextHelper:Lv6;

.field private final mBackgroundTintHelper:Lx5;

.field private final mCompoundButtonHelper:Ld6;

.field private final mTextHelper:Ll7;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .line 1
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, La6;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .line 2
    sget v0, Lsa1;->o:I

    invoke-direct {p0, p1, p2, v0}, La6;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .line 3
    invoke-static {p1}, Lqw1;->b(Landroid/content/Context;)Landroid/content/Context;

    move-result-object p1

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/CheckBox;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object p1

    invoke-static {p0, p1}, Lpv1;->a(Landroid/view/View;Landroid/content/Context;)V

    new-instance p1, Ld6;

    invoke-direct {p1, p0}, Ld6;-><init>(Landroid/widget/CompoundButton;)V

    iput-object p1, p0, La6;->mCompoundButtonHelper:Ld6;

    invoke-virtual {p1, p2, p3}, Ld6;->e(Landroid/util/AttributeSet;I)V

    new-instance p1, Lx5;

    invoke-direct {p1, p0}, Lx5;-><init>(Landroid/view/View;)V

    iput-object p1, p0, La6;->mBackgroundTintHelper:Lx5;

    invoke-virtual {p1, p2, p3}, Lx5;->e(Landroid/util/AttributeSet;I)V

    new-instance p1, Ll7;

    invoke-direct {p1, p0}, Ll7;-><init>(Landroid/widget/TextView;)V

    iput-object p1, p0, La6;->mTextHelper:Ll7;

    invoke-virtual {p1, p2, p3}, Ll7;->m(Landroid/util/AttributeSet;I)V

    invoke-direct {p0}, La6;->getEmojiTextViewHelper()Lv6;

    move-result-object p1

    invoke-virtual {p1, p2, p3}, Lv6;->c(Landroid/util/AttributeSet;I)V

    return-void
.end method

.method private getEmojiTextViewHelper()Lv6;
    .locals 1

    .line 1
    iget-object v0, p0, La6;->mAppCompatEmojiTextHelper:Lv6;

    if-nez v0, :cond_0

    new-instance v0, Lv6;

    invoke-direct {v0, p0}, Lv6;-><init>(Landroid/widget/TextView;)V

    iput-object v0, p0, La6;->mAppCompatEmojiTextHelper:Lv6;

    :cond_0
    iget-object v0, p0, La6;->mAppCompatEmojiTextHelper:Lv6;

    return-object v0
.end method


# virtual methods
.method public drawableStateChanged()V
    .locals 1

    .line 1
    invoke-super {p0}, Landroid/view/View;->drawableStateChanged()V

    iget-object v0, p0, La6;->mBackgroundTintHelper:Lx5;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lx5;->b()V

    :cond_0
    iget-object v0, p0, La6;->mTextHelper:Ll7;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ll7;->b()V

    :cond_1
    return-void
.end method

.method public getCompoundPaddingLeft()I
    .locals 2

    .line 1
    invoke-super {p0}, Landroid/widget/TextView;->getCompoundPaddingLeft()I

    move-result v0

    iget-object v1, p0, La6;->mCompoundButtonHelper:Ld6;

    if-eqz v1, :cond_0

    invoke-virtual {v1, v0}, Ld6;->b(I)I

    move-result v0

    :cond_0
    return v0
.end method

.method public getSupportBackgroundTintList()Landroid/content/res/ColorStateList;
    .locals 1

    .line 1
    iget-object v0, p0, La6;->mBackgroundTintHelper:Lx5;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lx5;->c()Landroid/content/res/ColorStateList;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public getSupportBackgroundTintMode()Landroid/graphics/PorterDuff$Mode;
    .locals 1

    .line 1
    iget-object v0, p0, La6;->mBackgroundTintHelper:Lx5;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lx5;->d()Landroid/graphics/PorterDuff$Mode;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public getSupportButtonTintList()Landroid/content/res/ColorStateList;
    .locals 1

    .line 1
    iget-object v0, p0, La6;->mCompoundButtonHelper:Ld6;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ld6;->c()Landroid/content/res/ColorStateList;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public getSupportButtonTintMode()Landroid/graphics/PorterDuff$Mode;
    .locals 1

    .line 1
    iget-object v0, p0, La6;->mCompoundButtonHelper:Ld6;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ld6;->d()Landroid/graphics/PorterDuff$Mode;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public getSupportCompoundDrawablesTintList()Landroid/content/res/ColorStateList;
    .locals 1

    .line 1
    iget-object v0, p0, La6;->mTextHelper:Ll7;

    invoke-virtual {v0}, Ll7;->j()Landroid/content/res/ColorStateList;

    move-result-object v0

    return-object v0
.end method

.method public getSupportCompoundDrawablesTintMode()Landroid/graphics/PorterDuff$Mode;
    .locals 1

    .line 1
    iget-object v0, p0, La6;->mTextHelper:Ll7;

    invoke-virtual {v0}, Ll7;->k()Landroid/graphics/PorterDuff$Mode;

    move-result-object v0

    return-object v0
.end method

.method public isEmojiCompatEnabled()Z
    .locals 1

    .line 1
    invoke-direct {p0}, La6;->getEmojiTextViewHelper()Lv6;

    move-result-object v0

    invoke-virtual {v0}, Lv6;->b()Z

    move-result v0

    return v0
.end method

.method public setAllCaps(Z)V
    .locals 1

    .line 1
    invoke-super {p0, p1}, Landroid/widget/TextView;->setAllCaps(Z)V

    invoke-direct {p0}, La6;->getEmojiTextViewHelper()Lv6;

    move-result-object v0

    invoke-virtual {v0, p1}, Lv6;->d(Z)V

    return-void
.end method

.method public setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .line 1
    invoke-super {p0, p1}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, La6;->mBackgroundTintHelper:Lx5;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lx5;->f(Landroid/graphics/drawable/Drawable;)V

    :cond_0
    return-void
.end method

.method public setBackgroundResource(I)V
    .locals 1

    .line 1
    invoke-super {p0, p1}, Landroid/view/View;->setBackgroundResource(I)V

    iget-object v0, p0, La6;->mBackgroundTintHelper:Lx5;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lx5;->g(I)V

    :cond_0
    return-void
.end method

.method public setButtonDrawable(I)V
    .locals 1

    .line 1
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, Lg7;->b(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object p1

    invoke-virtual {p0, p1}, La6;->setButtonDrawable(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method public setButtonDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .line 2
    invoke-super {p0, p1}, Landroid/widget/CompoundButton;->setButtonDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object p1, p0, La6;->mCompoundButtonHelper:Ld6;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ld6;->f()V

    :cond_0
    return-void
.end method

.method public setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .line 1
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/TextView;->setCompoundDrawables(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    iget-object p1, p0, La6;->mTextHelper:Ll7;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ll7;->p()V

    :cond_0
    return-void
.end method

.method public setCompoundDrawablesRelative(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V
    .locals 0

    .line 1
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/TextView;->setCompoundDrawablesRelative(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    iget-object p1, p0, La6;->mTextHelper:Ll7;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ll7;->p()V

    :cond_0
    return-void
.end method

.method public setEmojiCompatEnabled(Z)V
    .locals 1

    .line 1
    invoke-direct {p0}, La6;->getEmojiTextViewHelper()Lv6;

    move-result-object v0

    invoke-virtual {v0, p1}, Lv6;->e(Z)V

    return-void
.end method

.method public setFilters([Landroid/text/InputFilter;)V
    .locals 1

    .line 1
    invoke-direct {p0}, La6;->getEmojiTextViewHelper()Lv6;

    move-result-object v0

    invoke-virtual {v0, p1}, Lv6;->a([Landroid/text/InputFilter;)[Landroid/text/InputFilter;

    move-result-object p1

    invoke-super {p0, p1}, Landroid/widget/TextView;->setFilters([Landroid/text/InputFilter;)V

    return-void
.end method

.method public setSupportBackgroundTintList(Landroid/content/res/ColorStateList;)V
    .locals 1

    .line 1
    iget-object v0, p0, La6;->mBackgroundTintHelper:Lx5;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lx5;->i(Landroid/content/res/ColorStateList;)V

    :cond_0
    return-void
.end method

.method public setSupportBackgroundTintMode(Landroid/graphics/PorterDuff$Mode;)V
    .locals 1

    .line 1
    iget-object v0, p0, La6;->mBackgroundTintHelper:Lx5;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lx5;->j(Landroid/graphics/PorterDuff$Mode;)V

    :cond_0
    return-void
.end method

.method public setSupportButtonTintList(Landroid/content/res/ColorStateList;)V
    .locals 1

    .line 1
    iget-object v0, p0, La6;->mCompoundButtonHelper:Ld6;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Ld6;->g(Landroid/content/res/ColorStateList;)V

    :cond_0
    return-void
.end method

.method public setSupportButtonTintMode(Landroid/graphics/PorterDuff$Mode;)V
    .locals 1

    .line 1
    iget-object v0, p0, La6;->mCompoundButtonHelper:Ld6;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Ld6;->h(Landroid/graphics/PorterDuff$Mode;)V

    :cond_0
    return-void
.end method

.method public setSupportCompoundDrawablesTintList(Landroid/content/res/ColorStateList;)V
    .locals 1

    .line 1
    iget-object v0, p0, La6;->mTextHelper:Ll7;

    invoke-virtual {v0, p1}, Ll7;->w(Landroid/content/res/ColorStateList;)V

    iget-object p1, p0, La6;->mTextHelper:Ll7;

    invoke-virtual {p1}, Ll7;->b()V

    return-void
.end method

.method public setSupportCompoundDrawablesTintMode(Landroid/graphics/PorterDuff$Mode;)V
    .locals 1

    .line 1
    iget-object v0, p0, La6;->mTextHelper:Ll7;

    invoke-virtual {v0, p1}, Ll7;->x(Landroid/graphics/PorterDuff$Mode;)V

    iget-object p1, p0, La6;->mTextHelper:Ll7;

    invoke-virtual {p1}, Ll7;->b()V

    return-void
.end method

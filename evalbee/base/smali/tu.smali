.class public Ltu;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# instance fields
.field public a:Landroid/content/Context;

.field public b:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

.field public c:Z

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

.field public g:Landroid/app/ProgressDialog;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/ekodroid/omrevaluator/templateui/models/ExamId;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    iput-object p1, p0, Ltu;->a:Landroid/content/Context;

    iput-object p3, p0, Ltu;->b:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    iput-object p2, p0, Ltu;->f:Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-virtual {p3}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getName()Ljava/lang/String;

    move-result-object p2

    const-string p3, "[^a-zA-Z0-9_-]"

    const-string v0, "_"

    invoke-virtual {p2, p3, v0}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    iput-object p2, p0, Ltu;->d:Ljava/lang/String;

    sget-object p2, Lok;->c:Ljava/lang/String;

    invoke-virtual {p1, p2}, Landroid/content/Context;->getExternalFilesDir(Ljava/lang/String;)Ljava/io/File;

    move-result-object p1

    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result p2

    if-nez p2, :cond_0

    invoke-virtual {p1}, Ljava/io/File;->mkdirs()Z

    :cond_0
    new-instance p2, Ljava/io/File;

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, p0, Ltu;->d:Ljava/lang/String;

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "_key.csv"

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    invoke-direct {p2, p1, p3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {p2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Ltu;->e:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public final a([[Ljava/lang/String;Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;)V
    .locals 4

    .line 1
    const/4 v0, 0x0

    aget-object v1, p1, v0

    invoke-virtual {p2}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getQueNoString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    invoke-virtual {p2}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getQuestionNumberLabels()[Ljava/lang/String;

    move-result-object p2

    const/4 v1, 0x1

    :goto_0
    array-length v2, p1

    if-ge v1, v2, :cond_0

    aget-object v2, p1, v1

    add-int/lit8 v3, v1, -0x1

    aget-object v3, p2, v3

    aput-object v3, v2, v0

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public final b(Ljava/lang/String;)Z
    .locals 12

    .line 1
    const/4 v0, 0x0

    const/4 v1, 0x0

    :try_start_0
    new-instance v2, Ljava/io/FileOutputStream;

    new-instance v3, Ljava/io/File;

    invoke-direct {v3, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-direct {v2, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    new-instance p1, Lzd;

    new-instance v1, Ljava/io/OutputStreamWriter;

    invoke-direct {v1, v2}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;)V

    invoke-direct {p1, v1}, Lzd;-><init>(Ljava/io/Writer;)V

    iget-object v1, p0, Ltu;->b:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getLabelProfile()Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    move-result-object v1

    iget-object v3, p0, Ltu;->b:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    invoke-virtual {v3}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getAnswerKeys()[Lcom/ekodroid/omrevaluator/templateui/models/AnswerSetKey;

    move-result-object v3

    if-eqz v3, :cond_5

    array-length v4, v3

    if-nez v4, :cond_0

    goto/16 :goto_4

    :cond_0
    aget-object v4, v3, v0

    invoke-virtual {v4}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerSetKey;->getAnswerOptionKeys()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/4 v5, 0x1

    add-int/2addr v4, v5

    array-length v6, v3

    add-int/2addr v6, v5

    filled-new-array {v4, v6}, [I

    move-result-object v4

    const-class v6, Ljava/lang/String;

    invoke-static {v6, v4}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [[Ljava/lang/String;

    invoke-virtual {p0, v4, v1}, Ltu;->a([[Ljava/lang/String;Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;)V

    move v6, v0

    :goto_0
    array-length v7, v3

    if-ge v6, v7, :cond_2

    aget-object v7, v3, v6

    invoke-virtual {v7}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerSetKey;->getAnswerOptionKeys()Ljava/util/ArrayList;

    move-result-object v7

    aget-object v8, v4, v0

    add-int/lit8 v9, v6, 0x1

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getExamSetString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v11, " "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getExamSetLabels()[Ljava/lang/String;

    move-result-object v11

    aget-object v6, v11, v6

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v8, v9

    move v6, v0

    :goto_1
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-ge v6, v8, :cond_1

    invoke-virtual {v7, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;

    iget-object v10, p0, Ltu;->b:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    invoke-virtual {v10}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getLabelProfile()Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    move-result-object v10

    invoke-static {v8, v10}, Le5;->q(Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;)Ljava/lang/String;

    move-result-object v8

    add-int/lit8 v6, v6, 0x1

    aget-object v10, v4, v6

    aput-object v8, v10, v9

    goto :goto_1

    :cond_1
    move v6, v9

    goto :goto_0

    :cond_2
    array-length v1, v3

    if-ne v1, v5, :cond_3

    aget-object v1, v4, v0

    const-string v3, "KEY"

    aput-object v3, v1, v5

    :cond_3
    move v1, v0

    :goto_2
    array-length v3, v4

    if-ge v1, v3, :cond_4

    aget-object v3, v4, v1

    invoke-virtual {p1, v3}, Lh;->a([Ljava/lang/String;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_4
    invoke-virtual {p1}, Lh;->flush()V

    invoke-virtual {p1}, Lh;->close()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    invoke-virtual {v2}, Ljava/io/OutputStream;->flush()V

    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_3

    :catch_0
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V

    :goto_3
    return v5

    :cond_5
    :goto_4
    :try_start_3
    invoke-virtual {v2}, Ljava/io/OutputStream;->flush()V

    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_5

    :catch_1
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V

    :goto_5
    return v0

    :catchall_0
    move-exception p1

    move-object v1, v2

    goto :goto_8

    :catch_2
    move-exception p1

    move-object v1, v2

    goto :goto_6

    :catchall_1
    move-exception p1

    goto :goto_8

    :catch_3
    move-exception p1

    :goto_6
    :try_start_4
    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    if-eqz v1, :cond_6

    :try_start_5
    invoke-virtual {v1}, Ljava/io/OutputStream;->flush()V

    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_4

    goto :goto_7

    :catch_4
    move-exception p1

    invoke-virtual {p1}, Ljava/lang/Throwable;->printStackTrace()V

    :cond_6
    :goto_7
    return v0

    :goto_8
    if-eqz v1, :cond_7

    :try_start_6
    invoke-virtual {v1}, Ljava/io/OutputStream;->flush()V

    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_5

    goto :goto_9

    :catch_5
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    :cond_7
    :goto_9
    throw p1
.end method

.method public varargs c([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 0

    .line 1
    iget-object p1, p0, Ltu;->e:Ljava/lang/String;

    invoke-virtual {p0, p1}, Ltu;->b(Ljava/lang/String;)Z

    move-result p1

    iput-boolean p1, p0, Ltu;->c:Z

    const/4 p1, 0x0

    return-object p1
.end method

.method public d(Ljava/lang/Void;)V
    .locals 3

    .line 1
    iget-object p1, p0, Ltu;->g:Landroid/app/ProgressDialog;

    invoke-virtual {p1}, Landroid/app/Dialog;->isShowing()Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Ltu;->g:Landroid/app/ProgressDialog;

    invoke-virtual {p1}, Landroid/app/Dialog;->dismiss()V

    :cond_0
    iget-boolean p1, p0, Ltu;->c:Z

    if-eqz p1, :cond_1

    new-instance p1, Lgn1;

    iget-object v0, p0, Ltu;->a:Landroid/content/Context;

    iget-object v1, p0, Ltu;->e:Ljava/lang/String;

    iget-object v2, p0, Ltu;->d:Ljava/lang/String;

    invoke-direct {p1, v0, v1, v2}, Lgn1;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method public bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 1
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Ltu;->c([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object p1

    return-object p1
.end method

.method public varargs e([Ljava/lang/Integer;)V
    .locals 0

    .line 1
    return-void
.end method

.method public bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .line 1
    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Ltu;->d(Ljava/lang/Void;)V

    return-void
.end method

.method public onPreExecute()V
    .locals 2

    .line 1
    new-instance v0, Landroid/app/ProgressDialog;

    iget-object v1, p0, Ltu;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Ltu;->g:Landroid/app/ProgressDialog;

    const-string v1, "Creating csv, please wait..."

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Ltu;->g:Landroid/app/ProgressDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setCancelable(Z)V

    iget-object v0, p0, Ltu;->g:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    return-void
.end method

.method public bridge synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0

    .line 1
    check-cast p1, [Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Ltu;->e([Ljava/lang/Integer;)V

    return-void
.end method

.class public Lva0$c;
.super Lhr1;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lva0;->c()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic w:Lva0;


# direct methods
.method public constructor <init>(Lva0;ILjava/lang/String;Lcom/android/volley/d$b;Lcom/android/volley/d$a;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lva0$c;->w:Lva0;

    invoke-direct {p0, p2, p3, p4, p5}, Lhr1;-><init>(ILjava/lang/String;Lcom/android/volley/d$b;Lcom/android/volley/d$a;)V

    return-void
.end method


# virtual methods
.method public k()[B
    .locals 7

    .line 1
    const-string v0, "emailKey"

    :try_start_0
    invoke-static {}, Lcom/google/firebase/auth/FirebaseAuth;->getInstance()Lcom/google/firebase/auth/FirebaseAuth;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/firebase/auth/FirebaseAuth;->e()Lr30;

    move-result-object v1

    invoke-virtual {v1}, Lr30;->getEmail()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Lb;->i(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lva0$c;->w:Lva0;

    invoke-static {v2}, Lva0;->b(Lva0;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;->getRole()Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserRole;

    move-result-object v3

    sget-object v4, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserRole;->OWNER:Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserRole;

    if-eq v3, v4, :cond_1

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/OrgProfile;->getTeacherList()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/ekodroid/omrevaluator/more/models/Teacher;

    invoke-virtual {v3}, Lcom/ekodroid/omrevaluator/more/models/Teacher;->getUserRole()Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserRole;

    move-result-object v4

    sget-object v5, Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserRole;->OWNER:Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/UserRole;

    if-ne v4, v5, :cond_0

    invoke-virtual {v3}, Lcom/ekodroid/omrevaluator/more/models/Teacher;->getEmailId()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Lb;->i(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_1
    move-object v2, v1

    iget-object v0, p0, Lva0$c;->w:Lva0;

    iget-object v0, v0, Lva0;->e:Landroid/content/SharedPreferences;

    const-string v1, "user_country"

    const-string v3, ""

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v5

    new-instance v0, Lcom/ekodroid/omrevaluator/serializable/SmsAccount;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v6, 0x0

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lcom/ekodroid/omrevaluator/serializable/SmsAccount;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;)V

    new-instance v1, Lgc0;

    invoke-direct {v1}, Lgc0;-><init>()V

    invoke-virtual {v1, v0}, Lgc0;->s(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "UTF-8"

    invoke-virtual {v0, v1}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v0
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    goto :goto_1

    :catch_1
    move-exception v0

    :goto_1
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    const/4 v0, 0x0

    return-object v0
.end method

.method public l()Ljava/lang/String;
    .locals 1

    .line 1
    const-string v0, "application/json"

    return-object v0
.end method

.class public abstract Lu62$f;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lu62;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "f"
.end annotation


# instance fields
.field public final a:Lu62;

.field public b:[Lnf0;


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 1
    new-instance v0, Lu62;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lu62;-><init>(Lu62;)V

    invoke-direct {p0, v0}, Lu62$f;-><init>(Lu62;)V

    return-void
.end method

.method public constructor <init>(Lu62;)V
    .locals 0

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lu62$f;->a:Lu62;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 5

    .line 1
    iget-object v0, p0, Lu62$f;->b:[Lnf0;

    if-eqz v0, :cond_4

    const/4 v1, 0x1

    invoke-static {v1}, Lu62$m;->b(I)I

    move-result v2

    aget-object v0, v0, v2

    iget-object v2, p0, Lu62$f;->b:[Lnf0;

    const/4 v3, 0x2

    invoke-static {v3}, Lu62$m;->b(I)I

    move-result v4

    aget-object v2, v2, v4

    if-nez v2, :cond_0

    iget-object v2, p0, Lu62$f;->a:Lu62;

    invoke-virtual {v2, v3}, Lu62;->f(I)Lnf0;

    move-result-object v2

    :cond_0
    if-nez v0, :cond_1

    iget-object v0, p0, Lu62$f;->a:Lu62;

    invoke-virtual {v0, v1}, Lu62;->f(I)Lnf0;

    move-result-object v0

    :cond_1
    invoke-static {v0, v2}, Lnf0;->a(Lnf0;Lnf0;)Lnf0;

    move-result-object v0

    invoke-virtual {p0, v0}, Lu62$f;->g(Lnf0;)V

    iget-object v0, p0, Lu62$f;->b:[Lnf0;

    const/16 v1, 0x10

    invoke-static {v1}, Lu62$m;->b(I)I

    move-result v1

    aget-object v0, v0, v1

    if-eqz v0, :cond_2

    invoke-virtual {p0, v0}, Lu62$f;->f(Lnf0;)V

    :cond_2
    iget-object v0, p0, Lu62$f;->b:[Lnf0;

    const/16 v1, 0x20

    invoke-static {v1}, Lu62$m;->b(I)I

    move-result v1

    aget-object v0, v0, v1

    if-eqz v0, :cond_3

    invoke-virtual {p0, v0}, Lu62$f;->d(Lnf0;)V

    :cond_3
    iget-object v0, p0, Lu62$f;->b:[Lnf0;

    const/16 v1, 0x40

    invoke-static {v1}, Lu62$m;->b(I)I

    move-result v1

    aget-object v0, v0, v1

    if-eqz v0, :cond_4

    invoke-virtual {p0, v0}, Lu62$f;->h(Lnf0;)V

    :cond_4
    return-void
.end method

.method public abstract b()Lu62;
.end method

.method public c(ILnf0;)V
    .locals 3

    .line 1
    iget-object v0, p0, Lu62$f;->b:[Lnf0;

    if-nez v0, :cond_0

    const/16 v0, 0x9

    new-array v0, v0, [Lnf0;

    iput-object v0, p0, Lu62$f;->b:[Lnf0;

    :cond_0
    const/4 v0, 0x1

    :goto_0
    const/16 v1, 0x100

    if-gt v0, v1, :cond_2

    and-int v1, p1, v0

    if-nez v1, :cond_1

    goto :goto_1

    :cond_1
    iget-object v1, p0, Lu62$f;->b:[Lnf0;

    invoke-static {v0}, Lu62$m;->b(I)I

    move-result v2

    aput-object p2, v1, v2

    :goto_1
    shl-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    return-void
.end method

.method public d(Lnf0;)V
    .locals 0

    .line 1
    return-void
.end method

.method public abstract e(Lnf0;)V
.end method

.method public f(Lnf0;)V
    .locals 0

    .line 1
    return-void
.end method

.method public abstract g(Lnf0;)V
.end method

.method public h(Lnf0;)V
    .locals 0

    .line 1
    return-void
.end method

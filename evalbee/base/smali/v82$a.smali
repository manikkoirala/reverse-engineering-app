.class public Lv82$a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lv82;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Lum1;

.field public final synthetic b:Lv82;


# direct methods
.method public constructor <init>(Lv82;Lum1;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lv82$a;->b:Lv82;

    iput-object p2, p0, Lv82$a;->a:Lum1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .line 1
    iget-object v0, p0, Lv82$a;->b:Lv82;

    iget-object v0, v0, Lv82;->a:Lum1;

    invoke-virtual {v0}, Landroidx/work/impl/utils/futures/AbstractFuture;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    :try_start_0
    iget-object v0, p0, Lv82$a;->a:Lum1;

    invoke-virtual {v0}, Landroidx/work/impl/utils/futures/AbstractFuture;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lp70;

    if-eqz v0, :cond_1

    invoke-static {}, Lxl0;->e()Lxl0;

    move-result-object v1

    sget-object v2, Lv82;->g:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Updating notification for "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, p0, Lv82$a;->b:Lv82;

    iget-object v4, v4, Lv82;->c:Lp92;

    iget-object v4, v4, Lp92;->c:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lxl0;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lv82$a;->b:Lv82;

    iget-object v2, v1, Lv82;->a:Lum1;

    iget-object v3, v1, Lv82;->e:Lr70;

    iget-object v4, v1, Lv82;->b:Landroid/content/Context;

    iget-object v1, v1, Lv82;->d:Landroidx/work/c;

    invoke-virtual {v1}, Landroidx/work/c;->getId()Ljava/util/UUID;

    move-result-object v1

    invoke-interface {v3, v4, v1, v0}, Lr70;->a(Landroid/content/Context;Ljava/util/UUID;Lp70;)Lik0;

    move-result-object v0

    invoke-virtual {v2, v0}, Lum1;->q(Lik0;)Z

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Worker was marked important ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lv82$a;->b:Lv82;

    iget-object v1, v1, Lv82;->c:Lp92;

    iget-object v1, v1, Lp92;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ") but did not provide ForegroundInfo"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lv82$a;->b:Lv82;

    iget-object v1, v1, Lv82;->a:Lum1;

    invoke-virtual {v1, v0}, Lum1;->p(Ljava/lang/Throwable;)Z

    :goto_0
    return-void
.end method

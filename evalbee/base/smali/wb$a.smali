.class public final Lwb$a;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lwb;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# instance fields
.field public volatile a:Lce2;

.field public final b:Landroid/content/Context;

.field public volatile c:Lca1;

.field public volatile d:Z


# direct methods
.method public synthetic constructor <init>(Landroid/content/Context;Lag2;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lwb$a;->b:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method public a()Lwb;
    .locals 17

    .line 1
    move-object/from16 v0, p0

    iget-object v1, v0, Lwb$a;->b:Landroid/content/Context;

    if-eqz v1, :cond_4

    iget-object v1, v0, Lwb$a;->c:Lca1;

    if-nez v1, :cond_1

    iget-boolean v1, v0, Lwb$a;->d:Z

    if-eqz v1, :cond_0

    new-instance v1, Lxb;

    iget-object v2, v0, Lwb$a;->b:Landroid/content/Context;

    const/4 v3, 0x0

    invoke-direct {v1, v3, v2, v3, v3}, Lxb;-><init>(Ljava/lang/String;Landroid/content/Context;Ldd2;Ljava/util/concurrent/ExecutorService;)V

    return-object v1

    :cond_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Please provide a valid listener for purchases updates."

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    iget-object v1, v0, Lwb$a;->a:Lce2;

    if-eqz v1, :cond_3

    iget-object v1, v0, Lwb$a;->c:Lca1;

    if-eqz v1, :cond_2

    new-instance v1, Lxb;

    const/4 v3, 0x0

    iget-object v4, v0, Lwb$a;->a:Lce2;

    iget-object v5, v0, Lwb$a;->b:Landroid/content/Context;

    iget-object v6, v0, Lwb$a;->c:Lca1;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object v2, v1

    invoke-direct/range {v2 .. v9}, Lxb;-><init>(Ljava/lang/String;Lce2;Landroid/content/Context;Lca1;Ld4;Ldd2;Ljava/util/concurrent/ExecutorService;)V

    return-object v1

    :cond_2
    new-instance v1, Lxb;

    const/4 v11, 0x0

    iget-object v12, v0, Lwb$a;->a:Lce2;

    iget-object v13, v0, Lwb$a;->b:Landroid/content/Context;

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    move-object v10, v1

    invoke-direct/range {v10 .. v16}, Lxb;-><init>(Ljava/lang/String;Lce2;Landroid/content/Context;Lsd2;Ldd2;Ljava/util/concurrent/ExecutorService;)V

    return-object v1

    :cond_3
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Pending purchases for one-time products must be supported."

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_4
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Please provide a valid Context."

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public b()Lwb$a;
    .locals 2

    .line 1
    new-instance v0, Lyd2;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lyd2;-><init>(Lwd2;)V

    invoke-virtual {v0}, Lyd2;->a()Lyd2;

    invoke-virtual {v0}, Lyd2;->b()Lce2;

    move-result-object v0

    iput-object v0, p0, Lwb$a;->a:Lce2;

    return-object p0
.end method

.method public c(Lca1;)Lwb$a;
    .locals 0

    .line 1
    iput-object p1, p0, Lwb$a;->c:Lca1;

    return-object p0
.end method

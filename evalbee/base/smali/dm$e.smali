.class public Ldm$e;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Ldm;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Lcom/google/android/material/textfield/TextInputLayout;

.field public final synthetic b:Ldm;


# direct methods
.method public constructor <init>(Ldm;Lcom/google/android/material/textfield/TextInputLayout;)V
    .locals 0

    .line 1
    iput-object p1, p0, Ldm$e;->b:Ldm;

    iput-object p2, p0, Ldm$e;->a:Lcom/google/android/material/textfield/TextInputLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .line 1
    iget-object p1, p0, Ldm$e;->b:Ldm;

    invoke-static {p1}, Ldm;->b(Ldm;)Landroid/widget/AutoCompleteTextView;

    move-result-object p1

    invoke-virtual {p1}, Landroid/widget/TextView;->getEditableText()Landroid/text/Editable;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    iget-object v0, p0, Ldm$e;->a:Lcom/google/android/material/textfield/TextInputLayout;

    invoke-virtual {v0}, Lcom/google/android/material/textfield/TextInputLayout;->getEditText()Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Ldm$e;->b:Ldm;

    iget-object v1, v1, Ldm;->d:Lcom/google/android/material/textfield/TextInputLayout;

    invoke-virtual {v1}, Lcom/google/android/material/textfield/TextInputLayout;->getEditText()Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    const v4, 0x7f08016e

    const v5, 0x7f0800bd

    if-eqz v2, :cond_0

    iget-object p1, p0, Ldm$e;->b:Ldm;

    iget-object p1, p1, Ldm;->a:Landroid/content/Context;

    const v0, 0x7f1200b6

    invoke-static {p1, v0, v5, v4}, La91;->G(Landroid/content/Context;III)V

    return-void

    :cond_0
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object p1, p0, Ldm$e;->b:Ldm;

    iget-object p1, p1, Ldm;->a:Landroid/content/Context;

    const v0, 0x7f1200b8

    invoke-static {p1, v0, v5, v4}, La91;->G(Landroid/content/Context;III)V

    return-void

    :cond_1
    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object p1, p0, Ldm$e;->b:Ldm;

    iget-object p1, p1, Ldm;->a:Landroid/content/Context;

    const v0, 0x7f1200b7

    invoke-static {p1, v0, v5, v4}, La91;->G(Landroid/content/Context;III)V

    return-void

    :cond_2
    new-instance v2, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    invoke-direct {v2, v0, p1, v1}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Ldm$e;->b:Ldm;

    invoke-static {v0, v2}, Ldm;->d(Ldm;Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object p1, p0, Ldm$e;->b:Ldm;

    invoke-static {p1, v2}, Ldm;->e(Ldm;Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Ldm$e;->b:Ldm;

    invoke-static {v0}, Ldm;->f(Ldm;)Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    move-result-object v1

    invoke-static {v0, v2, v1}, Ldm;->g(Ldm;Lcom/ekodroid/omrevaluator/templateui/models/ExamId;Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Ldm$e;->b:Ldm;

    invoke-static {v0}, Ldm;->h(Ldm;)Ly01;

    move-result-object v0

    invoke-interface {v0, p1}, Ly01;->a(Ljava/lang/Object;)V

    :goto_0
    iget-object p1, p0, Ldm$e;->b:Ldm;

    invoke-virtual {p1}, Landroid/app/Dialog;->dismiss()V

    :cond_4
    return-void
.end method

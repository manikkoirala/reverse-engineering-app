.class public Lwg$j;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/widget/AbsListView$MultiChoiceModeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lwg;->r()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Lwg;


# direct methods
.method public constructor <init>(Lwg;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lwg$j;->a:Lwg;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onActionItemClicked(Landroid/view/ActionMode;Landroid/view/MenuItem;)Z
    .locals 11

    .line 1
    iget-object v0, p0, Lwg$j;->a:Lwg;

    iget-object v0, v0, Lwg;->g:Lg3;

    invoke-virtual {v0}, Lg3;->a()Landroid/util/SparseBooleanArray;

    move-result-object v0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result p2

    const v2, 0x7f0901c4

    if-ne p2, v2, :cond_2

    invoke-virtual {v0}, Landroid/util/SparseBooleanArray;->size()I

    move-result p2

    const/4 v2, 0x1

    sub-int/2addr p2, v2

    :goto_0
    if-ltz p2, :cond_1

    invoke-virtual {v0, p2}, Landroid/util/SparseBooleanArray;->valueAt(I)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lwg$j;->a:Lwg;

    iget-object v3, v3, Lwg;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Landroid/util/SparseBooleanArray;->keyAt(I)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/ekodroid/omrevaluator/database/ClassDataModel;

    invoke-virtual {v3}, Lcom/ekodroid/omrevaluator/database/ClassDataModel;->getClassName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 p2, p2, -0x1

    goto :goto_0

    :cond_1
    new-instance v4, Lwg$j$a;

    invoke-direct {v4, p0, v1, p1}, Lwg$j$a;-><init>(Lwg$j;Ljava/util/ArrayList;Landroid/view/ActionMode;)V

    iget-object p1, p0, Lwg$j;->a:Lwg;

    iget-object v3, p1, Lwg;->a:Landroid/content/Context;

    const v5, 0x7f12009b

    const v6, 0x7f120190

    const v7, 0x7f120097

    const v8, 0x7f120053

    const/4 v9, 0x0

    const v10, 0x7f0800e1

    invoke-static/range {v3 .. v10}, Lxs;->c(Landroid/content/Context;Ly01;IIIIII)V

    return v2

    :cond_2
    const/4 p1, 0x0

    return p1
.end method

.method public onCreateActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z
    .locals 2

    .line 1
    iget-object v0, p0, Lwg$j;->a:Lwg;

    invoke-virtual {v0}, Landroidx/fragment/app/Fragment;->getActivity()Landroidx/fragment/app/e;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f0e0001

    invoke-virtual {v0, v1, p2}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    iget-object p2, p0, Lwg$j;->a:Lwg;

    iput-object p1, p2, Lwg;->j:Landroid/view/ActionMode;

    const/4 p1, 0x1

    return p1
.end method

.method public onDestroyActionMode(Landroid/view/ActionMode;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lwg$j;->a:Lwg;

    iget-object v0, v0, Lwg;->g:Lg3;

    invoke-virtual {v0}, Lg3;->b()V

    invoke-virtual {p1}, Landroid/view/ActionMode;->finish()V

    iget-object p1, p0, Lwg$j;->a:Lwg;

    const/4 v0, 0x0

    iput-object v0, p1, Lwg;->j:Landroid/view/ActionMode;

    return-void
.end method

.method public onItemCheckedStateChanged(Landroid/view/ActionMode;IJZ)V
    .locals 0

    .line 1
    iget-object p3, p0, Lwg$j;->a:Lwg;

    iget-object p3, p3, Lwg;->e:Landroid/widget/ListView;

    invoke-virtual {p3}, Landroid/widget/AbsListView;->getCheckedItemCount()I

    move-result p3

    new-instance p4, Ljava/lang/StringBuilder;

    invoke-direct {p4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p4, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p3, " "

    invoke-virtual {p4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p3, p0, Lwg$j;->a:Lwg;

    const p5, 0x7f1202c7

    invoke-virtual {p3, p5}, Landroidx/fragment/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p1, p3}, Landroid/view/ActionMode;->setTitle(Ljava/lang/CharSequence;)V

    iget-object p1, p0, Lwg$j;->a:Lwg;

    iget-object p1, p1, Lwg;->g:Lg3;

    invoke-virtual {p1, p2}, Lg3;->d(I)V

    iget-object p1, p0, Lwg$j;->a:Lwg;

    iget-object p1, p1, Lwg;->g:Lg3;

    invoke-virtual {p1}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    return-void
.end method

.method public onPrepareActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z
    .locals 0

    .line 1
    const/4 p1, 0x0

    return p1
.end method

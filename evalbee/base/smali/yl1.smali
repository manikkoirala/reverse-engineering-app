.class public final Lyl1;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Lyl1;

.field public static final b:Lhp;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lyl1;

    invoke-direct {v0}, Lyl1;-><init>()V

    sput-object v0, Lyl1;->a:Lyl1;

    new-instance v0, Lmh0;

    invoke-direct {v0}, Lmh0;-><init>()V

    sget-object v1, Lga;->a:Ljk;

    invoke-virtual {v0, v1}, Lmh0;->j(Ljk;)Lmh0;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lmh0;->k(Z)Lmh0;

    move-result-object v0

    invoke-virtual {v0}, Lmh0;->i()Lhp;

    move-result-object v0

    const-string v1, "JsonDataEncoderBuilder()\u2026lues(true)\n      .build()"

    invoke-static {v0, v1}, Lfg0;->d(Ljava/lang/Object;Ljava/lang/String;)V

    sput-object v0, Lyl1;->b:Lhp;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lr10;Lwl1;Lcom/google/firebase/sessions/settings/SessionsSettings;Lf81;Ljava/util/List;Ljava/util/Map;Ljava/lang/String;)Lxl1;
    .locals 16

    .line 1
    move-object/from16 v0, p0

    move-object/from16 v1, p6

    const-string v2, "firebaseApp"

    move-object/from16 v3, p1

    invoke-static {v3, v2}, Lfg0;->e(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "sessionDetails"

    move-object/from16 v4, p2

    invoke-static {v4, v2}, Lfg0;->e(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "sessionsSettings"

    move-object/from16 v5, p3

    invoke-static {v5, v2}, Lfg0;->e(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "currentProcessDetails"

    move-object/from16 v6, p4

    invoke-static {v6, v2}, Lfg0;->e(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "appProcessDetails"

    move-object/from16 v6, p5

    invoke-static {v6, v2}, Lfg0;->e(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "subscribers"

    invoke-static {v1, v2}, Lfg0;->e(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v2, "firebaseInstallationId"

    move-object/from16 v11, p7

    invoke-static {v11, v2}, Lfg0;->e(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v2, Lxl1;

    sget-object v12, Lcom/google/firebase/sessions/EventType;->SESSION_START:Lcom/google/firebase/sessions/EventType;

    new-instance v13, Lam1;

    invoke-virtual/range {p2 .. p2}, Lwl1;->b()Ljava/lang/String;

    move-result-object v6

    invoke-virtual/range {p2 .. p2}, Lwl1;->a()Ljava/lang/String;

    move-result-object v7

    invoke-virtual/range {p2 .. p2}, Lwl1;->c()I

    move-result v8

    invoke-virtual/range {p2 .. p2}, Lwl1;->d()J

    move-result-wide v9

    new-instance v14, Lgp;

    sget-object v4, Lcom/google/firebase/sessions/api/SessionSubscriber$Name;->PERFORMANCE:Lcom/google/firebase/sessions/api/SessionSubscriber$Name;

    invoke-interface {v1, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/firebase/sessions/api/SessionSubscriber;

    invoke-virtual {v0, v4}, Lyl1;->d(Lcom/google/firebase/sessions/api/SessionSubscriber;)Lcom/google/firebase/sessions/DataCollectionState;

    move-result-object v4

    sget-object v15, Lcom/google/firebase/sessions/api/SessionSubscriber$Name;->CRASHLYTICS:Lcom/google/firebase/sessions/api/SessionSubscriber$Name;

    invoke-interface {v1, v15}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/firebase/sessions/api/SessionSubscriber;

    invoke-virtual {v0, v1}, Lyl1;->d(Lcom/google/firebase/sessions/api/SessionSubscriber;)Lcom/google/firebase/sessions/DataCollectionState;

    move-result-object v1

    move-object/from16 p4, v2

    invoke-virtual/range {p3 .. p3}, Lcom/google/firebase/sessions/settings/SessionsSettings;->b()D

    move-result-wide v2

    invoke-direct {v14, v4, v1, v2, v3}, Lgp;-><init>(Lcom/google/firebase/sessions/DataCollectionState;Lcom/google/firebase/sessions/DataCollectionState;D)V

    move-object v4, v13

    move-object v5, v6

    move-object v6, v7

    move v7, v8

    move-wide v8, v9

    move-object v10, v14

    invoke-direct/range {v4 .. v11}, Lam1;-><init>(Ljava/lang/String;Ljava/lang/String;IJLgp;Ljava/lang/String;)V

    invoke-virtual/range {p0 .. p1}, Lyl1;->b(Lr10;)Ly7;

    move-result-object v1

    move-object/from16 v2, p4

    invoke-direct {v2, v12, v13, v1}, Lxl1;-><init>(Lcom/google/firebase/sessions/EventType;Lam1;Ly7;)V

    return-object v2
.end method

.method public final b(Lr10;)Ly7;
    .locals 17

    .line 1
    const-string v0, "firebaseApp"

    move-object/from16 v1, p1

    invoke-static {v1, v0}, Lfg0;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual/range {p1 .. p1}, Lr10;->l()Landroid/content/Context;

    move-result-object v0

    const-string v2, "firebaseApp.applicationContext"

    invoke-static {v0, v2}, Lfg0;->d(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const/4 v3, 0x0

    invoke-virtual {v0, v4, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x1c

    if-lt v3, v5, :cond_0

    invoke-static {v0}, Lli2;->a(Landroid/content/pm/PackageInfo;)J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    :cond_0
    iget v3, v0, Landroid/content/pm/PackageInfo;->versionCode:I

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    :goto_0
    move-object v6, v3

    new-instance v14, Ly7;

    invoke-virtual/range {p1 .. p1}, Lr10;->p()Le30;

    move-result-object v3

    invoke-virtual {v3}, Le30;->c()Ljava/lang/String;

    move-result-object v10

    const-string v3, "firebaseApp.options.applicationId"

    invoke-static {v10, v3}, Lfg0;->d(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v11, Landroid/os/Build;->MODEL:Ljava/lang/String;

    const-string v3, "MODEL"

    invoke-static {v11, v3}, Lfg0;->d(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v12, "1.2.0"

    sget-object v13, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    const-string v3, "RELEASE"

    invoke-static {v13, v3}, Lfg0;->d(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v15, Lcom/google/firebase/sessions/LogEnvironment;->LOG_ENVIRONMENT_PROD:Lcom/google/firebase/sessions/LogEnvironment;

    new-instance v16, Lr4;

    const-string v3, "packageName"

    invoke-static {v4, v3}, Lfg0;->d(Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v0, v0, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    if-nez v0, :cond_1

    move-object v5, v6

    goto :goto_1

    :cond_1
    move-object v5, v0

    :goto_1
    sget-object v7, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    const-string v0, "MANUFACTURER"

    invoke-static {v7, v0}, Lfg0;->d(Ljava/lang/Object;Ljava/lang/String;)V

    sget-object v0, Lh81;->a:Lh81;

    invoke-virtual/range {p1 .. p1}, Lr10;->l()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, v2}, Lfg0;->d(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Lh81;->d(Landroid/content/Context;)Lf81;

    move-result-object v8

    invoke-virtual/range {p1 .. p1}, Lr10;->l()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v2}, Lfg0;->d(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lh81;->c(Landroid/content/Context;)Ljava/util/List;

    move-result-object v9

    move-object/from16 v3, v16

    invoke-direct/range {v3 .. v9}, Lr4;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lf81;Ljava/util/List;)V

    move-object v7, v14

    move-object v8, v10

    move-object v9, v11

    move-object v10, v12

    move-object v11, v13

    move-object v12, v15

    move-object/from16 v13, v16

    invoke-direct/range {v7 .. v13}, Ly7;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/firebase/sessions/LogEnvironment;Lr4;)V

    return-object v14
.end method

.method public final c()Lhp;
    .locals 1

    .line 1
    sget-object v0, Lyl1;->b:Lhp;

    return-object v0
.end method

.method public final d(Lcom/google/firebase/sessions/api/SessionSubscriber;)Lcom/google/firebase/sessions/DataCollectionState;
    .locals 0

    .line 1
    if-nez p1, :cond_0

    sget-object p1, Lcom/google/firebase/sessions/DataCollectionState;->COLLECTION_SDK_NOT_INSTALLED:Lcom/google/firebase/sessions/DataCollectionState;

    goto :goto_0

    :cond_0
    invoke-interface {p1}, Lcom/google/firebase/sessions/api/SessionSubscriber;->a()Z

    move-result p1

    if-eqz p1, :cond_1

    sget-object p1, Lcom/google/firebase/sessions/DataCollectionState;->COLLECTION_ENABLED:Lcom/google/firebase/sessions/DataCollectionState;

    goto :goto_0

    :cond_1
    sget-object p1, Lcom/google/firebase/sessions/DataCollectionState;->COLLECTION_DISABLED:Lcom/google/firebase/sessions/DataCollectionState;

    :goto_0
    return-object p1
.end method

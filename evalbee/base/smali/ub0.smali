.class public Lub0;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:[I

.field public c:I

.field public d:I


# direct methods
.method public constructor <init>(Ljava/lang/String;I)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lub0;->a:Ljava/lang/String;

    iput-object v0, p0, Lub0;->b:[I

    const/4 v0, 0x0

    iput v0, p0, Lub0;->c:I

    iput v0, p0, Lub0;->d:I

    if-eqz p1, :cond_0

    invoke-static {p1, p2}, Lub0;->d(Ljava/lang/String;I)[I

    move-result-object p2

    iput-object p2, p0, Lub0;->b:[I

    iput-object p1, p0, Lub0;->a:Ljava/lang/String;

    return-void

    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "control string cannot be null"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public static d(Ljava/lang/String;I)[I
    .locals 18

    .line 1
    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-virtual/range {p0 .. p0}, Ljava/lang/String;->length()I

    move-result v2

    const/4 v3, 0x1

    const/4 v4, 0x0

    move v5, v4

    move v6, v5

    :goto_0
    const/16 v7, 0x29

    const/16 v8, 0x28

    const/16 v9, 0x2c

    if-ge v5, v2, :cond_3

    invoke-virtual {v0, v5}, Ljava/lang/String;->charAt(I)C

    move-result v10

    if-ne v10, v9, :cond_0

    if-nez v6, :cond_0

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_0
    if-ne v10, v8, :cond_1

    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    :cond_1
    if-ne v10, v7, :cond_2

    add-int/lit8 v6, v6, -0x1

    :cond_2
    :goto_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    :cond_3
    if-nez v6, :cond_c

    mul-int/lit8 v3, v3, 0x2

    new-array v3, v3, [I

    new-instance v5, Lb32;

    invoke-direct {v5}, Lb32;-><init>()V

    new-instance v10, Ly80;

    invoke-direct {v10}, Ly80;-><init>()V

    invoke-virtual {v10}, Ly80;->b()V

    const/4 v11, -0x1

    move v12, v4

    move v13, v6

    move v14, v11

    move v6, v12

    :goto_2
    if-gt v4, v2, :cond_b

    if-ge v4, v2, :cond_4

    invoke-virtual {v0, v4}, Ljava/lang/String;->charAt(I)C

    move-result v15

    goto :goto_3

    :cond_4
    const/16 v15, 0x20

    :goto_3
    if-eq v4, v2, :cond_9

    if-ne v15, v9, :cond_5

    if-nez v13, :cond_5

    goto :goto_5

    :cond_5
    if-ne v15, v8, :cond_7

    add-int/lit8 v13, v13, 0x1

    :cond_6
    :goto_4
    move v7, v6

    goto :goto_7

    :cond_7
    if-ne v15, v7, :cond_8

    add-int/lit8 v13, v13, -0x1

    goto :goto_4

    :cond_8
    const/16 v7, 0x3a

    if-ne v15, v7, :cond_6

    move v14, v4

    goto :goto_4

    :cond_9
    :goto_5
    if-ne v14, v11, :cond_a

    invoke-static {v0, v5, v1, v6, v4}, Lub0;->f(Ljava/lang/String;Lb32;III)Loz;

    move-result-object v6

    invoke-virtual {v6, v5, v10}, Loz;->b(Lb32;Ly80;)D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Math;->round(D)J

    move-result-wide v6

    long-to-int v6, v6

    aput v6, v3, v12

    add-int/lit8 v7, v12, 0x1

    aput v6, v3, v7

    add-int/lit8 v12, v12, 0x2

    goto :goto_6

    :cond_a
    invoke-static {v0, v5, v1, v6, v14}, Lub0;->f(Ljava/lang/String;Lb32;III)Loz;

    move-result-object v6

    add-int/lit8 v7, v12, 0x1

    invoke-virtual {v6, v5, v10}, Loz;->b(Lb32;Ly80;)D

    move-result-wide v16

    invoke-static/range {v16 .. v17}, Ljava/lang/Math;->round(D)J

    move-result-wide v8

    long-to-int v8, v8

    aput v8, v3, v12

    add-int/lit8 v14, v14, 0x1

    invoke-static {v0, v5, v1, v14, v4}, Lub0;->f(Ljava/lang/String;Lb32;III)Loz;

    move-result-object v8

    add-int/lit8 v12, v7, 0x1

    invoke-virtual {v8, v5, v10}, Loz;->b(Lb32;Ly80;)D

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Math;->round(D)J

    move-result-wide v8

    long-to-int v8, v8

    aput v8, v3, v7

    :goto_6
    add-int/lit8 v7, v4, 0x1

    move v14, v11

    :goto_7
    add-int/lit8 v4, v4, 0x1

    move v6, v7

    const/16 v7, 0x29

    const/16 v8, 0x28

    const/16 v9, 0x2c

    goto :goto_2

    :cond_b
    return-object v3

    :cond_c
    new-instance v0, Lcom/graphbuilder/curve/ControlStringParseException;

    const-string v1, "round brackets do not balance"

    invoke-direct {v0, v1}, Lcom/graphbuilder/curve/ControlStringParseException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static f(Ljava/lang/String;Lb32;III)Loz;
    .locals 3

    .line 1
    :try_start_0
    invoke-virtual {p0, p3, p4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Lpz;->c(Ljava/lang/String;)Loz;

    move-result-object p0
    :try_end_0
    .catch Lcom/graphbuilder/math/ExpressionParseException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz p0, :cond_2

    invoke-virtual {p0}, Loz;->e()[Ljava/lang/String;

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x1

    if-gt v1, v2, :cond_1

    array-length p3, v0

    if-ne p3, v2, :cond_0

    const/4 p3, 0x0

    aget-object p3, v0, p3

    int-to-double v0, p2

    invoke-virtual {p1, p3, v0, v1}, Lb32;->b(Ljava/lang/String;D)V

    :cond_0
    return-object p0

    :cond_1
    new-instance p0, Lcom/graphbuilder/curve/ControlStringParseException;

    const-string p1, "too many variables"

    invoke-direct {p0, p1, p3, p4}, Lcom/graphbuilder/curve/ControlStringParseException;-><init>(Ljava/lang/String;II)V

    throw p0

    :cond_2
    new-instance p0, Lcom/graphbuilder/curve/ControlStringParseException;

    const-string p1, "control substring is empty"

    invoke-direct {p0, p1, p3, p4}, Lcom/graphbuilder/curve/ControlStringParseException;-><init>(Ljava/lang/String;II)V

    throw p0

    :catch_0
    move-exception p0

    new-instance p1, Lcom/graphbuilder/curve/ControlStringParseException;

    const-string p2, "error parsing expression"

    invoke-direct {p1, p2, p3, p4, p0}, Lcom/graphbuilder/curve/ControlStringParseException;-><init>(Ljava/lang/String;IILcom/graphbuilder/math/ExpressionParseException;)V

    throw p1
.end method


# virtual methods
.method public a()I
    .locals 5

    .line 1
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v2, p0, Lub0;->b:[I

    array-length v3, v2

    if-ge v0, v3, :cond_1

    aget v3, v2, v0

    add-int/lit8 v4, v0, 0x1

    aget v2, v2, v4

    sub-int/2addr v3, v2

    if-gez v3, :cond_0

    neg-int v3, v3

    :cond_0
    add-int/lit8 v3, v3, 0x1

    add-int/2addr v1, v3

    add-int/lit8 v0, v0, 0x2

    goto :goto_0

    :cond_1
    return v1
.end method

.method public b(II)Z
    .locals 4

    .line 1
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v2, p0, Lub0;->b:[I

    array-length v3, v2

    if-ge v1, v3, :cond_2

    aget v2, v2, v1

    if-lt v2, p1, :cond_1

    if-lt v2, p2, :cond_0

    goto :goto_1

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    :goto_1
    return v0

    :cond_2
    const/4 p1, 0x1

    return p1
.end method

.method public c()I
    .locals 5

    .line 1
    iget-object v0, p0, Lub0;->b:[I

    iget v1, p0, Lub0;->c:I

    aget v2, v0, v1

    add-int/lit8 v3, v1, 0x1

    aget v0, v0, v3

    const/4 v3, 0x0

    iget v4, p0, Lub0;->d:I

    if-gt v2, v0, :cond_1

    add-int/2addr v2, v4

    if-lt v2, v0, :cond_0

    :goto_0
    iput v3, p0, Lub0;->d:I

    add-int/lit8 v1, v1, 0x2

    iput v1, p0, Lub0;->c:I

    goto :goto_1

    :cond_0
    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lub0;->d:I

    goto :goto_1

    :cond_1
    sub-int/2addr v2, v4

    if-gt v2, v0, :cond_0

    goto :goto_0

    :goto_1
    return v2
.end method

.method public e(II)V
    .locals 2

    .line 1
    if-ltz p1, :cond_2

    rem-int/lit8 v0, p1, 0x2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    if-ltz p2, :cond_0

    iput p1, p0, Lub0;->c:I

    iput p2, p0, Lub0;->d:I

    return-void

    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "count_j >= 0 required"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "index_i must be an even number"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_2
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "index_i >= 0 required"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.class public Lxw1$a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/internal/BackgroundDetector$BackgroundStateChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lxw1;-><init>(Landroid/content/Context;Lgr;Lah;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Lgr;

.field public final synthetic b:Lah;

.field public final synthetic c:Lxw1;


# direct methods
.method public constructor <init>(Lxw1;Lgr;Lah;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lxw1$a;->c:Lxw1;

    iput-object p2, p0, Lxw1$a;->a:Lgr;

    iput-object p3, p0, Lxw1$a;->b:Lah;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onBackgroundStateChanged(Z)V
    .locals 4

    .line 1
    iget-object v0, p0, Lxw1$a;->c:Lxw1;

    invoke-static {v0, p1}, Lxw1;->a(Lxw1;Z)Z

    if-eqz p1, :cond_0

    iget-object p1, p0, Lxw1$a;->a:Lgr;

    invoke-virtual {p1}, Lgr;->c()V

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lxw1$a;->c:Lxw1;

    invoke-static {p1}, Lxw1;->b(Lxw1;)Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lxw1$a;->a:Lgr;

    iget-object v0, p0, Lxw1$a;->c:Lxw1;

    invoke-static {v0}, Lxw1;->c(Lxw1;)J

    move-result-wide v0

    iget-object v2, p0, Lxw1$a;->b:Lah;

    invoke-interface {v2}, Lah;->currentTimeMillis()J

    move-result-wide v2

    sub-long/2addr v0, v2

    invoke-virtual {p1, v0, v1}, Lgr;->g(J)V

    :cond_1
    :goto_0
    return-void
.end method

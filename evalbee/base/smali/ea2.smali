.class public final Lea2;
.super Lcom/google/protobuf/GeneratedMessageLite;
.source "SourceFile"

# interfaces
.implements Lxv0;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lea2$b;
    }
.end annotation


# static fields
.field public static final BASE_WRITES_FIELD_NUMBER:I = 0x4

.field public static final BATCH_ID_FIELD_NUMBER:I = 0x1

.field private static final DEFAULT_INSTANCE:Lea2;

.field public static final LOCAL_WRITE_TIME_FIELD_NUMBER:I = 0x3

.field private static volatile PARSER:Lb31; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lb31;"
        }
    .end annotation
.end field

.field public static final WRITES_FIELD_NUMBER:I = 0x2


# instance fields
.field private baseWrites_:Lcom/google/protobuf/t$e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/t$e;"
        }
    .end annotation
.end field

.field private batchId_:I

.field private localWriteTime_:Lcom/google/protobuf/j0;

.field private writes_:Lcom/google/protobuf/t$e;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/protobuf/t$e;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lea2;

    invoke-direct {v0}, Lea2;-><init>()V

    sput-object v0, Lea2;->DEFAULT_INSTANCE:Lea2;

    const-class v1, Lea2;

    invoke-static {v1, v0}, Lcom/google/protobuf/GeneratedMessageLite;->V(Ljava/lang/Class;Lcom/google/protobuf/GeneratedMessageLite;)V

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Lcom/google/protobuf/GeneratedMessageLite;-><init>()V

    invoke-static {}, Lcom/google/protobuf/GeneratedMessageLite;->A()Lcom/google/protobuf/t$e;

    move-result-object v0

    iput-object v0, p0, Lea2;->writes_:Lcom/google/protobuf/t$e;

    invoke-static {}, Lcom/google/protobuf/GeneratedMessageLite;->A()Lcom/google/protobuf/t$e;

    move-result-object v0

    iput-object v0, p0, Lea2;->baseWrites_:Lcom/google/protobuf/t$e;

    return-void
.end method

.method public static synthetic Z()Lea2;
    .locals 1

    .line 1
    sget-object v0, Lea2;->DEFAULT_INSTANCE:Lea2;

    return-object v0
.end method

.method public static synthetic a0(Lea2;I)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lea2;->r0(I)V

    return-void
.end method

.method public static synthetic b0(Lea2;Lcom/google/firestore/v1/Write;)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lea2;->e0(Lcom/google/firestore/v1/Write;)V

    return-void
.end method

.method public static synthetic c0(Lea2;Lcom/google/firestore/v1/Write;)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lea2;->f0(Lcom/google/firestore/v1/Write;)V

    return-void
.end method

.method public static synthetic d0(Lea2;Lcom/google/protobuf/j0;)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lea2;->s0(Lcom/google/protobuf/j0;)V

    return-void
.end method

.method public static o0()Lea2$b;
    .locals 1

    .line 1
    sget-object v0, Lea2;->DEFAULT_INSTANCE:Lea2;

    invoke-virtual {v0}, Lcom/google/protobuf/GeneratedMessageLite;->u()Lcom/google/protobuf/GeneratedMessageLite$a;

    move-result-object v0

    check-cast v0, Lea2$b;

    return-object v0
.end method

.method public static p0(Lcom/google/protobuf/ByteString;)Lea2;
    .locals 1

    .line 1
    sget-object v0, Lea2;->DEFAULT_INSTANCE:Lea2;

    invoke-static {v0, p0}, Lcom/google/protobuf/GeneratedMessageLite;->P(Lcom/google/protobuf/GeneratedMessageLite;Lcom/google/protobuf/ByteString;)Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object p0

    check-cast p0, Lea2;

    return-object p0
.end method

.method public static q0([B)Lea2;
    .locals 1

    .line 1
    sget-object v0, Lea2;->DEFAULT_INSTANCE:Lea2;

    invoke-static {v0, p0}, Lcom/google/protobuf/GeneratedMessageLite;->R(Lcom/google/protobuf/GeneratedMessageLite;[B)Lcom/google/protobuf/GeneratedMessageLite;

    move-result-object p0

    check-cast p0, Lea2;

    return-object p0
.end method


# virtual methods
.method public final e0(Lcom/google/firestore/v1/Write;)V
    .locals 1

    .line 1
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-virtual {p0}, Lea2;->g0()V

    iget-object v0, p0, Lea2;->baseWrites_:Lcom/google/protobuf/t$e;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public final f0(Lcom/google/firestore/v1/Write;)V
    .locals 1

    .line 1
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-virtual {p0}, Lea2;->h0()V

    iget-object v0, p0, Lea2;->writes_:Lcom/google/protobuf/t$e;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public final g0()V
    .locals 2

    .line 1
    iget-object v0, p0, Lea2;->baseWrites_:Lcom/google/protobuf/t$e;

    invoke-interface {v0}, Lcom/google/protobuf/t$e;->h()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {v0}, Lcom/google/protobuf/GeneratedMessageLite;->L(Lcom/google/protobuf/t$e;)Lcom/google/protobuf/t$e;

    move-result-object v0

    iput-object v0, p0, Lea2;->baseWrites_:Lcom/google/protobuf/t$e;

    :cond_0
    return-void
.end method

.method public final h0()V
    .locals 2

    .line 1
    iget-object v0, p0, Lea2;->writes_:Lcom/google/protobuf/t$e;

    invoke-interface {v0}, Lcom/google/protobuf/t$e;->h()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {v0}, Lcom/google/protobuf/GeneratedMessageLite;->L(Lcom/google/protobuf/t$e;)Lcom/google/protobuf/t$e;

    move-result-object v0

    iput-object v0, p0, Lea2;->writes_:Lcom/google/protobuf/t$e;

    :cond_0
    return-void
.end method

.method public i0(I)Lcom/google/firestore/v1/Write;
    .locals 1

    .line 1
    iget-object v0, p0, Lea2;->baseWrites_:Lcom/google/protobuf/t$e;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/google/firestore/v1/Write;

    return-object p1
.end method

.method public j0()I
    .locals 1

    .line 1
    iget-object v0, p0, Lea2;->baseWrites_:Lcom/google/protobuf/t$e;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public k0()I
    .locals 1

    .line 1
    iget v0, p0, Lea2;->batchId_:I

    return v0
.end method

.method public l0()Lcom/google/protobuf/j0;
    .locals 1

    .line 1
    iget-object v0, p0, Lea2;->localWriteTime_:Lcom/google/protobuf/j0;

    if-nez v0, :cond_0

    invoke-static {}, Lcom/google/protobuf/j0;->c0()Lcom/google/protobuf/j0;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method public m0(I)Lcom/google/firestore/v1/Write;
    .locals 1

    .line 1
    iget-object v0, p0, Lea2;->writes_:Lcom/google/protobuf/t$e;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/google/firestore/v1/Write;

    return-object p1
.end method

.method public n0()I
    .locals 1

    .line 1
    iget-object v0, p0, Lea2;->writes_:Lcom/google/protobuf/t$e;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final r0(I)V
    .locals 0

    .line 1
    iput p1, p0, Lea2;->batchId_:I

    return-void
.end method

.method public final s0(Lcom/google/protobuf/j0;)V
    .locals 0

    .line 1
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iput-object p1, p0, Lea2;->localWriteTime_:Lcom/google/protobuf/j0;

    return-void
.end method

.method public final y(Lcom/google/protobuf/GeneratedMessageLite$MethodToInvoke;Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 6

    .line 1
    sget-object p2, Lea2$a;->a:[I

    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result p1

    aget p1, p2, p1

    const/4 p2, 0x0

    packed-switch p1, :pswitch_data_0

    new-instance p1, Ljava/lang/UnsupportedOperationException;

    invoke-direct {p1}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw p1

    :pswitch_0
    return-object p2

    :pswitch_1
    const/4 p1, 0x1

    invoke-static {p1}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object p1

    return-object p1

    :pswitch_2
    sget-object p1, Lea2;->PARSER:Lb31;

    if-nez p1, :cond_1

    const-class p2, Lea2;

    monitor-enter p2

    :try_start_0
    sget-object p1, Lea2;->PARSER:Lb31;

    if-nez p1, :cond_0

    new-instance p1, Lcom/google/protobuf/GeneratedMessageLite$b;

    sget-object p3, Lea2;->DEFAULT_INSTANCE:Lea2;

    invoke-direct {p1, p3}, Lcom/google/protobuf/GeneratedMessageLite$b;-><init>(Lcom/google/protobuf/GeneratedMessageLite;)V

    sput-object p1, Lea2;->PARSER:Lb31;

    :cond_0
    monitor-exit p2

    goto :goto_0

    :catchall_0
    move-exception p1

    monitor-exit p2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1

    :cond_1
    :goto_0
    return-object p1

    :pswitch_3
    sget-object p1, Lea2;->DEFAULT_INSTANCE:Lea2;

    return-object p1

    :pswitch_4
    const-string v0, "batchId_"

    const-string v1, "writes_"

    const-class v2, Lcom/google/firestore/v1/Write;

    const-string v3, "localWriteTime_"

    const-string v4, "baseWrites_"

    const-class v5, Lcom/google/firestore/v1/Write;

    filled-new-array/range {v0 .. v5}, [Ljava/lang/Object;

    move-result-object p1

    const-string p2, "\u0000\u0004\u0000\u0000\u0001\u0004\u0004\u0000\u0002\u0000\u0001\u0004\u0002\u001b\u0003\t\u0004\u001b"

    sget-object p3, Lea2;->DEFAULT_INSTANCE:Lea2;

    invoke-static {p3, p2, p1}, Lcom/google/protobuf/GeneratedMessageLite;->N(Lcom/google/protobuf/a0;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    return-object p1

    :pswitch_5
    new-instance p1, Lea2$b;

    invoke-direct {p1, p2}, Lea2$b;-><init>(Lea2$a;)V

    return-object p1

    :pswitch_6
    new-instance p1, Lea2;

    invoke-direct {p1}, Lea2;-><init>()V

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

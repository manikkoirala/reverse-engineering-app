.class public Lwe0;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lwe0$a;
    }
.end annotation


# static fields
.field public static final f:J

.field public static final g:J


# instance fields
.field public final a:Lwe0$a;

.field public final b:Ld51;

.field public final c:Lis1;

.field public final d:Lis1;

.field public e:I


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .line 1
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v1, 0xf

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lwe0;->f:J

    sget-object v0, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v1, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lwe0;->g:J

    return-void
.end method

.method public constructor <init>(Ld51;Lcom/google/firebase/firestore/util/AsyncQueue;Lcom/google/firebase/firestore/local/a;)V
    .locals 2

    .line 2
    invoke-static {p3}, Ljava/util/Objects;->requireNonNull(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lse0;

    invoke-direct {v0, p3}, Lse0;-><init>(Lcom/google/firebase/firestore/local/a;)V

    new-instance v1, Lte0;

    invoke-direct {v1, p3}, Lte0;-><init>(Lcom/google/firebase/firestore/local/a;)V

    invoke-direct {p0, p1, p2, v0, v1}, Lwe0;-><init>(Ld51;Lcom/google/firebase/firestore/util/AsyncQueue;Lis1;Lis1;)V

    return-void
.end method

.method public constructor <init>(Ld51;Lcom/google/firebase/firestore/util/AsyncQueue;Lis1;Lis1;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0x32

    iput v0, p0, Lwe0;->e:I

    iput-object p1, p0, Lwe0;->b:Ld51;

    new-instance p1, Lwe0$a;

    invoke-direct {p1, p0, p2}, Lwe0$a;-><init>(Lwe0;Lcom/google/firebase/firestore/util/AsyncQueue;)V

    iput-object p1, p0, Lwe0;->a:Lwe0$a;

    iput-object p3, p0, Lwe0;->c:Lis1;

    iput-object p4, p0, Lwe0;->d:Lis1;

    return-void
.end method

.method public static synthetic a(Lwe0;)Ljava/lang/Integer;
    .locals 0

    .line 1
    invoke-direct {p0}, Lwe0;->g()Ljava/lang/Integer;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic b()J
    .locals 2

    .line 1
    sget-wide v0, Lwe0;->f:J

    return-wide v0
.end method

.method public static synthetic c()J
    .locals 2

    .line 1
    sget-wide v0, Lwe0;->g:J

    return-wide v0
.end method

.method private synthetic g()Ljava/lang/Integer;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lwe0;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public d()I
    .locals 3

    .line 1
    iget-object v0, p0, Lwe0;->b:Ld51;

    new-instance v1, Lue0;

    invoke-direct {v1, p0}, Lue0;-><init>(Lwe0;)V

    const-string v2, "Backfill Indexes"

    invoke-virtual {v0, v2, v1}, Ld51;->j(Ljava/lang/String;Lhs1;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public final e(Lcom/google/firebase/firestore/model/FieldIndex$a;Lwk0;)Lcom/google/firebase/firestore/model/FieldIndex$a;
    .locals 4

    .line 1
    invoke-virtual {p2}, Lwk0;->c()Lcom/google/firebase/database/collection/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/firebase/database/collection/b;->iterator()Ljava/util/Iterator;

    move-result-object v0

    move-object v1, p1

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lzt;

    invoke-static {v2}, Lcom/google/firebase/firestore/model/FieldIndex$a;->f(Lzt;)Lcom/google/firebase/firestore/model/FieldIndex$a;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/google/firebase/firestore/model/FieldIndex$a;->c(Lcom/google/firebase/firestore/model/FieldIndex$a;)I

    move-result v3

    if-lez v3, :cond_0

    move-object v1, v2

    goto :goto_0

    :cond_1
    invoke-virtual {v1}, Lcom/google/firebase/firestore/model/FieldIndex$a;->j()Lqo1;

    move-result-object v0

    invoke-virtual {v1}, Lcom/google/firebase/firestore/model/FieldIndex$a;->g()Ldu;

    move-result-object v1

    invoke-virtual {p2}, Lwk0;->b()I

    move-result p2

    invoke-virtual {p1}, Lcom/google/firebase/firestore/model/FieldIndex$a;->h()I

    move-result p1

    invoke-static {p2, p1}, Ljava/lang/Math;->max(II)I

    move-result p1

    invoke-static {v0, v1, p1}, Lcom/google/firebase/firestore/model/FieldIndex$a;->d(Lqo1;Ldu;I)Lcom/google/firebase/firestore/model/FieldIndex$a;

    move-result-object p1

    return-object p1
.end method

.method public f()Lwe0$a;
    .locals 1

    .line 1
    iget-object v0, p0, Lwe0;->a:Lwe0$a;

    return-object v0
.end method

.method public final h(Ljava/lang/String;I)I
    .locals 5

    .line 1
    iget-object v0, p0, Lwe0;->c:Lis1;

    invoke-interface {v0}, Lis1;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/firebase/firestore/local/IndexManager;

    iget-object v1, p0, Lwe0;->d:Lis1;

    invoke-interface {v1}, Lis1;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lxk0;

    invoke-interface {v0, p1}, Lcom/google/firebase/firestore/local/IndexManager;->e(Ljava/lang/String;)Lcom/google/firebase/firestore/model/FieldIndex$a;

    move-result-object v2

    invoke-virtual {v1, p1, v2, p2}, Lxk0;->k(Ljava/lang/String;Lcom/google/firebase/firestore/model/FieldIndex$a;I)Lwk0;

    move-result-object p2

    invoke-virtual {p2}, Lwk0;->c()Lcom/google/firebase/database/collection/b;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/firebase/firestore/local/IndexManager;->b(Lcom/google/firebase/database/collection/b;)V

    invoke-virtual {p0, v2, p2}, Lwe0;->e(Lcom/google/firebase/firestore/model/FieldIndex$a;Lwk0;)Lcom/google/firebase/firestore/model/FieldIndex$a;

    move-result-object v1

    const-string v2, "Updating offset: %s"

    filled-new-array {v1}, [Ljava/lang/Object;

    move-result-object v3

    const-string v4, "IndexBackfiller"

    invoke-static {v4, v2, v3}, Lcom/google/firebase/firestore/util/Logger;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-interface {v0, p1, v1}, Lcom/google/firebase/firestore/local/IndexManager;->f(Ljava/lang/String;Lcom/google/firebase/firestore/model/FieldIndex$a;)V

    invoke-virtual {p2}, Lwk0;->c()Lcom/google/firebase/database/collection/b;

    move-result-object p1

    invoke-virtual {p1}, Lcom/google/firebase/database/collection/b;->size()I

    move-result p1

    return p1
.end method

.method public final i()I
    .locals 7

    .line 1
    iget-object v0, p0, Lwe0;->c:Lis1;

    invoke-interface {v0}, Lis1;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/firebase/firestore/local/IndexManager;

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iget v2, p0, Lwe0;->e:I

    :goto_0
    if-lez v2, :cond_1

    invoke-interface {v0}, Lcom/google/firebase/firestore/local/IndexManager;->h()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-interface {v1, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    goto :goto_1

    :cond_0
    const-string v4, "Processing collection: %s"

    filled-new-array {v3}, [Ljava/lang/Object;

    move-result-object v5

    const-string v6, "IndexBackfiller"

    invoke-static {v6, v4, v5}, Lcom/google/firebase/firestore/util/Logger;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-virtual {p0, v3, v2}, Lwe0;->h(Ljava/lang/String;I)I

    move-result v4

    sub-int/2addr v2, v4

    invoke-interface {v1, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    :goto_1
    iget v0, p0, Lwe0;->e:I

    sub-int/2addr v0, v2

    return v0
.end method

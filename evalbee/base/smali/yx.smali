.class public abstract Lyx;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static a:I = 0x4


# direct methods
.method public static constructor <clinit>()V
    .locals 0

    .line 1
    return-void
.end method

.method public static a([B)Lcom/ekodroid/omrevaluator/serializable/ExmVersions/ExamDataV1;
    .locals 3

    .line 1
    :try_start_0
    invoke-static {p0}, Lyx;->c([B)Lcom/ekodroid/omrevaluator/serializable/ExmVersions/ExamFileModel;

    move-result-object p0

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/serializable/ExmVersions/ExamFileModel;->getJsonData()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/serializable/ExmVersions/ExamFileModel;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lb;->g(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lgc0;

    invoke-direct {v1}, Lgc0;-><init>()V

    const-class v2, Lcom/ekodroid/omrevaluator/serializable/ExmVersions/ExamDataV1;

    invoke-virtual {v1, v0, v2}, Lgc0;->j(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/ekodroid/omrevaluator/serializable/ExmVersions/ExamDataV1;

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/serializable/ExmVersions/ExamFileModel;->getVersion()I

    move-result v1

    sget v2, Lyx;->a:I

    if-ge v1, v2, :cond_0

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/serializable/ExmVersions/ExamFileModel;->getVersion()I

    move-result p0

    invoke-static {v0, p0}, Lyx;->f(Lcom/ekodroid/omrevaluator/serializable/ExmVersions/ExamDataV1;I)Lcom/ekodroid/omrevaluator/serializable/ExmVersions/ExamDataV1;

    move-result-object p0
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :cond_0
    return-object v0

    :catch_0
    move-exception p0

    invoke-virtual {p0}, Ljava/lang/Throwable;->printStackTrace()V

    const/4 p0, 0x0

    return-object p0
.end method

.method public static b(Lcom/ekodroid/omrevaluator/serializable/ExmVersions/ExamFileModel;)[B
    .locals 1

    .line 1
    new-instance v0, Lgc0;

    invoke-direct {v0}, Lgc0;-><init>()V

    invoke-virtual {v0, p0}, Lgc0;->s(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/String;->getBytes()[B

    move-result-object p0

    const-string v0, "examFileKeyPass"

    invoke-static {p0, v0}, Lb;->h([BLjava/lang/String;)[B

    move-result-object p0

    return-object p0
.end method

.method public static c([B)Lcom/ekodroid/omrevaluator/serializable/ExmVersions/ExamFileModel;
    .locals 2

    .line 1
    const-string v0, "examFileKeyPass"

    invoke-static {p0, v0}, Lb;->f([BLjava/lang/String;)[B

    move-result-object p0

    new-instance v0, Lgc0;

    invoke-direct {v0}, Lgc0;-><init>()V

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, p0}, Ljava/lang/String;-><init>([B)V

    const-class p0, Lcom/ekodroid/omrevaluator/serializable/ExmVersions/ExamFileModel;

    invoke-virtual {v0, v1, p0}, Lgc0;->j(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/ekodroid/omrevaluator/serializable/ExmVersions/ExamFileModel;

    return-object p0
.end method

.method public static d(Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;Ljava/util/ArrayList;Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)Lcom/ekodroid/omrevaluator/serializable/ExmVersions/ExamFileModel;
    .locals 4

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_0

    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;

    new-instance v2, Lcom/ekodroid/omrevaluator/serializable/ExmVersions/ResultDataV1;

    invoke-virtual {v1, p0}, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;->getResultItem(Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;

    move-result-object v3

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;->getRollNo()I

    move-result v1

    invoke-direct {v2, v3, v1}, Lcom/ekodroid/omrevaluator/serializable/ExmVersions/ResultDataV1;-><init>(Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;I)V

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    new-instance p1, Lcom/ekodroid/omrevaluator/serializable/ExmVersions/ExamDataV1;

    invoke-direct {p1, p0, v0, p2}, Lcom/ekodroid/omrevaluator/serializable/ExmVersions/ExamDataV1;-><init>(Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;Ljava/util/ArrayList;Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)V

    invoke-static {}, Lyx;->e()Ljava/lang/String;

    move-result-object p0

    new-instance p2, Lgc0;

    invoke-direct {p2}, Lgc0;-><init>()V

    invoke-virtual {p2, p1}, Lgc0;->s(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-static {p1, p0}, Lb;->i(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    new-instance p2, Lcom/ekodroid/omrevaluator/serializable/ExmVersions/ExamFileModel;

    sget v0, Lyx;->a:I

    invoke-direct {p2, v0, p0, p1}, Lcom/ekodroid/omrevaluator/serializable/ExmVersions/ExamFileModel;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    return-object p2
.end method

.method public static e()Ljava/lang/String;
    .locals 2

    .line 1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static f(Lcom/ekodroid/omrevaluator/serializable/ExmVersions/ExamDataV1;I)Lcom/ekodroid/omrevaluator/serializable/ExmVersions/ExamDataV1;
    .locals 33

    .line 1
    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-virtual/range {p0 .. p0}, Lcom/ekodroid/omrevaluator/serializable/ExmVersions/ExamDataV1;->getSheetTemplate2()Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    move-result-object v2

    const/4 v4, 0x1

    if-eq v1, v4, :cond_1

    const/4 v5, 0x2

    if-eq v1, v5, :cond_b

    const/4 v5, 0x3

    if-eq v1, v5, :cond_0

    goto/16 :goto_d

    :cond_0
    move v1, v4

    const/4 v6, 0x0

    goto/16 :goto_c

    :cond_1
    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getTemplateParams()Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;

    move-result-object v1

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;->getSubjects()[Lcom/ekodroid/omrevaluator/templateui/models/Subject2;

    move-result-object v1

    array-length v5, v1

    const/4 v6, 0x0

    :goto_0
    const-wide/16 v7, 0x0

    if-ge v6, v5, :cond_4

    aget-object v9, v1, v6

    invoke-virtual {v9}, Lcom/ekodroid/omrevaluator/templateui/models/Subject2;->getSections()[Lcom/ekodroid/omrevaluator/templateui/models/Section2;

    move-result-object v9

    array-length v10, v9

    const/4 v11, 0x0

    :goto_1
    if-ge v11, v10, :cond_3

    aget-object v12, v9, v11

    invoke-virtual {v12}, Lcom/ekodroid/omrevaluator/templateui/models/Section2;->getPartialMarks()D

    move-result-wide v13

    cmpl-double v13, v13, v7

    if-lez v13, :cond_2

    move v13, v4

    goto :goto_2

    :cond_2
    const/4 v13, 0x0

    :goto_2
    invoke-virtual {v12, v13}, Lcom/ekodroid/omrevaluator/templateui/models/Section2;->setPartialAllowed(Z)V

    add-int/lit8 v11, v11, 0x1

    goto :goto_1

    :cond_3
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    :cond_4
    move v1, v4

    :goto_3
    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getAnswerOptions()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ge v1, v5, :cond_6

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getAnswerOptions()Ljava/util/ArrayList;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;

    iget-wide v9, v5, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->partialMarks:D

    cmpl-double v6, v9, v7

    if-lez v6, :cond_5

    move v6, v4

    goto :goto_4

    :cond_5
    const/4 v6, 0x0

    :goto_4
    iput-boolean v6, v5, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->partialAllowed:Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    :cond_6
    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getAnswerKeys()[Lcom/ekodroid/omrevaluator/templateui/models/AnswerSetKey;

    move-result-object v1

    if-eqz v1, :cond_b

    array-length v5, v1

    if-lez v5, :cond_b

    array-length v5, v1

    const/4 v6, 0x0

    :goto_5
    if-ge v6, v5, :cond_b

    aget-object v9, v1, v6

    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v9}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerSetKey;->getAnswerValue2s()Ljava/util/ArrayList;

    move-result-object v11

    const/4 v12, 0x0

    :goto_6
    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v13

    if-ge v12, v13, :cond_a

    invoke-virtual {v11, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getAnswerOptions()Ljava/util/ArrayList;

    move-result-object v14

    add-int/lit8 v12, v12, 0x1

    invoke-virtual {v14, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;

    iget-wide v3, v14, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->partialMarks:D

    cmpl-double v3, v3, v7

    if-lez v3, :cond_9

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    const/4 v4, 0x0

    :goto_7
    invoke-virtual {v13}, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->getMarkedValues()[Z

    move-result-object v7

    array-length v7, v7

    if-ge v4, v7, :cond_8

    invoke-virtual {v13}, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->getMarkedValues()[Z

    move-result-object v7

    aget-boolean v7, v7, v4

    if-eqz v7, :cond_7

    invoke-virtual {v13}, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->getMarkedValues()[Z

    move-result-object v7

    array-length v7, v7

    new-array v7, v7, [Z

    const/4 v8, 0x1

    aput-boolean v8, v7, v4

    new-instance v8, Lcom/ekodroid/omrevaluator/templateui/models/PartialAnswerOptionKey;

    iget-object v15, v14, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->type:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    move-object/from16 v27, v1

    iget v1, v14, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->questionNumber:I

    move/from16 v28, v5

    iget v5, v14, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->subjectId:I

    move-object/from16 v29, v11

    iget v11, v14, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->sectionId:I

    move-object/from16 v30, v9

    move-object/from16 v31, v10

    iget-wide v9, v14, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->partialMarks:D

    move/from16 v32, v12

    iget-object v12, v14, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->payload:Ljava/lang/String;

    const/16 v25, 0x0

    const/16 v26, 0x0

    move-object/from16 v16, v8

    move-object/from16 v17, v15

    move/from16 v18, v1

    move-object/from16 v19, v7

    move/from16 v20, v5

    move/from16 v21, v11

    move-wide/from16 v22, v9

    move-object/from16 v24, v12

    invoke-direct/range {v16 .. v26}, Lcom/ekodroid/omrevaluator/templateui/models/PartialAnswerOptionKey;-><init>(Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;I[ZIIDLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_8

    :cond_7
    move-object/from16 v27, v1

    move/from16 v28, v5

    move-object/from16 v30, v9

    move-object/from16 v31, v10

    move-object/from16 v29, v11

    move/from16 v32, v12

    :goto_8
    add-int/lit8 v4, v4, 0x1

    move-object/from16 v1, v27

    move/from16 v5, v28

    move-object/from16 v11, v29

    move-object/from16 v9, v30

    move-object/from16 v10, v31

    move/from16 v12, v32

    goto :goto_7

    :cond_8
    move-object/from16 v27, v1

    move/from16 v28, v5

    move-object/from16 v30, v9

    move-object/from16 v31, v10

    move-object/from16 v29, v11

    move/from16 v32, v12

    goto :goto_9

    :cond_9
    move-object/from16 v27, v1

    move/from16 v28, v5

    move-object/from16 v30, v9

    move-object/from16 v31, v10

    move-object/from16 v29, v11

    move/from16 v32, v12

    const/4 v3, 0x0

    :goto_9
    move-object/from16 v23, v3

    new-instance v1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;

    invoke-virtual {v13}, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->getType()Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    move-result-object v17

    invoke-virtual {v13}, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->getQuestionNumber()I

    move-result v18

    invoke-virtual {v13}, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->getMarkedValues()[Z

    move-result-object v19

    const/16 v20, 0x0

    invoke-virtual {v13}, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->getSubjectId()I

    move-result v21

    invoke-virtual {v13}, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->getSectionId()I

    move-result v22

    invoke-virtual {v13}, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->getPayload()Ljava/lang/String;

    move-result-object v24

    move-object/from16 v16, v1

    invoke-direct/range {v16 .. v24}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;-><init>(Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;I[ZLjava/lang/String;IILjava/util/ArrayList;Ljava/lang/String;)V

    move-object/from16 v3, v31

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object v10, v3

    move-object/from16 v1, v27

    move/from16 v5, v28

    move-object/from16 v11, v29

    move-object/from16 v9, v30

    move/from16 v12, v32

    const/4 v4, 0x1

    const-wide/16 v7, 0x0

    goto/16 :goto_6

    :cond_a
    move-object/from16 v27, v1

    move/from16 v28, v5

    move-object v1, v9

    move-object v3, v10

    invoke-virtual {v1, v3}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerSetKey;->setAnswerOptionKeys(Ljava/util/ArrayList;)V

    add-int/lit8 v6, v6, 0x1

    move-object/from16 v1, v27

    const/4 v4, 0x1

    const-wide/16 v7, 0x0

    goto/16 :goto_5

    :cond_b
    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getTemplateParams()Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;

    move-result-object v1

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;->getExamSets()I

    move-result v1

    new-array v3, v1, [Lcom/ekodroid/omrevaluator/templateui/models/InvalidQuestionSet;

    const/4 v4, 0x1

    if-le v1, v4, :cond_d

    const/4 v4, 0x0

    :goto_a
    if-ge v4, v1, :cond_c

    new-instance v5, Lcom/ekodroid/omrevaluator/templateui/models/InvalidQuestionSet;

    add-int/lit8 v6, v4, 0x1

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getInvalidQuestions()[I

    move-result-object v7

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getInvalidQueMarks()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v5, v6, v7, v8}, Lcom/ekodroid/omrevaluator/templateui/models/InvalidQuestionSet;-><init>(I[ILjava/lang/String;)V

    aput-object v5, v3, v4

    move v4, v6

    goto :goto_a

    :cond_c
    const/4 v6, 0x0

    goto :goto_b

    :cond_d
    new-instance v1, Lcom/ekodroid/omrevaluator/templateui/models/InvalidQuestionSet;

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getInvalidQuestions()[I

    move-result-object v4

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getInvalidQueMarks()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-direct {v1, v6, v4, v5}, Lcom/ekodroid/omrevaluator/templateui/models/InvalidQuestionSet;-><init>(I[ILjava/lang/String;)V

    aput-object v1, v3, v6

    :goto_b
    invoke-virtual {v2, v3}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->setInvalidQuestionSets([Lcom/ekodroid/omrevaluator/templateui/models/InvalidQuestionSet;)V

    const/4 v1, 0x1

    :goto_c
    new-array v1, v1, [Lcom/ekodroid/omrevaluator/templateui/models/PageLayout;

    new-instance v3, Lcom/ekodroid/omrevaluator/templateui/models/PageLayout;

    iget v4, v2, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->rows:I

    iget v5, v2, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->columns:I

    iget-object v7, v2, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->columnWidthInBubbles:[I

    invoke-direct {v3, v4, v5, v7}, Lcom/ekodroid/omrevaluator/templateui/models/PageLayout;-><init>(II[I)V

    aput-object v3, v1, v6

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->setTemplateVersion(I)V

    invoke-virtual {v2, v1}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->setPageLayouts([Lcom/ekodroid/omrevaluator/templateui/models/PageLayout;)V

    :goto_d
    invoke-virtual {v0, v2}, Lcom/ekodroid/omrevaluator/serializable/ExmVersions/ExamDataV1;->setSheetTemplate2(Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)V

    return-object v0
.end method

.class public final Lu62$b;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lu62;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "b"
.end annotation


# instance fields
.field public final a:Lu62$f;


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x1e

    if-lt v0, v1, :cond_0

    new-instance v0, Lu62$e;

    invoke-direct {v0}, Lu62$e;-><init>()V

    :goto_0
    iput-object v0, p0, Lu62$b;->a:Lu62$f;

    goto :goto_1

    :cond_0
    const/16 v1, 0x1d

    if-lt v0, v1, :cond_1

    new-instance v0, Lu62$d;

    invoke-direct {v0}, Lu62$d;-><init>()V

    goto :goto_0

    :cond_1
    new-instance v0, Lu62$c;

    invoke-direct {v0}, Lu62$c;-><init>()V

    goto :goto_0

    :goto_1
    return-void
.end method

.method public constructor <init>(Lu62;)V
    .locals 2

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x1e

    if-lt v0, v1, :cond_0

    new-instance v0, Lu62$e;

    invoke-direct {v0, p1}, Lu62$e;-><init>(Lu62;)V

    :goto_0
    iput-object v0, p0, Lu62$b;->a:Lu62$f;

    goto :goto_1

    :cond_0
    const/16 v1, 0x1d

    if-lt v0, v1, :cond_1

    new-instance v0, Lu62$d;

    invoke-direct {v0, p1}, Lu62$d;-><init>(Lu62;)V

    goto :goto_0

    :cond_1
    new-instance v0, Lu62$c;

    invoke-direct {v0, p1}, Lu62$c;-><init>(Lu62;)V

    goto :goto_0

    :goto_1
    return-void
.end method


# virtual methods
.method public a()Lu62;
    .locals 1

    .line 1
    iget-object v0, p0, Lu62$b;->a:Lu62$f;

    invoke-virtual {v0}, Lu62$f;->b()Lu62;

    move-result-object v0

    return-object v0
.end method

.method public b(ILnf0;)Lu62$b;
    .locals 1

    .line 1
    iget-object v0, p0, Lu62$b;->a:Lu62$f;

    invoke-virtual {v0, p1, p2}, Lu62$f;->c(ILnf0;)V

    return-object p0
.end method

.method public c(Lnf0;)Lu62$b;
    .locals 1

    .line 1
    iget-object v0, p0, Lu62$b;->a:Lu62$f;

    invoke-virtual {v0, p1}, Lu62$f;->e(Lnf0;)V

    return-object p0
.end method

.method public d(Lnf0;)Lu62$b;
    .locals 1

    .line 1
    iget-object v0, p0, Lu62$b;->a:Lu62$f;

    invoke-virtual {v0, p1}, Lu62$f;->g(Lnf0;)V

    return-object p0
.end method

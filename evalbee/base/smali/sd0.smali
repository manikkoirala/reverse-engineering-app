.class public interface abstract Lsd0;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/IInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lsd0$b;,
        Lsd0$a;
    }
.end annotation


# static fields
.field public static final b0:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 1
    const/16 v0, 0x24

    const/16 v1, 0x2e

    const-string v2, "android$support$customtabs$ICustomTabsCallback"

    invoke-virtual {v2, v0, v1}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lsd0;->b0:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public abstract b(Ljava/lang/String;Landroid/os/Bundle;)Landroid/os/Bundle;
.end method

.method public abstract g(IILandroid/os/Bundle;)V
.end method

.method public abstract h(ILandroid/os/Bundle;)V
.end method

.method public abstract j(Ljava/lang/String;Landroid/os/Bundle;)V
.end method

.method public abstract m(Ljava/lang/String;Landroid/os/Bundle;)V
.end method

.method public abstract n(Landroid/os/Bundle;)V
.end method

.method public abstract o(ILandroid/net/Uri;ZLandroid/os/Bundle;)V
.end method

.class public Lca2;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lca2$c;
    }
.end annotation


# static fields
.field public static final w:Ljava/lang/String;


# instance fields
.field public a:Landroid/content/Context;

.field public final b:Ljava/lang/String;

.field public c:Landroidx/work/WorkerParameters$a;

.field public d:Lp92;

.field public e:Landroidx/work/c;

.field public f:Lhu1;

.field public g:Landroidx/work/c$a;

.field public h:Landroidx/work/a;

.field public i:Lch;

.field public j:Lq70;

.field public k:Landroidx/work/impl/WorkDatabase;

.field public l:Lq92;

.field public m:Lqs;

.field public n:Ljava/util/List;

.field public p:Ljava/lang/String;

.field public q:Lum1;

.field public final t:Lum1;

.field public volatile v:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .line 1
    const-string v0, "WorkerWrapper"

    invoke-static {v0}, Lxl0;->i(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lca2;->w:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lca2$c;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Landroidx/work/c$a;->a()Landroidx/work/c$a;

    move-result-object v0

    iput-object v0, p0, Lca2;->g:Landroidx/work/c$a;

    invoke-static {}, Lum1;->s()Lum1;

    move-result-object v0

    iput-object v0, p0, Lca2;->q:Lum1;

    invoke-static {}, Lum1;->s()Lum1;

    move-result-object v0

    iput-object v0, p0, Lca2;->t:Lum1;

    const/16 v0, -0x100

    iput v0, p0, Lca2;->v:I

    iget-object v0, p1, Lca2$c;->a:Landroid/content/Context;

    iput-object v0, p0, Lca2;->a:Landroid/content/Context;

    iget-object v0, p1, Lca2$c;->d:Lhu1;

    iput-object v0, p0, Lca2;->f:Lhu1;

    iget-object v0, p1, Lca2$c;->c:Lq70;

    iput-object v0, p0, Lca2;->j:Lq70;

    iget-object v0, p1, Lca2$c;->g:Lp92;

    iput-object v0, p0, Lca2;->d:Lp92;

    iget-object v0, v0, Lp92;->a:Ljava/lang/String;

    iput-object v0, p0, Lca2;->b:Ljava/lang/String;

    iget-object v0, p1, Lca2$c;->i:Landroidx/work/WorkerParameters$a;

    iput-object v0, p0, Lca2;->c:Landroidx/work/WorkerParameters$a;

    iget-object v0, p1, Lca2$c;->b:Landroidx/work/c;

    iput-object v0, p0, Lca2;->e:Landroidx/work/c;

    iget-object v0, p1, Lca2$c;->e:Landroidx/work/a;

    iput-object v0, p0, Lca2;->h:Landroidx/work/a;

    invoke-virtual {v0}, Landroidx/work/a;->a()Lch;

    move-result-object v0

    iput-object v0, p0, Lca2;->i:Lch;

    iget-object v0, p1, Lca2$c;->f:Landroidx/work/impl/WorkDatabase;

    iput-object v0, p0, Lca2;->k:Landroidx/work/impl/WorkDatabase;

    invoke-virtual {v0}, Landroidx/work/impl/WorkDatabase;->K()Lq92;

    move-result-object v0

    iput-object v0, p0, Lca2;->l:Lq92;

    iget-object v0, p0, Lca2;->k:Landroidx/work/impl/WorkDatabase;

    invoke-virtual {v0}, Landroidx/work/impl/WorkDatabase;->F()Lqs;

    move-result-object v0

    iput-object v0, p0, Lca2;->m:Lqs;

    invoke-static {p1}, Lca2$c;->a(Lca2$c;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lca2;->n:Ljava/util/List;

    return-void
.end method

.method public static synthetic a(Lca2;Lik0;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lca2;->i(Lik0;)V

    return-void
.end method

.method private synthetic i(Lik0;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lca2;->t:Lum1;

    invoke-virtual {v0}, Landroidx/work/impl/utils/futures/AbstractFuture;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-interface {p1, v0}, Ljava/util/concurrent/Future;->cancel(Z)Z

    :cond_0
    return-void
.end method


# virtual methods
.method public final b(Ljava/util/List;)Ljava/lang/String;
    .locals 4

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Work [ id="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lca2;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", tags={ "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    const/4 v1, 0x1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    goto :goto_1

    :cond_0
    const-string v3, ", "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_1
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_1
    const-string p1, " } ]"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public c()Lik0;
    .locals 1

    .line 1
    iget-object v0, p0, Lca2;->q:Lum1;

    return-object v0
.end method

.method public d()Lx82;
    .locals 1

    .line 1
    iget-object v0, p0, Lca2;->d:Lp92;

    invoke-static {v0}, Lu92;->a(Lp92;)Lx82;

    move-result-object v0

    return-object v0
.end method

.method public e()Lp92;
    .locals 1

    .line 1
    iget-object v0, p0, Lca2;->d:Lp92;

    return-object v0
.end method

.method public final f(Landroidx/work/c$a;)V
    .locals 3

    .line 1
    instance-of v0, p1, Landroidx/work/c$a$c;

    if-eqz v0, :cond_1

    invoke-static {}, Lxl0;->e()Lxl0;

    move-result-object p1

    sget-object v0, Lca2;->w:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Worker result SUCCESS for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lca2;->p:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lxl0;->f(Ljava/lang/String;Ljava/lang/String;)V

    iget-object p1, p0, Lca2;->d:Lp92;

    invoke-virtual {p1}, Lp92;->m()Z

    move-result p1

    if-eqz p1, :cond_0

    :goto_0
    invoke-virtual {p0}, Lca2;->l()V

    goto :goto_1

    :cond_0
    invoke-virtual {p0}, Lca2;->q()V

    goto :goto_1

    :cond_1
    instance-of p1, p1, Landroidx/work/c$a$b;

    if-eqz p1, :cond_2

    invoke-static {}, Lxl0;->e()Lxl0;

    move-result-object p1

    sget-object v0, Lca2;->w:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Worker result RETRY for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lca2;->p:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lxl0;->f(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lca2;->k()V

    goto :goto_1

    :cond_2
    invoke-static {}, Lxl0;->e()Lxl0;

    move-result-object p1

    sget-object v0, Lca2;->w:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Worker result FAILURE for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lca2;->p:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lxl0;->f(Ljava/lang/String;Ljava/lang/String;)V

    iget-object p1, p0, Lca2;->d:Lp92;

    invoke-virtual {p1}, Lp92;->m()Z

    move-result p1

    if-eqz p1, :cond_3

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lca2;->p()V

    :goto_1
    return-void
.end method

.method public g(I)V
    .locals 2

    .line 1
    iput p1, p0, Lca2;->v:I

    invoke-virtual {p0}, Lca2;->r()Z

    iget-object v0, p0, Lca2;->t:Lum1;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroidx/work/impl/utils/futures/AbstractFuture;->cancel(Z)Z

    iget-object v0, p0, Lca2;->e:Landroidx/work/c;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lca2;->t:Lum1;

    invoke-virtual {v0}, Landroidx/work/impl/utils/futures/AbstractFuture;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lca2;->e:Landroidx/work/c;

    invoke-virtual {v0, p1}, Landroidx/work/c;->stop(I)V

    goto :goto_0

    :cond_0
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "WorkSpec "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lca2;->d:Lp92;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v0, " is already done. Not interrupting."

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {}, Lxl0;->e()Lxl0;

    move-result-object v0

    sget-object v1, Lca2;->w:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Lxl0;->a(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void
.end method

.method public final h(Ljava/lang/String;)V
    .locals 3

    .line 1
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    :goto_0
    invoke-virtual {v0}, Ljava/util/AbstractCollection;->isEmpty()Z

    move-result p1

    if-nez p1, :cond_1

    invoke-virtual {v0}, Ljava/util/LinkedList;->remove()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    iget-object v1, p0, Lca2;->l:Lq92;

    invoke-interface {v1, p1}, Lq92;->d(Ljava/lang/String;)Landroidx/work/WorkInfo$State;

    move-result-object v1

    sget-object v2, Landroidx/work/WorkInfo$State;->CANCELLED:Landroidx/work/WorkInfo$State;

    if-eq v1, v2, :cond_0

    iget-object v1, p0, Lca2;->l:Lq92;

    sget-object v2, Landroidx/work/WorkInfo$State;->FAILED:Landroidx/work/WorkInfo$State;

    invoke-interface {v1, v2, p1}, Lq92;->i(Landroidx/work/WorkInfo$State;Ljava/lang/String;)I

    :cond_0
    iget-object v1, p0, Lca2;->m:Lqs;

    invoke-interface {v1, p1}, Lqs;->a(Ljava/lang/String;)Ljava/util/List;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    :cond_1
    return-void
.end method

.method public j()V
    .locals 3

    .line 1
    invoke-virtual {p0}, Lca2;->r()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lca2;->k:Landroidx/work/impl/WorkDatabase;

    invoke-virtual {v0}, Landroidx/room/RoomDatabase;->e()V

    :try_start_0
    iget-object v0, p0, Lca2;->l:Lq92;

    iget-object v1, p0, Lca2;->b:Ljava/lang/String;

    invoke-interface {v0, v1}, Lq92;->d(Ljava/lang/String;)Landroidx/work/WorkInfo$State;

    move-result-object v0

    iget-object v1, p0, Lca2;->k:Landroidx/work/impl/WorkDatabase;

    invoke-virtual {v1}, Landroidx/work/impl/WorkDatabase;->J()Lj92;

    move-result-object v1

    iget-object v2, p0, Lca2;->b:Ljava/lang/String;

    invoke-interface {v1, v2}, Lj92;->a(Ljava/lang/String;)V

    if-nez v0, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lca2;->m(Z)V

    goto :goto_0

    :cond_0
    sget-object v1, Landroidx/work/WorkInfo$State;->RUNNING:Landroidx/work/WorkInfo$State;

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lca2;->g:Landroidx/work/c$a;

    invoke-virtual {p0, v0}, Lca2;->f(Landroidx/work/c$a;)V

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Landroidx/work/WorkInfo$State;->isFinished()Z

    move-result v0

    if-nez v0, :cond_2

    const/16 v0, -0x200

    iput v0, p0, Lca2;->v:I

    invoke-virtual {p0}, Lca2;->k()V

    :cond_2
    :goto_0
    iget-object v0, p0, Lca2;->k:Landroidx/work/impl/WorkDatabase;

    invoke-virtual {v0}, Landroidx/room/RoomDatabase;->D()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lca2;->k:Landroidx/work/impl/WorkDatabase;

    invoke-virtual {v0}, Landroidx/room/RoomDatabase;->i()V

    goto :goto_1

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lca2;->k:Landroidx/work/impl/WorkDatabase;

    invoke-virtual {v1}, Landroidx/room/RoomDatabase;->i()V

    throw v0

    :cond_3
    :goto_1
    return-void
.end method

.method public final k()V
    .locals 5

    .line 1
    iget-object v0, p0, Lca2;->k:Landroidx/work/impl/WorkDatabase;

    invoke-virtual {v0}, Landroidx/room/RoomDatabase;->e()V

    const/4 v0, 0x1

    :try_start_0
    iget-object v1, p0, Lca2;->l:Lq92;

    sget-object v2, Landroidx/work/WorkInfo$State;->ENQUEUED:Landroidx/work/WorkInfo$State;

    iget-object v3, p0, Lca2;->b:Ljava/lang/String;

    invoke-interface {v1, v2, v3}, Lq92;->i(Landroidx/work/WorkInfo$State;Ljava/lang/String;)I

    iget-object v1, p0, Lca2;->l:Lq92;

    iget-object v2, p0, Lca2;->b:Ljava/lang/String;

    iget-object v3, p0, Lca2;->i:Lch;

    invoke-interface {v3}, Lch;->currentTimeMillis()J

    move-result-wide v3

    invoke-interface {v1, v2, v3, v4}, Lq92;->j(Ljava/lang/String;J)V

    iget-object v1, p0, Lca2;->l:Lq92;

    iget-object v2, p0, Lca2;->b:Ljava/lang/String;

    iget-object v3, p0, Lca2;->d:Lp92;

    invoke-virtual {v3}, Lp92;->h()I

    move-result v3

    invoke-interface {v1, v2, v3}, Lq92;->o(Ljava/lang/String;I)V

    iget-object v1, p0, Lca2;->l:Lq92;

    iget-object v2, p0, Lca2;->b:Ljava/lang/String;

    const-wide/16 v3, -0x1

    invoke-interface {v1, v2, v3, v4}, Lq92;->v(Ljava/lang/String;J)I

    iget-object v1, p0, Lca2;->k:Landroidx/work/impl/WorkDatabase;

    invoke-virtual {v1}, Landroidx/room/RoomDatabase;->D()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v1, p0, Lca2;->k:Landroidx/work/impl/WorkDatabase;

    invoke-virtual {v1}, Landroidx/room/RoomDatabase;->i()V

    invoke-virtual {p0, v0}, Lca2;->m(Z)V

    return-void

    :catchall_0
    move-exception v1

    iget-object v2, p0, Lca2;->k:Landroidx/work/impl/WorkDatabase;

    invoke-virtual {v2}, Landroidx/room/RoomDatabase;->i()V

    invoke-virtual {p0, v0}, Lca2;->m(Z)V

    throw v1
.end method

.method public final l()V
    .locals 5

    .line 1
    iget-object v0, p0, Lca2;->k:Landroidx/work/impl/WorkDatabase;

    invoke-virtual {v0}, Landroidx/room/RoomDatabase;->e()V

    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p0, Lca2;->l:Lq92;

    iget-object v2, p0, Lca2;->b:Ljava/lang/String;

    iget-object v3, p0, Lca2;->i:Lch;

    invoke-interface {v3}, Lch;->currentTimeMillis()J

    move-result-wide v3

    invoke-interface {v1, v2, v3, v4}, Lq92;->j(Ljava/lang/String;J)V

    iget-object v1, p0, Lca2;->l:Lq92;

    sget-object v2, Landroidx/work/WorkInfo$State;->ENQUEUED:Landroidx/work/WorkInfo$State;

    iget-object v3, p0, Lca2;->b:Ljava/lang/String;

    invoke-interface {v1, v2, v3}, Lq92;->i(Landroidx/work/WorkInfo$State;Ljava/lang/String;)I

    iget-object v1, p0, Lca2;->l:Lq92;

    iget-object v2, p0, Lca2;->b:Ljava/lang/String;

    invoke-interface {v1, v2}, Lq92;->m(Ljava/lang/String;)I

    iget-object v1, p0, Lca2;->l:Lq92;

    iget-object v2, p0, Lca2;->b:Ljava/lang/String;

    iget-object v3, p0, Lca2;->d:Lp92;

    invoke-virtual {v3}, Lp92;->h()I

    move-result v3

    invoke-interface {v1, v2, v3}, Lq92;->o(Ljava/lang/String;I)V

    iget-object v1, p0, Lca2;->l:Lq92;

    iget-object v2, p0, Lca2;->b:Ljava/lang/String;

    invoke-interface {v1, v2}, Lq92;->p(Ljava/lang/String;)V

    iget-object v1, p0, Lca2;->l:Lq92;

    iget-object v2, p0, Lca2;->b:Ljava/lang/String;

    const-wide/16 v3, -0x1

    invoke-interface {v1, v2, v3, v4}, Lq92;->v(Ljava/lang/String;J)I

    iget-object v1, p0, Lca2;->k:Landroidx/work/impl/WorkDatabase;

    invoke-virtual {v1}, Landroidx/room/RoomDatabase;->D()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v1, p0, Lca2;->k:Landroidx/work/impl/WorkDatabase;

    invoke-virtual {v1}, Landroidx/room/RoomDatabase;->i()V

    invoke-virtual {p0, v0}, Lca2;->m(Z)V

    return-void

    :catchall_0
    move-exception v1

    iget-object v2, p0, Lca2;->k:Landroidx/work/impl/WorkDatabase;

    invoke-virtual {v2}, Landroidx/room/RoomDatabase;->i()V

    invoke-virtual {p0, v0}, Lca2;->m(Z)V

    throw v1
.end method

.method public final m(Z)V
    .locals 4

    .line 1
    iget-object v0, p0, Lca2;->k:Landroidx/work/impl/WorkDatabase;

    invoke-virtual {v0}, Landroidx/room/RoomDatabase;->e()V

    :try_start_0
    iget-object v0, p0, Lca2;->k:Landroidx/work/impl/WorkDatabase;

    invoke-virtual {v0}, Landroidx/work/impl/WorkDatabase;->K()Lq92;

    move-result-object v0

    invoke-interface {v0}, Lq92;->k()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lca2;->a:Landroid/content/Context;

    const-class v1, Landroidx/work/impl/background/systemalarm/RescheduleReceiver;

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Ln21;->c(Landroid/content/Context;Ljava/lang/Class;Z)V

    :cond_0
    if-eqz p1, :cond_1

    iget-object v0, p0, Lca2;->l:Lq92;

    sget-object v1, Landroidx/work/WorkInfo$State;->ENQUEUED:Landroidx/work/WorkInfo$State;

    iget-object v2, p0, Lca2;->b:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lq92;->i(Landroidx/work/WorkInfo$State;Ljava/lang/String;)I

    iget-object v0, p0, Lca2;->l:Lq92;

    iget-object v1, p0, Lca2;->b:Ljava/lang/String;

    iget v2, p0, Lca2;->v:I

    invoke-interface {v0, v1, v2}, Lq92;->b(Ljava/lang/String;I)V

    iget-object v0, p0, Lca2;->l:Lq92;

    iget-object v1, p0, Lca2;->b:Ljava/lang/String;

    const-wide/16 v2, -0x1

    invoke-interface {v0, v1, v2, v3}, Lq92;->v(Ljava/lang/String;J)I

    :cond_1
    iget-object v0, p0, Lca2;->k:Landroidx/work/impl/WorkDatabase;

    invoke-virtual {v0}, Landroidx/room/RoomDatabase;->D()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lca2;->k:Landroidx/work/impl/WorkDatabase;

    invoke-virtual {v0}, Landroidx/room/RoomDatabase;->i()V

    iget-object v0, p0, Lca2;->q:Lum1;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-virtual {v0, p1}, Lum1;->o(Ljava/lang/Object;)Z

    return-void

    :catchall_0
    move-exception p1

    iget-object v0, p0, Lca2;->k:Landroidx/work/impl/WorkDatabase;

    invoke-virtual {v0}, Landroidx/room/RoomDatabase;->i()V

    throw p1
.end method

.method public final n()V
    .locals 5

    .line 1
    iget-object v0, p0, Lca2;->l:Lq92;

    iget-object v1, p0, Lca2;->b:Ljava/lang/String;

    invoke-interface {v0, v1}, Lq92;->d(Ljava/lang/String;)Landroidx/work/WorkInfo$State;

    move-result-object v0

    sget-object v1, Landroidx/work/WorkInfo$State;->RUNNING:Landroidx/work/WorkInfo$State;

    const-string v2, "Status for "

    if-ne v0, v1, :cond_0

    invoke-static {}, Lxl0;->e()Lxl0;

    move-result-object v0

    sget-object v1, Lca2;->w:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lca2;->b:Ljava/lang/String;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " is RUNNING; not doing any work and rescheduling for later execution"

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lxl0;->a(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    invoke-static {}, Lxl0;->e()Lxl0;

    move-result-object v1

    sget-object v3, Lca2;->w:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lca2;->b:Ljava/lang/String;

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " is "

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v0, " ; not doing any work"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v3, v0}, Lxl0;->a(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, v0}, Lca2;->m(Z)V

    return-void
.end method

.method public final o()V
    .locals 15

    .line 1
    invoke-virtual {p0}, Lca2;->r()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lca2;->k:Landroidx/work/impl/WorkDatabase;

    invoke-virtual {v0}, Landroidx/room/RoomDatabase;->e()V

    :try_start_0
    iget-object v0, p0, Lca2;->d:Lp92;

    iget-object v1, v0, Lp92;->b:Landroidx/work/WorkInfo$State;

    sget-object v2, Landroidx/work/WorkInfo$State;->ENQUEUED:Landroidx/work/WorkInfo$State;

    if-eq v1, v2, :cond_1

    invoke-virtual {p0}, Lca2;->n()V

    iget-object v0, p0, Lca2;->k:Landroidx/work/impl/WorkDatabase;

    invoke-virtual {v0}, Landroidx/room/RoomDatabase;->D()V

    invoke-static {}, Lxl0;->e()Lxl0;

    move-result-object v0

    sget-object v1, Lca2;->w:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lca2;->d:Lp92;

    iget-object v3, v3, Lp92;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, " is not in ENQUEUED state. Nothing more to do"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lxl0;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lca2;->k:Landroidx/work/impl/WorkDatabase;

    invoke-virtual {v0}, Landroidx/room/RoomDatabase;->i()V

    return-void

    :cond_1
    :try_start_1
    invoke-virtual {v0}, Lp92;->m()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lca2;->d:Lp92;

    invoke-virtual {v0}, Lp92;->l()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    iget-object v0, p0, Lca2;->i:Lch;

    invoke-interface {v0}, Lch;->currentTimeMillis()J

    move-result-wide v0

    iget-object v2, p0, Lca2;->d:Lp92;

    invoke-virtual {v2}, Lp92;->c()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-gez v0, :cond_3

    invoke-static {}, Lxl0;->e()Lxl0;

    move-result-object v0

    sget-object v1, Lca2;->w:Ljava/lang/String;

    const-string v2, "Delaying execution for %s because it is being executed before schedule."

    const/4 v3, 0x1

    new-array v4, v3, [Ljava/lang/Object;

    iget-object v5, p0, Lca2;->d:Lp92;

    iget-object v5, v5, Lp92;->c:Ljava/lang/String;

    const/4 v6, 0x0

    aput-object v5, v4, v6

    invoke-static {v2, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lxl0;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v3}, Lca2;->m(Z)V

    iget-object v0, p0, Lca2;->k:Landroidx/work/impl/WorkDatabase;

    invoke-virtual {v0}, Landroidx/room/RoomDatabase;->D()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v0, p0, Lca2;->k:Landroidx/work/impl/WorkDatabase;

    invoke-virtual {v0}, Landroidx/room/RoomDatabase;->i()V

    return-void

    :cond_3
    :try_start_2
    iget-object v0, p0, Lca2;->k:Landroidx/work/impl/WorkDatabase;

    invoke-virtual {v0}, Landroidx/room/RoomDatabase;->D()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    iget-object v0, p0, Lca2;->k:Landroidx/work/impl/WorkDatabase;

    invoke-virtual {v0}, Landroidx/room/RoomDatabase;->i()V

    iget-object v0, p0, Lca2;->d:Lp92;

    invoke-virtual {v0}, Lp92;->m()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lca2;->d:Lp92;

    iget-object v0, v0, Lp92;->e:Landroidx/work/b;

    :goto_0
    move-object v3, v0

    goto :goto_1

    :cond_4
    iget-object v0, p0, Lca2;->h:Landroidx/work/a;

    invoke-virtual {v0}, Landroidx/work/a;->f()Llf0;

    move-result-object v0

    iget-object v1, p0, Lca2;->d:Lp92;

    iget-object v1, v1, Lp92;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Llf0;->b(Ljava/lang/String;)Lkf0;

    move-result-object v0

    if-nez v0, :cond_5

    invoke-static {}, Lxl0;->e()Lxl0;

    move-result-object v0

    sget-object v1, Lca2;->w:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Could not create Input Merger "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lca2;->d:Lp92;

    iget-object v3, v3, Lp92;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lxl0;->c(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lca2;->p()V

    return-void

    :cond_5
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iget-object v2, p0, Lca2;->d:Lp92;

    iget-object v2, v2, Lp92;->e:Landroidx/work/b;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v2, p0, Lca2;->l:Lq92;

    iget-object v3, p0, Lca2;->b:Ljava/lang/String;

    invoke-interface {v2, v3}, Lq92;->g(Ljava/lang/String;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    invoke-virtual {v0, v1}, Lkf0;->a(Ljava/util/List;)Landroidx/work/b;

    move-result-object v0

    goto :goto_0

    :goto_1
    new-instance v0, Landroidx/work/WorkerParameters;

    iget-object v1, p0, Lca2;->b:Ljava/lang/String;

    invoke-static {v1}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v2

    iget-object v4, p0, Lca2;->n:Ljava/util/List;

    iget-object v5, p0, Lca2;->c:Landroidx/work/WorkerParameters$a;

    iget-object v1, p0, Lca2;->d:Lp92;

    iget v6, v1, Lp92;->k:I

    invoke-virtual {v1}, Lp92;->f()I

    move-result v7

    iget-object v1, p0, Lca2;->h:Landroidx/work/a;

    invoke-virtual {v1}, Landroidx/work/a;->d()Ljava/util/concurrent/Executor;

    move-result-object v8

    iget-object v9, p0, Lca2;->f:Lhu1;

    iget-object v1, p0, Lca2;->h:Landroidx/work/a;

    invoke-virtual {v1}, Landroidx/work/a;->n()Laa2;

    move-result-object v10

    new-instance v11, Ll92;

    iget-object v1, p0, Lca2;->k:Landroidx/work/impl/WorkDatabase;

    iget-object v12, p0, Lca2;->f:Lhu1;

    invoke-direct {v11, v1, v12}, Ll92;-><init>(Landroidx/work/impl/WorkDatabase;Lhu1;)V

    new-instance v12, Lw82;

    iget-object v1, p0, Lca2;->k:Landroidx/work/impl/WorkDatabase;

    iget-object v13, p0, Lca2;->j:Lq70;

    iget-object v14, p0, Lca2;->f:Lhu1;

    invoke-direct {v12, v1, v13, v14}, Lw82;-><init>(Landroidx/work/impl/WorkDatabase;Lq70;Lhu1;)V

    move-object v1, v0

    invoke-direct/range {v1 .. v12}, Landroidx/work/WorkerParameters;-><init>(Ljava/util/UUID;Landroidx/work/b;Ljava/util/Collection;Landroidx/work/WorkerParameters$a;IILjava/util/concurrent/Executor;Lhu1;Laa2;Le91;Lr70;)V

    iget-object v1, p0, Lca2;->e:Landroidx/work/c;

    if-nez v1, :cond_6

    iget-object v1, p0, Lca2;->h:Landroidx/work/a;

    invoke-virtual {v1}, Landroidx/work/a;->n()Laa2;

    move-result-object v1

    iget-object v2, p0, Lca2;->a:Landroid/content/Context;

    iget-object v3, p0, Lca2;->d:Lp92;

    iget-object v3, v3, Lp92;->c:Ljava/lang/String;

    invoke-virtual {v1, v2, v3, v0}, Laa2;->b(Landroid/content/Context;Ljava/lang/String;Landroidx/work/WorkerParameters;)Landroidx/work/c;

    move-result-object v1

    iput-object v1, p0, Lca2;->e:Landroidx/work/c;

    :cond_6
    iget-object v1, p0, Lca2;->e:Landroidx/work/c;

    if-nez v1, :cond_7

    invoke-static {}, Lxl0;->e()Lxl0;

    move-result-object v0

    sget-object v1, Lca2;->w:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Could not create Worker "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lca2;->d:Lp92;

    iget-object v3, v3, Lp92;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lxl0;->c(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lca2;->p()V

    return-void

    :cond_7
    invoke-virtual {v1}, Landroidx/work/c;->isUsed()Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-static {}, Lxl0;->e()Lxl0;

    move-result-object v0

    sget-object v1, Lca2;->w:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Received an already-used Worker "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lca2;->d:Lp92;

    iget-object v3, v3, Lp92;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "; Worker Factory should return new instances"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lxl0;->c(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lca2;->p()V

    return-void

    :cond_8
    iget-object v1, p0, Lca2;->e:Landroidx/work/c;

    invoke-virtual {v1}, Landroidx/work/c;->setUsed()V

    invoke-virtual {p0}, Lca2;->s()Z

    move-result v1

    if-eqz v1, :cond_a

    invoke-virtual {p0}, Lca2;->r()Z

    move-result v1

    if-eqz v1, :cond_9

    return-void

    :cond_9
    new-instance v1, Lv82;

    iget-object v3, p0, Lca2;->a:Landroid/content/Context;

    iget-object v4, p0, Lca2;->d:Lp92;

    iget-object v5, p0, Lca2;->e:Landroidx/work/c;

    invoke-virtual {v0}, Landroidx/work/WorkerParameters;->b()Lr70;

    move-result-object v6

    iget-object v7, p0, Lca2;->f:Lhu1;

    move-object v2, v1

    invoke-direct/range {v2 .. v7}, Lv82;-><init>(Landroid/content/Context;Lp92;Landroidx/work/c;Lr70;Lhu1;)V

    iget-object v0, p0, Lca2;->f:Lhu1;

    invoke-interface {v0}, Lhu1;->c()Ljava/util/concurrent/Executor;

    move-result-object v0

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    invoke-virtual {v1}, Lv82;->b()Lik0;

    move-result-object v0

    iget-object v1, p0, Lca2;->t:Lum1;

    new-instance v2, Lba2;

    invoke-direct {v2, p0, v0}, Lba2;-><init>(Lca2;Lik0;)V

    new-instance v3, Lbt1;

    invoke-direct {v3}, Lbt1;-><init>()V

    invoke-virtual {v1, v2, v3}, Landroidx/work/impl/utils/futures/AbstractFuture;->addListener(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V

    new-instance v1, Lca2$a;

    invoke-direct {v1, p0, v0}, Lca2$a;-><init>(Lca2;Lik0;)V

    iget-object v2, p0, Lca2;->f:Lhu1;

    invoke-interface {v2}, Lhu1;->c()Ljava/util/concurrent/Executor;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lik0;->addListener(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V

    iget-object v0, p0, Lca2;->p:Ljava/lang/String;

    iget-object v1, p0, Lca2;->t:Lum1;

    new-instance v2, Lca2$b;

    invoke-direct {v2, p0, v0}, Lca2$b;-><init>(Lca2;Ljava/lang/String;)V

    iget-object v0, p0, Lca2;->f:Lhu1;

    invoke-interface {v0}, Lhu1;->d()Ljl1;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroidx/work/impl/utils/futures/AbstractFuture;->addListener(Ljava/lang/Runnable;Ljava/util/concurrent/Executor;)V

    goto :goto_2

    :cond_a
    invoke-virtual {p0}, Lca2;->n()V

    :goto_2
    return-void

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lca2;->k:Landroidx/work/impl/WorkDatabase;

    invoke-virtual {v1}, Landroidx/room/RoomDatabase;->i()V

    throw v0
.end method

.method public p()V
    .locals 5

    .line 1
    iget-object v0, p0, Lca2;->k:Landroidx/work/impl/WorkDatabase;

    invoke-virtual {v0}, Landroidx/room/RoomDatabase;->e()V

    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p0, Lca2;->b:Ljava/lang/String;

    invoke-virtual {p0, v1}, Lca2;->h(Ljava/lang/String;)V

    iget-object v1, p0, Lca2;->g:Landroidx/work/c$a;

    check-cast v1, Landroidx/work/c$a$a;

    invoke-virtual {v1}, Landroidx/work/c$a$a;->e()Landroidx/work/b;

    move-result-object v1

    iget-object v2, p0, Lca2;->l:Lq92;

    iget-object v3, p0, Lca2;->b:Ljava/lang/String;

    iget-object v4, p0, Lca2;->d:Lp92;

    invoke-virtual {v4}, Lp92;->h()I

    move-result v4

    invoke-interface {v2, v3, v4}, Lq92;->o(Ljava/lang/String;I)V

    iget-object v2, p0, Lca2;->l:Lq92;

    iget-object v3, p0, Lca2;->b:Ljava/lang/String;

    invoke-interface {v2, v3, v1}, Lq92;->y(Ljava/lang/String;Landroidx/work/b;)V

    iget-object v1, p0, Lca2;->k:Landroidx/work/impl/WorkDatabase;

    invoke-virtual {v1}, Landroidx/room/RoomDatabase;->D()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v1, p0, Lca2;->k:Landroidx/work/impl/WorkDatabase;

    invoke-virtual {v1}, Landroidx/room/RoomDatabase;->i()V

    invoke-virtual {p0, v0}, Lca2;->m(Z)V

    return-void

    :catchall_0
    move-exception v1

    iget-object v2, p0, Lca2;->k:Landroidx/work/impl/WorkDatabase;

    invoke-virtual {v2}, Landroidx/room/RoomDatabase;->i()V

    invoke-virtual {p0, v0}, Lca2;->m(Z)V

    throw v1
.end method

.method public final q()V
    .locals 9

    .line 1
    iget-object v0, p0, Lca2;->k:Landroidx/work/impl/WorkDatabase;

    invoke-virtual {v0}, Landroidx/room/RoomDatabase;->e()V

    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p0, Lca2;->l:Lq92;

    sget-object v2, Landroidx/work/WorkInfo$State;->SUCCEEDED:Landroidx/work/WorkInfo$State;

    iget-object v3, p0, Lca2;->b:Ljava/lang/String;

    invoke-interface {v1, v2, v3}, Lq92;->i(Landroidx/work/WorkInfo$State;Ljava/lang/String;)I

    iget-object v1, p0, Lca2;->g:Landroidx/work/c$a;

    check-cast v1, Landroidx/work/c$a$c;

    invoke-virtual {v1}, Landroidx/work/c$a$c;->e()Landroidx/work/b;

    move-result-object v1

    iget-object v2, p0, Lca2;->l:Lq92;

    iget-object v3, p0, Lca2;->b:Ljava/lang/String;

    invoke-interface {v2, v3, v1}, Lq92;->y(Ljava/lang/String;Landroidx/work/b;)V

    iget-object v1, p0, Lca2;->i:Lch;

    invoke-interface {v1}, Lch;->currentTimeMillis()J

    move-result-wide v1

    iget-object v3, p0, Lca2;->m:Lqs;

    iget-object v4, p0, Lca2;->b:Ljava/lang/String;

    invoke-interface {v3, v4}, Lqs;->a(Ljava/lang/String;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    iget-object v5, p0, Lca2;->l:Lq92;

    invoke-interface {v5, v4}, Lq92;->d(Ljava/lang/String;)Landroidx/work/WorkInfo$State;

    move-result-object v5

    sget-object v6, Landroidx/work/WorkInfo$State;->BLOCKED:Landroidx/work/WorkInfo$State;

    if-ne v5, v6, :cond_0

    iget-object v5, p0, Lca2;->m:Lqs;

    invoke-interface {v5, v4}, Lqs;->b(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-static {}, Lxl0;->e()Lxl0;

    move-result-object v5

    sget-object v6, Lca2;->w:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Setting status to enqueued for "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Lxl0;->f(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v5, p0, Lca2;->l:Lq92;

    sget-object v6, Landroidx/work/WorkInfo$State;->ENQUEUED:Landroidx/work/WorkInfo$State;

    invoke-interface {v5, v6, v4}, Lq92;->i(Landroidx/work/WorkInfo$State;Ljava/lang/String;)I

    iget-object v5, p0, Lca2;->l:Lq92;

    invoke-interface {v5, v4, v1, v2}, Lq92;->j(Ljava/lang/String;J)V

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lca2;->k:Landroidx/work/impl/WorkDatabase;

    invoke-virtual {v1}, Landroidx/room/RoomDatabase;->D()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v1, p0, Lca2;->k:Landroidx/work/impl/WorkDatabase;

    invoke-virtual {v1}, Landroidx/room/RoomDatabase;->i()V

    invoke-virtual {p0, v0}, Lca2;->m(Z)V

    return-void

    :catchall_0
    move-exception v1

    iget-object v2, p0, Lca2;->k:Landroidx/work/impl/WorkDatabase;

    invoke-virtual {v2}, Landroidx/room/RoomDatabase;->i()V

    invoke-virtual {p0, v0}, Lca2;->m(Z)V

    throw v1
.end method

.method public final r()Z
    .locals 5

    .line 1
    iget v0, p0, Lca2;->v:I

    const/16 v1, -0x100

    const/4 v2, 0x0

    if-eq v0, v1, :cond_1

    invoke-static {}, Lxl0;->e()Lxl0;

    move-result-object v0

    sget-object v1, Lca2;->w:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Work interrupted for "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, p0, Lca2;->p:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Lxl0;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lca2;->l:Lq92;

    iget-object v1, p0, Lca2;->b:Ljava/lang/String;

    invoke-interface {v0, v1}, Lq92;->d(Ljava/lang/String;)Landroidx/work/WorkInfo$State;

    move-result-object v0

    const/4 v1, 0x1

    if-nez v0, :cond_0

    invoke-virtual {p0, v2}, Lca2;->m(Z)V

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Landroidx/work/WorkInfo$State;->isFinished()Z

    move-result v0

    xor-int/2addr v0, v1

    invoke-virtual {p0, v0}, Lca2;->m(Z)V

    :goto_0
    return v1

    :cond_1
    return v2
.end method

.method public run()V
    .locals 1

    .line 1
    iget-object v0, p0, Lca2;->n:Ljava/util/List;

    invoke-virtual {p0, v0}, Lca2;->b(Ljava/util/List;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lca2;->p:Ljava/lang/String;

    invoke-virtual {p0}, Lca2;->o()V

    return-void
.end method

.method public final s()Z
    .locals 3

    .line 1
    iget-object v0, p0, Lca2;->k:Landroidx/work/impl/WorkDatabase;

    invoke-virtual {v0}, Landroidx/room/RoomDatabase;->e()V

    :try_start_0
    iget-object v0, p0, Lca2;->l:Lq92;

    iget-object v1, p0, Lca2;->b:Ljava/lang/String;

    invoke-interface {v0, v1}, Lq92;->d(Ljava/lang/String;)Landroidx/work/WorkInfo$State;

    move-result-object v0

    sget-object v1, Landroidx/work/WorkInfo$State;->ENQUEUED:Landroidx/work/WorkInfo$State;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lca2;->l:Lq92;

    sget-object v1, Landroidx/work/WorkInfo$State;->RUNNING:Landroidx/work/WorkInfo$State;

    iget-object v2, p0, Lca2;->b:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lq92;->i(Landroidx/work/WorkInfo$State;Ljava/lang/String;)I

    iget-object v0, p0, Lca2;->l:Lq92;

    iget-object v1, p0, Lca2;->b:Ljava/lang/String;

    invoke-interface {v0, v1}, Lq92;->A(Ljava/lang/String;)I

    iget-object v0, p0, Lca2;->l:Lq92;

    iget-object v1, p0, Lca2;->b:Ljava/lang/String;

    const/16 v2, -0x100

    invoke-interface {v0, v1, v2}, Lq92;->b(Ljava/lang/String;I)V

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lca2;->k:Landroidx/work/impl/WorkDatabase;

    invoke-virtual {v1}, Landroidx/room/RoomDatabase;->D()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v1, p0, Lca2;->k:Landroidx/work/impl/WorkDatabase;

    invoke-virtual {v1}, Landroidx/room/RoomDatabase;->i()V

    return v0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lca2;->k:Landroidx/work/impl/WorkDatabase;

    invoke-virtual {v1}, Landroidx/room/RoomDatabase;->i()V

    throw v0
.end method

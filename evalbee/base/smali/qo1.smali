.class public final Lqo1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Comparable;


# static fields
.field public static final b:Lqo1;


# instance fields
.field public final a:Lpw1;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    .line 1
    new-instance v0, Lqo1;

    new-instance v1, Lpw1;

    const-wide/16 v2, 0x0

    const/4 v4, 0x0

    invoke-direct {v1, v2, v3, v4}, Lpw1;-><init>(JI)V

    invoke-direct {v0, v1}, Lqo1;-><init>(Lpw1;)V

    sput-object v0, Lqo1;->b:Lqo1;

    return-void
.end method

.method public constructor <init>(Lpw1;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lqo1;->a:Lpw1;

    return-void
.end method


# virtual methods
.method public a(Lqo1;)I
    .locals 1

    .line 1
    iget-object v0, p0, Lqo1;->a:Lpw1;

    iget-object p1, p1, Lqo1;->a:Lpw1;

    invoke-virtual {v0, p1}, Lpw1;->c(Lpw1;)I

    move-result p1

    return p1
.end method

.method public c()Lpw1;
    .locals 1

    .line 1
    iget-object v0, p0, Lqo1;->a:Lpw1;

    return-object v0
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 0

    .line 1
    check-cast p1, Lqo1;

    invoke-virtual {p0, p1}, Lqo1;->a(Lqo1;)I

    move-result p1

    return p1
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .line 1
    const/4 v0, 0x1

    if-ne p1, p0, :cond_0

    return v0

    :cond_0
    instance-of v1, p1, Lqo1;

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    :cond_1
    check-cast p1, Lqo1;

    invoke-virtual {p0, p1}, Lqo1;->a(Lqo1;)I

    move-result p1

    if-nez p1, :cond_2

    goto :goto_0

    :cond_2
    move v0, v2

    :goto_0
    return v0
.end method

.method public hashCode()I
    .locals 1

    .line 1
    invoke-virtual {p0}, Lqo1;->c()Lpw1;

    move-result-object v0

    invoke-virtual {v0}, Lpw1;->hashCode()I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SnapshotVersion(seconds="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lqo1;->a:Lpw1;

    invoke-virtual {v1}, Lpw1;->e()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, ", nanos="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lqo1;->a:Lpw1;

    invoke-virtual {v1}, Lpw1;->d()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.class public Lf2$b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lzg;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lf2;->v(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Z

.field public final synthetic b:Lf2;


# direct methods
.method public constructor <init>(Lf2;Z)V
    .locals 0

    .line 1
    iput-object p1, p0, Lf2$b;->b:Lf2;

    iput-boolean p2, p0, Lf2$b;->a:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(ZILjava/lang/Object;)V
    .locals 12

    .line 1
    if-eqz p1, :cond_6

    check-cast p3, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/SyncExamsFile;

    iget-object p1, p0, Lf2$b;->b:Lf2;

    iget-object p1, p1, Lf2;->a:Landroid/content/Context;

    invoke-static {p1}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;

    move-result-object p1

    invoke-virtual {p3}, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/SyncExamsFile;->getExams()Ljava/util/ArrayList;

    move-result-object p2

    const/4 v0, 0x1

    if-eqz p2, :cond_5

    invoke-virtual {p3}, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/SyncExamsFile;->getExams()Ljava/util/ArrayList;

    move-result-object p2

    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :cond_0
    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/ExamPartialResponse;

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/ExamPartialResponse;->getId()Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {p1, v2}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getTemplateJsonForCloudId(Ljava/lang/Long;)Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;

    move-result-object v2

    const/4 v3, 0x0

    const-class v4, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->isSynced()Z

    move-result v5

    if-nez v5, :cond_1

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->isCloudSyncOn()Z

    move-result v5

    if-nez v5, :cond_3

    :cond_1
    new-instance v5, Lgc0;

    invoke-direct {v5}, Lgc0;-><init>()V

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/ExamPartialResponse;->getTemplate()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6, v4}, Lgc0;->j(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    invoke-virtual {v2, v5}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->setSheetTemplate(Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)V

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/ExamPartialResponse;->getPublished()Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/Published;

    move-result-object v5

    sget-object v6, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/Published;->PUBLISHED:Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/Published;

    if-ne v5, v6, :cond_2

    move v5, v0

    goto :goto_1

    :cond_2
    move v5, v3

    :goto_1
    invoke-virtual {v2, v5}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->setPublished(Z)V

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/ExamPartialResponse;->getUpdatedOn()Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v2, v5}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->setLastUpdatedOn(Ljava/lang/Long;)V

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/ExamPartialResponse;->getUserId()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->setUserId(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->setCloudSyncOn(Z)V

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/ExamPartialResponse;->isSyncImages()Z

    move-result v5

    invoke-virtual {v2, v5}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->setSyncImages(Z)V

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/ExamPartialResponse;->getSharedType()Lcom/ekodroid/omrevaluator/serializable/SharedType;

    move-result-object v5

    invoke-virtual {v2, v5}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->setSharedType(Lcom/ekodroid/omrevaluator/serializable/SharedType;)V

    invoke-virtual {p1, v2}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->saveOrUpdateTemplateJsonAsSynced(Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;)Z

    :cond_3
    if-nez v2, :cond_0

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/ExamPartialResponse;->getExamId()Lcom/ekodroid/omrevaluator/templateui/models/ExamId;

    move-result-object v2

    new-instance v5, Lgc0;

    invoke-direct {v5}, Lgc0;-><init>()V

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/ExamPartialResponse;->getTemplate()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6, v4}, Lgc0;->j(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v4

    move-object v9, v4

    check-cast v9, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    iget-object v4, p0, Lf2$b;->b:Lf2;

    iget-object v4, v4, Lf2;->a:Landroid/content/Context;

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;->getExamDate()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lb10;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    new-instance v4, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;->getExamName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;->getClassName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/templateui/models/ExamId;->getExamDate()Ljava/lang/String;

    move-result-object v8

    const/4 v11, 0x0

    move-object v5, v4

    invoke-direct/range {v5 .. v11}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/ExamPartialResponse;->getPublished()Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/Published;

    move-result-object v5

    sget-object v6, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/Published;->PUBLISHED:Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/Published;

    if-ne v5, v6, :cond_4

    move v3, v0

    :cond_4
    invoke-virtual {v4, v3}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->setPublished(Z)V

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/ExamPartialResponse;->getUpdatedOn()Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v4, v3}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->setLastUpdatedOn(Ljava/lang/Long;)V

    invoke-virtual {v4, v0}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->setCloudSyncOn(Z)V

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/ExamPartialResponse;->getId()Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v4, v3}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->setCloudId(Ljava/lang/Long;)V

    const-wide/16 v5, 0x0

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v4, v3}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->setLastSyncedOn(Ljava/lang/Long;)V

    invoke-virtual {v4, v0}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->setSynced(Z)V

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/ExamPartialResponse;->getUserId()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->setUserId(Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/ExamPartialResponse;->isSyncImages()Z

    move-result v3

    invoke-virtual {v4, v3}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->setSyncImages(Z)V

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/ExamPartialResponse;->getSharedType()Lcom/ekodroid/omrevaluator/serializable/SharedType;

    move-result-object v1

    invoke-virtual {v4, v1}, Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;->setSharedType(Lcom/ekodroid/omrevaluator/serializable/SharedType;)V

    invoke-virtual {p1, v2}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->deleteTemplateJson(Lcom/ekodroid/omrevaluator/templateui/models/ExamId;)Z

    invoke-virtual {p1, v4}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->saveOrUpdateTemplateJsonAsSynced(Lcom/ekodroid/omrevaluator/database/TemplateDataJsonModel;)Z

    goto/16 :goto_0

    :cond_5
    iget-object p1, p0, Lf2$b;->b:Lf2;

    iget-boolean p2, p0, Lf2$b;->a:Z

    invoke-static {p1, p3, p2}, Lf2;->m(Lf2;Lcom/ekodroid/omrevaluator/clients/SyncClients/Models/SyncExamsFile;Z)V

    new-instance p1, Landroid/content/Intent;

    const-string p2, "UPDATE_EXAM_LIST"

    invoke-direct {p1, p2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string p2, "data_changed"

    invoke-virtual {p1, p2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    iget-object p2, p0, Lf2$b;->b:Lf2;

    iget-object p2, p2, Lf2;->a:Landroid/content/Context;

    invoke-virtual {p2, p1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    :cond_6
    return-void
.end method

.class public Lmg0$b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lmg0;->b()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/PendingJoinRequest;

.field public final synthetic b:Lmg0;


# direct methods
.method public constructor <init>(Lmg0;Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/PendingJoinRequest;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lmg0$b;->b:Lmg0;

    iput-object p2, p0, Lmg0$b;->a:Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/PendingJoinRequest;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2

    .line 1
    iget-object p2, p0, Lmg0$b;->b:Lmg0;

    iget-object v0, p0, Lmg0$b;->a:Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/PendingJoinRequest;

    sget-object v1, Lcom/ekodroid/omrevaluator/more/models/Teacher$MemberStatus;->REJECTED:Lcom/ekodroid/omrevaluator/more/models/Teacher$MemberStatus;

    invoke-static {p2, v0, v1}, Lmg0;->a(Lmg0;Lcom/ekodroid/omrevaluator/clients/UserProfileClients/models/PendingJoinRequest;Lcom/ekodroid/omrevaluator/more/models/Teacher$MemberStatus;)V

    iget-object p2, p0, Lmg0$b;->b:Lmg0;

    iget-object p2, p2, Lmg0;->a:Landroid/content/Context;

    invoke-static {p2}, Lcom/google/firebase/analytics/FirebaseAnalytics;->getInstance(Landroid/content/Context;)Lcom/google/firebase/analytics/FirebaseAnalytics;

    move-result-object p2

    const-string v0, "ADD_TEACHER_DECLINE"

    const/4 v1, 0x0

    invoke-virtual {p2, v0, v1}, Lcom/google/firebase/analytics/FirebaseAnalytics;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    return-void
.end method

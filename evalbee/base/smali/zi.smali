.class public final Lzi;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lzi$b;
    }
.end annotation


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/util/Set;

.field public final c:Ljava/util/Set;

.field public final d:I

.field public final e:I

.field public final f:Lmj;

.field public final g:Ljava/util/Set;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/util/Set;Ljava/util/Set;IILmj;Ljava/util/Set;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lzi;->a:Ljava/lang/String;

    invoke-static {p2}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object p1

    iput-object p1, p0, Lzi;->b:Ljava/util/Set;

    invoke-static {p3}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object p1

    iput-object p1, p0, Lzi;->c:Ljava/util/Set;

    iput p4, p0, Lzi;->d:I

    iput p5, p0, Lzi;->e:I

    iput-object p6, p0, Lzi;->f:Lmj;

    invoke-static {p7}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object p1

    iput-object p1, p0, Lzi;->g:Ljava/util/Set;

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/String;Ljava/util/Set;Ljava/util/Set;IILmj;Ljava/util/Set;Lzi$a;)V
    .locals 0

    .line 2
    invoke-direct/range {p0 .. p7}, Lzi;-><init>(Ljava/lang/String;Ljava/util/Set;Ljava/util/Set;IILmj;Ljava/util/Set;)V

    return-void
.end method

.method public static synthetic a(Ljava/lang/Object;Lgj;)Ljava/lang/Object;
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lzi;->q(Ljava/lang/Object;Lgj;)Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic b(Ljava/lang/Object;Lgj;)Ljava/lang/Object;
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lzi;->r(Ljava/lang/Object;Lgj;)Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method public static c(Lda1;)Lzi$b;
    .locals 3

    .line 1
    new-instance v0, Lzi$b;

    const/4 v1, 0x0

    new-array v1, v1, [Lda1;

    const/4 v2, 0x0

    invoke-direct {v0, p0, v1, v2}, Lzi$b;-><init>(Lda1;[Lda1;Lzi$a;)V

    return-object v0
.end method

.method public static varargs d(Lda1;[Lda1;)Lzi$b;
    .locals 2

    .line 1
    new-instance v0, Lzi$b;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1}, Lzi$b;-><init>(Lda1;[Lda1;Lzi$a;)V

    return-object v0
.end method

.method public static e(Ljava/lang/Class;)Lzi$b;
    .locals 3

    .line 1
    new-instance v0, Lzi$b;

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Class;

    const/4 v2, 0x0

    invoke-direct {v0, p0, v1, v2}, Lzi$b;-><init>(Ljava/lang/Class;[Ljava/lang/Class;Lzi$a;)V

    return-object v0
.end method

.method public static varargs f(Ljava/lang/Class;[Ljava/lang/Class;)Lzi$b;
    .locals 2

    .line 1
    new-instance v0, Lzi$b;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1}, Lzi$b;-><init>(Ljava/lang/Class;[Ljava/lang/Class;Lzi$a;)V

    return-object v0
.end method

.method public static l(Ljava/lang/Object;Ljava/lang/Class;)Lzi;
    .locals 1

    .line 1
    invoke-static {p1}, Lzi;->m(Ljava/lang/Class;)Lzi$b;

    move-result-object p1

    new-instance v0, Lxi;

    invoke-direct {v0, p0}, Lxi;-><init>(Ljava/lang/Object;)V

    invoke-virtual {p1, v0}, Lzi$b;->f(Lmj;)Lzi$b;

    move-result-object p0

    invoke-virtual {p0}, Lzi$b;->d()Lzi;

    move-result-object p0

    return-object p0
.end method

.method public static m(Ljava/lang/Class;)Lzi$b;
    .locals 0

    .line 1
    invoke-static {p0}, Lzi;->e(Ljava/lang/Class;)Lzi$b;

    move-result-object p0

    invoke-static {p0}, Lzi$b;->a(Lzi$b;)Lzi$b;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic q(Ljava/lang/Object;Lgj;)Ljava/lang/Object;
    .locals 0

    .line 1
    return-object p0
.end method

.method public static synthetic r(Ljava/lang/Object;Lgj;)Ljava/lang/Object;
    .locals 0

    .line 1
    return-object p0
.end method

.method public static varargs s(Ljava/lang/Object;Ljava/lang/Class;[Ljava/lang/Class;)Lzi;
    .locals 0

    .line 1
    invoke-static {p1, p2}, Lzi;->f(Ljava/lang/Class;[Ljava/lang/Class;)Lzi$b;

    move-result-object p1

    new-instance p2, Lyi;

    invoke-direct {p2, p0}, Lyi;-><init>(Ljava/lang/Object;)V

    invoke-virtual {p1, p2}, Lzi$b;->f(Lmj;)Lzi$b;

    move-result-object p0

    invoke-virtual {p0}, Lzi$b;->d()Lzi;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public g()Ljava/util/Set;
    .locals 1

    .line 1
    iget-object v0, p0, Lzi;->c:Ljava/util/Set;

    return-object v0
.end method

.method public h()Lmj;
    .locals 1

    .line 1
    iget-object v0, p0, Lzi;->f:Lmj;

    return-object v0
.end method

.method public i()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lzi;->a:Ljava/lang/String;

    return-object v0
.end method

.method public j()Ljava/util/Set;
    .locals 1

    .line 1
    iget-object v0, p0, Lzi;->b:Ljava/util/Set;

    return-object v0
.end method

.method public k()Ljava/util/Set;
    .locals 1

    .line 1
    iget-object v0, p0, Lzi;->g:Ljava/util/Set;

    return-object v0
.end method

.method public n()Z
    .locals 2

    .line 1
    iget v0, p0, Lzi;->d:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method public o()Z
    .locals 2

    .line 1
    iget v0, p0, Lzi;->d:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public p()Z
    .locals 1

    .line 1
    iget v0, p0, Lzi;->e:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public t(Lmj;)Lzi;
    .locals 9

    .line 1
    new-instance v8, Lzi;

    iget-object v1, p0, Lzi;->a:Ljava/lang/String;

    iget-object v2, p0, Lzi;->b:Ljava/util/Set;

    iget-object v3, p0, Lzi;->c:Ljava/util/Set;

    iget v4, p0, Lzi;->d:I

    iget v5, p0, Lzi;->e:I

    iget-object v7, p0, Lzi;->g:Ljava/util/Set;

    move-object v0, v8

    move-object v6, p1

    invoke-direct/range {v0 .. v7}, Lzi;-><init>(Ljava/lang/String;Ljava/util/Set;Ljava/util/Set;IILmj;Ljava/util/Set;)V

    return-object v8
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Component<"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lzi;->b:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ">{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lzi;->d:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lzi;->e:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", deps="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lzi;->c:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->toArray()[Ljava/lang/Object;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

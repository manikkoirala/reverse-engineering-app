.class public final Lga$e;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lw01;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lga;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "e"
.end annotation


# static fields
.field public static final a:Lga$e;

.field public static final b:Ln00;

.field public static final c:Ln00;

.field public static final d:Ln00;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .line 1
    new-instance v0, Lga$e;

    invoke-direct {v0}, Lga$e;-><init>()V

    sput-object v0, Lga$e;->a:Lga$e;

    const-string v0, "eventType"

    invoke-static {v0}, Ln00;->d(Ljava/lang/String;)Ln00;

    move-result-object v0

    sput-object v0, Lga$e;->b:Ln00;

    const-string v0, "sessionData"

    invoke-static {v0}, Ln00;->d(Ljava/lang/String;)Ln00;

    move-result-object v0

    sput-object v0, Lga$e;->c:Ln00;

    const-string v0, "applicationInfo"

    invoke-static {v0}, Ln00;->d(Ljava/lang/String;)Ln00;

    move-result-object v0

    sput-object v0, Lga$e;->d:Ln00;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lxl1;Lx01;)V
    .locals 2

    .line 1
    sget-object v0, Lga$e;->b:Ln00;

    invoke-virtual {p1}, Lxl1;->b()Lcom/google/firebase/sessions/EventType;

    move-result-object v1

    invoke-interface {p2, v0, v1}, Lx01;->f(Ln00;Ljava/lang/Object;)Lx01;

    sget-object v0, Lga$e;->c:Ln00;

    invoke-virtual {p1}, Lxl1;->c()Lam1;

    move-result-object v1

    invoke-interface {p2, v0, v1}, Lx01;->f(Ln00;Ljava/lang/Object;)Lx01;

    sget-object v0, Lga$e;->d:Ln00;

    invoke-virtual {p1}, Lxl1;->a()Ly7;

    move-result-object p1

    invoke-interface {p2, v0, p1}, Lx01;->f(Ln00;Ljava/lang/Object;)Lx01;

    return-void
.end method

.method public bridge synthetic encode(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .line 1
    check-cast p1, Lxl1;

    check-cast p2, Lx01;

    invoke-virtual {p0, p1, p2}, Lga$e;->a(Lxl1;Lx01;)V

    return-void
.end method

.class public Lb8;
.super Liu1;
.source "SourceFile"


# static fields
.field public static volatile c:Lb8;

.field public static final d:Ljava/util/concurrent/Executor;

.field public static final e:Ljava/util/concurrent/Executor;


# instance fields
.field public a:Liu1;

.field public final b:Liu1;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .line 1
    new-instance v0, Lz7;

    invoke-direct {v0}, Lz7;-><init>()V

    sput-object v0, Lb8;->d:Ljava/util/concurrent/Executor;

    new-instance v0, La8;

    invoke-direct {v0}, La8;-><init>()V

    sput-object v0, Lb8;->e:Ljava/util/concurrent/Executor;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Liu1;-><init>()V

    new-instance v0, Ldr;

    invoke-direct {v0}, Ldr;-><init>()V

    iput-object v0, p0, Lb8;->b:Liu1;

    iput-object v0, p0, Lb8;->a:Liu1;

    return-void
.end method

.method public static synthetic d(Ljava/lang/Runnable;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lb8;->i(Ljava/lang/Runnable;)V

    return-void
.end method

.method public static synthetic e(Ljava/lang/Runnable;)V
    .locals 0

    .line 1
    invoke-static {p0}, Lb8;->h(Ljava/lang/Runnable;)V

    return-void
.end method

.method public static f()Ljava/util/concurrent/Executor;
    .locals 1

    .line 1
    sget-object v0, Lb8;->e:Ljava/util/concurrent/Executor;

    return-object v0
.end method

.method public static g()Lb8;
    .locals 2

    .line 1
    sget-object v0, Lb8;->c:Lb8;

    if-eqz v0, :cond_0

    sget-object v0, Lb8;->c:Lb8;

    return-object v0

    :cond_0
    const-class v0, Lb8;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lb8;->c:Lb8;

    if-nez v1, :cond_1

    new-instance v1, Lb8;

    invoke-direct {v1}, Lb8;-><init>()V

    sput-object v1, Lb8;->c:Lb8;

    :cond_1
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    sget-object v0, Lb8;->c:Lb8;

    return-object v0

    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method public static synthetic h(Ljava/lang/Runnable;)V
    .locals 1

    .line 1
    invoke-static {}, Lb8;->g()Lb8;

    move-result-object v0

    invoke-virtual {v0, p0}, Lb8;->c(Ljava/lang/Runnable;)V

    return-void
.end method

.method public static synthetic i(Ljava/lang/Runnable;)V
    .locals 1

    .line 1
    invoke-static {}, Lb8;->g()Lb8;

    move-result-object v0

    invoke-virtual {v0, p0}, Lb8;->a(Ljava/lang/Runnable;)V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Runnable;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lb8;->a:Liu1;

    invoke-virtual {v0, p1}, Liu1;->a(Ljava/lang/Runnable;)V

    return-void
.end method

.method public b()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lb8;->a:Liu1;

    invoke-virtual {v0}, Liu1;->b()Z

    move-result v0

    return v0
.end method

.method public c(Ljava/lang/Runnable;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lb8;->a:Liu1;

    invoke-virtual {v0, p1}, Liu1;->c(Ljava/lang/Runnable;)V

    return-void
.end method

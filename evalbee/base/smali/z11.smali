.class public Lz11;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lz11$c;,
        Lz11$b;
    }
.end annotation


# instance fields
.field public a:I

.field public b:Lz11$c;

.field public c:Ljava/util/ArrayList;

.field public d:Ljava/util/ArrayList;

.field public e:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;


# direct methods
.method public constructor <init>(ILz11$c;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;Ljava/util/ArrayList;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lz11;->a:I

    iput-object p2, p0, Lz11;->b:Lz11$c;

    iput-object p3, p0, Lz11;->e:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    iput-object p4, p0, Lz11;->d:Ljava/util/ArrayList;

    new-instance p1, Lz11$b;

    const/4 p2, 0x0

    invoke-direct {p1, p0, p2}, Lz11$b;-><init>(Lz11;Lz11$a;)V

    const/4 p2, 0x0

    new-array p2, p2, [Ljava/lang/Void;

    invoke-virtual {p1, p2}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method


# virtual methods
.method public final a(Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;I)[Lb21;
    .locals 7

    .line 1
    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getAnswerOptions()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    new-array v1, v0, [Lb21;

    add-int/lit8 p2, p2, -0x1

    const/4 v2, 0x0

    if-gez p2, :cond_0

    move p2, v2

    :cond_0
    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getAnswerKeys()[Lcom/ekodroid/omrevaluator/templateui/models/AnswerSetKey;

    move-result-object v3

    aget-object p2, v3, p2

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getLabelProfile()Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    move-result-object p1

    :goto_0
    if-ge v2, v0, :cond_3

    invoke-virtual {p2}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerSetKey;->getAnswerOptionKeys()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;

    invoke-virtual {v3}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;->getType()Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    move-result-object v4

    sget-object v5, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;->DECIMAL:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    if-eq v4, v5, :cond_2

    invoke-virtual {v3}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;->getType()Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    move-result-object v4

    sget-object v5, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;->MATRIX:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    if-ne v4, v5, :cond_1

    goto :goto_1

    :cond_1
    new-instance v4, Lb21;

    add-int/lit8 v5, v2, 0x1

    invoke-virtual {v3}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;->getMarkedValues()[Z

    move-result-object v6

    invoke-virtual {p1, v3}, Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;->getOptionLabels(Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;)[Ljava/lang/String;

    move-result-object v3

    invoke-direct {v4, v5, v6, v3}, Lb21;-><init>(I[Z[Ljava/lang/String;)V

    aput-object v4, v1, v2

    :cond_2
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_3
    return-object v1
.end method

.method public b(I)Ljava/util/ArrayList;
    .locals 9

    .line 1
    iget-object v0, p0, Lz11;->e:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    invoke-virtual {p0, v0}, Lz11;->c(Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v1, p0, Lz11;->e:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    invoke-virtual {p0, v1, p1}, Lz11;->a(Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;I)[Lb21;

    move-result-object v1

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    const/4 v3, 0x0

    if-eqz v2, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;->getExamSet()I

    move-result v4

    if-ne v4, p1, :cond_0

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;->getAnswerValue2s()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;

    invoke-virtual {v4}, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->getType()Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    move-result-object v5

    sget-object v6, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;->DECIMAL:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    if-eq v5, v6, :cond_1

    invoke-virtual {v4}, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->getType()Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    move-result-object v5

    sget-object v6, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;->MATRIX:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    if-ne v5, v6, :cond_2

    goto :goto_0

    :cond_2
    invoke-virtual {v4}, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->getMarkedValues()[Z

    move-result-object v5

    move v6, v3

    :goto_1
    array-length v7, v5

    if-ge v6, v7, :cond_1

    aget-boolean v7, v5, v6

    if-eqz v7, :cond_3

    invoke-virtual {v4}, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->getQuestionNumber()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    aget-object v7, v1, v7

    invoke-virtual {v7}, Lb21;->b()[I

    move-result-object v7

    aget v8, v7, v6

    add-int/lit8 v8, v8, 0x1

    aput v8, v7, v6

    :cond_3
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    :cond_4
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    array-length v0, v1

    :goto_2
    if-ge v3, v0, :cond_6

    aget-object v2, v1, v3

    if-eqz v2, :cond_5

    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_5
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_6
    return-object p1
.end method

.method public final c(Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)Ljava/util/ArrayList;
    .locals 3

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget-object v1, p0, Lz11;->d:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;

    invoke-virtual {v2, p1}, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;->getResultItem(Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v0
.end method

.class public abstract Lmo;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:Lzl;

.field public b:Lub0;

.field public c:Z


# direct methods
.method public constructor <init>(Lzl;Lub0;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lmo;->c:Z

    invoke-virtual {p0, p1}, Lmo;->a(Lzl;)V

    invoke-virtual {p0, p2}, Lmo;->b(Lub0;)V

    return-void
.end method


# virtual methods
.method public a(Lzl;)V
    .locals 1

    .line 1
    if-eqz p1, :cond_0

    iput-object p1, p0, Lmo;->a:Lzl;

    return-void

    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "ControlPath cannot be null."

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public b(Lub0;)V
    .locals 1

    .line 1
    if-eqz p1, :cond_0

    iput-object p1, p0, Lmo;->b:Lub0;

    return-void

    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "GroupIterator cannot be null."

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.class public Lg6$h;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lc2$a;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lg6;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "h"
.end annotation


# instance fields
.field public a:Lc2$a;

.field public final synthetic b:Lg6;


# direct methods
.method public constructor <init>(Lg6;Lc2$a;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lg6$h;->b:Lg6;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lg6$h;->a:Lc2$a;

    return-void
.end method


# virtual methods
.method public a(Lc2;Landroid/view/MenuItem;)Z
    .locals 1

    .line 1
    iget-object v0, p0, Lg6$h;->a:Lc2$a;

    invoke-interface {v0, p1, p2}, Lc2$a;->a(Lc2;Landroid/view/MenuItem;)Z

    move-result p1

    return p1
.end method

.method public b(Lc2;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lg6$h;->a:Lc2$a;

    invoke-interface {v0, p1}, Lc2$a;->b(Lc2;)V

    iget-object p1, p0, Lg6$h;->b:Lg6;

    iget-object v0, p1, Lg6;->A:Landroid/widget/PopupWindow;

    if-eqz v0, :cond_0

    iget-object p1, p1, Lg6;->l:Landroid/view/Window;

    invoke-virtual {p1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object p1

    iget-object v0, p0, Lg6$h;->b:Lg6;

    iget-object v0, v0, Lg6;->C:Ljava/lang/Runnable;

    invoke-virtual {p1, v0}, Landroid/view/View;->removeCallbacks(Ljava/lang/Runnable;)Z

    :cond_0
    iget-object p1, p0, Lg6$h;->b:Lg6;

    iget-object v0, p1, Lg6;->z:Landroidx/appcompat/widget/ActionBarContextView;

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lg6;->f0()V

    iget-object p1, p0, Lg6$h;->b:Lg6;

    iget-object v0, p1, Lg6;->z:Landroidx/appcompat/widget/ActionBarContextView;

    invoke-static {v0}, Lo32;->e(Landroid/view/View;)Li42;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Li42;->b(F)Li42;

    move-result-object v0

    iput-object v0, p1, Lg6;->D:Li42;

    iget-object p1, p0, Lg6$h;->b:Lg6;

    iget-object p1, p1, Lg6;->D:Li42;

    new-instance v0, Lg6$h$a;

    invoke-direct {v0, p0}, Lg6$h$a;-><init>(Lg6$h;)V

    invoke-virtual {p1, v0}, Li42;->h(Lk42;)Li42;

    :cond_1
    iget-object p1, p0, Lg6$h;->b:Lg6;

    iget-object v0, p1, Lg6;->n:Lz5;

    if-eqz v0, :cond_2

    iget-object p1, p1, Lg6;->y:Lc2;

    invoke-interface {v0, p1}, Lz5;->onSupportActionModeFinished(Lc2;)V

    :cond_2
    iget-object p1, p0, Lg6$h;->b:Lg6;

    const/4 v0, 0x0

    iput-object v0, p1, Lg6;->y:Lc2;

    iget-object p1, p1, Lg6;->H:Landroid/view/ViewGroup;

    invoke-static {p1}, Lo32;->n0(Landroid/view/View;)V

    iget-object p1, p0, Lg6$h;->b:Lg6;

    invoke-virtual {p1}, Lg6;->Z0()V

    return-void
.end method

.method public c(Lc2;Landroid/view/Menu;)Z
    .locals 1

    .line 1
    iget-object v0, p0, Lg6$h;->a:Lc2$a;

    invoke-interface {v0, p1, p2}, Lc2$a;->c(Lc2;Landroid/view/Menu;)Z

    move-result p1

    return p1
.end method

.method public d(Lc2;Landroid/view/Menu;)Z
    .locals 1

    .line 1
    iget-object v0, p0, Lg6$h;->b:Lg6;

    iget-object v0, v0, Lg6;->H:Landroid/view/ViewGroup;

    invoke-static {v0}, Lo32;->n0(Landroid/view/View;)V

    iget-object v0, p0, Lg6$h;->a:Lc2$a;

    invoke-interface {v0, p1, p2}, Lc2$a;->d(Lc2;Landroid/view/Menu;)Z

    move-result p1

    return p1
.end method

.class public abstract Lve1;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(Ljava/util/ArrayList;Ljava/lang/String;)V
    .locals 7

    .line 1
    new-instance v0, Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList;

    invoke-direct {v0}, Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList;-><init>()V

    sget-object v1, Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList$ResultSortBy;->MARK:Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList$ResultSortBy;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p0, v2}, Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList;->j(Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList$ResultSortBy;Ljava/util/ArrayList;Z)V

    const/4 v0, 0x1

    if-eqz p1, :cond_2

    sget-object v1, Lok;->L:Ljava/lang/String;

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    :goto_0
    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result p1

    if-ge v2, p1, :cond_5

    if-nez v2, :cond_1

    :cond_0
    :goto_1
    invoke-virtual {p0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lye1;

    invoke-virtual {p1, v0}, Lye1;->o(I)V

    goto :goto_2

    :cond_1
    invoke-virtual {p0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lye1;

    invoke-virtual {p1}, Lye1;->d()D

    move-result-wide v3

    add-int/lit8 p1, v2, -0x1

    invoke-virtual {p0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lye1;

    invoke-virtual {p1}, Lye1;->d()D

    move-result-wide v5

    cmpg-double p1, v3, v5

    if-gez p1, :cond_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    :goto_3
    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result p1

    if-ge v2, p1, :cond_5

    invoke-virtual {p0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lye1;

    if-nez v2, :cond_3

    invoke-virtual {p1, v0}, Lye1;->o(I)V

    goto :goto_4

    :cond_3
    invoke-virtual {p1}, Lye1;->d()D

    move-result-wide v3

    add-int/lit8 p1, v2, -0x1

    invoke-virtual {p0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lye1;

    invoke-virtual {v1}, Lye1;->d()D

    move-result-wide v5

    cmpg-double v1, v3, v5

    if-gez v1, :cond_4

    invoke-virtual {p0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lye1;

    add-int/lit8 v1, v2, 0x1

    invoke-virtual {p1, v1}, Lye1;->o(I)V

    goto :goto_4

    :cond_4
    invoke-virtual {p0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lye1;

    invoke-virtual {p0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lye1;

    invoke-virtual {p1}, Lye1;->f()I

    move-result p1

    invoke-virtual {v1, p1}, Lye1;->o(I)V

    :goto_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_5
    return-void
.end method

.method public static b(Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;I)Ljava/util/ArrayList;
    .locals 2

    .line 1
    const/4 v0, 0x0

    if-gez p1, :cond_0

    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getAnswerKeys()[Lcom/ekodroid/omrevaluator/templateui/models/AnswerSetKey;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getAnswerKeys()[Lcom/ekodroid/omrevaluator/templateui/models/AnswerSetKey;

    move-result-object v1

    array-length v1, v1

    if-ge v1, p1, :cond_1

    goto :goto_1

    :cond_1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getAnswerKeys()[Lcom/ekodroid/omrevaluator/templateui/models/AnswerSetKey;

    move-result-object p0

    if-nez p1, :cond_2

    const/4 p1, 0x0

    aget-object p0, p0, p1

    goto :goto_0

    :cond_2
    add-int/lit8 p1, p1, -0x1

    aget-object p0, p0, p1

    :goto_0
    if-eqz p0, :cond_3

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerSetKey;->getAnswerOptionKeys()Ljava/util/ArrayList;

    move-result-object p1

    if-eqz p1, :cond_3

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerSetKey;->getAnswerOptionKeys()Ljava/util/ArrayList;

    move-result-object p1

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result p1

    if-lez p1, :cond_3

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerSetKey;->getAnswerOptionKeys()Ljava/util/ArrayList;

    move-result-object p0

    return-object p0

    :cond_3
    :goto_1
    return-object v0
.end method

.method public static c(Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)[[I
    .locals 4

    .line 1
    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getSectionsInSubject()[I

    move-result-object p1

    array-length v0, p1

    new-array v1, v0, [[I

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_0

    aget v3, p1, v2

    new-array v3, v3, [I

    aput-object v3, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;->getAnswerValue2s()Ljava/util/ArrayList;

    move-result-object p0

    invoke-virtual {p0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_1
    :goto_1
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result p1

    if-eqz p1, :cond_2

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->getMarkedState()Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;

    move-result-object v0

    sget-object v2, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;->CORRECT:Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;

    if-ne v0, v2, :cond_1

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->getSubjectId()I

    move-result v0

    aget-object v0, v1, v0

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->getSectionId()I

    move-result p1

    aget v2, v0, p1

    add-int/lit8 v2, v2, 0x1

    aput v2, v0, p1

    goto :goto_1

    :cond_2
    return-object v1
.end method

.method public static d([D)D
    .locals 5

    .line 1
    const-wide/16 v0, 0x0

    const/4 v2, 0x0

    :goto_0
    array-length v3, p0

    if-ge v2, v3, :cond_0

    aget-wide v3, p0, v2

    add-double/2addr v0, v3

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    return-wide v0
.end method

.method public static e([I)I
    .locals 3

    .line 1
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    array-length v2, p0

    if-ge v0, v2, :cond_0

    aget v2, p0, v0

    add-int/2addr v1, v2

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return v1
.end method

.method public static f([II)I
    .locals 3

    .line 1
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v0, p1, :cond_0

    aget v2, p0, v0

    add-int/2addr v1, v2

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return v1
.end method

.method public static g([[D)[D
    .locals 9

    .line 1
    array-length v0, p0

    new-array v0, v0, [D

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    array-length v3, p0

    if-ge v2, v3, :cond_1

    move v3, v1

    :goto_1
    aget-object v4, p0, v2

    array-length v5, v4

    if-ge v3, v5, :cond_0

    aget-wide v5, v0, v2

    aget-wide v7, v4, v3

    add-double/2addr v5, v7

    aput-wide v5, v0, v2

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method public static h([[I)[I
    .locals 6

    .line 1
    array-length v0, p0

    new-array v0, v0, [I

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    array-length v3, p0

    if-ge v2, v3, :cond_1

    move v3, v1

    :goto_1
    aget-object v4, p0, v2

    array-length v5, v4

    if-ge v3, v5, :cond_0

    aget v5, v0, v2

    aget v4, v4, v3

    add-int/2addr v5, v4

    aput v5, v0, v2

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method public static i(D[Lcom/ekodroid/omrevaluator/templateui/models/GradeLevel;)Ljava/lang/String;
    .locals 5

    .line 1
    if-nez p2, :cond_0

    const/4 p0, 0x0

    return-object p0

    :cond_0
    array-length v0, p2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_2

    aget-object v2, p2, v1

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/templateui/models/GradeLevel;->getMinMarks()D

    move-result-wide v3

    cmpl-double v3, p0, v3

    if-ltz v3, :cond_1

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/templateui/models/GradeLevel;->getMaxMarks()D

    move-result-wide v3

    cmpg-double v3, p0, v3

    if-gez v3, :cond_1

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/templateui/models/GradeLevel;->getValue()Ljava/lang/String;

    move-result-object p0

    return-object p0

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    const-string p0, "NA"

    return-object p0
.end method

.method public static j(Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)[[I
    .locals 4

    .line 1
    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getSectionsInSubject()[I

    move-result-object p1

    array-length v0, p1

    new-array v1, v0, [[I

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_0

    aget v3, p1, v2

    new-array v3, v3, [I

    aput-object v3, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;->getAnswerValue2s()Ljava/util/ArrayList;

    move-result-object p0

    invoke-virtual {p0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_1
    :goto_1
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result p1

    if-eqz p1, :cond_2

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->getMarkedState()Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;

    move-result-object v0

    sget-object v2, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;->INCORRECT:Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;

    if-ne v0, v2, :cond_1

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->getSubjectId()I

    move-result v0

    aget-object v0, v1, v0

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->getSectionId()I

    move-result p1

    aget v2, v0, p1

    add-int/lit8 v2, v2, 0x1

    aput v2, v0, p1

    goto :goto_1

    :cond_2
    return-object v1
.end method

.method public static k(Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)[[D
    .locals 7

    .line 1
    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getSectionsInSubject()[I

    move-result-object p1

    array-length v0, p1

    new-array v1, v0, [[D

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_0

    aget v3, p1, v2

    new-array v3, v3, [D

    aput-object v3, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;->getAnswerValue2s()Ljava/util/ArrayList;

    move-result-object p0

    invoke-virtual {p0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_1
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result p1

    if-eqz p1, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->getSubjectId()I

    move-result v0

    aget-object v0, v1, v0

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->getSectionId()I

    move-result v2

    aget-wide v3, v0, v2

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->getMarksForAnswer()D

    move-result-wide v5

    add-double/2addr v3, v5

    aput-wide v3, v0, v2

    goto :goto_1

    :cond_1
    return-object v1
.end method

.method public static l(Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)D
    .locals 2

    .line 1
    invoke-static {p0}, Lve1;->r(Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)[[D

    move-result-object p0

    invoke-static {p0}, Lve1;->g([[D)[D

    move-result-object p0

    invoke-static {p0}, Lve1;->d([D)D

    move-result-wide v0

    invoke-static {v0, v1}, Lve1;->o(D)D

    move-result-wide v0

    return-wide v0
.end method

.method public static m([D[D)[D
    .locals 7

    .line 1
    array-length v0, p0

    new-array v1, v0, [D

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_0

    aget-wide v3, p0, v2

    const-wide/high16 v5, 0x4059000000000000L    # 100.0

    mul-double/2addr v3, v5

    aget-wide v5, p1, v2

    div-double/2addr v3, v5

    aput-wide v3, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method public static n([[D[[D)[[D
    .locals 11

    .line 1
    array-length v0, p0

    new-array v1, v0, [[D

    const/4 v2, 0x0

    move v3, v2

    :goto_0
    if-ge v3, v0, :cond_0

    aget-object v4, p0, v3

    array-length v4, v4

    new-array v4, v4, [D

    aput-object v4, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_0
    move v3, v2

    :goto_1
    if-ge v3, v0, :cond_2

    move v4, v2

    :goto_2
    aget-object v5, v1, v3

    array-length v6, v5

    if-ge v4, v6, :cond_1

    aget-object v6, p0, v3

    aget-wide v7, v6, v4

    const-wide/high16 v9, 0x4059000000000000L    # 100.0

    mul-double/2addr v7, v9

    aget-object v6, p1, v3

    aget-wide v9, v6, v4

    div-double/2addr v7, v9

    aput-wide v7, v5, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_2
    return-object v1
.end method

.method public static o(D)D
    .locals 2

    .line 1
    const-wide v0, 0x408f400000000000L    # 1000.0

    mul-double/2addr p0, v0

    invoke-static {p0, p1}, Ljava/lang/Math;->round(D)J

    move-result-wide p0

    long-to-double p0, p0

    div-double/2addr p0, v0

    return-wide p0
.end method

.method public static p(Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;)I
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;->getAnswerValue2s()Ljava/util/ArrayList;

    move-result-object p0

    invoke-virtual {p0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p0

    const/4 v0, 0x0

    :cond_0
    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->getMarkedState()Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;

    move-result-object v1

    sget-object v2, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;->CORRECT:Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;

    if-ne v1, v2, :cond_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return v0
.end method

.method public static q(Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;)I
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;->getAnswerValue2s()Ljava/util/ArrayList;

    move-result-object p0

    invoke-virtual {p0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p0

    const/4 v0, 0x0

    :cond_0
    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->getMarkedState()Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;

    move-result-object v1

    sget-object v2, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;->INCORRECT:Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;

    if-ne v1, v2, :cond_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return v0
.end method

.method public static r(Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)[[D
    .locals 11

    .line 1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getTemplateParams()Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;

    move-result-object v0

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;->getSubjects()[Lcom/ekodroid/omrevaluator/templateui/models/Subject2;

    move-result-object v0

    array-length v1, v0

    new-array v2, v1, [[D

    const/4 v3, 0x0

    move v4, v3

    :goto_0
    if-ge v4, v1, :cond_0

    aget-object v5, v0, v4

    invoke-virtual {v5}, Lcom/ekodroid/omrevaluator/templateui/models/Subject2;->getSections()[Lcom/ekodroid/omrevaluator/templateui/models/Section2;

    move-result-object v5

    array-length v5, v5

    new-array v5, v5, [D

    aput-object v5, v2, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x1

    :goto_1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getAnswerOptions()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v1, v4, :cond_1

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getAnswerOptions()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;

    iget v5, v4, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->subjectId:I

    aget-object v5, v2, v5

    iget v6, v4, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->sectionId:I

    aget-wide v7, v5, v6

    iget-wide v9, v4, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->correctMarks:D

    add-double/2addr v7, v9

    aput-wide v7, v5, v6

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_1
    move p0, v3

    :goto_2
    array-length v1, v0

    if-ge p0, v1, :cond_4

    aget-object v1, v0, p0

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/templateui/models/Subject2;->getSections()[Lcom/ekodroid/omrevaluator/templateui/models/Section2;

    move-result-object v1

    move v4, v3

    :goto_3
    array-length v5, v1

    if-ge v4, v5, :cond_3

    aget-object v5, v1, v4

    invoke-virtual {v5}, Lcom/ekodroid/omrevaluator/templateui/models/Section2;->isOptionalAllowed()Z

    move-result v5

    if-eqz v5, :cond_2

    aget-object v5, v2, p0

    aget-object v6, v1, v4

    invoke-virtual {v6}, Lcom/ekodroid/omrevaluator/templateui/models/Section2;->getPositiveMark()D

    move-result-wide v6

    aget-object v8, v1, v4

    invoke-virtual {v8}, Lcom/ekodroid/omrevaluator/templateui/models/Section2;->getNoOfOptionalQue()I

    move-result v8

    int-to-double v8, v8

    mul-double/2addr v6, v8

    aput-wide v6, v5, v4

    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    :cond_3
    add-int/lit8 p0, p0, 0x1

    goto :goto_2

    :cond_4
    return-object v2
.end method

.method public static s(Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;)I
    .locals 3

    .line 1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;->getAnswerValue2s()Ljava/util/ArrayList;

    move-result-object p0

    invoke-virtual {p0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p0

    const/4 v0, 0x0

    :cond_0
    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->getMarkedState()Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;

    move-result-object v1

    sget-object v2, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;->UNATTEMPTED:Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;

    if-ne v1, v2, :cond_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return v0
.end method

.method public static t(Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;)V
    .locals 11

    .line 1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->getType()Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    move-result-object v0

    sget-object v1, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;->DECIMAL:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    if-ne v0, v1, :cond_0

    invoke-static {p0, p1, p2}, Lve1;->u(Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;)V

    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->getMarkedValues()[Z

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    move v3, v2

    move v4, v3

    :goto_0
    if-ge v3, v1, :cond_1

    aget-boolean v5, v0, v3

    or-int/2addr v4, v5

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    const-wide/16 v5, 0x0

    if-nez v4, :cond_2

    sget-object v1, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;->UNATTEMPTED:Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;

    invoke-virtual {p0, v1}, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->setMarkedState(Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;)V

    invoke-virtual {p0, v5, v6}, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->setMarksForAnswer(D)V

    :cond_2
    if-eqz p1, :cond_17

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;->getMarkedValues()[Z

    move-result-object v1

    if-nez v1, :cond_3

    goto/16 :goto_a

    :cond_3
    if-eqz v4, :cond_4

    sget-object v1, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;->CORRECT:Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;

    invoke-virtual {p0, v1}, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->setMarkedState(Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;)V

    iget-wide v5, p2, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->correctMarks:D

    invoke-virtual {p0, v5, v6}, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->setMarksForAnswer(D)V

    :cond_4
    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;->getMarkedValues()[Z

    move-result-object v1

    move v3, v2

    :goto_1
    array-length v5, v0

    if-ge v3, v5, :cond_6

    aget-boolean v5, v0, v3

    aget-boolean v6, v1, v3

    if-eq v5, v6, :cond_5

    if-eqz v4, :cond_5

    sget-object v1, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;->INCORRECT:Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;

    invoke-virtual {p0, v1}, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->setMarkedState(Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;)V

    iget-wide v3, p2, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->incorrectMarks:D

    invoke-virtual {p0, v3, v4}, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->setMarksForAnswer(D)V

    goto :goto_2

    :cond_5
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_6
    :goto_2
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->getMarkedState()Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;

    move-result-object v1

    sget-object v3, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;->CORRECT:Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;

    if-eq v1, v3, :cond_16

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;->getPartialAnswerOptionKeys()Ljava/util/ArrayList;

    move-result-object v1

    if-eqz v1, :cond_16

    iget-boolean p2, p2, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->partialAllowed:Z

    if-eqz p2, :cond_16

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;->getPartialAnswerOptionKeys()Ljava/util/ArrayList;

    move-result-object p1

    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_7
    :goto_3
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_16

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/ekodroid/omrevaluator/templateui/models/PartialAnswerOptionKey;

    if-eqz p2, :cond_16

    invoke-virtual {p2}, Lcom/ekodroid/omrevaluator/templateui/models/PartialAnswerOptionKey;->getMarkedValues()[Z

    move-result-object v1

    if-nez v1, :cond_8

    goto/16 :goto_9

    :cond_8
    invoke-virtual {p2}, Lcom/ekodroid/omrevaluator/templateui/models/PartialAnswerOptionKey;->getMarkedValues()[Z

    move-result-object v1

    invoke-virtual {p2}, Lcom/ekodroid/omrevaluator/templateui/models/PartialAnswerOptionKey;->getPartialType()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_c

    invoke-virtual {p2}, Lcom/ekodroid/omrevaluator/templateui/models/PartialAnswerOptionKey;->getPartialType()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lok;->O:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_c

    move v3, v2

    move v4, v3

    :goto_4
    array-length v5, v0

    if-ge v3, v5, :cond_b

    aget-boolean v5, v0, v3

    if-eqz v5, :cond_9

    aget-boolean v6, v1, v3

    if-eqz v6, :cond_9

    add-int/lit8 v4, v4, 0x1

    :cond_9
    if-eqz v5, :cond_a

    aget-boolean v5, v1, v3

    if-nez v5, :cond_a

    return-void

    :cond_a
    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    :cond_b
    if-lez v4, :cond_7

    sget-object v1, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;->PARTIAL_CORRECT:Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;

    invoke-virtual {p0, v1}, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->setMarkedState(Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;)V

    invoke-virtual {p2}, Lcom/ekodroid/omrevaluator/templateui/models/PartialAnswerOptionKey;->getPartialMarks()D

    move-result-wide v5

    int-to-double v3, v4

    mul-double/2addr v5, v3

    invoke-virtual {p0, v5, v6}, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->setMarksForAnswer(D)V

    goto :goto_3

    :cond_c
    invoke-virtual {p2}, Lcom/ekodroid/omrevaluator/templateui/models/PartialAnswerOptionKey;->getPartialType()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    if-eqz v3, :cond_13

    invoke-virtual {p2}, Lcom/ekodroid/omrevaluator/templateui/models/PartialAnswerOptionKey;->getPartialType()Ljava/lang/String;

    move-result-object v3

    sget-object v5, Lok;->P:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_13

    invoke-virtual {p2}, Lcom/ekodroid/omrevaluator/templateui/models/PartialAnswerOptionKey;->getType()Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    move-result-object v3

    sget-object v5, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;->MATRIX:Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption$AnswerOptionType;

    if-ne v3, v5, :cond_13

    invoke-virtual {p2}, Lcom/ekodroid/omrevaluator/templateui/models/PartialAnswerOptionKey;->getPayload()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lbc;->c(Ljava/lang/String;)Lcom/ekodroid/omrevaluator/templateui/models/MatrixOptionPayload;

    move-result-object p1

    if-eqz p1, :cond_d

    iget v3, p1, Lcom/ekodroid/omrevaluator/templateui/models/MatrixOptionPayload;->primaryOptions:I

    iget p1, p1, Lcom/ekodroid/omrevaluator/templateui/models/MatrixOptionPayload;->secondaryOptions:I

    goto :goto_5

    :cond_d
    const/4 v3, 0x4

    const/4 p1, 0x5

    :goto_5
    move v5, v2

    move v6, v5

    :goto_6
    if-ge v5, v3, :cond_11

    move v7, v2

    move v8, v4

    :goto_7
    if-ge v7, p1, :cond_f

    mul-int v9, v5, p1

    add-int/2addr v9, v7

    aget-boolean v10, v0, v9

    aget-boolean v9, v1, v9

    if-eq v10, v9, :cond_e

    move v8, v2

    :cond_e
    add-int/lit8 v7, v7, 0x1

    goto :goto_7

    :cond_f
    if-eqz v8, :cond_10

    add-int/lit8 v6, v6, 0x1

    :cond_10
    add-int/lit8 v5, v5, 0x1

    goto :goto_6

    :cond_11
    if-lez v6, :cond_12

    sget-object p1, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;->PARTIAL_CORRECT:Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;

    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->setMarkedState(Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;)V

    invoke-virtual {p2}, Lcom/ekodroid/omrevaluator/templateui/models/PartialAnswerOptionKey;->getPartialMarks()D

    move-result-wide p1

    int-to-double v0, v6

    mul-double/2addr p1, v0

    invoke-virtual {p0, p1, p2}, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->setMarksForAnswer(D)V

    :cond_12
    return-void

    :cond_13
    move v3, v2

    :goto_8
    array-length v5, v0

    if-ge v3, v5, :cond_15

    aget-boolean v5, v0, v3

    aget-boolean v6, v1, v3

    if-eq v5, v6, :cond_14

    move v4, v2

    :cond_14
    add-int/lit8 v3, v3, 0x1

    goto :goto_8

    :cond_15
    if-eqz v4, :cond_7

    sget-object p1, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;->PARTIAL_CORRECT:Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;

    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->setMarkedState(Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;)V

    invoke-virtual {p2}, Lcom/ekodroid/omrevaluator/templateui/models/PartialAnswerOptionKey;->getPartialMarks()D

    move-result-wide p1

    invoke-virtual {p0, p1, p2}, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->setMarksForAnswer(D)V

    :cond_16
    :goto_9
    return-void

    :cond_17
    :goto_a
    if-eqz v4, :cond_18

    sget-object p1, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;->ATTEMPTED:Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;

    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->setMarkedState(Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;)V

    invoke-virtual {p0, v5, v6}, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->setMarksForAnswer(D)V

    :cond_18
    return-void
.end method

.method public static u(Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;)V
    .locals 7

    .line 1
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->getMarkedValues()[Z

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    move v3, v2

    :goto_0
    if-ge v2, v1, :cond_0

    aget-boolean v4, v0, v2

    or-int/2addr v3, v4

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    const-wide/16 v0, 0x0

    if-nez v3, :cond_1

    sget-object p1, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;->UNATTEMPTED:Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;

    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->setMarkedState(Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;)V

    invoke-virtual {p0, v0, v1}, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->setMarksForAnswer(D)V

    return-void

    :cond_1
    if-eqz p1, :cond_7

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;->getMarkedPayload()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_2

    goto/16 :goto_2

    :cond_2
    invoke-static {p0}, Le5;->b(Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;)D

    move-result-wide v0

    new-instance v2, Lgc0;

    invoke-direct {v2}, Lgc0;-><init>()V

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;->getMarkedPayload()Ljava/lang/String;

    move-result-object v3

    const-class v4, Lcom/ekodroid/omrevaluator/templateui/models/DecimalRange;

    invoke-virtual {v2, v3, v4}, Lgc0;->j(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/ekodroid/omrevaluator/templateui/models/DecimalRange;

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/templateui/models/DecimalRange;->getLower()D

    move-result-wide v5

    cmpl-double v3, v0, v5

    if-ltz v3, :cond_3

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/templateui/models/DecimalRange;->getUpper()D

    move-result-wide v2

    cmpg-double v2, v0, v2

    if-gtz v2, :cond_3

    sget-object p1, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;->CORRECT:Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;

    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->setMarkedState(Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;)V

    iget-wide p1, p2, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->correctMarks:D

    invoke-virtual {p0, p1, p2}, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->setMarksForAnswer(D)V

    return-void

    :cond_3
    sget-object v2, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;->INCORRECT:Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;

    invoke-virtual {p0, v2}, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->setMarkedState(Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;)V

    iget-wide v2, p2, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->incorrectMarks:D

    invoke-virtual {p0, v2, v3}, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->setMarksForAnswer(D)V

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->getMarkedState()Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;

    move-result-object v2

    sget-object v3, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;->CORRECT:Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;

    if-eq v2, v3, :cond_6

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;->getPartialAnswerOptionKeys()Ljava/util/ArrayList;

    move-result-object v2

    if-eqz v2, :cond_6

    iget-boolean p2, p2, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->partialAllowed:Z

    if-eqz p2, :cond_6

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;->getPartialAnswerOptionKeys()Ljava/util/ArrayList;

    move-result-object p1

    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_4
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_6

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lcom/ekodroid/omrevaluator/templateui/models/PartialAnswerOptionKey;

    if-eqz p2, :cond_6

    invoke-virtual {p2}, Lcom/ekodroid/omrevaluator/templateui/models/PartialAnswerOptionKey;->getMarkedPayload()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_5

    goto :goto_1

    :cond_5
    new-instance v2, Lgc0;

    invoke-direct {v2}, Lgc0;-><init>()V

    invoke-virtual {p2}, Lcom/ekodroid/omrevaluator/templateui/models/PartialAnswerOptionKey;->getMarkedPayload()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v4}, Lgc0;->j(Ljava/lang/String;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/ekodroid/omrevaluator/templateui/models/DecimalRange;

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/templateui/models/DecimalRange;->getLower()D

    move-result-wide v5

    cmpl-double v3, v0, v5

    if-ltz v3, :cond_4

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/templateui/models/DecimalRange;->getUpper()D

    move-result-wide v2

    cmpg-double v2, v0, v2

    if-gtz v2, :cond_4

    sget-object p1, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;->PARTIAL_CORRECT:Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;

    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->setMarkedState(Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;)V

    invoke-virtual {p2}, Lcom/ekodroid/omrevaluator/templateui/models/PartialAnswerOptionKey;->getPartialMarks()D

    move-result-wide p1

    invoke-virtual {p0, p1, p2}, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->setMarksForAnswer(D)V

    :cond_6
    :goto_1
    return-void

    :cond_7
    :goto_2
    sget-object p1, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;->ATTEMPTED:Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;

    invoke-virtual {p0, p1}, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->setMarkedState(Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;)V

    invoke-virtual {p0, v0, v1}, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->setMarksForAnswer(D)V

    return-void
.end method

.method public static v(IILcom/ekodroid/omrevaluator/templateui/models/Section2;Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;)V
    .locals 5

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p3}, Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;->getAnswerValue2s()Ljava/util/ArrayList;

    move-result-object p3

    invoke-virtual {p3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p3

    const/4 v1, 0x0

    :cond_0
    :goto_0
    invoke-interface {p3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {p3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->getSubjectId()I

    move-result v3

    if-ne v3, p0, :cond_0

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->getSectionId()I

    move-result v3

    if-ne v3, p1, :cond_0

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->getMarkedState()Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;

    move-result-object v3

    sget-object v4, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;->UNATTEMPTED:Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;

    if-ne v3, v4, :cond_1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    invoke-virtual {p2}, Lcom/ekodroid/omrevaluator/templateui/models/Section2;->getNoOfQue()I

    move-result p0

    invoke-virtual {p2}, Lcom/ekodroid/omrevaluator/templateui/models/Section2;->getNoOfOptionalQue()I

    move-result p1

    sub-int/2addr p0, p1

    if-lt v1, p0, :cond_3

    return-void

    :cond_3
    invoke-virtual {p2}, Lcom/ekodroid/omrevaluator/templateui/models/Section2;->getOptionalType()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/String;->hashCode()I

    const-string p1, "Max Marks"

    invoke-virtual {p0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_5

    const-string p1, "Min Marks"

    invoke-virtual {p0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-nez p0, :cond_4

    goto :goto_2

    :cond_4
    new-instance p0, Lve1$b;

    invoke-direct {p0}, Lve1$b;-><init>()V

    goto :goto_1

    :cond_5
    new-instance p0, Lve1$a;

    invoke-direct {p0}, Lve1$a;-><init>()V

    :goto_1
    invoke-static {v0, p0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    :goto_2
    invoke-virtual {p2}, Lcom/ekodroid/omrevaluator/templateui/models/Section2;->getNoOfOptionalQue()I

    move-result p0

    :goto_3
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result p1

    if-ge p0, p1, :cond_6

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;

    sget-object p2, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;->INVALID:Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;

    invoke-virtual {p1, p2}, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->setMarkedState(Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;)V

    const-wide/16 p2, 0x0

    invoke-virtual {p1, p2, p3}, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->setMarksForAnswer(D)V

    add-int/lit8 p0, p0, 0x1

    goto :goto_3

    :cond_6
    return-void
.end method

.method public static w(Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)V
    .locals 11

    .line 1
    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getAnswerOptions()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;->getAnswerValue2s()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;->getExamSet()I

    move-result v3

    invoke-static {p1, v3}, Lve1;->b(Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;I)Ljava/util/ArrayList;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->getQuestionNumber()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;

    goto :goto_1

    :cond_0
    const/4 v3, 0x0

    :goto_1
    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->getQuestionNumber()I

    move-result v4

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;

    invoke-static {v2, v3, v4}, Lve1;->t(Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;Lcom/ekodroid/omrevaluator/templateui/models/AnswerOptionKey;Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;)V

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;->getAnswerValue2s()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-le v1, v2, :cond_2

    return-void

    :cond_2
    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;->getExamSet()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    const/4 v2, 0x0

    if-gez v1, :cond_3

    move v1, v2

    :cond_3
    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getInvalidQuestionSets()[Lcom/ekodroid/omrevaluator/templateui/models/InvalidQuestionSet;

    move-result-object v3

    if-eqz v3, :cond_6

    array-length v4, v3

    if-le v4, v1, :cond_6

    aget-object v4, v3, v1

    invoke-virtual {v4}, Lcom/ekodroid/omrevaluator/templateui/models/InvalidQuestionSet;->getInvalidQuestions()[I

    move-result-object v4

    if-eqz v4, :cond_6

    array-length v5, v4

    move v6, v2

    :goto_2
    if-ge v6, v5, :cond_6

    aget v7, v4, v6

    invoke-virtual {p0}, Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;->getAnswerValue2s()Ljava/util/ArrayList;

    move-result-object v8

    add-int/lit8 v9, v7, -0x1

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;

    sget-object v9, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;->INVALID:Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;

    invoke-virtual {v8, v9}, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->setMarkedState(Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;)V

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;

    aget-object v9, v3, v1

    invoke-virtual {v9}, Lcom/ekodroid/omrevaluator/templateui/models/InvalidQuestionSet;->getInvalidQueMarks()Ljava/lang/String;

    move-result-object v9

    if-eqz v9, :cond_5

    aget-object v9, v3, v1

    invoke-virtual {v9}, Lcom/ekodroid/omrevaluator/templateui/models/InvalidQuestionSet;->getInvalidQueMarks()Ljava/lang/String;

    move-result-object v9

    const-string v10, "Max"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_4

    goto :goto_3

    :cond_4
    const-wide/16 v9, 0x0

    goto :goto_4

    :cond_5
    :goto_3
    iget-wide v9, v7, Lcom/ekodroid/omrevaluator/templateui/models/AnswerOption;->correctMarks:D

    :goto_4
    invoke-virtual {v8, v9, v10}, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->setMarksForAnswer(D)V

    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    :cond_6
    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getTemplateParams()Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;

    move-result-object p1

    invoke-virtual {p1}, Lcom/ekodroid/omrevaluator/templateui/models/TemplateParams2;->getSubjects()[Lcom/ekodroid/omrevaluator/templateui/models/Subject2;

    move-result-object p1

    move v0, v2

    :goto_5
    array-length v1, p1

    if-ge v0, v1, :cond_9

    aget-object v1, p1, v0

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/templateui/models/Subject2;->getSections()[Lcom/ekodroid/omrevaluator/templateui/models/Section2;

    move-result-object v1

    move v3, v2

    :goto_6
    array-length v4, v1

    if-ge v3, v4, :cond_8

    aget-object v4, v1, v3

    invoke-virtual {v4}, Lcom/ekodroid/omrevaluator/templateui/models/Section2;->isOptionalAllowed()Z

    move-result v5

    if-eqz v5, :cond_7

    invoke-static {v0, v3, v4, p0}, Lve1;->v(IILcom/ekodroid/omrevaluator/templateui/models/Section2;Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;)V

    :cond_7
    add-int/lit8 v3, v3, 0x1

    goto :goto_6

    :cond_8
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    :cond_9
    return-void
.end method

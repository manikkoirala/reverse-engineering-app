.class public final Lkh$a;
.super Lhz1;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lkh;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "a"
.end annotation


# instance fields
.field public final a:Lhz1;

.field public final b:Lu01;


# direct methods
.method public constructor <init>(Lgc0;Ljava/lang/reflect/Type;Lhz1;Lu01;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Lhz1;-><init>()V

    new-instance v0, Ljz1;

    invoke-direct {v0, p1, p3, p2}, Ljz1;-><init>(Lgc0;Lhz1;Ljava/lang/reflect/Type;)V

    iput-object v0, p0, Lkh$a;->a:Lhz1;

    iput-object p4, p0, Lkh$a;->b:Lu01;

    return-void
.end method


# virtual methods
.method public bridge synthetic b(Lrh0;)Ljava/lang/Object;
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lkh$a;->e(Lrh0;)Ljava/util/Collection;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic d(Lvh0;Ljava/lang/Object;)V
    .locals 0

    .line 1
    check-cast p2, Ljava/util/Collection;

    invoke-virtual {p0, p1, p2}, Lkh$a;->f(Lvh0;Ljava/util/Collection;)V

    return-void
.end method

.method public e(Lrh0;)Ljava/util/Collection;
    .locals 2

    .line 1
    invoke-virtual {p1}, Lrh0;->o0()Lcom/google/gson/stream/JsonToken;

    move-result-object v0

    sget-object v1, Lcom/google/gson/stream/JsonToken;->NULL:Lcom/google/gson/stream/JsonToken;

    if-ne v0, v1, :cond_0

    invoke-virtual {p1}, Lrh0;->R()V

    const/4 p1, 0x0

    return-object p1

    :cond_0
    iget-object v0, p0, Lkh$a;->b:Lu01;

    invoke-interface {v0}, Lu01;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-virtual {p1}, Lrh0;->a()V

    :goto_0
    invoke-virtual {p1}, Lrh0;->k()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lkh$a;->a:Lhz1;

    invoke-virtual {v1, p1}, Lhz1;->b(Lrh0;)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Lrh0;->f()V

    return-object v0
.end method

.method public f(Lvh0;Ljava/util/Collection;)V
    .locals 2

    .line 1
    if-nez p2, :cond_0

    invoke-virtual {p1}, Lvh0;->u()Lvh0;

    return-void

    :cond_0
    invoke-virtual {p1}, Lvh0;->c()Lvh0;

    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, Lkh$a;->a:Lhz1;

    invoke-virtual {v1, p1, v0}, Lhz1;->d(Lvh0;Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Lvh0;->f()Lvh0;

    return-void
.end method

.class public Lzm$b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lzm;->I(Lzm1;Ljava/lang/Thread;Ljava/lang/Throwable;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:J

.field public final synthetic b:Ljava/lang/Throwable;

.field public final synthetic c:Ljava/lang/Thread;

.field public final synthetic d:Lzm1;

.field public final synthetic e:Z

.field public final synthetic f:Lzm;


# direct methods
.method public constructor <init>(Lzm;JLjava/lang/Throwable;Ljava/lang/Thread;Lzm1;Z)V
    .locals 0

    .line 1
    iput-object p1, p0, Lzm$b;->f:Lzm;

    iput-wide p2, p0, Lzm$b;->a:J

    iput-object p4, p0, Lzm$b;->b:Ljava/lang/Throwable;

    iput-object p5, p0, Lzm$b;->c:Ljava/lang/Thread;

    iput-object p6, p0, Lzm$b;->d:Lzm1;

    iput-boolean p7, p0, Lzm$b;->e:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Lcom/google/android/gms/tasks/Task;
    .locals 8

    .line 1
    iget-wide v0, p0, Lzm$b;->a:J

    invoke-static {v0, v1}, Lzm;->b(J)J

    move-result-wide v6

    iget-object v0, p0, Lzm$b;->f:Lzm;

    invoke-static {v0}, Lzm;->c(Lzm;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    invoke-static {}, Lzl0;->f()Lzl0;

    move-result-object v0

    const-string v2, "Tried to write a fatal exception while no session was open."

    invoke-virtual {v0, v2}, Lzl0;->d(Ljava/lang/String;)V

    invoke-static {v1}, Lcom/google/android/gms/tasks/Tasks;->forResult(Ljava/lang/Object;)Lcom/google/android/gms/tasks/Task;

    move-result-object v0

    return-object v0

    :cond_0
    iget-object v2, p0, Lzm$b;->f:Lzm;

    invoke-static {v2}, Lzm;->g(Lzm;)Lcn;

    move-result-object v2

    invoke-virtual {v2}, Lcn;->a()Z

    iget-object v2, p0, Lzm$b;->f:Lzm;

    invoke-static {v2}, Lzm;->h(Lzm;)Lnm1;

    move-result-object v2

    iget-object v3, p0, Lzm$b;->b:Ljava/lang/Throwable;

    iget-object v4, p0, Lzm$b;->c:Ljava/lang/Thread;

    move-object v5, v0

    invoke-virtual/range {v2 .. v7}, Lnm1;->t(Ljava/lang/Throwable;Ljava/lang/Thread;Ljava/lang/String;J)V

    iget-object v2, p0, Lzm$b;->f:Lzm;

    iget-wide v3, p0, Lzm$b;->a:J

    invoke-static {v2, v3, v4}, Lzm;->i(Lzm;J)V

    iget-object v2, p0, Lzm$b;->f:Lzm;

    iget-object v3, p0, Lzm$b;->d:Lzm1;

    invoke-virtual {v2, v3}, Lzm;->t(Lzm1;)V

    iget-object v2, p0, Lzm$b;->f:Lzm;

    new-instance v3, Lvd;

    iget-object v4, p0, Lzm$b;->f:Lzm;

    invoke-static {v4}, Lzm;->j(Lzm;)Lde0;

    move-result-object v4

    invoke-direct {v3, v4}, Lvd;-><init>(Lde0;)V

    invoke-virtual {v3}, Lvd;->toString()Ljava/lang/String;

    move-result-object v3

    iget-boolean v4, p0, Lzm$b;->e:Z

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lzm;->k(Lzm;Ljava/lang/String;Ljava/lang/Boolean;)V

    iget-object v2, p0, Lzm$b;->f:Lzm;

    invoke-static {v2}, Lzm;->l(Lzm;)Ldp;

    move-result-object v2

    invoke-virtual {v2}, Ldp;->d()Z

    move-result v2

    if-nez v2, :cond_1

    invoke-static {v1}, Lcom/google/android/gms/tasks/Tasks;->forResult(Ljava/lang/Object;)Lcom/google/android/gms/tasks/Task;

    move-result-object v0

    return-object v0

    :cond_1
    iget-object v1, p0, Lzm$b;->f:Lzm;

    invoke-static {v1}, Lzm;->m(Lzm;)Lxm;

    move-result-object v1

    invoke-virtual {v1}, Lxm;->c()Ljava/util/concurrent/Executor;

    move-result-object v1

    iget-object v2, p0, Lzm$b;->d:Lzm1;

    invoke-interface {v2}, Lzm1;->b()Lcom/google/android/gms/tasks/Task;

    move-result-object v2

    new-instance v3, Lzm$b$a;

    invoke-direct {v3, p0, v1, v0}, Lzm$b$a;-><init>(Lzm$b;Ljava/util/concurrent/Executor;Ljava/lang/String;)V

    invoke-virtual {v2, v1, v3}, Lcom/google/android/gms/tasks/Task;->onSuccessTask(Ljava/util/concurrent/Executor;Lcom/google/android/gms/tasks/SuccessContinuation;)Lcom/google/android/gms/tasks/Task;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic call()Ljava/lang/Object;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lzm$b;->a()Lcom/google/android/gms/tasks/Task;

    move-result-object v0

    return-object v0
.end method

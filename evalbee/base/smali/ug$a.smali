.class public Lug$a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lug;->m()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation


# instance fields
.field public final synthetic a:Lug$c;

.field public final synthetic b:Lug;


# direct methods
.method public constructor <init>(Lug;Lug$c;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lug$a;->b:Lug;

    iput-object p2, p0, Lug$a;->a:Lug$c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 3

    .line 1
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Float;

    invoke-virtual {p1}, Ljava/lang/Float;->floatValue()F

    move-result p1

    iget-object v0, p0, Lug$a;->b:Lug;

    iget-object v1, p0, Lug$a;->a:Lug$c;

    invoke-virtual {v0, p1, v1}, Lug;->n(FLug$c;)V

    iget-object v0, p0, Lug$a;->b:Lug;

    iget-object v1, p0, Lug$a;->a:Lug$c;

    const/4 v2, 0x0

    invoke-virtual {v0, p1, v1, v2}, Lug;->b(FLug$c;Z)V

    iget-object p1, p0, Lug$a;->b:Lug;

    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->invalidateSelf()V

    return-void
.end method

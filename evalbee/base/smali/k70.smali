.class public abstract Lk70;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lk70$c;,
        Lk70$a;,
        Lk70$b;
    }
.end annotation


# direct methods
.method public static a(Landroid/content/Context;Landroid/os/CancellationSignal;[Lk70$b;)Landroid/graphics/Typeface;
    .locals 1

    .line 1
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, v0}, Lsz1;->b(Landroid/content/Context;Landroid/os/CancellationSignal;[Lk70$b;I)Landroid/graphics/Typeface;

    move-result-object p0

    return-object p0
.end method

.method public static b(Landroid/content/Context;Landroid/os/CancellationSignal;Lg70;)Lk70$a;
    .locals 0

    .line 1
    invoke-static {p0, p2, p1}, Lf70;->e(Landroid/content/Context;Lg70;Landroid/os/CancellationSignal;)Lk70$a;

    move-result-object p0

    return-object p0
.end method

.method public static c(Landroid/content/Context;Lg70;IZILandroid/os/Handler;Lk70$c;)Landroid/graphics/Typeface;
    .locals 1

    .line 1
    new-instance v0, Lre;

    invoke-direct {v0, p6, p5}, Lre;-><init>(Lk70$c;Landroid/os/Handler;)V

    if-eqz p3, :cond_0

    invoke-static {p0, p1, v0, p2, p4}, Li70;->e(Landroid/content/Context;Lg70;Lre;II)Landroid/graphics/Typeface;

    move-result-object p0

    return-object p0

    :cond_0
    const/4 p3, 0x0

    invoke-static {p0, p1, p2, p3, v0}, Li70;->d(Landroid/content/Context;Lg70;ILjava/util/concurrent/Executor;Lre;)Landroid/graphics/Typeface;

    move-result-object p0

    return-object p0
.end method

.class public Lf5;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lf5$b;,
        Lf5$c;
    }
.end annotation


# instance fields
.field public a:I

.field public b:Lf5$b;

.field public c:Ljava/util/ArrayList;

.field public d:Ljava/util/ArrayList;

.field public e:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;


# direct methods
.method public constructor <init>(ILf5$b;Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;Ljava/util/ArrayList;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p3, p0, Lf5;->e:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    iput-object p4, p0, Lf5;->d:Ljava/util/ArrayList;

    iput p1, p0, Lf5;->a:I

    iput-object p2, p0, Lf5;->b:Lf5$b;

    new-instance p1, Lf5$c;

    const/4 p2, 0x0

    invoke-direct {p1, p0, p2}, Lf5$c;-><init>(Lf5;Lf5$a;)V

    const/4 p2, 0x0

    new-array p2, p2, [Ljava/lang/Void;

    invoke-virtual {p1, p2}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method


# virtual methods
.method public final a(I)[Lh5;
    .locals 4

    .line 1
    new-array v0, p1, [Lh5;

    const/4 v1, 0x0

    :goto_0
    if-ge v1, p1, :cond_0

    new-instance v2, Lh5;

    add-int/lit8 v3, v1, 0x1

    invoke-direct {v2, v3}, Lh5;-><init>(I)V

    aput-object v2, v0, v1

    move v1, v3

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method public b(I)Ljava/util/ArrayList;
    .locals 7

    .line 1
    iget-object v0, p0, Lf5;->e:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getAnswerKeys()[Lcom/ekodroid/omrevaluator/templateui/models/AnswerSetKey;

    move-result-object v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lf5;->e:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getAnswerKeys()[Lcom/ekodroid/omrevaluator/templateui/models/AnswerSetKey;

    move-result-object v0

    const/4 v1, 0x0

    aget-object v0, v0, v1

    if-nez v0, :cond_0

    goto/16 :goto_1

    :cond_0
    iget-object v0, p0, Lf5;->e:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    iget-object v1, p0, Lf5;->d:Ljava/util/ArrayList;

    invoke-virtual {p0, v0, v1}, Lf5;->c(Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v1, p0, Lf5;->e:Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;

    invoke-virtual {v1}, Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;->getAnswerOptions()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    const/4 v2, 0x1

    sub-int/2addr v1, v2

    invoke-virtual {p0, v1}, Lf5;->a(I)[Lh5;

    move-result-object v1

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;

    invoke-virtual {v3}, Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;->getExamSet()I

    move-result v4

    if-ne v4, p1, :cond_1

    invoke-virtual {v3}, Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;->getAnswerValue2s()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;

    sget-object v5, Lf5$a;->a:[I

    invoke-virtual {v4}, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->getMarkedState()Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue$MarkedState;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Enum;->ordinal()I

    move-result v6

    aget v5, v5, v6

    if-eq v5, v2, :cond_5

    const/4 v6, 0x2

    if-eq v5, v6, :cond_4

    const/4 v6, 0x3

    if-eq v5, v6, :cond_3

    const/4 v6, 0x4

    if-eq v5, v6, :cond_2

    goto :goto_0

    :cond_2
    invoke-virtual {v4}, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->getQuestionNumber()I

    move-result v4

    sub-int/2addr v4, v2

    aget-object v4, v1, v4

    invoke-virtual {v4}, Lh5;->g()I

    goto :goto_0

    :cond_3
    invoke-virtual {v4}, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->getQuestionNumber()I

    move-result v4

    sub-int/2addr v4, v2

    aget-object v4, v1, v4

    invoke-virtual {v4}, Lh5;->h()I

    goto :goto_0

    :cond_4
    invoke-virtual {v4}, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->getQuestionNumber()I

    move-result v4

    sub-int/2addr v4, v2

    aget-object v4, v1, v4

    invoke-virtual {v4}, Lh5;->f()I

    goto :goto_0

    :cond_5
    invoke-virtual {v4}, Lcom/ekodroid/omrevaluator/templateui/scanner/AnswerValue;->getQuestionNumber()I

    move-result v4

    sub-int/2addr v4, v2

    aget-object v4, v1, v4

    invoke-virtual {v4}, Lh5;->e()I

    goto :goto_0

    :cond_6
    new-instance p1, Ljava/util/ArrayList;

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object p1

    :cond_7
    :goto_1
    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    return-object p1
.end method

.method public final c(Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 2

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;

    invoke-virtual {v1, p1}, Lcom/ekodroid/omrevaluator/database/ResultDataJsonModel;->getResultItem(Lcom/ekodroid/omrevaluator/templateui/models/SheetTemplate2;)Lcom/ekodroid/omrevaluator/templateui/scanner/ResultItem;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v0
.end method

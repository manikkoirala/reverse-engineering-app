.class public Ll10;
.super Lu80;
.source "SourceFile"


# instance fields
.field public a:Landroid/content/Context;

.field public b:Lj10;

.field public c:Lud1;

.field public d:Landroid/widget/RadioButton;

.field public e:Landroid/widget/RadioButton;

.field public f:Landroid/widget/RadioButton;

.field public g:Landroid/widget/RadioButton;

.field public h:Landroid/widget/RadioButton;

.field public i:Landroid/widget/RadioButton;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lud1;Lj10;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lu80;-><init>(Landroid/content/Context;)V

    iput-object p1, p0, Ll10;->a:Landroid/content/Context;

    iput-object p3, p0, Ll10;->b:Lj10;

    iput-object p2, p0, Ll10;->c:Lud1;

    return-void
.end method

.method public static synthetic a(Ll10;)Lj10;
    .locals 0

    .line 1
    iget-object p0, p0, Ll10;->b:Lj10;

    return-object p0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    .line 1
    invoke-super {p0, p1}, Lu80;->onCreate(Landroid/os/Bundle;)V

    const p1, 0x7f0c0077

    invoke-virtual {p0, p1}, Landroid/app/Dialog;->setContentView(I)V

    const p1, 0x7f090441

    invoke-virtual {p0, p1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroidx/appcompat/widget/Toolbar;

    new-instance v0, Ll10$a;

    invoke-direct {v0, p0}, Ll10$a;-><init>(Ll10;)V

    invoke-virtual {p1, v0}, Landroidx/appcompat/widget/Toolbar;->setNavigationOnClickListener(Landroid/view/View$OnClickListener;)V

    const p1, 0x7f0902f4

    invoke-virtual {p0, p1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/RadioButton;

    iput-object p1, p0, Ll10;->d:Landroid/widget/RadioButton;

    const p1, 0x7f0902f2

    invoke-virtual {p0, p1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/RadioButton;

    iput-object p1, p0, Ll10;->e:Landroid/widget/RadioButton;

    const p1, 0x7f0902f3

    invoke-virtual {p0, p1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/RadioButton;

    iput-object p1, p0, Ll10;->g:Landroid/widget/RadioButton;

    const p1, 0x7f0902f1

    invoke-virtual {p0, p1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/RadioButton;

    iput-object p1, p0, Ll10;->f:Landroid/widget/RadioButton;

    const p1, 0x7f0902ed

    invoke-virtual {p0, p1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/RadioButton;

    iput-object p1, p0, Ll10;->h:Landroid/widget/RadioButton;

    const p1, 0x7f0902f0

    invoke-virtual {p0, p1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object p1

    check-cast p1, Landroid/widget/RadioButton;

    iput-object p1, p0, Ll10;->i:Landroid/widget/RadioButton;

    const p1, 0x7f090084

    invoke-virtual {p0, p1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object p1

    new-instance v0, Ll10$b;

    invoke-direct {v0, p0}, Ll10$b;-><init>(Ll10;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const p1, 0x7f0900a0

    invoke-virtual {p0, p1}, Landroid/app/Dialog;->findViewById(I)Landroid/view/View;

    move-result-object p1

    new-instance v0, Ll10$c;

    invoke-direct {v0, p0}, Ll10$c;-><init>(Ll10;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object p1, p0, Ll10;->c:Lud1;

    if-eqz p1, :cond_4

    iget-object v0, p0, Ll10;->i:Landroid/widget/RadioButton;

    iget-boolean p1, p1, Lud1;->b:Z

    const/4 v1, 0x1

    xor-int/2addr p1, v1

    invoke-virtual {v0, p1}, Landroid/widget/CompoundButton;->setChecked(Z)V

    iget-object p1, p0, Ll10;->h:Landroid/widget/RadioButton;

    iget-object v0, p0, Ll10;->c:Lud1;

    iget-boolean v0, v0, Lud1;->b:Z

    invoke-virtual {p1, v0}, Landroid/widget/CompoundButton;->setChecked(Z)V

    iget-object p1, p0, Ll10;->e:Landroid/widget/RadioButton;

    iget-object v0, p0, Ll10;->c:Lud1;

    iget-object v0, v0, Lud1;->a:Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList$ResultSortBy;

    sget-object v2, Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList$ResultSortBy;->NAME:Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList$ResultSortBy;

    const/4 v3, 0x0

    if-ne v0, v2, :cond_0

    move v0, v1

    goto :goto_0

    :cond_0
    move v0, v3

    :goto_0
    invoke-virtual {p1, v0}, Landroid/widget/CompoundButton;->setChecked(Z)V

    iget-object p1, p0, Ll10;->d:Landroid/widget/RadioButton;

    iget-object v0, p0, Ll10;->c:Lud1;

    iget-object v0, v0, Lud1;->a:Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList$ResultSortBy;

    sget-object v2, Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList$ResultSortBy;->ROLLNO:Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList$ResultSortBy;

    if-ne v0, v2, :cond_1

    move v0, v1

    goto :goto_1

    :cond_1
    move v0, v3

    :goto_1
    invoke-virtual {p1, v0}, Landroid/widget/CompoundButton;->setChecked(Z)V

    iget-object p1, p0, Ll10;->g:Landroid/widget/RadioButton;

    iget-object v0, p0, Ll10;->c:Lud1;

    iget-object v0, v0, Lud1;->a:Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList$ResultSortBy;

    sget-object v2, Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList$ResultSortBy;->RANK:Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList$ResultSortBy;

    if-ne v0, v2, :cond_2

    move v0, v1

    goto :goto_2

    :cond_2
    move v0, v3

    :goto_2
    invoke-virtual {p1, v0}, Landroid/widget/CompoundButton;->setChecked(Z)V

    iget-object p1, p0, Ll10;->f:Landroid/widget/RadioButton;

    iget-object v0, p0, Ll10;->c:Lud1;

    iget-object v0, v0, Lud1;->a:Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList$ResultSortBy;

    sget-object v2, Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList$ResultSortBy;->MARK:Lcom/ekodroid/omrevaluator/util/DataModels/SortResultList$ResultSortBy;

    if-ne v0, v2, :cond_3

    goto :goto_3

    :cond_3
    move v1, v3

    :goto_3
    invoke-virtual {p1, v1}, Landroid/widget/CompoundButton;->setChecked(Z)V

    :cond_4
    return-void
.end method

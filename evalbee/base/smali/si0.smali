.class public Lsi0;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:Landroid/content/Context;

.field public b:Ly01;

.field public c:Landroid/widget/Spinner;

.field public d:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

.field public e:Landroidx/appcompat/app/a;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ly01;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lsi0;->a:Landroid/content/Context;

    iput-object p2, p0, Lsi0;->b:Ly01;

    invoke-virtual {p0}, Lsi0;->f()V

    return-void
.end method

.method public static synthetic a(Lsi0;)Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;
    .locals 0

    .line 1
    iget-object p0, p0, Lsi0;->d:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    return-object p0
.end method

.method public static synthetic b(Lsi0;Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;)Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;
    .locals 0

    .line 1
    iput-object p1, p0, Lsi0;->d:Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;

    return-object p1
.end method

.method public static synthetic c(Lsi0;Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lsi0;->d(Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;)V

    return-void
.end method


# virtual methods
.method public final d(Lcom/ekodroid/omrevaluator/templateui/models/LabelProfile;)V
    .locals 3

    .line 1
    iget-object v0, p0, Lsi0;->e:Landroidx/appcompat/app/a;

    invoke-virtual {v0}, Lq6;->dismiss()V

    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lsi0;->a:Landroid/content/Context;

    const-class v2, Lcom/ekodroid/omrevaluator/templateui/createtemplate/LabelsActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "LABEL_PROFILE"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    iget-object p1, p0, Lsi0;->a:Landroid/content/Context;

    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method public final e()V
    .locals 3

    .line 1
    iget-object v0, p0, Lsi0;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getInstance(Landroid/content/Context;)Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;

    move-result-object v0

    invoke-virtual {v0}, Lcom/ekodroid/omrevaluator/database/repositories/TemplateRepository;->getAllLabelProfileJson()Ljava/util/ArrayList;

    move-result-object v0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    sget-object v2, Lok;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v2, Lok;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/ekodroid/omrevaluator/database/LabelProfileJsonModel;

    invoke-virtual {v2}, Lcom/ekodroid/omrevaluator/database/LabelProfileJsonModel;->getProfileName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    new-instance v0, Lp3;

    iget-object v2, p0, Lsi0;->a:Landroid/content/Context;

    invoke-direct {v0, v2, v1}, Lp3;-><init>(Landroid/content/Context;Ljava/util/ArrayList;)V

    iget-object v2, p0, Lsi0;->c:Landroid/widget/Spinner;

    invoke-virtual {v2, v0}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    iget-object v0, p0, Lsi0;->c:Landroid/widget/Spinner;

    new-instance v2, Lsi0$d;

    invoke-direct {v2, p0, v1}, Lsi0$d;-><init>(Lsi0;Ljava/util/ArrayList;)V

    invoke-virtual {v0, v2}, Landroid/widget/AdapterView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    iget-object v0, p0, Lsi0;->c:Landroid/widget/Spinner;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/AdapterView;->setSelection(I)V

    return-void
.end method

.method public final f()V
    .locals 4

    .line 1
    new-instance v0, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    iget-object v1, p0, Lsi0;->a:Landroid/content/Context;

    const v2, 0x7f130151

    invoke-direct {v0, v1, v2}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;-><init>(Landroid/content/Context;I)V

    iget-object v1, p0, Lsi0;->a:Landroid/content/Context;

    const-string v2, "layout_inflater"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    const v2, 0x7f0c0083

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->setView(Landroid/view/View;)Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    const v2, 0x7f1202e0

    invoke-virtual {v0, v2}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->setTitle(I)Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    const v2, 0x7f090346

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Spinner;

    iput-object v2, p0, Lsi0;->c:Landroid/widget/Spinner;

    invoke-virtual {p0}, Lsi0;->e()V

    const v2, 0x7f090195

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    new-instance v2, Lsi0$a;

    invoke-direct {v2, p0}, Lsi0$a;-><init>(Lsi0;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v1, Lsi0$b;

    invoke-direct {v1, p0}, Lsi0$b;-><init>(Lsi0;)V

    const v2, 0x7f1202a7

    invoke-virtual {v0, v2, v1}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    new-instance v1, Lsi0$c;

    invoke-direct {v1, p0}, Lsi0$c;-><init>(Lsi0;)V

    const v2, 0x7f120053

    invoke-virtual {v0, v2, v1}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;

    invoke-virtual {v0}, Lcom/google/android/material/dialog/MaterialAlertDialogBuilder;->create()Landroidx/appcompat/app/a;

    move-result-object v0

    iput-object v0, p0, Lsi0;->e:Landroidx/appcompat/app/a;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    return-void
.end method

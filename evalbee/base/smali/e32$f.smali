.class public abstract Le32$f;
.super Le32$e;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Le32;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "f"
.end annotation


# instance fields
.field public a:[Li31$b;

.field public b:Ljava/lang/String;

.field public c:I

.field public d:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Le32$e;-><init>(Le32$a;)V

    iput-object v0, p0, Le32$f;->a:[Li31$b;

    const/4 v0, 0x0

    iput v0, p0, Le32$f;->c:I

    return-void
.end method

.method public constructor <init>(Le32$f;)V
    .locals 1

    .line 2
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Le32$e;-><init>(Le32$a;)V

    iput-object v0, p0, Le32$f;->a:[Li31$b;

    const/4 v0, 0x0

    iput v0, p0, Le32$f;->c:I

    iget-object v0, p1, Le32$f;->b:Ljava/lang/String;

    iput-object v0, p0, Le32$f;->b:Ljava/lang/String;

    iget v0, p1, Le32$f;->d:I

    iput v0, p0, Le32$f;->d:I

    iget-object p1, p1, Le32$f;->a:[Li31$b;

    invoke-static {p1}, Li31;->f([Li31$b;)[Li31$b;

    move-result-object p1

    iput-object p1, p0, Le32$f;->a:[Li31$b;

    return-void
.end method


# virtual methods
.method public c()Z
    .locals 1

    .line 1
    const/4 v0, 0x0

    return v0
.end method

.method public d(Landroid/graphics/Path;)V
    .locals 1

    .line 1
    invoke-virtual {p1}, Landroid/graphics/Path;->reset()V

    iget-object v0, p0, Le32$f;->a:[Li31$b;

    if-eqz v0, :cond_0

    invoke-static {v0, p1}, Li31$b;->d([Li31$b;Landroid/graphics/Path;)V

    :cond_0
    return-void
.end method

.method public getPathData()[Li31$b;
    .locals 1

    .line 1
    iget-object v0, p0, Le32$f;->a:[Li31$b;

    return-object v0
.end method

.method public getPathName()Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Le32$f;->b:Ljava/lang/String;

    return-object v0
.end method

.method public setPathData([Li31$b;)V
    .locals 1

    .line 1
    iget-object v0, p0, Le32$f;->a:[Li31$b;

    invoke-static {v0, p1}, Li31;->b([Li31$b;[Li31$b;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p1}, Li31;->f([Li31$b;)[Li31$b;

    move-result-object p1

    iput-object p1, p0, Le32$f;->a:[Li31$b;

    goto :goto_0

    :cond_0
    iget-object v0, p0, Le32$f;->a:[Li31$b;

    invoke-static {v0, p1}, Li31;->j([Li31$b;[Li31$b;)V

    :goto_0
    return-void
.end method

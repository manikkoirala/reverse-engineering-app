.class public Lmb;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lqy0;


# instance fields
.field public final a:Lmd0;

.field public final b:Lgb;

.field public final c:Lld;


# direct methods
.method public constructor <init>(Lgb;)V
    .locals 2

    .line 1
    new-instance v0, Lld;

    const/16 v1, 0x1000

    invoke-direct {v0, v1}, Lld;-><init>(I)V

    invoke-direct {p0, p1, v0}, Lmb;-><init>(Lgb;Lld;)V

    return-void
.end method

.method public constructor <init>(Lgb;Lld;)V
    .locals 0

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lmb;->b:Lgb;

    iput-object p1, p0, Lmb;->a:Lmd0;

    iput-object p2, p0, Lmb;->c:Lld;

    return-void
.end method


# virtual methods
.method public a(Lcom/android/volley/Request;)Lyy0;
    .locals 18

    .line 1
    move-object/from16 v1, p0

    move-object/from16 v8, p1

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v9

    :goto_0
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    const/4 v2, 0x0

    :try_start_0
    invoke-virtual/range {p1 .. p1}, Lcom/android/volley/Request;->m()Lcom/android/volley/a$a;

    move-result-object v0

    invoke-static {v0}, Lid0;->c(Lcom/android/volley/a$a;)Ljava/util/Map;

    move-result-object v0

    iget-object v3, v1, Lmb;->b:Lgb;

    invoke-virtual {v3, v8, v0}, Lgb;->a(Lcom/android/volley/Request;Ljava/util/Map;)Lld0;

    move-result-object v3
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    :try_start_1
    invoke-virtual {v3}, Lld0;->d()I

    move-result v12

    invoke-virtual {v3}, Lld0;->c()Ljava/util/List;

    move-result-object v0

    const/16 v4, 0x130

    if-ne v12, v4, :cond_0

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    sub-long/2addr v4, v9

    invoke-static {v8, v4, v5, v0}, Ldz0;->b(Lcom/android/volley/Request;JLjava/util/List;)Lyy0;

    move-result-object v0

    return-object v0

    :cond_0
    invoke-virtual {v3}, Lld0;->a()Ljava/io/InputStream;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-virtual {v3}, Lld0;->b()I

    move-result v5

    iget-object v6, v1, Lmb;->c:Lld;

    invoke-static {v4, v5, v6}, Ldz0;->c(Ljava/io/InputStream;ILld;)[B

    move-result-object v2

    goto :goto_1

    :cond_1
    const/4 v4, 0x0

    new-array v2, v4, [B

    :goto_1
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    sub-long/2addr v4, v9

    invoke-static {v4, v5, v8, v2, v12}, Ldz0;->d(JLcom/android/volley/Request;[BI)V

    const/16 v4, 0xc8

    if-lt v12, v4, :cond_2

    const/16 v4, 0x12b

    if-gt v12, v4, :cond_2

    new-instance v4, Lyy0;

    const/4 v14, 0x0

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v5

    sub-long v15, v5, v9

    move-object v11, v4

    move-object v13, v2

    move-object/from16 v17, v0

    invoke-direct/range {v11 .. v17}, Lyy0;-><init>(I[BZJLjava/util/List;)V

    return-object v4

    :cond_2
    new-instance v0, Ljava/io/IOException;

    invoke-direct {v0}, Ljava/io/IOException;-><init>()V

    throw v0
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception v0

    move-object v7, v2

    move-object v6, v3

    move-object v3, v0

    goto :goto_2

    :catch_1
    move-exception v0

    move-object v3, v0

    move-object v6, v2

    move-object v7, v6

    :goto_2
    move-object/from16 v2, p1

    move-wide v4, v9

    invoke-static/range {v2 .. v7}, Ldz0;->e(Lcom/android/volley/Request;Ljava/io/IOException;JLld0;[B)Ldz0$b;

    move-result-object v0

    invoke-static {v8, v0}, Ldz0;->a(Lcom/android/volley/Request;Ldz0$b;)V

    goto :goto_0
.end method

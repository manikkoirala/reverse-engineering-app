.class public final enum Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle$XSSFBuiltinTypeStyleStyle;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum PivotStyleDark1:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum PivotStyleDark10:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum PivotStyleDark11:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum PivotStyleDark12:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum PivotStyleDark13:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum PivotStyleDark14:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum PivotStyleDark15:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum PivotStyleDark16:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum PivotStyleDark17:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum PivotStyleDark18:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum PivotStyleDark19:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum PivotStyleDark2:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum PivotStyleDark20:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum PivotStyleDark21:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum PivotStyleDark22:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum PivotStyleDark23:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum PivotStyleDark24:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum PivotStyleDark25:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum PivotStyleDark26:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum PivotStyleDark27:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum PivotStyleDark28:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum PivotStyleDark3:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum PivotStyleDark4:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum PivotStyleDark5:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum PivotStyleDark6:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum PivotStyleDark7:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum PivotStyleDark8:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum PivotStyleDark9:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum PivotStyleLight1:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum PivotStyleLight10:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum PivotStyleLight11:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum PivotStyleLight12:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum PivotStyleLight13:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum PivotStyleLight14:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum PivotStyleLight15:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum PivotStyleLight16:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum PivotStyleLight17:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum PivotStyleLight18:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum PivotStyleLight19:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum PivotStyleLight2:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum PivotStyleLight20:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum PivotStyleLight21:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum PivotStyleLight22:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum PivotStyleLight23:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum PivotStyleLight24:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum PivotStyleLight25:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum PivotStyleLight26:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum PivotStyleLight27:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum PivotStyleLight28:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum PivotStyleLight3:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum PivotStyleLight4:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum PivotStyleLight5:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum PivotStyleLight6:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum PivotStyleLight7:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum PivotStyleLight8:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum PivotStyleLight9:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum PivotStyleMedium1:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum PivotStyleMedium10:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum PivotStyleMedium11:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum PivotStyleMedium12:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum PivotStyleMedium13:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum PivotStyleMedium14:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum PivotStyleMedium15:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum PivotStyleMedium16:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum PivotStyleMedium17:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum PivotStyleMedium18:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum PivotStyleMedium19:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum PivotStyleMedium2:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum PivotStyleMedium20:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum PivotStyleMedium21:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum PivotStyleMedium22:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum PivotStyleMedium23:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum PivotStyleMedium24:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum PivotStyleMedium25:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum PivotStyleMedium26:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum PivotStyleMedium27:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum PivotStyleMedium28:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum PivotStyleMedium3:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum PivotStyleMedium4:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum PivotStyleMedium5:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum PivotStyleMedium6:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum PivotStyleMedium7:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum PivotStyleMedium8:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum PivotStyleMedium9:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum TableStyleDark1:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum TableStyleDark10:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum TableStyleDark11:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum TableStyleDark2:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum TableStyleDark3:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum TableStyleDark4:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum TableStyleDark5:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum TableStyleDark6:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum TableStyleDark7:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum TableStyleDark8:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum TableStyleDark9:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum TableStyleLight1:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum TableStyleLight10:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum TableStyleLight11:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum TableStyleLight12:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum TableStyleLight13:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum TableStyleLight14:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum TableStyleLight15:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum TableStyleLight16:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum TableStyleLight17:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum TableStyleLight18:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum TableStyleLight19:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum TableStyleLight2:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum TableStyleLight20:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum TableStyleLight21:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum TableStyleLight3:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum TableStyleLight4:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum TableStyleLight5:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum TableStyleLight6:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum TableStyleLight7:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum TableStyleLight8:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum TableStyleLight9:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum TableStyleMedium1:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum TableStyleMedium10:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum TableStyleMedium11:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum TableStyleMedium12:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum TableStyleMedium13:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum TableStyleMedium14:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum TableStyleMedium15:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum TableStyleMedium16:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum TableStyleMedium17:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum TableStyleMedium18:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum TableStyleMedium19:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum TableStyleMedium2:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum TableStyleMedium20:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum TableStyleMedium21:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum TableStyleMedium22:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum TableStyleMedium23:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum TableStyleMedium24:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum TableStyleMedium25:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum TableStyleMedium26:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum TableStyleMedium27:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum TableStyleMedium28:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum TableStyleMedium3:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum TableStyleMedium4:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum TableStyleMedium5:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum TableStyleMedium6:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum TableStyleMedium7:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum TableStyleMedium8:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field public static final enum TableStyleMedium9:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

.field private static final styleMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;",
            "Lorg/apache/poi/ss/usermodel/TableStyle;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 147

    new-instance v1, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object v0, v1

    const-string v2, "TableStyleDark1"

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->TableStyleDark1:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v2, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object v1, v2

    const-string v3, "TableStyleDark2"

    const/4 v4, 0x1

    invoke-direct {v2, v3, v4}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->TableStyleDark2:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v3, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object v2, v3

    const-string v4, "TableStyleDark3"

    const/4 v5, 0x2

    invoke-direct {v3, v4, v5}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v3, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->TableStyleDark3:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v4, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object v3, v4

    const-string v5, "TableStyleDark4"

    const/4 v6, 0x3

    invoke-direct {v4, v5, v6}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->TableStyleDark4:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v5, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object v4, v5

    const-string v6, "TableStyleDark5"

    const/4 v7, 0x4

    invoke-direct {v5, v6, v7}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v5, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->TableStyleDark5:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v6, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object v5, v6

    const-string v7, "TableStyleDark6"

    const/4 v8, 0x5

    invoke-direct {v6, v7, v8}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v6, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->TableStyleDark6:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v7, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object v6, v7

    const-string v8, "TableStyleDark7"

    const/4 v9, 0x6

    invoke-direct {v7, v8, v9}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v7, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->TableStyleDark7:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v8, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object v7, v8

    const-string v9, "TableStyleDark8"

    const/4 v10, 0x7

    invoke-direct {v8, v9, v10}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v8, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->TableStyleDark8:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v9, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object v8, v9

    const-string v10, "TableStyleDark9"

    const/16 v11, 0x8

    invoke-direct {v9, v10, v11}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v9, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->TableStyleDark9:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v10, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object v9, v10

    const-string v11, "TableStyleDark10"

    const/16 v12, 0x9

    invoke-direct {v10, v11, v12}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v10, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->TableStyleDark10:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v11, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object v10, v11

    const-string v12, "TableStyleDark11"

    const/16 v13, 0xa

    invoke-direct {v11, v12, v13}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v11, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->TableStyleDark11:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v12, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object v11, v12

    const-string v13, "TableStyleLight1"

    const/16 v14, 0xb

    invoke-direct {v12, v13, v14}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v12, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->TableStyleLight1:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v13, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object v12, v13

    const-string v14, "TableStyleLight2"

    const/16 v15, 0xc

    invoke-direct {v13, v14, v15}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v13, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->TableStyleLight2:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v14, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object v13, v14

    const-string v15, "TableStyleLight3"

    move-object/from16 v144, v0

    const/16 v0, 0xd

    invoke-direct {v14, v15, v0}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v14, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->TableStyleLight3:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object v14, v0

    const-string v15, "TableStyleLight4"

    move-object/from16 v145, v1

    const/16 v1, 0xe

    invoke-direct {v0, v15, v1}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->TableStyleLight4:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object v15, v0

    const-string v1, "TableStyleLight5"

    move-object/from16 v146, v2

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->TableStyleLight5:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object/from16 v16, v0

    const-string v1, "TableStyleLight6"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->TableStyleLight6:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object/from16 v17, v0

    const-string v1, "TableStyleLight7"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->TableStyleLight7:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object/from16 v18, v0

    const-string v1, "TableStyleLight8"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->TableStyleLight8:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object/from16 v19, v0

    const-string v1, "TableStyleLight9"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->TableStyleLight9:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object/from16 v20, v0

    const-string v1, "TableStyleLight10"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->TableStyleLight10:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object/from16 v21, v0

    const-string v1, "TableStyleLight11"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->TableStyleLight11:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object/from16 v22, v0

    const-string v1, "TableStyleLight12"

    const/16 v2, 0x16

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->TableStyleLight12:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object/from16 v23, v0

    const-string v1, "TableStyleLight13"

    const/16 v2, 0x17

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->TableStyleLight13:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object/from16 v24, v0

    const-string v1, "TableStyleLight14"

    const/16 v2, 0x18

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->TableStyleLight14:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object/from16 v25, v0

    const-string v1, "TableStyleLight15"

    const/16 v2, 0x19

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->TableStyleLight15:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object/from16 v26, v0

    const-string v1, "TableStyleLight16"

    const/16 v2, 0x1a

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->TableStyleLight16:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object/from16 v27, v0

    const-string v1, "TableStyleLight17"

    const/16 v2, 0x1b

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->TableStyleLight17:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object/from16 v28, v0

    const-string v1, "TableStyleLight18"

    const/16 v2, 0x1c

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->TableStyleLight18:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object/from16 v29, v0

    const-string v1, "TableStyleLight19"

    const/16 v2, 0x1d

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->TableStyleLight19:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object/from16 v30, v0

    const-string v1, "TableStyleLight20"

    const/16 v2, 0x1e

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->TableStyleLight20:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object/from16 v31, v0

    const-string v1, "TableStyleLight21"

    const/16 v2, 0x1f

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->TableStyleLight21:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object/from16 v32, v0

    const-string v1, "TableStyleMedium1"

    const/16 v2, 0x20

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->TableStyleMedium1:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object/from16 v33, v0

    const-string v1, "TableStyleMedium2"

    const/16 v2, 0x21

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->TableStyleMedium2:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object/from16 v34, v0

    const-string v1, "TableStyleMedium3"

    const/16 v2, 0x22

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->TableStyleMedium3:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object/from16 v35, v0

    const-string v1, "TableStyleMedium4"

    const/16 v2, 0x23

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->TableStyleMedium4:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object/from16 v36, v0

    const-string v1, "TableStyleMedium5"

    const/16 v2, 0x24

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->TableStyleMedium5:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object/from16 v37, v0

    const-string v1, "TableStyleMedium6"

    const/16 v2, 0x25

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->TableStyleMedium6:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object/from16 v38, v0

    const-string v1, "TableStyleMedium7"

    const/16 v2, 0x26

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->TableStyleMedium7:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object/from16 v39, v0

    const-string v1, "TableStyleMedium8"

    const/16 v2, 0x27

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->TableStyleMedium8:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object/from16 v40, v0

    const-string v1, "TableStyleMedium9"

    const/16 v2, 0x28

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->TableStyleMedium9:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object/from16 v41, v0

    const-string v1, "TableStyleMedium10"

    const/16 v2, 0x29

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->TableStyleMedium10:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object/from16 v42, v0

    const-string v1, "TableStyleMedium11"

    const/16 v2, 0x2a

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->TableStyleMedium11:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object/from16 v43, v0

    const-string v1, "TableStyleMedium12"

    const/16 v2, 0x2b

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->TableStyleMedium12:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object/from16 v44, v0

    const-string v1, "TableStyleMedium13"

    const/16 v2, 0x2c

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->TableStyleMedium13:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object/from16 v45, v0

    const-string v1, "TableStyleMedium14"

    const/16 v2, 0x2d

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->TableStyleMedium14:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object/from16 v46, v0

    const-string v1, "TableStyleMedium15"

    const/16 v2, 0x2e

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->TableStyleMedium15:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object/from16 v47, v0

    const-string v1, "TableStyleMedium16"

    const/16 v2, 0x2f

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->TableStyleMedium16:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object/from16 v48, v0

    const-string v1, "TableStyleMedium17"

    const/16 v2, 0x30

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->TableStyleMedium17:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object/from16 v49, v0

    const-string v1, "TableStyleMedium18"

    const/16 v2, 0x31

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->TableStyleMedium18:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object/from16 v50, v0

    const-string v1, "TableStyleMedium19"

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->TableStyleMedium19:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object/from16 v51, v0

    const-string v1, "TableStyleMedium20"

    const/16 v2, 0x33

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->TableStyleMedium20:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object/from16 v52, v0

    const-string v1, "TableStyleMedium21"

    const/16 v2, 0x34

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->TableStyleMedium21:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object/from16 v53, v0

    const-string v1, "TableStyleMedium22"

    const/16 v2, 0x35

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->TableStyleMedium22:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object/from16 v54, v0

    const-string v1, "TableStyleMedium23"

    const/16 v2, 0x36

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->TableStyleMedium23:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object/from16 v55, v0

    const-string v1, "TableStyleMedium24"

    const/16 v2, 0x37

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->TableStyleMedium24:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object/from16 v56, v0

    const-string v1, "TableStyleMedium25"

    const/16 v2, 0x38

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->TableStyleMedium25:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object/from16 v57, v0

    const-string v1, "TableStyleMedium26"

    const/16 v2, 0x39

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->TableStyleMedium26:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object/from16 v58, v0

    const-string v1, "TableStyleMedium27"

    const/16 v2, 0x3a

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->TableStyleMedium27:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object/from16 v59, v0

    const-string v1, "TableStyleMedium28"

    const/16 v2, 0x3b

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->TableStyleMedium28:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object/from16 v60, v0

    const-string v1, "PivotStyleMedium1"

    const/16 v2, 0x3c

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->PivotStyleMedium1:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object/from16 v61, v0

    const-string v1, "PivotStyleMedium2"

    const/16 v2, 0x3d

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->PivotStyleMedium2:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object/from16 v62, v0

    const-string v1, "PivotStyleMedium3"

    const/16 v2, 0x3e

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->PivotStyleMedium3:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object/from16 v63, v0

    const-string v1, "PivotStyleMedium4"

    const/16 v2, 0x3f

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->PivotStyleMedium4:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object/from16 v64, v0

    const-string v1, "PivotStyleMedium5"

    const/16 v2, 0x40

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->PivotStyleMedium5:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object/from16 v65, v0

    const-string v1, "PivotStyleMedium6"

    const/16 v2, 0x41

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->PivotStyleMedium6:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object/from16 v66, v0

    const-string v1, "PivotStyleMedium7"

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->PivotStyleMedium7:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object/from16 v67, v0

    const-string v1, "PivotStyleMedium8"

    const/16 v2, 0x43

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->PivotStyleMedium8:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object/from16 v68, v0

    const-string v1, "PivotStyleMedium9"

    const/16 v2, 0x44

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->PivotStyleMedium9:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object/from16 v69, v0

    const-string v1, "PivotStyleMedium10"

    const/16 v2, 0x45

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->PivotStyleMedium10:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object/from16 v70, v0

    const-string v1, "PivotStyleMedium11"

    const/16 v2, 0x46

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->PivotStyleMedium11:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object/from16 v71, v0

    const-string v1, "PivotStyleMedium12"

    const/16 v2, 0x47

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->PivotStyleMedium12:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object/from16 v72, v0

    const-string v1, "PivotStyleMedium13"

    const/16 v2, 0x48

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->PivotStyleMedium13:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object/from16 v73, v0

    const-string v1, "PivotStyleMedium14"

    const/16 v2, 0x49

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->PivotStyleMedium14:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object/from16 v74, v0

    const-string v1, "PivotStyleMedium15"

    const/16 v2, 0x4a

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->PivotStyleMedium15:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object/from16 v75, v0

    const-string v1, "PivotStyleMedium16"

    const/16 v2, 0x4b

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->PivotStyleMedium16:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object/from16 v76, v0

    const-string v1, "PivotStyleMedium17"

    const/16 v2, 0x4c

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->PivotStyleMedium17:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object/from16 v77, v0

    const-string v1, "PivotStyleMedium18"

    const/16 v2, 0x4d

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->PivotStyleMedium18:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object/from16 v78, v0

    const-string v1, "PivotStyleMedium19"

    const/16 v2, 0x4e

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->PivotStyleMedium19:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object/from16 v79, v0

    const-string v1, "PivotStyleMedium20"

    const/16 v2, 0x4f

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->PivotStyleMedium20:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object/from16 v80, v0

    const-string v1, "PivotStyleMedium21"

    const/16 v2, 0x50

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->PivotStyleMedium21:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object/from16 v81, v0

    const-string v1, "PivotStyleMedium22"

    const/16 v2, 0x51

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->PivotStyleMedium22:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object/from16 v82, v0

    const-string v1, "PivotStyleMedium23"

    const/16 v2, 0x52

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->PivotStyleMedium23:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object/from16 v83, v0

    const-string v1, "PivotStyleMedium24"

    const/16 v2, 0x53

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->PivotStyleMedium24:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object/from16 v84, v0

    const-string v1, "PivotStyleMedium25"

    const/16 v2, 0x54

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->PivotStyleMedium25:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object/from16 v85, v0

    const-string v1, "PivotStyleMedium26"

    const/16 v2, 0x55

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->PivotStyleMedium26:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object/from16 v86, v0

    const-string v1, "PivotStyleMedium27"

    const/16 v2, 0x56

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->PivotStyleMedium27:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object/from16 v87, v0

    const-string v1, "PivotStyleMedium28"

    const/16 v2, 0x57

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->PivotStyleMedium28:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object/from16 v88, v0

    const-string v1, "PivotStyleLight1"

    const/16 v2, 0x58

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->PivotStyleLight1:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object/from16 v89, v0

    const-string v1, "PivotStyleLight2"

    const/16 v2, 0x59

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->PivotStyleLight2:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object/from16 v90, v0

    const-string v1, "PivotStyleLight3"

    const/16 v2, 0x5a

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->PivotStyleLight3:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object/from16 v91, v0

    const-string v1, "PivotStyleLight4"

    const/16 v2, 0x5b

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->PivotStyleLight4:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object/from16 v92, v0

    const-string v1, "PivotStyleLight5"

    const/16 v2, 0x5c

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->PivotStyleLight5:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object/from16 v93, v0

    const-string v1, "PivotStyleLight6"

    const/16 v2, 0x5d

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->PivotStyleLight6:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object/from16 v94, v0

    const-string v1, "PivotStyleLight7"

    const/16 v2, 0x5e

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->PivotStyleLight7:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object/from16 v95, v0

    const-string v1, "PivotStyleLight8"

    const/16 v2, 0x5f

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->PivotStyleLight8:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object/from16 v96, v0

    const-string v1, "PivotStyleLight9"

    const/16 v2, 0x60

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->PivotStyleLight9:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object/from16 v97, v0

    const-string v1, "PivotStyleLight10"

    const/16 v2, 0x61

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->PivotStyleLight10:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object/from16 v98, v0

    const-string v1, "PivotStyleLight11"

    const/16 v2, 0x62

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->PivotStyleLight11:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object/from16 v99, v0

    const-string v1, "PivotStyleLight12"

    const/16 v2, 0x63

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->PivotStyleLight12:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object/from16 v100, v0

    const-string v1, "PivotStyleLight13"

    const/16 v2, 0x64

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->PivotStyleLight13:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object/from16 v101, v0

    const-string v1, "PivotStyleLight14"

    const/16 v2, 0x65

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->PivotStyleLight14:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object/from16 v102, v0

    const-string v1, "PivotStyleLight15"

    const/16 v2, 0x66

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->PivotStyleLight15:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object/from16 v103, v0

    const-string v1, "PivotStyleLight16"

    const/16 v2, 0x67

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->PivotStyleLight16:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object/from16 v104, v0

    const-string v1, "PivotStyleLight17"

    const/16 v2, 0x68

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->PivotStyleLight17:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object/from16 v105, v0

    const-string v1, "PivotStyleLight18"

    const/16 v2, 0x69

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->PivotStyleLight18:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object/from16 v106, v0

    const-string v1, "PivotStyleLight19"

    const/16 v2, 0x6a

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->PivotStyleLight19:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object/from16 v107, v0

    const-string v1, "PivotStyleLight20"

    const/16 v2, 0x6b

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->PivotStyleLight20:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object/from16 v108, v0

    const-string v1, "PivotStyleLight21"

    const/16 v2, 0x6c

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->PivotStyleLight21:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object/from16 v109, v0

    const-string v1, "PivotStyleLight22"

    const/16 v2, 0x6d

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->PivotStyleLight22:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object/from16 v110, v0

    const-string v1, "PivotStyleLight23"

    const/16 v2, 0x6e

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->PivotStyleLight23:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object/from16 v111, v0

    const-string v1, "PivotStyleLight24"

    const/16 v2, 0x6f

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->PivotStyleLight24:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object/from16 v112, v0

    const-string v1, "PivotStyleLight25"

    const/16 v2, 0x70

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->PivotStyleLight25:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object/from16 v113, v0

    const-string v1, "PivotStyleLight26"

    const/16 v2, 0x71

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->PivotStyleLight26:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object/from16 v114, v0

    const-string v1, "PivotStyleLight27"

    const/16 v2, 0x72

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->PivotStyleLight27:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object/from16 v115, v0

    const-string v1, "PivotStyleLight28"

    const/16 v2, 0x73

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->PivotStyleLight28:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object/from16 v116, v0

    const-string v1, "PivotStyleDark1"

    const/16 v2, 0x74

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->PivotStyleDark1:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object/from16 v117, v0

    const-string v1, "PivotStyleDark2"

    const/16 v2, 0x75

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->PivotStyleDark2:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object/from16 v118, v0

    const-string v1, "PivotStyleDark3"

    const/16 v2, 0x76

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->PivotStyleDark3:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object/from16 v119, v0

    const-string v1, "PivotStyleDark4"

    const/16 v2, 0x77

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->PivotStyleDark4:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object/from16 v120, v0

    const-string v1, "PivotStyleDark5"

    const/16 v2, 0x78

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->PivotStyleDark5:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object/from16 v121, v0

    const-string v1, "PivotStyleDark6"

    const/16 v2, 0x79

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->PivotStyleDark6:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object/from16 v122, v0

    const-string v1, "PivotStyleDark7"

    const/16 v2, 0x7a

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->PivotStyleDark7:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object/from16 v123, v0

    const-string v1, "PivotStyleDark8"

    const/16 v2, 0x7b

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->PivotStyleDark8:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object/from16 v124, v0

    const-string v1, "PivotStyleDark9"

    const/16 v2, 0x7c

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->PivotStyleDark9:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object/from16 v125, v0

    const-string v1, "PivotStyleDark10"

    const/16 v2, 0x7d

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->PivotStyleDark10:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object/from16 v126, v0

    const-string v1, "PivotStyleDark11"

    const/16 v2, 0x7e

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->PivotStyleDark11:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object/from16 v127, v0

    const-string v1, "PivotStyleDark12"

    const/16 v2, 0x7f

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->PivotStyleDark12:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object/from16 v128, v0

    const-string v1, "PivotStyleDark13"

    const/16 v2, 0x80

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->PivotStyleDark13:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object/from16 v129, v0

    const-string v1, "PivotStyleDark14"

    const/16 v2, 0x81

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->PivotStyleDark14:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object/from16 v130, v0

    const-string v1, "PivotStyleDark15"

    const/16 v2, 0x82

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->PivotStyleDark15:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object/from16 v131, v0

    const-string v1, "PivotStyleDark16"

    const/16 v2, 0x83

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->PivotStyleDark16:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object/from16 v132, v0

    const-string v1, "PivotStyleDark17"

    const/16 v2, 0x84

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->PivotStyleDark17:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object/from16 v133, v0

    const-string v1, "PivotStyleDark18"

    const/16 v2, 0x85

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->PivotStyleDark18:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object/from16 v134, v0

    const-string v1, "PivotStyleDark19"

    const/16 v2, 0x86

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->PivotStyleDark19:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object/from16 v135, v0

    const-string v1, "PivotStyleDark20"

    const/16 v2, 0x87

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->PivotStyleDark20:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object/from16 v136, v0

    const-string v1, "PivotStyleDark21"

    const/16 v2, 0x88

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->PivotStyleDark21:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object/from16 v137, v0

    const-string v1, "PivotStyleDark22"

    const/16 v2, 0x89

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->PivotStyleDark22:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object/from16 v138, v0

    const-string v1, "PivotStyleDark23"

    const/16 v2, 0x8a

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->PivotStyleDark23:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object/from16 v139, v0

    const-string v1, "PivotStyleDark24"

    const/16 v2, 0x8b

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->PivotStyleDark24:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object/from16 v140, v0

    const-string v1, "PivotStyleDark25"

    const/16 v2, 0x8c

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->PivotStyleDark25:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object/from16 v141, v0

    const-string v1, "PivotStyleDark26"

    const/16 v2, 0x8d

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->PivotStyleDark26:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object/from16 v142, v0

    const-string v1, "PivotStyleDark27"

    const/16 v2, 0x8e

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->PivotStyleDark27:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object/from16 v143, v0

    const-string v1, "PivotStyleDark28"

    const/16 v2, 0x8f

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->PivotStyleDark28:Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-object/from16 v0, v144

    move-object/from16 v1, v145

    move-object/from16 v2, v146

    filled-new-array/range {v0 .. v143}, [Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->$VALUES:[Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    new-instance v0, Ljava/util/EnumMap;

    const-class v1, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    invoke-direct {v0, v1}, Ljava/util/EnumMap;-><init>(Ljava/lang/Class;)V

    sput-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->styleMap:Ljava/util/Map;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static final declared-synchronized init()V
    .locals 11

    const-class v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->styleMap:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->isEmpty()Z

    move-result v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    if-nez v1, :cond_0

    monitor-exit v0

    return-void

    :cond_0
    :try_start_1
    const-class v1, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    const-string v2, "presetTableStyles.xml"

    invoke-virtual {v1, v2}, Ljava/lang/Class;->getResourceAsStream(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v1
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    invoke-static {v1}, Lorg/apache/poi/util/DocumentHelper;->readDocument(Ljava/io/InputStream;)Lorg/w3c/dom/Document;

    move-result-object v2

    invoke-interface {v2}, Lorg/w3c/dom/Document;->getDocumentElement()Lorg/w3c/dom/Element;

    move-result-object v2

    invoke-interface {v2}, Lorg/w3c/dom/Node;->getChildNodes()Lorg/w3c/dom/NodeList;

    move-result-object v2

    const/4 v3, 0x0

    move v4, v3

    :goto_0
    invoke-interface {v2}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v5

    if-ge v4, v5, :cond_2

    invoke-interface {v2, v4}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v5

    invoke-interface {v5}, Lorg/w3c/dom/Node;->getNodeType()S

    move-result v6

    const/4 v7, 0x1

    if-eq v6, v7, :cond_1

    goto :goto_1

    :cond_1
    check-cast v5, Lorg/w3c/dom/Element;

    invoke-interface {v5}, Lorg/w3c/dom/Element;->getTagName()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->valueOf(Ljava/lang/String;)Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    move-result-object v7

    const-string v8, "dxfs"

    invoke-interface {v5, v8}, Lorg/w3c/dom/Element;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v8

    invoke-interface {v8, v3}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v8

    const-string v9, "tableStyles"

    invoke-interface {v5, v9}, Lorg/w3c/dom/Element;->getElementsByTagName(Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v5

    invoke-interface {v5, v3}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v5

    new-instance v9, Lorg/apache/poi/xssf/model/StylesTable;

    invoke-direct {v9}, Lorg/apache/poi/xssf/model/StylesTable;-><init>()V

    new-instance v10, Ljava/io/ByteArrayInputStream;

    invoke-static {v8, v5}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->styleXML(Lorg/w3c/dom/Node;Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v5

    const-string v8, "UTF-8"

    invoke-static {v8}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v5

    invoke-direct {v10, v5}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-virtual {v9, v10}, Lorg/apache/poi/xssf/model/StylesTable;->readFrom(Ljava/io/InputStream;)V

    sget-object v5, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->styleMap:Ljava/util/Map;

    new-instance v8, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle$XSSFBuiltinTypeStyleStyle;

    invoke-virtual {v9, v6}, Lorg/apache/poi/xssf/model/StylesTable;->getExplicitTableStyle(Ljava/lang/String;)Lorg/apache/poi/ss/usermodel/TableStyle;

    move-result-object v6

    invoke-direct {v8, v7, v6}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle$XSSFBuiltinTypeStyleStyle;-><init>(Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;Lorg/apache/poi/ss/usermodel/TableStyle;)V

    invoke-interface {v5, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_2
    :try_start_3
    invoke-static {v1}, Lorg/apache/poi/util/IOUtils;->closeQuietly(Ljava/io/Closeable;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    monitor-exit v0

    return-void

    :catchall_0
    move-exception v2

    :try_start_4
    invoke-static {v1}, Lorg/apache/poi/util/IOUtils;->closeQuietly(Ljava/io/Closeable;)V

    throw v2
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :catch_0
    move-exception v1

    :try_start_5
    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :catchall_1
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public static isBuiltinStyle(Lorg/apache/poi/ss/usermodel/TableStyle;)Z
    .locals 1

    const/4 v0, 0x0

    if-nez p0, :cond_0

    return v0

    :cond_0
    :try_start_0
    invoke-interface {p0}, Lorg/apache/poi/ss/usermodel/TableStyle;->getName()Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->valueOf(Ljava/lang/String;)Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 p0, 0x1

    return p0

    :catch_0
    return v0
.end method

.method private static styleXML(Lorg/w3c/dom/Node;Lorg/w3c/dom/Node;)Ljava/lang/String;
    .locals 4

    invoke-interface {p0}, Lorg/w3c/dom/Node;->getOwnerDocument()Lorg/w3c/dom/Document;

    move-result-object v0

    const-string v1, "dxf"

    invoke-interface {v0, v1}, Lorg/w3c/dom/Document;->createElement(Ljava/lang/String;)Lorg/w3c/dom/Element;

    move-result-object v0

    invoke-interface {p0}, Lorg/w3c/dom/Node;->getFirstChild()Lorg/w3c/dom/Node;

    move-result-object v1

    invoke-interface {p0, v0, v1}, Lorg/w3c/dom/Node;->insertBefore(Lorg/w3c/dom/Node;Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    invoke-interface {p0}, Lorg/w3c/dom/Node;->getOwnerDocument()Lorg/w3c/dom/Document;

    move-result-object v0

    invoke-interface {v0}, Lorg/w3c/dom/Document;->getImplementation()Lorg/w3c/dom/DOMImplementation;

    move-result-object v0

    const-string v1, "LS"

    const-string v2, "3.0"

    invoke-interface {v0, v1, v2}, Lorg/w3c/dom/DOMImplementation;->getFeature(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/w3c/dom/ls/DOMImplementationLS;

    invoke-interface {v0}, Lorg/w3c/dom/ls/DOMImplementationLS;->createLSSerializer()Lorg/w3c/dom/ls/LSSerializer;

    move-result-object v0

    invoke-interface {v0}, Lorg/w3c/dom/ls/LSSerializer;->getDomConfig()Lorg/w3c/dom/DOMConfiguration;

    move-result-object v1

    const-string v2, "xml-declaration"

    sget-object v3, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    invoke-interface {v1, v2, v3}, Lorg/w3c/dom/DOMConfiguration;->setParameter(Ljava/lang/String;Ljava/lang/Object;)V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "<styleSheet xmlns=\"http://schemas.openxmlformats.org/spreadsheetml/2006/main\" xmlns:mc=\"http://schemas.openxmlformats.org/markup-compatibility/2006\" xmlns:x14ac=\"http://schemas.microsoft.com/office/spreadsheetml/2009/9/ac\" xmlns:x16r2=\"http://schemas.microsoft.com/office/spreadsheetml/2015/02/main\" mc:Ignorable=\"x14ac x16r2\">\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {v0, p0}, Lorg/w3c/dom/ls/LSSerializer;->writeToString(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {v0, p1}, Lorg/w3c/dom/ls/LSSerializer;->writeToString(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p0, "</styleSheet>"

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;
    .locals 1

    const-class v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    return-object p0
.end method

.method public static values()[Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;
    .locals 1

    sget-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->$VALUES:[Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    invoke-virtual {v0}, [Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;

    return-object v0
.end method


# virtual methods
.method public getStyle()Lorg/apache/poi/ss/usermodel/TableStyle;
    .locals 1

    invoke-static {}, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->init()V

    sget-object v0, Lorg/apache/poi/xssf/usermodel/XSSFBuiltinTableStyle;->styleMap:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/ss/usermodel/TableStyle;

    return-object v0
.end method

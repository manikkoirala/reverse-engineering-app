.class public final enum Lorg/apache/poi/xssf/usermodel/TextDirection;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lorg/apache/poi/xssf/usermodel/TextDirection;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lorg/apache/poi/xssf/usermodel/TextDirection;

.field public static final enum HORIZONTAL:Lorg/apache/poi/xssf/usermodel/TextDirection;

.field public static final enum STACKED:Lorg/apache/poi/xssf/usermodel/TextDirection;

.field public static final enum VERTICAL:Lorg/apache/poi/xssf/usermodel/TextDirection;

.field public static final enum VERTICAL_270:Lorg/apache/poi/xssf/usermodel/TextDirection;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    new-instance v0, Lorg/apache/poi/xssf/usermodel/TextDirection;

    const-string v1, "HORIZONTAL"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/xssf/usermodel/TextDirection;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/xssf/usermodel/TextDirection;->HORIZONTAL:Lorg/apache/poi/xssf/usermodel/TextDirection;

    new-instance v1, Lorg/apache/poi/xssf/usermodel/TextDirection;

    const-string v2, "VERTICAL"

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Lorg/apache/poi/xssf/usermodel/TextDirection;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lorg/apache/poi/xssf/usermodel/TextDirection;->VERTICAL:Lorg/apache/poi/xssf/usermodel/TextDirection;

    new-instance v2, Lorg/apache/poi/xssf/usermodel/TextDirection;

    const-string v3, "VERTICAL_270"

    const/4 v4, 0x2

    invoke-direct {v2, v3, v4}, Lorg/apache/poi/xssf/usermodel/TextDirection;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lorg/apache/poi/xssf/usermodel/TextDirection;->VERTICAL_270:Lorg/apache/poi/xssf/usermodel/TextDirection;

    new-instance v3, Lorg/apache/poi/xssf/usermodel/TextDirection;

    const-string v4, "STACKED"

    const/4 v5, 0x3

    invoke-direct {v3, v4, v5}, Lorg/apache/poi/xssf/usermodel/TextDirection;-><init>(Ljava/lang/String;I)V

    sput-object v3, Lorg/apache/poi/xssf/usermodel/TextDirection;->STACKED:Lorg/apache/poi/xssf/usermodel/TextDirection;

    filled-new-array {v0, v1, v2, v3}, [Lorg/apache/poi/xssf/usermodel/TextDirection;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/xssf/usermodel/TextDirection;->$VALUES:[Lorg/apache/poi/xssf/usermodel/TextDirection;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lorg/apache/poi/xssf/usermodel/TextDirection;
    .locals 1

    const-class v0, Lorg/apache/poi/xssf/usermodel/TextDirection;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lorg/apache/poi/xssf/usermodel/TextDirection;

    return-object p0
.end method

.method public static values()[Lorg/apache/poi/xssf/usermodel/TextDirection;
    .locals 1

    sget-object v0, Lorg/apache/poi/xssf/usermodel/TextDirection;->$VALUES:[Lorg/apache/poi/xssf/usermodel/TextDirection;

    invoke-virtual {v0}, [Lorg/apache/poi/xssf/usermodel/TextDirection;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lorg/apache/poi/xssf/usermodel/TextDirection;

    return-object v0
.end method

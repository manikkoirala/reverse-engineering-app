.class public final enum Lorg/apache/poi/xwpf/usermodel/Borders;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lorg/apache/poi/xwpf/usermodel/Borders;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum APPLES:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum ARCHED_SCALLOPS:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum BABY_PACIFIER:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum BABY_RATTLE:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum BALLOONS_3_COLORS:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum BALLOONS_HOT_AIR:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum BASIC_BLACK_DASHES:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum BASIC_BLACK_DOTS:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum BASIC_BLACK_SQUARES:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum BASIC_THIN_LINES:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum BASIC_WHITE_DASHES:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum BASIC_WHITE_DOTS:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum BASIC_WHITE_SQUARES:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum BASIC_WIDE_INLINE:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum BASIC_WIDE_MIDLINE:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum BASIC_WIDE_OUTLINE:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum BATS:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum BIRDS:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum BIRDS_FLIGHT:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum CABINS:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum CAKE_SLICE:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum CANDY_CORN:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum CELTIC_KNOTWORK:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum CERTIFICATE_BANNER:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum CHAIN_LINK:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum CHAMPAGNE_BOTTLE:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum CHECKED_BAR_BLACK:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum CHECKED_BAR_COLOR:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum CHECKERED:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum CHRISTMAS_TREE:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum CIRCLES_LINES:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum CIRCLES_RECTANGLES:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum CLASSICAL_WAVE:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum CLOCKS:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum COMPASS:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum CONFETTI:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum CONFETTI_GRAYS:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum CONFETTI_OUTLINE:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum CONFETTI_STREAMERS:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum CONFETTI_WHITE:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum CORNER_TRIANGLES:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum COUPON_CUTOUT_DASHES:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum COUPON_CUTOUT_DOTS:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum CRAZY_MAZE:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum CREATURES_BUTTERFLY:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum CREATURES_FISH:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum CREATURES_INSECTS:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum CREATURES_LADY_BUG:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum CROSS_STITCH:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum CUP:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum DASHED:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum DASH_DOT_STROKED:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum DASH_SMALL_GAP:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum DECO_ARCH:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum DECO_ARCH_COLOR:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum DECO_BLOCKS:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum DIAMONDS_GRAY:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum DOTTED:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum DOT_DASH:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum DOT_DOT_DASH:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum DOUBLE:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum DOUBLE_D:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum DOUBLE_DIAMONDS:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum DOUBLE_WAVE:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum EARTH_1:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum EARTH_2:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum ECLIPSING_SQUARES_1:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum ECLIPSING_SQUARES_2:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum EGGS_BLACK:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum FANS:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum FILM:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum FIRECRACKERS:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum FLOWERS_BLOCK_PRINT:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum FLOWERS_DAISIES:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum FLOWERS_MODERN_1:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum FLOWERS_MODERN_2:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum FLOWERS_PANSY:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum FLOWERS_RED_ROSE:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum FLOWERS_ROSES:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum FLOWERS_TEACUP:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum FLOWERS_TINY:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum GEMS:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum GINGERBREAD_MAN:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum GRADIENT:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum HANDMADE_1:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum HANDMADE_2:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum HEARTS:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum HEART_BALLOON:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum HEART_GRAY:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum HEEBIE_JEEBIES:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum HOLLY:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum HOUSE_FUNKY:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum HYPNOTIC:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum ICE_CREAM_CONES:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum INSET:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum LIGHTNING_1:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum LIGHTNING_2:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum LIGHT_BULB:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum MAPLE_LEAF:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum MAPLE_MUFFINS:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum MAP_PINS:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum MARQUEE:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum MARQUEE_TOOTHED:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum MOONS:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum MOSAIC:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum MUSIC_NOTES:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum NIL:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum NONE:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum NORTHWEST:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum OUTSET:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum OVALS:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum PACKAGES:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum PALMS_BLACK:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum PALMS_COLOR:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum PAPER_CLIPS:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum PAPYRUS:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum PARTY_FAVOR:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum PARTY_GLASS:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum PENCILS:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum PEOPLE:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum PEOPLE_HATS:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum PEOPLE_WAVING:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum POINSETTIAS:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum POSTAGE_STAMP:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum PUMPKIN_1:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum PUSH_PIN_NOTE_1:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum PUSH_PIN_NOTE_2:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum PYRAMIDS:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum PYRAMIDS_ABOVE:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum QUADRANTS:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum RINGS:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum SAFARI:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum SAWTOOTH:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum SAWTOOTH_GRAY:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum SCARED_CAT:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum SEATTLE:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum SHADOWED_SQUARES:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum SHARKS_TEETH:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum SHOREBIRD_TRACKS:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum SINGLE:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum SKYROCKET:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum SNOWFLAKES:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum SNOWFLAKE_FANCY:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum SOMBRERO:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum SOUTHWEST:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum STARS:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum STARS_3_D:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum STARS_BLACK:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum STARS_SHADOWED:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum STARS_TOP:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum SUN:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum SWIRLIGIG:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum THICK:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum THICK_THIN_LARGE_GAP:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum THICK_THIN_MEDIUM_GAP:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum THICK_THIN_SMALL_GAP:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum THIN_THICK_LARGE_GAP:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum THIN_THICK_MEDIUM_GAP:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum THIN_THICK_SMALL_GAP:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum THIN_THICK_THIN_LARGE_GAP:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum THIN_THICK_THIN_MEDIUM_GAP:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum THIN_THICK_THIN_SMALL_GAP:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum THREE_D_EMBOSS:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum THREE_D_ENGRAVE:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum TORN_PAPER:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum TORN_PAPER_BLACK:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum TREES:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum TRIANGLES:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum TRIANGLE_PARTY:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum TRIBAL_1:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum TRIBAL_2:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum TRIBAL_3:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum TRIBAL_4:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum TRIBAL_5:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum TRIBAL_6:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum TRIPLE:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum TWISTED_LINES_1:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum TWISTED_LINES_2:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum VINE:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum WAVE:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum WAVELINE:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum WEAVING_ANGLES:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum WEAVING_BRAID:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum WEAVING_RIBBON:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum WEAVING_STRIPS:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum WHITE_FLOWERS:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum WOODWORK:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum X_ILLUSIONS:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum ZANY_TRIANGLES:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum ZIG_ZAG:Lorg/apache/poi/xwpf/usermodel/Borders;

.field public static final enum ZIG_ZAG_STITCH:Lorg/apache/poi/xwpf/usermodel/Borders;

.field private static imap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Lorg/apache/poi/xwpf/usermodel/Borders;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final value:I


# direct methods
.method public static constructor <clinit>()V
    .locals 196

    new-instance v1, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object v0, v1

    const-string v2, "NIL"

    const/4 v15, 0x0

    const/4 v3, 0x1

    invoke-direct {v1, v2, v15, v3}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lorg/apache/poi/xwpf/usermodel/Borders;->NIL:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v2, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object v1, v2

    const-string v4, "NONE"

    const/4 v5, 0x2

    invoke-direct {v2, v4, v3, v5}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lorg/apache/poi/xwpf/usermodel/Borders;->NONE:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v3, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object v2, v3

    const-string v4, "SINGLE"

    const/4 v6, 0x3

    invoke-direct {v3, v4, v5, v6}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v3, Lorg/apache/poi/xwpf/usermodel/Borders;->SINGLE:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v4, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object v3, v4

    const-string v5, "THICK"

    const/4 v7, 0x4

    invoke-direct {v4, v5, v6, v7}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lorg/apache/poi/xwpf/usermodel/Borders;->THICK:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v5, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object v4, v5

    const-string v6, "DOUBLE"

    const/4 v8, 0x5

    invoke-direct {v5, v6, v7, v8}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v5, Lorg/apache/poi/xwpf/usermodel/Borders;->DOUBLE:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v6, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object v5, v6

    const-string v7, "DOTTED"

    const/4 v9, 0x6

    invoke-direct {v6, v7, v8, v9}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v6, Lorg/apache/poi/xwpf/usermodel/Borders;->DOTTED:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v7, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object v6, v7

    const-string v8, "DASHED"

    const/4 v10, 0x7

    invoke-direct {v7, v8, v9, v10}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v7, Lorg/apache/poi/xwpf/usermodel/Borders;->DASHED:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v8, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object v7, v8

    const-string v9, "DOT_DASH"

    const/16 v11, 0x8

    invoke-direct {v8, v9, v10, v11}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v8, Lorg/apache/poi/xwpf/usermodel/Borders;->DOT_DASH:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v9, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object v8, v9

    const-string v10, "DOT_DOT_DASH"

    const/16 v12, 0x9

    invoke-direct {v9, v10, v11, v12}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v9, Lorg/apache/poi/xwpf/usermodel/Borders;->DOT_DOT_DASH:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v10, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object v9, v10

    const-string v11, "TRIPLE"

    const/16 v13, 0xa

    invoke-direct {v10, v11, v12, v13}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v10, Lorg/apache/poi/xwpf/usermodel/Borders;->TRIPLE:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v11, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object v10, v11

    const-string v12, "THIN_THICK_SMALL_GAP"

    const/16 v14, 0xb

    invoke-direct {v11, v12, v13, v14}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v11, Lorg/apache/poi/xwpf/usermodel/Borders;->THIN_THICK_SMALL_GAP:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v12, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object v11, v12

    const-string v13, "THICK_THIN_SMALL_GAP"

    const/16 v15, 0xc

    invoke-direct {v12, v13, v14, v15}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v12, Lorg/apache/poi/xwpf/usermodel/Borders;->THICK_THIN_SMALL_GAP:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v13, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object v12, v13

    const-string v14, "THIN_THICK_THIN_SMALL_GAP"

    move-object/from16 v191, v0

    const/16 v0, 0xd

    invoke-direct {v13, v14, v15, v0}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v13, Lorg/apache/poi/xwpf/usermodel/Borders;->THIN_THICK_THIN_SMALL_GAP:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v14, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object v13, v14

    const-string v15, "THIN_THICK_MEDIUM_GAP"

    move-object/from16 v192, v1

    const/16 v1, 0xe

    invoke-direct {v14, v15, v0, v1}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v14, Lorg/apache/poi/xwpf/usermodel/Borders;->THIN_THICK_MEDIUM_GAP:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object v14, v0

    const-string v15, "THICK_THIN_MEDIUM_GAP"

    move-object/from16 v193, v2

    const/16 v2, 0xf

    invoke-direct {v0, v15, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->THICK_THIN_MEDIUM_GAP:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    const/16 v194, 0x0

    move-object v15, v0

    const-string v1, "THIN_THICK_THIN_MEDIUM_GAP"

    move-object/from16 v195, v3

    const/16 v3, 0x10

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->THIN_THICK_THIN_MEDIUM_GAP:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v16, v0

    const-string v1, "THIN_THICK_LARGE_GAP"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v3, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->THIN_THICK_LARGE_GAP:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v17, v0

    const-string v1, "THICK_THIN_LARGE_GAP"

    const/16 v3, 0x12

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->THICK_THIN_LARGE_GAP:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v18, v0

    const-string v1, "THIN_THICK_THIN_LARGE_GAP"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v3, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->THIN_THICK_THIN_LARGE_GAP:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v19, v0

    const-string v1, "WAVE"

    const/16 v3, 0x14

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->WAVE:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v20, v0

    const-string v1, "DOUBLE_WAVE"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v3, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->DOUBLE_WAVE:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v21, v0

    const-string v1, "DASH_SMALL_GAP"

    const/16 v3, 0x16

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->DASH_SMALL_GAP:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v22, v0

    const/16 v1, 0x16

    const/16 v2, 0x17

    const-string v3, "DASH_DOT_STROKED"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->DASH_DOT_STROKED:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v23, v0

    const/16 v1, 0x17

    const/16 v2, 0x18

    const-string v3, "THREE_D_EMBOSS"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->THREE_D_EMBOSS:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v24, v0

    const/16 v1, 0x18

    const/16 v2, 0x19

    const-string v3, "THREE_D_ENGRAVE"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->THREE_D_ENGRAVE:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v25, v0

    const/16 v1, 0x19

    const/16 v2, 0x1a

    const-string v3, "OUTSET"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->OUTSET:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v26, v0

    const/16 v1, 0x1a

    const/16 v2, 0x1b

    const-string v3, "INSET"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->INSET:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v27, v0

    const/16 v1, 0x1b

    const/16 v2, 0x1c

    const-string v3, "APPLES"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->APPLES:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v28, v0

    const/16 v1, 0x1c

    const/16 v2, 0x1d

    const-string v3, "ARCHED_SCALLOPS"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->ARCHED_SCALLOPS:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v29, v0

    const/16 v1, 0x1d

    const/16 v2, 0x1e

    const-string v3, "BABY_PACIFIER"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->BABY_PACIFIER:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v30, v0

    const/16 v1, 0x1e

    const/16 v2, 0x1f

    const-string v3, "BABY_RATTLE"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->BABY_RATTLE:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v31, v0

    const/16 v1, 0x1f

    const/16 v2, 0x20

    const-string v3, "BALLOONS_3_COLORS"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->BALLOONS_3_COLORS:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v32, v0

    const/16 v1, 0x20

    const/16 v2, 0x21

    const-string v3, "BALLOONS_HOT_AIR"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->BALLOONS_HOT_AIR:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v33, v0

    const/16 v1, 0x21

    const/16 v2, 0x22

    const-string v3, "BASIC_BLACK_DASHES"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->BASIC_BLACK_DASHES:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v34, v0

    const/16 v1, 0x22

    const/16 v2, 0x23

    const-string v3, "BASIC_BLACK_DOTS"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->BASIC_BLACK_DOTS:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v35, v0

    const/16 v1, 0x23

    const/16 v2, 0x24

    const-string v3, "BASIC_BLACK_SQUARES"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->BASIC_BLACK_SQUARES:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v36, v0

    const/16 v1, 0x24

    const/16 v2, 0x25

    const-string v3, "BASIC_THIN_LINES"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->BASIC_THIN_LINES:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v37, v0

    const/16 v1, 0x25

    const/16 v2, 0x26

    const-string v3, "BASIC_WHITE_DASHES"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->BASIC_WHITE_DASHES:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v38, v0

    const/16 v1, 0x26

    const/16 v2, 0x27

    const-string v3, "BASIC_WHITE_DOTS"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->BASIC_WHITE_DOTS:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v39, v0

    const/16 v1, 0x27

    const/16 v2, 0x28

    const-string v3, "BASIC_WHITE_SQUARES"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->BASIC_WHITE_SQUARES:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v40, v0

    const/16 v1, 0x28

    const/16 v2, 0x29

    const-string v3, "BASIC_WIDE_INLINE"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->BASIC_WIDE_INLINE:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v41, v0

    const/16 v1, 0x29

    const/16 v2, 0x2a

    const-string v3, "BASIC_WIDE_MIDLINE"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->BASIC_WIDE_MIDLINE:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v42, v0

    const/16 v1, 0x2a

    const/16 v2, 0x2b

    const-string v3, "BASIC_WIDE_OUTLINE"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->BASIC_WIDE_OUTLINE:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v43, v0

    const/16 v1, 0x2b

    const/16 v2, 0x2c

    const-string v3, "BATS"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->BATS:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v44, v0

    const/16 v1, 0x2c

    const/16 v2, 0x2d

    const-string v3, "BIRDS"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->BIRDS:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v45, v0

    const/16 v1, 0x2d

    const/16 v2, 0x2e

    const-string v3, "BIRDS_FLIGHT"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->BIRDS_FLIGHT:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v46, v0

    const/16 v1, 0x2e

    const/16 v2, 0x2f

    const-string v3, "CABINS"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->CABINS:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v47, v0

    const/16 v1, 0x2f

    const/16 v2, 0x30

    const-string v3, "CAKE_SLICE"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->CAKE_SLICE:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v48, v0

    const/16 v1, 0x30

    const/16 v2, 0x31

    const-string v3, "CANDY_CORN"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->CANDY_CORN:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v49, v0

    const/16 v1, 0x31

    const/16 v2, 0x32

    const-string v3, "CELTIC_KNOTWORK"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->CELTIC_KNOTWORK:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v50, v0

    const/16 v1, 0x32

    const/16 v2, 0x33

    const-string v3, "CERTIFICATE_BANNER"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->CERTIFICATE_BANNER:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v51, v0

    const/16 v1, 0x33

    const/16 v2, 0x34

    const-string v3, "CHAIN_LINK"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->CHAIN_LINK:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v52, v0

    const/16 v1, 0x34

    const/16 v2, 0x35

    const-string v3, "CHAMPAGNE_BOTTLE"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->CHAMPAGNE_BOTTLE:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v53, v0

    const/16 v1, 0x35

    const/16 v2, 0x36

    const-string v3, "CHECKED_BAR_BLACK"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->CHECKED_BAR_BLACK:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v54, v0

    const/16 v1, 0x36

    const/16 v2, 0x37

    const-string v3, "CHECKED_BAR_COLOR"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->CHECKED_BAR_COLOR:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v55, v0

    const/16 v1, 0x37

    const/16 v2, 0x38

    const-string v3, "CHECKERED"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->CHECKERED:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v56, v0

    const/16 v1, 0x38

    const/16 v2, 0x39

    const-string v3, "CHRISTMAS_TREE"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->CHRISTMAS_TREE:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v57, v0

    const/16 v1, 0x39

    const/16 v2, 0x3a

    const-string v3, "CIRCLES_LINES"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->CIRCLES_LINES:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v58, v0

    const/16 v1, 0x3a

    const/16 v2, 0x3b

    const-string v3, "CIRCLES_RECTANGLES"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->CIRCLES_RECTANGLES:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v59, v0

    const/16 v1, 0x3b

    const/16 v2, 0x3c

    const-string v3, "CLASSICAL_WAVE"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->CLASSICAL_WAVE:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v60, v0

    const/16 v1, 0x3c

    const/16 v2, 0x3d

    const-string v3, "CLOCKS"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->CLOCKS:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v61, v0

    const/16 v1, 0x3d

    const/16 v2, 0x3e

    const-string v3, "COMPASS"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->COMPASS:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v62, v0

    const/16 v1, 0x3e

    const/16 v2, 0x3f

    const-string v3, "CONFETTI"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->CONFETTI:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v63, v0

    const/16 v1, 0x3f

    const/16 v2, 0x40

    const-string v3, "CONFETTI_GRAYS"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->CONFETTI_GRAYS:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v64, v0

    const/16 v1, 0x40

    const/16 v2, 0x41

    const-string v3, "CONFETTI_OUTLINE"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->CONFETTI_OUTLINE:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v65, v0

    const/16 v1, 0x41

    const/16 v2, 0x42

    const-string v3, "CONFETTI_STREAMERS"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->CONFETTI_STREAMERS:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v66, v0

    const/16 v1, 0x42

    const/16 v2, 0x43

    const-string v3, "CONFETTI_WHITE"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->CONFETTI_WHITE:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v67, v0

    const/16 v1, 0x43

    const/16 v2, 0x44

    const-string v3, "CORNER_TRIANGLES"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->CORNER_TRIANGLES:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v68, v0

    const/16 v1, 0x44

    const/16 v2, 0x45

    const-string v3, "COUPON_CUTOUT_DASHES"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->COUPON_CUTOUT_DASHES:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v69, v0

    const/16 v1, 0x45

    const/16 v2, 0x46

    const-string v3, "COUPON_CUTOUT_DOTS"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->COUPON_CUTOUT_DOTS:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v70, v0

    const/16 v1, 0x46

    const/16 v2, 0x47

    const-string v3, "CRAZY_MAZE"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->CRAZY_MAZE:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v71, v0

    const/16 v1, 0x47

    const/16 v2, 0x48

    const-string v3, "CREATURES_BUTTERFLY"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->CREATURES_BUTTERFLY:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v72, v0

    const/16 v1, 0x48

    const/16 v2, 0x49

    const-string v3, "CREATURES_FISH"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->CREATURES_FISH:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v73, v0

    const/16 v1, 0x49

    const/16 v2, 0x4a

    const-string v3, "CREATURES_INSECTS"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->CREATURES_INSECTS:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v74, v0

    const/16 v1, 0x4a

    const/16 v2, 0x4b

    const-string v3, "CREATURES_LADY_BUG"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->CREATURES_LADY_BUG:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v75, v0

    const/16 v1, 0x4b

    const/16 v2, 0x4c

    const-string v3, "CROSS_STITCH"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->CROSS_STITCH:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v76, v0

    const/16 v1, 0x4c

    const/16 v2, 0x4d

    const-string v3, "CUP"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->CUP:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v77, v0

    const/16 v1, 0x4d

    const/16 v2, 0x4e

    const-string v3, "DECO_ARCH"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->DECO_ARCH:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v78, v0

    const/16 v1, 0x4e

    const/16 v2, 0x4f

    const-string v3, "DECO_ARCH_COLOR"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->DECO_ARCH_COLOR:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v79, v0

    const/16 v1, 0x4f

    const/16 v2, 0x50

    const-string v3, "DECO_BLOCKS"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->DECO_BLOCKS:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v80, v0

    const/16 v1, 0x50

    const/16 v2, 0x51

    const-string v3, "DIAMONDS_GRAY"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->DIAMONDS_GRAY:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v81, v0

    const/16 v1, 0x51

    const/16 v2, 0x52

    const-string v3, "DOUBLE_D"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->DOUBLE_D:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v82, v0

    const/16 v1, 0x52

    const/16 v2, 0x53

    const-string v3, "DOUBLE_DIAMONDS"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->DOUBLE_DIAMONDS:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v83, v0

    const/16 v1, 0x53

    const/16 v2, 0x54

    const-string v3, "EARTH_1"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->EARTH_1:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v84, v0

    const/16 v1, 0x54

    const/16 v2, 0x55

    const-string v3, "EARTH_2"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->EARTH_2:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v85, v0

    const/16 v1, 0x55

    const/16 v2, 0x56

    const-string v3, "ECLIPSING_SQUARES_1"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->ECLIPSING_SQUARES_1:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v86, v0

    const/16 v1, 0x56

    const/16 v2, 0x57

    const-string v3, "ECLIPSING_SQUARES_2"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->ECLIPSING_SQUARES_2:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v87, v0

    const/16 v1, 0x57

    const/16 v2, 0x58

    const-string v3, "EGGS_BLACK"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->EGGS_BLACK:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v88, v0

    const/16 v1, 0x58

    const/16 v2, 0x59

    const-string v3, "FANS"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->FANS:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v89, v0

    const/16 v1, 0x59

    const/16 v2, 0x5a

    const-string v3, "FILM"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->FILM:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v90, v0

    const/16 v1, 0x5a

    const/16 v2, 0x5b

    const-string v3, "FIRECRACKERS"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->FIRECRACKERS:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v91, v0

    const/16 v1, 0x5b

    const/16 v2, 0x5c

    const-string v3, "FLOWERS_BLOCK_PRINT"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->FLOWERS_BLOCK_PRINT:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v92, v0

    const/16 v1, 0x5c

    const/16 v2, 0x5d

    const-string v3, "FLOWERS_DAISIES"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->FLOWERS_DAISIES:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v93, v0

    const/16 v1, 0x5d

    const/16 v2, 0x5e

    const-string v3, "FLOWERS_MODERN_1"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->FLOWERS_MODERN_1:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v94, v0

    const/16 v1, 0x5e

    const/16 v2, 0x5f

    const-string v3, "FLOWERS_MODERN_2"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->FLOWERS_MODERN_2:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v95, v0

    const/16 v1, 0x5f

    const/16 v2, 0x60

    const-string v3, "FLOWERS_PANSY"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->FLOWERS_PANSY:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v96, v0

    const/16 v1, 0x60

    const/16 v2, 0x61

    const-string v3, "FLOWERS_RED_ROSE"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->FLOWERS_RED_ROSE:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v97, v0

    const/16 v1, 0x61

    const/16 v2, 0x62

    const-string v3, "FLOWERS_ROSES"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->FLOWERS_ROSES:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v98, v0

    const/16 v1, 0x62

    const/16 v2, 0x63

    const-string v3, "FLOWERS_TEACUP"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->FLOWERS_TEACUP:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v99, v0

    const/16 v1, 0x63

    const/16 v2, 0x64

    const-string v3, "FLOWERS_TINY"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->FLOWERS_TINY:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v100, v0

    const/16 v1, 0x64

    const/16 v2, 0x65

    const-string v3, "GEMS"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->GEMS:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v101, v0

    const/16 v1, 0x65

    const/16 v2, 0x66

    const-string v3, "GINGERBREAD_MAN"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->GINGERBREAD_MAN:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v102, v0

    const/16 v1, 0x66

    const/16 v2, 0x67

    const-string v3, "GRADIENT"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->GRADIENT:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v103, v0

    const/16 v1, 0x67

    const/16 v2, 0x68

    const-string v3, "HANDMADE_1"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->HANDMADE_1:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v104, v0

    const/16 v1, 0x68

    const/16 v2, 0x69

    const-string v3, "HANDMADE_2"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->HANDMADE_2:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v105, v0

    const/16 v1, 0x69

    const/16 v2, 0x6a

    const-string v3, "HEART_BALLOON"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->HEART_BALLOON:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v106, v0

    const/16 v1, 0x6a

    const/16 v2, 0x6b

    const-string v3, "HEART_GRAY"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->HEART_GRAY:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v107, v0

    const/16 v1, 0x6b

    const/16 v2, 0x6c

    const-string v3, "HEARTS"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->HEARTS:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v108, v0

    const/16 v1, 0x6c

    const/16 v2, 0x6d

    const-string v3, "HEEBIE_JEEBIES"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->HEEBIE_JEEBIES:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v109, v0

    const/16 v1, 0x6d

    const/16 v2, 0x6e

    const-string v3, "HOLLY"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->HOLLY:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v110, v0

    const/16 v1, 0x6e

    const/16 v2, 0x6f

    const-string v3, "HOUSE_FUNKY"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->HOUSE_FUNKY:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v111, v0

    const/16 v1, 0x6f

    const/16 v2, 0x70

    const-string v3, "HYPNOTIC"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->HYPNOTIC:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v112, v0

    const/16 v1, 0x70

    const/16 v2, 0x71

    const-string v3, "ICE_CREAM_CONES"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->ICE_CREAM_CONES:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v113, v0

    const/16 v1, 0x71

    const/16 v2, 0x72

    const-string v3, "LIGHT_BULB"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->LIGHT_BULB:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v114, v0

    const/16 v1, 0x72

    const/16 v2, 0x73

    const-string v3, "LIGHTNING_1"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->LIGHTNING_1:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v115, v0

    const/16 v1, 0x73

    const/16 v2, 0x74

    const-string v3, "LIGHTNING_2"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->LIGHTNING_2:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v116, v0

    const/16 v1, 0x74

    const/16 v2, 0x75

    const-string v3, "MAP_PINS"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->MAP_PINS:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v117, v0

    const/16 v1, 0x75

    const/16 v2, 0x76

    const-string v3, "MAPLE_LEAF"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->MAPLE_LEAF:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v118, v0

    const/16 v1, 0x76

    const/16 v2, 0x77

    const-string v3, "MAPLE_MUFFINS"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->MAPLE_MUFFINS:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v119, v0

    const/16 v1, 0x77

    const/16 v2, 0x78

    const-string v3, "MARQUEE"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->MARQUEE:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v120, v0

    const/16 v1, 0x78

    const/16 v2, 0x79

    const-string v3, "MARQUEE_TOOTHED"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->MARQUEE_TOOTHED:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v121, v0

    const/16 v1, 0x79

    const/16 v2, 0x7a

    const-string v3, "MOONS"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->MOONS:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v122, v0

    const/16 v1, 0x7a

    const/16 v2, 0x7b

    const-string v3, "MOSAIC"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->MOSAIC:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v123, v0

    const/16 v1, 0x7b

    const/16 v2, 0x7c

    const-string v3, "MUSIC_NOTES"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->MUSIC_NOTES:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v124, v0

    const/16 v1, 0x7c

    const/16 v2, 0x7d

    const-string v3, "NORTHWEST"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->NORTHWEST:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v125, v0

    const/16 v1, 0x7d

    const/16 v2, 0x7e

    const-string v3, "OVALS"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->OVALS:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v126, v0

    const/16 v1, 0x7e

    const/16 v2, 0x7f

    const-string v3, "PACKAGES"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->PACKAGES:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v127, v0

    const/16 v1, 0x7f

    const/16 v2, 0x80

    const-string v3, "PALMS_BLACK"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->PALMS_BLACK:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v128, v0

    const/16 v1, 0x80

    const/16 v2, 0x81

    const-string v3, "PALMS_COLOR"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->PALMS_COLOR:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v129, v0

    const/16 v1, 0x81

    const/16 v2, 0x82

    const-string v3, "PAPER_CLIPS"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->PAPER_CLIPS:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v130, v0

    const/16 v1, 0x82

    const/16 v2, 0x83

    const-string v3, "PAPYRUS"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->PAPYRUS:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v131, v0

    const/16 v1, 0x83

    const/16 v2, 0x84

    const-string v3, "PARTY_FAVOR"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->PARTY_FAVOR:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v132, v0

    const/16 v1, 0x84

    const/16 v2, 0x85

    const-string v3, "PARTY_GLASS"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->PARTY_GLASS:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v133, v0

    const/16 v1, 0x85

    const/16 v2, 0x86

    const-string v3, "PENCILS"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->PENCILS:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v134, v0

    const/16 v1, 0x86

    const/16 v2, 0x87

    const-string v3, "PEOPLE"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->PEOPLE:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v135, v0

    const/16 v1, 0x87

    const/16 v2, 0x88

    const-string v3, "PEOPLE_WAVING"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->PEOPLE_WAVING:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v136, v0

    const/16 v1, 0x88

    const/16 v2, 0x89

    const-string v3, "PEOPLE_HATS"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->PEOPLE_HATS:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v137, v0

    const/16 v1, 0x89

    const/16 v2, 0x8a

    const-string v3, "POINSETTIAS"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->POINSETTIAS:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v138, v0

    const/16 v1, 0x8a

    const/16 v2, 0x8b

    const-string v3, "POSTAGE_STAMP"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->POSTAGE_STAMP:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v139, v0

    const/16 v1, 0x8b

    const/16 v2, 0x8c

    const-string v3, "PUMPKIN_1"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->PUMPKIN_1:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v140, v0

    const/16 v1, 0x8c

    const/16 v2, 0x8d

    const-string v3, "PUSH_PIN_NOTE_2"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->PUSH_PIN_NOTE_2:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v141, v0

    const/16 v1, 0x8d

    const/16 v2, 0x8e

    const-string v3, "PUSH_PIN_NOTE_1"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->PUSH_PIN_NOTE_1:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v142, v0

    const/16 v1, 0x8e

    const/16 v2, 0x8f

    const-string v3, "PYRAMIDS"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->PYRAMIDS:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v143, v0

    const/16 v1, 0x8f

    const/16 v2, 0x90

    const-string v3, "PYRAMIDS_ABOVE"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->PYRAMIDS_ABOVE:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v144, v0

    const/16 v1, 0x90

    const/16 v2, 0x91

    const-string v3, "QUADRANTS"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->QUADRANTS:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v145, v0

    const/16 v1, 0x91

    const/16 v2, 0x92

    const-string v3, "RINGS"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->RINGS:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v146, v0

    const/16 v1, 0x92

    const/16 v2, 0x93

    const-string v3, "SAFARI"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->SAFARI:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v147, v0

    const/16 v1, 0x93

    const/16 v2, 0x94

    const-string v3, "SAWTOOTH"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->SAWTOOTH:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v148, v0

    const/16 v1, 0x94

    const/16 v2, 0x95

    const-string v3, "SAWTOOTH_GRAY"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->SAWTOOTH_GRAY:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v149, v0

    const/16 v1, 0x95

    const/16 v2, 0x96

    const-string v3, "SCARED_CAT"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->SCARED_CAT:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v150, v0

    const/16 v1, 0x96

    const/16 v2, 0x97

    const-string v3, "SEATTLE"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->SEATTLE:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v151, v0

    const/16 v1, 0x97

    const/16 v2, 0x98

    const-string v3, "SHADOWED_SQUARES"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->SHADOWED_SQUARES:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v152, v0

    const/16 v1, 0x98

    const/16 v2, 0x99

    const-string v3, "SHARKS_TEETH"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->SHARKS_TEETH:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v153, v0

    const/16 v1, 0x99

    const/16 v2, 0x9a

    const-string v3, "SHOREBIRD_TRACKS"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->SHOREBIRD_TRACKS:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v154, v0

    const/16 v1, 0x9a

    const/16 v2, 0x9b

    const-string v3, "SKYROCKET"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->SKYROCKET:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v155, v0

    const/16 v1, 0x9b

    const/16 v2, 0x9c

    const-string v3, "SNOWFLAKE_FANCY"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->SNOWFLAKE_FANCY:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v156, v0

    const/16 v1, 0x9c

    const/16 v2, 0x9d

    const-string v3, "SNOWFLAKES"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->SNOWFLAKES:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v157, v0

    const/16 v1, 0x9d

    const/16 v2, 0x9e

    const-string v3, "SOMBRERO"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->SOMBRERO:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v158, v0

    const/16 v1, 0x9e

    const/16 v2, 0x9f

    const-string v3, "SOUTHWEST"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->SOUTHWEST:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v159, v0

    const/16 v1, 0x9f

    const/16 v2, 0xa0

    const-string v3, "STARS"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->STARS:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v160, v0

    const/16 v1, 0xa0

    const/16 v2, 0xa1

    const-string v3, "STARS_TOP"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->STARS_TOP:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v161, v0

    const/16 v1, 0xa1

    const/16 v2, 0xa2

    const-string v3, "STARS_3_D"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->STARS_3_D:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v162, v0

    const/16 v1, 0xa2

    const/16 v2, 0xa3

    const-string v3, "STARS_BLACK"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->STARS_BLACK:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v163, v0

    const/16 v1, 0xa3

    const/16 v2, 0xa4

    const-string v3, "STARS_SHADOWED"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->STARS_SHADOWED:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v164, v0

    const/16 v1, 0xa4

    const/16 v2, 0xa5

    const-string v3, "SUN"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->SUN:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v165, v0

    const/16 v1, 0xa5

    const/16 v2, 0xa6

    const-string v3, "SWIRLIGIG"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->SWIRLIGIG:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v166, v0

    const/16 v1, 0xa6

    const/16 v2, 0xa7

    const-string v3, "TORN_PAPER"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->TORN_PAPER:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v167, v0

    const/16 v1, 0xa7

    const/16 v2, 0xa8

    const-string v3, "TORN_PAPER_BLACK"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->TORN_PAPER_BLACK:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v168, v0

    const/16 v1, 0xa8

    const/16 v2, 0xa9

    const-string v3, "TREES"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->TREES:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v169, v0

    const/16 v1, 0xa9

    const/16 v2, 0xaa

    const-string v3, "TRIANGLE_PARTY"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->TRIANGLE_PARTY:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v170, v0

    const/16 v1, 0xaa

    const/16 v2, 0xab

    const-string v3, "TRIANGLES"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->TRIANGLES:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v171, v0

    const/16 v1, 0xab

    const/16 v2, 0xac

    const-string v3, "TRIBAL_1"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->TRIBAL_1:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v172, v0

    const/16 v1, 0xac

    const/16 v2, 0xad

    const-string v3, "TRIBAL_2"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->TRIBAL_2:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v173, v0

    const/16 v1, 0xad

    const/16 v2, 0xae

    const-string v3, "TRIBAL_3"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->TRIBAL_3:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v174, v0

    const/16 v1, 0xae

    const/16 v2, 0xaf

    const-string v3, "TRIBAL_4"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->TRIBAL_4:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v175, v0

    const/16 v1, 0xaf

    const/16 v2, 0xb0

    const-string v3, "TRIBAL_5"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->TRIBAL_5:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v176, v0

    const/16 v1, 0xb0

    const/16 v2, 0xb1

    const-string v3, "TRIBAL_6"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->TRIBAL_6:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v177, v0

    const/16 v1, 0xb1

    const/16 v2, 0xb2

    const-string v3, "TWISTED_LINES_1"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->TWISTED_LINES_1:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v178, v0

    const/16 v1, 0xb2

    const/16 v2, 0xb3

    const-string v3, "TWISTED_LINES_2"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->TWISTED_LINES_2:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v179, v0

    const/16 v1, 0xb3

    const/16 v2, 0xb4

    const-string v3, "VINE"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->VINE:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v180, v0

    const/16 v1, 0xb4

    const/16 v2, 0xb5

    const-string v3, "WAVELINE"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->WAVELINE:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v181, v0

    const/16 v1, 0xb5

    const/16 v2, 0xb6

    const-string v3, "WEAVING_ANGLES"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->WEAVING_ANGLES:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v182, v0

    const/16 v1, 0xb6

    const/16 v2, 0xb7

    const-string v3, "WEAVING_BRAID"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->WEAVING_BRAID:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v183, v0

    const/16 v1, 0xb7

    const/16 v2, 0xb8

    const-string v3, "WEAVING_RIBBON"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->WEAVING_RIBBON:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v184, v0

    const/16 v1, 0xb8

    const/16 v2, 0xb9

    const-string v3, "WEAVING_STRIPS"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->WEAVING_STRIPS:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v185, v0

    const/16 v1, 0xb9

    const/16 v2, 0xba

    const-string v3, "WHITE_FLOWERS"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->WHITE_FLOWERS:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v186, v0

    const/16 v1, 0xba

    const/16 v2, 0xbb

    const-string v3, "WOODWORK"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->WOODWORK:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v187, v0

    const/16 v1, 0xbb

    const/16 v2, 0xbc

    const-string v3, "X_ILLUSIONS"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->X_ILLUSIONS:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v188, v0

    const/16 v1, 0xbc

    const/16 v2, 0xbd

    const-string v3, "ZANY_TRIANGLES"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->ZANY_TRIANGLES:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v189, v0

    const/16 v1, 0xbd

    const/16 v2, 0xbe

    const-string v3, "ZIG_ZAG"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->ZIG_ZAG:Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v190, v0

    const/16 v1, 0xbe

    const/16 v2, 0xbf

    const-string v3, "ZIG_ZAG_STITCH"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/xwpf/usermodel/Borders;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->ZIG_ZAG_STITCH:Lorg/apache/poi/xwpf/usermodel/Borders;

    move-object/from16 v0, v191

    move-object/from16 v1, v192

    move-object/from16 v2, v193

    move-object/from16 v3, v195

    filled-new-array/range {v0 .. v190}, [Lorg/apache/poi/xwpf/usermodel/Borders;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->$VALUES:[Lorg/apache/poi/xwpf/usermodel/Borders;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->imap:Ljava/util/Map;

    invoke-static {}, Lorg/apache/poi/xwpf/usermodel/Borders;->values()[Lorg/apache/poi/xwpf/usermodel/Borders;

    move-result-object v0

    array-length v1, v0

    move/from16 v15, v194

    :goto_0
    if-ge v15, v1, :cond_0

    aget-object v2, v0, v15

    sget-object v3, Lorg/apache/poi/xwpf/usermodel/Borders;->imap:Ljava/util/Map;

    invoke-virtual {v2}, Lorg/apache/poi/xwpf/usermodel/Borders;->getValue()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v15, v15, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lorg/apache/poi/xwpf/usermodel/Borders;->value:I

    return-void
.end method

.method public static valueOf(I)Lorg/apache/poi/xwpf/usermodel/Borders;
    .locals 3

    .line 1
    sget-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->imap:Ljava/util/Map;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    if-eqz v0, :cond_0

    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown paragraph border: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static valueOf(Ljava/lang/String;)Lorg/apache/poi/xwpf/usermodel/Borders;
    .locals 1

    .line 2
    const-class v0, Lorg/apache/poi/xwpf/usermodel/Borders;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lorg/apache/poi/xwpf/usermodel/Borders;

    return-object p0
.end method

.method public static values()[Lorg/apache/poi/xwpf/usermodel/Borders;
    .locals 1

    sget-object v0, Lorg/apache/poi/xwpf/usermodel/Borders;->$VALUES:[Lorg/apache/poi/xwpf/usermodel/Borders;

    invoke-virtual {v0}, [Lorg/apache/poi/xwpf/usermodel/Borders;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lorg/apache/poi/xwpf/usermodel/Borders;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    iget v0, p0, Lorg/apache/poi/xwpf/usermodel/Borders;->value:I

    return v0
.end method

.class public Lorg/apache/xmlbeans/impl/xpath/saxon/XBeansXPath;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/xmlbeans/impl/store/PathDelegate$SelectPathInterface;


# instance fields
.field private contextVar:Ljava/lang/String;

.field private defaultNS:Ljava/lang/String;

.field private namespaceMap:[Ljava/lang/Object;

.field private path:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lorg/apache/xmlbeans/impl/xpath/saxon/XBeansXPath;->path:Ljava/lang/String;

    iput-object p2, p0, Lorg/apache/xmlbeans/impl/xpath/saxon/XBeansXPath;->contextVar:Ljava/lang/String;

    iput-object p4, p0, Lorg/apache/xmlbeans/impl/xpath/saxon/XBeansXPath;->defaultNS:Ljava/lang/String;

    invoke-interface {p3}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Set;->toArray()[Ljava/lang/Object;

    move-result-object p1

    iput-object p1, p0, Lorg/apache/xmlbeans/impl/xpath/saxon/XBeansXPath;->namespaceMap:[Ljava/lang/Object;

    return-void
.end method

.method private static getUnderlyingNode(Lnet/sf/saxon/om/VirtualNode;)Lorg/w3c/dom/Node;
    .locals 1

    :goto_0
    instance-of v0, p0, Lnet/sf/saxon/om/VirtualNode;

    if-eqz v0, :cond_0

    check-cast p0, Lnet/sf/saxon/om/VirtualNode;

    invoke-interface {p0}, Lnet/sf/saxon/om/VirtualNode;->getUnderlyingNode()Ljava/lang/Object;

    move-result-object p0

    goto :goto_0

    :cond_0
    check-cast p0, Lorg/w3c/dom/Node;

    return-object p0
.end method


# virtual methods
.method public selectNodes(Ljava/lang/Object;)Ljava/util/List;
    .locals 6

    :try_start_0
    check-cast p1, Lorg/w3c/dom/Node;

    new-instance v0, Lnet/sf/saxon/sxpath/XPathEvaluator;

    invoke-direct {v0}, Lnet/sf/saxon/sxpath/XPathEvaluator;-><init>()V

    new-instance v1, Lnet/sf/saxon/Configuration;

    invoke-direct {v1}, Lnet/sf/saxon/Configuration;-><init>()V

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lnet/sf/saxon/Configuration;->setDOMLevel(I)V

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lnet/sf/saxon/Configuration;->setTreeModel(I)V

    new-instance v3, Lnet/sf/saxon/sxpath/IndependentContext;

    invoke-direct {v3, v1}, Lnet/sf/saxon/sxpath/IndependentContext;-><init>(Lnet/sf/saxon/Configuration;)V

    iget-object v4, p0, Lorg/apache/xmlbeans/impl/xpath/saxon/XBeansXPath;->defaultNS:Ljava/lang/String;

    if-eqz v4, :cond_0

    invoke-virtual {v3, v4}, Lnet/sf/saxon/sxpath/IndependentContext;->setDefaultElementNamespace(Ljava/lang/String;)V

    :cond_0
    :goto_0
    iget-object v4, p0, Lorg/apache/xmlbeans/impl/xpath/saxon/XBeansXPath;->namespaceMap:[Ljava/lang/Object;

    array-length v5, v4

    if-ge v2, v5, :cond_1

    aget-object v4, v4, v2

    check-cast v4, Ljava/util/Map$Entry;

    invoke-interface {v4}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v3, v5, v4}, Lnet/sf/saxon/sxpath/IndependentContext;->declareNamespace(Ljava/lang/String;Ljava/lang/String;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {v0, v3}, Lnet/sf/saxon/sxpath/XPathEvaluator;->setStaticContext(Lnet/sf/saxon/sxpath/XPathStaticContext;)V

    const-string v2, ""

    iget-object v3, p0, Lorg/apache/xmlbeans/impl/xpath/saxon/XBeansXPath;->contextVar:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Lnet/sf/saxon/sxpath/XPathEvaluator;->declareVariable(Ljava/lang/String;Ljava/lang/String;)Lnet/sf/saxon/sxpath/XPathVariable;

    move-result-object v2

    iget-object v3, p0, Lorg/apache/xmlbeans/impl/xpath/saxon/XBeansXPath;->path:Ljava/lang/String;

    invoke-virtual {v0, v3}, Lnet/sf/saxon/sxpath/XPathEvaluator;->createExpression(Ljava/lang/String;)Lnet/sf/saxon/sxpath/XPathExpression;

    move-result-object v0

    new-instance v3, Ljavax/xml/transform/dom/DOMSource;

    invoke-direct {v3, p1}, Ljavax/xml/transform/dom/DOMSource;-><init>(Lorg/w3c/dom/Node;)V

    invoke-virtual {v1, v3}, Lnet/sf/saxon/Configuration;->unravel(Ljavax/xml/transform/Source;)Lnet/sf/saxon/om/NodeInfo;

    move-result-object p1

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lnet/sf/saxon/sxpath/XPathExpression;->createDynamicContext(Lnet/sf/saxon/om/Item;)Lnet/sf/saxon/sxpath/XPathDynamicContext;

    move-result-object v1

    invoke-virtual {v1, p1}, Lnet/sf/saxon/sxpath/XPathDynamicContext;->setContextItem(Lnet/sf/saxon/om/Item;)V

    invoke-virtual {v1, v2, p1}, Lnet/sf/saxon/sxpath/XPathDynamicContext;->setVariable(Lnet/sf/saxon/sxpath/XPathVariable;Lnet/sf/saxon/om/ValueRepresentation;)V

    invoke-virtual {v0, v1}, Lnet/sf/saxon/sxpath/XPathExpression;->evaluate(Lnet/sf/saxon/sxpath/XPathDynamicContext;)Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->listIterator()Ljava/util/ListIterator;

    move-result-object v0

    :cond_2
    :goto_1
    invoke-interface {v0}, Ljava/util/ListIterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {v0}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v1

    instance-of v2, v1, Lnet/sf/saxon/om/NodeInfo;

    if-eqz v2, :cond_4

    instance-of v2, v1, Lnet/sf/saxon/dom/NodeWrapper;

    if-eqz v2, :cond_3

    check-cast v1, Lnet/sf/saxon/dom/NodeWrapper;

    invoke-static {v1}, Lorg/apache/xmlbeans/impl/xpath/saxon/XBeansXPath;->getUnderlyingNode(Lnet/sf/saxon/om/VirtualNode;)Lorg/w3c/dom/Node;

    move-result-object v1

    :goto_2
    invoke-interface {v0, v1}, Ljava/util/ListIterator;->set(Ljava/lang/Object;)V

    goto :goto_1

    :cond_3
    check-cast v1, Lnet/sf/saxon/om/NodeInfo;

    invoke-interface {v1}, Lnet/sf/saxon/om/NodeInfo;->getStringValue()Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    :cond_4
    instance-of v2, v1, Lnet/sf/saxon/om/Item;

    if-eqz v2, :cond_2

    check-cast v1, Lnet/sf/saxon/om/Item;

    invoke-static {v1}, Lnet/sf/saxon/value/Value;->convertToJava(Lnet/sf/saxon/om/Item;)Ljava/lang/Object;

    move-result-object v1
    :try_end_0
    .catch Ljavax/xml/transform/TransformerException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    :cond_5
    return-object p1

    :catch_0
    move-exception p1

    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {v0, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v0
.end method

.method public selectPath(Ljava/lang/Object;)Ljava/util/List;
    .locals 0

    invoke-virtual {p0, p1}, Lorg/apache/xmlbeans/impl/xpath/saxon/XBeansXPath;->selectNodes(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

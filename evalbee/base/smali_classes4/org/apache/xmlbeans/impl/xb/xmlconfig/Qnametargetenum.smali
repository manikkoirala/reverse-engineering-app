.class public interface abstract Lorg/apache/xmlbeans/impl/xb/xmlconfig/Qnametargetenum;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/xmlbeans/XmlToken;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/xmlbeans/impl/xb/xmlconfig/Qnametargetenum$Factory;,
        Lorg/apache/xmlbeans/impl/xb/xmlconfig/Qnametargetenum$Enum;
    }
.end annotation


# static fields
.field public static final ACCESSOR_ATTRIBUTE:Lorg/apache/xmlbeans/impl/xb/xmlconfig/Qnametargetenum$Enum;

.field public static final ACCESSOR_ELEMENT:Lorg/apache/xmlbeans/impl/xb/xmlconfig/Qnametargetenum$Enum;

.field public static final DOCUMENT_TYPE:Lorg/apache/xmlbeans/impl/xb/xmlconfig/Qnametargetenum$Enum;

.field public static final INT_ACCESSOR_ATTRIBUTE:I = 0x4

.field public static final INT_ACCESSOR_ELEMENT:I = 0x3

.field public static final INT_DOCUMENT_TYPE:I = 0x2

.field public static final INT_TYPE:I = 0x1

.field public static final TYPE:Lorg/apache/xmlbeans/impl/xb/xmlconfig/Qnametargetenum$Enum;

.field public static final type:Lorg/apache/xmlbeans/SchemaType;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-class v0, Lorg/apache/xmlbeans/impl/xb/xmlconfig/Qnametargetenum;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    const-string v1, "schemaorg_apache_xmlbeans.system.sXMLCONFIG"

    invoke-static {v0, v1}, Lorg/apache/xmlbeans/XmlBeans;->typeSystemForClassLoader(Ljava/lang/ClassLoader;Ljava/lang/String;)Lorg/apache/xmlbeans/SchemaTypeSystem;

    move-result-object v0

    const-string v1, "qnametargetenum9f8ftype"

    invoke-interface {v0, v1}, Lorg/apache/xmlbeans/SchemaTypeSystem;->resolveHandle(Ljava/lang/String;)Lorg/apache/xmlbeans/SchemaComponent;

    move-result-object v0

    check-cast v0, Lorg/apache/xmlbeans/SchemaType;

    sput-object v0, Lorg/apache/xmlbeans/impl/xb/xmlconfig/Qnametargetenum;->type:Lorg/apache/xmlbeans/SchemaType;

    const-string v0, "type"

    invoke-static {v0}, Lorg/apache/xmlbeans/impl/xb/xmlconfig/Qnametargetenum$Enum;->forString(Ljava/lang/String;)Lorg/apache/xmlbeans/impl/xb/xmlconfig/Qnametargetenum$Enum;

    move-result-object v0

    sput-object v0, Lorg/apache/xmlbeans/impl/xb/xmlconfig/Qnametargetenum;->TYPE:Lorg/apache/xmlbeans/impl/xb/xmlconfig/Qnametargetenum$Enum;

    const-string v0, "document-type"

    invoke-static {v0}, Lorg/apache/xmlbeans/impl/xb/xmlconfig/Qnametargetenum$Enum;->forString(Ljava/lang/String;)Lorg/apache/xmlbeans/impl/xb/xmlconfig/Qnametargetenum$Enum;

    move-result-object v0

    sput-object v0, Lorg/apache/xmlbeans/impl/xb/xmlconfig/Qnametargetenum;->DOCUMENT_TYPE:Lorg/apache/xmlbeans/impl/xb/xmlconfig/Qnametargetenum$Enum;

    const-string v0, "accessor-element"

    invoke-static {v0}, Lorg/apache/xmlbeans/impl/xb/xmlconfig/Qnametargetenum$Enum;->forString(Ljava/lang/String;)Lorg/apache/xmlbeans/impl/xb/xmlconfig/Qnametargetenum$Enum;

    move-result-object v0

    sput-object v0, Lorg/apache/xmlbeans/impl/xb/xmlconfig/Qnametargetenum;->ACCESSOR_ELEMENT:Lorg/apache/xmlbeans/impl/xb/xmlconfig/Qnametargetenum$Enum;

    const-string v0, "accessor-attribute"

    invoke-static {v0}, Lorg/apache/xmlbeans/impl/xb/xmlconfig/Qnametargetenum$Enum;->forString(Ljava/lang/String;)Lorg/apache/xmlbeans/impl/xb/xmlconfig/Qnametargetenum$Enum;

    move-result-object v0

    sput-object v0, Lorg/apache/xmlbeans/impl/xb/xmlconfig/Qnametargetenum;->ACCESSOR_ATTRIBUTE:Lorg/apache/xmlbeans/impl/xb/xmlconfig/Qnametargetenum$Enum;

    return-void
.end method


# virtual methods
.method public abstract enumValue()Lorg/apache/xmlbeans/StringEnumAbstractBase;
.end method

.method public abstract set(Lorg/apache/xmlbeans/StringEnumAbstractBase;)V
.end method

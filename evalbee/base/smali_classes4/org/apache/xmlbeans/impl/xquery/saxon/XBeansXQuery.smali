.class public Lorg/apache/xmlbeans/impl/xquery/saxon/XBeansXQuery;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/xmlbeans/impl/store/QueryDelegate$QueryInterface;


# instance fields
.field private config:Lnet/sf/saxon/Configuration;

.field private contextVar:Ljava/lang/String;

.field private xquery:Lnet/sf/saxon/query/XQueryExpression;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Lorg/apache/xmlbeans/XmlOptions;)V
    .locals 5

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lnet/sf/saxon/Configuration;

    invoke-direct {v0}, Lnet/sf/saxon/Configuration;-><init>()V

    iput-object v0, p0, Lorg/apache/xmlbeans/impl/xquery/saxon/XBeansXQuery;->config:Lnet/sf/saxon/Configuration;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lnet/sf/saxon/Configuration;->setDOMLevel(I)V

    iget-object v0, p0, Lorg/apache/xmlbeans/impl/xquery/saxon/XBeansXQuery;->config:Lnet/sf/saxon/Configuration;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lnet/sf/saxon/Configuration;->setTreeModel(I)V

    new-instance v0, Lnet/sf/saxon/query/StaticQueryContext;

    iget-object v2, p0, Lorg/apache/xmlbeans/impl/xquery/saxon/XBeansXQuery;->config:Lnet/sf/saxon/Configuration;

    invoke-direct {v0, v2}, Lnet/sf/saxon/query/StaticQueryContext;-><init>(Lnet/sf/saxon/Configuration;)V

    const-string v2, "LOAD_ADDITIONAL_NAMESPACES"

    invoke-virtual {p4, v2}, Lorg/apache/xmlbeans/XmlOptions;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p4

    check-cast p4, Ljava/util/Map;

    if-eqz p4, :cond_0

    invoke-interface {p4}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p4

    invoke-interface {p4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p4

    :goto_0
    invoke-interface {p4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {p4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v0, v3, v2}, Lnet/sf/saxon/query/StaticQueryContext;->declareNamespace(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    iput-object p2, p0, Lorg/apache/xmlbeans/impl/xquery/saxon/XBeansXQuery;->contextVar:Ljava/lang/String;

    invoke-virtual {p3}, Ljava/lang/Integer;->intValue()I

    move-result p4

    const-string v2, " external;"

    const-string v3, "declare variable $"

    if-nez p4, :cond_1

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p3, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_1

    :cond_1
    new-instance p4, Ljava/lang/StringBuilder;

    invoke-direct {p4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p3}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {p1, v1, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/Integer;->intValue()I

    move-result p2

    invoke-virtual {p1, p2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    :goto_1
    :try_start_0
    invoke-virtual {v0, p1}, Lnet/sf/saxon/query/StaticQueryContext;->compileQuery(Ljava/lang/String;)Lnet/sf/saxon/query/XQueryExpression;

    move-result-object p1

    iput-object p1, p0, Lorg/apache/xmlbeans/impl/xquery/saxon/XBeansXQuery;->xquery:Lnet/sf/saxon/query/XQueryExpression;
    :try_end_0
    .catch Ljavax/xml/transform/TransformerException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    new-instance p2, Lorg/apache/xmlbeans/XmlRuntimeException;

    invoke-direct {p2, p1}, Lorg/apache/xmlbeans/XmlRuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw p2
.end method


# virtual methods
.method public execQuery(Ljava/lang/Object;Ljava/util/Map;)Ljava/util/List;
    .locals 3

    :try_start_0
    check-cast p1, Lorg/w3c/dom/Node;

    iget-object v0, p0, Lorg/apache/xmlbeans/impl/xquery/saxon/XBeansXQuery;->config:Lnet/sf/saxon/Configuration;

    new-instance v1, Ljavax/xml/transform/dom/DOMSource;

    invoke-direct {v1, p1}, Ljavax/xml/transform/dom/DOMSource;-><init>(Lorg/w3c/dom/Node;)V

    invoke-virtual {v0, v1}, Lnet/sf/saxon/Configuration;->buildDocument(Ljavax/xml/transform/Source;)Lnet/sf/saxon/om/DocumentInfo;

    move-result-object p1

    new-instance v0, Lnet/sf/saxon/query/DynamicQueryContext;

    iget-object v1, p0, Lorg/apache/xmlbeans/impl/xquery/saxon/XBeansXQuery;->config:Lnet/sf/saxon/Configuration;

    invoke-direct {v0, v1}, Lnet/sf/saxon/query/DynamicQueryContext;-><init>(Lnet/sf/saxon/Configuration;)V

    invoke-virtual {v0, p1}, Lnet/sf/saxon/query/DynamicQueryContext;->setContextItem(Lnet/sf/saxon/om/Item;)V

    iget-object v1, p0, Lorg/apache/xmlbeans/impl/xquery/saxon/XBeansXQuery;->contextVar:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Lnet/sf/saxon/query/DynamicQueryContext;->setParameter(Ljava/lang/String;Ljava/lang/Object;)V

    if-eqz p2, :cond_2

    invoke-interface {p2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/util/Map$Entry;

    invoke-interface {p2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {p2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object p2

    instance-of v2, p2, Lorg/apache/xmlbeans/XmlTokenSource;

    if-eqz v2, :cond_1

    check-cast p2, Lorg/apache/xmlbeans/XmlTokenSource;

    invoke-interface {p2}, Lorg/apache/xmlbeans/XmlTokenSource;->getDomNode()Lorg/w3c/dom/Node;

    move-result-object p2

    :goto_1
    invoke-virtual {v0, v1, p2}, Lnet/sf/saxon/query/DynamicQueryContext;->setParameter(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    instance-of v2, p2, Ljava/lang/String;

    if-eqz v2, :cond_0

    goto :goto_1

    :cond_2
    iget-object p1, p0, Lorg/apache/xmlbeans/impl/xquery/saxon/XBeansXQuery;->xquery:Lnet/sf/saxon/query/XQueryExpression;

    invoke-virtual {p1, v0}, Lnet/sf/saxon/query/XQueryExpression;->evaluate(Lnet/sf/saxon/query/DynamicQueryContext;)Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->listIterator()Ljava/util/ListIterator;

    move-result-object p2

    :cond_3
    :goto_2
    invoke-interface {p2}, Ljava/util/ListIterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {p2}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Lnet/sf/saxon/om/NodeInfo;

    if-eqz v1, :cond_3

    check-cast v0, Lnet/sf/saxon/om/NodeInfo;

    invoke-static {v0}, Lnet/sf/saxon/dom/NodeOverNodeInfo;->wrap(Lnet/sf/saxon/om/NodeInfo;)Lnet/sf/saxon/dom/NodeOverNodeInfo;

    move-result-object v0

    invoke-interface {p2, v0}, Ljava/util/ListIterator;->set(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljavax/xml/transform/TransformerException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    :cond_4
    return-object p1

    :catch_0
    move-exception p1

    new-instance p2, Ljava/lang/RuntimeException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Error binding "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lorg/apache/xmlbeans/impl/xquery/saxon/XBeansXQuery;->contextVar:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p2, v0, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw p2
.end method

.class public abstract Lorg/apache/xmlbeans/impl/store/Query;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/xmlbeans/impl/store/Query$DelegateQueryImpl;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z = false

.field public static final QUERY_DELEGATE_INTERFACE:Ljava/lang/String; = "QUERY_DELEGATE_INTERFACE"

.field private static _delIntfName:Ljava/lang/String; = null

.field public static _useDelegateForXQuery:Ljava/lang/String; = "use delegate for xquery"

.field public static _useXdkForXQuery:Ljava/lang/String; = "use xdk for xquery"

.field private static _xdkAvailable:Z

.field private static _xdkCompileQuery:Ljava/lang/reflect/Method;

.field private static _xdkQueryCache:Ljava/util/HashMap;

.field private static _xqrl2002Available:Z

.field private static _xqrl2002CompileQuery:Ljava/lang/reflect/Method;

.field private static _xqrl2002QueryCache:Ljava/util/HashMap;

.field private static _xqrlAvailable:Z

.field private static _xqrlCompileQuery:Ljava/lang/reflect/Method;

.field private static _xqrlQueryCache:Ljava/util/HashMap;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lorg/apache/xmlbeans/impl/store/Query;->_xdkQueryCache:Ljava/util/HashMap;

    const/4 v0, 0x1

    sput-boolean v0, Lorg/apache/xmlbeans/impl/store/Query;->_xdkAvailable:Z

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    sput-object v1, Lorg/apache/xmlbeans/impl/store/Query;->_xqrlQueryCache:Ljava/util/HashMap;

    sput-boolean v0, Lorg/apache/xmlbeans/impl/store/Query;->_xqrlAvailable:Z

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    sput-object v1, Lorg/apache/xmlbeans/impl/store/Query;->_xqrl2002QueryCache:Ljava/util/HashMap;

    sput-boolean v0, Lorg/apache/xmlbeans/impl/store/Query;->_xqrl2002Available:Z

    new-instance v0, Lorg/apache/xmlbeans/impl/common/DefaultClassLoaderResourceLoader;

    invoke-direct {v0}, Lorg/apache/xmlbeans/impl/common/DefaultClassLoaderResourceLoader;-><init>()V

    const-string v1, "META-INF/services/org.apache.xmlbeans.impl.store.QueryDelegate.QueryInterface"

    invoke-virtual {v0, v1}, Lorg/apache/xmlbeans/impl/common/DefaultClassLoaderResourceLoader;->getResourceAsStream(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v0

    :try_start_0
    new-instance v1, Ljava/io/BufferedReader;

    new-instance v2, Ljava/io/InputStreamReader;

    invoke-direct {v2, v0}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v1, v2}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    invoke-virtual {v1}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lorg/apache/xmlbeans/impl/store/Query;->_delIntfName:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    const/4 v0, 0x0

    sput-object v0, Lorg/apache/xmlbeans/impl/store/Query;->_delIntfName:Ljava/lang/String;

    :goto_0
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static declared-synchronized compileQuery(Ljava/lang/String;Lorg/apache/xmlbeans/XmlOptions;)Ljava/lang/String;
    .locals 1

    const-class v0, Lorg/apache/xmlbeans/impl/store/Query;

    monitor-enter v0

    :try_start_0
    invoke-static {p0, p1}, Lorg/apache/xmlbeans/impl/store/Query;->getCompiledQuery(Ljava/lang/String;Lorg/apache/xmlbeans/XmlOptions;)Lorg/apache/xmlbeans/impl/store/Query;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object p0

    :catchall_0
    move-exception p0

    monitor-exit v0

    throw p0
.end method

.method private static createXdkCompiledQuery(Ljava/lang/String;Ljava/lang/String;)Lorg/apache/xmlbeans/impl/store/Query;
    .locals 8

    const-class v0, Ljava/lang/String;

    sget-boolean v1, Lorg/apache/xmlbeans/impl/store/Query;->_xdkAvailable:Z

    const/4 v2, 0x0

    if-nez v1, :cond_0

    return-object v2

    :cond_0
    sget-object v1, Lorg/apache/xmlbeans/impl/store/Query;->_xdkCompileQuery:Ljava/lang/reflect/Method;

    const/4 v3, 0x1

    if-nez v1, :cond_1

    const/4 v1, 0x0

    :try_start_0
    const-string v4, "org.apache.xmlbeans.impl.store.OXQXBXqrlImpl"

    invoke-static {v4}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v4

    const-string v5, "compileQuery"

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/Class;

    aput-object v0, v6, v1

    aput-object v0, v6, v3

    const-class v0, Ljava/lang/Boolean;

    const/4 v7, 0x2

    aput-object v0, v6, v7

    invoke-virtual {v4, v5, v6}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Lorg/apache/xmlbeans/impl/store/Query;->_xdkCompileQuery:Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    sput-boolean v1, Lorg/apache/xmlbeans/impl/store/Query;->_xdkAvailable:Z

    new-instance p1, Ljava/lang/RuntimeException;

    invoke-virtual {p0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0, p0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw p1

    :catch_1
    sput-boolean v1, Lorg/apache/xmlbeans/impl/store/Query;->_xdkAvailable:Z

    return-object v2

    :cond_1
    :goto_0
    new-instance v0, Ljava/lang/Boolean;

    invoke-direct {v0, v3}, Ljava/lang/Boolean;-><init>(Z)V

    filled-new-array {p0, p1, v0}, [Ljava/lang/Object;

    move-result-object p0

    :try_start_1
    sget-object p1, Lorg/apache/xmlbeans/impl/store/Query;->_xdkCompileQuery:Ljava/lang/reflect/Method;

    invoke-virtual {p1, v2, p0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lorg/apache/xmlbeans/impl/store/Query;
    :try_end_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_2

    return-object p0

    :catch_2
    move-exception p0

    new-instance p1, Ljava/lang/RuntimeException;

    invoke-virtual {p0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0, p0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw p1

    :catch_3
    move-exception p0

    invoke-virtual {p0}, Ljava/lang/reflect/InvocationTargetException;->getCause()Ljava/lang/Throwable;

    move-result-object p0

    new-instance p1, Ljava/lang/RuntimeException;

    invoke-virtual {p0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0, p0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw p1
.end method

.method private static createXqrlCompiledQuery(Ljava/lang/String;Ljava/lang/String;)Lorg/apache/xmlbeans/impl/store/Query;
    .locals 8

    const-class v0, Ljava/lang/String;

    sget-boolean v1, Lorg/apache/xmlbeans/impl/store/Query;->_xqrlAvailable:Z

    const/4 v2, 0x0

    if-nez v1, :cond_0

    return-object v2

    :cond_0
    sget-object v1, Lorg/apache/xmlbeans/impl/store/Query;->_xqrlCompileQuery:Ljava/lang/reflect/Method;

    const/4 v3, 0x1

    if-nez v1, :cond_1

    const/4 v1, 0x0

    :try_start_0
    const-string v4, "org.apache.xmlbeans.impl.store.XqrlImpl"

    invoke-static {v4}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v4

    const-string v5, "compileQuery"

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/Class;

    aput-object v0, v6, v1

    aput-object v0, v6, v3

    const-class v0, Ljava/lang/Boolean;

    const/4 v7, 0x2

    aput-object v0, v6, v7

    invoke-virtual {v4, v5, v6}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Lorg/apache/xmlbeans/impl/store/Query;->_xqrlCompileQuery:Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    sput-boolean v1, Lorg/apache/xmlbeans/impl/store/Query;->_xqrlAvailable:Z

    new-instance p1, Ljava/lang/RuntimeException;

    invoke-virtual {p0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0, p0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw p1

    :catch_1
    sput-boolean v1, Lorg/apache/xmlbeans/impl/store/Query;->_xqrlAvailable:Z

    return-object v2

    :cond_1
    :goto_0
    new-instance v0, Ljava/lang/Boolean;

    invoke-direct {v0, v3}, Ljava/lang/Boolean;-><init>(Z)V

    filled-new-array {p0, p1, v0}, [Ljava/lang/Object;

    move-result-object p0

    :try_start_1
    sget-object p1, Lorg/apache/xmlbeans/impl/store/Query;->_xqrlCompileQuery:Ljava/lang/reflect/Method;

    invoke-virtual {p1, v2, p0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lorg/apache/xmlbeans/impl/store/Query;
    :try_end_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_2

    return-object p0

    :catch_2
    move-exception p0

    new-instance p1, Ljava/lang/RuntimeException;

    invoke-virtual {p0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0, p0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw p1

    :catch_3
    move-exception p0

    invoke-virtual {p0}, Ljava/lang/reflect/InvocationTargetException;->getCause()Ljava/lang/Throwable;

    move-result-object p0

    new-instance p1, Ljava/lang/RuntimeException;

    invoke-virtual {p0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0, p0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw p1
.end method

.method public static cursorExecQuery(Lorg/apache/xmlbeans/impl/store/Cur;Ljava/lang/String;Lorg/apache/xmlbeans/XmlOptions;)Lorg/apache/xmlbeans/XmlCursor;
    .locals 0

    invoke-static {p1, p2}, Lorg/apache/xmlbeans/impl/store/Query;->getCompiledQuery(Ljava/lang/String;Lorg/apache/xmlbeans/XmlOptions;)Lorg/apache/xmlbeans/impl/store/Query;

    move-result-object p1

    invoke-virtual {p1, p0, p2}, Lorg/apache/xmlbeans/impl/store/Query;->cursorExecute(Lorg/apache/xmlbeans/impl/store/Cur;Lorg/apache/xmlbeans/XmlOptions;)Lorg/apache/xmlbeans/XmlCursor;

    move-result-object p0

    return-object p0
.end method

.method public static declared-synchronized getCompiledQuery(Ljava/lang/String;Ljava/lang/String;Lorg/apache/xmlbeans/XmlOptions;)Lorg/apache/xmlbeans/impl/store/Query;
    .locals 4

    .line 1
    const-class v0, Lorg/apache/xmlbeans/impl/store/Query;

    monitor-enter v0

    :try_start_0
    invoke-static {p2}, Lorg/apache/xmlbeans/XmlOptions;->maskNull(Lorg/apache/xmlbeans/XmlOptions;)Lorg/apache/xmlbeans/XmlOptions;

    move-result-object p2

    sget-object v1, Lorg/apache/xmlbeans/impl/store/Path;->_forceXqrl2002ForXpathXQuery:Ljava/lang/String;

    invoke-virtual {p2, v1}, Lorg/apache/xmlbeans/XmlOptions;->hasOption(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    sget-object p2, Lorg/apache/xmlbeans/impl/store/Query;->_xqrl2002QueryCache:Ljava/util/HashMap;

    invoke-virtual {p2, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lorg/apache/xmlbeans/impl/store/Query;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    if-eqz p2, :cond_0

    monitor-exit v0

    return-object p2

    :cond_0
    :try_start_1
    invoke-static {p0, p1}, Lorg/apache/xmlbeans/impl/store/Query;->getXqrl2002CompiledQuery(Ljava/lang/String;Ljava/lang/String;)Lorg/apache/xmlbeans/impl/store/Query;

    move-result-object p1

    if-eqz p1, :cond_1

    sget-object p2, Lorg/apache/xmlbeans/impl/store/Query;->_xqrl2002QueryCache:Ljava/util/HashMap;

    invoke-virtual {p2, p0, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    monitor-exit v0

    return-object p1

    :cond_1
    :try_start_2
    new-instance p0, Ljava/lang/RuntimeException;

    const-string p1, "No 2002 query engine found."

    invoke-direct {p0, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw p0

    :cond_2
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    const/4 v2, 0x0

    :try_start_3
    invoke-static {p0, p1, v1}, Lorg/apache/xmlbeans/impl/common/XPath;->compileXPath(Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;)Lorg/apache/xmlbeans/impl/common/XPath;
    :try_end_3
    .catch Lorg/apache/xmlbeans/impl/common/XPath$XPathCompileException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    const-string v3, "$xmlbeans!ns_boundary"

    invoke-interface {v1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    if-nez v3, :cond_3

    goto :goto_2

    :cond_3
    const-string v2, "$xmlbeans!ns_boundary"

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    :goto_0
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    goto :goto_2

    :catchall_0
    move-exception p0

    const-string p1, "$xmlbeans!ns_boundary"

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    if-nez p1, :cond_4

    goto :goto_1

    :cond_4
    const-string p1, "$xmlbeans!ns_boundary"

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    :goto_1
    throw p0

    :catch_0
    const-string v3, "$xmlbeans!ns_boundary"

    invoke-interface {v1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    if-nez v3, :cond_5

    goto :goto_2

    :cond_5
    const-string v2, "$xmlbeans!ns_boundary"

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    goto :goto_0

    :goto_2
    sget-object v1, Lorg/apache/xmlbeans/impl/store/Query;->_useXdkForXQuery:Ljava/lang/String;

    invoke-virtual {p2, v1}, Lorg/apache/xmlbeans/XmlOptions;->hasOption(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    sget-object v1, Lorg/apache/xmlbeans/impl/store/Query;->_xdkQueryCache:Ljava/util/HashMap;

    invoke-virtual {v1, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/xmlbeans/impl/store/Query;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    if-eqz v1, :cond_6

    monitor-exit v0

    return-object v1

    :cond_6
    :try_start_5
    invoke-static {p0, p1}, Lorg/apache/xmlbeans/impl/store/Query;->createXdkCompiledQuery(Ljava/lang/String;Ljava/lang/String;)Lorg/apache/xmlbeans/impl/store/Query;

    move-result-object v1

    if-eqz v1, :cond_7

    sget-object p1, Lorg/apache/xmlbeans/impl/store/Query;->_xdkQueryCache:Ljava/util/HashMap;

    invoke-virtual {p1, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    monitor-exit v0

    return-object v1

    :cond_7
    :try_start_6
    sget-object v1, Lorg/apache/xmlbeans/impl/store/Query;->_useDelegateForXQuery:Ljava/lang/String;

    invoke-virtual {p2, v1}, Lorg/apache/xmlbeans/XmlOptions;->hasOption(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_9

    sget-object v1, Lorg/apache/xmlbeans/impl/store/Query;->_xqrlQueryCache:Ljava/util/HashMap;

    invoke-virtual {v1, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/xmlbeans/impl/store/Query;
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    if-eqz v1, :cond_8

    monitor-exit v0

    return-object v1

    :cond_8
    :try_start_7
    invoke-static {p0, p1}, Lorg/apache/xmlbeans/impl/store/Query;->createXqrlCompiledQuery(Ljava/lang/String;Ljava/lang/String;)Lorg/apache/xmlbeans/impl/store/Query;

    move-result-object v1

    if-eqz v1, :cond_9

    sget-object p1, Lorg/apache/xmlbeans/impl/store/Query;->_xqrlQueryCache:Ljava/util/HashMap;

    invoke-virtual {p1, p0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    monitor-exit v0

    return-object v1

    :cond_9
    :try_start_8
    const-string v1, "QUERY_DELEGATE_INTERFACE"

    invoke-virtual {p2, v1}, Lorg/apache/xmlbeans/XmlOptions;->hasOption(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    const-string v1, "QUERY_DELEGATE_INTERFACE"

    invoke-virtual {p2, v1}, Lorg/apache/xmlbeans/XmlOptions;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    goto :goto_3

    :cond_a
    sget-object v1, Lorg/apache/xmlbeans/impl/store/Query;->_delIntfName:Ljava/lang/String;

    :goto_3
    invoke-static {v1, p0, p1, v2, p2}, Lorg/apache/xmlbeans/impl/store/Query$DelegateQueryImpl;->createDelegateCompiledQuery(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILorg/apache/xmlbeans/XmlOptions;)Lorg/apache/xmlbeans/impl/store/Query;

    move-result-object p0
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    if-eqz p0, :cond_b

    monitor-exit v0

    return-object p0

    :cond_b
    :try_start_9
    new-instance p0, Ljava/lang/RuntimeException;

    const-string p1, "No query engine found"

    invoke-direct {p0, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw p0
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    :catchall_1
    move-exception p0

    monitor-exit v0

    throw p0
.end method

.method public static declared-synchronized getCompiledQuery(Ljava/lang/String;Lorg/apache/xmlbeans/XmlOptions;)Lorg/apache/xmlbeans/impl/store/Query;
    .locals 2

    .line 2
    const-class v0, Lorg/apache/xmlbeans/impl/store/Query;

    monitor-enter v0

    :try_start_0
    invoke-static {p1}, Lorg/apache/xmlbeans/impl/store/Path;->getCurrentNodeVar(Lorg/apache/xmlbeans/XmlOptions;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1, p1}, Lorg/apache/xmlbeans/impl/store/Query;->getCompiledQuery(Ljava/lang/String;Ljava/lang/String;Lorg/apache/xmlbeans/XmlOptions;)Lorg/apache/xmlbeans/impl/store/Query;

    move-result-object p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object p0

    :catchall_0
    move-exception p0

    monitor-exit v0

    throw p0
.end method

.method private static getXqrl2002CompiledQuery(Ljava/lang/String;Ljava/lang/String;)Lorg/apache/xmlbeans/impl/store/Query;
    .locals 8

    const-class v0, Ljava/lang/String;

    sget-boolean v1, Lorg/apache/xmlbeans/impl/store/Query;->_xqrl2002Available:Z

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-eqz v1, :cond_0

    sget-object v1, Lorg/apache/xmlbeans/impl/store/Query;->_xqrl2002CompileQuery:Ljava/lang/reflect/Method;

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :try_start_0
    const-string v4, "org.apache.xmlbeans.impl.store.Xqrl2002Impl"

    invoke-static {v4}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v4

    const-string v5, "compileQuery"

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/Class;

    aput-object v0, v6, v1

    aput-object v0, v6, v3

    const-class v0, Ljava/lang/Boolean;

    const/4 v7, 0x2

    aput-object v0, v6, v7

    invoke-virtual {v4, v5, v6}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Lorg/apache/xmlbeans/impl/store/Query;->_xqrl2002CompileQuery:Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    sput-boolean v1, Lorg/apache/xmlbeans/impl/store/Query;->_xqrl2002Available:Z

    new-instance p1, Ljava/lang/RuntimeException;

    invoke-virtual {p0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0, p0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw p1

    :catch_1
    sput-boolean v1, Lorg/apache/xmlbeans/impl/store/Query;->_xqrl2002Available:Z

    return-object v2

    :cond_0
    :goto_0
    new-instance v0, Ljava/lang/Boolean;

    invoke-direct {v0, v3}, Ljava/lang/Boolean;-><init>(Z)V

    filled-new-array {p0, p1, v0}, [Ljava/lang/Object;

    move-result-object p0

    :try_start_1
    sget-object p1, Lorg/apache/xmlbeans/impl/store/Query;->_xqrl2002CompileQuery:Ljava/lang/reflect/Method;

    invoke-virtual {p1, v2, p0}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lorg/apache/xmlbeans/impl/store/Query;
    :try_end_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_2

    return-object p0

    :catch_2
    move-exception p0

    new-instance p1, Ljava/lang/RuntimeException;

    invoke-virtual {p0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0, p0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw p1

    :catch_3
    move-exception p0

    invoke-virtual {p0}, Ljava/lang/reflect/InvocationTargetException;->getCause()Ljava/lang/Throwable;

    move-result-object p0

    new-instance p1, Ljava/lang/RuntimeException;

    invoke-virtual {p0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0, p0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw p1
.end method

.method public static objectExecQuery(Lorg/apache/xmlbeans/impl/store/Cur;Ljava/lang/String;Lorg/apache/xmlbeans/XmlOptions;)[Lorg/apache/xmlbeans/XmlObject;
    .locals 0

    invoke-static {p1, p2}, Lorg/apache/xmlbeans/impl/store/Query;->getCompiledQuery(Ljava/lang/String;Lorg/apache/xmlbeans/XmlOptions;)Lorg/apache/xmlbeans/impl/store/Query;

    move-result-object p1

    invoke-virtual {p1, p0, p2}, Lorg/apache/xmlbeans/impl/store/Query;->objectExecute(Lorg/apache/xmlbeans/impl/store/Cur;Lorg/apache/xmlbeans/XmlOptions;)[Lorg/apache/xmlbeans/XmlObject;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public abstract cursorExecute(Lorg/apache/xmlbeans/impl/store/Cur;Lorg/apache/xmlbeans/XmlOptions;)Lorg/apache/xmlbeans/XmlCursor;
.end method

.method public abstract objectExecute(Lorg/apache/xmlbeans/impl/store/Cur;Lorg/apache/xmlbeans/XmlOptions;)[Lorg/apache/xmlbeans/XmlObject;
.end method

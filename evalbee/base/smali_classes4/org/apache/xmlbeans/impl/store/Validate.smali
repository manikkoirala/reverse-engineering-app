.class final Lorg/apache/xmlbeans/impl/store/Validate;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/xmlbeans/impl/common/ValidatorListener$Event;


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private _cur:Lorg/apache/xmlbeans/impl/store/Cur;

.field private _hasText:Z

.field private _oneChunk:Z

.field private _sink:Lorg/apache/xmlbeans/impl/common/ValidatorListener;

.field private _textCur:Lorg/apache/xmlbeans/impl/store/Cur;

.field private _textSb:Ljava/lang/StringBuffer;


# direct methods
.method public static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>(Lorg/apache/xmlbeans/impl/store/Cur;Lorg/apache/xmlbeans/impl/common/ValidatorListener;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Lorg/apache/xmlbeans/impl/store/Cur;->isUserNode()Z

    move-result v0

    if-eqz v0, :cond_0

    iput-object p2, p0, Lorg/apache/xmlbeans/impl/store/Validate;->_sink:Lorg/apache/xmlbeans/impl/common/ValidatorListener;

    iput-object p1, p0, Lorg/apache/xmlbeans/impl/store/Validate;->_cur:Lorg/apache/xmlbeans/impl/store/Cur;

    invoke-virtual {p1}, Lorg/apache/xmlbeans/impl/store/Cur;->tempCur()Lorg/apache/xmlbeans/impl/store/Cur;

    move-result-object p1

    iput-object p1, p0, Lorg/apache/xmlbeans/impl/store/Validate;->_textCur:Lorg/apache/xmlbeans/impl/store/Cur;

    const/4 p1, 0x0

    iput-boolean p1, p0, Lorg/apache/xmlbeans/impl/store/Validate;->_hasText:Z

    iget-object p1, p0, Lorg/apache/xmlbeans/impl/store/Validate;->_cur:Lorg/apache/xmlbeans/impl/store/Cur;

    invoke-virtual {p1}, Lorg/apache/xmlbeans/impl/store/Cur;->push()V

    const/4 p1, 0x0

    :try_start_0
    invoke-direct {p0}, Lorg/apache/xmlbeans/impl/store/Validate;->process()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object p2, p0, Lorg/apache/xmlbeans/impl/store/Validate;->_cur:Lorg/apache/xmlbeans/impl/store/Cur;

    invoke-virtual {p2}, Lorg/apache/xmlbeans/impl/store/Cur;->pop()Z

    iput-object p1, p0, Lorg/apache/xmlbeans/impl/store/Validate;->_cur:Lorg/apache/xmlbeans/impl/store/Cur;

    iput-object p1, p0, Lorg/apache/xmlbeans/impl/store/Validate;->_sink:Lorg/apache/xmlbeans/impl/common/ValidatorListener;

    iget-object p1, p0, Lorg/apache/xmlbeans/impl/store/Validate;->_textCur:Lorg/apache/xmlbeans/impl/store/Cur;

    invoke-virtual {p1}, Lorg/apache/xmlbeans/impl/store/Cur;->release()V

    return-void

    :catchall_0
    move-exception p2

    iget-object v0, p0, Lorg/apache/xmlbeans/impl/store/Validate;->_cur:Lorg/apache/xmlbeans/impl/store/Cur;

    invoke-virtual {v0}, Lorg/apache/xmlbeans/impl/store/Cur;->pop()Z

    iput-object p1, p0, Lorg/apache/xmlbeans/impl/store/Validate;->_cur:Lorg/apache/xmlbeans/impl/store/Cur;

    iput-object p1, p0, Lorg/apache/xmlbeans/impl/store/Validate;->_sink:Lorg/apache/xmlbeans/impl/common/ValidatorListener;

    iget-object p1, p0, Lorg/apache/xmlbeans/impl/store/Validate;->_textCur:Lorg/apache/xmlbeans/impl/store/Cur;

    invoke-virtual {p1}, Lorg/apache/xmlbeans/impl/store/Cur;->release()V

    throw p2

    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Inappropriate location to validate"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private doAttrs()V
    .locals 2

    iget-object v0, p0, Lorg/apache/xmlbeans/impl/store/Validate;->_cur:Lorg/apache/xmlbeans/impl/store/Cur;

    invoke-virtual {v0}, Lorg/apache/xmlbeans/impl/store/Cur;->toFirstAttr()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    iget-object v0, p0, Lorg/apache/xmlbeans/impl/store/Validate;->_cur:Lorg/apache/xmlbeans/impl/store/Cur;

    invoke-virtual {v0}, Lorg/apache/xmlbeans/impl/store/Cur;->isNormalAttr()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lorg/apache/xmlbeans/impl/store/Validate;->_cur:Lorg/apache/xmlbeans/impl/store/Cur;

    invoke-virtual {v0}, Lorg/apache/xmlbeans/impl/store/Cur;->getUri()Ljava/lang/String;

    move-result-object v0

    const-string v1, "http://www.w3.org/2001/XMLSchema-instance"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lorg/apache/xmlbeans/impl/store/Validate;->_sink:Lorg/apache/xmlbeans/impl/common/ValidatorListener;

    const/4 v1, 0x4

    invoke-interface {v0, v1, p0}, Lorg/apache/xmlbeans/impl/common/ValidatorListener;->nextEvent(ILorg/apache/xmlbeans/impl/common/ValidatorListener$Event;)V

    :cond_1
    iget-object v0, p0, Lorg/apache/xmlbeans/impl/store/Validate;->_cur:Lorg/apache/xmlbeans/impl/store/Cur;

    invoke-virtual {v0}, Lorg/apache/xmlbeans/impl/store/Cur;->toNextAttr()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/xmlbeans/impl/store/Validate;->_cur:Lorg/apache/xmlbeans/impl/store/Cur;

    invoke-virtual {v0}, Lorg/apache/xmlbeans/impl/store/Cur;->toParent()Z

    :cond_2
    iget-object v0, p0, Lorg/apache/xmlbeans/impl/store/Validate;->_sink:Lorg/apache/xmlbeans/impl/common/ValidatorListener;

    const/4 v1, 0x5

    invoke-interface {v0, v1, p0}, Lorg/apache/xmlbeans/impl/common/ValidatorListener;->nextEvent(ILorg/apache/xmlbeans/impl/common/ValidatorListener$Event;)V

    return-void
.end method

.method private emitEvent(I)V
    .locals 2

    iget-boolean v0, p0, Lorg/apache/xmlbeans/impl/store/Validate;->_hasText:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/xmlbeans/impl/store/Validate;->_sink:Lorg/apache/xmlbeans/impl/common/ValidatorListener;

    const/4 v1, 0x3

    invoke-interface {v0, v1, p0}, Lorg/apache/xmlbeans/impl/common/ValidatorListener;->nextEvent(ILorg/apache/xmlbeans/impl/common/ValidatorListener$Event;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/xmlbeans/impl/store/Validate;->_hasText:Z

    :cond_0
    iget-object v0, p0, Lorg/apache/xmlbeans/impl/store/Validate;->_sink:Lorg/apache/xmlbeans/impl/common/ValidatorListener;

    invoke-interface {v0, p1, p0}, Lorg/apache/xmlbeans/impl/common/ValidatorListener;->nextEvent(ILorg/apache/xmlbeans/impl/common/ValidatorListener$Event;)V

    return-void
.end method

.method private emitText()V
    .locals 6

    iget-boolean v0, p0, Lorg/apache/xmlbeans/impl/store/Validate;->_hasText:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lorg/apache/xmlbeans/impl/store/Validate;->_oneChunk:Z

    const/4 v1, -0x1

    if-eqz v0, :cond_1

    iget-object v0, p0, Lorg/apache/xmlbeans/impl/store/Validate;->_textSb:Ljava/lang/StringBuffer;

    const/4 v2, 0x0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    iput-object v0, p0, Lorg/apache/xmlbeans/impl/store/Validate;->_textSb:Ljava/lang/StringBuffer;

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->length()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuffer;->delete(II)Ljava/lang/StringBuffer;

    :goto_0
    iget-object v0, p0, Lorg/apache/xmlbeans/impl/store/Validate;->_textSb:Ljava/lang/StringBuffer;

    iget-object v3, p0, Lorg/apache/xmlbeans/impl/store/Validate;->_textCur:Lorg/apache/xmlbeans/impl/store/Cur;

    invoke-virtual {v3, v1}, Lorg/apache/xmlbeans/impl/store/Cur;->getChars(I)Ljava/lang/Object;

    move-result-object v3

    iget-object v4, p0, Lorg/apache/xmlbeans/impl/store/Validate;->_textCur:Lorg/apache/xmlbeans/impl/store/Cur;

    iget v5, v4, Lorg/apache/xmlbeans/impl/store/Cur;->_offSrc:I

    iget v4, v4, Lorg/apache/xmlbeans/impl/store/Cur;->_cchSrc:I

    invoke-static {v0, v3, v5, v4}, Lorg/apache/xmlbeans/impl/store/CharUtil;->getString(Ljava/lang/StringBuffer;Ljava/lang/Object;II)V

    iput-boolean v2, p0, Lorg/apache/xmlbeans/impl/store/Validate;->_oneChunk:Z

    :cond_1
    iget-object v0, p0, Lorg/apache/xmlbeans/impl/store/Validate;->_textSb:Ljava/lang/StringBuffer;

    iget-object v2, p0, Lorg/apache/xmlbeans/impl/store/Validate;->_cur:Lorg/apache/xmlbeans/impl/store/Cur;

    invoke-virtual {v2, v1}, Lorg/apache/xmlbeans/impl/store/Cur;->getChars(I)Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/xmlbeans/impl/store/Validate;->_cur:Lorg/apache/xmlbeans/impl/store/Cur;

    iget v3, v2, Lorg/apache/xmlbeans/impl/store/Cur;->_offSrc:I

    iget v2, v2, Lorg/apache/xmlbeans/impl/store/Cur;->_cchSrc:I

    invoke-static {v0, v1, v3, v2}, Lorg/apache/xmlbeans/impl/store/CharUtil;->getString(Ljava/lang/StringBuffer;Ljava/lang/Object;II)V

    goto :goto_1

    :cond_2
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/xmlbeans/impl/store/Validate;->_hasText:Z

    iput-boolean v0, p0, Lorg/apache/xmlbeans/impl/store/Validate;->_oneChunk:Z

    iget-object v0, p0, Lorg/apache/xmlbeans/impl/store/Validate;->_textCur:Lorg/apache/xmlbeans/impl/store/Cur;

    iget-object v1, p0, Lorg/apache/xmlbeans/impl/store/Validate;->_cur:Lorg/apache/xmlbeans/impl/store/Cur;

    invoke-virtual {v0, v1}, Lorg/apache/xmlbeans/impl/store/Cur;->moveToCur(Lorg/apache/xmlbeans/impl/store/Cur;)V

    :goto_1
    return-void
.end method

.method private process()V
    .locals 4

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lorg/apache/xmlbeans/impl/store/Validate;->emitEvent(I)V

    iget-object v1, p0, Lorg/apache/xmlbeans/impl/store/Validate;->_cur:Lorg/apache/xmlbeans/impl/store/Cur;

    invoke-virtual {v1}, Lorg/apache/xmlbeans/impl/store/Cur;->isAttr()Z

    move-result v1

    const/4 v2, 0x2

    if-eqz v1, :cond_0

    iget-object v0, p0, Lorg/apache/xmlbeans/impl/store/Validate;->_cur:Lorg/apache/xmlbeans/impl/store/Cur;

    invoke-virtual {v0}, Lorg/apache/xmlbeans/impl/store/Cur;->next()Z

    iget-object v0, p0, Lorg/apache/xmlbeans/impl/store/Validate;->_cur:Lorg/apache/xmlbeans/impl/store/Cur;

    invoke-virtual {v0}, Lorg/apache/xmlbeans/impl/store/Cur;->isText()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-direct {p0}, Lorg/apache/xmlbeans/impl/store/Validate;->emitText()V

    goto :goto_3

    :cond_0
    :goto_0
    invoke-direct {p0}, Lorg/apache/xmlbeans/impl/store/Validate;->doAttrs()V

    :goto_1
    iget-object v1, p0, Lorg/apache/xmlbeans/impl/store/Validate;->_cur:Lorg/apache/xmlbeans/impl/store/Cur;

    invoke-virtual {v1}, Lorg/apache/xmlbeans/impl/store/Cur;->next()Z

    iget-object v1, p0, Lorg/apache/xmlbeans/impl/store/Validate;->_cur:Lorg/apache/xmlbeans/impl/store/Cur;

    invoke-virtual {v1}, Lorg/apache/xmlbeans/impl/store/Cur;->isAtEndOfLastPush()Z

    move-result v1

    if-nez v1, :cond_6

    iget-object v1, p0, Lorg/apache/xmlbeans/impl/store/Validate;->_cur:Lorg/apache/xmlbeans/impl/store/Cur;

    invoke-virtual {v1}, Lorg/apache/xmlbeans/impl/store/Cur;->kind()I

    move-result v1

    const/4 v3, -0x2

    if-eq v1, v3, :cond_5

    if-eqz v1, :cond_4

    if-eq v1, v2, :cond_3

    const/4 v3, 0x4

    if-eq v1, v3, :cond_2

    const/4 v3, 0x5

    if-ne v1, v3, :cond_1

    goto :goto_2

    :cond_1
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unexpected kind: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lorg/apache/xmlbeans/impl/store/Validate;->_cur:Lorg/apache/xmlbeans/impl/store/Cur;

    invoke-virtual {v2}, Lorg/apache/xmlbeans/impl/store/Cur;->kind()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    :goto_2
    iget-object v1, p0, Lorg/apache/xmlbeans/impl/store/Validate;->_cur:Lorg/apache/xmlbeans/impl/store/Cur;

    invoke-virtual {v1}, Lorg/apache/xmlbeans/impl/store/Cur;->toEnd()V

    goto :goto_1

    :cond_3
    invoke-direct {p0, v0}, Lorg/apache/xmlbeans/impl/store/Validate;->emitEvent(I)V

    goto :goto_0

    :cond_4
    invoke-direct {p0}, Lorg/apache/xmlbeans/impl/store/Validate;->emitText()V

    goto :goto_1

    :cond_5
    invoke-direct {p0, v2}, Lorg/apache/xmlbeans/impl/store/Validate;->emitEvent(I)V

    goto :goto_1

    :cond_6
    :goto_3
    invoke-direct {p0, v2}, Lorg/apache/xmlbeans/impl/store/Validate;->emitEvent(I)V

    return-void
.end method


# virtual methods
.method public getLocation()Ljavax/xml/stream/Location;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getLocationAsCursor()Lorg/apache/xmlbeans/XmlCursor;
    .locals 2

    new-instance v0, Lorg/apache/xmlbeans/impl/store/Cursor;

    iget-object v1, p0, Lorg/apache/xmlbeans/impl/store/Validate;->_cur:Lorg/apache/xmlbeans/impl/store/Cur;

    invoke-direct {v0, v1}, Lorg/apache/xmlbeans/impl/store/Cursor;-><init>(Lorg/apache/xmlbeans/impl/store/Cur;)V

    return-object v0
.end method

.method public getName()Ljavax/xml/namespace/QName;
    .locals 1

    iget-object v0, p0, Lorg/apache/xmlbeans/impl/store/Validate;->_cur:Lorg/apache/xmlbeans/impl/store/Cur;

    invoke-virtual {v0}, Lorg/apache/xmlbeans/impl/store/Cur;->isAtLastPush()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lorg/apache/xmlbeans/impl/store/Validate;->_cur:Lorg/apache/xmlbeans/impl/store/Cur;

    invoke-virtual {v0}, Lorg/apache/xmlbeans/impl/store/Cur;->getName()Ljavax/xml/namespace/QName;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public getNamespaceForPrefix(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lorg/apache/xmlbeans/impl/store/Validate;->_cur:Lorg/apache/xmlbeans/impl/store/Cur;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lorg/apache/xmlbeans/impl/store/Cur;->namespaceForPrefix(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getText()Ljava/lang/String;
    .locals 2

    .line 1
    iget-object v0, p0, Lorg/apache/xmlbeans/impl/store/Validate;->_cur:Lorg/apache/xmlbeans/impl/store/Cur;

    invoke-virtual {v0}, Lorg/apache/xmlbeans/impl/store/Cur;->isAttr()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/xmlbeans/impl/store/Validate;->_cur:Lorg/apache/xmlbeans/impl/store/Cur;

    invoke-virtual {v0}, Lorg/apache/xmlbeans/impl/store/Cur;->getValueAsString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    iget-boolean v0, p0, Lorg/apache/xmlbeans/impl/store/Validate;->_oneChunk:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lorg/apache/xmlbeans/impl/store/Validate;->_textCur:Lorg/apache/xmlbeans/impl/store/Cur;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lorg/apache/xmlbeans/impl/store/Cur;->getCharsAsString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lorg/apache/xmlbeans/impl/store/Validate;->_textSb:Ljava/lang/StringBuffer;

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public getText(I)Ljava/lang/String;
    .locals 2

    .line 2
    iget-object v0, p0, Lorg/apache/xmlbeans/impl/store/Validate;->_cur:Lorg/apache/xmlbeans/impl/store/Cur;

    invoke-virtual {v0}, Lorg/apache/xmlbeans/impl/store/Cur;->isAttr()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/xmlbeans/impl/store/Validate;->_cur:Lorg/apache/xmlbeans/impl/store/Cur;

    invoke-virtual {v0, p1}, Lorg/apache/xmlbeans/impl/store/Cur;->getValueAsString(I)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_0
    iget-boolean v0, p0, Lorg/apache/xmlbeans/impl/store/Validate;->_oneChunk:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lorg/apache/xmlbeans/impl/store/Validate;->_textCur:Lorg/apache/xmlbeans/impl/store/Cur;

    const/4 v1, -0x1

    invoke-virtual {v0, v1, p1}, Lorg/apache/xmlbeans/impl/store/Cur;->getCharsAsString(II)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_1
    iget-object v0, p0, Lorg/apache/xmlbeans/impl/store/Validate;->_textSb:Ljava/lang/StringBuffer;

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p1}, Lorg/apache/xmlbeans/impl/store/Locale;->applyWhiteSpaceRule(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getXsiLoc()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lorg/apache/xmlbeans/impl/store/Validate;->_cur:Lorg/apache/xmlbeans/impl/store/Cur;

    sget-object v1, Lorg/apache/xmlbeans/impl/store/Locale;->_xsiLoc:Ljavax/xml/namespace/QName;

    invoke-virtual {v0, v1}, Lorg/apache/xmlbeans/impl/store/Cur;->getAttrValue(Ljavax/xml/namespace/QName;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getXsiNil()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lorg/apache/xmlbeans/impl/store/Validate;->_cur:Lorg/apache/xmlbeans/impl/store/Cur;

    sget-object v1, Lorg/apache/xmlbeans/impl/store/Locale;->_xsiNil:Ljavax/xml/namespace/QName;

    invoke-virtual {v0, v1}, Lorg/apache/xmlbeans/impl/store/Cur;->getAttrValue(Ljavax/xml/namespace/QName;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getXsiNoLoc()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lorg/apache/xmlbeans/impl/store/Validate;->_cur:Lorg/apache/xmlbeans/impl/store/Cur;

    sget-object v1, Lorg/apache/xmlbeans/impl/store/Locale;->_xsiNoLoc:Ljavax/xml/namespace/QName;

    invoke-virtual {v0, v1}, Lorg/apache/xmlbeans/impl/store/Cur;->getAttrValue(Ljavax/xml/namespace/QName;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getXsiType()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lorg/apache/xmlbeans/impl/store/Validate;->_cur:Lorg/apache/xmlbeans/impl/store/Cur;

    sget-object v1, Lorg/apache/xmlbeans/impl/store/Locale;->_xsiType:Ljavax/xml/namespace/QName;

    invoke-virtual {v0, v1}, Lorg/apache/xmlbeans/impl/store/Cur;->getAttrValue(Ljavax/xml/namespace/QName;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public textIsWhitespace()Z
    .locals 4

    iget-object v0, p0, Lorg/apache/xmlbeans/impl/store/Validate;->_cur:Lorg/apache/xmlbeans/impl/store/Cur;

    invoke-virtual {v0}, Lorg/apache/xmlbeans/impl/store/Cur;->isAttr()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/xmlbeans/impl/store/Validate;->_cur:Lorg/apache/xmlbeans/impl/store/Cur;

    iget-object v0, v0, Lorg/apache/xmlbeans/impl/store/Cur;->_locale:Lorg/apache/xmlbeans/impl/store/Locale;

    invoke-virtual {v0}, Lorg/apache/xmlbeans/impl/store/Locale;->getCharUtil()Lorg/apache/xmlbeans/impl/store/CharUtil;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/xmlbeans/impl/store/Validate;->_cur:Lorg/apache/xmlbeans/impl/store/Cur;

    invoke-virtual {v1}, Lorg/apache/xmlbeans/impl/store/Cur;->getFirstChars()Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/xmlbeans/impl/store/Validate;->_cur:Lorg/apache/xmlbeans/impl/store/Cur;

    iget v3, v2, Lorg/apache/xmlbeans/impl/store/Cur;->_offSrc:I

    iget v2, v2, Lorg/apache/xmlbeans/impl/store/Cur;->_cchSrc:I

    invoke-virtual {v0, v1, v3, v2}, Lorg/apache/xmlbeans/impl/store/CharUtil;->isWhiteSpace(Ljava/lang/Object;II)Z

    move-result v0

    return v0

    :cond_0
    iget-boolean v0, p0, Lorg/apache/xmlbeans/impl/store/Validate;->_oneChunk:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lorg/apache/xmlbeans/impl/store/Validate;->_cur:Lorg/apache/xmlbeans/impl/store/Cur;

    iget-object v0, v0, Lorg/apache/xmlbeans/impl/store/Cur;->_locale:Lorg/apache/xmlbeans/impl/store/Locale;

    invoke-virtual {v0}, Lorg/apache/xmlbeans/impl/store/Locale;->getCharUtil()Lorg/apache/xmlbeans/impl/store/CharUtil;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/xmlbeans/impl/store/Validate;->_textCur:Lorg/apache/xmlbeans/impl/store/Cur;

    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Lorg/apache/xmlbeans/impl/store/Cur;->getChars(I)Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/xmlbeans/impl/store/Validate;->_textCur:Lorg/apache/xmlbeans/impl/store/Cur;

    iget v3, v2, Lorg/apache/xmlbeans/impl/store/Cur;->_offSrc:I

    iget v2, v2, Lorg/apache/xmlbeans/impl/store/Cur;->_cchSrc:I

    invoke-virtual {v0, v1, v3, v2}, Lorg/apache/xmlbeans/impl/store/CharUtil;->isWhiteSpace(Ljava/lang/Object;II)Z

    move-result v0

    return v0

    :cond_1
    iget-object v0, p0, Lorg/apache/xmlbeans/impl/store/Validate;->_textSb:Ljava/lang/StringBuffer;

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/xmlbeans/impl/store/Validate;->_cur:Lorg/apache/xmlbeans/impl/store/Cur;

    iget-object v1, v1, Lorg/apache/xmlbeans/impl/store/Cur;->_locale:Lorg/apache/xmlbeans/impl/store/Locale;

    invoke-virtual {v1}, Lorg/apache/xmlbeans/impl/store/Locale;->getCharUtil()Lorg/apache/xmlbeans/impl/store/CharUtil;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v3, v2}, Lorg/apache/xmlbeans/impl/store/CharUtil;->isWhiteSpace(Ljava/lang/Object;II)Z

    move-result v0

    return v0
.end method

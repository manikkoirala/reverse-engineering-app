.class public interface abstract Lorg/apache/xmlbeans/impl/jam/JAnnotationValue;
.super Ljava/lang/Object;
.source "SourceFile"


# virtual methods
.method public abstract asAnnotation()Lorg/apache/xmlbeans/impl/jam/JAnnotation;
.end method

.method public abstract asAnnotationArray()[Lorg/apache/xmlbeans/impl/jam/JAnnotation;
.end method

.method public abstract asBoolean()Z
.end method

.method public abstract asBooleanArray()[Z
.end method

.method public abstract asByte()B
.end method

.method public abstract asByteArray()[B
.end method

.method public abstract asChar()C
.end method

.method public abstract asCharArray()[C
.end method

.method public abstract asClass()Lorg/apache/xmlbeans/impl/jam/JClass;
.end method

.method public abstract asClassArray()[Lorg/apache/xmlbeans/impl/jam/JClass;
.end method

.method public abstract asDouble()D
.end method

.method public abstract asDoubleArray()[D
.end method

.method public abstract asFloat()F
.end method

.method public abstract asFloatArray()[F
.end method

.method public abstract asInt()I
.end method

.method public abstract asIntArray()[I
.end method

.method public abstract asLong()J
.end method

.method public abstract asLongArray()[J
.end method

.method public abstract asShort()S
.end method

.method public abstract asShortArray()[S
.end method

.method public abstract asString()Ljava/lang/String;
.end method

.method public abstract asStringArray()[Ljava/lang/String;
.end method

.method public abstract getName()Ljava/lang/String;
.end method

.method public abstract getType()Lorg/apache/xmlbeans/impl/jam/JClass;
.end method

.method public abstract getValue()Ljava/lang/Object;
.end method

.method public abstract isDefaultValueUsed()Z
.end method

.class public final Lorg/apache/xmlbeans/impl/jam/internal/reflect/ReflectTigerDelegateImpl_150;
.super Lorg/apache/xmlbeans/impl/jam/internal/reflect/ReflectTigerDelegate;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lorg/apache/xmlbeans/impl/jam/internal/reflect/ReflectTigerDelegate;-><init>()V

    return-void
.end method


# virtual methods
.method public extractAnnotations(Lorg/apache/xmlbeans/impl/jam/mutable/MClass;Ljava/lang/Class;)V
    .locals 0

    .line 1
    return-void
.end method

.method public extractAnnotations(Lorg/apache/xmlbeans/impl/jam/mutable/MConstructor;Ljava/lang/reflect/Constructor;)V
    .locals 0

    .line 2
    return-void
.end method

.method public extractAnnotations(Lorg/apache/xmlbeans/impl/jam/mutable/MField;Ljava/lang/reflect/Field;)V
    .locals 0

    .line 3
    return-void
.end method

.method public extractAnnotations(Lorg/apache/xmlbeans/impl/jam/mutable/MMember;Ljava/lang/reflect/Method;)V
    .locals 0

    .line 4
    return-void
.end method

.method public extractAnnotations(Lorg/apache/xmlbeans/impl/jam/mutable/MParameter;Ljava/lang/reflect/Constructor;I)V
    .locals 0

    .line 5
    return-void
.end method

.method public extractAnnotations(Lorg/apache/xmlbeans/impl/jam/mutable/MParameter;Ljava/lang/reflect/Method;I)V
    .locals 0

    .line 6
    return-void
.end method

.method public getEnclosingConstructor(Ljava/lang/Class;)Ljava/lang/reflect/Constructor;
    .locals 0

    const/4 p1, 0x0

    return-object p1
.end method

.method public getEnclosingMethod(Ljava/lang/Class;)Ljava/lang/reflect/Method;
    .locals 0

    const/4 p1, 0x0

    return-object p1
.end method

.method public isEnum(Ljava/lang/Class;)Z
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

.method public populateAnnotationTypeIfNecessary(Ljava/lang/Class;Lorg/apache/xmlbeans/impl/jam/mutable/MClass;Lorg/apache/xmlbeans/impl/jam/internal/reflect/ReflectClassBuilder;)V
    .locals 0

    return-void
.end method

.class public final Lorg/apache/xmlbeans/impl/common/SAXHelper;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final IGNORING_ENTITY_RESOLVER:Lorg/xml/sax/EntityResolver;

.field private static lastLog:J

.field private static final logger:Lorg/apache/xmlbeans/impl/common/XBLogger;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    const-class v0, Lorg/apache/xmlbeans/impl/common/SAXHelper;

    invoke-static {v0}, Lorg/apache/xmlbeans/impl/common/XBLogFactory;->getLogger(Ljava/lang/Class;)Lorg/apache/xmlbeans/impl/common/XBLogger;

    move-result-object v0

    sput-object v0, Lorg/apache/xmlbeans/impl/common/SAXHelper;->logger:Lorg/apache/xmlbeans/impl/common/XBLogger;

    new-instance v0, Lorg/apache/xmlbeans/impl/common/SAXHelper$1;

    invoke-direct {v0}, Lorg/apache/xmlbeans/impl/common/SAXHelper$1;-><init>()V

    sput-object v0, Lorg/apache/xmlbeans/impl/common/SAXHelper;->IGNORING_ENTITY_RESOLVER:Lorg/xml/sax/EntityResolver;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static newXMLReader(Lorg/apache/xmlbeans/XmlOptionsBean;)Lorg/xml/sax/XMLReader;
    .locals 2

    invoke-static {p0}, Lorg/apache/xmlbeans/impl/common/SAXHelper;->saxFactory(Lorg/apache/xmlbeans/XmlOptionsBean;)Ljavax/xml/parsers/SAXParserFactory;

    move-result-object v0

    invoke-virtual {v0}, Ljavax/xml/parsers/SAXParserFactory;->newSAXParser()Ljavax/xml/parsers/SAXParser;

    move-result-object v0

    invoke-virtual {v0}, Ljavax/xml/parsers/SAXParser;->getXMLReader()Lorg/xml/sax/XMLReader;

    move-result-object v0

    sget-object v1, Lorg/apache/xmlbeans/impl/common/SAXHelper;->IGNORING_ENTITY_RESOLVER:Lorg/xml/sax/EntityResolver;

    invoke-interface {v0, v1}, Lorg/xml/sax/XMLReader;->setEntityResolver(Lorg/xml/sax/EntityResolver;)V

    const-string v1, "http://javax.xml.XMLConstants/feature/secure-processing"

    invoke-static {v0, v1}, Lorg/apache/xmlbeans/impl/common/SAXHelper;->trySetSAXFeature(Lorg/xml/sax/XMLReader;Ljava/lang/String;)V

    invoke-static {v0, p0}, Lorg/apache/xmlbeans/impl/common/SAXHelper;->trySetXercesSecurityManager(Lorg/xml/sax/XMLReader;Lorg/apache/xmlbeans/XmlOptionsBean;)V

    return-object v0
.end method

.method public static saxFactory()Ljavax/xml/parsers/SAXParserFactory;
    .locals 1

    .line 1
    new-instance v0, Lorg/apache/xmlbeans/XmlOptionsBean;

    invoke-direct {v0}, Lorg/apache/xmlbeans/XmlOptionsBean;-><init>()V

    invoke-static {v0}, Lorg/apache/xmlbeans/impl/common/SAXHelper;->saxFactory(Lorg/apache/xmlbeans/XmlOptionsBean;)Ljavax/xml/parsers/SAXParserFactory;

    move-result-object v0

    return-object v0
.end method

.method public static saxFactory(Lorg/apache/xmlbeans/XmlOptionsBean;)Ljavax/xml/parsers/SAXParserFactory;
    .locals 3

    .line 2
    invoke-static {}, Ljavax/xml/parsers/SAXParserFactory;->newInstance()Ljavax/xml/parsers/SAXParserFactory;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljavax/xml/parsers/SAXParserFactory;->setValidating(Z)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljavax/xml/parsers/SAXParserFactory;->setNamespaceAware(Z)V

    const-string v2, "http://javax.xml.XMLConstants/feature/secure-processing"

    invoke-static {v0, v2, v1}, Lorg/apache/xmlbeans/impl/common/SAXHelper;->trySetSAXFeature(Ljavax/xml/parsers/SAXParserFactory;Ljava/lang/String;Z)V

    invoke-virtual {p0}, Lorg/apache/xmlbeans/XmlOptionsBean;->isLoadDTDGrammar()Z

    move-result v1

    const-string v2, "http://apache.org/xml/features/nonvalidating/load-dtd-grammar"

    invoke-static {v0, v2, v1}, Lorg/apache/xmlbeans/impl/common/SAXHelper;->trySetSAXFeature(Ljavax/xml/parsers/SAXParserFactory;Ljava/lang/String;Z)V

    const-string v1, "http://apache.org/xml/features/nonvalidating/load-external-dtd"

    invoke-virtual {p0}, Lorg/apache/xmlbeans/XmlOptionsBean;->isLoadExternalDTD()Z

    move-result p0

    invoke-static {v0, v1, p0}, Lorg/apache/xmlbeans/impl/common/SAXHelper;->trySetSAXFeature(Ljavax/xml/parsers/SAXParserFactory;Ljava/lang/String;Z)V

    return-object v0
.end method

.method private static trySetSAXFeature(Ljavax/xml/parsers/SAXParserFactory;Ljava/lang/String;Z)V
    .locals 2

    .line 1
    const/4 v0, 0x5

    :try_start_0
    invoke-virtual {p0, p1, p2}, Ljavax/xml/parsers/SAXParserFactory;->setFeature(Ljava/lang/String;Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/AbstractMethodError; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    sget-object p2, Lorg/apache/xmlbeans/impl/common/SAXHelper;->logger:Lorg/apache/xmlbeans/impl/common/XBLogger;

    const-string v1, "Cannot set SAX feature because outdated XML parser in classpath"

    filled-new-array {v1, p1, p0}, [Ljava/lang/Object;

    move-result-object p0

    invoke-virtual {p2, v0, p0}, Lorg/apache/xmlbeans/impl/common/XBLogger;->log(I[Ljava/lang/Object;)V

    goto :goto_0

    :catch_1
    move-exception p0

    sget-object p2, Lorg/apache/xmlbeans/impl/common/SAXHelper;->logger:Lorg/apache/xmlbeans/impl/common/XBLogger;

    const-string v1, "SAX Feature unsupported"

    filled-new-array {v1, p1, p0}, [Ljava/lang/Object;

    move-result-object p0

    invoke-virtual {p2, v0, p0}, Lorg/apache/xmlbeans/impl/common/XBLogger;->log(I[Ljava/lang/Object;)V

    :goto_0
    return-void
.end method

.method private static trySetSAXFeature(Lorg/xml/sax/XMLReader;Ljava/lang/String;)V
    .locals 3

    .line 2
    const/4 v0, 0x1

    const/4 v1, 0x5

    :try_start_0
    invoke-interface {p0, p1, v0}, Lorg/xml/sax/XMLReader;->setFeature(Ljava/lang/String;Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/AbstractMethodError; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    sget-object v0, Lorg/apache/xmlbeans/impl/common/SAXHelper;->logger:Lorg/apache/xmlbeans/impl/common/XBLogger;

    const-string v2, "Cannot set SAX feature because outdated XML parser in classpath"

    filled-new-array {v2, p1, p0}, [Ljava/lang/Object;

    move-result-object p0

    invoke-virtual {v0, v1, p0}, Lorg/apache/xmlbeans/impl/common/XBLogger;->log(I[Ljava/lang/Object;)V

    goto :goto_0

    :catch_1
    move-exception p0

    sget-object v0, Lorg/apache/xmlbeans/impl/common/SAXHelper;->logger:Lorg/apache/xmlbeans/impl/common/XBLogger;

    const-string v2, "SAX Feature unsupported"

    filled-new-array {v2, p1, p0}, [Ljava/lang/Object;

    move-result-object p0

    invoke-virtual {v0, v1, p0}, Lorg/apache/xmlbeans/impl/common/XBLogger;->log(I[Ljava/lang/Object;)V

    :goto_0
    return-void
.end method

.method private static trySetXercesSecurityManager(Lorg/xml/sax/XMLReader;Lorg/apache/xmlbeans/XmlOptionsBean;)V
    .locals 10

    const-string v0, "org.apache.xerces.util.SecurityManager"

    filled-new-array {v0}, [Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    aget-object v0, v0, v1

    :try_start_0
    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    const-string v3, "setEntityExpansionLimit"

    const/4 v4, 0x1

    new-array v5, v4, [Ljava/lang/Class;

    sget-object v6, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v6, v5, v1

    invoke-virtual {v2, v3, v5}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    new-array v3, v4, [Ljava/lang/Object;

    invoke-virtual {p1}, Lorg/apache/xmlbeans/XmlOptionsBean;->getEntityExpansionLimit()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v1

    invoke-virtual {v2, v0, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "http://apache.org/xml/properties/security-manager"

    invoke-interface {p0, v1, v0}, Lorg/xml/sax/XMLReader;->setProperty(Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    sget-wide v3, Lorg/apache/xmlbeans/impl/common/SAXHelper;->lastLog:J

    sget-object v5, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v6, 0x5

    invoke-virtual {v5, v6, v7}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v8

    add-long/2addr v3, v8

    cmp-long v1, v1, v3

    const-string v2, "SAX Security Manager could not be setup [log suppressed for 5 minutes]"

    const/4 v3, 0x5

    if-lez v1, :cond_0

    sget-object v1, Lorg/apache/xmlbeans/impl/common/SAXHelper;->logger:Lorg/apache/xmlbeans/impl/common/XBLogger;

    filled-new-array {v2, v0}, [Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v1, v3, v0}, Lorg/apache/xmlbeans/impl/common/XBLogger;->log(I[Ljava/lang/Object;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    sput-wide v0, Lorg/apache/xmlbeans/impl/common/SAXHelper;->lastLog:J

    :cond_0
    :try_start_1
    const-string v0, "http://www.oracle.com/xml/jaxp/properties/entityExpansionLimit"

    invoke-virtual {p1}, Lorg/apache/xmlbeans/XmlOptionsBean;->getEntityExpansionLimit()I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {p0, v0, p1}, Lorg/xml/sax/XMLReader;->setProperty(Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_1
    .catch Lorg/xml/sax/SAXException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    sget-wide v4, Lorg/apache/xmlbeans/impl/common/SAXHelper;->lastLog:J

    sget-object p1, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p1, v6, v7}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v6

    add-long/2addr v4, v6

    cmp-long p1, v0, v4

    if-lez p1, :cond_1

    sget-object p1, Lorg/apache/xmlbeans/impl/common/SAXHelper;->logger:Lorg/apache/xmlbeans/impl/common/XBLogger;

    filled-new-array {v2, p0}, [Ljava/lang/Object;

    move-result-object p0

    invoke-virtual {p1, v3, p0}, Lorg/apache/xmlbeans/impl/common/XBLogger;->log(I[Ljava/lang/Object;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide p0

    sput-wide p0, Lorg/apache/xmlbeans/impl/common/SAXHelper;->lastLog:J

    :cond_1
    :goto_0
    return-void
.end method

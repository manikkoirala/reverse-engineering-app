.class public interface abstract Lorg/codehaus/stax2/DTDInfo;
.super Ljava/lang/Object;
.source "SourceFile"


# virtual methods
.method public abstract getDTDInternalSubset()Ljava/lang/String;
.end method

.method public abstract getDTDPublicId()Ljava/lang/String;
.end method

.method public abstract getDTDRootName()Ljava/lang/String;
.end method

.method public abstract getDTDSystemId()Ljava/lang/String;
.end method

.method public abstract getProcessedDTD()Ljava/lang/Object;
.end method

.method public abstract getProcessedDTDSchema()Lorg/codehaus/stax2/validation/DTDValidationSchema;
.end method

.class public Lorg/codehaus/stax2/ri/evt/Stax2EventReaderAdapter;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/codehaus/stax2/XMLEventReader2;


# instance fields
.field protected final mReader:Ljavax/xml/stream/XMLEventReader;


# direct methods
.method public constructor <init>(Ljavax/xml/stream/XMLEventReader;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lorg/codehaus/stax2/ri/evt/Stax2EventReaderAdapter;->mReader:Ljavax/xml/stream/XMLEventReader;

    return-void
.end method

.method public static wrapIfNecessary(Ljavax/xml/stream/XMLEventReader;)Lorg/codehaus/stax2/XMLEventReader2;
    .locals 1

    instance-of v0, p0, Lorg/codehaus/stax2/XMLEventReader2;

    if-eqz v0, :cond_0

    check-cast p0, Lorg/codehaus/stax2/XMLEventReader2;

    return-object p0

    :cond_0
    new-instance v0, Lorg/codehaus/stax2/ri/evt/Stax2EventReaderAdapter;

    invoke-direct {v0, p0}, Lorg/codehaus/stax2/ri/evt/Stax2EventReaderAdapter;-><init>(Ljavax/xml/stream/XMLEventReader;)V

    return-object v0
.end method


# virtual methods
.method public close()V
    .locals 1

    iget-object v0, p0, Lorg/codehaus/stax2/ri/evt/Stax2EventReaderAdapter;->mReader:Ljavax/xml/stream/XMLEventReader;

    invoke-interface {v0}, Ljavax/xml/stream/XMLEventReader;->close()V

    return-void
.end method

.method public getElementText()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/codehaus/stax2/ri/evt/Stax2EventReaderAdapter;->mReader:Ljavax/xml/stream/XMLEventReader;

    invoke-interface {v0}, Ljavax/xml/stream/XMLEventReader;->getElementText()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getProperty(Ljava/lang/String;)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lorg/codehaus/stax2/ri/evt/Stax2EventReaderAdapter;->mReader:Ljavax/xml/stream/XMLEventReader;

    invoke-interface {v0, p1}, Ljavax/xml/stream/XMLEventReader;->getProperty(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public hasNext()Z
    .locals 1

    iget-object v0, p0, Lorg/codehaus/stax2/ri/evt/Stax2EventReaderAdapter;->mReader:Ljavax/xml/stream/XMLEventReader;

    invoke-interface {v0}, Ljavax/xml/stream/XMLEventReader;->hasNext()Z

    move-result v0

    return v0
.end method

.method public hasNextEvent()Z
    .locals 1

    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/evt/Stax2EventReaderAdapter;->peek()Ljavax/xml/stream/events/XMLEvent;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isPropertySupported(Ljava/lang/String;)Z
    .locals 1

    :try_start_0
    iget-object v0, p0, Lorg/codehaus/stax2/ri/evt/Stax2EventReaderAdapter;->mReader:Ljavax/xml/stream/XMLEventReader;

    invoke-interface {v0, p1}, Ljavax/xml/stream/XMLEventReader;->getProperty(Ljava/lang/String;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 p1, 0x1

    return p1

    :catch_0
    const/4 p1, 0x0

    return p1
.end method

.method public next()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lorg/codehaus/stax2/ri/evt/Stax2EventReaderAdapter;->mReader:Ljavax/xml/stream/XMLEventReader;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public nextEvent()Ljavax/xml/stream/events/XMLEvent;
    .locals 1

    iget-object v0, p0, Lorg/codehaus/stax2/ri/evt/Stax2EventReaderAdapter;->mReader:Ljavax/xml/stream/XMLEventReader;

    invoke-interface {v0}, Ljavax/xml/stream/XMLEventReader;->nextEvent()Ljavax/xml/stream/events/XMLEvent;

    move-result-object v0

    return-object v0
.end method

.method public nextTag()Ljavax/xml/stream/events/XMLEvent;
    .locals 1

    iget-object v0, p0, Lorg/codehaus/stax2/ri/evt/Stax2EventReaderAdapter;->mReader:Ljavax/xml/stream/XMLEventReader;

    invoke-interface {v0}, Ljavax/xml/stream/XMLEventReader;->nextTag()Ljavax/xml/stream/events/XMLEvent;

    move-result-object v0

    return-object v0
.end method

.method public peek()Ljavax/xml/stream/events/XMLEvent;
    .locals 1

    iget-object v0, p0, Lorg/codehaus/stax2/ri/evt/Stax2EventReaderAdapter;->mReader:Ljavax/xml/stream/XMLEventReader;

    invoke-interface {v0}, Ljavax/xml/stream/XMLEventReader;->peek()Ljavax/xml/stream/events/XMLEvent;

    move-result-object v0

    return-object v0
.end method

.method public remove()V
    .locals 1

    iget-object v0, p0, Lorg/codehaus/stax2/ri/evt/Stax2EventReaderAdapter;->mReader:Ljavax/xml/stream/XMLEventReader;

    invoke-interface {v0}, Ljava/util/Iterator;->remove()V

    return-void
.end method

.method public setProperty(Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

.class public Lorg/codehaus/stax2/ri/Stax2ReaderAdapter;
.super Ljavax/xml/stream/util/StreamReaderDelegate;
.source "SourceFile"

# interfaces
.implements Lorg/codehaus/stax2/XMLStreamReader2;
.implements Lorg/codehaus/stax2/AttributeInfo;
.implements Lorg/codehaus/stax2/DTDInfo;
.implements Lorg/codehaus/stax2/LocationInfo;


# static fields
.field static final INT_SPACE:I = 0x20

.field private static final MASK_GET_ELEMENT_TEXT:I = 0x1250

.field protected static final MASK_TYPED_ACCESS_BINARY:I = 0x1052


# instance fields
.field protected _base64Decoder:Lorg/codehaus/stax2/ri/typed/StringBase64Decoder;

.field protected _decoderFactory:Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;

.field protected _depth:I

.field protected _typedContent:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljavax/xml/stream/XMLStreamReader;)V
    .locals 0

    invoke-direct {p0, p1}, Ljavax/xml/stream/util/StreamReaderDelegate;-><init>(Ljavax/xml/stream/XMLStreamReader;)V

    const/4 p1, 0x0

    iput-object p1, p0, Lorg/codehaus/stax2/ri/Stax2ReaderAdapter;->_base64Decoder:Lorg/codehaus/stax2/ri/typed/StringBase64Decoder;

    const/4 p1, 0x0

    iput p1, p0, Lorg/codehaus/stax2/ri/Stax2ReaderAdapter;->_depth:I

    return-void
.end method

.method private final checkExpand(Lorg/codehaus/stax2/typed/TypedArrayDecoder;)Z
    .locals 1

    instance-of v0, p1, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$BaseArrayDecoder;

    if-eqz v0, :cond_0

    check-cast p1, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$BaseArrayDecoder;

    invoke-virtual {p1}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$BaseArrayDecoder;->expand()V

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method public static wrapIfNecessary(Ljavax/xml/stream/XMLStreamReader;)Lorg/codehaus/stax2/XMLStreamReader2;
    .locals 1

    instance-of v0, p0, Lorg/codehaus/stax2/XMLStreamReader2;

    if-eqz v0, :cond_0

    check-cast p0, Lorg/codehaus/stax2/XMLStreamReader2;

    return-object p0

    :cond_0
    new-instance v0, Lorg/codehaus/stax2/ri/Stax2ReaderAdapter;

    invoke-direct {v0, p0}, Lorg/codehaus/stax2/ri/Stax2ReaderAdapter;-><init>(Ljavax/xml/stream/XMLStreamReader;)V

    return-object v0
.end method


# virtual methods
.method public _base64Decoder()Lorg/codehaus/stax2/ri/typed/StringBase64Decoder;
    .locals 1

    iget-object v0, p0, Lorg/codehaus/stax2/ri/Stax2ReaderAdapter;->_base64Decoder:Lorg/codehaus/stax2/ri/typed/StringBase64Decoder;

    if-nez v0, :cond_0

    new-instance v0, Lorg/codehaus/stax2/ri/typed/StringBase64Decoder;

    invoke-direct {v0}, Lorg/codehaus/stax2/ri/typed/StringBase64Decoder;-><init>()V

    iput-object v0, p0, Lorg/codehaus/stax2/ri/Stax2ReaderAdapter;->_base64Decoder:Lorg/codehaus/stax2/ri/typed/StringBase64Decoder;

    :cond_0
    iget-object v0, p0, Lorg/codehaus/stax2/ri/Stax2ReaderAdapter;->_base64Decoder:Lorg/codehaus/stax2/ri/typed/StringBase64Decoder;

    return-object v0
.end method

.method public _constructTypeException(Ljava/lang/IllegalArgumentException;Ljava/lang/String;)Lorg/codehaus/stax2/typed/TypedXMLStreamException;
    .locals 3

    .line 1
    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, ""

    :cond_0
    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/Stax2ReaderAdapter;->getStartLocation()Lorg/codehaus/stax2/XMLStreamLocation2;

    move-result-object v1

    if-nez v1, :cond_1

    new-instance v1, Lorg/codehaus/stax2/typed/TypedXMLStreamException;

    invoke-direct {v1, p2, v0, p1}, Lorg/codehaus/stax2/typed/TypedXMLStreamException;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/IllegalArgumentException;)V

    return-object v1

    :cond_1
    new-instance v2, Lorg/codehaus/stax2/typed/TypedXMLStreamException;

    invoke-direct {v2, p2, v0, v1, p1}, Lorg/codehaus/stax2/typed/TypedXMLStreamException;-><init>(Ljava/lang/String;Ljava/lang/String;Ljavax/xml/stream/Location;Ljava/lang/IllegalArgumentException;)V

    return-object v2
.end method

.method public _constructTypeException(Ljava/lang/String;Ljava/lang/String;)Lorg/codehaus/stax2/typed/TypedXMLStreamException;
    .locals 2

    .line 2
    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/Stax2ReaderAdapter;->getStartLocation()Lorg/codehaus/stax2/XMLStreamLocation2;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Lorg/codehaus/stax2/typed/TypedXMLStreamException;

    invoke-direct {v0, p2, p1}, Lorg/codehaus/stax2/typed/TypedXMLStreamException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0

    :cond_0
    new-instance v1, Lorg/codehaus/stax2/typed/TypedXMLStreamException;

    invoke-direct {v1, p2, p1, v0}, Lorg/codehaus/stax2/typed/TypedXMLStreamException;-><init>(Ljava/lang/String;Ljava/lang/String;Ljavax/xml/stream/Location;)V

    return-object v1
.end method

.method public _decoderFactory()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;
    .locals 1

    iget-object v0, p0, Lorg/codehaus/stax2/ri/Stax2ReaderAdapter;->_decoderFactory:Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;

    if-nez v0, :cond_0

    new-instance v0, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;

    invoke-direct {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;-><init>()V

    iput-object v0, p0, Lorg/codehaus/stax2/ri/Stax2ReaderAdapter;->_decoderFactory:Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;

    :cond_0
    iget-object v0, p0, Lorg/codehaus/stax2/ri/Stax2ReaderAdapter;->_decoderFactory:Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;

    return-object v0
.end method

.method public _getAttributeAsArray(Lorg/codehaus/stax2/typed/TypedArrayDecoder;Ljava/lang/String;)I
    .locals 7

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    move-object v3, v2

    move v2, v1

    :goto_0
    if-ge v1, v0, :cond_4

    :cond_0
    :try_start_0
    invoke-virtual {p2, v1}, Ljava/lang/String;->charAt(I)C

    move-result v4

    const/16 v5, 0x20

    if-gt v4, v5, :cond_1

    add-int/lit8 v1, v1, 0x1

    if-lt v1, v0, :cond_0

    goto :goto_2

    :cond_1
    add-int/lit8 v4, v1, 0x1

    :goto_1
    if-ge v4, v0, :cond_2

    invoke-virtual {p2, v4}, Ljava/lang/String;->charAt(I)C

    move-result v6

    if-le v6, v5, :cond_2

    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :cond_2
    add-int/lit8 v5, v4, 0x1

    invoke-virtual {p2, v1, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {p1, v3}, Lorg/codehaus/stax2/typed/TypedArrayDecoder;->decodeValue(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-direct {p0, p1}, Lorg/codehaus/stax2/ri/Stax2ReaderAdapter;->checkExpand(Lorg/codehaus/stax2/typed/TypedArrayDecoder;)Z

    move-result v1
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_3

    goto :goto_2

    :cond_3
    move v1, v5

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-virtual {p0}, Ljavax/xml/stream/util/StreamReaderDelegate;->getLocation()Ljavax/xml/stream/Location;

    move-result-object p2

    new-instance v0, Lorg/codehaus/stax2/typed/TypedXMLStreamException;

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v3, v1, p2, p1}, Lorg/codehaus/stax2/typed/TypedXMLStreamException;-><init>(Ljava/lang/String;Ljava/lang/String;Ljavax/xml/stream/Location;Ljava/lang/IllegalArgumentException;)V

    throw v0

    :cond_4
    :goto_2
    return v2
.end method

.method public closeCompletely()V
    .locals 0

    invoke-virtual {p0}, Ljavax/xml/stream/util/StreamReaderDelegate;->close()V

    return-void
.end method

.method public findAttributeIndex(Ljava/lang/String;Ljava/lang/String;)I
    .locals 3

    const-string v0, ""

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 p1, 0x0

    :cond_0
    invoke-virtual {p0}, Ljavax/xml/stream/util/StreamReaderDelegate;->getAttributeCount()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_4

    invoke-virtual {p0, v1}, Ljavax/xml/stream/util/StreamReaderDelegate;->getAttributeLocalName(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {p0, v1}, Ljavax/xml/stream/util/StreamReaderDelegate;->getAttributeNamespace(I)Ljava/lang/String;

    move-result-object v2

    if-nez p1, :cond_2

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_3

    :cond_1
    return v1

    :cond_2
    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    return v1

    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_4
    const/4 p1, -0x1

    return p1
.end method

.method public getAttributeAs(ILorg/codehaus/stax2/typed/TypedValueDecoder;)V
    .locals 0

    invoke-virtual {p0, p1}, Ljavax/xml/stream/util/StreamReaderDelegate;->getAttributeValue(I)Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lorg/codehaus/stax2/ri/Stax2Util;->trimSpaces(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    if-nez p1, :cond_0

    :try_start_0
    invoke-virtual {p2}, Lorg/codehaus/stax2/typed/TypedValueDecoder;->handleEmptyValue()V

    goto :goto_0

    :cond_0
    invoke-virtual {p2, p1}, Lorg/codehaus/stax2/typed/TypedValueDecoder;->decode(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception p2

    invoke-virtual {p0, p2, p1}, Lorg/codehaus/stax2/ri/Stax2ReaderAdapter;->_constructTypeException(Ljava/lang/IllegalArgumentException;Ljava/lang/String;)Lorg/codehaus/stax2/typed/TypedXMLStreamException;

    move-result-object p1

    throw p1
.end method

.method public getAttributeAsArray(ILorg/codehaus/stax2/typed/TypedArrayDecoder;)I
    .locals 0

    invoke-virtual {p0, p1}, Ljavax/xml/stream/util/StreamReaderDelegate;->getAttributeValue(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p2, p1}, Lorg/codehaus/stax2/ri/Stax2ReaderAdapter;->_getAttributeAsArray(Lorg/codehaus/stax2/typed/TypedArrayDecoder;Ljava/lang/String;)I

    move-result p1

    return p1
.end method

.method public getAttributeAsBinary(I)[B
    .locals 1

    .line 1
    invoke-static {}, Lorg/codehaus/stax2/typed/Base64Variants;->getDefaultVariant()Lorg/codehaus/stax2/typed/Base64Variant;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lorg/codehaus/stax2/ri/Stax2ReaderAdapter;->getAttributeAsBinary(ILorg/codehaus/stax2/typed/Base64Variant;)[B

    move-result-object p1

    return-object p1
.end method

.method public getAttributeAsBinary(ILorg/codehaus/stax2/typed/Base64Variant;)[B
    .locals 3

    .line 2
    invoke-virtual {p0, p1}, Ljavax/xml/stream/util/StreamReaderDelegate;->getAttributeValue(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/Stax2ReaderAdapter;->_base64Decoder()Lorg/codehaus/stax2/ri/typed/StringBase64Decoder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, p2, v1, p1}, Lorg/codehaus/stax2/ri/typed/StringBase64Decoder;->init(Lorg/codehaus/stax2/typed/Base64Variant;ZLjava/lang/String;)V

    :try_start_0
    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/StringBase64Decoder;->decodeCompletely()[B

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p2

    new-instance v0, Lorg/codehaus/stax2/typed/TypedXMLStreamException;

    invoke-virtual {p2}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Ljavax/xml/stream/util/StreamReaderDelegate;->getLocation()Ljavax/xml/stream/Location;

    move-result-object v2

    invoke-direct {v0, p1, v1, v2, p2}, Lorg/codehaus/stax2/typed/TypedXMLStreamException;-><init>(Ljava/lang/String;Ljava/lang/String;Ljavax/xml/stream/Location;Ljava/lang/IllegalArgumentException;)V

    throw v0
.end method

.method public getAttributeAsBoolean(I)Z
    .locals 1

    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/Stax2ReaderAdapter;->_decoderFactory()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;

    move-result-object v0

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;->getBooleanDecoder()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$BooleanDecoder;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lorg/codehaus/stax2/ri/Stax2ReaderAdapter;->getAttributeAs(ILorg/codehaus/stax2/typed/TypedValueDecoder;)V

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$BooleanDecoder;->getValue()Z

    move-result p1

    return p1
.end method

.method public getAttributeAsDecimal(I)Ljava/math/BigDecimal;
    .locals 1

    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/Stax2ReaderAdapter;->_decoderFactory()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;

    move-result-object v0

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;->getDecimalDecoder()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$DecimalDecoder;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lorg/codehaus/stax2/ri/Stax2ReaderAdapter;->getAttributeAs(ILorg/codehaus/stax2/typed/TypedValueDecoder;)V

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$DecimalDecoder;->getValue()Ljava/math/BigDecimal;

    move-result-object p1

    return-object p1
.end method

.method public getAttributeAsDouble(I)D
    .locals 2

    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/Stax2ReaderAdapter;->_decoderFactory()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;

    move-result-object v0

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;->getDoubleDecoder()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$DoubleDecoder;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lorg/codehaus/stax2/ri/Stax2ReaderAdapter;->getAttributeAs(ILorg/codehaus/stax2/typed/TypedValueDecoder;)V

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$DoubleDecoder;->getValue()D

    move-result-wide v0

    return-wide v0
.end method

.method public getAttributeAsDoubleArray(I)[D
    .locals 1

    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/Stax2ReaderAdapter;->_decoderFactory()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;

    move-result-object v0

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;->getDoubleArrayDecoder()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$DoubleArrayDecoder;

    move-result-object v0

    invoke-virtual {p0, p1}, Ljavax/xml/stream/util/StreamReaderDelegate;->getAttributeValue(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, v0, p1}, Lorg/codehaus/stax2/ri/Stax2ReaderAdapter;->_getAttributeAsArray(Lorg/codehaus/stax2/typed/TypedArrayDecoder;Ljava/lang/String;)I

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$DoubleArrayDecoder;->getValues()[D

    move-result-object p1

    return-object p1
.end method

.method public getAttributeAsFloat(I)F
    .locals 1

    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/Stax2ReaderAdapter;->_decoderFactory()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;

    move-result-object v0

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;->getFloatDecoder()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$FloatDecoder;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lorg/codehaus/stax2/ri/Stax2ReaderAdapter;->getAttributeAs(ILorg/codehaus/stax2/typed/TypedValueDecoder;)V

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$FloatDecoder;->getValue()F

    move-result p1

    return p1
.end method

.method public getAttributeAsFloatArray(I)[F
    .locals 1

    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/Stax2ReaderAdapter;->_decoderFactory()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;

    move-result-object v0

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;->getFloatArrayDecoder()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$FloatArrayDecoder;

    move-result-object v0

    invoke-virtual {p0, p1}, Ljavax/xml/stream/util/StreamReaderDelegate;->getAttributeValue(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, v0, p1}, Lorg/codehaus/stax2/ri/Stax2ReaderAdapter;->_getAttributeAsArray(Lorg/codehaus/stax2/typed/TypedArrayDecoder;Ljava/lang/String;)I

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$FloatArrayDecoder;->getValues()[F

    move-result-object p1

    return-object p1
.end method

.method public getAttributeAsInt(I)I
    .locals 1

    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/Stax2ReaderAdapter;->_decoderFactory()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;

    move-result-object v0

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;->getIntDecoder()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$IntDecoder;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lorg/codehaus/stax2/ri/Stax2ReaderAdapter;->getAttributeAs(ILorg/codehaus/stax2/typed/TypedValueDecoder;)V

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$IntDecoder;->getValue()I

    move-result p1

    return p1
.end method

.method public getAttributeAsIntArray(I)[I
    .locals 1

    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/Stax2ReaderAdapter;->_decoderFactory()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;

    move-result-object v0

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;->getIntArrayDecoder()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$IntArrayDecoder;

    move-result-object v0

    invoke-virtual {p0, p1}, Ljavax/xml/stream/util/StreamReaderDelegate;->getAttributeValue(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, v0, p1}, Lorg/codehaus/stax2/ri/Stax2ReaderAdapter;->_getAttributeAsArray(Lorg/codehaus/stax2/typed/TypedArrayDecoder;Ljava/lang/String;)I

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$IntArrayDecoder;->getValues()[I

    move-result-object p1

    return-object p1
.end method

.method public getAttributeAsInteger(I)Ljava/math/BigInteger;
    .locals 1

    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/Stax2ReaderAdapter;->_decoderFactory()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;

    move-result-object v0

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;->getIntegerDecoder()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$IntegerDecoder;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lorg/codehaus/stax2/ri/Stax2ReaderAdapter;->getAttributeAs(ILorg/codehaus/stax2/typed/TypedValueDecoder;)V

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$IntegerDecoder;->getValue()Ljava/math/BigInteger;

    move-result-object p1

    return-object p1
.end method

.method public getAttributeAsLong(I)J
    .locals 2

    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/Stax2ReaderAdapter;->_decoderFactory()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;

    move-result-object v0

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;->getLongDecoder()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$LongDecoder;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lorg/codehaus/stax2/ri/Stax2ReaderAdapter;->getAttributeAs(ILorg/codehaus/stax2/typed/TypedValueDecoder;)V

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$LongDecoder;->getValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public getAttributeAsLongArray(I)[J
    .locals 1

    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/Stax2ReaderAdapter;->_decoderFactory()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;

    move-result-object v0

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;->getLongArrayDecoder()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$LongArrayDecoder;

    move-result-object v0

    invoke-virtual {p0, p1}, Ljavax/xml/stream/util/StreamReaderDelegate;->getAttributeValue(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, v0, p1}, Lorg/codehaus/stax2/ri/Stax2ReaderAdapter;->_getAttributeAsArray(Lorg/codehaus/stax2/typed/TypedArrayDecoder;Ljava/lang/String;)I

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$LongArrayDecoder;->getValues()[J

    move-result-object p1

    return-object p1
.end method

.method public getAttributeAsQName(I)Ljavax/xml/namespace/QName;
    .locals 2

    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/Stax2ReaderAdapter;->_decoderFactory()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;

    move-result-object v0

    invoke-virtual {p0}, Ljavax/xml/stream/util/StreamReaderDelegate;->getNamespaceContext()Ljavax/xml/namespace/NamespaceContext;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;->getQNameDecoder(Ljavax/xml/namespace/NamespaceContext;)Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$QNameDecoder;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lorg/codehaus/stax2/ri/Stax2ReaderAdapter;->getAttributeAs(ILorg/codehaus/stax2/typed/TypedValueDecoder;)V

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$QNameDecoder;->getValue()Ljavax/xml/namespace/QName;

    move-result-object p1

    return-object p1
.end method

.method public getAttributeIndex(Ljava/lang/String;Ljava/lang/String;)I
    .locals 0

    invoke-virtual {p0, p1, p2}, Lorg/codehaus/stax2/ri/Stax2ReaderAdapter;->findAttributeIndex(Ljava/lang/String;Ljava/lang/String;)I

    move-result p1

    return p1
.end method

.method public getAttributeInfo()Lorg/codehaus/stax2/AttributeInfo;
    .locals 2

    invoke-virtual {p0}, Ljavax/xml/stream/util/StreamReaderDelegate;->getEventType()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    invoke-virtual {p0}, Ljavax/xml/stream/util/StreamReaderDelegate;->getEventType()I

    move-result v0

    invoke-virtual {p0, v0}, Lorg/codehaus/stax2/ri/Stax2ReaderAdapter;->throwNotStartElem(I)V

    :cond_0
    return-object p0
.end method

.method public getCurrentLocation()Lorg/codehaus/stax2/XMLStreamLocation2;
    .locals 2

    new-instance v0, Lorg/codehaus/stax2/ri/Stax2LocationAdapter;

    invoke-virtual {p0}, Ljavax/xml/stream/util/StreamReaderDelegate;->getLocation()Ljavax/xml/stream/Location;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/codehaus/stax2/ri/Stax2LocationAdapter;-><init>(Ljavax/xml/stream/Location;)V

    return-object v0
.end method

.method public getDTDInfo()Lorg/codehaus/stax2/DTDInfo;
    .locals 2

    invoke-virtual {p0}, Ljavax/xml/stream/util/StreamReaderDelegate;->getEventType()I

    move-result v0

    const/16 v1, 0xb

    if-eq v0, v1, :cond_0

    const/4 v0, 0x0

    return-object v0

    :cond_0
    return-object p0
.end method

.method public getDTDInternalSubset()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Ljavax/xml/stream/util/StreamReaderDelegate;->getEventType()I

    move-result v0

    const/16 v1, 0xb

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Ljavax/xml/stream/util/StreamReaderDelegate;->getText()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public getDTDPublicId()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getDTDRootName()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getDTDSystemId()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getDepth()I
    .locals 2

    invoke-virtual {p0}, Ljavax/xml/stream/util/StreamReaderDelegate;->getEventType()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    iget v0, p0, Lorg/codehaus/stax2/ri/Stax2ReaderAdapter;->_depth:I

    add-int/lit8 v0, v0, 0x1

    return v0

    :cond_0
    iget v0, p0, Lorg/codehaus/stax2/ri/Stax2ReaderAdapter;->_depth:I

    return v0
.end method

.method public getElementAs(Lorg/codehaus/stax2/typed/TypedValueDecoder;)V
    .locals 1

    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/Stax2ReaderAdapter;->getElementText()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/codehaus/stax2/ri/Stax2Util;->trimSpaces(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    :try_start_0
    invoke-virtual {p1}, Lorg/codehaus/stax2/typed/TypedValueDecoder;->handleEmptyValue()V

    goto :goto_0

    :cond_0
    invoke-virtual {p1, v0}, Lorg/codehaus/stax2/typed/TypedValueDecoder;->decode(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception p1

    invoke-virtual {p0, p1, v0}, Lorg/codehaus/stax2/ri/Stax2ReaderAdapter;->_constructTypeException(Ljava/lang/IllegalArgumentException;Ljava/lang/String;)Lorg/codehaus/stax2/typed/TypedXMLStreamException;

    move-result-object p1

    throw p1
.end method

.method public getElementAsBinary()[B
    .locals 1

    .line 1
    invoke-static {}, Lorg/codehaus/stax2/typed/Base64Variants;->getDefaultVariant()Lorg/codehaus/stax2/typed/Base64Variant;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/codehaus/stax2/ri/Stax2ReaderAdapter;->getElementAsBinary(Lorg/codehaus/stax2/typed/Base64Variant;)[B

    move-result-object v0

    return-object v0
.end method

.method public getElementAsBinary(Lorg/codehaus/stax2/typed/Base64Variant;)[B
    .locals 6

    .line 2
    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/Stax2ReaderAdapter;->_base64Decoder()Lorg/codehaus/stax2/ri/typed/StringBase64Decoder;

    move-result-object v0

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/StringBase64Decoder;->getByteAggregator()Lorg/codehaus/stax2/ri/Stax2Util$ByteAggregator;

    move-result-object v0

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/Stax2Util$ByteAggregator;->startAggregation()[B

    move-result-object v1

    :goto_0
    array-length v2, v1

    const/4 v3, 0x0

    :cond_0
    invoke-virtual {p0, v1, v3, v2, p1}, Lorg/codehaus/stax2/ri/Stax2ReaderAdapter;->readElementAsBinary([BIILorg/codehaus/stax2/typed/Base64Variant;)I

    move-result v4

    const/4 v5, 0x1

    if-ge v4, v5, :cond_1

    invoke-virtual {v0, v1, v3}, Lorg/codehaus/stax2/ri/Stax2Util$ByteAggregator;->aggregateAll([BI)[B

    move-result-object p1

    return-object p1

    :cond_1
    add-int/2addr v3, v4

    sub-int/2addr v2, v4

    if-gtz v2, :cond_0

    invoke-virtual {v0, v1}, Lorg/codehaus/stax2/ri/Stax2Util$ByteAggregator;->addFullBlock([B)[B

    move-result-object v1

    goto :goto_0
.end method

.method public getElementAsBoolean()Z
    .locals 1

    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/Stax2ReaderAdapter;->_decoderFactory()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;

    move-result-object v0

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;->getBooleanDecoder()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$BooleanDecoder;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/codehaus/stax2/ri/Stax2ReaderAdapter;->getElementAs(Lorg/codehaus/stax2/typed/TypedValueDecoder;)V

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$BooleanDecoder;->getValue()Z

    move-result v0

    return v0
.end method

.method public getElementAsDecimal()Ljava/math/BigDecimal;
    .locals 1

    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/Stax2ReaderAdapter;->_decoderFactory()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;

    move-result-object v0

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;->getDecimalDecoder()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$DecimalDecoder;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/codehaus/stax2/ri/Stax2ReaderAdapter;->getElementAs(Lorg/codehaus/stax2/typed/TypedValueDecoder;)V

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$DecimalDecoder;->getValue()Ljava/math/BigDecimal;

    move-result-object v0

    return-object v0
.end method

.method public getElementAsDouble()D
    .locals 2

    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/Stax2ReaderAdapter;->_decoderFactory()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;

    move-result-object v0

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;->getDoubleDecoder()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$DoubleDecoder;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/codehaus/stax2/ri/Stax2ReaderAdapter;->getElementAs(Lorg/codehaus/stax2/typed/TypedValueDecoder;)V

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$DoubleDecoder;->getValue()D

    move-result-wide v0

    return-wide v0
.end method

.method public getElementAsFloat()F
    .locals 1

    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/Stax2ReaderAdapter;->_decoderFactory()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;

    move-result-object v0

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;->getFloatDecoder()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$FloatDecoder;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/codehaus/stax2/ri/Stax2ReaderAdapter;->getElementAs(Lorg/codehaus/stax2/typed/TypedValueDecoder;)V

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$FloatDecoder;->getValue()F

    move-result v0

    return v0
.end method

.method public getElementAsInt()I
    .locals 1

    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/Stax2ReaderAdapter;->_decoderFactory()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;

    move-result-object v0

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;->getIntDecoder()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$IntDecoder;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/codehaus/stax2/ri/Stax2ReaderAdapter;->getElementAs(Lorg/codehaus/stax2/typed/TypedValueDecoder;)V

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$IntDecoder;->getValue()I

    move-result v0

    return v0
.end method

.method public getElementAsInteger()Ljava/math/BigInteger;
    .locals 1

    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/Stax2ReaderAdapter;->_decoderFactory()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;

    move-result-object v0

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;->getIntegerDecoder()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$IntegerDecoder;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/codehaus/stax2/ri/Stax2ReaderAdapter;->getElementAs(Lorg/codehaus/stax2/typed/TypedValueDecoder;)V

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$IntegerDecoder;->getValue()Ljava/math/BigInteger;

    move-result-object v0

    return-object v0
.end method

.method public getElementAsLong()J
    .locals 2

    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/Stax2ReaderAdapter;->_decoderFactory()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;

    move-result-object v0

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;->getLongDecoder()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$LongDecoder;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/codehaus/stax2/ri/Stax2ReaderAdapter;->getElementAs(Lorg/codehaus/stax2/typed/TypedValueDecoder;)V

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$LongDecoder;->getValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public getElementAsQName()Ljavax/xml/namespace/QName;
    .locals 2

    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/Stax2ReaderAdapter;->_decoderFactory()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;

    move-result-object v0

    invoke-virtual {p0}, Ljavax/xml/stream/util/StreamReaderDelegate;->getNamespaceContext()Ljavax/xml/namespace/NamespaceContext;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;->getQNameDecoder(Ljavax/xml/namespace/NamespaceContext;)Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$QNameDecoder;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/codehaus/stax2/ri/Stax2ReaderAdapter;->getElementAs(Lorg/codehaus/stax2/typed/TypedValueDecoder;)V

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$QNameDecoder;->getValue()Ljavax/xml/namespace/QName;

    move-result-object v0

    return-object v0
.end method

.method public getElementText()Ljava/lang/String;
    .locals 3

    invoke-virtual {p0}, Ljavax/xml/stream/util/StreamReaderDelegate;->getEventType()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    move v0, v1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-super {p0}, Ljavax/xml/stream/util/StreamReaderDelegate;->getElementText()Ljava/lang/String;

    move-result-object v2

    if-eqz v0, :cond_1

    iget v0, p0, Lorg/codehaus/stax2/ri/Stax2ReaderAdapter;->_depth:I

    sub-int/2addr v0, v1

    iput v0, p0, Lorg/codehaus/stax2/ri/Stax2ReaderAdapter;->_depth:I

    :cond_1
    return-object v2
.end method

.method public final getEndLocation()Lorg/codehaus/stax2/XMLStreamLocation2;
    .locals 1

    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/Stax2ReaderAdapter;->getCurrentLocation()Lorg/codehaus/stax2/XMLStreamLocation2;

    move-result-object v0

    return-object v0
.end method

.method public getEndingByteOffset()J
    .locals 2

    const-wide/16 v0, -0x1

    return-wide v0
.end method

.method public getEndingCharOffset()J
    .locals 2

    const-wide/16 v0, -0x1

    return-wide v0
.end method

.method public getFeature(Ljava/lang/String;)Ljava/lang/Object;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    const/4 p1, 0x0

    return-object p1
.end method

.method public getIdAttributeIndex()I
    .locals 4

    invoke-virtual {p0}, Ljavax/xml/stream/util/StreamReaderDelegate;->getAttributeCount()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_1

    const-string v2, "ID"

    invoke-virtual {p0, v1}, Ljavax/xml/stream/util/StreamReaderDelegate;->getAttributeType(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    return v1

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, -0x1

    return v0
.end method

.method public final getLocationInfo()Lorg/codehaus/stax2/LocationInfo;
    .locals 0

    return-object p0
.end method

.method public getNonTransientNamespaceContext()Ljavax/xml/namespace/NamespaceContext;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getNotationAttributeIndex()I
    .locals 4

    invoke-virtual {p0}, Ljavax/xml/stream/util/StreamReaderDelegate;->getAttributeCount()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_1

    const-string v2, "NOTATION"

    invoke-virtual {p0, v1}, Ljavax/xml/stream/util/StreamReaderDelegate;->getAttributeType(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    return v1

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, -0x1

    return v0
.end method

.method public getPrefixedName()Ljava/lang/String;
    .locals 5

    invoke-virtual {p0}, Ljavax/xml/stream/util/StreamReaderDelegate;->getEventType()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_3

    const/4 v2, 0x2

    if-eq v0, v2, :cond_3

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/16 v1, 0x9

    if-eq v0, v1, :cond_1

    const/16 v1, 0xb

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/Stax2ReaderAdapter;->getDTDRootName()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Current state not START_ELEMENT, END_ELEMENT, ENTITY_REFERENCE, PROCESSING_INSTRUCTION or DTD"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    invoke-virtual {p0}, Ljavax/xml/stream/util/StreamReaderDelegate;->getLocalName()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_2
    invoke-virtual {p0}, Ljavax/xml/stream/util/StreamReaderDelegate;->getPITarget()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_3
    invoke-virtual {p0}, Ljavax/xml/stream/util/StreamReaderDelegate;->getPrefix()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Ljavax/xml/stream/util/StreamReaderDelegate;->getLocalName()Ljava/lang/String;

    move-result-object v2

    if-eqz v0, :cond_5

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_4

    goto :goto_0

    :cond_4
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v4, v1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    add-int/2addr v4, v1

    invoke-direct {v3, v4}, Ljava/lang/StringBuffer;-><init>(I)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const/16 v0, 0x3a

    invoke-virtual {v3, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_5
    :goto_0
    return-object v2
.end method

.method public getProcessedDTD()Ljava/lang/Object;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getProcessedDTDSchema()Lorg/codehaus/stax2/validation/DTDValidationSchema;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getStartLocation()Lorg/codehaus/stax2/XMLStreamLocation2;
    .locals 1

    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/Stax2ReaderAdapter;->getCurrentLocation()Lorg/codehaus/stax2/XMLStreamLocation2;

    move-result-object v0

    return-object v0
.end method

.method public getStartingByteOffset()J
    .locals 2

    const-wide/16 v0, -0x1

    return-wide v0
.end method

.method public getStartingCharOffset()J
    .locals 2

    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getText(Ljava/io/Writer;Z)I
    .locals 2

    invoke-virtual {p0}, Ljavax/xml/stream/util/StreamReaderDelegate;->getTextCharacters()[C

    move-result-object p2

    invoke-virtual {p0}, Ljavax/xml/stream/util/StreamReaderDelegate;->getTextStart()I

    move-result v0

    invoke-virtual {p0}, Ljavax/xml/stream/util/StreamReaderDelegate;->getTextLength()I

    move-result v1

    if-lez v1, :cond_0

    invoke-virtual {p1, p2, v0, v1}, Ljava/io/Writer;->write([CII)V

    :cond_0
    return v1
.end method

.method public isEmptyElement()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isPropertySupported(Ljava/lang/String;)Z
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

.method public next()I
    .locals 3

    iget-object v0, p0, Lorg/codehaus/stax2/ri/Stax2ReaderAdapter;->_typedContent:Ljava/lang/String;

    const/4 v1, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/codehaus/stax2/ri/Stax2ReaderAdapter;->_typedContent:Ljava/lang/String;

    return v1

    :cond_0
    invoke-super {p0}, Ljavax/xml/stream/util/StreamReaderDelegate;->next()I

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_1

    iget v1, p0, Lorg/codehaus/stax2/ri/Stax2ReaderAdapter;->_depth:I

    add-int/2addr v1, v2

    :goto_0
    iput v1, p0, Lorg/codehaus/stax2/ri/Stax2ReaderAdapter;->_depth:I

    goto :goto_1

    :cond_1
    if-ne v0, v1, :cond_2

    iget v1, p0, Lorg/codehaus/stax2/ri/Stax2ReaderAdapter;->_depth:I

    sub-int/2addr v1, v2

    goto :goto_0

    :cond_2
    :goto_1
    return v0
.end method

.method public readElementAsArray(Lorg/codehaus/stax2/typed/TypedArrayDecoder;)I
    .locals 11

    iget-object v0, p0, Lorg/codehaus/stax2/ri/Stax2ReaderAdapter;->_typedContent:Ljava/lang/String;

    const/4 v1, -0x1

    const/4 v2, 0x1

    if-nez v0, :cond_2

    invoke-virtual {p0}, Ljavax/xml/stream/util/StreamReaderDelegate;->getEventType()I

    move-result v0

    const/4 v3, 0x2

    if-ne v0, v3, :cond_0

    return v1

    :cond_0
    if-ne v0, v2, :cond_1

    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/Stax2ReaderAdapter;->getElementText()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/codehaus/stax2/ri/Stax2ReaderAdapter;->_typedContent:Ljava/lang/String;

    goto :goto_0

    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "First call to readElementAsArray() must be for a START_ELEMENT"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_2
    :goto_0
    iget-object v0, p0, Lorg/codehaus/stax2/ri/Stax2ReaderAdapter;->_typedContent:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    move v6, v4

    move-object v7, v5

    :cond_3
    if-ge v4, v3, :cond_8

    :cond_4
    :try_start_0
    invoke-virtual {v0, v4}, Ljava/lang/String;->charAt(I)C

    move-result v8
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    const/16 v9, 0x20

    if-gt v8, v9, :cond_5

    add-int/lit8 v4, v4, 0x1

    if-lt v4, v3, :cond_4

    goto :goto_5

    :cond_5
    add-int/lit8 v8, v4, 0x1

    :goto_1
    if-ge v8, v3, :cond_6

    :try_start_1
    invoke-virtual {v0, v8}, Ljava/lang/String;->charAt(I)C

    move-result v10

    if-le v10, v9, :cond_6

    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    :cond_6
    add-int/lit8 v6, v6, 0x1

    invoke-virtual {v0, v4, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    add-int/lit8 v4, v8, 0x1

    :try_start_2
    invoke-virtual {p1, v7}, Lorg/codehaus/stax2/typed/TypedArrayDecoder;->decodeValue(Ljava/lang/String;)Z

    move-result v8
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    if-eqz v8, :cond_3

    goto :goto_5

    :catchall_0
    move-exception p1

    move v4, v8

    goto :goto_3

    :catch_0
    move-exception p1

    move v4, v8

    goto :goto_2

    :catchall_1
    move-exception p1

    goto :goto_3

    :catch_1
    move-exception p1

    :goto_2
    :try_start_3
    invoke-virtual {p0}, Ljavax/xml/stream/util/StreamReaderDelegate;->getLocation()Ljavax/xml/stream/Location;

    move-result-object v1

    new-instance v6, Lorg/codehaus/stax2/typed/TypedXMLStreamException;

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v6, v7, v8, v1, p1}, Lorg/codehaus/stax2/typed/TypedXMLStreamException;-><init>(Ljava/lang/String;Ljava/lang/String;Ljavax/xml/stream/Location;Ljava/lang/IllegalArgumentException;)V

    throw v6
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :goto_3
    sub-int/2addr v3, v4

    if-ge v3, v2, :cond_7

    goto :goto_4

    :cond_7
    invoke-virtual {v0, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    :goto_4
    iput-object v5, p0, Lorg/codehaus/stax2/ri/Stax2ReaderAdapter;->_typedContent:Ljava/lang/String;

    throw p1

    :cond_8
    :goto_5
    sub-int/2addr v3, v4

    if-ge v3, v2, :cond_9

    goto :goto_6

    :cond_9
    invoke-virtual {v0, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    :goto_6
    iput-object v5, p0, Lorg/codehaus/stax2/ri/Stax2ReaderAdapter;->_typedContent:Ljava/lang/String;

    if-ge v6, v2, :cond_a

    goto :goto_7

    :cond_a
    move v1, v6

    :goto_7
    return v1
.end method

.method public readElementAsBinary([BII)I
    .locals 1

    .line 1
    invoke-static {}, Lorg/codehaus/stax2/typed/Base64Variants;->getDefaultVariant()Lorg/codehaus/stax2/typed/Base64Variant;

    move-result-object v0

    invoke-virtual {p0, p1, p2, p3, v0}, Lorg/codehaus/stax2/ri/Stax2ReaderAdapter;->readElementAsBinary([BIILorg/codehaus/stax2/typed/Base64Variant;)I

    move-result p1

    return p1
.end method

.method public readElementAsBinary([BIILorg/codehaus/stax2/typed/Base64Variant;)I
    .locals 11

    .line 2
    const-string v0, ""

    if-eqz p1, :cond_12

    if-ltz p2, :cond_11

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-lt p3, v2, :cond_f

    add-int v3, p2, p3

    array-length v4, p1

    if-le v3, v4, :cond_0

    goto/16 :goto_4

    :cond_0
    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/Stax2ReaderAdapter;->_base64Decoder()Lorg/codehaus/stax2/ri/typed/StringBase64Decoder;

    move-result-object v3

    invoke-virtual {p0}, Ljavax/xml/stream/util/StreamReaderDelegate;->getEventType()I

    move-result v4

    shl-int v5, v2, v4

    and-int/lit16 v5, v5, 0x1052

    const/4 v6, -0x1

    const/4 v7, 0x2

    if-nez v5, :cond_2

    if-ne v4, v7, :cond_1

    invoke-virtual {v3}, Lorg/codehaus/stax2/ri/typed/StringBase64Decoder;->hasData()Z

    move-result v5

    if-nez v5, :cond_2

    return v6

    :cond_1
    invoke-virtual {p0, v4}, Lorg/codehaus/stax2/ri/Stax2ReaderAdapter;->throwNotStartElemOrTextual(I)V

    :cond_2
    const/4 v5, 0x3

    const/4 v8, 0x5

    if-ne v4, v2, :cond_7

    :cond_3
    :goto_0
    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/Stax2ReaderAdapter;->next()I

    move-result v4

    if-ne v4, v7, :cond_4

    return v6

    :cond_4
    if-eq v4, v8, :cond_3

    if-ne v4, v5, :cond_5

    goto :goto_0

    :cond_5
    shl-int v9, v2, v4

    and-int/lit16 v9, v9, 0x1250

    if-nez v9, :cond_6

    invoke-virtual {p0, v4}, Lorg/codehaus/stax2/ri/Stax2ReaderAdapter;->throwNotStartElemOrTextual(I)V

    :cond_6
    invoke-virtual {p0}, Ljavax/xml/stream/util/StreamReaderDelegate;->getText()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, p4, v2, v4}, Lorg/codehaus/stax2/ri/typed/StringBase64Decoder;->init(Lorg/codehaus/stax2/typed/Base64Variant;ZLjava/lang/String;)V

    :cond_7
    move v4, v1

    :goto_1
    :try_start_0
    invoke-virtual {v3, p1, p2, p3}, Lorg/codehaus/stax2/ri/typed/StringBase64Decoder;->decode([BII)I

    move-result v9
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    add-int/2addr p2, v9

    add-int/2addr v4, v9

    sub-int/2addr p3, v9

    if-lt p3, v2, :cond_d

    invoke-virtual {p0}, Ljavax/xml/stream/util/StreamReaderDelegate;->getEventType()I

    move-result v9

    if-ne v9, v7, :cond_8

    goto :goto_3

    :cond_8
    :goto_2
    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/Stax2ReaderAdapter;->next()I

    move-result v9

    if-eq v9, v8, :cond_8

    if-eq v9, v5, :cond_8

    const/4 v10, 0x6

    if-ne v9, v10, :cond_9

    goto :goto_2

    :cond_9
    if-ne v9, v7, :cond_b

    invoke-virtual {v3}, Lorg/codehaus/stax2/ri/typed/StringBase64Decoder;->endOfContent()I

    move-result v9

    if-ltz v9, :cond_a

    if-lez v9, :cond_d

    goto :goto_1

    :cond_a
    const-string p1, "Incomplete base64 triplet at the end of decoded content"

    invoke-virtual {p0, p1, v0}, Lorg/codehaus/stax2/ri/Stax2ReaderAdapter;->_constructTypeException(Ljava/lang/String;Ljava/lang/String;)Lorg/codehaus/stax2/typed/TypedXMLStreamException;

    move-result-object p1

    throw p1

    :cond_b
    shl-int v10, v2, v9

    and-int/lit16 v10, v10, 0x1250

    if-nez v10, :cond_c

    invoke-virtual {p0, v9}, Lorg/codehaus/stax2/ri/Stax2ReaderAdapter;->throwNotStartElemOrTextual(I)V

    :cond_c
    invoke-virtual {p0}, Ljavax/xml/stream/util/StreamReaderDelegate;->getText()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v3, p4, v1, v9}, Lorg/codehaus/stax2/ri/typed/StringBase64Decoder;->init(Lorg/codehaus/stax2/typed/Base64Variant;ZLjava/lang/String;)V

    goto :goto_1

    :cond_d
    :goto_3
    if-lez v4, :cond_e

    move v6, v4

    :cond_e
    return v6

    :catch_0
    move-exception p1

    invoke-virtual {p0, p1, v0}, Lorg/codehaus/stax2/ri/Stax2ReaderAdapter;->_constructTypeException(Ljava/lang/IllegalArgumentException;Ljava/lang/String;)Lorg/codehaus/stax2/typed/TypedXMLStreamException;

    move-result-object p1

    throw p1

    :cond_f
    :goto_4
    if-nez p3, :cond_10

    return v1

    :cond_10
    new-instance p2, Ljava/lang/IllegalArgumentException;

    new-instance p4, Ljava/lang/StringBuilder;

    invoke-direct {p4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Illegal maxLength ("

    invoke-virtual {p4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p4, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p3, "), has to be positive number, and offset+maxLength can not exceed"

    invoke-virtual {p4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    array-length p1, p1

    invoke-virtual {p4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p2

    :cond_11
    new-instance p3, Ljava/lang/IllegalArgumentException;

    new-instance p4, Ljava/lang/StringBuilder;

    invoke-direct {p4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Illegal offset ("

    invoke-virtual {p4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p2, "), must be [0, "

    invoke-virtual {p4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    array-length p1, p1

    invoke-virtual {p4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, "["

    invoke-virtual {p4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p3, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p3

    :cond_12
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "resultBuffer is null"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public readElementAsDoubleArray([DII)I
    .locals 1

    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/Stax2ReaderAdapter;->_decoderFactory()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;->getDoubleArrayDecoder([DII)Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$DoubleArrayDecoder;

    move-result-object p1

    invoke-virtual {p0, p1}, Lorg/codehaus/stax2/ri/Stax2ReaderAdapter;->readElementAsArray(Lorg/codehaus/stax2/typed/TypedArrayDecoder;)I

    move-result p1

    return p1
.end method

.method public readElementAsFloatArray([FII)I
    .locals 1

    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/Stax2ReaderAdapter;->_decoderFactory()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;->getFloatArrayDecoder([FII)Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$FloatArrayDecoder;

    move-result-object p1

    invoke-virtual {p0, p1}, Lorg/codehaus/stax2/ri/Stax2ReaderAdapter;->readElementAsArray(Lorg/codehaus/stax2/typed/TypedArrayDecoder;)I

    move-result p1

    return p1
.end method

.method public readElementAsIntArray([III)I
    .locals 1

    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/Stax2ReaderAdapter;->_decoderFactory()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;->getIntArrayDecoder([III)Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$IntArrayDecoder;

    move-result-object p1

    invoke-virtual {p0, p1}, Lorg/codehaus/stax2/ri/Stax2ReaderAdapter;->readElementAsArray(Lorg/codehaus/stax2/typed/TypedArrayDecoder;)I

    move-result p1

    return p1
.end method

.method public readElementAsLongArray([JII)I
    .locals 1

    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/Stax2ReaderAdapter;->_decoderFactory()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;->getLongArrayDecoder([JII)Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$LongArrayDecoder;

    move-result-object p1

    invoke-virtual {p0, p1}, Lorg/codehaus/stax2/ri/Stax2ReaderAdapter;->readElementAsArray(Lorg/codehaus/stax2/typed/TypedArrayDecoder;)I

    move-result p1

    return p1
.end method

.method public setFeature(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    return-void
.end method

.method public setProperty(Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

.method public setValidationProblemHandler(Lorg/codehaus/stax2/validation/ValidationProblemHandler;)Lorg/codehaus/stax2/validation/ValidationProblemHandler;
    .locals 0

    const/4 p1, 0x0

    return-object p1
.end method

.method public skipElement()V
    .locals 4

    invoke-virtual {p0}, Ljavax/xml/stream/util/StreamReaderDelegate;->getEventType()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    invoke-virtual {p0}, Ljavax/xml/stream/util/StreamReaderDelegate;->getEventType()I

    move-result v0

    invoke-virtual {p0, v0}, Lorg/codehaus/stax2/ri/Stax2ReaderAdapter;->throwNotStartElem(I)V

    :cond_0
    move v0, v1

    :cond_1
    :goto_0
    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/Stax2ReaderAdapter;->next()I

    move-result v2

    if-ne v2, v1, :cond_2

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    const/4 v3, 0x2

    if-ne v2, v3, :cond_1

    add-int/lit8 v0, v0, -0x1

    if-nez v0, :cond_1

    return-void
.end method

.method public stopValidatingAgainst(Lorg/codehaus/stax2/validation/XMLValidationSchema;)Lorg/codehaus/stax2/validation/XMLValidator;
    .locals 0

    .line 1
    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/Stax2ReaderAdapter;->throwUnsupported()V

    const/4 p1, 0x0

    return-object p1
.end method

.method public stopValidatingAgainst(Lorg/codehaus/stax2/validation/XMLValidator;)Lorg/codehaus/stax2/validation/XMLValidator;
    .locals 0

    .line 2
    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/Stax2ReaderAdapter;->throwUnsupported()V

    const/4 p1, 0x0

    return-object p1
.end method

.method public throwNotStartElem(I)V
    .locals 3

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Current event ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {p1}, Lorg/codehaus/stax2/ri/Stax2Util;->eventTypeDesc(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, ") not START_ELEMENT"

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public throwNotStartElemOrTextual(I)V
    .locals 3

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Current event ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {p1}, Lorg/codehaus/stax2/ri/Stax2Util;->eventTypeDesc(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, ") not START_ELEMENT, END_ELEMENT, CHARACTERS or CDATA"

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public throwUnsupported()V
    .locals 2

    new-instance v0, Ljavax/xml/stream/XMLStreamException;

    const-string v1, "Unsupported method"

    invoke-direct {v0, v1}, Ljavax/xml/stream/XMLStreamException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public validateAgainst(Lorg/codehaus/stax2/validation/XMLValidationSchema;)Lorg/codehaus/stax2/validation/XMLValidator;
    .locals 0

    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/Stax2ReaderAdapter;->throwUnsupported()V

    const/4 p1, 0x0

    return-object p1
.end method

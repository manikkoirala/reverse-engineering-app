.class public final Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$BooleanDecoder;
.super Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$DecoderBase;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "BooleanDecoder"
.end annotation


# instance fields
.field protected mValue:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$DecoderBase;-><init>()V

    return-void
.end method


# virtual methods
.method public decode(Ljava/lang/String;)V
    .locals 9

    .line 1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0x74

    const/16 v4, 0x65

    const/4 v5, 0x3

    const/4 v6, 0x2

    const/4 v7, 0x4

    const/4 v8, 0x1

    if-ne v2, v3, :cond_0

    if-ne v0, v7, :cond_3

    invoke-virtual {p1, v8}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v1, 0x72

    if-ne v0, v1, :cond_3

    invoke-virtual {p1, v6}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v1, 0x75

    if-ne v0, v1, :cond_3

    invoke-virtual {p1, v5}, Ljava/lang/String;->charAt(I)C

    move-result v0

    if-ne v0, v4, :cond_3

    iput-boolean v8, p0, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$BooleanDecoder;->mValue:Z

    return-void

    :cond_0
    const/16 v3, 0x66

    if-ne v2, v3, :cond_1

    const/4 v2, 0x5

    if-ne v0, v2, :cond_3

    invoke-virtual {p1, v8}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v2, 0x61

    if-ne v0, v2, :cond_3

    invoke-virtual {p1, v6}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v2, 0x6c

    if-ne v0, v2, :cond_3

    invoke-virtual {p1, v5}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v2, 0x73

    if-ne v0, v2, :cond_3

    invoke-virtual {p1, v7}, Ljava/lang/String;->charAt(I)C

    move-result v0

    if-ne v0, v4, :cond_3

    iput-boolean v1, p0, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$BooleanDecoder;->mValue:Z

    return-void

    :cond_1
    const/16 v3, 0x30

    if-ne v2, v3, :cond_2

    if-ne v0, v8, :cond_3

    iput-boolean v1, p0, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$BooleanDecoder;->mValue:Z

    return-void

    :cond_2
    const/16 v1, 0x31

    if-ne v2, v1, :cond_3

    if-ne v0, v8, :cond_3

    iput-boolean v8, p0, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$BooleanDecoder;->mValue:Z

    return-void

    :cond_3
    invoke-virtual {p0, p1}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$DecoderBase;->constructInvalidValue(Ljava/lang/String;)Ljava/lang/IllegalArgumentException;

    move-result-object p1

    throw p1
.end method

.method public decode([CII)V
    .locals 6

    .line 2
    sub-int v0, p3, p2

    aget-char v1, p1, p2

    const/16 v2, 0x74

    const/16 v3, 0x65

    const/4 v4, 0x1

    if-ne v1, v2, :cond_0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_3

    add-int/lit8 v0, p2, 0x1

    aget-char v0, p1, v0

    const/16 v1, 0x72

    if-ne v0, v1, :cond_3

    add-int/lit8 v0, p2, 0x2

    aget-char v0, p1, v0

    const/16 v1, 0x75

    if-ne v0, v1, :cond_3

    add-int/lit8 v0, p2, 0x3

    aget-char v0, p1, v0

    if-ne v0, v3, :cond_3

    iput-boolean v4, p0, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$BooleanDecoder;->mValue:Z

    return-void

    :cond_0
    const/16 v2, 0x66

    const/4 v5, 0x0

    if-ne v1, v2, :cond_1

    const/4 v1, 0x5

    if-ne v0, v1, :cond_3

    add-int/lit8 v0, p2, 0x1

    aget-char v0, p1, v0

    const/16 v1, 0x61

    if-ne v0, v1, :cond_3

    add-int/lit8 v0, p2, 0x2

    aget-char v0, p1, v0

    const/16 v1, 0x6c

    if-ne v0, v1, :cond_3

    add-int/lit8 v0, p2, 0x3

    aget-char v0, p1, v0

    const/16 v1, 0x73

    if-ne v0, v1, :cond_3

    add-int/lit8 v0, p2, 0x4

    aget-char v0, p1, v0

    if-ne v0, v3, :cond_3

    iput-boolean v5, p0, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$BooleanDecoder;->mValue:Z

    return-void

    :cond_1
    const/16 v2, 0x30

    if-ne v1, v2, :cond_2

    if-ne v0, v4, :cond_3

    iput-boolean v5, p0, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$BooleanDecoder;->mValue:Z

    return-void

    :cond_2
    const/16 v2, 0x31

    if-ne v1, v2, :cond_3

    if-ne v0, v4, :cond_3

    iput-boolean v4, p0, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$BooleanDecoder;->mValue:Z

    return-void

    :cond_3
    invoke-virtual {p0, p1, p2, p3}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$DecoderBase;->constructInvalidValue([CII)Ljava/lang/IllegalArgumentException;

    move-result-object p1

    throw p1
.end method

.method public getType()Ljava/lang/String;
    .locals 1

    const-string v0, "boolean"

    return-object v0
.end method

.method public getValue()Z
    .locals 1

    iget-boolean v0, p0, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$BooleanDecoder;->mValue:Z

    return v0
.end method

.class public Lorg/codehaus/stax2/ri/typed/SimpleValueEncoder;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field protected final mBuffer:[C

.field protected final mEncoderFactory:Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0x1f4

    new-array v0, v0, [C

    iput-object v0, p0, Lorg/codehaus/stax2/ri/typed/SimpleValueEncoder;->mBuffer:[C

    new-instance v0, Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory;

    invoke-direct {v0}, Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory;-><init>()V

    iput-object v0, p0, Lorg/codehaus/stax2/ri/typed/SimpleValueEncoder;->mEncoderFactory:Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory;

    return-void
.end method


# virtual methods
.method public encode(Lorg/codehaus/stax2/ri/typed/AsciiValueEncoder;)Ljava/lang/String;
    .locals 4

    iget-object v0, p0, Lorg/codehaus/stax2/ri/typed/SimpleValueEncoder;->mBuffer:[C

    array-length v1, v0

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v2, v1}, Lorg/codehaus/stax2/ri/typed/AsciiValueEncoder;->encodeMore([CII)I

    move-result v0

    invoke-virtual {p1}, Lorg/codehaus/stax2/ri/typed/AsciiValueEncoder;->isCompleted()Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance p1, Ljava/lang/String;

    iget-object v1, p0, Lorg/codehaus/stax2/ri/typed/SimpleValueEncoder;->mBuffer:[C

    invoke-direct {p1, v1, v2, v0}, Ljava/lang/String;-><init>([CII)V

    return-object p1

    :cond_0
    new-instance v1, Ljava/lang/StringBuffer;

    iget-object v3, p0, Lorg/codehaus/stax2/ri/typed/SimpleValueEncoder;->mBuffer:[C

    array-length v3, v3

    shl-int/lit8 v3, v3, 0x1

    invoke-direct {v1, v3}, Ljava/lang/StringBuffer;-><init>(I)V

    iget-object v3, p0, Lorg/codehaus/stax2/ri/typed/SimpleValueEncoder;->mBuffer:[C

    invoke-virtual {v1, v3, v2, v0}, Ljava/lang/StringBuffer;->append([CII)Ljava/lang/StringBuffer;

    :cond_1
    iget-object v0, p0, Lorg/codehaus/stax2/ri/typed/SimpleValueEncoder;->mBuffer:[C

    array-length v3, v0

    invoke-virtual {p1, v0, v2, v3}, Lorg/codehaus/stax2/ri/typed/AsciiValueEncoder;->encodeMore([CII)I

    move-result v0

    iget-object v3, p0, Lorg/codehaus/stax2/ri/typed/SimpleValueEncoder;->mBuffer:[C

    invoke-virtual {v1, v3, v2, v0}, Ljava/lang/StringBuffer;->append([CII)Ljava/lang/StringBuffer;

    invoke-virtual {p1}, Lorg/codehaus/stax2/ri/typed/AsciiValueEncoder;->isCompleted()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public encodeAsString(Lorg/codehaus/stax2/typed/Base64Variant;[BII)Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lorg/codehaus/stax2/ri/typed/SimpleValueEncoder;->mEncoderFactory:Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory;

    invoke-virtual {v0, p1, p2, p3, p4}, Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory;->getEncoder(Lorg/codehaus/stax2/typed/Base64Variant;[BII)Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory$Base64Encoder;

    move-result-object p1

    invoke-virtual {p0, p1}, Lorg/codehaus/stax2/ri/typed/SimpleValueEncoder;->encode(Lorg/codehaus/stax2/ri/typed/AsciiValueEncoder;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public encodeAsString([DII)Ljava/lang/String;
    .locals 1

    .line 2
    iget-object v0, p0, Lorg/codehaus/stax2/ri/typed/SimpleValueEncoder;->mEncoderFactory:Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory;

    invoke-virtual {v0, p1, p2, p3}, Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory;->getEncoder([DII)Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory$DoubleArrayEncoder;

    move-result-object p1

    invoke-virtual {p0, p1}, Lorg/codehaus/stax2/ri/typed/SimpleValueEncoder;->encode(Lorg/codehaus/stax2/ri/typed/AsciiValueEncoder;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public encodeAsString([FII)Ljava/lang/String;
    .locals 1

    .line 3
    iget-object v0, p0, Lorg/codehaus/stax2/ri/typed/SimpleValueEncoder;->mEncoderFactory:Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory;

    invoke-virtual {v0, p1, p2, p3}, Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory;->getEncoder([FII)Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory$FloatArrayEncoder;

    move-result-object p1

    invoke-virtual {p0, p1}, Lorg/codehaus/stax2/ri/typed/SimpleValueEncoder;->encode(Lorg/codehaus/stax2/ri/typed/AsciiValueEncoder;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public encodeAsString([III)Ljava/lang/String;
    .locals 1

    .line 4
    iget-object v0, p0, Lorg/codehaus/stax2/ri/typed/SimpleValueEncoder;->mEncoderFactory:Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory;

    invoke-virtual {v0, p1, p2, p3}, Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory;->getEncoder([III)Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory$IntArrayEncoder;

    move-result-object p1

    invoke-virtual {p0, p1}, Lorg/codehaus/stax2/ri/typed/SimpleValueEncoder;->encode(Lorg/codehaus/stax2/ri/typed/AsciiValueEncoder;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public encodeAsString([JII)Ljava/lang/String;
    .locals 1

    .line 5
    iget-object v0, p0, Lorg/codehaus/stax2/ri/typed/SimpleValueEncoder;->mEncoderFactory:Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory;

    invoke-virtual {v0, p1, p2, p3}, Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory;->getEncoder([JII)Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory$LongArrayEncoder;

    move-result-object p1

    invoke-virtual {p0, p1}, Lorg/codehaus/stax2/ri/typed/SimpleValueEncoder;->encode(Lorg/codehaus/stax2/ri/typed/AsciiValueEncoder;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.class final Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory$DoubleEncoder;
.super Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory$TypedScalarEncoder;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DoubleEncoder"
.end annotation


# instance fields
.field _value:D


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory$TypedScalarEncoder;-><init>()V

    return-void
.end method


# virtual methods
.method public encodeMore([BII)I
    .locals 2

    .line 1
    iget-wide v0, p0, Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory$DoubleEncoder;->_value:D

    invoke-static {v0, v1, p1, p2}, Lorg/codehaus/stax2/ri/typed/NumberUtil;->writeDouble(D[BI)I

    move-result p1

    return p1
.end method

.method public encodeMore([CII)I
    .locals 2

    .line 2
    iget-wide v0, p0, Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory$DoubleEncoder;->_value:D

    invoke-static {v0, v1, p1, p2}, Lorg/codehaus/stax2/ri/typed/NumberUtil;->writeDouble(D[CI)I

    move-result p1

    return p1
.end method

.method public reset(D)V
    .locals 0

    iput-wide p1, p0, Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory$DoubleEncoder;->_value:D

    return-void
.end method

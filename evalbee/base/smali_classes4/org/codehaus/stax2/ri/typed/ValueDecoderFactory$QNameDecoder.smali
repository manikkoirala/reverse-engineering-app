.class public final Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$QNameDecoder;
.super Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$DecoderBase;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "QNameDecoder"
.end annotation


# instance fields
.field final mNsCtxt:Ljavax/xml/namespace/NamespaceContext;

.field protected mValue:Ljavax/xml/namespace/QName;


# direct methods
.method public constructor <init>(Ljavax/xml/namespace/NamespaceContext;)V
    .locals 0

    invoke-direct {p0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$DecoderBase;-><init>()V

    iput-object p1, p0, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$QNameDecoder;->mNsCtxt:Ljavax/xml/namespace/NamespaceContext;

    return-void
.end method


# virtual methods
.method public decode(Ljava/lang/String;)V
    .locals 2

    .line 1
    const/16 v0, 0x3a

    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    if-ltz v0, :cond_0

    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, v1, p1}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$QNameDecoder;->resolveQName(Ljava/lang/String;Ljava/lang/String;)Ljavax/xml/namespace/QName;

    move-result-object p1

    goto :goto_0

    :cond_0
    invoke-virtual {p0, p1}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$QNameDecoder;->resolveQName(Ljava/lang/String;)Ljavax/xml/namespace/QName;

    move-result-object p1

    :goto_0
    iput-object p1, p0, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$QNameDecoder;->mValue:Ljavax/xml/namespace/QName;

    return-void
.end method

.method public decode([CII)V
    .locals 3

    .line 2
    move v0, p2

    :goto_0
    if-ge v0, p3, :cond_1

    aget-char v1, p1, v0

    const/16 v2, 0x3a

    if-ne v1, v2, :cond_0

    new-instance v1, Ljava/lang/String;

    sub-int v2, v0, p2

    invoke-direct {v1, p1, p2, v2}, Ljava/lang/String;-><init>([CII)V

    new-instance p2, Ljava/lang/String;

    add-int/lit8 v2, v0, 0x1

    sub-int/2addr p3, v0

    add-int/lit8 p3, p3, -0x1

    invoke-direct {p2, p1, v2, p3}, Ljava/lang/String;-><init>([CII)V

    invoke-virtual {p0, v1, p2}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$QNameDecoder;->resolveQName(Ljava/lang/String;Ljava/lang/String;)Ljavax/xml/namespace/QName;

    move-result-object p1

    :goto_1
    iput-object p1, p0, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$QNameDecoder;->mValue:Ljavax/xml/namespace/QName;

    return-void

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/lang/String;

    sub-int/2addr p3, p2

    invoke-direct {v0, p1, p2, p3}, Ljava/lang/String;-><init>([CII)V

    invoke-virtual {p0, v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$QNameDecoder;->resolveQName(Ljava/lang/String;)Ljavax/xml/namespace/QName;

    move-result-object p1

    goto :goto_1
.end method

.method public getType()Ljava/lang/String;
    .locals 1

    const-string v0, "QName"

    return-object v0
.end method

.method public getValue()Ljavax/xml/namespace/QName;
    .locals 1

    iget-object v0, p0, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$QNameDecoder;->mValue:Ljavax/xml/namespace/QName;

    return-object v0
.end method

.method public resolveQName(Ljava/lang/String;)Ljavax/xml/namespace/QName;
    .locals 2

    .line 1
    iget-object v0, p0, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$QNameDecoder;->mNsCtxt:Ljavax/xml/namespace/NamespaceContext;

    const-string v1, ""

    invoke-interface {v0, v1}, Ljavax/xml/namespace/NamespaceContext;->getNamespaceURI(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    move-object v1, v0

    :goto_0
    new-instance v0, Ljavax/xml/namespace/QName;

    invoke-direct {v0, v1, p1}, Ljavax/xml/namespace/QName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public resolveQName(Ljava/lang/String;Ljava/lang/String;)Ljavax/xml/namespace/QName;
    .locals 4

    .line 2
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const-string v1, ":"

    if-eqz v0, :cond_1

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$QNameDecoder;->mNsCtxt:Ljavax/xml/namespace/NamespaceContext;

    invoke-interface {v0, p1}, Ljavax/xml/namespace/NamespaceContext;->getNamespaceURI(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    new-instance v1, Ljavax/xml/namespace/QName;

    invoke-direct {v1, v0, p2, p1}, Ljavax/xml/namespace/QName;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v1

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Value \""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p0, p2}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$DecoderBase;->lexicalDesc(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, "\" not a valid QName: prefix \'"

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "\' not bound to a namespace"

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$DecoderBase;->constructInvalidValue(Ljava/lang/String;)Ljava/lang/IllegalArgumentException;

    move-result-object p1

    throw p1
.end method

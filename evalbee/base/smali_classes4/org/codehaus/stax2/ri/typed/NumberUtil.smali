.class public final Lorg/codehaus/stax2/ri/typed/NumberUtil;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final BILLION:I = 0x3b9aca00

.field private static final BYTE_1:B = 0x31t

.field private static final BYTE_2:B = 0x32t

.field private static final BYTE_HYPHEN:B = 0x2dt

.field static final FULL_TRIPLETS:[C

.field static final LEADING_TRIPLETS:[C

.field public static final MAX_DOUBLE_CLEN:I = 0x20

.field public static final MAX_FLOAT_CLEN:I = 0x20

.field private static MAX_INT_AS_LONG:J = 0x7fffffffL

.field public static final MAX_INT_CLEN:I = 0xb

.field public static final MAX_LONG_CLEN:I = 0x15

.field private static final MILLION:I = 0xf4240

.field private static MIN_INT_AS_LONG:J = -0x7fffffffL

.field private static final NULL_CHAR:C = '\u0000'

.field private static final TEN_BILLION_L:J = 0x2540be400L

.field private static final THOUSAND_L:J = 0x3e8L


# direct methods
.method public static constructor <clinit>()V
    .locals 14

    const/16 v0, 0xfa0

    new-array v1, v0, [C

    sput-object v1, Lorg/codehaus/stax2/ri/typed/NumberUtil;->LEADING_TRIPLETS:[C

    new-array v0, v0, [C

    sput-object v0, Lorg/codehaus/stax2/ri/typed/NumberUtil;->FULL_TRIPLETS:[C

    const/4 v0, 0x0

    move v1, v0

    move v2, v1

    :goto_0
    const/16 v3, 0xa

    if-ge v1, v3, :cond_4

    add-int/lit8 v4, v1, 0x30

    int-to-char v4, v4

    if-nez v1, :cond_0

    move v5, v0

    goto :goto_1

    :cond_0
    move v5, v4

    :goto_1
    move v6, v0

    :goto_2
    if-ge v6, v3, :cond_3

    add-int/lit8 v7, v6, 0x30

    int-to-char v7, v7

    if-nez v1, :cond_1

    if-nez v6, :cond_1

    move v8, v0

    goto :goto_3

    :cond_1
    move v8, v7

    :goto_3
    move v9, v0

    :goto_4
    if-ge v9, v3, :cond_2

    add-int/lit8 v10, v9, 0x30

    int-to-char v10, v10

    sget-object v11, Lorg/codehaus/stax2/ri/typed/NumberUtil;->LEADING_TRIPLETS:[C

    aput-char v5, v11, v2

    add-int/lit8 v12, v2, 0x1

    aput-char v8, v11, v12

    add-int/lit8 v13, v2, 0x2

    aput-char v10, v11, v13

    sget-object v11, Lorg/codehaus/stax2/ri/typed/NumberUtil;->FULL_TRIPLETS:[C

    aput-char v4, v11, v2

    aput-char v7, v11, v12

    aput-char v10, v11, v13

    add-int/lit8 v2, v2, 0x4

    add-int/lit8 v9, v9, 0x1

    goto :goto_4

    :cond_2
    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_4
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static calcLongStrLength(J)I
    .locals 6

    const/16 v0, 0xa

    const-wide v1, 0x2540be400L

    :goto_0
    cmp-long v3, p0, v1

    if-ltz v3, :cond_1

    const/16 v3, 0x13

    if-ne v0, v3, :cond_0

    goto :goto_1

    :cond_0
    add-int/lit8 v0, v0, 0x1

    const/4 v3, 0x3

    shl-long v3, v1, v3

    const/4 v5, 0x1

    shl-long/2addr v1, v5

    add-long/2addr v1, v3

    goto :goto_0

    :cond_1
    :goto_1
    return v0
.end method

.method private static getAsciiBytes(Ljava/lang/String;[BI)I
    .locals 4

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    add-int/lit8 v2, p2, 0x1

    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v3

    int-to-byte v3, v3

    aput-byte v3, p1, p2

    add-int/lit8 v1, v1, 0x1

    move p2, v2

    goto :goto_0

    :cond_0
    return p2
.end method

.method private static getChars(Ljava/lang/String;[CI)I
    .locals 2

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x0

    invoke-virtual {p0, v1, v0, p1, p2}, Ljava/lang/String;->getChars(II[CI)V

    add-int/2addr p2, v0

    return p2
.end method

.method public static writeDouble(D[BI)I
    .locals 0

    .line 1
    invoke-static {p0, p1}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object p0

    invoke-static {p0, p2, p3}, Lorg/codehaus/stax2/ri/typed/NumberUtil;->getAsciiBytes(Ljava/lang/String;[BI)I

    move-result p0

    return p0
.end method

.method public static writeDouble(D[CI)I
    .locals 0

    .line 2
    invoke-static {p0, p1}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object p0

    invoke-static {p0, p2, p3}, Lorg/codehaus/stax2/ri/typed/NumberUtil;->getChars(Ljava/lang/String;[CI)I

    move-result p0

    return p0
.end method

.method public static writeFloat(F[BI)I
    .locals 0

    .line 1
    invoke-static {p0}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object p0

    invoke-static {p0, p1, p2}, Lorg/codehaus/stax2/ri/typed/NumberUtil;->getAsciiBytes(Ljava/lang/String;[BI)I

    move-result p0

    return p0
.end method

.method public static writeFloat(F[CI)I
    .locals 0

    .line 2
    invoke-static {p0}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object p0

    invoke-static {p0, p1, p2}, Lorg/codehaus/stax2/ri/typed/NumberUtil;->getChars(Ljava/lang/String;[CI)I

    move-result p0

    return p0
.end method

.method private static writeFullTriplet(I[BI)I
    .locals 3

    .line 1
    shl-int/lit8 p0, p0, 0x2

    add-int/lit8 v0, p2, 0x1

    sget-object v1, Lorg/codehaus/stax2/ri/typed/NumberUtil;->FULL_TRIPLETS:[C

    add-int/lit8 v2, p0, 0x1

    aget-char p0, v1, p0

    int-to-byte p0, p0

    aput-byte p0, p1, p2

    add-int/lit8 p0, v0, 0x1

    add-int/lit8 p2, v2, 0x1

    aget-char v2, v1, v2

    int-to-byte v2, v2

    aput-byte v2, p1, v0

    add-int/lit8 v0, p0, 0x1

    aget-char p2, v1, p2

    int-to-byte p2, p2

    aput-byte p2, p1, p0

    return v0
.end method

.method private static writeFullTriplet(I[CI)I
    .locals 3

    .line 2
    shl-int/lit8 p0, p0, 0x2

    add-int/lit8 v0, p2, 0x1

    sget-object v1, Lorg/codehaus/stax2/ri/typed/NumberUtil;->FULL_TRIPLETS:[C

    add-int/lit8 v2, p0, 0x1

    aget-char p0, v1, p0

    aput-char p0, p1, p2

    add-int/lit8 p0, v0, 0x1

    add-int/lit8 p2, v2, 0x1

    aget-char v2, v1, v2

    aput-char v2, p1, v0

    add-int/lit8 v0, p0, 0x1

    aget-char p2, v1, p2

    aput-char p2, p1, p0

    return v0
.end method

.method public static writeInt(I[BI)I
    .locals 4

    .line 1
    if-gez p0, :cond_1

    const/high16 v0, -0x80000000

    if-ne p0, v0, :cond_0

    int-to-long v0, p0

    invoke-static {v0, v1, p1, p2}, Lorg/codehaus/stax2/ri/typed/NumberUtil;->writeLong(J[BI)I

    move-result p0

    return p0

    :cond_0
    add-int/lit8 v0, p2, 0x1

    const/16 v1, 0x2d

    aput-byte v1, p1, p2

    neg-int p0, p0

    move p2, v0

    :cond_1
    const v0, 0xf4240

    if-ge p0, v0, :cond_4

    const/16 v0, 0x3e8

    if-ge p0, v0, :cond_3

    const/16 v0, 0xa

    if-ge p0, v0, :cond_2

    add-int/lit8 v0, p2, 0x1

    add-int/lit8 p0, p0, 0x30

    int-to-byte p0, p0

    aput-byte p0, p1, p2

    goto :goto_0

    :cond_2
    invoke-static {p0, p1, p2}, Lorg/codehaus/stax2/ri/typed/NumberUtil;->writeLeadingTriplet(I[BI)I

    move-result v0

    goto :goto_0

    :cond_3
    div-int/lit16 v0, p0, 0x3e8

    mul-int/lit16 v1, v0, 0x3e8

    sub-int/2addr p0, v1

    invoke-static {v0, p1, p2}, Lorg/codehaus/stax2/ri/typed/NumberUtil;->writeLeadingTriplet(I[BI)I

    move-result p2

    invoke-static {p0, p1, p2}, Lorg/codehaus/stax2/ri/typed/NumberUtil;->writeFullTriplet(I[BI)I

    move-result v0

    :goto_0
    return v0

    :cond_4
    const v0, 0x3b9aca00

    if-lt p0, v0, :cond_5

    const/4 v1, 0x1

    goto :goto_1

    :cond_5
    const/4 v1, 0x0

    :goto_1
    if-eqz v1, :cond_7

    sub-int/2addr p0, v0

    if-lt p0, v0, :cond_6

    sub-int/2addr p0, v0

    add-int/lit8 v0, p2, 0x1

    const/16 v2, 0x32

    aput-byte v2, p1, p2

    goto :goto_2

    :cond_6
    add-int/lit8 v0, p2, 0x1

    const/16 v2, 0x31

    aput-byte v2, p1, p2

    :goto_2
    move p2, v0

    :cond_7
    div-int/lit16 v0, p0, 0x3e8

    mul-int/lit16 v2, v0, 0x3e8

    sub-int/2addr p0, v2

    div-int/lit16 v2, v0, 0x3e8

    mul-int/lit16 v3, v2, 0x3e8

    sub-int/2addr v0, v3

    if-eqz v1, :cond_8

    invoke-static {v2, p1, p2}, Lorg/codehaus/stax2/ri/typed/NumberUtil;->writeFullTriplet(I[BI)I

    move-result p2

    goto :goto_3

    :cond_8
    invoke-static {v2, p1, p2}, Lorg/codehaus/stax2/ri/typed/NumberUtil;->writeLeadingTriplet(I[BI)I

    move-result p2

    :goto_3
    invoke-static {v0, p1, p2}, Lorg/codehaus/stax2/ri/typed/NumberUtil;->writeFullTriplet(I[BI)I

    move-result p2

    invoke-static {p0, p1, p2}, Lorg/codehaus/stax2/ri/typed/NumberUtil;->writeFullTriplet(I[BI)I

    move-result p0

    return p0
.end method

.method public static writeInt(I[CI)I
    .locals 4

    .line 2
    if-gez p0, :cond_1

    const/high16 v0, -0x80000000

    if-ne p0, v0, :cond_0

    int-to-long v0, p0

    invoke-static {v0, v1, p1, p2}, Lorg/codehaus/stax2/ri/typed/NumberUtil;->writeLong(J[CI)I

    move-result p0

    return p0

    :cond_0
    add-int/lit8 v0, p2, 0x1

    const/16 v1, 0x2d

    aput-char v1, p1, p2

    neg-int p0, p0

    move p2, v0

    :cond_1
    const v0, 0xf4240

    if-ge p0, v0, :cond_4

    const/16 v0, 0x3e8

    if-ge p0, v0, :cond_3

    const/16 v0, 0xa

    if-ge p0, v0, :cond_2

    add-int/lit8 v0, p2, 0x1

    add-int/lit8 p0, p0, 0x30

    int-to-char p0, p0

    aput-char p0, p1, p2

    goto :goto_0

    :cond_2
    invoke-static {p0, p1, p2}, Lorg/codehaus/stax2/ri/typed/NumberUtil;->writeLeadingTriplet(I[CI)I

    move-result v0

    goto :goto_0

    :cond_3
    div-int/lit16 v0, p0, 0x3e8

    mul-int/lit16 v1, v0, 0x3e8

    sub-int/2addr p0, v1

    invoke-static {v0, p1, p2}, Lorg/codehaus/stax2/ri/typed/NumberUtil;->writeLeadingTriplet(I[CI)I

    move-result p2

    invoke-static {p0, p1, p2}, Lorg/codehaus/stax2/ri/typed/NumberUtil;->writeFullTriplet(I[CI)I

    move-result v0

    :goto_0
    return v0

    :cond_4
    const v0, 0x3b9aca00

    if-lt p0, v0, :cond_5

    const/4 v1, 0x1

    goto :goto_1

    :cond_5
    const/4 v1, 0x0

    :goto_1
    if-eqz v1, :cond_7

    sub-int/2addr p0, v0

    if-lt p0, v0, :cond_6

    sub-int/2addr p0, v0

    add-int/lit8 v0, p2, 0x1

    const/16 v2, 0x32

    aput-char v2, p1, p2

    goto :goto_2

    :cond_6
    add-int/lit8 v0, p2, 0x1

    const/16 v2, 0x31

    aput-char v2, p1, p2

    :goto_2
    move p2, v0

    :cond_7
    div-int/lit16 v0, p0, 0x3e8

    mul-int/lit16 v2, v0, 0x3e8

    sub-int/2addr p0, v2

    div-int/lit16 v2, v0, 0x3e8

    mul-int/lit16 v3, v2, 0x3e8

    sub-int/2addr v0, v3

    if-eqz v1, :cond_8

    invoke-static {v2, p1, p2}, Lorg/codehaus/stax2/ri/typed/NumberUtil;->writeFullTriplet(I[CI)I

    move-result p2

    goto :goto_3

    :cond_8
    invoke-static {v2, p1, p2}, Lorg/codehaus/stax2/ri/typed/NumberUtil;->writeLeadingTriplet(I[CI)I

    move-result p2

    :goto_3
    invoke-static {v0, p1, p2}, Lorg/codehaus/stax2/ri/typed/NumberUtil;->writeFullTriplet(I[CI)I

    move-result p2

    invoke-static {p0, p1, p2}, Lorg/codehaus/stax2/ri/typed/NumberUtil;->writeFullTriplet(I[CI)I

    move-result p0

    return p0
.end method

.method private static writeLeadingTriplet(I[BI)I
    .locals 3

    .line 1
    shl-int/lit8 p0, p0, 0x2

    sget-object v0, Lorg/codehaus/stax2/ri/typed/NumberUtil;->LEADING_TRIPLETS:[C

    add-int/lit8 v1, p0, 0x1

    aget-char p0, v0, p0

    if-eqz p0, :cond_0

    add-int/lit8 v2, p2, 0x1

    int-to-byte p0, p0

    aput-byte p0, p1, p2

    move p2, v2

    :cond_0
    add-int/lit8 p0, v1, 0x1

    aget-char v1, v0, v1

    if-eqz v1, :cond_1

    add-int/lit8 v2, p2, 0x1

    int-to-byte v1, v1

    aput-byte v1, p1, p2

    move p2, v2

    :cond_1
    add-int/lit8 v1, p2, 0x1

    aget-char p0, v0, p0

    int-to-byte p0, p0

    aput-byte p0, p1, p2

    return v1
.end method

.method private static writeLeadingTriplet(I[CI)I
    .locals 3

    .line 2
    shl-int/lit8 p0, p0, 0x2

    sget-object v0, Lorg/codehaus/stax2/ri/typed/NumberUtil;->LEADING_TRIPLETS:[C

    add-int/lit8 v1, p0, 0x1

    aget-char p0, v0, p0

    if-eqz p0, :cond_0

    add-int/lit8 v2, p2, 0x1

    aput-char p0, p1, p2

    move p2, v2

    :cond_0
    add-int/lit8 p0, v1, 0x1

    aget-char v1, v0, v1

    if-eqz v1, :cond_1

    add-int/lit8 v2, p2, 0x1

    aput-char v1, p1, p2

    move p2, v2

    :cond_1
    add-int/lit8 v1, p2, 0x1

    aget-char p0, v0, p0

    aput-char p0, p1, p2

    return v1
.end method

.method public static writeLong(J[BI)I
    .locals 6

    .line 1
    const-wide/16 v0, 0x0

    cmp-long v0, p0, v0

    if-gez v0, :cond_2

    sget-wide v0, Lorg/codehaus/stax2/ri/typed/NumberUtil;->MIN_INT_AS_LONG:J

    cmp-long v0, p0, v0

    if-ltz v0, :cond_0

    long-to-int p0, p0

    invoke-static {p0, p2, p3}, Lorg/codehaus/stax2/ri/typed/NumberUtil;->writeInt(I[BI)I

    move-result p0

    return p0

    :cond_0
    const-wide/high16 v0, -0x8000000000000000L

    cmp-long v0, p0, v0

    if-nez v0, :cond_1

    invoke-static {p0, p1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object p0

    invoke-static {p0, p2, p3}, Lorg/codehaus/stax2/ri/typed/NumberUtil;->getAsciiBytes(Ljava/lang/String;[BI)I

    move-result p0

    return p0

    :cond_1
    add-int/lit8 v0, p3, 0x1

    const/16 v1, 0x2d

    aput-byte v1, p2, p3

    neg-long p0, p0

    move p3, v0

    goto :goto_0

    :cond_2
    sget-wide v0, Lorg/codehaus/stax2/ri/typed/NumberUtil;->MAX_INT_AS_LONG:J

    cmp-long v0, p0, v0

    if-gtz v0, :cond_3

    long-to-int p0, p0

    invoke-static {p0, p2, p3}, Lorg/codehaus/stax2/ri/typed/NumberUtil;->writeInt(I[BI)I

    move-result p0

    return p0

    :cond_3
    :goto_0
    invoke-static {p0, p1}, Lorg/codehaus/stax2/ri/typed/NumberUtil;->calcLongStrLength(J)I

    move-result v0

    add-int/2addr v0, p3

    move v1, v0

    :goto_1
    sget-wide v2, Lorg/codehaus/stax2/ri/typed/NumberUtil;->MAX_INT_AS_LONG:J

    cmp-long v2, p0, v2

    if-lez v2, :cond_4

    add-int/lit8 v1, v1, -0x3

    const-wide/16 v2, 0x3e8

    div-long v4, p0, v2

    mul-long/2addr v2, v4

    sub-long/2addr p0, v2

    long-to-int p0, p0

    invoke-static {p0, p2, v1}, Lorg/codehaus/stax2/ri/typed/NumberUtil;->writeFullTriplet(I[BI)I

    move-wide p0, v4

    goto :goto_1

    :cond_4
    long-to-int p0, p0

    :goto_2
    const/16 p1, 0x3e8

    if-lt p0, p1, :cond_5

    add-int/lit8 v1, v1, -0x3

    div-int/lit16 p1, p0, 0x3e8

    mul-int/lit16 v2, p1, 0x3e8

    sub-int/2addr p0, v2

    invoke-static {p0, p2, v1}, Lorg/codehaus/stax2/ri/typed/NumberUtil;->writeFullTriplet(I[BI)I

    move p0, p1

    goto :goto_2

    :cond_5
    invoke-static {p0, p2, p3}, Lorg/codehaus/stax2/ri/typed/NumberUtil;->writeLeadingTriplet(I[BI)I

    return v0
.end method

.method public static writeLong(J[CI)I
    .locals 6

    .line 2
    const-wide/16 v0, 0x0

    cmp-long v0, p0, v0

    if-gez v0, :cond_2

    sget-wide v0, Lorg/codehaus/stax2/ri/typed/NumberUtil;->MIN_INT_AS_LONG:J

    cmp-long v0, p0, v0

    if-ltz v0, :cond_0

    long-to-int p0, p0

    invoke-static {p0, p2, p3}, Lorg/codehaus/stax2/ri/typed/NumberUtil;->writeInt(I[CI)I

    move-result p0

    return p0

    :cond_0
    const-wide/high16 v0, -0x8000000000000000L

    cmp-long v0, p0, v0

    if-nez v0, :cond_1

    invoke-static {p0, p1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object p0

    invoke-static {p0, p2, p3}, Lorg/codehaus/stax2/ri/typed/NumberUtil;->getChars(Ljava/lang/String;[CI)I

    move-result p0

    return p0

    :cond_1
    add-int/lit8 v0, p3, 0x1

    const/16 v1, 0x2d

    aput-char v1, p2, p3

    neg-long p0, p0

    move p3, v0

    goto :goto_0

    :cond_2
    sget-wide v0, Lorg/codehaus/stax2/ri/typed/NumberUtil;->MAX_INT_AS_LONG:J

    cmp-long v0, p0, v0

    if-gtz v0, :cond_3

    long-to-int p0, p0

    invoke-static {p0, p2, p3}, Lorg/codehaus/stax2/ri/typed/NumberUtil;->writeInt(I[CI)I

    move-result p0

    return p0

    :cond_3
    :goto_0
    invoke-static {p0, p1}, Lorg/codehaus/stax2/ri/typed/NumberUtil;->calcLongStrLength(J)I

    move-result v0

    add-int/2addr v0, p3

    move v1, v0

    :goto_1
    sget-wide v2, Lorg/codehaus/stax2/ri/typed/NumberUtil;->MAX_INT_AS_LONG:J

    cmp-long v2, p0, v2

    if-lez v2, :cond_4

    add-int/lit8 v1, v1, -0x3

    const-wide/16 v2, 0x3e8

    div-long v4, p0, v2

    mul-long/2addr v2, v4

    sub-long/2addr p0, v2

    long-to-int p0, p0

    invoke-static {p0, p2, v1}, Lorg/codehaus/stax2/ri/typed/NumberUtil;->writeFullTriplet(I[CI)I

    move-wide p0, v4

    goto :goto_1

    :cond_4
    long-to-int p0, p0

    :goto_2
    const/16 p1, 0x3e8

    if-lt p0, p1, :cond_5

    add-int/lit8 v1, v1, -0x3

    div-int/lit16 p1, p0, 0x3e8

    mul-int/lit16 v2, p1, 0x3e8

    sub-int/2addr p0, v2

    invoke-static {p0, p2, v1}, Lorg/codehaus/stax2/ri/typed/NumberUtil;->writeFullTriplet(I[CI)I

    move p0, p1

    goto :goto_2

    :cond_5
    invoke-static {p0, p2, p3}, Lorg/codehaus/stax2/ri/typed/NumberUtil;->writeLeadingTriplet(I[CI)I

    return v0
.end method

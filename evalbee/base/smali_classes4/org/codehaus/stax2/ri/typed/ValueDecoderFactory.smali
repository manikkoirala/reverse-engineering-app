.class public final Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$DoubleArrayDecoder;,
        Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$FloatArrayDecoder;,
        Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$LongArrayDecoder;,
        Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$IntArrayDecoder;,
        Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$BaseArrayDecoder;,
        Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$QNameDecoder;,
        Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$DecimalDecoder;,
        Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$IntegerDecoder;,
        Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$DoubleDecoder;,
        Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$FloatDecoder;,
        Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$LongDecoder;,
        Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$IntDecoder;,
        Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$BooleanDecoder;,
        Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$DecoderBase;
    }
.end annotation


# instance fields
.field protected mBooleanDecoder:Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$BooleanDecoder;

.field protected mDoubleDecoder:Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$DoubleDecoder;

.field protected mFloatDecoder:Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$FloatDecoder;

.field protected mIntDecoder:Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$IntDecoder;

.field protected mLongDecoder:Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$LongDecoder;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;->mBooleanDecoder:Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$BooleanDecoder;

    iput-object v0, p0, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;->mIntDecoder:Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$IntDecoder;

    iput-object v0, p0, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;->mLongDecoder:Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$LongDecoder;

    iput-object v0, p0, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;->mFloatDecoder:Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$FloatDecoder;

    iput-object v0, p0, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;->mDoubleDecoder:Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$DoubleDecoder;

    return-void
.end method


# virtual methods
.method public getBooleanDecoder()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$BooleanDecoder;
    .locals 1

    iget-object v0, p0, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;->mBooleanDecoder:Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$BooleanDecoder;

    if-nez v0, :cond_0

    new-instance v0, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$BooleanDecoder;

    invoke-direct {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$BooleanDecoder;-><init>()V

    iput-object v0, p0, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;->mBooleanDecoder:Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$BooleanDecoder;

    :cond_0
    iget-object v0, p0, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;->mBooleanDecoder:Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$BooleanDecoder;

    return-object v0
.end method

.method public getDecimalDecoder()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$DecimalDecoder;
    .locals 1

    new-instance v0, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$DecimalDecoder;

    invoke-direct {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$DecimalDecoder;-><init>()V

    return-object v0
.end method

.method public getDoubleArrayDecoder()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$DoubleArrayDecoder;
    .locals 2

    .line 1
    new-instance v0, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$DoubleArrayDecoder;

    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;->getDoubleDecoder()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$DoubleDecoder;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$DoubleArrayDecoder;-><init>(Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$DoubleDecoder;)V

    return-object v0
.end method

.method public getDoubleArrayDecoder([DII)Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$DoubleArrayDecoder;
    .locals 2

    .line 2
    new-instance v0, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$DoubleArrayDecoder;

    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;->getDoubleDecoder()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$DoubleDecoder;

    move-result-object v1

    invoke-direct {v0, p1, p2, p3, v1}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$DoubleArrayDecoder;-><init>([DIILorg/codehaus/stax2/ri/typed/ValueDecoderFactory$DoubleDecoder;)V

    return-object v0
.end method

.method public getDoubleDecoder()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$DoubleDecoder;
    .locals 1

    iget-object v0, p0, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;->mDoubleDecoder:Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$DoubleDecoder;

    if-nez v0, :cond_0

    new-instance v0, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$DoubleDecoder;

    invoke-direct {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$DoubleDecoder;-><init>()V

    iput-object v0, p0, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;->mDoubleDecoder:Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$DoubleDecoder;

    :cond_0
    iget-object v0, p0, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;->mDoubleDecoder:Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$DoubleDecoder;

    return-object v0
.end method

.method public getFloatArrayDecoder()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$FloatArrayDecoder;
    .locals 2

    .line 1
    new-instance v0, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$FloatArrayDecoder;

    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;->getFloatDecoder()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$FloatDecoder;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$FloatArrayDecoder;-><init>(Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$FloatDecoder;)V

    return-object v0
.end method

.method public getFloatArrayDecoder([FII)Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$FloatArrayDecoder;
    .locals 2

    .line 2
    new-instance v0, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$FloatArrayDecoder;

    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;->getFloatDecoder()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$FloatDecoder;

    move-result-object v1

    invoke-direct {v0, p1, p2, p3, v1}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$FloatArrayDecoder;-><init>([FIILorg/codehaus/stax2/ri/typed/ValueDecoderFactory$FloatDecoder;)V

    return-object v0
.end method

.method public getFloatDecoder()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$FloatDecoder;
    .locals 1

    iget-object v0, p0, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;->mFloatDecoder:Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$FloatDecoder;

    if-nez v0, :cond_0

    new-instance v0, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$FloatDecoder;

    invoke-direct {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$FloatDecoder;-><init>()V

    iput-object v0, p0, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;->mFloatDecoder:Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$FloatDecoder;

    :cond_0
    iget-object v0, p0, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;->mFloatDecoder:Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$FloatDecoder;

    return-object v0
.end method

.method public getIntArrayDecoder()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$IntArrayDecoder;
    .locals 2

    .line 1
    new-instance v0, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$IntArrayDecoder;

    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;->getIntDecoder()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$IntDecoder;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$IntArrayDecoder;-><init>(Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$IntDecoder;)V

    return-object v0
.end method

.method public getIntArrayDecoder([III)Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$IntArrayDecoder;
    .locals 2

    .line 2
    new-instance v0, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$IntArrayDecoder;

    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;->getIntDecoder()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$IntDecoder;

    move-result-object v1

    invoke-direct {v0, p1, p2, p3, v1}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$IntArrayDecoder;-><init>([IIILorg/codehaus/stax2/ri/typed/ValueDecoderFactory$IntDecoder;)V

    return-object v0
.end method

.method public getIntDecoder()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$IntDecoder;
    .locals 1

    iget-object v0, p0, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;->mIntDecoder:Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$IntDecoder;

    if-nez v0, :cond_0

    new-instance v0, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$IntDecoder;

    invoke-direct {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$IntDecoder;-><init>()V

    iput-object v0, p0, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;->mIntDecoder:Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$IntDecoder;

    :cond_0
    iget-object v0, p0, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;->mIntDecoder:Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$IntDecoder;

    return-object v0
.end method

.method public getIntegerDecoder()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$IntegerDecoder;
    .locals 1

    new-instance v0, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$IntegerDecoder;

    invoke-direct {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$IntegerDecoder;-><init>()V

    return-object v0
.end method

.method public getLongArrayDecoder()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$LongArrayDecoder;
    .locals 2

    .line 1
    new-instance v0, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$LongArrayDecoder;

    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;->getLongDecoder()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$LongDecoder;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$LongArrayDecoder;-><init>(Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$LongDecoder;)V

    return-object v0
.end method

.method public getLongArrayDecoder([JII)Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$LongArrayDecoder;
    .locals 2

    .line 2
    new-instance v0, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$LongArrayDecoder;

    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;->getLongDecoder()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$LongDecoder;

    move-result-object v1

    invoke-direct {v0, p1, p2, p3, v1}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$LongArrayDecoder;-><init>([JIILorg/codehaus/stax2/ri/typed/ValueDecoderFactory$LongDecoder;)V

    return-object v0
.end method

.method public getLongDecoder()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$LongDecoder;
    .locals 1

    iget-object v0, p0, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;->mLongDecoder:Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$LongDecoder;

    if-nez v0, :cond_0

    new-instance v0, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$LongDecoder;

    invoke-direct {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$LongDecoder;-><init>()V

    iput-object v0, p0, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;->mLongDecoder:Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$LongDecoder;

    :cond_0
    iget-object v0, p0, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;->mLongDecoder:Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$LongDecoder;

    return-object v0
.end method

.method public getQNameDecoder(Ljavax/xml/namespace/NamespaceContext;)Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$QNameDecoder;
    .locals 1

    new-instance v0, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$QNameDecoder;

    invoke-direct {v0, p1}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$QNameDecoder;-><init>(Ljavax/xml/namespace/NamespaceContext;)V

    return-object v0
.end method

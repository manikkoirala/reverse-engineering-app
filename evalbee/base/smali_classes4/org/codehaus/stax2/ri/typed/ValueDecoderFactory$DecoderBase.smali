.class public abstract Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$DecoderBase;
.super Lorg/codehaus/stax2/typed/TypedValueDecoder;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "DecoderBase"
.end annotation


# static fields
.field static final BD_MAX_LONG:Ljava/math/BigInteger;

.field static final BD_MIN_LONG:Ljava/math/BigInteger;

.field static final L_BILLION:J = 0x3b9aca00L

.field static final L_MAX_INT:J = 0x7fffffffL

.field static final L_MIN_INT:J = -0x80000000L


# instance fields
.field protected mNextPtr:I


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    const-wide/high16 v0, -0x8000000000000000L

    invoke-static {v0, v1}, Ljava/math/BigInteger;->valueOf(J)Ljava/math/BigInteger;

    move-result-object v0

    sput-object v0, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$DecoderBase;->BD_MIN_LONG:Ljava/math/BigInteger;

    const-wide v0, 0x7fffffffffffffffL

    invoke-static {v0, v1}, Ljava/math/BigInteger;->valueOf(J)Ljava/math/BigInteger;

    move-result-object v0

    sput-object v0, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$DecoderBase;->BD_MAX_LONG:Ljava/math/BigInteger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lorg/codehaus/stax2/typed/TypedValueDecoder;-><init>()V

    return-void
.end method

.method public static final parseInt(ILjava/lang/String;II)I
    .locals 1

    .line 1
    mul-int/lit8 p0, p0, 0xa

    invoke-virtual {p1, p2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    add-int/lit8 v0, v0, -0x30

    add-int/2addr p0, v0

    add-int/lit8 p2, p2, 0x1

    if-ge p2, p3, :cond_0

    mul-int/lit8 p0, p0, 0xa

    invoke-virtual {p1, p2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    add-int/lit8 v0, v0, -0x30

    add-int/2addr p0, v0

    add-int/lit8 p2, p2, 0x1

    if-ge p2, p3, :cond_0

    mul-int/lit8 p0, p0, 0xa

    invoke-virtual {p1, p2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    add-int/lit8 v0, v0, -0x30

    add-int/2addr p0, v0

    add-int/lit8 p2, p2, 0x1

    if-ge p2, p3, :cond_0

    mul-int/lit8 p0, p0, 0xa

    invoke-virtual {p1, p2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    add-int/lit8 v0, v0, -0x30

    add-int/2addr p0, v0

    add-int/lit8 p2, p2, 0x1

    if-ge p2, p3, :cond_0

    mul-int/lit8 p0, p0, 0xa

    invoke-virtual {p1, p2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    add-int/lit8 v0, v0, -0x30

    add-int/2addr p0, v0

    add-int/lit8 p2, p2, 0x1

    if-ge p2, p3, :cond_0

    mul-int/lit8 p0, p0, 0xa

    invoke-virtual {p1, p2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    add-int/lit8 v0, v0, -0x30

    add-int/2addr p0, v0

    add-int/lit8 p2, p2, 0x1

    if-ge p2, p3, :cond_0

    mul-int/lit8 p0, p0, 0xa

    invoke-virtual {p1, p2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    add-int/lit8 v0, v0, -0x30

    add-int/2addr p0, v0

    add-int/lit8 p2, p2, 0x1

    if-ge p2, p3, :cond_0

    mul-int/lit8 p0, p0, 0xa

    invoke-virtual {p1, p2}, Ljava/lang/String;->charAt(I)C

    move-result p1

    add-int/lit8 p1, p1, -0x30

    add-int/2addr p0, p1

    :cond_0
    return p0
.end method

.method public static final parseInt(I[CII)I
    .locals 1

    .line 2
    mul-int/lit8 p0, p0, 0xa

    aget-char v0, p1, p2

    add-int/lit8 v0, v0, -0x30

    add-int/2addr p0, v0

    add-int/lit8 p2, p2, 0x1

    if-ge p2, p3, :cond_0

    mul-int/lit8 p0, p0, 0xa

    aget-char v0, p1, p2

    add-int/lit8 v0, v0, -0x30

    add-int/2addr p0, v0

    add-int/lit8 p2, p2, 0x1

    if-ge p2, p3, :cond_0

    mul-int/lit8 p0, p0, 0xa

    aget-char v0, p1, p2

    add-int/lit8 v0, v0, -0x30

    add-int/2addr p0, v0

    add-int/lit8 p2, p2, 0x1

    if-ge p2, p3, :cond_0

    mul-int/lit8 p0, p0, 0xa

    aget-char v0, p1, p2

    add-int/lit8 v0, v0, -0x30

    add-int/2addr p0, v0

    add-int/lit8 p2, p2, 0x1

    if-ge p2, p3, :cond_0

    mul-int/lit8 p0, p0, 0xa

    aget-char v0, p1, p2

    add-int/lit8 v0, v0, -0x30

    add-int/2addr p0, v0

    add-int/lit8 p2, p2, 0x1

    if-ge p2, p3, :cond_0

    mul-int/lit8 p0, p0, 0xa

    aget-char v0, p1, p2

    add-int/lit8 v0, v0, -0x30

    add-int/2addr p0, v0

    add-int/lit8 p2, p2, 0x1

    if-ge p2, p3, :cond_0

    mul-int/lit8 p0, p0, 0xa

    aget-char v0, p1, p2

    add-int/lit8 v0, v0, -0x30

    add-int/2addr p0, v0

    add-int/lit8 p2, p2, 0x1

    if-ge p2, p3, :cond_0

    mul-int/lit8 p0, p0, 0xa

    aget-char p1, p1, p2

    add-int/lit8 p1, p1, -0x30

    add-int/2addr p0, p1

    :cond_0
    return p0
.end method

.method public static final parseInt(Ljava/lang/String;II)I
    .locals 2

    .line 3
    invoke-virtual {p0, p1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    add-int/lit8 v0, v0, -0x30

    add-int/lit8 p1, p1, 0x1

    if-ge p1, p2, :cond_0

    mul-int/lit8 v0, v0, 0xa

    invoke-virtual {p0, p1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    add-int/lit8 v1, v1, -0x30

    add-int/2addr v0, v1

    add-int/lit8 p1, p1, 0x1

    if-ge p1, p2, :cond_0

    mul-int/lit8 v0, v0, 0xa

    invoke-virtual {p0, p1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    add-int/lit8 v1, v1, -0x30

    add-int/2addr v0, v1

    add-int/lit8 p1, p1, 0x1

    if-ge p1, p2, :cond_0

    mul-int/lit8 v0, v0, 0xa

    invoke-virtual {p0, p1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    add-int/lit8 v1, v1, -0x30

    add-int/2addr v0, v1

    add-int/lit8 p1, p1, 0x1

    if-ge p1, p2, :cond_0

    mul-int/lit8 v0, v0, 0xa

    invoke-virtual {p0, p1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    add-int/lit8 v1, v1, -0x30

    add-int/2addr v0, v1

    add-int/lit8 p1, p1, 0x1

    if-ge p1, p2, :cond_0

    mul-int/lit8 v0, v0, 0xa

    invoke-virtual {p0, p1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    add-int/lit8 v1, v1, -0x30

    add-int/2addr v0, v1

    add-int/lit8 p1, p1, 0x1

    if-ge p1, p2, :cond_0

    mul-int/lit8 v0, v0, 0xa

    invoke-virtual {p0, p1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    add-int/lit8 v1, v1, -0x30

    add-int/2addr v0, v1

    add-int/lit8 p1, p1, 0x1

    if-ge p1, p2, :cond_0

    mul-int/lit8 v0, v0, 0xa

    invoke-virtual {p0, p1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    add-int/lit8 v1, v1, -0x30

    add-int/2addr v0, v1

    add-int/lit8 p1, p1, 0x1

    if-ge p1, p2, :cond_0

    mul-int/lit8 v0, v0, 0xa

    invoke-virtual {p0, p1}, Ljava/lang/String;->charAt(I)C

    move-result p0

    add-int/lit8 p0, p0, -0x30

    add-int/2addr v0, p0

    :cond_0
    return v0
.end method

.method public static final parseInt([CII)I
    .locals 2

    .line 4
    aget-char v0, p0, p1

    add-int/lit8 v0, v0, -0x30

    add-int/lit8 p1, p1, 0x1

    if-ge p1, p2, :cond_0

    mul-int/lit8 v0, v0, 0xa

    aget-char v1, p0, p1

    add-int/lit8 v1, v1, -0x30

    add-int/2addr v0, v1

    add-int/lit8 p1, p1, 0x1

    if-ge p1, p2, :cond_0

    mul-int/lit8 v0, v0, 0xa

    aget-char v1, p0, p1

    add-int/lit8 v1, v1, -0x30

    add-int/2addr v0, v1

    add-int/lit8 p1, p1, 0x1

    if-ge p1, p2, :cond_0

    mul-int/lit8 v0, v0, 0xa

    aget-char v1, p0, p1

    add-int/lit8 v1, v1, -0x30

    add-int/2addr v0, v1

    add-int/lit8 p1, p1, 0x1

    if-ge p1, p2, :cond_0

    mul-int/lit8 v0, v0, 0xa

    aget-char v1, p0, p1

    add-int/lit8 v1, v1, -0x30

    add-int/2addr v0, v1

    add-int/lit8 p1, p1, 0x1

    if-ge p1, p2, :cond_0

    mul-int/lit8 v0, v0, 0xa

    aget-char v1, p0, p1

    add-int/lit8 v1, v1, -0x30

    add-int/2addr v0, v1

    add-int/lit8 p1, p1, 0x1

    if-ge p1, p2, :cond_0

    mul-int/lit8 v0, v0, 0xa

    aget-char v1, p0, p1

    add-int/lit8 v1, v1, -0x30

    add-int/2addr v0, v1

    add-int/lit8 p1, p1, 0x1

    if-ge p1, p2, :cond_0

    mul-int/lit8 v0, v0, 0xa

    aget-char v1, p0, p1

    add-int/lit8 v1, v1, -0x30

    add-int/2addr v0, v1

    add-int/lit8 p1, p1, 0x1

    if-ge p1, p2, :cond_0

    mul-int/lit8 v0, v0, 0xa

    aget-char p0, p0, p1

    add-int/lit8 p0, p0, -0x30

    add-int/2addr v0, p0

    :cond_0
    return v0
.end method

.method public static final parseLong(Ljava/lang/String;II)J
    .locals 5

    .line 1
    add-int/lit8 v0, p2, -0x9

    invoke-static {p0, p1, v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$DecoderBase;->parseInt(Ljava/lang/String;II)I

    move-result p1

    int-to-long v1, p1

    const-wide/32 v3, 0x3b9aca00

    mul-long/2addr v1, v3

    invoke-static {p0, v0, p2}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$DecoderBase;->parseInt(Ljava/lang/String;II)I

    move-result p0

    int-to-long p0, p0

    add-long/2addr v1, p0

    return-wide v1
.end method

.method public static final parseLong([CII)J
    .locals 5

    .line 2
    add-int/lit8 v0, p2, -0x9

    invoke-static {p0, p1, v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$DecoderBase;->parseInt([CII)I

    move-result p1

    int-to-long v1, p1

    const-wide/32 v3, 0x3b9aca00

    mul-long/2addr v1, v3

    invoke-static {p0, v0, p2}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$DecoderBase;->parseInt([CII)I

    move-result p0

    int-to-long p0, p0

    add-long/2addr v1, p0

    return-wide v1
.end method


# virtual methods
.method public _clean(Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public constructInvalidValue(Ljava/lang/String;)Ljava/lang/IllegalArgumentException;
    .locals 3

    .line 1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Value \""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "\" not a valid lexical representation of "

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$DecoderBase;->getType()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public constructInvalidValue([CII)Ljava/lang/IllegalArgumentException;
    .locals 3

    .line 2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Value \""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0, p1, p2, p3}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$DecoderBase;->lexicalDesc([CII)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "\" not a valid lexical representation of "

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$DecoderBase;->getType()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public abstract getType()Ljava/lang/String;
.end method

.method public handleEmptyValue()V
    .locals 3

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Empty value (all white space) not a valid lexical representation of "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$DecoderBase;->getType()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public lexicalDesc(Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$DecoderBase;->_clean(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public lexicalDesc([CII)Ljava/lang/String;
    .locals 1

    .line 2
    new-instance v0, Ljava/lang/String;

    sub-int/2addr p3, p2

    invoke-direct {v0, p1, p2, p3}, Ljava/lang/String;-><init>([CII)V

    invoke-virtual {p0, v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$DecoderBase;->_clean(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public skipSignAndZeroes(Ljava/lang/String;CZI)I
    .locals 2

    .line 1
    const/4 v0, 0x1

    if-eqz p3, :cond_1

    if-ge v0, p4, :cond_0

    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result p2

    const/4 v0, 0x2

    goto :goto_0

    :cond_0
    invoke-virtual {p0, p1}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$DecoderBase;->constructInvalidValue(Ljava/lang/String;)Ljava/lang/IllegalArgumentException;

    move-result-object p1

    throw p1

    :cond_1
    :goto_0
    add-int/lit8 p2, p2, -0x30

    if-ltz p2, :cond_4

    const/16 p3, 0x9

    if-gt p2, p3, :cond_4

    :goto_1
    if-nez p2, :cond_3

    if-ge v0, p4, :cond_3

    invoke-virtual {p1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v1

    add-int/lit8 v1, v1, -0x30

    if-ltz v1, :cond_3

    if-le v1, p3, :cond_2

    goto :goto_2

    :cond_2
    add-int/lit8 v0, v0, 0x1

    move p2, v1

    goto :goto_1

    :cond_3
    :goto_2
    iput v0, p0, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$DecoderBase;->mNextPtr:I

    return p2

    :cond_4
    invoke-virtual {p0, p1}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$DecoderBase;->constructInvalidValue(Ljava/lang/String;)Ljava/lang/IllegalArgumentException;

    move-result-object p1

    throw p1
.end method

.method public skipSignAndZeroes([CCZII)I
    .locals 1

    .line 2
    add-int/lit8 v0, p4, 0x1

    if-eqz p3, :cond_1

    if-ge v0, p5, :cond_0

    add-int/lit8 p2, v0, 0x1

    aget-char p3, p1, v0

    move v0, p2

    move p2, p3

    goto :goto_0

    :cond_0
    invoke-virtual {p0, p1, p4, p5}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$DecoderBase;->constructInvalidValue([CII)Ljava/lang/IllegalArgumentException;

    move-result-object p1

    throw p1

    :cond_1
    :goto_0
    add-int/lit8 p2, p2, -0x30

    if-ltz p2, :cond_4

    const/16 p3, 0x9

    if-gt p2, p3, :cond_4

    :goto_1
    if-nez p2, :cond_3

    if-ge v0, p5, :cond_3

    aget-char p4, p1, v0

    add-int/lit8 p4, p4, -0x30

    if-ltz p4, :cond_3

    if-le p4, p3, :cond_2

    goto :goto_2

    :cond_2
    add-int/lit8 v0, v0, 0x1

    move p2, p4

    goto :goto_1

    :cond_3
    :goto_2
    iput v0, p0, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$DecoderBase;->mNextPtr:I

    return p2

    :cond_4
    invoke-virtual {p0, p1, p4, p5}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$DecoderBase;->constructInvalidValue([CII)Ljava/lang/IllegalArgumentException;

    move-result-object p1

    throw p1
.end method

.method public verifyDigits(Ljava/lang/String;II)V
    .locals 2

    .line 1
    :goto_0
    if-ge p2, p3, :cond_1

    invoke-virtual {p1, p2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v1, 0x39

    if-gt v0, v1, :cond_0

    const/16 v1, 0x30

    if-lt v0, v1, :cond_0

    add-int/lit8 p2, p2, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {p0, p1}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$DecoderBase;->constructInvalidValue(Ljava/lang/String;)Ljava/lang/IllegalArgumentException;

    move-result-object p1

    throw p1

    :cond_1
    return-void
.end method

.method public verifyDigits([CIII)V
    .locals 2

    .line 2
    :goto_0
    if-ge p4, p3, :cond_1

    aget-char v0, p1, p4

    const/16 v1, 0x39

    if-gt v0, v1, :cond_0

    const/16 v1, 0x30

    if-lt v0, v1, :cond_0

    add-int/lit8 p4, p4, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {p0, p1, p2, p3}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$DecoderBase;->constructInvalidValue([CII)Ljava/lang/IllegalArgumentException;

    move-result-object p1

    throw p1

    :cond_1
    return-void
.end method

.class final Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory$DoubleArrayEncoder;
.super Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory$ArrayEncoder;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DoubleArrayEncoder"
.end annotation


# instance fields
.field final _values:[D


# direct methods
.method public constructor <init>([DII)V
    .locals 0

    invoke-direct {p0, p2, p3}, Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory$ArrayEncoder;-><init>(II)V

    iput-object p1, p0, Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory$DoubleArrayEncoder;->_values:[D

    return-void
.end method


# virtual methods
.method public encodeMore([BII)I
    .locals 4

    .line 1
    add-int/lit8 p3, p3, -0x21

    :goto_0
    if-gt p2, p3, :cond_0

    iget v0, p0, Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory$ArrayEncoder;->_ptr:I

    iget v1, p0, Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory$ArrayEncoder;->_end:I

    if-ge v0, v1, :cond_0

    add-int/lit8 v1, p2, 0x1

    const/16 v2, 0x20

    aput-byte v2, p1, p2

    iget-object p2, p0, Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory$DoubleArrayEncoder;->_values:[D

    add-int/lit8 v2, v0, 0x1

    iput v2, p0, Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory$ArrayEncoder;->_ptr:I

    aget-wide v2, p2, v0

    invoke-static {v2, v3, p1, v1}, Lorg/codehaus/stax2/ri/typed/NumberUtil;->writeDouble(D[BI)I

    move-result p2

    goto :goto_0

    :cond_0
    return p2
.end method

.method public encodeMore([CII)I
    .locals 4

    .line 2
    add-int/lit8 p3, p3, -0x21

    :goto_0
    if-gt p2, p3, :cond_0

    iget v0, p0, Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory$ArrayEncoder;->_ptr:I

    iget v1, p0, Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory$ArrayEncoder;->_end:I

    if-ge v0, v1, :cond_0

    add-int/lit8 v1, p2, 0x1

    const/16 v2, 0x20

    aput-char v2, p1, p2

    iget-object p2, p0, Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory$DoubleArrayEncoder;->_values:[D

    add-int/lit8 v2, v0, 0x1

    iput v2, p0, Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory$ArrayEncoder;->_ptr:I

    aget-wide v2, p2, v0

    invoke-static {v2, v3, p1, v1}, Lorg/codehaus/stax2/ri/typed/NumberUtil;->writeDouble(D[CI)I

    move-result p2

    goto :goto_0

    :cond_0
    return p2
.end method

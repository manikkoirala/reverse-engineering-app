.class final Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory$Base64Encoder;
.super Lorg/codehaus/stax2/ri/typed/AsciiValueEncoder;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Base64Encoder"
.end annotation


# static fields
.field static final LF_BYTE:B = 0xat

.field static final LF_CHAR:B = 0xat

.field static final PAD_BYTE:B = 0x3dt

.field static final PAD_CHAR:C = '='


# instance fields
.field _chunksBeforeLf:I

.field final _input:[B

.field final _inputEnd:I

.field _inputPtr:I

.field final _variant:Lorg/codehaus/stax2/typed/Base64Variant;


# direct methods
.method public constructor <init>(Lorg/codehaus/stax2/typed/Base64Variant;[BII)V
    .locals 0

    invoke-direct {p0}, Lorg/codehaus/stax2/ri/typed/AsciiValueEncoder;-><init>()V

    iput-object p1, p0, Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory$Base64Encoder;->_variant:Lorg/codehaus/stax2/typed/Base64Variant;

    iput-object p2, p0, Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory$Base64Encoder;->_input:[B

    iput p3, p0, Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory$Base64Encoder;->_inputPtr:I

    iput p4, p0, Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory$Base64Encoder;->_inputEnd:I

    invoke-virtual {p1}, Lorg/codehaus/stax2/typed/Base64Variant;->getMaxLineLength()I

    move-result p1

    shr-int/lit8 p1, p1, 0x2

    iput p1, p0, Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory$Base64Encoder;->_chunksBeforeLf:I

    return-void
.end method


# virtual methods
.method public encodeMore([BII)I
    .locals 6

    .line 1
    iget v0, p0, Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory$Base64Encoder;->_inputEnd:I

    add-int/lit8 v0, v0, -0x3

    add-int/lit8 p3, p3, -0x5

    :cond_0
    :goto_0
    iget v1, p0, Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory$Base64Encoder;->_inputPtr:I

    const/4 v2, 0x2

    if-gt v1, v0, :cond_2

    if-le p2, p3, :cond_1

    return p2

    :cond_1
    iget-object v3, p0, Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory$Base64Encoder;->_input:[B

    add-int/lit8 v4, v1, 0x1

    aget-byte v1, v3, v1

    shl-int/lit8 v1, v1, 0x8

    add-int/lit8 v5, v4, 0x1

    aget-byte v4, v3, v4

    and-int/lit16 v4, v4, 0xff

    or-int/2addr v1, v4

    shl-int/lit8 v1, v1, 0x8

    add-int/lit8 v4, v5, 0x1

    iput v4, p0, Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory$Base64Encoder;->_inputPtr:I

    aget-byte v3, v3, v5

    and-int/lit16 v3, v3, 0xff

    or-int/2addr v1, v3

    iget-object v3, p0, Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory$Base64Encoder;->_variant:Lorg/codehaus/stax2/typed/Base64Variant;

    invoke-virtual {v3, v1, p1, p2}, Lorg/codehaus/stax2/typed/Base64Variant;->encodeBase64Chunk(I[BI)I

    move-result p2

    iget v1, p0, Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory$Base64Encoder;->_chunksBeforeLf:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory$Base64Encoder;->_chunksBeforeLf:I

    if-gtz v1, :cond_0

    add-int/lit8 v1, p2, 0x1

    const/16 v3, 0xa

    aput-byte v3, p1, p2

    iget-object p2, p0, Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory$Base64Encoder;->_variant:Lorg/codehaus/stax2/typed/Base64Variant;

    invoke-virtual {p2}, Lorg/codehaus/stax2/typed/Base64Variant;->getMaxLineLength()I

    move-result p2

    shr-int/2addr p2, v2

    iput p2, p0, Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory$Base64Encoder;->_chunksBeforeLf:I

    move p2, v1

    goto :goto_0

    :cond_2
    iget v0, p0, Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory$Base64Encoder;->_inputEnd:I

    sub-int/2addr v0, v1

    if-lez v0, :cond_4

    if-gt p2, p3, :cond_4

    iget-object p3, p0, Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory$Base64Encoder;->_input:[B

    add-int/lit8 v3, v1, 0x1

    iput v3, p0, Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory$Base64Encoder;->_inputPtr:I

    aget-byte v1, p3, v1

    shl-int/lit8 v1, v1, 0x10

    if-ne v0, v2, :cond_3

    add-int/lit8 v2, v3, 0x1

    iput v2, p0, Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory$Base64Encoder;->_inputPtr:I

    aget-byte p3, p3, v3

    and-int/lit16 p3, p3, 0xff

    shl-int/lit8 p3, p3, 0x8

    or-int/2addr v1, p3

    :cond_3
    iget-object p3, p0, Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory$Base64Encoder;->_variant:Lorg/codehaus/stax2/typed/Base64Variant;

    invoke-virtual {p3, v1, v0, p1, p2}, Lorg/codehaus/stax2/typed/Base64Variant;->encodeBase64Partial(II[BI)I

    move-result p2

    :cond_4
    return p2
.end method

.method public encodeMore([CII)I
    .locals 6

    .line 2
    iget v0, p0, Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory$Base64Encoder;->_inputEnd:I

    add-int/lit8 v0, v0, -0x3

    add-int/lit8 p3, p3, -0x5

    :cond_0
    :goto_0
    iget v1, p0, Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory$Base64Encoder;->_inputPtr:I

    const/4 v2, 0x2

    if-gt v1, v0, :cond_2

    if-le p2, p3, :cond_1

    return p2

    :cond_1
    iget-object v3, p0, Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory$Base64Encoder;->_input:[B

    add-int/lit8 v4, v1, 0x1

    aget-byte v1, v3, v1

    shl-int/lit8 v1, v1, 0x8

    add-int/lit8 v5, v4, 0x1

    aget-byte v4, v3, v4

    and-int/lit16 v4, v4, 0xff

    or-int/2addr v1, v4

    shl-int/lit8 v1, v1, 0x8

    add-int/lit8 v4, v5, 0x1

    iput v4, p0, Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory$Base64Encoder;->_inputPtr:I

    aget-byte v3, v3, v5

    and-int/lit16 v3, v3, 0xff

    or-int/2addr v1, v3

    iget-object v3, p0, Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory$Base64Encoder;->_variant:Lorg/codehaus/stax2/typed/Base64Variant;

    invoke-virtual {v3, v1, p1, p2}, Lorg/codehaus/stax2/typed/Base64Variant;->encodeBase64Chunk(I[CI)I

    move-result p2

    iget v1, p0, Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory$Base64Encoder;->_chunksBeforeLf:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory$Base64Encoder;->_chunksBeforeLf:I

    if-gtz v1, :cond_0

    add-int/lit8 v1, p2, 0x1

    const/16 v3, 0xa

    aput-char v3, p1, p2

    iget-object p2, p0, Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory$Base64Encoder;->_variant:Lorg/codehaus/stax2/typed/Base64Variant;

    invoke-virtual {p2}, Lorg/codehaus/stax2/typed/Base64Variant;->getMaxLineLength()I

    move-result p2

    shr-int/2addr p2, v2

    iput p2, p0, Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory$Base64Encoder;->_chunksBeforeLf:I

    move p2, v1

    goto :goto_0

    :cond_2
    iget v0, p0, Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory$Base64Encoder;->_inputEnd:I

    sub-int/2addr v0, v1

    if-lez v0, :cond_4

    if-gt p2, p3, :cond_4

    iget-object p3, p0, Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory$Base64Encoder;->_input:[B

    add-int/lit8 v3, v1, 0x1

    iput v3, p0, Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory$Base64Encoder;->_inputPtr:I

    aget-byte v1, p3, v1

    shl-int/lit8 v1, v1, 0x10

    if-ne v0, v2, :cond_3

    add-int/lit8 v2, v3, 0x1

    iput v2, p0, Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory$Base64Encoder;->_inputPtr:I

    aget-byte p3, p3, v3

    and-int/lit16 p3, p3, 0xff

    shl-int/lit8 p3, p3, 0x8

    or-int/2addr v1, p3

    :cond_3
    iget-object p3, p0, Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory$Base64Encoder;->_variant:Lorg/codehaus/stax2/typed/Base64Variant;

    invoke-virtual {p3, v1, v0, p1, p2}, Lorg/codehaus/stax2/typed/Base64Variant;->encodeBase64Partial(II[CI)I

    move-result p2

    :cond_4
    return p2
.end method

.method public isCompleted()Z
    .locals 2

    iget v0, p0, Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory$Base64Encoder;->_inputPtr:I

    iget v1, p0, Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory$Base64Encoder;->_inputEnd:I

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

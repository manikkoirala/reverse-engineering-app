.class public final Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$LongArrayDecoder;
.super Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$BaseArrayDecoder;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "LongArrayDecoder"
.end annotation


# instance fields
.field final mDecoder:Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$LongDecoder;

.field mResult:[J


# direct methods
.method public constructor <init>(Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$LongDecoder;)V
    .locals 2

    .line 1
    const/4 v0, 0x0

    const/16 v1, 0x28

    invoke-direct {p0, v0, v1}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$BaseArrayDecoder;-><init>(II)V

    new-array v0, v1, [J

    iput-object v0, p0, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$LongArrayDecoder;->mResult:[J

    iput-object p1, p0, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$LongArrayDecoder;->mDecoder:Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$LongDecoder;

    return-void
.end method

.method public constructor <init>([JIILorg/codehaus/stax2/ri/typed/ValueDecoderFactory$LongDecoder;)V
    .locals 0

    .line 2
    invoke-direct {p0, p2, p3}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$BaseArrayDecoder;-><init>(II)V

    iput-object p1, p0, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$LongArrayDecoder;->mResult:[J

    iput-object p4, p0, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$LongArrayDecoder;->mDecoder:Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$LongDecoder;

    return-void
.end method


# virtual methods
.method public decodeValue(Ljava/lang/String;)Z
    .locals 3

    .line 1
    iget-object v0, p0, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$LongArrayDecoder;->mDecoder:Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$LongDecoder;

    invoke-virtual {v0, p1}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$LongDecoder;->decode(Ljava/lang/String;)V

    iget-object p1, p0, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$LongArrayDecoder;->mResult:[J

    iget v0, p0, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$BaseArrayDecoder;->mStart:I

    iget v1, p0, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$BaseArrayDecoder;->mCount:I

    add-int/2addr v0, v1

    iget-object v1, p0, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$LongArrayDecoder;->mDecoder:Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$LongDecoder;

    invoke-virtual {v1}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$LongDecoder;->getValue()J

    move-result-wide v1

    aput-wide v1, p1, v0

    iget p1, p0, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$BaseArrayDecoder;->mCount:I

    const/4 v0, 0x1

    add-int/2addr p1, v0

    iput p1, p0, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$BaseArrayDecoder;->mCount:I

    iget v1, p0, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$BaseArrayDecoder;->mEnd:I

    if-lt p1, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public decodeValue([CII)Z
    .locals 2

    .line 2
    iget-object v0, p0, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$LongArrayDecoder;->mDecoder:Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$LongDecoder;

    invoke-virtual {v0, p1, p2, p3}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$LongDecoder;->decode([CII)V

    iget-object p1, p0, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$LongArrayDecoder;->mResult:[J

    iget p2, p0, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$BaseArrayDecoder;->mStart:I

    iget p3, p0, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$BaseArrayDecoder;->mCount:I

    add-int/2addr p2, p3

    iget-object p3, p0, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$LongArrayDecoder;->mDecoder:Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$LongDecoder;

    invoke-virtual {p3}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$LongDecoder;->getValue()J

    move-result-wide v0

    aput-wide v0, p1, p2

    iget p1, p0, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$BaseArrayDecoder;->mCount:I

    const/4 p2, 0x1

    add-int/2addr p1, p2

    iput p1, p0, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$BaseArrayDecoder;->mCount:I

    iget p3, p0, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$BaseArrayDecoder;->mEnd:I

    if-lt p1, p3, :cond_0

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    :goto_0
    return p2
.end method

.method public expand()V
    .locals 6

    iget-object v0, p0, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$LongArrayDecoder;->mResult:[J

    array-length v1, v0

    invoke-virtual {p0, v1}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$BaseArrayDecoder;->calcNewSize(I)I

    move-result v2

    new-array v3, v2, [J

    iput-object v3, p0, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$LongArrayDecoder;->mResult:[J

    iget v4, p0, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$BaseArrayDecoder;->mStart:I

    const/4 v5, 0x0

    invoke-static {v0, v4, v3, v5, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput v5, p0, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$BaseArrayDecoder;->mStart:I

    iput v2, p0, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$BaseArrayDecoder;->mEnd:I

    return-void
.end method

.method public getValues()[J
    .locals 5

    iget v0, p0, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$BaseArrayDecoder;->mCount:I

    new-array v1, v0, [J

    iget-object v2, p0, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$LongArrayDecoder;->mResult:[J

    iget v3, p0, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$BaseArrayDecoder;->mStart:I

    const/4 v4, 0x0

    invoke-static {v2, v3, v1, v4, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v1
.end method

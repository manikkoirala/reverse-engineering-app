.class public abstract Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$BaseArrayDecoder;
.super Lorg/codehaus/stax2/typed/TypedArrayDecoder;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "BaseArrayDecoder"
.end annotation


# static fields
.field protected static final INITIAL_RESULT_BUFFER_SIZE:I = 0x28

.field protected static final SMALL_RESULT_BUFFER_SIZE:I = 0xfa0


# instance fields
.field protected mCount:I

.field protected mEnd:I

.field protected mStart:I


# direct methods
.method public constructor <init>(II)V
    .locals 1

    invoke-direct {p0}, Lorg/codehaus/stax2/typed/TypedArrayDecoder;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$BaseArrayDecoder;->mCount:I

    iput p1, p0, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$BaseArrayDecoder;->mStart:I

    const/4 p1, 0x1

    if-lt p2, p1, :cond_0

    iput p2, p0, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$BaseArrayDecoder;->mEnd:I

    return-void

    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Number of elements to read can not be less than 1"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public calcNewSize(I)I
    .locals 1

    const/16 v0, 0xfa0

    if-ge p1, v0, :cond_0

    shl-int/lit8 p1, p1, 0x2

    return p1

    :cond_0
    add-int/2addr p1, p1

    return p1
.end method

.method public abstract expand()V
.end method

.method public final getCount()I
    .locals 1

    iget v0, p0, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$BaseArrayDecoder;->mCount:I

    return v0
.end method

.method public final hasRoom()Z
    .locals 2

    iget v0, p0, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$BaseArrayDecoder;->mCount:I

    iget v1, p0, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$BaseArrayDecoder;->mEnd:I

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

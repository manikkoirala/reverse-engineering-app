.class public final Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$IntDecoder;
.super Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$DecoderBase;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "IntDecoder"
.end annotation


# instance fields
.field protected mValue:I


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$DecoderBase;-><init>()V

    return-void
.end method


# virtual methods
.method public decode(Ljava/lang/String;)V
    .locals 6

    .line 1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0x2d

    const/4 v4, 0x1

    if-ne v2, v3, :cond_0

    move v3, v4

    goto :goto_0

    :cond_0
    move v3, v1

    :goto_0
    if-nez v3, :cond_2

    const/16 v5, 0x2b

    if-ne v2, v5, :cond_1

    goto :goto_1

    :cond_1
    invoke-virtual {p0, p1, v2, v1, v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$DecoderBase;->skipSignAndZeroes(Ljava/lang/String;CZI)I

    move-result v1

    goto :goto_2

    :cond_2
    :goto_1
    invoke-virtual {p0, p1, v2, v4, v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$DecoderBase;->skipSignAndZeroes(Ljava/lang/String;CZI)I

    move-result v1

    :goto_2
    iget v2, p0, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$DecoderBase;->mNextPtr:I

    sub-int v4, v0, v2

    if-nez v4, :cond_4

    if-eqz v3, :cond_3

    neg-int v1, v1

    :cond_3
    iput v1, p0, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$IntDecoder;->mValue:I

    return-void

    :cond_4
    invoke-virtual {p0, p1, v2, v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$DecoderBase;->verifyDigits(Ljava/lang/String;II)V

    const/16 v0, 0x8

    if-gt v4, v0, :cond_6

    add-int/2addr v4, v2

    invoke-static {v1, p1, v2, v4}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$DecoderBase;->parseInt(ILjava/lang/String;II)I

    move-result p1

    if-eqz v3, :cond_5

    neg-int p1, p1

    :cond_5
    iput p1, p0, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$IntDecoder;->mValue:I

    return-void

    :cond_6
    const/16 v0, 0x9

    if-ne v4, v0, :cond_9

    const/4 v0, 0x3

    if-ge v1, v0, :cond_9

    const/4 v0, 0x2

    if-ne v1, v0, :cond_7

    const-wide/32 v0, 0x77359400

    goto :goto_3

    :cond_7
    const-wide/32 v0, 0x3b9aca00

    :goto_3
    add-int/2addr v4, v2

    invoke-static {p1, v2, v4}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$DecoderBase;->parseInt(Ljava/lang/String;II)I

    move-result v2

    int-to-long v4, v2

    add-long/2addr v0, v4

    if-eqz v3, :cond_8

    neg-long v0, v0

    const-wide/32 v2, -0x80000000

    cmp-long v2, v0, v2

    if-ltz v2, :cond_9

    :goto_4
    long-to-int p1, v0

    iput p1, p0, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$IntDecoder;->mValue:I

    return-void

    :cond_8
    const-wide/32 v2, 0x7fffffff

    cmp-long v2, v0, v2

    if-gtz v2, :cond_9

    goto :goto_4

    :cond_9
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "value \""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0, p1}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$DecoderBase;->lexicalDesc(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "\" not a valid 32-bit integer: overflow."

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public decode([CII)V
    .locals 7

    .line 2
    aget-char v2, p1, p2

    const/16 v0, 0x2d

    if-ne v2, v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    move v6, v0

    if-nez v6, :cond_2

    const/16 v0, 0x2b

    if-ne v2, v0, :cond_1

    goto :goto_1

    :cond_1
    const/4 v3, 0x0

    move-object v0, p0

    move-object v1, p1

    move v4, p2

    move v5, p3

    invoke-virtual/range {v0 .. v5}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$DecoderBase;->skipSignAndZeroes([CCZII)I

    move-result v0

    goto :goto_2

    :cond_2
    :goto_1
    const/4 v3, 0x1

    move-object v0, p0

    move-object v1, p1

    move v4, p2

    move v5, p3

    invoke-virtual/range {v0 .. v5}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$DecoderBase;->skipSignAndZeroes([CCZII)I

    move-result v0

    :goto_2
    iget v1, p0, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$DecoderBase;->mNextPtr:I

    sub-int v2, p3, v1

    if-nez v2, :cond_4

    if-eqz v6, :cond_3

    neg-int v0, v0

    :cond_3
    iput v0, p0, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$IntDecoder;->mValue:I

    return-void

    :cond_4
    invoke-virtual {p0, p1, p2, p3, v1}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$DecoderBase;->verifyDigits([CIII)V

    const/16 v3, 0x8

    if-gt v2, v3, :cond_6

    add-int/2addr v2, v1

    invoke-static {v0, p1, v1, v2}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$DecoderBase;->parseInt(I[CII)I

    move-result p1

    if-eqz v6, :cond_5

    neg-int p1, p1

    :cond_5
    iput p1, p0, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$IntDecoder;->mValue:I

    return-void

    :cond_6
    const/16 v3, 0x9

    if-ne v2, v3, :cond_9

    const/4 v3, 0x3

    if-ge v0, v3, :cond_9

    const/4 v3, 0x2

    if-ne v0, v3, :cond_7

    const-wide/32 v3, 0x77359400

    goto :goto_3

    :cond_7
    const-wide/32 v3, 0x3b9aca00

    :goto_3
    add-int/2addr v2, v1

    invoke-static {p1, v1, v2}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$DecoderBase;->parseInt([CII)I

    move-result v0

    int-to-long v0, v0

    add-long/2addr v3, v0

    if-eqz v6, :cond_8

    neg-long v0, v3

    const-wide/32 v2, -0x80000000

    cmp-long v2, v0, v2

    if-ltz v2, :cond_9

    long-to-int p1, v0

    :goto_4
    iput p1, p0, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$IntDecoder;->mValue:I

    return-void

    :cond_8
    const-wide/32 v0, 0x7fffffff

    cmp-long v0, v3, v0

    if-gtz v0, :cond_9

    long-to-int p1, v3

    goto :goto_4

    :cond_9
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "value \""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0, p1, p2, p3}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$DecoderBase;->lexicalDesc([CII)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "\" not a valid 32-bit integer: overflow."

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getType()Ljava/lang/String;
    .locals 1

    const-string v0, "int"

    return-object v0
.end method

.method public getValue()I
    .locals 1

    iget v0, p0, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$IntDecoder;->mValue:I

    return v0
.end method

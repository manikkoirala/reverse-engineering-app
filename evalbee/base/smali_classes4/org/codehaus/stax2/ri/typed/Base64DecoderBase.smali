.class abstract Lorg/codehaus/stax2/ri/typed/Base64DecoderBase;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field static final INT_SPACE:I = 0x20

.field static final STATE_INITIAL:I = 0x0

.field static final STATE_OUTPUT_1:I = 0x6

.field static final STATE_OUTPUT_2:I = 0x5

.field static final STATE_OUTPUT_3:I = 0x4

.field static final STATE_VALID_1:I = 0x1

.field static final STATE_VALID_2:I = 0x2

.field static final STATE_VALID_2_AND_PADDING:I = 0x7

.field static final STATE_VALID_3:I = 0x3


# instance fields
.field _byteAggr:Lorg/codehaus/stax2/ri/Stax2Util$ByteAggregator;

.field _decodedData:I

.field _state:I

.field _variant:Lorg/codehaus/stax2/typed/Base64Variant;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lorg/codehaus/stax2/ri/typed/Base64DecoderBase;->_state:I

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/codehaus/stax2/ri/typed/Base64DecoderBase;->_byteAggr:Lorg/codehaus/stax2/ri/Stax2Util$ByteAggregator;

    return-void
.end method


# virtual methods
.method public abstract decode([BII)I
.end method

.method public decodeCompletely()[B
    .locals 6

    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/typed/Base64DecoderBase;->getByteAggregator()Lorg/codehaus/stax2/ri/Stax2Util$ByteAggregator;

    move-result-object v0

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/Stax2Util$ByteAggregator;->startAggregation()[B

    move-result-object v1

    :goto_0
    array-length v2, v1

    const/4 v3, 0x0

    :cond_0
    invoke-virtual {p0, v1, v3, v2}, Lorg/codehaus/stax2/ri/typed/Base64DecoderBase;->decode([BII)I

    move-result v4

    const/4 v5, 0x1

    if-ge v4, v5, :cond_3

    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/typed/Base64DecoderBase;->endOfContent()I

    move-result v4

    if-ltz v4, :cond_2

    if-lez v4, :cond_1

    goto :goto_1

    :cond_1
    invoke-virtual {v0, v1, v3}, Lorg/codehaus/stax2/ri/Stax2Util$ByteAggregator;->aggregateAll([BI)[B

    move-result-object v0

    return-object v0

    :cond_2
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Incomplete base64 triplet at the end of decoded content"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    add-int/2addr v3, v4

    sub-int/2addr v2, v4

    :goto_1
    if-gtz v2, :cond_0

    invoke-virtual {v0, v1}, Lorg/codehaus/stax2/ri/Stax2Util$ByteAggregator;->addFullBlock([B)[B

    move-result-object v1

    goto :goto_0
.end method

.method public endOfContent()I
    .locals 6

    iget v0, p0, Lorg/codehaus/stax2/ri/typed/Base64DecoderBase;->_state:I

    if-eqz v0, :cond_4

    const/4 v1, 0x4

    if-eq v0, v1, :cond_4

    const/4 v2, 0x5

    if-eq v0, v2, :cond_4

    const/4 v3, 0x6

    if-ne v0, v3, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lorg/codehaus/stax2/ri/typed/Base64DecoderBase;->_variant:Lorg/codehaus/stax2/typed/Base64Variant;

    invoke-virtual {v0}, Lorg/codehaus/stax2/typed/Base64Variant;->usesPadding()Z

    move-result v0

    const/4 v4, -0x1

    if-eqz v0, :cond_1

    return v4

    :cond_1
    iget v0, p0, Lorg/codehaus/stax2/ri/typed/Base64DecoderBase;->_state:I

    const/4 v5, 0x2

    if-ne v0, v5, :cond_2

    iput v3, p0, Lorg/codehaus/stax2/ri/typed/Base64DecoderBase;->_state:I

    iget v0, p0, Lorg/codehaus/stax2/ri/typed/Base64DecoderBase;->_decodedData:I

    shr-int/2addr v0, v1

    iput v0, p0, Lorg/codehaus/stax2/ri/typed/Base64DecoderBase;->_decodedData:I

    const/4 v0, 0x1

    return v0

    :cond_2
    const/4 v1, 0x3

    if-ne v0, v1, :cond_3

    iget v0, p0, Lorg/codehaus/stax2/ri/typed/Base64DecoderBase;->_decodedData:I

    shr-int/2addr v0, v5

    iput v0, p0, Lorg/codehaus/stax2/ri/typed/Base64DecoderBase;->_decodedData:I

    iput v2, p0, Lorg/codehaus/stax2/ri/typed/Base64DecoderBase;->_state:I

    return v5

    :cond_3
    return v4

    :cond_4
    :goto_0
    const/4 v0, 0x0

    return v0
.end method

.method public getByteAggregator()Lorg/codehaus/stax2/ri/Stax2Util$ByteAggregator;
    .locals 1

    iget-object v0, p0, Lorg/codehaus/stax2/ri/typed/Base64DecoderBase;->_byteAggr:Lorg/codehaus/stax2/ri/Stax2Util$ByteAggregator;

    if-nez v0, :cond_0

    new-instance v0, Lorg/codehaus/stax2/ri/Stax2Util$ByteAggregator;

    invoke-direct {v0}, Lorg/codehaus/stax2/ri/Stax2Util$ByteAggregator;-><init>()V

    iput-object v0, p0, Lorg/codehaus/stax2/ri/typed/Base64DecoderBase;->_byteAggr:Lorg/codehaus/stax2/ri/Stax2Util$ByteAggregator;

    :cond_0
    iget-object v0, p0, Lorg/codehaus/stax2/ri/typed/Base64DecoderBase;->_byteAggr:Lorg/codehaus/stax2/ri/Stax2Util$ByteAggregator;

    return-object v0
.end method

.method public hasData()Z
    .locals 2

    iget v0, p0, Lorg/codehaus/stax2/ri/typed/Base64DecoderBase;->_state:I

    const/4 v1, 0x4

    if-lt v0, v1, :cond_0

    const/4 v1, 0x6

    if-gt v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public reportInvalidChar(CI)Ljava/lang/IllegalArgumentException;
    .locals 1

    .line 1
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lorg/codehaus/stax2/ri/typed/Base64DecoderBase;->reportInvalidChar(CILjava/lang/String;)Ljava/lang/IllegalArgumentException;

    move-result-object p1

    return-object p1
.end method

.method public reportInvalidChar(CILjava/lang/String;)Ljava/lang/IllegalArgumentException;
    .locals 2

    .line 2
    const/16 v0, 0x20

    if-gt p1, v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Illegal white space character (code 0x"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, ") as character #"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 p2, p2, 0x1

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, " of 4-char base64 unit: can only used between units"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_2

    :cond_0
    iget-object v0, p0, Lorg/codehaus/stax2/ri/typed/Base64DecoderBase;->_variant:Lorg/codehaus/stax2/typed/Base64Variant;

    invoke-virtual {v0, p1}, Lorg/codehaus/stax2/typed/Base64Variant;->usesPaddingChar(C)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Unexpected padding character (\'"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lorg/codehaus/stax2/ri/typed/Base64DecoderBase;->_variant:Lorg/codehaus/stax2/typed/Base64Variant;

    invoke-virtual {v0}, Lorg/codehaus/stax2/typed/Base64Variant;->getPaddingChar()C

    move-result v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v0, "\') as character #"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 p2, p2, 0x1

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p2, " of 4-char base64 unit: padding only legal as 3rd or 4th character"

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_2

    :cond_1
    invoke-static {p1}, Ljava/lang/Character;->isDefined(C)Z

    move-result p2

    const-string v0, ") in base64 content"

    if-eqz p2, :cond_3

    invoke-static {p1}, Ljava/lang/Character;->isISOControl(C)Z

    move-result p2

    if-eqz p2, :cond_2

    goto :goto_0

    :cond_2
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Illegal character \'"

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v1, "\' (code 0x"

    goto :goto_1

    :cond_3
    :goto_0
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Illegal character (code 0x"

    :goto_1
    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    :goto_2
    if-eqz p3, :cond_4

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, ": "

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    :cond_4
    new-instance p2, Ljava/lang/IllegalArgumentException;

    invoke-direct {p2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    return-object p2
.end method

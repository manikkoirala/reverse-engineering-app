.class final Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory$IntEncoder;
.super Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory$TypedScalarEncoder;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "IntEncoder"
.end annotation


# instance fields
.field _value:I


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory$TypedScalarEncoder;-><init>()V

    return-void
.end method


# virtual methods
.method public encodeMore([BII)I
    .locals 0

    .line 1
    iget p3, p0, Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory$IntEncoder;->_value:I

    invoke-static {p3, p1, p2}, Lorg/codehaus/stax2/ri/typed/NumberUtil;->writeInt(I[BI)I

    move-result p1

    return p1
.end method

.method public encodeMore([CII)I
    .locals 0

    .line 2
    iget p3, p0, Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory$IntEncoder;->_value:I

    invoke-static {p3, p1, p2}, Lorg/codehaus/stax2/ri/typed/NumberUtil;->writeInt(I[CI)I

    move-result p1

    return p1
.end method

.method public reset(I)V
    .locals 0

    iput p1, p0, Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory$IntEncoder;->_value:I

    return-void
.end method

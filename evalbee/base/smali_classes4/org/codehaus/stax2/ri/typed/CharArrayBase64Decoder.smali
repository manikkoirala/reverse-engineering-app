.class public Lorg/codehaus/stax2/ri/typed/CharArrayBase64Decoder;
.super Lorg/codehaus/stax2/ri/typed/Base64DecoderBase;
.source "SourceFile"


# instance fields
.field protected _currSegment:[C

.field protected _currSegmentEnd:I

.field protected _currSegmentPtr:I

.field protected _lastSegmentEnd:I

.field protected _lastSegmentOffset:I

.field protected _nextSegmentIndex:I

.field protected final _nextSegments:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList<",
            "[C>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lorg/codehaus/stax2/ri/typed/Base64DecoderBase;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/codehaus/stax2/ri/typed/CharArrayBase64Decoder;->_nextSegments:Ljava/util/ArrayList;

    return-void
.end method

.method private nextSegment()Z
    .locals 4

    iget v0, p0, Lorg/codehaus/stax2/ri/typed/CharArrayBase64Decoder;->_nextSegmentIndex:I

    iget-object v1, p0, Lorg/codehaus/stax2/ri/typed/CharArrayBase64Decoder;->_nextSegments:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    const/4 v2, 0x0

    if-ge v0, v1, :cond_1

    iget-object v0, p0, Lorg/codehaus/stax2/ri/typed/CharArrayBase64Decoder;->_nextSegments:Ljava/util/ArrayList;

    iget v1, p0, Lorg/codehaus/stax2/ri/typed/CharArrayBase64Decoder;->_nextSegmentIndex:I

    add-int/lit8 v3, v1, 0x1

    iput v3, p0, Lorg/codehaus/stax2/ri/typed/CharArrayBase64Decoder;->_nextSegmentIndex:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [C

    iput-object v0, p0, Lorg/codehaus/stax2/ri/typed/CharArrayBase64Decoder;->_currSegment:[C

    iget v0, p0, Lorg/codehaus/stax2/ri/typed/CharArrayBase64Decoder;->_nextSegmentIndex:I

    iget-object v1, p0, Lorg/codehaus/stax2/ri/typed/CharArrayBase64Decoder;->_nextSegments:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ne v0, v1, :cond_0

    iget v0, p0, Lorg/codehaus/stax2/ri/typed/CharArrayBase64Decoder;->_lastSegmentOffset:I

    iput v0, p0, Lorg/codehaus/stax2/ri/typed/CharArrayBase64Decoder;->_currSegmentPtr:I

    iget v0, p0, Lorg/codehaus/stax2/ri/typed/CharArrayBase64Decoder;->_lastSegmentEnd:I

    goto :goto_0

    :cond_0
    iput v2, p0, Lorg/codehaus/stax2/ri/typed/CharArrayBase64Decoder;->_currSegmentPtr:I

    iget-object v0, p0, Lorg/codehaus/stax2/ri/typed/CharArrayBase64Decoder;->_currSegment:[C

    array-length v0, v0

    :goto_0
    iput v0, p0, Lorg/codehaus/stax2/ri/typed/CharArrayBase64Decoder;->_currSegmentEnd:I

    const/4 v0, 0x1

    return v0

    :cond_1
    return v2
.end method


# virtual methods
.method public decode([BII)I
    .locals 12

    add-int/2addr p3, p2

    move v0, p2

    :goto_0
    iget v1, p0, Lorg/codehaus/stax2/ri/typed/Base64DecoderBase;->_state:I

    const/4 v2, -0x2

    const/4 v3, 0x0

    const/4 v4, 0x5

    const/4 v5, 0x4

    const/4 v6, 0x3

    const/4 v7, 0x2

    const/4 v8, 0x6

    packed-switch v1, :pswitch_data_0

    new-instance p1, Ljava/lang/IllegalStateException;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string p3, "Illegal internal state "

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget p3, p0, Lorg/codehaus/stax2/ri/typed/Base64DecoderBase;->_state:I

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :pswitch_0
    iget v1, p0, Lorg/codehaus/stax2/ri/typed/CharArrayBase64Decoder;->_currSegmentPtr:I

    iget v2, p0, Lorg/codehaus/stax2/ri/typed/CharArrayBase64Decoder;->_currSegmentEnd:I

    if-lt v1, v2, :cond_0

    invoke-direct {p0}, Lorg/codehaus/stax2/ri/typed/CharArrayBase64Decoder;->nextSegment()Z

    move-result v1

    if-nez v1, :cond_0

    goto/16 :goto_1

    :cond_0
    iget-object v1, p0, Lorg/codehaus/stax2/ri/typed/CharArrayBase64Decoder;->_currSegment:[C

    iget v2, p0, Lorg/codehaus/stax2/ri/typed/CharArrayBase64Decoder;->_currSegmentPtr:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lorg/codehaus/stax2/ri/typed/CharArrayBase64Decoder;->_currSegmentPtr:I

    aget-char v1, v1, v2

    iget-object v2, p0, Lorg/codehaus/stax2/ri/typed/Base64DecoderBase;->_variant:Lorg/codehaus/stax2/typed/Base64Variant;

    invoke-virtual {v2, v1}, Lorg/codehaus/stax2/typed/Base64Variant;->usesPaddingChar(C)Z

    move-result v2

    if-eqz v2, :cond_1

    iput v8, p0, Lorg/codehaus/stax2/ri/typed/Base64DecoderBase;->_state:I

    iget v1, p0, Lorg/codehaus/stax2/ri/typed/Base64DecoderBase;->_decodedData:I

    shr-int/2addr v1, v5

    iput v1, p0, Lorg/codehaus/stax2/ri/typed/Base64DecoderBase;->_decodedData:I

    goto :goto_0

    :cond_1
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string p2, "expected padding character \'"

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p2, p0, Lorg/codehaus/stax2/ri/typed/Base64DecoderBase;->_variant:Lorg/codehaus/stax2/typed/Base64Variant;

    invoke-virtual {p2}, Lorg/codehaus/stax2/typed/Base64Variant;->getPaddingChar()C

    move-result p2

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string p2, "\'"

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, v1, v6, p1}, Lorg/codehaus/stax2/ri/typed/Base64DecoderBase;->reportInvalidChar(CILjava/lang/String;)Ljava/lang/IllegalArgumentException;

    move-result-object p1

    throw p1

    :cond_2
    :pswitch_1
    iget v1, p0, Lorg/codehaus/stax2/ri/typed/CharArrayBase64Decoder;->_currSegmentPtr:I

    iget v9, p0, Lorg/codehaus/stax2/ri/typed/CharArrayBase64Decoder;->_currSegmentEnd:I

    if-lt v1, v9, :cond_3

    invoke-direct {p0}, Lorg/codehaus/stax2/ri/typed/CharArrayBase64Decoder;->nextSegment()Z

    move-result v1

    if-nez v1, :cond_3

    goto/16 :goto_1

    :cond_3
    iget-object v1, p0, Lorg/codehaus/stax2/ri/typed/CharArrayBase64Decoder;->_currSegment:[C

    iget v9, p0, Lorg/codehaus/stax2/ri/typed/CharArrayBase64Decoder;->_currSegmentPtr:I

    add-int/lit8 v10, v9, 0x1

    iput v10, p0, Lorg/codehaus/stax2/ri/typed/CharArrayBase64Decoder;->_currSegmentPtr:I

    aget-char v1, v1, v9

    const/16 v9, 0x20

    if-le v1, v9, :cond_2

    iget-object v9, p0, Lorg/codehaus/stax2/ri/typed/Base64DecoderBase;->_variant:Lorg/codehaus/stax2/typed/Base64Variant;

    invoke-virtual {v9, v1}, Lorg/codehaus/stax2/typed/Base64Variant;->decodeBase64Char(C)I

    move-result v9

    if-ltz v9, :cond_f

    iput v9, p0, Lorg/codehaus/stax2/ri/typed/Base64DecoderBase;->_decodedData:I

    :pswitch_2
    iget v1, p0, Lorg/codehaus/stax2/ri/typed/CharArrayBase64Decoder;->_currSegmentPtr:I

    iget v9, p0, Lorg/codehaus/stax2/ri/typed/CharArrayBase64Decoder;->_currSegmentEnd:I

    const/4 v10, 0x1

    if-lt v1, v9, :cond_4

    invoke-direct {p0}, Lorg/codehaus/stax2/ri/typed/CharArrayBase64Decoder;->nextSegment()Z

    move-result v1

    if-nez v1, :cond_4

    iput v10, p0, Lorg/codehaus/stax2/ri/typed/Base64DecoderBase;->_state:I

    goto/16 :goto_1

    :cond_4
    iget-object v1, p0, Lorg/codehaus/stax2/ri/typed/CharArrayBase64Decoder;->_currSegment:[C

    iget v9, p0, Lorg/codehaus/stax2/ri/typed/CharArrayBase64Decoder;->_currSegmentPtr:I

    add-int/lit8 v11, v9, 0x1

    iput v11, p0, Lorg/codehaus/stax2/ri/typed/CharArrayBase64Decoder;->_currSegmentPtr:I

    aget-char v1, v1, v9

    iget-object v9, p0, Lorg/codehaus/stax2/ri/typed/Base64DecoderBase;->_variant:Lorg/codehaus/stax2/typed/Base64Variant;

    invoke-virtual {v9, v1}, Lorg/codehaus/stax2/typed/Base64Variant;->decodeBase64Char(C)I

    move-result v9

    if-ltz v9, :cond_e

    iget v1, p0, Lorg/codehaus/stax2/ri/typed/Base64DecoderBase;->_decodedData:I

    shl-int/2addr v1, v8

    or-int/2addr v1, v9

    iput v1, p0, Lorg/codehaus/stax2/ri/typed/Base64DecoderBase;->_decodedData:I

    :pswitch_3
    iget v1, p0, Lorg/codehaus/stax2/ri/typed/CharArrayBase64Decoder;->_currSegmentPtr:I

    iget v9, p0, Lorg/codehaus/stax2/ri/typed/CharArrayBase64Decoder;->_currSegmentEnd:I

    if-lt v1, v9, :cond_5

    invoke-direct {p0}, Lorg/codehaus/stax2/ri/typed/CharArrayBase64Decoder;->nextSegment()Z

    move-result v1

    if-nez v1, :cond_5

    iput v7, p0, Lorg/codehaus/stax2/ri/typed/Base64DecoderBase;->_state:I

    goto/16 :goto_1

    :cond_5
    iget-object v1, p0, Lorg/codehaus/stax2/ri/typed/CharArrayBase64Decoder;->_currSegment:[C

    iget v9, p0, Lorg/codehaus/stax2/ri/typed/CharArrayBase64Decoder;->_currSegmentPtr:I

    add-int/lit8 v10, v9, 0x1

    iput v10, p0, Lorg/codehaus/stax2/ri/typed/CharArrayBase64Decoder;->_currSegmentPtr:I

    aget-char v1, v1, v9

    iget-object v9, p0, Lorg/codehaus/stax2/ri/typed/Base64DecoderBase;->_variant:Lorg/codehaus/stax2/typed/Base64Variant;

    invoke-virtual {v9, v1}, Lorg/codehaus/stax2/typed/Base64Variant;->decodeBase64Char(C)I

    move-result v9

    if-gez v9, :cond_7

    if-ne v9, v2, :cond_6

    const/4 v1, 0x7

    iput v1, p0, Lorg/codehaus/stax2/ri/typed/Base64DecoderBase;->_state:I

    goto/16 :goto_0

    :cond_6
    invoke-virtual {p0, v1, v7}, Lorg/codehaus/stax2/ri/typed/Base64DecoderBase;->reportInvalidChar(CI)Ljava/lang/IllegalArgumentException;

    move-result-object p1

    throw p1

    :cond_7
    iget v1, p0, Lorg/codehaus/stax2/ri/typed/Base64DecoderBase;->_decodedData:I

    shl-int/2addr v1, v8

    or-int/2addr v1, v9

    iput v1, p0, Lorg/codehaus/stax2/ri/typed/Base64DecoderBase;->_decodedData:I

    :pswitch_4
    iget v1, p0, Lorg/codehaus/stax2/ri/typed/CharArrayBase64Decoder;->_currSegmentPtr:I

    iget v9, p0, Lorg/codehaus/stax2/ri/typed/CharArrayBase64Decoder;->_currSegmentEnd:I

    if-lt v1, v9, :cond_8

    invoke-direct {p0}, Lorg/codehaus/stax2/ri/typed/CharArrayBase64Decoder;->nextSegment()Z

    move-result v1

    if-nez v1, :cond_8

    iput v6, p0, Lorg/codehaus/stax2/ri/typed/Base64DecoderBase;->_state:I

    goto :goto_1

    :cond_8
    iget-object v1, p0, Lorg/codehaus/stax2/ri/typed/CharArrayBase64Decoder;->_currSegment:[C

    iget v9, p0, Lorg/codehaus/stax2/ri/typed/CharArrayBase64Decoder;->_currSegmentPtr:I

    add-int/lit8 v10, v9, 0x1

    iput v10, p0, Lorg/codehaus/stax2/ri/typed/CharArrayBase64Decoder;->_currSegmentPtr:I

    aget-char v1, v1, v9

    iget-object v9, p0, Lorg/codehaus/stax2/ri/typed/Base64DecoderBase;->_variant:Lorg/codehaus/stax2/typed/Base64Variant;

    invoke-virtual {v9, v1}, Lorg/codehaus/stax2/typed/Base64Variant;->decodeBase64Char(C)I

    move-result v9

    if-gez v9, :cond_a

    if-ne v9, v2, :cond_9

    iget v1, p0, Lorg/codehaus/stax2/ri/typed/Base64DecoderBase;->_decodedData:I

    shr-int/2addr v1, v7

    iput v1, p0, Lorg/codehaus/stax2/ri/typed/Base64DecoderBase;->_decodedData:I

    iput v4, p0, Lorg/codehaus/stax2/ri/typed/Base64DecoderBase;->_state:I

    goto/16 :goto_0

    :cond_9
    invoke-virtual {p0, v1, v6}, Lorg/codehaus/stax2/ri/typed/Base64DecoderBase;->reportInvalidChar(CI)Ljava/lang/IllegalArgumentException;

    move-result-object p1

    throw p1

    :cond_a
    iget v1, p0, Lorg/codehaus/stax2/ri/typed/Base64DecoderBase;->_decodedData:I

    shl-int/2addr v1, v8

    or-int/2addr v1, v9

    iput v1, p0, Lorg/codehaus/stax2/ri/typed/Base64DecoderBase;->_decodedData:I

    :pswitch_5
    if-lt v0, p3, :cond_b

    iput v5, p0, Lorg/codehaus/stax2/ri/typed/Base64DecoderBase;->_state:I

    goto :goto_1

    :cond_b
    add-int/lit8 v1, v0, 0x1

    iget v2, p0, Lorg/codehaus/stax2/ri/typed/Base64DecoderBase;->_decodedData:I

    shr-int/lit8 v2, v2, 0x10

    int-to-byte v2, v2

    aput-byte v2, p1, v0

    move v0, v1

    :pswitch_6
    if-lt v0, p3, :cond_c

    iput v4, p0, Lorg/codehaus/stax2/ri/typed/Base64DecoderBase;->_state:I

    goto :goto_1

    :cond_c
    add-int/lit8 v1, v0, 0x1

    iget v2, p0, Lorg/codehaus/stax2/ri/typed/Base64DecoderBase;->_decodedData:I

    shr-int/lit8 v2, v2, 0x8

    int-to-byte v2, v2

    aput-byte v2, p1, v0

    move v0, v1

    :pswitch_7
    if-lt v0, p3, :cond_d

    iput v8, p0, Lorg/codehaus/stax2/ri/typed/Base64DecoderBase;->_state:I

    :goto_1
    sub-int/2addr v0, p2

    return v0

    :cond_d
    add-int/lit8 v1, v0, 0x1

    iget v2, p0, Lorg/codehaus/stax2/ri/typed/Base64DecoderBase;->_decodedData:I

    int-to-byte v2, v2

    aput-byte v2, p1, v0

    iput v3, p0, Lorg/codehaus/stax2/ri/typed/Base64DecoderBase;->_state:I

    move v0, v1

    goto/16 :goto_0

    :cond_e
    invoke-virtual {p0, v1, v10}, Lorg/codehaus/stax2/ri/typed/Base64DecoderBase;->reportInvalidChar(CI)Ljava/lang/IllegalArgumentException;

    move-result-object p1

    throw p1

    :cond_f
    invoke-virtual {p0, v1, v3}, Lorg/codehaus/stax2/ri/typed/Base64DecoderBase;->reportInvalidChar(CI)Ljava/lang/IllegalArgumentException;

    move-result-object p1

    throw p1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic decodeCompletely()[B
    .locals 1

    invoke-super {p0}, Lorg/codehaus/stax2/ri/typed/Base64DecoderBase;->decodeCompletely()[B

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic endOfContent()I
    .locals 1

    .line 1
    invoke-super {p0}, Lorg/codehaus/stax2/ri/typed/Base64DecoderBase;->endOfContent()I

    move-result v0

    return v0
.end method

.method public bridge synthetic getByteAggregator()Lorg/codehaus/stax2/ri/Stax2Util$ByteAggregator;
    .locals 1

    invoke-super {p0}, Lorg/codehaus/stax2/ri/typed/Base64DecoderBase;->getByteAggregator()Lorg/codehaus/stax2/ri/Stax2Util$ByteAggregator;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic hasData()Z
    .locals 1

    .line 1
    invoke-super {p0}, Lorg/codehaus/stax2/ri/typed/Base64DecoderBase;->hasData()Z

    move-result v0

    return v0
.end method

.method public init(Lorg/codehaus/stax2/typed/Base64Variant;Z[CIILjava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/codehaus/stax2/typed/Base64Variant;",
            "Z[CII",
            "Ljava/util/List<",
            "[C>;)V"
        }
    .end annotation

    iput-object p1, p0, Lorg/codehaus/stax2/ri/typed/Base64DecoderBase;->_variant:Lorg/codehaus/stax2/typed/Base64Variant;

    const/4 p1, 0x0

    if-eqz p2, :cond_0

    iput p1, p0, Lorg/codehaus/stax2/ri/typed/Base64DecoderBase;->_state:I

    :cond_0
    iget-object p2, p0, Lorg/codehaus/stax2/ri/typed/CharArrayBase64Decoder;->_nextSegments:Ljava/util/ArrayList;

    invoke-virtual {p2}, Ljava/util/ArrayList;->clear()V

    if-eqz p6, :cond_4

    invoke-interface {p6}, Ljava/util/List;->isEmpty()Z

    move-result p2

    if-eqz p2, :cond_1

    goto :goto_1

    :cond_1
    if-eqz p3, :cond_3

    invoke-interface {p6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p6

    check-cast p6, [C

    iput-object p6, p0, Lorg/codehaus/stax2/ri/typed/CharArrayBase64Decoder;->_currSegment:[C

    iput p1, p0, Lorg/codehaus/stax2/ri/typed/CharArrayBase64Decoder;->_currSegmentPtr:I

    array-length p6, p6

    iput p6, p0, Lorg/codehaus/stax2/ri/typed/CharArrayBase64Decoder;->_currSegmentEnd:I

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result p6

    if-eqz p6, :cond_2

    iget-object p6, p0, Lorg/codehaus/stax2/ri/typed/CharArrayBase64Decoder;->_nextSegments:Ljava/util/ArrayList;

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    iput p1, p0, Lorg/codehaus/stax2/ri/typed/CharArrayBase64Decoder;->_nextSegmentIndex:I

    iget-object p1, p0, Lorg/codehaus/stax2/ri/typed/CharArrayBase64Decoder;->_nextSegments:Ljava/util/ArrayList;

    invoke-virtual {p1, p3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iput p4, p0, Lorg/codehaus/stax2/ri/typed/CharArrayBase64Decoder;->_lastSegmentOffset:I

    add-int/2addr p4, p5

    iput p4, p0, Lorg/codehaus/stax2/ri/typed/CharArrayBase64Decoder;->_lastSegmentEnd:I

    goto :goto_2

    :cond_3
    new-instance p1, Ljava/lang/IllegalArgumentException;

    invoke-direct {p1}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw p1

    :cond_4
    :goto_1
    iput-object p3, p0, Lorg/codehaus/stax2/ri/typed/CharArrayBase64Decoder;->_currSegment:[C

    iput p4, p0, Lorg/codehaus/stax2/ri/typed/CharArrayBase64Decoder;->_currSegmentPtr:I

    add-int/2addr p4, p5

    iput p4, p0, Lorg/codehaus/stax2/ri/typed/CharArrayBase64Decoder;->_currSegmentEnd:I

    :goto_2
    return-void
.end method

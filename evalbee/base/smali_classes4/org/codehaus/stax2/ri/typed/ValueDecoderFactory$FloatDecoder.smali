.class public final Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$FloatDecoder;
.super Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$DecoderBase;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "FloatDecoder"
.end annotation


# instance fields
.field protected mValue:F


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$DecoderBase;-><init>()V

    return-void
.end method


# virtual methods
.method public decode(Ljava/lang/String;)V
    .locals 9

    .line 1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0x46

    const/16 v2, 0x49

    const/4 v3, 0x0

    const/4 v4, 0x2

    const/4 v5, 0x1

    const/16 v6, 0x4e

    const/4 v7, 0x3

    if-ne v0, v7, :cond_1

    invoke-virtual {p1, v3}, Ljava/lang/String;->charAt(I)C

    move-result v0

    if-ne v0, v2, :cond_0

    invoke-virtual {p1, v5}, Ljava/lang/String;->charAt(I)C

    move-result v0

    if-ne v0, v6, :cond_2

    invoke-virtual {p1, v4}, Ljava/lang/String;->charAt(I)C

    move-result v0

    if-ne v0, v1, :cond_2

    const/high16 p1, 0x7f800000    # Float.POSITIVE_INFINITY

    :goto_0
    iput p1, p0, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$FloatDecoder;->mValue:F

    return-void

    :cond_0
    if-ne v0, v6, :cond_2

    invoke-virtual {p1, v5}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v1, 0x61

    if-ne v0, v1, :cond_2

    invoke-virtual {p1, v4}, Ljava/lang/String;->charAt(I)C

    move-result v0

    if-ne v0, v6, :cond_2

    const/high16 p1, 0x7fc00000    # Float.NaN

    goto :goto_0

    :cond_1
    const/4 v8, 0x4

    if-ne v0, v8, :cond_2

    invoke-virtual {p1, v3}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v3, 0x2d

    if-ne v0, v3, :cond_2

    invoke-virtual {p1, v5}, Ljava/lang/String;->charAt(I)C

    move-result v0

    if-ne v0, v2, :cond_2

    invoke-virtual {p1, v4}, Ljava/lang/String;->charAt(I)C

    move-result v0

    if-ne v0, v6, :cond_2

    invoke-virtual {p1, v7}, Ljava/lang/String;->charAt(I)C

    move-result v0

    if-ne v0, v1, :cond_2

    const/high16 p1, -0x800000    # Float.NEGATIVE_INFINITY

    goto :goto_0

    :cond_2
    :try_start_0
    invoke-static {p1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v0

    iput v0, p0, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$FloatDecoder;->mValue:F
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    invoke-virtual {p0, p1}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$DecoderBase;->constructInvalidValue(Ljava/lang/String;)Ljava/lang/IllegalArgumentException;

    move-result-object p1

    throw p1
.end method

.method public decode([CII)V
    .locals 5

    .line 2
    sub-int/2addr p3, p2

    const/4 v0, 0x3

    const/16 v1, 0x46

    const/16 v2, 0x49

    const/16 v3, 0x4e

    if-ne p3, v0, :cond_1

    aget-char v0, p1, p2

    if-ne v0, v2, :cond_0

    add-int/lit8 v0, p2, 0x1

    aget-char v0, p1, v0

    if-ne v0, v3, :cond_2

    add-int/lit8 v0, p2, 0x2

    aget-char v0, p1, v0

    if-ne v0, v1, :cond_2

    const/high16 p1, 0x7f800000    # Float.POSITIVE_INFINITY

    :goto_0
    iput p1, p0, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$FloatDecoder;->mValue:F

    return-void

    :cond_0
    if-ne v0, v3, :cond_2

    add-int/lit8 v0, p2, 0x1

    aget-char v0, p1, v0

    const/16 v1, 0x61

    if-ne v0, v1, :cond_2

    add-int/lit8 v0, p2, 0x2

    aget-char v0, p1, v0

    if-ne v0, v3, :cond_2

    const/high16 p1, 0x7fc00000    # Float.NaN

    goto :goto_0

    :cond_1
    const/4 v0, 0x4

    if-ne p3, v0, :cond_2

    aget-char v0, p1, p2

    const/16 v4, 0x2d

    if-ne v0, v4, :cond_2

    add-int/lit8 v0, p2, 0x1

    aget-char v0, p1, v0

    if-ne v0, v2, :cond_2

    add-int/lit8 v0, p2, 0x2

    aget-char v0, p1, v0

    if-ne v0, v3, :cond_2

    add-int/lit8 v0, p2, 0x3

    aget-char v0, p1, v0

    if-ne v0, v1, :cond_2

    const/high16 p1, -0x800000    # Float.NEGATIVE_INFINITY

    goto :goto_0

    :cond_2
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, p1, p2, p3}, Ljava/lang/String;-><init>([CII)V

    :try_start_0
    invoke-static {v0}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result p1

    iput p1, p0, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$FloatDecoder;->mValue:F
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    invoke-virtual {p0, v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$DecoderBase;->constructInvalidValue(Ljava/lang/String;)Ljava/lang/IllegalArgumentException;

    move-result-object p1

    throw p1
.end method

.method public getType()Ljava/lang/String;
    .locals 1

    const-string v0, "float"

    return-object v0
.end method

.method public getValue()F
    .locals 1

    iget v0, p0, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$FloatDecoder;->mValue:F

    return v0
.end method

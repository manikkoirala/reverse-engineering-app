.class final Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory$TokenEncoder;
.super Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory$ScalarEncoder;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "TokenEncoder"
.end annotation


# instance fields
.field _value:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory$ScalarEncoder;-><init>()V

    return-void
.end method


# virtual methods
.method public encodeMore([BII)I
    .locals 4

    .line 1
    iget-object p3, p0, Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory$TokenEncoder;->_value:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory$TokenEncoder;->_value:Ljava/lang/String;

    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    add-int/lit8 v2, p2, 0x1

    invoke-virtual {p3, v1}, Ljava/lang/String;->charAt(I)C

    move-result v3

    int-to-byte v3, v3

    aput-byte v3, p1, p2

    add-int/lit8 v1, v1, 0x1

    move p2, v2

    goto :goto_0

    :cond_0
    return p2
.end method

.method public encodeMore([CII)I
    .locals 2

    .line 2
    iget-object p3, p0, Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory$TokenEncoder;->_value:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory$TokenEncoder;->_value:Ljava/lang/String;

    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x0

    invoke-virtual {p3, v1, v0, p1, p2}, Ljava/lang/String;->getChars(II[CI)V

    add-int/2addr p2, v0

    return p2
.end method

.method public isCompleted()Z
    .locals 1

    iget-object v0, p0, Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory$TokenEncoder;->_value:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public reset(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lorg/codehaus/stax2/ri/typed/ValueEncoderFactory$TokenEncoder;->_value:Ljava/lang/String;

    return-void
.end method

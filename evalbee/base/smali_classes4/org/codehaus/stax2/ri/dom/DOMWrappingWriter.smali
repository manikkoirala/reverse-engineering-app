.class public abstract Lorg/codehaus/stax2/ri/dom/DOMWrappingWriter;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/codehaus/stax2/XMLStreamWriter2;


# static fields
.field static final DEFAULT_OUTPUT_ENCODING:Ljava/lang/String; = "UTF-8"

.field static final DEFAULT_XML_VERSION:Ljava/lang/String; = "1.0"


# instance fields
.field protected final mDocument:Lorg/w3c/dom/Document;

.field protected mEncoding:Ljava/lang/String;

.field protected final mNsAware:Z

.field protected mNsContext:Ljavax/xml/namespace/NamespaceContext;

.field protected final mNsRepairing:Z

.field protected mValueEncoder:Lorg/codehaus/stax2/ri/typed/SimpleValueEncoder;


# direct methods
.method public constructor <init>(Lorg/w3c/dom/Node;ZZ)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingWriter;->mEncoding:Ljava/lang/String;

    if-eqz p1, :cond_4

    iput-boolean p2, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingWriter;->mNsAware:Z

    iput-boolean p3, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingWriter;->mNsRepairing:Z

    invoke-interface {p1}, Lorg/w3c/dom/Node;->getNodeType()S

    move-result p2

    const/4 p3, 0x1

    if-eq p2, p3, :cond_2

    const/16 p3, 0x9

    if-eq p2, p3, :cond_1

    const/16 p3, 0xb

    if-ne p2, p3, :cond_0

    goto :goto_0

    :cond_0
    new-instance p2, Ljavax/xml/stream/XMLStreamException;

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Can not create an XMLStreamWriter for a DOM node of type "

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljavax/xml/stream/XMLStreamException;-><init>(Ljava/lang/String;)V

    throw p2

    :cond_1
    move-object p2, p1

    check-cast p2, Lorg/w3c/dom/Document;

    goto :goto_1

    :cond_2
    :goto_0
    invoke-interface {p1}, Lorg/w3c/dom/Node;->getOwnerDocument()Lorg/w3c/dom/Document;

    move-result-object p2

    :goto_1
    iput-object p2, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingWriter;->mDocument:Lorg/w3c/dom/Document;

    iget-object p2, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingWriter;->mDocument:Lorg/w3c/dom/Document;

    if-eqz p2, :cond_3

    return-void

    :cond_3
    new-instance p2, Ljavax/xml/stream/XMLStreamException;

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Can not create an XMLStreamWriter for given node (of type "

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p1, "): did not have owner document"

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljavax/xml/stream/XMLStreamException;-><init>(Ljava/lang/String;)V

    throw p2

    :cond_4
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Can not pass null Node for constructing a DOM-based XMLStreamWriter"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public static throwOutputError(Ljava/lang/String;)V
    .locals 1

    .line 1
    new-instance v0, Ljavax/xml/stream/XMLStreamException;

    invoke-direct {v0, p0}, Ljavax/xml/stream/XMLStreamException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static throwOutputError(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 0

    .line 2
    filled-new-array {p1}, [Ljava/lang/Object;

    move-result-object p1

    invoke-static {p0, p1}, Ljava/text/MessageFormat;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Lorg/codehaus/stax2/ri/dom/DOMWrappingWriter;->throwOutputError(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public abstract appendLeaf(Lorg/w3c/dom/Node;)V
.end method

.method public close()V
    .locals 0

    return-void
.end method

.method public closeCompletely()V
    .locals 0

    return-void
.end method

.method public copyEventFromReader(Lorg/codehaus/stax2/XMLStreamReader2;Z)V
    .locals 0

    return-void
.end method

.method public flush()V
    .locals 0

    return-void
.end method

.method public getEncoding()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingWriter;->mEncoding:Ljava/lang/String;

    return-object v0
.end method

.method public getLocation()Lorg/codehaus/stax2/XMLStreamLocation2;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public abstract getNamespaceContext()Ljavax/xml/namespace/NamespaceContext;
.end method

.method public abstract getPrefix(Ljava/lang/String;)Ljava/lang/String;
.end method

.method public abstract getProperty(Ljava/lang/String;)Ljava/lang/Object;
.end method

.method public getValueEncoder()Lorg/codehaus/stax2/ri/typed/SimpleValueEncoder;
    .locals 1

    iget-object v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingWriter;->mValueEncoder:Lorg/codehaus/stax2/ri/typed/SimpleValueEncoder;

    if-nez v0, :cond_0

    new-instance v0, Lorg/codehaus/stax2/ri/typed/SimpleValueEncoder;

    invoke-direct {v0}, Lorg/codehaus/stax2/ri/typed/SimpleValueEncoder;-><init>()V

    iput-object v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingWriter;->mValueEncoder:Lorg/codehaus/stax2/ri/typed/SimpleValueEncoder;

    :cond_0
    iget-object v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingWriter;->mValueEncoder:Lorg/codehaus/stax2/ri/typed/SimpleValueEncoder;

    return-object v0
.end method

.method public abstract isPropertySupported(Ljava/lang/String;)Z
.end method

.method public reportUnsupported(Ljava/lang/String;)V
    .locals 2

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, " can not be used with DOM-backed writer"

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public serializeQNameValue(Ljavax/xml/namespace/QName;)Ljava/lang/String;
    .locals 3

    iget-boolean v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingWriter;->mNsRepairing:Z

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Ljavax/xml/namespace/QName;->getNamespaceURI()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/dom/DOMWrappingWriter;->getNamespaceContext()Ljavax/xml/namespace/NamespaceContext;

    move-result-object v1

    if-nez v1, :cond_0

    const/4 v1, 0x0

    goto :goto_0

    :cond_0
    invoke-interface {v1, v0}, Ljavax/xml/namespace/NamespaceContext;->getPrefix(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :goto_0
    if-nez v1, :cond_4

    invoke-virtual {p1}, Ljavax/xml/namespace/QName;->getPrefix()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_1

    goto :goto_1

    :cond_1
    invoke-interface {p0, v1, v0}, Ljavax/xml/stream/XMLStreamWriter;->writeNamespace(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    :cond_2
    :goto_1
    invoke-virtual {p0, v0}, Lorg/codehaus/stax2/ri/dom/DOMWrappingWriter;->writeDefaultNamespace(Ljava/lang/String;)V

    const-string v1, ""

    goto :goto_2

    :cond_3
    invoke-virtual {p1}, Ljavax/xml/namespace/QName;->getPrefix()Ljava/lang/String;

    move-result-object v1

    :cond_4
    :goto_2
    invoke-virtual {p1}, Ljavax/xml/namespace/QName;->getLocalPart()Ljava/lang/String;

    move-result-object p1

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_5

    goto :goto_3

    :cond_5
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    :cond_6
    :goto_3
    return-object p1
.end method

.method public abstract setDefaultNamespace(Ljava/lang/String;)V
.end method

.method public setNamespaceContext(Ljavax/xml/namespace/NamespaceContext;)V
    .locals 0

    iput-object p1, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingWriter;->mNsContext:Ljavax/xml/namespace/NamespaceContext;

    return-void
.end method

.method public abstract setPrefix(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract setProperty(Ljava/lang/String;Ljava/lang/Object;)Z
.end method

.method public setValidationProblemHandler(Lorg/codehaus/stax2/validation/ValidationProblemHandler;)Lorg/codehaus/stax2/validation/ValidationProblemHandler;
    .locals 0

    const/4 p1, 0x0

    return-object p1
.end method

.method public stopValidatingAgainst(Lorg/codehaus/stax2/validation/XMLValidationSchema;)Lorg/codehaus/stax2/validation/XMLValidator;
    .locals 0

    .line 1
    const/4 p1, 0x0

    return-object p1
.end method

.method public stopValidatingAgainst(Lorg/codehaus/stax2/validation/XMLValidator;)Lorg/codehaus/stax2/validation/XMLValidator;
    .locals 0

    .line 2
    const/4 p1, 0x0

    return-object p1
.end method

.method public validateAgainst(Lorg/codehaus/stax2/validation/XMLValidationSchema;)Lorg/codehaus/stax2/validation/XMLValidator;
    .locals 0

    const/4 p1, 0x0

    return-object p1
.end method

.method public abstract writeAttribute(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract writeAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract writeAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public writeBinary(Lorg/codehaus/stax2/typed/Base64Variant;[BII)V
    .locals 1

    .line 1
    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/dom/DOMWrappingWriter;->getValueEncoder()Lorg/codehaus/stax2/ri/typed/SimpleValueEncoder;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3, p4}, Lorg/codehaus/stax2/ri/typed/SimpleValueEncoder;->encodeAsString(Lorg/codehaus/stax2/typed/Base64Variant;[BII)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lorg/codehaus/stax2/ri/dom/DOMWrappingWriter;->writeCharacters(Ljava/lang/String;)V

    return-void
.end method

.method public writeBinary([BII)V
    .locals 1

    .line 2
    invoke-static {}, Lorg/codehaus/stax2/typed/Base64Variants;->getDefaultVariant()Lorg/codehaus/stax2/typed/Base64Variant;

    move-result-object v0

    invoke-virtual {p0, v0, p1, p2, p3}, Lorg/codehaus/stax2/ri/dom/DOMWrappingWriter;->writeBinary(Lorg/codehaus/stax2/typed/Base64Variant;[BII)V

    return-void
.end method

.method public writeBinaryAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[B)V
    .locals 6

    .line 1
    invoke-static {}, Lorg/codehaus/stax2/typed/Base64Variants;->getDefaultVariant()Lorg/codehaus/stax2/typed/Base64Variant;

    move-result-object v1

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lorg/codehaus/stax2/ri/dom/DOMWrappingWriter;->writeBinaryAttribute(Lorg/codehaus/stax2/typed/Base64Variant;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[B)V

    return-void
.end method

.method public writeBinaryAttribute(Lorg/codehaus/stax2/typed/Base64Variant;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[B)V
    .locals 3

    .line 2
    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/dom/DOMWrappingWriter;->getValueEncoder()Lorg/codehaus/stax2/ri/typed/SimpleValueEncoder;

    move-result-object v0

    array-length v1, p5

    const/4 v2, 0x0

    invoke-virtual {v0, p1, p5, v2, v1}, Lorg/codehaus/stax2/ri/typed/SimpleValueEncoder;->encodeAsString(Lorg/codehaus/stax2/typed/Base64Variant;[BII)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p2, p3, p4, p1}, Lorg/codehaus/stax2/ri/dom/DOMWrappingWriter;->writeAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public writeBoolean(Z)V
    .locals 0

    if-eqz p1, :cond_0

    const-string p1, "true"

    goto :goto_0

    :cond_0
    const-string p1, "false"

    :goto_0
    invoke-virtual {p0, p1}, Lorg/codehaus/stax2/ri/dom/DOMWrappingWriter;->writeCharacters(Ljava/lang/String;)V

    return-void
.end method

.method public writeBooleanAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 0

    if-eqz p4, :cond_0

    const-string p4, "true"

    goto :goto_0

    :cond_0
    const-string p4, "false"

    :goto_0
    invoke-virtual {p0, p1, p2, p3, p4}, Lorg/codehaus/stax2/ri/dom/DOMWrappingWriter;->writeAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public writeCData(Ljava/lang/String;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingWriter;->mDocument:Lorg/w3c/dom/Document;

    invoke-interface {v0, p1}, Lorg/w3c/dom/Document;->createCDATASection(Ljava/lang/String;)Lorg/w3c/dom/CDATASection;

    move-result-object p1

    invoke-virtual {p0, p1}, Lorg/codehaus/stax2/ri/dom/DOMWrappingWriter;->appendLeaf(Lorg/w3c/dom/Node;)V

    return-void
.end method

.method public writeCData([CII)V
    .locals 1

    .line 2
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, p1, p2, p3}, Ljava/lang/String;-><init>([CII)V

    invoke-virtual {p0, v0}, Lorg/codehaus/stax2/ri/dom/DOMWrappingWriter;->writeCData(Ljava/lang/String;)V

    return-void
.end method

.method public writeCharacters(Ljava/lang/String;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingWriter;->mDocument:Lorg/w3c/dom/Document;

    invoke-interface {v0, p1}, Lorg/w3c/dom/Document;->createTextNode(Ljava/lang/String;)Lorg/w3c/dom/Text;

    move-result-object p1

    invoke-virtual {p0, p1}, Lorg/codehaus/stax2/ri/dom/DOMWrappingWriter;->appendLeaf(Lorg/w3c/dom/Node;)V

    return-void
.end method

.method public writeCharacters([CII)V
    .locals 1

    .line 2
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, p1, p2, p3}, Ljava/lang/String;-><init>([CII)V

    invoke-virtual {p0, v0}, Lorg/codehaus/stax2/ri/dom/DOMWrappingWriter;->writeCharacters(Ljava/lang/String;)V

    return-void
.end method

.method public writeComment(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingWriter;->mDocument:Lorg/w3c/dom/Document;

    invoke-interface {v0, p1}, Lorg/w3c/dom/Document;->createComment(Ljava/lang/String;)Lorg/w3c/dom/Comment;

    move-result-object p1

    invoke-virtual {p0, p1}, Lorg/codehaus/stax2/ri/dom/DOMWrappingWriter;->appendLeaf(Lorg/w3c/dom/Node;)V

    return-void
.end method

.method public writeDTD(Ljava/lang/String;)V
    .locals 0

    .line 1
    const-string p1, "writeDTD()"

    invoke-virtual {p0, p1}, Lorg/codehaus/stax2/ri/dom/DOMWrappingWriter;->reportUnsupported(Ljava/lang/String;)V

    return-void
.end method

.method public abstract writeDTD(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public writeDecimal(Ljava/math/BigDecimal;)V
    .locals 0

    invoke-virtual {p1}, Ljava/math/BigDecimal;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lorg/codehaus/stax2/ri/dom/DOMWrappingWriter;->writeCharacters(Ljava/lang/String;)V

    return-void
.end method

.method public writeDecimalAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/math/BigDecimal;)V
    .locals 0

    invoke-virtual {p4}, Ljava/math/BigDecimal;->toString()Ljava/lang/String;

    move-result-object p4

    invoke-virtual {p0, p1, p2, p3, p4}, Lorg/codehaus/stax2/ri/dom/DOMWrappingWriter;->writeAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public abstract writeDefaultNamespace(Ljava/lang/String;)V
.end method

.method public writeDouble(D)V
    .locals 0

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lorg/codehaus/stax2/ri/dom/DOMWrappingWriter;->writeCharacters(Ljava/lang/String;)V

    return-void
.end method

.method public writeDoubleArray([DII)V
    .locals 1

    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/dom/DOMWrappingWriter;->getValueEncoder()Lorg/codehaus/stax2/ri/typed/SimpleValueEncoder;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lorg/codehaus/stax2/ri/typed/SimpleValueEncoder;->encodeAsString([DII)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lorg/codehaus/stax2/ri/dom/DOMWrappingWriter;->writeCharacters(Ljava/lang/String;)V

    return-void
.end method

.method public writeDoubleArrayAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[D)V
    .locals 3

    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/dom/DOMWrappingWriter;->getValueEncoder()Lorg/codehaus/stax2/ri/typed/SimpleValueEncoder;

    move-result-object v0

    array-length v1, p4

    const/4 v2, 0x0

    invoke-virtual {v0, p4, v2, v1}, Lorg/codehaus/stax2/ri/typed/SimpleValueEncoder;->encodeAsString([DII)Ljava/lang/String;

    move-result-object p4

    invoke-virtual {p0, p1, p2, p3, p4}, Lorg/codehaus/stax2/ri/dom/DOMWrappingWriter;->writeAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public writeDoubleAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;D)V
    .locals 0

    invoke-static {p4, p5}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object p4

    invoke-virtual {p0, p1, p2, p3, p4}, Lorg/codehaus/stax2/ri/dom/DOMWrappingWriter;->writeAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public abstract writeEmptyElement(Ljava/lang/String;)V
.end method

.method public abstract writeEmptyElement(Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract writeEmptyElement(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract writeEndDocument()V
.end method

.method public writeEntityRef(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingWriter;->mDocument:Lorg/w3c/dom/Document;

    invoke-interface {v0, p1}, Lorg/w3c/dom/Document;->createEntityReference(Ljava/lang/String;)Lorg/w3c/dom/EntityReference;

    move-result-object p1

    invoke-virtual {p0, p1}, Lorg/codehaus/stax2/ri/dom/DOMWrappingWriter;->appendLeaf(Lorg/w3c/dom/Node;)V

    return-void
.end method

.method public writeFloat(F)V
    .locals 0

    invoke-static {p1}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lorg/codehaus/stax2/ri/dom/DOMWrappingWriter;->writeCharacters(Ljava/lang/String;)V

    return-void
.end method

.method public writeFloatArray([FII)V
    .locals 1

    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/dom/DOMWrappingWriter;->getValueEncoder()Lorg/codehaus/stax2/ri/typed/SimpleValueEncoder;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lorg/codehaus/stax2/ri/typed/SimpleValueEncoder;->encodeAsString([FII)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lorg/codehaus/stax2/ri/dom/DOMWrappingWriter;->writeCharacters(Ljava/lang/String;)V

    return-void
.end method

.method public writeFloatArrayAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[F)V
    .locals 3

    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/dom/DOMWrappingWriter;->getValueEncoder()Lorg/codehaus/stax2/ri/typed/SimpleValueEncoder;

    move-result-object v0

    array-length v1, p4

    const/4 v2, 0x0

    invoke-virtual {v0, p4, v2, v1}, Lorg/codehaus/stax2/ri/typed/SimpleValueEncoder;->encodeAsString([FII)Ljava/lang/String;

    move-result-object p4

    invoke-virtual {p0, p1, p2, p3, p4}, Lorg/codehaus/stax2/ri/dom/DOMWrappingWriter;->writeAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public writeFloatAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;F)V
    .locals 0

    invoke-static {p4}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object p4

    invoke-virtual {p0, p1, p2, p3, p4}, Lorg/codehaus/stax2/ri/dom/DOMWrappingWriter;->writeAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public writeFullEndElement()V
    .locals 0

    invoke-interface {p0}, Ljavax/xml/stream/XMLStreamWriter;->writeEndElement()V

    return-void
.end method

.method public writeInt(I)V
    .locals 0

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lorg/codehaus/stax2/ri/dom/DOMWrappingWriter;->writeCharacters(Ljava/lang/String;)V

    return-void
.end method

.method public writeIntArray([III)V
    .locals 1

    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/dom/DOMWrappingWriter;->getValueEncoder()Lorg/codehaus/stax2/ri/typed/SimpleValueEncoder;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lorg/codehaus/stax2/ri/typed/SimpleValueEncoder;->encodeAsString([III)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lorg/codehaus/stax2/ri/dom/DOMWrappingWriter;->writeCharacters(Ljava/lang/String;)V

    return-void
.end method

.method public writeIntArrayAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[I)V
    .locals 3

    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/dom/DOMWrappingWriter;->getValueEncoder()Lorg/codehaus/stax2/ri/typed/SimpleValueEncoder;

    move-result-object v0

    array-length v1, p4

    const/4 v2, 0x0

    invoke-virtual {v0, p4, v2, v1}, Lorg/codehaus/stax2/ri/typed/SimpleValueEncoder;->encodeAsString([III)Ljava/lang/String;

    move-result-object p4

    invoke-virtual {p0, p1, p2, p3, p4}, Lorg/codehaus/stax2/ri/dom/DOMWrappingWriter;->writeAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public writeIntAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 0

    invoke-static {p4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p4

    invoke-virtual {p0, p1, p2, p3, p4}, Lorg/codehaus/stax2/ri/dom/DOMWrappingWriter;->writeAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public writeInteger(Ljava/math/BigInteger;)V
    .locals 0

    invoke-virtual {p1}, Ljava/math/BigInteger;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lorg/codehaus/stax2/ri/dom/DOMWrappingWriter;->writeCharacters(Ljava/lang/String;)V

    return-void
.end method

.method public writeIntegerAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/math/BigInteger;)V
    .locals 0

    invoke-virtual {p4}, Ljava/math/BigInteger;->toString()Ljava/lang/String;

    move-result-object p4

    invoke-virtual {p0, p1, p2, p3, p4}, Lorg/codehaus/stax2/ri/dom/DOMWrappingWriter;->writeAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public writeLong(J)V
    .locals 0

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lorg/codehaus/stax2/ri/dom/DOMWrappingWriter;->writeCharacters(Ljava/lang/String;)V

    return-void
.end method

.method public writeLongArray([JII)V
    .locals 1

    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/dom/DOMWrappingWriter;->getValueEncoder()Lorg/codehaus/stax2/ri/typed/SimpleValueEncoder;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lorg/codehaus/stax2/ri/typed/SimpleValueEncoder;->encodeAsString([JII)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lorg/codehaus/stax2/ri/dom/DOMWrappingWriter;->writeCharacters(Ljava/lang/String;)V

    return-void
.end method

.method public writeLongArrayAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[J)V
    .locals 3

    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/dom/DOMWrappingWriter;->getValueEncoder()Lorg/codehaus/stax2/ri/typed/SimpleValueEncoder;

    move-result-object v0

    array-length v1, p4

    const/4 v2, 0x0

    invoke-virtual {v0, p4, v2, v1}, Lorg/codehaus/stax2/ri/typed/SimpleValueEncoder;->encodeAsString([JII)Ljava/lang/String;

    move-result-object p4

    invoke-virtual {p0, p1, p2, p3, p4}, Lorg/codehaus/stax2/ri/dom/DOMWrappingWriter;->writeAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public writeLongAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V
    .locals 0

    invoke-static {p4, p5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object p4

    invoke-virtual {p0, p1, p2, p3, p4}, Lorg/codehaus/stax2/ri/dom/DOMWrappingWriter;->writeAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public writeProcessingInstruction(Ljava/lang/String;)V
    .locals 1

    .line 1
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lorg/codehaus/stax2/ri/dom/DOMWrappingWriter;->writeProcessingInstruction(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public writeProcessingInstruction(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .line 2
    iget-object v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingWriter;->mDocument:Lorg/w3c/dom/Document;

    invoke-interface {v0, p1, p2}, Lorg/w3c/dom/Document;->createProcessingInstruction(Ljava/lang/String;Ljava/lang/String;)Lorg/w3c/dom/ProcessingInstruction;

    move-result-object p1

    invoke-virtual {p0, p1}, Lorg/codehaus/stax2/ri/dom/DOMWrappingWriter;->appendLeaf(Lorg/w3c/dom/Node;)V

    return-void
.end method

.method public writeQName(Ljavax/xml/namespace/QName;)V
    .locals 0

    invoke-virtual {p0, p1}, Lorg/codehaus/stax2/ri/dom/DOMWrappingWriter;->serializeQNameValue(Ljavax/xml/namespace/QName;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lorg/codehaus/stax2/ri/dom/DOMWrappingWriter;->writeCharacters(Ljava/lang/String;)V

    return-void
.end method

.method public writeQNameAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljavax/xml/namespace/QName;)V
    .locals 0

    invoke-virtual {p0, p4}, Lorg/codehaus/stax2/ri/dom/DOMWrappingWriter;->serializeQNameValue(Ljavax/xml/namespace/QName;)Ljava/lang/String;

    move-result-object p4

    invoke-virtual {p0, p1, p2, p3, p4}, Lorg/codehaus/stax2/ri/dom/DOMWrappingWriter;->writeAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public writeRaw(Ljava/lang/String;)V
    .locals 0

    .line 1
    const-string p1, "writeRaw()"

    invoke-virtual {p0, p1}, Lorg/codehaus/stax2/ri/dom/DOMWrappingWriter;->reportUnsupported(Ljava/lang/String;)V

    return-void
.end method

.method public writeRaw(Ljava/lang/String;II)V
    .locals 0

    .line 2
    const-string p1, "writeRaw()"

    invoke-virtual {p0, p1}, Lorg/codehaus/stax2/ri/dom/DOMWrappingWriter;->reportUnsupported(Ljava/lang/String;)V

    return-void
.end method

.method public writeRaw([CII)V
    .locals 0

    .line 3
    const-string p1, "writeRaw()"

    invoke-virtual {p0, p1}, Lorg/codehaus/stax2/ri/dom/DOMWrappingWriter;->reportUnsupported(Ljava/lang/String;)V

    return-void
.end method

.method public writeSpace(Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lorg/codehaus/stax2/ri/dom/DOMWrappingWriter;->writeCharacters(Ljava/lang/String;)V

    return-void
.end method

.method public writeSpace([CII)V
    .locals 1

    .line 2
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, p1, p2, p3}, Ljava/lang/String;-><init>([CII)V

    invoke-virtual {p0, v0}, Lorg/codehaus/stax2/ri/dom/DOMWrappingWriter;->writeSpace(Ljava/lang/String;)V

    return-void
.end method

.method public writeStartDocument()V
    .locals 2

    .line 1
    const-string v0, "UTF-8"

    const-string v1, "1.0"

    invoke-virtual {p0, v0, v1}, Lorg/codehaus/stax2/ri/dom/DOMWrappingWriter;->writeStartDocument(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public writeStartDocument(Ljava/lang/String;)V
    .locals 1

    .line 2
    const/4 v0, 0x0

    invoke-virtual {p0, v0, p1}, Lorg/codehaus/stax2/ri/dom/DOMWrappingWriter;->writeStartDocument(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public writeStartDocument(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 3
    iput-object p1, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingWriter;->mEncoding:Ljava/lang/String;

    return-void
.end method

.method public writeStartDocument(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 0

    .line 4
    invoke-virtual {p0, p2, p1}, Lorg/codehaus/stax2/ri/dom/DOMWrappingWriter;->writeStartDocument(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

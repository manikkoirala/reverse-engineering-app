.class public abstract Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/codehaus/stax2/XMLStreamReader2;
.implements Lorg/codehaus/stax2/AttributeInfo;
.implements Lorg/codehaus/stax2/DTDInfo;
.implements Lorg/codehaus/stax2/LocationInfo;
.implements Ljavax/xml/namespace/NamespaceContext;
.implements Ljavax/xml/stream/XMLStreamConstants;


# static fields
.field protected static final ERR_STATE_NOT_ELEM:I = 0x2

.field protected static final ERR_STATE_NOT_PI:I = 0x3

.field protected static final ERR_STATE_NOT_START_ELEM:I = 0x1

.field protected static final ERR_STATE_NOT_TEXTUAL:I = 0x4

.field protected static final ERR_STATE_NOT_TEXTUAL_OR_ELEM:I = 0x6

.field protected static final ERR_STATE_NOT_TEXTUAL_XXX:I = 0x5

.field protected static final ERR_STATE_NO_LOCALNAME:I = 0x7

.field protected static final INT_SPACE:I = 0x20

.field private static final MASK_GET_ELEMENT_TEXT:I = 0x1250

.field private static final MASK_GET_TEXT:I = 0x1a70

.field private static final MASK_GET_TEXT_XXX:I = 0x1070

.field protected static final MASK_TYPED_ACCESS_BINARY:I = 0x1052


# instance fields
.field protected _attrList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lorg/w3c/dom/Node;",
            ">;"
        }
    .end annotation
.end field

.field protected _base64Decoder:Lorg/codehaus/stax2/ri/typed/StringBase64Decoder;

.field protected _cfgInternNames:Z

.field protected _cfgInternNsURIs:Z

.field protected final _cfgNsAware:Z

.field protected _coalescedText:Ljava/lang/String;

.field protected final _coalescing:Z

.field protected _currEvent:I

.field protected _currNode:Lorg/w3c/dom/Node;

.field protected _decoderFactory:Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;

.field protected _depth:I

.field protected _nsDeclList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected final _rootNode:Lorg/w3c/dom/Node;

.field protected final _systemId:Ljava/lang/String;

.field protected _textBuffer:Lorg/codehaus/stax2/ri/Stax2Util$TextBuffer;


# direct methods
.method public constructor <init>(Ljavax/xml/transform/dom/DOMSource;ZZ)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_cfgInternNames:Z

    iput-boolean v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_cfgInternNsURIs:Z

    const/4 v1, 0x7

    iput v1, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_currEvent:I

    iput v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_depth:I

    new-instance v0, Lorg/codehaus/stax2/ri/Stax2Util$TextBuffer;

    invoke-direct {v0}, Lorg/codehaus/stax2/ri/Stax2Util$TextBuffer;-><init>()V

    iput-object v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_textBuffer:Lorg/codehaus/stax2/ri/Stax2Util$TextBuffer;

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_attrList:Ljava/util/List;

    iput-object v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_nsDeclList:Ljava/util/List;

    iput-object v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_base64Decoder:Lorg/codehaus/stax2/ri/typed/StringBase64Decoder;

    invoke-virtual {p1}, Ljavax/xml/transform/dom/DOMSource;->getNode()Lorg/w3c/dom/Node;

    move-result-object v0

    if-eqz v0, :cond_2

    iput-boolean p2, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_cfgNsAware:Z

    iput-boolean p3, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_coalescing:Z

    invoke-virtual {p1}, Ljavax/xml/transform/dom/DOMSource;->getSystemId()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_systemId:Ljava/lang/String;

    invoke-interface {v0}, Lorg/w3c/dom/Node;->getNodeType()S

    move-result p1

    const/4 p2, 0x1

    if-eq p1, p2, :cond_1

    const/16 p2, 0x9

    if-eq p1, p2, :cond_1

    const/16 p2, 0xb

    if-ne p1, p2, :cond_0

    goto :goto_0

    :cond_0
    new-instance p1, Ljavax/xml/stream/XMLStreamException;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string p3, "Can not create an XMLStreamReader for a DOM node of type "

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p3

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljavax/xml/stream/XMLStreamException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    :goto_0
    iput-object v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_currNode:Lorg/w3c/dom/Node;

    iput-object v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_rootNode:Lorg/w3c/dom/Node;

    return-void

    :cond_2
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Can not pass null Node for constructing a DOM-based XMLStreamReader"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private _calcNsAndAttrLists(Z)V
    .locals 10

    iget-object v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_currNode:Lorg/w3c/dom/Node;

    invoke-interface {v0}, Lorg/w3c/dom/Node;->getAttributes()Lorg/w3c/dom/NamedNodeMap;

    move-result-object v0

    invoke-interface {v0}, Lorg/w3c/dom/NamedNodeMap;->getLength()I

    move-result v1

    if-nez v1, :cond_1

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_attrList:Ljava/util/List;

    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_nsDeclList:Ljava/util/List;

    return-void

    :cond_1
    iget-boolean v2, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_cfgNsAware:Z

    const/4 v3, 0x0

    if-nez v2, :cond_2

    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object p1, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_attrList:Ljava/util/List;

    :goto_0
    if-ge v3, v1, :cond_0

    iget-object p1, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_attrList:Ljava/util/List;

    invoke-interface {v0, v3}, Lorg/w3c/dom/NamedNodeMap;->item(I)Lorg/w3c/dom/Node;

    move-result-object v2

    invoke-interface {p1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_2
    const/4 v2, 0x0

    move-object v4, v2

    move-object v5, v4

    :goto_1
    if-ge v3, v1, :cond_a

    invoke-interface {v0, v3}, Lorg/w3c/dom/NamedNodeMap;->item(I)Lorg/w3c/dom/Node;

    move-result-object v6

    invoke-interface {v6}, Lorg/w3c/dom/Node;->getPrefix()Ljava/lang/String;

    move-result-object v7

    const-string v8, "xmlns"

    if-eqz v7, :cond_5

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v9

    if-nez v9, :cond_3

    goto :goto_2

    :cond_3
    invoke-virtual {v8, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_4

    if-eqz p1, :cond_9

    if-nez v4, :cond_6

    new-instance v4, Ljava/util/ArrayList;

    sub-int v7, v1, v3

    invoke-direct {v4, v7}, Ljava/util/ArrayList;-><init>(I)V

    goto :goto_3

    :cond_4
    invoke-interface {v6}, Lorg/w3c/dom/Node;->getLocalName()Ljava/lang/String;

    move-result-object v7

    goto :goto_4

    :cond_5
    :goto_2
    invoke-interface {v6}, Lorg/w3c/dom/Node;->getLocalName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v8, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_7

    if-eqz p1, :cond_9

    if-nez v4, :cond_6

    new-instance v4, Ljava/util/ArrayList;

    sub-int v7, v1, v3

    invoke-direct {v4, v7}, Ljava/util/ArrayList;-><init>(I)V

    :cond_6
    :goto_3
    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_5

    :cond_7
    move-object v7, v2

    :goto_4
    if-nez v5, :cond_8

    new-instance v5, Ljava/util/ArrayList;

    sub-int v8, v1, v3

    mul-int/lit8 v8, v8, 0x2

    invoke-direct {v5, v8}, Ljava/util/ArrayList;-><init>(I)V

    :cond_8
    invoke-virtual {p0, v7}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_internName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-interface {v6}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_internNsURI(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_9
    :goto_5
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_a
    if-nez v4, :cond_b

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v4

    :cond_b
    iput-object v4, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_attrList:Ljava/util/List;

    if-nez v5, :cond_c

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v5

    :cond_c
    iput-object v5, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_nsDeclList:Ljava/util/List;

    return-void
.end method

.method private _constructQName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljavax/xml/namespace/QName;
    .locals 1

    new-instance v0, Ljavax/xml/namespace/QName;

    invoke-virtual {p0, p1}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_internNsURI(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p2}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_internName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p0, p3}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_internName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p3

    invoke-direct {v0, p1, p2, p3}, Ljavax/xml/namespace/QName;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method private _safeGetLocalName(Lorg/w3c/dom/Node;)Ljava/lang/String;
    .locals 1

    invoke-interface {p1}, Lorg/w3c/dom/Node;->getLocalName()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-interface {p1}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method private final checkExpand(Lorg/codehaus/stax2/typed/TypedArrayDecoder;)Z
    .locals 1

    instance-of v0, p1, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$BaseArrayDecoder;

    if-eqz v0, :cond_0

    check-cast p1, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$BaseArrayDecoder;

    invoke-virtual {p1}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$BaseArrayDecoder;->expand()V

    const/4 p1, 0x1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method private coalesceTypedText(Lorg/w3c/dom/Node;)Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_textBuffer:Lorg/codehaus/stax2/ri/Stax2Util$TextBuffer;

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/Stax2Util$TextBuffer;->reset()V

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_attrList:Ljava/util/List;

    :goto_0
    if-eqz p1, :cond_3

    invoke-interface {p1}, Lorg/w3c/dom/Node;->getNodeType()S

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_1

    const/4 v1, 0x4

    if-eq v0, v1, :cond_1

    const/4 v1, 0x7

    if-eq v0, v1, :cond_2

    const/16 v1, 0x8

    if-eq v0, v1, :cond_2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unexpected DOM node type ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {p1}, Lorg/w3c/dom/Node;->getNodeType()S

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ") when trying to decode Typed content"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->throwStreamException(Ljava/lang/String;)V

    goto :goto_1

    :cond_0
    const-string v0, "Element content can not contain child START_ELEMENT when using Typed Access methods"

    invoke-virtual {p0, v0}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->throwStreamException(Ljava/lang/String;)V

    :cond_1
    iget-object v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_textBuffer:Lorg/codehaus/stax2/ri/Stax2Util$TextBuffer;

    invoke-interface {p1}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/codehaus/stax2/ri/Stax2Util$TextBuffer;->append(Ljava/lang/String;)V

    :cond_2
    :goto_1
    invoke-interface {p1}, Lorg/w3c/dom/Node;->getNextSibling()Lorg/w3c/dom/Node;

    move-result-object p1

    goto :goto_0

    :cond_3
    iget-object p1, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_textBuffer:Lorg/codehaus/stax2/ri/Stax2Util$TextBuffer;

    invoke-virtual {p1}, Lorg/codehaus/stax2/ri/Stax2Util$TextBuffer;->get()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private handleIllegalAttrIndex(I)V
    .locals 4

    iget-object v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_currNode:Lorg/w3c/dom/Node;

    check-cast v0, Lorg/w3c/dom/Element;

    invoke-interface {v0}, Lorg/w3c/dom/Node;->getAttributes()Lorg/w3c/dom/NamedNodeMap;

    move-result-object v1

    invoke-interface {v1}, Lorg/w3c/dom/NamedNodeMap;->getLength()I

    move-result v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Illegal attribute index "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, "; element <"

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {v0}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "> has "

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-nez v1, :cond_0

    const-string p1, "no"

    goto :goto_0

    :cond_0
    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p1

    :goto_0
    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, " attributes"

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private handleIllegalNsIndex(I)V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Illegal namespace declaration index "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, " (has "

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->getNamespaceCount()I

    move-result p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, " ns declarations)"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public _base64Decoder()Lorg/codehaus/stax2/ri/typed/StringBase64Decoder;
    .locals 1

    iget-object v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_base64Decoder:Lorg/codehaus/stax2/ri/typed/StringBase64Decoder;

    if-nez v0, :cond_0

    new-instance v0, Lorg/codehaus/stax2/ri/typed/StringBase64Decoder;

    invoke-direct {v0}, Lorg/codehaus/stax2/ri/typed/StringBase64Decoder;-><init>()V

    iput-object v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_base64Decoder:Lorg/codehaus/stax2/ri/typed/StringBase64Decoder;

    :cond_0
    iget-object v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_base64Decoder:Lorg/codehaus/stax2/ri/typed/StringBase64Decoder;

    return-object v0
.end method

.method public _constructTypeException(Ljava/lang/IllegalArgumentException;Ljava/lang/String;)Lorg/codehaus/stax2/typed/TypedXMLStreamException;
    .locals 2

    .line 1
    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, ""

    :cond_0
    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->getStartLocation()Lorg/codehaus/stax2/XMLStreamLocation2;

    move-result-object v1

    if-nez v1, :cond_1

    new-instance v1, Lorg/codehaus/stax2/typed/TypedXMLStreamException;

    invoke-direct {v1, p2, v0, p1}, Lorg/codehaus/stax2/typed/TypedXMLStreamException;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/IllegalArgumentException;)V

    return-object v1

    :cond_1
    new-instance p1, Lorg/codehaus/stax2/typed/TypedXMLStreamException;

    invoke-direct {p1, p2, v0, v1}, Lorg/codehaus/stax2/typed/TypedXMLStreamException;-><init>(Ljava/lang/String;Ljava/lang/String;Ljavax/xml/stream/Location;)V

    return-object p1
.end method

.method public _constructTypeException(Ljava/lang/String;Ljava/lang/String;)Lorg/codehaus/stax2/typed/TypedXMLStreamException;
    .locals 2

    .line 2
    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->getStartLocation()Lorg/codehaus/stax2/XMLStreamLocation2;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Lorg/codehaus/stax2/typed/TypedXMLStreamException;

    invoke-direct {v0, p2, p1}, Lorg/codehaus/stax2/typed/TypedXMLStreamException;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0

    :cond_0
    new-instance v1, Lorg/codehaus/stax2/typed/TypedXMLStreamException;

    invoke-direct {v1, p2, p1, v0}, Lorg/codehaus/stax2/typed/TypedXMLStreamException;-><init>(Ljava/lang/String;Ljava/lang/String;Ljavax/xml/stream/Location;)V

    return-object v1
.end method

.method public _decoderFactory()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;
    .locals 1

    iget-object v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_decoderFactory:Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;

    if-nez v0, :cond_0

    new-instance v0, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;

    invoke-direct {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;-><init>()V

    iput-object v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_decoderFactory:Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;

    :cond_0
    iget-object v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_decoderFactory:Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;

    return-object v0
.end method

.method public _getAttributeAsArray(Lorg/codehaus/stax2/typed/TypedArrayDecoder;Ljava/lang/String;)I
    .locals 7

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    move-object v3, v2

    move v2, v1

    :goto_0
    if-ge v1, v0, :cond_4

    :cond_0
    :try_start_0
    invoke-virtual {p2, v1}, Ljava/lang/String;->charAt(I)C

    move-result v4

    const/16 v5, 0x20

    if-gt v4, v5, :cond_1

    add-int/lit8 v1, v1, 0x1

    if-lt v1, v0, :cond_0

    goto :goto_2

    :cond_1
    add-int/lit8 v4, v1, 0x1

    :goto_1
    if-ge v4, v0, :cond_2

    invoke-virtual {p2, v4}, Ljava/lang/String;->charAt(I)C

    move-result v6

    if-le v6, v5, :cond_2

    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :cond_2
    add-int/lit8 v5, v4, 0x1

    invoke-virtual {p2, v1, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {p1, v3}, Lorg/codehaus/stax2/typed/TypedArrayDecoder;->decodeValue(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-direct {p0, p1}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->checkExpand(Lorg/codehaus/stax2/typed/TypedArrayDecoder;)Z

    move-result v1
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    if-nez v1, :cond_3

    goto :goto_2

    :cond_3
    move v1, v5

    goto :goto_0

    :catch_0
    move-exception p1

    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->getLocation()Ljavax/xml/stream/Location;

    move-result-object p2

    new-instance v0, Lorg/codehaus/stax2/typed/TypedXMLStreamException;

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v3, v1, p2, p1}, Lorg/codehaus/stax2/typed/TypedXMLStreamException;-><init>(Ljava/lang/String;Ljava/lang/String;Ljavax/xml/stream/Location;Ljava/lang/IllegalArgumentException;)V

    throw v0

    :cond_4
    :goto_2
    return v2
.end method

.method public _internName(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    if-nez p1, :cond_0

    const-string p1, ""

    return-object p1

    :cond_0
    iget-boolean v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_cfgInternNames:Z

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object p1

    :cond_1
    return-object p1
.end method

.method public _internNsURI(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    if-nez p1, :cond_0

    const-string p1, ""

    return-object p1

    :cond_0
    iget-boolean v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_cfgInternNsURIs:Z

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Ljava/lang/String;->intern()Ljava/lang/String;

    move-result-object p1

    :cond_1
    return-object p1
.end method

.method public close()V
    .locals 0

    return-void
.end method

.method public closeCompletely()V
    .locals 0

    return-void
.end method

.method public coalesceText(I)V
    .locals 3

    iget-object p1, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_textBuffer:Lorg/codehaus/stax2/ri/Stax2Util$TextBuffer;

    invoke-virtual {p1}, Lorg/codehaus/stax2/ri/Stax2Util$TextBuffer;->reset()V

    iget-object p1, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_textBuffer:Lorg/codehaus/stax2/ri/Stax2Util$TextBuffer;

    iget-object v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_currNode:Lorg/w3c/dom/Node;

    invoke-interface {v0}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lorg/codehaus/stax2/ri/Stax2Util$TextBuffer;->append(Ljava/lang/String;)V

    :goto_0
    iget-object p1, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_currNode:Lorg/w3c/dom/Node;

    invoke-interface {p1}, Lorg/w3c/dom/Node;->getNextSibling()Lorg/w3c/dom/Node;

    move-result-object p1

    const/4 v0, 0x4

    if-eqz p1, :cond_1

    invoke-interface {p1}, Lorg/w3c/dom/Node;->getNodeType()S

    move-result v1

    const/4 v2, 0x3

    if-eq v1, v2, :cond_0

    if-eq v1, v0, :cond_0

    goto :goto_1

    :cond_0
    iput-object p1, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_currNode:Lorg/w3c/dom/Node;

    iget-object v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_textBuffer:Lorg/codehaus/stax2/ri/Stax2Util$TextBuffer;

    invoke-interface {p1}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lorg/codehaus/stax2/ri/Stax2Util$TextBuffer;->append(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    :goto_1
    iget-object p1, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_textBuffer:Lorg/codehaus/stax2/ri/Stax2Util$TextBuffer;

    invoke-virtual {p1}, Lorg/codehaus/stax2/ri/Stax2Util$TextBuffer;->get()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_coalescedText:Ljava/lang/String;

    iput v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_currEvent:I

    return-void
.end method

.method public findAttributeIndex(Ljava/lang/String;Ljava/lang/String;)I
    .locals 7

    iget v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_currEvent:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    invoke-virtual {p0, v1}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->reportWrongState(I)V

    :cond_0
    iget-object v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_currNode:Lorg/w3c/dom/Node;

    check-cast v0, Lorg/w3c/dom/Element;

    invoke-interface {v0}, Lorg/w3c/dom/Node;->getAttributes()Lorg/w3c/dom/NamedNodeMap;

    move-result-object v0

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_1

    const/4 p1, 0x0

    :cond_1
    invoke-interface {v0}, Lorg/w3c/dom/NamedNodeMap;->getLength()I

    move-result v2

    const/4 v3, 0x0

    move v4, v3

    :goto_0
    if-ge v4, v2, :cond_6

    invoke-interface {v0, v4}, Lorg/w3c/dom/NamedNodeMap;->item(I)Lorg/w3c/dom/Node;

    move-result-object v5

    invoke-direct {p0, v5}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_safeGetLocalName(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_5

    invoke-interface {v5}, Lorg/w3c/dom/Node;->getNamespaceURI()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_3

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v6

    if-nez v6, :cond_2

    goto :goto_1

    :cond_2
    move v6, v3

    goto :goto_2

    :cond_3
    :goto_1
    move v6, v1

    :goto_2
    if-nez p1, :cond_4

    if-eqz v6, :cond_5

    return v4

    :cond_4
    if-nez v6, :cond_5

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    return v4

    :cond_5
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_6
    const/4 p1, -0x1

    return p1
.end method

.method public findErrorDesc(II)Ljava/lang/String;
    .locals 2

    invoke-static {p2}, Lorg/codehaus/stax2/ri/Stax2Util;->eventTypeDesc(I)Ljava/lang/String;

    move-result-object p2

    const-string v0, "Current event "

    const-string v1, "Current event ("

    packed-switch p1, :pswitch_data_0

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Internal error (unrecognized error type: "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, ")"

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1

    :pswitch_0
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, ") has no local name"

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1

    :pswitch_1
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, " not START_ELEMENT, END_ELEMENT, CHARACTERS or CDATA"

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1

    :pswitch_2
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, ", needs to be one of CHARACTERS, CDATA, SPACE or COMMENT"

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1

    :pswitch_3
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, ") not a textual event"

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1

    :pswitch_4
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, ") needs to be PROCESSING_INSTRUCTION"

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1

    :pswitch_5
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, ", needs to be START_ELEMENT or END_ELEMENT"

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1

    :pswitch_6
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, ", needs to be START_ELEMENT"

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final getAttributeAs(ILorg/codehaus/stax2/typed/TypedValueDecoder;)V
    .locals 0

    invoke-virtual {p0, p1}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->getAttributeValue(I)Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lorg/codehaus/stax2/ri/Stax2Util;->trimSpaces(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    if-nez p1, :cond_0

    :try_start_0
    invoke-virtual {p2}, Lorg/codehaus/stax2/typed/TypedValueDecoder;->handleEmptyValue()V

    goto :goto_0

    :cond_0
    invoke-virtual {p2, p1}, Lorg/codehaus/stax2/typed/TypedValueDecoder;->decode(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception p2

    invoke-virtual {p0, p2, p1}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_constructTypeException(Ljava/lang/IllegalArgumentException;Ljava/lang/String;)Lorg/codehaus/stax2/typed/TypedXMLStreamException;

    move-result-object p1

    throw p1
.end method

.method public getAttributeAsArray(ILorg/codehaus/stax2/typed/TypedArrayDecoder;)I
    .locals 0

    invoke-virtual {p0, p1}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->getAttributeValue(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p2, p1}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_getAttributeAsArray(Lorg/codehaus/stax2/typed/TypedArrayDecoder;Ljava/lang/String;)I

    move-result p1

    return p1
.end method

.method public getAttributeAsBinary(I)[B
    .locals 1

    .line 1
    invoke-static {}, Lorg/codehaus/stax2/typed/Base64Variants;->getDefaultVariant()Lorg/codehaus/stax2/typed/Base64Variant;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->getAttributeAsBinary(ILorg/codehaus/stax2/typed/Base64Variant;)[B

    move-result-object p1

    return-object p1
.end method

.method public getAttributeAsBinary(ILorg/codehaus/stax2/typed/Base64Variant;)[B
    .locals 2

    .line 2
    invoke-virtual {p0, p1}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->getAttributeValue(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_base64Decoder()Lorg/codehaus/stax2/ri/typed/StringBase64Decoder;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, p2, v1, p1}, Lorg/codehaus/stax2/ri/typed/StringBase64Decoder;->init(Lorg/codehaus/stax2/typed/Base64Variant;ZLjava/lang/String;)V

    :try_start_0
    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/StringBase64Decoder;->decodeCompletely()[B

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :catch_0
    move-exception p2

    invoke-virtual {p0, p2, p1}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_constructTypeException(Ljava/lang/IllegalArgumentException;Ljava/lang/String;)Lorg/codehaus/stax2/typed/TypedXMLStreamException;

    move-result-object p1

    throw p1
.end method

.method public getAttributeAsBoolean(I)Z
    .locals 1

    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_decoderFactory()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;

    move-result-object v0

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;->getBooleanDecoder()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$BooleanDecoder;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->getAttributeAs(ILorg/codehaus/stax2/typed/TypedValueDecoder;)V

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$BooleanDecoder;->getValue()Z

    move-result p1

    return p1
.end method

.method public getAttributeAsDecimal(I)Ljava/math/BigDecimal;
    .locals 1

    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_decoderFactory()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;

    move-result-object v0

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;->getDecimalDecoder()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$DecimalDecoder;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->getAttributeAs(ILorg/codehaus/stax2/typed/TypedValueDecoder;)V

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$DecimalDecoder;->getValue()Ljava/math/BigDecimal;

    move-result-object p1

    return-object p1
.end method

.method public getAttributeAsDouble(I)D
    .locals 2

    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_decoderFactory()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;

    move-result-object v0

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;->getDoubleDecoder()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$DoubleDecoder;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->getAttributeAs(ILorg/codehaus/stax2/typed/TypedValueDecoder;)V

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$DoubleDecoder;->getValue()D

    move-result-wide v0

    return-wide v0
.end method

.method public getAttributeAsDoubleArray(I)[D
    .locals 1

    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_decoderFactory()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;

    move-result-object v0

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;->getDoubleArrayDecoder()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$DoubleArrayDecoder;

    move-result-object v0

    invoke-virtual {p0, p1}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->getAttributeValue(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, v0, p1}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_getAttributeAsArray(Lorg/codehaus/stax2/typed/TypedArrayDecoder;Ljava/lang/String;)I

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$DoubleArrayDecoder;->getValues()[D

    move-result-object p1

    return-object p1
.end method

.method public getAttributeAsFloat(I)F
    .locals 1

    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_decoderFactory()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;

    move-result-object v0

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;->getFloatDecoder()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$FloatDecoder;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->getAttributeAs(ILorg/codehaus/stax2/typed/TypedValueDecoder;)V

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$FloatDecoder;->getValue()F

    move-result p1

    return p1
.end method

.method public getAttributeAsFloatArray(I)[F
    .locals 1

    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_decoderFactory()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;

    move-result-object v0

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;->getFloatArrayDecoder()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$FloatArrayDecoder;

    move-result-object v0

    invoke-virtual {p0, p1}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->getAttributeValue(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, v0, p1}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_getAttributeAsArray(Lorg/codehaus/stax2/typed/TypedArrayDecoder;Ljava/lang/String;)I

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$FloatArrayDecoder;->getValues()[F

    move-result-object p1

    return-object p1
.end method

.method public getAttributeAsInt(I)I
    .locals 1

    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_decoderFactory()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;

    move-result-object v0

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;->getIntDecoder()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$IntDecoder;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->getAttributeAs(ILorg/codehaus/stax2/typed/TypedValueDecoder;)V

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$IntDecoder;->getValue()I

    move-result p1

    return p1
.end method

.method public getAttributeAsIntArray(I)[I
    .locals 1

    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_decoderFactory()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;

    move-result-object v0

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;->getIntArrayDecoder()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$IntArrayDecoder;

    move-result-object v0

    invoke-virtual {p0, p1}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->getAttributeValue(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, v0, p1}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_getAttributeAsArray(Lorg/codehaus/stax2/typed/TypedArrayDecoder;Ljava/lang/String;)I

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$IntArrayDecoder;->getValues()[I

    move-result-object p1

    return-object p1
.end method

.method public getAttributeAsInteger(I)Ljava/math/BigInteger;
    .locals 1

    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_decoderFactory()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;

    move-result-object v0

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;->getIntegerDecoder()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$IntegerDecoder;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->getAttributeAs(ILorg/codehaus/stax2/typed/TypedValueDecoder;)V

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$IntegerDecoder;->getValue()Ljava/math/BigInteger;

    move-result-object p1

    return-object p1
.end method

.method public getAttributeAsLong(I)J
    .locals 2

    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_decoderFactory()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;

    move-result-object v0

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;->getLongDecoder()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$LongDecoder;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->getAttributeAs(ILorg/codehaus/stax2/typed/TypedValueDecoder;)V

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$LongDecoder;->getValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public getAttributeAsLongArray(I)[J
    .locals 1

    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_decoderFactory()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;

    move-result-object v0

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;->getLongArrayDecoder()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$LongArrayDecoder;

    move-result-object v0

    invoke-virtual {p0, p1}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->getAttributeValue(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, v0, p1}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_getAttributeAsArray(Lorg/codehaus/stax2/typed/TypedArrayDecoder;Ljava/lang/String;)I

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$LongArrayDecoder;->getValues()[J

    move-result-object p1

    return-object p1
.end method

.method public getAttributeAsQName(I)Ljavax/xml/namespace/QName;
    .locals 2

    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_decoderFactory()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;

    move-result-object v0

    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->getNamespaceContext()Ljavax/xml/namespace/NamespaceContext;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;->getQNameDecoder(Ljavax/xml/namespace/NamespaceContext;)Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$QNameDecoder;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->getAttributeAs(ILorg/codehaus/stax2/typed/TypedValueDecoder;)V

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$QNameDecoder;->getValue()Ljavax/xml/namespace/QName;

    move-result-object p1

    return-object p1
.end method

.method public getAttributeCount()I
    .locals 2

    iget v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_currEvent:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    invoke-virtual {p0, v1}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->reportWrongState(I)V

    :cond_0
    iget-object v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_attrList:Ljava/util/List;

    if-nez v0, :cond_1

    invoke-direct {p0, v1}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_calcNsAndAttrLists(Z)V

    :cond_1
    iget-object v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_attrList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getAttributeIndex(Ljava/lang/String;Ljava/lang/String;)I
    .locals 0

    invoke-virtual {p0, p1, p2}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->findAttributeIndex(Ljava/lang/String;Ljava/lang/String;)I

    move-result p1

    return p1
.end method

.method public getAttributeInfo()Lorg/codehaus/stax2/AttributeInfo;
    .locals 2

    iget v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_currEvent:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    invoke-virtual {p0, v1}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->reportWrongState(I)V

    :cond_0
    return-object p0
.end method

.method public getAttributeLocalName(I)Ljava/lang/String;
    .locals 2

    iget v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_currEvent:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    invoke-virtual {p0, v1}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->reportWrongState(I)V

    :cond_0
    iget-object v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_attrList:Ljava/util/List;

    if-nez v0, :cond_1

    invoke-direct {p0, v1}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_calcNsAndAttrLists(Z)V

    :cond_1
    iget-object v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_attrList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_3

    if-gez p1, :cond_2

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_attrList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lorg/w3c/dom/Attr;

    invoke-direct {p0, p1}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_safeGetLocalName(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_internName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_3
    :goto_0
    invoke-direct {p0, p1}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->handleIllegalAttrIndex(I)V

    const/4 p1, 0x0

    return-object p1
.end method

.method public getAttributeName(I)Ljavax/xml/namespace/QName;
    .locals 2

    iget v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_currEvent:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    invoke-virtual {p0, v1}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->reportWrongState(I)V

    :cond_0
    iget-object v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_attrList:Ljava/util/List;

    if-nez v0, :cond_1

    invoke-direct {p0, v1}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_calcNsAndAttrLists(Z)V

    :cond_1
    iget-object v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_attrList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_3

    if-gez p1, :cond_2

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_attrList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lorg/w3c/dom/Attr;

    invoke-interface {p1}, Lorg/w3c/dom/Node;->getNamespaceURI()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_safeGetLocalName(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1}, Lorg/w3c/dom/Node;->getPrefix()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, v0, v1, p1}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_constructQName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljavax/xml/namespace/QName;

    move-result-object p1

    return-object p1

    :cond_3
    :goto_0
    invoke-direct {p0, p1}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->handleIllegalAttrIndex(I)V

    const/4 p1, 0x0

    return-object p1
.end method

.method public getAttributeNamespace(I)Ljava/lang/String;
    .locals 2

    iget v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_currEvent:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    invoke-virtual {p0, v1}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->reportWrongState(I)V

    :cond_0
    iget-object v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_attrList:Ljava/util/List;

    if-nez v0, :cond_1

    invoke-direct {p0, v1}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_calcNsAndAttrLists(Z)V

    :cond_1
    iget-object v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_attrList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_3

    if-gez p1, :cond_2

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_attrList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lorg/w3c/dom/Attr;

    invoke-interface {p1}, Lorg/w3c/dom/Node;->getNamespaceURI()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_internNsURI(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_3
    :goto_0
    invoke-direct {p0, p1}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->handleIllegalAttrIndex(I)V

    const/4 p1, 0x0

    return-object p1
.end method

.method public getAttributePrefix(I)Ljava/lang/String;
    .locals 2

    iget v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_currEvent:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    invoke-virtual {p0, v1}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->reportWrongState(I)V

    :cond_0
    iget-object v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_attrList:Ljava/util/List;

    if-nez v0, :cond_1

    invoke-direct {p0, v1}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_calcNsAndAttrLists(Z)V

    :cond_1
    iget-object v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_attrList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_3

    if-gez p1, :cond_2

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_attrList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lorg/w3c/dom/Attr;

    invoke-interface {p1}, Lorg/w3c/dom/Node;->getPrefix()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_internName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_3
    :goto_0
    invoke-direct {p0, p1}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->handleIllegalAttrIndex(I)V

    const/4 p1, 0x0

    return-object p1
.end method

.method public getAttributeType(I)Ljava/lang/String;
    .locals 2

    iget v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_currEvent:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    invoke-virtual {p0, v1}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->reportWrongState(I)V

    :cond_0
    iget-object v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_attrList:Ljava/util/List;

    if-nez v0, :cond_1

    invoke-direct {p0, v1}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_calcNsAndAttrLists(Z)V

    :cond_1
    iget-object v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_attrList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_3

    if-gez p1, :cond_2

    goto :goto_0

    :cond_2
    const-string p1, "CDATA"

    return-object p1

    :cond_3
    :goto_0
    invoke-direct {p0, p1}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->handleIllegalAttrIndex(I)V

    const/4 p1, 0x0

    return-object p1
.end method

.method public getAttributeValue(I)Ljava/lang/String;
    .locals 2

    .line 1
    iget v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_currEvent:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    invoke-virtual {p0, v1}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->reportWrongState(I)V

    :cond_0
    iget-object v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_attrList:Ljava/util/List;

    if-nez v0, :cond_1

    invoke-direct {p0, v1}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_calcNsAndAttrLists(Z)V

    :cond_1
    iget-object v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_attrList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_3

    if-gez p1, :cond_2

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_attrList:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lorg/w3c/dom/Attr;

    invoke-interface {p1}, Lorg/w3c/dom/Attr;->getValue()Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_3
    :goto_0
    invoke-direct {p0, p1}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->handleIllegalAttrIndex(I)V

    const/4 p1, 0x0

    return-object p1
.end method

.method public getAttributeValue(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .line 2
    iget v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_currEvent:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    invoke-virtual {p0, v1}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->reportWrongState(I)V

    :cond_0
    iget-object v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_currNode:Lorg/w3c/dom/Node;

    check-cast v0, Lorg/w3c/dom/Element;

    invoke-interface {v0}, Lorg/w3c/dom/Node;->getAttributes()Lorg/w3c/dom/NamedNodeMap;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_1

    move-object p1, v1

    :cond_1
    invoke-interface {v0, p1, p2}, Lorg/w3c/dom/NamedNodeMap;->getNamedItemNS(Ljava/lang/String;Ljava/lang/String;)Lorg/w3c/dom/Node;

    move-result-object p1

    check-cast p1, Lorg/w3c/dom/Attr;

    if-nez p1, :cond_2

    goto :goto_0

    :cond_2
    invoke-interface {p1}, Lorg/w3c/dom/Attr;->getValue()Ljava/lang/String;

    move-result-object v1

    :goto_0
    return-object v1
.end method

.method public getCharacterEncodingScheme()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getCurrentLocation()Lorg/codehaus/stax2/XMLStreamLocation2;
    .locals 1

    sget-object v0, Lorg/codehaus/stax2/XMLStreamLocation2;->NOT_AVAILABLE:Lorg/codehaus/stax2/XMLStreamLocation2;

    return-object v0
.end method

.method public getDTDInfo()Lorg/codehaus/stax2/DTDInfo;
    .locals 2

    iget v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_currEvent:I

    const/16 v1, 0xb

    if-eq v0, v1, :cond_0

    const/4 v0, 0x0

    return-object v0

    :cond_0
    return-object p0
.end method

.method public getDTDInternalSubset()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getDTDPublicId()Ljava/lang/String;
    .locals 2

    iget v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_currEvent:I

    const/16 v1, 0xb

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_currNode:Lorg/w3c/dom/Node;

    check-cast v0, Lorg/w3c/dom/DocumentType;

    invoke-interface {v0}, Lorg/w3c/dom/DocumentType;->getPublicId()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public getDTDRootName()Ljava/lang/String;
    .locals 2

    iget v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_currEvent:I

    const/16 v1, 0xb

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_currNode:Lorg/w3c/dom/Node;

    check-cast v0, Lorg/w3c/dom/DocumentType;

    invoke-interface {v0}, Lorg/w3c/dom/DocumentType;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_internName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public getDTDSystemId()Ljava/lang/String;
    .locals 2

    iget v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_currEvent:I

    const/16 v1, 0xb

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_currNode:Lorg/w3c/dom/Node;

    check-cast v0, Lorg/w3c/dom/DocumentType;

    invoke-interface {v0}, Lorg/w3c/dom/DocumentType;->getSystemId()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public getDepth()I
    .locals 1

    iget v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_depth:I

    return v0
.end method

.method public getElementAs(Lorg/codehaus/stax2/typed/TypedValueDecoder;)V
    .locals 1

    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->getElementText()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/codehaus/stax2/ri/Stax2Util;->trimSpaces(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    :try_start_0
    invoke-virtual {p1}, Lorg/codehaus/stax2/typed/TypedValueDecoder;->handleEmptyValue()V

    goto :goto_0

    :cond_0
    invoke-virtual {p1, v0}, Lorg/codehaus/stax2/typed/TypedValueDecoder;->decode(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception p1

    invoke-virtual {p0, p1, v0}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_constructTypeException(Ljava/lang/IllegalArgumentException;Ljava/lang/String;)Lorg/codehaus/stax2/typed/TypedXMLStreamException;

    move-result-object p1

    throw p1
.end method

.method public getElementAsBinary()[B
    .locals 1

    .line 1
    invoke-static {}, Lorg/codehaus/stax2/typed/Base64Variants;->getDefaultVariant()Lorg/codehaus/stax2/typed/Base64Variant;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->getElementAsBinary(Lorg/codehaus/stax2/typed/Base64Variant;)[B

    move-result-object v0

    return-object v0
.end method

.method public getElementAsBinary(Lorg/codehaus/stax2/typed/Base64Variant;)[B
    .locals 6

    .line 2
    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_base64Decoder()Lorg/codehaus/stax2/ri/typed/StringBase64Decoder;

    move-result-object v0

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/StringBase64Decoder;->getByteAggregator()Lorg/codehaus/stax2/ri/Stax2Util$ByteAggregator;

    move-result-object v0

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/Stax2Util$ByteAggregator;->startAggregation()[B

    move-result-object v1

    :goto_0
    array-length v2, v1

    const/4 v3, 0x0

    :cond_0
    invoke-virtual {p0, v1, v3, v2, p1}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->readElementAsBinary([BIILorg/codehaus/stax2/typed/Base64Variant;)I

    move-result v4

    const/4 v5, 0x1

    if-ge v4, v5, :cond_1

    invoke-virtual {v0, v1, v3}, Lorg/codehaus/stax2/ri/Stax2Util$ByteAggregator;->aggregateAll([BI)[B

    move-result-object p1

    return-object p1

    :cond_1
    add-int/2addr v3, v4

    sub-int/2addr v2, v4

    if-gtz v2, :cond_0

    invoke-virtual {v0, v1}, Lorg/codehaus/stax2/ri/Stax2Util$ByteAggregator;->addFullBlock([B)[B

    move-result-object v1

    goto :goto_0
.end method

.method public getElementAsBoolean()Z
    .locals 1

    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_decoderFactory()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;

    move-result-object v0

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;->getBooleanDecoder()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$BooleanDecoder;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->getElementAs(Lorg/codehaus/stax2/typed/TypedValueDecoder;)V

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$BooleanDecoder;->getValue()Z

    move-result v0

    return v0
.end method

.method public getElementAsDecimal()Ljava/math/BigDecimal;
    .locals 1

    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_decoderFactory()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;

    move-result-object v0

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;->getDecimalDecoder()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$DecimalDecoder;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->getElementAs(Lorg/codehaus/stax2/typed/TypedValueDecoder;)V

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$DecimalDecoder;->getValue()Ljava/math/BigDecimal;

    move-result-object v0

    return-object v0
.end method

.method public getElementAsDouble()D
    .locals 2

    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_decoderFactory()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;

    move-result-object v0

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;->getDoubleDecoder()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$DoubleDecoder;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->getElementAs(Lorg/codehaus/stax2/typed/TypedValueDecoder;)V

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$DoubleDecoder;->getValue()D

    move-result-wide v0

    return-wide v0
.end method

.method public getElementAsFloat()F
    .locals 1

    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_decoderFactory()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;

    move-result-object v0

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;->getFloatDecoder()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$FloatDecoder;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->getElementAs(Lorg/codehaus/stax2/typed/TypedValueDecoder;)V

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$FloatDecoder;->getValue()F

    move-result v0

    return v0
.end method

.method public getElementAsInt()I
    .locals 1

    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_decoderFactory()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;

    move-result-object v0

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;->getIntDecoder()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$IntDecoder;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->getElementAs(Lorg/codehaus/stax2/typed/TypedValueDecoder;)V

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$IntDecoder;->getValue()I

    move-result v0

    return v0
.end method

.method public getElementAsInteger()Ljava/math/BigInteger;
    .locals 1

    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_decoderFactory()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;

    move-result-object v0

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;->getIntegerDecoder()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$IntegerDecoder;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->getElementAs(Lorg/codehaus/stax2/typed/TypedValueDecoder;)V

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$IntegerDecoder;->getValue()Ljava/math/BigInteger;

    move-result-object v0

    return-object v0
.end method

.method public getElementAsLong()J
    .locals 2

    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_decoderFactory()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;

    move-result-object v0

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;->getLongDecoder()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$LongDecoder;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->getElementAs(Lorg/codehaus/stax2/typed/TypedValueDecoder;)V

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$LongDecoder;->getValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public getElementAsQName()Ljavax/xml/namespace/QName;
    .locals 2

    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_decoderFactory()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;

    move-result-object v0

    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->getNamespaceContext()Ljavax/xml/namespace/NamespaceContext;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;->getQNameDecoder(Ljavax/xml/namespace/NamespaceContext;)Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$QNameDecoder;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->getElementAs(Lorg/codehaus/stax2/typed/TypedValueDecoder;)V

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$QNameDecoder;->getValue()Ljavax/xml/namespace/QName;

    move-result-object v0

    return-object v0
.end method

.method public getElementText()Ljava/lang/String;
    .locals 7

    iget v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_currEvent:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    invoke-virtual {p0, v1}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->reportParseProblem(I)V

    :cond_0
    iget-boolean v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_coalescing:Z

    const/4 v2, 0x4

    const/4 v3, 0x3

    const/4 v4, 0x5

    const/4 v5, 0x2

    if-eqz v0, :cond_7

    const/4 v0, 0x0

    :cond_1
    :goto_0
    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->next()I

    move-result v6

    if-ne v6, v5, :cond_3

    if-nez v0, :cond_2

    const-string v0, ""

    :cond_2
    return-object v0

    :cond_3
    if-eq v6, v4, :cond_1

    if-ne v6, v3, :cond_4

    goto :goto_0

    :cond_4
    shl-int v6, v1, v6

    and-int/lit16 v6, v6, 0x1250

    if-nez v6, :cond_5

    invoke-virtual {p0, v2}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->reportParseProblem(I)V

    :cond_5
    if-nez v0, :cond_6

    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->getText()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_6
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->getText()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_7
    iget-object v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_textBuffer:Lorg/codehaus/stax2/ri/Stax2Util$TextBuffer;

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/Stax2Util$TextBuffer;->reset()V

    :cond_8
    :goto_1
    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->next()I

    move-result v0

    if-ne v0, v5, :cond_9

    iget-object v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_textBuffer:Lorg/codehaus/stax2/ri/Stax2Util$TextBuffer;

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/Stax2Util$TextBuffer;->get()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_9
    if-eq v0, v4, :cond_8

    if-ne v0, v3, :cond_a

    goto :goto_1

    :cond_a
    shl-int v0, v1, v0

    and-int/lit16 v0, v0, 0x1250

    if-nez v0, :cond_b

    invoke-virtual {p0, v2}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->reportParseProblem(I)V

    :cond_b
    iget-object v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_textBuffer:Lorg/codehaus/stax2/ri/Stax2Util$TextBuffer;

    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->getText()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Lorg/codehaus/stax2/ri/Stax2Util$TextBuffer;->append(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public getEncoding()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->getCharacterEncodingScheme()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getEndLocation()Lorg/codehaus/stax2/XMLStreamLocation2;
    .locals 1

    sget-object v0, Lorg/codehaus/stax2/XMLStreamLocation2;->NOT_AVAILABLE:Lorg/codehaus/stax2/XMLStreamLocation2;

    return-object v0
.end method

.method public getEndingByteOffset()J
    .locals 2

    const-wide/16 v0, -0x1

    return-wide v0
.end method

.method public getEndingCharOffset()J
    .locals 2

    const-wide/16 v0, -0x1

    return-wide v0
.end method

.method public getErrorLocation()Ljavax/xml/stream/Location;
    .locals 1

    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->getCurrentLocation()Lorg/codehaus/stax2/XMLStreamLocation2;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->getLocation()Ljavax/xml/stream/Location;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method public getEventType()I
    .locals 1

    iget v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_currEvent:I

    return v0
.end method

.method public getFeature(Ljava/lang/String;)Ljava/lang/Object;
    .locals 3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unrecognized feature \""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "\""

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getIdAttributeIndex()I
    .locals 1

    const/4 v0, -0x1

    return v0
.end method

.method public getLocalName()Ljava/lang/String;
    .locals 2

    iget v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_currEvent:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    goto :goto_1

    :cond_0
    const/16 v1, 0x9

    if-eq v0, v1, :cond_1

    const/4 v0, 0x7

    invoke-virtual {p0, v0}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->reportWrongState(I)V

    :cond_1
    iget-object v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_currNode:Lorg/w3c/dom/Node;

    invoke-interface {v0}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {p0, v0}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_internName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_2
    :goto_1
    iget-object v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_currNode:Lorg/w3c/dom/Node;

    invoke-direct {p0, v0}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_safeGetLocalName(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final getLocation()Ljavax/xml/stream/Location;
    .locals 1

    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->getStartLocation()Lorg/codehaus/stax2/XMLStreamLocation2;

    move-result-object v0

    return-object v0
.end method

.method public final getLocationInfo()Lorg/codehaus/stax2/LocationInfo;
    .locals 0

    return-object p0
.end method

.method public getName()Ljavax/xml/namespace/QName;
    .locals 3

    iget v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_currEvent:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    const/4 v2, 0x2

    if-eq v0, v2, :cond_0

    invoke-virtual {p0, v1}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->reportWrongState(I)V

    :cond_0
    iget-object v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_currNode:Lorg/w3c/dom/Node;

    invoke-interface {v0}, Lorg/w3c/dom/Node;->getNamespaceURI()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_currNode:Lorg/w3c/dom/Node;

    invoke-direct {p0, v1}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_safeGetLocalName(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_currNode:Lorg/w3c/dom/Node;

    invoke-interface {v2}, Lorg/w3c/dom/Node;->getPrefix()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_constructQName(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljavax/xml/namespace/QName;

    move-result-object v0

    return-object v0
.end method

.method public getNamespaceContext()Ljavax/xml/namespace/NamespaceContext;
    .locals 0

    return-object p0
.end method

.method public getNamespaceCount()I
    .locals 4

    iget v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_currEvent:I

    const/4 v1, 0x2

    const/4 v2, 0x1

    if-eq v0, v2, :cond_0

    if-eq v0, v1, :cond_0

    invoke-virtual {p0, v1}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->reportWrongState(I)V

    :cond_0
    iget-object v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_nsDeclList:Ljava/util/List;

    if-nez v0, :cond_3

    iget-boolean v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_cfgNsAware:Z

    const/4 v3, 0x0

    if-nez v0, :cond_1

    return v3

    :cond_1
    iget v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_currEvent:I

    if-ne v0, v2, :cond_2

    goto :goto_0

    :cond_2
    move v2, v3

    :goto_0
    invoke-direct {p0, v2}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_calcNsAndAttrLists(Z)V

    :cond_3
    iget-object v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_nsDeclList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    div-int/2addr v0, v1

    return v0
.end method

.method public getNamespacePrefix(I)Ljava/lang/String;
    .locals 3

    iget v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_currEvent:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    const/4 v2, 0x2

    if-eq v0, v2, :cond_0

    invoke-virtual {p0, v2}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->reportWrongState(I)V

    :cond_0
    iget-object v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_nsDeclList:Ljava/util/List;

    if-nez v0, :cond_3

    iget-boolean v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_cfgNsAware:Z

    if-nez v0, :cond_1

    invoke-direct {p0, p1}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->handleIllegalNsIndex(I)V

    :cond_1
    iget v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_currEvent:I

    if-ne v0, v1, :cond_2

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    :goto_0
    invoke-direct {p0, v1}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_calcNsAndAttrLists(Z)V

    :cond_3
    if-ltz p1, :cond_4

    add-int v0, p1, p1

    iget-object v1, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_nsDeclList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lt v0, v1, :cond_5

    :cond_4
    invoke-direct {p0, p1}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->handleIllegalNsIndex(I)V

    :cond_5
    iget-object v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_nsDeclList:Ljava/util/List;

    add-int/2addr p1, p1

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    return-object p1
.end method

.method public getNamespaceURI()Ljava/lang/String;
    .locals 2

    .line 1
    iget v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_currEvent:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    invoke-virtual {p0, v1}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->reportWrongState(I)V

    :cond_0
    iget-object v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_currNode:Lorg/w3c/dom/Node;

    invoke-interface {v0}, Lorg/w3c/dom/Node;->getNamespaceURI()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_internNsURI(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getNamespaceURI(I)Ljava/lang/String;
    .locals 3

    .line 2
    iget v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_currEvent:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    const/4 v2, 0x2

    if-eq v0, v2, :cond_0

    invoke-virtual {p0, v2}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->reportWrongState(I)V

    :cond_0
    iget-object v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_nsDeclList:Ljava/util/List;

    if-nez v0, :cond_3

    iget-boolean v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_cfgNsAware:Z

    if-nez v0, :cond_1

    invoke-direct {p0, p1}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->handleIllegalNsIndex(I)V

    :cond_1
    iget v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_currEvent:I

    if-ne v0, v1, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    invoke-direct {p0, v0}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_calcNsAndAttrLists(Z)V

    :cond_3
    if-ltz p1, :cond_4

    add-int v0, p1, p1

    iget-object v2, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_nsDeclList:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lt v0, v2, :cond_5

    :cond_4
    invoke-direct {p0, p1}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->handleIllegalNsIndex(I)V

    :cond_5
    iget-object v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_nsDeclList:Ljava/util/List;

    add-int/2addr p1, p1

    add-int/2addr p1, v1

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    return-object p1
.end method

.method public getNamespaceURI(Ljava/lang/String;)Ljava/lang/String;
    .locals 10

    .line 3
    iget-object v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_currNode:Lorg/w3c/dom/Node;

    const/4 v1, 0x0

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_0

    goto :goto_0

    :cond_0
    move v2, v1

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v2, 0x1

    :goto_1
    if-eqz v0, :cond_6

    invoke-interface {v0}, Lorg/w3c/dom/Node;->getAttributes()Lorg/w3c/dom/NamedNodeMap;

    move-result-object v3

    if-eqz v3, :cond_5

    invoke-interface {v3}, Lorg/w3c/dom/NamedNodeMap;->getLength()I

    move-result v4

    move v5, v1

    :goto_2
    if-ge v5, v4, :cond_5

    invoke-interface {v3, v5}, Lorg/w3c/dom/NamedNodeMap;->item(I)Lorg/w3c/dom/Node;

    move-result-object v6

    invoke-interface {v6}, Lorg/w3c/dom/Node;->getPrefix()Ljava/lang/String;

    move-result-object v7

    const-string v8, "xmlns"

    if-eqz v7, :cond_3

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v9

    if-nez v9, :cond_2

    goto :goto_3

    :cond_2
    if-nez v2, :cond_4

    invoke-virtual {v8, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    invoke-interface {v6}, Lorg/w3c/dom/Node;->getLocalName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    invoke-interface {v6}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_3
    :goto_3
    if-eqz v2, :cond_4

    invoke-interface {v6}, Lorg/w3c/dom/Node;->getLocalName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v8, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    invoke-interface {v6}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_4
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    :cond_5
    invoke-interface {v0}, Lorg/w3c/dom/Node;->getParentNode()Lorg/w3c/dom/Node;

    move-result-object v0

    goto :goto_1

    :cond_6
    const/4 p1, 0x0

    return-object p1
.end method

.method public getNonTransientNamespaceContext()Ljavax/xml/namespace/NamespaceContext;
    .locals 1

    invoke-static {}, Lorg/codehaus/stax2/ri/EmptyNamespaceContext;->getInstance()Lorg/codehaus/stax2/ri/EmptyNamespaceContext;

    move-result-object v0

    return-object v0
.end method

.method public getNotationAttributeIndex()I
    .locals 1

    const/4 v0, -0x1

    return v0
.end method

.method public getPIData()Ljava/lang/String;
    .locals 2

    iget v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_currEvent:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    invoke-virtual {p0, v1}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->reportWrongState(I)V

    :cond_0
    iget-object v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_currNode:Lorg/w3c/dom/Node;

    invoke-interface {v0}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPITarget()Ljava/lang/String;
    .locals 2

    iget v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_currEvent:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    invoke-virtual {p0, v1}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->reportWrongState(I)V

    :cond_0
    iget-object v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_currNode:Lorg/w3c/dom/Node;

    invoke-interface {v0}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_internName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPrefix()Ljava/lang/String;
    .locals 2

    .line 1
    iget v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_currEvent:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    invoke-virtual {p0, v1}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->reportWrongState(I)V

    :cond_0
    iget-object v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_currNode:Lorg/w3c/dom/Node;

    invoke-interface {v0}, Lorg/w3c/dom/Node;->getPrefix()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_internName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPrefix(Ljava/lang/String;)Ljava/lang/String;
    .locals 9

    .line 2
    iget-object v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_currNode:Lorg/w3c/dom/Node;

    const-string v1, ""

    if-nez p1, :cond_0

    move-object p1, v1

    :cond_0
    :goto_0
    if-eqz v0, :cond_5

    invoke-interface {v0}, Lorg/w3c/dom/Node;->getAttributes()Lorg/w3c/dom/NamedNodeMap;

    move-result-object v2

    invoke-interface {v2}, Lorg/w3c/dom/NamedNodeMap;->getLength()I

    move-result v3

    const/4 v4, 0x0

    :goto_1
    if-ge v4, v3, :cond_4

    invoke-interface {v2, v4}, Lorg/w3c/dom/NamedNodeMap;->item(I)Lorg/w3c/dom/Node;

    move-result-object v5

    invoke-interface {v5}, Lorg/w3c/dom/Node;->getPrefix()Ljava/lang/String;

    move-result-object v6

    const-string v7, "xmlns"

    if-eqz v6, :cond_2

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v8

    if-nez v8, :cond_1

    goto :goto_2

    :cond_1
    invoke-virtual {v7, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {v5}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {v5}, Lorg/w3c/dom/Node;->getLocalName()Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_2
    :goto_2
    invoke-interface {v5}, Lorg/w3c/dom/Node;->getLocalName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v7, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-interface {v5}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    return-object v1

    :cond_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :cond_4
    invoke-interface {v0}, Lorg/w3c/dom/Node;->getParentNode()Lorg/w3c/dom/Node;

    move-result-object v0

    goto :goto_0

    :cond_5
    const/4 p1, 0x0

    return-object p1
.end method

.method public getPrefixedName()Ljava/lang/String;
    .locals 5

    iget v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_currEvent:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_3

    const/4 v2, 0x2

    if-eq v0, v2, :cond_3

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/16 v1, 0x9

    if-eq v0, v1, :cond_1

    const/16 v1, 0xb

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->getDTDRootName()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Current state ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_currEvent:I

    invoke-static {v2}, Lorg/codehaus/stax2/ri/Stax2Util;->eventTypeDesc(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ") not START_ELEMENT, END_ELEMENT, ENTITY_REFERENCE, PROCESSING_INSTRUCTION or DTD"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->getLocalName()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_2
    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->getPITarget()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_3
    iget-object v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_currNode:Lorg/w3c/dom/Node;

    invoke-interface {v0}, Lorg/w3c/dom/Node;->getPrefix()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_currNode:Lorg/w3c/dom/Node;

    invoke-direct {p0, v2}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_safeGetLocalName(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v2

    if-nez v0, :cond_4

    invoke-virtual {p0, v2}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_internName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_4
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v4, v1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    add-int/2addr v4, v1

    invoke-direct {v3, v4}, Ljava/lang/StringBuffer;-><init>(I)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const/16 v0, 0x3a

    invoke-virtual {v3, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_internName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPrefixes(Ljava/lang/String;)Ljava/util/Iterator;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Iterator<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    invoke-virtual {p0, p1}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->getPrefix(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    if-nez p1, :cond_0

    invoke-static {}, Lorg/codehaus/stax2/ri/EmptyIterator;->getInstance()Ljava/util/Iterator;

    move-result-object p1

    return-object p1

    :cond_0
    invoke-static {p1}, Lorg/codehaus/stax2/ri/SingletonIterator;->create(Ljava/lang/Object;)Lorg/codehaus/stax2/ri/SingletonIterator;

    move-result-object p1

    return-object p1
.end method

.method public getProcessedDTD()Ljava/lang/Object;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getProcessedDTDSchema()Lorg/codehaus/stax2/validation/DTDValidationSchema;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public abstract getProperty(Ljava/lang/String;)Ljava/lang/Object;
.end method

.method public getStartLocation()Lorg/codehaus/stax2/XMLStreamLocation2;
    .locals 1

    sget-object v0, Lorg/codehaus/stax2/XMLStreamLocation2;->NOT_AVAILABLE:Lorg/codehaus/stax2/XMLStreamLocation2;

    return-object v0
.end method

.method public getStartingByteOffset()J
    .locals 2

    const-wide/16 v0, -0x1

    return-wide v0
.end method

.method public getStartingCharOffset()J
    .locals 2

    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getText(Ljava/io/Writer;Z)I
    .locals 0

    .line 1
    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->getText()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result p1

    return p1
.end method

.method public getText()Ljava/lang/String;
    .locals 2

    .line 2
    iget-object v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_coalescedText:Ljava/lang/String;

    if-eqz v0, :cond_0

    return-object v0

    :cond_0
    const/4 v0, 0x1

    iget v1, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_currEvent:I

    shl-int/2addr v0, v1

    and-int/lit16 v0, v0, 0x1a70

    if-nez v0, :cond_1

    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->reportWrongState(I)V

    :cond_1
    iget-object v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_currNode:Lorg/w3c/dom/Node;

    invoke-interface {v0}, Lorg/w3c/dom/Node;->getNodeValue()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTextCharacters(I[CII)I
    .locals 2

    .line 1
    const/4 v0, 0x1

    iget v1, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_currEvent:I

    shl-int/2addr v0, v1

    and-int/lit16 v0, v0, 0x1070

    if-nez v0, :cond_0

    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->reportWrongState(I)V

    :cond_0
    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->getText()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-le p4, v1, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result p4

    :cond_1
    add-int v1, p1, p4

    invoke-virtual {v0, p1, v1, p2, p3}, Ljava/lang/String;->getChars(II[CI)V

    return p4
.end method

.method public getTextCharacters()[C
    .locals 1

    .line 2
    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->getText()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    return-object v0
.end method

.method public getTextLength()I
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_currEvent:I

    shl-int/2addr v0, v1

    and-int/lit16 v0, v0, 0x1070

    if-nez v0, :cond_0

    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->reportWrongState(I)V

    :cond_0
    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->getText()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    return v0
.end method

.method public getTextStart()I
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_currEvent:I

    shl-int/2addr v0, v1

    and-int/lit16 v0, v0, 0x1070

    if-nez v0, :cond_0

    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->reportWrongState(I)V

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public getVersion()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public hasName()Z
    .locals 3

    iget v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_currEvent:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 v2, 0x2

    if-ne v0, v2, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :cond_1
    :goto_0
    return v1
.end method

.method public hasNext()Z
    .locals 2

    iget v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_currEvent:I

    const/16 v1, 0x8

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public hasText()Z
    .locals 2

    iget v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_currEvent:I

    const/4 v1, 0x1

    shl-int v0, v1, v0

    and-int/lit16 v0, v0, 0x1a70

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method public isAttributeSpecified(I)Z
    .locals 2

    iget v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_currEvent:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    invoke-virtual {p0, v1}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->reportWrongState(I)V

    :cond_0
    iget-object v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_currNode:Lorg/w3c/dom/Node;

    check-cast v0, Lorg/w3c/dom/Element;

    invoke-interface {v0}, Lorg/w3c/dom/Node;->getAttributes()Lorg/w3c/dom/NamedNodeMap;

    move-result-object v0

    invoke-interface {v0, p1}, Lorg/w3c/dom/NamedNodeMap;->item(I)Lorg/w3c/dom/Node;

    move-result-object v0

    check-cast v0, Lorg/w3c/dom/Attr;

    if-nez v0, :cond_1

    invoke-direct {p0, p1}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->handleIllegalAttrIndex(I)V

    const/4 p1, 0x0

    return p1

    :cond_1
    invoke-interface {v0}, Lorg/w3c/dom/Attr;->getSpecified()Z

    move-result p1

    return p1
.end method

.method public isCharacters()Z
    .locals 2

    iget v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_currEvent:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isEmptyElement()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isEndElement()Z
    .locals 2

    iget v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_currEvent:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public abstract isPropertySupported(Ljava/lang/String;)Z
.end method

.method public isStandalone()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isStartElement()Z
    .locals 2

    iget v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_currEvent:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method public isWhiteSpace()Z
    .locals 7

    iget v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_currEvent:I

    const/4 v1, 0x4

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eq v0, v1, :cond_2

    const/16 v1, 0xc

    if-ne v0, v1, :cond_0

    goto :goto_1

    :cond_0
    const/4 v1, 0x6

    if-ne v0, v1, :cond_1

    goto :goto_0

    :cond_1
    move v2, v3

    :goto_0
    return v2

    :cond_2
    :goto_1
    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->getText()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    move v4, v3

    :goto_2
    if-ge v4, v1, :cond_4

    invoke-virtual {v0, v4}, Ljava/lang/String;->charAt(I)C

    move-result v5

    const/16 v6, 0x20

    if-le v5, v6, :cond_3

    return v3

    :cond_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    :cond_4
    return v2
.end method

.method public next()I
    .locals 8

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_coalescedText:Ljava/lang/String;

    iget v1, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_currEvent:I

    const-string v2, "\'"

    const/16 v3, 0xb

    const/16 v4, 0x9

    const/4 v5, 0x2

    const/4 v6, 0x1

    if-eq v1, v6, :cond_b

    const/16 v7, 0x8

    if-eq v1, v5, :cond_5

    const/4 v0, 0x7

    if-eq v1, v0, :cond_1

    if-eq v1, v7, :cond_0

    goto :goto_1

    :cond_0
    new-instance v0, Ljava/util/NoSuchElementException;

    const-string v1, "Can not call next() after receiving END_DOCUMENT"

    invoke-direct {v0, v1}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget-object v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_currNode:Lorg/w3c/dom/Node;

    invoke-interface {v0}, Lorg/w3c/dom/Node;->getNodeType()S

    move-result v0

    if-eq v0, v6, :cond_4

    if-eq v0, v4, :cond_3

    if-ne v0, v3, :cond_2

    goto :goto_0

    :cond_2
    new-instance v0, Ljavax/xml/stream/XMLStreamException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Internal error: unexpected DOM root node type "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_currNode:Lorg/w3c/dom/Node;

    invoke-interface {v3}, Lorg/w3c/dom/Node;->getNodeType()S

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v3, " for node \'"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_currNode:Lorg/w3c/dom/Node;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljavax/xml/stream/XMLStreamException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    :goto_0
    iget-object v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_currNode:Lorg/w3c/dom/Node;

    invoke-interface {v0}, Lorg/w3c/dom/Node;->getFirstChild()Lorg/w3c/dom/Node;

    move-result-object v0

    iput-object v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_currNode:Lorg/w3c/dom/Node;

    if-nez v0, :cond_d

    iput v7, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_currEvent:I

    return v7

    :cond_4
    iput v6, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_currEvent:I

    return v6

    :cond_5
    iget v1, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_depth:I

    sub-int/2addr v1, v6

    iput v1, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_depth:I

    iput-object v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_attrList:Ljava/util/List;

    iput-object v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_nsDeclList:Ljava/util/List;

    iget-object v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_currNode:Lorg/w3c/dom/Node;

    iget-object v1, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_rootNode:Lorg/w3c/dom/Node;

    if-ne v0, v1, :cond_6

    iput v7, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_currEvent:I

    return v7

    :cond_6
    :goto_1
    iget-object v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_currNode:Lorg/w3c/dom/Node;

    invoke-interface {v0}, Lorg/w3c/dom/Node;->getNextSibling()Lorg/w3c/dom/Node;

    move-result-object v0

    if-eqz v0, :cond_7

    iput-object v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_currNode:Lorg/w3c/dom/Node;

    goto :goto_2

    :cond_7
    iget-object v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_currNode:Lorg/w3c/dom/Node;

    invoke-interface {v0}, Lorg/w3c/dom/Node;->getParentNode()Lorg/w3c/dom/Node;

    move-result-object v0

    iput-object v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_currNode:Lorg/w3c/dom/Node;

    invoke-interface {v0}, Lorg/w3c/dom/Node;->getNodeType()S

    move-result v0

    if-ne v0, v6, :cond_8

    iput v5, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_currEvent:I

    return v5

    :cond_8
    iget-object v1, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_currNode:Lorg/w3c/dom/Node;

    iget-object v2, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_rootNode:Lorg/w3c/dom/Node;

    if-ne v1, v2, :cond_a

    if-eq v0, v4, :cond_9

    if-ne v0, v3, :cond_a

    :cond_9
    iput v7, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_currEvent:I

    return v7

    :cond_a
    new-instance v1, Ljavax/xml/stream/XMLStreamException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Internal error: non-element parent node ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, ") that is not the initial root node"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljavax/xml/stream/XMLStreamException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_b
    iget v1, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_depth:I

    add-int/2addr v1, v6

    iput v1, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_depth:I

    iput-object v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_attrList:Ljava/util/List;

    iget-object v1, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_currNode:Lorg/w3c/dom/Node;

    invoke-interface {v1}, Lorg/w3c/dom/Node;->getFirstChild()Lorg/w3c/dom/Node;

    move-result-object v1

    if-nez v1, :cond_c

    iput v5, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_currEvent:I

    return v5

    :cond_c
    iput-object v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_nsDeclList:Ljava/util/List;

    iput-object v1, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_currNode:Lorg/w3c/dom/Node;

    :cond_d
    :goto_2
    iget-object v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_currNode:Lorg/w3c/dom/Node;

    invoke-interface {v0}, Lorg/w3c/dom/Node;->getNodeType()S

    move-result v0

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    new-instance v0, Ljavax/xml/stream/XMLStreamException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Internal error: unrecognized DOM node type "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_currNode:Lorg/w3c/dom/Node;

    invoke-interface {v3}, Lorg/w3c/dom/Node;->getNodeType()S

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v3, ", for node \'"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_currNode:Lorg/w3c/dom/Node;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljavax/xml/stream/XMLStreamException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_1
    iput v3, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_currEvent:I

    goto :goto_5

    :pswitch_2
    const/4 v0, 0x5

    goto :goto_3

    :pswitch_3
    const/4 v0, 0x3

    :goto_3
    iput v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_currEvent:I

    goto :goto_5

    :pswitch_4
    iput v4, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_currEvent:I

    goto :goto_5

    :pswitch_5
    iget-boolean v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_coalescing:Z

    const/16 v1, 0xc

    if-eqz v0, :cond_e

    goto :goto_4

    :pswitch_6
    iget-boolean v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_coalescing:Z

    const/4 v1, 0x4

    if-eqz v0, :cond_e

    :goto_4
    invoke-virtual {p0, v1}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->coalesceText(I)V

    goto :goto_5

    :cond_e
    iput v1, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_currEvent:I

    goto :goto_5

    :pswitch_7
    new-instance v0, Ljavax/xml/stream/XMLStreamException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Internal error: unexpected DOM node type "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_currNode:Lorg/w3c/dom/Node;

    invoke-interface {v3}, Lorg/w3c/dom/Node;->getNodeType()S

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v3, " (attr/entity/notation?), for node \'"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_currNode:Lorg/w3c/dom/Node;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljavax/xml/stream/XMLStreamException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_8
    iput v6, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_currEvent:I

    :goto_5
    iget v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_currEvent:I

    return v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_7
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_7
    .end packed-switch
.end method

.method public nextTag()I
    .locals 3

    :goto_0
    :pswitch_0
    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->next()I

    move-result v0

    const/16 v1, 0xc

    if-eq v0, v1, :cond_0

    packed-switch v0, :pswitch_data_0

    goto :goto_1

    :pswitch_1
    return v0

    :cond_0
    :pswitch_2
    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->isWhiteSpace()Z

    move-result v1

    if-eqz v1, :cond_1

    goto :goto_0

    :cond_1
    const-string v1, "Received non-all-whitespace CHARACTERS or CDATA event in nextTag()."

    invoke-virtual {p0, v1}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->throwStreamException(Ljava/lang/String;)V

    :goto_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Received event "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {v0}, Lorg/codehaus/stax2/ri/Stax2Util;->eventTypeDesc(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, ", instead of START_ELEMENT or END_ELEMENT."

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->throwStreamException(Ljava/lang/String;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public readElementAsArray(Lorg/codehaus/stax2/typed/TypedArrayDecoder;)I
    .locals 12

    iget v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_currEvent:I

    const/4 v1, 0x4

    const/4 v2, -0x1

    const/4 v3, 0x2

    const/4 v4, 0x1

    if-ne v0, v4, :cond_1

    iget-object v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_currNode:Lorg/w3c/dom/Node;

    invoke-interface {v0}, Lorg/w3c/dom/Node;->getFirstChild()Lorg/w3c/dom/Node;

    move-result-object v0

    if-nez v0, :cond_0

    iput v3, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_currEvent:I

    return v2

    :cond_0
    invoke-direct {p0, v0}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->coalesceTypedText(Lorg/w3c/dom/Node;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_coalescedText:Ljava/lang/String;

    iput v1, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_currEvent:I

    iget-object v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_currNode:Lorg/w3c/dom/Node;

    invoke-interface {v0}, Lorg/w3c/dom/Node;->getLastChild()Lorg/w3c/dom/Node;

    move-result-object v0

    iput-object v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_currNode:Lorg/w3c/dom/Node;

    goto :goto_0

    :cond_1
    if-eq v0, v1, :cond_3

    const/16 v1, 0xc

    if-eq v0, v1, :cond_3

    if-ne v0, v3, :cond_2

    return v2

    :cond_2
    const/4 v0, 0x6

    invoke-virtual {p0, v0}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->reportWrongState(I)V

    :cond_3
    iget-object v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_coalescedText:Ljava/lang/String;

    if-eqz v0, :cond_c

    :goto_0
    iget-object v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_coalescedText:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v7, v6

    move v6, v5

    :cond_4
    const-string v8, ""

    if-ge v5, v1, :cond_9

    :cond_5
    :try_start_0
    invoke-virtual {v0, v5}, Ljava/lang/String;->charAt(I)C

    move-result v9
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    const/16 v10, 0x20

    if-gt v9, v10, :cond_6

    add-int/lit8 v5, v5, 0x1

    if-lt v5, v1, :cond_5

    goto :goto_5

    :cond_6
    add-int/lit8 v9, v5, 0x1

    :goto_1
    if-ge v9, v1, :cond_7

    :try_start_1
    invoke-virtual {v0, v9}, Ljava/lang/String;->charAt(I)C

    move-result v11

    if-le v11, v10, :cond_7

    add-int/lit8 v9, v9, 0x1

    goto :goto_1

    :cond_7
    add-int/lit8 v6, v6, 0x1

    invoke-virtual {v0, v5, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    add-int/lit8 v5, v9, 0x1

    :try_start_2
    invoke-virtual {p1, v7}, Lorg/codehaus/stax2/typed/TypedArrayDecoder;->decodeValue(Ljava/lang/String;)Z

    move-result v9
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    if-eqz v9, :cond_4

    goto :goto_5

    :catchall_0
    move-exception p1

    move v5, v9

    goto :goto_3

    :catch_0
    move-exception p1

    move v5, v9

    goto :goto_2

    :catchall_1
    move-exception p1

    goto :goto_3

    :catch_1
    move-exception p1

    :goto_2
    :try_start_3
    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->getLocation()Ljavax/xml/stream/Location;

    move-result-object v2

    new-instance v3, Lorg/codehaus/stax2/typed/TypedXMLStreamException;

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v3, v7, v6, v2, p1}, Lorg/codehaus/stax2/typed/TypedXMLStreamException;-><init>(Ljava/lang/String;Ljava/lang/String;Ljavax/xml/stream/Location;Ljava/lang/IllegalArgumentException;)V

    throw v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :goto_3
    sub-int/2addr v1, v5

    if-ge v1, v4, :cond_8

    goto :goto_4

    :cond_8
    invoke-virtual {v0, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v8

    :goto_4
    iput-object v8, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_coalescedText:Ljava/lang/String;

    throw p1

    :cond_9
    :goto_5
    sub-int/2addr v1, v5

    if-ge v1, v4, :cond_a

    goto :goto_6

    :cond_a
    invoke-virtual {v0, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v8

    :goto_6
    iput-object v8, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_coalescedText:Ljava/lang/String;

    if-ge v6, v4, :cond_b

    iput v3, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_currEvent:I

    iget-object p1, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_currNode:Lorg/w3c/dom/Node;

    invoke-interface {p1}, Lorg/w3c/dom/Node;->getParentNode()Lorg/w3c/dom/Node;

    move-result-object p1

    iput-object p1, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_currNode:Lorg/w3c/dom/Node;

    return v2

    :cond_b
    return v6

    :cond_c
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "First call to readElementAsArray() must be for a START_ELEMENT, not directly for a textual event"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public readElementAsBinary([BII)I
    .locals 1

    .line 1
    invoke-static {}, Lorg/codehaus/stax2/typed/Base64Variants;->getDefaultVariant()Lorg/codehaus/stax2/typed/Base64Variant;

    move-result-object v0

    invoke-virtual {p0, p1, p2, p3, v0}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->readElementAsBinary([BIILorg/codehaus/stax2/typed/Base64Variant;)I

    move-result p1

    return p1
.end method

.method public readElementAsBinary([BIILorg/codehaus/stax2/typed/Base64Variant;)I
    .locals 17

    .line 2
    move-object/from16 v1, p0

    move-object/from16 v0, p1

    move/from16 v2, p2

    move/from16 v3, p3

    move-object/from16 v4, p4

    const-string v5, ""

    if-eqz v0, :cond_12

    if-ltz v2, :cond_11

    const/4 v6, 0x0

    const/4 v7, 0x1

    if-lt v3, v7, :cond_f

    add-int v8, v2, v3

    array-length v9, v0

    if-le v8, v9, :cond_0

    goto/16 :goto_6

    :cond_0
    invoke-virtual/range {p0 .. p0}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_base64Decoder()Lorg/codehaus/stax2/ri/typed/StringBase64Decoder;

    move-result-object v8

    iget v9, v1, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_currEvent:I

    shl-int v10, v7, v9

    and-int/lit16 v10, v10, 0x1052

    const/4 v11, 0x6

    const/4 v12, -0x1

    const/4 v13, 0x2

    if-nez v10, :cond_2

    if-ne v9, v13, :cond_1

    invoke-virtual {v8}, Lorg/codehaus/stax2/ri/typed/StringBase64Decoder;->hasData()Z

    move-result v10

    if-nez v10, :cond_2

    return v12

    :cond_1
    invoke-virtual {v1, v11}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->reportWrongState(I)V

    :cond_2
    const/4 v10, 0x4

    const/4 v14, 0x3

    const/4 v15, 0x5

    if-ne v9, v7, :cond_7

    :cond_3
    :goto_0
    invoke-virtual/range {p0 .. p0}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->next()I

    move-result v9

    if-ne v9, v13, :cond_4

    return v12

    :cond_4
    if-eq v9, v15, :cond_3

    if-ne v9, v14, :cond_5

    goto :goto_0

    :cond_5
    shl-int v9, v7, v9

    and-int/lit16 v9, v9, 0x1250

    if-nez v9, :cond_6

    invoke-virtual {v1, v10}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->reportParseProblem(I)V

    :cond_6
    invoke-virtual/range {p0 .. p0}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->getText()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v4, v7, v9}, Lorg/codehaus/stax2/ri/typed/StringBase64Decoder;->init(Lorg/codehaus/stax2/typed/Base64Variant;ZLjava/lang/String;)V

    :cond_7
    move v9, v6

    :goto_1
    :try_start_0
    invoke-virtual {v8, v0, v2, v3}, Lorg/codehaus/stax2/ri/typed/StringBase64Decoder;->decode([BII)I

    move-result v16
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    add-int v2, v2, v16

    add-int v9, v9, v16

    sub-int v3, v3, v16

    if-lt v3, v7, :cond_d

    iget v12, v1, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_currEvent:I

    if-ne v12, v13, :cond_8

    goto :goto_4

    :cond_8
    :goto_2
    invoke-virtual/range {p0 .. p0}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->next()I

    move-result v12

    if-eq v12, v15, :cond_8

    if-eq v12, v14, :cond_8

    if-ne v12, v11, :cond_9

    goto :goto_2

    :cond_9
    if-ne v12, v13, :cond_b

    invoke-virtual {v8}, Lorg/codehaus/stax2/ri/typed/StringBase64Decoder;->endOfContent()I

    move-result v12

    if-ltz v12, :cond_a

    if-lez v12, :cond_d

    goto :goto_3

    :cond_a
    const-string v0, "Incomplete base64 triplet at the end of decoded content"

    invoke-virtual {v1, v0, v5}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_constructTypeException(Ljava/lang/String;Ljava/lang/String;)Lorg/codehaus/stax2/typed/TypedXMLStreamException;

    move-result-object v0

    throw v0

    :cond_b
    shl-int v12, v7, v12

    and-int/lit16 v12, v12, 0x1250

    if-nez v12, :cond_c

    invoke-virtual {v1, v10}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->reportParseProblem(I)V

    :cond_c
    invoke-virtual/range {p0 .. p0}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->getText()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v8, v4, v6, v12}, Lorg/codehaus/stax2/ri/typed/StringBase64Decoder;->init(Lorg/codehaus/stax2/typed/Base64Variant;ZLjava/lang/String;)V

    :goto_3
    const/4 v12, -0x1

    goto :goto_1

    :cond_d
    :goto_4
    if-lez v9, :cond_e

    move v12, v9

    goto :goto_5

    :cond_e
    const/4 v12, -0x1

    :goto_5
    return v12

    :catch_0
    move-exception v0

    move-object v2, v0

    invoke-virtual {v1, v2, v5}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_constructTypeException(Ljava/lang/IllegalArgumentException;Ljava/lang/String;)Lorg/codehaus/stax2/typed/TypedXMLStreamException;

    move-result-object v0

    throw v0

    :cond_f
    :goto_6
    if-nez v3, :cond_10

    return v6

    :cond_10
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Illegal maxLength ("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v3, "), has to be positive number, and offset+maxLength can not exceed"

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    array-length v0, v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_11
    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Illegal offset ("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, "), must be [0, "

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    array-length v0, v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, "["

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_12
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v2, "resultBuffer is null"

    invoke-direct {v0, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public readElementAsDoubleArray([DII)I
    .locals 1

    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_decoderFactory()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;->getDoubleArrayDecoder([DII)Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$DoubleArrayDecoder;

    move-result-object p1

    invoke-virtual {p0, p1}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->readElementAsArray(Lorg/codehaus/stax2/typed/TypedArrayDecoder;)I

    move-result p1

    return p1
.end method

.method public readElementAsFloatArray([FII)I
    .locals 1

    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_decoderFactory()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;->getFloatArrayDecoder([FII)Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$FloatArrayDecoder;

    move-result-object p1

    invoke-virtual {p0, p1}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->readElementAsArray(Lorg/codehaus/stax2/typed/TypedArrayDecoder;)I

    move-result p1

    return p1
.end method

.method public readElementAsIntArray([III)I
    .locals 1

    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_decoderFactory()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;->getIntArrayDecoder([III)Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$IntArrayDecoder;

    move-result-object p1

    invoke-virtual {p0, p1}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->readElementAsArray(Lorg/codehaus/stax2/typed/TypedArrayDecoder;)I

    move-result p1

    return p1
.end method

.method public readElementAsLongArray([JII)I
    .locals 1

    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_decoderFactory()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;->getLongArrayDecoder([JII)Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$LongArrayDecoder;

    move-result-object p1

    invoke-virtual {p0, p1}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->readElementAsArray(Lorg/codehaus/stax2/typed/TypedArrayDecoder;)I

    move-result p1

    return p1
.end method

.method public reportParseProblem(I)V
    .locals 1

    iget v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_currEvent:I

    invoke-virtual {p0, p1, v0}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->findErrorDesc(II)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->throwStreamException(Ljava/lang/String;)V

    return-void
.end method

.method public reportWrongState(I)V
    .locals 2

    new-instance v0, Ljava/lang/IllegalStateException;

    iget v1, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_currEvent:I

    invoke-virtual {p0, p1, v1}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->findErrorDesc(II)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public require(ILjava/lang/String;Ljava/lang/String;)V
    .locals 7

    iget v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_currEvent:I

    if-eq v0, p1, :cond_1

    const/16 v1, 0xc

    const/4 v2, 0x4

    if-ne v0, v1, :cond_0

    :goto_0
    move v0, v2

    goto :goto_1

    :cond_0
    const/4 v1, 0x6

    if-ne v0, v1, :cond_1

    goto :goto_0

    :cond_1
    :goto_1
    if-eq p1, v0, :cond_2

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Required type "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {p1}, Lorg/codehaus/stax2/ri/Stax2Util;->eventTypeDesc(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, ", current type "

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {v0}, Lorg/codehaus/stax2/ri/Stax2Util;->eventTypeDesc(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->throwStreamException(Ljava/lang/String;)V

    :cond_2
    const-string p1, ")"

    const/4 v1, 0x2

    const/4 v2, 0x1

    const-string v3, "\'."

    if-eqz p3, :cond_4

    if-eq v0, v2, :cond_3

    if-eq v0, v1, :cond_3

    const/16 v4, 0x9

    if-eq v0, v4, :cond_3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Required a non-null local name, but current token not a START_ELEMENT, END_ELEMENT or ENTITY_REFERENCE (was "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v5, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_currEvent:I

    invoke-static {v5}, Lorg/codehaus/stax2/ri/Stax2Util;->eventTypeDesc(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->throwStreamException(Ljava/lang/String;)V

    :cond_3
    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->getLocalName()Ljava/lang/String;

    move-result-object v4

    if-eq v4, p3, :cond_4

    invoke-virtual {v4, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Required local name \'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p3, "\'; current local name \'"

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p0, p3}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->throwStreamException(Ljava/lang/String;)V

    :cond_4
    if-eqz p2, :cond_7

    if-eq v0, v2, :cond_5

    if-eq v0, v1, :cond_5

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Required non-null NS URI, but current token not a START_ELEMENT or END_ELEMENT (was "

    invoke-virtual {p3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {v0}, Lorg/codehaus/stax2/ri/Stax2Util;->eventTypeDesc(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->throwStreamException(Ljava/lang/String;)V

    :cond_5
    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->getNamespaceURI()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result p3

    if-nez p3, :cond_6

    if-eqz p1, :cond_7

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result p2

    if-lez p2, :cond_7

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string p3, "Required empty namespace, instead have \'"

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_2

    :cond_6
    if-eq p2, p1, :cond_7

    invoke-virtual {p2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p3

    if-nez p3, :cond_7

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Required namespace \'"

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, "\'; have \'"

    invoke-virtual {p3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    :goto_2
    invoke-virtual {p0, p1}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->throwStreamException(Ljava/lang/String;)V

    :cond_7
    return-void
.end method

.method public setFeature(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    new-instance p2, Ljava/lang/IllegalArgumentException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unrecognized feature \""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "\""

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p2
.end method

.method public setInternNames(Z)V
    .locals 0

    iput-boolean p1, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_cfgInternNames:Z

    return-void
.end method

.method public setInternNsURIs(Z)V
    .locals 0

    iput-boolean p1, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_cfgInternNsURIs:Z

    return-void
.end method

.method public abstract setProperty(Ljava/lang/String;Ljava/lang/Object;)Z
.end method

.method public setValidationProblemHandler(Lorg/codehaus/stax2/validation/ValidationProblemHandler;)Lorg/codehaus/stax2/validation/ValidationProblemHandler;
    .locals 0

    const/4 p1, 0x0

    return-object p1
.end method

.method public skipElement()V
    .locals 4

    iget v0, p0, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->_currEvent:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    invoke-virtual {p0, v1}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->reportWrongState(I)V

    :cond_0
    move v0, v1

    :cond_1
    :goto_0
    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->next()I

    move-result v2

    if-ne v2, v1, :cond_2

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    const/4 v3, 0x2

    if-ne v2, v3, :cond_1

    add-int/lit8 v0, v0, -0x1

    if-nez v0, :cond_1

    return-void
.end method

.method public standaloneSet()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public stopValidatingAgainst(Lorg/codehaus/stax2/validation/XMLValidationSchema;)Lorg/codehaus/stax2/validation/XMLValidator;
    .locals 0

    .line 1
    const/4 p1, 0x0

    return-object p1
.end method

.method public stopValidatingAgainst(Lorg/codehaus/stax2/validation/XMLValidator;)Lorg/codehaus/stax2/validation/XMLValidator;
    .locals 0

    .line 2
    const/4 p1, 0x0

    return-object p1
.end method

.method public throwStreamException(Ljava/lang/String;)V
    .locals 1

    .line 1
    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->getErrorLocation()Ljavax/xml/stream/Location;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lorg/codehaus/stax2/ri/dom/DOMWrappingReader;->throwStreamException(Ljava/lang/String;Ljavax/xml/stream/Location;)V

    return-void
.end method

.method public abstract throwStreamException(Ljava/lang/String;Ljavax/xml/stream/Location;)V
.end method

.method public validateAgainst(Lorg/codehaus/stax2/validation/XMLValidationSchema;)Lorg/codehaus/stax2/validation/XMLValidator;
    .locals 0

    const/4 p1, 0x0

    return-object p1
.end method

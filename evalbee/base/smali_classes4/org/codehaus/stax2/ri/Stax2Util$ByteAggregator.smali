.class public final Lorg/codehaus/stax2/ri/Stax2Util$ByteAggregator;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/codehaus/stax2/ri/Stax2Util;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ByteAggregator"
.end annotation


# static fields
.field static final DEFAULT_BLOCK_ARRAY_SIZE:I = 0x64

.field private static final INITIAL_BLOCK_SIZE:I = 0x1f4

.field private static final NO_BYTES:[B


# instance fields
.field private mBlockCount:I

.field private mBlocks:[[B

.field private mSpareBlock:[B

.field private mTotalLen:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    new-array v0, v0, [B

    sput-object v0, Lorg/codehaus/stax2/ri/Stax2Util$ByteAggregator;->NO_BYTES:[B

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public addFullBlock([B)[B
    .locals 5

    array-length v0, p1

    iget-object v1, p0, Lorg/codehaus/stax2/ri/Stax2Util$ByteAggregator;->mBlocks:[[B

    if-nez v1, :cond_0

    const/16 v1, 0x64

    new-array v1, v1, [[B

    iput-object v1, p0, Lorg/codehaus/stax2/ri/Stax2Util$ByteAggregator;->mBlocks:[[B

    goto :goto_0

    :cond_0
    array-length v2, v1

    iget v3, p0, Lorg/codehaus/stax2/ri/Stax2Util$ByteAggregator;->mBlockCount:I

    if-lt v3, v2, :cond_1

    add-int v3, v2, v2

    new-array v3, v3, [[B

    iput-object v3, p0, Lorg/codehaus/stax2/ri/Stax2Util$ByteAggregator;->mBlocks:[[B

    const/4 v4, 0x0

    invoke-static {v1, v4, v3, v4, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_0
    iget-object v1, p0, Lorg/codehaus/stax2/ri/Stax2Util$ByteAggregator;->mBlocks:[[B

    iget v2, p0, Lorg/codehaus/stax2/ri/Stax2Util$ByteAggregator;->mBlockCount:I

    aput-object p1, v1, v2

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lorg/codehaus/stax2/ri/Stax2Util$ByteAggregator;->mBlockCount:I

    iget p1, p0, Lorg/codehaus/stax2/ri/Stax2Util$ByteAggregator;->mTotalLen:I

    add-int/2addr p1, v0

    iput p1, p0, Lorg/codehaus/stax2/ri/Stax2Util$ByteAggregator;->mTotalLen:I

    shr-int/lit8 p1, p1, 0x1

    const/16 v0, 0x3e8

    invoke-static {p1, v0}, Ljava/lang/Math;->max(II)I

    move-result p1

    new-array p1, p1, [B

    return-object p1
.end method

.method public aggregateAll([BI)[B
    .locals 7

    iget v0, p0, Lorg/codehaus/stax2/ri/Stax2Util$ByteAggregator;->mTotalLen:I

    add-int/2addr v0, p2

    if-nez v0, :cond_0

    sget-object p1, Lorg/codehaus/stax2/ri/Stax2Util$ByteAggregator;->NO_BYTES:[B

    return-object p1

    :cond_0
    new-array v1, v0, [B

    iget-object v2, p0, Lorg/codehaus/stax2/ri/Stax2Util$ByteAggregator;->mBlocks:[[B

    const/4 v3, 0x0

    if-eqz v2, :cond_1

    move v2, v3

    move v4, v2

    :goto_0
    iget v5, p0, Lorg/codehaus/stax2/ri/Stax2Util$ByteAggregator;->mBlockCount:I

    if-ge v2, v5, :cond_2

    iget-object v5, p0, Lorg/codehaus/stax2/ri/Stax2Util$ByteAggregator;->mBlocks:[[B

    aget-object v5, v5, v2

    array-length v6, v5

    invoke-static {v5, v3, v1, v4, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    add-int/2addr v4, v6

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    move v4, v3

    :cond_2
    invoke-static {p1, v3, v1, v4, p2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object p1, p0, Lorg/codehaus/stax2/ri/Stax2Util$ByteAggregator;->mSpareBlock:[B

    add-int/2addr v4, p2

    if-ne v4, v0, :cond_3

    return-object v1

    :cond_3
    new-instance p1, Ljava/lang/RuntimeException;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Internal error: total len assumed to be "

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, ", copied "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, " bytes"

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public startAggregation()[B
    .locals 2

    const/4 v0, 0x0

    iput v0, p0, Lorg/codehaus/stax2/ri/Stax2Util$ByteAggregator;->mTotalLen:I

    iput v0, p0, Lorg/codehaus/stax2/ri/Stax2Util$ByteAggregator;->mBlockCount:I

    iget-object v0, p0, Lorg/codehaus/stax2/ri/Stax2Util$ByteAggregator;->mSpareBlock:[B

    if-nez v0, :cond_0

    const/16 v0, 0x1f4

    new-array v0, v0, [B

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    iput-object v1, p0, Lorg/codehaus/stax2/ri/Stax2Util$ByteAggregator;->mSpareBlock:[B

    :goto_0
    return-object v0
.end method

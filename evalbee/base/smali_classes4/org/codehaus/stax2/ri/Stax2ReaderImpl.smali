.class public abstract Lorg/codehaus/stax2/ri/Stax2ReaderImpl;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/codehaus/stax2/XMLStreamReader2;
.implements Lorg/codehaus/stax2/AttributeInfo;
.implements Lorg/codehaus/stax2/DTDInfo;
.implements Lorg/codehaus/stax2/LocationInfo;


# instance fields
.field protected _decoderFactory:Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public _constructTypeException(Ljava/lang/IllegalArgumentException;Ljava/lang/String;)Lorg/codehaus/stax2/typed/TypedXMLStreamException;
    .locals 3

    new-instance v0, Lorg/codehaus/stax2/typed/TypedXMLStreamException;

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/Stax2ReaderImpl;->getStartLocation()Lorg/codehaus/stax2/XMLStreamLocation2;

    move-result-object v2

    invoke-direct {v0, p2, v1, v2, p1}, Lorg/codehaus/stax2/typed/TypedXMLStreamException;-><init>(Ljava/lang/String;Ljava/lang/String;Ljavax/xml/stream/Location;Ljava/lang/IllegalArgumentException;)V

    return-object v0
.end method

.method public _decoderFactory()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;
    .locals 1

    iget-object v0, p0, Lorg/codehaus/stax2/ri/Stax2ReaderImpl;->_decoderFactory:Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;

    if-nez v0, :cond_0

    new-instance v0, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;

    invoke-direct {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;-><init>()V

    iput-object v0, p0, Lorg/codehaus/stax2/ri/Stax2ReaderImpl;->_decoderFactory:Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;

    :cond_0
    iget-object v0, p0, Lorg/codehaus/stax2/ri/Stax2ReaderImpl;->_decoderFactory:Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;

    return-object v0
.end method

.method public closeCompletely()V
    .locals 0

    invoke-interface {p0}, Ljavax/xml/stream/XMLStreamReader;->close()V

    return-void
.end method

.method public findAttributeIndex(Ljava/lang/String;Ljava/lang/String;)I
    .locals 0

    const/4 p1, -0x1

    return p1
.end method

.method public getAttributeAs(ILorg/codehaus/stax2/typed/TypedValueDecoder;)V
    .locals 0

    invoke-interface {p0, p1}, Ljavax/xml/stream/XMLStreamReader;->getAttributeValue(I)Ljava/lang/String;

    move-result-object p1

    :try_start_0
    invoke-virtual {p2, p1}, Lorg/codehaus/stax2/typed/TypedValueDecoder;->decode(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p2

    invoke-virtual {p0, p2, p1}, Lorg/codehaus/stax2/ri/Stax2ReaderImpl;->_constructTypeException(Ljava/lang/IllegalArgumentException;Ljava/lang/String;)Lorg/codehaus/stax2/typed/TypedXMLStreamException;

    move-result-object p1

    throw p1
.end method

.method public abstract getAttributeAsArray(ILorg/codehaus/stax2/typed/TypedArrayDecoder;)I
.end method

.method public getAttributeAsBinary(I)[B
    .locals 1

    .line 1
    invoke-static {}, Lorg/codehaus/stax2/typed/Base64Variants;->getDefaultVariant()Lorg/codehaus/stax2/typed/Base64Variant;

    move-result-object v0

    invoke-virtual {p0, v0, p1}, Lorg/codehaus/stax2/ri/Stax2ReaderImpl;->getAttributeAsBinary(Lorg/codehaus/stax2/typed/Base64Variant;I)[B

    move-result-object p1

    return-object p1
.end method

.method public abstract getAttributeAsBinary(Lorg/codehaus/stax2/typed/Base64Variant;I)[B
.end method

.method public getAttributeAsBoolean(I)Z
    .locals 1

    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/Stax2ReaderImpl;->_decoderFactory()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;

    move-result-object v0

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;->getBooleanDecoder()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$BooleanDecoder;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lorg/codehaus/stax2/ri/Stax2ReaderImpl;->getAttributeAs(ILorg/codehaus/stax2/typed/TypedValueDecoder;)V

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$BooleanDecoder;->getValue()Z

    move-result p1

    return p1
.end method

.method public getAttributeAsDecimal(I)Ljava/math/BigDecimal;
    .locals 1

    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/Stax2ReaderImpl;->_decoderFactory()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;

    move-result-object v0

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;->getDecimalDecoder()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$DecimalDecoder;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lorg/codehaus/stax2/ri/Stax2ReaderImpl;->getAttributeAs(ILorg/codehaus/stax2/typed/TypedValueDecoder;)V

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$DecimalDecoder;->getValue()Ljava/math/BigDecimal;

    move-result-object p1

    return-object p1
.end method

.method public getAttributeAsDouble(I)D
    .locals 2

    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/Stax2ReaderImpl;->_decoderFactory()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;

    move-result-object v0

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;->getDoubleDecoder()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$DoubleDecoder;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lorg/codehaus/stax2/ri/Stax2ReaderImpl;->getAttributeAs(ILorg/codehaus/stax2/typed/TypedValueDecoder;)V

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$DoubleDecoder;->getValue()D

    move-result-wide v0

    return-wide v0
.end method

.method public getAttributeAsDoubleArray(I)[D
    .locals 1

    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/Stax2ReaderImpl;->_decoderFactory()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;

    move-result-object v0

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;->getDoubleArrayDecoder()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$DoubleArrayDecoder;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lorg/codehaus/stax2/ri/Stax2ReaderImpl;->getAttributeAsArray(ILorg/codehaus/stax2/typed/TypedArrayDecoder;)I

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$DoubleArrayDecoder;->getValues()[D

    move-result-object p1

    return-object p1
.end method

.method public getAttributeAsFloat(I)F
    .locals 1

    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/Stax2ReaderImpl;->_decoderFactory()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;

    move-result-object v0

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;->getFloatDecoder()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$FloatDecoder;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lorg/codehaus/stax2/ri/Stax2ReaderImpl;->getAttributeAs(ILorg/codehaus/stax2/typed/TypedValueDecoder;)V

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$FloatDecoder;->getValue()F

    move-result p1

    return p1
.end method

.method public getAttributeAsFloatArray(I)[F
    .locals 1

    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/Stax2ReaderImpl;->_decoderFactory()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;

    move-result-object v0

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;->getFloatArrayDecoder()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$FloatArrayDecoder;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lorg/codehaus/stax2/ri/Stax2ReaderImpl;->getAttributeAsArray(ILorg/codehaus/stax2/typed/TypedArrayDecoder;)I

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$FloatArrayDecoder;->getValues()[F

    move-result-object p1

    return-object p1
.end method

.method public getAttributeAsInt(I)I
    .locals 1

    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/Stax2ReaderImpl;->_decoderFactory()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;

    move-result-object v0

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;->getIntDecoder()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$IntDecoder;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lorg/codehaus/stax2/ri/Stax2ReaderImpl;->getAttributeAs(ILorg/codehaus/stax2/typed/TypedValueDecoder;)V

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$IntDecoder;->getValue()I

    move-result p1

    return p1
.end method

.method public getAttributeAsIntArray(I)[I
    .locals 1

    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/Stax2ReaderImpl;->_decoderFactory()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;

    move-result-object v0

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;->getIntArrayDecoder()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$IntArrayDecoder;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lorg/codehaus/stax2/ri/Stax2ReaderImpl;->getAttributeAsArray(ILorg/codehaus/stax2/typed/TypedArrayDecoder;)I

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$IntArrayDecoder;->getValues()[I

    move-result-object p1

    return-object p1
.end method

.method public getAttributeAsInteger(I)Ljava/math/BigInteger;
    .locals 1

    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/Stax2ReaderImpl;->_decoderFactory()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;

    move-result-object v0

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;->getIntegerDecoder()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$IntegerDecoder;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lorg/codehaus/stax2/ri/Stax2ReaderImpl;->getAttributeAs(ILorg/codehaus/stax2/typed/TypedValueDecoder;)V

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$IntegerDecoder;->getValue()Ljava/math/BigInteger;

    move-result-object p1

    return-object p1
.end method

.method public getAttributeAsLong(I)J
    .locals 2

    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/Stax2ReaderImpl;->_decoderFactory()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;

    move-result-object v0

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;->getLongDecoder()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$LongDecoder;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lorg/codehaus/stax2/ri/Stax2ReaderImpl;->getAttributeAs(ILorg/codehaus/stax2/typed/TypedValueDecoder;)V

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$LongDecoder;->getValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public getAttributeAsLongArray(I)[J
    .locals 1

    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/Stax2ReaderImpl;->_decoderFactory()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;

    move-result-object v0

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;->getLongArrayDecoder()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$LongArrayDecoder;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lorg/codehaus/stax2/ri/Stax2ReaderImpl;->getAttributeAsArray(ILorg/codehaus/stax2/typed/TypedArrayDecoder;)I

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$LongArrayDecoder;->getValues()[J

    move-result-object p1

    return-object p1
.end method

.method public getAttributeAsQName(I)Ljavax/xml/namespace/QName;
    .locals 2

    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/Stax2ReaderImpl;->_decoderFactory()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;

    move-result-object v0

    invoke-interface {p0}, Ljavax/xml/stream/XMLStreamReader;->getNamespaceContext()Ljavax/xml/namespace/NamespaceContext;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;->getQNameDecoder(Ljavax/xml/namespace/NamespaceContext;)Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$QNameDecoder;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lorg/codehaus/stax2/ri/Stax2ReaderImpl;->getAttributeAs(ILorg/codehaus/stax2/typed/TypedValueDecoder;)V

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$QNameDecoder;->getValue()Ljavax/xml/namespace/QName;

    move-result-object p1

    return-object p1
.end method

.method public abstract getAttributeIndex(Ljava/lang/String;Ljava/lang/String;)I
.end method

.method public getAttributeInfo()Lorg/codehaus/stax2/AttributeInfo;
    .locals 2

    invoke-interface {p0}, Ljavax/xml/stream/XMLStreamReader;->getEventType()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/Stax2ReaderImpl;->throwNotStartElem()V

    :cond_0
    return-object p0
.end method

.method public abstract getCurrentLocation()Lorg/codehaus/stax2/XMLStreamLocation2;
.end method

.method public getDTDInfo()Lorg/codehaus/stax2/DTDInfo;
    .locals 2

    invoke-interface {p0}, Ljavax/xml/stream/XMLStreamReader;->getEventType()I

    move-result v0

    const/16 v1, 0xb

    if-eq v0, v1, :cond_0

    const/4 v0, 0x0

    return-object v0

    :cond_0
    return-object p0
.end method

.method public getDTDInternalSubset()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getDTDPublicId()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getDTDRootName()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getDTDSystemId()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public abstract getDepth()I
.end method

.method public getElementAs(Lorg/codehaus/stax2/typed/TypedValueDecoder;)V
    .locals 1

    invoke-interface {p0}, Ljavax/xml/stream/XMLStreamReader;->getElementText()Ljava/lang/String;

    move-result-object v0

    :try_start_0
    invoke-virtual {p1, v0}, Lorg/codehaus/stax2/typed/TypedValueDecoder;->decode(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception p1

    invoke-virtual {p0, p1, v0}, Lorg/codehaus/stax2/ri/Stax2ReaderImpl;->_constructTypeException(Ljava/lang/IllegalArgumentException;Ljava/lang/String;)Lorg/codehaus/stax2/typed/TypedXMLStreamException;

    move-result-object p1

    throw p1
.end method

.method public getElementAsBinary()[B
    .locals 1

    .line 1
    invoke-static {}, Lorg/codehaus/stax2/typed/Base64Variants;->getDefaultVariant()Lorg/codehaus/stax2/typed/Base64Variant;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/codehaus/stax2/ri/Stax2ReaderImpl;->getElementAsBinary(Lorg/codehaus/stax2/typed/Base64Variant;)[B

    move-result-object v0

    return-object v0
.end method

.method public abstract getElementAsBinary(Lorg/codehaus/stax2/typed/Base64Variant;)[B
.end method

.method public getElementAsBoolean()Z
    .locals 1

    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/Stax2ReaderImpl;->_decoderFactory()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;

    move-result-object v0

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;->getBooleanDecoder()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$BooleanDecoder;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/codehaus/stax2/ri/Stax2ReaderImpl;->getElementAs(Lorg/codehaus/stax2/typed/TypedValueDecoder;)V

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$BooleanDecoder;->getValue()Z

    move-result v0

    return v0
.end method

.method public getElementAsDecimal()Ljava/math/BigDecimal;
    .locals 1

    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/Stax2ReaderImpl;->_decoderFactory()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;

    move-result-object v0

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;->getDecimalDecoder()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$DecimalDecoder;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/codehaus/stax2/ri/Stax2ReaderImpl;->getElementAs(Lorg/codehaus/stax2/typed/TypedValueDecoder;)V

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$DecimalDecoder;->getValue()Ljava/math/BigDecimal;

    move-result-object v0

    return-object v0
.end method

.method public getElementAsDouble()D
    .locals 2

    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/Stax2ReaderImpl;->_decoderFactory()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;

    move-result-object v0

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;->getDoubleDecoder()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$DoubleDecoder;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/codehaus/stax2/ri/Stax2ReaderImpl;->getElementAs(Lorg/codehaus/stax2/typed/TypedValueDecoder;)V

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$DoubleDecoder;->getValue()D

    move-result-wide v0

    return-wide v0
.end method

.method public getElementAsFloat()F
    .locals 1

    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/Stax2ReaderImpl;->_decoderFactory()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;

    move-result-object v0

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;->getFloatDecoder()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$FloatDecoder;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/codehaus/stax2/ri/Stax2ReaderImpl;->getElementAs(Lorg/codehaus/stax2/typed/TypedValueDecoder;)V

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$FloatDecoder;->getValue()F

    move-result v0

    return v0
.end method

.method public getElementAsInt()I
    .locals 1

    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/Stax2ReaderImpl;->_decoderFactory()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;

    move-result-object v0

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;->getIntDecoder()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$IntDecoder;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/codehaus/stax2/ri/Stax2ReaderImpl;->getElementAs(Lorg/codehaus/stax2/typed/TypedValueDecoder;)V

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$IntDecoder;->getValue()I

    move-result v0

    return v0
.end method

.method public getElementAsInteger()Ljava/math/BigInteger;
    .locals 1

    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/Stax2ReaderImpl;->_decoderFactory()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;

    move-result-object v0

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;->getIntegerDecoder()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$IntegerDecoder;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/codehaus/stax2/ri/Stax2ReaderImpl;->getElementAs(Lorg/codehaus/stax2/typed/TypedValueDecoder;)V

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$IntegerDecoder;->getValue()Ljava/math/BigInteger;

    move-result-object v0

    return-object v0
.end method

.method public getElementAsLong()J
    .locals 2

    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/Stax2ReaderImpl;->_decoderFactory()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;

    move-result-object v0

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;->getLongDecoder()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$LongDecoder;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/codehaus/stax2/ri/Stax2ReaderImpl;->getElementAs(Lorg/codehaus/stax2/typed/TypedValueDecoder;)V

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$LongDecoder;->getValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public getElementAsQName()Ljavax/xml/namespace/QName;
    .locals 2

    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/Stax2ReaderImpl;->_decoderFactory()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;

    move-result-object v0

    invoke-interface {p0}, Ljavax/xml/stream/XMLStreamReader;->getNamespaceContext()Ljavax/xml/namespace/NamespaceContext;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;->getQNameDecoder(Ljavax/xml/namespace/NamespaceContext;)Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$QNameDecoder;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/codehaus/stax2/ri/Stax2ReaderImpl;->getElementAs(Lorg/codehaus/stax2/typed/TypedValueDecoder;)V

    invoke-virtual {v0}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$QNameDecoder;->getValue()Ljavax/xml/namespace/QName;

    move-result-object v0

    return-object v0
.end method

.method public abstract getEndLocation()Lorg/codehaus/stax2/XMLStreamLocation2;
.end method

.method public getEndingByteOffset()J
    .locals 2

    const-wide/16 v0, -0x1

    return-wide v0
.end method

.method public getEndingCharOffset()J
    .locals 2

    const-wide/16 v0, -0x1

    return-wide v0
.end method

.method public getFeature(Ljava/lang/String;)Ljava/lang/Object;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    const/4 p1, 0x0

    return-object p1
.end method

.method public getIdAttributeIndex()I
    .locals 1

    const/4 v0, -0x1

    return v0
.end method

.method public final getLocationInfo()Lorg/codehaus/stax2/LocationInfo;
    .locals 0

    return-object p0
.end method

.method public abstract getNonTransientNamespaceContext()Ljavax/xml/namespace/NamespaceContext;
.end method

.method public getNotationAttributeIndex()I
    .locals 1

    const/4 v0, -0x1

    return v0
.end method

.method public getPrefixedName()Ljava/lang/String;
    .locals 5

    invoke-interface {p0}, Ljavax/xml/stream/XMLStreamReader;->getEventType()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_3

    const/4 v2, 0x2

    if-eq v0, v2, :cond_3

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/16 v1, 0x9

    if-eq v0, v1, :cond_1

    const/16 v1, 0xb

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/Stax2ReaderImpl;->getDTDRootName()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Current state not START_ELEMENT, END_ELEMENT, ENTITY_REFERENCE, PROCESSING_INSTRUCTION or DTD"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    invoke-interface {p0}, Ljavax/xml/stream/XMLStreamReader;->getLocalName()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_2
    invoke-interface {p0}, Ljavax/xml/stream/XMLStreamReader;->getPITarget()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_3
    invoke-interface {p0}, Ljavax/xml/stream/XMLStreamReader;->getPrefix()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p0}, Ljavax/xml/stream/XMLStreamReader;->getLocalName()Ljava/lang/String;

    move-result-object v2

    if-nez v0, :cond_4

    return-object v2

    :cond_4
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v4, v1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    add-int/2addr v4, v1

    invoke-direct {v3, v4}, Ljava/lang/StringBuffer;-><init>(I)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const/16 v0, 0x3a

    invoke-virtual {v3, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getProcessedDTD()Ljava/lang/Object;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getProcessedDTDSchema()Lorg/codehaus/stax2/validation/DTDValidationSchema;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public abstract getStartLocation()Lorg/codehaus/stax2/XMLStreamLocation2;
.end method

.method public getStartingByteOffset()J
    .locals 2

    const-wide/16 v0, -0x1

    return-wide v0
.end method

.method public getStartingCharOffset()J
    .locals 2

    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getText(Ljava/io/Writer;Z)I
    .locals 2

    invoke-interface {p0}, Ljavax/xml/stream/XMLStreamReader;->getTextCharacters()[C

    move-result-object p2

    invoke-interface {p0}, Ljavax/xml/stream/XMLStreamReader;->getTextStart()I

    move-result v0

    invoke-interface {p0}, Ljavax/xml/stream/XMLStreamReader;->getTextLength()I

    move-result v1

    if-lez v1, :cond_0

    invoke-virtual {p1, p2, v0, v1}, Ljava/io/Writer;->write([CII)V

    :cond_0
    return v1
.end method

.method public abstract isEmptyElement()Z
.end method

.method public isPropertySupported(Ljava/lang/String;)Z
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

.method public abstract readElementAsArray(Lorg/codehaus/stax2/typed/TypedArrayDecoder;)I
.end method

.method public abstract readElementAsBinary(Lorg/codehaus/stax2/typed/Base64Variant;[BII)I
.end method

.method public readElementAsBinary([BII)I
    .locals 1

    .line 1
    invoke-static {}, Lorg/codehaus/stax2/typed/Base64Variants;->getDefaultVariant()Lorg/codehaus/stax2/typed/Base64Variant;

    move-result-object v0

    invoke-virtual {p0, v0, p1, p2, p3}, Lorg/codehaus/stax2/ri/Stax2ReaderImpl;->readElementAsBinary(Lorg/codehaus/stax2/typed/Base64Variant;[BII)I

    move-result p1

    return p1
.end method

.method public readElementAsDoubleArray([DII)I
    .locals 1

    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/Stax2ReaderImpl;->_decoderFactory()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;->getDoubleArrayDecoder([DII)Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$DoubleArrayDecoder;

    move-result-object p1

    invoke-virtual {p0, p1}, Lorg/codehaus/stax2/ri/Stax2ReaderImpl;->readElementAsArray(Lorg/codehaus/stax2/typed/TypedArrayDecoder;)I

    move-result p1

    return p1
.end method

.method public readElementAsFloatArray([FII)I
    .locals 1

    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/Stax2ReaderImpl;->_decoderFactory()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;->getFloatArrayDecoder([FII)Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$FloatArrayDecoder;

    move-result-object p1

    invoke-virtual {p0, p1}, Lorg/codehaus/stax2/ri/Stax2ReaderImpl;->readElementAsArray(Lorg/codehaus/stax2/typed/TypedArrayDecoder;)I

    move-result p1

    return p1
.end method

.method public readElementAsIntArray([III)I
    .locals 1

    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/Stax2ReaderImpl;->_decoderFactory()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;->getIntArrayDecoder([III)Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$IntArrayDecoder;

    move-result-object p1

    invoke-virtual {p0, p1}, Lorg/codehaus/stax2/ri/Stax2ReaderImpl;->readElementAsArray(Lorg/codehaus/stax2/typed/TypedArrayDecoder;)I

    move-result p1

    return p1
.end method

.method public readElementAsLongArray([JII)I
    .locals 1

    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/Stax2ReaderImpl;->_decoderFactory()Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory;->getLongArrayDecoder([JII)Lorg/codehaus/stax2/ri/typed/ValueDecoderFactory$LongArrayDecoder;

    move-result-object p1

    invoke-virtual {p0, p1}, Lorg/codehaus/stax2/ri/Stax2ReaderImpl;->readElementAsArray(Lorg/codehaus/stax2/typed/TypedArrayDecoder;)I

    move-result p1

    return p1
.end method

.method public setFeature(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    return-void
.end method

.method public setProperty(Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

.method public abstract setValidationProblemHandler(Lorg/codehaus/stax2/validation/ValidationProblemHandler;)Lorg/codehaus/stax2/validation/ValidationProblemHandler;
.end method

.method public skipElement()V
    .locals 4

    invoke-interface {p0}, Ljavax/xml/stream/XMLStreamReader;->getEventType()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/Stax2ReaderImpl;->throwNotStartElem()V

    :cond_0
    move v0, v1

    :cond_1
    :goto_0
    invoke-interface {p0}, Ljavax/xml/stream/XMLStreamReader;->next()I

    move-result v2

    if-ne v2, v1, :cond_2

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    const/4 v3, 0x2

    if-ne v2, v3, :cond_1

    add-int/lit8 v0, v0, -0x1

    if-nez v0, :cond_1

    return-void
.end method

.method public stopValidatingAgainst(Lorg/codehaus/stax2/validation/XMLValidationSchema;)Lorg/codehaus/stax2/validation/XMLValidator;
    .locals 0

    .line 1
    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/Stax2ReaderImpl;->throwUnsupported()V

    const/4 p1, 0x0

    return-object p1
.end method

.method public stopValidatingAgainst(Lorg/codehaus/stax2/validation/XMLValidator;)Lorg/codehaus/stax2/validation/XMLValidator;
    .locals 0

    .line 2
    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/Stax2ReaderImpl;->throwUnsupported()V

    const/4 p1, 0x0

    return-object p1
.end method

.method public throwNotStartElem()V
    .locals 2

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Current state not START_ELEMENT"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public throwUnsupported()V
    .locals 2

    new-instance v0, Ljavax/xml/stream/XMLStreamException;

    const-string v1, "Unsupported method"

    invoke-direct {v0, v1}, Ljavax/xml/stream/XMLStreamException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public validateAgainst(Lorg/codehaus/stax2/validation/XMLValidationSchema;)Lorg/codehaus/stax2/validation/XMLValidator;
    .locals 0

    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/Stax2ReaderImpl;->throwUnsupported()V

    const/4 p1, 0x0

    return-object p1
.end method

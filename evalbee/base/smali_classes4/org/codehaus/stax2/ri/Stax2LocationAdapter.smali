.class public Lorg/codehaus/stax2/ri/Stax2LocationAdapter;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/codehaus/stax2/XMLStreamLocation2;


# instance fields
.field protected final mParentLocation:Ljavax/xml/stream/Location;

.field protected final mWrappedLocation:Ljavax/xml/stream/Location;


# direct methods
.method public constructor <init>(Ljavax/xml/stream/Location;)V
    .locals 1

    .line 1
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lorg/codehaus/stax2/ri/Stax2LocationAdapter;-><init>(Ljavax/xml/stream/Location;Ljavax/xml/stream/Location;)V

    return-void
.end method

.method public constructor <init>(Ljavax/xml/stream/Location;Ljavax/xml/stream/Location;)V
    .locals 0

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lorg/codehaus/stax2/ri/Stax2LocationAdapter;->mWrappedLocation:Ljavax/xml/stream/Location;

    iput-object p2, p0, Lorg/codehaus/stax2/ri/Stax2LocationAdapter;->mParentLocation:Ljavax/xml/stream/Location;

    return-void
.end method


# virtual methods
.method public getCharacterOffset()I
    .locals 1

    iget-object v0, p0, Lorg/codehaus/stax2/ri/Stax2LocationAdapter;->mWrappedLocation:Ljavax/xml/stream/Location;

    invoke-interface {v0}, Ljavax/xml/stream/Location;->getCharacterOffset()I

    move-result v0

    return v0
.end method

.method public getColumnNumber()I
    .locals 1

    iget-object v0, p0, Lorg/codehaus/stax2/ri/Stax2LocationAdapter;->mWrappedLocation:Ljavax/xml/stream/Location;

    invoke-interface {v0}, Ljavax/xml/stream/Location;->getColumnNumber()I

    move-result v0

    return v0
.end method

.method public getContext()Lorg/codehaus/stax2/XMLStreamLocation2;
    .locals 2

    iget-object v0, p0, Lorg/codehaus/stax2/ri/Stax2LocationAdapter;->mParentLocation:Ljavax/xml/stream/Location;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    :cond_0
    instance-of v1, v0, Lorg/codehaus/stax2/XMLStreamLocation2;

    if-eqz v1, :cond_1

    check-cast v0, Lorg/codehaus/stax2/XMLStreamLocation2;

    return-object v0

    :cond_1
    new-instance v1, Lorg/codehaus/stax2/ri/Stax2LocationAdapter;

    invoke-direct {v1, v0}, Lorg/codehaus/stax2/ri/Stax2LocationAdapter;-><init>(Ljavax/xml/stream/Location;)V

    return-object v1
.end method

.method public getLineNumber()I
    .locals 1

    iget-object v0, p0, Lorg/codehaus/stax2/ri/Stax2LocationAdapter;->mWrappedLocation:Ljavax/xml/stream/Location;

    invoke-interface {v0}, Ljavax/xml/stream/Location;->getLineNumber()I

    move-result v0

    return v0
.end method

.method public getPublicId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/codehaus/stax2/ri/Stax2LocationAdapter;->mWrappedLocation:Ljavax/xml/stream/Location;

    invoke-interface {v0}, Ljavax/xml/stream/Location;->getPublicId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSystemId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/codehaus/stax2/ri/Stax2LocationAdapter;->mWrappedLocation:Ljavax/xml/stream/Location;

    invoke-interface {v0}, Ljavax/xml/stream/Location;->getSystemId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

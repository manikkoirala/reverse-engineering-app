.class public Lorg/codehaus/stax2/ri/Stax2WriterAdapter;
.super Lorg/codehaus/stax2/util/StreamWriterDelegate;
.source "SourceFile"

# interfaces
.implements Lorg/codehaus/stax2/XMLStreamWriter2;
.implements Ljavax/xml/stream/XMLStreamConstants;


# instance fields
.field protected mEncoding:Ljava/lang/String;

.field protected final mNsRepairing:Z

.field protected mValueEncoder:Lorg/codehaus/stax2/ri/typed/SimpleValueEncoder;


# direct methods
.method public constructor <init>(Ljavax/xml/stream/XMLStreamWriter;)V
    .locals 1

    invoke-direct {p0, p1}, Lorg/codehaus/stax2/util/StreamWriterDelegate;-><init>(Ljavax/xml/stream/XMLStreamWriter;)V

    iput-object p1, p0, Lorg/codehaus/stax2/util/StreamWriterDelegate;->mDelegate:Ljavax/xml/stream/XMLStreamWriter;

    const-string v0, "javax.xml.stream.isRepairingNamespaces"

    invoke-interface {p1, v0}, Ljavax/xml/stream/XMLStreamWriter;->getProperty(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    instance-of v0, p1, Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    iput-boolean p1, p0, Lorg/codehaus/stax2/ri/Stax2WriterAdapter;->mNsRepairing:Z

    return-void
.end method

.method public static wrapIfNecessary(Ljavax/xml/stream/XMLStreamWriter;)Lorg/codehaus/stax2/XMLStreamWriter2;
    .locals 1

    instance-of v0, p0, Lorg/codehaus/stax2/XMLStreamWriter2;

    if-eqz v0, :cond_0

    check-cast p0, Lorg/codehaus/stax2/XMLStreamWriter2;

    return-object p0

    :cond_0
    new-instance v0, Lorg/codehaus/stax2/ri/Stax2WriterAdapter;

    invoke-direct {v0, p0}, Lorg/codehaus/stax2/ri/Stax2WriterAdapter;-><init>(Ljavax/xml/stream/XMLStreamWriter;)V

    return-object v0
.end method


# virtual methods
.method public closeCompletely()V
    .locals 0

    invoke-virtual {p0}, Lorg/codehaus/stax2/util/StreamWriterDelegate;->close()V

    return-void
.end method

.method public copyEventFromReader(Lorg/codehaus/stax2/XMLStreamReader2;Z)V
    .locals 2

    invoke-interface {p1}, Ljavax/xml/stream/XMLStreamReader;->getEventType()I

    move-result p2

    packed-switch p2, :pswitch_data_0

    :pswitch_0
    new-instance p2, Ljavax/xml/stream/XMLStreamException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unrecognized event type ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {p1}, Ljavax/xml/stream/XMLStreamReader;->getEventType()I

    move-result p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, "); not sure how to copy"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljavax/xml/stream/XMLStreamException;-><init>(Ljava/lang/String;)V

    throw p2

    :pswitch_1
    invoke-interface {p1}, Ljavax/xml/stream/XMLStreamReader;->getTextCharacters()[C

    move-result-object p2

    invoke-interface {p1}, Ljavax/xml/stream/XMLStreamReader;->getTextStart()I

    move-result v0

    invoke-interface {p1}, Ljavax/xml/stream/XMLStreamReader;->getTextLength()I

    move-result p1

    invoke-virtual {p0, p2, v0, p1}, Lorg/codehaus/stax2/ri/Stax2WriterAdapter;->writeCData([CII)V

    return-void

    :pswitch_2
    invoke-interface {p1}, Lorg/codehaus/stax2/XMLStreamReader2;->getDTDInfo()Lorg/codehaus/stax2/DTDInfo;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-interface {p1}, Lorg/codehaus/stax2/DTDInfo;->getDTDRootName()Ljava/lang/String;

    move-result-object p2

    invoke-interface {p1}, Lorg/codehaus/stax2/DTDInfo;->getDTDSystemId()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1}, Lorg/codehaus/stax2/DTDInfo;->getDTDPublicId()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1}, Lorg/codehaus/stax2/DTDInfo;->getDTDInternalSubset()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p2, v0, v1, p1}, Lorg/codehaus/stax2/ri/Stax2WriterAdapter;->writeDTD(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :cond_0
    new-instance p1, Ljavax/xml/stream/XMLStreamException;

    const-string p2, "Current state DOCTYPE, but not DTDInfo Object returned -- reader doesn\'t support DTDs?"

    invoke-direct {p1, p2}, Ljavax/xml/stream/XMLStreamException;-><init>(Ljava/lang/String;)V

    throw p1

    :pswitch_3
    invoke-interface {p1}, Ljavax/xml/stream/XMLStreamReader;->getLocalName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lorg/codehaus/stax2/util/StreamWriterDelegate;->writeEntityRef(Ljava/lang/String;)V

    return-void

    :pswitch_4
    invoke-virtual {p0}, Lorg/codehaus/stax2/util/StreamWriterDelegate;->writeEndDocument()V

    return-void

    :pswitch_5
    invoke-interface {p1}, Ljavax/xml/stream/XMLStreamReader;->getVersion()Ljava/lang/String;

    move-result-object p2

    if-eqz p2, :cond_3

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result p2

    if-nez p2, :cond_1

    goto :goto_0

    :cond_1
    invoke-interface {p1}, Ljavax/xml/stream/XMLStreamReader;->standaloneSet()Z

    move-result p2

    if-eqz p2, :cond_2

    invoke-interface {p1}, Ljavax/xml/stream/XMLStreamReader;->getVersion()Ljava/lang/String;

    move-result-object p2

    invoke-interface {p1}, Ljavax/xml/stream/XMLStreamReader;->getCharacterEncodingScheme()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1}, Ljavax/xml/stream/XMLStreamReader;->isStandalone()Z

    move-result p1

    invoke-virtual {p0, p2, v0, p1}, Lorg/codehaus/stax2/ri/Stax2WriterAdapter;->writeStartDocument(Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_0

    :cond_2
    invoke-interface {p1}, Ljavax/xml/stream/XMLStreamReader;->getCharacterEncodingScheme()Ljava/lang/String;

    move-result-object p2

    invoke-interface {p1}, Ljavax/xml/stream/XMLStreamReader;->getVersion()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p2, p1}, Lorg/codehaus/stax2/util/StreamWriterDelegate;->writeStartDocument(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    :goto_0
    return-void

    :pswitch_6
    invoke-interface {p1}, Ljavax/xml/stream/XMLStreamReader;->getTextCharacters()[C

    move-result-object p2

    invoke-interface {p1}, Ljavax/xml/stream/XMLStreamReader;->getTextStart()I

    move-result v0

    invoke-interface {p1}, Ljavax/xml/stream/XMLStreamReader;->getTextLength()I

    move-result p1

    invoke-virtual {p0, p2, v0, p1}, Lorg/codehaus/stax2/ri/Stax2WriterAdapter;->writeSpace([CII)V

    return-void

    :pswitch_7
    invoke-interface {p1}, Ljavax/xml/stream/XMLStreamReader;->getText()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lorg/codehaus/stax2/util/StreamWriterDelegate;->writeComment(Ljava/lang/String;)V

    return-void

    :pswitch_8
    invoke-interface {p1}, Ljavax/xml/stream/XMLStreamReader;->getTextCharacters()[C

    move-result-object p2

    invoke-interface {p1}, Ljavax/xml/stream/XMLStreamReader;->getTextStart()I

    move-result v0

    invoke-interface {p1}, Ljavax/xml/stream/XMLStreamReader;->getTextLength()I

    move-result p1

    invoke-virtual {p0, p2, v0, p1}, Lorg/codehaus/stax2/util/StreamWriterDelegate;->writeCharacters([CII)V

    return-void

    :pswitch_9
    invoke-interface {p1}, Ljavax/xml/stream/XMLStreamReader;->getPITarget()Ljava/lang/String;

    move-result-object p2

    invoke-interface {p1}, Ljavax/xml/stream/XMLStreamReader;->getPIData()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p2, p1}, Lorg/codehaus/stax2/util/StreamWriterDelegate;->writeProcessingInstruction(Ljava/lang/String;Ljava/lang/String;)V

    return-void

    :pswitch_a
    invoke-virtual {p0}, Lorg/codehaus/stax2/util/StreamWriterDelegate;->writeEndElement()V

    return-void

    :pswitch_b
    invoke-virtual {p0, p1}, Lorg/codehaus/stax2/ri/Stax2WriterAdapter;->copyStartElement(Ljavax/xml/stream/XMLStreamReader;)V

    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public copyStartElement(Ljavax/xml/stream/XMLStreamReader;)V
    .locals 6

    invoke-interface {p1}, Ljavax/xml/stream/XMLStreamReader;->getNamespaceCount()I

    move-result v0

    const/4 v1, 0x0

    if-lez v0, :cond_2

    move v2, v1

    :goto_0
    if-ge v2, v0, :cond_2

    invoke-interface {p1, v2}, Ljavax/xml/stream/XMLStreamReader;->getNamespacePrefix(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1, v2}, Ljavax/xml/stream/XMLStreamReader;->getNamespaceURI(I)Ljava/lang/String;

    move-result-object v4

    if-eqz v3, :cond_1

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v5

    if-nez v5, :cond_0

    goto :goto_1

    :cond_0
    invoke-virtual {p0, v3, v4}, Lorg/codehaus/stax2/util/StreamWriterDelegate;->setPrefix(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    :cond_1
    :goto_1
    invoke-virtual {p0, v4}, Lorg/codehaus/stax2/util/StreamWriterDelegate;->setDefaultNamespace(Ljava/lang/String;)V

    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    invoke-interface {p1}, Ljavax/xml/stream/XMLStreamReader;->getPrefix()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1}, Ljavax/xml/stream/XMLStreamReader;->getLocalName()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1}, Ljavax/xml/stream/XMLStreamReader;->getNamespaceURI()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v2, v3, v4}, Lorg/codehaus/stax2/util/StreamWriterDelegate;->writeStartElement(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    if-lez v0, :cond_5

    move v2, v1

    :goto_3
    if-ge v2, v0, :cond_5

    invoke-interface {p1, v2}, Ljavax/xml/stream/XMLStreamReader;->getNamespacePrefix(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1, v2}, Ljavax/xml/stream/XMLStreamReader;->getNamespaceURI(I)Ljava/lang/String;

    move-result-object v4

    if-eqz v3, :cond_4

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v5

    if-nez v5, :cond_3

    goto :goto_4

    :cond_3
    invoke-virtual {p0, v3, v4}, Lorg/codehaus/stax2/util/StreamWriterDelegate;->writeNamespace(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_5

    :cond_4
    :goto_4
    invoke-virtual {p0, v4}, Lorg/codehaus/stax2/util/StreamWriterDelegate;->writeDefaultNamespace(Ljava/lang/String;)V

    :goto_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_5
    invoke-interface {p1}, Ljavax/xml/stream/XMLStreamReader;->getAttributeCount()I

    move-result v0

    if-lez v0, :cond_6

    :goto_6
    if-ge v1, v0, :cond_6

    invoke-interface {p1, v1}, Ljavax/xml/stream/XMLStreamReader;->getAttributePrefix(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, v1}, Ljavax/xml/stream/XMLStreamReader;->getAttributeNamespace(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1, v1}, Ljavax/xml/stream/XMLStreamReader;->getAttributeLocalName(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {p1, v1}, Ljavax/xml/stream/XMLStreamReader;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v2, v3, v4, v5}, Lorg/codehaus/stax2/util/StreamWriterDelegate;->writeAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    :cond_6
    return-void
.end method

.method public getEncoding()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/codehaus/stax2/ri/Stax2WriterAdapter;->mEncoding:Ljava/lang/String;

    return-object v0
.end method

.method public getLocation()Lorg/codehaus/stax2/XMLStreamLocation2;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getValueEncoder()Lorg/codehaus/stax2/ri/typed/SimpleValueEncoder;
    .locals 1

    iget-object v0, p0, Lorg/codehaus/stax2/ri/Stax2WriterAdapter;->mValueEncoder:Lorg/codehaus/stax2/ri/typed/SimpleValueEncoder;

    if-nez v0, :cond_0

    new-instance v0, Lorg/codehaus/stax2/ri/typed/SimpleValueEncoder;

    invoke-direct {v0}, Lorg/codehaus/stax2/ri/typed/SimpleValueEncoder;-><init>()V

    iput-object v0, p0, Lorg/codehaus/stax2/ri/Stax2WriterAdapter;->mValueEncoder:Lorg/codehaus/stax2/ri/typed/SimpleValueEncoder;

    :cond_0
    iget-object v0, p0, Lorg/codehaus/stax2/ri/Stax2WriterAdapter;->mValueEncoder:Lorg/codehaus/stax2/ri/typed/SimpleValueEncoder;

    return-object v0
.end method

.method public isPropertySupported(Ljava/lang/String;)Z
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

.method public serializeQNameValue(Ljavax/xml/namespace/QName;)Ljava/lang/String;
    .locals 3

    iget-boolean v0, p0, Lorg/codehaus/stax2/ri/Stax2WriterAdapter;->mNsRepairing:Z

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Ljavax/xml/namespace/QName;->getNamespaceURI()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lorg/codehaus/stax2/util/StreamWriterDelegate;->getNamespaceContext()Ljavax/xml/namespace/NamespaceContext;

    move-result-object v1

    if-nez v1, :cond_0

    const/4 v1, 0x0

    goto :goto_0

    :cond_0
    invoke-interface {v1, v0}, Ljavax/xml/namespace/NamespaceContext;->getPrefix(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :goto_0
    if-nez v1, :cond_4

    invoke-virtual {p1}, Ljavax/xml/namespace/QName;->getPrefix()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_1

    goto :goto_1

    :cond_1
    invoke-virtual {p0, v1, v0}, Lorg/codehaus/stax2/util/StreamWriterDelegate;->writeNamespace(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    :cond_2
    :goto_1
    invoke-virtual {p0, v0}, Lorg/codehaus/stax2/util/StreamWriterDelegate;->writeDefaultNamespace(Ljava/lang/String;)V

    const-string v1, ""

    goto :goto_2

    :cond_3
    invoke-virtual {p1}, Ljavax/xml/namespace/QName;->getPrefix()Ljava/lang/String;

    move-result-object v1

    :cond_4
    :goto_2
    invoke-virtual {p1}, Ljavax/xml/namespace/QName;->getLocalPart()Ljava/lang/String;

    move-result-object p1

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_5

    goto :goto_3

    :cond_5
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    :cond_6
    :goto_3
    return-object p1
.end method

.method public setProperty(Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 2

    new-instance p2, Ljava/lang/IllegalArgumentException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "No settable property \'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "\'"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p2
.end method

.method public setValidationProblemHandler(Lorg/codehaus/stax2/validation/ValidationProblemHandler;)Lorg/codehaus/stax2/validation/ValidationProblemHandler;
    .locals 0

    const/4 p1, 0x0

    return-object p1
.end method

.method public stopValidatingAgainst(Lorg/codehaus/stax2/validation/XMLValidationSchema;)Lorg/codehaus/stax2/validation/XMLValidator;
    .locals 0

    .line 1
    const/4 p1, 0x0

    return-object p1
.end method

.method public stopValidatingAgainst(Lorg/codehaus/stax2/validation/XMLValidator;)Lorg/codehaus/stax2/validation/XMLValidator;
    .locals 0

    .line 2
    const/4 p1, 0x0

    return-object p1
.end method

.method public validateAgainst(Lorg/codehaus/stax2/validation/XMLValidationSchema;)Lorg/codehaus/stax2/validation/XMLValidator;
    .locals 1

    new-instance p1, Ljava/lang/UnsupportedOperationException;

    const-string v0, "Not yet implemented"

    invoke-direct {p1, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public writeBinary(Lorg/codehaus/stax2/typed/Base64Variant;[BII)V
    .locals 2

    .line 1
    iget-object v0, p0, Lorg/codehaus/stax2/util/StreamWriterDelegate;->mDelegate:Ljavax/xml/stream/XMLStreamWriter;

    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/Stax2WriterAdapter;->getValueEncoder()Lorg/codehaus/stax2/ri/typed/SimpleValueEncoder;

    move-result-object v1

    invoke-virtual {v1, p1, p2, p3, p4}, Lorg/codehaus/stax2/ri/typed/SimpleValueEncoder;->encodeAsString(Lorg/codehaus/stax2/typed/Base64Variant;[BII)Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1}, Ljavax/xml/stream/XMLStreamWriter;->writeCharacters(Ljava/lang/String;)V

    return-void
.end method

.method public writeBinary([BII)V
    .locals 1

    .line 2
    invoke-static {}, Lorg/codehaus/stax2/typed/Base64Variants;->getDefaultVariant()Lorg/codehaus/stax2/typed/Base64Variant;

    move-result-object v0

    invoke-virtual {p0, v0, p1, p2, p3}, Lorg/codehaus/stax2/ri/Stax2WriterAdapter;->writeBinary(Lorg/codehaus/stax2/typed/Base64Variant;[BII)V

    return-void
.end method

.method public writeBinaryAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[B)V
    .locals 6

    .line 1
    invoke-static {}, Lorg/codehaus/stax2/typed/Base64Variants;->getDefaultVariant()Lorg/codehaus/stax2/typed/Base64Variant;

    move-result-object v1

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-virtual/range {v0 .. v5}, Lorg/codehaus/stax2/ri/Stax2WriterAdapter;->writeBinaryAttribute(Lorg/codehaus/stax2/typed/Base64Variant;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[B)V

    return-void
.end method

.method public writeBinaryAttribute(Lorg/codehaus/stax2/typed/Base64Variant;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[B)V
    .locals 4

    .line 2
    iget-object v0, p0, Lorg/codehaus/stax2/util/StreamWriterDelegate;->mDelegate:Ljavax/xml/stream/XMLStreamWriter;

    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/Stax2WriterAdapter;->getValueEncoder()Lorg/codehaus/stax2/ri/typed/SimpleValueEncoder;

    move-result-object v1

    array-length v2, p5

    const/4 v3, 0x0

    invoke-virtual {v1, p1, p5, v3, v2}, Lorg/codehaus/stax2/ri/typed/SimpleValueEncoder;->encodeAsString(Lorg/codehaus/stax2/typed/Base64Variant;[BII)Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p2, p3, p4, p1}, Ljavax/xml/stream/XMLStreamWriter;->writeAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public writeBoolean(Z)V
    .locals 1

    iget-object v0, p0, Lorg/codehaus/stax2/util/StreamWriterDelegate;->mDelegate:Ljavax/xml/stream/XMLStreamWriter;

    if-eqz p1, :cond_0

    const-string p1, "true"

    goto :goto_0

    :cond_0
    const-string p1, "false"

    :goto_0
    invoke-interface {v0, p1}, Ljavax/xml/stream/XMLStreamWriter;->writeCharacters(Ljava/lang/String;)V

    return-void
.end method

.method public writeBooleanAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 1

    iget-object v0, p0, Lorg/codehaus/stax2/util/StreamWriterDelegate;->mDelegate:Ljavax/xml/stream/XMLStreamWriter;

    if-eqz p4, :cond_0

    const-string p4, "true"

    goto :goto_0

    :cond_0
    const-string p4, "false"

    :goto_0
    invoke-interface {v0, p1, p2, p3, p4}, Ljavax/xml/stream/XMLStreamWriter;->writeAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public writeCData([CII)V
    .locals 1

    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, p1, p2, p3}, Ljava/lang/String;-><init>([CII)V

    invoke-virtual {p0, v0}, Lorg/codehaus/stax2/util/StreamWriterDelegate;->writeCData(Ljava/lang/String;)V

    return-void
.end method

.method public writeDTD(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    const-string v1, "<!DOCTYPE"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    if-eqz p2, :cond_1

    if-eqz p3, :cond_0

    const-string p1, " PUBLIC \""

    invoke-virtual {v0, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-virtual {v0, p3}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string p1, "\" \""

    goto :goto_0

    :cond_0
    const-string p1, " SYSTEM \""

    :goto_0
    invoke-virtual {v0, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const/16 p1, 0x22

    invoke-virtual {v0, p1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    :cond_1
    if-eqz p4, :cond_2

    invoke-virtual {p4}, Ljava/lang/String;->length()I

    move-result p1

    if-lez p1, :cond_2

    const-string p1, " ["

    invoke-virtual {v0, p1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-virtual {v0, p4}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const/16 p1, 0x5d

    invoke-virtual {v0, p1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    :cond_2
    const/16 p1, 0x3e

    invoke-virtual {v0, p1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lorg/codehaus/stax2/util/StreamWriterDelegate;->writeDTD(Ljava/lang/String;)V

    return-void
.end method

.method public writeDecimal(Ljava/math/BigDecimal;)V
    .locals 1

    iget-object v0, p0, Lorg/codehaus/stax2/util/StreamWriterDelegate;->mDelegate:Ljavax/xml/stream/XMLStreamWriter;

    invoke-virtual {p1}, Ljava/math/BigDecimal;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1}, Ljavax/xml/stream/XMLStreamWriter;->writeCharacters(Ljava/lang/String;)V

    return-void
.end method

.method public writeDecimalAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/math/BigDecimal;)V
    .locals 1

    iget-object v0, p0, Lorg/codehaus/stax2/util/StreamWriterDelegate;->mDelegate:Ljavax/xml/stream/XMLStreamWriter;

    invoke-virtual {p4}, Ljava/math/BigDecimal;->toString()Ljava/lang/String;

    move-result-object p4

    invoke-interface {v0, p1, p2, p3, p4}, Ljavax/xml/stream/XMLStreamWriter;->writeAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public writeDouble(D)V
    .locals 1

    iget-object v0, p0, Lorg/codehaus/stax2/util/StreamWriterDelegate;->mDelegate:Ljavax/xml/stream/XMLStreamWriter;

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1}, Ljavax/xml/stream/XMLStreamWriter;->writeCharacters(Ljava/lang/String;)V

    return-void
.end method

.method public writeDoubleArray([DII)V
    .locals 2

    iget-object v0, p0, Lorg/codehaus/stax2/util/StreamWriterDelegate;->mDelegate:Ljavax/xml/stream/XMLStreamWriter;

    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/Stax2WriterAdapter;->getValueEncoder()Lorg/codehaus/stax2/ri/typed/SimpleValueEncoder;

    move-result-object v1

    invoke-virtual {v1, p1, p2, p3}, Lorg/codehaus/stax2/ri/typed/SimpleValueEncoder;->encodeAsString([DII)Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1}, Ljavax/xml/stream/XMLStreamWriter;->writeCharacters(Ljava/lang/String;)V

    return-void
.end method

.method public writeDoubleArrayAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[D)V
    .locals 4

    iget-object v0, p0, Lorg/codehaus/stax2/util/StreamWriterDelegate;->mDelegate:Ljavax/xml/stream/XMLStreamWriter;

    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/Stax2WriterAdapter;->getValueEncoder()Lorg/codehaus/stax2/ri/typed/SimpleValueEncoder;

    move-result-object v1

    array-length v2, p4

    const/4 v3, 0x0

    invoke-virtual {v1, p4, v3, v2}, Lorg/codehaus/stax2/ri/typed/SimpleValueEncoder;->encodeAsString([DII)Ljava/lang/String;

    move-result-object p4

    invoke-interface {v0, p1, p2, p3, p4}, Ljavax/xml/stream/XMLStreamWriter;->writeAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public writeDoubleAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;D)V
    .locals 1

    iget-object v0, p0, Lorg/codehaus/stax2/util/StreamWriterDelegate;->mDelegate:Ljavax/xml/stream/XMLStreamWriter;

    invoke-static {p4, p5}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object p4

    invoke-interface {v0, p1, p2, p3, p4}, Ljavax/xml/stream/XMLStreamWriter;->writeAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public writeFloat(F)V
    .locals 1

    iget-object v0, p0, Lorg/codehaus/stax2/util/StreamWriterDelegate;->mDelegate:Ljavax/xml/stream/XMLStreamWriter;

    invoke-static {p1}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1}, Ljavax/xml/stream/XMLStreamWriter;->writeCharacters(Ljava/lang/String;)V

    return-void
.end method

.method public writeFloatArray([FII)V
    .locals 2

    iget-object v0, p0, Lorg/codehaus/stax2/util/StreamWriterDelegate;->mDelegate:Ljavax/xml/stream/XMLStreamWriter;

    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/Stax2WriterAdapter;->getValueEncoder()Lorg/codehaus/stax2/ri/typed/SimpleValueEncoder;

    move-result-object v1

    invoke-virtual {v1, p1, p2, p3}, Lorg/codehaus/stax2/ri/typed/SimpleValueEncoder;->encodeAsString([FII)Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1}, Ljavax/xml/stream/XMLStreamWriter;->writeCharacters(Ljava/lang/String;)V

    return-void
.end method

.method public writeFloatArrayAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[F)V
    .locals 4

    iget-object v0, p0, Lorg/codehaus/stax2/util/StreamWriterDelegate;->mDelegate:Ljavax/xml/stream/XMLStreamWriter;

    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/Stax2WriterAdapter;->getValueEncoder()Lorg/codehaus/stax2/ri/typed/SimpleValueEncoder;

    move-result-object v1

    array-length v2, p4

    const/4 v3, 0x0

    invoke-virtual {v1, p4, v3, v2}, Lorg/codehaus/stax2/ri/typed/SimpleValueEncoder;->encodeAsString([FII)Ljava/lang/String;

    move-result-object p4

    invoke-interface {v0, p1, p2, p3, p4}, Ljavax/xml/stream/XMLStreamWriter;->writeAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public writeFloatAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;F)V
    .locals 1

    iget-object v0, p0, Lorg/codehaus/stax2/util/StreamWriterDelegate;->mDelegate:Ljavax/xml/stream/XMLStreamWriter;

    invoke-static {p4}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object p4

    invoke-interface {v0, p1, p2, p3, p4}, Ljavax/xml/stream/XMLStreamWriter;->writeAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public writeFullEndElement()V
    .locals 2

    iget-object v0, p0, Lorg/codehaus/stax2/util/StreamWriterDelegate;->mDelegate:Ljavax/xml/stream/XMLStreamWriter;

    const-string v1, ""

    invoke-interface {v0, v1}, Ljavax/xml/stream/XMLStreamWriter;->writeCharacters(Ljava/lang/String;)V

    iget-object v0, p0, Lorg/codehaus/stax2/util/StreamWriterDelegate;->mDelegate:Ljavax/xml/stream/XMLStreamWriter;

    invoke-interface {v0}, Ljavax/xml/stream/XMLStreamWriter;->writeEndElement()V

    return-void
.end method

.method public writeInt(I)V
    .locals 1

    iget-object v0, p0, Lorg/codehaus/stax2/util/StreamWriterDelegate;->mDelegate:Ljavax/xml/stream/XMLStreamWriter;

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1}, Ljavax/xml/stream/XMLStreamWriter;->writeCharacters(Ljava/lang/String;)V

    return-void
.end method

.method public writeIntArray([III)V
    .locals 2

    iget-object v0, p0, Lorg/codehaus/stax2/util/StreamWriterDelegate;->mDelegate:Ljavax/xml/stream/XMLStreamWriter;

    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/Stax2WriterAdapter;->getValueEncoder()Lorg/codehaus/stax2/ri/typed/SimpleValueEncoder;

    move-result-object v1

    invoke-virtual {v1, p1, p2, p3}, Lorg/codehaus/stax2/ri/typed/SimpleValueEncoder;->encodeAsString([III)Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1}, Ljavax/xml/stream/XMLStreamWriter;->writeCharacters(Ljava/lang/String;)V

    return-void
.end method

.method public writeIntArrayAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[I)V
    .locals 4

    iget-object v0, p0, Lorg/codehaus/stax2/util/StreamWriterDelegate;->mDelegate:Ljavax/xml/stream/XMLStreamWriter;

    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/Stax2WriterAdapter;->getValueEncoder()Lorg/codehaus/stax2/ri/typed/SimpleValueEncoder;

    move-result-object v1

    array-length v2, p4

    const/4 v3, 0x0

    invoke-virtual {v1, p4, v3, v2}, Lorg/codehaus/stax2/ri/typed/SimpleValueEncoder;->encodeAsString([III)Ljava/lang/String;

    move-result-object p4

    invoke-interface {v0, p1, p2, p3, p4}, Ljavax/xml/stream/XMLStreamWriter;->writeAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public writeIntAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 1

    iget-object v0, p0, Lorg/codehaus/stax2/util/StreamWriterDelegate;->mDelegate:Ljavax/xml/stream/XMLStreamWriter;

    invoke-static {p4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p4

    invoke-interface {v0, p1, p2, p3, p4}, Ljavax/xml/stream/XMLStreamWriter;->writeAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public writeInteger(Ljava/math/BigInteger;)V
    .locals 1

    iget-object v0, p0, Lorg/codehaus/stax2/util/StreamWriterDelegate;->mDelegate:Ljavax/xml/stream/XMLStreamWriter;

    invoke-virtual {p1}, Ljava/math/BigInteger;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1}, Ljavax/xml/stream/XMLStreamWriter;->writeCharacters(Ljava/lang/String;)V

    return-void
.end method

.method public writeIntegerAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/math/BigInteger;)V
    .locals 1

    iget-object v0, p0, Lorg/codehaus/stax2/util/StreamWriterDelegate;->mDelegate:Ljavax/xml/stream/XMLStreamWriter;

    invoke-virtual {p4}, Ljava/math/BigInteger;->toString()Ljava/lang/String;

    move-result-object p4

    invoke-interface {v0, p1, p2, p3, p4}, Ljavax/xml/stream/XMLStreamWriter;->writeAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public writeLong(J)V
    .locals 1

    iget-object v0, p0, Lorg/codehaus/stax2/util/StreamWriterDelegate;->mDelegate:Ljavax/xml/stream/XMLStreamWriter;

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1}, Ljavax/xml/stream/XMLStreamWriter;->writeCharacters(Ljava/lang/String;)V

    return-void
.end method

.method public writeLongArray([JII)V
    .locals 2

    iget-object v0, p0, Lorg/codehaus/stax2/util/StreamWriterDelegate;->mDelegate:Ljavax/xml/stream/XMLStreamWriter;

    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/Stax2WriterAdapter;->getValueEncoder()Lorg/codehaus/stax2/ri/typed/SimpleValueEncoder;

    move-result-object v1

    invoke-virtual {v1, p1, p2, p3}, Lorg/codehaus/stax2/ri/typed/SimpleValueEncoder;->encodeAsString([JII)Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1}, Ljavax/xml/stream/XMLStreamWriter;->writeCharacters(Ljava/lang/String;)V

    return-void
.end method

.method public writeLongArrayAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[J)V
    .locals 4

    iget-object v0, p0, Lorg/codehaus/stax2/util/StreamWriterDelegate;->mDelegate:Ljavax/xml/stream/XMLStreamWriter;

    invoke-virtual {p0}, Lorg/codehaus/stax2/ri/Stax2WriterAdapter;->getValueEncoder()Lorg/codehaus/stax2/ri/typed/SimpleValueEncoder;

    move-result-object v1

    array-length v2, p4

    const/4 v3, 0x0

    invoke-virtual {v1, p4, v3, v2}, Lorg/codehaus/stax2/ri/typed/SimpleValueEncoder;->encodeAsString([JII)Ljava/lang/String;

    move-result-object p4

    invoke-interface {v0, p1, p2, p3, p4}, Ljavax/xml/stream/XMLStreamWriter;->writeAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public writeLongAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V
    .locals 1

    iget-object v0, p0, Lorg/codehaus/stax2/util/StreamWriterDelegate;->mDelegate:Ljavax/xml/stream/XMLStreamWriter;

    invoke-static {p4, p5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object p4

    invoke-interface {v0, p1, p2, p3, p4}, Ljavax/xml/stream/XMLStreamWriter;->writeAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public writeQName(Ljavax/xml/namespace/QName;)V
    .locals 1

    iget-object v0, p0, Lorg/codehaus/stax2/util/StreamWriterDelegate;->mDelegate:Ljavax/xml/stream/XMLStreamWriter;

    invoke-virtual {p0, p1}, Lorg/codehaus/stax2/ri/Stax2WriterAdapter;->serializeQNameValue(Ljavax/xml/namespace/QName;)Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1}, Ljavax/xml/stream/XMLStreamWriter;->writeCharacters(Ljava/lang/String;)V

    return-void
.end method

.method public writeQNameAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljavax/xml/namespace/QName;)V
    .locals 1

    iget-object v0, p0, Lorg/codehaus/stax2/util/StreamWriterDelegate;->mDelegate:Ljavax/xml/stream/XMLStreamWriter;

    invoke-virtual {p0, p4}, Lorg/codehaus/stax2/ri/Stax2WriterAdapter;->serializeQNameValue(Ljavax/xml/namespace/QName;)Ljava/lang/String;

    move-result-object p4

    invoke-interface {v0, p1, p2, p3, p4}, Ljavax/xml/stream/XMLStreamWriter;->writeAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public writeRaw(Ljava/lang/String;)V
    .locals 2

    .line 1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v1, v0}, Lorg/codehaus/stax2/ri/Stax2WriterAdapter;->writeRaw(Ljava/lang/String;II)V

    return-void
.end method

.method public writeRaw(Ljava/lang/String;II)V
    .locals 0

    .line 2
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    const-string p2, "Not implemented"

    invoke-direct {p1, p2}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public writeRaw([CII)V
    .locals 1

    .line 3
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, p1, p2, p3}, Ljava/lang/String;-><init>([CII)V

    invoke-virtual {p0, v0}, Lorg/codehaus/stax2/ri/Stax2WriterAdapter;->writeRaw(Ljava/lang/String;)V

    return-void
.end method

.method public writeSpace(Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lorg/codehaus/stax2/ri/Stax2WriterAdapter;->writeRaw(Ljava/lang/String;)V

    return-void
.end method

.method public writeSpace([CII)V
    .locals 0

    .line 2
    invoke-virtual {p0, p1, p2, p3}, Lorg/codehaus/stax2/ri/Stax2WriterAdapter;->writeRaw([CII)V

    return-void
.end method

.method public writeStartDocument(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 0

    invoke-virtual {p0, p2, p1}, Lorg/codehaus/stax2/util/StreamWriterDelegate;->writeStartDocument(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

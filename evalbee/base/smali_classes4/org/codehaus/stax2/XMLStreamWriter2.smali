.class public interface abstract Lorg/codehaus/stax2/XMLStreamWriter2;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/codehaus/stax2/typed/TypedXMLStreamWriter;
.implements Lorg/codehaus/stax2/validation/Validatable;


# virtual methods
.method public abstract closeCompletely()V
.end method

.method public abstract copyEventFromReader(Lorg/codehaus/stax2/XMLStreamReader2;Z)V
.end method

.method public abstract getEncoding()Ljava/lang/String;
.end method

.method public abstract getLocation()Lorg/codehaus/stax2/XMLStreamLocation2;
.end method

.method public abstract isPropertySupported(Ljava/lang/String;)Z
.end method

.method public abstract setProperty(Ljava/lang/String;Ljava/lang/Object;)Z
.end method

.method public abstract writeCData([CII)V
.end method

.method public abstract writeDTD(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract writeFullEndElement()V
.end method

.method public abstract writeRaw(Ljava/lang/String;)V
.end method

.method public abstract writeRaw(Ljava/lang/String;II)V
.end method

.method public abstract writeRaw([CII)V
.end method

.method public abstract writeSpace(Ljava/lang/String;)V
.end method

.method public abstract writeSpace([CII)V
.end method

.method public abstract writeStartDocument(Ljava/lang/String;Ljava/lang/String;Z)V
.end method

.class public Lorg/codehaus/stax2/util/StreamWriter2Delegate;
.super Lorg/codehaus/stax2/util/StreamWriterDelegate;
.source "SourceFile"

# interfaces
.implements Lorg/codehaus/stax2/XMLStreamWriter2;


# instance fields
.field protected mDelegate2:Lorg/codehaus/stax2/XMLStreamWriter2;


# direct methods
.method public constructor <init>(Lorg/codehaus/stax2/XMLStreamWriter2;)V
    .locals 0

    invoke-direct {p0, p1}, Lorg/codehaus/stax2/util/StreamWriterDelegate;-><init>(Ljavax/xml/stream/XMLStreamWriter;)V

    return-void
.end method


# virtual methods
.method public closeCompletely()V
    .locals 1

    iget-object v0, p0, Lorg/codehaus/stax2/util/StreamWriter2Delegate;->mDelegate2:Lorg/codehaus/stax2/XMLStreamWriter2;

    invoke-interface {v0}, Lorg/codehaus/stax2/XMLStreamWriter2;->closeCompletely()V

    return-void
.end method

.method public copyEventFromReader(Lorg/codehaus/stax2/XMLStreamReader2;Z)V
    .locals 1

    iget-object v0, p0, Lorg/codehaus/stax2/util/StreamWriter2Delegate;->mDelegate2:Lorg/codehaus/stax2/XMLStreamWriter2;

    invoke-interface {v0, p1, p2}, Lorg/codehaus/stax2/XMLStreamWriter2;->copyEventFromReader(Lorg/codehaus/stax2/XMLStreamReader2;Z)V

    return-void
.end method

.method public getEncoding()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/codehaus/stax2/util/StreamWriter2Delegate;->mDelegate2:Lorg/codehaus/stax2/XMLStreamWriter2;

    invoke-interface {v0}, Lorg/codehaus/stax2/XMLStreamWriter2;->getEncoding()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getLocation()Lorg/codehaus/stax2/XMLStreamLocation2;
    .locals 1

    iget-object v0, p0, Lorg/codehaus/stax2/util/StreamWriter2Delegate;->mDelegate2:Lorg/codehaus/stax2/XMLStreamWriter2;

    invoke-interface {v0}, Lorg/codehaus/stax2/XMLStreamWriter2;->getLocation()Lorg/codehaus/stax2/XMLStreamLocation2;

    move-result-object v0

    return-object v0
.end method

.method public isPropertySupported(Ljava/lang/String;)Z
    .locals 1

    iget-object v0, p0, Lorg/codehaus/stax2/util/StreamWriter2Delegate;->mDelegate2:Lorg/codehaus/stax2/XMLStreamWriter2;

    invoke-interface {v0, p1}, Lorg/codehaus/stax2/XMLStreamWriter2;->isPropertySupported(Ljava/lang/String;)Z

    move-result p1

    return p1
.end method

.method public setParent(Ljavax/xml/stream/XMLStreamWriter;)V
    .locals 0

    invoke-super {p0, p1}, Lorg/codehaus/stax2/util/StreamWriterDelegate;->setParent(Ljavax/xml/stream/XMLStreamWriter;)V

    check-cast p1, Lorg/codehaus/stax2/XMLStreamWriter2;

    iput-object p1, p0, Lorg/codehaus/stax2/util/StreamWriter2Delegate;->mDelegate2:Lorg/codehaus/stax2/XMLStreamWriter2;

    return-void
.end method

.method public setProperty(Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 1

    iget-object v0, p0, Lorg/codehaus/stax2/util/StreamWriter2Delegate;->mDelegate2:Lorg/codehaus/stax2/XMLStreamWriter2;

    invoke-interface {v0, p1, p2}, Lorg/codehaus/stax2/XMLStreamWriter2;->setProperty(Ljava/lang/String;Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public setValidationProblemHandler(Lorg/codehaus/stax2/validation/ValidationProblemHandler;)Lorg/codehaus/stax2/validation/ValidationProblemHandler;
    .locals 1

    iget-object v0, p0, Lorg/codehaus/stax2/util/StreamWriter2Delegate;->mDelegate2:Lorg/codehaus/stax2/XMLStreamWriter2;

    invoke-interface {v0, p1}, Lorg/codehaus/stax2/validation/Validatable;->setValidationProblemHandler(Lorg/codehaus/stax2/validation/ValidationProblemHandler;)Lorg/codehaus/stax2/validation/ValidationProblemHandler;

    move-result-object p1

    return-object p1
.end method

.method public stopValidatingAgainst(Lorg/codehaus/stax2/validation/XMLValidationSchema;)Lorg/codehaus/stax2/validation/XMLValidator;
    .locals 1

    .line 1
    iget-object v0, p0, Lorg/codehaus/stax2/util/StreamWriter2Delegate;->mDelegate2:Lorg/codehaus/stax2/XMLStreamWriter2;

    invoke-interface {v0, p1}, Lorg/codehaus/stax2/validation/Validatable;->stopValidatingAgainst(Lorg/codehaus/stax2/validation/XMLValidationSchema;)Lorg/codehaus/stax2/validation/XMLValidator;

    move-result-object p1

    return-object p1
.end method

.method public stopValidatingAgainst(Lorg/codehaus/stax2/validation/XMLValidator;)Lorg/codehaus/stax2/validation/XMLValidator;
    .locals 1

    .line 2
    iget-object v0, p0, Lorg/codehaus/stax2/util/StreamWriter2Delegate;->mDelegate2:Lorg/codehaus/stax2/XMLStreamWriter2;

    invoke-interface {v0, p1}, Lorg/codehaus/stax2/validation/Validatable;->stopValidatingAgainst(Lorg/codehaus/stax2/validation/XMLValidator;)Lorg/codehaus/stax2/validation/XMLValidator;

    move-result-object p1

    return-object p1
.end method

.method public validateAgainst(Lorg/codehaus/stax2/validation/XMLValidationSchema;)Lorg/codehaus/stax2/validation/XMLValidator;
    .locals 1

    iget-object v0, p0, Lorg/codehaus/stax2/util/StreamWriter2Delegate;->mDelegate2:Lorg/codehaus/stax2/XMLStreamWriter2;

    invoke-interface {v0, p1}, Lorg/codehaus/stax2/validation/Validatable;->validateAgainst(Lorg/codehaus/stax2/validation/XMLValidationSchema;)Lorg/codehaus/stax2/validation/XMLValidator;

    move-result-object p1

    return-object p1
.end method

.method public writeBinary(Lorg/codehaus/stax2/typed/Base64Variant;[BII)V
    .locals 1

    .line 1
    iget-object v0, p0, Lorg/codehaus/stax2/util/StreamWriter2Delegate;->mDelegate2:Lorg/codehaus/stax2/XMLStreamWriter2;

    invoke-interface {v0, p1, p2, p3, p4}, Lorg/codehaus/stax2/typed/TypedXMLStreamWriter;->writeBinary(Lorg/codehaus/stax2/typed/Base64Variant;[BII)V

    return-void
.end method

.method public writeBinary([BII)V
    .locals 1

    .line 2
    iget-object v0, p0, Lorg/codehaus/stax2/util/StreamWriter2Delegate;->mDelegate2:Lorg/codehaus/stax2/XMLStreamWriter2;

    invoke-interface {v0, p1, p2, p3}, Lorg/codehaus/stax2/typed/TypedXMLStreamWriter;->writeBinary([BII)V

    return-void
.end method

.method public writeBinaryAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[B)V
    .locals 1

    .line 1
    iget-object v0, p0, Lorg/codehaus/stax2/util/StreamWriter2Delegate;->mDelegate2:Lorg/codehaus/stax2/XMLStreamWriter2;

    invoke-interface {v0, p1, p2, p3, p4}, Lorg/codehaus/stax2/typed/TypedXMLStreamWriter;->writeBinaryAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[B)V

    return-void
.end method

.method public writeBinaryAttribute(Lorg/codehaus/stax2/typed/Base64Variant;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[B)V
    .locals 6

    .line 2
    iget-object v0, p0, Lorg/codehaus/stax2/util/StreamWriter2Delegate;->mDelegate2:Lorg/codehaus/stax2/XMLStreamWriter2;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-interface/range {v0 .. v5}, Lorg/codehaus/stax2/typed/TypedXMLStreamWriter;->writeBinaryAttribute(Lorg/codehaus/stax2/typed/Base64Variant;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[B)V

    return-void
.end method

.method public writeBoolean(Z)V
    .locals 1

    iget-object v0, p0, Lorg/codehaus/stax2/util/StreamWriter2Delegate;->mDelegate2:Lorg/codehaus/stax2/XMLStreamWriter2;

    invoke-interface {v0, p1}, Lorg/codehaus/stax2/typed/TypedXMLStreamWriter;->writeBoolean(Z)V

    return-void
.end method

.method public writeBooleanAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 1

    iget-object v0, p0, Lorg/codehaus/stax2/util/StreamWriter2Delegate;->mDelegate2:Lorg/codehaus/stax2/XMLStreamWriter2;

    invoke-interface {v0, p1, p2, p3, p4}, Lorg/codehaus/stax2/typed/TypedXMLStreamWriter;->writeBooleanAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    return-void
.end method

.method public writeCData([CII)V
    .locals 1

    iget-object v0, p0, Lorg/codehaus/stax2/util/StreamWriter2Delegate;->mDelegate2:Lorg/codehaus/stax2/XMLStreamWriter2;

    invoke-interface {v0, p1, p2, p3}, Lorg/codehaus/stax2/XMLStreamWriter2;->writeCData([CII)V

    return-void
.end method

.method public writeDTD(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lorg/codehaus/stax2/util/StreamWriter2Delegate;->mDelegate2:Lorg/codehaus/stax2/XMLStreamWriter2;

    invoke-interface {v0, p1, p2, p3, p4}, Lorg/codehaus/stax2/XMLStreamWriter2;->writeDTD(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public writeDecimal(Ljava/math/BigDecimal;)V
    .locals 1

    iget-object v0, p0, Lorg/codehaus/stax2/util/StreamWriter2Delegate;->mDelegate2:Lorg/codehaus/stax2/XMLStreamWriter2;

    invoke-interface {v0, p1}, Lorg/codehaus/stax2/typed/TypedXMLStreamWriter;->writeDecimal(Ljava/math/BigDecimal;)V

    return-void
.end method

.method public writeDecimalAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/math/BigDecimal;)V
    .locals 1

    iget-object v0, p0, Lorg/codehaus/stax2/util/StreamWriter2Delegate;->mDelegate2:Lorg/codehaus/stax2/XMLStreamWriter2;

    invoke-interface {v0, p1, p2, p3, p4}, Lorg/codehaus/stax2/typed/TypedXMLStreamWriter;->writeDecimalAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/math/BigDecimal;)V

    return-void
.end method

.method public writeDouble(D)V
    .locals 1

    iget-object v0, p0, Lorg/codehaus/stax2/util/StreamWriter2Delegate;->mDelegate2:Lorg/codehaus/stax2/XMLStreamWriter2;

    invoke-interface {v0, p1, p2}, Lorg/codehaus/stax2/typed/TypedXMLStreamWriter;->writeDouble(D)V

    return-void
.end method

.method public writeDoubleArray([DII)V
    .locals 1

    iget-object v0, p0, Lorg/codehaus/stax2/util/StreamWriter2Delegate;->mDelegate2:Lorg/codehaus/stax2/XMLStreamWriter2;

    invoke-interface {v0, p1, p2, p3}, Lorg/codehaus/stax2/typed/TypedXMLStreamWriter;->writeDoubleArray([DII)V

    return-void
.end method

.method public writeDoubleArrayAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[D)V
    .locals 1

    iget-object v0, p0, Lorg/codehaus/stax2/util/StreamWriter2Delegate;->mDelegate2:Lorg/codehaus/stax2/XMLStreamWriter2;

    invoke-interface {v0, p1, p2, p3, p4}, Lorg/codehaus/stax2/typed/TypedXMLStreamWriter;->writeDoubleArrayAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[D)V

    return-void
.end method

.method public writeDoubleAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;D)V
    .locals 6

    iget-object v0, p0, Lorg/codehaus/stax2/util/StreamWriter2Delegate;->mDelegate2:Lorg/codehaus/stax2/XMLStreamWriter2;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-wide v4, p4

    invoke-interface/range {v0 .. v5}, Lorg/codehaus/stax2/typed/TypedXMLStreamWriter;->writeDoubleAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;D)V

    return-void
.end method

.method public writeFloat(F)V
    .locals 1

    iget-object v0, p0, Lorg/codehaus/stax2/util/StreamWriter2Delegate;->mDelegate2:Lorg/codehaus/stax2/XMLStreamWriter2;

    invoke-interface {v0, p1}, Lorg/codehaus/stax2/typed/TypedXMLStreamWriter;->writeFloat(F)V

    return-void
.end method

.method public writeFloatArray([FII)V
    .locals 1

    iget-object v0, p0, Lorg/codehaus/stax2/util/StreamWriter2Delegate;->mDelegate2:Lorg/codehaus/stax2/XMLStreamWriter2;

    invoke-interface {v0, p1, p2, p3}, Lorg/codehaus/stax2/typed/TypedXMLStreamWriter;->writeFloatArray([FII)V

    return-void
.end method

.method public writeFloatArrayAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[F)V
    .locals 1

    iget-object v0, p0, Lorg/codehaus/stax2/util/StreamWriter2Delegate;->mDelegate2:Lorg/codehaus/stax2/XMLStreamWriter2;

    invoke-interface {v0, p1, p2, p3, p4}, Lorg/codehaus/stax2/typed/TypedXMLStreamWriter;->writeFloatArrayAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[F)V

    return-void
.end method

.method public writeFloatAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;F)V
    .locals 1

    iget-object v0, p0, Lorg/codehaus/stax2/util/StreamWriter2Delegate;->mDelegate2:Lorg/codehaus/stax2/XMLStreamWriter2;

    invoke-interface {v0, p1, p2, p3, p4}, Lorg/codehaus/stax2/typed/TypedXMLStreamWriter;->writeFloatAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;F)V

    return-void
.end method

.method public writeFullEndElement()V
    .locals 1

    iget-object v0, p0, Lorg/codehaus/stax2/util/StreamWriter2Delegate;->mDelegate2:Lorg/codehaus/stax2/XMLStreamWriter2;

    invoke-interface {v0}, Lorg/codehaus/stax2/XMLStreamWriter2;->writeFullEndElement()V

    return-void
.end method

.method public writeInt(I)V
    .locals 1

    iget-object v0, p0, Lorg/codehaus/stax2/util/StreamWriter2Delegate;->mDelegate2:Lorg/codehaus/stax2/XMLStreamWriter2;

    invoke-interface {v0, p1}, Lorg/codehaus/stax2/typed/TypedXMLStreamWriter;->writeInt(I)V

    return-void
.end method

.method public writeIntArray([III)V
    .locals 1

    iget-object v0, p0, Lorg/codehaus/stax2/util/StreamWriter2Delegate;->mDelegate2:Lorg/codehaus/stax2/XMLStreamWriter2;

    invoke-interface {v0, p1, p2, p3}, Lorg/codehaus/stax2/typed/TypedXMLStreamWriter;->writeIntArray([III)V

    return-void
.end method

.method public writeIntArrayAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[I)V
    .locals 1

    iget-object v0, p0, Lorg/codehaus/stax2/util/StreamWriter2Delegate;->mDelegate2:Lorg/codehaus/stax2/XMLStreamWriter2;

    invoke-interface {v0, p1, p2, p3, p4}, Lorg/codehaus/stax2/typed/TypedXMLStreamWriter;->writeIntArrayAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[I)V

    return-void
.end method

.method public writeIntAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 1

    iget-object v0, p0, Lorg/codehaus/stax2/util/StreamWriter2Delegate;->mDelegate2:Lorg/codehaus/stax2/XMLStreamWriter2;

    invoke-interface {v0, p1, p2, p3, p4}, Lorg/codehaus/stax2/typed/TypedXMLStreamWriter;->writeIntAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    return-void
.end method

.method public writeInteger(Ljava/math/BigInteger;)V
    .locals 1

    iget-object v0, p0, Lorg/codehaus/stax2/util/StreamWriter2Delegate;->mDelegate2:Lorg/codehaus/stax2/XMLStreamWriter2;

    invoke-interface {v0, p1}, Lorg/codehaus/stax2/typed/TypedXMLStreamWriter;->writeInteger(Ljava/math/BigInteger;)V

    return-void
.end method

.method public writeIntegerAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/math/BigInteger;)V
    .locals 1

    iget-object v0, p0, Lorg/codehaus/stax2/util/StreamWriter2Delegate;->mDelegate2:Lorg/codehaus/stax2/XMLStreamWriter2;

    invoke-interface {v0, p1, p2, p3, p4}, Lorg/codehaus/stax2/typed/TypedXMLStreamWriter;->writeIntegerAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/math/BigInteger;)V

    return-void
.end method

.method public writeLong(J)V
    .locals 1

    iget-object v0, p0, Lorg/codehaus/stax2/util/StreamWriter2Delegate;->mDelegate2:Lorg/codehaus/stax2/XMLStreamWriter2;

    invoke-interface {v0, p1, p2}, Lorg/codehaus/stax2/typed/TypedXMLStreamWriter;->writeLong(J)V

    return-void
.end method

.method public writeLongArray([JII)V
    .locals 1

    iget-object v0, p0, Lorg/codehaus/stax2/util/StreamWriter2Delegate;->mDelegate2:Lorg/codehaus/stax2/XMLStreamWriter2;

    invoke-interface {v0, p1, p2, p3}, Lorg/codehaus/stax2/typed/TypedXMLStreamWriter;->writeLongArray([JII)V

    return-void
.end method

.method public writeLongArrayAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[J)V
    .locals 1

    iget-object v0, p0, Lorg/codehaus/stax2/util/StreamWriter2Delegate;->mDelegate2:Lorg/codehaus/stax2/XMLStreamWriter2;

    invoke-interface {v0, p1, p2, p3, p4}, Lorg/codehaus/stax2/typed/TypedXMLStreamWriter;->writeLongArrayAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[J)V

    return-void
.end method

.method public writeLongAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V
    .locals 6

    iget-object v0, p0, Lorg/codehaus/stax2/util/StreamWriter2Delegate;->mDelegate2:Lorg/codehaus/stax2/XMLStreamWriter2;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-wide v4, p4

    invoke-interface/range {v0 .. v5}, Lorg/codehaus/stax2/typed/TypedXMLStreamWriter;->writeLongAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V

    return-void
.end method

.method public writeQName(Ljavax/xml/namespace/QName;)V
    .locals 1

    iget-object v0, p0, Lorg/codehaus/stax2/util/StreamWriter2Delegate;->mDelegate2:Lorg/codehaus/stax2/XMLStreamWriter2;

    invoke-interface {v0, p1}, Lorg/codehaus/stax2/typed/TypedXMLStreamWriter;->writeQName(Ljavax/xml/namespace/QName;)V

    return-void
.end method

.method public writeQNameAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljavax/xml/namespace/QName;)V
    .locals 1

    iget-object v0, p0, Lorg/codehaus/stax2/util/StreamWriter2Delegate;->mDelegate2:Lorg/codehaus/stax2/XMLStreamWriter2;

    invoke-interface {v0, p1, p2, p3, p4}, Lorg/codehaus/stax2/typed/TypedXMLStreamWriter;->writeQNameAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljavax/xml/namespace/QName;)V

    return-void
.end method

.method public writeRaw(Ljava/lang/String;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lorg/codehaus/stax2/util/StreamWriter2Delegate;->mDelegate2:Lorg/codehaus/stax2/XMLStreamWriter2;

    invoke-interface {v0, p1}, Lorg/codehaus/stax2/XMLStreamWriter2;->writeRaw(Ljava/lang/String;)V

    return-void
.end method

.method public writeRaw(Ljava/lang/String;II)V
    .locals 1

    .line 2
    iget-object v0, p0, Lorg/codehaus/stax2/util/StreamWriter2Delegate;->mDelegate2:Lorg/codehaus/stax2/XMLStreamWriter2;

    invoke-interface {v0, p1, p2, p3}, Lorg/codehaus/stax2/XMLStreamWriter2;->writeRaw(Ljava/lang/String;II)V

    return-void
.end method

.method public writeRaw([CII)V
    .locals 1

    .line 3
    iget-object v0, p0, Lorg/codehaus/stax2/util/StreamWriter2Delegate;->mDelegate2:Lorg/codehaus/stax2/XMLStreamWriter2;

    invoke-interface {v0, p1, p2, p3}, Lorg/codehaus/stax2/XMLStreamWriter2;->writeRaw([CII)V

    return-void
.end method

.method public writeSpace(Ljava/lang/String;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lorg/codehaus/stax2/util/StreamWriter2Delegate;->mDelegate2:Lorg/codehaus/stax2/XMLStreamWriter2;

    invoke-interface {v0, p1}, Lorg/codehaus/stax2/XMLStreamWriter2;->writeSpace(Ljava/lang/String;)V

    return-void
.end method

.method public writeSpace([CII)V
    .locals 1

    .line 2
    iget-object v0, p0, Lorg/codehaus/stax2/util/StreamWriter2Delegate;->mDelegate2:Lorg/codehaus/stax2/XMLStreamWriter2;

    invoke-interface {v0, p1, p2, p3}, Lorg/codehaus/stax2/XMLStreamWriter2;->writeSpace([CII)V

    return-void
.end method

.method public writeStartDocument(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 1

    iget-object v0, p0, Lorg/codehaus/stax2/util/StreamWriter2Delegate;->mDelegate2:Lorg/codehaus/stax2/XMLStreamWriter2;

    invoke-interface {v0, p1, p2, p3}, Lorg/codehaus/stax2/XMLStreamWriter2;->writeStartDocument(Ljava/lang/String;Ljava/lang/String;Z)V

    return-void
.end method

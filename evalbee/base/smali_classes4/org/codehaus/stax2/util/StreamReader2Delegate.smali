.class public Lorg/codehaus/stax2/util/StreamReader2Delegate;
.super Ljavax/xml/stream/util/StreamReaderDelegate;
.source "SourceFile"

# interfaces
.implements Lorg/codehaus/stax2/XMLStreamReader2;


# instance fields
.field protected _delegate2:Lorg/codehaus/stax2/XMLStreamReader2;


# direct methods
.method public constructor <init>(Lorg/codehaus/stax2/XMLStreamReader2;)V
    .locals 0

    invoke-direct {p0, p1}, Ljavax/xml/stream/util/StreamReaderDelegate;-><init>(Ljavax/xml/stream/XMLStreamReader;)V

    iput-object p1, p0, Lorg/codehaus/stax2/util/StreamReader2Delegate;->_delegate2:Lorg/codehaus/stax2/XMLStreamReader2;

    return-void
.end method


# virtual methods
.method public closeCompletely()V
    .locals 1

    iget-object v0, p0, Lorg/codehaus/stax2/util/StreamReader2Delegate;->_delegate2:Lorg/codehaus/stax2/XMLStreamReader2;

    invoke-interface {v0}, Lorg/codehaus/stax2/XMLStreamReader2;->closeCompletely()V

    return-void
.end method

.method public getAttributeAs(ILorg/codehaus/stax2/typed/TypedValueDecoder;)V
    .locals 1

    iget-object v0, p0, Lorg/codehaus/stax2/util/StreamReader2Delegate;->_delegate2:Lorg/codehaus/stax2/XMLStreamReader2;

    invoke-interface {v0, p1, p2}, Lorg/codehaus/stax2/typed/TypedXMLStreamReader;->getAttributeAs(ILorg/codehaus/stax2/typed/TypedValueDecoder;)V

    return-void
.end method

.method public getAttributeAsArray(ILorg/codehaus/stax2/typed/TypedArrayDecoder;)I
    .locals 1

    iget-object v0, p0, Lorg/codehaus/stax2/util/StreamReader2Delegate;->_delegate2:Lorg/codehaus/stax2/XMLStreamReader2;

    invoke-interface {v0, p1, p2}, Lorg/codehaus/stax2/typed/TypedXMLStreamReader;->getAttributeAsArray(ILorg/codehaus/stax2/typed/TypedArrayDecoder;)I

    move-result p1

    return p1
.end method

.method public getAttributeAsBinary(I)[B
    .locals 1

    .line 1
    iget-object v0, p0, Lorg/codehaus/stax2/util/StreamReader2Delegate;->_delegate2:Lorg/codehaus/stax2/XMLStreamReader2;

    invoke-interface {v0, p1}, Lorg/codehaus/stax2/typed/TypedXMLStreamReader;->getAttributeAsBinary(I)[B

    move-result-object p1

    return-object p1
.end method

.method public getAttributeAsBinary(ILorg/codehaus/stax2/typed/Base64Variant;)[B
    .locals 1

    .line 2
    iget-object v0, p0, Lorg/codehaus/stax2/util/StreamReader2Delegate;->_delegate2:Lorg/codehaus/stax2/XMLStreamReader2;

    invoke-interface {v0, p1, p2}, Lorg/codehaus/stax2/typed/TypedXMLStreamReader;->getAttributeAsBinary(ILorg/codehaus/stax2/typed/Base64Variant;)[B

    move-result-object p1

    return-object p1
.end method

.method public getAttributeAsBoolean(I)Z
    .locals 1

    iget-object v0, p0, Lorg/codehaus/stax2/util/StreamReader2Delegate;->_delegate2:Lorg/codehaus/stax2/XMLStreamReader2;

    invoke-interface {v0, p1}, Lorg/codehaus/stax2/typed/TypedXMLStreamReader;->getAttributeAsBoolean(I)Z

    move-result p1

    return p1
.end method

.method public getAttributeAsDecimal(I)Ljava/math/BigDecimal;
    .locals 1

    iget-object v0, p0, Lorg/codehaus/stax2/util/StreamReader2Delegate;->_delegate2:Lorg/codehaus/stax2/XMLStreamReader2;

    invoke-interface {v0, p1}, Lorg/codehaus/stax2/typed/TypedXMLStreamReader;->getAttributeAsDecimal(I)Ljava/math/BigDecimal;

    move-result-object p1

    return-object p1
.end method

.method public getAttributeAsDouble(I)D
    .locals 2

    iget-object v0, p0, Lorg/codehaus/stax2/util/StreamReader2Delegate;->_delegate2:Lorg/codehaus/stax2/XMLStreamReader2;

    invoke-interface {v0, p1}, Lorg/codehaus/stax2/typed/TypedXMLStreamReader;->getAttributeAsDouble(I)D

    move-result-wide v0

    return-wide v0
.end method

.method public getAttributeAsDoubleArray(I)[D
    .locals 1

    iget-object v0, p0, Lorg/codehaus/stax2/util/StreamReader2Delegate;->_delegate2:Lorg/codehaus/stax2/XMLStreamReader2;

    invoke-interface {v0, p1}, Lorg/codehaus/stax2/typed/TypedXMLStreamReader;->getAttributeAsDoubleArray(I)[D

    move-result-object p1

    return-object p1
.end method

.method public getAttributeAsFloat(I)F
    .locals 1

    iget-object v0, p0, Lorg/codehaus/stax2/util/StreamReader2Delegate;->_delegate2:Lorg/codehaus/stax2/XMLStreamReader2;

    invoke-interface {v0, p1}, Lorg/codehaus/stax2/typed/TypedXMLStreamReader;->getAttributeAsFloat(I)F

    move-result p1

    return p1
.end method

.method public getAttributeAsFloatArray(I)[F
    .locals 1

    iget-object v0, p0, Lorg/codehaus/stax2/util/StreamReader2Delegate;->_delegate2:Lorg/codehaus/stax2/XMLStreamReader2;

    invoke-interface {v0, p1}, Lorg/codehaus/stax2/typed/TypedXMLStreamReader;->getAttributeAsFloatArray(I)[F

    move-result-object p1

    return-object p1
.end method

.method public getAttributeAsInt(I)I
    .locals 1

    iget-object v0, p0, Lorg/codehaus/stax2/util/StreamReader2Delegate;->_delegate2:Lorg/codehaus/stax2/XMLStreamReader2;

    invoke-interface {v0, p1}, Lorg/codehaus/stax2/typed/TypedXMLStreamReader;->getAttributeAsInt(I)I

    move-result p1

    return p1
.end method

.method public getAttributeAsIntArray(I)[I
    .locals 1

    iget-object v0, p0, Lorg/codehaus/stax2/util/StreamReader2Delegate;->_delegate2:Lorg/codehaus/stax2/XMLStreamReader2;

    invoke-interface {v0, p1}, Lorg/codehaus/stax2/typed/TypedXMLStreamReader;->getAttributeAsIntArray(I)[I

    move-result-object p1

    return-object p1
.end method

.method public getAttributeAsInteger(I)Ljava/math/BigInteger;
    .locals 1

    iget-object v0, p0, Lorg/codehaus/stax2/util/StreamReader2Delegate;->_delegate2:Lorg/codehaus/stax2/XMLStreamReader2;

    invoke-interface {v0, p1}, Lorg/codehaus/stax2/typed/TypedXMLStreamReader;->getAttributeAsInteger(I)Ljava/math/BigInteger;

    move-result-object p1

    return-object p1
.end method

.method public getAttributeAsLong(I)J
    .locals 2

    iget-object v0, p0, Lorg/codehaus/stax2/util/StreamReader2Delegate;->_delegate2:Lorg/codehaus/stax2/XMLStreamReader2;

    invoke-interface {v0, p1}, Lorg/codehaus/stax2/typed/TypedXMLStreamReader;->getAttributeAsLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public getAttributeAsLongArray(I)[J
    .locals 1

    iget-object v0, p0, Lorg/codehaus/stax2/util/StreamReader2Delegate;->_delegate2:Lorg/codehaus/stax2/XMLStreamReader2;

    invoke-interface {v0, p1}, Lorg/codehaus/stax2/typed/TypedXMLStreamReader;->getAttributeAsLongArray(I)[J

    move-result-object p1

    return-object p1
.end method

.method public getAttributeAsQName(I)Ljavax/xml/namespace/QName;
    .locals 1

    iget-object v0, p0, Lorg/codehaus/stax2/util/StreamReader2Delegate;->_delegate2:Lorg/codehaus/stax2/XMLStreamReader2;

    invoke-interface {v0, p1}, Lorg/codehaus/stax2/typed/TypedXMLStreamReader;->getAttributeAsQName(I)Ljavax/xml/namespace/QName;

    move-result-object p1

    return-object p1
.end method

.method public getAttributeIndex(Ljava/lang/String;Ljava/lang/String;)I
    .locals 1

    iget-object v0, p0, Lorg/codehaus/stax2/util/StreamReader2Delegate;->_delegate2:Lorg/codehaus/stax2/XMLStreamReader2;

    invoke-interface {v0, p1, p2}, Lorg/codehaus/stax2/typed/TypedXMLStreamReader;->getAttributeIndex(Ljava/lang/String;Ljava/lang/String;)I

    move-result p1

    return p1
.end method

.method public getAttributeInfo()Lorg/codehaus/stax2/AttributeInfo;
    .locals 1

    iget-object v0, p0, Lorg/codehaus/stax2/util/StreamReader2Delegate;->_delegate2:Lorg/codehaus/stax2/XMLStreamReader2;

    invoke-interface {v0}, Lorg/codehaus/stax2/XMLStreamReader2;->getAttributeInfo()Lorg/codehaus/stax2/AttributeInfo;

    move-result-object v0

    return-object v0
.end method

.method public getDTDInfo()Lorg/codehaus/stax2/DTDInfo;
    .locals 1

    iget-object v0, p0, Lorg/codehaus/stax2/util/StreamReader2Delegate;->_delegate2:Lorg/codehaus/stax2/XMLStreamReader2;

    invoke-interface {v0}, Lorg/codehaus/stax2/XMLStreamReader2;->getDTDInfo()Lorg/codehaus/stax2/DTDInfo;

    move-result-object v0

    return-object v0
.end method

.method public getDepth()I
    .locals 1

    iget-object v0, p0, Lorg/codehaus/stax2/util/StreamReader2Delegate;->_delegate2:Lorg/codehaus/stax2/XMLStreamReader2;

    invoke-interface {v0}, Lorg/codehaus/stax2/XMLStreamReader2;->getDepth()I

    move-result v0

    return v0
.end method

.method public getElementAs(Lorg/codehaus/stax2/typed/TypedValueDecoder;)V
    .locals 1

    iget-object v0, p0, Lorg/codehaus/stax2/util/StreamReader2Delegate;->_delegate2:Lorg/codehaus/stax2/XMLStreamReader2;

    invoke-interface {v0, p1}, Lorg/codehaus/stax2/typed/TypedXMLStreamReader;->getElementAs(Lorg/codehaus/stax2/typed/TypedValueDecoder;)V

    return-void
.end method

.method public getElementAsBinary()[B
    .locals 1

    .line 1
    iget-object v0, p0, Lorg/codehaus/stax2/util/StreamReader2Delegate;->_delegate2:Lorg/codehaus/stax2/XMLStreamReader2;

    invoke-interface {v0}, Lorg/codehaus/stax2/typed/TypedXMLStreamReader;->getElementAsBinary()[B

    move-result-object v0

    return-object v0
.end method

.method public getElementAsBinary(Lorg/codehaus/stax2/typed/Base64Variant;)[B
    .locals 1

    .line 2
    iget-object v0, p0, Lorg/codehaus/stax2/util/StreamReader2Delegate;->_delegate2:Lorg/codehaus/stax2/XMLStreamReader2;

    invoke-interface {v0, p1}, Lorg/codehaus/stax2/typed/TypedXMLStreamReader;->getElementAsBinary(Lorg/codehaus/stax2/typed/Base64Variant;)[B

    move-result-object p1

    return-object p1
.end method

.method public getElementAsBoolean()Z
    .locals 1

    iget-object v0, p0, Lorg/codehaus/stax2/util/StreamReader2Delegate;->_delegate2:Lorg/codehaus/stax2/XMLStreamReader2;

    invoke-interface {v0}, Lorg/codehaus/stax2/typed/TypedXMLStreamReader;->getElementAsBoolean()Z

    move-result v0

    return v0
.end method

.method public getElementAsDecimal()Ljava/math/BigDecimal;
    .locals 1

    iget-object v0, p0, Lorg/codehaus/stax2/util/StreamReader2Delegate;->_delegate2:Lorg/codehaus/stax2/XMLStreamReader2;

    invoke-interface {v0}, Lorg/codehaus/stax2/typed/TypedXMLStreamReader;->getElementAsDecimal()Ljava/math/BigDecimal;

    move-result-object v0

    return-object v0
.end method

.method public getElementAsDouble()D
    .locals 2

    iget-object v0, p0, Lorg/codehaus/stax2/util/StreamReader2Delegate;->_delegate2:Lorg/codehaus/stax2/XMLStreamReader2;

    invoke-interface {v0}, Lorg/codehaus/stax2/typed/TypedXMLStreamReader;->getElementAsDouble()D

    move-result-wide v0

    return-wide v0
.end method

.method public getElementAsFloat()F
    .locals 1

    iget-object v0, p0, Lorg/codehaus/stax2/util/StreamReader2Delegate;->_delegate2:Lorg/codehaus/stax2/XMLStreamReader2;

    invoke-interface {v0}, Lorg/codehaus/stax2/typed/TypedXMLStreamReader;->getElementAsFloat()F

    move-result v0

    return v0
.end method

.method public getElementAsInt()I
    .locals 1

    iget-object v0, p0, Lorg/codehaus/stax2/util/StreamReader2Delegate;->_delegate2:Lorg/codehaus/stax2/XMLStreamReader2;

    invoke-interface {v0}, Lorg/codehaus/stax2/typed/TypedXMLStreamReader;->getElementAsInt()I

    move-result v0

    return v0
.end method

.method public getElementAsInteger()Ljava/math/BigInteger;
    .locals 1

    iget-object v0, p0, Lorg/codehaus/stax2/util/StreamReader2Delegate;->_delegate2:Lorg/codehaus/stax2/XMLStreamReader2;

    invoke-interface {v0}, Lorg/codehaus/stax2/typed/TypedXMLStreamReader;->getElementAsInteger()Ljava/math/BigInteger;

    move-result-object v0

    return-object v0
.end method

.method public getElementAsLong()J
    .locals 2

    iget-object v0, p0, Lorg/codehaus/stax2/util/StreamReader2Delegate;->_delegate2:Lorg/codehaus/stax2/XMLStreamReader2;

    invoke-interface {v0}, Lorg/codehaus/stax2/typed/TypedXMLStreamReader;->getElementAsLong()J

    move-result-wide v0

    return-wide v0
.end method

.method public getElementAsQName()Ljavax/xml/namespace/QName;
    .locals 1

    iget-object v0, p0, Lorg/codehaus/stax2/util/StreamReader2Delegate;->_delegate2:Lorg/codehaus/stax2/XMLStreamReader2;

    invoke-interface {v0}, Lorg/codehaus/stax2/typed/TypedXMLStreamReader;->getElementAsQName()Ljavax/xml/namespace/QName;

    move-result-object v0

    return-object v0
.end method

.method public getFeature(Ljava/lang/String;)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lorg/codehaus/stax2/util/StreamReader2Delegate;->_delegate2:Lorg/codehaus/stax2/XMLStreamReader2;

    invoke-interface {v0, p1}, Lorg/codehaus/stax2/XMLStreamReader2;->getFeature(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public getLocationInfo()Lorg/codehaus/stax2/LocationInfo;
    .locals 1

    iget-object v0, p0, Lorg/codehaus/stax2/util/StreamReader2Delegate;->_delegate2:Lorg/codehaus/stax2/XMLStreamReader2;

    invoke-interface {v0}, Lorg/codehaus/stax2/XMLStreamReader2;->getLocationInfo()Lorg/codehaus/stax2/LocationInfo;

    move-result-object v0

    return-object v0
.end method

.method public getNonTransientNamespaceContext()Ljavax/xml/namespace/NamespaceContext;
    .locals 1

    iget-object v0, p0, Lorg/codehaus/stax2/util/StreamReader2Delegate;->_delegate2:Lorg/codehaus/stax2/XMLStreamReader2;

    invoke-interface {v0}, Lorg/codehaus/stax2/XMLStreamReader2;->getNonTransientNamespaceContext()Ljavax/xml/namespace/NamespaceContext;

    move-result-object v0

    return-object v0
.end method

.method public getPrefixedName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/codehaus/stax2/util/StreamReader2Delegate;->_delegate2:Lorg/codehaus/stax2/XMLStreamReader2;

    invoke-interface {v0}, Lorg/codehaus/stax2/XMLStreamReader2;->getPrefixedName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getText(Ljava/io/Writer;Z)I
    .locals 1

    iget-object v0, p0, Lorg/codehaus/stax2/util/StreamReader2Delegate;->_delegate2:Lorg/codehaus/stax2/XMLStreamReader2;

    invoke-interface {v0, p1, p2}, Lorg/codehaus/stax2/XMLStreamReader2;->getText(Ljava/io/Writer;Z)I

    move-result p1

    return p1
.end method

.method public isEmptyElement()Z
    .locals 1

    iget-object v0, p0, Lorg/codehaus/stax2/util/StreamReader2Delegate;->_delegate2:Lorg/codehaus/stax2/XMLStreamReader2;

    invoke-interface {v0}, Lorg/codehaus/stax2/XMLStreamReader2;->isEmptyElement()Z

    move-result v0

    return v0
.end method

.method public isPropertySupported(Ljava/lang/String;)Z
    .locals 1

    iget-object v0, p0, Lorg/codehaus/stax2/util/StreamReader2Delegate;->_delegate2:Lorg/codehaus/stax2/XMLStreamReader2;

    invoke-interface {v0, p1}, Lorg/codehaus/stax2/XMLStreamReader2;->isPropertySupported(Ljava/lang/String;)Z

    move-result p1

    return p1
.end method

.method public readElementAsArray(Lorg/codehaus/stax2/typed/TypedArrayDecoder;)I
    .locals 1

    iget-object v0, p0, Lorg/codehaus/stax2/util/StreamReader2Delegate;->_delegate2:Lorg/codehaus/stax2/XMLStreamReader2;

    invoke-interface {v0, p1}, Lorg/codehaus/stax2/typed/TypedXMLStreamReader;->readElementAsArray(Lorg/codehaus/stax2/typed/TypedArrayDecoder;)I

    move-result p1

    return p1
.end method

.method public readElementAsBinary([BII)I
    .locals 1

    .line 1
    iget-object v0, p0, Lorg/codehaus/stax2/util/StreamReader2Delegate;->_delegate2:Lorg/codehaus/stax2/XMLStreamReader2;

    invoke-interface {v0, p1, p2, p3}, Lorg/codehaus/stax2/typed/TypedXMLStreamReader;->readElementAsBinary([BII)I

    move-result p1

    return p1
.end method

.method public readElementAsBinary([BIILorg/codehaus/stax2/typed/Base64Variant;)I
    .locals 1

    .line 2
    iget-object v0, p0, Lorg/codehaus/stax2/util/StreamReader2Delegate;->_delegate2:Lorg/codehaus/stax2/XMLStreamReader2;

    invoke-interface {v0, p1, p2, p3, p4}, Lorg/codehaus/stax2/typed/TypedXMLStreamReader;->readElementAsBinary([BIILorg/codehaus/stax2/typed/Base64Variant;)I

    move-result p1

    return p1
.end method

.method public readElementAsDoubleArray([DII)I
    .locals 1

    iget-object v0, p0, Lorg/codehaus/stax2/util/StreamReader2Delegate;->_delegate2:Lorg/codehaus/stax2/XMLStreamReader2;

    invoke-interface {v0, p1, p2, p3}, Lorg/codehaus/stax2/typed/TypedXMLStreamReader;->readElementAsDoubleArray([DII)I

    move-result p1

    return p1
.end method

.method public readElementAsFloatArray([FII)I
    .locals 1

    iget-object v0, p0, Lorg/codehaus/stax2/util/StreamReader2Delegate;->_delegate2:Lorg/codehaus/stax2/XMLStreamReader2;

    invoke-interface {v0, p1, p2, p3}, Lorg/codehaus/stax2/typed/TypedXMLStreamReader;->readElementAsFloatArray([FII)I

    move-result p1

    return p1
.end method

.method public readElementAsIntArray([III)I
    .locals 1

    iget-object v0, p0, Lorg/codehaus/stax2/util/StreamReader2Delegate;->_delegate2:Lorg/codehaus/stax2/XMLStreamReader2;

    invoke-interface {v0, p1, p2, p3}, Lorg/codehaus/stax2/typed/TypedXMLStreamReader;->readElementAsIntArray([III)I

    move-result p1

    return p1
.end method

.method public readElementAsLongArray([JII)I
    .locals 1

    iget-object v0, p0, Lorg/codehaus/stax2/util/StreamReader2Delegate;->_delegate2:Lorg/codehaus/stax2/XMLStreamReader2;

    invoke-interface {v0, p1, p2, p3}, Lorg/codehaus/stax2/typed/TypedXMLStreamReader;->readElementAsLongArray([JII)I

    move-result p1

    return p1
.end method

.method public setFeature(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 1

    iget-object v0, p0, Lorg/codehaus/stax2/util/StreamReader2Delegate;->_delegate2:Lorg/codehaus/stax2/XMLStreamReader2;

    invoke-interface {v0, p1, p2}, Lorg/codehaus/stax2/XMLStreamReader2;->setFeature(Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method

.method public setParent(Ljavax/xml/stream/XMLStreamReader;)V
    .locals 0

    invoke-super {p0, p1}, Ljavax/xml/stream/util/StreamReaderDelegate;->setParent(Ljavax/xml/stream/XMLStreamReader;)V

    check-cast p1, Lorg/codehaus/stax2/XMLStreamReader2;

    iput-object p1, p0, Lorg/codehaus/stax2/util/StreamReader2Delegate;->_delegate2:Lorg/codehaus/stax2/XMLStreamReader2;

    return-void
.end method

.method public setProperty(Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 1

    iget-object v0, p0, Lorg/codehaus/stax2/util/StreamReader2Delegate;->_delegate2:Lorg/codehaus/stax2/XMLStreamReader2;

    invoke-interface {v0, p1, p2}, Lorg/codehaus/stax2/XMLStreamReader2;->setProperty(Ljava/lang/String;Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public setValidationProblemHandler(Lorg/codehaus/stax2/validation/ValidationProblemHandler;)Lorg/codehaus/stax2/validation/ValidationProblemHandler;
    .locals 1

    iget-object v0, p0, Lorg/codehaus/stax2/util/StreamReader2Delegate;->_delegate2:Lorg/codehaus/stax2/XMLStreamReader2;

    invoke-interface {v0, p1}, Lorg/codehaus/stax2/validation/Validatable;->setValidationProblemHandler(Lorg/codehaus/stax2/validation/ValidationProblemHandler;)Lorg/codehaus/stax2/validation/ValidationProblemHandler;

    move-result-object p1

    return-object p1
.end method

.method public skipElement()V
    .locals 1

    iget-object v0, p0, Lorg/codehaus/stax2/util/StreamReader2Delegate;->_delegate2:Lorg/codehaus/stax2/XMLStreamReader2;

    invoke-interface {v0}, Lorg/codehaus/stax2/XMLStreamReader2;->skipElement()V

    return-void
.end method

.method public stopValidatingAgainst(Lorg/codehaus/stax2/validation/XMLValidationSchema;)Lorg/codehaus/stax2/validation/XMLValidator;
    .locals 1

    .line 1
    iget-object v0, p0, Lorg/codehaus/stax2/util/StreamReader2Delegate;->_delegate2:Lorg/codehaus/stax2/XMLStreamReader2;

    invoke-interface {v0, p1}, Lorg/codehaus/stax2/validation/Validatable;->stopValidatingAgainst(Lorg/codehaus/stax2/validation/XMLValidationSchema;)Lorg/codehaus/stax2/validation/XMLValidator;

    move-result-object p1

    return-object p1
.end method

.method public stopValidatingAgainst(Lorg/codehaus/stax2/validation/XMLValidator;)Lorg/codehaus/stax2/validation/XMLValidator;
    .locals 1

    .line 2
    iget-object v0, p0, Lorg/codehaus/stax2/util/StreamReader2Delegate;->_delegate2:Lorg/codehaus/stax2/XMLStreamReader2;

    invoke-interface {v0, p1}, Lorg/codehaus/stax2/validation/Validatable;->stopValidatingAgainst(Lorg/codehaus/stax2/validation/XMLValidator;)Lorg/codehaus/stax2/validation/XMLValidator;

    move-result-object p1

    return-object p1
.end method

.method public validateAgainst(Lorg/codehaus/stax2/validation/XMLValidationSchema;)Lorg/codehaus/stax2/validation/XMLValidator;
    .locals 1

    iget-object v0, p0, Lorg/codehaus/stax2/util/StreamReader2Delegate;->_delegate2:Lorg/codehaus/stax2/XMLStreamReader2;

    invoke-interface {v0, p1}, Lorg/codehaus/stax2/validation/Validatable;->validateAgainst(Lorg/codehaus/stax2/validation/XMLValidationSchema;)Lorg/codehaus/stax2/validation/XMLValidator;

    move-result-object p1

    return-object p1
.end method

.class public interface abstract Lorg/codehaus/stax2/XMLStreamProperties;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final XSP_IMPLEMENTATION_NAME:Ljava/lang/String; = "org.codehaus.stax2.implName"

.field public static final XSP_IMPLEMENTATION_VERSION:Ljava/lang/String; = "org.codehaus.stax2.implVersion"

.field public static final XSP_NAMESPACE_AWARE:Ljava/lang/String; = "javax.xml.stream.isNamespaceAware"

.field public static final XSP_PROBLEM_REPORTER:Ljava/lang/String; = "javax.xml.stream.reporter"

.field public static final XSP_SUPPORTS_XML11:Ljava/lang/String; = "org.codehaus.stax2.supportsXml11"

.field public static final XSP_SUPPORT_XMLID:Ljava/lang/String; = "org.codehaus.stax2.supportXmlId"

.field public static final XSP_V_XMLID_FULL:Ljava/lang/String; = "xmlidFull"

.field public static final XSP_V_XMLID_NONE:Ljava/lang/String; = "disable"

.field public static final XSP_V_XMLID_TYPING:Ljava/lang/String; = "xmlidTyping"

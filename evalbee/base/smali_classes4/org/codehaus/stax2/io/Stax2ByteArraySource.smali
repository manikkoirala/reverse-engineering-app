.class public Lorg/codehaus/stax2/io/Stax2ByteArraySource;
.super Lorg/codehaus/stax2/io/Stax2BlockSource;
.source "SourceFile"


# static fields
.field private static final DEFAULT_ENCODING:Ljava/lang/String; = "UTF-8"


# instance fields
.field final mBuffer:[B

.field final mLength:I

.field final mStart:I


# direct methods
.method public constructor <init>([BII)V
    .locals 0

    invoke-direct {p0}, Lorg/codehaus/stax2/io/Stax2BlockSource;-><init>()V

    iput-object p1, p0, Lorg/codehaus/stax2/io/Stax2ByteArraySource;->mBuffer:[B

    iput p2, p0, Lorg/codehaus/stax2/io/Stax2ByteArraySource;->mStart:I

    iput p3, p0, Lorg/codehaus/stax2/io/Stax2ByteArraySource;->mLength:I

    return-void
.end method


# virtual methods
.method public constructInputStream()Ljava/io/InputStream;
    .locals 4

    new-instance v0, Ljava/io/ByteArrayInputStream;

    iget-object v1, p0, Lorg/codehaus/stax2/io/Stax2ByteArraySource;->mBuffer:[B

    iget v2, p0, Lorg/codehaus/stax2/io/Stax2ByteArraySource;->mStart:I

    iget v3, p0, Lorg/codehaus/stax2/io/Stax2ByteArraySource;->mLength:I

    invoke-direct {v0, v1, v2, v3}, Ljava/io/ByteArrayInputStream;-><init>([BII)V

    return-object v0
.end method

.method public constructReader()Ljava/io/Reader;
    .locals 3

    invoke-virtual {p0}, Lorg/codehaus/stax2/io/Stax2Source;->getEncoding()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lorg/codehaus/stax2/io/Stax2ByteArraySource;->constructInputStream()Ljava/io/InputStream;

    move-result-object v1

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    const-string v0, "UTF-8"

    :cond_1
    new-instance v2, Ljava/io/InputStreamReader;

    invoke-direct {v2, v1, v0}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/lang/String;)V

    return-object v2
.end method

.method public getBuffer()[B
    .locals 1

    iget-object v0, p0, Lorg/codehaus/stax2/io/Stax2ByteArraySource;->mBuffer:[B

    return-object v0
.end method

.method public getBufferEnd()I
    .locals 2

    iget v0, p0, Lorg/codehaus/stax2/io/Stax2ByteArraySource;->mStart:I

    iget v1, p0, Lorg/codehaus/stax2/io/Stax2ByteArraySource;->mLength:I

    if-lez v1, :cond_0

    add-int/2addr v0, v1

    :cond_0
    return v0
.end method

.method public getBufferLength()I
    .locals 1

    iget v0, p0, Lorg/codehaus/stax2/io/Stax2ByteArraySource;->mLength:I

    return v0
.end method

.method public getBufferStart()I
    .locals 1

    iget v0, p0, Lorg/codehaus/stax2/io/Stax2ByteArraySource;->mStart:I

    return v0
.end method

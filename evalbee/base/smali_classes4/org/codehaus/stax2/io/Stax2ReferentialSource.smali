.class public abstract Lorg/codehaus/stax2/io/Stax2ReferentialSource;
.super Lorg/codehaus/stax2/io/Stax2Source;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lorg/codehaus/stax2/io/Stax2Source;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract constructInputStream()Ljava/io/InputStream;
.end method

.method public abstract constructReader()Ljava/io/Reader;
.end method

.method public abstract getReference()Ljava/net/URL;
.end method

.method public getSystemId()Ljava/lang/String;
    .locals 1

    invoke-super {p0}, Lorg/codehaus/stax2/io/Stax2Source;->getSystemId()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lorg/codehaus/stax2/io/Stax2ReferentialSource;->getReference()Ljava/net/URL;

    move-result-object v0

    invoke-virtual {v0}, Ljava/net/URL;->toExternalForm()Ljava/lang/String;

    move-result-object v0

    :cond_0
    return-object v0
.end method

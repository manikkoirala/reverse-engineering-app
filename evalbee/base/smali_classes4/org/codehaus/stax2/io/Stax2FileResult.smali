.class public Lorg/codehaus/stax2/io/Stax2FileResult;
.super Lorg/codehaus/stax2/io/Stax2ReferentialResult;
.source "SourceFile"


# instance fields
.field final mFile:Ljava/io/File;


# direct methods
.method public constructor <init>(Ljava/io/File;)V
    .locals 0

    invoke-direct {p0}, Lorg/codehaus/stax2/io/Stax2ReferentialResult;-><init>()V

    iput-object p1, p0, Lorg/codehaus/stax2/io/Stax2FileResult;->mFile:Ljava/io/File;

    return-void
.end method


# virtual methods
.method public constructOutputStream()Ljava/io/OutputStream;
    .locals 2

    new-instance v0, Ljava/io/FileOutputStream;

    iget-object v1, p0, Lorg/codehaus/stax2/io/Stax2FileResult;->mFile:Ljava/io/File;

    invoke-direct {v0, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    return-object v0
.end method

.method public constructWriter()Ljava/io/Writer;
    .locals 3

    invoke-virtual {p0}, Lorg/codehaus/stax2/io/Stax2Result;->getEncoding()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_0

    new-instance v1, Ljava/io/OutputStreamWriter;

    invoke-virtual {p0}, Lorg/codehaus/stax2/io/Stax2FileResult;->constructOutputStream()Ljava/io/OutputStream;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;Ljava/lang/String;)V

    return-object v1

    :cond_0
    new-instance v0, Ljava/io/FileWriter;

    iget-object v1, p0, Lorg/codehaus/stax2/io/Stax2FileResult;->mFile:Ljava/io/File;

    invoke-direct {v0, v1}, Ljava/io/FileWriter;-><init>(Ljava/io/File;)V

    return-object v0
.end method

.method public getFile()Ljava/io/File;
    .locals 1

    iget-object v0, p0, Lorg/codehaus/stax2/io/Stax2FileResult;->mFile:Ljava/io/File;

    return-object v0
.end method

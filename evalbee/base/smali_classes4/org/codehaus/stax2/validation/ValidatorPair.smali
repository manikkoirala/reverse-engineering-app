.class public Lorg/codehaus/stax2/validation/ValidatorPair;
.super Lorg/codehaus/stax2/validation/XMLValidator;
.source "SourceFile"


# static fields
.field public static final ATTR_TYPE_DEFAULT:Ljava/lang/String; = "CDATA"


# instance fields
.field protected mFirst:Lorg/codehaus/stax2/validation/XMLValidator;

.field protected mSecond:Lorg/codehaus/stax2/validation/XMLValidator;


# direct methods
.method public constructor <init>(Lorg/codehaus/stax2/validation/XMLValidator;Lorg/codehaus/stax2/validation/XMLValidator;)V
    .locals 0

    invoke-direct {p0}, Lorg/codehaus/stax2/validation/XMLValidator;-><init>()V

    iput-object p1, p0, Lorg/codehaus/stax2/validation/ValidatorPair;->mFirst:Lorg/codehaus/stax2/validation/XMLValidator;

    iput-object p2, p0, Lorg/codehaus/stax2/validation/ValidatorPair;->mSecond:Lorg/codehaus/stax2/validation/XMLValidator;

    return-void
.end method

.method private doRemoveValidator(Lorg/codehaus/stax2/validation/XMLValidationSchema;[Lorg/codehaus/stax2/validation/XMLValidator;)Z
    .locals 2

    .line 1
    iget-object v0, p0, Lorg/codehaus/stax2/validation/ValidatorPair;->mFirst:Lorg/codehaus/stax2/validation/XMLValidator;

    invoke-static {v0, p1, p2}, Lorg/codehaus/stax2/validation/ValidatorPair;->removeValidator(Lorg/codehaus/stax2/validation/XMLValidator;Lorg/codehaus/stax2/validation/XMLValidationSchema;[Lorg/codehaus/stax2/validation/XMLValidator;)Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_1

    aget-object p1, p2, v1

    if-nez p1, :cond_0

    iget-object p1, p0, Lorg/codehaus/stax2/validation/ValidatorPair;->mSecond:Lorg/codehaus/stax2/validation/XMLValidator;

    aput-object p1, p2, v1

    goto :goto_0

    :cond_0
    iput-object p1, p0, Lorg/codehaus/stax2/validation/ValidatorPair;->mFirst:Lorg/codehaus/stax2/validation/XMLValidator;

    aput-object p0, p2, v1

    :goto_0
    return v1

    :cond_1
    iget-object v0, p0, Lorg/codehaus/stax2/validation/ValidatorPair;->mSecond:Lorg/codehaus/stax2/validation/XMLValidator;

    invoke-static {v0, p1, p2}, Lorg/codehaus/stax2/validation/ValidatorPair;->removeValidator(Lorg/codehaus/stax2/validation/XMLValidator;Lorg/codehaus/stax2/validation/XMLValidationSchema;[Lorg/codehaus/stax2/validation/XMLValidator;)Z

    move-result p1

    if-eqz p1, :cond_3

    aget-object p1, p2, v1

    if-nez p1, :cond_2

    iget-object p1, p0, Lorg/codehaus/stax2/validation/ValidatorPair;->mFirst:Lorg/codehaus/stax2/validation/XMLValidator;

    aput-object p1, p2, v1

    goto :goto_1

    :cond_2
    iput-object p1, p0, Lorg/codehaus/stax2/validation/ValidatorPair;->mSecond:Lorg/codehaus/stax2/validation/XMLValidator;

    aput-object p0, p2, v1

    :goto_1
    return v1

    :cond_3
    const/4 p1, 0x0

    return p1
.end method

.method private doRemoveValidator(Lorg/codehaus/stax2/validation/XMLValidator;[Lorg/codehaus/stax2/validation/XMLValidator;)Z
    .locals 2

    .line 2
    iget-object v0, p0, Lorg/codehaus/stax2/validation/ValidatorPair;->mFirst:Lorg/codehaus/stax2/validation/XMLValidator;

    invoke-static {v0, p1, p2}, Lorg/codehaus/stax2/validation/ValidatorPair;->removeValidator(Lorg/codehaus/stax2/validation/XMLValidator;Lorg/codehaus/stax2/validation/XMLValidator;[Lorg/codehaus/stax2/validation/XMLValidator;)Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_1

    aget-object p1, p2, v1

    if-nez p1, :cond_0

    iget-object p1, p0, Lorg/codehaus/stax2/validation/ValidatorPair;->mSecond:Lorg/codehaus/stax2/validation/XMLValidator;

    aput-object p1, p2, v1

    goto :goto_0

    :cond_0
    iput-object p1, p0, Lorg/codehaus/stax2/validation/ValidatorPair;->mFirst:Lorg/codehaus/stax2/validation/XMLValidator;

    aput-object p0, p2, v1

    :goto_0
    return v1

    :cond_1
    iget-object v0, p0, Lorg/codehaus/stax2/validation/ValidatorPair;->mSecond:Lorg/codehaus/stax2/validation/XMLValidator;

    invoke-static {v0, p1, p2}, Lorg/codehaus/stax2/validation/ValidatorPair;->removeValidator(Lorg/codehaus/stax2/validation/XMLValidator;Lorg/codehaus/stax2/validation/XMLValidator;[Lorg/codehaus/stax2/validation/XMLValidator;)Z

    move-result p1

    if-eqz p1, :cond_3

    aget-object p1, p2, v1

    if-nez p1, :cond_2

    iget-object p1, p0, Lorg/codehaus/stax2/validation/ValidatorPair;->mFirst:Lorg/codehaus/stax2/validation/XMLValidator;

    aput-object p1, p2, v1

    goto :goto_1

    :cond_2
    iput-object p1, p0, Lorg/codehaus/stax2/validation/ValidatorPair;->mSecond:Lorg/codehaus/stax2/validation/XMLValidator;

    aput-object p0, p2, v1

    :goto_1
    return v1

    :cond_3
    const/4 p1, 0x0

    return p1
.end method

.method public static removeValidator(Lorg/codehaus/stax2/validation/XMLValidator;Lorg/codehaus/stax2/validation/XMLValidationSchema;[Lorg/codehaus/stax2/validation/XMLValidator;)Z
    .locals 2

    .line 1
    instance-of v0, p0, Lorg/codehaus/stax2/validation/ValidatorPair;

    if-eqz v0, :cond_0

    check-cast p0, Lorg/codehaus/stax2/validation/ValidatorPair;

    invoke-direct {p0, p1, p2}, Lorg/codehaus/stax2/validation/ValidatorPair;->doRemoveValidator(Lorg/codehaus/stax2/validation/XMLValidationSchema;[Lorg/codehaus/stax2/validation/XMLValidator;)Z

    move-result p0

    return p0

    :cond_0
    invoke-virtual {p0}, Lorg/codehaus/stax2/validation/XMLValidator;->getSchema()Lorg/codehaus/stax2/validation/XMLValidationSchema;

    move-result-object v0

    const/4 v1, 0x0

    if-ne v0, p1, :cond_1

    aput-object p0, p2, v1

    const/4 p0, 0x0

    const/4 p1, 0x1

    aput-object p0, p2, p1

    return p1

    :cond_1
    return v1
.end method

.method public static removeValidator(Lorg/codehaus/stax2/validation/XMLValidator;Lorg/codehaus/stax2/validation/XMLValidator;[Lorg/codehaus/stax2/validation/XMLValidator;)Z
    .locals 2

    .line 2
    const/4 v0, 0x0

    if-ne p0, p1, :cond_0

    aput-object p0, p2, v0

    const/4 p0, 0x0

    const/4 p1, 0x1

    aput-object p0, p2, p1

    return p1

    :cond_0
    instance-of v1, p0, Lorg/codehaus/stax2/validation/ValidatorPair;

    if-eqz v1, :cond_1

    check-cast p0, Lorg/codehaus/stax2/validation/ValidatorPair;

    invoke-direct {p0, p1, p2}, Lorg/codehaus/stax2/validation/ValidatorPair;->doRemoveValidator(Lorg/codehaus/stax2/validation/XMLValidator;[Lorg/codehaus/stax2/validation/XMLValidator;)Z

    move-result p0

    return p0

    :cond_1
    return v0
.end method


# virtual methods
.method public getAttributeType(I)Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lorg/codehaus/stax2/validation/ValidatorPair;->mFirst:Lorg/codehaus/stax2/validation/XMLValidator;

    invoke-virtual {v0, p1}, Lorg/codehaus/stax2/validation/XMLValidator;->getAttributeType(I)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "CDATA"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    iget-object v1, p0, Lorg/codehaus/stax2/validation/ValidatorPair;->mSecond:Lorg/codehaus/stax2/validation/XMLValidator;

    invoke-virtual {v1, p1}, Lorg/codehaus/stax2/validation/XMLValidator;->getAttributeType(I)Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_1

    return-object p1

    :cond_1
    return-object v0
.end method

.method public getIdAttrIndex()I
    .locals 1

    iget-object v0, p0, Lorg/codehaus/stax2/validation/ValidatorPair;->mFirst:Lorg/codehaus/stax2/validation/XMLValidator;

    invoke-virtual {v0}, Lorg/codehaus/stax2/validation/XMLValidator;->getIdAttrIndex()I

    move-result v0

    if-gez v0, :cond_0

    iget-object v0, p0, Lorg/codehaus/stax2/validation/ValidatorPair;->mSecond:Lorg/codehaus/stax2/validation/XMLValidator;

    invoke-virtual {v0}, Lorg/codehaus/stax2/validation/XMLValidator;->getIdAttrIndex()I

    move-result v0

    :cond_0
    return v0
.end method

.method public getNotationAttrIndex()I
    .locals 1

    iget-object v0, p0, Lorg/codehaus/stax2/validation/ValidatorPair;->mFirst:Lorg/codehaus/stax2/validation/XMLValidator;

    invoke-virtual {v0}, Lorg/codehaus/stax2/validation/XMLValidator;->getNotationAttrIndex()I

    move-result v0

    if-gez v0, :cond_0

    iget-object v0, p0, Lorg/codehaus/stax2/validation/ValidatorPair;->mSecond:Lorg/codehaus/stax2/validation/XMLValidator;

    invoke-virtual {v0}, Lorg/codehaus/stax2/validation/XMLValidator;->getNotationAttrIndex()I

    move-result v0

    :cond_0
    return v0
.end method

.method public getSchema()Lorg/codehaus/stax2/validation/XMLValidationSchema;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public validateAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .line 1
    iget-object v0, p0, Lorg/codehaus/stax2/validation/ValidatorPair;->mFirst:Lorg/codehaus/stax2/validation/XMLValidator;

    invoke-virtual {v0, p1, p2, p3, p4}, Lorg/codehaus/stax2/validation/XMLValidator;->validateAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    move-object p4, v0

    :cond_0
    iget-object v0, p0, Lorg/codehaus/stax2/validation/ValidatorPair;->mSecond:Lorg/codehaus/stax2/validation/XMLValidator;

    invoke-virtual {v0, p1, p2, p3, p4}, Lorg/codehaus/stax2/validation/XMLValidator;->validateAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public validateAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[CII)Ljava/lang/String;
    .locals 7

    .line 2
    iget-object v0, p0, Lorg/codehaus/stax2/validation/ValidatorPair;->mFirst:Lorg/codehaus/stax2/validation/XMLValidator;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    move v6, p6

    invoke-virtual/range {v0 .. v6}, Lorg/codehaus/stax2/validation/XMLValidator;->validateAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[CII)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object p4, p0, Lorg/codehaus/stax2/validation/ValidatorPair;->mSecond:Lorg/codehaus/stax2/validation/XMLValidator;

    invoke-virtual {p4, p1, p2, p3, v0}, Lorg/codehaus/stax2/validation/XMLValidator;->validateAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_0
    iget-object v0, p0, Lorg/codehaus/stax2/validation/ValidatorPair;->mSecond:Lorg/codehaus/stax2/validation/XMLValidator;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    move v6, p6

    invoke-virtual/range {v0 .. v6}, Lorg/codehaus/stax2/validation/XMLValidator;->validateAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[CII)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public validateElementAndAttributes()I
    .locals 2

    iget-object v0, p0, Lorg/codehaus/stax2/validation/ValidatorPair;->mFirst:Lorg/codehaus/stax2/validation/XMLValidator;

    invoke-virtual {v0}, Lorg/codehaus/stax2/validation/XMLValidator;->validateElementAndAttributes()I

    move-result v0

    iget-object v1, p0, Lorg/codehaus/stax2/validation/ValidatorPair;->mSecond:Lorg/codehaus/stax2/validation/XMLValidator;

    invoke-virtual {v1}, Lorg/codehaus/stax2/validation/XMLValidator;->validateElementAndAttributes()I

    move-result v1

    if-ge v0, v1, :cond_0

    goto :goto_0

    :cond_0
    move v0, v1

    :goto_0
    return v0
.end method

.method public validateElementEnd(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    .locals 2

    iget-object v0, p0, Lorg/codehaus/stax2/validation/ValidatorPair;->mFirst:Lorg/codehaus/stax2/validation/XMLValidator;

    invoke-virtual {v0, p1, p2, p3}, Lorg/codehaus/stax2/validation/XMLValidator;->validateElementEnd(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iget-object v1, p0, Lorg/codehaus/stax2/validation/ValidatorPair;->mSecond:Lorg/codehaus/stax2/validation/XMLValidator;

    invoke-virtual {v1, p1, p2, p3}, Lorg/codehaus/stax2/validation/XMLValidator;->validateElementEnd(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result p1

    if-ge v0, p1, :cond_0

    goto :goto_0

    :cond_0
    move v0, p1

    :goto_0
    return v0
.end method

.method public validateElementStart(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lorg/codehaus/stax2/validation/ValidatorPair;->mFirst:Lorg/codehaus/stax2/validation/XMLValidator;

    invoke-virtual {v0, p1, p2, p3}, Lorg/codehaus/stax2/validation/XMLValidator;->validateElementStart(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lorg/codehaus/stax2/validation/ValidatorPair;->mSecond:Lorg/codehaus/stax2/validation/XMLValidator;

    invoke-virtual {v0, p1, p2, p3}, Lorg/codehaus/stax2/validation/XMLValidator;->validateElementStart(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public validateText(Ljava/lang/String;Z)V
    .locals 1

    .line 1
    iget-object v0, p0, Lorg/codehaus/stax2/validation/ValidatorPair;->mFirst:Lorg/codehaus/stax2/validation/XMLValidator;

    invoke-virtual {v0, p1, p2}, Lorg/codehaus/stax2/validation/XMLValidator;->validateText(Ljava/lang/String;Z)V

    iget-object v0, p0, Lorg/codehaus/stax2/validation/ValidatorPair;->mSecond:Lorg/codehaus/stax2/validation/XMLValidator;

    invoke-virtual {v0, p1, p2}, Lorg/codehaus/stax2/validation/XMLValidator;->validateText(Ljava/lang/String;Z)V

    return-void
.end method

.method public validateText([CIIZ)V
    .locals 1

    .line 2
    iget-object v0, p0, Lorg/codehaus/stax2/validation/ValidatorPair;->mFirst:Lorg/codehaus/stax2/validation/XMLValidator;

    invoke-virtual {v0, p1, p2, p3, p4}, Lorg/codehaus/stax2/validation/XMLValidator;->validateText([CIIZ)V

    iget-object v0, p0, Lorg/codehaus/stax2/validation/ValidatorPair;->mSecond:Lorg/codehaus/stax2/validation/XMLValidator;

    invoke-virtual {v0, p1, p2, p3, p4}, Lorg/codehaus/stax2/validation/XMLValidator;->validateText([CIIZ)V

    return-void
.end method

.method public validationCompleted(Z)V
    .locals 1

    iget-object v0, p0, Lorg/codehaus/stax2/validation/ValidatorPair;->mFirst:Lorg/codehaus/stax2/validation/XMLValidator;

    invoke-virtual {v0, p1}, Lorg/codehaus/stax2/validation/XMLValidator;->validationCompleted(Z)V

    iget-object v0, p0, Lorg/codehaus/stax2/validation/ValidatorPair;->mSecond:Lorg/codehaus/stax2/validation/XMLValidator;

    invoke-virtual {v0, p1}, Lorg/codehaus/stax2/validation/XMLValidator;->validationCompleted(Z)V

    return-void
.end method

.class public Lorg/codehaus/stax2/typed/TypedXMLStreamException;
.super Ljavax/xml/stream/XMLStreamException;
.source "SourceFile"


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field protected mLexical:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/IllegalArgumentException;)V
    .locals 0

    .line 1
    invoke-direct {p0, p2}, Ljavax/xml/stream/XMLStreamException;-><init>(Ljava/lang/Throwable;)V

    iput-object p1, p0, Lorg/codehaus/stax2/typed/TypedXMLStreamException;->mLexical:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 2
    invoke-direct {p0, p2}, Ljavax/xml/stream/XMLStreamException;-><init>(Ljava/lang/String;)V

    iput-object p1, p0, Lorg/codehaus/stax2/typed/TypedXMLStreamException;->mLexical:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/IllegalArgumentException;)V
    .locals 0

    .line 3
    invoke-direct {p0, p2, p3}, Ljavax/xml/stream/XMLStreamException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    iput-object p1, p0, Lorg/codehaus/stax2/typed/TypedXMLStreamException;->mLexical:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljavax/xml/stream/Location;)V
    .locals 0

    .line 4
    invoke-direct {p0, p2, p3}, Ljavax/xml/stream/XMLStreamException;-><init>(Ljava/lang/String;Ljavax/xml/stream/Location;)V

    iput-object p1, p0, Lorg/codehaus/stax2/typed/TypedXMLStreamException;->mLexical:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljavax/xml/stream/Location;Ljava/lang/IllegalArgumentException;)V
    .locals 0

    .line 5
    invoke-direct {p0, p2, p3, p4}, Ljavax/xml/stream/XMLStreamException;-><init>(Ljava/lang/String;Ljavax/xml/stream/Location;Ljava/lang/Throwable;)V

    iput-object p1, p0, Lorg/codehaus/stax2/typed/TypedXMLStreamException;->mLexical:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getLexical()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/codehaus/stax2/typed/TypedXMLStreamException;->mLexical:Ljava/lang/String;

    return-object v0
.end method

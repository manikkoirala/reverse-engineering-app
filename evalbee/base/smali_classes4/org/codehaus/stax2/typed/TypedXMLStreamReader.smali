.class public interface abstract Lorg/codehaus/stax2/typed/TypedXMLStreamReader;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljavax/xml/stream/XMLStreamReader;


# virtual methods
.method public abstract getAttributeAs(ILorg/codehaus/stax2/typed/TypedValueDecoder;)V
.end method

.method public abstract getAttributeAsArray(ILorg/codehaus/stax2/typed/TypedArrayDecoder;)I
.end method

.method public abstract getAttributeAsBinary(I)[B
.end method

.method public abstract getAttributeAsBinary(ILorg/codehaus/stax2/typed/Base64Variant;)[B
.end method

.method public abstract getAttributeAsBoolean(I)Z
.end method

.method public abstract getAttributeAsDecimal(I)Ljava/math/BigDecimal;
.end method

.method public abstract getAttributeAsDouble(I)D
.end method

.method public abstract getAttributeAsDoubleArray(I)[D
.end method

.method public abstract getAttributeAsFloat(I)F
.end method

.method public abstract getAttributeAsFloatArray(I)[F
.end method

.method public abstract getAttributeAsInt(I)I
.end method

.method public abstract getAttributeAsIntArray(I)[I
.end method

.method public abstract getAttributeAsInteger(I)Ljava/math/BigInteger;
.end method

.method public abstract getAttributeAsLong(I)J
.end method

.method public abstract getAttributeAsLongArray(I)[J
.end method

.method public abstract getAttributeAsQName(I)Ljavax/xml/namespace/QName;
.end method

.method public abstract getAttributeIndex(Ljava/lang/String;Ljava/lang/String;)I
.end method

.method public abstract getElementAs(Lorg/codehaus/stax2/typed/TypedValueDecoder;)V
.end method

.method public abstract getElementAsBinary()[B
.end method

.method public abstract getElementAsBinary(Lorg/codehaus/stax2/typed/Base64Variant;)[B
.end method

.method public abstract getElementAsBoolean()Z
.end method

.method public abstract getElementAsDecimal()Ljava/math/BigDecimal;
.end method

.method public abstract getElementAsDouble()D
.end method

.method public abstract getElementAsFloat()F
.end method

.method public abstract getElementAsInt()I
.end method

.method public abstract getElementAsInteger()Ljava/math/BigInteger;
.end method

.method public abstract getElementAsLong()J
.end method

.method public abstract getElementAsQName()Ljavax/xml/namespace/QName;
.end method

.method public abstract readElementAsArray(Lorg/codehaus/stax2/typed/TypedArrayDecoder;)I
.end method

.method public abstract readElementAsBinary([BII)I
.end method

.method public abstract readElementAsBinary([BIILorg/codehaus/stax2/typed/Base64Variant;)I
.end method

.method public abstract readElementAsDoubleArray([DII)I
.end method

.method public abstract readElementAsFloatArray([FII)I
.end method

.method public abstract readElementAsIntArray([III)I
.end method

.method public abstract readElementAsLongArray([JII)I
.end method

.class public interface abstract Lorg/codehaus/stax2/typed/TypedXMLStreamWriter;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljavax/xml/stream/XMLStreamWriter;


# virtual methods
.method public abstract writeBinary(Lorg/codehaus/stax2/typed/Base64Variant;[BII)V
.end method

.method public abstract writeBinary([BII)V
.end method

.method public abstract writeBinaryAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[B)V
.end method

.method public abstract writeBinaryAttribute(Lorg/codehaus/stax2/typed/Base64Variant;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[B)V
.end method

.method public abstract writeBoolean(Z)V
.end method

.method public abstract writeBooleanAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
.end method

.method public abstract writeDecimal(Ljava/math/BigDecimal;)V
.end method

.method public abstract writeDecimalAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/math/BigDecimal;)V
.end method

.method public abstract writeDouble(D)V
.end method

.method public abstract writeDoubleArray([DII)V
.end method

.method public abstract writeDoubleArrayAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[D)V
.end method

.method public abstract writeDoubleAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;D)V
.end method

.method public abstract writeFloat(F)V
.end method

.method public abstract writeFloatArray([FII)V
.end method

.method public abstract writeFloatArrayAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[F)V
.end method

.method public abstract writeFloatAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;F)V
.end method

.method public abstract writeInt(I)V
.end method

.method public abstract writeIntArray([III)V
.end method

.method public abstract writeIntArrayAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[I)V
.end method

.method public abstract writeIntAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
.end method

.method public abstract writeInteger(Ljava/math/BigInteger;)V
.end method

.method public abstract writeIntegerAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/math/BigInteger;)V
.end method

.method public abstract writeLong(J)V
.end method

.method public abstract writeLongArray([JII)V
.end method

.method public abstract writeLongArrayAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[J)V
.end method

.method public abstract writeLongAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V
.end method

.method public abstract writeQName(Ljavax/xml/namespace/QName;)V
.end method

.method public abstract writeQNameAttribute(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljavax/xml/namespace/QName;)V
.end method

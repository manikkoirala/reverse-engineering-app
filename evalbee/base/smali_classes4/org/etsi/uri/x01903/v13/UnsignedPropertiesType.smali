.class public interface abstract Lorg/etsi/uri/x01903/v13/UnsignedPropertiesType;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/xmlbeans/XmlObject;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/etsi/uri/x01903/v13/UnsignedPropertiesType$Factory;
    }
.end annotation


# static fields
.field public static final type:Lorg/apache/xmlbeans/SchemaType;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-class v0, Lorg/etsi/uri/x01903/v13/UnsignedPropertiesType;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    const-string v1, "schemaorg_apache_xmlbeans.system.s8C3F193EE11A2F798ACF65489B9E6078"

    invoke-static {v0, v1}, Lorg/apache/xmlbeans/XmlBeans;->typeSystemForClassLoader(Ljava/lang/ClassLoader;Ljava/lang/String;)Lorg/apache/xmlbeans/SchemaTypeSystem;

    move-result-object v0

    const-string v1, "unsignedpropertiestype49d6type"

    invoke-interface {v0, v1}, Lorg/apache/xmlbeans/SchemaTypeSystem;->resolveHandle(Ljava/lang/String;)Lorg/apache/xmlbeans/SchemaComponent;

    move-result-object v0

    check-cast v0, Lorg/apache/xmlbeans/SchemaType;

    sput-object v0, Lorg/etsi/uri/x01903/v13/UnsignedPropertiesType;->type:Lorg/apache/xmlbeans/SchemaType;

    return-void
.end method


# virtual methods
.method public abstract addNewUnsignedDataObjectProperties()Lorg/etsi/uri/x01903/v13/UnsignedDataObjectPropertiesType;
.end method

.method public abstract addNewUnsignedSignatureProperties()Lorg/etsi/uri/x01903/v13/UnsignedSignaturePropertiesType;
.end method

.method public abstract getId()Ljava/lang/String;
.end method

.method public abstract getUnsignedDataObjectProperties()Lorg/etsi/uri/x01903/v13/UnsignedDataObjectPropertiesType;
.end method

.method public abstract getUnsignedSignatureProperties()Lorg/etsi/uri/x01903/v13/UnsignedSignaturePropertiesType;
.end method

.method public abstract isSetId()Z
.end method

.method public abstract isSetUnsignedDataObjectProperties()Z
.end method

.method public abstract isSetUnsignedSignatureProperties()Z
.end method

.method public abstract setId(Ljava/lang/String;)V
.end method

.method public abstract setUnsignedDataObjectProperties(Lorg/etsi/uri/x01903/v13/UnsignedDataObjectPropertiesType;)V
.end method

.method public abstract setUnsignedSignatureProperties(Lorg/etsi/uri/x01903/v13/UnsignedSignaturePropertiesType;)V
.end method

.method public abstract unsetId()V
.end method

.method public abstract unsetUnsignedDataObjectProperties()V
.end method

.method public abstract unsetUnsignedSignatureProperties()V
.end method

.method public abstract xgetId()Lorg/apache/xmlbeans/XmlID;
.end method

.method public abstract xsetId(Lorg/apache/xmlbeans/XmlID;)V
.end method

.class public interface abstract Lorg/etsi/uri/x01903/v13/RevocationValuesType;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/xmlbeans/XmlObject;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/etsi/uri/x01903/v13/RevocationValuesType$Factory;
    }
.end annotation


# static fields
.field public static final type:Lorg/apache/xmlbeans/SchemaType;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-class v0, Lorg/etsi/uri/x01903/v13/RevocationValuesType;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    const-string v1, "schemaorg_apache_xmlbeans.system.s8C3F193EE11A2F798ACF65489B9E6078"

    invoke-static {v0, v1}, Lorg/apache/xmlbeans/XmlBeans;->typeSystemForClassLoader(Ljava/lang/ClassLoader;Ljava/lang/String;)Lorg/apache/xmlbeans/SchemaTypeSystem;

    move-result-object v0

    const-string v1, "revocationvaluestype9a6etype"

    invoke-interface {v0, v1}, Lorg/apache/xmlbeans/SchemaTypeSystem;->resolveHandle(Ljava/lang/String;)Lorg/apache/xmlbeans/SchemaComponent;

    move-result-object v0

    check-cast v0, Lorg/apache/xmlbeans/SchemaType;

    sput-object v0, Lorg/etsi/uri/x01903/v13/RevocationValuesType;->type:Lorg/apache/xmlbeans/SchemaType;

    return-void
.end method


# virtual methods
.method public abstract addNewCRLValues()Lorg/etsi/uri/x01903/v13/CRLValuesType;
.end method

.method public abstract addNewOCSPValues()Lorg/etsi/uri/x01903/v13/OCSPValuesType;
.end method

.method public abstract addNewOtherValues()Lorg/etsi/uri/x01903/v13/OtherCertStatusValuesType;
.end method

.method public abstract getCRLValues()Lorg/etsi/uri/x01903/v13/CRLValuesType;
.end method

.method public abstract getId()Ljava/lang/String;
.end method

.method public abstract getOCSPValues()Lorg/etsi/uri/x01903/v13/OCSPValuesType;
.end method

.method public abstract getOtherValues()Lorg/etsi/uri/x01903/v13/OtherCertStatusValuesType;
.end method

.method public abstract isSetCRLValues()Z
.end method

.method public abstract isSetId()Z
.end method

.method public abstract isSetOCSPValues()Z
.end method

.method public abstract isSetOtherValues()Z
.end method

.method public abstract setCRLValues(Lorg/etsi/uri/x01903/v13/CRLValuesType;)V
.end method

.method public abstract setId(Ljava/lang/String;)V
.end method

.method public abstract setOCSPValues(Lorg/etsi/uri/x01903/v13/OCSPValuesType;)V
.end method

.method public abstract setOtherValues(Lorg/etsi/uri/x01903/v13/OtherCertStatusValuesType;)V
.end method

.method public abstract unsetCRLValues()V
.end method

.method public abstract unsetId()V
.end method

.method public abstract unsetOCSPValues()V
.end method

.method public abstract unsetOtherValues()V
.end method

.method public abstract xgetId()Lorg/apache/xmlbeans/XmlID;
.end method

.method public abstract xsetId(Lorg/apache/xmlbeans/XmlID;)V
.end method

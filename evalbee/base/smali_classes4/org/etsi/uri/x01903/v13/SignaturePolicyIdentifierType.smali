.class public interface abstract Lorg/etsi/uri/x01903/v13/SignaturePolicyIdentifierType;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/xmlbeans/XmlObject;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/etsi/uri/x01903/v13/SignaturePolicyIdentifierType$Factory;
    }
.end annotation


# static fields
.field public static final type:Lorg/apache/xmlbeans/SchemaType;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-class v0, Lorg/etsi/uri/x01903/v13/SignaturePolicyIdentifierType;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    const-string v1, "schemaorg_apache_xmlbeans.system.s8C3F193EE11A2F798ACF65489B9E6078"

    invoke-static {v0, v1}, Lorg/apache/xmlbeans/XmlBeans;->typeSystemForClassLoader(Ljava/lang/ClassLoader;Ljava/lang/String;)Lorg/apache/xmlbeans/SchemaTypeSystem;

    move-result-object v0

    const-string v1, "signaturepolicyidentifiertype80aftype"

    invoke-interface {v0, v1}, Lorg/apache/xmlbeans/SchemaTypeSystem;->resolveHandle(Ljava/lang/String;)Lorg/apache/xmlbeans/SchemaComponent;

    move-result-object v0

    check-cast v0, Lorg/apache/xmlbeans/SchemaType;

    sput-object v0, Lorg/etsi/uri/x01903/v13/SignaturePolicyIdentifierType;->type:Lorg/apache/xmlbeans/SchemaType;

    return-void
.end method


# virtual methods
.method public abstract addNewSignaturePolicyId()Lorg/etsi/uri/x01903/v13/SignaturePolicyIdType;
.end method

.method public abstract addNewSignaturePolicyImplied()Lorg/apache/xmlbeans/XmlObject;
.end method

.method public abstract getSignaturePolicyId()Lorg/etsi/uri/x01903/v13/SignaturePolicyIdType;
.end method

.method public abstract getSignaturePolicyImplied()Lorg/apache/xmlbeans/XmlObject;
.end method

.method public abstract isSetSignaturePolicyId()Z
.end method

.method public abstract isSetSignaturePolicyImplied()Z
.end method

.method public abstract setSignaturePolicyId(Lorg/etsi/uri/x01903/v13/SignaturePolicyIdType;)V
.end method

.method public abstract setSignaturePolicyImplied(Lorg/apache/xmlbeans/XmlObject;)V
.end method

.method public abstract unsetSignaturePolicyId()V
.end method

.method public abstract unsetSignaturePolicyImplied()V
.end method

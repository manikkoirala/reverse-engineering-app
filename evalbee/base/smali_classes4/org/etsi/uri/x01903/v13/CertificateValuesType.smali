.class public interface abstract Lorg/etsi/uri/x01903/v13/CertificateValuesType;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/xmlbeans/XmlObject;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/etsi/uri/x01903/v13/CertificateValuesType$Factory;
    }
.end annotation


# static fields
.field public static final type:Lorg/apache/xmlbeans/SchemaType;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-class v0, Lorg/etsi/uri/x01903/v13/CertificateValuesType;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    const-string v1, "schemaorg_apache_xmlbeans.system.s8C3F193EE11A2F798ACF65489B9E6078"

    invoke-static {v0, v1}, Lorg/apache/xmlbeans/XmlBeans;->typeSystemForClassLoader(Ljava/lang/ClassLoader;Ljava/lang/String;)Lorg/apache/xmlbeans/SchemaTypeSystem;

    move-result-object v0

    const-string v1, "certificatevaluestype5c75type"

    invoke-interface {v0, v1}, Lorg/apache/xmlbeans/SchemaTypeSystem;->resolveHandle(Ljava/lang/String;)Lorg/apache/xmlbeans/SchemaComponent;

    move-result-object v0

    check-cast v0, Lorg/apache/xmlbeans/SchemaType;

    sput-object v0, Lorg/etsi/uri/x01903/v13/CertificateValuesType;->type:Lorg/apache/xmlbeans/SchemaType;

    return-void
.end method


# virtual methods
.method public abstract addNewEncapsulatedX509Certificate()Lorg/etsi/uri/x01903/v13/EncapsulatedPKIDataType;
.end method

.method public abstract addNewOtherCertificate()Lorg/etsi/uri/x01903/v13/AnyType;
.end method

.method public abstract getEncapsulatedX509CertificateArray(I)Lorg/etsi/uri/x01903/v13/EncapsulatedPKIDataType;
.end method

.method public abstract getEncapsulatedX509CertificateArray()[Lorg/etsi/uri/x01903/v13/EncapsulatedPKIDataType;
.end method

.method public abstract getEncapsulatedX509CertificateList()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lorg/etsi/uri/x01903/v13/EncapsulatedPKIDataType;",
            ">;"
        }
    .end annotation
.end method

.method public abstract getId()Ljava/lang/String;
.end method

.method public abstract getOtherCertificateArray(I)Lorg/etsi/uri/x01903/v13/AnyType;
.end method

.method public abstract getOtherCertificateArray()[Lorg/etsi/uri/x01903/v13/AnyType;
.end method

.method public abstract getOtherCertificateList()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lorg/etsi/uri/x01903/v13/AnyType;",
            ">;"
        }
    .end annotation
.end method

.method public abstract insertNewEncapsulatedX509Certificate(I)Lorg/etsi/uri/x01903/v13/EncapsulatedPKIDataType;
.end method

.method public abstract insertNewOtherCertificate(I)Lorg/etsi/uri/x01903/v13/AnyType;
.end method

.method public abstract isSetId()Z
.end method

.method public abstract removeEncapsulatedX509Certificate(I)V
.end method

.method public abstract removeOtherCertificate(I)V
.end method

.method public abstract setEncapsulatedX509CertificateArray(ILorg/etsi/uri/x01903/v13/EncapsulatedPKIDataType;)V
.end method

.method public abstract setEncapsulatedX509CertificateArray([Lorg/etsi/uri/x01903/v13/EncapsulatedPKIDataType;)V
.end method

.method public abstract setId(Ljava/lang/String;)V
.end method

.method public abstract setOtherCertificateArray(ILorg/etsi/uri/x01903/v13/AnyType;)V
.end method

.method public abstract setOtherCertificateArray([Lorg/etsi/uri/x01903/v13/AnyType;)V
.end method

.method public abstract sizeOfEncapsulatedX509CertificateArray()I
.end method

.method public abstract sizeOfOtherCertificateArray()I
.end method

.method public abstract unsetId()V
.end method

.method public abstract xgetId()Lorg/apache/xmlbeans/XmlID;
.end method

.method public abstract xsetId(Lorg/apache/xmlbeans/XmlID;)V
.end method

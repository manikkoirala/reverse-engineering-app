.class public interface abstract Lorg/etsi/uri/x01903/v13/SignedSignaturePropertiesType;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/xmlbeans/XmlObject;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/etsi/uri/x01903/v13/SignedSignaturePropertiesType$Factory;
    }
.end annotation


# static fields
.field public static final type:Lorg/apache/xmlbeans/SchemaType;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-class v0, Lorg/etsi/uri/x01903/v13/SignedSignaturePropertiesType;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    const-string v1, "schemaorg_apache_xmlbeans.system.s8C3F193EE11A2F798ACF65489B9E6078"

    invoke-static {v0, v1}, Lorg/apache/xmlbeans/XmlBeans;->typeSystemForClassLoader(Ljava/lang/ClassLoader;Ljava/lang/String;)Lorg/apache/xmlbeans/SchemaTypeSystem;

    move-result-object v0

    const-string v1, "signedsignaturepropertiestype06abtype"

    invoke-interface {v0, v1}, Lorg/apache/xmlbeans/SchemaTypeSystem;->resolveHandle(Ljava/lang/String;)Lorg/apache/xmlbeans/SchemaComponent;

    move-result-object v0

    check-cast v0, Lorg/apache/xmlbeans/SchemaType;

    sput-object v0, Lorg/etsi/uri/x01903/v13/SignedSignaturePropertiesType;->type:Lorg/apache/xmlbeans/SchemaType;

    return-void
.end method


# virtual methods
.method public abstract addNewSignaturePolicyIdentifier()Lorg/etsi/uri/x01903/v13/SignaturePolicyIdentifierType;
.end method

.method public abstract addNewSignatureProductionPlace()Lorg/etsi/uri/x01903/v13/SignatureProductionPlaceType;
.end method

.method public abstract addNewSignerRole()Lorg/etsi/uri/x01903/v13/SignerRoleType;
.end method

.method public abstract addNewSigningCertificate()Lorg/etsi/uri/x01903/v13/CertIDListType;
.end method

.method public abstract getId()Ljava/lang/String;
.end method

.method public abstract getSignaturePolicyIdentifier()Lorg/etsi/uri/x01903/v13/SignaturePolicyIdentifierType;
.end method

.method public abstract getSignatureProductionPlace()Lorg/etsi/uri/x01903/v13/SignatureProductionPlaceType;
.end method

.method public abstract getSignerRole()Lorg/etsi/uri/x01903/v13/SignerRoleType;
.end method

.method public abstract getSigningCertificate()Lorg/etsi/uri/x01903/v13/CertIDListType;
.end method

.method public abstract getSigningTime()Ljava/util/Calendar;
.end method

.method public abstract isSetId()Z
.end method

.method public abstract isSetSignaturePolicyIdentifier()Z
.end method

.method public abstract isSetSignatureProductionPlace()Z
.end method

.method public abstract isSetSignerRole()Z
.end method

.method public abstract isSetSigningCertificate()Z
.end method

.method public abstract isSetSigningTime()Z
.end method

.method public abstract setId(Ljava/lang/String;)V
.end method

.method public abstract setSignaturePolicyIdentifier(Lorg/etsi/uri/x01903/v13/SignaturePolicyIdentifierType;)V
.end method

.method public abstract setSignatureProductionPlace(Lorg/etsi/uri/x01903/v13/SignatureProductionPlaceType;)V
.end method

.method public abstract setSignerRole(Lorg/etsi/uri/x01903/v13/SignerRoleType;)V
.end method

.method public abstract setSigningCertificate(Lorg/etsi/uri/x01903/v13/CertIDListType;)V
.end method

.method public abstract setSigningTime(Ljava/util/Calendar;)V
.end method

.method public abstract unsetId()V
.end method

.method public abstract unsetSignaturePolicyIdentifier()V
.end method

.method public abstract unsetSignatureProductionPlace()V
.end method

.method public abstract unsetSignerRole()V
.end method

.method public abstract unsetSigningCertificate()V
.end method

.method public abstract unsetSigningTime()V
.end method

.method public abstract xgetId()Lorg/apache/xmlbeans/XmlID;
.end method

.method public abstract xgetSigningTime()Lorg/apache/xmlbeans/XmlDateTime;
.end method

.method public abstract xsetId(Lorg/apache/xmlbeans/XmlID;)V
.end method

.method public abstract xsetSigningTime(Lorg/apache/xmlbeans/XmlDateTime;)V
.end method

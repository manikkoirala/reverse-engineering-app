.class public interface abstract Lorg/etsi/uri/x01903/v13/CertIDType;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/xmlbeans/XmlObject;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/etsi/uri/x01903/v13/CertIDType$Factory;
    }
.end annotation


# static fields
.field public static final type:Lorg/apache/xmlbeans/SchemaType;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-class v0, Lorg/etsi/uri/x01903/v13/CertIDType;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    const-string v1, "schemaorg_apache_xmlbeans.system.s8C3F193EE11A2F798ACF65489B9E6078"

    invoke-static {v0, v1}, Lorg/apache/xmlbeans/XmlBeans;->typeSystemForClassLoader(Ljava/lang/ClassLoader;Ljava/lang/String;)Lorg/apache/xmlbeans/SchemaTypeSystem;

    move-result-object v0

    const-string v1, "certidtypee64dtype"

    invoke-interface {v0, v1}, Lorg/apache/xmlbeans/SchemaTypeSystem;->resolveHandle(Ljava/lang/String;)Lorg/apache/xmlbeans/SchemaComponent;

    move-result-object v0

    check-cast v0, Lorg/apache/xmlbeans/SchemaType;

    sput-object v0, Lorg/etsi/uri/x01903/v13/CertIDType;->type:Lorg/apache/xmlbeans/SchemaType;

    return-void
.end method


# virtual methods
.method public abstract addNewCertDigest()Lorg/etsi/uri/x01903/v13/DigestAlgAndValueType;
.end method

.method public abstract addNewIssuerSerial()Lorg/w3/x2000/x09/xmldsig/X509IssuerSerialType;
.end method

.method public abstract getCertDigest()Lorg/etsi/uri/x01903/v13/DigestAlgAndValueType;
.end method

.method public abstract getIssuerSerial()Lorg/w3/x2000/x09/xmldsig/X509IssuerSerialType;
.end method

.method public abstract getURI()Ljava/lang/String;
.end method

.method public abstract isSetURI()Z
.end method

.method public abstract setCertDigest(Lorg/etsi/uri/x01903/v13/DigestAlgAndValueType;)V
.end method

.method public abstract setIssuerSerial(Lorg/w3/x2000/x09/xmldsig/X509IssuerSerialType;)V
.end method

.method public abstract setURI(Ljava/lang/String;)V
.end method

.method public abstract unsetURI()V
.end method

.method public abstract xgetURI()Lorg/apache/xmlbeans/XmlAnyURI;
.end method

.method public abstract xsetURI(Lorg/apache/xmlbeans/XmlAnyURI;)V
.end method

.class public interface abstract Lorg/etsi/uri/x01903/v13/CompleteRevocationRefsType;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/xmlbeans/XmlObject;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/etsi/uri/x01903/v13/CompleteRevocationRefsType$Factory;
    }
.end annotation


# static fields
.field public static final type:Lorg/apache/xmlbeans/SchemaType;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-class v0, Lorg/etsi/uri/x01903/v13/CompleteRevocationRefsType;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    const-string v1, "schemaorg_apache_xmlbeans.system.s8C3F193EE11A2F798ACF65489B9E6078"

    invoke-static {v0, v1}, Lorg/apache/xmlbeans/XmlBeans;->typeSystemForClassLoader(Ljava/lang/ClassLoader;Ljava/lang/String;)Lorg/apache/xmlbeans/SchemaTypeSystem;

    move-result-object v0

    const-string v1, "completerevocationrefstyped8a5type"

    invoke-interface {v0, v1}, Lorg/apache/xmlbeans/SchemaTypeSystem;->resolveHandle(Ljava/lang/String;)Lorg/apache/xmlbeans/SchemaComponent;

    move-result-object v0

    check-cast v0, Lorg/apache/xmlbeans/SchemaType;

    sput-object v0, Lorg/etsi/uri/x01903/v13/CompleteRevocationRefsType;->type:Lorg/apache/xmlbeans/SchemaType;

    return-void
.end method


# virtual methods
.method public abstract addNewCRLRefs()Lorg/etsi/uri/x01903/v13/CRLRefsType;
.end method

.method public abstract addNewOCSPRefs()Lorg/etsi/uri/x01903/v13/OCSPRefsType;
.end method

.method public abstract addNewOtherRefs()Lorg/etsi/uri/x01903/v13/OtherCertStatusRefsType;
.end method

.method public abstract getCRLRefs()Lorg/etsi/uri/x01903/v13/CRLRefsType;
.end method

.method public abstract getId()Ljava/lang/String;
.end method

.method public abstract getOCSPRefs()Lorg/etsi/uri/x01903/v13/OCSPRefsType;
.end method

.method public abstract getOtherRefs()Lorg/etsi/uri/x01903/v13/OtherCertStatusRefsType;
.end method

.method public abstract isSetCRLRefs()Z
.end method

.method public abstract isSetId()Z
.end method

.method public abstract isSetOCSPRefs()Z
.end method

.method public abstract isSetOtherRefs()Z
.end method

.method public abstract setCRLRefs(Lorg/etsi/uri/x01903/v13/CRLRefsType;)V
.end method

.method public abstract setId(Ljava/lang/String;)V
.end method

.method public abstract setOCSPRefs(Lorg/etsi/uri/x01903/v13/OCSPRefsType;)V
.end method

.method public abstract setOtherRefs(Lorg/etsi/uri/x01903/v13/OtherCertStatusRefsType;)V
.end method

.method public abstract unsetCRLRefs()V
.end method

.method public abstract unsetId()V
.end method

.method public abstract unsetOCSPRefs()V
.end method

.method public abstract unsetOtherRefs()V
.end method

.method public abstract xgetId()Lorg/apache/xmlbeans/XmlID;
.end method

.method public abstract xsetId(Lorg/apache/xmlbeans/XmlID;)V
.end method

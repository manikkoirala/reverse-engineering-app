.class public interface abstract Lorg/etsi/uri/x01903/v13/SignedPropertiesType;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/xmlbeans/XmlObject;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/etsi/uri/x01903/v13/SignedPropertiesType$Factory;
    }
.end annotation


# static fields
.field public static final type:Lorg/apache/xmlbeans/SchemaType;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-class v0, Lorg/etsi/uri/x01903/v13/SignedPropertiesType;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    const-string v1, "schemaorg_apache_xmlbeans.system.s8C3F193EE11A2F798ACF65489B9E6078"

    invoke-static {v0, v1}, Lorg/apache/xmlbeans/XmlBeans;->typeSystemForClassLoader(Ljava/lang/ClassLoader;Ljava/lang/String;)Lorg/apache/xmlbeans/SchemaTypeSystem;

    move-result-object v0

    const-string v1, "signedpropertiestype163dtype"

    invoke-interface {v0, v1}, Lorg/apache/xmlbeans/SchemaTypeSystem;->resolveHandle(Ljava/lang/String;)Lorg/apache/xmlbeans/SchemaComponent;

    move-result-object v0

    check-cast v0, Lorg/apache/xmlbeans/SchemaType;

    sput-object v0, Lorg/etsi/uri/x01903/v13/SignedPropertiesType;->type:Lorg/apache/xmlbeans/SchemaType;

    return-void
.end method


# virtual methods
.method public abstract addNewSignedDataObjectProperties()Lorg/etsi/uri/x01903/v13/SignedDataObjectPropertiesType;
.end method

.method public abstract addNewSignedSignatureProperties()Lorg/etsi/uri/x01903/v13/SignedSignaturePropertiesType;
.end method

.method public abstract getId()Ljava/lang/String;
.end method

.method public abstract getSignedDataObjectProperties()Lorg/etsi/uri/x01903/v13/SignedDataObjectPropertiesType;
.end method

.method public abstract getSignedSignatureProperties()Lorg/etsi/uri/x01903/v13/SignedSignaturePropertiesType;
.end method

.method public abstract isSetId()Z
.end method

.method public abstract isSetSignedDataObjectProperties()Z
.end method

.method public abstract isSetSignedSignatureProperties()Z
.end method

.method public abstract setId(Ljava/lang/String;)V
.end method

.method public abstract setSignedDataObjectProperties(Lorg/etsi/uri/x01903/v13/SignedDataObjectPropertiesType;)V
.end method

.method public abstract setSignedSignatureProperties(Lorg/etsi/uri/x01903/v13/SignedSignaturePropertiesType;)V
.end method

.method public abstract unsetId()V
.end method

.method public abstract unsetSignedDataObjectProperties()V
.end method

.method public abstract unsetSignedSignatureProperties()V
.end method

.method public abstract xgetId()Lorg/apache/xmlbeans/XmlID;
.end method

.method public abstract xsetId(Lorg/apache/xmlbeans/XmlID;)V
.end method

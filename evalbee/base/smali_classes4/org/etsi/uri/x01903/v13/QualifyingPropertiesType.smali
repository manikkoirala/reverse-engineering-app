.class public interface abstract Lorg/etsi/uri/x01903/v13/QualifyingPropertiesType;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/xmlbeans/XmlObject;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/etsi/uri/x01903/v13/QualifyingPropertiesType$Factory;
    }
.end annotation


# static fields
.field public static final type:Lorg/apache/xmlbeans/SchemaType;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-class v0, Lorg/etsi/uri/x01903/v13/QualifyingPropertiesType;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    const-string v1, "schemaorg_apache_xmlbeans.system.s8C3F193EE11A2F798ACF65489B9E6078"

    invoke-static {v0, v1}, Lorg/apache/xmlbeans/XmlBeans;->typeSystemForClassLoader(Ljava/lang/ClassLoader;Ljava/lang/String;)Lorg/apache/xmlbeans/SchemaTypeSystem;

    move-result-object v0

    const-string v1, "qualifyingpropertiestype9e16type"

    invoke-interface {v0, v1}, Lorg/apache/xmlbeans/SchemaTypeSystem;->resolveHandle(Ljava/lang/String;)Lorg/apache/xmlbeans/SchemaComponent;

    move-result-object v0

    check-cast v0, Lorg/apache/xmlbeans/SchemaType;

    sput-object v0, Lorg/etsi/uri/x01903/v13/QualifyingPropertiesType;->type:Lorg/apache/xmlbeans/SchemaType;

    return-void
.end method


# virtual methods
.method public abstract addNewSignedProperties()Lorg/etsi/uri/x01903/v13/SignedPropertiesType;
.end method

.method public abstract addNewUnsignedProperties()Lorg/etsi/uri/x01903/v13/UnsignedPropertiesType;
.end method

.method public abstract getId()Ljava/lang/String;
.end method

.method public abstract getSignedProperties()Lorg/etsi/uri/x01903/v13/SignedPropertiesType;
.end method

.method public abstract getTarget()Ljava/lang/String;
.end method

.method public abstract getUnsignedProperties()Lorg/etsi/uri/x01903/v13/UnsignedPropertiesType;
.end method

.method public abstract isSetId()Z
.end method

.method public abstract isSetSignedProperties()Z
.end method

.method public abstract isSetUnsignedProperties()Z
.end method

.method public abstract setId(Ljava/lang/String;)V
.end method

.method public abstract setSignedProperties(Lorg/etsi/uri/x01903/v13/SignedPropertiesType;)V
.end method

.method public abstract setTarget(Ljava/lang/String;)V
.end method

.method public abstract setUnsignedProperties(Lorg/etsi/uri/x01903/v13/UnsignedPropertiesType;)V
.end method

.method public abstract unsetId()V
.end method

.method public abstract unsetSignedProperties()V
.end method

.method public abstract unsetUnsignedProperties()V
.end method

.method public abstract xgetId()Lorg/apache/xmlbeans/XmlID;
.end method

.method public abstract xgetTarget()Lorg/apache/xmlbeans/XmlAnyURI;
.end method

.method public abstract xsetId(Lorg/apache/xmlbeans/XmlID;)V
.end method

.method public abstract xsetTarget(Lorg/apache/xmlbeans/XmlAnyURI;)V
.end method

.class public interface abstract Lorg/etsi/uri/x01903/v13/DigestAlgAndValueType;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/xmlbeans/XmlObject;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/etsi/uri/x01903/v13/DigestAlgAndValueType$Factory;
    }
.end annotation


# static fields
.field public static final type:Lorg/apache/xmlbeans/SchemaType;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-class v0, Lorg/etsi/uri/x01903/v13/DigestAlgAndValueType;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    const-string v1, "schemaorg_apache_xmlbeans.system.s8C3F193EE11A2F798ACF65489B9E6078"

    invoke-static {v0, v1}, Lorg/apache/xmlbeans/XmlBeans;->typeSystemForClassLoader(Ljava/lang/ClassLoader;Ljava/lang/String;)Lorg/apache/xmlbeans/SchemaTypeSystem;

    move-result-object v0

    const-string v1, "digestalgandvaluetype234etype"

    invoke-interface {v0, v1}, Lorg/apache/xmlbeans/SchemaTypeSystem;->resolveHandle(Ljava/lang/String;)Lorg/apache/xmlbeans/SchemaComponent;

    move-result-object v0

    check-cast v0, Lorg/apache/xmlbeans/SchemaType;

    sput-object v0, Lorg/etsi/uri/x01903/v13/DigestAlgAndValueType;->type:Lorg/apache/xmlbeans/SchemaType;

    return-void
.end method


# virtual methods
.method public abstract addNewDigestMethod()Lorg/w3/x2000/x09/xmldsig/DigestMethodType;
.end method

.method public abstract getDigestMethod()Lorg/w3/x2000/x09/xmldsig/DigestMethodType;
.end method

.method public abstract getDigestValue()[B
.end method

.method public abstract setDigestMethod(Lorg/w3/x2000/x09/xmldsig/DigestMethodType;)V
.end method

.method public abstract setDigestValue([B)V
.end method

.method public abstract xgetDigestValue()Lorg/w3/x2000/x09/xmldsig/DigestValueType;
.end method

.method public abstract xsetDigestValue(Lorg/w3/x2000/x09/xmldsig/DigestValueType;)V
.end method

.class public interface abstract Lorg/etsi/uri/x01903/v13/ResponderIDType;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/xmlbeans/XmlObject;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/etsi/uri/x01903/v13/ResponderIDType$Factory;
    }
.end annotation


# static fields
.field public static final type:Lorg/apache/xmlbeans/SchemaType;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-class v0, Lorg/etsi/uri/x01903/v13/ResponderIDType;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    const-string v1, "schemaorg_apache_xmlbeans.system.s8C3F193EE11A2F798ACF65489B9E6078"

    invoke-static {v0, v1}, Lorg/apache/xmlbeans/XmlBeans;->typeSystemForClassLoader(Ljava/lang/ClassLoader;Ljava/lang/String;)Lorg/apache/xmlbeans/SchemaTypeSystem;

    move-result-object v0

    const-string v1, "responderidtype55b9type"

    invoke-interface {v0, v1}, Lorg/apache/xmlbeans/SchemaTypeSystem;->resolveHandle(Ljava/lang/String;)Lorg/apache/xmlbeans/SchemaComponent;

    move-result-object v0

    check-cast v0, Lorg/apache/xmlbeans/SchemaType;

    sput-object v0, Lorg/etsi/uri/x01903/v13/ResponderIDType;->type:Lorg/apache/xmlbeans/SchemaType;

    return-void
.end method


# virtual methods
.method public abstract getByKey()[B
.end method

.method public abstract getByName()Ljava/lang/String;
.end method

.method public abstract isSetByKey()Z
.end method

.method public abstract isSetByName()Z
.end method

.method public abstract setByKey([B)V
.end method

.method public abstract setByName(Ljava/lang/String;)V
.end method

.method public abstract unsetByKey()V
.end method

.method public abstract unsetByName()V
.end method

.method public abstract xgetByKey()Lorg/apache/xmlbeans/XmlBase64Binary;
.end method

.method public abstract xgetByName()Lorg/apache/xmlbeans/XmlString;
.end method

.method public abstract xsetByKey(Lorg/apache/xmlbeans/XmlBase64Binary;)V
.end method

.method public abstract xsetByName(Lorg/apache/xmlbeans/XmlString;)V
.end method

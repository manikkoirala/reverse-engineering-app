.class public interface abstract Lorg/etsi/uri/x01903/v13/CRLRefsType;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/xmlbeans/XmlObject;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/etsi/uri/x01903/v13/CRLRefsType$Factory;
    }
.end annotation


# static fields
.field public static final type:Lorg/apache/xmlbeans/SchemaType;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-class v0, Lorg/etsi/uri/x01903/v13/CRLRefsType;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    const-string v1, "schemaorg_apache_xmlbeans.system.s8C3F193EE11A2F798ACF65489B9E6078"

    invoke-static {v0, v1}, Lorg/apache/xmlbeans/XmlBeans;->typeSystemForClassLoader(Ljava/lang/ClassLoader;Ljava/lang/String;)Lorg/apache/xmlbeans/SchemaTypeSystem;

    move-result-object v0

    const-string v1, "crlrefstype2a59type"

    invoke-interface {v0, v1}, Lorg/apache/xmlbeans/SchemaTypeSystem;->resolveHandle(Ljava/lang/String;)Lorg/apache/xmlbeans/SchemaComponent;

    move-result-object v0

    check-cast v0, Lorg/apache/xmlbeans/SchemaType;

    sput-object v0, Lorg/etsi/uri/x01903/v13/CRLRefsType;->type:Lorg/apache/xmlbeans/SchemaType;

    return-void
.end method


# virtual methods
.method public abstract addNewCRLRef()Lorg/etsi/uri/x01903/v13/CRLRefType;
.end method

.method public abstract getCRLRefArray(I)Lorg/etsi/uri/x01903/v13/CRLRefType;
.end method

.method public abstract getCRLRefArray()[Lorg/etsi/uri/x01903/v13/CRLRefType;
.end method

.method public abstract getCRLRefList()Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lorg/etsi/uri/x01903/v13/CRLRefType;",
            ">;"
        }
    .end annotation
.end method

.method public abstract insertNewCRLRef(I)Lorg/etsi/uri/x01903/v13/CRLRefType;
.end method

.method public abstract removeCRLRef(I)V
.end method

.method public abstract setCRLRefArray(ILorg/etsi/uri/x01903/v13/CRLRefType;)V
.end method

.method public abstract setCRLRefArray([Lorg/etsi/uri/x01903/v13/CRLRefType;)V
.end method

.method public abstract sizeOfCRLRefArray()I
.end method

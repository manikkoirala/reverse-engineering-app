.class public interface abstract Lorg/etsi/uri/x01903/v13/CRLRefType;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/xmlbeans/XmlObject;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/etsi/uri/x01903/v13/CRLRefType$Factory;
    }
.end annotation


# static fields
.field public static final type:Lorg/apache/xmlbeans/SchemaType;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-class v0, Lorg/etsi/uri/x01903/v13/CRLRefType;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    const-string v1, "schemaorg_apache_xmlbeans.system.s8C3F193EE11A2F798ACF65489B9E6078"

    invoke-static {v0, v1}, Lorg/apache/xmlbeans/XmlBeans;->typeSystemForClassLoader(Ljava/lang/ClassLoader;Ljava/lang/String;)Lorg/apache/xmlbeans/SchemaTypeSystem;

    move-result-object v0

    const-string v1, "crlreftype4444type"

    invoke-interface {v0, v1}, Lorg/apache/xmlbeans/SchemaTypeSystem;->resolveHandle(Ljava/lang/String;)Lorg/apache/xmlbeans/SchemaComponent;

    move-result-object v0

    check-cast v0, Lorg/apache/xmlbeans/SchemaType;

    sput-object v0, Lorg/etsi/uri/x01903/v13/CRLRefType;->type:Lorg/apache/xmlbeans/SchemaType;

    return-void
.end method


# virtual methods
.method public abstract addNewCRLIdentifier()Lorg/etsi/uri/x01903/v13/CRLIdentifierType;
.end method

.method public abstract addNewDigestAlgAndValue()Lorg/etsi/uri/x01903/v13/DigestAlgAndValueType;
.end method

.method public abstract getCRLIdentifier()Lorg/etsi/uri/x01903/v13/CRLIdentifierType;
.end method

.method public abstract getDigestAlgAndValue()Lorg/etsi/uri/x01903/v13/DigestAlgAndValueType;
.end method

.method public abstract isSetCRLIdentifier()Z
.end method

.method public abstract setCRLIdentifier(Lorg/etsi/uri/x01903/v13/CRLIdentifierType;)V
.end method

.method public abstract setDigestAlgAndValue(Lorg/etsi/uri/x01903/v13/DigestAlgAndValueType;)V
.end method

.method public abstract unsetCRLIdentifier()V
.end method

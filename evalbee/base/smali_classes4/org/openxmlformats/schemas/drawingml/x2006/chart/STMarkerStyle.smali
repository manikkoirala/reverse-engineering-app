.class public interface abstract Lorg/openxmlformats/schemas/drawingml/x2006/chart/STMarkerStyle;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/xmlbeans/XmlString;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/openxmlformats/schemas/drawingml/x2006/chart/STMarkerStyle$Factory;,
        Lorg/openxmlformats/schemas/drawingml/x2006/chart/STMarkerStyle$Enum;
    }
.end annotation


# static fields
.field public static final CIRCLE:Lorg/openxmlformats/schemas/drawingml/x2006/chart/STMarkerStyle$Enum;

.field public static final DASH:Lorg/openxmlformats/schemas/drawingml/x2006/chart/STMarkerStyle$Enum;

.field public static final DIAMOND:Lorg/openxmlformats/schemas/drawingml/x2006/chart/STMarkerStyle$Enum;

.field public static final DOT:Lorg/openxmlformats/schemas/drawingml/x2006/chart/STMarkerStyle$Enum;

.field public static final INT_CIRCLE:I = 0x1

.field public static final INT_DASH:I = 0x2

.field public static final INT_DIAMOND:I = 0x3

.field public static final INT_DOT:I = 0x4

.field public static final INT_NONE:I = 0x5

.field public static final INT_PICTURE:I = 0x6

.field public static final INT_PLUS:I = 0x7

.field public static final INT_SQUARE:I = 0x8

.field public static final INT_STAR:I = 0x9

.field public static final INT_TRIANGLE:I = 0xa

.field public static final INT_X:I = 0xb

.field public static final NONE:Lorg/openxmlformats/schemas/drawingml/x2006/chart/STMarkerStyle$Enum;

.field public static final PICTURE:Lorg/openxmlformats/schemas/drawingml/x2006/chart/STMarkerStyle$Enum;

.field public static final PLUS:Lorg/openxmlformats/schemas/drawingml/x2006/chart/STMarkerStyle$Enum;

.field public static final SQUARE:Lorg/openxmlformats/schemas/drawingml/x2006/chart/STMarkerStyle$Enum;

.field public static final STAR:Lorg/openxmlformats/schemas/drawingml/x2006/chart/STMarkerStyle$Enum;

.field public static final TRIANGLE:Lorg/openxmlformats/schemas/drawingml/x2006/chart/STMarkerStyle$Enum;

.field public static final X:Lorg/openxmlformats/schemas/drawingml/x2006/chart/STMarkerStyle$Enum;

.field public static final type:Lorg/apache/xmlbeans/SchemaType;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-class v0, Lorg/openxmlformats/schemas/drawingml/x2006/chart/STMarkerStyle;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    const-string v1, "schemaorg_apache_xmlbeans.system.sF1327CCA741569E70F9CA8C9AF9B44B2"

    invoke-static {v0, v1}, Lorg/apache/xmlbeans/XmlBeans;->typeSystemForClassLoader(Ljava/lang/ClassLoader;Ljava/lang/String;)Lorg/apache/xmlbeans/SchemaTypeSystem;

    move-result-object v0

    const-string v1, "stmarkerstyle177ftype"

    invoke-interface {v0, v1}, Lorg/apache/xmlbeans/SchemaTypeSystem;->resolveHandle(Ljava/lang/String;)Lorg/apache/xmlbeans/SchemaComponent;

    move-result-object v0

    check-cast v0, Lorg/apache/xmlbeans/SchemaType;

    sput-object v0, Lorg/openxmlformats/schemas/drawingml/x2006/chart/STMarkerStyle;->type:Lorg/apache/xmlbeans/SchemaType;

    const-string v0, "circle"

    invoke-static {v0}, Lorg/openxmlformats/schemas/drawingml/x2006/chart/STMarkerStyle$Enum;->forString(Ljava/lang/String;)Lorg/openxmlformats/schemas/drawingml/x2006/chart/STMarkerStyle$Enum;

    move-result-object v0

    sput-object v0, Lorg/openxmlformats/schemas/drawingml/x2006/chart/STMarkerStyle;->CIRCLE:Lorg/openxmlformats/schemas/drawingml/x2006/chart/STMarkerStyle$Enum;

    const-string v0, "dash"

    invoke-static {v0}, Lorg/openxmlformats/schemas/drawingml/x2006/chart/STMarkerStyle$Enum;->forString(Ljava/lang/String;)Lorg/openxmlformats/schemas/drawingml/x2006/chart/STMarkerStyle$Enum;

    move-result-object v0

    sput-object v0, Lorg/openxmlformats/schemas/drawingml/x2006/chart/STMarkerStyle;->DASH:Lorg/openxmlformats/schemas/drawingml/x2006/chart/STMarkerStyle$Enum;

    const-string v0, "diamond"

    invoke-static {v0}, Lorg/openxmlformats/schemas/drawingml/x2006/chart/STMarkerStyle$Enum;->forString(Ljava/lang/String;)Lorg/openxmlformats/schemas/drawingml/x2006/chart/STMarkerStyle$Enum;

    move-result-object v0

    sput-object v0, Lorg/openxmlformats/schemas/drawingml/x2006/chart/STMarkerStyle;->DIAMOND:Lorg/openxmlformats/schemas/drawingml/x2006/chart/STMarkerStyle$Enum;

    const-string v0, "dot"

    invoke-static {v0}, Lorg/openxmlformats/schemas/drawingml/x2006/chart/STMarkerStyle$Enum;->forString(Ljava/lang/String;)Lorg/openxmlformats/schemas/drawingml/x2006/chart/STMarkerStyle$Enum;

    move-result-object v0

    sput-object v0, Lorg/openxmlformats/schemas/drawingml/x2006/chart/STMarkerStyle;->DOT:Lorg/openxmlformats/schemas/drawingml/x2006/chart/STMarkerStyle$Enum;

    const-string v0, "none"

    invoke-static {v0}, Lorg/openxmlformats/schemas/drawingml/x2006/chart/STMarkerStyle$Enum;->forString(Ljava/lang/String;)Lorg/openxmlformats/schemas/drawingml/x2006/chart/STMarkerStyle$Enum;

    move-result-object v0

    sput-object v0, Lorg/openxmlformats/schemas/drawingml/x2006/chart/STMarkerStyle;->NONE:Lorg/openxmlformats/schemas/drawingml/x2006/chart/STMarkerStyle$Enum;

    const-string v0, "picture"

    invoke-static {v0}, Lorg/openxmlformats/schemas/drawingml/x2006/chart/STMarkerStyle$Enum;->forString(Ljava/lang/String;)Lorg/openxmlformats/schemas/drawingml/x2006/chart/STMarkerStyle$Enum;

    move-result-object v0

    sput-object v0, Lorg/openxmlformats/schemas/drawingml/x2006/chart/STMarkerStyle;->PICTURE:Lorg/openxmlformats/schemas/drawingml/x2006/chart/STMarkerStyle$Enum;

    const-string v0, "plus"

    invoke-static {v0}, Lorg/openxmlformats/schemas/drawingml/x2006/chart/STMarkerStyle$Enum;->forString(Ljava/lang/String;)Lorg/openxmlformats/schemas/drawingml/x2006/chart/STMarkerStyle$Enum;

    move-result-object v0

    sput-object v0, Lorg/openxmlformats/schemas/drawingml/x2006/chart/STMarkerStyle;->PLUS:Lorg/openxmlformats/schemas/drawingml/x2006/chart/STMarkerStyle$Enum;

    const-string v0, "square"

    invoke-static {v0}, Lorg/openxmlformats/schemas/drawingml/x2006/chart/STMarkerStyle$Enum;->forString(Ljava/lang/String;)Lorg/openxmlformats/schemas/drawingml/x2006/chart/STMarkerStyle$Enum;

    move-result-object v0

    sput-object v0, Lorg/openxmlformats/schemas/drawingml/x2006/chart/STMarkerStyle;->SQUARE:Lorg/openxmlformats/schemas/drawingml/x2006/chart/STMarkerStyle$Enum;

    const-string v0, "star"

    invoke-static {v0}, Lorg/openxmlformats/schemas/drawingml/x2006/chart/STMarkerStyle$Enum;->forString(Ljava/lang/String;)Lorg/openxmlformats/schemas/drawingml/x2006/chart/STMarkerStyle$Enum;

    move-result-object v0

    sput-object v0, Lorg/openxmlformats/schemas/drawingml/x2006/chart/STMarkerStyle;->STAR:Lorg/openxmlformats/schemas/drawingml/x2006/chart/STMarkerStyle$Enum;

    const-string v0, "triangle"

    invoke-static {v0}, Lorg/openxmlformats/schemas/drawingml/x2006/chart/STMarkerStyle$Enum;->forString(Ljava/lang/String;)Lorg/openxmlformats/schemas/drawingml/x2006/chart/STMarkerStyle$Enum;

    move-result-object v0

    sput-object v0, Lorg/openxmlformats/schemas/drawingml/x2006/chart/STMarkerStyle;->TRIANGLE:Lorg/openxmlformats/schemas/drawingml/x2006/chart/STMarkerStyle$Enum;

    const-string v0, "x"

    invoke-static {v0}, Lorg/openxmlformats/schemas/drawingml/x2006/chart/STMarkerStyle$Enum;->forString(Ljava/lang/String;)Lorg/openxmlformats/schemas/drawingml/x2006/chart/STMarkerStyle$Enum;

    move-result-object v0

    sput-object v0, Lorg/openxmlformats/schemas/drawingml/x2006/chart/STMarkerStyle;->X:Lorg/openxmlformats/schemas/drawingml/x2006/chart/STMarkerStyle$Enum;

    return-void
.end method


# virtual methods
.method public abstract enumValue()Lorg/apache/xmlbeans/StringEnumAbstractBase;
.end method

.method public abstract set(Lorg/apache/xmlbeans/StringEnumAbstractBase;)V
.end method

.class public final Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;
.super Lorg/apache/xmlbeans/StringEnumAbstractBase;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Enum"
.end annotation


# static fields
.field static final INT_ACCENT_BORDER_CALLOUT_1:I = 0x72

.field static final INT_ACCENT_BORDER_CALLOUT_2:I = 0x73

.field static final INT_ACCENT_BORDER_CALLOUT_3:I = 0x74

.field static final INT_ACCENT_CALLOUT_1:I = 0x6c

.field static final INT_ACCENT_CALLOUT_2:I = 0x6d

.field static final INT_ACCENT_CALLOUT_3:I = 0x6e

.field static final INT_ACTION_BUTTON_BACK_PREVIOUS:I = 0xa6

.field static final INT_ACTION_BUTTON_BEGINNING:I = 0xa8

.field static final INT_ACTION_BUTTON_BLANK:I = 0xa1

.field static final INT_ACTION_BUTTON_DOCUMENT:I = 0xaa

.field static final INT_ACTION_BUTTON_END:I = 0xa7

.field static final INT_ACTION_BUTTON_FORWARD_NEXT:I = 0xa5

.field static final INT_ACTION_BUTTON_HELP:I = 0xa3

.field static final INT_ACTION_BUTTON_HOME:I = 0xa2

.field static final INT_ACTION_BUTTON_INFORMATION:I = 0xa4

.field static final INT_ACTION_BUTTON_MOVIE:I = 0xac

.field static final INT_ACTION_BUTTON_RETURN:I = 0xa9

.field static final INT_ACTION_BUTTON_SOUND:I = 0xab

.field static final INT_ARC:I = 0x59

.field static final INT_BENT_ARROW:I = 0x3f

.field static final INT_BENT_CONNECTOR_2:I = 0x61

.field static final INT_BENT_CONNECTOR_3:I = 0x62

.field static final INT_BENT_CONNECTOR_4:I = 0x63

.field static final INT_BENT_CONNECTOR_5:I = 0x64

.field static final INT_BENT_UP_ARROW:I = 0x32

.field static final INT_BEVEL:I = 0x53

.field static final INT_BLOCK_ARC:I = 0x29

.field static final INT_BORDER_CALLOUT_1:I = 0x6f

.field static final INT_BORDER_CALLOUT_2:I = 0x70

.field static final INT_BORDER_CALLOUT_3:I = 0x71

.field static final INT_BRACE_PAIR:I = 0x5f

.field static final INT_BRACKET_PAIR:I = 0x5e

.field static final INT_CALLOUT_1:I = 0x69

.field static final INT_CALLOUT_2:I = 0x6a

.field static final INT_CALLOUT_3:I = 0x6b

.field static final INT_CAN:I = 0x4a

.field static final INT_CHART_PLUS:I = 0xbb

.field static final INT_CHART_STAR:I = 0xba

.field static final INT_CHART_X:I = 0xb9

.field static final INT_CHEVRON:I = 0x26

.field static final INT_CHORD:I = 0x58

.field static final INT_CIRCULAR_ARROW:I = 0x41

.field static final INT_CLOUD:I = 0x79

.field static final INT_CLOUD_CALLOUT:I = 0x78

.field static final INT_CORNER:I = 0x56

.field static final INT_CORNER_TABS:I = 0xb6

.field static final INT_CUBE:I = 0x49

.field static final INT_CURVED_CONNECTOR_2:I = 0x65

.field static final INT_CURVED_CONNECTOR_3:I = 0x66

.field static final INT_CURVED_CONNECTOR_4:I = 0x67

.field static final INT_CURVED_CONNECTOR_5:I = 0x68

.field static final INT_CURVED_DOWN_ARROW:I = 0x47

.field static final INT_CURVED_LEFT_ARROW:I = 0x45

.field static final INT_CURVED_RIGHT_ARROW:I = 0x44

.field static final INT_CURVED_UP_ARROW:I = 0x46

.field static final INT_DECAGON:I = 0xe

.field static final INT_DIAG_STRIPE:I = 0x57

.field static final INT_DIAMOND:I = 0x6

.field static final INT_DODECAGON:I = 0xf

.field static final INT_DONUT:I = 0x2a

.field static final INT_DOUBLE_WAVE:I = 0x82

.field static final INT_DOWN_ARROW:I = 0x2f

.field static final INT_DOWN_ARROW_CALLOUT:I = 0x3b

.field static final INT_ELLIPSE:I = 0x23

.field static final INT_ELLIPSE_RIBBON:I = 0x7c

.field static final INT_ELLIPSE_RIBBON_2:I = 0x7d

.field static final INT_FLOW_CHART_ALTERNATE_PROCESS:I = 0x9f

.field static final INT_FLOW_CHART_COLLATE:I = 0x94

.field static final INT_FLOW_CHART_CONNECTOR:I = 0x8f

.field static final INT_FLOW_CHART_DECISION:I = 0x85

.field static final INT_FLOW_CHART_DELAY:I = 0x9e

.field static final INT_FLOW_CHART_DISPLAY:I = 0x9d

.field static final INT_FLOW_CHART_DOCUMENT:I = 0x89

.field static final INT_FLOW_CHART_EXTRACT:I = 0x96

.field static final INT_FLOW_CHART_INPUT_OUTPUT:I = 0x86

.field static final INT_FLOW_CHART_INTERNAL_STORAGE:I = 0x88

.field static final INT_FLOW_CHART_MAGNETIC_DISK:I = 0x9b

.field static final INT_FLOW_CHART_MAGNETIC_DRUM:I = 0x9c

.field static final INT_FLOW_CHART_MAGNETIC_TAPE:I = 0x9a

.field static final INT_FLOW_CHART_MANUAL_INPUT:I = 0x8d

.field static final INT_FLOW_CHART_MANUAL_OPERATION:I = 0x8e

.field static final INT_FLOW_CHART_MERGE:I = 0x97

.field static final INT_FLOW_CHART_MULTIDOCUMENT:I = 0x8a

.field static final INT_FLOW_CHART_OFFLINE_STORAGE:I = 0x98

.field static final INT_FLOW_CHART_OFFPAGE_CONNECTOR:I = 0xa0

.field static final INT_FLOW_CHART_ONLINE_STORAGE:I = 0x99

.field static final INT_FLOW_CHART_OR:I = 0x93

.field static final INT_FLOW_CHART_PREDEFINED_PROCESS:I = 0x87

.field static final INT_FLOW_CHART_PREPARATION:I = 0x8c

.field static final INT_FLOW_CHART_PROCESS:I = 0x84

.field static final INT_FLOW_CHART_PUNCHED_CARD:I = 0x90

.field static final INT_FLOW_CHART_PUNCHED_TAPE:I = 0x91

.field static final INT_FLOW_CHART_SORT:I = 0x95

.field static final INT_FLOW_CHART_SUMMING_JUNCTION:I = 0x92

.field static final INT_FLOW_CHART_TERMINATOR:I = 0x8b

.field static final INT_FOLDED_CORNER:I = 0x52

.field static final INT_FRAME:I = 0x54

.field static final INT_FUNNEL:I = 0xaf

.field static final INT_GEAR_6:I = 0xad

.field static final INT_GEAR_9:I = 0xae

.field static final INT_HALF_FRAME:I = 0x55

.field static final INT_HEART:I = 0x4c

.field static final INT_HEPTAGON:I = 0xc

.field static final INT_HEXAGON:I = 0xb

.field static final INT_HOME_PLATE:I = 0x25

.field static final INT_HORIZONTAL_SCROLL:I = 0x80

.field static final INT_IRREGULAR_SEAL_1:I = 0x50

.field static final INT_IRREGULAR_SEAL_2:I = 0x51

.field static final INT_LEFT_ARROW:I = 0x2d

.field static final INT_LEFT_ARROW_CALLOUT:I = 0x38

.field static final INT_LEFT_BRACE:I = 0x5c

.field static final INT_LEFT_BRACKET:I = 0x5a

.field static final INT_LEFT_CIRCULAR_ARROW:I = 0x42

.field static final INT_LEFT_RIGHT_ARROW:I = 0x33

.field static final INT_LEFT_RIGHT_ARROW_CALLOUT:I = 0x3c

.field static final INT_LEFT_RIGHT_CIRCULAR_ARROW:I = 0x43

.field static final INT_LEFT_RIGHT_RIBBON:I = 0x7e

.field static final INT_LEFT_RIGHT_UP_ARROW:I = 0x36

.field static final INT_LEFT_UP_ARROW:I = 0x35

.field static final INT_LIGHTNING_BOLT:I = 0x4b

.field static final INT_LINE:I = 0x1

.field static final INT_LINE_INV:I = 0x2

.field static final INT_MATH_DIVIDE:I = 0xb3

.field static final INT_MATH_EQUAL:I = 0xb4

.field static final INT_MATH_MINUS:I = 0xb1

.field static final INT_MATH_MULTIPLY:I = 0xb2

.field static final INT_MATH_NOT_EQUAL:I = 0xb5

.field static final INT_MATH_PLUS:I = 0xb0

.field static final INT_MOON:I = 0x4e

.field static final INT_NON_ISOSCELES_TRAPEZOID:I = 0x9

.field static final INT_NOTCHED_RIGHT_ARROW:I = 0x31

.field static final INT_NO_SMOKING:I = 0x2b

.field static final INT_OCTAGON:I = 0xd

.field static final INT_PARALLELOGRAM:I = 0x7

.field static final INT_PENTAGON:I = 0xa

.field static final INT_PIE:I = 0x28

.field static final INT_PIE_WEDGE:I = 0x27

.field static final INT_PLAQUE:I = 0x22

.field static final INT_PLAQUE_TABS:I = 0xb8

.field static final INT_PLUS:I = 0x83

.field static final INT_QUAD_ARROW:I = 0x37

.field static final INT_QUAD_ARROW_CALLOUT:I = 0x3e

.field static final INT_RECT:I = 0x5

.field static final INT_RIBBON:I = 0x7a

.field static final INT_RIBBON_2:I = 0x7b

.field static final INT_RIGHT_ARROW:I = 0x2c

.field static final INT_RIGHT_ARROW_CALLOUT:I = 0x39

.field static final INT_RIGHT_BRACE:I = 0x5d

.field static final INT_RIGHT_BRACKET:I = 0x5b

.field static final INT_ROUND_1_RECT:I = 0x1b

.field static final INT_ROUND_2_DIAG_RECT:I = 0x1d

.field static final INT_ROUND_2_SAME_RECT:I = 0x1c

.field static final INT_ROUND_RECT:I = 0x1a

.field static final INT_RT_TRIANGLE:I = 0x4

.field static final INT_SMILEY_FACE:I = 0x4f

.field static final INT_SNIP_1_RECT:I = 0x1f

.field static final INT_SNIP_2_DIAG_RECT:I = 0x21

.field static final INT_SNIP_2_SAME_RECT:I = 0x20

.field static final INT_SNIP_ROUND_RECT:I = 0x1e

.field static final INT_SQUARE_TABS:I = 0xb7

.field static final INT_STAR_10:I = 0x15

.field static final INT_STAR_12:I = 0x16

.field static final INT_STAR_16:I = 0x17

.field static final INT_STAR_24:I = 0x18

.field static final INT_STAR_32:I = 0x19

.field static final INT_STAR_4:I = 0x10

.field static final INT_STAR_5:I = 0x11

.field static final INT_STAR_6:I = 0x12

.field static final INT_STAR_7:I = 0x13

.field static final INT_STAR_8:I = 0x14

.field static final INT_STRAIGHT_CONNECTOR_1:I = 0x60

.field static final INT_STRIPED_RIGHT_ARROW:I = 0x30

.field static final INT_SUN:I = 0x4d

.field static final INT_SWOOSH_ARROW:I = 0x48

.field static final INT_TEARDROP:I = 0x24

.field static final INT_TRAPEZOID:I = 0x8

.field static final INT_TRIANGLE:I = 0x3

.field static final INT_UP_ARROW:I = 0x2e

.field static final INT_UP_ARROW_CALLOUT:I = 0x3a

.field static final INT_UP_DOWN_ARROW:I = 0x34

.field static final INT_UP_DOWN_ARROW_CALLOUT:I = 0x3d

.field static final INT_UTURN_ARROW:I = 0x40

.field static final INT_VERTICAL_SCROLL:I = 0x7f

.field static final INT_WAVE:I = 0x81

.field static final INT_WEDGE_ELLIPSE_CALLOUT:I = 0x77

.field static final INT_WEDGE_RECT_CALLOUT:I = 0x75

.field static final INT_WEDGE_ROUND_RECT_CALLOUT:I = 0x76

.field private static final serialVersionUID:J = 0x1L

.field public static final table:Lorg/apache/xmlbeans/StringEnumAbstractBase$Table;


# direct methods
.method public static constructor <clinit>()V
    .locals 191

    new-instance v0, Lorg/apache/xmlbeans/StringEnumAbstractBase$Table;

    new-instance v2, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object v1, v2

    const-string v3, "line"

    const/4 v4, 0x1

    invoke-direct {v2, v3, v4}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v3, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object v2, v3

    const-string v4, "lineInv"

    const/4 v5, 0x2

    invoke-direct {v3, v4, v5}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v4, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object v3, v4

    const-string v5, "triangle"

    const/4 v6, 0x3

    invoke-direct {v4, v5, v6}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v5, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object v4, v5

    const-string v6, "rtTriangle"

    const/4 v7, 0x4

    invoke-direct {v5, v6, v7}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v6, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object v5, v6

    const-string v7, "rect"

    const/4 v8, 0x5

    invoke-direct {v6, v7, v8}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v7, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object v6, v7

    const-string v8, "diamond"

    const/4 v9, 0x6

    invoke-direct {v7, v8, v9}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v8, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object v7, v8

    const-string v9, "parallelogram"

    const/4 v10, 0x7

    invoke-direct {v8, v9, v10}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v9, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object v8, v9

    const-string v10, "trapezoid"

    const/16 v11, 0x8

    invoke-direct {v9, v10, v11}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v10, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object v9, v10

    const-string v11, "nonIsoscelesTrapezoid"

    const/16 v12, 0x9

    invoke-direct {v10, v11, v12}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v11, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object v10, v11

    const-string v12, "pentagon"

    const/16 v13, 0xa

    invoke-direct {v11, v12, v13}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v12, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object v11, v12

    const-string v13, "hexagon"

    const/16 v14, 0xb

    invoke-direct {v12, v13, v14}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v13, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object v12, v13

    const-string v14, "heptagon"

    const/16 v15, 0xc

    invoke-direct {v13, v14, v15}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v14, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object v13, v14

    const-string v15, "octagon"

    move-object/from16 v188, v0

    const/16 v0, 0xd

    invoke-direct {v14, v15, v0}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object v14, v0

    const-string v15, "decagon"

    move-object/from16 v189, v1

    const/16 v1, 0xe

    invoke-direct {v0, v15, v1}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object v15, v0

    const-string v1, "dodecagon"

    move-object/from16 v190, v2

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v16, v0

    const-string v1, "star4"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v17, v0

    const-string v1, "star5"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v18, v0

    const-string v1, "star6"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v19, v0

    const-string v1, "star7"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v20, v0

    const-string v1, "star8"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v21, v0

    const-string v1, "star10"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v22, v0

    const-string v1, "star12"

    const/16 v2, 0x16

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v23, v0

    const-string v1, "star16"

    const/16 v2, 0x17

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v24, v0

    const-string v1, "star24"

    const/16 v2, 0x18

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v25, v0

    const-string v1, "star32"

    const/16 v2, 0x19

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v26, v0

    const-string v1, "roundRect"

    const/16 v2, 0x1a

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v27, v0

    const-string v1, "round1Rect"

    const/16 v2, 0x1b

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v28, v0

    const-string v1, "round2SameRect"

    const/16 v2, 0x1c

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v29, v0

    const-string v1, "round2DiagRect"

    const/16 v2, 0x1d

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v30, v0

    const-string v1, "snipRoundRect"

    const/16 v2, 0x1e

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v31, v0

    const-string v1, "snip1Rect"

    const/16 v2, 0x1f

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v32, v0

    const-string v1, "snip2SameRect"

    const/16 v2, 0x20

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v33, v0

    const-string v1, "snip2DiagRect"

    const/16 v2, 0x21

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v34, v0

    const-string v1, "plaque"

    const/16 v2, 0x22

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v35, v0

    const-string v1, "ellipse"

    const/16 v2, 0x23

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v36, v0

    const-string v1, "teardrop"

    const/16 v2, 0x24

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v37, v0

    const-string v1, "homePlate"

    const/16 v2, 0x25

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v38, v0

    const-string v1, "chevron"

    const/16 v2, 0x26

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v39, v0

    const-string v1, "pieWedge"

    const/16 v2, 0x27

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v40, v0

    const-string v1, "pie"

    const/16 v2, 0x28

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v41, v0

    const-string v1, "blockArc"

    const/16 v2, 0x29

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v42, v0

    const-string v1, "donut"

    const/16 v2, 0x2a

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v43, v0

    const-string v1, "noSmoking"

    const/16 v2, 0x2b

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v44, v0

    const-string v1, "rightArrow"

    const/16 v2, 0x2c

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v45, v0

    const-string v1, "leftArrow"

    const/16 v2, 0x2d

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v46, v0

    const-string v1, "upArrow"

    const/16 v2, 0x2e

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v47, v0

    const-string v1, "downArrow"

    const/16 v2, 0x2f

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v48, v0

    const-string v1, "stripedRightArrow"

    const/16 v2, 0x30

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v49, v0

    const-string v1, "notchedRightArrow"

    const/16 v2, 0x31

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v50, v0

    const-string v1, "bentUpArrow"

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v51, v0

    const-string v1, "leftRightArrow"

    const/16 v2, 0x33

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v52, v0

    const-string v1, "upDownArrow"

    const/16 v2, 0x34

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v53, v0

    const-string v1, "leftUpArrow"

    const/16 v2, 0x35

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v54, v0

    const-string v1, "leftRightUpArrow"

    const/16 v2, 0x36

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v55, v0

    const-string v1, "quadArrow"

    const/16 v2, 0x37

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v56, v0

    const-string v1, "leftArrowCallout"

    const/16 v2, 0x38

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v57, v0

    const-string v1, "rightArrowCallout"

    const/16 v2, 0x39

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v58, v0

    const-string v1, "upArrowCallout"

    const/16 v2, 0x3a

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v59, v0

    const-string v1, "downArrowCallout"

    const/16 v2, 0x3b

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v60, v0

    const-string v1, "leftRightArrowCallout"

    const/16 v2, 0x3c

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v61, v0

    const-string v1, "upDownArrowCallout"

    const/16 v2, 0x3d

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v62, v0

    const-string v1, "quadArrowCallout"

    const/16 v2, 0x3e

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v63, v0

    const-string v1, "bentArrow"

    const/16 v2, 0x3f

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v64, v0

    const-string v1, "uturnArrow"

    const/16 v2, 0x40

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v65, v0

    const-string v1, "circularArrow"

    const/16 v2, 0x41

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v66, v0

    const-string v1, "leftCircularArrow"

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v67, v0

    const-string v1, "leftRightCircularArrow"

    const/16 v2, 0x43

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v68, v0

    const-string v1, "curvedRightArrow"

    const/16 v2, 0x44

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v69, v0

    const-string v1, "curvedLeftArrow"

    const/16 v2, 0x45

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v70, v0

    const-string v1, "curvedUpArrow"

    const/16 v2, 0x46

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v71, v0

    const-string v1, "curvedDownArrow"

    const/16 v2, 0x47

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v72, v0

    const-string v1, "swooshArrow"

    const/16 v2, 0x48

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v73, v0

    const-string v1, "cube"

    const/16 v2, 0x49

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v74, v0

    const-string v1, "can"

    const/16 v2, 0x4a

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v75, v0

    const-string v1, "lightningBolt"

    const/16 v2, 0x4b

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v76, v0

    const-string v1, "heart"

    const/16 v2, 0x4c

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v77, v0

    const-string v1, "sun"

    const/16 v2, 0x4d

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v78, v0

    const-string v1, "moon"

    const/16 v2, 0x4e

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v79, v0

    const-string v1, "smileyFace"

    const/16 v2, 0x4f

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v80, v0

    const-string v1, "irregularSeal1"

    const/16 v2, 0x50

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v81, v0

    const-string v1, "irregularSeal2"

    const/16 v2, 0x51

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v82, v0

    const-string v1, "foldedCorner"

    const/16 v2, 0x52

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v83, v0

    const-string v1, "bevel"

    const/16 v2, 0x53

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v84, v0

    const-string v1, "frame"

    const/16 v2, 0x54

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v85, v0

    const-string v1, "halfFrame"

    const/16 v2, 0x55

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v86, v0

    const-string v1, "corner"

    const/16 v2, 0x56

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v87, v0

    const-string v1, "diagStripe"

    const/16 v2, 0x57

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v88, v0

    const-string v1, "chord"

    const/16 v2, 0x58

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v89, v0

    const-string v1, "arc"

    const/16 v2, 0x59

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v90, v0

    const-string v1, "leftBracket"

    const/16 v2, 0x5a

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v91, v0

    const-string v1, "rightBracket"

    const/16 v2, 0x5b

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v92, v0

    const-string v1, "leftBrace"

    const/16 v2, 0x5c

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v93, v0

    const-string v1, "rightBrace"

    const/16 v2, 0x5d

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v94, v0

    const-string v1, "bracketPair"

    const/16 v2, 0x5e

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v95, v0

    const-string v1, "bracePair"

    const/16 v2, 0x5f

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v96, v0

    const-string v1, "straightConnector1"

    const/16 v2, 0x60

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v97, v0

    const-string v1, "bentConnector2"

    const/16 v2, 0x61

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v98, v0

    const-string v1, "bentConnector3"

    const/16 v2, 0x62

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v99, v0

    const-string v1, "bentConnector4"

    const/16 v2, 0x63

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v100, v0

    const-string v1, "bentConnector5"

    const/16 v2, 0x64

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v101, v0

    const-string v1, "curvedConnector2"

    const/16 v2, 0x65

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v102, v0

    const-string v1, "curvedConnector3"

    const/16 v2, 0x66

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v103, v0

    const-string v1, "curvedConnector4"

    const/16 v2, 0x67

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v104, v0

    const-string v1, "curvedConnector5"

    const/16 v2, 0x68

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v105, v0

    const-string v1, "callout1"

    const/16 v2, 0x69

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v106, v0

    const-string v1, "callout2"

    const/16 v2, 0x6a

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v107, v0

    const-string v1, "callout3"

    const/16 v2, 0x6b

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v108, v0

    const-string v1, "accentCallout1"

    const/16 v2, 0x6c

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v109, v0

    const-string v1, "accentCallout2"

    const/16 v2, 0x6d

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v110, v0

    const-string v1, "accentCallout3"

    const/16 v2, 0x6e

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v111, v0

    const-string v1, "borderCallout1"

    const/16 v2, 0x6f

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v112, v0

    const-string v1, "borderCallout2"

    const/16 v2, 0x70

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v113, v0

    const-string v1, "borderCallout3"

    const/16 v2, 0x71

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v114, v0

    const-string v1, "accentBorderCallout1"

    const/16 v2, 0x72

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v115, v0

    const-string v1, "accentBorderCallout2"

    const/16 v2, 0x73

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v116, v0

    const-string v1, "accentBorderCallout3"

    const/16 v2, 0x74

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v117, v0

    const-string v1, "wedgeRectCallout"

    const/16 v2, 0x75

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v118, v0

    const-string v1, "wedgeRoundRectCallout"

    const/16 v2, 0x76

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v119, v0

    const-string v1, "wedgeEllipseCallout"

    const/16 v2, 0x77

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v120, v0

    const-string v1, "cloudCallout"

    const/16 v2, 0x78

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v121, v0

    const-string v1, "cloud"

    const/16 v2, 0x79

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v122, v0

    const-string v1, "ribbon"

    const/16 v2, 0x7a

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v123, v0

    const-string v1, "ribbon2"

    const/16 v2, 0x7b

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v124, v0

    const-string v1, "ellipseRibbon"

    const/16 v2, 0x7c

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v125, v0

    const-string v1, "ellipseRibbon2"

    const/16 v2, 0x7d

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v126, v0

    const-string v1, "leftRightRibbon"

    const/16 v2, 0x7e

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v127, v0

    const-string v1, "verticalScroll"

    const/16 v2, 0x7f

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v128, v0

    const-string v1, "horizontalScroll"

    const/16 v2, 0x80

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v129, v0

    const-string v1, "wave"

    const/16 v2, 0x81

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v130, v0

    const-string v1, "doubleWave"

    const/16 v2, 0x82

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v131, v0

    const-string v1, "plus"

    const/16 v2, 0x83

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v132, v0

    const-string v1, "flowChartProcess"

    const/16 v2, 0x84

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v133, v0

    const-string v1, "flowChartDecision"

    const/16 v2, 0x85

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v134, v0

    const-string v1, "flowChartInputOutput"

    const/16 v2, 0x86

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v135, v0

    const-string v1, "flowChartPredefinedProcess"

    const/16 v2, 0x87

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v136, v0

    const-string v1, "flowChartInternalStorage"

    const/16 v2, 0x88

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v137, v0

    const-string v1, "flowChartDocument"

    const/16 v2, 0x89

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v138, v0

    const-string v1, "flowChartMultidocument"

    const/16 v2, 0x8a

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v139, v0

    const-string v1, "flowChartTerminator"

    const/16 v2, 0x8b

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v140, v0

    const-string v1, "flowChartPreparation"

    const/16 v2, 0x8c

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v141, v0

    const-string v1, "flowChartManualInput"

    const/16 v2, 0x8d

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v142, v0

    const-string v1, "flowChartManualOperation"

    const/16 v2, 0x8e

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v143, v0

    const-string v1, "flowChartConnector"

    const/16 v2, 0x8f

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v144, v0

    const-string v1, "flowChartPunchedCard"

    const/16 v2, 0x90

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v145, v0

    const-string v1, "flowChartPunchedTape"

    const/16 v2, 0x91

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v146, v0

    const-string v1, "flowChartSummingJunction"

    const/16 v2, 0x92

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v147, v0

    const-string v1, "flowChartOr"

    const/16 v2, 0x93

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v148, v0

    const-string v1, "flowChartCollate"

    const/16 v2, 0x94

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v149, v0

    const-string v1, "flowChartSort"

    const/16 v2, 0x95

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v150, v0

    const-string v1, "flowChartExtract"

    const/16 v2, 0x96

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v151, v0

    const-string v1, "flowChartMerge"

    const/16 v2, 0x97

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v152, v0

    const-string v1, "flowChartOfflineStorage"

    const/16 v2, 0x98

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v153, v0

    const-string v1, "flowChartOnlineStorage"

    const/16 v2, 0x99

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v154, v0

    const-string v1, "flowChartMagneticTape"

    const/16 v2, 0x9a

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v155, v0

    const-string v1, "flowChartMagneticDisk"

    const/16 v2, 0x9b

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v156, v0

    const-string v1, "flowChartMagneticDrum"

    const/16 v2, 0x9c

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v157, v0

    const-string v1, "flowChartDisplay"

    const/16 v2, 0x9d

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v158, v0

    const-string v1, "flowChartDelay"

    const/16 v2, 0x9e

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v159, v0

    const-string v1, "flowChartAlternateProcess"

    const/16 v2, 0x9f

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v160, v0

    const-string v1, "flowChartOffpageConnector"

    const/16 v2, 0xa0

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v161, v0

    const-string v1, "actionButtonBlank"

    const/16 v2, 0xa1

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v162, v0

    const-string v1, "actionButtonHome"

    const/16 v2, 0xa2

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v163, v0

    const-string v1, "actionButtonHelp"

    const/16 v2, 0xa3

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v164, v0

    const-string v1, "actionButtonInformation"

    const/16 v2, 0xa4

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v165, v0

    const-string v1, "actionButtonForwardNext"

    const/16 v2, 0xa5

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v166, v0

    const-string v1, "actionButtonBackPrevious"

    const/16 v2, 0xa6

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v167, v0

    const-string v1, "actionButtonEnd"

    const/16 v2, 0xa7

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v168, v0

    const-string v1, "actionButtonBeginning"

    const/16 v2, 0xa8

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v169, v0

    const-string v1, "actionButtonReturn"

    const/16 v2, 0xa9

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v170, v0

    const-string v1, "actionButtonDocument"

    const/16 v2, 0xaa

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v171, v0

    const-string v1, "actionButtonSound"

    const/16 v2, 0xab

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v172, v0

    const-string v1, "actionButtonMovie"

    const/16 v2, 0xac

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v173, v0

    const-string v1, "gear6"

    const/16 v2, 0xad

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v174, v0

    const-string v1, "gear9"

    const/16 v2, 0xae

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v175, v0

    const-string v1, "funnel"

    const/16 v2, 0xaf

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v176, v0

    const-string v1, "mathPlus"

    const/16 v2, 0xb0

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v177, v0

    const-string v1, "mathMinus"

    const/16 v2, 0xb1

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v178, v0

    const-string v1, "mathMultiply"

    const/16 v2, 0xb2

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v179, v0

    const-string v1, "mathDivide"

    const/16 v2, 0xb3

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v180, v0

    const-string v1, "mathEqual"

    const/16 v2, 0xb4

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v181, v0

    const-string v1, "mathNotEqual"

    const/16 v2, 0xb5

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v182, v0

    const-string v1, "cornerTabs"

    const/16 v2, 0xb6

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v183, v0

    const-string v1, "squareTabs"

    const/16 v2, 0xb7

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v184, v0

    const-string v1, "plaqueTabs"

    const/16 v2, 0xb8

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v185, v0

    const-string v1, "chartX"

    const/16 v2, 0xb9

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v186, v0

    const-string v1, "chartStar"

    const/16 v2, 0xba

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-object/from16 v187, v0

    const-string v1, "chartPlus"

    const/16 v2, 0xbb

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;-><init>(Ljava/lang/String;I)V

    move-object/from16 v1, v189

    move-object/from16 v2, v190

    filled-new-array/range {v1 .. v187}, [Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-result-object v0

    move-object/from16 v1, v188

    invoke-direct {v1, v0}, Lorg/apache/xmlbeans/StringEnumAbstractBase$Table;-><init>([Lorg/apache/xmlbeans/StringEnumAbstractBase;)V

    sput-object v1, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;->table:Lorg/apache/xmlbeans/StringEnumAbstractBase$Table;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lorg/apache/xmlbeans/StringEnumAbstractBase;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static forInt(I)Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;
    .locals 1

    sget-object v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;->table:Lorg/apache/xmlbeans/StringEnumAbstractBase$Table;

    invoke-virtual {v0, p0}, Lorg/apache/xmlbeans/StringEnumAbstractBase$Table;->forInt(I)Lorg/apache/xmlbeans/StringEnumAbstractBase;

    move-result-object p0

    check-cast p0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    return-object p0
.end method

.method public static forString(Ljava/lang/String;)Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;
    .locals 1

    sget-object v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;->table:Lorg/apache/xmlbeans/StringEnumAbstractBase$Table;

    invoke-virtual {v0, p0}, Lorg/apache/xmlbeans/StringEnumAbstractBase$Table;->forString(Ljava/lang/String;)Lorg/apache/xmlbeans/StringEnumAbstractBase;

    move-result-object p0

    check-cast p0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    return-object p0
.end method

.method private readResolve()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lorg/apache/xmlbeans/StringEnumAbstractBase;->intValue()I

    move-result v0

    invoke-static {v0}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;->forInt(I)Lorg/openxmlformats/schemas/drawingml/x2006/main/STShapeType$Enum;

    move-result-object v0

    return-object v0
.end method

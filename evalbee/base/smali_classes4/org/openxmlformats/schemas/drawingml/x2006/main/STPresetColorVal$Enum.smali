.class public final Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;
.super Lorg/apache/xmlbeans/StringEnumAbstractBase;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Enum"
.end annotation


# static fields
.field static final INT_ALICE_BLUE:I = 0x1

.field static final INT_ANTIQUE_WHITE:I = 0x2

.field static final INT_AQUA:I = 0x3

.field static final INT_AQUAMARINE:I = 0x4

.field static final INT_AZURE:I = 0x5

.field static final INT_BEIGE:I = 0x6

.field static final INT_BISQUE:I = 0x7

.field static final INT_BLACK:I = 0x8

.field static final INT_BLANCHED_ALMOND:I = 0x9

.field static final INT_BLUE:I = 0xa

.field static final INT_BLUE_VIOLET:I = 0xb

.field static final INT_BROWN:I = 0xc

.field static final INT_BURLY_WOOD:I = 0xd

.field static final INT_CADET_BLUE:I = 0xe

.field static final INT_CHARTREUSE:I = 0xf

.field static final INT_CHOCOLATE:I = 0x10

.field static final INT_CORAL:I = 0x11

.field static final INT_CORNFLOWER_BLUE:I = 0x12

.field static final INT_CORNSILK:I = 0x13

.field static final INT_CRIMSON:I = 0x14

.field static final INT_CYAN:I = 0x15

.field static final INT_DEEP_PINK:I = 0x27

.field static final INT_DEEP_SKY_BLUE:I = 0x28

.field static final INT_DIM_GRAY:I = 0x29

.field static final INT_DK_BLUE:I = 0x16

.field static final INT_DK_CYAN:I = 0x17

.field static final INT_DK_GOLDENROD:I = 0x18

.field static final INT_DK_GRAY:I = 0x19

.field static final INT_DK_GREEN:I = 0x1a

.field static final INT_DK_KHAKI:I = 0x1b

.field static final INT_DK_MAGENTA:I = 0x1c

.field static final INT_DK_OLIVE_GREEN:I = 0x1d

.field static final INT_DK_ORANGE:I = 0x1e

.field static final INT_DK_ORCHID:I = 0x1f

.field static final INT_DK_RED:I = 0x20

.field static final INT_DK_SALMON:I = 0x21

.field static final INT_DK_SEA_GREEN:I = 0x22

.field static final INT_DK_SLATE_BLUE:I = 0x23

.field static final INT_DK_SLATE_GRAY:I = 0x24

.field static final INT_DK_TURQUOISE:I = 0x25

.field static final INT_DK_VIOLET:I = 0x26

.field static final INT_DODGER_BLUE:I = 0x2a

.field static final INT_FIREBRICK:I = 0x2b

.field static final INT_FLORAL_WHITE:I = 0x2c

.field static final INT_FOREST_GREEN:I = 0x2d

.field static final INT_FUCHSIA:I = 0x2e

.field static final INT_GAINSBORO:I = 0x2f

.field static final INT_GHOST_WHITE:I = 0x30

.field static final INT_GOLD:I = 0x31

.field static final INT_GOLDENROD:I = 0x32

.field static final INT_GRAY:I = 0x33

.field static final INT_GREEN:I = 0x34

.field static final INT_GREEN_YELLOW:I = 0x35

.field static final INT_HONEYDEW:I = 0x36

.field static final INT_HOT_PINK:I = 0x37

.field static final INT_INDIAN_RED:I = 0x38

.field static final INT_INDIGO:I = 0x39

.field static final INT_IVORY:I = 0x3a

.field static final INT_KHAKI:I = 0x3b

.field static final INT_LAVENDER:I = 0x3c

.field static final INT_LAVENDER_BLUSH:I = 0x3d

.field static final INT_LAWN_GREEN:I = 0x3e

.field static final INT_LEMON_CHIFFON:I = 0x3f

.field static final INT_LIME:I = 0x4d

.field static final INT_LIME_GREEN:I = 0x4e

.field static final INT_LINEN:I = 0x4f

.field static final INT_LT_BLUE:I = 0x40

.field static final INT_LT_CORAL:I = 0x41

.field static final INT_LT_CYAN:I = 0x42

.field static final INT_LT_GOLDENROD_YELLOW:I = 0x43

.field static final INT_LT_GRAY:I = 0x44

.field static final INT_LT_GREEN:I = 0x45

.field static final INT_LT_PINK:I = 0x46

.field static final INT_LT_SALMON:I = 0x47

.field static final INT_LT_SEA_GREEN:I = 0x48

.field static final INT_LT_SKY_BLUE:I = 0x49

.field static final INT_LT_SLATE_GRAY:I = 0x4a

.field static final INT_LT_STEEL_BLUE:I = 0x4b

.field static final INT_LT_YELLOW:I = 0x4c

.field static final INT_MAGENTA:I = 0x50

.field static final INT_MAROON:I = 0x51

.field static final INT_MED_AQUAMARINE:I = 0x52

.field static final INT_MED_BLUE:I = 0x53

.field static final INT_MED_ORCHID:I = 0x54

.field static final INT_MED_PURPLE:I = 0x55

.field static final INT_MED_SEA_GREEN:I = 0x56

.field static final INT_MED_SLATE_BLUE:I = 0x57

.field static final INT_MED_SPRING_GREEN:I = 0x58

.field static final INT_MED_TURQUOISE:I = 0x59

.field static final INT_MED_VIOLET_RED:I = 0x5a

.field static final INT_MIDNIGHT_BLUE:I = 0x5b

.field static final INT_MINT_CREAM:I = 0x5c

.field static final INT_MISTY_ROSE:I = 0x5d

.field static final INT_MOCCASIN:I = 0x5e

.field static final INT_NAVAJO_WHITE:I = 0x5f

.field static final INT_NAVY:I = 0x60

.field static final INT_OLD_LACE:I = 0x61

.field static final INT_OLIVE:I = 0x62

.field static final INT_OLIVE_DRAB:I = 0x63

.field static final INT_ORANGE:I = 0x64

.field static final INT_ORANGE_RED:I = 0x65

.field static final INT_ORCHID:I = 0x66

.field static final INT_PALE_GOLDENROD:I = 0x67

.field static final INT_PALE_GREEN:I = 0x68

.field static final INT_PALE_TURQUOISE:I = 0x69

.field static final INT_PALE_VIOLET_RED:I = 0x6a

.field static final INT_PAPAYA_WHIP:I = 0x6b

.field static final INT_PEACH_PUFF:I = 0x6c

.field static final INT_PERU:I = 0x6d

.field static final INT_PINK:I = 0x6e

.field static final INT_PLUM:I = 0x6f

.field static final INT_POWDER_BLUE:I = 0x70

.field static final INT_PURPLE:I = 0x71

.field static final INT_RED:I = 0x72

.field static final INT_ROSY_BROWN:I = 0x73

.field static final INT_ROYAL_BLUE:I = 0x74

.field static final INT_SADDLE_BROWN:I = 0x75

.field static final INT_SALMON:I = 0x76

.field static final INT_SANDY_BROWN:I = 0x77

.field static final INT_SEA_GREEN:I = 0x78

.field static final INT_SEA_SHELL:I = 0x79

.field static final INT_SIENNA:I = 0x7a

.field static final INT_SILVER:I = 0x7b

.field static final INT_SKY_BLUE:I = 0x7c

.field static final INT_SLATE_BLUE:I = 0x7d

.field static final INT_SLATE_GRAY:I = 0x7e

.field static final INT_SNOW:I = 0x7f

.field static final INT_SPRING_GREEN:I = 0x80

.field static final INT_STEEL_BLUE:I = 0x81

.field static final INT_TAN:I = 0x82

.field static final INT_TEAL:I = 0x83

.field static final INT_THISTLE:I = 0x84

.field static final INT_TOMATO:I = 0x85

.field static final INT_TURQUOISE:I = 0x86

.field static final INT_VIOLET:I = 0x87

.field static final INT_WHEAT:I = 0x88

.field static final INT_WHITE:I = 0x89

.field static final INT_WHITE_SMOKE:I = 0x8a

.field static final INT_YELLOW:I = 0x8b

.field static final INT_YELLOW_GREEN:I = 0x8c

.field private static final serialVersionUID:J = 0x1L

.field public static final table:Lorg/apache/xmlbeans/StringEnumAbstractBase$Table;


# direct methods
.method public static constructor <clinit>()V
    .locals 144

    new-instance v0, Lorg/apache/xmlbeans/StringEnumAbstractBase$Table;

    new-instance v2, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object v1, v2

    const-string v3, "aliceBlue"

    const/4 v4, 0x1

    invoke-direct {v2, v3, v4}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v3, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object v2, v3

    const-string v4, "antiqueWhite"

    const/4 v5, 0x2

    invoke-direct {v3, v4, v5}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v4, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object v3, v4

    const-string v5, "aqua"

    const/4 v6, 0x3

    invoke-direct {v4, v5, v6}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v5, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object v4, v5

    const-string v6, "aquamarine"

    const/4 v7, 0x4

    invoke-direct {v5, v6, v7}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v6, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object v5, v6

    const-string v7, "azure"

    const/4 v8, 0x5

    invoke-direct {v6, v7, v8}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v7, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object v6, v7

    const-string v8, "beige"

    const/4 v9, 0x6

    invoke-direct {v7, v8, v9}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v8, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object v7, v8

    const-string v9, "bisque"

    const/4 v10, 0x7

    invoke-direct {v8, v9, v10}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v9, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object v8, v9

    const-string v10, "black"

    const/16 v11, 0x8

    invoke-direct {v9, v10, v11}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v10, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object v9, v10

    const-string v11, "blanchedAlmond"

    const/16 v12, 0x9

    invoke-direct {v10, v11, v12}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v11, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object v10, v11

    const-string v12, "blue"

    const/16 v13, 0xa

    invoke-direct {v11, v12, v13}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v12, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object v11, v12

    const-string v13, "blueViolet"

    const/16 v14, 0xb

    invoke-direct {v12, v13, v14}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v13, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object v12, v13

    const-string v14, "brown"

    const/16 v15, 0xc

    invoke-direct {v13, v14, v15}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v14, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object v13, v14

    const-string v15, "burlyWood"

    move-object/from16 v141, v0

    const/16 v0, 0xd

    invoke-direct {v14, v15, v0}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object v14, v0

    const-string v15, "cadetBlue"

    move-object/from16 v142, v1

    const/16 v1, 0xe

    invoke-direct {v0, v15, v1}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object v15, v0

    const-string v1, "chartreuse"

    move-object/from16 v143, v2

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object/from16 v16, v0

    const-string v1, "chocolate"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object/from16 v17, v0

    const-string v1, "coral"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object/from16 v18, v0

    const-string v1, "cornflowerBlue"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object/from16 v19, v0

    const-string v1, "cornsilk"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object/from16 v20, v0

    const-string v1, "crimson"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object/from16 v21, v0

    const-string v1, "cyan"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object/from16 v22, v0

    const-string v1, "dkBlue"

    const/16 v2, 0x16

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object/from16 v23, v0

    const-string v1, "dkCyan"

    const/16 v2, 0x17

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object/from16 v24, v0

    const-string v1, "dkGoldenrod"

    const/16 v2, 0x18

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object/from16 v25, v0

    const-string v1, "dkGray"

    const/16 v2, 0x19

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object/from16 v26, v0

    const-string v1, "dkGreen"

    const/16 v2, 0x1a

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object/from16 v27, v0

    const-string v1, "dkKhaki"

    const/16 v2, 0x1b

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object/from16 v28, v0

    const-string v1, "dkMagenta"

    const/16 v2, 0x1c

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object/from16 v29, v0

    const-string v1, "dkOliveGreen"

    const/16 v2, 0x1d

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object/from16 v30, v0

    const-string v1, "dkOrange"

    const/16 v2, 0x1e

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object/from16 v31, v0

    const-string v1, "dkOrchid"

    const/16 v2, 0x1f

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object/from16 v32, v0

    const-string v1, "dkRed"

    const/16 v2, 0x20

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object/from16 v33, v0

    const-string v1, "dkSalmon"

    const/16 v2, 0x21

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object/from16 v34, v0

    const-string v1, "dkSeaGreen"

    const/16 v2, 0x22

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object/from16 v35, v0

    const-string v1, "dkSlateBlue"

    const/16 v2, 0x23

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object/from16 v36, v0

    const-string v1, "dkSlateGray"

    const/16 v2, 0x24

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object/from16 v37, v0

    const-string v1, "dkTurquoise"

    const/16 v2, 0x25

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object/from16 v38, v0

    const-string v1, "dkViolet"

    const/16 v2, 0x26

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object/from16 v39, v0

    const-string v1, "deepPink"

    const/16 v2, 0x27

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object/from16 v40, v0

    const-string v1, "deepSkyBlue"

    const/16 v2, 0x28

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object/from16 v41, v0

    const-string v1, "dimGray"

    const/16 v2, 0x29

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object/from16 v42, v0

    const-string v1, "dodgerBlue"

    const/16 v2, 0x2a

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object/from16 v43, v0

    const-string v1, "firebrick"

    const/16 v2, 0x2b

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object/from16 v44, v0

    const-string v1, "floralWhite"

    const/16 v2, 0x2c

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object/from16 v45, v0

    const-string v1, "forestGreen"

    const/16 v2, 0x2d

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object/from16 v46, v0

    const-string v1, "fuchsia"

    const/16 v2, 0x2e

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object/from16 v47, v0

    const-string v1, "gainsboro"

    const/16 v2, 0x2f

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object/from16 v48, v0

    const-string v1, "ghostWhite"

    const/16 v2, 0x30

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object/from16 v49, v0

    const-string v1, "gold"

    const/16 v2, 0x31

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object/from16 v50, v0

    const-string v1, "goldenrod"

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object/from16 v51, v0

    const-string v1, "gray"

    const/16 v2, 0x33

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object/from16 v52, v0

    const-string v1, "green"

    const/16 v2, 0x34

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object/from16 v53, v0

    const-string v1, "greenYellow"

    const/16 v2, 0x35

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object/from16 v54, v0

    const-string v1, "honeydew"

    const/16 v2, 0x36

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object/from16 v55, v0

    const-string v1, "hotPink"

    const/16 v2, 0x37

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object/from16 v56, v0

    const-string v1, "indianRed"

    const/16 v2, 0x38

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object/from16 v57, v0

    const-string v1, "indigo"

    const/16 v2, 0x39

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object/from16 v58, v0

    const-string v1, "ivory"

    const/16 v2, 0x3a

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object/from16 v59, v0

    const-string v1, "khaki"

    const/16 v2, 0x3b

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object/from16 v60, v0

    const-string v1, "lavender"

    const/16 v2, 0x3c

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object/from16 v61, v0

    const-string v1, "lavenderBlush"

    const/16 v2, 0x3d

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object/from16 v62, v0

    const-string v1, "lawnGreen"

    const/16 v2, 0x3e

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object/from16 v63, v0

    const-string v1, "lemonChiffon"

    const/16 v2, 0x3f

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object/from16 v64, v0

    const-string v1, "ltBlue"

    const/16 v2, 0x40

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object/from16 v65, v0

    const-string v1, "ltCoral"

    const/16 v2, 0x41

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object/from16 v66, v0

    const-string v1, "ltCyan"

    const/16 v2, 0x42

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object/from16 v67, v0

    const-string v1, "ltGoldenrodYellow"

    const/16 v2, 0x43

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object/from16 v68, v0

    const-string v1, "ltGray"

    const/16 v2, 0x44

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object/from16 v69, v0

    const-string v1, "ltGreen"

    const/16 v2, 0x45

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object/from16 v70, v0

    const-string v1, "ltPink"

    const/16 v2, 0x46

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object/from16 v71, v0

    const-string v1, "ltSalmon"

    const/16 v2, 0x47

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object/from16 v72, v0

    const-string v1, "ltSeaGreen"

    const/16 v2, 0x48

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object/from16 v73, v0

    const-string v1, "ltSkyBlue"

    const/16 v2, 0x49

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object/from16 v74, v0

    const-string v1, "ltSlateGray"

    const/16 v2, 0x4a

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object/from16 v75, v0

    const-string v1, "ltSteelBlue"

    const/16 v2, 0x4b

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object/from16 v76, v0

    const-string v1, "ltYellow"

    const/16 v2, 0x4c

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object/from16 v77, v0

    const-string v1, "lime"

    const/16 v2, 0x4d

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object/from16 v78, v0

    const-string v1, "limeGreen"

    const/16 v2, 0x4e

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object/from16 v79, v0

    const-string v1, "linen"

    const/16 v2, 0x4f

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object/from16 v80, v0

    const-string v1, "magenta"

    const/16 v2, 0x50

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object/from16 v81, v0

    const-string v1, "maroon"

    const/16 v2, 0x51

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object/from16 v82, v0

    const-string v1, "medAquamarine"

    const/16 v2, 0x52

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object/from16 v83, v0

    const-string v1, "medBlue"

    const/16 v2, 0x53

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object/from16 v84, v0

    const-string v1, "medOrchid"

    const/16 v2, 0x54

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object/from16 v85, v0

    const-string v1, "medPurple"

    const/16 v2, 0x55

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object/from16 v86, v0

    const-string v1, "medSeaGreen"

    const/16 v2, 0x56

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object/from16 v87, v0

    const-string v1, "medSlateBlue"

    const/16 v2, 0x57

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object/from16 v88, v0

    const-string v1, "medSpringGreen"

    const/16 v2, 0x58

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object/from16 v89, v0

    const-string v1, "medTurquoise"

    const/16 v2, 0x59

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object/from16 v90, v0

    const-string v1, "medVioletRed"

    const/16 v2, 0x5a

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object/from16 v91, v0

    const-string v1, "midnightBlue"

    const/16 v2, 0x5b

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object/from16 v92, v0

    const-string v1, "mintCream"

    const/16 v2, 0x5c

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object/from16 v93, v0

    const-string v1, "mistyRose"

    const/16 v2, 0x5d

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object/from16 v94, v0

    const-string v1, "moccasin"

    const/16 v2, 0x5e

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object/from16 v95, v0

    const-string v1, "navajoWhite"

    const/16 v2, 0x5f

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object/from16 v96, v0

    const-string v1, "navy"

    const/16 v2, 0x60

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object/from16 v97, v0

    const-string v1, "oldLace"

    const/16 v2, 0x61

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object/from16 v98, v0

    const-string v1, "olive"

    const/16 v2, 0x62

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object/from16 v99, v0

    const-string v1, "oliveDrab"

    const/16 v2, 0x63

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object/from16 v100, v0

    const-string v1, "orange"

    const/16 v2, 0x64

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object/from16 v101, v0

    const-string v1, "orangeRed"

    const/16 v2, 0x65

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object/from16 v102, v0

    const-string v1, "orchid"

    const/16 v2, 0x66

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object/from16 v103, v0

    const-string v1, "paleGoldenrod"

    const/16 v2, 0x67

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object/from16 v104, v0

    const-string v1, "paleGreen"

    const/16 v2, 0x68

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object/from16 v105, v0

    const-string v1, "paleTurquoise"

    const/16 v2, 0x69

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object/from16 v106, v0

    const-string v1, "paleVioletRed"

    const/16 v2, 0x6a

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object/from16 v107, v0

    const-string v1, "papayaWhip"

    const/16 v2, 0x6b

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object/from16 v108, v0

    const-string v1, "peachPuff"

    const/16 v2, 0x6c

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object/from16 v109, v0

    const-string v1, "peru"

    const/16 v2, 0x6d

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object/from16 v110, v0

    const-string v1, "pink"

    const/16 v2, 0x6e

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object/from16 v111, v0

    const-string v1, "plum"

    const/16 v2, 0x6f

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object/from16 v112, v0

    const-string v1, "powderBlue"

    const/16 v2, 0x70

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object/from16 v113, v0

    const-string v1, "purple"

    const/16 v2, 0x71

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object/from16 v114, v0

    const-string v1, "red"

    const/16 v2, 0x72

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object/from16 v115, v0

    const-string v1, "rosyBrown"

    const/16 v2, 0x73

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object/from16 v116, v0

    const-string v1, "royalBlue"

    const/16 v2, 0x74

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object/from16 v117, v0

    const-string v1, "saddleBrown"

    const/16 v2, 0x75

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object/from16 v118, v0

    const-string v1, "salmon"

    const/16 v2, 0x76

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object/from16 v119, v0

    const-string v1, "sandyBrown"

    const/16 v2, 0x77

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object/from16 v120, v0

    const-string v1, "seaGreen"

    const/16 v2, 0x78

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object/from16 v121, v0

    const-string v1, "seaShell"

    const/16 v2, 0x79

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object/from16 v122, v0

    const-string v1, "sienna"

    const/16 v2, 0x7a

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object/from16 v123, v0

    const-string v1, "silver"

    const/16 v2, 0x7b

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object/from16 v124, v0

    const-string v1, "skyBlue"

    const/16 v2, 0x7c

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object/from16 v125, v0

    const-string v1, "slateBlue"

    const/16 v2, 0x7d

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object/from16 v126, v0

    const-string v1, "slateGray"

    const/16 v2, 0x7e

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object/from16 v127, v0

    const-string v1, "snow"

    const/16 v2, 0x7f

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object/from16 v128, v0

    const-string v1, "springGreen"

    const/16 v2, 0x80

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object/from16 v129, v0

    const-string v1, "steelBlue"

    const/16 v2, 0x81

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object/from16 v130, v0

    const-string v1, "tan"

    const/16 v2, 0x82

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object/from16 v131, v0

    const-string v1, "teal"

    const/16 v2, 0x83

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object/from16 v132, v0

    const-string v1, "thistle"

    const/16 v2, 0x84

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object/from16 v133, v0

    const-string v1, "tomato"

    const/16 v2, 0x85

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object/from16 v134, v0

    const-string v1, "turquoise"

    const/16 v2, 0x86

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object/from16 v135, v0

    const-string v1, "violet"

    const/16 v2, 0x87

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object/from16 v136, v0

    const-string v1, "wheat"

    const/16 v2, 0x88

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object/from16 v137, v0

    const-string v1, "white"

    const/16 v2, 0x89

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object/from16 v138, v0

    const-string v1, "whiteSmoke"

    const/16 v2, 0x8a

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object/from16 v139, v0

    const-string v1, "yellow"

    const/16 v2, 0x8b

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-object/from16 v140, v0

    const-string v1, "yellowGreen"

    const/16 v2, 0x8c

    invoke-direct {v0, v1, v2}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;-><init>(Ljava/lang/String;I)V

    move-object/from16 v1, v142

    move-object/from16 v2, v143

    filled-new-array/range {v1 .. v140}, [Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-result-object v0

    move-object/from16 v1, v141

    invoke-direct {v1, v0}, Lorg/apache/xmlbeans/StringEnumAbstractBase$Table;-><init>([Lorg/apache/xmlbeans/StringEnumAbstractBase;)V

    sput-object v1, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;->table:Lorg/apache/xmlbeans/StringEnumAbstractBase$Table;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lorg/apache/xmlbeans/StringEnumAbstractBase;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static forInt(I)Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;
    .locals 1

    sget-object v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;->table:Lorg/apache/xmlbeans/StringEnumAbstractBase$Table;

    invoke-virtual {v0, p0}, Lorg/apache/xmlbeans/StringEnumAbstractBase$Table;->forInt(I)Lorg/apache/xmlbeans/StringEnumAbstractBase;

    move-result-object p0

    check-cast p0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    return-object p0
.end method

.method public static forString(Ljava/lang/String;)Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;
    .locals 1

    sget-object v0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;->table:Lorg/apache/xmlbeans/StringEnumAbstractBase$Table;

    invoke-virtual {v0, p0}, Lorg/apache/xmlbeans/StringEnumAbstractBase$Table;->forString(Ljava/lang/String;)Lorg/apache/xmlbeans/StringEnumAbstractBase;

    move-result-object p0

    check-cast p0, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    return-object p0
.end method

.method private readResolve()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lorg/apache/xmlbeans/StringEnumAbstractBase;->intValue()I

    move-result v0

    invoke-static {v0}, Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;->forInt(I)Lorg/openxmlformats/schemas/drawingml/x2006/main/STPresetColorVal$Enum;

    move-result-object v0

    return-object v0
.end method

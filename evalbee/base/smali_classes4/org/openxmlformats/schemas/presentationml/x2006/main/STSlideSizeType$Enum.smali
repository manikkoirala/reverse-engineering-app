.class public final Lorg/openxmlformats/schemas/presentationml/x2006/main/STSlideSizeType$Enum;
.super Lorg/apache/xmlbeans/StringEnumAbstractBase;
.source "SourceFile"


# static fields
.field static final INT_A_3:I = 0x9

.field static final INT_A_4:I = 0x3

.field static final INT_BANNER:I = 0x6

.field static final INT_B_4_ISO:I = 0xa

.field static final INT_B_4_JIS:I = 0xc

.field static final INT_B_5_ISO:I = 0xb

.field static final INT_B_5_JIS:I = 0xd

.field static final INT_CUSTOM:I = 0x7

.field static final INT_HAGAKI_CARD:I = 0xe

.field static final INT_LEDGER:I = 0x8

.field static final INT_LETTER:I = 0x2

.field static final INT_OVERHEAD:I = 0x5

.field static final INT_SCREEN_16_X_10:I = 0x10

.field static final INT_SCREEN_16_X_9:I = 0xf

.field static final INT_SCREEN_4_X_3:I = 0x1

.field static final INT_X_35_MM:I = 0x4

.field private static final serialVersionUID:J = 0x1L

.field public static final table:Lorg/apache/xmlbeans/StringEnumAbstractBase$Table;


# direct methods
.method public static constructor <clinit>()V
    .locals 19

    new-instance v0, Lorg/apache/xmlbeans/StringEnumAbstractBase$Table;

    new-instance v1, Lorg/openxmlformats/schemas/presentationml/x2006/main/STSlideSizeType$Enum;

    const-string v2, "screen4x3"

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Lorg/openxmlformats/schemas/presentationml/x2006/main/STSlideSizeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v2, Lorg/openxmlformats/schemas/presentationml/x2006/main/STSlideSizeType$Enum;

    const-string v3, "letter"

    const/4 v4, 0x2

    invoke-direct {v2, v3, v4}, Lorg/openxmlformats/schemas/presentationml/x2006/main/STSlideSizeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v3, Lorg/openxmlformats/schemas/presentationml/x2006/main/STSlideSizeType$Enum;

    const-string v4, "A4"

    const/4 v5, 0x3

    invoke-direct {v3, v4, v5}, Lorg/openxmlformats/schemas/presentationml/x2006/main/STSlideSizeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v4, Lorg/openxmlformats/schemas/presentationml/x2006/main/STSlideSizeType$Enum;

    const-string v5, "35mm"

    const/4 v6, 0x4

    invoke-direct {v4, v5, v6}, Lorg/openxmlformats/schemas/presentationml/x2006/main/STSlideSizeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v5, Lorg/openxmlformats/schemas/presentationml/x2006/main/STSlideSizeType$Enum;

    const-string v6, "overhead"

    const/4 v7, 0x5

    invoke-direct {v5, v6, v7}, Lorg/openxmlformats/schemas/presentationml/x2006/main/STSlideSizeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v6, Lorg/openxmlformats/schemas/presentationml/x2006/main/STSlideSizeType$Enum;

    const-string v7, "banner"

    const/4 v8, 0x6

    invoke-direct {v6, v7, v8}, Lorg/openxmlformats/schemas/presentationml/x2006/main/STSlideSizeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v7, Lorg/openxmlformats/schemas/presentationml/x2006/main/STSlideSizeType$Enum;

    const-string v8, "custom"

    const/4 v9, 0x7

    invoke-direct {v7, v8, v9}, Lorg/openxmlformats/schemas/presentationml/x2006/main/STSlideSizeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v8, Lorg/openxmlformats/schemas/presentationml/x2006/main/STSlideSizeType$Enum;

    const-string v9, "ledger"

    const/16 v10, 0x8

    invoke-direct {v8, v9, v10}, Lorg/openxmlformats/schemas/presentationml/x2006/main/STSlideSizeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v9, Lorg/openxmlformats/schemas/presentationml/x2006/main/STSlideSizeType$Enum;

    const-string v10, "A3"

    const/16 v11, 0x9

    invoke-direct {v9, v10, v11}, Lorg/openxmlformats/schemas/presentationml/x2006/main/STSlideSizeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v10, Lorg/openxmlformats/schemas/presentationml/x2006/main/STSlideSizeType$Enum;

    const-string v11, "B4ISO"

    const/16 v12, 0xa

    invoke-direct {v10, v11, v12}, Lorg/openxmlformats/schemas/presentationml/x2006/main/STSlideSizeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v11, Lorg/openxmlformats/schemas/presentationml/x2006/main/STSlideSizeType$Enum;

    const-string v12, "B5ISO"

    const/16 v13, 0xb

    invoke-direct {v11, v12, v13}, Lorg/openxmlformats/schemas/presentationml/x2006/main/STSlideSizeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v12, Lorg/openxmlformats/schemas/presentationml/x2006/main/STSlideSizeType$Enum;

    const-string v13, "B4JIS"

    const/16 v14, 0xc

    invoke-direct {v12, v13, v14}, Lorg/openxmlformats/schemas/presentationml/x2006/main/STSlideSizeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v13, Lorg/openxmlformats/schemas/presentationml/x2006/main/STSlideSizeType$Enum;

    const-string v14, "B5JIS"

    const/16 v15, 0xd

    invoke-direct {v13, v14, v15}, Lorg/openxmlformats/schemas/presentationml/x2006/main/STSlideSizeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v14, Lorg/openxmlformats/schemas/presentationml/x2006/main/STSlideSizeType$Enum;

    const-string v15, "hagakiCard"

    move-object/from16 v17, v0

    const/16 v0, 0xe

    invoke-direct {v14, v15, v0}, Lorg/openxmlformats/schemas/presentationml/x2006/main/STSlideSizeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v15, Lorg/openxmlformats/schemas/presentationml/x2006/main/STSlideSizeType$Enum;

    const-string v0, "screen16x9"

    move-object/from16 v16, v14

    const/16 v14, 0xf

    invoke-direct {v15, v0, v14}, Lorg/openxmlformats/schemas/presentationml/x2006/main/STSlideSizeType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lorg/openxmlformats/schemas/presentationml/x2006/main/STSlideSizeType$Enum;

    const-string v14, "screen16x10"

    move-object/from16 v18, v15

    const/16 v15, 0x10

    invoke-direct {v0, v14, v15}, Lorg/openxmlformats/schemas/presentationml/x2006/main/STSlideSizeType$Enum;-><init>(Ljava/lang/String;I)V

    move-object/from16 v14, v16

    move-object/from16 v15, v18

    move-object/from16 v16, v0

    filled-new-array/range {v1 .. v16}, [Lorg/openxmlformats/schemas/presentationml/x2006/main/STSlideSizeType$Enum;

    move-result-object v0

    move-object/from16 v1, v17

    invoke-direct {v1, v0}, Lorg/apache/xmlbeans/StringEnumAbstractBase$Table;-><init>([Lorg/apache/xmlbeans/StringEnumAbstractBase;)V

    sput-object v1, Lorg/openxmlformats/schemas/presentationml/x2006/main/STSlideSizeType$Enum;->table:Lorg/apache/xmlbeans/StringEnumAbstractBase$Table;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lorg/apache/xmlbeans/StringEnumAbstractBase;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static forInt(I)Lorg/openxmlformats/schemas/presentationml/x2006/main/STSlideSizeType$Enum;
    .locals 1

    sget-object v0, Lorg/openxmlformats/schemas/presentationml/x2006/main/STSlideSizeType$Enum;->table:Lorg/apache/xmlbeans/StringEnumAbstractBase$Table;

    invoke-virtual {v0, p0}, Lorg/apache/xmlbeans/StringEnumAbstractBase$Table;->forInt(I)Lorg/apache/xmlbeans/StringEnumAbstractBase;

    move-result-object p0

    check-cast p0, Lorg/openxmlformats/schemas/presentationml/x2006/main/STSlideSizeType$Enum;

    return-object p0
.end method

.method public static forString(Ljava/lang/String;)Lorg/openxmlformats/schemas/presentationml/x2006/main/STSlideSizeType$Enum;
    .locals 1

    sget-object v0, Lorg/openxmlformats/schemas/presentationml/x2006/main/STSlideSizeType$Enum;->table:Lorg/apache/xmlbeans/StringEnumAbstractBase$Table;

    invoke-virtual {v0, p0}, Lorg/apache/xmlbeans/StringEnumAbstractBase$Table;->forString(Ljava/lang/String;)Lorg/apache/xmlbeans/StringEnumAbstractBase;

    move-result-object p0

    check-cast p0, Lorg/openxmlformats/schemas/presentationml/x2006/main/STSlideSizeType$Enum;

    return-object p0
.end method

.method private readResolve()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lorg/apache/xmlbeans/StringEnumAbstractBase;->intValue()I

    move-result v0

    invoke-static {v0}, Lorg/openxmlformats/schemas/presentationml/x2006/main/STSlideSizeType$Enum;->forInt(I)Lorg/openxmlformats/schemas/presentationml/x2006/main/STSlideSizeType$Enum;

    move-result-object v0

    return-object v0
.end method

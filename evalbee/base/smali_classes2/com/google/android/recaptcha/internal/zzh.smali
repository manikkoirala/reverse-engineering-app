.class public final Lcom/google/android/recaptcha/internal/zzh;
.super Ljava/lang/Exception;
.source "SourceFile"


# static fields
.field public static final zza:Lcom/google/android/recaptcha/internal/zzg;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private static final zzb:Ljava/util/Map;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# instance fields
.field private final zzc:Lcom/google/android/recaptcha/internal/zzf;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final zzd:Lcom/google/android/recaptcha/internal/zzd;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field

.field private final zze:Ljava/util/Map;
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 11

    new-instance v0, Lcom/google/android/recaptcha/internal/zzg;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/recaptcha/internal/zzg;-><init>(Lgq;)V

    sput-object v0, Lcom/google/android/recaptcha/internal/zzh;->zza:Lcom/google/android/recaptcha/internal/zzg;

    sget-object v0, Lcom/google/android/recaptcha/internal/zzmf;->zzc:Lcom/google/android/recaptcha/internal/zzmf;

    new-instance v1, Lcom/google/android/recaptcha/internal/zzh;

    sget-object v2, Lcom/google/android/recaptcha/internal/zzf;->zze:Lcom/google/android/recaptcha/internal/zzf;

    sget-object v3, Lcom/google/android/recaptcha/internal/zzd;->zzm:Lcom/google/android/recaptcha/internal/zzd;

    invoke-direct {v1, v2, v3}, Lcom/google/android/recaptcha/internal/zzh;-><init>(Lcom/google/android/recaptcha/internal/zzf;Lcom/google/android/recaptcha/internal/zzd;)V

    invoke-static {v0, v1}, Ldz1;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v4

    sget-object v0, Lcom/google/android/recaptcha/internal/zzmf;->zzd:Lcom/google/android/recaptcha/internal/zzmf;

    new-instance v1, Lcom/google/android/recaptcha/internal/zzh;

    sget-object v2, Lcom/google/android/recaptcha/internal/zzf;->zzc:Lcom/google/android/recaptcha/internal/zzf;

    sget-object v3, Lcom/google/android/recaptcha/internal/zzd;->zzk:Lcom/google/android/recaptcha/internal/zzd;

    invoke-direct {v1, v2, v3}, Lcom/google/android/recaptcha/internal/zzh;-><init>(Lcom/google/android/recaptcha/internal/zzf;Lcom/google/android/recaptcha/internal/zzd;)V

    invoke-static {v0, v1}, Ldz1;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v5

    sget-object v0, Lcom/google/android/recaptcha/internal/zzmf;->zze:Lcom/google/android/recaptcha/internal/zzmf;

    new-instance v1, Lcom/google/android/recaptcha/internal/zzh;

    sget-object v3, Lcom/google/android/recaptcha/internal/zzf;->zzf:Lcom/google/android/recaptcha/internal/zzf;

    sget-object v6, Lcom/google/android/recaptcha/internal/zzd;->zzn:Lcom/google/android/recaptcha/internal/zzd;

    invoke-direct {v1, v3, v6}, Lcom/google/android/recaptcha/internal/zzh;-><init>(Lcom/google/android/recaptcha/internal/zzf;Lcom/google/android/recaptcha/internal/zzd;)V

    invoke-static {v0, v1}, Ldz1;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v6

    sget-object v0, Lcom/google/android/recaptcha/internal/zzmf;->zzf:Lcom/google/android/recaptcha/internal/zzmf;

    new-instance v1, Lcom/google/android/recaptcha/internal/zzh;

    sget-object v3, Lcom/google/android/recaptcha/internal/zzf;->zzg:Lcom/google/android/recaptcha/internal/zzf;

    sget-object v7, Lcom/google/android/recaptcha/internal/zzd;->zzo:Lcom/google/android/recaptcha/internal/zzd;

    invoke-direct {v1, v3, v7}, Lcom/google/android/recaptcha/internal/zzh;-><init>(Lcom/google/android/recaptcha/internal/zzf;Lcom/google/android/recaptcha/internal/zzd;)V

    invoke-static {v0, v1}, Ldz1;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v7

    sget-object v0, Lcom/google/android/recaptcha/internal/zzmf;->zzi:Lcom/google/android/recaptcha/internal/zzmf;

    new-instance v1, Lcom/google/android/recaptcha/internal/zzh;

    sget-object v3, Lcom/google/android/recaptcha/internal/zzf;->zzh:Lcom/google/android/recaptcha/internal/zzf;

    sget-object v8, Lcom/google/android/recaptcha/internal/zzd;->zzp:Lcom/google/android/recaptcha/internal/zzd;

    invoke-direct {v1, v3, v8}, Lcom/google/android/recaptcha/internal/zzh;-><init>(Lcom/google/android/recaptcha/internal/zzf;Lcom/google/android/recaptcha/internal/zzd;)V

    invoke-static {v0, v1}, Ldz1;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v8

    sget-object v0, Lcom/google/android/recaptcha/internal/zzmf;->zzh:Lcom/google/android/recaptcha/internal/zzmf;

    new-instance v1, Lcom/google/android/recaptcha/internal/zzh;

    sget-object v3, Lcom/google/android/recaptcha/internal/zzf;->zzi:Lcom/google/android/recaptcha/internal/zzf;

    sget-object v9, Lcom/google/android/recaptcha/internal/zzd;->zzq:Lcom/google/android/recaptcha/internal/zzd;

    invoke-direct {v1, v3, v9}, Lcom/google/android/recaptcha/internal/zzh;-><init>(Lcom/google/android/recaptcha/internal/zzf;Lcom/google/android/recaptcha/internal/zzd;)V

    invoke-static {v0, v1}, Ldz1;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v9

    sget-object v0, Lcom/google/android/recaptcha/internal/zzmf;->zzj:Lcom/google/android/recaptcha/internal/zzmf;

    new-instance v1, Lcom/google/android/recaptcha/internal/zzh;

    sget-object v3, Lcom/google/android/recaptcha/internal/zzd;->zzv:Lcom/google/android/recaptcha/internal/zzd;

    invoke-direct {v1, v2, v3}, Lcom/google/android/recaptcha/internal/zzh;-><init>(Lcom/google/android/recaptcha/internal/zzf;Lcom/google/android/recaptcha/internal/zzd;)V

    invoke-static {v0, v1}, Ldz1;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v10

    filled-new-array/range {v4 .. v10}, [Lkotlin/Pair;

    move-result-object v0

    invoke-static {v0}, Lkotlin/collections/b;->j([Lkotlin/Pair;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lcom/google/android/recaptcha/internal/zzh;->zzb:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/recaptcha/internal/zzf;Lcom/google/android/recaptcha/internal/zzd;)V
    .locals 9
    .param p1    # Lcom/google/android/recaptcha/internal/zzf;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param
    .param p2    # Lcom/google/android/recaptcha/internal/zzd;
        .annotation build Lorg/jetbrains/annotations/NotNull;
        .end annotation
    .end param

    invoke-direct {p0}, Ljava/lang/Exception;-><init>()V

    iput-object p1, p0, Lcom/google/android/recaptcha/internal/zzh;->zzc:Lcom/google/android/recaptcha/internal/zzf;

    iput-object p2, p0, Lcom/google/android/recaptcha/internal/zzh;->zzd:Lcom/google/android/recaptcha/internal/zzd;

    sget-object p1, Lcom/google/android/recaptcha/internal/zzf;->zze:Lcom/google/android/recaptcha/internal/zzf;

    new-instance p2, Lcom/google/android/recaptcha/RecaptchaException;

    sget-object v0, Lcom/google/android/recaptcha/RecaptchaErrorCode;->NETWORK_ERROR:Lcom/google/android/recaptcha/RecaptchaErrorCode;

    const/4 v1, 0x0

    const/4 v2, 0x2

    invoke-direct {p2, v0, v1, v2, v1}, Lcom/google/android/recaptcha/RecaptchaException;-><init>(Lcom/google/android/recaptcha/RecaptchaErrorCode;Ljava/lang/String;ILgq;)V

    invoke-static {p1, p2}, Ldz1;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v3

    sget-object p1, Lcom/google/android/recaptcha/internal/zzf;->zzf:Lcom/google/android/recaptcha/internal/zzf;

    new-instance p2, Lcom/google/android/recaptcha/RecaptchaException;

    sget-object v0, Lcom/google/android/recaptcha/RecaptchaErrorCode;->INVALID_SITEKEY:Lcom/google/android/recaptcha/RecaptchaErrorCode;

    invoke-direct {p2, v0, v1, v2, v1}, Lcom/google/android/recaptcha/RecaptchaException;-><init>(Lcom/google/android/recaptcha/RecaptchaErrorCode;Ljava/lang/String;ILgq;)V

    invoke-static {p1, p2}, Ldz1;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v4

    sget-object p1, Lcom/google/android/recaptcha/internal/zzf;->zzg:Lcom/google/android/recaptcha/internal/zzf;

    new-instance p2, Lcom/google/android/recaptcha/RecaptchaException;

    sget-object v0, Lcom/google/android/recaptcha/RecaptchaErrorCode;->INVALID_KEYTYPE:Lcom/google/android/recaptcha/RecaptchaErrorCode;

    invoke-direct {p2, v0, v1, v2, v1}, Lcom/google/android/recaptcha/RecaptchaException;-><init>(Lcom/google/android/recaptcha/RecaptchaErrorCode;Ljava/lang/String;ILgq;)V

    invoke-static {p1, p2}, Ldz1;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v5

    sget-object p1, Lcom/google/android/recaptcha/internal/zzf;->zzh:Lcom/google/android/recaptcha/internal/zzf;

    new-instance p2, Lcom/google/android/recaptcha/RecaptchaException;

    sget-object v0, Lcom/google/android/recaptcha/RecaptchaErrorCode;->INVALID_PACKAGE_NAME:Lcom/google/android/recaptcha/RecaptchaErrorCode;

    invoke-direct {p2, v0, v1, v2, v1}, Lcom/google/android/recaptcha/RecaptchaException;-><init>(Lcom/google/android/recaptcha/RecaptchaErrorCode;Ljava/lang/String;ILgq;)V

    invoke-static {p1, p2}, Ldz1;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v6

    sget-object p1, Lcom/google/android/recaptcha/internal/zzf;->zzi:Lcom/google/android/recaptcha/internal/zzf;

    new-instance p2, Lcom/google/android/recaptcha/RecaptchaException;

    sget-object v0, Lcom/google/android/recaptcha/RecaptchaErrorCode;->INVALID_ACTION:Lcom/google/android/recaptcha/RecaptchaErrorCode;

    invoke-direct {p2, v0, v1, v2, v1}, Lcom/google/android/recaptcha/RecaptchaException;-><init>(Lcom/google/android/recaptcha/RecaptchaErrorCode;Ljava/lang/String;ILgq;)V

    invoke-static {p1, p2}, Ldz1;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v7

    sget-object p1, Lcom/google/android/recaptcha/internal/zzf;->zzc:Lcom/google/android/recaptcha/internal/zzf;

    new-instance p2, Lcom/google/android/recaptcha/RecaptchaException;

    sget-object v0, Lcom/google/android/recaptcha/RecaptchaErrorCode;->INTERNAL_ERROR:Lcom/google/android/recaptcha/RecaptchaErrorCode;

    invoke-direct {p2, v0, v1, v2, v1}, Lcom/google/android/recaptcha/RecaptchaException;-><init>(Lcom/google/android/recaptcha/RecaptchaErrorCode;Ljava/lang/String;ILgq;)V

    invoke-static {p1, p2}, Ldz1;->a(Ljava/lang/Object;Ljava/lang/Object;)Lkotlin/Pair;

    move-result-object v8

    filled-new-array/range {v3 .. v8}, [Lkotlin/Pair;

    move-result-object p1

    invoke-static {p1}, Lkotlin/collections/b;->j([Lkotlin/Pair;)Ljava/util/Map;

    move-result-object p1

    iput-object p1, p0, Lcom/google/android/recaptcha/internal/zzh;->zze:Ljava/util/Map;

    return-void
.end method

.method public static final synthetic zzd()Ljava/util/Map;
    .locals 1

    sget-object v0, Lcom/google/android/recaptcha/internal/zzh;->zzb:Ljava/util/Map;

    return-object v0
.end method


# virtual methods
.method public final zza()Lcom/google/android/recaptcha/internal/zzd;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    iget-object v0, p0, Lcom/google/android/recaptcha/internal/zzh;->zzd:Lcom/google/android/recaptcha/internal/zzd;

    return-object v0
.end method

.method public final zzb()Lcom/google/android/recaptcha/internal/zzf;
    .locals 1
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    iget-object v0, p0, Lcom/google/android/recaptcha/internal/zzh;->zzc:Lcom/google/android/recaptcha/internal/zzf;

    return-object v0
.end method

.method public final zzc()Lcom/google/android/recaptcha/RecaptchaException;
    .locals 4
    .annotation build Lorg/jetbrains/annotations/NotNull;
    .end annotation

    iget-object v0, p0, Lcom/google/android/recaptcha/internal/zzh;->zze:Ljava/util/Map;

    iget-object v1, p0, Lcom/google/android/recaptcha/internal/zzh;->zzc:Lcom/google/android/recaptcha/internal/zzf;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/recaptcha/RecaptchaException;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/recaptcha/RecaptchaException;

    sget-object v1, Lcom/google/android/recaptcha/RecaptchaErrorCode;->UNKNOWN_ERROR:Lcom/google/android/recaptcha/RecaptchaErrorCode;

    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v3, v2, v3}, Lcom/google/android/recaptcha/RecaptchaException;-><init>(Lcom/google/android/recaptcha/RecaptchaErrorCode;Ljava/lang/String;ILgq;)V

    :cond_0
    return-object v0
.end method

.class public final Lcom/google/android/material/R$string;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/material/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final abc_action_bar_home_description:I = 0x7f120000

.field public static final abc_action_bar_up_description:I = 0x7f120001

.field public static final abc_action_menu_overflow_description:I = 0x7f120002

.field public static final abc_action_mode_done:I = 0x7f120003

.field public static final abc_activity_chooser_view_see_all:I = 0x7f120004

.field public static final abc_activitychooserview_choose_application:I = 0x7f120005

.field public static final abc_capital_off:I = 0x7f120006

.field public static final abc_capital_on:I = 0x7f120007

.field public static final abc_menu_alt_shortcut_label:I = 0x7f120008

.field public static final abc_menu_ctrl_shortcut_label:I = 0x7f120009

.field public static final abc_menu_delete_shortcut_label:I = 0x7f12000a

.field public static final abc_menu_enter_shortcut_label:I = 0x7f12000b

.field public static final abc_menu_function_shortcut_label:I = 0x7f12000c

.field public static final abc_menu_meta_shortcut_label:I = 0x7f12000d

.field public static final abc_menu_shift_shortcut_label:I = 0x7f12000e

.field public static final abc_menu_space_shortcut_label:I = 0x7f12000f

.field public static final abc_menu_sym_shortcut_label:I = 0x7f120010

.field public static final abc_prepend_shortcut_label:I = 0x7f120011

.field public static final abc_search_hint:I = 0x7f120012

.field public static final abc_searchview_description_clear:I = 0x7f120013

.field public static final abc_searchview_description_query:I = 0x7f120014

.field public static final abc_searchview_description_search:I = 0x7f120015

.field public static final abc_searchview_description_submit:I = 0x7f120016

.field public static final abc_searchview_description_voice:I = 0x7f120017

.field public static final abc_shareactionprovider_share_with:I = 0x7f120018

.field public static final abc_shareactionprovider_share_with_application:I = 0x7f120019

.field public static final abc_toolbar_collapse_description:I = 0x7f12001a

.field public static final appbar_scrolling_view_behavior:I = 0x7f12003c

.field public static final bottom_sheet_behavior:I = 0x7f12004a

.field public static final bottomsheet_action_collapse:I = 0x7f12004b

.field public static final bottomsheet_action_expand:I = 0x7f12004c

.field public static final bottomsheet_action_expand_halfway:I = 0x7f12004d

.field public static final bottomsheet_drag_handle_clicked:I = 0x7f12004e

.field public static final bottomsheet_drag_handle_content_description:I = 0x7f12004f

.field public static final character_counter_content_description:I = 0x7f120056

.field public static final character_counter_overflowed_content_description:I = 0x7f120057

.field public static final character_counter_pattern:I = 0x7f120058

.field public static final clear_text_end_icon_content_description:I = 0x7f12005e

.field public static final error_a11y_label:I = 0x7f1200cb

.field public static final error_icon_content_description:I = 0x7f1200cc

.field public static final exposed_dropdown_menu_content_description:I = 0x7f1200de

.field public static final fab_transformation_scrim_behavior:I = 0x7f1200e0

.field public static final fab_transformation_sheet_behavior:I = 0x7f1200e1

.field public static final hide_bottom_view_on_scroll_behavior:I = 0x7f12010c

.field public static final icon_content_description:I = 0x7f12010f

.field public static final item_view_role_description:I = 0x7f120121

.field public static final m3_exceed_max_badge_text_suffix:I = 0x7f12013a

.field public static final m3_ref_typeface_brand_medium:I = 0x7f12013b

.field public static final m3_ref_typeface_brand_regular:I = 0x7f12013c

.field public static final m3_ref_typeface_plain_medium:I = 0x7f12013d

.field public static final m3_ref_typeface_plain_regular:I = 0x7f12013e

.field public static final m3_sys_motion_easing_emphasized:I = 0x7f12013f

.field public static final m3_sys_motion_easing_emphasized_accelerate:I = 0x7f120140

.field public static final m3_sys_motion_easing_emphasized_decelerate:I = 0x7f120141

.field public static final m3_sys_motion_easing_emphasized_path_data:I = 0x7f120142

.field public static final m3_sys_motion_easing_legacy:I = 0x7f120143

.field public static final m3_sys_motion_easing_legacy_accelerate:I = 0x7f120144

.field public static final m3_sys_motion_easing_legacy_decelerate:I = 0x7f120145

.field public static final m3_sys_motion_easing_linear:I = 0x7f120146

.field public static final m3_sys_motion_easing_standard:I = 0x7f120147

.field public static final m3_sys_motion_easing_standard_accelerate:I = 0x7f120148

.field public static final m3_sys_motion_easing_standard_decelerate:I = 0x7f120149

.field public static final material_clock_display_divider:I = 0x7f12014e

.field public static final material_clock_toggle_content_description:I = 0x7f12014f

.field public static final material_hour_24h_suffix:I = 0x7f120150

.field public static final material_hour_selection:I = 0x7f120151

.field public static final material_hour_suffix:I = 0x7f120152

.field public static final material_minute_selection:I = 0x7f120153

.field public static final material_minute_suffix:I = 0x7f120154

.field public static final material_motion_easing_accelerated:I = 0x7f120155

.field public static final material_motion_easing_decelerated:I = 0x7f120156

.field public static final material_motion_easing_emphasized:I = 0x7f120157

.field public static final material_motion_easing_linear:I = 0x7f120158

.field public static final material_motion_easing_standard:I = 0x7f120159

.field public static final material_slider_range_end:I = 0x7f12015a

.field public static final material_slider_range_start:I = 0x7f12015b

.field public static final material_slider_value:I = 0x7f12015c

.field public static final material_timepicker_am:I = 0x7f12015d

.field public static final material_timepicker_clock_mode_description:I = 0x7f12015e

.field public static final material_timepicker_hour:I = 0x7f12015f

.field public static final material_timepicker_minute:I = 0x7f120160

.field public static final material_timepicker_pm:I = 0x7f120161

.field public static final material_timepicker_select_time:I = 0x7f120162

.field public static final material_timepicker_text_input_mode_description:I = 0x7f120163

.field public static final mtrl_badge_numberless_content_description:I = 0x7f1201fb

.field public static final mtrl_checkbox_button_icon_path_checked:I = 0x7f1201fc

.field public static final mtrl_checkbox_button_icon_path_group_name:I = 0x7f1201fd

.field public static final mtrl_checkbox_button_icon_path_indeterminate:I = 0x7f1201fe

.field public static final mtrl_checkbox_button_icon_path_name:I = 0x7f1201ff

.field public static final mtrl_checkbox_button_path_checked:I = 0x7f120200

.field public static final mtrl_checkbox_button_path_group_name:I = 0x7f120201

.field public static final mtrl_checkbox_button_path_name:I = 0x7f120202

.field public static final mtrl_checkbox_button_path_unchecked:I = 0x7f120203

.field public static final mtrl_checkbox_state_description_checked:I = 0x7f120204

.field public static final mtrl_checkbox_state_description_indeterminate:I = 0x7f120205

.field public static final mtrl_checkbox_state_description_unchecked:I = 0x7f120206

.field public static final mtrl_chip_close_icon_content_description:I = 0x7f120207

.field public static final mtrl_exceed_max_badge_number_content_description:I = 0x7f120208

.field public static final mtrl_exceed_max_badge_number_suffix:I = 0x7f120209

.field public static final mtrl_picker_a11y_next_month:I = 0x7f12020a

.field public static final mtrl_picker_a11y_prev_month:I = 0x7f12020b

.field public static final mtrl_picker_announce_current_range_selection:I = 0x7f12020c

.field public static final mtrl_picker_announce_current_selection:I = 0x7f12020d

.field public static final mtrl_picker_announce_current_selection_none:I = 0x7f12020e

.field public static final mtrl_picker_cancel:I = 0x7f12020f

.field public static final mtrl_picker_confirm:I = 0x7f120210

.field public static final mtrl_picker_date_header_selected:I = 0x7f120211

.field public static final mtrl_picker_date_header_title:I = 0x7f120212

.field public static final mtrl_picker_date_header_unselected:I = 0x7f120213

.field public static final mtrl_picker_day_of_week_column_header:I = 0x7f120214

.field public static final mtrl_picker_end_date_description:I = 0x7f120215

.field public static final mtrl_picker_invalid_format:I = 0x7f120216

.field public static final mtrl_picker_invalid_format_example:I = 0x7f120217

.field public static final mtrl_picker_invalid_format_use:I = 0x7f120218

.field public static final mtrl_picker_invalid_range:I = 0x7f120219

.field public static final mtrl_picker_navigate_to_current_year_description:I = 0x7f12021a

.field public static final mtrl_picker_navigate_to_year_description:I = 0x7f12021b

.field public static final mtrl_picker_out_of_range:I = 0x7f12021c

.field public static final mtrl_picker_range_header_only_end_selected:I = 0x7f12021d

.field public static final mtrl_picker_range_header_only_start_selected:I = 0x7f12021e

.field public static final mtrl_picker_range_header_selected:I = 0x7f12021f

.field public static final mtrl_picker_range_header_title:I = 0x7f120220

.field public static final mtrl_picker_range_header_unselected:I = 0x7f120221

.field public static final mtrl_picker_save:I = 0x7f120222

.field public static final mtrl_picker_start_date_description:I = 0x7f120223

.field public static final mtrl_picker_text_input_date_hint:I = 0x7f120224

.field public static final mtrl_picker_text_input_date_range_end_hint:I = 0x7f120225

.field public static final mtrl_picker_text_input_date_range_start_hint:I = 0x7f120226

.field public static final mtrl_picker_text_input_day_abbr:I = 0x7f120227

.field public static final mtrl_picker_text_input_month_abbr:I = 0x7f120228

.field public static final mtrl_picker_text_input_year_abbr:I = 0x7f120229

.field public static final mtrl_picker_today_description:I = 0x7f12022a

.field public static final mtrl_picker_toggle_to_calendar_input_mode:I = 0x7f12022b

.field public static final mtrl_picker_toggle_to_day_selection:I = 0x7f12022c

.field public static final mtrl_picker_toggle_to_text_input_mode:I = 0x7f12022d

.field public static final mtrl_picker_toggle_to_year_selection:I = 0x7f12022e

.field public static final mtrl_switch_thumb_group_name:I = 0x7f12022f

.field public static final mtrl_switch_thumb_path_checked:I = 0x7f120230

.field public static final mtrl_switch_thumb_path_morphing:I = 0x7f120231

.field public static final mtrl_switch_thumb_path_name:I = 0x7f120232

.field public static final mtrl_switch_thumb_path_pressed:I = 0x7f120233

.field public static final mtrl_switch_thumb_path_unchecked:I = 0x7f120234

.field public static final mtrl_switch_track_decoration_path:I = 0x7f120235

.field public static final mtrl_switch_track_path:I = 0x7f120236

.field public static final mtrl_timepicker_cancel:I = 0x7f120237

.field public static final mtrl_timepicker_confirm:I = 0x7f120238

.field public static final password_toggle_content_description:I = 0x7f120264

.field public static final path_password_eye:I = 0x7f120265

.field public static final path_password_eye_mask_strike_through:I = 0x7f120266

.field public static final path_password_eye_mask_visible:I = 0x7f120267

.field public static final path_password_strike_through:I = 0x7f120268

.field public static final search_menu_title:I = 0x7f1202b6

.field public static final searchbar_scrolling_view_behavior:I = 0x7f1202b7

.field public static final searchview_clear_text_content_description:I = 0x7f1202b8

.field public static final searchview_navigation_content_description:I = 0x7f1202b9

.field public static final side_sheet_accessibility_pane_title:I = 0x7f1202ed

.field public static final side_sheet_behavior:I = 0x7f1202ee

.field public static final status_bar_notification_info_overflow:I = 0x7f120301


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.class final Lcom/google/android/gms/internal/firebase-auth-api/zzads;
.super Lg51;
.source "SourceFile"


# instance fields
.field private final synthetic zza:Lg51;

.field private final synthetic zzb:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lg51;Ljava/lang/String;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lcom/google/android/gms/internal/firebase-auth-api/zzads;->zza:Lg51;

    iput-object p2, p0, Lcom/google/android/gms/internal/firebase-auth-api/zzads;->zzb:Ljava/lang/String;

    invoke-direct {p0}, Lg51;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCodeAutoRetrievalTimeOut(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/firebase-auth-api/zzads;->zzb:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/gms/internal/firebase-auth-api/zzadt;->zza(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/internal/firebase-auth-api/zzads;->zza:Lg51;

    invoke-virtual {v0, p1}, Lg51;->onCodeAutoRetrievalTimeOut(Ljava/lang/String;)V

    return-void
.end method

.method public final onCodeSent(Ljava/lang/String;Lf51;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/google/android/gms/internal/firebase-auth-api/zzads;->zza:Lg51;

    invoke-virtual {v0, p1, p2}, Lg51;->onCodeSent(Ljava/lang/String;Lf51;)V

    return-void
.end method

.method public final onVerificationCompleted(Le51;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/google/android/gms/internal/firebase-auth-api/zzads;->zzb:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/gms/internal/firebase-auth-api/zzadt;->zza(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/internal/firebase-auth-api/zzads;->zza:Lg51;

    invoke-virtual {v0, p1}, Lg51;->onVerificationCompleted(Le51;)V

    return-void
.end method

.method public final onVerificationFailed(Lcom/google/firebase/FirebaseException;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/firebase-auth-api/zzads;->zzb:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/gms/internal/firebase-auth-api/zzadt;->zza(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/internal/firebase-auth-api/zzads;->zza:Lg51;

    invoke-virtual {v0, p1}, Lg51;->onVerificationFailed(Lcom/google/firebase/FirebaseException;)V

    return-void
.end method

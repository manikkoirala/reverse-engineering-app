.class public Lcom/j256/ormlite/logger/LoggerFactory;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final LOG_TYPE_SYSTEM_PROPERTY:Ljava/lang/String; = "com.j256.simplelogger.backend"

.field private static logBackendFactory:Lcom/j256/ormlite/logger/LogBackendFactory;


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static findLogBackendFactory()Lcom/j256/ormlite/logger/LogBackendFactory;
    .locals 5

    const-string v0, "com.j256.simplelogger.backend"

    invoke-static {v0}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    :try_start_0
    invoke-static {v0}, Lcom/j256/ormlite/logger/LogBackendType;->valueOf(Ljava/lang/String;)Lcom/j256/ormlite/logger/LogBackendType;

    move-result-object v0
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    new-instance v1, Lcom/j256/ormlite/logger/LocalLogBackend;

    const-class v2, Lcom/j256/ormlite/logger/LoggerFactory;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/j256/ormlite/logger/LocalLogBackend;-><init>(Ljava/lang/String;)V

    sget-object v2, Lcom/j256/ormlite/logger/Level;->WARNING:Lcom/j256/ormlite/logger/Level;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Could not find valid log-type from system property \'com.j256.simplelogger.backend\', value \'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "\'"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v2, v0}, Lcom/j256/ormlite/logger/LogBackend;->log(Lcom/j256/ormlite/logger/Level;Ljava/lang/String;)V

    :cond_0
    invoke-static {}, Lcom/j256/ormlite/logger/LogBackendType;->values()[Lcom/j256/ormlite/logger/LogBackendType;

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_2

    aget-object v3, v0, v2

    invoke-virtual {v3}, Lcom/j256/ormlite/logger/LogBackendType;->isAvailable()Z

    move-result v4

    if-eqz v4, :cond_1

    return-object v3

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    sget-object v0, Lcom/j256/ormlite/logger/LogBackendType;->LOCAL:Lcom/j256/ormlite/logger/LogBackendType;

    return-object v0
.end method

.method public static getLogBackendFactory()Lcom/j256/ormlite/logger/LogBackendFactory;
    .locals 1

    sget-object v0, Lcom/j256/ormlite/logger/LoggerFactory;->logBackendFactory:Lcom/j256/ormlite/logger/LogBackendFactory;

    return-object v0
.end method

.method public static getLogger(Ljava/lang/Class;)Lcom/j256/ormlite/logger/Logger;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class<",
            "*>;)",
            "Lcom/j256/ormlite/logger/Logger;"
        }
    .end annotation

    .line 1
    invoke-virtual {p0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Lcom/j256/ormlite/logger/LoggerFactory;->getLogger(Ljava/lang/String;)Lcom/j256/ormlite/logger/Logger;

    move-result-object p0

    return-object p0
.end method

.method public static getLogger(Ljava/lang/String;)Lcom/j256/ormlite/logger/Logger;
    .locals 2

    .line 2
    sget-object v0, Lcom/j256/ormlite/logger/LoggerFactory;->logBackendFactory:Lcom/j256/ormlite/logger/LogBackendFactory;

    if-nez v0, :cond_0

    invoke-static {}, Lcom/j256/ormlite/logger/LoggerFactory;->findLogBackendFactory()Lcom/j256/ormlite/logger/LogBackendFactory;

    move-result-object v0

    sput-object v0, Lcom/j256/ormlite/logger/LoggerFactory;->logBackendFactory:Lcom/j256/ormlite/logger/LogBackendFactory;

    :cond_0
    new-instance v0, Lcom/j256/ormlite/logger/Logger;

    sget-object v1, Lcom/j256/ormlite/logger/LoggerFactory;->logBackendFactory:Lcom/j256/ormlite/logger/LogBackendFactory;

    invoke-interface {v1, p0}, Lcom/j256/ormlite/logger/LogBackendFactory;->createLogBackend(Ljava/lang/String;)Lcom/j256/ormlite/logger/LogBackend;

    move-result-object p0

    invoke-direct {v0, p0}, Lcom/j256/ormlite/logger/Logger;-><init>(Lcom/j256/ormlite/logger/LogBackend;)V

    return-object v0
.end method

.method public static getSimpleClassName(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    const/16 v0, 0x2e

    invoke-virtual {p0, v0}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    if-ltz v0, :cond_1

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p0

    :cond_1
    :goto_0
    return-object p0
.end method

.method public static setLogBackendFactory(Lcom/j256/ormlite/logger/LogBackendFactory;)V
    .locals 0

    sput-object p0, Lcom/j256/ormlite/logger/LoggerFactory;->logBackendFactory:Lcom/j256/ormlite/logger/LogBackendFactory;

    return-void
.end method

.method public static setLogBackendType(Lcom/j256/ormlite/logger/LogBackendType;)V
    .locals 3

    invoke-virtual {p0}, Lcom/j256/ormlite/logger/LogBackendType;->isAvailable()Z

    move-result v0

    if-eqz v0, :cond_0

    sput-object p0, Lcom/j256/ormlite/logger/LoggerFactory;->logBackendFactory:Lcom/j256/ormlite/logger/LogBackendFactory;

    return-void

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Logging backend type "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p0, " is not available on the classpath"

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.class public final enum Lcom/j256/ormlite/logger/LogBackendType;
.super Ljava/lang/Enum;
.source "SourceFile"

# interfaces
.implements Lcom/j256/ormlite/logger/LogBackendFactory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/j256/ormlite/logger/LogBackendType;",
        ">;",
        "Lcom/j256/ormlite/logger/LogBackendFactory;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/j256/ormlite/logger/LogBackendType;

.field public static final enum ANDROID:Lcom/j256/ormlite/logger/LogBackendType;

.field public static final enum COMMONS_LOGGING:Lcom/j256/ormlite/logger/LogBackendType;

.field public static final enum CONSOLE:Lcom/j256/ormlite/logger/LogBackendType;

.field public static final enum JAVA_UTIL:Lcom/j256/ormlite/logger/LogBackendType;

.field public static final enum LOCAL:Lcom/j256/ormlite/logger/LogBackendType;

.field public static final enum LOG4J:Lcom/j256/ormlite/logger/LogBackendType;

.field public static final enum LOG4J2:Lcom/j256/ormlite/logger/LogBackendType;

.field public static final enum LOGBACK:Lcom/j256/ormlite/logger/LogBackendType;

.field public static final enum NULL:Lcom/j256/ormlite/logger/LogBackendType;

.field public static final enum SLF4J:Lcom/j256/ormlite/logger/LogBackendType;


# instance fields
.field private final factory:Lcom/j256/ormlite/logger/LogBackendFactory;


# direct methods
.method public static constructor <clinit>()V
    .locals 13

    new-instance v0, Lcom/j256/ormlite/logger/LogBackendType;

    const/4 v1, 0x0

    const-string v2, "com.j256.ormlite.logger.Slf4jLoggingLogBackend$Slf4jLoggingLogBackendFactory"

    const-string v3, "SLF4J"

    invoke-direct {v0, v3, v1, v2}, Lcom/j256/ormlite/logger/LogBackendType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcom/j256/ormlite/logger/LogBackendType;->SLF4J:Lcom/j256/ormlite/logger/LogBackendType;

    new-instance v1, Lcom/j256/ormlite/logger/LogBackendType;

    const/4 v2, 0x1

    const-string v3, "com.j256.ormlite.android.AndroidLogBackend$AndroidLogBackendFactory"

    const-string v4, "ANDROID"

    invoke-direct {v1, v4, v2, v3}, Lcom/j256/ormlite/logger/LogBackendType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lcom/j256/ormlite/logger/LogBackendType;->ANDROID:Lcom/j256/ormlite/logger/LogBackendType;

    new-instance v2, Lcom/j256/ormlite/logger/LogBackendType;

    const/4 v3, 0x2

    const-string v4, "com.j256.ormlite.logger.LogbackLogBackend$LogbackLogBackendFactory"

    const-string v5, "LOGBACK"

    invoke-direct {v2, v5, v3, v4}, Lcom/j256/ormlite/logger/LogBackendType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v2, Lcom/j256/ormlite/logger/LogBackendType;->LOGBACK:Lcom/j256/ormlite/logger/LogBackendType;

    new-instance v3, Lcom/j256/ormlite/logger/LogBackendType;

    const/4 v4, 0x3

    const-string v5, "com.j256.ormlite.logger.CommonsLoggingLogBackend$CommonsLoggingLogBackendFactory"

    const-string v6, "COMMONS_LOGGING"

    invoke-direct {v3, v6, v4, v5}, Lcom/j256/ormlite/logger/LogBackendType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v3, Lcom/j256/ormlite/logger/LogBackendType;->COMMONS_LOGGING:Lcom/j256/ormlite/logger/LogBackendType;

    new-instance v4, Lcom/j256/ormlite/logger/LogBackendType;

    const/4 v5, 0x4

    const-string v6, "com.j256.ormlite.logger.Log4j2LogBackend$Log4j2LogBackendFactory"

    const-string v7, "LOG4J2"

    invoke-direct {v4, v7, v5, v6}, Lcom/j256/ormlite/logger/LogBackendType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v4, Lcom/j256/ormlite/logger/LogBackendType;->LOG4J2:Lcom/j256/ormlite/logger/LogBackendType;

    new-instance v5, Lcom/j256/ormlite/logger/LogBackendType;

    const/4 v6, 0x5

    const-string v7, "com.j256.ormlite.logger.Log4jLogBackend$Log4jLogBackendFactory"

    const-string v8, "LOG4J"

    invoke-direct {v5, v8, v6, v7}, Lcom/j256/ormlite/logger/LogBackendType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lcom/j256/ormlite/logger/LogBackendType;->LOG4J:Lcom/j256/ormlite/logger/LogBackendType;

    new-instance v6, Lcom/j256/ormlite/logger/LogBackendType;

    new-instance v7, Lcom/j256/ormlite/logger/LocalLogBackend$LocalLogBackendFactory;

    invoke-direct {v7}, Lcom/j256/ormlite/logger/LocalLogBackend$LocalLogBackendFactory;-><init>()V

    const-string v8, "LOCAL"

    const/4 v9, 0x6

    invoke-direct {v6, v8, v9, v7}, Lcom/j256/ormlite/logger/LogBackendType;-><init>(Ljava/lang/String;ILcom/j256/ormlite/logger/LogBackendFactory;)V

    sput-object v6, Lcom/j256/ormlite/logger/LogBackendType;->LOCAL:Lcom/j256/ormlite/logger/LogBackendType;

    new-instance v7, Lcom/j256/ormlite/logger/LogBackendType;

    new-instance v8, Lcom/j256/ormlite/logger/ConsoleLogBackend$ConsoleLogBackendFactory;

    invoke-direct {v8}, Lcom/j256/ormlite/logger/ConsoleLogBackend$ConsoleLogBackendFactory;-><init>()V

    const-string v9, "CONSOLE"

    const/4 v10, 0x7

    invoke-direct {v7, v9, v10, v8}, Lcom/j256/ormlite/logger/LogBackendType;-><init>(Ljava/lang/String;ILcom/j256/ormlite/logger/LogBackendFactory;)V

    sput-object v7, Lcom/j256/ormlite/logger/LogBackendType;->CONSOLE:Lcom/j256/ormlite/logger/LogBackendType;

    new-instance v8, Lcom/j256/ormlite/logger/LogBackendType;

    const/16 v9, 0x8

    const-string v10, "com.j256.ormlite.logger.JavaUtilLogBackend$JavaUtilLogBackendFactory"

    const-string v11, "JAVA_UTIL"

    invoke-direct {v8, v11, v9, v10}, Lcom/j256/ormlite/logger/LogBackendType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v8, Lcom/j256/ormlite/logger/LogBackendType;->JAVA_UTIL:Lcom/j256/ormlite/logger/LogBackendType;

    new-instance v9, Lcom/j256/ormlite/logger/LogBackendType;

    new-instance v10, Lcom/j256/ormlite/logger/NullLogBackend$NullLogBackendFactory;

    invoke-direct {v10}, Lcom/j256/ormlite/logger/NullLogBackend$NullLogBackendFactory;-><init>()V

    const-string v11, "NULL"

    const/16 v12, 0x9

    invoke-direct {v9, v11, v12, v10}, Lcom/j256/ormlite/logger/LogBackendType;-><init>(Ljava/lang/String;ILcom/j256/ormlite/logger/LogBackendFactory;)V

    sput-object v9, Lcom/j256/ormlite/logger/LogBackendType;->NULL:Lcom/j256/ormlite/logger/LogBackendType;

    filled-new-array/range {v0 .. v9}, [Lcom/j256/ormlite/logger/LogBackendType;

    move-result-object v0

    sput-object v0, Lcom/j256/ormlite/logger/LogBackendType;->$VALUES:[Lcom/j256/ormlite/logger/LogBackendType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcom/j256/ormlite/logger/LogBackendFactory;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/j256/ormlite/logger/LogBackendFactory;",
            ")V"
        }
    .end annotation

    .line 1
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lcom/j256/ormlite/logger/LogBackendType;->factory:Lcom/j256/ormlite/logger/LogBackendFactory;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .line 2
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    invoke-direct {p0, p3}, Lcom/j256/ormlite/logger/LogBackendType;->detectFactory(Ljava/lang/String;)Lcom/j256/ormlite/logger/LogBackendFactory;

    move-result-object p1

    iput-object p1, p0, Lcom/j256/ormlite/logger/LogBackendType;->factory:Lcom/j256/ormlite/logger/LogBackendFactory;

    return-void
.end method

.method private detectFactory(Ljava/lang/String;)Lcom/j256/ormlite/logger/LogBackendFactory;
    .locals 5

    :try_start_0
    invoke-static {p1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/j256/ormlite/logger/LogBackendFactory;

    const-string v1, "test"

    invoke-interface {v0, v1}, Lcom/j256/ormlite/logger/LogBackendFactory;->createLogBackend(Ljava/lang/String;)Lcom/j256/ormlite/logger/LogBackend;

    move-result-object v1

    sget-object v2, Lcom/j256/ormlite/logger/Level;->INFO:Lcom/j256/ormlite/logger/Level;

    invoke-interface {v1, v2}, Lcom/j256/ormlite/logger/LogBackend;->isLevelEnabled(Lcom/j256/ormlite/logger/Level;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    :catchall_0
    move-exception v0

    new-instance v1, Lcom/j256/ormlite/logger/LocalLogBackend;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-class v3, Lcom/j256/ormlite/logger/LogBackendType;

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/j256/ormlite/logger/LocalLogBackend;-><init>(Ljava/lang/String;)V

    sget-object v2, Lcom/j256/ormlite/logger/Level;->WARNING:Lcom/j256/ormlite/logger/Level;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unable to get new instance of class "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, ", so had to use local log: "

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v1, v2, p1}, Lcom/j256/ormlite/logger/LogBackend;->log(Lcom/j256/ormlite/logger/Level;Ljava/lang/String;)V

    new-instance p1, Lcom/j256/ormlite/logger/LocalLogBackend$LocalLogBackendFactory;

    invoke-direct {p1}, Lcom/j256/ormlite/logger/LocalLogBackend$LocalLogBackendFactory;-><init>()V

    return-object p1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/j256/ormlite/logger/LogBackendType;
    .locals 1

    const-class v0, Lcom/j256/ormlite/logger/LogBackendType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/j256/ormlite/logger/LogBackendType;

    return-object p0
.end method

.method public static values()[Lcom/j256/ormlite/logger/LogBackendType;
    .locals 1

    sget-object v0, Lcom/j256/ormlite/logger/LogBackendType;->$VALUES:[Lcom/j256/ormlite/logger/LogBackendType;

    invoke-virtual {v0}, [Lcom/j256/ormlite/logger/LogBackendType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/j256/ormlite/logger/LogBackendType;

    return-object v0
.end method


# virtual methods
.method public createLogBackend(Ljava/lang/String;)Lcom/j256/ormlite/logger/LogBackend;
    .locals 1

    iget-object v0, p0, Lcom/j256/ormlite/logger/LogBackendType;->factory:Lcom/j256/ormlite/logger/LogBackendFactory;

    invoke-interface {v0, p1}, Lcom/j256/ormlite/logger/LogBackendFactory;->createLogBackend(Ljava/lang/String;)Lcom/j256/ormlite/logger/LogBackend;

    move-result-object p1

    return-object p1
.end method

.method public isAvailable()Z
    .locals 1

    sget-object v0, Lcom/j256/ormlite/logger/LogBackendType;->LOCAL:Lcom/j256/ormlite/logger/LogBackendType;

    if-eq p0, v0, :cond_1

    sget-object v0, Lcom/j256/ormlite/logger/LogBackendType;->NULL:Lcom/j256/ormlite/logger/LogBackendType;

    if-eq p0, v0, :cond_0

    iget-object v0, p0, Lcom/j256/ormlite/logger/LogBackendType;->factory:Lcom/j256/ormlite/logger/LogBackendFactory;

    instance-of v0, v0, Lcom/j256/ormlite/logger/LocalLogBackend$LocalLogBackendFactory;

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

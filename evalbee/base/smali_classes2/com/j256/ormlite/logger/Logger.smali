.class public Lcom/j256/ormlite/logger/Logger;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final ARG_STRING:Ljava/lang/String; = "{}"

.field private static final ARG_STRING_LENGTH:I = 0x2

.field private static final DEFAULT_FULL_MESSAGE_LENGTH:I = 0x80

.field private static final UNKNOWN_ARG:Ljava/lang/Object;

.field private static globalLevel:Lcom/j256/ormlite/logger/Level;


# instance fields
.field private final backend:Lcom/j256/ormlite/logger/LogBackend;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/j256/ormlite/logger/Logger;->UNKNOWN_ARG:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Lcom/j256/ormlite/logger/LogBackend;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/j256/ormlite/logger/Logger;->backend:Lcom/j256/ormlite/logger/LogBackend;

    return-void
.end method

.method private appendArg(Ljava/lang/StringBuilder;Ljava/lang/Object;)V
    .locals 3

    sget-object v0, Lcom/j256/ormlite/logger/Logger;->UNKNOWN_ARG:Ljava/lang/Object;

    if-ne p2, v0, :cond_0

    goto :goto_2

    :cond_0
    if-nez p2, :cond_1

    const-string p2, "null"

    :goto_0
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    :cond_1
    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->isArray()Z

    move-result v0

    if-eqz v0, :cond_4

    const/16 v0, 0x5b

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-static {p2}, Ljava/lang/reflect/Array;->getLength(Ljava/lang/Object;)I

    move-result v0

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v0, :cond_3

    if-lez v1, :cond_2

    const-string v2, ", "

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    invoke-static {p2, v1}, Ljava/lang/reflect/Array;->get(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v2

    invoke-direct {p0, p1, v2}, Lcom/j256/ormlite/logger/Logger;->appendArg(Ljava/lang/StringBuilder;Ljava/lang/Object;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_3
    const/16 p2, 0x5d

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_2

    :cond_4
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p2

    goto :goto_0

    :goto_2
    return-void
.end method

.method private buildFullMessage(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/String;
    .locals 5

    const/4 v0, 0x0

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    const-string v3, "{}"

    invoke-virtual {p1, v3, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v3

    const/4 v4, -0x1

    if-ne v3, v4, :cond_1

    if-nez v0, :cond_0

    return-object p1

    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result p2

    invoke-virtual {v0, p1, v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;II)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_1
    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v4, 0x80

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    :cond_2
    invoke-virtual {v0, p1, v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;II)Ljava/lang/StringBuilder;

    sget v1, Lcom/j256/ormlite/logger/Logger;->ARG_STRING_LENGTH:I

    add-int/2addr v1, v3

    if-nez p6, :cond_6

    if-nez v2, :cond_3

    invoke-direct {p0, v0, p2}, Lcom/j256/ormlite/logger/Logger;->appendArg(Ljava/lang/StringBuilder;Ljava/lang/Object;)V

    goto :goto_1

    :cond_3
    const/4 v3, 0x1

    if-ne v2, v3, :cond_4

    invoke-direct {p0, v0, p3}, Lcom/j256/ormlite/logger/Logger;->appendArg(Ljava/lang/StringBuilder;Ljava/lang/Object;)V

    goto :goto_1

    :cond_4
    const/4 v3, 0x2

    if-ne v2, v3, :cond_5

    invoke-direct {p0, v0, p4}, Lcom/j256/ormlite/logger/Logger;->appendArg(Ljava/lang/StringBuilder;Ljava/lang/Object;)V

    goto :goto_1

    :cond_5
    const/4 v3, 0x3

    if-ne v2, v3, :cond_7

    invoke-direct {p0, v0, p5}, Lcom/j256/ormlite/logger/Logger;->appendArg(Ljava/lang/StringBuilder;Ljava/lang/Object;)V

    goto :goto_1

    :cond_6
    array-length v3, p6

    if-ge v2, v3, :cond_7

    aget-object v3, p6, v2

    invoke-direct {p0, v0, v3}, Lcom/j256/ormlite/logger/Logger;->appendArg(Ljava/lang/StringBuilder;Ljava/lang/Object;)V

    :cond_7
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method private logIfEnabled(Lcom/j256/ormlite/logger/Level;Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)V
    .locals 7

    sget-object v0, Lcom/j256/ormlite/logger/Logger;->globalLevel:Lcom/j256/ormlite/logger/Level;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lcom/j256/ormlite/logger/Level;->isEnabled(Lcom/j256/ormlite/logger/Level;)Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/j256/ormlite/logger/Logger;->backend:Lcom/j256/ormlite/logger/LogBackend;

    invoke-interface {v0, p1}, Lcom/j256/ormlite/logger/LogBackend;->isLevelEnabled(Lcom/j256/ormlite/logger/Level;)Z

    move-result v0

    if-eqz v0, :cond_2

    move-object v0, p0

    move-object v1, p3

    move-object v2, p4

    move-object v3, p5

    move-object v4, p6

    move-object v5, p7

    move-object v6, p8

    invoke-direct/range {v0 .. v6}, Lcom/j256/ormlite/logger/Logger;->buildFullMessage(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p3

    if-nez p2, :cond_1

    iget-object p2, p0, Lcom/j256/ormlite/logger/Logger;->backend:Lcom/j256/ormlite/logger/LogBackend;

    invoke-interface {p2, p1, p3}, Lcom/j256/ormlite/logger/LogBackend;->log(Lcom/j256/ormlite/logger/Level;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    iget-object p4, p0, Lcom/j256/ormlite/logger/Logger;->backend:Lcom/j256/ormlite/logger/LogBackend;

    invoke-interface {p4, p1, p3, p2}, Lcom/j256/ormlite/logger/LogBackend;->log(Lcom/j256/ormlite/logger/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_2
    :goto_0
    return-void
.end method

.method public static setGlobalLogLevel(Lcom/j256/ormlite/logger/Level;)V
    .locals 0

    sput-object p0, Lcom/j256/ormlite/logger/Logger;->globalLevel:Lcom/j256/ormlite/logger/Level;

    return-void
.end method


# virtual methods
.method public debug(Ljava/lang/String;)V
    .locals 9

    .line 1
    sget-object v1, Lcom/j256/ormlite/logger/Level;->DEBUG:Lcom/j256/ormlite/logger/Level;

    const/4 v2, 0x0

    sget-object v7, Lcom/j256/ormlite/logger/Logger;->UNKNOWN_ARG:Ljava/lang/Object;

    const/4 v8, 0x0

    move-object v0, p0

    move-object v3, p1

    move-object v4, v7

    move-object v5, v7

    move-object v6, v7

    invoke-direct/range {v0 .. v8}, Lcom/j256/ormlite/logger/Logger;->logIfEnabled(Lcom/j256/ormlite/logger/Level;Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)V

    return-void
.end method

.method public debug(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 9

    .line 2
    sget-object v1, Lcom/j256/ormlite/logger/Level;->DEBUG:Lcom/j256/ormlite/logger/Level;

    const/4 v2, 0x0

    sget-object v7, Lcom/j256/ormlite/logger/Logger;->UNKNOWN_ARG:Ljava/lang/Object;

    const/4 v8, 0x0

    move-object v0, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, v7

    move-object v6, v7

    invoke-direct/range {v0 .. v8}, Lcom/j256/ormlite/logger/Logger;->logIfEnabled(Lcom/j256/ormlite/logger/Level;Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)V

    return-void
.end method

.method public debug(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 9

    .line 3
    sget-object v1, Lcom/j256/ormlite/logger/Level;->DEBUG:Lcom/j256/ormlite/logger/Level;

    const/4 v2, 0x0

    sget-object v7, Lcom/j256/ormlite/logger/Logger;->UNKNOWN_ARG:Ljava/lang/Object;

    const/4 v8, 0x0

    move-object v0, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, v7

    invoke-direct/range {v0 .. v8}, Lcom/j256/ormlite/logger/Logger;->logIfEnabled(Lcom/j256/ormlite/logger/Level;Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)V

    return-void
.end method

.method public debug(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 9

    .line 4
    sget-object v1, Lcom/j256/ormlite/logger/Level;->DEBUG:Lcom/j256/ormlite/logger/Level;

    const/4 v2, 0x0

    sget-object v7, Lcom/j256/ormlite/logger/Logger;->UNKNOWN_ARG:Ljava/lang/Object;

    const/4 v8, 0x0

    move-object v0, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    invoke-direct/range {v0 .. v8}, Lcom/j256/ormlite/logger/Logger;->logIfEnabled(Lcom/j256/ormlite/logger/Level;Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)V

    return-void
.end method

.method public debug(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 9

    .line 5
    sget-object v1, Lcom/j256/ormlite/logger/Level;->DEBUG:Lcom/j256/ormlite/logger/Level;

    const/4 v2, 0x0

    const/4 v8, 0x0

    move-object v0, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    move-object v7, p5

    invoke-direct/range {v0 .. v8}, Lcom/j256/ormlite/logger/Logger;->logIfEnabled(Lcom/j256/ormlite/logger/Level;Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)V

    return-void
.end method

.method public debug(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 9

    .line 6
    sget-object v1, Lcom/j256/ormlite/logger/Level;->DEBUG:Lcom/j256/ormlite/logger/Level;

    const/4 v2, 0x0

    sget-object v7, Lcom/j256/ormlite/logger/Logger;->UNKNOWN_ARG:Ljava/lang/Object;

    move-object v0, p0

    move-object v3, p1

    move-object v4, v7

    move-object v5, v7

    move-object v6, v7

    move-object v8, p2

    invoke-direct/range {v0 .. v8}, Lcom/j256/ormlite/logger/Logger;->logIfEnabled(Lcom/j256/ormlite/logger/Level;Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)V

    return-void
.end method

.method public debug(Ljava/lang/Throwable;Ljava/lang/String;)V
    .locals 9

    .line 7
    sget-object v1, Lcom/j256/ormlite/logger/Level;->DEBUG:Lcom/j256/ormlite/logger/Level;

    sget-object v7, Lcom/j256/ormlite/logger/Logger;->UNKNOWN_ARG:Ljava/lang/Object;

    const/4 v8, 0x0

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, v7

    move-object v5, v7

    move-object v6, v7

    invoke-direct/range {v0 .. v8}, Lcom/j256/ormlite/logger/Logger;->logIfEnabled(Lcom/j256/ormlite/logger/Level;Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)V

    return-void
.end method

.method public debug(Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/Object;)V
    .locals 9

    .line 8
    sget-object v1, Lcom/j256/ormlite/logger/Level;->DEBUG:Lcom/j256/ormlite/logger/Level;

    sget-object v7, Lcom/j256/ormlite/logger/Logger;->UNKNOWN_ARG:Ljava/lang/Object;

    const/4 v8, 0x0

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, v7

    move-object v6, v7

    invoke-direct/range {v0 .. v8}, Lcom/j256/ormlite/logger/Logger;->logIfEnabled(Lcom/j256/ormlite/logger/Level;Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)V

    return-void
.end method

.method public debug(Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 9

    .line 9
    sget-object v1, Lcom/j256/ormlite/logger/Level;->DEBUG:Lcom/j256/ormlite/logger/Level;

    sget-object v7, Lcom/j256/ormlite/logger/Logger;->UNKNOWN_ARG:Ljava/lang/Object;

    const/4 v8, 0x0

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, v7

    invoke-direct/range {v0 .. v8}, Lcom/j256/ormlite/logger/Logger;->logIfEnabled(Lcom/j256/ormlite/logger/Level;Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)V

    return-void
.end method

.method public debug(Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 9

    .line 10
    sget-object v1, Lcom/j256/ormlite/logger/Level;->DEBUG:Lcom/j256/ormlite/logger/Level;

    sget-object v7, Lcom/j256/ormlite/logger/Logger;->UNKNOWN_ARG:Ljava/lang/Object;

    const/4 v8, 0x0

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v8}, Lcom/j256/ormlite/logger/Logger;->logIfEnabled(Lcom/j256/ormlite/logger/Level;Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)V

    return-void
.end method

.method public debug(Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 9

    .line 11
    sget-object v1, Lcom/j256/ormlite/logger/Level;->DEBUG:Lcom/j256/ormlite/logger/Level;

    const/4 v8, 0x0

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v8}, Lcom/j256/ormlite/logger/Logger;->logIfEnabled(Lcom/j256/ormlite/logger/Level;Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)V

    return-void
.end method

.method public debug(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 9

    .line 12
    sget-object v1, Lcom/j256/ormlite/logger/Level;->DEBUG:Lcom/j256/ormlite/logger/Level;

    sget-object v7, Lcom/j256/ormlite/logger/Logger;->UNKNOWN_ARG:Ljava/lang/Object;

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, v7

    move-object v5, v7

    move-object v6, v7

    move-object v8, p3

    invoke-direct/range {v0 .. v8}, Lcom/j256/ormlite/logger/Logger;->logIfEnabled(Lcom/j256/ormlite/logger/Level;Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)V

    return-void
.end method

.method public error(Ljava/lang/String;)V
    .locals 9

    .line 1
    sget-object v1, Lcom/j256/ormlite/logger/Level;->ERROR:Lcom/j256/ormlite/logger/Level;

    const/4 v2, 0x0

    sget-object v7, Lcom/j256/ormlite/logger/Logger;->UNKNOWN_ARG:Ljava/lang/Object;

    const/4 v8, 0x0

    move-object v0, p0

    move-object v3, p1

    move-object v4, v7

    move-object v5, v7

    move-object v6, v7

    invoke-direct/range {v0 .. v8}, Lcom/j256/ormlite/logger/Logger;->logIfEnabled(Lcom/j256/ormlite/logger/Level;Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)V

    return-void
.end method

.method public error(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 9

    .line 2
    sget-object v1, Lcom/j256/ormlite/logger/Level;->ERROR:Lcom/j256/ormlite/logger/Level;

    const/4 v2, 0x0

    sget-object v7, Lcom/j256/ormlite/logger/Logger;->UNKNOWN_ARG:Ljava/lang/Object;

    const/4 v8, 0x0

    move-object v0, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, v7

    move-object v6, v7

    invoke-direct/range {v0 .. v8}, Lcom/j256/ormlite/logger/Logger;->logIfEnabled(Lcom/j256/ormlite/logger/Level;Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)V

    return-void
.end method

.method public error(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 9

    .line 3
    sget-object v1, Lcom/j256/ormlite/logger/Level;->ERROR:Lcom/j256/ormlite/logger/Level;

    const/4 v2, 0x0

    sget-object v7, Lcom/j256/ormlite/logger/Logger;->UNKNOWN_ARG:Ljava/lang/Object;

    const/4 v8, 0x0

    move-object v0, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, v7

    invoke-direct/range {v0 .. v8}, Lcom/j256/ormlite/logger/Logger;->logIfEnabled(Lcom/j256/ormlite/logger/Level;Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)V

    return-void
.end method

.method public error(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 9

    .line 4
    sget-object v1, Lcom/j256/ormlite/logger/Level;->ERROR:Lcom/j256/ormlite/logger/Level;

    const/4 v2, 0x0

    sget-object v7, Lcom/j256/ormlite/logger/Logger;->UNKNOWN_ARG:Ljava/lang/Object;

    const/4 v8, 0x0

    move-object v0, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    invoke-direct/range {v0 .. v8}, Lcom/j256/ormlite/logger/Logger;->logIfEnabled(Lcom/j256/ormlite/logger/Level;Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)V

    return-void
.end method

.method public error(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 9

    .line 5
    sget-object v1, Lcom/j256/ormlite/logger/Level;->ERROR:Lcom/j256/ormlite/logger/Level;

    const/4 v2, 0x0

    const/4 v8, 0x0

    move-object v0, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    move-object v7, p5

    invoke-direct/range {v0 .. v8}, Lcom/j256/ormlite/logger/Logger;->logIfEnabled(Lcom/j256/ormlite/logger/Level;Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)V

    return-void
.end method

.method public error(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 9

    .line 6
    sget-object v1, Lcom/j256/ormlite/logger/Level;->ERROR:Lcom/j256/ormlite/logger/Level;

    const/4 v2, 0x0

    sget-object v7, Lcom/j256/ormlite/logger/Logger;->UNKNOWN_ARG:Ljava/lang/Object;

    move-object v0, p0

    move-object v3, p1

    move-object v4, v7

    move-object v5, v7

    move-object v6, v7

    move-object v8, p2

    invoke-direct/range {v0 .. v8}, Lcom/j256/ormlite/logger/Logger;->logIfEnabled(Lcom/j256/ormlite/logger/Level;Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)V

    return-void
.end method

.method public error(Ljava/lang/Throwable;Ljava/lang/String;)V
    .locals 9

    .line 7
    sget-object v1, Lcom/j256/ormlite/logger/Level;->ERROR:Lcom/j256/ormlite/logger/Level;

    sget-object v7, Lcom/j256/ormlite/logger/Logger;->UNKNOWN_ARG:Ljava/lang/Object;

    const/4 v8, 0x0

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, v7

    move-object v5, v7

    move-object v6, v7

    invoke-direct/range {v0 .. v8}, Lcom/j256/ormlite/logger/Logger;->logIfEnabled(Lcom/j256/ormlite/logger/Level;Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)V

    return-void
.end method

.method public error(Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/Object;)V
    .locals 9

    .line 8
    sget-object v1, Lcom/j256/ormlite/logger/Level;->ERROR:Lcom/j256/ormlite/logger/Level;

    sget-object v7, Lcom/j256/ormlite/logger/Logger;->UNKNOWN_ARG:Ljava/lang/Object;

    const/4 v8, 0x0

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, v7

    move-object v6, v7

    invoke-direct/range {v0 .. v8}, Lcom/j256/ormlite/logger/Logger;->logIfEnabled(Lcom/j256/ormlite/logger/Level;Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)V

    return-void
.end method

.method public error(Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 9

    .line 9
    sget-object v1, Lcom/j256/ormlite/logger/Level;->ERROR:Lcom/j256/ormlite/logger/Level;

    sget-object v7, Lcom/j256/ormlite/logger/Logger;->UNKNOWN_ARG:Ljava/lang/Object;

    const/4 v8, 0x0

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, v7

    invoke-direct/range {v0 .. v8}, Lcom/j256/ormlite/logger/Logger;->logIfEnabled(Lcom/j256/ormlite/logger/Level;Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)V

    return-void
.end method

.method public error(Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 9

    .line 10
    sget-object v1, Lcom/j256/ormlite/logger/Level;->ERROR:Lcom/j256/ormlite/logger/Level;

    sget-object v7, Lcom/j256/ormlite/logger/Logger;->UNKNOWN_ARG:Ljava/lang/Object;

    const/4 v8, 0x0

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v8}, Lcom/j256/ormlite/logger/Logger;->logIfEnabled(Lcom/j256/ormlite/logger/Level;Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)V

    return-void
.end method

.method public error(Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 9

    .line 11
    sget-object v1, Lcom/j256/ormlite/logger/Level;->ERROR:Lcom/j256/ormlite/logger/Level;

    const/4 v8, 0x0

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v8}, Lcom/j256/ormlite/logger/Logger;->logIfEnabled(Lcom/j256/ormlite/logger/Level;Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)V

    return-void
.end method

.method public error(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 9

    .line 12
    sget-object v1, Lcom/j256/ormlite/logger/Level;->ERROR:Lcom/j256/ormlite/logger/Level;

    sget-object v7, Lcom/j256/ormlite/logger/Logger;->UNKNOWN_ARG:Ljava/lang/Object;

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, v7

    move-object v5, v7

    move-object v6, v7

    move-object v8, p3

    invoke-direct/range {v0 .. v8}, Lcom/j256/ormlite/logger/Logger;->logIfEnabled(Lcom/j256/ormlite/logger/Level;Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)V

    return-void
.end method

.method public fatal(Ljava/lang/String;)V
    .locals 9

    .line 1
    sget-object v1, Lcom/j256/ormlite/logger/Level;->FATAL:Lcom/j256/ormlite/logger/Level;

    const/4 v2, 0x0

    sget-object v7, Lcom/j256/ormlite/logger/Logger;->UNKNOWN_ARG:Ljava/lang/Object;

    const/4 v8, 0x0

    move-object v0, p0

    move-object v3, p1

    move-object v4, v7

    move-object v5, v7

    move-object v6, v7

    invoke-direct/range {v0 .. v8}, Lcom/j256/ormlite/logger/Logger;->logIfEnabled(Lcom/j256/ormlite/logger/Level;Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)V

    return-void
.end method

.method public fatal(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 9

    .line 2
    sget-object v1, Lcom/j256/ormlite/logger/Level;->FATAL:Lcom/j256/ormlite/logger/Level;

    const/4 v2, 0x0

    sget-object v7, Lcom/j256/ormlite/logger/Logger;->UNKNOWN_ARG:Ljava/lang/Object;

    const/4 v8, 0x0

    move-object v0, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, v7

    move-object v6, v7

    invoke-direct/range {v0 .. v8}, Lcom/j256/ormlite/logger/Logger;->logIfEnabled(Lcom/j256/ormlite/logger/Level;Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)V

    return-void
.end method

.method public fatal(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 9

    .line 3
    sget-object v1, Lcom/j256/ormlite/logger/Level;->FATAL:Lcom/j256/ormlite/logger/Level;

    const/4 v2, 0x0

    sget-object v7, Lcom/j256/ormlite/logger/Logger;->UNKNOWN_ARG:Ljava/lang/Object;

    const/4 v8, 0x0

    move-object v0, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, v7

    invoke-direct/range {v0 .. v8}, Lcom/j256/ormlite/logger/Logger;->logIfEnabled(Lcom/j256/ormlite/logger/Level;Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)V

    return-void
.end method

.method public fatal(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 9

    .line 4
    sget-object v1, Lcom/j256/ormlite/logger/Level;->FATAL:Lcom/j256/ormlite/logger/Level;

    const/4 v2, 0x0

    sget-object v7, Lcom/j256/ormlite/logger/Logger;->UNKNOWN_ARG:Ljava/lang/Object;

    const/4 v8, 0x0

    move-object v0, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    invoke-direct/range {v0 .. v8}, Lcom/j256/ormlite/logger/Logger;->logIfEnabled(Lcom/j256/ormlite/logger/Level;Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)V

    return-void
.end method

.method public fatal(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 9

    .line 5
    sget-object v1, Lcom/j256/ormlite/logger/Level;->FATAL:Lcom/j256/ormlite/logger/Level;

    const/4 v2, 0x0

    const/4 v8, 0x0

    move-object v0, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    move-object v7, p5

    invoke-direct/range {v0 .. v8}, Lcom/j256/ormlite/logger/Logger;->logIfEnabled(Lcom/j256/ormlite/logger/Level;Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)V

    return-void
.end method

.method public fatal(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 9

    .line 6
    sget-object v1, Lcom/j256/ormlite/logger/Level;->FATAL:Lcom/j256/ormlite/logger/Level;

    const/4 v2, 0x0

    sget-object v7, Lcom/j256/ormlite/logger/Logger;->UNKNOWN_ARG:Ljava/lang/Object;

    move-object v0, p0

    move-object v3, p1

    move-object v4, v7

    move-object v5, v7

    move-object v6, v7

    move-object v8, p2

    invoke-direct/range {v0 .. v8}, Lcom/j256/ormlite/logger/Logger;->logIfEnabled(Lcom/j256/ormlite/logger/Level;Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)V

    return-void
.end method

.method public fatal(Ljava/lang/Throwable;Ljava/lang/String;)V
    .locals 9

    .line 7
    sget-object v1, Lcom/j256/ormlite/logger/Level;->FATAL:Lcom/j256/ormlite/logger/Level;

    sget-object v7, Lcom/j256/ormlite/logger/Logger;->UNKNOWN_ARG:Ljava/lang/Object;

    const/4 v8, 0x0

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, v7

    move-object v5, v7

    move-object v6, v7

    invoke-direct/range {v0 .. v8}, Lcom/j256/ormlite/logger/Logger;->logIfEnabled(Lcom/j256/ormlite/logger/Level;Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)V

    return-void
.end method

.method public fatal(Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/Object;)V
    .locals 9

    .line 8
    sget-object v1, Lcom/j256/ormlite/logger/Level;->FATAL:Lcom/j256/ormlite/logger/Level;

    sget-object v7, Lcom/j256/ormlite/logger/Logger;->UNKNOWN_ARG:Ljava/lang/Object;

    const/4 v8, 0x0

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, v7

    move-object v6, v7

    invoke-direct/range {v0 .. v8}, Lcom/j256/ormlite/logger/Logger;->logIfEnabled(Lcom/j256/ormlite/logger/Level;Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)V

    return-void
.end method

.method public fatal(Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 9

    .line 9
    sget-object v1, Lcom/j256/ormlite/logger/Level;->FATAL:Lcom/j256/ormlite/logger/Level;

    sget-object v7, Lcom/j256/ormlite/logger/Logger;->UNKNOWN_ARG:Ljava/lang/Object;

    const/4 v8, 0x0

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, v7

    invoke-direct/range {v0 .. v8}, Lcom/j256/ormlite/logger/Logger;->logIfEnabled(Lcom/j256/ormlite/logger/Level;Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)V

    return-void
.end method

.method public fatal(Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 9

    .line 10
    sget-object v1, Lcom/j256/ormlite/logger/Level;->FATAL:Lcom/j256/ormlite/logger/Level;

    sget-object v7, Lcom/j256/ormlite/logger/Logger;->UNKNOWN_ARG:Ljava/lang/Object;

    const/4 v8, 0x0

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v8}, Lcom/j256/ormlite/logger/Logger;->logIfEnabled(Lcom/j256/ormlite/logger/Level;Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)V

    return-void
.end method

.method public fatal(Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 9

    .line 11
    sget-object v1, Lcom/j256/ormlite/logger/Level;->FATAL:Lcom/j256/ormlite/logger/Level;

    const/4 v8, 0x0

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v8}, Lcom/j256/ormlite/logger/Logger;->logIfEnabled(Lcom/j256/ormlite/logger/Level;Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)V

    return-void
.end method

.method public fatal(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 9

    .line 12
    sget-object v1, Lcom/j256/ormlite/logger/Level;->FATAL:Lcom/j256/ormlite/logger/Level;

    sget-object v7, Lcom/j256/ormlite/logger/Logger;->UNKNOWN_ARG:Ljava/lang/Object;

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, v7

    move-object v5, v7

    move-object v6, v7

    move-object v8, p3

    invoke-direct/range {v0 .. v8}, Lcom/j256/ormlite/logger/Logger;->logIfEnabled(Lcom/j256/ormlite/logger/Level;Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)V

    return-void
.end method

.method public getLogBackend()Lcom/j256/ormlite/logger/LogBackend;
    .locals 1

    iget-object v0, p0, Lcom/j256/ormlite/logger/Logger;->backend:Lcom/j256/ormlite/logger/LogBackend;

    return-object v0
.end method

.method public info(Ljava/lang/String;)V
    .locals 9

    .line 1
    sget-object v1, Lcom/j256/ormlite/logger/Level;->INFO:Lcom/j256/ormlite/logger/Level;

    const/4 v2, 0x0

    sget-object v7, Lcom/j256/ormlite/logger/Logger;->UNKNOWN_ARG:Ljava/lang/Object;

    const/4 v8, 0x0

    move-object v0, p0

    move-object v3, p1

    move-object v4, v7

    move-object v5, v7

    move-object v6, v7

    invoke-direct/range {v0 .. v8}, Lcom/j256/ormlite/logger/Logger;->logIfEnabled(Lcom/j256/ormlite/logger/Level;Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)V

    return-void
.end method

.method public info(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 9

    .line 2
    sget-object v1, Lcom/j256/ormlite/logger/Level;->INFO:Lcom/j256/ormlite/logger/Level;

    const/4 v2, 0x0

    sget-object v7, Lcom/j256/ormlite/logger/Logger;->UNKNOWN_ARG:Ljava/lang/Object;

    const/4 v8, 0x0

    move-object v0, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, v7

    move-object v6, v7

    invoke-direct/range {v0 .. v8}, Lcom/j256/ormlite/logger/Logger;->logIfEnabled(Lcom/j256/ormlite/logger/Level;Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)V

    return-void
.end method

.method public info(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 9

    .line 3
    sget-object v1, Lcom/j256/ormlite/logger/Level;->INFO:Lcom/j256/ormlite/logger/Level;

    const/4 v2, 0x0

    sget-object v7, Lcom/j256/ormlite/logger/Logger;->UNKNOWN_ARG:Ljava/lang/Object;

    const/4 v8, 0x0

    move-object v0, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, v7

    invoke-direct/range {v0 .. v8}, Lcom/j256/ormlite/logger/Logger;->logIfEnabled(Lcom/j256/ormlite/logger/Level;Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)V

    return-void
.end method

.method public info(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 9

    .line 4
    sget-object v1, Lcom/j256/ormlite/logger/Level;->INFO:Lcom/j256/ormlite/logger/Level;

    const/4 v2, 0x0

    sget-object v7, Lcom/j256/ormlite/logger/Logger;->UNKNOWN_ARG:Ljava/lang/Object;

    const/4 v8, 0x0

    move-object v0, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    invoke-direct/range {v0 .. v8}, Lcom/j256/ormlite/logger/Logger;->logIfEnabled(Lcom/j256/ormlite/logger/Level;Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)V

    return-void
.end method

.method public info(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 9

    .line 5
    sget-object v1, Lcom/j256/ormlite/logger/Level;->INFO:Lcom/j256/ormlite/logger/Level;

    const/4 v2, 0x0

    const/4 v8, 0x0

    move-object v0, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    move-object v7, p5

    invoke-direct/range {v0 .. v8}, Lcom/j256/ormlite/logger/Logger;->logIfEnabled(Lcom/j256/ormlite/logger/Level;Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)V

    return-void
.end method

.method public info(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 9

    .line 6
    sget-object v1, Lcom/j256/ormlite/logger/Level;->INFO:Lcom/j256/ormlite/logger/Level;

    const/4 v2, 0x0

    sget-object v7, Lcom/j256/ormlite/logger/Logger;->UNKNOWN_ARG:Ljava/lang/Object;

    move-object v0, p0

    move-object v3, p1

    move-object v4, v7

    move-object v5, v7

    move-object v6, v7

    move-object v8, p2

    invoke-direct/range {v0 .. v8}, Lcom/j256/ormlite/logger/Logger;->logIfEnabled(Lcom/j256/ormlite/logger/Level;Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)V

    return-void
.end method

.method public info(Ljava/lang/Throwable;Ljava/lang/String;)V
    .locals 9

    .line 7
    sget-object v1, Lcom/j256/ormlite/logger/Level;->INFO:Lcom/j256/ormlite/logger/Level;

    sget-object v7, Lcom/j256/ormlite/logger/Logger;->UNKNOWN_ARG:Ljava/lang/Object;

    const/4 v8, 0x0

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, v7

    move-object v5, v7

    move-object v6, v7

    invoke-direct/range {v0 .. v8}, Lcom/j256/ormlite/logger/Logger;->logIfEnabled(Lcom/j256/ormlite/logger/Level;Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)V

    return-void
.end method

.method public info(Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/Object;)V
    .locals 9

    .line 8
    sget-object v1, Lcom/j256/ormlite/logger/Level;->INFO:Lcom/j256/ormlite/logger/Level;

    sget-object v7, Lcom/j256/ormlite/logger/Logger;->UNKNOWN_ARG:Ljava/lang/Object;

    const/4 v8, 0x0

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, v7

    move-object v6, v7

    invoke-direct/range {v0 .. v8}, Lcom/j256/ormlite/logger/Logger;->logIfEnabled(Lcom/j256/ormlite/logger/Level;Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)V

    return-void
.end method

.method public info(Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 9

    .line 9
    sget-object v1, Lcom/j256/ormlite/logger/Level;->INFO:Lcom/j256/ormlite/logger/Level;

    sget-object v7, Lcom/j256/ormlite/logger/Logger;->UNKNOWN_ARG:Ljava/lang/Object;

    const/4 v8, 0x0

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, v7

    invoke-direct/range {v0 .. v8}, Lcom/j256/ormlite/logger/Logger;->logIfEnabled(Lcom/j256/ormlite/logger/Level;Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)V

    return-void
.end method

.method public info(Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 9

    .line 10
    sget-object v1, Lcom/j256/ormlite/logger/Level;->INFO:Lcom/j256/ormlite/logger/Level;

    sget-object v7, Lcom/j256/ormlite/logger/Logger;->UNKNOWN_ARG:Ljava/lang/Object;

    const/4 v8, 0x0

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v8}, Lcom/j256/ormlite/logger/Logger;->logIfEnabled(Lcom/j256/ormlite/logger/Level;Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)V

    return-void
.end method

.method public info(Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 9

    .line 11
    sget-object v1, Lcom/j256/ormlite/logger/Level;->INFO:Lcom/j256/ormlite/logger/Level;

    const/4 v8, 0x0

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v8}, Lcom/j256/ormlite/logger/Logger;->logIfEnabled(Lcom/j256/ormlite/logger/Level;Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)V

    return-void
.end method

.method public info(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 9

    .line 12
    sget-object v1, Lcom/j256/ormlite/logger/Level;->INFO:Lcom/j256/ormlite/logger/Level;

    sget-object v7, Lcom/j256/ormlite/logger/Logger;->UNKNOWN_ARG:Ljava/lang/Object;

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, v7

    move-object v5, v7

    move-object v6, v7

    move-object v8, p3

    invoke-direct/range {v0 .. v8}, Lcom/j256/ormlite/logger/Logger;->logIfEnabled(Lcom/j256/ormlite/logger/Level;Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)V

    return-void
.end method

.method public isLevelEnabled(Lcom/j256/ormlite/logger/Level;)Z
    .locals 1

    iget-object v0, p0, Lcom/j256/ormlite/logger/Logger;->backend:Lcom/j256/ormlite/logger/LogBackend;

    invoke-interface {v0, p1}, Lcom/j256/ormlite/logger/LogBackend;->isLevelEnabled(Lcom/j256/ormlite/logger/Level;)Z

    move-result p1

    return p1
.end method

.method public log(Lcom/j256/ormlite/logger/Level;Ljava/lang/String;)V
    .locals 9

    .line 1
    const/4 v2, 0x0

    sget-object v7, Lcom/j256/ormlite/logger/Logger;->UNKNOWN_ARG:Ljava/lang/Object;

    const/4 v8, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-object v4, v7

    move-object v5, v7

    move-object v6, v7

    invoke-direct/range {v0 .. v8}, Lcom/j256/ormlite/logger/Logger;->logIfEnabled(Lcom/j256/ormlite/logger/Level;Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)V

    return-void
.end method

.method public log(Lcom/j256/ormlite/logger/Level;Ljava/lang/String;Ljava/lang/Object;)V
    .locals 9

    .line 2
    const/4 v2, 0x0

    sget-object v7, Lcom/j256/ormlite/logger/Logger;->UNKNOWN_ARG:Ljava/lang/Object;

    const/4 v8, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, v7

    move-object v6, v7

    invoke-direct/range {v0 .. v8}, Lcom/j256/ormlite/logger/Logger;->logIfEnabled(Lcom/j256/ormlite/logger/Level;Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)V

    return-void
.end method

.method public log(Lcom/j256/ormlite/logger/Level;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 9

    .line 3
    const/4 v2, 0x0

    sget-object v7, Lcom/j256/ormlite/logger/Logger;->UNKNOWN_ARG:Ljava/lang/Object;

    const/4 v8, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, v7

    invoke-direct/range {v0 .. v8}, Lcom/j256/ormlite/logger/Logger;->logIfEnabled(Lcom/j256/ormlite/logger/Level;Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)V

    return-void
.end method

.method public log(Lcom/j256/ormlite/logger/Level;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 9

    .line 4
    const/4 v2, 0x0

    sget-object v7, Lcom/j256/ormlite/logger/Logger;->UNKNOWN_ARG:Ljava/lang/Object;

    const/4 v8, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v8}, Lcom/j256/ormlite/logger/Logger;->logIfEnabled(Lcom/j256/ormlite/logger/Level;Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)V

    return-void
.end method

.method public log(Lcom/j256/ormlite/logger/Level;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 9

    .line 5
    const/4 v2, 0x0

    const/4 v8, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v8}, Lcom/j256/ormlite/logger/Logger;->logIfEnabled(Lcom/j256/ormlite/logger/Level;Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)V

    return-void
.end method

.method public log(Lcom/j256/ormlite/logger/Level;Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 9

    .line 6
    const/4 v2, 0x0

    sget-object v7, Lcom/j256/ormlite/logger/Logger;->UNKNOWN_ARG:Ljava/lang/Object;

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-object v4, v7

    move-object v5, v7

    move-object v6, v7

    move-object v8, p3

    invoke-direct/range {v0 .. v8}, Lcom/j256/ormlite/logger/Logger;->logIfEnabled(Lcom/j256/ormlite/logger/Level;Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)V

    return-void
.end method

.method public log(Lcom/j256/ormlite/logger/Level;Ljava/lang/Throwable;Ljava/lang/String;)V
    .locals 9

    .line 7
    sget-object v7, Lcom/j256/ormlite/logger/Logger;->UNKNOWN_ARG:Ljava/lang/Object;

    const/4 v8, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, v7

    move-object v5, v7

    move-object v6, v7

    invoke-direct/range {v0 .. v8}, Lcom/j256/ormlite/logger/Logger;->logIfEnabled(Lcom/j256/ormlite/logger/Level;Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)V

    return-void
.end method

.method public log(Lcom/j256/ormlite/logger/Level;Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/Object;)V
    .locals 9

    .line 8
    sget-object v7, Lcom/j256/ormlite/logger/Logger;->UNKNOWN_ARG:Ljava/lang/Object;

    const/4 v8, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, v7

    move-object v6, v7

    invoke-direct/range {v0 .. v8}, Lcom/j256/ormlite/logger/Logger;->logIfEnabled(Lcom/j256/ormlite/logger/Level;Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)V

    return-void
.end method

.method public log(Lcom/j256/ormlite/logger/Level;Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 9

    .line 9
    sget-object v7, Lcom/j256/ormlite/logger/Logger;->UNKNOWN_ARG:Ljava/lang/Object;

    const/4 v8, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, v7

    invoke-direct/range {v0 .. v8}, Lcom/j256/ormlite/logger/Logger;->logIfEnabled(Lcom/j256/ormlite/logger/Level;Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)V

    return-void
.end method

.method public log(Lcom/j256/ormlite/logger/Level;Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 9

    .line 10
    sget-object v7, Lcom/j256/ormlite/logger/Logger;->UNKNOWN_ARG:Ljava/lang/Object;

    const/4 v8, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v8}, Lcom/j256/ormlite/logger/Logger;->logIfEnabled(Lcom/j256/ormlite/logger/Level;Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)V

    return-void
.end method

.method public log(Lcom/j256/ormlite/logger/Level;Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 9

    .line 11
    const/4 v8, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    move-object/from16 v7, p7

    invoke-direct/range {v0 .. v8}, Lcom/j256/ormlite/logger/Logger;->logIfEnabled(Lcom/j256/ormlite/logger/Level;Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)V

    return-void
.end method

.method public log(Lcom/j256/ormlite/logger/Level;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 9

    .line 12
    sget-object v7, Lcom/j256/ormlite/logger/Logger;->UNKNOWN_ARG:Ljava/lang/Object;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, v7

    move-object v5, v7

    move-object v6, v7

    move-object v8, p4

    invoke-direct/range {v0 .. v8}, Lcom/j256/ormlite/logger/Logger;->logIfEnabled(Lcom/j256/ormlite/logger/Level;Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)V

    return-void
.end method

.method public trace(Ljava/lang/String;)V
    .locals 9

    .line 1
    sget-object v1, Lcom/j256/ormlite/logger/Level;->TRACE:Lcom/j256/ormlite/logger/Level;

    const/4 v2, 0x0

    sget-object v7, Lcom/j256/ormlite/logger/Logger;->UNKNOWN_ARG:Ljava/lang/Object;

    const/4 v8, 0x0

    move-object v0, p0

    move-object v3, p1

    move-object v4, v7

    move-object v5, v7

    move-object v6, v7

    invoke-direct/range {v0 .. v8}, Lcom/j256/ormlite/logger/Logger;->logIfEnabled(Lcom/j256/ormlite/logger/Level;Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)V

    return-void
.end method

.method public trace(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 9

    .line 2
    sget-object v1, Lcom/j256/ormlite/logger/Level;->TRACE:Lcom/j256/ormlite/logger/Level;

    const/4 v2, 0x0

    sget-object v7, Lcom/j256/ormlite/logger/Logger;->UNKNOWN_ARG:Ljava/lang/Object;

    const/4 v8, 0x0

    move-object v0, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, v7

    move-object v6, v7

    invoke-direct/range {v0 .. v8}, Lcom/j256/ormlite/logger/Logger;->logIfEnabled(Lcom/j256/ormlite/logger/Level;Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)V

    return-void
.end method

.method public trace(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 9

    .line 3
    sget-object v1, Lcom/j256/ormlite/logger/Level;->TRACE:Lcom/j256/ormlite/logger/Level;

    const/4 v2, 0x0

    sget-object v7, Lcom/j256/ormlite/logger/Logger;->UNKNOWN_ARG:Ljava/lang/Object;

    const/4 v8, 0x0

    move-object v0, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, v7

    invoke-direct/range {v0 .. v8}, Lcom/j256/ormlite/logger/Logger;->logIfEnabled(Lcom/j256/ormlite/logger/Level;Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)V

    return-void
.end method

.method public trace(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 9

    .line 4
    sget-object v1, Lcom/j256/ormlite/logger/Level;->TRACE:Lcom/j256/ormlite/logger/Level;

    const/4 v2, 0x0

    sget-object v7, Lcom/j256/ormlite/logger/Logger;->UNKNOWN_ARG:Ljava/lang/Object;

    const/4 v8, 0x0

    move-object v0, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    invoke-direct/range {v0 .. v8}, Lcom/j256/ormlite/logger/Logger;->logIfEnabled(Lcom/j256/ormlite/logger/Level;Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)V

    return-void
.end method

.method public trace(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 9

    .line 5
    sget-object v1, Lcom/j256/ormlite/logger/Level;->TRACE:Lcom/j256/ormlite/logger/Level;

    const/4 v2, 0x0

    const/4 v8, 0x0

    move-object v0, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    move-object v7, p5

    invoke-direct/range {v0 .. v8}, Lcom/j256/ormlite/logger/Logger;->logIfEnabled(Lcom/j256/ormlite/logger/Level;Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)V

    return-void
.end method

.method public trace(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 9

    .line 6
    sget-object v1, Lcom/j256/ormlite/logger/Level;->TRACE:Lcom/j256/ormlite/logger/Level;

    const/4 v2, 0x0

    sget-object v7, Lcom/j256/ormlite/logger/Logger;->UNKNOWN_ARG:Ljava/lang/Object;

    move-object v0, p0

    move-object v3, p1

    move-object v4, v7

    move-object v5, v7

    move-object v6, v7

    move-object v8, p2

    invoke-direct/range {v0 .. v8}, Lcom/j256/ormlite/logger/Logger;->logIfEnabled(Lcom/j256/ormlite/logger/Level;Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)V

    return-void
.end method

.method public trace(Ljava/lang/Throwable;Ljava/lang/String;)V
    .locals 9

    .line 7
    sget-object v1, Lcom/j256/ormlite/logger/Level;->TRACE:Lcom/j256/ormlite/logger/Level;

    sget-object v6, Lcom/j256/ormlite/logger/Logger;->UNKNOWN_ARG:Ljava/lang/Object;

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, v6

    move-object v5, v6

    invoke-direct/range {v0 .. v8}, Lcom/j256/ormlite/logger/Logger;->logIfEnabled(Lcom/j256/ormlite/logger/Level;Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)V

    return-void
.end method

.method public trace(Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/Object;)V
    .locals 9

    .line 8
    sget-object v1, Lcom/j256/ormlite/logger/Level;->TRACE:Lcom/j256/ormlite/logger/Level;

    sget-object v7, Lcom/j256/ormlite/logger/Logger;->UNKNOWN_ARG:Ljava/lang/Object;

    const/4 v8, 0x0

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, v7

    move-object v6, v7

    invoke-direct/range {v0 .. v8}, Lcom/j256/ormlite/logger/Logger;->logIfEnabled(Lcom/j256/ormlite/logger/Level;Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)V

    return-void
.end method

.method public trace(Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 9

    .line 9
    sget-object v1, Lcom/j256/ormlite/logger/Level;->TRACE:Lcom/j256/ormlite/logger/Level;

    sget-object v7, Lcom/j256/ormlite/logger/Logger;->UNKNOWN_ARG:Ljava/lang/Object;

    const/4 v8, 0x0

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, v7

    invoke-direct/range {v0 .. v8}, Lcom/j256/ormlite/logger/Logger;->logIfEnabled(Lcom/j256/ormlite/logger/Level;Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)V

    return-void
.end method

.method public trace(Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 9

    .line 10
    sget-object v1, Lcom/j256/ormlite/logger/Level;->TRACE:Lcom/j256/ormlite/logger/Level;

    sget-object v7, Lcom/j256/ormlite/logger/Logger;->UNKNOWN_ARG:Ljava/lang/Object;

    const/4 v8, 0x0

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v8}, Lcom/j256/ormlite/logger/Logger;->logIfEnabled(Lcom/j256/ormlite/logger/Level;Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)V

    return-void
.end method

.method public trace(Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 9

    .line 11
    sget-object v1, Lcom/j256/ormlite/logger/Level;->TRACE:Lcom/j256/ormlite/logger/Level;

    const/4 v8, 0x0

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v8}, Lcom/j256/ormlite/logger/Logger;->logIfEnabled(Lcom/j256/ormlite/logger/Level;Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)V

    return-void
.end method

.method public trace(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 9

    .line 12
    sget-object v1, Lcom/j256/ormlite/logger/Level;->TRACE:Lcom/j256/ormlite/logger/Level;

    sget-object v7, Lcom/j256/ormlite/logger/Logger;->UNKNOWN_ARG:Ljava/lang/Object;

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, v7

    move-object v5, v7

    move-object v6, v7

    move-object v8, p3

    invoke-direct/range {v0 .. v8}, Lcom/j256/ormlite/logger/Logger;->logIfEnabled(Lcom/j256/ormlite/logger/Level;Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)V

    return-void
.end method

.method public warn(Ljava/lang/String;)V
    .locals 9

    .line 1
    sget-object v1, Lcom/j256/ormlite/logger/Level;->WARNING:Lcom/j256/ormlite/logger/Level;

    const/4 v2, 0x0

    sget-object v7, Lcom/j256/ormlite/logger/Logger;->UNKNOWN_ARG:Ljava/lang/Object;

    const/4 v8, 0x0

    move-object v0, p0

    move-object v3, p1

    move-object v4, v7

    move-object v5, v7

    move-object v6, v7

    invoke-direct/range {v0 .. v8}, Lcom/j256/ormlite/logger/Logger;->logIfEnabled(Lcom/j256/ormlite/logger/Level;Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)V

    return-void
.end method

.method public warn(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 9

    .line 2
    sget-object v1, Lcom/j256/ormlite/logger/Level;->WARNING:Lcom/j256/ormlite/logger/Level;

    const/4 v2, 0x0

    sget-object v7, Lcom/j256/ormlite/logger/Logger;->UNKNOWN_ARG:Ljava/lang/Object;

    const/4 v8, 0x0

    move-object v0, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, v7

    move-object v6, v7

    invoke-direct/range {v0 .. v8}, Lcom/j256/ormlite/logger/Logger;->logIfEnabled(Lcom/j256/ormlite/logger/Level;Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)V

    return-void
.end method

.method public warn(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 9

    .line 3
    sget-object v1, Lcom/j256/ormlite/logger/Level;->WARNING:Lcom/j256/ormlite/logger/Level;

    const/4 v2, 0x0

    sget-object v7, Lcom/j256/ormlite/logger/Logger;->UNKNOWN_ARG:Ljava/lang/Object;

    const/4 v8, 0x0

    move-object v0, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, v7

    invoke-direct/range {v0 .. v8}, Lcom/j256/ormlite/logger/Logger;->logIfEnabled(Lcom/j256/ormlite/logger/Level;Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)V

    return-void
.end method

.method public warn(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 9

    .line 4
    sget-object v1, Lcom/j256/ormlite/logger/Level;->WARNING:Lcom/j256/ormlite/logger/Level;

    const/4 v2, 0x0

    sget-object v7, Lcom/j256/ormlite/logger/Logger;->UNKNOWN_ARG:Ljava/lang/Object;

    const/4 v8, 0x0

    move-object v0, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    invoke-direct/range {v0 .. v8}, Lcom/j256/ormlite/logger/Logger;->logIfEnabled(Lcom/j256/ormlite/logger/Level;Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)V

    return-void
.end method

.method public warn(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 9

    .line 5
    sget-object v1, Lcom/j256/ormlite/logger/Level;->WARNING:Lcom/j256/ormlite/logger/Level;

    const/4 v2, 0x0

    const/4 v8, 0x0

    move-object v0, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-object v6, p4

    move-object v7, p5

    invoke-direct/range {v0 .. v8}, Lcom/j256/ormlite/logger/Logger;->logIfEnabled(Lcom/j256/ormlite/logger/Level;Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)V

    return-void
.end method

.method public warn(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 9

    .line 6
    sget-object v1, Lcom/j256/ormlite/logger/Level;->WARNING:Lcom/j256/ormlite/logger/Level;

    const/4 v2, 0x0

    sget-object v7, Lcom/j256/ormlite/logger/Logger;->UNKNOWN_ARG:Ljava/lang/Object;

    move-object v0, p0

    move-object v3, p1

    move-object v4, v7

    move-object v5, v7

    move-object v6, v7

    move-object v8, p2

    invoke-direct/range {v0 .. v8}, Lcom/j256/ormlite/logger/Logger;->logIfEnabled(Lcom/j256/ormlite/logger/Level;Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)V

    return-void
.end method

.method public warn(Ljava/lang/Throwable;Ljava/lang/String;)V
    .locals 9

    .line 7
    sget-object v1, Lcom/j256/ormlite/logger/Level;->WARNING:Lcom/j256/ormlite/logger/Level;

    sget-object v7, Lcom/j256/ormlite/logger/Logger;->UNKNOWN_ARG:Ljava/lang/Object;

    const/4 v8, 0x0

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, v7

    move-object v5, v7

    move-object v6, v7

    invoke-direct/range {v0 .. v8}, Lcom/j256/ormlite/logger/Logger;->logIfEnabled(Lcom/j256/ormlite/logger/Level;Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)V

    return-void
.end method

.method public warn(Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/Object;)V
    .locals 9

    .line 8
    sget-object v1, Lcom/j256/ormlite/logger/Level;->WARNING:Lcom/j256/ormlite/logger/Level;

    sget-object v7, Lcom/j256/ormlite/logger/Logger;->UNKNOWN_ARG:Ljava/lang/Object;

    const/4 v8, 0x0

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, v7

    move-object v6, v7

    invoke-direct/range {v0 .. v8}, Lcom/j256/ormlite/logger/Logger;->logIfEnabled(Lcom/j256/ormlite/logger/Level;Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)V

    return-void
.end method

.method public warn(Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 9

    .line 9
    sget-object v1, Lcom/j256/ormlite/logger/Level;->WARNING:Lcom/j256/ormlite/logger/Level;

    sget-object v7, Lcom/j256/ormlite/logger/Logger;->UNKNOWN_ARG:Ljava/lang/Object;

    const/4 v8, 0x0

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, v7

    invoke-direct/range {v0 .. v8}, Lcom/j256/ormlite/logger/Logger;->logIfEnabled(Lcom/j256/ormlite/logger/Level;Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)V

    return-void
.end method

.method public warn(Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 9

    .line 10
    sget-object v1, Lcom/j256/ormlite/logger/Level;->WARNING:Lcom/j256/ormlite/logger/Level;

    sget-object v7, Lcom/j256/ormlite/logger/Logger;->UNKNOWN_ARG:Ljava/lang/Object;

    const/4 v8, 0x0

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v8}, Lcom/j256/ormlite/logger/Logger;->logIfEnabled(Lcom/j256/ormlite/logger/Level;Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)V

    return-void
.end method

.method public warn(Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 9

    .line 11
    sget-object v1, Lcom/j256/ormlite/logger/Level;->WARNING:Lcom/j256/ormlite/logger/Level;

    const/4 v8, 0x0

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v8}, Lcom/j256/ormlite/logger/Logger;->logIfEnabled(Lcom/j256/ormlite/logger/Level;Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)V

    return-void
.end method

.method public warn(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 9

    .line 12
    sget-object v1, Lcom/j256/ormlite/logger/Level;->WARNING:Lcom/j256/ormlite/logger/Level;

    sget-object v7, Lcom/j256/ormlite/logger/Logger;->UNKNOWN_ARG:Ljava/lang/Object;

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, v7

    move-object v5, v7

    move-object v6, v7

    move-object v8, p3

    invoke-direct/range {v0 .. v8}, Lcom/j256/ormlite/logger/Logger;->logIfEnabled(Lcom/j256/ormlite/logger/Level;Ljava/lang/Throwable;Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;Ljava/lang/Object;[Ljava/lang/Object;)V

    return-void
.end method

.class public final enum Lcom/j256/ormlite/logger/Level;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lcom/j256/ormlite/logger/Level;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/j256/ormlite/logger/Level;

.field public static final enum DEBUG:Lcom/j256/ormlite/logger/Level;

.field public static final enum ERROR:Lcom/j256/ormlite/logger/Level;

.field public static final enum FATAL:Lcom/j256/ormlite/logger/Level;

.field public static final enum INFO:Lcom/j256/ormlite/logger/Level;

.field public static final enum OFF:Lcom/j256/ormlite/logger/Level;

.field public static final enum TRACE:Lcom/j256/ormlite/logger/Level;

.field public static final enum WARNING:Lcom/j256/ormlite/logger/Level;


# instance fields
.field private final value:I


# direct methods
.method public static constructor <clinit>()V
    .locals 10

    new-instance v0, Lcom/j256/ormlite/logger/Level;

    const-string v1, "TRACE"

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, Lcom/j256/ormlite/logger/Level;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/j256/ormlite/logger/Level;->TRACE:Lcom/j256/ormlite/logger/Level;

    new-instance v1, Lcom/j256/ormlite/logger/Level;

    const-string v2, "DEBUG"

    const/4 v4, 0x2

    invoke-direct {v1, v2, v3, v4}, Lcom/j256/ormlite/logger/Level;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lcom/j256/ormlite/logger/Level;->DEBUG:Lcom/j256/ormlite/logger/Level;

    new-instance v2, Lcom/j256/ormlite/logger/Level;

    const-string v3, "INFO"

    const/4 v5, 0x3

    invoke-direct {v2, v3, v4, v5}, Lcom/j256/ormlite/logger/Level;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lcom/j256/ormlite/logger/Level;->INFO:Lcom/j256/ormlite/logger/Level;

    new-instance v3, Lcom/j256/ormlite/logger/Level;

    const-string v4, "WARNING"

    const/4 v6, 0x4

    invoke-direct {v3, v4, v5, v6}, Lcom/j256/ormlite/logger/Level;-><init>(Ljava/lang/String;II)V

    sput-object v3, Lcom/j256/ormlite/logger/Level;->WARNING:Lcom/j256/ormlite/logger/Level;

    new-instance v4, Lcom/j256/ormlite/logger/Level;

    const-string v5, "ERROR"

    const/4 v7, 0x5

    invoke-direct {v4, v5, v6, v7}, Lcom/j256/ormlite/logger/Level;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lcom/j256/ormlite/logger/Level;->ERROR:Lcom/j256/ormlite/logger/Level;

    new-instance v5, Lcom/j256/ormlite/logger/Level;

    const-string v6, "FATAL"

    const/4 v8, 0x6

    invoke-direct {v5, v6, v7, v8}, Lcom/j256/ormlite/logger/Level;-><init>(Ljava/lang/String;II)V

    sput-object v5, Lcom/j256/ormlite/logger/Level;->FATAL:Lcom/j256/ormlite/logger/Level;

    new-instance v6, Lcom/j256/ormlite/logger/Level;

    const-string v7, "OFF"

    const/4 v9, 0x7

    invoke-direct {v6, v7, v8, v9}, Lcom/j256/ormlite/logger/Level;-><init>(Ljava/lang/String;II)V

    sput-object v6, Lcom/j256/ormlite/logger/Level;->OFF:Lcom/j256/ormlite/logger/Level;

    filled-new-array/range {v0 .. v6}, [Lcom/j256/ormlite/logger/Level;

    move-result-object v0

    sput-object v0, Lcom/j256/ormlite/logger/Level;->$VALUES:[Lcom/j256/ormlite/logger/Level;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/j256/ormlite/logger/Level;->value:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/j256/ormlite/logger/Level;
    .locals 1

    const-class v0, Lcom/j256/ormlite/logger/Level;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lcom/j256/ormlite/logger/Level;

    return-object p0
.end method

.method public static values()[Lcom/j256/ormlite/logger/Level;
    .locals 1

    sget-object v0, Lcom/j256/ormlite/logger/Level;->$VALUES:[Lcom/j256/ormlite/logger/Level;

    invoke-virtual {v0}, [Lcom/j256/ormlite/logger/Level;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/j256/ormlite/logger/Level;

    return-object v0
.end method


# virtual methods
.method public isEnabled(Lcom/j256/ormlite/logger/Level;)Z
    .locals 1

    sget-object v0, Lcom/j256/ormlite/logger/Level;->OFF:Lcom/j256/ormlite/logger/Level;

    if-eq p0, v0, :cond_0

    if-eq p1, v0, :cond_0

    iget v0, p0, Lcom/j256/ormlite/logger/Level;->value:I

    iget p1, p1, Lcom/j256/ormlite/logger/Level;->value:I

    if-gt v0, p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

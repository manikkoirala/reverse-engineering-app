.class public Lcom/j256/ormlite/logger/LogbackLogBackend$LogbackLogBackendFactory;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/j256/ormlite/logger/LogBackendFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/j256/ormlite/logger/LogbackLogBackend;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "LogbackLogBackendFactory"
.end annotation


# instance fields
.field private final factory:Lorg/slf4j/ILoggerFactory;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Lorg/slf4j/impl/StaticLoggerBinder;->getSingleton()Lorg/slf4j/impl/StaticLoggerBinder;

    move-result-object v0

    invoke-virtual {v0}, Lorg/slf4j/impl/StaticLoggerBinder;->getLoggerFactory()Lorg/slf4j/ILoggerFactory;

    move-result-object v0

    iput-object v0, p0, Lcom/j256/ormlite/logger/LogbackLogBackend$LogbackLogBackendFactory;->factory:Lorg/slf4j/ILoggerFactory;

    return-void
.end method


# virtual methods
.method public createLogBackend(Ljava/lang/String;)Lcom/j256/ormlite/logger/LogBackend;
    .locals 2

    new-instance v0, Lcom/j256/ormlite/logger/LogbackLogBackend;

    iget-object v1, p0, Lcom/j256/ormlite/logger/LogbackLogBackend$LogbackLogBackendFactory;->factory:Lorg/slf4j/ILoggerFactory;

    invoke-interface {v1, p1}, Lorg/slf4j/ILoggerFactory;->getLogger(Ljava/lang/String;)Lorg/slf4j/Logger;

    move-result-object p1

    invoke-direct {v0, p1}, Lcom/j256/ormlite/logger/LogbackLogBackend;-><init>(Lorg/slf4j/Logger;)V

    return-object v0
.end method

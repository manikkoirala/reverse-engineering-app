.class public Lcom/j256/ormlite/logger/Log4jLogBackend;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/j256/ormlite/logger/LogBackend;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/j256/ormlite/logger/Log4jLogBackend$Log4jLogBackendFactory;
    }
.end annotation


# instance fields
.field private final logger:Lorg/apache/log4j/Logger;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lorg/apache/log4j/Logger;->getLogger(Ljava/lang/String;)Lorg/apache/log4j/Logger;

    move-result-object p1

    iput-object p1, p0, Lcom/j256/ormlite/logger/Log4jLogBackend;->logger:Lorg/apache/log4j/Logger;

    return-void
.end method

.method private levelToLog4jLevel(Lcom/j256/ormlite/logger/Level;)Lorg/apache/log4j/Level;
    .locals 1

    sget-object v0, Lcom/j256/ormlite/logger/Log4jLogBackend$1;->$SwitchMap$com$j256$ormlite$logger$Level:[I

    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_4

    const/4 v0, 0x2

    if-eq p1, v0, :cond_3

    const/4 v0, 0x3

    if-eq p1, v0, :cond_2

    const/4 v0, 0x4

    if-eq p1, v0, :cond_1

    const/4 v0, 0x5

    if-eq p1, v0, :cond_0

    sget-object p1, Lorg/apache/log4j/Level;->INFO:Lorg/apache/log4j/Level;

    return-object p1

    :cond_0
    sget-object p1, Lorg/apache/log4j/Level;->FATAL:Lorg/apache/log4j/Level;

    return-object p1

    :cond_1
    sget-object p1, Lorg/apache/log4j/Level;->ERROR:Lorg/apache/log4j/Level;

    return-object p1

    :cond_2
    sget-object p1, Lorg/apache/log4j/Level;->WARN:Lorg/apache/log4j/Level;

    return-object p1

    :cond_3
    sget-object p1, Lorg/apache/log4j/Level;->DEBUG:Lorg/apache/log4j/Level;

    return-object p1

    :cond_4
    sget-object p1, Lorg/apache/log4j/Level;->TRACE:Lorg/apache/log4j/Level;

    return-object p1
.end method


# virtual methods
.method public isLevelEnabled(Lcom/j256/ormlite/logger/Level;)Z
    .locals 1

    iget-object v0, p0, Lcom/j256/ormlite/logger/Log4jLogBackend;->logger:Lorg/apache/log4j/Logger;

    invoke-direct {p0, p1}, Lcom/j256/ormlite/logger/Log4jLogBackend;->levelToLog4jLevel(Lcom/j256/ormlite/logger/Level;)Lorg/apache/log4j/Level;

    move-result-object p1

    invoke-virtual {v0, p1}, Lorg/apache/log4j/Logger;->isEnabledFor(Lorg/apache/log4j/Priority;)Z

    move-result p1

    return p1
.end method

.method public log(Lcom/j256/ormlite/logger/Level;Ljava/lang/String;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/j256/ormlite/logger/Log4jLogBackend;->logger:Lorg/apache/log4j/Logger;

    invoke-direct {p0, p1}, Lcom/j256/ormlite/logger/Log4jLogBackend;->levelToLog4jLevel(Lcom/j256/ormlite/logger/Level;)Lorg/apache/log4j/Level;

    move-result-object p1

    invoke-virtual {v0, p1, p2}, Lorg/apache/log4j/Logger;->log(Lorg/apache/log4j/Priority;Ljava/lang/Object;)V

    return-void
.end method

.method public log(Lcom/j256/ormlite/logger/Level;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1

    .line 2
    iget-object v0, p0, Lcom/j256/ormlite/logger/Log4jLogBackend;->logger:Lorg/apache/log4j/Logger;

    invoke-direct {p0, p1}, Lcom/j256/ormlite/logger/Log4jLogBackend;->levelToLog4jLevel(Lcom/j256/ormlite/logger/Level;)Lorg/apache/log4j/Level;

    move-result-object p1

    invoke-virtual {v0, p1, p2, p3}, Lorg/apache/log4j/Logger;->log(Lorg/apache/log4j/Priority;Ljava/lang/Object;Ljava/lang/Throwable;)V

    return-void
.end method

.class public Lcom/j256/ormlite/android/AndroidLogBackend;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/j256/ormlite/logger/LogBackend;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/j256/ormlite/android/AndroidLogBackend$AndroidLogBackendFactory;
    }
.end annotation


# static fields
.field private static final ALL_LOGS_NAME:Ljava/lang/String; = "ORMLite"

.field private static final MAX_TAG_LENGTH:I = 0x17

.field private static final REFRESH_LEVEL_CACHE_EVERY:I = 0xc8


# instance fields
.field private final className:Ljava/lang/String;

.field private final levelCache:[Z

.field private volatile levelCacheC:I


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/j256/ormlite/android/AndroidLogBackend;->levelCacheC:I

    invoke-static {p1}, Lcom/j256/ormlite/logger/LoggerFactory;->getSimpleClassName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    const/16 v3, 0x17

    if-le v2, v3, :cond_0

    add-int/lit8 v1, v2, -0x17

    invoke-virtual {p1, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    :cond_0
    iput-object v1, p0, Lcom/j256/ormlite/android/AndroidLogBackend;->className:Ljava/lang/String;

    invoke-static {}, Lcom/j256/ormlite/logger/Level;->values()[Lcom/j256/ormlite/logger/Level;

    move-result-object p1

    array-length v1, p1

    move v2, v0

    :goto_0
    if-ge v0, v1, :cond_2

    aget-object v3, p1, v0

    invoke-direct {p0, v3}, Lcom/j256/ormlite/android/AndroidLogBackend;->levelToAndroidLevel(Lcom/j256/ormlite/logger/Level;)I

    move-result v3

    if-le v3, v2, :cond_1

    move v2, v3

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    add-int/lit8 v2, v2, 0x1

    new-array p1, v2, [Z

    iput-object p1, p0, Lcom/j256/ormlite/android/AndroidLogBackend;->levelCache:[Z

    invoke-direct {p0}, Lcom/j256/ormlite/android/AndroidLogBackend;->refreshLevelCache()V

    return-void
.end method

.method private isLevelEnabledInternal(I)Z
    .locals 1

    iget-object v0, p0, Lcom/j256/ormlite/android/AndroidLogBackend;->className:Ljava/lang/String;

    invoke-static {v0, p1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "ORMLite"

    invoke-static {v0, p1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method

.method private levelToAndroidLevel(Lcom/j256/ormlite/logger/Level;)I
    .locals 3

    sget-object v0, Lcom/j256/ormlite/android/AndroidLogBackend$1;->$SwitchMap$com$j256$ormlite$logger$Level:[I

    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    const/4 v1, 0x2

    if-eq p1, v0, :cond_3

    const/4 v0, 0x3

    if-eq p1, v1, :cond_2

    const/4 v1, 0x5

    if-eq p1, v0, :cond_1

    const/4 v0, 0x6

    const/4 v2, 0x4

    if-eq p1, v2, :cond_0

    if-eq p1, v1, :cond_0

    return v2

    :cond_0
    return v0

    :cond_1
    return v1

    :cond_2
    return v0

    :cond_3
    return v1
.end method

.method private refreshLevelCache()V
    .locals 6

    invoke-static {}, Lcom/j256/ormlite/logger/Level;->values()[Lcom/j256/ormlite/logger/Level;

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_1

    aget-object v3, v0, v2

    invoke-direct {p0, v3}, Lcom/j256/ormlite/android/AndroidLogBackend;->levelToAndroidLevel(Lcom/j256/ormlite/logger/Level;)I

    move-result v3

    iget-object v4, p0, Lcom/j256/ormlite/android/AndroidLogBackend;->levelCache:[Z

    array-length v5, v4

    if-ge v3, v5, :cond_0

    invoke-direct {p0, v3}, Lcom/j256/ormlite/android/AndroidLogBackend;->isLevelEnabledInternal(I)Z

    move-result v5

    aput-boolean v5, v4, v3

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method


# virtual methods
.method public isLevelEnabled(Lcom/j256/ormlite/logger/Level;)Z
    .locals 2

    iget v0, p0, Lcom/j256/ormlite/android/AndroidLogBackend;->levelCacheC:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/j256/ormlite/android/AndroidLogBackend;->levelCacheC:I

    const/16 v1, 0xc8

    if-lt v0, v1, :cond_0

    invoke-direct {p0}, Lcom/j256/ormlite/android/AndroidLogBackend;->refreshLevelCache()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/j256/ormlite/android/AndroidLogBackend;->levelCacheC:I

    :cond_0
    invoke-direct {p0, p1}, Lcom/j256/ormlite/android/AndroidLogBackend;->levelToAndroidLevel(Lcom/j256/ormlite/logger/Level;)I

    move-result p1

    iget-object v0, p0, Lcom/j256/ormlite/android/AndroidLogBackend;->levelCache:[Z

    array-length v1, v0

    if-ge p1, v1, :cond_1

    aget-boolean p1, v0, p1

    return p1

    :cond_1
    invoke-direct {p0, p1}, Lcom/j256/ormlite/android/AndroidLogBackend;->isLevelEnabledInternal(I)Z

    move-result p1

    return p1
.end method

.method public log(Lcom/j256/ormlite/logger/Level;Ljava/lang/String;)V
    .locals 1

    .line 1
    sget-object v0, Lcom/j256/ormlite/android/AndroidLogBackend$1;->$SwitchMap$com$j256$ormlite$logger$Level:[I

    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_3

    const/4 v0, 0x2

    if-eq p1, v0, :cond_2

    const/4 v0, 0x3

    if-eq p1, v0, :cond_1

    const/4 v0, 0x4

    if-eq p1, v0, :cond_0

    const/4 v0, 0x5

    if-eq p1, v0, :cond_0

    iget-object p1, p0, Lcom/j256/ormlite/android/AndroidLogBackend;->className:Ljava/lang/String;

    invoke-static {p1, p2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/j256/ormlite/android/AndroidLogBackend;->className:Ljava/lang/String;

    invoke-static {p1, p2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    iget-object p1, p0, Lcom/j256/ormlite/android/AndroidLogBackend;->className:Ljava/lang/String;

    invoke-static {p1, p2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    iget-object p1, p0, Lcom/j256/ormlite/android/AndroidLogBackend;->className:Ljava/lang/String;

    invoke-static {p1, p2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_3
    iget-object p1, p0, Lcom/j256/ormlite/android/AndroidLogBackend;->className:Ljava/lang/String;

    invoke-static {p1, p2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void
.end method

.method public log(Lcom/j256/ormlite/logger/Level;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1

    .line 2
    sget-object v0, Lcom/j256/ormlite/android/AndroidLogBackend$1;->$SwitchMap$com$j256$ormlite$logger$Level:[I

    invoke-virtual {p1}, Ljava/lang/Enum;->ordinal()I

    move-result p1

    aget p1, v0, p1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_3

    const/4 v0, 0x2

    if-eq p1, v0, :cond_2

    const/4 v0, 0x3

    if-eq p1, v0, :cond_1

    const/4 v0, 0x4

    if-eq p1, v0, :cond_0

    const/4 v0, 0x5

    if-eq p1, v0, :cond_0

    iget-object p1, p0, Lcom/j256/ormlite/android/AndroidLogBackend;->className:Ljava/lang/String;

    invoke-static {p1, p2, p3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lcom/j256/ormlite/android/AndroidLogBackend;->className:Ljava/lang/String;

    invoke-static {p1, p2, p3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :cond_1
    iget-object p1, p0, Lcom/j256/ormlite/android/AndroidLogBackend;->className:Ljava/lang/String;

    invoke-static {p1, p2, p3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :cond_2
    iget-object p1, p0, Lcom/j256/ormlite/android/AndroidLogBackend;->className:Ljava/lang/String;

    invoke-static {p1, p2, p3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :cond_3
    iget-object p1, p0, Lcom/j256/ormlite/android/AndroidLogBackend;->className:Ljava/lang/String;

    invoke-static {p1, p2, p3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_0
    return-void
.end method

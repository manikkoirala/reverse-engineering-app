.class public Lcom/j256/ormlite/field/types/DateStringFormatConfig;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final dateFormat:Ljava/text/DateFormat;

.field private final dateFormatStr:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/j256/ormlite/field/types/DateStringFormatConfig;->dateFormatStr:Ljava/lang/String;

    new-instance v0, Ljava/text/SimpleDateFormat;

    invoke-direct {v0, p1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/j256/ormlite/field/types/DateStringFormatConfig;->dateFormat:Ljava/text/DateFormat;

    return-void
.end method


# virtual methods
.method public getDateFormat()Ljava/text/DateFormat;
    .locals 1

    iget-object v0, p0, Lcom/j256/ormlite/field/types/DateStringFormatConfig;->dateFormat:Ljava/text/DateFormat;

    invoke-virtual {v0}, Ljava/text/DateFormat;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/text/DateFormat;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/j256/ormlite/field/types/DateStringFormatConfig;->dateFormatStr:Ljava/lang/String;

    return-object v0
.end method

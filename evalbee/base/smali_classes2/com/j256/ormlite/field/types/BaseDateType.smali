.class public abstract Lcom/j256/ormlite/field/types/BaseDateType;
.super Lcom/j256/ormlite/field/types/BaseDataType;
.source "SourceFile"


# static fields
.field private static final DEFAULT_DATE_FORMAT_CONFIG:Lcom/j256/ormlite/field/types/DateStringFormatConfig;

.field private static final NO_MILLIS_DATE_FORMAT_CONFIG:Lcom/j256/ormlite/field/types/DateStringFormatConfig;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lcom/j256/ormlite/field/types/DateStringFormatConfig;

    const-string v1, "yyyy-MM-dd HH:mm:ss.SSSSSS"

    invoke-direct {v0, v1}, Lcom/j256/ormlite/field/types/DateStringFormatConfig;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/j256/ormlite/field/types/BaseDateType;->DEFAULT_DATE_FORMAT_CONFIG:Lcom/j256/ormlite/field/types/DateStringFormatConfig;

    new-instance v0, Lcom/j256/ormlite/field/types/DateStringFormatConfig;

    const-string v1, "yyyy-MM-dd HH:mm:ss"

    invoke-direct {v0, v1}, Lcom/j256/ormlite/field/types/DateStringFormatConfig;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/j256/ormlite/field/types/BaseDateType;->NO_MILLIS_DATE_FORMAT_CONFIG:Lcom/j256/ormlite/field/types/DateStringFormatConfig;

    return-void
.end method

.method public constructor <init>(Lcom/j256/ormlite/field/SqlType;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lcom/j256/ormlite/field/types/BaseDataType;-><init>(Lcom/j256/ormlite/field/SqlType;)V

    return-void
.end method

.method public constructor <init>(Lcom/j256/ormlite/field/SqlType;[Ljava/lang/Class;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/j256/ormlite/field/SqlType;",
            "[",
            "Ljava/lang/Class<",
            "*>;)V"
        }
    .end annotation

    .line 2
    invoke-direct {p0, p1, p2}, Lcom/j256/ormlite/field/types/BaseDataType;-><init>(Lcom/j256/ormlite/field/SqlType;[Ljava/lang/Class;)V

    return-void
.end method

.method private static conditionalFormat(Lcom/j256/ormlite/field/types/DateStringFormatConfig;Ljava/lang/String;)Ljava/text/DateFormat;
    .locals 1

    sget-object v0, Lcom/j256/ormlite/field/types/BaseDateType;->DEFAULT_DATE_FORMAT_CONFIG:Lcom/j256/ormlite/field/types/DateStringFormatConfig;

    if-ne p0, v0, :cond_0

    const/16 v0, 0x2e

    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(I)I

    move-result p1

    if-gez p1, :cond_0

    sget-object p0, Lcom/j256/ormlite/field/types/BaseDateType;->NO_MILLIS_DATE_FORMAT_CONFIG:Lcom/j256/ormlite/field/types/DateStringFormatConfig;

    invoke-virtual {p0}, Lcom/j256/ormlite/field/types/DateStringFormatConfig;->getDateFormat()Ljava/text/DateFormat;

    move-result-object p0

    return-object p0

    :cond_0
    invoke-virtual {p0}, Lcom/j256/ormlite/field/types/DateStringFormatConfig;->getDateFormat()Ljava/text/DateFormat;

    move-result-object p0

    return-object p0
.end method

.method public static convertDateStringConfig(Lcom/j256/ormlite/field/FieldType;Lcom/j256/ormlite/field/types/DateStringFormatConfig;)Lcom/j256/ormlite/field/types/DateStringFormatConfig;
    .locals 0

    if-nez p0, :cond_0

    return-object p1

    :cond_0
    invoke-virtual {p0}, Lcom/j256/ormlite/field/FieldType;->getDataTypeConfigObj()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lcom/j256/ormlite/field/types/DateStringFormatConfig;

    if-nez p0, :cond_1

    return-object p1

    :cond_1
    return-object p0
.end method

.method public static normalizeDateString(Lcom/j256/ormlite/field/types/DateStringFormatConfig;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    invoke-static {p0, p1}, Lcom/j256/ormlite/field/types/BaseDateType;->conditionalFormat(Lcom/j256/ormlite/field/types/DateStringFormatConfig;Ljava/lang/String;)Ljava/text/DateFormat;

    move-result-object p0

    invoke-virtual {p0, p1}, Ljava/text/DateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object p1

    invoke-virtual {p0, p1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static parseDateString(Lcom/j256/ormlite/field/types/DateStringFormatConfig;Ljava/lang/String;)Ljava/util/Date;
    .locals 0

    invoke-static {p0, p1}, Lcom/j256/ormlite/field/types/BaseDateType;->conditionalFormat(Lcom/j256/ormlite/field/types/DateStringFormatConfig;Ljava/lang/String;)Ljava/text/DateFormat;

    move-result-object p0

    invoke-virtual {p0, p1}, Ljava/text/DateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public getDefaultDateFormatConfig()Lcom/j256/ormlite/field/types/DateStringFormatConfig;
    .locals 1

    sget-object v0, Lcom/j256/ormlite/field/types/BaseDateType;->DEFAULT_DATE_FORMAT_CONFIG:Lcom/j256/ormlite/field/types/DateStringFormatConfig;

    return-object v0
.end method

.method public isValidForField(Ljava/lang/reflect/Field;)Z
    .locals 1

    invoke-virtual {p1}, Ljava/lang/reflect/Field;->getType()Ljava/lang/Class;

    move-result-object p1

    const-class v0, Ljava/util/Date;

    if-ne p1, v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public isValidForVersion()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public moveToNextValue(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    if-nez p1, :cond_0

    new-instance p1, Ljava/util/Date;

    invoke-direct {p1, v0, v1}, Ljava/util/Date;-><init>(J)V

    return-object p1

    :cond_0
    check-cast p1, Ljava/util/Date;

    invoke-virtual {p1}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    cmp-long p1, v0, v2

    if-nez p1, :cond_1

    new-instance p1, Ljava/util/Date;

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    invoke-direct {p1, v0, v1}, Ljava/util/Date;-><init>(J)V

    return-object p1

    :cond_1
    new-instance p1, Ljava/util/Date;

    invoke-direct {p1, v0, v1}, Ljava/util/Date;-><init>(J)V

    return-object p1
.end method

.class public abstract Lmq;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Z

.field public static final b:Lkotlinx/coroutines/h;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .line 1
    const-string v0, "kotlinx.coroutines.main.delay"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lqt1;->f(Ljava/lang/String;Z)Z

    move-result v0

    sput-boolean v0, Lmq;->a:Z

    invoke-static {}, Lmq;->b()Lkotlinx/coroutines/h;

    move-result-object v0

    sput-object v0, Lmq;->b:Lkotlinx/coroutines/h;

    return-void
.end method

.method public static final a()Lkotlinx/coroutines/h;
    .locals 1

    .line 1
    sget-object v0, Lmq;->b:Lkotlinx/coroutines/h;

    return-object v0
.end method

.method public static final b()Lkotlinx/coroutines/h;
    .locals 2

    .line 1
    sget-boolean v0, Lmq;->a:Z

    if-nez v0, :cond_0

    sget-object v0, Lkotlinx/coroutines/g;->i:Lkotlinx/coroutines/g;

    return-object v0

    :cond_0
    invoke-static {}, Lpt;->c()Lpm0;

    move-result-object v0

    invoke-static {v0}, Lsm0;->c(Lpm0;)Z

    move-result v1

    if-nez v1, :cond_2

    instance-of v1, v0, Lkotlinx/coroutines/h;

    if-nez v1, :cond_1

    goto :goto_0

    :cond_1
    check-cast v0, Lkotlinx/coroutines/h;

    goto :goto_1

    :cond_2
    :goto_0
    sget-object v0, Lkotlinx/coroutines/g;->i:Lkotlinx/coroutines/g;

    :goto_1
    return-object v0
.end method

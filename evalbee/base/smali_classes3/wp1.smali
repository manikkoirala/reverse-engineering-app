.class public abstract Lwp1;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Lxs1;

.field public static final b:Lxs1;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lxs1;

    const-string v1, "NONE"

    invoke-direct {v0, v1}, Lxs1;-><init>(Ljava/lang/String;)V

    sput-object v0, Lwp1;->a:Lxs1;

    new-instance v0, Lxs1;

    const-string v1, "PENDING"

    invoke-direct {v0, v1}, Lxs1;-><init>(Ljava/lang/String;)V

    sput-object v0, Lwp1;->b:Lxs1;

    return-void
.end method

.method public static final a(Ljava/lang/Object;)Lvx0;
    .locals 1

    .line 1
    new-instance v0, Lkotlinx/coroutines/flow/StateFlowImpl;

    if-nez p0, :cond_0

    sget-object p0, Lj01;->a:Lxs1;

    :cond_0
    invoke-direct {v0, p0}, Lkotlinx/coroutines/flow/StateFlowImpl;-><init>(Ljava/lang/Object;)V

    return-object v0
.end method

.method public static final synthetic b()Lxs1;
    .locals 1

    .line 1
    sget-object v0, Lwp1;->a:Lxs1;

    return-object v0
.end method

.method public static final synthetic c()Lxs1;
    .locals 1

    .line 1
    sget-object v0, Lwp1;->b:Lxs1;

    return-object v0
.end method

.method public static final d(Lvp1;Lkotlin/coroutines/CoroutineContext;ILkotlinx/coroutines/channels/BufferOverflow;)Ly40;
    .locals 2

    .line 1
    const/4 v0, 0x0

    if-ltz p2, :cond_0

    const/4 v1, 0x2

    if-ge p2, v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    if-nez v0, :cond_1

    const/4 v0, -0x2

    if-ne p2, v0, :cond_2

    :cond_1
    sget-object v0, Lkotlinx/coroutines/channels/BufferOverflow;->DROP_OLDEST:Lkotlinx/coroutines/channels/BufferOverflow;

    if-ne p3, v0, :cond_2

    return-object p0

    :cond_2
    invoke-static {p0, p1, p2, p3}, Lkn1;->c(Ljn1;Lkotlin/coroutines/CoroutineContext;ILkotlinx/coroutines/channels/BufferOverflow;)Ly40;

    move-result-object p0

    return-object p0
.end method

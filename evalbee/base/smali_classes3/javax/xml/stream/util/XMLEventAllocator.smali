.class public interface abstract Ljavax/xml/stream/util/XMLEventAllocator;
.super Ljava/lang/Object;
.source "SourceFile"


# virtual methods
.method public abstract allocate(Ljavax/xml/stream/XMLStreamReader;)Ljavax/xml/stream/events/XMLEvent;
.end method

.method public abstract allocate(Ljavax/xml/stream/XMLStreamReader;Ljavax/xml/stream/util/XMLEventConsumer;)V
.end method

.method public abstract newInstance()Ljavax/xml/stream/util/XMLEventAllocator;
.end method

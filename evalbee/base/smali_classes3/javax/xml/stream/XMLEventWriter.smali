.class public interface abstract Ljavax/xml/stream/XMLEventWriter;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljavax/xml/stream/util/XMLEventConsumer;


# virtual methods
.method public abstract add(Ljavax/xml/stream/XMLEventReader;)V
.end method

.method public abstract add(Ljavax/xml/stream/events/XMLEvent;)V
.end method

.method public abstract close()V
.end method

.method public abstract flush()V
.end method

.method public abstract getNamespaceContext()Ljavax/xml/namespace/NamespaceContext;
.end method

.method public abstract getPrefix(Ljava/lang/String;)Ljava/lang/String;
.end method

.method public abstract setDefaultNamespace(Ljava/lang/String;)V
.end method

.method public abstract setNamespaceContext(Ljavax/xml/namespace/NamespaceContext;)V
.end method

.method public abstract setPrefix(Ljava/lang/String;Ljava/lang/String;)V
.end method

.class public Lxd;
.super Lg;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lxd$b;
    }
.end annotation


# instance fields
.field public final f:C

.field public final g:Z

.field public final h:Z

.field public final i:Z

.field public j:I

.field public k:Z

.field public l:Ljava/util/Locale;


# direct methods
.method public constructor <init>(CCCZZZLcom/opencsv/enums/CSVReaderNullFieldIndicator;Ljava/util/Locale;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p2, p7}, Lg;-><init>(CCLcom/opencsv/enums/CSVReaderNullFieldIndicator;)V

    const/4 p7, -0x1

    iput p7, p0, Lxd;->j:I

    const/4 p7, 0x0

    iput-boolean p7, p0, Lxd;->k:Z

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object p7

    invoke-static {p8, p7}, Lorg/apache/commons/lang3/ObjectUtils;->defaultIfNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p7

    check-cast p7, Ljava/util/Locale;

    iput-object p7, p0, Lxd;->l:Ljava/util/Locale;

    invoke-virtual {p0, p1, p2, p3}, Lxd;->e(CCC)Z

    move-result p2

    const-string p7, "opencsv"

    if-nez p2, :cond_1

    if-eqz p1, :cond_0

    iput-char p3, p0, Lxd;->f:C

    iput-boolean p4, p0, Lxd;->g:Z

    iput-boolean p5, p0, Lxd;->h:Z

    iput-boolean p6, p0, Lxd;->i:Z

    return-void

    :cond_0
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    iget-object p2, p0, Lxd;->l:Ljava/util/Locale;

    invoke-static {p7, p2}, Ljava/util/ResourceBundle;->getBundle(Ljava/lang/String;Ljava/util/Locale;)Ljava/util/ResourceBundle;

    move-result-object p2

    const-string p3, "define.separator"

    invoke-virtual {p2, p3}, Ljava/util/ResourceBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    iget-object p2, p0, Lxd;->l:Ljava/util/Locale;

    invoke-static {p7, p2}, Ljava/util/ResourceBundle;->getBundle(Ljava/lang/String;Ljava/util/Locale;)Ljava/util/ResourceBundle;

    move-result-object p2

    const-string p3, "special.characters.must.differ"

    invoke-virtual {p2, p3}, Ljava/util/ResourceBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public d(Ljava/lang/String;Z)[Ljava/lang/String;
    .locals 9

    .line 1
    const/4 v0, 0x0

    if-nez p2, :cond_0

    iget-object v1, p0, Lg;->e:Ljava/lang/String;

    if-eqz v1, :cond_0

    iput-object v0, p0, Lg;->e:Ljava/lang/String;

    :cond_0
    if-nez p1, :cond_2

    iget-object p1, p0, Lg;->e:Ljava/lang/String;

    if-eqz p1, :cond_1

    iput-object v0, p0, Lg;->e:Ljava/lang/String;

    filled-new-array {p1}, [Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_1
    return-object v0

    :cond_2
    iget v1, p0, Lxd;->j:I

    if-gtz v1, :cond_3

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    goto :goto_0

    :cond_3
    new-instance v1, Ljava/util/ArrayList;

    iget v2, p0, Lxd;->j:I

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    :goto_0
    new-instance v2, Lxd$b;

    invoke-direct {v2, p1}, Lxd$b;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lg;->e:Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v5, 0x1

    if-eqz v3, :cond_4

    invoke-virtual {v2, v3}, Lxd$b;->c(Ljava/lang/String;)V

    iput-object v0, p0, Lg;->e:Ljava/lang/String;

    iget-boolean v0, p0, Lxd;->i:Z

    xor-int/2addr v0, v5

    :goto_1
    move v3, v4

    goto :goto_2

    :cond_4
    move v0, v4

    move v3, v0

    :cond_5
    :goto_2
    invoke-virtual {v2}, Lxd$b;->f()Z

    move-result v6

    if-nez v6, :cond_f

    invoke-virtual {v2}, Lxd$b;->j()C

    move-result v6

    iget-char v7, p0, Lxd;->f:C

    if-ne v6, v7, :cond_6

    invoke-virtual {p0, v0}, Lxd;->g(Z)Z

    move-result v6

    invoke-static {v2}, Lxd$b;->a(Lxd$b;)I

    move-result v7

    sub-int/2addr v7, v5

    invoke-virtual {p0, p1, v6, v7}, Lxd;->k(Ljava/lang/String;ZI)Z

    move-result v6

    if-eqz v6, :cond_5

    invoke-virtual {v2}, Lxd$b;->j()C

    invoke-virtual {v2}, Lxd$b;->d()V

    goto :goto_2

    :cond_6
    iget-char v7, p0, Lg;->c:C

    if-ne v6, v7, :cond_b

    invoke-virtual {p0, v0}, Lxd;->g(Z)Z

    move-result v6

    invoke-static {v2}, Lxd$b;->a(Lxd$b;)I

    move-result v7

    sub-int/2addr v7, v5

    invoke-virtual {p0, p1, v6, v7}, Lxd;->l(Ljava/lang/String;ZI)Z

    move-result v6

    if-eqz v6, :cond_8

    invoke-virtual {v2}, Lxd$b;->j()C

    :cond_7
    invoke-virtual {v2}, Lxd$b;->d()V

    goto :goto_3

    :cond_8
    xor-int/lit8 v0, v0, 0x1

    invoke-virtual {v2}, Lxd$b;->g()Z

    move-result v6

    if-eqz v6, :cond_9

    move v3, v5

    :cond_9
    iget-boolean v6, p0, Lxd;->g:Z

    if-nez v6, :cond_a

    invoke-static {v2}, Lxd$b;->a(Lxd$b;)I

    move-result v6

    const/4 v7, 0x3

    if-le v6, v7, :cond_a

    add-int/lit8 v7, v6, -0x2

    invoke-virtual {p1, v7}, Ljava/lang/String;->charAt(I)C

    move-result v7

    iget-char v8, p0, Lg;->b:C

    if-eq v7, v8, :cond_a

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v7

    if-le v7, v6, :cond_a

    invoke-virtual {p1, v6}, Ljava/lang/String;->charAt(I)C

    move-result v6

    iget-char v7, p0, Lg;->b:C

    if-eq v6, v7, :cond_a

    iget-boolean v6, p0, Lxd;->h:Z

    if-eqz v6, :cond_7

    invoke-virtual {v2}, Lxd$b;->g()Z

    move-result v6

    if-nez v6, :cond_7

    invoke-virtual {v2}, Lxd$b;->i()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lorg/apache/commons/lang3/StringUtils;->isWhitespace(Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_7

    invoke-virtual {v2}, Lxd$b;->e()V

    :cond_a
    :goto_3
    iget-boolean v6, p0, Lxd;->k:Z

    xor-int/2addr v6, v5

    iput-boolean v6, p0, Lxd;->k:Z

    goto/16 :goto_2

    :cond_b
    iget-char v7, p0, Lg;->b:C

    if-ne v6, v7, :cond_d

    if-eqz v0, :cond_c

    iget-boolean v6, p0, Lxd;->i:Z

    if-eqz v6, :cond_d

    :cond_c
    invoke-virtual {v2}, Lxd$b;->k()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6, v3}, Lxd;->f(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iput-boolean v4, p0, Lxd;->k:Z

    goto/16 :goto_1

    :cond_d
    iget-boolean v6, p0, Lxd;->g:Z

    if-eqz v6, :cond_e

    if-eqz v0, :cond_5

    iget-boolean v6, p0, Lxd;->i:Z

    if-nez v6, :cond_5

    :cond_e
    invoke-virtual {v2}, Lxd$b;->d()V

    iput-boolean v5, p0, Lxd;->k:Z

    move v3, v5

    goto/16 :goto_2

    :cond_f
    if-eqz v0, :cond_11

    iget-boolean p1, p0, Lxd;->i:Z

    if-nez p1, :cond_11

    if-eqz p2, :cond_10

    const/16 p1, 0xa

    invoke-virtual {v2, p1}, Lxd$b;->b(C)V

    invoke-virtual {v2}, Lxd$b;->i()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lg;->e:Ljava/lang/String;

    goto :goto_4

    :cond_10
    new-instance p1, Ljava/io/IOException;

    const-string p2, "opencsv"

    iget-object v0, p0, Lxd;->l:Ljava/util/Locale;

    invoke-static {p2, v0}, Ljava/util/ResourceBundle;->getBundle(Ljava/lang/String;Ljava/util/Locale;)Ljava/util/ResourceBundle;

    move-result-object p2

    const-string v0, "unterminated.quote"

    invoke-virtual {p2, v0}, Ljava/util/ResourceBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v2}, Lxd$b;->i()Ljava/lang/String;

    move-result-object v0

    filled-new-array {v0}, [Ljava/lang/Object;

    move-result-object v0

    invoke-static {p2, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_11
    iput-boolean v4, p0, Lxd;->k:Z

    invoke-virtual {v2}, Lxd$b;->k()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1, v3}, Lxd;->f(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object p1

    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_4
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result p1

    iput p1, p0, Lxd;->j:I

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result p1

    new-array p1, p1, [Ljava/lang/String;

    invoke-interface {v1, p1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object p1

    check-cast p1, [Ljava/lang/String;

    return-object p1
.end method

.method public final e(CCC)Z
    .locals 1

    .line 1
    invoke-virtual {p0, p1, p2}, Lxd;->m(CC)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0, p1, p3}, Lxd;->m(CC)Z

    move-result p1

    if-nez p1, :cond_1

    invoke-virtual {p0, p2, p3}, Lxd;->m(CC)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method

.method public final f(Ljava/lang/String;Z)Ljava/lang/String;
    .locals 1

    .line 1
    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p2}, Lxd;->n(Z)Z

    move-result p2

    if-eqz p2, :cond_0

    const/4 p1, 0x0

    :cond_0
    return-object p1
.end method

.method public final g(Z)Z
    .locals 0

    .line 1
    if-eqz p1, :cond_0

    iget-boolean p1, p0, Lxd;->i:Z

    if-eqz p1, :cond_1

    :cond_0
    iget-boolean p1, p0, Lxd;->k:Z

    if-eqz p1, :cond_2

    :cond_1
    const/4 p1, 0x1

    goto :goto_0

    :cond_2
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public final h(C)Z
    .locals 1

    .line 1
    invoke-virtual {p0, p1}, Lxd;->j(C)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0, p1}, Lxd;->i(C)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method

.method public final i(C)Z
    .locals 1

    .line 1
    iget-char v0, p0, Lxd;->f:C

    if-ne p1, v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public final j(C)Z
    .locals 1

    .line 1
    iget-char v0, p0, Lg;->c:C

    if-ne p1, v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public k(Ljava/lang/String;ZI)Z
    .locals 1

    .line 1
    if-eqz p2, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result p2

    const/4 v0, 0x1

    add-int/2addr p3, v0

    if-le p2, p3, :cond_0

    invoke-virtual {p1, p3}, Ljava/lang/String;->charAt(I)C

    move-result p1

    invoke-virtual {p0, p1}, Lxd;->h(C)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final l(Ljava/lang/String;ZI)Z
    .locals 1

    .line 1
    if-eqz p2, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result p2

    const/4 v0, 0x1

    add-int/2addr p3, v0

    if-le p2, p3, :cond_0

    invoke-virtual {p1, p3}, Ljava/lang/String;->charAt(I)C

    move-result p1

    invoke-virtual {p0, p1}, Lxd;->j(C)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final m(CC)Z
    .locals 0

    .line 1
    if-eqz p1, :cond_0

    if-ne p1, p2, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public final n(Z)Z
    .locals 3

    .line 1
    sget-object v0, Lxd$a;->a:[I

    iget-object v1, p0, Lg;->d:Lcom/opencsv/enums/CSVReaderNullFieldIndicator;

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v2, 0x2

    if-eq v0, v2, :cond_1

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    const/4 p1, 0x0

    :cond_0
    return p1

    :cond_1
    xor-int/2addr p1, v1

    return p1

    :cond_2
    return v1
.end method

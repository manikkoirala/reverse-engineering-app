.class public abstract Lnt;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Lxs1;

.field public static final b:Lxs1;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lxs1;

    const-string v1, "UNDEFINED"

    invoke-direct {v0, v1}, Lxs1;-><init>(Ljava/lang/String;)V

    sput-object v0, Lnt;->a:Lxs1;

    new-instance v0, Lxs1;

    const-string v1, "REUSABLE_CLAIMED"

    invoke-direct {v0, v1}, Lxs1;-><init>(Ljava/lang/String;)V

    sput-object v0, Lnt;->b:Lxs1;

    return-void
.end method

.method public static final synthetic a()Lxs1;
    .locals 1

    .line 1
    sget-object v0, Lnt;->a:Lxs1;

    return-object v0
.end method

.method public static final b(Lvl;Ljava/lang/Object;Lc90;)V
    .locals 6

    .line 1
    instance-of v0, p0, Lmt;

    if-eqz v0, :cond_8

    check-cast p0, Lmt;

    invoke-static {p1, p2}, Lvi;->c(Ljava/lang/Object;Lc90;)Ljava/lang/Object;

    move-result-object p2

    iget-object v0, p0, Lmt;->d:Lkotlinx/coroutines/CoroutineDispatcher;

    invoke-virtual {p0}, Lmt;->getContext()Lkotlin/coroutines/CoroutineContext;

    move-result-object v1

    invoke-virtual {v0, v1}, Lkotlinx/coroutines/CoroutineDispatcher;->q0(Lkotlin/coroutines/CoroutineContext;)Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    iput-object p2, p0, Lmt;->f:Ljava/lang/Object;

    iput v1, p0, Lkotlinx/coroutines/j;->c:I

    iget-object p1, p0, Lmt;->d:Lkotlinx/coroutines/CoroutineDispatcher;

    invoke-virtual {p0}, Lmt;->getContext()Lkotlin/coroutines/CoroutineContext;

    move-result-object p2

    invoke-virtual {p1, p2, p0}, Lkotlinx/coroutines/CoroutineDispatcher;->f(Lkotlin/coroutines/CoroutineContext;Ljava/lang/Runnable;)V

    goto/16 :goto_4

    :cond_0
    sget-object v0, Luv1;->a:Luv1;

    invoke-virtual {v0}, Luv1;->b()Lsx;

    move-result-object v0

    invoke-virtual {v0}, Lsx;->z0()Z

    move-result v2

    if-eqz v2, :cond_1

    iput-object p2, p0, Lmt;->f:Ljava/lang/Object;

    iput v1, p0, Lkotlinx/coroutines/j;->c:I

    invoke-virtual {v0, p0}, Lsx;->v0(Lkotlinx/coroutines/j;)V

    goto/16 :goto_4

    :cond_1
    invoke-virtual {v0, v1}, Lsx;->x0(Z)V

    const/4 v2, 0x0

    :try_start_0
    invoke-virtual {p0}, Lmt;->getContext()Lkotlin/coroutines/CoroutineContext;

    move-result-object v3

    sget-object v4, Lkotlinx/coroutines/n;->r1:Lkotlinx/coroutines/n$b;

    invoke-interface {v3, v4}, Lkotlin/coroutines/CoroutineContext;->get(Lkotlin/coroutines/CoroutineContext$b;)Lkotlin/coroutines/CoroutineContext$a;

    move-result-object v3

    check-cast v3, Lkotlinx/coroutines/n;

    if-eqz v3, :cond_2

    invoke-interface {v3}, Lkotlinx/coroutines/n;->a()Z

    move-result v4

    if-nez v4, :cond_2

    invoke-interface {v3}, Lkotlinx/coroutines/n;->R()Ljava/util/concurrent/CancellationException;

    move-result-object v3

    invoke-virtual {p0, p2, v3}, Lmt;->b(Ljava/lang/Object;Ljava/lang/Throwable;)V

    sget-object p2, Lkotlin/Result;->Companion:Lkotlin/Result$a;

    invoke-static {v3}, Lxe1;->a(Ljava/lang/Throwable;)Ljava/lang/Object;

    move-result-object p2

    invoke-static {p2}, Lkotlin/Result;->constructor-impl(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    invoke-interface {p0, p2}, Lvl;->resumeWith(Ljava/lang/Object;)V

    move p2, v1

    goto :goto_0

    :cond_2
    const/4 p2, 0x0

    :goto_0
    if-nez p2, :cond_7

    iget-object p2, p0, Lmt;->e:Lvl;

    iget-object v3, p0, Lmt;->g:Ljava/lang/Object;

    invoke-interface {p2}, Lvl;->getContext()Lkotlin/coroutines/CoroutineContext;

    move-result-object v4

    invoke-static {v4, v3}, Lkotlinx/coroutines/internal/ThreadContextKt;->c(Lkotlin/coroutines/CoroutineContext;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    sget-object v5, Lkotlinx/coroutines/internal/ThreadContextKt;->a:Lxs1;

    if-eq v3, v5, :cond_3

    invoke-static {p2, v4, v3}, Lkotlinx/coroutines/CoroutineContextKt;->g(Lvl;Lkotlin/coroutines/CoroutineContext;Ljava/lang/Object;)Lq02;

    move-result-object p2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    goto :goto_1

    :cond_3
    move-object p2, v2

    :goto_1
    :try_start_1
    iget-object v5, p0, Lmt;->e:Lvl;

    invoke-interface {v5, p1}, Lvl;->resumeWith(Ljava/lang/Object;)V

    sget-object p1, Lu02;->a:Lu02;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz p2, :cond_4

    :try_start_2
    invoke-virtual {p2}, Lq02;->e1()Z

    move-result p1

    if-eqz p1, :cond_7

    :cond_4
    invoke-static {v4, v3}, Lkotlinx/coroutines/internal/ThreadContextKt;->a(Lkotlin/coroutines/CoroutineContext;Ljava/lang/Object;)V

    goto :goto_2

    :catchall_0
    move-exception p1

    if-eqz p2, :cond_5

    invoke-virtual {p2}, Lq02;->e1()Z

    move-result p2

    if-eqz p2, :cond_6

    :cond_5
    invoke-static {v4, v3}, Lkotlinx/coroutines/internal/ThreadContextKt;->a(Lkotlin/coroutines/CoroutineContext;Ljava/lang/Object;)V

    :cond_6
    throw p1

    :cond_7
    :goto_2
    invoke-virtual {v0}, Lsx;->C0()Z

    move-result p1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    if-nez p1, :cond_7

    goto :goto_3

    :catchall_1
    move-exception p1

    :try_start_3
    invoke-virtual {p0, p1, v2}, Lkotlinx/coroutines/j;->f(Ljava/lang/Throwable;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    :goto_3
    invoke-virtual {v0, v1}, Lsx;->s0(Z)V

    goto :goto_4

    :catchall_2
    move-exception p0

    invoke-virtual {v0, v1}, Lsx;->s0(Z)V

    throw p0

    :cond_8
    invoke-interface {p0, p1}, Lvl;->resumeWith(Ljava/lang/Object;)V

    :goto_4
    return-void
.end method

.method public static synthetic c(Lvl;Ljava/lang/Object;Lc90;ILjava/lang/Object;)V
    .locals 0

    .line 1
    and-int/lit8 p3, p3, 0x2

    if-eqz p3, :cond_0

    const/4 p2, 0x0

    :cond_0
    invoke-static {p0, p1, p2}, Lnt;->b(Lvl;Ljava/lang/Object;Lc90;)V

    return-void
.end method

.method public static final d(Lmt;)Z
    .locals 5

    .line 1
    sget-object v0, Lu02;->a:Lu02;

    sget-object v1, Luv1;->a:Luv1;

    invoke-virtual {v1}, Luv1;->b()Lsx;

    move-result-object v1

    invoke-virtual {v1}, Lsx;->A0()Z

    move-result v2

    const/4 v3, 0x0

    if-eqz v2, :cond_0

    goto :goto_1

    :cond_0
    invoke-virtual {v1}, Lsx;->z0()Z

    move-result v2

    const/4 v4, 0x1

    if-eqz v2, :cond_1

    iput-object v0, p0, Lmt;->f:Ljava/lang/Object;

    iput v4, p0, Lkotlinx/coroutines/j;->c:I

    invoke-virtual {v1, p0}, Lsx;->v0(Lkotlinx/coroutines/j;)V

    move v3, v4

    goto :goto_1

    :cond_1
    invoke-virtual {v1, v4}, Lsx;->x0(Z)V

    :try_start_0
    invoke-virtual {p0}, Lkotlinx/coroutines/j;->run()V

    :cond_2
    invoke-virtual {v1}, Lsx;->C0()Z

    move-result v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_2

    goto :goto_0

    :catchall_0
    move-exception v0

    const/4 v2, 0x0

    :try_start_1
    invoke-virtual {p0, v0, v2}, Lkotlinx/coroutines/j;->f(Ljava/lang/Throwable;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :goto_0
    invoke-virtual {v1, v4}, Lsx;->s0(Z)V

    :goto_1
    return v3

    :catchall_1
    move-exception p0

    invoke-virtual {v1, v4}, Lsx;->s0(Z)V

    throw p0
.end method

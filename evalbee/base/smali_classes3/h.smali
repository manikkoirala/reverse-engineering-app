.class public abstract Lh;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Closeable;
.implements Ljava/io/Flushable;


# instance fields
.field public final a:Ljava/io/Writer;

.field public b:Ljava/lang/String;

.field public volatile c:Ljava/io/IOException;


# direct methods
.method public constructor <init>(Ljava/io/Writer;Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lh;->a:Ljava/io/Writer;

    iput-object p2, p0, Lh;->b:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public a([Ljava/lang/String;)V
    .locals 1

    .line 1
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lh;->b([Ljava/lang/String;Z)V

    return-void
.end method

.method public b([Ljava/lang/String;Z)V
    .locals 2

    .line 1
    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x400

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {p0, p1, p2, v0}, Lh;->c([Ljava/lang/String;ZLjava/lang/Appendable;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p1

    iput-object p1, p0, Lh;->c:Ljava/io/IOException;

    :goto_0
    return-void
.end method

.method public abstract c([Ljava/lang/String;ZLjava/lang/Appendable;)V
.end method

.method public close()V
    .locals 1

    .line 1
    invoke-virtual {p0}, Lh;->flush()V

    iget-object v0, p0, Lh;->a:Ljava/io/Writer;

    invoke-virtual {v0}, Ljava/io/Writer;->close()V

    return-void
.end method

.method public flush()V
    .locals 1

    .line 1
    iget-object v0, p0, Lh;->a:Ljava/io/Writer;

    invoke-virtual {v0}, Ljava/io/Writer;->flush()V

    return-void
.end method

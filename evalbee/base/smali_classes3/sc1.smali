.class public final Lsc1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lxc;


# instance fields
.field public final a:Lokio/a;

.field public final b:Luo1;

.field public c:Z


# direct methods
.method public constructor <init>(Luo1;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lokio/a;

    invoke-direct {v0}, Lokio/a;-><init>()V

    iput-object v0, p0, Lsc1;->a:Lokio/a;

    if-eqz p1, :cond_0

    iput-object p1, p0, Lsc1;->b:Luo1;

    return-void

    :cond_0
    new-instance p1, Ljava/lang/NullPointerException;

    const-string v0, "source == null"

    invoke-direct {p1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public Q()Lokio/a;
    .locals 1

    .line 1
    iget-object v0, p0, Lsc1;->a:Lokio/a;

    return-object v0
.end method

.method public Y(J)[B
    .locals 1

    .line 1
    invoke-virtual {p0, p1, p2}, Lsc1;->a0(J)V

    iget-object v0, p0, Lsc1;->a:Lokio/a;

    invoke-virtual {v0, p1, p2}, Lokio/a;->Y(J)[B

    move-result-object p1

    return-object p1
.end method

.method public a(J)Z
    .locals 4

    .line 1
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-ltz v0, :cond_3

    iget-boolean v0, p0, Lsc1;->c:Z

    if-nez v0, :cond_2

    :cond_0
    iget-object v0, p0, Lsc1;->a:Lokio/a;

    iget-wide v1, v0, Lokio/a;->b:J

    cmp-long v1, v1, p1

    if-gez v1, :cond_1

    iget-object v1, p0, Lsc1;->b:Luo1;

    const-wide/16 v2, 0x2000

    invoke-interface {v1, v0, v2, v3}, Luo1;->read(Lokio/a;J)J

    move-result-wide v0

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 p1, 0x0

    return p1

    :cond_1
    const/4 p1, 0x1

    return p1

    :cond_2
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "closed"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "byteCount < 0: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a0(J)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1, p2}, Lsc1;->a(J)Z

    move-result p1

    if-eqz p1, :cond_0

    return-void

    :cond_0
    new-instance p1, Ljava/io/EOFException;

    invoke-direct {p1}, Ljava/io/EOFException;-><init>()V

    throw p1
.end method

.method public close()V
    .locals 1

    .line 1
    iget-boolean v0, p0, Lsc1;->c:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lsc1;->c:Z

    iget-object v0, p0, Lsc1;->b:Luo1;

    invoke-interface {v0}, Luo1;->close()V

    iget-object v0, p0, Lsc1;->a:Lokio/a;

    invoke-virtual {v0}, Lokio/a;->b()V

    return-void
.end method

.method public e0()Z
    .locals 4

    .line 1
    iget-boolean v0, p0, Lsc1;->c:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lsc1;->a:Lokio/a;

    invoke-virtual {v0}, Lokio/a;->e0()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lsc1;->b:Luo1;

    iget-object v1, p0, Lsc1;->a:Lokio/a;

    const-wide/16 v2, 0x2000

    invoke-interface {v0, v1, v2, v3}, Luo1;->read(Lokio/a;J)J

    move-result-wide v0

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "closed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public isOpen()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lsc1;->c:Z

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public read(Ljava/nio/ByteBuffer;)I
    .locals 5

    .line 1
    iget-object v0, p0, Lsc1;->a:Lokio/a;

    iget-wide v1, v0, Lokio/a;->b:J

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-nez v1, :cond_0

    iget-object v1, p0, Lsc1;->b:Luo1;

    const-wide/16 v2, 0x2000

    invoke-interface {v1, v0, v2, v3}, Luo1;->read(Lokio/a;J)J

    move-result-wide v0

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 p1, -0x1

    return p1

    :cond_0
    iget-object v0, p0, Lsc1;->a:Lokio/a;

    invoke-virtual {v0, p1}, Lokio/a;->read(Ljava/nio/ByteBuffer;)I

    move-result p1

    return p1
.end method

.method public read(Lokio/a;J)J
    .locals 5

    .line 2
    if-eqz p1, :cond_3

    const-wide/16 v0, 0x0

    cmp-long v2, p2, v0

    if-ltz v2, :cond_2

    iget-boolean v2, p0, Lsc1;->c:Z

    if-nez v2, :cond_1

    iget-object v2, p0, Lsc1;->a:Lokio/a;

    iget-wide v3, v2, Lokio/a;->b:J

    cmp-long v0, v3, v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lsc1;->b:Luo1;

    const-wide/16 v3, 0x2000

    invoke-interface {v0, v2, v3, v4}, Luo1;->read(Lokio/a;J)J

    move-result-wide v0

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    return-wide v2

    :cond_0
    iget-object v0, p0, Lsc1;->a:Lokio/a;

    iget-wide v0, v0, Lokio/a;->b:J

    invoke-static {p2, p3, v0, v1}, Ljava/lang/Math;->min(JJ)J

    move-result-wide p2

    iget-object v0, p0, Lsc1;->a:Lokio/a;

    invoke-virtual {v0, p1, p2, p3}, Lokio/a;->read(Lokio/a;J)J

    move-result-wide p1

    return-wide p1

    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "closed"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_2
    new-instance p1, Ljava/lang/IllegalArgumentException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "byteCount < 0: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_3
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "sink == null"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public readByte()B
    .locals 2

    .line 1
    const-wide/16 v0, 0x1

    invoke-virtual {p0, v0, v1}, Lsc1;->a0(J)V

    iget-object v0, p0, Lsc1;->a:Lokio/a;

    invoke-virtual {v0}, Lokio/a;->readByte()B

    move-result v0

    return v0
.end method

.method public readInt()I
    .locals 2

    .line 1
    const-wide/16 v0, 0x4

    invoke-virtual {p0, v0, v1}, Lsc1;->a0(J)V

    iget-object v0, p0, Lsc1;->a:Lokio/a;

    invoke-virtual {v0}, Lokio/a;->readInt()I

    move-result v0

    return v0
.end method

.method public readShort()S
    .locals 2

    .line 1
    const-wide/16 v0, 0x2

    invoke-virtual {p0, v0, v1}, Lsc1;->a0(J)V

    iget-object v0, p0, Lsc1;->a:Lokio/a;

    invoke-virtual {v0}, Lokio/a;->readShort()S

    move-result v0

    return v0
.end method

.method public skip(J)V
    .locals 5

    .line 1
    iget-boolean v0, p0, Lsc1;->c:Z

    if-nez v0, :cond_3

    :goto_0
    const-wide/16 v0, 0x0

    cmp-long v2, p1, v0

    if-lez v2, :cond_2

    iget-object v2, p0, Lsc1;->a:Lokio/a;

    iget-wide v3, v2, Lokio/a;->b:J

    cmp-long v0, v3, v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lsc1;->b:Luo1;

    const-wide/16 v3, 0x2000

    invoke-interface {v0, v2, v3, v4}, Luo1;->read(Lokio/a;J)J

    move-result-wide v0

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    goto :goto_1

    :cond_0
    new-instance p1, Ljava/io/EOFException;

    invoke-direct {p1}, Ljava/io/EOFException;-><init>()V

    throw p1

    :cond_1
    :goto_1
    iget-object v0, p0, Lsc1;->a:Lokio/a;

    invoke-virtual {v0}, Lokio/a;->R()J

    move-result-wide v0

    invoke-static {p1, p2, v0, v1}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    iget-object v2, p0, Lsc1;->a:Lokio/a;

    invoke-virtual {v2, v0, v1}, Lokio/a;->skip(J)V

    sub-long/2addr p1, v0

    goto :goto_0

    :cond_2
    return-void

    :cond_3
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "closed"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public timeout()Lmw1;
    .locals 1

    .line 1
    iget-object v0, p0, Lsc1;->b:Luo1;

    invoke-interface {v0}, Luo1;->timeout()Lmw1;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "buffer("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lsc1;->b:Luo1;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public w(J)Lokio/ByteString;
    .locals 1

    .line 1
    invoke-virtual {p0, p1, p2}, Lsc1;->a0(J)V

    iget-object v0, p0, Lsc1;->a:Lokio/a;

    invoke-virtual {v0, p1, p2}, Lokio/a;->w(J)Lokio/ByteString;

    move-result-object p1

    return-object p1
.end method

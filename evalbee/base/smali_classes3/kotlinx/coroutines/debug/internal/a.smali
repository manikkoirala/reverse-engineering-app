.class public final Lkotlinx/coroutines/debug/internal/a;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lkotlinx/coroutines/debug/internal/a$a;,
        Lkotlinx/coroutines/debug/internal/a$b;,
        Lkotlinx/coroutines/debug/internal/a$c;
    }
.end annotation


# static fields
.field public static final a:Lkotlinx/coroutines/debug/internal/a;

.field public static final b:Ljava/lang/StackTraceElement;

.field public static final c:Ljava/text/SimpleDateFormat;

.field public static final d:Lkotlinx/coroutines/debug/internal/ConcurrentWeakMap;

.field public static e:Z

.field public static f:Z

.field public static final g:Lc90;

.field public static final h:Lkotlinx/coroutines/debug/internal/ConcurrentWeakMap;

.field public static final i:Lkotlinx/coroutines/debug/internal/a$b;

.field public static final j:Lkotlinx/coroutines/debug/internal/a$c;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    new-instance v0, Lkotlinx/coroutines/debug/internal/a;

    invoke-direct {v0}, Lkotlinx/coroutines/debug/internal/a;-><init>()V

    sput-object v0, Lkotlinx/coroutines/debug/internal/a;->a:Lkotlinx/coroutines/debug/internal/a;

    new-instance v1, Lc9;

    invoke-direct {v1}, Lc9;-><init>()V

    invoke-virtual {v1}, Lc9;->b()Ljava/lang/StackTraceElement;

    move-result-object v1

    sput-object v1, Lkotlinx/coroutines/debug/internal/a;->b:Ljava/lang/StackTraceElement;

    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v2, "yyyy/MM/dd HH:mm:ss"

    invoke-direct {v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    sput-object v1, Lkotlinx/coroutines/debug/internal/a;->c:Ljava/text/SimpleDateFormat;

    new-instance v1, Lkotlinx/coroutines/debug/internal/ConcurrentWeakMap;

    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-direct {v1, v2, v3, v4}, Lkotlinx/coroutines/debug/internal/ConcurrentWeakMap;-><init>(ZILgq;)V

    sput-object v1, Lkotlinx/coroutines/debug/internal/a;->d:Lkotlinx/coroutines/debug/internal/ConcurrentWeakMap;

    sput-boolean v3, Lkotlinx/coroutines/debug/internal/a;->e:Z

    sput-boolean v3, Lkotlinx/coroutines/debug/internal/a;->f:Z

    invoke-virtual {v0}, Lkotlinx/coroutines/debug/internal/a;->c()Lc90;

    move-result-object v0

    sput-object v0, Lkotlinx/coroutines/debug/internal/a;->g:Lc90;

    new-instance v0, Lkotlinx/coroutines/debug/internal/ConcurrentWeakMap;

    invoke-direct {v0, v3}, Lkotlinx/coroutines/debug/internal/ConcurrentWeakMap;-><init>(Z)V

    sput-object v0, Lkotlinx/coroutines/debug/internal/a;->h:Lkotlinx/coroutines/debug/internal/ConcurrentWeakMap;

    new-instance v0, Lkotlinx/coroutines/debug/internal/a$b;

    invoke-direct {v0, v4}, Lkotlinx/coroutines/debug/internal/a$b;-><init>(Lgq;)V

    sput-object v0, Lkotlinx/coroutines/debug/internal/a;->i:Lkotlinx/coroutines/debug/internal/a$b;

    new-instance v0, Lkotlinx/coroutines/debug/internal/a$c;

    invoke-direct {v0, v4}, Lkotlinx/coroutines/debug/internal/a$c;-><init>(Lgq;)V

    sput-object v0, Lkotlinx/coroutines/debug/internal/a;->j:Lkotlinx/coroutines/debug/internal/a$c;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final synthetic a()Lkotlinx/coroutines/debug/internal/ConcurrentWeakMap;
    .locals 1

    .line 1
    sget-object v0, Lkotlinx/coroutines/debug/internal/a;->h:Lkotlinx/coroutines/debug/internal/ConcurrentWeakMap;

    return-object v0
.end method

.method public static final synthetic b(Lkotlinx/coroutines/debug/internal/a;Lkotlinx/coroutines/debug/internal/a$a;)Z
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lkotlinx/coroutines/debug/internal/a;->d(Lkotlinx/coroutines/debug/internal/a$a;)Z

    move-result p0

    return p0
.end method


# virtual methods
.method public final c()Lc90;
    .locals 2

    .line 1
    :try_start_0
    sget-object v0, Lkotlin/Result;->Companion:Lkotlin/Result$a;

    const-string v0, "kotlinx.coroutines.debug.internal.ByteBuddyDynamicAttach"

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getConstructors()[Ljava/lang/reflect/Constructor;

    move-result-object v0

    const/4 v1, 0x0

    aget-object v0, v0, v1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    const-string v1, "null cannot be cast to non-null type kotlin.Function1<kotlin.Boolean, kotlin.Unit>"

    invoke-static {v0, v1}, Lfg0;->c(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lmz1;->a(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lc90;

    invoke-static {v0}, Lkotlin/Result;->constructor-impl(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    sget-object v1, Lkotlin/Result;->Companion:Lkotlin/Result$a;

    invoke-static {v0}, Lxe1;->a(Ljava/lang/Throwable;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lkotlin/Result;->constructor-impl(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    :goto_0
    invoke-static {v0}, Lkotlin/Result;->isFailure-impl(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    check-cast v0, Lc90;

    return-object v0
.end method

.method public final d(Lkotlinx/coroutines/debug/internal/a$a;)Z
    .locals 0

    .line 1
    const/4 p1, 0x0

    throw p1
.end method

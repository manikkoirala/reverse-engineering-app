.class public Lyd;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Closeable;
.implements Ljava/lang/Iterable;


# static fields
.field public static final n:Ljava/util/List;


# instance fields
.field public a:Lqd0;

.field public b:I

.field public c:Ljava/io/BufferedReader;

.field public d:Lxj0;

.field public e:Z

.field public f:Z

.field public g:Z

.field public h:Z

.field public i:I

.field public j:Ljava/util/Locale;

.field public k:J

.field public l:J

.field public m:[Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .line 1
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/Class;

    const/4 v1, 0x0

    const-class v2, Ljava/nio/charset/CharacterCodingException;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-class v2, Ljava/io/CharConversionException;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-class v2, Ljava/io/UnsupportedEncodingException;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-class v2, Ljava/io/UTFDataFormatException;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-class v2, Ljava/util/zip/ZipException;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-class v2, Ljava/io/FileNotFoundException;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lyd;->n:Ljava/util/List;

    return-void
.end method

.method public constructor <init>(Ljava/io/Reader;)V
    .locals 3

    .line 1
    const/16 v0, 0x22

    const/16 v1, 0x5c

    const/16 v2, 0x2c

    invoke-direct {p0, p1, v2, v0, v1}, Lyd;-><init>(Ljava/io/Reader;CCC)V

    return-void
.end method

.method public constructor <init>(Ljava/io/Reader;CCC)V
    .locals 7

    .line 2
    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    invoke-direct/range {v0 .. v6}, Lyd;-><init>(Ljava/io/Reader;CCCIZ)V

    return-void
.end method

.method public constructor <init>(Ljava/io/Reader;CCCIZ)V
    .locals 8

    .line 3
    const/4 v7, 0x1

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    invoke-direct/range {v0 .. v7}, Lyd;-><init>(Ljava/io/Reader;CCCIZZ)V

    return-void
.end method

.method public constructor <init>(Ljava/io/Reader;CCCIZZ)V
    .locals 10

    .line 4
    new-instance v9, Lxd;

    const/4 v6, 0x0

    sget-object v7, Lqd0;->a:Lcom/opencsv/enums/CSVReaderNullFieldIndicator;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v8

    move-object v0, v9

    move v1, p2

    move v2, p3

    move v3, p4

    move/from16 v4, p6

    move/from16 v5, p7

    invoke-direct/range {v0 .. v8}, Lxd;-><init>(CCCZZZLcom/opencsv/enums/CSVReaderNullFieldIndicator;Ljava/util/Locale;)V

    move-object v0, p0

    move-object v1, p1

    move v2, p5

    invoke-direct {p0, p1, p5, v9}, Lyd;-><init>(Ljava/io/Reader;ILqd0;)V

    return-void
.end method

.method public constructor <init>(Ljava/io/Reader;ILqd0;)V
    .locals 8

    .line 5
    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v7

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v7}, Lyd;-><init>(Ljava/io/Reader;ILqd0;ZZILjava/util/Locale;)V

    return-void
.end method

.method public constructor <init>(Ljava/io/Reader;ILqd0;ZZILjava/util/Locale;)V
    .locals 2

    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lyd;->e:Z

    const/4 v0, 0x0

    iput v0, p0, Lyd;->i:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lyd;->k:J

    iput-wide v0, p0, Lyd;->l:J

    const/4 v0, 0x0

    iput-object v0, p0, Lyd;->m:[Ljava/lang/String;

    instance-of v0, p1, Ljava/io/BufferedReader;

    if-eqz v0, :cond_0

    check-cast p1, Ljava/io/BufferedReader;

    goto :goto_0

    :cond_0
    new-instance v0, Ljava/io/BufferedReader;

    invoke-direct {v0, p1}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    move-object p1, v0

    :goto_0
    iput-object p1, p0, Lyd;->c:Ljava/io/BufferedReader;

    new-instance v0, Lxj0;

    invoke-direct {v0, p1, p4}, Lxj0;-><init>(Ljava/io/BufferedReader;Z)V

    iput-object v0, p0, Lyd;->d:Lxj0;

    iput p2, p0, Lyd;->b:I

    iput-object p3, p0, Lyd;->a:Lqd0;

    iput-boolean p4, p0, Lyd;->g:Z

    iput-boolean p5, p0, Lyd;->h:Z

    iput p6, p0, Lyd;->i:I

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object p1

    invoke-static {p7, p1}, Lorg/apache/commons/lang3/ObjectUtils;->defaultIfNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Locale;

    iput-object p1, p0, Lyd;->j:Ljava/util/Locale;

    return-void
.end method


# virtual methods
.method public a([Ljava/lang/String;[Ljava/lang/String;)[Ljava/lang/String;
    .locals 3

    .line 1
    array-length v0, p1

    array-length v1, p2

    add-int/2addr v0, v1

    new-array v0, v0, [Ljava/lang/String;

    array-length v1, p1

    const/4 v2, 0x0

    invoke-static {p1, v2, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    array-length p1, p1

    array-length v1, p2

    invoke-static {p2, v2, v0, p1, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v0
.end method

.method public b()Ljava/lang/String;
    .locals 7

    .line 1
    invoke-virtual {p0}, Lyd;->isClosed()Z

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    iput-boolean v2, p0, Lyd;->e:Z

    return-object v1

    :cond_0
    iget-boolean v0, p0, Lyd;->f:Z

    const-wide/16 v3, 0x1

    if-nez v0, :cond_2

    move v0, v2

    :goto_0
    iget v5, p0, Lyd;->b:I

    if-ge v0, v5, :cond_1

    iget-object v5, p0, Lyd;->d:Lxj0;

    invoke-virtual {v5}, Lxj0;->a()Ljava/lang/String;

    iget-wide v5, p0, Lyd;->k:J

    add-long/2addr v5, v3

    iput-wide v5, p0, Lyd;->k:J

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lyd;->f:Z

    :cond_2
    iget-object v0, p0, Lyd;->d:Lxj0;

    invoke-virtual {v0}, Lxj0;->a()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_3

    iput-boolean v2, p0, Lyd;->e:Z

    goto :goto_1

    :cond_3
    iget-wide v5, p0, Lyd;->k:J

    add-long/2addr v5, v3

    iput-wide v5, p0, Lyd;->k:J

    :goto_1
    iget-boolean v2, p0, Lyd;->e:Z

    if-eqz v2, :cond_4

    move-object v1, v0

    :cond_4
    return-object v1
.end method

.method public c()[Ljava/lang/String;
    .locals 18

    .line 1
    move-object/from16 v0, p0

    iget-object v1, v0, Lyd;->m:[Ljava/lang/String;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    iput-object v2, v0, Lyd;->m:[Ljava/lang/String;

    return-object v1

    :cond_0
    iget-wide v3, v0, Lyd;->k:J

    const/4 v1, 0x0

    move v5, v1

    :cond_1
    invoke-virtual/range {p0 .. p0}, Lyd;->b()Ljava/lang/String;

    move-result-object v6

    add-int/lit8 v5, v5, 0x1

    iget-boolean v7, v0, Lyd;->e:Z

    const-wide/16 v8, 0x1

    const-string v10, "opencsv"

    const/16 v11, 0x64

    if-nez v7, :cond_3

    iget-object v1, v0, Lyd;->a:Lqd0;

    invoke-interface {v1}, Lqd0;->c()Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {v0, v2}, Lyd;->g([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    return-object v1

    :cond_2
    new-instance v1, Lcom/opencsv/exceptions/CsvMalformedLineException;

    iget-object v2, v0, Lyd;->j:Ljava/util/Locale;

    invoke-static {v10, v2}, Ljava/util/ResourceBundle;->getBundle(Ljava/lang/String;Ljava/util/Locale;)Ljava/util/ResourceBundle;

    move-result-object v2

    const-string v5, "unterminated.quote"

    invoke-virtual {v2, v5}, Ljava/util/ResourceBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v5, v0, Lyd;->a:Lqd0;

    invoke-interface {v5}, Lqd0;->b()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5, v11}, Lorg/apache/commons/lang3/StringUtils;->abbreviate(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v5

    filled-new-array {v5}, [Ljava/lang/Object;

    move-result-object v5

    invoke-static {v2, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    add-long/2addr v3, v8

    iget-object v5, v0, Lyd;->a:Lqd0;

    invoke-interface {v5}, Lqd0;->b()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/opencsv/exceptions/CsvMalformedLineException;-><init>(Ljava/lang/String;JLjava/lang/String;)V

    throw v1

    :cond_3
    iget v7, v0, Lyd;->i:I

    if-lez v7, :cond_5

    if-le v5, v7, :cond_5

    iget-wide v2, v0, Lyd;->l:J

    add-long v14, v2, v8

    iget-object v2, v0, Lyd;->a:Lqd0;

    invoke-interface {v2}, Lqd0;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-le v3, v11, :cond_4

    invoke-virtual {v2, v1, v11}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    :cond_4
    iget-object v1, v0, Lyd;->j:Ljava/util/Locale;

    invoke-static {v10, v1}, Ljava/util/ResourceBundle;->getBundle(Ljava/lang/String;Ljava/util/Locale;)Ljava/util/ResourceBundle;

    move-result-object v1

    const-string v3, "multiline.limit.broken"

    invoke-virtual {v1, v3}, Ljava/util/ResourceBundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v3, v0, Lyd;->j:Ljava/util/Locale;

    iget v4, v0, Lyd;->i:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    filled-new-array {v4, v5, v2}, [Ljava/lang/Object;

    move-result-object v2

    invoke-static {v3, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    new-instance v1, Lcom/opencsv/exceptions/CsvMultilineLimitBrokenException;

    iget-object v2, v0, Lyd;->a:Lqd0;

    invoke-interface {v2}, Lqd0;->b()Ljava/lang/String;

    move-result-object v16

    iget v2, v0, Lyd;->i:I

    move-object v12, v1

    move/from16 v17, v2

    invoke-direct/range {v12 .. v17}, Lcom/opencsv/exceptions/CsvMultilineLimitBrokenException;-><init>(Ljava/lang/String;JLjava/lang/String;I)V

    throw v1

    :cond_5
    iget-object v7, v0, Lyd;->a:Lqd0;

    invoke-interface {v7, v6}, Lqd0;->a(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    array-length v7, v6

    if-lez v7, :cond_7

    if-nez v2, :cond_6

    move-object v2, v6

    goto :goto_0

    :cond_6
    invoke-virtual {v0, v2, v6}, Lyd;->a([Ljava/lang/String;[Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    :cond_7
    :goto_0
    iget-object v6, v0, Lyd;->a:Lqd0;

    invoke-interface {v6}, Lqd0;->c()Z

    move-result v6

    if-nez v6, :cond_1

    invoke-virtual {v0, v2}, Lyd;->g([Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public close()V
    .locals 1

    .line 1
    iget-object v0, p0, Lyd;->c:Ljava/io/BufferedReader;

    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V

    return-void
.end method

.method public g([Ljava/lang/String;)[Ljava/lang/String;
    .locals 4

    .line 1
    if-eqz p1, :cond_0

    iget-wide v0, p0, Lyd;->l:J

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    iput-wide v0, p0, Lyd;->l:J

    :cond_0
    return-object p1
.end method

.method public isClosed()Z
    .locals 4

    .line 1
    iget-boolean v0, p0, Lyd;->h:Z

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    :cond_0
    const/4 v0, 0x1

    :try_start_0
    iget-object v2, p0, Lyd;->c:Ljava/io/BufferedReader;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Ljava/io/BufferedReader;->mark(I)V

    iget-object v2, p0, Lyd;->c:Ljava/io/BufferedReader;

    invoke-virtual {v2}, Ljava/io/BufferedReader;->read()I

    move-result v2

    iget-object v3, p0, Lyd;->c:Ljava/io/BufferedReader;

    invoke-virtual {v3}, Ljava/io/BufferedReader;->reset()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v3, -0x1

    if-ne v2, v3, :cond_1

    move v1, v0

    :cond_1
    return v1

    :catch_0
    move-exception v1

    sget-object v2, Lyd;->n:Ljava/util/List;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    return v0

    :cond_2
    throw v1
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 2

    .line 1
    :try_start_0
    new-instance v0, Lwd;

    invoke-direct {v0, p0}, Lwd;-><init>(Lyd;)V

    iget-object v1, p0, Lyd;->j:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Lwd;->c(Ljava/util/Locale;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

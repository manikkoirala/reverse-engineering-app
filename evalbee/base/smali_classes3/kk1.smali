.class public final Lkk1;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:[B

.field public b:I

.field public c:I

.field public d:Z

.field public e:Z

.field public f:Lkk1;

.field public g:Lkk1;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0x2000

    new-array v0, v0, [B

    iput-object v0, p0, Lkk1;->a:[B

    const/4 v0, 0x1

    iput-boolean v0, p0, Lkk1;->e:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lkk1;->d:Z

    return-void
.end method

.method public constructor <init>([BIIZZ)V
    .locals 0

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lkk1;->a:[B

    iput p2, p0, Lkk1;->b:I

    iput p3, p0, Lkk1;->c:I

    iput-boolean p4, p0, Lkk1;->d:Z

    iput-boolean p5, p0, Lkk1;->e:Z

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .line 1
    iget-object v0, p0, Lkk1;->g:Lkk1;

    if-eq v0, p0, :cond_3

    iget-boolean v1, v0, Lkk1;->e:Z

    if-nez v1, :cond_0

    return-void

    :cond_0
    iget v1, p0, Lkk1;->c:I

    iget v2, p0, Lkk1;->b:I

    sub-int/2addr v1, v2

    iget v2, v0, Lkk1;->c:I

    rsub-int v2, v2, 0x2000

    iget-boolean v3, v0, Lkk1;->d:Z

    if-eqz v3, :cond_1

    const/4 v3, 0x0

    goto :goto_0

    :cond_1
    iget v3, v0, Lkk1;->b:I

    :goto_0
    add-int/2addr v2, v3

    if-le v1, v2, :cond_2

    return-void

    :cond_2
    invoke-virtual {p0, v0, v1}, Lkk1;->f(Lkk1;I)V

    invoke-virtual {p0}, Lkk1;->b()Lkk1;

    invoke-static {p0}, Lnk1;->a(Lkk1;)V

    return-void

    :cond_3
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0
.end method

.method public final b()Lkk1;
    .locals 4

    .line 1
    iget-object v0, p0, Lkk1;->f:Lkk1;

    const/4 v1, 0x0

    if-eq v0, p0, :cond_0

    move-object v2, v0

    goto :goto_0

    :cond_0
    move-object v2, v1

    :goto_0
    iget-object v3, p0, Lkk1;->g:Lkk1;

    iput-object v0, v3, Lkk1;->f:Lkk1;

    iget-object v0, p0, Lkk1;->f:Lkk1;

    iput-object v3, v0, Lkk1;->g:Lkk1;

    iput-object v1, p0, Lkk1;->f:Lkk1;

    iput-object v1, p0, Lkk1;->g:Lkk1;

    return-object v2
.end method

.method public final c(Lkk1;)Lkk1;
    .locals 1

    .line 1
    iput-object p0, p1, Lkk1;->g:Lkk1;

    iget-object v0, p0, Lkk1;->f:Lkk1;

    iput-object v0, p1, Lkk1;->f:Lkk1;

    iget-object v0, p0, Lkk1;->f:Lkk1;

    iput-object p1, v0, Lkk1;->g:Lkk1;

    iput-object p1, p0, Lkk1;->f:Lkk1;

    return-object p1
.end method

.method public final d()Lkk1;
    .locals 7

    .line 1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lkk1;->d:Z

    new-instance v0, Lkk1;

    iget-object v2, p0, Lkk1;->a:[B

    iget v3, p0, Lkk1;->b:I

    iget v4, p0, Lkk1;->c:I

    const/4 v5, 0x1

    const/4 v6, 0x0

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lkk1;-><init>([BIIZZ)V

    return-object v0
.end method

.method public final e(I)Lkk1;
    .locals 5

    .line 1
    if-lez p1, :cond_1

    iget v0, p0, Lkk1;->c:I

    iget v1, p0, Lkk1;->b:I

    sub-int/2addr v0, v1

    if-gt p1, v0, :cond_1

    const/16 v0, 0x400

    if-lt p1, v0, :cond_0

    invoke-virtual {p0}, Lkk1;->d()Lkk1;

    move-result-object v0

    goto :goto_0

    :cond_0
    invoke-static {}, Lnk1;->b()Lkk1;

    move-result-object v0

    iget-object v1, p0, Lkk1;->a:[B

    iget v2, p0, Lkk1;->b:I

    iget-object v3, v0, Lkk1;->a:[B

    const/4 v4, 0x0

    invoke-static {v1, v2, v3, v4, p1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :goto_0
    iget v1, v0, Lkk1;->b:I

    add-int/2addr v1, p1

    iput v1, v0, Lkk1;->c:I

    iget v1, p0, Lkk1;->b:I

    add-int/2addr v1, p1

    iput v1, p0, Lkk1;->b:I

    iget-object p1, p0, Lkk1;->g:Lkk1;

    invoke-virtual {p1, v0}, Lkk1;->c(Lkk1;)Lkk1;

    return-object v0

    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    invoke-direct {p1}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw p1
.end method

.method public final f(Lkk1;I)V
    .locals 4

    .line 1
    iget-boolean v0, p1, Lkk1;->e:Z

    if-eqz v0, :cond_3

    iget v0, p1, Lkk1;->c:I

    add-int v1, v0, p2

    const/16 v2, 0x2000

    if-le v1, v2, :cond_2

    iget-boolean v1, p1, Lkk1;->d:Z

    if-nez v1, :cond_1

    add-int v1, v0, p2

    iget v3, p1, Lkk1;->b:I

    sub-int/2addr v1, v3

    if-gt v1, v2, :cond_0

    iget-object v1, p1, Lkk1;->a:[B

    sub-int/2addr v0, v3

    const/4 v2, 0x0

    invoke-static {v1, v3, v1, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget v0, p1, Lkk1;->c:I

    iget v1, p1, Lkk1;->b:I

    sub-int/2addr v0, v1

    iput v0, p1, Lkk1;->c:I

    iput v2, p1, Lkk1;->b:I

    goto :goto_0

    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    invoke-direct {p1}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw p1

    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    invoke-direct {p1}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw p1

    :cond_2
    :goto_0
    iget-object v0, p0, Lkk1;->a:[B

    iget v1, p0, Lkk1;->b:I

    iget-object v2, p1, Lkk1;->a:[B

    iget v3, p1, Lkk1;->c:I

    invoke-static {v0, v1, v2, v3, p2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget v0, p1, Lkk1;->c:I

    add-int/2addr v0, p2

    iput v0, p1, Lkk1;->c:I

    iget p1, p0, Lkk1;->b:I

    add-int/2addr p1, p2

    iput p1, p0, Lkk1;->b:I

    return-void

    :cond_3
    new-instance p1, Ljava/lang/IllegalArgumentException;

    invoke-direct {p1}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw p1
.end method

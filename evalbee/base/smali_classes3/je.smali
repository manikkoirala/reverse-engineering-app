.class public interface abstract Lje;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/xmlbeans/XmlObject;


# virtual methods
.method public abstract getBlockSize()I
.end method

.method public abstract getCipherAlgorithm()Lcom/microsoft/schemas/office/x2006/encryption/STCipherAlgorithm$Enum;
.end method

.method public abstract getCipherChaining()Lcom/microsoft/schemas/office/x2006/encryption/STCipherChaining$Enum;
.end method

.method public abstract getEncryptedKeyValue()[B
.end method

.method public abstract getEncryptedVerifierHashInput()[B
.end method

.method public abstract getEncryptedVerifierHashValue()[B
.end method

.method public abstract getHashAlgorithm()Lcom/microsoft/schemas/office/x2006/encryption/STHashAlgorithm$Enum;
.end method

.method public abstract getHashSize()I
.end method

.method public abstract getKeyBits()J
.end method

.method public abstract getSaltSize()I
.end method

.method public abstract getSaltValue()[B
.end method

.method public abstract getSpinCount()I
.end method

.method public abstract setBlockSize(I)V
.end method

.method public abstract setCipherAlgorithm(Lcom/microsoft/schemas/office/x2006/encryption/STCipherAlgorithm$Enum;)V
.end method

.method public abstract setCipherChaining(Lcom/microsoft/schemas/office/x2006/encryption/STCipherChaining$Enum;)V
.end method

.method public abstract setEncryptedKeyValue([B)V
.end method

.method public abstract setEncryptedVerifierHashInput([B)V
.end method

.method public abstract setEncryptedVerifierHashValue([B)V
.end method

.method public abstract setHashAlgorithm(Lcom/microsoft/schemas/office/x2006/encryption/STHashAlgorithm$Enum;)V
.end method

.method public abstract setHashSize(I)V
.end method

.method public abstract setKeyBits(J)V
.end method

.method public abstract setSaltSize(I)V
.end method

.method public abstract setSaltValue([B)V
.end method

.method public abstract setSpinCount(I)V
.end method

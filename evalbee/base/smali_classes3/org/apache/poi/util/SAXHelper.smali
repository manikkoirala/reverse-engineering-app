.class public final Lorg/apache/poi/util/SAXHelper;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field static final IGNORING_ENTITY_RESOLVER:Lorg/xml/sax/EntityResolver;

.field private static lastLog:J

.field private static final logger:Lorg/apache/poi/util/POILogger;

.field private static final saxFactory:Ljavax/xml/parsers/SAXParserFactory;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    const-string v0, "Failed to create SAXParserFactory"

    const-class v1, Lorg/apache/poi/util/SAXHelper;

    invoke-static {v1}, Lorg/apache/poi/util/POILogFactory;->getLogger(Ljava/lang/Class;)Lorg/apache/poi/util/POILogger;

    move-result-object v1

    sput-object v1, Lorg/apache/poi/util/SAXHelper;->logger:Lorg/apache/poi/util/POILogger;

    new-instance v1, Lorg/apache/poi/util/SAXHelper$1;

    invoke-direct {v1}, Lorg/apache/poi/util/SAXHelper$1;-><init>()V

    sput-object v1, Lorg/apache/poi/util/SAXHelper;->IGNORING_ENTITY_RESOLVER:Lorg/xml/sax/EntityResolver;

    const/4 v1, 0x5

    :try_start_0
    invoke-static {}, Ljavax/xml/parsers/SAXParserFactory;->newInstance()Ljavax/xml/parsers/SAXParserFactory;

    move-result-object v2

    sput-object v2, Lorg/apache/poi/util/SAXHelper;->saxFactory:Ljavax/xml/parsers/SAXParserFactory;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljavax/xml/parsers/SAXParserFactory;->setValidating(Z)V

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljavax/xml/parsers/SAXParserFactory;->setNamespaceAware(Z)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Error; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v2

    sget-object v3, Lorg/apache/poi/util/SAXHelper;->logger:Lorg/apache/poi/util/POILogger;

    filled-new-array {v0, v2}, [Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v3, v1, v0}, Lorg/apache/poi/util/POILogger;->log(I[Ljava/lang/Object;)V

    throw v2

    :catch_1
    move-exception v2

    sget-object v3, Lorg/apache/poi/util/SAXHelper;->logger:Lorg/apache/poi/util/POILogger;

    filled-new-array {v0, v2}, [Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v1, v4}, Lorg/apache/poi/util/POILogger;->log(I[Ljava/lang/Object;)V

    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    :catch_2
    move-exception v2

    sget-object v3, Lorg/apache/poi/util/SAXHelper;->logger:Lorg/apache/poi/util/POILogger;

    filled-new-array {v0, v2}, [Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v3, v1, v0}, Lorg/apache/poi/util/POILogger;->log(I[Ljava/lang/Object;)V

    throw v2
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static declared-synchronized newXMLReader()Lorg/xml/sax/XMLReader;
    .locals 3

    const-class v0, Lorg/apache/poi/util/SAXHelper;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lorg/apache/poi/util/SAXHelper;->saxFactory:Ljavax/xml/parsers/SAXParserFactory;

    invoke-virtual {v1}, Ljavax/xml/parsers/SAXParserFactory;->newSAXParser()Ljavax/xml/parsers/SAXParser;

    move-result-object v1

    invoke-virtual {v1}, Ljavax/xml/parsers/SAXParser;->getXMLReader()Lorg/xml/sax/XMLReader;

    move-result-object v1

    sget-object v2, Lorg/apache/poi/util/SAXHelper;->IGNORING_ENTITY_RESOLVER:Lorg/xml/sax/EntityResolver;

    invoke-interface {v1, v2}, Lorg/xml/sax/XMLReader;->setEntityResolver(Lorg/xml/sax/EntityResolver;)V

    const-string v2, "http://javax.xml.XMLConstants/feature/secure-processing"

    invoke-static {v1, v2}, Lorg/apache/poi/util/SAXHelper;->trySetSAXFeature(Lorg/xml/sax/XMLReader;Ljava/lang/String;)V

    invoke-static {v1}, Lorg/apache/poi/util/SAXHelper;->trySetXercesSecurityManager(Lorg/xml/sax/XMLReader;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-object v1

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method private static trySetSAXFeature(Lorg/xml/sax/XMLReader;Ljava/lang/String;)V
    .locals 3

    const/4 v0, 0x1

    const/4 v1, 0x5

    :try_start_0
    invoke-interface {p0, p1, v0}, Lorg/xml/sax/XMLReader;->setFeature(Ljava/lang/String;Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/AbstractMethodError; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    sget-object v0, Lorg/apache/poi/util/SAXHelper;->logger:Lorg/apache/poi/util/POILogger;

    const-string v2, "Cannot set SAX feature because outdated XML parser in classpath"

    filled-new-array {v2, p1, p0}, [Ljava/lang/Object;

    move-result-object p0

    invoke-virtual {v0, v1, p0}, Lorg/apache/poi/util/POILogger;->log(I[Ljava/lang/Object;)V

    goto :goto_0

    :catch_1
    move-exception p0

    sget-object v0, Lorg/apache/poi/util/SAXHelper;->logger:Lorg/apache/poi/util/POILogger;

    const-string v2, "SAX Feature unsupported"

    filled-new-array {v2, p1, p0}, [Ljava/lang/Object;

    move-result-object p0

    invoke-virtual {v0, v1, p0}, Lorg/apache/poi/util/POILogger;->log(I[Ljava/lang/Object;)V

    :goto_0
    return-void
.end method

.method private static trySetXercesSecurityManager(Lorg/xml/sax/XMLReader;)V
    .locals 11

    const-string v0, "com.sun.org.apache.xerces.internal.util.SecurityManager"

    const-string v1, "org.apache.xerces.util.SecurityManager"

    filled-new-array {v0, v1}, [Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    const/4 v3, 0x2

    if-ge v2, v3, :cond_1

    aget-object v3, v0, v2

    :try_start_0
    invoke-static {v3}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    const-string v5, "setEntityExpansionLimit"

    const/4 v6, 0x1

    new-array v7, v6, [Ljava/lang/Class;

    sget-object v8, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v8, v7, v1

    invoke-virtual {v4, v5, v7}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v4

    new-array v5, v6, [Ljava/lang/Object;

    const/16 v6, 0x1000

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v1

    invoke-virtual {v4, v3, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    const-string v4, "http://apache.org/xml/properties/security-manager"

    invoke-interface {p0, v4, v3}, Lorg/xml/sax/XMLReader;->setProperty(Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sget-wide v6, Lorg/apache/poi/util/SAXHelper;->lastLog:J

    sget-object v8, Ljava/util/concurrent/TimeUnit;->MINUTES:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v9, 0x5

    invoke-virtual {v8, v9, v10}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v8

    add-long/2addr v6, v8

    cmp-long v4, v4, v6

    if-lez v4, :cond_0

    sget-object v4, Lorg/apache/poi/util/SAXHelper;->logger:Lorg/apache/poi/util/POILogger;

    const-string v5, "SAX Security Manager could not be setup [log suppressed for 5 minutes]"

    filled-new-array {v5, v3}, [Ljava/lang/Object;

    move-result-object v3

    const/4 v5, 0x5

    invoke-virtual {v4, v5, v3}, Lorg/apache/poi/util/POILogger;->log(I[Ljava/lang/Object;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    sput-wide v3, Lorg/apache/poi/util/SAXHelper;->lastLog:J

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

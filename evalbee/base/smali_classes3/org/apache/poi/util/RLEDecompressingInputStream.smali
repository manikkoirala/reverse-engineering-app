.class public Lorg/apache/poi/util/RLEDecompressingInputStream;
.super Ljava/io/InputStream;
.source "SourceFile"


# static fields
.field private static final POWER2:[I


# instance fields
.field private final buf:[B

.field private final in:Ljava/io/InputStream;

.field private len:I

.field private pos:I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    const/16 v0, 0x10

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lorg/apache/poi/util/RLEDecompressingInputStream;->POWER2:[I

    return-void

    :array_0
    .array-data 4
        0x1
        0x2
        0x4
        0x8
        0x10
        0x20
        0x40
        0x80
        0x100
        0x200
        0x400
        0x800
        0x1000
        0x2000
        0x4000
        0x8000
    .end array-data
.end method

.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 3

    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    iput-object p1, p0, Lorg/apache/poi/util/RLEDecompressingInputStream;->in:Ljava/io/InputStream;

    const/16 v0, 0x1000

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/apache/poi/util/RLEDecompressingInputStream;->buf:[B

    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/poi/util/RLEDecompressingInputStream;->pos:I

    invoke-virtual {p1}, Ljava/io/InputStream;->read()I

    move-result p1

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    invoke-direct {p0}, Lorg/apache/poi/util/RLEDecompressingInputStream;->readChunk()I

    move-result p1

    iput p1, p0, Lorg/apache/poi/util/RLEDecompressingInputStream;->len:I

    return-void

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    sget-object v1, Ljava/util/Locale;->ROOT:Ljava/util/Locale;

    and-int/lit16 p1, p1, 0xff

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    filled-new-array {p1}, [Ljava/lang/Object;

    move-result-object p1

    const-string v2, "Header byte 0x01 expected, received 0x%02X"

    invoke-static {v1, v2, p1}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static decompress([B)[B
    .locals 2

    .line 1
    array-length v0, p0

    const/4 v1, 0x0

    invoke-static {p0, v1, v0}, Lorg/apache/poi/util/RLEDecompressingInputStream;->decompress([BII)[B

    move-result-object p0

    return-object p0
.end method

.method public static decompress([BII)[B
    .locals 2

    .line 2
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    new-instance v1, Ljava/io/ByteArrayInputStream;

    invoke-direct {v1, p0, p1, p2}, Ljava/io/ByteArrayInputStream;-><init>([BII)V

    new-instance p0, Lorg/apache/poi/util/RLEDecompressingInputStream;

    invoke-direct {p0, v1}, Lorg/apache/poi/util/RLEDecompressingInputStream;-><init>(Ljava/io/InputStream;)V

    invoke-static {p0, v0}, Lorg/apache/poi/util/IOUtils;->copy(Ljava/io/InputStream;Ljava/io/OutputStream;)V

    invoke-virtual {p0}, Ljava/io/InputStream;->close()V

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->close()V

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object p0

    return-object p0
.end method

.method public static getCopyLenBits(I)I
    .locals 2

    const/16 v0, 0xb

    :goto_0
    const/4 v1, 0x4

    if-lt v0, v1, :cond_1

    sget-object v1, Lorg/apache/poi/util/RLEDecompressingInputStream;->POWER2:[I

    aget v1, v1, v0

    and-int/2addr v1, p0

    if-eqz v1, :cond_0

    rsub-int/lit8 p0, v0, 0xf

    return p0

    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_1
    const/16 p0, 0xc

    return p0
.end method

.method private readChunk()I
    .locals 13

    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/poi/util/RLEDecompressingInputStream;->pos:I

    iget-object v1, p0, Lorg/apache/poi/util/RLEDecompressingInputStream;->in:Ljava/io/InputStream;

    invoke-direct {p0, v1}, Lorg/apache/poi/util/RLEDecompressingInputStream;->readShort(Ljava/io/InputStream;)I

    move-result v1

    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    return v2

    :cond_0
    and-int/lit16 v3, v1, 0xfff

    const/4 v4, 0x1

    add-int/2addr v3, v4

    and-int/lit16 v5, v1, 0x7000

    const/16 v6, 0x3000

    if-ne v5, v6, :cond_c

    const v5, 0x8000

    and-int/2addr v1, v5

    if-nez v1, :cond_1

    move v1, v4

    goto :goto_0

    :cond_1
    move v1, v0

    :goto_0
    if-eqz v1, :cond_3

    iget-object v1, p0, Lorg/apache/poi/util/RLEDecompressingInputStream;->in:Ljava/io/InputStream;

    iget-object v2, p0, Lorg/apache/poi/util/RLEDecompressingInputStream;->buf:[B

    invoke-virtual {v1, v2, v0, v3}, Ljava/io/InputStream;->read([BII)I

    move-result v0

    if-lt v0, v3, :cond_2

    return v3

    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    sget-object v1, Ljava/util/Locale;->ROOT:Ljava/util/Locale;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    filled-new-array {v2}, [Ljava/lang/Object;

    move-result-object v2

    const-string v3, "Not enough bytes read, expected %d"

    invoke-static {v1, v3, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    move v1, v0

    move v5, v1

    :cond_4
    :goto_1
    if-ge v1, v3, :cond_b

    iget-object v6, p0, Lorg/apache/poi/util/RLEDecompressingInputStream;->in:Ljava/io/InputStream;

    invoke-virtual {v6}, Ljava/io/InputStream;->read()I

    move-result v6

    add-int/lit8 v1, v1, 0x1

    if-ne v6, v2, :cond_5

    goto :goto_5

    :cond_5
    move v7, v0

    :goto_2
    const/16 v8, 0x8

    if-ge v7, v8, :cond_4

    if-lt v1, v3, :cond_6

    goto :goto_1

    :cond_6
    sget-object v8, Lorg/apache/poi/util/RLEDecompressingInputStream;->POWER2:[I

    aget v9, v8, v7

    and-int/2addr v9, v6

    if-nez v9, :cond_8

    iget-object v8, p0, Lorg/apache/poi/util/RLEDecompressingInputStream;->in:Ljava/io/InputStream;

    invoke-virtual {v8}, Ljava/io/InputStream;->read()I

    move-result v8

    if-ne v8, v2, :cond_7

    return v2

    :cond_7
    iget-object v9, p0, Lorg/apache/poi/util/RLEDecompressingInputStream;->buf:[B

    add-int/lit8 v10, v5, 0x1

    int-to-byte v8, v8

    aput-byte v8, v9, v5

    add-int/lit8 v1, v1, 0x1

    move v5, v10

    goto :goto_4

    :cond_8
    iget-object v9, p0, Lorg/apache/poi/util/RLEDecompressingInputStream;->in:Ljava/io/InputStream;

    invoke-direct {p0, v9}, Lorg/apache/poi/util/RLEDecompressingInputStream;->readShort(Ljava/io/InputStream;)I

    move-result v9

    if-ne v9, v2, :cond_9

    return v2

    :cond_9
    add-int/lit8 v1, v1, 0x2

    add-int/lit8 v10, v5, -0x1

    invoke-static {v10}, Lorg/apache/poi/util/RLEDecompressingInputStream;->getCopyLenBits(I)I

    move-result v10

    shr-int v11, v9, v10

    add-int/2addr v11, v4

    aget v8, v8, v10

    sub-int/2addr v8, v4

    and-int/2addr v8, v9

    add-int/lit8 v8, v8, 0x3

    sub-int v9, v5, v11

    add-int/2addr v8, v9

    :goto_3
    if-ge v9, v8, :cond_a

    iget-object v10, p0, Lorg/apache/poi/util/RLEDecompressingInputStream;->buf:[B

    add-int/lit8 v11, v5, 0x1

    aget-byte v12, v10, v9

    aput-byte v12, v10, v5

    add-int/lit8 v9, v9, 0x1

    move v5, v11

    goto :goto_3

    :cond_a
    :goto_4
    add-int/lit8 v7, v7, 0x1

    goto :goto_2

    :cond_b
    :goto_5
    return v5

    :cond_c
    new-instance v0, Ljava/lang/IllegalArgumentException;

    sget-object v2, Ljava/util/Locale;->ROOT:Ljava/util/Locale;

    const v3, 0xe000

    and-int/2addr v1, v3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    filled-new-array {v1}, [Ljava/lang/Object;

    move-result-object v1

    const-string v3, "Chunksize header A should be 0x3000, received 0x%04X"

    invoke-static {v2, v3, v1}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private readInt(Ljava/io/InputStream;)I
    .locals 4

    .line 2
    invoke-virtual {p1}, Ljava/io/InputStream;->read()I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    return v1

    :cond_0
    invoke-virtual {p1}, Ljava/io/InputStream;->read()I

    move-result v2

    if-ne v2, v1, :cond_1

    return v1

    :cond_1
    invoke-virtual {p1}, Ljava/io/InputStream;->read()I

    move-result v3

    if-ne v3, v1, :cond_2

    return v1

    :cond_2
    invoke-virtual {p1}, Ljava/io/InputStream;->read()I

    move-result p1

    if-ne p1, v1, :cond_3

    return v1

    :cond_3
    and-int/lit16 v0, v0, 0xff

    and-int/lit16 v1, v2, 0xff

    shl-int/lit8 v1, v1, 0x8

    or-int/2addr v0, v1

    and-int/lit16 v1, v3, 0xff

    shl-int/lit8 v1, v1, 0x10

    or-int/2addr v0, v1

    and-int/lit16 p1, p1, 0xff

    shl-int/lit8 p1, p1, 0x18

    or-int/2addr p1, v0

    return p1
.end method

.method private readShort(Ljava/io/InputStream;)I
    .locals 2

    .line 2
    invoke-virtual {p1}, Ljava/io/InputStream;->read()I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    return v1

    :cond_0
    invoke-virtual {p1}, Ljava/io/InputStream;->read()I

    move-result p1

    if-ne p1, v1, :cond_1

    return v1

    :cond_1
    and-int/lit16 v0, v0, 0xff

    and-int/lit16 p1, p1, 0xff

    shl-int/lit8 p1, p1, 0x8

    or-int/2addr p1, v0

    return p1
.end method


# virtual methods
.method public available()I
    .locals 2

    iget v0, p0, Lorg/apache/poi/util/RLEDecompressingInputStream;->len:I

    if-lez v0, :cond_0

    iget v1, p0, Lorg/apache/poi/util/RLEDecompressingInputStream;->pos:I

    sub-int/2addr v0, v1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public close()V
    .locals 1

    iget-object v0, p0, Lorg/apache/poi/util/RLEDecompressingInputStream;->in:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    return-void
.end method

.method public read()I
    .locals 3

    .line 1
    iget v0, p0, Lorg/apache/poi/util/RLEDecompressingInputStream;->len:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    return v1

    :cond_0
    iget v2, p0, Lorg/apache/poi/util/RLEDecompressingInputStream;->pos:I

    if-lt v2, v0, :cond_1

    invoke-direct {p0}, Lorg/apache/poi/util/RLEDecompressingInputStream;->readChunk()I

    move-result v0

    iput v0, p0, Lorg/apache/poi/util/RLEDecompressingInputStream;->len:I

    if-ne v0, v1, :cond_1

    return v1

    :cond_1
    iget-object v0, p0, Lorg/apache/poi/util/RLEDecompressingInputStream;->buf:[B

    iget v1, p0, Lorg/apache/poi/util/RLEDecompressingInputStream;->pos:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lorg/apache/poi/util/RLEDecompressingInputStream;->pos:I

    aget-byte v0, v0, v1

    and-int/lit16 v0, v0, 0xff

    return v0
.end method

.method public read([B)I
    .locals 2

    .line 2
    array-length v0, p1

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v1, v0}, Lorg/apache/poi/util/RLEDecompressingInputStream;->read([BII)I

    move-result p1

    return p1
.end method

.method public read([BII)I
    .locals 6

    .line 3
    iget v0, p0, Lorg/apache/poi/util/RLEDecompressingInputStream;->len:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    return v1

    :cond_0
    move v2, p2

    move v0, p3

    :goto_0
    if-lez v0, :cond_3

    iget v3, p0, Lorg/apache/poi/util/RLEDecompressingInputStream;->pos:I

    iget v4, p0, Lorg/apache/poi/util/RLEDecompressingInputStream;->len:I

    if-lt v3, v4, :cond_2

    invoke-direct {p0}, Lorg/apache/poi/util/RLEDecompressingInputStream;->readChunk()I

    move-result v3

    iput v3, p0, Lorg/apache/poi/util/RLEDecompressingInputStream;->len:I

    if-ne v3, v1, :cond_2

    if-le v2, p2, :cond_1

    sub-int v1, v2, p2

    :cond_1
    return v1

    :cond_2
    iget v3, p0, Lorg/apache/poi/util/RLEDecompressingInputStream;->len:I

    iget v4, p0, Lorg/apache/poi/util/RLEDecompressingInputStream;->pos:I

    sub-int/2addr v3, v4

    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    move-result v3

    iget-object v4, p0, Lorg/apache/poi/util/RLEDecompressingInputStream;->buf:[B

    iget v5, p0, Lorg/apache/poi/util/RLEDecompressingInputStream;->pos:I

    invoke-static {v4, v5, p1, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget v4, p0, Lorg/apache/poi/util/RLEDecompressingInputStream;->pos:I

    add-int/2addr v4, v3

    iput v4, p0, Lorg/apache/poi/util/RLEDecompressingInputStream;->pos:I

    sub-int/2addr v0, v3

    add-int/2addr v2, v3

    goto :goto_0

    :cond_3
    return p3
.end method

.method public readInt()I
    .locals 1

    .line 1
    invoke-direct {p0, p0}, Lorg/apache/poi/util/RLEDecompressingInputStream;->readInt(Ljava/io/InputStream;)I

    move-result v0

    return v0
.end method

.method public readShort()I
    .locals 1

    .line 1
    invoke-direct {p0, p0}, Lorg/apache/poi/util/RLEDecompressingInputStream;->readShort(Ljava/io/InputStream;)I

    move-result v0

    return v0
.end method

.method public skip(J)J
    .locals 4

    move-wide v0, p1

    :goto_0
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-lez v2, :cond_1

    iget v2, p0, Lorg/apache/poi/util/RLEDecompressingInputStream;->pos:I

    iget v3, p0, Lorg/apache/poi/util/RLEDecompressingInputStream;->len:I

    if-lt v2, v3, :cond_0

    invoke-direct {p0}, Lorg/apache/poi/util/RLEDecompressingInputStream;->readChunk()I

    move-result v2

    iput v2, p0, Lorg/apache/poi/util/RLEDecompressingInputStream;->len:I

    const/4 v3, -0x1

    if-ne v2, v3, :cond_0

    const-wide/16 p1, -0x1

    return-wide p1

    :cond_0
    iget v2, p0, Lorg/apache/poi/util/RLEDecompressingInputStream;->len:I

    iget v3, p0, Lorg/apache/poi/util/RLEDecompressingInputStream;->pos:I

    sub-int/2addr v2, v3

    int-to-long v2, v2

    invoke-static {p1, p2, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v2

    long-to-int v2, v2

    iget v3, p0, Lorg/apache/poi/util/RLEDecompressingInputStream;->pos:I

    add-int/2addr v3, v2

    iput v3, p0, Lorg/apache/poi/util/RLEDecompressingInputStream;->pos:I

    int-to-long v2, v2

    sub-long/2addr v0, v2

    goto :goto_0

    :cond_1
    return-wide p1
.end method

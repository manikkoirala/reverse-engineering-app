.class public final Lorg/apache/poi/util/StaxHelper;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final logger:Lorg/apache/poi/util/POILogger;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    const-class v0, Lorg/apache/poi/util/StaxHelper;

    invoke-static {v0}, Lorg/apache/poi/util/POILogFactory;->getLogger(Ljava/lang/Class;)Lorg/apache/poi/util/POILogger;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/util/StaxHelper;->logger:Lorg/apache/poi/util/POILogger;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static newXMLEventFactory()Ljavax/xml/stream/XMLEventFactory;
    .locals 1

    invoke-static {}, Ljavax/xml/stream/XMLEventFactory;->newFactory()Ljavax/xml/stream/XMLEventFactory;

    move-result-object v0

    return-object v0
.end method

.method public static newXMLInputFactory()Ljavax/xml/stream/XMLInputFactory;
    .locals 3

    invoke-static {}, Ljavax/xml/stream/XMLInputFactory;->newFactory()Ljavax/xml/stream/XMLInputFactory;

    move-result-object v0

    const-string v1, "javax.xml.stream.isNamespaceAware"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lorg/apache/poi/util/StaxHelper;->trySetProperty(Ljavax/xml/stream/XMLInputFactory;Ljava/lang/String;Z)V

    const-string v1, "javax.xml.stream.isValidating"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lorg/apache/poi/util/StaxHelper;->trySetProperty(Ljavax/xml/stream/XMLInputFactory;Ljava/lang/String;Z)V

    const-string v1, "javax.xml.stream.supportDTD"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/util/StaxHelper;->trySetProperty(Ljavax/xml/stream/XMLInputFactory;Ljava/lang/String;Z)V

    const-string v1, "javax.xml.stream.isSupportingExternalEntities"

    invoke-static {v0, v1, v2}, Lorg/apache/poi/util/StaxHelper;->trySetProperty(Ljavax/xml/stream/XMLInputFactory;Ljava/lang/String;Z)V

    return-object v0
.end method

.method public static newXMLOutputFactory()Ljavax/xml/stream/XMLOutputFactory;
    .locals 3

    invoke-static {}, Ljavax/xml/stream/XMLOutputFactory;->newFactory()Ljavax/xml/stream/XMLOutputFactory;

    move-result-object v0

    const-string v1, "javax.xml.stream.isRepairingNamespaces"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lorg/apache/poi/util/StaxHelper;->trySetProperty(Ljavax/xml/stream/XMLOutputFactory;Ljava/lang/String;Z)V

    return-object v0
.end method

.method private static trySetProperty(Ljavax/xml/stream/XMLInputFactory;Ljava/lang/String;Z)V
    .locals 2

    .line 1
    const/4 v0, 0x5

    :try_start_0
    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p2

    invoke-virtual {p0, p1, p2}, Ljavax/xml/stream/XMLInputFactory;->setProperty(Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/AbstractMethodError; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    sget-object p2, Lorg/apache/poi/util/StaxHelper;->logger:Lorg/apache/poi/util/POILogger;

    const-string v1, "Cannot set StAX property because outdated StAX parser in classpath"

    filled-new-array {v1, p1, p0}, [Ljava/lang/Object;

    move-result-object p0

    invoke-virtual {p2, v0, p0}, Lorg/apache/poi/util/POILogger;->log(I[Ljava/lang/Object;)V

    goto :goto_0

    :catch_1
    move-exception p0

    sget-object p2, Lorg/apache/poi/util/StaxHelper;->logger:Lorg/apache/poi/util/POILogger;

    const-string v1, "StAX Property unsupported"

    filled-new-array {v1, p1, p0}, [Ljava/lang/Object;

    move-result-object p0

    invoke-virtual {p2, v0, p0}, Lorg/apache/poi/util/POILogger;->log(I[Ljava/lang/Object;)V

    :goto_0
    return-void
.end method

.method private static trySetProperty(Ljavax/xml/stream/XMLOutputFactory;Ljava/lang/String;Z)V
    .locals 2

    .line 2
    const/4 v0, 0x5

    :try_start_0
    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p2

    invoke-virtual {p0, p1, p2}, Ljavax/xml/stream/XMLOutputFactory;->setProperty(Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/AbstractMethodError; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception p0

    sget-object p2, Lorg/apache/poi/util/StaxHelper;->logger:Lorg/apache/poi/util/POILogger;

    const-string v1, "Cannot set StAX property because outdated StAX parser in classpath"

    filled-new-array {v1, p1, p0}, [Ljava/lang/Object;

    move-result-object p0

    invoke-virtual {p2, v0, p0}, Lorg/apache/poi/util/POILogger;->log(I[Ljava/lang/Object;)V

    goto :goto_0

    :catch_1
    move-exception p0

    sget-object p2, Lorg/apache/poi/util/StaxHelper;->logger:Lorg/apache/poi/util/POILogger;

    const-string v1, "StAX Property unsupported"

    filled-new-array {v1, p1, p0}, [Ljava/lang/Object;

    move-result-object p0

    invoke-virtual {p2, v0, p0}, Lorg/apache/poi/util/POILogger;->log(I[Ljava/lang/Object;)V

    :goto_0
    return-void
.end method

.class public Lorg/apache/poi/xdgf/geom/SplineRenderer;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static createNurbsSpline(Lzl;Lz22;Lz22;I)Ldn1;
    .locals 9

    .line 1
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lz22;->c(I)D

    move-result-wide v1

    invoke-virtual {p1}, Lz22;->f()I

    move-result v3

    add-int/lit8 v4, v3, -0x1

    invoke-virtual {p1, v4}, Lz22;->c(I)D

    move-result-wide v4

    move v6, v0

    :goto_0
    if-ge v6, v3, :cond_0

    invoke-virtual {p1, v6}, Lz22;->c(I)D

    move-result-wide v7

    sub-double/2addr v7, v1

    div-double/2addr v7, v4

    invoke-virtual {p1, v7, v8, v6}, Lz22;->e(DI)V

    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lzl;->c()I

    move-result v1

    add-int/2addr v1, p3

    add-int/lit8 v1, v1, 0x1

    :goto_1
    if-ge v3, v1, :cond_1

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    invoke-virtual {p1, v4, v5}, Lz22;->a(D)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_1
    new-instance v1, Lub0;

    const-string v2, "0:n-1"

    invoke-virtual {p0}, Lzl;->c()I

    move-result v3

    invoke-direct {v1, v2, v3}, Lub0;-><init>(Ljava/lang/String;I)V

    new-instance v2, Lcy0;

    invoke-direct {v2, p0, v1}, Lcy0;-><init>(Lzl;Lub0;)V

    invoke-virtual {v2, p3}, Lwa;->g(I)V

    const/4 p0, 0x2

    invoke-virtual {v2, p0}, Lwa;->i(I)V

    invoke-virtual {v2, p1}, Lwa;->h(Lz22;)V

    if-nez p2, :cond_2

    invoke-virtual {v2, v0}, Lcy0;->j(Z)V

    goto :goto_2

    :cond_2
    invoke-virtual {v2, p2}, Lcy0;->k(Lz22;)V

    :goto_2
    new-instance p0, Ldn1;

    invoke-direct {p0}, Ldn1;-><init>()V

    const-wide p1, 0x3f847ae147ae147bL    # 0.01

    invoke-virtual {p0, p1, p2}, Lpx0;->g(D)V

    invoke-virtual {v2, p0}, Lcy0;->f(Lpx0;)V

    return-object p0
.end method

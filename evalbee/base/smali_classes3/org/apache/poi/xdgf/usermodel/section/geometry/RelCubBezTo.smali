.class public Lorg/apache/poi/xdgf/usermodel/section/geometry/RelCubBezTo;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/poi/xdgf/usermodel/section/geometry/GeometryRow;


# instance fields
.field _master:Lorg/apache/poi/xdgf/usermodel/section/geometry/RelCubBezTo;

.field a:Ljava/lang/Double;

.field b:Ljava/lang/Double;

.field c:Ljava/lang/Double;

.field d:Ljava/lang/Double;

.field deleted:Ljava/lang/Boolean;

.field x:Ljava/lang/Double;

.field y:Ljava/lang/Double;


# direct methods
.method public constructor <init>(Lxf1;)V
    .locals 5

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/poi/xdgf/usermodel/section/geometry/RelCubBezTo;->_master:Lorg/apache/poi/xdgf/usermodel/section/geometry/RelCubBezTo;

    iput-object v0, p0, Lorg/apache/poi/xdgf/usermodel/section/geometry/RelCubBezTo;->x:Ljava/lang/Double;

    iput-object v0, p0, Lorg/apache/poi/xdgf/usermodel/section/geometry/RelCubBezTo;->y:Ljava/lang/Double;

    iput-object v0, p0, Lorg/apache/poi/xdgf/usermodel/section/geometry/RelCubBezTo;->a:Ljava/lang/Double;

    iput-object v0, p0, Lorg/apache/poi/xdgf/usermodel/section/geometry/RelCubBezTo;->b:Ljava/lang/Double;

    iput-object v0, p0, Lorg/apache/poi/xdgf/usermodel/section/geometry/RelCubBezTo;->c:Ljava/lang/Double;

    iput-object v0, p0, Lorg/apache/poi/xdgf/usermodel/section/geometry/RelCubBezTo;->d:Ljava/lang/Double;

    iput-object v0, p0, Lorg/apache/poi/xdgf/usermodel/section/geometry/RelCubBezTo;->deleted:Ljava/lang/Boolean;

    invoke-interface {p1}, Lxf1;->isSetDel()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Lxf1;->getDel()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/xdgf/usermodel/section/geometry/RelCubBezTo;->deleted:Ljava/lang/Boolean;

    :cond_0
    invoke-interface {p1}, Lxf1;->getCellArray()[Lrf;

    move-result-object p1

    array-length v0, p1

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_7

    aget-object v2, p1, v1

    invoke-interface {v2}, Lrf;->getN()Ljava/lang/String;

    move-result-object v3

    const-string v4, "X"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-static {v2}, Lorg/apache/poi/xdgf/usermodel/XDGFCell;->parseDoubleValue(Lrf;)Ljava/lang/Double;

    move-result-object v2

    iput-object v2, p0, Lorg/apache/poi/xdgf/usermodel/section/geometry/RelCubBezTo;->x:Ljava/lang/Double;

    goto :goto_1

    :cond_1
    const-string v4, "Y"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-static {v2}, Lorg/apache/poi/xdgf/usermodel/XDGFCell;->parseDoubleValue(Lrf;)Ljava/lang/Double;

    move-result-object v2

    iput-object v2, p0, Lorg/apache/poi/xdgf/usermodel/section/geometry/RelCubBezTo;->y:Ljava/lang/Double;

    goto :goto_1

    :cond_2
    const-string v4, "A"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-static {v2}, Lorg/apache/poi/xdgf/usermodel/XDGFCell;->parseDoubleValue(Lrf;)Ljava/lang/Double;

    move-result-object v2

    iput-object v2, p0, Lorg/apache/poi/xdgf/usermodel/section/geometry/RelCubBezTo;->a:Ljava/lang/Double;

    goto :goto_1

    :cond_3
    const-string v4, "B"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-static {v2}, Lorg/apache/poi/xdgf/usermodel/XDGFCell;->parseDoubleValue(Lrf;)Ljava/lang/Double;

    move-result-object v2

    iput-object v2, p0, Lorg/apache/poi/xdgf/usermodel/section/geometry/RelCubBezTo;->b:Ljava/lang/Double;

    goto :goto_1

    :cond_4
    const-string v4, "C"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-static {v2}, Lorg/apache/poi/xdgf/usermodel/XDGFCell;->parseDoubleValue(Lrf;)Ljava/lang/Double;

    move-result-object v2

    iput-object v2, p0, Lorg/apache/poi/xdgf/usermodel/section/geometry/RelCubBezTo;->c:Ljava/lang/Double;

    goto :goto_1

    :cond_5
    const-string v4, "D"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    invoke-static {v2}, Lorg/apache/poi/xdgf/usermodel/XDGFCell;->parseDoubleValue(Lrf;)Ljava/lang/Double;

    move-result-object v2

    iput-object v2, p0, Lorg/apache/poi/xdgf/usermodel/section/geometry/RelCubBezTo;->d:Ljava/lang/Double;

    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_6
    new-instance p1, Lorg/apache/poi/POIXMLException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Invalid cell \'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\' in RelCubBezTo row"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Lorg/apache/poi/POIXMLException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_7
    return-void
.end method


# virtual methods
.method public addToPath(Ljava/awt/geom/Path2D$Double;Lorg/apache/poi/xdgf/usermodel/XDGFShape;)V
    .locals 19

    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/xdgf/usermodel/section/geometry/RelCubBezTo;->getDel()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    invoke-virtual/range {p2 .. p2}, Lorg/apache/poi/xdgf/usermodel/XDGFShape;->getWidth()Ljava/lang/Double;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    invoke-virtual/range {p2 .. p2}, Lorg/apache/poi/xdgf/usermodel/XDGFShape;->getHeight()Ljava/lang/Double;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/xdgf/usermodel/section/geometry/RelCubBezTo;->getA()Ljava/lang/Double;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    mul-double v7, v4, v0

    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/xdgf/usermodel/section/geometry/RelCubBezTo;->getB()Ljava/lang/Double;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    mul-double v9, v4, v2

    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/xdgf/usermodel/section/geometry/RelCubBezTo;->getC()Ljava/lang/Double;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    mul-double v11, v4, v0

    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/xdgf/usermodel/section/geometry/RelCubBezTo;->getD()Ljava/lang/Double;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    mul-double v13, v4, v2

    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/xdgf/usermodel/section/geometry/RelCubBezTo;->getX()Ljava/lang/Double;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    mul-double v15, v4, v0

    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/xdgf/usermodel/section/geometry/RelCubBezTo;->getY()Ljava/lang/Double;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v0

    mul-double v17, v0, v2

    move-object/from16 v6, p1

    invoke-virtual/range {v6 .. v18}, Ljava/awt/geom/Path2D$Double;->curveTo(DDDDDD)V

    return-void
.end method

.method public getA()Ljava/lang/Double;
    .locals 1

    iget-object v0, p0, Lorg/apache/poi/xdgf/usermodel/section/geometry/RelCubBezTo;->a:Ljava/lang/Double;

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/poi/xdgf/usermodel/section/geometry/RelCubBezTo;->_master:Lorg/apache/poi/xdgf/usermodel/section/geometry/RelCubBezTo;

    iget-object v0, v0, Lorg/apache/poi/xdgf/usermodel/section/geometry/RelCubBezTo;->a:Ljava/lang/Double;

    :cond_0
    return-object v0
.end method

.method public getB()Ljava/lang/Double;
    .locals 1

    iget-object v0, p0, Lorg/apache/poi/xdgf/usermodel/section/geometry/RelCubBezTo;->b:Ljava/lang/Double;

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/poi/xdgf/usermodel/section/geometry/RelCubBezTo;->_master:Lorg/apache/poi/xdgf/usermodel/section/geometry/RelCubBezTo;

    iget-object v0, v0, Lorg/apache/poi/xdgf/usermodel/section/geometry/RelCubBezTo;->b:Ljava/lang/Double;

    :cond_0
    return-object v0
.end method

.method public getC()Ljava/lang/Double;
    .locals 1

    iget-object v0, p0, Lorg/apache/poi/xdgf/usermodel/section/geometry/RelCubBezTo;->c:Ljava/lang/Double;

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/poi/xdgf/usermodel/section/geometry/RelCubBezTo;->_master:Lorg/apache/poi/xdgf/usermodel/section/geometry/RelCubBezTo;

    iget-object v0, v0, Lorg/apache/poi/xdgf/usermodel/section/geometry/RelCubBezTo;->c:Ljava/lang/Double;

    :cond_0
    return-object v0
.end method

.method public getD()Ljava/lang/Double;
    .locals 1

    iget-object v0, p0, Lorg/apache/poi/xdgf/usermodel/section/geometry/RelCubBezTo;->d:Ljava/lang/Double;

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/poi/xdgf/usermodel/section/geometry/RelCubBezTo;->_master:Lorg/apache/poi/xdgf/usermodel/section/geometry/RelCubBezTo;

    iget-object v0, v0, Lorg/apache/poi/xdgf/usermodel/section/geometry/RelCubBezTo;->d:Ljava/lang/Double;

    :cond_0
    return-object v0
.end method

.method public getDel()Z
    .locals 1

    iget-object v0, p0, Lorg/apache/poi/xdgf/usermodel/section/geometry/RelCubBezTo;->deleted:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0

    :cond_0
    iget-object v0, p0, Lorg/apache/poi/xdgf/usermodel/section/geometry/RelCubBezTo;->_master:Lorg/apache/poi/xdgf/usermodel/section/geometry/RelCubBezTo;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lorg/apache/poi/xdgf/usermodel/section/geometry/RelCubBezTo;->getDel()Z

    move-result v0

    return v0

    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method public getX()Ljava/lang/Double;
    .locals 1

    iget-object v0, p0, Lorg/apache/poi/xdgf/usermodel/section/geometry/RelCubBezTo;->x:Ljava/lang/Double;

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/poi/xdgf/usermodel/section/geometry/RelCubBezTo;->_master:Lorg/apache/poi/xdgf/usermodel/section/geometry/RelCubBezTo;

    iget-object v0, v0, Lorg/apache/poi/xdgf/usermodel/section/geometry/RelCubBezTo;->x:Ljava/lang/Double;

    :cond_0
    return-object v0
.end method

.method public getY()Ljava/lang/Double;
    .locals 1

    iget-object v0, p0, Lorg/apache/poi/xdgf/usermodel/section/geometry/RelCubBezTo;->y:Ljava/lang/Double;

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/poi/xdgf/usermodel/section/geometry/RelCubBezTo;->_master:Lorg/apache/poi/xdgf/usermodel/section/geometry/RelCubBezTo;

    iget-object v0, v0, Lorg/apache/poi/xdgf/usermodel/section/geometry/RelCubBezTo;->y:Ljava/lang/Double;

    :cond_0
    return-object v0
.end method

.method public setupMaster(Lorg/apache/poi/xdgf/usermodel/section/geometry/GeometryRow;)V
    .locals 0

    check-cast p1, Lorg/apache/poi/xdgf/usermodel/section/geometry/RelCubBezTo;

    iput-object p1, p0, Lorg/apache/poi/xdgf/usermodel/section/geometry/RelCubBezTo;->_master:Lorg/apache/poi/xdgf/usermodel/section/geometry/RelCubBezTo;

    return-void
.end method

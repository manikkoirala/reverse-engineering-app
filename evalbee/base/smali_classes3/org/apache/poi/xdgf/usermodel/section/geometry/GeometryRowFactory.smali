.class public Lorg/apache/poi/xdgf/usermodel/section/geometry/GeometryRowFactory;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field static final _rowTypes:Lorg/apache/poi/xdgf/util/ObjectFactory;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/poi/xdgf/util/ObjectFactory<",
            "Lorg/apache/poi/xdgf/usermodel/section/geometry/GeometryRow;",
            "Lxf1;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    const-class v0, Lorg/apache/poi/xdgf/usermodel/section/geometry/PolyLineTo;

    const-string v1, "Internal error"

    const-class v2, Lxf1;

    new-instance v3, Lorg/apache/poi/xdgf/util/ObjectFactory;

    invoke-direct {v3}, Lorg/apache/poi/xdgf/util/ObjectFactory;-><init>()V

    sput-object v3, Lorg/apache/poi/xdgf/usermodel/section/geometry/GeometryRowFactory;->_rowTypes:Lorg/apache/poi/xdgf/util/ObjectFactory;

    :try_start_0
    const-string v4, "ArcTo"

    const-class v5, Lorg/apache/poi/xdgf/usermodel/section/geometry/ArcTo;

    const/4 v6, 0x1

    new-array v7, v6, [Ljava/lang/Class;

    const/4 v8, 0x0

    aput-object v2, v7, v8

    invoke-virtual {v3, v4, v5, v7}, Lorg/apache/poi/xdgf/util/ObjectFactory;->put(Ljava/lang/String;Ljava/lang/Class;[Ljava/lang/Class;)V

    const-string v4, "Ellipse"

    const-class v5, Lorg/apache/poi/xdgf/usermodel/section/geometry/Ellipse;

    new-array v7, v6, [Ljava/lang/Class;

    aput-object v2, v7, v8

    invoke-virtual {v3, v4, v5, v7}, Lorg/apache/poi/xdgf/util/ObjectFactory;->put(Ljava/lang/String;Ljava/lang/Class;[Ljava/lang/Class;)V

    const-string v4, "EllipticalArcTo"

    const-class v5, Lorg/apache/poi/xdgf/usermodel/section/geometry/EllipticalArcTo;

    new-array v7, v6, [Ljava/lang/Class;

    aput-object v2, v7, v8

    invoke-virtual {v3, v4, v5, v7}, Lorg/apache/poi/xdgf/util/ObjectFactory;->put(Ljava/lang/String;Ljava/lang/Class;[Ljava/lang/Class;)V

    const-string v4, "InfiniteLine"

    const-class v5, Lorg/apache/poi/xdgf/usermodel/section/geometry/InfiniteLine;

    new-array v7, v6, [Ljava/lang/Class;

    aput-object v2, v7, v8

    invoke-virtual {v3, v4, v5, v7}, Lorg/apache/poi/xdgf/util/ObjectFactory;->put(Ljava/lang/String;Ljava/lang/Class;[Ljava/lang/Class;)V

    const-string v4, "LineTo"

    const-class v5, Lorg/apache/poi/xdgf/usermodel/section/geometry/LineTo;

    new-array v7, v6, [Ljava/lang/Class;

    aput-object v2, v7, v8

    invoke-virtual {v3, v4, v5, v7}, Lorg/apache/poi/xdgf/util/ObjectFactory;->put(Ljava/lang/String;Ljava/lang/Class;[Ljava/lang/Class;)V

    const-string v4, "MoveTo"

    const-class v5, Lorg/apache/poi/xdgf/usermodel/section/geometry/MoveTo;

    new-array v7, v6, [Ljava/lang/Class;

    aput-object v2, v7, v8

    invoke-virtual {v3, v4, v5, v7}, Lorg/apache/poi/xdgf/util/ObjectFactory;->put(Ljava/lang/String;Ljava/lang/Class;[Ljava/lang/Class;)V

    const-string v4, "NURBSTo"

    const-class v5, Lorg/apache/poi/xdgf/usermodel/section/geometry/NURBSTo;

    new-array v7, v6, [Ljava/lang/Class;

    aput-object v2, v7, v8

    invoke-virtual {v3, v4, v5, v7}, Lorg/apache/poi/xdgf/util/ObjectFactory;->put(Ljava/lang/String;Ljava/lang/Class;[Ljava/lang/Class;)V

    const-string v4, "PolylineTo"

    new-array v5, v6, [Ljava/lang/Class;

    aput-object v2, v5, v8

    invoke-virtual {v3, v4, v0, v5}, Lorg/apache/poi/xdgf/util/ObjectFactory;->put(Ljava/lang/String;Ljava/lang/Class;[Ljava/lang/Class;)V

    const-string v4, "PolyLineTo"

    new-array v5, v6, [Ljava/lang/Class;

    aput-object v2, v5, v8

    invoke-virtual {v3, v4, v0, v5}, Lorg/apache/poi/xdgf/util/ObjectFactory;->put(Ljava/lang/String;Ljava/lang/Class;[Ljava/lang/Class;)V

    const-string v0, "RelCubBezTo"

    const-class v4, Lorg/apache/poi/xdgf/usermodel/section/geometry/RelCubBezTo;

    new-array v5, v6, [Ljava/lang/Class;

    aput-object v2, v5, v8

    invoke-virtual {v3, v0, v4, v5}, Lorg/apache/poi/xdgf/util/ObjectFactory;->put(Ljava/lang/String;Ljava/lang/Class;[Ljava/lang/Class;)V

    const-string v0, "RelEllipticalArcTo"

    const-class v4, Lorg/apache/poi/xdgf/usermodel/section/geometry/RelEllipticalArcTo;

    new-array v5, v6, [Ljava/lang/Class;

    aput-object v2, v5, v8

    invoke-virtual {v3, v0, v4, v5}, Lorg/apache/poi/xdgf/util/ObjectFactory;->put(Ljava/lang/String;Ljava/lang/Class;[Ljava/lang/Class;)V

    const-string v0, "RelLineTo"

    const-class v4, Lorg/apache/poi/xdgf/usermodel/section/geometry/RelLineTo;

    new-array v5, v6, [Ljava/lang/Class;

    aput-object v2, v5, v8

    invoke-virtual {v3, v0, v4, v5}, Lorg/apache/poi/xdgf/util/ObjectFactory;->put(Ljava/lang/String;Ljava/lang/Class;[Ljava/lang/Class;)V

    const-string v0, "RelMoveTo"

    const-class v4, Lorg/apache/poi/xdgf/usermodel/section/geometry/RelMoveTo;

    new-array v5, v6, [Ljava/lang/Class;

    aput-object v2, v5, v8

    invoke-virtual {v3, v0, v4, v5}, Lorg/apache/poi/xdgf/util/ObjectFactory;->put(Ljava/lang/String;Ljava/lang/Class;[Ljava/lang/Class;)V

    const-string v0, "RelQuadBezTo"

    const-class v4, Lorg/apache/poi/xdgf/usermodel/section/geometry/RelQuadBezTo;

    new-array v5, v6, [Ljava/lang/Class;

    aput-object v2, v5, v8

    invoke-virtual {v3, v0, v4, v5}, Lorg/apache/poi/xdgf/util/ObjectFactory;->put(Ljava/lang/String;Ljava/lang/Class;[Ljava/lang/Class;)V

    const-string v0, "SplineKnot"

    const-class v4, Lorg/apache/poi/xdgf/usermodel/section/geometry/SplineKnot;

    new-array v5, v6, [Ljava/lang/Class;

    aput-object v2, v5, v8

    invoke-virtual {v3, v0, v4, v5}, Lorg/apache/poi/xdgf/util/ObjectFactory;->put(Ljava/lang/String;Ljava/lang/Class;[Ljava/lang/Class;)V

    const-string v0, "SplineStart"

    const-class v4, Lorg/apache/poi/xdgf/usermodel/section/geometry/SplineStart;

    new-array v5, v6, [Ljava/lang/Class;

    aput-object v2, v5, v8

    invoke-virtual {v3, v0, v4, v5}, Lorg/apache/poi/xdgf/util/ObjectFactory;->put(Ljava/lang/String;Ljava/lang/Class;[Ljava/lang/Class;)V
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v2, Lorg/apache/poi/POIXMLException;

    invoke-direct {v2, v1, v0}, Lorg/apache/poi/POIXMLException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2

    :catch_1
    move-exception v0

    new-instance v2, Lorg/apache/poi/POIXMLException;

    invoke-direct {v2, v1, v0}, Lorg/apache/poi/POIXMLException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static load(Lxf1;)Lorg/apache/poi/xdgf/usermodel/section/geometry/GeometryRow;
    .locals 2

    .line 1
    sget-object v0, Lorg/apache/poi/xdgf/usermodel/section/geometry/GeometryRowFactory;->_rowTypes:Lorg/apache/poi/xdgf/util/ObjectFactory;

    invoke-interface {p0}, Lxf1;->getT()Ljava/lang/String;

    move-result-object v1

    filled-new-array {p0}, [Ljava/lang/Object;

    move-result-object p0

    invoke-virtual {v0, v1, p0}, Lorg/apache/poi/xdgf/util/ObjectFactory;->load(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lorg/apache/poi/xdgf/usermodel/section/geometry/GeometryRow;

    return-object p0
.end method

.class public abstract Lorg/apache/poi/xdgf/usermodel/section/XDGFSection;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field static final _sectionTypes:Lorg/apache/poi/xdgf/util/ObjectFactory;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lorg/apache/poi/xdgf/util/ObjectFactory<",
            "Lorg/apache/poi/xdgf/usermodel/section/XDGFSection;",
            "Lik1;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field protected _cells:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lorg/apache/poi/xdgf/usermodel/XDGFCell;",
            ">;"
        }
    .end annotation
.end field

.field protected _containingSheet:Lorg/apache/poi/xdgf/usermodel/XDGFSheet;

.field protected _section:Lik1;


# direct methods
.method public static constructor <clinit>()V
    .locals 11

    const-string v0, "Internal error"

    const-class v1, Lorg/apache/poi/xdgf/usermodel/section/GenericSection;

    const-class v2, Lorg/apache/poi/xdgf/usermodel/XDGFSheet;

    const-class v3, Lik1;

    new-instance v4, Lorg/apache/poi/xdgf/util/ObjectFactory;

    invoke-direct {v4}, Lorg/apache/poi/xdgf/util/ObjectFactory;-><init>()V

    sput-object v4, Lorg/apache/poi/xdgf/usermodel/section/XDGFSection;->_sectionTypes:Lorg/apache/poi/xdgf/util/ObjectFactory;

    :try_start_0
    const-string v5, "LineGradient"

    const/4 v6, 0x2

    new-array v7, v6, [Ljava/lang/Class;

    const/4 v8, 0x0

    aput-object v3, v7, v8

    const/4 v9, 0x1

    aput-object v2, v7, v9

    invoke-virtual {v4, v5, v1, v7}, Lorg/apache/poi/xdgf/util/ObjectFactory;->put(Ljava/lang/String;Ljava/lang/Class;[Ljava/lang/Class;)V

    const-string v5, "FillGradient"

    new-array v7, v6, [Ljava/lang/Class;

    aput-object v3, v7, v8

    aput-object v2, v7, v9

    invoke-virtual {v4, v5, v1, v7}, Lorg/apache/poi/xdgf/util/ObjectFactory;->put(Ljava/lang/String;Ljava/lang/Class;[Ljava/lang/Class;)V

    const-string v5, "Character"

    const-class v7, Lorg/apache/poi/xdgf/usermodel/section/CharacterSection;

    new-array v10, v6, [Ljava/lang/Class;

    aput-object v3, v10, v8

    aput-object v2, v10, v9

    invoke-virtual {v4, v5, v7, v10}, Lorg/apache/poi/xdgf/util/ObjectFactory;->put(Ljava/lang/String;Ljava/lang/Class;[Ljava/lang/Class;)V

    const-string v5, "Paragraph"

    new-array v7, v6, [Ljava/lang/Class;

    aput-object v3, v7, v8

    aput-object v2, v7, v9

    invoke-virtual {v4, v5, v1, v7}, Lorg/apache/poi/xdgf/util/ObjectFactory;->put(Ljava/lang/String;Ljava/lang/Class;[Ljava/lang/Class;)V

    const-string v5, "Tabs"

    new-array v7, v6, [Ljava/lang/Class;

    aput-object v3, v7, v8

    aput-object v2, v7, v9

    invoke-virtual {v4, v5, v1, v7}, Lorg/apache/poi/xdgf/util/ObjectFactory;->put(Ljava/lang/String;Ljava/lang/Class;[Ljava/lang/Class;)V

    const-string v5, "Scratch"

    new-array v7, v6, [Ljava/lang/Class;

    aput-object v3, v7, v8

    aput-object v2, v7, v9

    invoke-virtual {v4, v5, v1, v7}, Lorg/apache/poi/xdgf/util/ObjectFactory;->put(Ljava/lang/String;Ljava/lang/Class;[Ljava/lang/Class;)V

    const-string v5, "Connection"

    new-array v7, v6, [Ljava/lang/Class;

    aput-object v3, v7, v8

    aput-object v2, v7, v9

    invoke-virtual {v4, v5, v1, v7}, Lorg/apache/poi/xdgf/util/ObjectFactory;->put(Ljava/lang/String;Ljava/lang/Class;[Ljava/lang/Class;)V

    const-string v5, "ConnectionABCD"

    new-array v7, v6, [Ljava/lang/Class;

    aput-object v3, v7, v8

    aput-object v2, v7, v9

    invoke-virtual {v4, v5, v1, v7}, Lorg/apache/poi/xdgf/util/ObjectFactory;->put(Ljava/lang/String;Ljava/lang/Class;[Ljava/lang/Class;)V

    const-string v5, "Field"

    new-array v7, v6, [Ljava/lang/Class;

    aput-object v3, v7, v8

    aput-object v2, v7, v9

    invoke-virtual {v4, v5, v1, v7}, Lorg/apache/poi/xdgf/util/ObjectFactory;->put(Ljava/lang/String;Ljava/lang/Class;[Ljava/lang/Class;)V

    const-string v5, "Control"

    new-array v7, v6, [Ljava/lang/Class;

    aput-object v3, v7, v8

    aput-object v2, v7, v9

    invoke-virtual {v4, v5, v1, v7}, Lorg/apache/poi/xdgf/util/ObjectFactory;->put(Ljava/lang/String;Ljava/lang/Class;[Ljava/lang/Class;)V

    const-string v5, "Geometry"

    const-class v7, Lorg/apache/poi/xdgf/usermodel/section/GeometrySection;

    new-array v10, v6, [Ljava/lang/Class;

    aput-object v3, v10, v8

    aput-object v2, v10, v9

    invoke-virtual {v4, v5, v7, v10}, Lorg/apache/poi/xdgf/util/ObjectFactory;->put(Ljava/lang/String;Ljava/lang/Class;[Ljava/lang/Class;)V

    const-string v5, "Actions"

    new-array v7, v6, [Ljava/lang/Class;

    aput-object v3, v7, v8

    aput-object v2, v7, v9

    invoke-virtual {v4, v5, v1, v7}, Lorg/apache/poi/xdgf/util/ObjectFactory;->put(Ljava/lang/String;Ljava/lang/Class;[Ljava/lang/Class;)V

    const-string v5, "Layer"

    new-array v7, v6, [Ljava/lang/Class;

    aput-object v3, v7, v8

    aput-object v2, v7, v9

    invoke-virtual {v4, v5, v1, v7}, Lorg/apache/poi/xdgf/util/ObjectFactory;->put(Ljava/lang/String;Ljava/lang/Class;[Ljava/lang/Class;)V

    const-string v5, "User"

    new-array v7, v6, [Ljava/lang/Class;

    aput-object v3, v7, v8

    aput-object v2, v7, v9

    invoke-virtual {v4, v5, v1, v7}, Lorg/apache/poi/xdgf/util/ObjectFactory;->put(Ljava/lang/String;Ljava/lang/Class;[Ljava/lang/Class;)V

    const-string v5, "Property"

    new-array v7, v6, [Ljava/lang/Class;

    aput-object v3, v7, v8

    aput-object v2, v7, v9

    invoke-virtual {v4, v5, v1, v7}, Lorg/apache/poi/xdgf/util/ObjectFactory;->put(Ljava/lang/String;Ljava/lang/Class;[Ljava/lang/Class;)V

    const-string v5, "Hyperlink"

    new-array v7, v6, [Ljava/lang/Class;

    aput-object v3, v7, v8

    aput-object v2, v7, v9

    invoke-virtual {v4, v5, v1, v7}, Lorg/apache/poi/xdgf/util/ObjectFactory;->put(Ljava/lang/String;Ljava/lang/Class;[Ljava/lang/Class;)V

    const-string v5, "Reviewer"

    new-array v7, v6, [Ljava/lang/Class;

    aput-object v3, v7, v8

    aput-object v2, v7, v9

    invoke-virtual {v4, v5, v1, v7}, Lorg/apache/poi/xdgf/util/ObjectFactory;->put(Ljava/lang/String;Ljava/lang/Class;[Ljava/lang/Class;)V

    const-string v5, "Annotation"

    new-array v7, v6, [Ljava/lang/Class;

    aput-object v3, v7, v8

    aput-object v2, v7, v9

    invoke-virtual {v4, v5, v1, v7}, Lorg/apache/poi/xdgf/util/ObjectFactory;->put(Ljava/lang/String;Ljava/lang/Class;[Ljava/lang/Class;)V

    const-string v5, "ActionTag"

    new-array v6, v6, [Ljava/lang/Class;

    aput-object v3, v6, v8

    aput-object v2, v6, v9

    invoke-virtual {v4, v5, v1, v6}, Lorg/apache/poi/xdgf/util/ObjectFactory;->put(Ljava/lang/String;Ljava/lang/Class;[Ljava/lang/Class;)V
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    new-instance v1, Lorg/apache/poi/POIXMLException;

    invoke-direct {v1, v0}, Lorg/apache/poi/POIXMLException;-><init>(Ljava/lang/String;)V

    throw v1

    :catch_1
    new-instance v1, Lorg/apache/poi/POIXMLException;

    invoke-direct {v1, v0}, Lorg/apache/poi/POIXMLException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public constructor <init>(Lik1;Lorg/apache/poi/xdgf/usermodel/XDGFSheet;)V
    .locals 5

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/xdgf/usermodel/section/XDGFSection;->_cells:Ljava/util/Map;

    iput-object p1, p0, Lorg/apache/poi/xdgf/usermodel/section/XDGFSection;->_section:Lik1;

    iput-object p2, p0, Lorg/apache/poi/xdgf/usermodel/section/XDGFSection;->_containingSheet:Lorg/apache/poi/xdgf/usermodel/XDGFSheet;

    invoke-interface {p1}, Lik1;->getCellArray()[Lrf;

    move-result-object p1

    array-length p2, p1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, p2, :cond_0

    aget-object v1, p1, v0

    iget-object v2, p0, Lorg/apache/poi/xdgf/usermodel/section/XDGFSection;->_cells:Ljava/util/Map;

    invoke-interface {v1}, Lrf;->getN()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lorg/apache/poi/xdgf/usermodel/XDGFCell;

    invoke-direct {v4, v1}, Lorg/apache/poi/xdgf/usermodel/XDGFCell;-><init>(Lrf;)V

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public static load(Lik1;Lorg/apache/poi/xdgf/usermodel/XDGFSheet;)Lorg/apache/poi/xdgf/usermodel/section/XDGFSection;
    .locals 2

    .line 1
    sget-object v0, Lorg/apache/poi/xdgf/usermodel/section/XDGFSection;->_sectionTypes:Lorg/apache/poi/xdgf/util/ObjectFactory;

    invoke-interface {p0}, Lik1;->getN()Ljava/lang/String;

    move-result-object v1

    filled-new-array {p0, p1}, [Ljava/lang/Object;

    move-result-object p0

    invoke-virtual {v0, v1, p0}, Lorg/apache/poi/xdgf/util/ObjectFactory;->load(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lorg/apache/poi/xdgf/usermodel/section/XDGFSection;

    return-object p0
.end method


# virtual methods
.method public getXmlObject()Lik1;
    .locals 1
    .annotation runtime Lorg/apache/poi/util/Internal;
    .end annotation

    .line 1
    iget-object v0, p0, Lorg/apache/poi/xdgf/usermodel/section/XDGFSection;->_section:Lik1;

    return-object v0
.end method

.method public abstract setupMaster(Lorg/apache/poi/xdgf/usermodel/section/XDGFSection;)V
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "<Section type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lorg/apache/poi/xdgf/usermodel/section/XDGFSection;->_section:Lik1;

    invoke-interface {v1}, Lik1;->getN()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " from "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lorg/apache/poi/xdgf/usermodel/section/XDGFSection;->_containingSheet:Lorg/apache/poi/xdgf/usermodel/XDGFSheet;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ">"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

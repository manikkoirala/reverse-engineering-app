.class public Lorg/apache/poi/xdgf/usermodel/section/geometry/NURBSTo;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/poi/xdgf/usermodel/section/geometry/GeometryRow;


# instance fields
.field _master:Lorg/apache/poi/xdgf/usermodel/section/geometry/NURBSTo;

.field a:Ljava/lang/Double;

.field b:Ljava/lang/Double;

.field c:Ljava/lang/Double;

.field d:Ljava/lang/Double;

.field deleted:Ljava/lang/Boolean;

.field e:Ljava/lang/String;

.field x:Ljava/lang/Double;

.field y:Ljava/lang/Double;


# direct methods
.method public constructor <init>(Lxf1;)V
    .locals 5

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/poi/xdgf/usermodel/section/geometry/NURBSTo;->_master:Lorg/apache/poi/xdgf/usermodel/section/geometry/NURBSTo;

    iput-object v0, p0, Lorg/apache/poi/xdgf/usermodel/section/geometry/NURBSTo;->x:Ljava/lang/Double;

    iput-object v0, p0, Lorg/apache/poi/xdgf/usermodel/section/geometry/NURBSTo;->y:Ljava/lang/Double;

    iput-object v0, p0, Lorg/apache/poi/xdgf/usermodel/section/geometry/NURBSTo;->a:Ljava/lang/Double;

    iput-object v0, p0, Lorg/apache/poi/xdgf/usermodel/section/geometry/NURBSTo;->b:Ljava/lang/Double;

    iput-object v0, p0, Lorg/apache/poi/xdgf/usermodel/section/geometry/NURBSTo;->c:Ljava/lang/Double;

    iput-object v0, p0, Lorg/apache/poi/xdgf/usermodel/section/geometry/NURBSTo;->d:Ljava/lang/Double;

    iput-object v0, p0, Lorg/apache/poi/xdgf/usermodel/section/geometry/NURBSTo;->e:Ljava/lang/String;

    iput-object v0, p0, Lorg/apache/poi/xdgf/usermodel/section/geometry/NURBSTo;->deleted:Ljava/lang/Boolean;

    invoke-interface {p1}, Lxf1;->isSetDel()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Lxf1;->getDel()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/xdgf/usermodel/section/geometry/NURBSTo;->deleted:Ljava/lang/Boolean;

    :cond_0
    invoke-interface {p1}, Lxf1;->getCellArray()[Lrf;

    move-result-object p1

    array-length v0, p1

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_8

    aget-object v2, p1, v1

    invoke-interface {v2}, Lrf;->getN()Ljava/lang/String;

    move-result-object v3

    const-string v4, "X"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-static {v2}, Lorg/apache/poi/xdgf/usermodel/XDGFCell;->parseDoubleValue(Lrf;)Ljava/lang/Double;

    move-result-object v2

    iput-object v2, p0, Lorg/apache/poi/xdgf/usermodel/section/geometry/NURBSTo;->x:Ljava/lang/Double;

    goto :goto_1

    :cond_1
    const-string v4, "Y"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-static {v2}, Lorg/apache/poi/xdgf/usermodel/XDGFCell;->parseDoubleValue(Lrf;)Ljava/lang/Double;

    move-result-object v2

    iput-object v2, p0, Lorg/apache/poi/xdgf/usermodel/section/geometry/NURBSTo;->y:Ljava/lang/Double;

    goto :goto_1

    :cond_2
    const-string v4, "A"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-static {v2}, Lorg/apache/poi/xdgf/usermodel/XDGFCell;->parseDoubleValue(Lrf;)Ljava/lang/Double;

    move-result-object v2

    iput-object v2, p0, Lorg/apache/poi/xdgf/usermodel/section/geometry/NURBSTo;->a:Ljava/lang/Double;

    goto :goto_1

    :cond_3
    const-string v4, "B"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-static {v2}, Lorg/apache/poi/xdgf/usermodel/XDGFCell;->parseDoubleValue(Lrf;)Ljava/lang/Double;

    move-result-object v2

    iput-object v2, p0, Lorg/apache/poi/xdgf/usermodel/section/geometry/NURBSTo;->b:Ljava/lang/Double;

    goto :goto_1

    :cond_4
    const-string v4, "C"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-static {v2}, Lorg/apache/poi/xdgf/usermodel/XDGFCell;->parseDoubleValue(Lrf;)Ljava/lang/Double;

    move-result-object v2

    iput-object v2, p0, Lorg/apache/poi/xdgf/usermodel/section/geometry/NURBSTo;->c:Ljava/lang/Double;

    goto :goto_1

    :cond_5
    const-string v4, "D"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    invoke-static {v2}, Lorg/apache/poi/xdgf/usermodel/XDGFCell;->parseDoubleValue(Lrf;)Ljava/lang/Double;

    move-result-object v2

    iput-object v2, p0, Lorg/apache/poi/xdgf/usermodel/section/geometry/NURBSTo;->d:Ljava/lang/Double;

    goto :goto_1

    :cond_6
    const-string v4, "E"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    invoke-interface {v2}, Lrf;->getV()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lorg/apache/poi/xdgf/usermodel/section/geometry/NURBSTo;->e:Ljava/lang/String;

    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_7
    new-instance p1, Lorg/apache/poi/POIXMLException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Invalid cell \'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\' in NURBS row"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Lorg/apache/poi/POIXMLException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_8
    return-void
.end method


# virtual methods
.method public addToPath(Ljava/awt/geom/Path2D$Double;Lorg/apache/poi/xdgf/usermodel/XDGFShape;)V
    .locals 33

    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/xdgf/usermodel/section/geometry/NURBSTo;->getDel()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    invoke-virtual/range {p1 .. p1}, Ljava/awt/geom/Path2D$Double;->getCurrentPoint()Ljava/awt/geom/Point2D;

    move-result-object v0

    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/xdgf/usermodel/section/geometry/NURBSTo;->getE()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    const-string v2, "NURBS("

    invoke-virtual {v1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    const/4 v3, 0x1

    sub-int/2addr v2, v3

    const/4 v4, 0x6

    invoke-virtual {v1, v4, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    array-length v2, v1

    const/16 v4, 0x8

    if-lt v2, v4, :cond_5

    array-length v2, v1

    add-int/lit8 v2, v2, -0x4

    rem-int/lit8 v2, v2, 0x4

    if-nez v2, :cond_4

    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/xdgf/usermodel/section/geometry/NURBSTo;->getX()Ljava/lang/Double;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/xdgf/usermodel/section/geometry/NURBSTo;->getY()Ljava/lang/Double;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v6

    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/xdgf/usermodel/section/geometry/NURBSTo;->getA()Ljava/lang/Double;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v8

    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/xdgf/usermodel/section/geometry/NURBSTo;->getB()Ljava/lang/Double;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v10

    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/xdgf/usermodel/section/geometry/NURBSTo;->getC()Ljava/lang/Double;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v12

    invoke-virtual/range {p0 .. p0}, Lorg/apache/poi/xdgf/usermodel/section/geometry/NURBSTo;->getD()Ljava/lang/Double;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v14

    const/4 v2, 0x0

    aget-object v16, v1, v2

    invoke-virtual/range {v16 .. v16}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v16

    move-wide/from16 v17, v4

    invoke-static/range {v16 .. v16}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v4

    aget-object v16, v1, v3

    invoke-virtual/range {v16 .. v16}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    const/16 v16, 0x2

    aget-object v16, v1, v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v16

    const/16 v19, 0x3

    aget-object v20, v1, v19

    invoke-virtual/range {v20 .. v20}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v20

    const-wide/high16 v21, 0x3ff0000000000000L    # 1.0

    if-nez v16, :cond_1

    invoke-virtual/range {p2 .. p2}, Lorg/apache/poi/xdgf/usermodel/XDGFShape;->getWidth()Ljava/lang/Double;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v23

    goto :goto_0

    :cond_1
    move-wide/from16 v23, v21

    :goto_0
    if-nez v20, :cond_2

    invoke-virtual/range {p2 .. p2}, Lorg/apache/poi/xdgf/usermodel/XDGFShape;->getHeight()Ljava/lang/Double;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v21

    :cond_2
    new-instance v3, Lzl;

    invoke-direct {v3}, Lzl;-><init>()V

    move/from16 v20, v2

    new-instance v2, Lz22;

    invoke-direct {v2}, Lz22;-><init>()V

    move-wide/from16 v25, v6

    new-instance v6, Lz22;

    invoke-direct {v6}, Lz22;-><init>()V

    invoke-virtual {v2, v12, v13}, Lz22;->a(D)V

    invoke-virtual {v6, v14, v15}, Lz22;->a(D)V

    invoke-virtual {v0}, Ljava/awt/geom/Point2D;->getX()D

    move-result-wide v12

    invoke-virtual {v0}, Ljava/awt/geom/Point2D;->getY()D

    move-result-wide v14

    invoke-static {v12, v13, v14, v15}, Ls51;->a(DD)Lq51;

    move-result-object v0

    invoke-virtual {v3, v0}, Lzl;->a(Lr51;)V

    array-length v0, v1

    add-int/lit8 v0, v0, -0x4

    div-int/lit8 v0, v0, 0x4

    const/4 v7, 0x0

    :goto_1
    if-ge v7, v0, :cond_3

    mul-int/lit8 v12, v7, 0x4

    add-int/lit8 v12, v12, 0x4

    add-int/lit8 v13, v12, 0x0

    aget-object v13, v1, v13

    invoke-virtual {v13}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v13

    add-int/lit8 v15, v12, 0x1

    aget-object v15, v1, v15

    invoke-virtual {v15}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v15

    invoke-static {v15}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v27

    add-int/lit8 v15, v12, 0x2

    aget-object v15, v1, v15

    invoke-virtual {v15}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v15

    move-wide/from16 v29, v10

    invoke-static {v15}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v10

    add-int/lit8 v12, v12, 0x3

    aget-object v12, v1, v12

    invoke-virtual {v12}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v12

    move/from16 p2, v0

    move-object v15, v1

    invoke-static {v12}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v0

    mul-double v13, v13, v23

    move-wide/from16 v31, v4

    mul-double v4, v27, v21

    invoke-static {v13, v14, v4, v5}, Ls51;->a(DD)Lq51;

    move-result-object v4

    invoke-virtual {v3, v4}, Lzl;->a(Lr51;)V

    invoke-virtual {v2, v10, v11}, Lz22;->a(D)V

    invoke-virtual {v6, v0, v1}, Lz22;->a(D)V

    add-int/lit8 v7, v7, 0x1

    move/from16 v0, p2

    move-object v1, v15

    move-wide/from16 v10, v29

    move-wide/from16 v4, v31

    goto :goto_1

    :cond_3
    move-wide/from16 v31, v4

    move-wide/from16 v29, v10

    invoke-virtual {v2, v8, v9}, Lz22;->a(D)V

    move-wide/from16 v0, v31

    invoke-virtual {v2, v0, v1}, Lz22;->a(D)V

    move-wide/from16 v0, v29

    invoke-virtual {v6, v0, v1}, Lz22;->a(D)V

    move-wide/from16 v0, v17

    move-wide/from16 v4, v25

    invoke-static {v0, v1, v4, v5}, Ls51;->a(DD)Lq51;

    move-result-object v0

    invoke-virtual {v3, v0}, Lzl;->a(Lr51;)V

    move/from16 v0, v20

    invoke-static {v3, v2, v6, v0}, Lorg/apache/poi/xdgf/geom/SplineRenderer;->createNurbsSpline(Lzl;Lz22;Lz22;I)Ldn1;

    move-result-object v0

    move-object/from16 v1, p1

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Ljava/awt/geom/Path2D$Double;->append(Ljava/awt/Shape;Z)V

    return-void

    :cond_4
    move-object v15, v1

    new-instance v0, Lorg/apache/poi/POIXMLException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid NURBS formula -- need 4 + n*4 arguments, got "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-object v2, v15

    array-length v2, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/poi/POIXMLException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_5
    new-instance v0, Lorg/apache/poi/POIXMLException;

    const-string v1, "Invalid NURBS formula (not enough arguments)"

    invoke-direct {v0, v1}, Lorg/apache/poi/POIXMLException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_6
    new-instance v0, Lorg/apache/poi/POIXMLException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Invalid NURBS formula: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/poi/POIXMLException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getA()Ljava/lang/Double;
    .locals 1

    iget-object v0, p0, Lorg/apache/poi/xdgf/usermodel/section/geometry/NURBSTo;->a:Ljava/lang/Double;

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/poi/xdgf/usermodel/section/geometry/NURBSTo;->_master:Lorg/apache/poi/xdgf/usermodel/section/geometry/NURBSTo;

    iget-object v0, v0, Lorg/apache/poi/xdgf/usermodel/section/geometry/NURBSTo;->a:Ljava/lang/Double;

    :cond_0
    return-object v0
.end method

.method public getB()Ljava/lang/Double;
    .locals 1

    iget-object v0, p0, Lorg/apache/poi/xdgf/usermodel/section/geometry/NURBSTo;->b:Ljava/lang/Double;

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/poi/xdgf/usermodel/section/geometry/NURBSTo;->_master:Lorg/apache/poi/xdgf/usermodel/section/geometry/NURBSTo;

    iget-object v0, v0, Lorg/apache/poi/xdgf/usermodel/section/geometry/NURBSTo;->b:Ljava/lang/Double;

    :cond_0
    return-object v0
.end method

.method public getC()Ljava/lang/Double;
    .locals 1

    iget-object v0, p0, Lorg/apache/poi/xdgf/usermodel/section/geometry/NURBSTo;->c:Ljava/lang/Double;

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/poi/xdgf/usermodel/section/geometry/NURBSTo;->_master:Lorg/apache/poi/xdgf/usermodel/section/geometry/NURBSTo;

    iget-object v0, v0, Lorg/apache/poi/xdgf/usermodel/section/geometry/NURBSTo;->c:Ljava/lang/Double;

    :cond_0
    return-object v0
.end method

.method public getD()Ljava/lang/Double;
    .locals 1

    iget-object v0, p0, Lorg/apache/poi/xdgf/usermodel/section/geometry/NURBSTo;->d:Ljava/lang/Double;

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/poi/xdgf/usermodel/section/geometry/NURBSTo;->_master:Lorg/apache/poi/xdgf/usermodel/section/geometry/NURBSTo;

    iget-object v0, v0, Lorg/apache/poi/xdgf/usermodel/section/geometry/NURBSTo;->d:Ljava/lang/Double;

    :cond_0
    return-object v0
.end method

.method public getDel()Z
    .locals 1

    iget-object v0, p0, Lorg/apache/poi/xdgf/usermodel/section/geometry/NURBSTo;->deleted:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0

    :cond_0
    iget-object v0, p0, Lorg/apache/poi/xdgf/usermodel/section/geometry/NURBSTo;->_master:Lorg/apache/poi/xdgf/usermodel/section/geometry/NURBSTo;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lorg/apache/poi/xdgf/usermodel/section/geometry/NURBSTo;->getDel()Z

    move-result v0

    return v0

    :cond_1
    const/4 v0, 0x0

    return v0
.end method

.method public getE()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/apache/poi/xdgf/usermodel/section/geometry/NURBSTo;->e:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/poi/xdgf/usermodel/section/geometry/NURBSTo;->_master:Lorg/apache/poi/xdgf/usermodel/section/geometry/NURBSTo;

    iget-object v0, v0, Lorg/apache/poi/xdgf/usermodel/section/geometry/NURBSTo;->e:Ljava/lang/String;

    :cond_0
    return-object v0
.end method

.method public getX()Ljava/lang/Double;
    .locals 1

    iget-object v0, p0, Lorg/apache/poi/xdgf/usermodel/section/geometry/NURBSTo;->x:Ljava/lang/Double;

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/poi/xdgf/usermodel/section/geometry/NURBSTo;->_master:Lorg/apache/poi/xdgf/usermodel/section/geometry/NURBSTo;

    iget-object v0, v0, Lorg/apache/poi/xdgf/usermodel/section/geometry/NURBSTo;->x:Ljava/lang/Double;

    :cond_0
    return-object v0
.end method

.method public getY()Ljava/lang/Double;
    .locals 1

    iget-object v0, p0, Lorg/apache/poi/xdgf/usermodel/section/geometry/NURBSTo;->y:Ljava/lang/Double;

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/poi/xdgf/usermodel/section/geometry/NURBSTo;->_master:Lorg/apache/poi/xdgf/usermodel/section/geometry/NURBSTo;

    iget-object v0, v0, Lorg/apache/poi/xdgf/usermodel/section/geometry/NURBSTo;->y:Ljava/lang/Double;

    :cond_0
    return-object v0
.end method

.method public setupMaster(Lorg/apache/poi/xdgf/usermodel/section/geometry/GeometryRow;)V
    .locals 0

    check-cast p1, Lorg/apache/poi/xdgf/usermodel/section/geometry/NURBSTo;

    iput-object p1, p0, Lorg/apache/poi/xdgf/usermodel/section/geometry/NURBSTo;->_master:Lorg/apache/poi/xdgf/usermodel/section/geometry/NURBSTo;

    return-void
.end method

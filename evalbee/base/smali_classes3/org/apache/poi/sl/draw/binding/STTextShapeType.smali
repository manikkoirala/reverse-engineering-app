.class public final enum Lorg/apache/poi/sl/draw/binding/STTextShapeType;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lorg/apache/poi/sl/draw/binding/STTextShapeType;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/xml/bind/annotation/XmlEnum;
.end annotation

.annotation runtime Ljavax/xml/bind/annotation/XmlType;
    name = "ST_TextShapeType"
    namespace = "http://schemas.openxmlformats.org/drawingml/2006/main"
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lorg/apache/poi/sl/draw/binding/STTextShapeType;

.field public static final enum TEXT_ARCH_DOWN:Lorg/apache/poi/sl/draw/binding/STTextShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "textArchDown"
    .end annotation
.end field

.field public static final enum TEXT_ARCH_DOWN_POUR:Lorg/apache/poi/sl/draw/binding/STTextShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "textArchDownPour"
    .end annotation
.end field

.field public static final enum TEXT_ARCH_UP:Lorg/apache/poi/sl/draw/binding/STTextShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "textArchUp"
    .end annotation
.end field

.field public static final enum TEXT_ARCH_UP_POUR:Lorg/apache/poi/sl/draw/binding/STTextShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "textArchUpPour"
    .end annotation
.end field

.field public static final enum TEXT_BUTTON:Lorg/apache/poi/sl/draw/binding/STTextShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "textButton"
    .end annotation
.end field

.field public static final enum TEXT_BUTTON_POUR:Lorg/apache/poi/sl/draw/binding/STTextShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "textButtonPour"
    .end annotation
.end field

.field public static final enum TEXT_CAN_DOWN:Lorg/apache/poi/sl/draw/binding/STTextShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "textCanDown"
    .end annotation
.end field

.field public static final enum TEXT_CAN_UP:Lorg/apache/poi/sl/draw/binding/STTextShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "textCanUp"
    .end annotation
.end field

.field public static final enum TEXT_CASCADE_DOWN:Lorg/apache/poi/sl/draw/binding/STTextShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "textCascadeDown"
    .end annotation
.end field

.field public static final enum TEXT_CASCADE_UP:Lorg/apache/poi/sl/draw/binding/STTextShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "textCascadeUp"
    .end annotation
.end field

.field public static final enum TEXT_CHEVRON:Lorg/apache/poi/sl/draw/binding/STTextShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "textChevron"
    .end annotation
.end field

.field public static final enum TEXT_CHEVRON_INVERTED:Lorg/apache/poi/sl/draw/binding/STTextShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "textChevronInverted"
    .end annotation
.end field

.field public static final enum TEXT_CIRCLE:Lorg/apache/poi/sl/draw/binding/STTextShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "textCircle"
    .end annotation
.end field

.field public static final enum TEXT_CIRCLE_POUR:Lorg/apache/poi/sl/draw/binding/STTextShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "textCirclePour"
    .end annotation
.end field

.field public static final enum TEXT_CURVE_DOWN:Lorg/apache/poi/sl/draw/binding/STTextShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "textCurveDown"
    .end annotation
.end field

.field public static final enum TEXT_CURVE_UP:Lorg/apache/poi/sl/draw/binding/STTextShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "textCurveUp"
    .end annotation
.end field

.field public static final enum TEXT_DEFLATE:Lorg/apache/poi/sl/draw/binding/STTextShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "textDeflate"
    .end annotation
.end field

.field public static final enum TEXT_DEFLATE_BOTTOM:Lorg/apache/poi/sl/draw/binding/STTextShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "textDeflateBottom"
    .end annotation
.end field

.field public static final enum TEXT_DEFLATE_INFLATE:Lorg/apache/poi/sl/draw/binding/STTextShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "textDeflateInflate"
    .end annotation
.end field

.field public static final enum TEXT_DEFLATE_INFLATE_DEFLATE:Lorg/apache/poi/sl/draw/binding/STTextShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "textDeflateInflateDeflate"
    .end annotation
.end field

.field public static final enum TEXT_DEFLATE_TOP:Lorg/apache/poi/sl/draw/binding/STTextShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "textDeflateTop"
    .end annotation
.end field

.field public static final enum TEXT_DOUBLE_WAVE_1:Lorg/apache/poi/sl/draw/binding/STTextShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "textDoubleWave1"
    .end annotation
.end field

.field public static final enum TEXT_FADE_DOWN:Lorg/apache/poi/sl/draw/binding/STTextShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "textFadeDown"
    .end annotation
.end field

.field public static final enum TEXT_FADE_LEFT:Lorg/apache/poi/sl/draw/binding/STTextShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "textFadeLeft"
    .end annotation
.end field

.field public static final enum TEXT_FADE_RIGHT:Lorg/apache/poi/sl/draw/binding/STTextShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "textFadeRight"
    .end annotation
.end field

.field public static final enum TEXT_FADE_UP:Lorg/apache/poi/sl/draw/binding/STTextShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "textFadeUp"
    .end annotation
.end field

.field public static final enum TEXT_INFLATE:Lorg/apache/poi/sl/draw/binding/STTextShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "textInflate"
    .end annotation
.end field

.field public static final enum TEXT_INFLATE_BOTTOM:Lorg/apache/poi/sl/draw/binding/STTextShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "textInflateBottom"
    .end annotation
.end field

.field public static final enum TEXT_INFLATE_TOP:Lorg/apache/poi/sl/draw/binding/STTextShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "textInflateTop"
    .end annotation
.end field

.field public static final enum TEXT_NO_SHAPE:Lorg/apache/poi/sl/draw/binding/STTextShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "textNoShape"
    .end annotation
.end field

.field public static final enum TEXT_PLAIN:Lorg/apache/poi/sl/draw/binding/STTextShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "textPlain"
    .end annotation
.end field

.field public static final enum TEXT_RING_INSIDE:Lorg/apache/poi/sl/draw/binding/STTextShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "textRingInside"
    .end annotation
.end field

.field public static final enum TEXT_RING_OUTSIDE:Lorg/apache/poi/sl/draw/binding/STTextShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "textRingOutside"
    .end annotation
.end field

.field public static final enum TEXT_SLANT_DOWN:Lorg/apache/poi/sl/draw/binding/STTextShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "textSlantDown"
    .end annotation
.end field

.field public static final enum TEXT_SLANT_UP:Lorg/apache/poi/sl/draw/binding/STTextShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "textSlantUp"
    .end annotation
.end field

.field public static final enum TEXT_STOP:Lorg/apache/poi/sl/draw/binding/STTextShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "textStop"
    .end annotation
.end field

.field public static final enum TEXT_TRIANGLE:Lorg/apache/poi/sl/draw/binding/STTextShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "textTriangle"
    .end annotation
.end field

.field public static final enum TEXT_TRIANGLE_INVERTED:Lorg/apache/poi/sl/draw/binding/STTextShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "textTriangleInverted"
    .end annotation
.end field

.field public static final enum TEXT_WAVE_1:Lorg/apache/poi/sl/draw/binding/STTextShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "textWave1"
    .end annotation
.end field

.field public static final enum TEXT_WAVE_2:Lorg/apache/poi/sl/draw/binding/STTextShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "textWave2"
    .end annotation
.end field

.field public static final enum TEXT_WAVE_4:Lorg/apache/poi/sl/draw/binding/STTextShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "textWave4"
    .end annotation
.end field


# instance fields
.field private final value:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 45

    new-instance v1, Lorg/apache/poi/sl/draw/binding/STTextShapeType;

    move-object v0, v1

    const/4 v2, 0x0

    const-string v3, "textNoShape"

    const-string v4, "TEXT_NO_SHAPE"

    invoke-direct {v1, v4, v2, v3}, Lorg/apache/poi/sl/draw/binding/STTextShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lorg/apache/poi/sl/draw/binding/STTextShapeType;->TEXT_NO_SHAPE:Lorg/apache/poi/sl/draw/binding/STTextShapeType;

    new-instance v2, Lorg/apache/poi/sl/draw/binding/STTextShapeType;

    move-object v1, v2

    const/4 v3, 0x1

    const-string v4, "textPlain"

    const-string v5, "TEXT_PLAIN"

    invoke-direct {v2, v5, v3, v4}, Lorg/apache/poi/sl/draw/binding/STTextShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v2, Lorg/apache/poi/sl/draw/binding/STTextShapeType;->TEXT_PLAIN:Lorg/apache/poi/sl/draw/binding/STTextShapeType;

    new-instance v3, Lorg/apache/poi/sl/draw/binding/STTextShapeType;

    move-object v2, v3

    const/4 v4, 0x2

    const-string v5, "textStop"

    const-string v6, "TEXT_STOP"

    invoke-direct {v3, v6, v4, v5}, Lorg/apache/poi/sl/draw/binding/STTextShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v3, Lorg/apache/poi/sl/draw/binding/STTextShapeType;->TEXT_STOP:Lorg/apache/poi/sl/draw/binding/STTextShapeType;

    new-instance v4, Lorg/apache/poi/sl/draw/binding/STTextShapeType;

    move-object v3, v4

    const/4 v5, 0x3

    const-string v6, "textTriangle"

    const-string v7, "TEXT_TRIANGLE"

    invoke-direct {v4, v7, v5, v6}, Lorg/apache/poi/sl/draw/binding/STTextShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v4, Lorg/apache/poi/sl/draw/binding/STTextShapeType;->TEXT_TRIANGLE:Lorg/apache/poi/sl/draw/binding/STTextShapeType;

    new-instance v5, Lorg/apache/poi/sl/draw/binding/STTextShapeType;

    move-object v4, v5

    const/4 v6, 0x4

    const-string v7, "textTriangleInverted"

    const-string v8, "TEXT_TRIANGLE_INVERTED"

    invoke-direct {v5, v8, v6, v7}, Lorg/apache/poi/sl/draw/binding/STTextShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lorg/apache/poi/sl/draw/binding/STTextShapeType;->TEXT_TRIANGLE_INVERTED:Lorg/apache/poi/sl/draw/binding/STTextShapeType;

    new-instance v6, Lorg/apache/poi/sl/draw/binding/STTextShapeType;

    move-object v5, v6

    const/4 v7, 0x5

    const-string v8, "textChevron"

    const-string v9, "TEXT_CHEVRON"

    invoke-direct {v6, v9, v7, v8}, Lorg/apache/poi/sl/draw/binding/STTextShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v6, Lorg/apache/poi/sl/draw/binding/STTextShapeType;->TEXT_CHEVRON:Lorg/apache/poi/sl/draw/binding/STTextShapeType;

    new-instance v7, Lorg/apache/poi/sl/draw/binding/STTextShapeType;

    move-object v6, v7

    const/4 v8, 0x6

    const-string v9, "textChevronInverted"

    const-string v10, "TEXT_CHEVRON_INVERTED"

    invoke-direct {v7, v10, v8, v9}, Lorg/apache/poi/sl/draw/binding/STTextShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v7, Lorg/apache/poi/sl/draw/binding/STTextShapeType;->TEXT_CHEVRON_INVERTED:Lorg/apache/poi/sl/draw/binding/STTextShapeType;

    new-instance v8, Lorg/apache/poi/sl/draw/binding/STTextShapeType;

    move-object v7, v8

    const/4 v9, 0x7

    const-string v10, "textRingInside"

    const-string v11, "TEXT_RING_INSIDE"

    invoke-direct {v8, v11, v9, v10}, Lorg/apache/poi/sl/draw/binding/STTextShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v8, Lorg/apache/poi/sl/draw/binding/STTextShapeType;->TEXT_RING_INSIDE:Lorg/apache/poi/sl/draw/binding/STTextShapeType;

    new-instance v9, Lorg/apache/poi/sl/draw/binding/STTextShapeType;

    move-object v8, v9

    const/16 v10, 0x8

    const-string v11, "textRingOutside"

    const-string v12, "TEXT_RING_OUTSIDE"

    invoke-direct {v9, v12, v10, v11}, Lorg/apache/poi/sl/draw/binding/STTextShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v9, Lorg/apache/poi/sl/draw/binding/STTextShapeType;->TEXT_RING_OUTSIDE:Lorg/apache/poi/sl/draw/binding/STTextShapeType;

    new-instance v10, Lorg/apache/poi/sl/draw/binding/STTextShapeType;

    move-object v9, v10

    const/16 v11, 0x9

    const-string v12, "textArchUp"

    const-string v13, "TEXT_ARCH_UP"

    invoke-direct {v10, v13, v11, v12}, Lorg/apache/poi/sl/draw/binding/STTextShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v10, Lorg/apache/poi/sl/draw/binding/STTextShapeType;->TEXT_ARCH_UP:Lorg/apache/poi/sl/draw/binding/STTextShapeType;

    new-instance v11, Lorg/apache/poi/sl/draw/binding/STTextShapeType;

    move-object v10, v11

    const/16 v12, 0xa

    const-string v13, "textArchDown"

    const-string v14, "TEXT_ARCH_DOWN"

    invoke-direct {v11, v14, v12, v13}, Lorg/apache/poi/sl/draw/binding/STTextShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v11, Lorg/apache/poi/sl/draw/binding/STTextShapeType;->TEXT_ARCH_DOWN:Lorg/apache/poi/sl/draw/binding/STTextShapeType;

    new-instance v12, Lorg/apache/poi/sl/draw/binding/STTextShapeType;

    move-object v11, v12

    const/16 v13, 0xb

    const-string v14, "textCircle"

    const-string v15, "TEXT_CIRCLE"

    invoke-direct {v12, v15, v13, v14}, Lorg/apache/poi/sl/draw/binding/STTextShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v12, Lorg/apache/poi/sl/draw/binding/STTextShapeType;->TEXT_CIRCLE:Lorg/apache/poi/sl/draw/binding/STTextShapeType;

    new-instance v13, Lorg/apache/poi/sl/draw/binding/STTextShapeType;

    move-object v12, v13

    const/16 v14, 0xc

    const-string v15, "textButton"

    move-object/from16 v41, v0

    const-string v0, "TEXT_BUTTON"

    invoke-direct {v13, v0, v14, v15}, Lorg/apache/poi/sl/draw/binding/STTextShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v13, Lorg/apache/poi/sl/draw/binding/STTextShapeType;->TEXT_BUTTON:Lorg/apache/poi/sl/draw/binding/STTextShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STTextShapeType;

    move-object v13, v0

    const/16 v14, 0xd

    const-string v15, "textArchUpPour"

    move-object/from16 v42, v1

    const-string v1, "TEXT_ARCH_UP_POUR"

    invoke-direct {v0, v1, v14, v15}, Lorg/apache/poi/sl/draw/binding/STTextShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STTextShapeType;->TEXT_ARCH_UP_POUR:Lorg/apache/poi/sl/draw/binding/STTextShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STTextShapeType;

    move-object v14, v0

    const/16 v1, 0xe

    const-string v15, "textArchDownPour"

    move-object/from16 v43, v2

    const-string v2, "TEXT_ARCH_DOWN_POUR"

    invoke-direct {v0, v2, v1, v15}, Lorg/apache/poi/sl/draw/binding/STTextShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STTextShapeType;->TEXT_ARCH_DOWN_POUR:Lorg/apache/poi/sl/draw/binding/STTextShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STTextShapeType;

    move-object v15, v0

    const/16 v1, 0xf

    const-string v2, "textCirclePour"

    move-object/from16 v44, v3

    const-string v3, "TEXT_CIRCLE_POUR"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STTextShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STTextShapeType;->TEXT_CIRCLE_POUR:Lorg/apache/poi/sl/draw/binding/STTextShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STTextShapeType;

    move-object/from16 v16, v0

    const/16 v1, 0x10

    const-string v2, "textButtonPour"

    const-string v3, "TEXT_BUTTON_POUR"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STTextShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STTextShapeType;->TEXT_BUTTON_POUR:Lorg/apache/poi/sl/draw/binding/STTextShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STTextShapeType;

    move-object/from16 v17, v0

    const/16 v1, 0x11

    const-string v2, "textCurveUp"

    const-string v3, "TEXT_CURVE_UP"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STTextShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STTextShapeType;->TEXT_CURVE_UP:Lorg/apache/poi/sl/draw/binding/STTextShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STTextShapeType;

    move-object/from16 v18, v0

    const/16 v1, 0x12

    const-string v2, "textCurveDown"

    const-string v3, "TEXT_CURVE_DOWN"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STTextShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STTextShapeType;->TEXT_CURVE_DOWN:Lorg/apache/poi/sl/draw/binding/STTextShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STTextShapeType;

    move-object/from16 v19, v0

    const/16 v1, 0x13

    const-string v2, "textCanUp"

    const-string v3, "TEXT_CAN_UP"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STTextShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STTextShapeType;->TEXT_CAN_UP:Lorg/apache/poi/sl/draw/binding/STTextShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STTextShapeType;

    move-object/from16 v20, v0

    const/16 v1, 0x14

    const-string v2, "textCanDown"

    const-string v3, "TEXT_CAN_DOWN"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STTextShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STTextShapeType;->TEXT_CAN_DOWN:Lorg/apache/poi/sl/draw/binding/STTextShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STTextShapeType;

    move-object/from16 v21, v0

    const/16 v1, 0x15

    const-string v2, "textWave1"

    const-string v3, "TEXT_WAVE_1"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STTextShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STTextShapeType;->TEXT_WAVE_1:Lorg/apache/poi/sl/draw/binding/STTextShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STTextShapeType;

    move-object/from16 v22, v0

    const/16 v1, 0x16

    const-string v2, "textWave2"

    const-string v3, "TEXT_WAVE_2"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STTextShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STTextShapeType;->TEXT_WAVE_2:Lorg/apache/poi/sl/draw/binding/STTextShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STTextShapeType;

    move-object/from16 v23, v0

    const/16 v1, 0x17

    const-string v2, "textDoubleWave1"

    const-string v3, "TEXT_DOUBLE_WAVE_1"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STTextShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STTextShapeType;->TEXT_DOUBLE_WAVE_1:Lorg/apache/poi/sl/draw/binding/STTextShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STTextShapeType;

    move-object/from16 v24, v0

    const/16 v1, 0x18

    const-string v2, "textWave4"

    const-string v3, "TEXT_WAVE_4"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STTextShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STTextShapeType;->TEXT_WAVE_4:Lorg/apache/poi/sl/draw/binding/STTextShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STTextShapeType;

    move-object/from16 v25, v0

    const/16 v1, 0x19

    const-string v2, "textInflate"

    const-string v3, "TEXT_INFLATE"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STTextShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STTextShapeType;->TEXT_INFLATE:Lorg/apache/poi/sl/draw/binding/STTextShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STTextShapeType;

    move-object/from16 v26, v0

    const/16 v1, 0x1a

    const-string v2, "textDeflate"

    const-string v3, "TEXT_DEFLATE"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STTextShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STTextShapeType;->TEXT_DEFLATE:Lorg/apache/poi/sl/draw/binding/STTextShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STTextShapeType;

    move-object/from16 v27, v0

    const/16 v1, 0x1b

    const-string v2, "textInflateBottom"

    const-string v3, "TEXT_INFLATE_BOTTOM"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STTextShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STTextShapeType;->TEXT_INFLATE_BOTTOM:Lorg/apache/poi/sl/draw/binding/STTextShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STTextShapeType;

    move-object/from16 v28, v0

    const/16 v1, 0x1c

    const-string v2, "textDeflateBottom"

    const-string v3, "TEXT_DEFLATE_BOTTOM"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STTextShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STTextShapeType;->TEXT_DEFLATE_BOTTOM:Lorg/apache/poi/sl/draw/binding/STTextShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STTextShapeType;

    move-object/from16 v29, v0

    const/16 v1, 0x1d

    const-string v2, "textInflateTop"

    const-string v3, "TEXT_INFLATE_TOP"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STTextShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STTextShapeType;->TEXT_INFLATE_TOP:Lorg/apache/poi/sl/draw/binding/STTextShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STTextShapeType;

    move-object/from16 v30, v0

    const/16 v1, 0x1e

    const-string v2, "textDeflateTop"

    const-string v3, "TEXT_DEFLATE_TOP"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STTextShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STTextShapeType;->TEXT_DEFLATE_TOP:Lorg/apache/poi/sl/draw/binding/STTextShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STTextShapeType;

    move-object/from16 v31, v0

    const/16 v1, 0x1f

    const-string v2, "textDeflateInflate"

    const-string v3, "TEXT_DEFLATE_INFLATE"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STTextShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STTextShapeType;->TEXT_DEFLATE_INFLATE:Lorg/apache/poi/sl/draw/binding/STTextShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STTextShapeType;

    move-object/from16 v32, v0

    const/16 v1, 0x20

    const-string v2, "textDeflateInflateDeflate"

    const-string v3, "TEXT_DEFLATE_INFLATE_DEFLATE"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STTextShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STTextShapeType;->TEXT_DEFLATE_INFLATE_DEFLATE:Lorg/apache/poi/sl/draw/binding/STTextShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STTextShapeType;

    move-object/from16 v33, v0

    const/16 v1, 0x21

    const-string v2, "textFadeRight"

    const-string v3, "TEXT_FADE_RIGHT"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STTextShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STTextShapeType;->TEXT_FADE_RIGHT:Lorg/apache/poi/sl/draw/binding/STTextShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STTextShapeType;

    move-object/from16 v34, v0

    const/16 v1, 0x22

    const-string v2, "textFadeLeft"

    const-string v3, "TEXT_FADE_LEFT"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STTextShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STTextShapeType;->TEXT_FADE_LEFT:Lorg/apache/poi/sl/draw/binding/STTextShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STTextShapeType;

    move-object/from16 v35, v0

    const/16 v1, 0x23

    const-string v2, "textFadeUp"

    const-string v3, "TEXT_FADE_UP"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STTextShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STTextShapeType;->TEXT_FADE_UP:Lorg/apache/poi/sl/draw/binding/STTextShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STTextShapeType;

    move-object/from16 v36, v0

    const/16 v1, 0x24

    const-string v2, "textFadeDown"

    const-string v3, "TEXT_FADE_DOWN"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STTextShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STTextShapeType;->TEXT_FADE_DOWN:Lorg/apache/poi/sl/draw/binding/STTextShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STTextShapeType;

    move-object/from16 v37, v0

    const/16 v1, 0x25

    const-string v2, "textSlantUp"

    const-string v3, "TEXT_SLANT_UP"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STTextShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STTextShapeType;->TEXT_SLANT_UP:Lorg/apache/poi/sl/draw/binding/STTextShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STTextShapeType;

    move-object/from16 v38, v0

    const/16 v1, 0x26

    const-string v2, "textSlantDown"

    const-string v3, "TEXT_SLANT_DOWN"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STTextShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STTextShapeType;->TEXT_SLANT_DOWN:Lorg/apache/poi/sl/draw/binding/STTextShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STTextShapeType;

    move-object/from16 v39, v0

    const/16 v1, 0x27

    const-string v2, "textCascadeUp"

    const-string v3, "TEXT_CASCADE_UP"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STTextShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STTextShapeType;->TEXT_CASCADE_UP:Lorg/apache/poi/sl/draw/binding/STTextShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STTextShapeType;

    move-object/from16 v40, v0

    const/16 v1, 0x28

    const-string v2, "textCascadeDown"

    const-string v3, "TEXT_CASCADE_DOWN"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STTextShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STTextShapeType;->TEXT_CASCADE_DOWN:Lorg/apache/poi/sl/draw/binding/STTextShapeType;

    move-object/from16 v0, v41

    move-object/from16 v1, v42

    move-object/from16 v2, v43

    move-object/from16 v3, v44

    filled-new-array/range {v0 .. v40}, [Lorg/apache/poi/sl/draw/binding/STTextShapeType;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STTextShapeType;->$VALUES:[Lorg/apache/poi/sl/draw/binding/STTextShapeType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lorg/apache/poi/sl/draw/binding/STTextShapeType;->value:Ljava/lang/String;

    return-void
.end method

.method public static fromValue(Ljava/lang/String;)Lorg/apache/poi/sl/draw/binding/STTextShapeType;
    .locals 5

    invoke-static {}, Lorg/apache/poi/sl/draw/binding/STTextShapeType;->values()[Lorg/apache/poi/sl/draw/binding/STTextShapeType;

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_1

    aget-object v3, v0, v2

    iget-object v4, v3, Lorg/apache/poi/sl/draw/binding/STTextShapeType;->value:Ljava/lang/String;

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    return-object v3

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static valueOf(Ljava/lang/String;)Lorg/apache/poi/sl/draw/binding/STTextShapeType;
    .locals 1

    const-class v0, Lorg/apache/poi/sl/draw/binding/STTextShapeType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lorg/apache/poi/sl/draw/binding/STTextShapeType;

    return-object p0
.end method

.method public static values()[Lorg/apache/poi/sl/draw/binding/STTextShapeType;
    .locals 1

    sget-object v0, Lorg/apache/poi/sl/draw/binding/STTextShapeType;->$VALUES:[Lorg/apache/poi/sl/draw/binding/STTextShapeType;

    invoke-virtual {v0}, [Lorg/apache/poi/sl/draw/binding/STTextShapeType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lorg/apache/poi/sl/draw/binding/STTextShapeType;

    return-object v0
.end method


# virtual methods
.method public value()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/apache/poi/sl/draw/binding/STTextShapeType;->value:Ljava/lang/String;

    return-object v0
.end method

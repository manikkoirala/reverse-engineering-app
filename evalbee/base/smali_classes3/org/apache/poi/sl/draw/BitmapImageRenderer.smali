.class public Lorg/apache/poi/sl/draw/BitmapImageRenderer;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/poi/sl/draw/ImageRenderer;


# static fields
.field private static final LOG:Lorg/apache/poi/util/POILogger;


# instance fields
.field protected img:Ljava/awt/image/BufferedImage;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    const-class v0, Lorg/apache/poi/sl/draw/ImageRenderer;

    invoke-static {v0}, Lorg/apache/poi/util/POILogFactory;->getLogger(Ljava/lang/Class;)Lorg/apache/poi/util/POILogger;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/sl/draw/BitmapImageRenderer;->LOG:Lorg/apache/poi/util/POILogger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static findTruncatedBlackBox(Ljava/awt/image/BufferedImage;II)I
    .locals 3

    add-int/lit8 p2, p2, -0x1

    :goto_0
    if-lez p2, :cond_2

    add-int/lit8 v0, p1, -0x1

    :goto_1
    if-lez v0, :cond_1

    invoke-virtual {p0, v0, p2}, Ljava/awt/image/BufferedImage;->getRGB(II)I

    move-result v1

    const/high16 v2, -0x1000000

    if-eq v1, v2, :cond_0

    add-int/lit8 p2, p2, 0x1

    return p2

    :cond_0
    div-int/lit8 v1, p1, 0xa

    sub-int/2addr v0, v1

    goto :goto_1

    :cond_1
    add-int/lit8 p2, p2, -0x1

    goto :goto_0

    :cond_2
    const/4 p0, 0x0

    return p0
.end method

.method private static readImage(Ljava/io/InputStream;Ljava/lang/String;)Ljava/awt/image/BufferedImage;
    .locals 14

    invoke-virtual {p0}, Ljava/io/InputStream;->markSupported()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Ljava/io/InputStream;->available()I

    move-result v0

    invoke-virtual {p0, v0}, Ljava/io/InputStream;->mark(I)V

    :cond_0
    new-instance v0, Ljavax/imageio/stream/MemoryCacheImageInputStream;

    invoke-direct {v0, p0}, Ljavax/imageio/stream/MemoryCacheImageInputStream;-><init>(Ljava/io/InputStream;)V

    :try_start_0
    new-instance v1, Ljavax/imageio/stream/MemoryCacheImageInputStream;

    invoke-direct {v1, p0}, Ljavax/imageio/stream/MemoryCacheImageInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    :try_start_1
    invoke-interface {v1}, Ljavax/imageio/stream/ImageInputStream;->mark()V

    invoke-static {v1}, Ljavax/imageio/ImageIO;->getImageReaders(Ljava/lang/Object;)Ljava/util/Iterator;

    move-result-object v0

    const/4 v2, 0x0

    move-object v3, v2

    move-object v4, v3

    :goto_0
    const/4 v5, 0x2

    const/4 v6, 0x0

    if-nez v3, :cond_b

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_b

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljavax/imageio/ImageReader;

    invoke-virtual {v7}, Ljavax/imageio/ImageReader;->getDefaultReadParam()Ljavax/imageio/ImageReadParam;

    move-result-object v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move v9, v6

    :goto_1
    if-nez v3, :cond_a

    const/4 v10, 0x3

    if-ge v9, v10, :cond_a

    :try_start_2
    invoke-interface {v1}, Ljavax/imageio/stream/ImageInputStream;->reset()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_2

    :catch_0
    move-exception v4

    :try_start_3
    invoke-virtual {p0}, Ljava/io/InputStream;->markSupported()Z

    move-result v10

    if-eqz v10, :cond_a

    invoke-virtual {p0}, Ljava/io/InputStream;->reset()V

    invoke-virtual {p0}, Ljava/io/InputStream;->available()I

    move-result v4

    invoke-virtual {p0, v4}, Ljava/io/InputStream;->mark(I)V

    invoke-interface {v1}, Ljavax/imageio/stream/ImageInputStream;->close()V

    new-instance v4, Ljavax/imageio/stream/MemoryCacheImageInputStream;

    invoke-direct {v4, p0}, Ljavax/imageio/stream/MemoryCacheImageInputStream;-><init>(Ljava/io/InputStream;)V

    move-object v1, v4

    :goto_2
    invoke-interface {v1}, Ljavax/imageio/stream/ImageInputStream;->mark()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    const/4 v4, 0x1

    if-eqz v9, :cond_7

    if-eq v9, v4, :cond_4

    if-eq v9, v5, :cond_1

    goto/16 :goto_5

    :cond_1
    :try_start_4
    invoke-virtual {v7, v1, v6, v4}, Ljavax/imageio/ImageReader;->setInput(Ljava/lang/Object;ZZ)V

    invoke-virtual {v7, v6}, Ljavax/imageio/ImageReader;->getHeight(I)I

    move-result v4

    invoke-virtual {v7, v6}, Ljavax/imageio/ImageReader;->getWidth(I)I

    move-result v10

    invoke-virtual {v7, v6}, Ljavax/imageio/ImageReader;->getImageTypes(I)Ljava/util/Iterator;

    move-result-object v11

    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_3

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljavax/imageio/ImageTypeSpecifier;

    invoke-virtual {v11, v10, v4}, Ljavax/imageio/ImageTypeSpecifier;->createBufferedImage(II)Ljava/awt/image/BufferedImage;

    move-result-object v3

    invoke-virtual {v8, v3}, Ljavax/imageio/ImageReadParam;->setDestination(Ljava/awt/image/BufferedImage;)V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/lang/RuntimeException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :try_start_5
    invoke-virtual {v7, v6, v8}, Ljavax/imageio/ImageReader;->read(ILjavax/imageio/ImageReadParam;)Ljava/awt/image/BufferedImage;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :try_start_6
    invoke-virtual {v3}, Ljava/awt/image/BufferedImage;->getType()I

    move-result v11

    if-eq v11, v5, :cond_9

    invoke-static {v3, v10, v4}, Lorg/apache/poi/sl/draw/BitmapImageRenderer;->findTruncatedBlackBox(Ljava/awt/image/BufferedImage;II)I

    move-result v11

    if-ge v11, v4, :cond_9

    new-instance v12, Ljava/awt/image/BufferedImage;

    invoke-direct {v12, v10, v4, v5}, Ljava/awt/image/BufferedImage;-><init>(III)V

    invoke-virtual {v12}, Ljava/awt/image/BufferedImage;->createGraphics()Ljava/awt/Graphics2D;

    move-result-object v4

    invoke-virtual {v4, v6, v6, v10, v11}, Ljava/awt/Graphics2D;->clipRect(IIII)V

    invoke-virtual {v4, v3, v6, v6, v2}, Ljava/awt/Graphics2D;->drawImage(Ljava/awt/Image;IILjava/awt/image/ImageObserver;)Z

    invoke-virtual {v4}, Ljava/awt/Graphics2D;->dispose()V

    invoke-virtual {v3}, Ljava/awt/image/BufferedImage;->flush()V

    move-object v3, v12

    goto/16 :goto_5

    :catchall_0
    move-exception v11

    invoke-virtual {v3}, Ljava/awt/image/BufferedImage;->getType()I

    move-result v12

    if-eq v12, v5, :cond_2

    invoke-static {v3, v10, v4}, Lorg/apache/poi/sl/draw/BitmapImageRenderer;->findTruncatedBlackBox(Ljava/awt/image/BufferedImage;II)I

    move-result v12

    if-ge v12, v4, :cond_2

    new-instance v13, Ljava/awt/image/BufferedImage;

    invoke-direct {v13, v10, v4, v5}, Ljava/awt/image/BufferedImage;-><init>(III)V

    invoke-virtual {v13}, Ljava/awt/image/BufferedImage;->createGraphics()Ljava/awt/Graphics2D;

    move-result-object v4

    invoke-virtual {v4, v6, v6, v10, v12}, Ljava/awt/Graphics2D;->clipRect(IIII)V

    invoke-virtual {v4, v3, v6, v6, v2}, Ljava/awt/Graphics2D;->drawImage(Ljava/awt/Image;IILjava/awt/image/ImageObserver;)Z

    invoke-virtual {v4}, Ljava/awt/Graphics2D;->dispose()V

    invoke-virtual {v3}, Ljava/awt/image/BufferedImage;->flush()V

    move-object v3, v13

    :cond_2
    throw v11

    :cond_3
    new-instance v4, Ljava/io/IOException;

    const-string v10, "unable to load even a truncated version of the image."

    invoke-direct {v4, v10}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    goto :goto_6

    :cond_4
    invoke-virtual {v7, v6}, Ljavax/imageio/ImageReader;->getImageTypes(I)Ljava/util/Iterator;

    move-result-object v10

    :cond_5
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_6

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljavax/imageio/ImageTypeSpecifier;

    invoke-virtual {v11}, Ljavax/imageio/ImageTypeSpecifier;->getBufferedImageType()I

    move-result v12

    const/16 v13, 0xa

    if-ne v12, v13, :cond_5

    invoke-virtual {v8, v11}, Ljavax/imageio/ImageReadParam;->setDestinationType(Ljavax/imageio/ImageTypeSpecifier;)V

    :cond_6
    invoke-virtual {v7, v1, v6, v4}, Ljavax/imageio/ImageReader;->setInput(Ljava/lang/Object;ZZ)V

    :goto_3
    invoke-virtual {v7, v6, v8}, Ljavax/imageio/ImageReader;->read(ILjavax/imageio/ImageReadParam;)Ljava/awt/image/BufferedImage;

    move-result-object v3

    goto :goto_5

    :cond_7
    invoke-virtual {v7, v1, v6, v4}, Ljavax/imageio/ImageReader;->setInput(Ljava/lang/Object;ZZ)V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2
    .catch Ljava/lang/RuntimeException; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    goto :goto_3

    :catch_1
    move-exception v4

    if-ge v9, v5, :cond_9

    :try_start_7
    new-instance v10, Ljava/io/IOException;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "ImageIO runtime exception - "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-nez v9, :cond_8

    const-string v12, "normal"

    goto :goto_4

    :cond_8
    const-string v12, "fallback"

    :goto_4
    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11, v4}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v4, v10

    goto :goto_6

    :cond_9
    :goto_5
    move-object v4, v2

    goto :goto_6

    :catch_2
    move-exception v4

    if-ge v9, v5, :cond_9

    :goto_6
    add-int/lit8 v9, v9, 0x1

    goto/16 :goto_1

    :cond_a
    invoke-virtual {v7}, Ljavax/imageio/ImageReader;->dispose()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    goto/16 :goto_0

    :cond_b
    invoke-interface {v1}, Ljavax/imageio/stream/ImageInputStream;->close()V

    if-nez v3, :cond_d

    if-nez v4, :cond_c

    sget-object p0, Lorg/apache/poi/sl/draw/BitmapImageRenderer;->LOG:Lorg/apache/poi/util/POILogger;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Content-type: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, " is not support. Image ignored."

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    filled-new-array {p1}, [Ljava/lang/Object;

    move-result-object p1

    const/4 v0, 0x5

    invoke-virtual {p0, v0, p1}, Lorg/apache/poi/util/POILogger;->log(I[Ljava/lang/Object;)V

    return-object v2

    :cond_c
    throw v4

    :cond_d
    invoke-virtual {v3}, Ljava/awt/image/BufferedImage;->getType()I

    move-result p0

    if-eq p0, v5, :cond_e

    new-instance p0, Ljava/awt/image/BufferedImage;

    invoke-virtual {v3}, Ljava/awt/image/BufferedImage;->getWidth()I

    move-result p1

    invoke-virtual {v3}, Ljava/awt/image/BufferedImage;->getHeight()I

    move-result v0

    invoke-direct {p0, p1, v0, v5}, Ljava/awt/image/BufferedImage;-><init>(III)V

    invoke-virtual {p0}, Ljava/awt/image/BufferedImage;->getGraphics()Ljava/awt/Graphics;

    move-result-object p1

    invoke-virtual {p1, v3, v6, v6, v2}, Ljava/awt/Graphics;->drawImage(Ljava/awt/Image;IILjava/awt/image/ImageObserver;)Z

    invoke-virtual {p1}, Ljava/awt/Graphics;->dispose()V

    return-object p0

    :cond_e
    return-object v3

    :catchall_1
    move-exception p0

    move-object v0, v1

    goto :goto_7

    :catchall_2
    move-exception p0

    :goto_7
    invoke-interface {v0}, Ljavax/imageio/stream/ImageInputStream;->close()V

    throw p0
.end method


# virtual methods
.method public drawImage(Ljava/awt/Graphics2D;Ljava/awt/geom/Rectangle2D;)Z
    .locals 1

    .line 1
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lorg/apache/poi/sl/draw/BitmapImageRenderer;->drawImage(Ljava/awt/Graphics2D;Ljava/awt/geom/Rectangle2D;Ljava/awt/Insets;)Z

    move-result p1

    return p1
.end method

.method public drawImage(Ljava/awt/Graphics2D;Ljava/awt/geom/Rectangle2D;Ljava/awt/Insets;)Z
    .locals 28

    .line 2
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    iget-object v2, v0, Lorg/apache/poi/sl/draw/BitmapImageRenderer;->img:Ljava/awt/image/BufferedImage;

    const/4 v3, 0x0

    if-nez v2, :cond_0

    return v3

    :cond_0
    if-nez p3, :cond_1

    new-instance v4, Ljava/awt/Insets;

    invoke-direct {v4, v3, v3, v3, v3}, Ljava/awt/Insets;-><init>(IIII)V

    goto :goto_0

    :cond_1
    move-object/from16 v4, p3

    const/4 v3, 0x1

    :goto_0
    iget-object v5, v0, Lorg/apache/poi/sl/draw/BitmapImageRenderer;->img:Ljava/awt/image/BufferedImage;

    invoke-virtual {v5}, Ljava/awt/image/BufferedImage;->getWidth()I

    move-result v5

    iget-object v6, v0, Lorg/apache/poi/sl/draw/BitmapImageRenderer;->img:Ljava/awt/image/BufferedImage;

    invoke-virtual {v6}, Ljava/awt/image/BufferedImage;->getHeight()I

    move-result v6

    iget v7, v4, Ljava/awt/Insets;->left:I

    const v8, 0x186a0

    sub-int v7, v8, v7

    iget v9, v4, Ljava/awt/Insets;->right:I

    sub-int/2addr v7, v9

    int-to-double v9, v7

    const-wide v11, 0x40f86a0000000000L    # 100000.0

    div-double/2addr v9, v11

    iget v7, v4, Ljava/awt/Insets;->top:I

    sub-int/2addr v8, v7

    iget v7, v4, Ljava/awt/Insets;->bottom:I

    sub-int/2addr v8, v7

    int-to-double v7, v8

    div-double/2addr v7, v11

    invoke-virtual/range {p2 .. p2}, Ljava/awt/geom/Rectangle2D;->getWidth()D

    move-result-wide v13

    move/from16 p3, v3

    int-to-double v2, v5

    mul-double/2addr v9, v2

    div-double v16, v13, v9

    invoke-virtual/range {p2 .. p2}, Ljava/awt/geom/Rectangle2D;->getHeight()D

    move-result-wide v9

    int-to-double v5, v6

    mul-double/2addr v7, v5

    div-double v22, v9, v7

    invoke-virtual/range {p2 .. p2}, Ljava/awt/geom/Rectangle2D;->getX()D

    move-result-wide v7

    mul-double v2, v2, v16

    iget v9, v4, Ljava/awt/Insets;->left:I

    int-to-double v9, v9

    mul-double/2addr v2, v9

    div-double/2addr v2, v11

    sub-double v24, v7, v2

    invoke-virtual/range {p2 .. p2}, Ljava/awt/geom/Rectangle2D;->getY()D

    move-result-wide v2

    mul-double v5, v5, v22

    iget v4, v4, Ljava/awt/Insets;->top:I

    int-to-double v7, v4

    mul-double/2addr v5, v7

    div-double/2addr v5, v11

    sub-double v26, v2, v5

    new-instance v2, Ljava/awt/geom/AffineTransform;

    const-wide/16 v18, 0x0

    const-wide/16 v20, 0x0

    move-object v15, v2

    invoke-direct/range {v15 .. v27}, Ljava/awt/geom/AffineTransform;-><init>(DDDDDD)V

    invoke-virtual/range {p1 .. p1}, Ljava/awt/Graphics2D;->getClip()Ljava/awt/Shape;

    move-result-object v3

    if-eqz p3, :cond_2

    invoke-virtual/range {p2 .. p2}, Ljava/awt/geom/Rectangle2D;->getBounds2D()Ljava/awt/geom/Rectangle2D;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/awt/Graphics2D;->clip(Ljava/awt/Shape;)V

    :cond_2
    iget-object v4, v0, Lorg/apache/poi/sl/draw/BitmapImageRenderer;->img:Ljava/awt/image/BufferedImage;

    invoke-virtual {v1, v4, v2}, Ljava/awt/Graphics2D;->drawRenderedImage(Ljava/awt/image/RenderedImage;Ljava/awt/geom/AffineTransform;)V

    invoke-virtual {v1, v3}, Ljava/awt/Graphics2D;->setClip(Ljava/awt/Shape;)V

    const/4 v1, 0x1

    return v1
.end method

.method public getDimension()Ljava/awt/Dimension;
    .locals 3

    iget-object v0, p0, Lorg/apache/poi/sl/draw/BitmapImageRenderer;->img:Ljava/awt/image/BufferedImage;

    if-nez v0, :cond_0

    new-instance v0, Ljava/awt/Dimension;

    const/4 v1, 0x0

    invoke-direct {v0, v1, v1}, Ljava/awt/Dimension;-><init>(II)V

    goto :goto_0

    :cond_0
    new-instance v0, Ljava/awt/Dimension;

    iget-object v1, p0, Lorg/apache/poi/sl/draw/BitmapImageRenderer;->img:Ljava/awt/image/BufferedImage;

    invoke-virtual {v1}, Ljava/awt/image/BufferedImage;->getWidth()I

    move-result v1

    iget-object v2, p0, Lorg/apache/poi/sl/draw/BitmapImageRenderer;->img:Ljava/awt/image/BufferedImage;

    invoke-virtual {v2}, Ljava/awt/image/BufferedImage;->getHeight()I

    move-result v2

    invoke-direct {v0, v1, v2}, Ljava/awt/Dimension;-><init>(II)V

    :goto_0
    return-object v0
.end method

.method public getImage()Ljava/awt/image/BufferedImage;
    .locals 1

    .line 1
    iget-object v0, p0, Lorg/apache/poi/sl/draw/BitmapImageRenderer;->img:Ljava/awt/image/BufferedImage;

    return-object v0
.end method

.method public getImage(Ljava/awt/Dimension;)Ljava/awt/image/BufferedImage;
    .locals 10

    .line 2
    iget-object v0, p0, Lorg/apache/poi/sl/draw/BitmapImageRenderer;->img:Ljava/awt/image/BufferedImage;

    invoke-virtual {v0}, Ljava/awt/image/BufferedImage;->getWidth()I

    move-result v0

    int-to-double v0, v0

    iget-object v2, p0, Lorg/apache/poi/sl/draw/BitmapImageRenderer;->img:Ljava/awt/image/BufferedImage;

    invoke-virtual {v2}, Ljava/awt/image/BufferedImage;->getHeight()I

    move-result v2

    int-to-double v2, v2

    new-instance v4, Ljava/awt/image/BufferedImage;

    double-to-int v5, v0

    double-to-int v6, v2

    const/4 v7, 0x2

    invoke-direct {v4, v5, v6, v7}, Ljava/awt/image/BufferedImage;-><init>(III)V

    invoke-virtual {p1}, Ljava/awt/Dimension;->getWidth()D

    move-result-wide v5

    invoke-virtual {p1}, Ljava/awt/Dimension;->getHeight()D

    move-result-wide v8

    new-instance p1, Ljava/awt/geom/AffineTransform;

    invoke-direct {p1}, Ljava/awt/geom/AffineTransform;-><init>()V

    div-double/2addr v5, v0

    div-double/2addr v8, v2

    invoke-virtual {p1, v5, v6, v8, v9}, Ljava/awt/geom/AffineTransform;->scale(DD)V

    new-instance v0, Ljava/awt/image/AffineTransformOp;

    invoke-direct {v0, p1, v7}, Ljava/awt/image/AffineTransformOp;-><init>(Ljava/awt/geom/AffineTransform;I)V

    iget-object p1, p0, Lorg/apache/poi/sl/draw/BitmapImageRenderer;->img:Ljava/awt/image/BufferedImage;

    invoke-virtual {v0, p1, v4}, Ljava/awt/image/AffineTransformOp;->filter(Ljava/awt/image/BufferedImage;Ljava/awt/image/BufferedImage;)Ljava/awt/image/BufferedImage;

    return-object v4
.end method

.method public loadImage(Ljava/io/InputStream;Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-static {p1, p2}, Lorg/apache/poi/sl/draw/BitmapImageRenderer;->readImage(Ljava/io/InputStream;Ljava/lang/String;)Ljava/awt/image/BufferedImage;

    move-result-object p1

    iput-object p1, p0, Lorg/apache/poi/sl/draw/BitmapImageRenderer;->img:Ljava/awt/image/BufferedImage;

    return-void
.end method

.method public loadImage([BLjava/lang/String;)V
    .locals 1

    .line 2
    new-instance v0, Ljava/io/ByteArrayInputStream;

    invoke-direct {v0, p1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-static {v0, p2}, Lorg/apache/poi/sl/draw/BitmapImageRenderer;->readImage(Ljava/io/InputStream;Ljava/lang/String;)Ljava/awt/image/BufferedImage;

    move-result-object p1

    iput-object p1, p0, Lorg/apache/poi/sl/draw/BitmapImageRenderer;->img:Ljava/awt/image/BufferedImage;

    return-void
.end method

.method public setAlpha(D)V
    .locals 9

    iget-object v0, p0, Lorg/apache/poi/sl/draw/BitmapImageRenderer;->img:Ljava/awt/image/BufferedImage;

    if-nez v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0}, Lorg/apache/poi/sl/draw/BitmapImageRenderer;->getDimension()Ljava/awt/Dimension;

    move-result-object v0

    new-instance v1, Ljava/awt/image/BufferedImage;

    invoke-virtual {v0}, Ljava/awt/Dimension;->getWidth()D

    move-result-wide v2

    double-to-int v2, v2

    invoke-virtual {v0}, Ljava/awt/Dimension;->getHeight()D

    move-result-wide v3

    double-to-int v0, v3

    const/4 v3, 0x2

    invoke-direct {v1, v2, v0, v3}, Ljava/awt/image/BufferedImage;-><init>(III)V

    invoke-virtual {v1}, Ljava/awt/image/BufferedImage;->createGraphics()Ljava/awt/Graphics2D;

    move-result-object v0

    new-instance v2, Ljava/awt/image/RescaleOp;

    const/4 v4, 0x4

    new-array v5, v4, [F

    const/4 v6, 0x0

    const/high16 v7, 0x3f800000    # 1.0f

    aput v7, v5, v6

    const/4 v8, 0x1

    aput v7, v5, v8

    aput v7, v5, v3

    const/4 v3, 0x3

    double-to-float p1, p1

    aput p1, v5, v3

    new-array p1, v4, [F

    fill-array-data p1, :array_0

    const/4 p2, 0x0

    invoke-direct {v2, v5, p1, p2}, Ljava/awt/image/RescaleOp;-><init>([F[FLjava/awt/RenderingHints;)V

    iget-object p1, p0, Lorg/apache/poi/sl/draw/BitmapImageRenderer;->img:Ljava/awt/image/BufferedImage;

    invoke-virtual {v0, p1, v2, v6, v6}, Ljava/awt/Graphics2D;->drawImage(Ljava/awt/image/BufferedImage;Ljava/awt/image/BufferedImageOp;II)V

    invoke-virtual {v0}, Ljava/awt/Graphics2D;->dispose()V

    iput-object v1, p0, Lorg/apache/poi/sl/draw/BitmapImageRenderer;->img:Ljava/awt/image/BufferedImage;

    return-void

    :array_0
    .array-data 4
        0x0
        0x0
        0x0
        0x0
    .end array-data
.end method

.class final enum Lorg/apache/poi/sl/draw/geom/BuiltInGuide;
.super Ljava/lang/Enum;
.source "SourceFile"

# interfaces
.implements Lorg/apache/poi/sl/draw/geom/Formula;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lorg/apache/poi/sl/draw/geom/BuiltInGuide;",
        ">;",
        "Lorg/apache/poi/sl/draw/geom/Formula;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lorg/apache/poi/sl/draw/geom/BuiltInGuide;

.field public static final enum _3cd4:Lorg/apache/poi/sl/draw/geom/BuiltInGuide;

.field public static final enum _3cd8:Lorg/apache/poi/sl/draw/geom/BuiltInGuide;

.field public static final enum _5cd8:Lorg/apache/poi/sl/draw/geom/BuiltInGuide;

.field public static final enum _7cd8:Lorg/apache/poi/sl/draw/geom/BuiltInGuide;

.field public static final enum _b:Lorg/apache/poi/sl/draw/geom/BuiltInGuide;

.field public static final enum _cd2:Lorg/apache/poi/sl/draw/geom/BuiltInGuide;

.field public static final enum _cd4:Lorg/apache/poi/sl/draw/geom/BuiltInGuide;

.field public static final enum _cd8:Lorg/apache/poi/sl/draw/geom/BuiltInGuide;

.field public static final enum _h:Lorg/apache/poi/sl/draw/geom/BuiltInGuide;

.field public static final enum _hc:Lorg/apache/poi/sl/draw/geom/BuiltInGuide;

.field public static final enum _hd2:Lorg/apache/poi/sl/draw/geom/BuiltInGuide;

.field public static final enum _hd3:Lorg/apache/poi/sl/draw/geom/BuiltInGuide;

.field public static final enum _hd4:Lorg/apache/poi/sl/draw/geom/BuiltInGuide;

.field public static final enum _hd5:Lorg/apache/poi/sl/draw/geom/BuiltInGuide;

.field public static final enum _hd6:Lorg/apache/poi/sl/draw/geom/BuiltInGuide;

.field public static final enum _hd8:Lorg/apache/poi/sl/draw/geom/BuiltInGuide;

.field public static final enum _l:Lorg/apache/poi/sl/draw/geom/BuiltInGuide;

.field public static final enum _ls:Lorg/apache/poi/sl/draw/geom/BuiltInGuide;

.field public static final enum _r:Lorg/apache/poi/sl/draw/geom/BuiltInGuide;

.field public static final enum _ss:Lorg/apache/poi/sl/draw/geom/BuiltInGuide;

.field public static final enum _ssd16:Lorg/apache/poi/sl/draw/geom/BuiltInGuide;

.field public static final enum _ssd2:Lorg/apache/poi/sl/draw/geom/BuiltInGuide;

.field public static final enum _ssd32:Lorg/apache/poi/sl/draw/geom/BuiltInGuide;

.field public static final enum _ssd4:Lorg/apache/poi/sl/draw/geom/BuiltInGuide;

.field public static final enum _ssd6:Lorg/apache/poi/sl/draw/geom/BuiltInGuide;

.field public static final enum _ssd8:Lorg/apache/poi/sl/draw/geom/BuiltInGuide;

.field public static final enum _t:Lorg/apache/poi/sl/draw/geom/BuiltInGuide;

.field public static final enum _vc:Lorg/apache/poi/sl/draw/geom/BuiltInGuide;

.field public static final enum _w:Lorg/apache/poi/sl/draw/geom/BuiltInGuide;

.field public static final enum _wd10:Lorg/apache/poi/sl/draw/geom/BuiltInGuide;

.field public static final enum _wd2:Lorg/apache/poi/sl/draw/geom/BuiltInGuide;

.field public static final enum _wd3:Lorg/apache/poi/sl/draw/geom/BuiltInGuide;

.field public static final enum _wd32:Lorg/apache/poi/sl/draw/geom/BuiltInGuide;

.field public static final enum _wd4:Lorg/apache/poi/sl/draw/geom/BuiltInGuide;

.field public static final enum _wd5:Lorg/apache/poi/sl/draw/geom/BuiltInGuide;

.field public static final enum _wd6:Lorg/apache/poi/sl/draw/geom/BuiltInGuide;

.field public static final enum _wd8:Lorg/apache/poi/sl/draw/geom/BuiltInGuide;


# direct methods
.method public static constructor <clinit>()V
    .locals 40

    new-instance v1, Lorg/apache/poi/sl/draw/geom/BuiltInGuide;

    move-object v0, v1

    const-string v2, "_3cd4"

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lorg/apache/poi/sl/draw/geom/BuiltInGuide;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lorg/apache/poi/sl/draw/geom/BuiltInGuide;->_3cd4:Lorg/apache/poi/sl/draw/geom/BuiltInGuide;

    new-instance v2, Lorg/apache/poi/sl/draw/geom/BuiltInGuide;

    move-object v1, v2

    const-string v3, "_3cd8"

    const/4 v4, 0x1

    invoke-direct {v2, v3, v4}, Lorg/apache/poi/sl/draw/geom/BuiltInGuide;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lorg/apache/poi/sl/draw/geom/BuiltInGuide;->_3cd8:Lorg/apache/poi/sl/draw/geom/BuiltInGuide;

    new-instance v3, Lorg/apache/poi/sl/draw/geom/BuiltInGuide;

    move-object v2, v3

    const-string v4, "_5cd8"

    const/4 v5, 0x2

    invoke-direct {v3, v4, v5}, Lorg/apache/poi/sl/draw/geom/BuiltInGuide;-><init>(Ljava/lang/String;I)V

    sput-object v3, Lorg/apache/poi/sl/draw/geom/BuiltInGuide;->_5cd8:Lorg/apache/poi/sl/draw/geom/BuiltInGuide;

    new-instance v4, Lorg/apache/poi/sl/draw/geom/BuiltInGuide;

    move-object v3, v4

    const-string v5, "_7cd8"

    const/4 v6, 0x3

    invoke-direct {v4, v5, v6}, Lorg/apache/poi/sl/draw/geom/BuiltInGuide;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lorg/apache/poi/sl/draw/geom/BuiltInGuide;->_7cd8:Lorg/apache/poi/sl/draw/geom/BuiltInGuide;

    new-instance v5, Lorg/apache/poi/sl/draw/geom/BuiltInGuide;

    move-object v4, v5

    const-string v6, "_b"

    const/4 v7, 0x4

    invoke-direct {v5, v6, v7}, Lorg/apache/poi/sl/draw/geom/BuiltInGuide;-><init>(Ljava/lang/String;I)V

    sput-object v5, Lorg/apache/poi/sl/draw/geom/BuiltInGuide;->_b:Lorg/apache/poi/sl/draw/geom/BuiltInGuide;

    new-instance v6, Lorg/apache/poi/sl/draw/geom/BuiltInGuide;

    move-object v5, v6

    const-string v7, "_cd2"

    const/4 v8, 0x5

    invoke-direct {v6, v7, v8}, Lorg/apache/poi/sl/draw/geom/BuiltInGuide;-><init>(Ljava/lang/String;I)V

    sput-object v6, Lorg/apache/poi/sl/draw/geom/BuiltInGuide;->_cd2:Lorg/apache/poi/sl/draw/geom/BuiltInGuide;

    new-instance v7, Lorg/apache/poi/sl/draw/geom/BuiltInGuide;

    move-object v6, v7

    const-string v8, "_cd4"

    const/4 v9, 0x6

    invoke-direct {v7, v8, v9}, Lorg/apache/poi/sl/draw/geom/BuiltInGuide;-><init>(Ljava/lang/String;I)V

    sput-object v7, Lorg/apache/poi/sl/draw/geom/BuiltInGuide;->_cd4:Lorg/apache/poi/sl/draw/geom/BuiltInGuide;

    new-instance v8, Lorg/apache/poi/sl/draw/geom/BuiltInGuide;

    move-object v7, v8

    const-string v9, "_cd8"

    const/4 v10, 0x7

    invoke-direct {v8, v9, v10}, Lorg/apache/poi/sl/draw/geom/BuiltInGuide;-><init>(Ljava/lang/String;I)V

    sput-object v8, Lorg/apache/poi/sl/draw/geom/BuiltInGuide;->_cd8:Lorg/apache/poi/sl/draw/geom/BuiltInGuide;

    new-instance v9, Lorg/apache/poi/sl/draw/geom/BuiltInGuide;

    move-object v8, v9

    const-string v10, "_hc"

    const/16 v11, 0x8

    invoke-direct {v9, v10, v11}, Lorg/apache/poi/sl/draw/geom/BuiltInGuide;-><init>(Ljava/lang/String;I)V

    sput-object v9, Lorg/apache/poi/sl/draw/geom/BuiltInGuide;->_hc:Lorg/apache/poi/sl/draw/geom/BuiltInGuide;

    new-instance v10, Lorg/apache/poi/sl/draw/geom/BuiltInGuide;

    move-object v9, v10

    const-string v11, "_h"

    const/16 v12, 0x9

    invoke-direct {v10, v11, v12}, Lorg/apache/poi/sl/draw/geom/BuiltInGuide;-><init>(Ljava/lang/String;I)V

    sput-object v10, Lorg/apache/poi/sl/draw/geom/BuiltInGuide;->_h:Lorg/apache/poi/sl/draw/geom/BuiltInGuide;

    new-instance v11, Lorg/apache/poi/sl/draw/geom/BuiltInGuide;

    move-object v10, v11

    const-string v12, "_hd2"

    const/16 v13, 0xa

    invoke-direct {v11, v12, v13}, Lorg/apache/poi/sl/draw/geom/BuiltInGuide;-><init>(Ljava/lang/String;I)V

    sput-object v11, Lorg/apache/poi/sl/draw/geom/BuiltInGuide;->_hd2:Lorg/apache/poi/sl/draw/geom/BuiltInGuide;

    new-instance v12, Lorg/apache/poi/sl/draw/geom/BuiltInGuide;

    move-object v11, v12

    const-string v13, "_hd3"

    const/16 v14, 0xb

    invoke-direct {v12, v13, v14}, Lorg/apache/poi/sl/draw/geom/BuiltInGuide;-><init>(Ljava/lang/String;I)V

    sput-object v12, Lorg/apache/poi/sl/draw/geom/BuiltInGuide;->_hd3:Lorg/apache/poi/sl/draw/geom/BuiltInGuide;

    new-instance v13, Lorg/apache/poi/sl/draw/geom/BuiltInGuide;

    move-object v12, v13

    const-string v14, "_hd4"

    const/16 v15, 0xc

    invoke-direct {v13, v14, v15}, Lorg/apache/poi/sl/draw/geom/BuiltInGuide;-><init>(Ljava/lang/String;I)V

    sput-object v13, Lorg/apache/poi/sl/draw/geom/BuiltInGuide;->_hd4:Lorg/apache/poi/sl/draw/geom/BuiltInGuide;

    new-instance v14, Lorg/apache/poi/sl/draw/geom/BuiltInGuide;

    move-object v13, v14

    const-string v15, "_hd5"

    move-object/from16 v37, v0

    const/16 v0, 0xd

    invoke-direct {v14, v15, v0}, Lorg/apache/poi/sl/draw/geom/BuiltInGuide;-><init>(Ljava/lang/String;I)V

    sput-object v14, Lorg/apache/poi/sl/draw/geom/BuiltInGuide;->_hd5:Lorg/apache/poi/sl/draw/geom/BuiltInGuide;

    new-instance v0, Lorg/apache/poi/sl/draw/geom/BuiltInGuide;

    move-object v14, v0

    const-string v15, "_hd6"

    move-object/from16 v38, v1

    const/16 v1, 0xe

    invoke-direct {v0, v15, v1}, Lorg/apache/poi/sl/draw/geom/BuiltInGuide;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/sl/draw/geom/BuiltInGuide;->_hd6:Lorg/apache/poi/sl/draw/geom/BuiltInGuide;

    new-instance v0, Lorg/apache/poi/sl/draw/geom/BuiltInGuide;

    move-object v15, v0

    const-string v1, "_hd8"

    move-object/from16 v39, v2

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/sl/draw/geom/BuiltInGuide;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/sl/draw/geom/BuiltInGuide;->_hd8:Lorg/apache/poi/sl/draw/geom/BuiltInGuide;

    new-instance v0, Lorg/apache/poi/sl/draw/geom/BuiltInGuide;

    move-object/from16 v16, v0

    const-string v1, "_l"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/sl/draw/geom/BuiltInGuide;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/sl/draw/geom/BuiltInGuide;->_l:Lorg/apache/poi/sl/draw/geom/BuiltInGuide;

    new-instance v0, Lorg/apache/poi/sl/draw/geom/BuiltInGuide;

    move-object/from16 v17, v0

    const-string v1, "_ls"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/sl/draw/geom/BuiltInGuide;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/sl/draw/geom/BuiltInGuide;->_ls:Lorg/apache/poi/sl/draw/geom/BuiltInGuide;

    new-instance v0, Lorg/apache/poi/sl/draw/geom/BuiltInGuide;

    move-object/from16 v18, v0

    const-string v1, "_r"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/sl/draw/geom/BuiltInGuide;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/sl/draw/geom/BuiltInGuide;->_r:Lorg/apache/poi/sl/draw/geom/BuiltInGuide;

    new-instance v0, Lorg/apache/poi/sl/draw/geom/BuiltInGuide;

    move-object/from16 v19, v0

    const-string v1, "_ss"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/sl/draw/geom/BuiltInGuide;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/sl/draw/geom/BuiltInGuide;->_ss:Lorg/apache/poi/sl/draw/geom/BuiltInGuide;

    new-instance v0, Lorg/apache/poi/sl/draw/geom/BuiltInGuide;

    move-object/from16 v20, v0

    const-string v1, "_ssd2"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/sl/draw/geom/BuiltInGuide;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/sl/draw/geom/BuiltInGuide;->_ssd2:Lorg/apache/poi/sl/draw/geom/BuiltInGuide;

    new-instance v0, Lorg/apache/poi/sl/draw/geom/BuiltInGuide;

    move-object/from16 v21, v0

    const-string v1, "_ssd4"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/sl/draw/geom/BuiltInGuide;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/sl/draw/geom/BuiltInGuide;->_ssd4:Lorg/apache/poi/sl/draw/geom/BuiltInGuide;

    new-instance v0, Lorg/apache/poi/sl/draw/geom/BuiltInGuide;

    move-object/from16 v22, v0

    const-string v1, "_ssd6"

    const/16 v2, 0x16

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/sl/draw/geom/BuiltInGuide;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/sl/draw/geom/BuiltInGuide;->_ssd6:Lorg/apache/poi/sl/draw/geom/BuiltInGuide;

    new-instance v0, Lorg/apache/poi/sl/draw/geom/BuiltInGuide;

    move-object/from16 v23, v0

    const-string v1, "_ssd8"

    const/16 v2, 0x17

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/sl/draw/geom/BuiltInGuide;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/sl/draw/geom/BuiltInGuide;->_ssd8:Lorg/apache/poi/sl/draw/geom/BuiltInGuide;

    new-instance v0, Lorg/apache/poi/sl/draw/geom/BuiltInGuide;

    move-object/from16 v24, v0

    const-string v1, "_ssd16"

    const/16 v2, 0x18

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/sl/draw/geom/BuiltInGuide;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/sl/draw/geom/BuiltInGuide;->_ssd16:Lorg/apache/poi/sl/draw/geom/BuiltInGuide;

    new-instance v0, Lorg/apache/poi/sl/draw/geom/BuiltInGuide;

    move-object/from16 v25, v0

    const-string v1, "_ssd32"

    const/16 v2, 0x19

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/sl/draw/geom/BuiltInGuide;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/sl/draw/geom/BuiltInGuide;->_ssd32:Lorg/apache/poi/sl/draw/geom/BuiltInGuide;

    new-instance v0, Lorg/apache/poi/sl/draw/geom/BuiltInGuide;

    move-object/from16 v26, v0

    const-string v1, "_t"

    const/16 v2, 0x1a

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/sl/draw/geom/BuiltInGuide;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/sl/draw/geom/BuiltInGuide;->_t:Lorg/apache/poi/sl/draw/geom/BuiltInGuide;

    new-instance v0, Lorg/apache/poi/sl/draw/geom/BuiltInGuide;

    move-object/from16 v27, v0

    const-string v1, "_vc"

    const/16 v2, 0x1b

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/sl/draw/geom/BuiltInGuide;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/sl/draw/geom/BuiltInGuide;->_vc:Lorg/apache/poi/sl/draw/geom/BuiltInGuide;

    new-instance v0, Lorg/apache/poi/sl/draw/geom/BuiltInGuide;

    move-object/from16 v28, v0

    const-string v1, "_w"

    const/16 v2, 0x1c

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/sl/draw/geom/BuiltInGuide;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/sl/draw/geom/BuiltInGuide;->_w:Lorg/apache/poi/sl/draw/geom/BuiltInGuide;

    new-instance v0, Lorg/apache/poi/sl/draw/geom/BuiltInGuide;

    move-object/from16 v29, v0

    const-string v1, "_wd2"

    const/16 v2, 0x1d

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/sl/draw/geom/BuiltInGuide;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/sl/draw/geom/BuiltInGuide;->_wd2:Lorg/apache/poi/sl/draw/geom/BuiltInGuide;

    new-instance v0, Lorg/apache/poi/sl/draw/geom/BuiltInGuide;

    move-object/from16 v30, v0

    const-string v1, "_wd3"

    const/16 v2, 0x1e

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/sl/draw/geom/BuiltInGuide;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/sl/draw/geom/BuiltInGuide;->_wd3:Lorg/apache/poi/sl/draw/geom/BuiltInGuide;

    new-instance v0, Lorg/apache/poi/sl/draw/geom/BuiltInGuide;

    move-object/from16 v31, v0

    const-string v1, "_wd4"

    const/16 v2, 0x1f

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/sl/draw/geom/BuiltInGuide;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/sl/draw/geom/BuiltInGuide;->_wd4:Lorg/apache/poi/sl/draw/geom/BuiltInGuide;

    new-instance v0, Lorg/apache/poi/sl/draw/geom/BuiltInGuide;

    move-object/from16 v32, v0

    const-string v1, "_wd5"

    const/16 v2, 0x20

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/sl/draw/geom/BuiltInGuide;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/sl/draw/geom/BuiltInGuide;->_wd5:Lorg/apache/poi/sl/draw/geom/BuiltInGuide;

    new-instance v0, Lorg/apache/poi/sl/draw/geom/BuiltInGuide;

    move-object/from16 v33, v0

    const-string v1, "_wd6"

    const/16 v2, 0x21

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/sl/draw/geom/BuiltInGuide;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/sl/draw/geom/BuiltInGuide;->_wd6:Lorg/apache/poi/sl/draw/geom/BuiltInGuide;

    new-instance v0, Lorg/apache/poi/sl/draw/geom/BuiltInGuide;

    move-object/from16 v34, v0

    const-string v1, "_wd8"

    const/16 v2, 0x22

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/sl/draw/geom/BuiltInGuide;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/sl/draw/geom/BuiltInGuide;->_wd8:Lorg/apache/poi/sl/draw/geom/BuiltInGuide;

    new-instance v0, Lorg/apache/poi/sl/draw/geom/BuiltInGuide;

    move-object/from16 v35, v0

    const-string v1, "_wd10"

    const/16 v2, 0x23

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/sl/draw/geom/BuiltInGuide;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/sl/draw/geom/BuiltInGuide;->_wd10:Lorg/apache/poi/sl/draw/geom/BuiltInGuide;

    new-instance v0, Lorg/apache/poi/sl/draw/geom/BuiltInGuide;

    move-object/from16 v36, v0

    const-string v1, "_wd32"

    const/16 v2, 0x24

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/sl/draw/geom/BuiltInGuide;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/sl/draw/geom/BuiltInGuide;->_wd32:Lorg/apache/poi/sl/draw/geom/BuiltInGuide;

    move-object/from16 v0, v37

    move-object/from16 v1, v38

    move-object/from16 v2, v39

    filled-new-array/range {v0 .. v36}, [Lorg/apache/poi/sl/draw/geom/BuiltInGuide;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/sl/draw/geom/BuiltInGuide;->$VALUES:[Lorg/apache/poi/sl/draw/geom/BuiltInGuide;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lorg/apache/poi/sl/draw/geom/BuiltInGuide;
    .locals 1

    const-class v0, Lorg/apache/poi/sl/draw/geom/BuiltInGuide;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lorg/apache/poi/sl/draw/geom/BuiltInGuide;

    return-object p0
.end method

.method public static values()[Lorg/apache/poi/sl/draw/geom/BuiltInGuide;
    .locals 1

    sget-object v0, Lorg/apache/poi/sl/draw/geom/BuiltInGuide;->$VALUES:[Lorg/apache/poi/sl/draw/geom/BuiltInGuide;

    invoke-virtual {v0}, [Lorg/apache/poi/sl/draw/geom/BuiltInGuide;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lorg/apache/poi/sl/draw/geom/BuiltInGuide;

    return-object v0
.end method


# virtual methods
.method public evaluate(Lorg/apache/poi/sl/draw/geom/Context;)D
    .locals 22

    invoke-virtual/range {p1 .. p1}, Lorg/apache/poi/sl/draw/geom/Context;->getShapeAnchor()Ljava/awt/geom/Rectangle2D;

    move-result-object v0

    invoke-virtual {v0}, Ljava/awt/geom/Rectangle2D;->getHeight()D

    move-result-wide v1

    invoke-virtual {v0}, Ljava/awt/geom/Rectangle2D;->getWidth()D

    move-result-wide v3

    invoke-static {v3, v4, v1, v2}, Ljava/lang/Math;->min(DD)D

    move-result-wide v5

    sget-object v7, Lorg/apache/poi/sl/draw/geom/BuiltInGuide$1;->$SwitchMap$org$apache$poi$sl$draw$geom$BuiltInGuide:[I

    invoke-virtual/range {p0 .. p0}, Ljava/lang/Enum;->ordinal()I

    move-result v8

    aget v7, v7, v8

    const-wide/high16 v8, 0x4040000000000000L    # 32.0

    const-wide/high16 v10, 0x4014000000000000L    # 5.0

    const-wide/high16 v12, 0x4008000000000000L    # 3.0

    const-wide/high16 v14, 0x4020000000000000L    # 8.0

    const-wide/high16 v16, 0x4018000000000000L    # 6.0

    const-wide/high16 v18, 0x4010000000000000L    # 4.0

    const-wide/high16 v20, 0x4000000000000000L    # 2.0

    packed-switch v7, :pswitch_data_0

    const-wide/16 v0, 0x0

    return-wide v0

    :pswitch_0
    div-double/2addr v3, v8

    return-wide v3

    :pswitch_1
    const-wide/high16 v0, 0x4024000000000000L    # 10.0

    div-double/2addr v3, v0

    return-wide v3

    :pswitch_2
    div-double/2addr v3, v14

    return-wide v3

    :pswitch_3
    div-double v3, v3, v16

    return-wide v3

    :pswitch_4
    div-double/2addr v3, v10

    return-wide v3

    :pswitch_5
    div-double v3, v3, v18

    return-wide v3

    :pswitch_6
    div-double/2addr v3, v12

    return-wide v3

    :pswitch_7
    div-double v3, v3, v20

    :pswitch_8
    return-wide v3

    :pswitch_9
    invoke-virtual {v0}, Ljava/awt/geom/Rectangle2D;->getCenterY()D

    move-result-wide v0

    return-wide v0

    :pswitch_a
    div-double/2addr v5, v8

    return-wide v5

    :pswitch_b
    const-wide/high16 v0, 0x4030000000000000L    # 16.0

    div-double/2addr v5, v0

    return-wide v5

    :pswitch_c
    div-double/2addr v5, v14

    return-wide v5

    :pswitch_d
    div-double v5, v5, v16

    return-wide v5

    :pswitch_e
    div-double v5, v5, v18

    return-wide v5

    :pswitch_f
    div-double v5, v5, v20

    :pswitch_10
    return-wide v5

    :pswitch_11
    invoke-static {v3, v4, v1, v2}, Ljava/lang/Math;->max(DD)D

    move-result-wide v0

    return-wide v0

    :pswitch_12
    div-double/2addr v1, v14

    return-wide v1

    :pswitch_13
    div-double v1, v1, v16

    return-wide v1

    :pswitch_14
    div-double/2addr v1, v10

    return-wide v1

    :pswitch_15
    div-double v1, v1, v18

    return-wide v1

    :pswitch_16
    div-double/2addr v1, v12

    return-wide v1

    :pswitch_17
    div-double v1, v1, v20

    :pswitch_18
    return-wide v1

    :pswitch_19
    invoke-virtual {v0}, Ljava/awt/geom/Rectangle2D;->getCenterX()D

    move-result-wide v0

    return-wide v0

    :pswitch_1a
    const-wide v0, 0x4144997000000000L    # 2700000.0

    return-wide v0

    :pswitch_1b
    const-wide v0, 0x4154997000000000L    # 5400000.0

    return-wide v0

    :pswitch_1c
    const-wide v0, 0x4164997000000000L    # 1.08E7

    return-wide v0

    :pswitch_1d
    invoke-virtual {v0}, Ljava/awt/geom/Rectangle2D;->getMaxX()D

    move-result-wide v0

    return-wide v0

    :pswitch_1e
    invoke-virtual {v0}, Ljava/awt/geom/Rectangle2D;->getX()D

    move-result-wide v0

    return-wide v0

    :pswitch_1f
    invoke-virtual {v0}, Ljava/awt/geom/Rectangle2D;->getMaxY()D

    move-result-wide v0

    return-wide v0

    :pswitch_20
    invoke-virtual {v0}, Ljava/awt/geom/Rectangle2D;->getY()D

    move-result-wide v0

    return-wide v0

    :pswitch_21
    const-wide v0, 0x4172064200000000L    # 1.89E7

    return-wide v0

    :pswitch_22
    const-wide v0, 0x4169bfcc00000000L    # 1.35E7

    return-wide v0

    :pswitch_23
    const-wide v0, 0x415ee62800000000L    # 8100000.0

    return-wide v0

    :pswitch_24
    const-wide v0, 0x416ee62800000000L    # 1.62E7

    return-wide v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_24
        :pswitch_23
        :pswitch_22
        :pswitch_21
        :pswitch_20
        :pswitch_1f
        :pswitch_1e
        :pswitch_1d
        :pswitch_1c
        :pswitch_1b
        :pswitch_1a
        :pswitch_19
        :pswitch_18
        :pswitch_17
        :pswitch_16
        :pswitch_15
        :pswitch_14
        :pswitch_13
        :pswitch_12
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public getName()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

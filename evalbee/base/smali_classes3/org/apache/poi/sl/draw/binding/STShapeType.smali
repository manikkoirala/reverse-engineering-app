.class public final enum Lorg/apache/poi/sl/draw/binding/STShapeType;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lorg/apache/poi/sl/draw/binding/STShapeType;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/xml/bind/annotation/XmlEnum;
.end annotation

.annotation runtime Ljavax/xml/bind/annotation/XmlType;
    name = "ST_ShapeType"
    namespace = "http://schemas.openxmlformats.org/drawingml/2006/main"
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lorg/apache/poi/sl/draw/binding/STShapeType;

.field public static final enum ACCENT_BORDER_CALLOUT_1:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "accentBorderCallout1"
    .end annotation
.end field

.field public static final enum ACCENT_BORDER_CALLOUT_2:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "accentBorderCallout2"
    .end annotation
.end field

.field public static final enum ACCENT_BORDER_CALLOUT_3:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "accentBorderCallout3"
    .end annotation
.end field

.field public static final enum ACCENT_CALLOUT_1:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "accentCallout1"
    .end annotation
.end field

.field public static final enum ACCENT_CALLOUT_2:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "accentCallout2"
    .end annotation
.end field

.field public static final enum ACCENT_CALLOUT_3:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "accentCallout3"
    .end annotation
.end field

.field public static final enum ACTION_BUTTON_BACK_PREVIOUS:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "actionButtonBackPrevious"
    .end annotation
.end field

.field public static final enum ACTION_BUTTON_BEGINNING:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "actionButtonBeginning"
    .end annotation
.end field

.field public static final enum ACTION_BUTTON_BLANK:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "actionButtonBlank"
    .end annotation
.end field

.field public static final enum ACTION_BUTTON_DOCUMENT:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "actionButtonDocument"
    .end annotation
.end field

.field public static final enum ACTION_BUTTON_END:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "actionButtonEnd"
    .end annotation
.end field

.field public static final enum ACTION_BUTTON_FORWARD_NEXT:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "actionButtonForwardNext"
    .end annotation
.end field

.field public static final enum ACTION_BUTTON_HELP:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "actionButtonHelp"
    .end annotation
.end field

.field public static final enum ACTION_BUTTON_HOME:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "actionButtonHome"
    .end annotation
.end field

.field public static final enum ACTION_BUTTON_INFORMATION:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "actionButtonInformation"
    .end annotation
.end field

.field public static final enum ACTION_BUTTON_MOVIE:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "actionButtonMovie"
    .end annotation
.end field

.field public static final enum ACTION_BUTTON_RETURN:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "actionButtonReturn"
    .end annotation
.end field

.field public static final enum ACTION_BUTTON_SOUND:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "actionButtonSound"
    .end annotation
.end field

.field public static final enum ARC:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "arc"
    .end annotation
.end field

.field public static final enum BENT_ARROW:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "bentArrow"
    .end annotation
.end field

.field public static final enum BENT_CONNECTOR_2:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "bentConnector2"
    .end annotation
.end field

.field public static final enum BENT_CONNECTOR_3:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "bentConnector3"
    .end annotation
.end field

.field public static final enum BENT_CONNECTOR_4:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "bentConnector4"
    .end annotation
.end field

.field public static final enum BENT_CONNECTOR_5:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "bentConnector5"
    .end annotation
.end field

.field public static final enum BENT_UP_ARROW:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "bentUpArrow"
    .end annotation
.end field

.field public static final enum BEVEL:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "bevel"
    .end annotation
.end field

.field public static final enum BLOCK_ARC:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "blockArc"
    .end annotation
.end field

.field public static final enum BORDER_CALLOUT_1:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "borderCallout1"
    .end annotation
.end field

.field public static final enum BORDER_CALLOUT_2:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "borderCallout2"
    .end annotation
.end field

.field public static final enum BORDER_CALLOUT_3:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "borderCallout3"
    .end annotation
.end field

.field public static final enum BRACE_PAIR:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "bracePair"
    .end annotation
.end field

.field public static final enum BRACKET_PAIR:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "bracketPair"
    .end annotation
.end field

.field public static final enum CALLOUT_1:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "callout1"
    .end annotation
.end field

.field public static final enum CALLOUT_2:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "callout2"
    .end annotation
.end field

.field public static final enum CALLOUT_3:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "callout3"
    .end annotation
.end field

.field public static final enum CAN:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "can"
    .end annotation
.end field

.field public static final enum CHART_PLUS:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "chartPlus"
    .end annotation
.end field

.field public static final enum CHART_STAR:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "chartStar"
    .end annotation
.end field

.field public static final enum CHART_X:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "chartX"
    .end annotation
.end field

.field public static final enum CHEVRON:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "chevron"
    .end annotation
.end field

.field public static final enum CHORD:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "chord"
    .end annotation
.end field

.field public static final enum CIRCULAR_ARROW:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "circularArrow"
    .end annotation
.end field

.field public static final enum CLOUD:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "cloud"
    .end annotation
.end field

.field public static final enum CLOUD_CALLOUT:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "cloudCallout"
    .end annotation
.end field

.field public static final enum CORNER:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "corner"
    .end annotation
.end field

.field public static final enum CORNER_TABS:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "cornerTabs"
    .end annotation
.end field

.field public static final enum CUBE:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "cube"
    .end annotation
.end field

.field public static final enum CURVED_CONNECTOR_2:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "curvedConnector2"
    .end annotation
.end field

.field public static final enum CURVED_CONNECTOR_3:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "curvedConnector3"
    .end annotation
.end field

.field public static final enum CURVED_CONNECTOR_4:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "curvedConnector4"
    .end annotation
.end field

.field public static final enum CURVED_CONNECTOR_5:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "curvedConnector5"
    .end annotation
.end field

.field public static final enum CURVED_DOWN_ARROW:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "curvedDownArrow"
    .end annotation
.end field

.field public static final enum CURVED_LEFT_ARROW:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "curvedLeftArrow"
    .end annotation
.end field

.field public static final enum CURVED_RIGHT_ARROW:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "curvedRightArrow"
    .end annotation
.end field

.field public static final enum CURVED_UP_ARROW:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "curvedUpArrow"
    .end annotation
.end field

.field public static final enum DECAGON:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "decagon"
    .end annotation
.end field

.field public static final enum DIAG_STRIPE:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "diagStripe"
    .end annotation
.end field

.field public static final enum DIAMOND:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "diamond"
    .end annotation
.end field

.field public static final enum DODECAGON:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "dodecagon"
    .end annotation
.end field

.field public static final enum DONUT:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "donut"
    .end annotation
.end field

.field public static final enum DOUBLE_WAVE:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "doubleWave"
    .end annotation
.end field

.field public static final enum DOWN_ARROW:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "downArrow"
    .end annotation
.end field

.field public static final enum DOWN_ARROW_CALLOUT:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "downArrowCallout"
    .end annotation
.end field

.field public static final enum ELLIPSE:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "ellipse"
    .end annotation
.end field

.field public static final enum ELLIPSE_RIBBON:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "ellipseRibbon"
    .end annotation
.end field

.field public static final enum ELLIPSE_RIBBON_2:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "ellipseRibbon2"
    .end annotation
.end field

.field public static final enum FLOW_CHART_ALTERNATE_PROCESS:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "flowChartAlternateProcess"
    .end annotation
.end field

.field public static final enum FLOW_CHART_COLLATE:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "flowChartCollate"
    .end annotation
.end field

.field public static final enum FLOW_CHART_CONNECTOR:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "flowChartConnector"
    .end annotation
.end field

.field public static final enum FLOW_CHART_DECISION:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "flowChartDecision"
    .end annotation
.end field

.field public static final enum FLOW_CHART_DELAY:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "flowChartDelay"
    .end annotation
.end field

.field public static final enum FLOW_CHART_DISPLAY:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "flowChartDisplay"
    .end annotation
.end field

.field public static final enum FLOW_CHART_DOCUMENT:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "flowChartDocument"
    .end annotation
.end field

.field public static final enum FLOW_CHART_EXTRACT:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "flowChartExtract"
    .end annotation
.end field

.field public static final enum FLOW_CHART_INPUT_OUTPUT:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "flowChartInputOutput"
    .end annotation
.end field

.field public static final enum FLOW_CHART_INTERNAL_STORAGE:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "flowChartInternalStorage"
    .end annotation
.end field

.field public static final enum FLOW_CHART_MAGNETIC_DISK:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "flowChartMagneticDisk"
    .end annotation
.end field

.field public static final enum FLOW_CHART_MAGNETIC_DRUM:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "flowChartMagneticDrum"
    .end annotation
.end field

.field public static final enum FLOW_CHART_MAGNETIC_TAPE:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "flowChartMagneticTape"
    .end annotation
.end field

.field public static final enum FLOW_CHART_MANUAL_INPUT:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "flowChartManualInput"
    .end annotation
.end field

.field public static final enum FLOW_CHART_MANUAL_OPERATION:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "flowChartManualOperation"
    .end annotation
.end field

.field public static final enum FLOW_CHART_MERGE:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "flowChartMerge"
    .end annotation
.end field

.field public static final enum FLOW_CHART_MULTIDOCUMENT:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "flowChartMultidocument"
    .end annotation
.end field

.field public static final enum FLOW_CHART_OFFLINE_STORAGE:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "flowChartOfflineStorage"
    .end annotation
.end field

.field public static final enum FLOW_CHART_OFFPAGE_CONNECTOR:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "flowChartOffpageConnector"
    .end annotation
.end field

.field public static final enum FLOW_CHART_ONLINE_STORAGE:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "flowChartOnlineStorage"
    .end annotation
.end field

.field public static final enum FLOW_CHART_OR:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "flowChartOr"
    .end annotation
.end field

.field public static final enum FLOW_CHART_PREDEFINED_PROCESS:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "flowChartPredefinedProcess"
    .end annotation
.end field

.field public static final enum FLOW_CHART_PREPARATION:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "flowChartPreparation"
    .end annotation
.end field

.field public static final enum FLOW_CHART_PROCESS:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "flowChartProcess"
    .end annotation
.end field

.field public static final enum FLOW_CHART_PUNCHED_CARD:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "flowChartPunchedCard"
    .end annotation
.end field

.field public static final enum FLOW_CHART_PUNCHED_TAPE:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "flowChartPunchedTape"
    .end annotation
.end field

.field public static final enum FLOW_CHART_SORT:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "flowChartSort"
    .end annotation
.end field

.field public static final enum FLOW_CHART_SUMMING_JUNCTION:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "flowChartSummingJunction"
    .end annotation
.end field

.field public static final enum FLOW_CHART_TERMINATOR:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "flowChartTerminator"
    .end annotation
.end field

.field public static final enum FOLDED_CORNER:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "foldedCorner"
    .end annotation
.end field

.field public static final enum FRAME:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "frame"
    .end annotation
.end field

.field public static final enum FUNNEL:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "funnel"
    .end annotation
.end field

.field public static final enum GEAR_6:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "gear6"
    .end annotation
.end field

.field public static final enum GEAR_9:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "gear9"
    .end annotation
.end field

.field public static final enum HALF_FRAME:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "halfFrame"
    .end annotation
.end field

.field public static final enum HEART:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "heart"
    .end annotation
.end field

.field public static final enum HEPTAGON:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "heptagon"
    .end annotation
.end field

.field public static final enum HEXAGON:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "hexagon"
    .end annotation
.end field

.field public static final enum HOME_PLATE:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "homePlate"
    .end annotation
.end field

.field public static final enum HORIZONTAL_SCROLL:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "horizontalScroll"
    .end annotation
.end field

.field public static final enum IRREGULAR_SEAL_1:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "irregularSeal1"
    .end annotation
.end field

.field public static final enum IRREGULAR_SEAL_2:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "irregularSeal2"
    .end annotation
.end field

.field public static final enum LEFT_ARROW:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "leftArrow"
    .end annotation
.end field

.field public static final enum LEFT_ARROW_CALLOUT:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "leftArrowCallout"
    .end annotation
.end field

.field public static final enum LEFT_BRACE:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "leftBrace"
    .end annotation
.end field

.field public static final enum LEFT_BRACKET:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "leftBracket"
    .end annotation
.end field

.field public static final enum LEFT_CIRCULAR_ARROW:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "leftCircularArrow"
    .end annotation
.end field

.field public static final enum LEFT_RIGHT_ARROW:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "leftRightArrow"
    .end annotation
.end field

.field public static final enum LEFT_RIGHT_ARROW_CALLOUT:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "leftRightArrowCallout"
    .end annotation
.end field

.field public static final enum LEFT_RIGHT_CIRCULAR_ARROW:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "leftRightCircularArrow"
    .end annotation
.end field

.field public static final enum LEFT_RIGHT_RIBBON:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "leftRightRibbon"
    .end annotation
.end field

.field public static final enum LEFT_RIGHT_UP_ARROW:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "leftRightUpArrow"
    .end annotation
.end field

.field public static final enum LEFT_UP_ARROW:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "leftUpArrow"
    .end annotation
.end field

.field public static final enum LIGHTNING_BOLT:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "lightningBolt"
    .end annotation
.end field

.field public static final enum LINE:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "line"
    .end annotation
.end field

.field public static final enum LINE_INV:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "lineInv"
    .end annotation
.end field

.field public static final enum MATH_DIVIDE:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "mathDivide"
    .end annotation
.end field

.field public static final enum MATH_EQUAL:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "mathEqual"
    .end annotation
.end field

.field public static final enum MATH_MINUS:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "mathMinus"
    .end annotation
.end field

.field public static final enum MATH_MULTIPLY:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "mathMultiply"
    .end annotation
.end field

.field public static final enum MATH_NOT_EQUAL:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "mathNotEqual"
    .end annotation
.end field

.field public static final enum MATH_PLUS:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "mathPlus"
    .end annotation
.end field

.field public static final enum MOON:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "moon"
    .end annotation
.end field

.field public static final enum NON_ISOSCELES_TRAPEZOID:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "nonIsoscelesTrapezoid"
    .end annotation
.end field

.field public static final enum NOTCHED_RIGHT_ARROW:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "notchedRightArrow"
    .end annotation
.end field

.field public static final enum NO_SMOKING:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "noSmoking"
    .end annotation
.end field

.field public static final enum OCTAGON:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "octagon"
    .end annotation
.end field

.field public static final enum PARALLELOGRAM:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "parallelogram"
    .end annotation
.end field

.field public static final enum PENTAGON:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "pentagon"
    .end annotation
.end field

.field public static final enum PIE:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "pie"
    .end annotation
.end field

.field public static final enum PIE_WEDGE:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "pieWedge"
    .end annotation
.end field

.field public static final enum PLAQUE:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "plaque"
    .end annotation
.end field

.field public static final enum PLAQUE_TABS:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "plaqueTabs"
    .end annotation
.end field

.field public static final enum PLUS:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "plus"
    .end annotation
.end field

.field public static final enum QUAD_ARROW:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "quadArrow"
    .end annotation
.end field

.field public static final enum QUAD_ARROW_CALLOUT:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "quadArrowCallout"
    .end annotation
.end field

.field public static final enum RECT:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "rect"
    .end annotation
.end field

.field public static final enum RIBBON:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "ribbon"
    .end annotation
.end field

.field public static final enum RIBBON_2:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "ribbon2"
    .end annotation
.end field

.field public static final enum RIGHT_ARROW:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "rightArrow"
    .end annotation
.end field

.field public static final enum RIGHT_ARROW_CALLOUT:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "rightArrowCallout"
    .end annotation
.end field

.field public static final enum RIGHT_BRACE:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "rightBrace"
    .end annotation
.end field

.field public static final enum RIGHT_BRACKET:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "rightBracket"
    .end annotation
.end field

.field public static final enum ROUND_1_RECT:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "round1Rect"
    .end annotation
.end field

.field public static final enum ROUND_2_DIAG_RECT:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "round2DiagRect"
    .end annotation
.end field

.field public static final enum ROUND_2_SAME_RECT:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "round2SameRect"
    .end annotation
.end field

.field public static final enum ROUND_RECT:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "roundRect"
    .end annotation
.end field

.field public static final enum RT_TRIANGLE:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "rtTriangle"
    .end annotation
.end field

.field public static final enum SMILEY_FACE:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "smileyFace"
    .end annotation
.end field

.field public static final enum SNIP_1_RECT:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "snip1Rect"
    .end annotation
.end field

.field public static final enum SNIP_2_DIAG_RECT:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "snip2DiagRect"
    .end annotation
.end field

.field public static final enum SNIP_2_SAME_RECT:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "snip2SameRect"
    .end annotation
.end field

.field public static final enum SNIP_ROUND_RECT:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "snipRoundRect"
    .end annotation
.end field

.field public static final enum SQUARE_TABS:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "squareTabs"
    .end annotation
.end field

.field public static final enum STAR_10:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "star10"
    .end annotation
.end field

.field public static final enum STAR_12:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "star12"
    .end annotation
.end field

.field public static final enum STAR_16:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "star16"
    .end annotation
.end field

.field public static final enum STAR_24:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "star24"
    .end annotation
.end field

.field public static final enum STAR_32:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "star32"
    .end annotation
.end field

.field public static final enum STAR_4:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "star4"
    .end annotation
.end field

.field public static final enum STAR_5:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "star5"
    .end annotation
.end field

.field public static final enum STAR_6:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "star6"
    .end annotation
.end field

.field public static final enum STAR_7:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "star7"
    .end annotation
.end field

.field public static final enum STAR_8:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "star8"
    .end annotation
.end field

.field public static final enum STRAIGHT_CONNECTOR_1:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "straightConnector1"
    .end annotation
.end field

.field public static final enum STRIPED_RIGHT_ARROW:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "stripedRightArrow"
    .end annotation
.end field

.field public static final enum SUN:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "sun"
    .end annotation
.end field

.field public static final enum SWOOSH_ARROW:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "swooshArrow"
    .end annotation
.end field

.field public static final enum TEARDROP:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "teardrop"
    .end annotation
.end field

.field public static final enum TRAPEZOID:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "trapezoid"
    .end annotation
.end field

.field public static final enum TRIANGLE:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "triangle"
    .end annotation
.end field

.field public static final enum UP_ARROW:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "upArrow"
    .end annotation
.end field

.field public static final enum UP_ARROW_CALLOUT:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "upArrowCallout"
    .end annotation
.end field

.field public static final enum UP_DOWN_ARROW:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "upDownArrow"
    .end annotation
.end field

.field public static final enum UP_DOWN_ARROW_CALLOUT:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "upDownArrowCallout"
    .end annotation
.end field

.field public static final enum UTURN_ARROW:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "uturnArrow"
    .end annotation
.end field

.field public static final enum VERTICAL_SCROLL:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "verticalScroll"
    .end annotation
.end field

.field public static final enum WAVE:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "wave"
    .end annotation
.end field

.field public static final enum WEDGE_ELLIPSE_CALLOUT:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "wedgeEllipseCallout"
    .end annotation
.end field

.field public static final enum WEDGE_RECT_CALLOUT:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "wedgeRectCallout"
    .end annotation
.end field

.field public static final enum WEDGE_ROUND_RECT_CALLOUT:Lorg/apache/poi/sl/draw/binding/STShapeType;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "wedgeRoundRectCallout"
    .end annotation
.end field


# instance fields
.field private final value:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 191

    new-instance v1, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object v0, v1

    const/4 v2, 0x0

    const-string v3, "line"

    const-string v4, "LINE"

    invoke-direct {v1, v4, v2, v3}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lorg/apache/poi/sl/draw/binding/STShapeType;->LINE:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v2, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object v1, v2

    const/4 v3, 0x1

    const-string v4, "lineInv"

    const-string v5, "LINE_INV"

    invoke-direct {v2, v5, v3, v4}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v2, Lorg/apache/poi/sl/draw/binding/STShapeType;->LINE_INV:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v3, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object v2, v3

    const/4 v4, 0x2

    const-string v5, "triangle"

    const-string v6, "TRIANGLE"

    invoke-direct {v3, v6, v4, v5}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v3, Lorg/apache/poi/sl/draw/binding/STShapeType;->TRIANGLE:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v4, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object v3, v4

    const/4 v5, 0x3

    const-string v6, "rtTriangle"

    const-string v7, "RT_TRIANGLE"

    invoke-direct {v4, v7, v5, v6}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v4, Lorg/apache/poi/sl/draw/binding/STShapeType;->RT_TRIANGLE:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v5, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object v4, v5

    const/4 v6, 0x4

    const-string v7, "rect"

    const-string v8, "RECT"

    invoke-direct {v5, v8, v6, v7}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lorg/apache/poi/sl/draw/binding/STShapeType;->RECT:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v6, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object v5, v6

    const/4 v7, 0x5

    const-string v8, "diamond"

    const-string v9, "DIAMOND"

    invoke-direct {v6, v9, v7, v8}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v6, Lorg/apache/poi/sl/draw/binding/STShapeType;->DIAMOND:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v7, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object v6, v7

    const/4 v8, 0x6

    const-string v9, "parallelogram"

    const-string v10, "PARALLELOGRAM"

    invoke-direct {v7, v10, v8, v9}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v7, Lorg/apache/poi/sl/draw/binding/STShapeType;->PARALLELOGRAM:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v8, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object v7, v8

    const/4 v9, 0x7

    const-string v10, "trapezoid"

    const-string v11, "TRAPEZOID"

    invoke-direct {v8, v11, v9, v10}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v8, Lorg/apache/poi/sl/draw/binding/STShapeType;->TRAPEZOID:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v9, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object v8, v9

    const/16 v10, 0x8

    const-string v11, "nonIsoscelesTrapezoid"

    const-string v12, "NON_ISOSCELES_TRAPEZOID"

    invoke-direct {v9, v12, v10, v11}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v9, Lorg/apache/poi/sl/draw/binding/STShapeType;->NON_ISOSCELES_TRAPEZOID:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v10, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object v9, v10

    const/16 v11, 0x9

    const-string v12, "pentagon"

    const-string v13, "PENTAGON"

    invoke-direct {v10, v13, v11, v12}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v10, Lorg/apache/poi/sl/draw/binding/STShapeType;->PENTAGON:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v11, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object v10, v11

    const/16 v12, 0xa

    const-string v13, "hexagon"

    const-string v14, "HEXAGON"

    invoke-direct {v11, v14, v12, v13}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v11, Lorg/apache/poi/sl/draw/binding/STShapeType;->HEXAGON:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v12, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object v11, v12

    const/16 v13, 0xb

    const-string v14, "heptagon"

    const-string v15, "HEPTAGON"

    invoke-direct {v12, v15, v13, v14}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v12, Lorg/apache/poi/sl/draw/binding/STShapeType;->HEPTAGON:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v13, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object v12, v13

    const/16 v14, 0xc

    const-string v15, "octagon"

    move-object/from16 v187, v0

    const-string v0, "OCTAGON"

    invoke-direct {v13, v0, v14, v15}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v13, Lorg/apache/poi/sl/draw/binding/STShapeType;->OCTAGON:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object v13, v0

    const/16 v14, 0xd

    const-string v15, "decagon"

    move-object/from16 v188, v1

    const-string v1, "DECAGON"

    invoke-direct {v0, v1, v14, v15}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->DECAGON:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object v14, v0

    const/16 v1, 0xe

    const-string v15, "dodecagon"

    move-object/from16 v189, v2

    const-string v2, "DODECAGON"

    invoke-direct {v0, v2, v1, v15}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->DODECAGON:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object v15, v0

    const/16 v1, 0xf

    const-string v2, "star4"

    move-object/from16 v190, v3

    const-string v3, "STAR_4"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->STAR_4:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v16, v0

    const/16 v1, 0x10

    const-string v2, "star5"

    const-string v3, "STAR_5"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->STAR_5:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v17, v0

    const/16 v1, 0x11

    const-string v2, "star6"

    const-string v3, "STAR_6"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->STAR_6:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v18, v0

    const/16 v1, 0x12

    const-string v2, "star7"

    const-string v3, "STAR_7"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->STAR_7:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v19, v0

    const/16 v1, 0x13

    const-string v2, "star8"

    const-string v3, "STAR_8"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->STAR_8:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v20, v0

    const/16 v1, 0x14

    const-string v2, "star10"

    const-string v3, "STAR_10"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->STAR_10:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v21, v0

    const/16 v1, 0x15

    const-string v2, "star12"

    const-string v3, "STAR_12"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->STAR_12:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v22, v0

    const/16 v1, 0x16

    const-string v2, "star16"

    const-string v3, "STAR_16"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->STAR_16:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v23, v0

    const/16 v1, 0x17

    const-string v2, "star24"

    const-string v3, "STAR_24"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->STAR_24:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v24, v0

    const/16 v1, 0x18

    const-string v2, "star32"

    const-string v3, "STAR_32"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->STAR_32:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v25, v0

    const/16 v1, 0x19

    const-string v2, "roundRect"

    const-string v3, "ROUND_RECT"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->ROUND_RECT:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v26, v0

    const/16 v1, 0x1a

    const-string v2, "round1Rect"

    const-string v3, "ROUND_1_RECT"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->ROUND_1_RECT:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v27, v0

    const/16 v1, 0x1b

    const-string v2, "round2SameRect"

    const-string v3, "ROUND_2_SAME_RECT"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->ROUND_2_SAME_RECT:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v28, v0

    const/16 v1, 0x1c

    const-string v2, "round2DiagRect"

    const-string v3, "ROUND_2_DIAG_RECT"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->ROUND_2_DIAG_RECT:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v29, v0

    const/16 v1, 0x1d

    const-string v2, "snipRoundRect"

    const-string v3, "SNIP_ROUND_RECT"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->SNIP_ROUND_RECT:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v30, v0

    const/16 v1, 0x1e

    const-string v2, "snip1Rect"

    const-string v3, "SNIP_1_RECT"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->SNIP_1_RECT:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v31, v0

    const/16 v1, 0x1f

    const-string v2, "snip2SameRect"

    const-string v3, "SNIP_2_SAME_RECT"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->SNIP_2_SAME_RECT:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v32, v0

    const/16 v1, 0x20

    const-string v2, "snip2DiagRect"

    const-string v3, "SNIP_2_DIAG_RECT"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->SNIP_2_DIAG_RECT:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v33, v0

    const/16 v1, 0x21

    const-string v2, "plaque"

    const-string v3, "PLAQUE"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->PLAQUE:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v34, v0

    const/16 v1, 0x22

    const-string v2, "ellipse"

    const-string v3, "ELLIPSE"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->ELLIPSE:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v35, v0

    const/16 v1, 0x23

    const-string v2, "teardrop"

    const-string v3, "TEARDROP"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->TEARDROP:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v36, v0

    const/16 v1, 0x24

    const-string v2, "homePlate"

    const-string v3, "HOME_PLATE"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->HOME_PLATE:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v37, v0

    const/16 v1, 0x25

    const-string v2, "chevron"

    const-string v3, "CHEVRON"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->CHEVRON:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v38, v0

    const/16 v1, 0x26

    const-string v2, "pieWedge"

    const-string v3, "PIE_WEDGE"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->PIE_WEDGE:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v39, v0

    const/16 v1, 0x27

    const-string v2, "pie"

    const-string v3, "PIE"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->PIE:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v40, v0

    const/16 v1, 0x28

    const-string v2, "blockArc"

    const-string v3, "BLOCK_ARC"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->BLOCK_ARC:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v41, v0

    const/16 v1, 0x29

    const-string v2, "donut"

    const-string v3, "DONUT"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->DONUT:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v42, v0

    const/16 v1, 0x2a

    const-string v2, "noSmoking"

    const-string v3, "NO_SMOKING"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->NO_SMOKING:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v43, v0

    const/16 v1, 0x2b

    const-string v2, "rightArrow"

    const-string v3, "RIGHT_ARROW"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->RIGHT_ARROW:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v44, v0

    const/16 v1, 0x2c

    const-string v2, "leftArrow"

    const-string v3, "LEFT_ARROW"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->LEFT_ARROW:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v45, v0

    const/16 v1, 0x2d

    const-string v2, "upArrow"

    const-string v3, "UP_ARROW"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->UP_ARROW:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v46, v0

    const/16 v1, 0x2e

    const-string v2, "downArrow"

    const-string v3, "DOWN_ARROW"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->DOWN_ARROW:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v47, v0

    const/16 v1, 0x2f

    const-string v2, "stripedRightArrow"

    const-string v3, "STRIPED_RIGHT_ARROW"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->STRIPED_RIGHT_ARROW:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v48, v0

    const/16 v1, 0x30

    const-string v2, "notchedRightArrow"

    const-string v3, "NOTCHED_RIGHT_ARROW"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->NOTCHED_RIGHT_ARROW:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v49, v0

    const/16 v1, 0x31

    const-string v2, "bentUpArrow"

    const-string v3, "BENT_UP_ARROW"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->BENT_UP_ARROW:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v50, v0

    const/16 v1, 0x32

    const-string v2, "leftRightArrow"

    const-string v3, "LEFT_RIGHT_ARROW"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->LEFT_RIGHT_ARROW:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v51, v0

    const/16 v1, 0x33

    const-string v2, "upDownArrow"

    const-string v3, "UP_DOWN_ARROW"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->UP_DOWN_ARROW:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v52, v0

    const/16 v1, 0x34

    const-string v2, "leftUpArrow"

    const-string v3, "LEFT_UP_ARROW"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->LEFT_UP_ARROW:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v53, v0

    const/16 v1, 0x35

    const-string v2, "leftRightUpArrow"

    const-string v3, "LEFT_RIGHT_UP_ARROW"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->LEFT_RIGHT_UP_ARROW:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v54, v0

    const/16 v1, 0x36

    const-string v2, "quadArrow"

    const-string v3, "QUAD_ARROW"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->QUAD_ARROW:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v55, v0

    const/16 v1, 0x37

    const-string v2, "leftArrowCallout"

    const-string v3, "LEFT_ARROW_CALLOUT"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->LEFT_ARROW_CALLOUT:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v56, v0

    const/16 v1, 0x38

    const-string v2, "rightArrowCallout"

    const-string v3, "RIGHT_ARROW_CALLOUT"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->RIGHT_ARROW_CALLOUT:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v57, v0

    const/16 v1, 0x39

    const-string v2, "upArrowCallout"

    const-string v3, "UP_ARROW_CALLOUT"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->UP_ARROW_CALLOUT:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v58, v0

    const/16 v1, 0x3a

    const-string v2, "downArrowCallout"

    const-string v3, "DOWN_ARROW_CALLOUT"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->DOWN_ARROW_CALLOUT:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v59, v0

    const/16 v1, 0x3b

    const-string v2, "leftRightArrowCallout"

    const-string v3, "LEFT_RIGHT_ARROW_CALLOUT"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->LEFT_RIGHT_ARROW_CALLOUT:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v60, v0

    const/16 v1, 0x3c

    const-string v2, "upDownArrowCallout"

    const-string v3, "UP_DOWN_ARROW_CALLOUT"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->UP_DOWN_ARROW_CALLOUT:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v61, v0

    const/16 v1, 0x3d

    const-string v2, "quadArrowCallout"

    const-string v3, "QUAD_ARROW_CALLOUT"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->QUAD_ARROW_CALLOUT:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v62, v0

    const/16 v1, 0x3e

    const-string v2, "bentArrow"

    const-string v3, "BENT_ARROW"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->BENT_ARROW:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v63, v0

    const/16 v1, 0x3f

    const-string v2, "uturnArrow"

    const-string v3, "UTURN_ARROW"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->UTURN_ARROW:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v64, v0

    const/16 v1, 0x40

    const-string v2, "circularArrow"

    const-string v3, "CIRCULAR_ARROW"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->CIRCULAR_ARROW:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v65, v0

    const/16 v1, 0x41

    const-string v2, "leftCircularArrow"

    const-string v3, "LEFT_CIRCULAR_ARROW"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->LEFT_CIRCULAR_ARROW:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v66, v0

    const/16 v1, 0x42

    const-string v2, "leftRightCircularArrow"

    const-string v3, "LEFT_RIGHT_CIRCULAR_ARROW"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->LEFT_RIGHT_CIRCULAR_ARROW:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v67, v0

    const/16 v1, 0x43

    const-string v2, "curvedRightArrow"

    const-string v3, "CURVED_RIGHT_ARROW"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->CURVED_RIGHT_ARROW:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v68, v0

    const/16 v1, 0x44

    const-string v2, "curvedLeftArrow"

    const-string v3, "CURVED_LEFT_ARROW"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->CURVED_LEFT_ARROW:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v69, v0

    const/16 v1, 0x45

    const-string v2, "curvedUpArrow"

    const-string v3, "CURVED_UP_ARROW"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->CURVED_UP_ARROW:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v70, v0

    const/16 v1, 0x46

    const-string v2, "curvedDownArrow"

    const-string v3, "CURVED_DOWN_ARROW"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->CURVED_DOWN_ARROW:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v71, v0

    const/16 v1, 0x47

    const-string v2, "swooshArrow"

    const-string v3, "SWOOSH_ARROW"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->SWOOSH_ARROW:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v72, v0

    const/16 v1, 0x48

    const-string v2, "cube"

    const-string v3, "CUBE"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->CUBE:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v73, v0

    const/16 v1, 0x49

    const-string v2, "can"

    const-string v3, "CAN"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->CAN:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v74, v0

    const/16 v1, 0x4a

    const-string v2, "lightningBolt"

    const-string v3, "LIGHTNING_BOLT"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->LIGHTNING_BOLT:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v75, v0

    const/16 v1, 0x4b

    const-string v2, "heart"

    const-string v3, "HEART"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->HEART:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v76, v0

    const/16 v1, 0x4c

    const-string v2, "sun"

    const-string v3, "SUN"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->SUN:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v77, v0

    const/16 v1, 0x4d

    const-string v2, "moon"

    const-string v3, "MOON"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->MOON:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v78, v0

    const/16 v1, 0x4e

    const-string v2, "smileyFace"

    const-string v3, "SMILEY_FACE"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->SMILEY_FACE:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v79, v0

    const/16 v1, 0x4f

    const-string v2, "irregularSeal1"

    const-string v3, "IRREGULAR_SEAL_1"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->IRREGULAR_SEAL_1:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v80, v0

    const/16 v1, 0x50

    const-string v2, "irregularSeal2"

    const-string v3, "IRREGULAR_SEAL_2"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->IRREGULAR_SEAL_2:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v81, v0

    const/16 v1, 0x51

    const-string v2, "foldedCorner"

    const-string v3, "FOLDED_CORNER"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->FOLDED_CORNER:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v82, v0

    const/16 v1, 0x52

    const-string v2, "bevel"

    const-string v3, "BEVEL"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->BEVEL:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v83, v0

    const/16 v1, 0x53

    const-string v2, "frame"

    const-string v3, "FRAME"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->FRAME:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v84, v0

    const/16 v1, 0x54

    const-string v2, "halfFrame"

    const-string v3, "HALF_FRAME"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->HALF_FRAME:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v85, v0

    const/16 v1, 0x55

    const-string v2, "corner"

    const-string v3, "CORNER"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->CORNER:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v86, v0

    const/16 v1, 0x56

    const-string v2, "diagStripe"

    const-string v3, "DIAG_STRIPE"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->DIAG_STRIPE:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v87, v0

    const/16 v1, 0x57

    const-string v2, "chord"

    const-string v3, "CHORD"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->CHORD:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v88, v0

    const/16 v1, 0x58

    const-string v2, "arc"

    const-string v3, "ARC"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->ARC:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v89, v0

    const/16 v1, 0x59

    const-string v2, "leftBracket"

    const-string v3, "LEFT_BRACKET"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->LEFT_BRACKET:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v90, v0

    const/16 v1, 0x5a

    const-string v2, "rightBracket"

    const-string v3, "RIGHT_BRACKET"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->RIGHT_BRACKET:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v91, v0

    const/16 v1, 0x5b

    const-string v2, "leftBrace"

    const-string v3, "LEFT_BRACE"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->LEFT_BRACE:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v92, v0

    const/16 v1, 0x5c

    const-string v2, "rightBrace"

    const-string v3, "RIGHT_BRACE"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->RIGHT_BRACE:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v93, v0

    const/16 v1, 0x5d

    const-string v2, "bracketPair"

    const-string v3, "BRACKET_PAIR"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->BRACKET_PAIR:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v94, v0

    const/16 v1, 0x5e

    const-string v2, "bracePair"

    const-string v3, "BRACE_PAIR"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->BRACE_PAIR:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v95, v0

    const/16 v1, 0x5f

    const-string v2, "straightConnector1"

    const-string v3, "STRAIGHT_CONNECTOR_1"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->STRAIGHT_CONNECTOR_1:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v96, v0

    const/16 v1, 0x60

    const-string v2, "bentConnector2"

    const-string v3, "BENT_CONNECTOR_2"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->BENT_CONNECTOR_2:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v97, v0

    const/16 v1, 0x61

    const-string v2, "bentConnector3"

    const-string v3, "BENT_CONNECTOR_3"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->BENT_CONNECTOR_3:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v98, v0

    const/16 v1, 0x62

    const-string v2, "bentConnector4"

    const-string v3, "BENT_CONNECTOR_4"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->BENT_CONNECTOR_4:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v99, v0

    const/16 v1, 0x63

    const-string v2, "bentConnector5"

    const-string v3, "BENT_CONNECTOR_5"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->BENT_CONNECTOR_5:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v100, v0

    const/16 v1, 0x64

    const-string v2, "curvedConnector2"

    const-string v3, "CURVED_CONNECTOR_2"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->CURVED_CONNECTOR_2:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v101, v0

    const/16 v1, 0x65

    const-string v2, "curvedConnector3"

    const-string v3, "CURVED_CONNECTOR_3"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->CURVED_CONNECTOR_3:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v102, v0

    const/16 v1, 0x66

    const-string v2, "curvedConnector4"

    const-string v3, "CURVED_CONNECTOR_4"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->CURVED_CONNECTOR_4:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v103, v0

    const/16 v1, 0x67

    const-string v2, "curvedConnector5"

    const-string v3, "CURVED_CONNECTOR_5"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->CURVED_CONNECTOR_5:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v104, v0

    const/16 v1, 0x68

    const-string v2, "callout1"

    const-string v3, "CALLOUT_1"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->CALLOUT_1:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v105, v0

    const/16 v1, 0x69

    const-string v2, "callout2"

    const-string v3, "CALLOUT_2"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->CALLOUT_2:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v106, v0

    const/16 v1, 0x6a

    const-string v2, "callout3"

    const-string v3, "CALLOUT_3"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->CALLOUT_3:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v107, v0

    const/16 v1, 0x6b

    const-string v2, "accentCallout1"

    const-string v3, "ACCENT_CALLOUT_1"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->ACCENT_CALLOUT_1:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v108, v0

    const/16 v1, 0x6c

    const-string v2, "accentCallout2"

    const-string v3, "ACCENT_CALLOUT_2"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->ACCENT_CALLOUT_2:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v109, v0

    const/16 v1, 0x6d

    const-string v2, "accentCallout3"

    const-string v3, "ACCENT_CALLOUT_3"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->ACCENT_CALLOUT_3:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v110, v0

    const/16 v1, 0x6e

    const-string v2, "borderCallout1"

    const-string v3, "BORDER_CALLOUT_1"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->BORDER_CALLOUT_1:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v111, v0

    const/16 v1, 0x6f

    const-string v2, "borderCallout2"

    const-string v3, "BORDER_CALLOUT_2"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->BORDER_CALLOUT_2:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v112, v0

    const/16 v1, 0x70

    const-string v2, "borderCallout3"

    const-string v3, "BORDER_CALLOUT_3"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->BORDER_CALLOUT_3:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v113, v0

    const/16 v1, 0x71

    const-string v2, "accentBorderCallout1"

    const-string v3, "ACCENT_BORDER_CALLOUT_1"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->ACCENT_BORDER_CALLOUT_1:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v114, v0

    const/16 v1, 0x72

    const-string v2, "accentBorderCallout2"

    const-string v3, "ACCENT_BORDER_CALLOUT_2"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->ACCENT_BORDER_CALLOUT_2:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v115, v0

    const/16 v1, 0x73

    const-string v2, "accentBorderCallout3"

    const-string v3, "ACCENT_BORDER_CALLOUT_3"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->ACCENT_BORDER_CALLOUT_3:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v116, v0

    const/16 v1, 0x74

    const-string v2, "wedgeRectCallout"

    const-string v3, "WEDGE_RECT_CALLOUT"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->WEDGE_RECT_CALLOUT:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v117, v0

    const/16 v1, 0x75

    const-string v2, "wedgeRoundRectCallout"

    const-string v3, "WEDGE_ROUND_RECT_CALLOUT"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->WEDGE_ROUND_RECT_CALLOUT:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v118, v0

    const/16 v1, 0x76

    const-string v2, "wedgeEllipseCallout"

    const-string v3, "WEDGE_ELLIPSE_CALLOUT"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->WEDGE_ELLIPSE_CALLOUT:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v119, v0

    const/16 v1, 0x77

    const-string v2, "cloudCallout"

    const-string v3, "CLOUD_CALLOUT"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->CLOUD_CALLOUT:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v120, v0

    const/16 v1, 0x78

    const-string v2, "cloud"

    const-string v3, "CLOUD"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->CLOUD:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v121, v0

    const/16 v1, 0x79

    const-string v2, "ribbon"

    const-string v3, "RIBBON"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->RIBBON:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v122, v0

    const/16 v1, 0x7a

    const-string v2, "ribbon2"

    const-string v3, "RIBBON_2"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->RIBBON_2:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v123, v0

    const/16 v1, 0x7b

    const-string v2, "ellipseRibbon"

    const-string v3, "ELLIPSE_RIBBON"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->ELLIPSE_RIBBON:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v124, v0

    const/16 v1, 0x7c

    const-string v2, "ellipseRibbon2"

    const-string v3, "ELLIPSE_RIBBON_2"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->ELLIPSE_RIBBON_2:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v125, v0

    const/16 v1, 0x7d

    const-string v2, "leftRightRibbon"

    const-string v3, "LEFT_RIGHT_RIBBON"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->LEFT_RIGHT_RIBBON:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v126, v0

    const/16 v1, 0x7e

    const-string v2, "verticalScroll"

    const-string v3, "VERTICAL_SCROLL"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->VERTICAL_SCROLL:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v127, v0

    const/16 v1, 0x7f

    const-string v2, "horizontalScroll"

    const-string v3, "HORIZONTAL_SCROLL"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->HORIZONTAL_SCROLL:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v128, v0

    const/16 v1, 0x80

    const-string v2, "wave"

    const-string v3, "WAVE"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->WAVE:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v129, v0

    const/16 v1, 0x81

    const-string v2, "doubleWave"

    const-string v3, "DOUBLE_WAVE"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->DOUBLE_WAVE:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v130, v0

    const/16 v1, 0x82

    const-string v2, "plus"

    const-string v3, "PLUS"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->PLUS:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v131, v0

    const/16 v1, 0x83

    const-string v2, "flowChartProcess"

    const-string v3, "FLOW_CHART_PROCESS"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->FLOW_CHART_PROCESS:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v132, v0

    const/16 v1, 0x84

    const-string v2, "flowChartDecision"

    const-string v3, "FLOW_CHART_DECISION"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->FLOW_CHART_DECISION:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v133, v0

    const/16 v1, 0x85

    const-string v2, "flowChartInputOutput"

    const-string v3, "FLOW_CHART_INPUT_OUTPUT"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->FLOW_CHART_INPUT_OUTPUT:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v134, v0

    const/16 v1, 0x86

    const-string v2, "flowChartPredefinedProcess"

    const-string v3, "FLOW_CHART_PREDEFINED_PROCESS"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->FLOW_CHART_PREDEFINED_PROCESS:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v135, v0

    const/16 v1, 0x87

    const-string v2, "flowChartInternalStorage"

    const-string v3, "FLOW_CHART_INTERNAL_STORAGE"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->FLOW_CHART_INTERNAL_STORAGE:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v136, v0

    const/16 v1, 0x88

    const-string v2, "flowChartDocument"

    const-string v3, "FLOW_CHART_DOCUMENT"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->FLOW_CHART_DOCUMENT:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v137, v0

    const/16 v1, 0x89

    const-string v2, "flowChartMultidocument"

    const-string v3, "FLOW_CHART_MULTIDOCUMENT"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->FLOW_CHART_MULTIDOCUMENT:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v138, v0

    const/16 v1, 0x8a

    const-string v2, "flowChartTerminator"

    const-string v3, "FLOW_CHART_TERMINATOR"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->FLOW_CHART_TERMINATOR:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v139, v0

    const/16 v1, 0x8b

    const-string v2, "flowChartPreparation"

    const-string v3, "FLOW_CHART_PREPARATION"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->FLOW_CHART_PREPARATION:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v140, v0

    const/16 v1, 0x8c

    const-string v2, "flowChartManualInput"

    const-string v3, "FLOW_CHART_MANUAL_INPUT"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->FLOW_CHART_MANUAL_INPUT:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v141, v0

    const/16 v1, 0x8d

    const-string v2, "flowChartManualOperation"

    const-string v3, "FLOW_CHART_MANUAL_OPERATION"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->FLOW_CHART_MANUAL_OPERATION:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v142, v0

    const/16 v1, 0x8e

    const-string v2, "flowChartConnector"

    const-string v3, "FLOW_CHART_CONNECTOR"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->FLOW_CHART_CONNECTOR:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v143, v0

    const/16 v1, 0x8f

    const-string v2, "flowChartPunchedCard"

    const-string v3, "FLOW_CHART_PUNCHED_CARD"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->FLOW_CHART_PUNCHED_CARD:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v144, v0

    const/16 v1, 0x90

    const-string v2, "flowChartPunchedTape"

    const-string v3, "FLOW_CHART_PUNCHED_TAPE"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->FLOW_CHART_PUNCHED_TAPE:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v145, v0

    const/16 v1, 0x91

    const-string v2, "flowChartSummingJunction"

    const-string v3, "FLOW_CHART_SUMMING_JUNCTION"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->FLOW_CHART_SUMMING_JUNCTION:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v146, v0

    const/16 v1, 0x92

    const-string v2, "flowChartOr"

    const-string v3, "FLOW_CHART_OR"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->FLOW_CHART_OR:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v147, v0

    const/16 v1, 0x93

    const-string v2, "flowChartCollate"

    const-string v3, "FLOW_CHART_COLLATE"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->FLOW_CHART_COLLATE:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v148, v0

    const/16 v1, 0x94

    const-string v2, "flowChartSort"

    const-string v3, "FLOW_CHART_SORT"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->FLOW_CHART_SORT:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v149, v0

    const/16 v1, 0x95

    const-string v2, "flowChartExtract"

    const-string v3, "FLOW_CHART_EXTRACT"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->FLOW_CHART_EXTRACT:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v150, v0

    const/16 v1, 0x96

    const-string v2, "flowChartMerge"

    const-string v3, "FLOW_CHART_MERGE"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->FLOW_CHART_MERGE:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v151, v0

    const/16 v1, 0x97

    const-string v2, "flowChartOfflineStorage"

    const-string v3, "FLOW_CHART_OFFLINE_STORAGE"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->FLOW_CHART_OFFLINE_STORAGE:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v152, v0

    const/16 v1, 0x98

    const-string v2, "flowChartOnlineStorage"

    const-string v3, "FLOW_CHART_ONLINE_STORAGE"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->FLOW_CHART_ONLINE_STORAGE:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v153, v0

    const/16 v1, 0x99

    const-string v2, "flowChartMagneticTape"

    const-string v3, "FLOW_CHART_MAGNETIC_TAPE"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->FLOW_CHART_MAGNETIC_TAPE:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v154, v0

    const/16 v1, 0x9a

    const-string v2, "flowChartMagneticDisk"

    const-string v3, "FLOW_CHART_MAGNETIC_DISK"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->FLOW_CHART_MAGNETIC_DISK:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v155, v0

    const/16 v1, 0x9b

    const-string v2, "flowChartMagneticDrum"

    const-string v3, "FLOW_CHART_MAGNETIC_DRUM"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->FLOW_CHART_MAGNETIC_DRUM:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v156, v0

    const/16 v1, 0x9c

    const-string v2, "flowChartDisplay"

    const-string v3, "FLOW_CHART_DISPLAY"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->FLOW_CHART_DISPLAY:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v157, v0

    const/16 v1, 0x9d

    const-string v2, "flowChartDelay"

    const-string v3, "FLOW_CHART_DELAY"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->FLOW_CHART_DELAY:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v158, v0

    const/16 v1, 0x9e

    const-string v2, "flowChartAlternateProcess"

    const-string v3, "FLOW_CHART_ALTERNATE_PROCESS"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->FLOW_CHART_ALTERNATE_PROCESS:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v159, v0

    const/16 v1, 0x9f

    const-string v2, "flowChartOffpageConnector"

    const-string v3, "FLOW_CHART_OFFPAGE_CONNECTOR"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->FLOW_CHART_OFFPAGE_CONNECTOR:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v160, v0

    const/16 v1, 0xa0

    const-string v2, "actionButtonBlank"

    const-string v3, "ACTION_BUTTON_BLANK"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->ACTION_BUTTON_BLANK:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v161, v0

    const/16 v1, 0xa1

    const-string v2, "actionButtonHome"

    const-string v3, "ACTION_BUTTON_HOME"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->ACTION_BUTTON_HOME:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v162, v0

    const/16 v1, 0xa2

    const-string v2, "actionButtonHelp"

    const-string v3, "ACTION_BUTTON_HELP"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->ACTION_BUTTON_HELP:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v163, v0

    const/16 v1, 0xa3

    const-string v2, "actionButtonInformation"

    const-string v3, "ACTION_BUTTON_INFORMATION"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->ACTION_BUTTON_INFORMATION:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v164, v0

    const/16 v1, 0xa4

    const-string v2, "actionButtonForwardNext"

    const-string v3, "ACTION_BUTTON_FORWARD_NEXT"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->ACTION_BUTTON_FORWARD_NEXT:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v165, v0

    const/16 v1, 0xa5

    const-string v2, "actionButtonBackPrevious"

    const-string v3, "ACTION_BUTTON_BACK_PREVIOUS"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->ACTION_BUTTON_BACK_PREVIOUS:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v166, v0

    const/16 v1, 0xa6

    const-string v2, "actionButtonEnd"

    const-string v3, "ACTION_BUTTON_END"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->ACTION_BUTTON_END:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v167, v0

    const/16 v1, 0xa7

    const-string v2, "actionButtonBeginning"

    const-string v3, "ACTION_BUTTON_BEGINNING"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->ACTION_BUTTON_BEGINNING:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v168, v0

    const/16 v1, 0xa8

    const-string v2, "actionButtonReturn"

    const-string v3, "ACTION_BUTTON_RETURN"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->ACTION_BUTTON_RETURN:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v169, v0

    const/16 v1, 0xa9

    const-string v2, "actionButtonDocument"

    const-string v3, "ACTION_BUTTON_DOCUMENT"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->ACTION_BUTTON_DOCUMENT:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v170, v0

    const/16 v1, 0xaa

    const-string v2, "actionButtonSound"

    const-string v3, "ACTION_BUTTON_SOUND"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->ACTION_BUTTON_SOUND:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v171, v0

    const/16 v1, 0xab

    const-string v2, "actionButtonMovie"

    const-string v3, "ACTION_BUTTON_MOVIE"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->ACTION_BUTTON_MOVIE:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v172, v0

    const/16 v1, 0xac

    const-string v2, "gear6"

    const-string v3, "GEAR_6"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->GEAR_6:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v173, v0

    const/16 v1, 0xad

    const-string v2, "gear9"

    const-string v3, "GEAR_9"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->GEAR_9:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v174, v0

    const/16 v1, 0xae

    const-string v2, "funnel"

    const-string v3, "FUNNEL"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->FUNNEL:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v175, v0

    const/16 v1, 0xaf

    const-string v2, "mathPlus"

    const-string v3, "MATH_PLUS"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->MATH_PLUS:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v176, v0

    const/16 v1, 0xb0

    const-string v2, "mathMinus"

    const-string v3, "MATH_MINUS"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->MATH_MINUS:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v177, v0

    const/16 v1, 0xb1

    const-string v2, "mathMultiply"

    const-string v3, "MATH_MULTIPLY"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->MATH_MULTIPLY:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v178, v0

    const/16 v1, 0xb2

    const-string v2, "mathDivide"

    const-string v3, "MATH_DIVIDE"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->MATH_DIVIDE:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v179, v0

    const/16 v1, 0xb3

    const-string v2, "mathEqual"

    const-string v3, "MATH_EQUAL"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->MATH_EQUAL:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v180, v0

    const/16 v1, 0xb4

    const-string v2, "mathNotEqual"

    const-string v3, "MATH_NOT_EQUAL"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->MATH_NOT_EQUAL:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v181, v0

    const/16 v1, 0xb5

    const-string v2, "cornerTabs"

    const-string v3, "CORNER_TABS"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->CORNER_TABS:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v182, v0

    const/16 v1, 0xb6

    const-string v2, "squareTabs"

    const-string v3, "SQUARE_TABS"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->SQUARE_TABS:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v183, v0

    const/16 v1, 0xb7

    const-string v2, "plaqueTabs"

    const-string v3, "PLAQUE_TABS"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->PLAQUE_TABS:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v184, v0

    const/16 v1, 0xb8

    const-string v2, "chartX"

    const-string v3, "CHART_X"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->CHART_X:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v185, v0

    const/16 v1, 0xb9

    const-string v2, "chartStar"

    const-string v3, "CHART_STAR"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->CHART_STAR:Lorg/apache/poi/sl/draw/binding/STShapeType;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v186, v0

    const/16 v1, 0xba

    const-string v2, "chartPlus"

    const-string v3, "CHART_PLUS"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STShapeType;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->CHART_PLUS:Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-object/from16 v0, v187

    move-object/from16 v1, v188

    move-object/from16 v2, v189

    move-object/from16 v3, v190

    filled-new-array/range {v0 .. v186}, [Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->$VALUES:[Lorg/apache/poi/sl/draw/binding/STShapeType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lorg/apache/poi/sl/draw/binding/STShapeType;->value:Ljava/lang/String;

    return-void
.end method

.method public static fromValue(Ljava/lang/String;)Lorg/apache/poi/sl/draw/binding/STShapeType;
    .locals 5

    invoke-static {}, Lorg/apache/poi/sl/draw/binding/STShapeType;->values()[Lorg/apache/poi/sl/draw/binding/STShapeType;

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_1

    aget-object v3, v0, v2

    iget-object v4, v3, Lorg/apache/poi/sl/draw/binding/STShapeType;->value:Ljava/lang/String;

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    return-object v3

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static valueOf(Ljava/lang/String;)Lorg/apache/poi/sl/draw/binding/STShapeType;
    .locals 1

    const-class v0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lorg/apache/poi/sl/draw/binding/STShapeType;

    return-object p0
.end method

.method public static values()[Lorg/apache/poi/sl/draw/binding/STShapeType;
    .locals 1

    sget-object v0, Lorg/apache/poi/sl/draw/binding/STShapeType;->$VALUES:[Lorg/apache/poi/sl/draw/binding/STShapeType;

    invoke-virtual {v0}, [Lorg/apache/poi/sl/draw/binding/STShapeType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lorg/apache/poi/sl/draw/binding/STShapeType;

    return-object v0
.end method


# virtual methods
.method public value()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/apache/poi/sl/draw/binding/STShapeType;->value:Ljava/lang/String;

    return-object v0
.end method

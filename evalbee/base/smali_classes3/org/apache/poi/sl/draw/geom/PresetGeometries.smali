.class public Lorg/apache/poi/sl/draw/geom/PresetGeometries;
.super Ljava/util/LinkedHashMap;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/LinkedHashMap<",
        "Ljava/lang/String;",
        "Lorg/apache/poi/sl/draw/geom/CustomGeometry;",
        ">;"
    }
.end annotation


# static fields
.field protected static final BINDING_PACKAGE:Ljava/lang/String; = "org.apache.poi.sl.draw.binding"

.field private static final LOG:Lorg/apache/poi/util/POILogger;

.field protected static _inst:Lorg/apache/poi/sl/draw/geom/PresetGeometries;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    const-class v0, Lorg/apache/poi/sl/draw/geom/PresetGeometries;

    invoke-static {v0}, Lorg/apache/poi/util/POILogFactory;->getLogger(Ljava/lang/Class;)Lorg/apache/poi/util/POILogger;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/sl/draw/geom/PresetGeometries;->LOG:Lorg/apache/poi/util/POILogger;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/util/LinkedHashMap;-><init>()V

    return-void
.end method

.method public static convertCustomGeometry(Ljavax/xml/stream/XMLStreamReader;)Lorg/apache/poi/sl/draw/geom/CustomGeometry;
    .locals 2

    :try_start_0
    const-string v0, "org.apache.poi.sl.draw.binding"

    invoke-static {v0}, Ljavax/xml/bind/JAXBContext;->newInstance(Ljava/lang/String;)Ljavax/xml/bind/JAXBContext;

    move-result-object v0

    invoke-virtual {v0}, Ljavax/xml/bind/JAXBContext;->createUnmarshaller()Ljavax/xml/bind/Unmarshaller;

    move-result-object v0

    const-class v1, Lorg/apache/poi/sl/draw/binding/CTCustomGeometry2D;

    invoke-interface {v0, p0, v1}, Ljavax/xml/bind/Unmarshaller;->unmarshal(Ljavax/xml/stream/XMLStreamReader;Ljava/lang/Class;)Ljavax/xml/bind/JAXBElement;

    move-result-object p0

    new-instance v0, Lorg/apache/poi/sl/draw/geom/CustomGeometry;

    invoke-virtual {p0}, Ljavax/xml/bind/JAXBElement;->getValue()Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lorg/apache/poi/sl/draw/binding/CTCustomGeometry2D;

    invoke-direct {v0, p0}, Lorg/apache/poi/sl/draw/geom/CustomGeometry;-><init>(Lorg/apache/poi/sl/draw/binding/CTCustomGeometry2D;)V
    :try_end_0
    .catch Ljavax/xml/bind/JAXBException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception p0

    sget-object v0, Lorg/apache/poi/sl/draw/geom/PresetGeometries;->LOG:Lorg/apache/poi/util/POILogger;

    const-string v1, "Unable to parse single custom geometry"

    filled-new-array {v1, p0}, [Ljava/lang/Object;

    move-result-object p0

    const/4 v1, 0x7

    invoke-virtual {v0, v1, p0}, Lorg/apache/poi/util/POILogger;->log(I[Ljava/lang/Object;)V

    const/4 p0, 0x0

    return-object p0
.end method

.method public static declared-synchronized getInstance()Lorg/apache/poi/sl/draw/geom/PresetGeometries;
    .locals 4

    const-class v0, Lorg/apache/poi/sl/draw/geom/PresetGeometries;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lorg/apache/poi/sl/draw/geom/PresetGeometries;->_inst:Lorg/apache/poi/sl/draw/geom/PresetGeometries;

    if-nez v1, :cond_0

    new-instance v1, Lorg/apache/poi/sl/draw/geom/PresetGeometries;

    invoke-direct {v1}, Lorg/apache/poi/sl/draw/geom/PresetGeometries;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    const-class v2, Lorg/apache/poi/sl/draw/geom/PresetGeometries;

    const-string v3, "presetShapeDefinitions.xml"

    invoke-virtual {v2, v3}, Ljava/lang/Class;->getResourceAsStream(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v2
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    invoke-virtual {v1, v2}, Lorg/apache/poi/sl/draw/geom/PresetGeometries;->init(Ljava/io/InputStream;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :try_start_4
    sput-object v1, Lorg/apache/poi/sl/draw/geom/PresetGeometries;->_inst:Lorg/apache/poi/sl/draw/geom/PresetGeometries;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_0

    :catchall_0
    move-exception v1

    :try_start_5
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    throw v1
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :catch_0
    move-exception v1

    :try_start_6
    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v2

    :cond_0
    :goto_0
    sget-object v1, Lorg/apache/poi/sl/draw/geom/PresetGeometries;->_inst:Lorg/apache/poi/sl/draw/geom/PresetGeometries;
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    monitor-exit v0

    return-object v1

    :catchall_1
    move-exception v1

    monitor-exit v0

    throw v1
.end method


# virtual methods
.method public init(Ljava/io/InputStream;)V
    .locals 7

    new-instance v0, Lorg/apache/poi/sl/draw/geom/PresetGeometries$1;

    invoke-direct {v0, p0}, Lorg/apache/poi/sl/draw/geom/PresetGeometries$1;-><init>(Lorg/apache/poi/sl/draw/geom/PresetGeometries;)V

    invoke-static {}, Lorg/apache/poi/util/StaxHelper;->newXMLInputFactory()Ljavax/xml/stream/XMLInputFactory;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljavax/xml/stream/XMLInputFactory;->createXMLEventReader(Ljava/io/InputStream;)Ljavax/xml/stream/XMLEventReader;

    move-result-object p1

    invoke-virtual {v1, p1, v0}, Ljavax/xml/stream/XMLInputFactory;->createFilteredReader(Ljavax/xml/stream/XMLEventReader;Ljavax/xml/stream/EventFilter;)Ljavax/xml/stream/XMLEventReader;

    move-result-object v0

    invoke-interface {v0}, Ljavax/xml/stream/XMLEventReader;->nextEvent()Ljavax/xml/stream/events/XMLEvent;

    const-string v1, "org.apache.poi.sl.draw.binding"

    invoke-static {v1}, Ljavax/xml/bind/JAXBContext;->newInstance(Ljava/lang/String;)Ljavax/xml/bind/JAXBContext;

    move-result-object v1

    invoke-virtual {v1}, Ljavax/xml/bind/JAXBContext;->createUnmarshaller()Ljavax/xml/bind/Unmarshaller;

    move-result-object v1

    :goto_0
    invoke-interface {v0}, Ljavax/xml/stream/XMLEventReader;->peek()Ljavax/xml/stream/events/XMLEvent;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljavax/xml/stream/XMLEventReader;->peek()Ljavax/xml/stream/events/XMLEvent;

    move-result-object v2

    check-cast v2, Ljavax/xml/stream/events/StartElement;

    invoke-interface {v2}, Ljavax/xml/stream/events/StartElement;->getName()Ljavax/xml/namespace/QName;

    move-result-object v2

    invoke-virtual {v2}, Ljavax/xml/namespace/QName;->getLocalPart()Ljava/lang/String;

    move-result-object v2

    const-class v3, Lorg/apache/poi/sl/draw/binding/CTCustomGeometry2D;

    invoke-interface {v1, p1, v3}, Ljavax/xml/bind/Unmarshaller;->unmarshal(Ljavax/xml/stream/XMLEventReader;Ljava/lang/Class;)Ljavax/xml/bind/JAXBElement;

    move-result-object v3

    invoke-virtual {v3}, Ljavax/xml/bind/JAXBElement;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/poi/sl/draw/binding/CTCustomGeometry2D;

    invoke-virtual {p0, v2}, Ljava/util/AbstractMap;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    sget-object v4, Lorg/apache/poi/sl/draw/geom/PresetGeometries;->LOG:Lorg/apache/poi/util/POILogger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Duplicate definition of "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    filled-new-array {v5}, [Ljava/lang/Object;

    move-result-object v5

    const/4 v6, 0x5

    invoke-virtual {v4, v6, v5}, Lorg/apache/poi/util/POILogger;->log(I[Ljava/lang/Object;)V

    :cond_0
    new-instance v4, Lorg/apache/poi/sl/draw/geom/CustomGeometry;

    invoke-direct {v4, v3}, Lorg/apache/poi/sl/draw/geom/CustomGeometry;-><init>(Lorg/apache/poi/sl/draw/binding/CTCustomGeometry2D;)V

    invoke-virtual {p0, v2, v4}, Ljava/util/AbstractMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_1
    return-void
.end method

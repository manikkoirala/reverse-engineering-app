.class public final enum Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lorg/apache/poi/sl/draw/binding/STPresetColorVal;",
        ">;"
    }
.end annotation

.annotation runtime Ljavax/xml/bind/annotation/XmlEnum;
.end annotation

.annotation runtime Ljavax/xml/bind/annotation/XmlType;
    name = "ST_PresetColorVal"
    namespace = "http://schemas.openxmlformats.org/drawingml/2006/main"
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

.field public static final enum ALICE_BLUE:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "aliceBlue"
    .end annotation
.end field

.field public static final enum ANTIQUE_WHITE:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "antiqueWhite"
    .end annotation
.end field

.field public static final enum AQUA:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "aqua"
    .end annotation
.end field

.field public static final enum AQUAMARINE:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "aquamarine"
    .end annotation
.end field

.field public static final enum AZURE:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "azure"
    .end annotation
.end field

.field public static final enum BEIGE:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "beige"
    .end annotation
.end field

.field public static final enum BISQUE:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "bisque"
    .end annotation
.end field

.field public static final enum BLACK:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "black"
    .end annotation
.end field

.field public static final enum BLANCHED_ALMOND:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "blanchedAlmond"
    .end annotation
.end field

.field public static final enum BLUE:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "blue"
    .end annotation
.end field

.field public static final enum BLUE_VIOLET:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "blueViolet"
    .end annotation
.end field

.field public static final enum BROWN:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "brown"
    .end annotation
.end field

.field public static final enum BURLY_WOOD:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "burlyWood"
    .end annotation
.end field

.field public static final enum CADET_BLUE:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "cadetBlue"
    .end annotation
.end field

.field public static final enum CHARTREUSE:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "chartreuse"
    .end annotation
.end field

.field public static final enum CHOCOLATE:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "chocolate"
    .end annotation
.end field

.field public static final enum CORAL:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "coral"
    .end annotation
.end field

.field public static final enum CORNFLOWER_BLUE:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "cornflowerBlue"
    .end annotation
.end field

.field public static final enum CORNSILK:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "cornsilk"
    .end annotation
.end field

.field public static final enum CRIMSON:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "crimson"
    .end annotation
.end field

.field public static final enum CYAN:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "cyan"
    .end annotation
.end field

.field public static final enum DEEP_PINK:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "deepPink"
    .end annotation
.end field

.field public static final enum DEEP_SKY_BLUE:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "deepSkyBlue"
    .end annotation
.end field

.field public static final enum DIM_GRAY:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "dimGray"
    .end annotation
.end field

.field public static final enum DK_BLUE:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "dkBlue"
    .end annotation
.end field

.field public static final enum DK_CYAN:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "dkCyan"
    .end annotation
.end field

.field public static final enum DK_GOLDENROD:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "dkGoldenrod"
    .end annotation
.end field

.field public static final enum DK_GRAY:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "dkGray"
    .end annotation
.end field

.field public static final enum DK_GREEN:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "dkGreen"
    .end annotation
.end field

.field public static final enum DK_KHAKI:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "dkKhaki"
    .end annotation
.end field

.field public static final enum DK_MAGENTA:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "dkMagenta"
    .end annotation
.end field

.field public static final enum DK_OLIVE_GREEN:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "dkOliveGreen"
    .end annotation
.end field

.field public static final enum DK_ORANGE:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "dkOrange"
    .end annotation
.end field

.field public static final enum DK_ORCHID:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "dkOrchid"
    .end annotation
.end field

.field public static final enum DK_RED:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "dkRed"
    .end annotation
.end field

.field public static final enum DK_SALMON:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "dkSalmon"
    .end annotation
.end field

.field public static final enum DK_SEA_GREEN:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "dkSeaGreen"
    .end annotation
.end field

.field public static final enum DK_SLATE_BLUE:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "dkSlateBlue"
    .end annotation
.end field

.field public static final enum DK_SLATE_GRAY:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "dkSlateGray"
    .end annotation
.end field

.field public static final enum DK_TURQUOISE:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "dkTurquoise"
    .end annotation
.end field

.field public static final enum DK_VIOLET:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "dkViolet"
    .end annotation
.end field

.field public static final enum DODGER_BLUE:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "dodgerBlue"
    .end annotation
.end field

.field public static final enum FIREBRICK:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "firebrick"
    .end annotation
.end field

.field public static final enum FLORAL_WHITE:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "floralWhite"
    .end annotation
.end field

.field public static final enum FOREST_GREEN:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "forestGreen"
    .end annotation
.end field

.field public static final enum FUCHSIA:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "fuchsia"
    .end annotation
.end field

.field public static final enum GAINSBORO:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "gainsboro"
    .end annotation
.end field

.field public static final enum GHOST_WHITE:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "ghostWhite"
    .end annotation
.end field

.field public static final enum GOLD:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "gold"
    .end annotation
.end field

.field public static final enum GOLDENROD:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "goldenrod"
    .end annotation
.end field

.field public static final enum GRAY:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "gray"
    .end annotation
.end field

.field public static final enum GREEN:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "green"
    .end annotation
.end field

.field public static final enum GREEN_YELLOW:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "greenYellow"
    .end annotation
.end field

.field public static final enum HONEYDEW:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "honeydew"
    .end annotation
.end field

.field public static final enum HOT_PINK:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "hotPink"
    .end annotation
.end field

.field public static final enum INDIAN_RED:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "indianRed"
    .end annotation
.end field

.field public static final enum INDIGO:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "indigo"
    .end annotation
.end field

.field public static final enum IVORY:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "ivory"
    .end annotation
.end field

.field public static final enum KHAKI:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "khaki"
    .end annotation
.end field

.field public static final enum LAVENDER:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "lavender"
    .end annotation
.end field

.field public static final enum LAVENDER_BLUSH:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "lavenderBlush"
    .end annotation
.end field

.field public static final enum LAWN_GREEN:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "lawnGreen"
    .end annotation
.end field

.field public static final enum LEMON_CHIFFON:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "lemonChiffon"
    .end annotation
.end field

.field public static final enum LIME:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "lime"
    .end annotation
.end field

.field public static final enum LIME_GREEN:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "limeGreen"
    .end annotation
.end field

.field public static final enum LINEN:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "linen"
    .end annotation
.end field

.field public static final enum LT_BLUE:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "ltBlue"
    .end annotation
.end field

.field public static final enum LT_CORAL:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "ltCoral"
    .end annotation
.end field

.field public static final enum LT_CYAN:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "ltCyan"
    .end annotation
.end field

.field public static final enum LT_GOLDENROD_YELLOW:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "ltGoldenrodYellow"
    .end annotation
.end field

.field public static final enum LT_GRAY:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "ltGray"
    .end annotation
.end field

.field public static final enum LT_GREEN:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "ltGreen"
    .end annotation
.end field

.field public static final enum LT_PINK:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "ltPink"
    .end annotation
.end field

.field public static final enum LT_SALMON:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "ltSalmon"
    .end annotation
.end field

.field public static final enum LT_SEA_GREEN:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "ltSeaGreen"
    .end annotation
.end field

.field public static final enum LT_SKY_BLUE:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "ltSkyBlue"
    .end annotation
.end field

.field public static final enum LT_SLATE_GRAY:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "ltSlateGray"
    .end annotation
.end field

.field public static final enum LT_STEEL_BLUE:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "ltSteelBlue"
    .end annotation
.end field

.field public static final enum LT_YELLOW:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "ltYellow"
    .end annotation
.end field

.field public static final enum MAGENTA:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "magenta"
    .end annotation
.end field

.field public static final enum MAROON:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "maroon"
    .end annotation
.end field

.field public static final enum MED_AQUAMARINE:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "medAquamarine"
    .end annotation
.end field

.field public static final enum MED_BLUE:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "medBlue"
    .end annotation
.end field

.field public static final enum MED_ORCHID:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "medOrchid"
    .end annotation
.end field

.field public static final enum MED_PURPLE:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "medPurple"
    .end annotation
.end field

.field public static final enum MED_SEA_GREEN:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "medSeaGreen"
    .end annotation
.end field

.field public static final enum MED_SLATE_BLUE:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "medSlateBlue"
    .end annotation
.end field

.field public static final enum MED_SPRING_GREEN:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "medSpringGreen"
    .end annotation
.end field

.field public static final enum MED_TURQUOISE:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "medTurquoise"
    .end annotation
.end field

.field public static final enum MED_VIOLET_RED:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "medVioletRed"
    .end annotation
.end field

.field public static final enum MIDNIGHT_BLUE:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "midnightBlue"
    .end annotation
.end field

.field public static final enum MINT_CREAM:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "mintCream"
    .end annotation
.end field

.field public static final enum MISTY_ROSE:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "mistyRose"
    .end annotation
.end field

.field public static final enum MOCCASIN:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "moccasin"
    .end annotation
.end field

.field public static final enum NAVAJO_WHITE:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "navajoWhite"
    .end annotation
.end field

.field public static final enum NAVY:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "navy"
    .end annotation
.end field

.field public static final enum OLD_LACE:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "oldLace"
    .end annotation
.end field

.field public static final enum OLIVE:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "olive"
    .end annotation
.end field

.field public static final enum OLIVE_DRAB:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "oliveDrab"
    .end annotation
.end field

.field public static final enum ORANGE:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "orange"
    .end annotation
.end field

.field public static final enum ORANGE_RED:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "orangeRed"
    .end annotation
.end field

.field public static final enum ORCHID:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "orchid"
    .end annotation
.end field

.field public static final enum PALE_GOLDENROD:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "paleGoldenrod"
    .end annotation
.end field

.field public static final enum PALE_GREEN:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "paleGreen"
    .end annotation
.end field

.field public static final enum PALE_TURQUOISE:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "paleTurquoise"
    .end annotation
.end field

.field public static final enum PALE_VIOLET_RED:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "paleVioletRed"
    .end annotation
.end field

.field public static final enum PAPAYA_WHIP:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "papayaWhip"
    .end annotation
.end field

.field public static final enum PEACH_PUFF:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "peachPuff"
    .end annotation
.end field

.field public static final enum PERU:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "peru"
    .end annotation
.end field

.field public static final enum PINK:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "pink"
    .end annotation
.end field

.field public static final enum PLUM:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "plum"
    .end annotation
.end field

.field public static final enum POWDER_BLUE:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "powderBlue"
    .end annotation
.end field

.field public static final enum PURPLE:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "purple"
    .end annotation
.end field

.field public static final enum RED:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "red"
    .end annotation
.end field

.field public static final enum ROSY_BROWN:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "rosyBrown"
    .end annotation
.end field

.field public static final enum ROYAL_BLUE:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "royalBlue"
    .end annotation
.end field

.field public static final enum SADDLE_BROWN:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "saddleBrown"
    .end annotation
.end field

.field public static final enum SALMON:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "salmon"
    .end annotation
.end field

.field public static final enum SANDY_BROWN:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "sandyBrown"
    .end annotation
.end field

.field public static final enum SEA_GREEN:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "seaGreen"
    .end annotation
.end field

.field public static final enum SEA_SHELL:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "seaShell"
    .end annotation
.end field

.field public static final enum SIENNA:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "sienna"
    .end annotation
.end field

.field public static final enum SILVER:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "silver"
    .end annotation
.end field

.field public static final enum SKY_BLUE:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "skyBlue"
    .end annotation
.end field

.field public static final enum SLATE_BLUE:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "slateBlue"
    .end annotation
.end field

.field public static final enum SLATE_GRAY:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "slateGray"
    .end annotation
.end field

.field public static final enum SNOW:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "snow"
    .end annotation
.end field

.field public static final enum SPRING_GREEN:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "springGreen"
    .end annotation
.end field

.field public static final enum STEEL_BLUE:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "steelBlue"
    .end annotation
.end field

.field public static final enum TAN:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "tan"
    .end annotation
.end field

.field public static final enum TEAL:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "teal"
    .end annotation
.end field

.field public static final enum THISTLE:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "thistle"
    .end annotation
.end field

.field public static final enum TOMATO:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "tomato"
    .end annotation
.end field

.field public static final enum TURQUOISE:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "turquoise"
    .end annotation
.end field

.field public static final enum VIOLET:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "violet"
    .end annotation
.end field

.field public static final enum WHEAT:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "wheat"
    .end annotation
.end field

.field public static final enum WHITE:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "white"
    .end annotation
.end field

.field public static final enum WHITE_SMOKE:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "whiteSmoke"
    .end annotation
.end field

.field public static final enum YELLOW:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "yellow"
    .end annotation
.end field

.field public static final enum YELLOW_GREEN:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .annotation runtime Ljavax/xml/bind/annotation/XmlEnumValue;
        value = "yellowGreen"
    .end annotation
.end field


# instance fields
.field private final value:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 144

    new-instance v1, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object v0, v1

    const/4 v2, 0x0

    const-string v3, "aliceBlue"

    const-string v4, "ALICE_BLUE"

    invoke-direct {v1, v4, v2, v3}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->ALICE_BLUE:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v2, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object v1, v2

    const/4 v3, 0x1

    const-string v4, "antiqueWhite"

    const-string v5, "ANTIQUE_WHITE"

    invoke-direct {v2, v5, v3, v4}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v2, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->ANTIQUE_WHITE:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v3, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object v2, v3

    const/4 v4, 0x2

    const-string v5, "aqua"

    const-string v6, "AQUA"

    invoke-direct {v3, v6, v4, v5}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v3, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->AQUA:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v4, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object v3, v4

    const/4 v5, 0x3

    const-string v6, "aquamarine"

    const-string v7, "AQUAMARINE"

    invoke-direct {v4, v7, v5, v6}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v4, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->AQUAMARINE:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v5, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object v4, v5

    const/4 v6, 0x4

    const-string v7, "azure"

    const-string v8, "AZURE"

    invoke-direct {v5, v8, v6, v7}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->AZURE:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v6, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object v5, v6

    const/4 v7, 0x5

    const-string v8, "beige"

    const-string v9, "BEIGE"

    invoke-direct {v6, v9, v7, v8}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v6, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->BEIGE:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v7, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object v6, v7

    const/4 v8, 0x6

    const-string v9, "bisque"

    const-string v10, "BISQUE"

    invoke-direct {v7, v10, v8, v9}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v7, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->BISQUE:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v8, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object v7, v8

    const/4 v9, 0x7

    const-string v10, "black"

    const-string v11, "BLACK"

    invoke-direct {v8, v11, v9, v10}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v8, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->BLACK:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v9, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object v8, v9

    const/16 v10, 0x8

    const-string v11, "blanchedAlmond"

    const-string v12, "BLANCHED_ALMOND"

    invoke-direct {v9, v12, v10, v11}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v9, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->BLANCHED_ALMOND:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v10, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object v9, v10

    const/16 v11, 0x9

    const-string v12, "blue"

    const-string v13, "BLUE"

    invoke-direct {v10, v13, v11, v12}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v10, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->BLUE:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v11, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object v10, v11

    const/16 v12, 0xa

    const-string v13, "blueViolet"

    const-string v14, "BLUE_VIOLET"

    invoke-direct {v11, v14, v12, v13}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v11, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->BLUE_VIOLET:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v12, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object v11, v12

    const/16 v13, 0xb

    const-string v14, "brown"

    const-string v15, "BROWN"

    invoke-direct {v12, v15, v13, v14}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v12, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->BROWN:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v13, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object v12, v13

    const/16 v14, 0xc

    const-string v15, "burlyWood"

    move-object/from16 v140, v0

    const-string v0, "BURLY_WOOD"

    invoke-direct {v13, v0, v14, v15}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v13, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->BURLY_WOOD:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object v13, v0

    const/16 v14, 0xd

    const-string v15, "cadetBlue"

    move-object/from16 v141, v1

    const-string v1, "CADET_BLUE"

    invoke-direct {v0, v1, v14, v15}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->CADET_BLUE:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object v14, v0

    const/16 v1, 0xe

    const-string v15, "chartreuse"

    move-object/from16 v142, v2

    const-string v2, "CHARTREUSE"

    invoke-direct {v0, v2, v1, v15}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->CHARTREUSE:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object v15, v0

    const/16 v1, 0xf

    const-string v2, "chocolate"

    move-object/from16 v143, v3

    const-string v3, "CHOCOLATE"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->CHOCOLATE:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object/from16 v16, v0

    const/16 v1, 0x10

    const-string v2, "coral"

    const-string v3, "CORAL"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->CORAL:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object/from16 v17, v0

    const/16 v1, 0x11

    const-string v2, "cornflowerBlue"

    const-string v3, "CORNFLOWER_BLUE"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->CORNFLOWER_BLUE:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object/from16 v18, v0

    const/16 v1, 0x12

    const-string v2, "cornsilk"

    const-string v3, "CORNSILK"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->CORNSILK:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object/from16 v19, v0

    const/16 v1, 0x13

    const-string v2, "crimson"

    const-string v3, "CRIMSON"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->CRIMSON:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object/from16 v20, v0

    const/16 v1, 0x14

    const-string v2, "cyan"

    const-string v3, "CYAN"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->CYAN:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object/from16 v21, v0

    const/16 v1, 0x15

    const-string v2, "dkBlue"

    const-string v3, "DK_BLUE"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->DK_BLUE:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object/from16 v22, v0

    const/16 v1, 0x16

    const-string v2, "dkCyan"

    const-string v3, "DK_CYAN"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->DK_CYAN:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object/from16 v23, v0

    const/16 v1, 0x17

    const-string v2, "dkGoldenrod"

    const-string v3, "DK_GOLDENROD"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->DK_GOLDENROD:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object/from16 v24, v0

    const/16 v1, 0x18

    const-string v2, "dkGray"

    const-string v3, "DK_GRAY"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->DK_GRAY:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object/from16 v25, v0

    const/16 v1, 0x19

    const-string v2, "dkGreen"

    const-string v3, "DK_GREEN"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->DK_GREEN:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object/from16 v26, v0

    const/16 v1, 0x1a

    const-string v2, "dkKhaki"

    const-string v3, "DK_KHAKI"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->DK_KHAKI:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object/from16 v27, v0

    const/16 v1, 0x1b

    const-string v2, "dkMagenta"

    const-string v3, "DK_MAGENTA"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->DK_MAGENTA:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object/from16 v28, v0

    const/16 v1, 0x1c

    const-string v2, "dkOliveGreen"

    const-string v3, "DK_OLIVE_GREEN"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->DK_OLIVE_GREEN:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object/from16 v29, v0

    const/16 v1, 0x1d

    const-string v2, "dkOrange"

    const-string v3, "DK_ORANGE"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->DK_ORANGE:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object/from16 v30, v0

    const/16 v1, 0x1e

    const-string v2, "dkOrchid"

    const-string v3, "DK_ORCHID"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->DK_ORCHID:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object/from16 v31, v0

    const/16 v1, 0x1f

    const-string v2, "dkRed"

    const-string v3, "DK_RED"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->DK_RED:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object/from16 v32, v0

    const/16 v1, 0x20

    const-string v2, "dkSalmon"

    const-string v3, "DK_SALMON"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->DK_SALMON:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object/from16 v33, v0

    const/16 v1, 0x21

    const-string v2, "dkSeaGreen"

    const-string v3, "DK_SEA_GREEN"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->DK_SEA_GREEN:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object/from16 v34, v0

    const/16 v1, 0x22

    const-string v2, "dkSlateBlue"

    const-string v3, "DK_SLATE_BLUE"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->DK_SLATE_BLUE:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object/from16 v35, v0

    const/16 v1, 0x23

    const-string v2, "dkSlateGray"

    const-string v3, "DK_SLATE_GRAY"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->DK_SLATE_GRAY:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object/from16 v36, v0

    const/16 v1, 0x24

    const-string v2, "dkTurquoise"

    const-string v3, "DK_TURQUOISE"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->DK_TURQUOISE:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object/from16 v37, v0

    const/16 v1, 0x25

    const-string v2, "dkViolet"

    const-string v3, "DK_VIOLET"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->DK_VIOLET:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object/from16 v38, v0

    const/16 v1, 0x26

    const-string v2, "deepPink"

    const-string v3, "DEEP_PINK"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->DEEP_PINK:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object/from16 v39, v0

    const/16 v1, 0x27

    const-string v2, "deepSkyBlue"

    const-string v3, "DEEP_SKY_BLUE"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->DEEP_SKY_BLUE:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object/from16 v40, v0

    const/16 v1, 0x28

    const-string v2, "dimGray"

    const-string v3, "DIM_GRAY"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->DIM_GRAY:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object/from16 v41, v0

    const/16 v1, 0x29

    const-string v2, "dodgerBlue"

    const-string v3, "DODGER_BLUE"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->DODGER_BLUE:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object/from16 v42, v0

    const/16 v1, 0x2a

    const-string v2, "firebrick"

    const-string v3, "FIREBRICK"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->FIREBRICK:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object/from16 v43, v0

    const/16 v1, 0x2b

    const-string v2, "floralWhite"

    const-string v3, "FLORAL_WHITE"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->FLORAL_WHITE:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object/from16 v44, v0

    const/16 v1, 0x2c

    const-string v2, "forestGreen"

    const-string v3, "FOREST_GREEN"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->FOREST_GREEN:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object/from16 v45, v0

    const/16 v1, 0x2d

    const-string v2, "fuchsia"

    const-string v3, "FUCHSIA"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->FUCHSIA:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object/from16 v46, v0

    const/16 v1, 0x2e

    const-string v2, "gainsboro"

    const-string v3, "GAINSBORO"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->GAINSBORO:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object/from16 v47, v0

    const/16 v1, 0x2f

    const-string v2, "ghostWhite"

    const-string v3, "GHOST_WHITE"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->GHOST_WHITE:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object/from16 v48, v0

    const/16 v1, 0x30

    const-string v2, "gold"

    const-string v3, "GOLD"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->GOLD:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object/from16 v49, v0

    const/16 v1, 0x31

    const-string v2, "goldenrod"

    const-string v3, "GOLDENROD"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->GOLDENROD:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object/from16 v50, v0

    const/16 v1, 0x32

    const-string v2, "gray"

    const-string v3, "GRAY"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->GRAY:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object/from16 v51, v0

    const/16 v1, 0x33

    const-string v2, "green"

    const-string v3, "GREEN"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->GREEN:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object/from16 v52, v0

    const/16 v1, 0x34

    const-string v2, "greenYellow"

    const-string v3, "GREEN_YELLOW"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->GREEN_YELLOW:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object/from16 v53, v0

    const/16 v1, 0x35

    const-string v2, "honeydew"

    const-string v3, "HONEYDEW"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->HONEYDEW:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object/from16 v54, v0

    const/16 v1, 0x36

    const-string v2, "hotPink"

    const-string v3, "HOT_PINK"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->HOT_PINK:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object/from16 v55, v0

    const/16 v1, 0x37

    const-string v2, "indianRed"

    const-string v3, "INDIAN_RED"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->INDIAN_RED:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object/from16 v56, v0

    const/16 v1, 0x38

    const-string v2, "indigo"

    const-string v3, "INDIGO"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->INDIGO:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object/from16 v57, v0

    const/16 v1, 0x39

    const-string v2, "ivory"

    const-string v3, "IVORY"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->IVORY:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object/from16 v58, v0

    const/16 v1, 0x3a

    const-string v2, "khaki"

    const-string v3, "KHAKI"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->KHAKI:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object/from16 v59, v0

    const/16 v1, 0x3b

    const-string v2, "lavender"

    const-string v3, "LAVENDER"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->LAVENDER:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object/from16 v60, v0

    const/16 v1, 0x3c

    const-string v2, "lavenderBlush"

    const-string v3, "LAVENDER_BLUSH"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->LAVENDER_BLUSH:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object/from16 v61, v0

    const/16 v1, 0x3d

    const-string v2, "lawnGreen"

    const-string v3, "LAWN_GREEN"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->LAWN_GREEN:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object/from16 v62, v0

    const/16 v1, 0x3e

    const-string v2, "lemonChiffon"

    const-string v3, "LEMON_CHIFFON"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->LEMON_CHIFFON:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object/from16 v63, v0

    const/16 v1, 0x3f

    const-string v2, "ltBlue"

    const-string v3, "LT_BLUE"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->LT_BLUE:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object/from16 v64, v0

    const/16 v1, 0x40

    const-string v2, "ltCoral"

    const-string v3, "LT_CORAL"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->LT_CORAL:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object/from16 v65, v0

    const/16 v1, 0x41

    const-string v2, "ltCyan"

    const-string v3, "LT_CYAN"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->LT_CYAN:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object/from16 v66, v0

    const/16 v1, 0x42

    const-string v2, "ltGoldenrodYellow"

    const-string v3, "LT_GOLDENROD_YELLOW"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->LT_GOLDENROD_YELLOW:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object/from16 v67, v0

    const/16 v1, 0x43

    const-string v2, "ltGray"

    const-string v3, "LT_GRAY"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->LT_GRAY:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object/from16 v68, v0

    const/16 v1, 0x44

    const-string v2, "ltGreen"

    const-string v3, "LT_GREEN"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->LT_GREEN:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object/from16 v69, v0

    const/16 v1, 0x45

    const-string v2, "ltPink"

    const-string v3, "LT_PINK"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->LT_PINK:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object/from16 v70, v0

    const/16 v1, 0x46

    const-string v2, "ltSalmon"

    const-string v3, "LT_SALMON"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->LT_SALMON:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object/from16 v71, v0

    const/16 v1, 0x47

    const-string v2, "ltSeaGreen"

    const-string v3, "LT_SEA_GREEN"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->LT_SEA_GREEN:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object/from16 v72, v0

    const/16 v1, 0x48

    const-string v2, "ltSkyBlue"

    const-string v3, "LT_SKY_BLUE"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->LT_SKY_BLUE:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object/from16 v73, v0

    const/16 v1, 0x49

    const-string v2, "ltSlateGray"

    const-string v3, "LT_SLATE_GRAY"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->LT_SLATE_GRAY:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object/from16 v74, v0

    const/16 v1, 0x4a

    const-string v2, "ltSteelBlue"

    const-string v3, "LT_STEEL_BLUE"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->LT_STEEL_BLUE:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object/from16 v75, v0

    const/16 v1, 0x4b

    const-string v2, "ltYellow"

    const-string v3, "LT_YELLOW"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->LT_YELLOW:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object/from16 v76, v0

    const/16 v1, 0x4c

    const-string v2, "lime"

    const-string v3, "LIME"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->LIME:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object/from16 v77, v0

    const/16 v1, 0x4d

    const-string v2, "limeGreen"

    const-string v3, "LIME_GREEN"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->LIME_GREEN:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object/from16 v78, v0

    const/16 v1, 0x4e

    const-string v2, "linen"

    const-string v3, "LINEN"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->LINEN:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object/from16 v79, v0

    const/16 v1, 0x4f

    const-string v2, "magenta"

    const-string v3, "MAGENTA"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->MAGENTA:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object/from16 v80, v0

    const/16 v1, 0x50

    const-string v2, "maroon"

    const-string v3, "MAROON"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->MAROON:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object/from16 v81, v0

    const/16 v1, 0x51

    const-string v2, "medAquamarine"

    const-string v3, "MED_AQUAMARINE"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->MED_AQUAMARINE:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object/from16 v82, v0

    const/16 v1, 0x52

    const-string v2, "medBlue"

    const-string v3, "MED_BLUE"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->MED_BLUE:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object/from16 v83, v0

    const/16 v1, 0x53

    const-string v2, "medOrchid"

    const-string v3, "MED_ORCHID"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->MED_ORCHID:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object/from16 v84, v0

    const/16 v1, 0x54

    const-string v2, "medPurple"

    const-string v3, "MED_PURPLE"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->MED_PURPLE:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object/from16 v85, v0

    const/16 v1, 0x55

    const-string v2, "medSeaGreen"

    const-string v3, "MED_SEA_GREEN"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->MED_SEA_GREEN:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object/from16 v86, v0

    const/16 v1, 0x56

    const-string v2, "medSlateBlue"

    const-string v3, "MED_SLATE_BLUE"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->MED_SLATE_BLUE:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object/from16 v87, v0

    const/16 v1, 0x57

    const-string v2, "medSpringGreen"

    const-string v3, "MED_SPRING_GREEN"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->MED_SPRING_GREEN:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object/from16 v88, v0

    const/16 v1, 0x58

    const-string v2, "medTurquoise"

    const-string v3, "MED_TURQUOISE"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->MED_TURQUOISE:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object/from16 v89, v0

    const/16 v1, 0x59

    const-string v2, "medVioletRed"

    const-string v3, "MED_VIOLET_RED"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->MED_VIOLET_RED:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object/from16 v90, v0

    const/16 v1, 0x5a

    const-string v2, "midnightBlue"

    const-string v3, "MIDNIGHT_BLUE"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->MIDNIGHT_BLUE:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object/from16 v91, v0

    const/16 v1, 0x5b

    const-string v2, "mintCream"

    const-string v3, "MINT_CREAM"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->MINT_CREAM:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object/from16 v92, v0

    const/16 v1, 0x5c

    const-string v2, "mistyRose"

    const-string v3, "MISTY_ROSE"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->MISTY_ROSE:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object/from16 v93, v0

    const/16 v1, 0x5d

    const-string v2, "moccasin"

    const-string v3, "MOCCASIN"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->MOCCASIN:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object/from16 v94, v0

    const/16 v1, 0x5e

    const-string v2, "navajoWhite"

    const-string v3, "NAVAJO_WHITE"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->NAVAJO_WHITE:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object/from16 v95, v0

    const/16 v1, 0x5f

    const-string v2, "navy"

    const-string v3, "NAVY"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->NAVY:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object/from16 v96, v0

    const/16 v1, 0x60

    const-string v2, "oldLace"

    const-string v3, "OLD_LACE"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->OLD_LACE:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object/from16 v97, v0

    const/16 v1, 0x61

    const-string v2, "olive"

    const-string v3, "OLIVE"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->OLIVE:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object/from16 v98, v0

    const/16 v1, 0x62

    const-string v2, "oliveDrab"

    const-string v3, "OLIVE_DRAB"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->OLIVE_DRAB:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object/from16 v99, v0

    const/16 v1, 0x63

    const-string v2, "orange"

    const-string v3, "ORANGE"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->ORANGE:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object/from16 v100, v0

    const/16 v1, 0x64

    const-string v2, "orangeRed"

    const-string v3, "ORANGE_RED"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->ORANGE_RED:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object/from16 v101, v0

    const/16 v1, 0x65

    const-string v2, "orchid"

    const-string v3, "ORCHID"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->ORCHID:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object/from16 v102, v0

    const/16 v1, 0x66

    const-string v2, "paleGoldenrod"

    const-string v3, "PALE_GOLDENROD"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->PALE_GOLDENROD:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object/from16 v103, v0

    const/16 v1, 0x67

    const-string v2, "paleGreen"

    const-string v3, "PALE_GREEN"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->PALE_GREEN:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object/from16 v104, v0

    const/16 v1, 0x68

    const-string v2, "paleTurquoise"

    const-string v3, "PALE_TURQUOISE"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->PALE_TURQUOISE:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object/from16 v105, v0

    const/16 v1, 0x69

    const-string v2, "paleVioletRed"

    const-string v3, "PALE_VIOLET_RED"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->PALE_VIOLET_RED:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object/from16 v106, v0

    const/16 v1, 0x6a

    const-string v2, "papayaWhip"

    const-string v3, "PAPAYA_WHIP"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->PAPAYA_WHIP:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object/from16 v107, v0

    const/16 v1, 0x6b

    const-string v2, "peachPuff"

    const-string v3, "PEACH_PUFF"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->PEACH_PUFF:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object/from16 v108, v0

    const/16 v1, 0x6c

    const-string v2, "peru"

    const-string v3, "PERU"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->PERU:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object/from16 v109, v0

    const/16 v1, 0x6d

    const-string v2, "pink"

    const-string v3, "PINK"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->PINK:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object/from16 v110, v0

    const/16 v1, 0x6e

    const-string v2, "plum"

    const-string v3, "PLUM"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->PLUM:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object/from16 v111, v0

    const/16 v1, 0x6f

    const-string v2, "powderBlue"

    const-string v3, "POWDER_BLUE"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->POWDER_BLUE:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object/from16 v112, v0

    const/16 v1, 0x70

    const-string v2, "purple"

    const-string v3, "PURPLE"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->PURPLE:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object/from16 v113, v0

    const/16 v1, 0x71

    const-string v2, "red"

    const-string v3, "RED"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->RED:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object/from16 v114, v0

    const/16 v1, 0x72

    const-string v2, "rosyBrown"

    const-string v3, "ROSY_BROWN"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->ROSY_BROWN:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object/from16 v115, v0

    const/16 v1, 0x73

    const-string v2, "royalBlue"

    const-string v3, "ROYAL_BLUE"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->ROYAL_BLUE:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object/from16 v116, v0

    const/16 v1, 0x74

    const-string v2, "saddleBrown"

    const-string v3, "SADDLE_BROWN"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->SADDLE_BROWN:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object/from16 v117, v0

    const/16 v1, 0x75

    const-string v2, "salmon"

    const-string v3, "SALMON"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->SALMON:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object/from16 v118, v0

    const/16 v1, 0x76

    const-string v2, "sandyBrown"

    const-string v3, "SANDY_BROWN"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->SANDY_BROWN:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object/from16 v119, v0

    const/16 v1, 0x77

    const-string v2, "seaGreen"

    const-string v3, "SEA_GREEN"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->SEA_GREEN:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object/from16 v120, v0

    const/16 v1, 0x78

    const-string v2, "seaShell"

    const-string v3, "SEA_SHELL"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->SEA_SHELL:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object/from16 v121, v0

    const/16 v1, 0x79

    const-string v2, "sienna"

    const-string v3, "SIENNA"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->SIENNA:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object/from16 v122, v0

    const/16 v1, 0x7a

    const-string v2, "silver"

    const-string v3, "SILVER"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->SILVER:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object/from16 v123, v0

    const/16 v1, 0x7b

    const-string v2, "skyBlue"

    const-string v3, "SKY_BLUE"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->SKY_BLUE:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object/from16 v124, v0

    const/16 v1, 0x7c

    const-string v2, "slateBlue"

    const-string v3, "SLATE_BLUE"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->SLATE_BLUE:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object/from16 v125, v0

    const/16 v1, 0x7d

    const-string v2, "slateGray"

    const-string v3, "SLATE_GRAY"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->SLATE_GRAY:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object/from16 v126, v0

    const/16 v1, 0x7e

    const-string v2, "snow"

    const-string v3, "SNOW"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->SNOW:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object/from16 v127, v0

    const/16 v1, 0x7f

    const-string v2, "springGreen"

    const-string v3, "SPRING_GREEN"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->SPRING_GREEN:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object/from16 v128, v0

    const/16 v1, 0x80

    const-string v2, "steelBlue"

    const-string v3, "STEEL_BLUE"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->STEEL_BLUE:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object/from16 v129, v0

    const/16 v1, 0x81

    const-string v2, "tan"

    const-string v3, "TAN"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->TAN:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object/from16 v130, v0

    const/16 v1, 0x82

    const-string v2, "teal"

    const-string v3, "TEAL"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->TEAL:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object/from16 v131, v0

    const/16 v1, 0x83

    const-string v2, "thistle"

    const-string v3, "THISTLE"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->THISTLE:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object/from16 v132, v0

    const/16 v1, 0x84

    const-string v2, "tomato"

    const-string v3, "TOMATO"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->TOMATO:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object/from16 v133, v0

    const/16 v1, 0x85

    const-string v2, "turquoise"

    const-string v3, "TURQUOISE"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->TURQUOISE:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object/from16 v134, v0

    const/16 v1, 0x86

    const-string v2, "violet"

    const-string v3, "VIOLET"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->VIOLET:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object/from16 v135, v0

    const/16 v1, 0x87

    const-string v2, "wheat"

    const-string v3, "WHEAT"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->WHEAT:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object/from16 v136, v0

    const/16 v1, 0x88

    const-string v2, "white"

    const-string v3, "WHITE"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->WHITE:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object/from16 v137, v0

    const/16 v1, 0x89

    const-string v2, "whiteSmoke"

    const-string v3, "WHITE_SMOKE"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->WHITE_SMOKE:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object/from16 v138, v0

    const/16 v1, 0x8a

    const-string v2, "yellow"

    const-string v3, "YELLOW"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->YELLOW:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    new-instance v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object/from16 v139, v0

    const/16 v1, 0x8b

    const-string v2, "yellowGreen"

    const-string v3, "YELLOW_GREEN"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->YELLOW_GREEN:Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-object/from16 v0, v140

    move-object/from16 v1, v141

    move-object/from16 v2, v142

    move-object/from16 v3, v143

    filled-new-array/range {v0 .. v139}, [Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->$VALUES:[Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->value:Ljava/lang/String;

    return-void
.end method

.method public static fromValue(Ljava/lang/String;)Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .locals 5

    invoke-static {}, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->values()[Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_1

    aget-object v3, v0, v2

    iget-object v4, v3, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->value:Ljava/lang/String;

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    return-object v3

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static valueOf(Ljava/lang/String;)Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .locals 1

    const-class v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    return-object p0
.end method

.method public static values()[Lorg/apache/poi/sl/draw/binding/STPresetColorVal;
    .locals 1

    sget-object v0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->$VALUES:[Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    invoke-virtual {v0}, [Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lorg/apache/poi/sl/draw/binding/STPresetColorVal;

    return-object v0
.end method


# virtual methods
.method public value()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/apache/poi/sl/draw/binding/STPresetColorVal;->value:Ljava/lang/String;

    return-object v0
.end method

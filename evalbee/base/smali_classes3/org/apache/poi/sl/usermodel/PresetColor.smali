.class public final enum Lorg/apache/poi/sl/usermodel/PresetColor;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lorg/apache/poi/sl/usermodel/PresetColor;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum ActiveBorder:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum ActiveCaption:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum ActiveCaptionText:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum AliceBlue:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum AntiqueWhite:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum AppWorkspace:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum Aqua:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum Aquamarine:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum Azure:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum Beige:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum Bisque:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum Black:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum BlanchedAlmond:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum Blue:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum BlueViolet:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum Brown:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum BurlyWood:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum ButtonFace:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum ButtonHighlight:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum ButtonShadow:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum CadetBlue:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum Chartreuse:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum Chocolate:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum Control:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum ControlDark:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum ControlDarkDark:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum ControlLight:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum ControlLightLight:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum ControlText:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum Coral:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum CornflowerBlue:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum Cornsilk:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum Crimson:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum Cyan:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum DarkBlue:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum DarkCyan:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum DarkGoldenrod:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum DarkGray:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum DarkGreen:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum DarkKhaki:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum DarkMagenta:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum DarkOliveGreen:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum DarkOrange:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum DarkOrchid:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum DarkRed:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum DarkSalmon:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum DarkSeaGreen:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum DarkSlateBlue:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum DarkSlateGray:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum DarkTurquoise:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum DarkViolet:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum DeepPink:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum DeepSkyBlue:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum Desktop:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum DimGray:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum DodgerBlue:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum Firebrick:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum FloralWhite:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum ForestGreen:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum Fuchsia:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum Gainsboro:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum GhostWhite:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum Gold:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum Goldenrod:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum GradientActiveCaption:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum GradientInactiveCaption:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum Gray:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum GrayText:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum Green:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum GreenYellow:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum Highlight:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum HighlightText:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum Honeydew:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum HotPink:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum HotTrack:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum InactiveBorder:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum InactiveCaption:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum InactiveCaptionText:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum IndianRed:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum Indigo:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum Info:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum InfoText:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum Ivory:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum Khaki:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum Lavender:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum LavenderBlush:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum LawnGreen:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum LemonChiffon:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum LightBlue:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum LightCoral:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum LightCyan:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum LightGoldenrodYellow:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum LightGray:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum LightGreen:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum LightPink:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum LightSalmon:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum LightSeaGreen:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum LightSkyBlue:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum LightSlateGray:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum LightSteelBlue:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum LightYellow:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum Lime:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum LimeGreen:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum Linen:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum Magenta:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum Maroon:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum MediumAquamarine:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum MediumBlue:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum MediumOrchid:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum MediumPurple:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum MediumSeaGreen:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum MediumSlateBlue:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum MediumSpringGreen:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum MediumTurquoise:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum MediumVioletRed:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum Menu:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum MenuBar:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum MenuHighlight:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum MenuText:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum MidnightBlue:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum MintCream:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum MistyRose:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum Moccasin:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum NavajoWhite:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum Navy:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum OldLace:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum Olive:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum OliveDrab:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum Orange:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum OrangeRed:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum Orchid:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum PaleGoldenrod:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum PaleGreen:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum PaleTurquoise:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum PaleVioletRed:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum PapayaWhip:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum PeachPuff:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum Peru:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum Pink:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum Plum:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum PowderBlue:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum Purple:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum Red:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum RosyBrown:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum RoyalBlue:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum SaddleBrown:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum Salmon:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum SandyBrown:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum ScrollBar:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum SeaGreen:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum SeaShell:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum Sienna:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum Silver:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum SkyBlue:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum SlateBlue:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum SlateGray:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum Snow:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum SpringGreen:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum SteelBlue:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum Tan:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum Teal:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum Thistle:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum Tomato:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum Transparent:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum Turquoise:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum Violet:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum Wheat:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum White:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum WhiteSmoke:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum Window:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum WindowFrame:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum WindowText:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum Yellow:Lorg/apache/poi/sl/usermodel/PresetColor;

.field public static final enum YellowGreen:Lorg/apache/poi/sl/usermodel/PresetColor;

.field private static final lookupOoxmlId:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lorg/apache/poi/sl/usermodel/PresetColor;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final color:Ljava/awt/Color;

.field public final nativeId:I

.field public final ooxmlId:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 199

    new-instance v7, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object v6, v7

    const-string v1, "ActiveBorder"

    const/4 v2, 0x0

    const v0, -0x4b4b4c

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/4 v4, 0x1

    const-string v5, "activeBorder"

    move-object v0, v7

    invoke-direct/range {v0 .. v5}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v7, Lorg/apache/poi/sl/usermodel/PresetColor;->ActiveBorder:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object v7, v0

    const-string v9, "ActiveCaption"

    const/4 v10, 0x1

    const v1, -0x664b2f

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    const/4 v12, 0x2

    const-string v13, "activeCaption"

    move-object v8, v0

    invoke-direct/range {v8 .. v13}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->ActiveCaption:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object v8, v0

    const-string v15, "ActiveCaptionText"

    const/16 v16, 0x2

    const/high16 v1, -0x1000000

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v44

    const/16 v18, 0x3

    const-string v19, "captionText"

    move-object v14, v0

    move-object/from16 v17, v44

    invoke-direct/range {v14 .. v19}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->ActiveCaptionText:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object v9, v0

    const-string v21, "AppWorkspace"

    const/16 v22, 0x3

    const v1, -0x545455

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v23

    const/16 v24, 0x4

    const-string v25, "appWorkspace"

    move-object/from16 v20, v0

    invoke-direct/range {v20 .. v25}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->AppWorkspace:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object v10, v0

    const-string v12, "Control"

    const/4 v13, 0x4

    const v1, -0xf0f10

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v182

    const/4 v15, 0x5

    const-string v16, "btnFace"

    move-object v11, v0

    move-object/from16 v14, v182

    invoke-direct/range {v11 .. v16}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->Control:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object v11, v0

    const-string v18, "ControlDark"

    const/16 v19, 0x5

    const v1, -0x969697

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v77

    const/16 v21, 0x6

    const-string v22, "btnShadow"

    move-object/from16 v17, v0

    move-object/from16 v20, v77

    invoke-direct/range {v17 .. v22}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->ControlDark:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object v12, v0

    const-string v18, "ControlDarkDark"

    const/16 v19, 0x6

    const/16 v21, 0x7

    const-string v22, "3dDkShadow"

    move-object/from16 v17, v0

    move-object/from16 v20, v44

    invoke-direct/range {v17 .. v22}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->ControlDarkDark:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object v13, v0

    const-string v24, "ControlLight"

    const/16 v25, 0x7

    const v1, -0x1c1c1d

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    const/16 v27, 0x8

    const-string v28, "btnHighlight"

    move-object/from16 v23, v0

    move-object/from16 v26, v18

    invoke-direct/range {v23 .. v28}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->ControlLight:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object v14, v0

    const-string v16, "ControlLightLight"

    const/16 v17, 0x8

    const/16 v19, 0x9

    const-string v20, "3dLight"

    move-object v15, v0

    invoke-direct/range {v15 .. v20}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->ControlLightLight:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object v15, v0

    const-string v18, "ControlText"

    const/16 v21, 0xa

    const-string v22, "btnText"

    move-object/from16 v17, v0

    move-object/from16 v20, v44

    invoke-direct/range {v17 .. v22}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->ControlText:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v16, v0

    const-string v18, "Desktop"

    const/16 v19, 0xa

    const/16 v21, 0xb

    const-string v22, "background"

    move-object/from16 v17, v0

    invoke-direct/range {v17 .. v22}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->Desktop:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v17, v0

    const-string v24, "GrayText"

    const/16 v25, 0xb

    const v1, -0x929293

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v26

    const/16 v27, 0xc

    const-string v28, "grayText"

    move-object/from16 v23, v0

    invoke-direct/range {v23 .. v28}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->GrayText:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v18, v0

    const-string v30, "Highlight"

    const/16 v31, 0xc

    const v1, -0xcc6601

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v186

    const/16 v33, 0xd

    const-string v34, "highlight"

    move-object/from16 v29, v0

    move-object/from16 v32, v186

    invoke-direct/range {v29 .. v34}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->Highlight:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v19, v0

    const-string v21, "HighlightText"

    const/16 v22, 0xd

    const/4 v1, -0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/16 v24, 0xe

    const-string v25, "highlightText"

    move-object/from16 v20, v0

    move-object/from16 v23, v1

    invoke-direct/range {v20 .. v25}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->HighlightText:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v20, v0

    const-string v27, "HotTrack"

    const/16 v28, 0xe

    const v2, -0xff9934

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v29

    const/16 v30, 0xf

    const-string v31, "hotLight"

    move-object/from16 v26, v0

    invoke-direct/range {v26 .. v31}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->HotTrack:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v21, v0

    const-string v33, "InactiveBorder"

    const/16 v34, 0xf

    const v2, -0xb0804

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v35

    const/16 v36, 0x10

    const-string v37, "inactiveBorder"

    move-object/from16 v32, v0

    invoke-direct/range {v32 .. v37}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->InactiveBorder:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v22, v0

    const-string v24, "InactiveCaption"

    const/16 v25, 0x10

    const v2, -0x403225

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v26

    const/16 v27, 0x11

    const-string v28, "inactiveCaption"

    move-object/from16 v23, v0

    invoke-direct/range {v23 .. v28}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->InactiveCaption:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v23, v0

    const-string v25, "InactiveCaptionText"

    const/16 v26, 0x11

    const/16 v28, 0x12

    const-string v29, "inactiveCaptionText"

    move-object/from16 v24, v0

    move-object/from16 v27, v44

    invoke-direct/range {v24 .. v29}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->InactiveCaptionText:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v24, v0

    const-string v31, "Info"

    const/16 v32, 0x12

    const/16 v2, -0x1f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v33

    const/16 v34, 0x13

    const-string v35, "infoBk"

    move-object/from16 v30, v0

    invoke-direct/range {v30 .. v35}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->Info:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v25, v0

    const-string v27, "InfoText"

    const/16 v28, 0x13

    const/16 v30, 0x14

    const-string v31, "infoText"

    move-object/from16 v26, v0

    move-object/from16 v29, v44

    invoke-direct/range {v26 .. v31}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->InfoText:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v26, v0

    const-string v28, "Menu"

    const/16 v29, 0x14

    const/16 v31, 0x15

    const-string v32, "menu"

    move-object/from16 v27, v0

    move-object/from16 v30, v182

    invoke-direct/range {v27 .. v32}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->Menu:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v27, v0

    const-string v29, "MenuText"

    const/16 v30, 0x15

    const/16 v32, 0x16

    const-string v33, "menuText"

    move-object/from16 v28, v0

    move-object/from16 v31, v44

    invoke-direct/range {v28 .. v33}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->MenuText:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v28, v0

    const-string v35, "ScrollBar"

    const/16 v36, 0x16

    const v2, -0x373738

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v37

    const/16 v38, 0x17

    const-string v39, "scrollBar"

    move-object/from16 v34, v0

    invoke-direct/range {v34 .. v39}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->ScrollBar:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v29, v0

    const-string v31, "Window"

    const/16 v32, 0x17

    const/16 v34, 0x18

    const-string v35, "window"

    move-object/from16 v30, v0

    move-object/from16 v33, v1

    invoke-direct/range {v30 .. v35}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->Window:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v30, v0

    const-string v37, "WindowFrame"

    const/16 v38, 0x18

    const v2, -0x9b9b9c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v39

    const/16 v40, 0x19

    const-string v41, "windowFrame"

    move-object/from16 v36, v0

    invoke-direct/range {v36 .. v41}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->WindowFrame:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v31, v0

    const-string v33, "WindowText"

    const/16 v34, 0x19

    const/16 v36, 0x1a

    const-string v37, "windowText"

    move-object/from16 v32, v0

    move-object/from16 v35, v44

    invoke-direct/range {v32 .. v37}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->WindowText:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v32, v0

    const-string v39, "Transparent"

    const/16 v40, 0x1a

    const v2, 0xffffff

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v41

    const/16 v42, 0x1b

    const/16 v43, 0x0

    move-object/from16 v38, v0

    invoke-direct/range {v38 .. v43}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->Transparent:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v33, v0

    const-string v46, "AliceBlue"

    const/16 v47, 0x1b

    const v2, -0xf0701

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v48

    const/16 v49, 0x1c

    const-string v50, "aliceBlue"

    move-object/from16 v45, v0

    invoke-direct/range {v45 .. v50}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->AliceBlue:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v34, v0

    const-string v36, "AntiqueWhite"

    const/16 v37, 0x1c

    const v2, -0x51429

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v38

    const/16 v39, 0x1d

    const-string v40, "antiqueWhite"

    move-object/from16 v35, v0

    invoke-direct/range {v35 .. v40}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->AntiqueWhite:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v35, v0

    const-string v46, "Aqua"

    const/16 v47, 0x1d

    const v2, -0xff0001

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v57

    const/16 v49, 0x1e

    const-string v50, "aqua"

    move-object/from16 v45, v0

    move-object/from16 v48, v57

    invoke-direct/range {v45 .. v50}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->Aqua:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v36, v0

    const-string v38, "Aquamarine"

    const/16 v39, 0x1e

    const v2, -0x80002c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v40

    const/16 v41, 0x1f

    const-string v42, "aquamarine"

    move-object/from16 v37, v0

    invoke-direct/range {v37 .. v42}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->Aquamarine:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v37, v0

    const-string v46, "Azure"

    const/16 v47, 0x1f

    const v2, -0xf0001

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v48

    const/16 v49, 0x20

    const-string v50, "azure"

    move-object/from16 v45, v0

    invoke-direct/range {v45 .. v50}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->Azure:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v38, v0

    const-string v52, "Beige"

    const/16 v53, 0x20

    const v2, -0xa0a24

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v54

    const/16 v55, 0x21

    const-string v56, "beige"

    move-object/from16 v51, v0

    invoke-direct/range {v51 .. v56}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->Beige:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v39, v0

    const-string v46, "Bisque"

    const/16 v47, 0x21

    const/16 v2, -0x1b3c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v48

    const/16 v49, 0x22

    const-string v50, "bisque"

    move-object/from16 v45, v0

    invoke-direct/range {v45 .. v50}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->Bisque:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v40, v0

    const-string v42, "Black"

    const/16 v43, 0x22

    const/16 v45, 0x23

    const-string v46, "black"

    move-object/from16 v41, v0

    invoke-direct/range {v41 .. v46}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->Black:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v41, v0

    const-string v48, "BlanchedAlmond"

    const/16 v49, 0x23

    const/16 v2, -0x1433

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v50

    const/16 v51, 0x24

    const-string v52, "blanchedAlmond"

    move-object/from16 v47, v0

    invoke-direct/range {v47 .. v52}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->BlanchedAlmond:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v42, v0

    const-string v59, "Blue"

    const/16 v60, 0x24

    const v2, -0xffff01

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v61

    const/16 v62, 0x25

    const-string v63, "blue"

    move-object/from16 v58, v0

    invoke-direct/range {v58 .. v63}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->Blue:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v43, v0

    const-string v45, "BlueViolet"

    const/16 v46, 0x25

    const v2, -0x75d41e

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v47

    const/16 v48, 0x26

    const-string v49, "blueViolet"

    move-object/from16 v44, v0

    invoke-direct/range {v44 .. v49}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->BlueViolet:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v44, v0

    const-string v51, "Brown"

    const/16 v52, 0x26

    const v2, -0x5ad5d6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v53

    const/16 v54, 0x27

    const-string v55, "brown"

    move-object/from16 v50, v0

    invoke-direct/range {v50 .. v55}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->Brown:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v45, v0

    const-string v59, "BurlyWood"

    const/16 v60, 0x27

    const v2, -0x214779

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v61

    const/16 v62, 0x28

    const-string v63, "burlyWood"

    move-object/from16 v58, v0

    invoke-direct/range {v58 .. v63}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->BurlyWood:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v46, v0

    const-string v48, "CadetBlue"

    const/16 v49, 0x28

    const v2, -0xa06160

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v50

    const/16 v51, 0x29

    const-string v52, "cadetBlue"

    move-object/from16 v47, v0

    invoke-direct/range {v47 .. v52}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->CadetBlue:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v47, v0

    const-string v59, "Chartreuse"

    const/16 v60, 0x29

    const v2, -0x800100

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v61

    const/16 v62, 0x2a

    const-string v63, "chartreuse"

    move-object/from16 v58, v0

    invoke-direct/range {v58 .. v63}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->Chartreuse:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v48, v0

    const-string v50, "Chocolate"

    const/16 v51, 0x2a

    const v2, -0x2d96e2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v52

    const/16 v53, 0x2b

    const-string v54, "chocolate"

    move-object/from16 v49, v0

    invoke-direct/range {v49 .. v54}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->Chocolate:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v49, v0

    const-string v59, "Coral"

    const/16 v60, 0x2b

    const v2, -0x80b0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v61

    const/16 v62, 0x2c

    const-string v63, "coral"

    move-object/from16 v58, v0

    invoke-direct/range {v58 .. v63}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->Coral:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v50, v0

    const-string v52, "CornflowerBlue"

    const/16 v53, 0x2c

    const v2, -0x9b6a13

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v54

    const/16 v55, 0x2d

    const-string v56, "cornflowerBlue"

    move-object/from16 v51, v0

    invoke-direct/range {v51 .. v56}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->CornflowerBlue:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v51, v0

    const-string v59, "Cornsilk"

    const/16 v60, 0x2d

    const/16 v2, -0x724

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v61

    const/16 v62, 0x2e

    const-string v63, "cornsilk"

    move-object/from16 v58, v0

    invoke-direct/range {v58 .. v63}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->Cornsilk:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v52, v0

    const-string v65, "Crimson"

    const/16 v66, 0x2e

    const v2, -0x23ebc4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v67

    const/16 v68, 0x2f

    const-string v69, "crimson"

    move-object/from16 v64, v0

    invoke-direct/range {v64 .. v69}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->Crimson:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v53, v0

    const-string v55, "Cyan"

    const/16 v56, 0x2f

    const/16 v58, 0x30

    const-string v59, "cyan"

    move-object/from16 v54, v0

    invoke-direct/range {v54 .. v59}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->Cyan:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v54, v0

    const-string v61, "DarkBlue"

    const/16 v62, 0x30

    const v2, -0xffff75

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v63

    const/16 v64, 0x31

    const-string v65, "dkBlue"

    move-object/from16 v60, v0

    invoke-direct/range {v60 .. v65}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->DarkBlue:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v55, v0

    const-string v67, "DarkCyan"

    const/16 v68, 0x31

    const v2, -0xff7475

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v69

    const/16 v70, 0x32

    const-string v71, "dkCyan"

    move-object/from16 v66, v0

    invoke-direct/range {v66 .. v71}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->DarkCyan:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v56, v0

    const-string v58, "DarkGoldenrod"

    const/16 v59, 0x32

    const v2, -0x4779f5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v60

    const/16 v61, 0x33

    const-string v62, "dkGoldenrod"

    move-object/from16 v57, v0

    invoke-direct/range {v57 .. v62}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->DarkGoldenrod:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v57, v0

    const-string v64, "DarkGray"

    const/16 v65, 0x33

    const v2, -0x565657

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v66

    const/16 v67, 0x34

    const-string v68, "dkGray"

    move-object/from16 v63, v0

    invoke-direct/range {v63 .. v68}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->DarkGray:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v58, v0

    const-string v70, "DarkGreen"

    const/16 v71, 0x34

    const v2, -0xff9c00

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v72

    const/16 v73, 0x35

    const-string v74, "dkGreen"

    move-object/from16 v69, v0

    invoke-direct/range {v69 .. v74}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->DarkGreen:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v59, v0

    const-string v61, "DarkKhaki"

    const/16 v62, 0x35

    const v2, -0x424895

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v63

    const/16 v64, 0x36

    const-string v65, "dkKhaki"

    move-object/from16 v60, v0

    invoke-direct/range {v60 .. v65}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->DarkKhaki:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v60, v0

    const-string v67, "DarkMagenta"

    const/16 v68, 0x36

    const v2, -0x74ff75

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v69

    const/16 v70, 0x37

    const-string v71, "dkMagenta"

    move-object/from16 v66, v0

    invoke-direct/range {v66 .. v71}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->DarkMagenta:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v61, v0

    const-string v79, "DarkOliveGreen"

    const/16 v80, 0x37

    const v2, -0xaa94d1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v81

    const/16 v82, 0x38

    const-string v83, "dkOliveGreen"

    move-object/from16 v78, v0

    invoke-direct/range {v78 .. v83}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->DarkOliveGreen:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v62, v0

    const-string v64, "DarkOrange"

    const/16 v65, 0x38

    const/16 v2, -0x7400

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v66

    const/16 v67, 0x39

    const-string v68, "dkOrange"

    move-object/from16 v63, v0

    invoke-direct/range {v63 .. v68}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->DarkOrange:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v63, v0

    const-string v70, "DarkOrchid"

    const/16 v71, 0x39

    const v2, -0x66cd34

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v72

    const/16 v73, 0x3a

    const-string v74, "dkOrchid"

    move-object/from16 v69, v0

    invoke-direct/range {v69 .. v74}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->DarkOrchid:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v64, v0

    const-string v79, "DarkRed"

    const/16 v80, 0x3a

    const/high16 v2, -0x750000

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v81

    const/16 v82, 0x3b

    const-string v83, "dkRed"

    move-object/from16 v78, v0

    invoke-direct/range {v78 .. v83}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->DarkRed:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v65, v0

    const-string v67, "DarkSalmon"

    const/16 v68, 0x3b

    const v2, -0x166986

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v69

    const/16 v70, 0x3c

    const-string v71, "dkSalmon"

    move-object/from16 v66, v0

    invoke-direct/range {v66 .. v71}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->DarkSalmon:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v66, v0

    const-string v79, "DarkSeaGreen"

    const/16 v80, 0x3c

    const v2, -0x704375

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v81

    const/16 v82, 0x3d

    const-string v83, "dkSeaGreen"

    move-object/from16 v78, v0

    invoke-direct/range {v78 .. v83}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->DarkSeaGreen:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v67, v0

    const-string v69, "DarkSlateBlue"

    const/16 v70, 0x3d

    const v2, -0xb7c275

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v71

    const/16 v72, 0x3e

    const-string v73, "dkSlateBlue"

    move-object/from16 v68, v0

    invoke-direct/range {v68 .. v73}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->DarkSlateBlue:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v68, v0

    const-string v79, "DarkSlateGray"

    const/16 v80, 0x3e

    const v2, -0xd0b0b1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v81

    const/16 v82, 0x3f

    const-string v83, "dkSlateGray"

    move-object/from16 v78, v0

    invoke-direct/range {v78 .. v83}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->DarkSlateGray:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v69, v0

    const-string v71, "DarkTurquoise"

    const/16 v72, 0x3f

    const v2, -0xff312f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v73

    const/16 v74, 0x40

    const-string v75, "dkTurquoise"

    move-object/from16 v70, v0

    invoke-direct/range {v70 .. v75}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->DarkTurquoise:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v70, v0

    const-string v79, "DarkViolet"

    const/16 v80, 0x40

    const v2, -0x6bff2d

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v81

    const/16 v82, 0x41

    const-string v83, "dkViolet"

    move-object/from16 v78, v0

    invoke-direct/range {v78 .. v83}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->DarkViolet:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v71, v0

    const-string v85, "DeepPink"

    const/16 v86, 0x41

    const v2, -0xeb6d

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v87

    const/16 v88, 0x42

    const-string v89, "deepPink"

    move-object/from16 v84, v0

    invoke-direct/range {v84 .. v89}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->DeepPink:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v72, v0

    const-string v79, "DeepSkyBlue"

    const/16 v80, 0x42

    const v2, -0xff4001

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v81

    const/16 v82, 0x43

    const-string v83, "deepSkyBlue"

    move-object/from16 v78, v0

    invoke-direct/range {v78 .. v83}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->DeepSkyBlue:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v73, v0

    const-string v75, "DimGray"

    const/16 v76, 0x43

    const/16 v78, 0x44

    const-string v79, "dimGray"

    move-object/from16 v74, v0

    invoke-direct/range {v74 .. v79}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->DimGray:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v74, v0

    const-string v81, "DodgerBlue"

    const/16 v82, 0x44

    const v2, -0xe16f01

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v83

    const/16 v84, 0x45

    const-string v85, "dodgerBlue"

    move-object/from16 v80, v0

    invoke-direct/range {v80 .. v85}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->DodgerBlue:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v75, v0

    const-string v87, "Firebrick"

    const/16 v88, 0x45

    const v2, -0x4dddde

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v89

    const/16 v90, 0x46

    const-string v91, "firebrick"

    move-object/from16 v86, v0

    invoke-direct/range {v86 .. v91}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->Firebrick:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v76, v0

    const-string v78, "FloralWhite"

    const/16 v79, 0x46

    const/16 v2, -0x510

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v80

    const/16 v81, 0x47

    const-string v82, "floralWhite"

    move-object/from16 v77, v0

    invoke-direct/range {v77 .. v82}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->FloralWhite:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v77, v0

    const-string v84, "ForestGreen"

    const/16 v85, 0x47

    const v2, -0xdd74de

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v86

    const/16 v87, 0x48

    const-string v88, "forestGreen"

    move-object/from16 v83, v0

    invoke-direct/range {v83 .. v88}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->ForestGreen:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v78, v0

    const-string v90, "Fuchsia"

    const/16 v91, 0x48

    const v2, -0xff01

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v116

    const/16 v93, 0x49

    const-string v94, "fuchsia"

    move-object/from16 v89, v0

    move-object/from16 v92, v116

    invoke-direct/range {v89 .. v94}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->Fuchsia:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v79, v0

    const-string v81, "Gainsboro"

    const/16 v82, 0x49

    const v2, -0x232324

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v83

    const/16 v84, 0x4a

    const-string v85, "gainsboro"

    move-object/from16 v80, v0

    invoke-direct/range {v80 .. v85}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->Gainsboro:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v80, v0

    const-string v87, "GhostWhite"

    const/16 v88, 0x4a

    const v2, -0x70701

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v89

    const/16 v90, 0x4b

    const-string v91, "ghostWhite"

    move-object/from16 v86, v0

    invoke-direct/range {v86 .. v91}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->GhostWhite:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v81, v0

    const-string v93, "Gold"

    const/16 v94, 0x4b

    const/16 v2, -0x2900

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v95

    const/16 v96, 0x4c

    const-string v97, "gold"

    move-object/from16 v92, v0

    invoke-direct/range {v92 .. v97}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->Gold:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v82, v0

    const-string v84, "Goldenrod"

    const/16 v85, 0x4c

    const v2, -0x255ae0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v86

    const/16 v87, 0x4d

    const-string v88, "goldenrod"

    move-object/from16 v83, v0

    invoke-direct/range {v83 .. v88}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->Goldenrod:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v83, v0

    const-string v90, "Gray"

    const/16 v91, 0x4d

    const v2, -0x7f7f80

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v92

    const/16 v93, 0x4e

    const-string v94, "gray"

    move-object/from16 v89, v0

    invoke-direct/range {v89 .. v94}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->Gray:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v84, v0

    const-string v96, "Green"

    const/16 v97, 0x4e

    const v2, -0xff8000

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v98

    const/16 v99, 0x4f

    const-string v100, "green"

    move-object/from16 v95, v0

    invoke-direct/range {v95 .. v100}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->Green:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v85, v0

    const-string v87, "GreenYellow"

    const/16 v88, 0x4f

    const v2, -0x5200d1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v89

    const/16 v90, 0x50

    const-string v91, "greenYellow"

    move-object/from16 v86, v0

    invoke-direct/range {v86 .. v91}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->GreenYellow:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v86, v0

    const-string v93, "Honeydew"

    const/16 v94, 0x50

    const v2, -0xf0010

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v95

    const/16 v96, 0x51

    const-string v97, "honeydew"

    move-object/from16 v92, v0

    invoke-direct/range {v92 .. v97}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->Honeydew:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v87, v0

    const-string v99, "HotPink"

    const/16 v100, 0x51

    const v2, -0x964c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v101

    const/16 v102, 0x52

    const-string v103, "hotPink"

    move-object/from16 v98, v0

    invoke-direct/range {v98 .. v103}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->HotPink:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v88, v0

    const-string v90, "IndianRed"

    const/16 v91, 0x52

    const v2, -0x32a3a4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v92

    const/16 v93, 0x53

    const-string v94, "indianRed"

    move-object/from16 v89, v0

    invoke-direct/range {v89 .. v94}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->IndianRed:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v89, v0

    const-string v96, "Indigo"

    const/16 v97, 0x53

    const v2, -0xb4ff7e

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v98

    const/16 v99, 0x54

    const-string v100, "indigo"

    move-object/from16 v95, v0

    invoke-direct/range {v95 .. v100}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->Indigo:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v90, v0

    const-string v102, "Ivory"

    const/16 v103, 0x54

    const/16 v2, -0x10

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v104

    const/16 v105, 0x55

    const-string v106, "ivory"

    move-object/from16 v101, v0

    invoke-direct/range {v101 .. v106}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->Ivory:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v91, v0

    const-string v93, "Khaki"

    const/16 v94, 0x55

    const v2, -0xf1974

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v95

    const/16 v96, 0x56

    const-string v97, "khaki"

    move-object/from16 v92, v0

    invoke-direct/range {v92 .. v97}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->Khaki:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v92, v0

    const-string v99, "Lavender"

    const/16 v100, 0x56

    const v2, -0x191906

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v101

    const/16 v102, 0x57

    const-string v103, "lavender"

    move-object/from16 v98, v0

    invoke-direct/range {v98 .. v103}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->Lavender:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v93, v0

    const-string v105, "LavenderBlush"

    const/16 v106, 0x57

    const/16 v2, -0xf0b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v107

    const/16 v108, 0x58

    const-string v109, "lavenderBlush"

    move-object/from16 v104, v0

    invoke-direct/range {v104 .. v109}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->LavenderBlush:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v94, v0

    const-string v96, "LawnGreen"

    const/16 v97, 0x58

    const v2, -0x830400

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v98

    const/16 v99, 0x59

    const-string v100, "lawnGreen"

    move-object/from16 v95, v0

    invoke-direct/range {v95 .. v100}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->LawnGreen:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v95, v0

    const-string v102, "LemonChiffon"

    const/16 v103, 0x59

    const/16 v2, -0x533

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v104

    const/16 v105, 0x5a

    const-string v106, "lemonChiffon"

    move-object/from16 v101, v0

    invoke-direct/range {v101 .. v106}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->LemonChiffon:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v96, v0

    const-string v108, "LightBlue"

    const/16 v109, 0x5a

    const v2, -0x52271a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v110

    const/16 v111, 0x5b

    const-string v112, "ltBlue"

    move-object/from16 v107, v0

    invoke-direct/range {v107 .. v112}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->LightBlue:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v97, v0

    const-string v99, "LightCoral"

    const/16 v100, 0x5b

    const v2, -0xf7f80

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v101

    const/16 v102, 0x5c

    const-string v103, "ltCoral"

    move-object/from16 v98, v0

    invoke-direct/range {v98 .. v103}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->LightCoral:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v98, v0

    const-string v105, "LightCyan"

    const/16 v106, 0x5c

    const v2, -0x1f0001

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v107

    const/16 v108, 0x5d

    const-string v109, "ltCyan"

    move-object/from16 v104, v0

    invoke-direct/range {v104 .. v109}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->LightCyan:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v99, v0

    const-string v111, "LightGoldenrodYellow"

    const/16 v112, 0x5d

    const v2, -0x50588

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v113

    const/16 v114, 0x5e

    const-string v115, "ltGoldenrodYellow"

    move-object/from16 v110, v0

    invoke-direct/range {v110 .. v115}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->LightGoldenrodYellow:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v100, v0

    const-string v102, "LightGray"

    const/16 v103, 0x5e

    const v2, -0x2c2c2d

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v104

    const/16 v105, 0x5f

    const-string v106, "ltGray"

    move-object/from16 v101, v0

    invoke-direct/range {v101 .. v106}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->LightGray:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v101, v0

    const-string v108, "LightGreen"

    const/16 v109, 0x5f

    const v2, -0x6f1170

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v110

    const/16 v111, 0x60

    const-string v112, "ltGreen"

    move-object/from16 v107, v0

    invoke-direct/range {v107 .. v112}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->LightGreen:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v102, v0

    const-string v118, "LightPink"

    const/16 v119, 0x60

    const/16 v2, -0x493f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v120

    const/16 v121, 0x61

    const-string v122, "ltPink"

    move-object/from16 v117, v0

    invoke-direct/range {v117 .. v122}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->LightPink:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v103, v0

    const-string v105, "LightSalmon"

    const/16 v106, 0x61

    const/16 v2, -0x5f86

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v107

    const/16 v108, 0x62

    const-string v109, "ltSalmon"

    move-object/from16 v104, v0

    invoke-direct/range {v104 .. v109}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->LightSalmon:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v104, v0

    const-string v111, "LightSeaGreen"

    const/16 v112, 0x62

    const v2, -0xdf4d56

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v113

    const/16 v114, 0x63

    const-string v115, "ltSeaGreen"

    move-object/from16 v110, v0

    invoke-direct/range {v110 .. v115}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->LightSeaGreen:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v105, v0

    const-string v118, "LightSkyBlue"

    const/16 v119, 0x63

    const v2, -0x783106

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v120

    const/16 v121, 0x64

    const-string v122, "ltSkyBlue"

    move-object/from16 v117, v0

    invoke-direct/range {v117 .. v122}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->LightSkyBlue:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v106, v0

    const-string v108, "LightSlateGray"

    const/16 v109, 0x64

    const v2, -0x887767

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v110

    const/16 v111, 0x65

    const-string v112, "ltSlateGray"

    move-object/from16 v107, v0

    invoke-direct/range {v107 .. v112}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->LightSlateGray:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v107, v0

    const-string v118, "LightSteelBlue"

    const/16 v119, 0x65

    const v2, -0x4f3b22

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v120

    const/16 v121, 0x66

    const-string v122, "ltSteelBlue"

    move-object/from16 v117, v0

    invoke-direct/range {v117 .. v122}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->LightSteelBlue:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v108, v0

    const-string v110, "LightYellow"

    const/16 v111, 0x66

    const/16 v2, -0x20

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v112

    const/16 v113, 0x67

    const-string v114, "ltYellow"

    move-object/from16 v109, v0

    invoke-direct/range {v109 .. v114}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->LightYellow:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v109, v0

    const-string v118, "Lime"

    const/16 v119, 0x67

    const v2, -0xff0100

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v120

    const/16 v121, 0x68

    const-string v122, "lime"

    move-object/from16 v117, v0

    invoke-direct/range {v117 .. v122}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->Lime:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v110, v0

    const-string v124, "LimeGreen"

    const/16 v125, 0x68

    const v2, -0xcd32ce

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v126

    const/16 v127, 0x69

    const-string v128, "limeGreen"

    move-object/from16 v123, v0

    invoke-direct/range {v123 .. v128}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->LimeGreen:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v111, v0

    const-string v118, "Linen"

    const/16 v119, 0x69

    const v2, -0x50f1a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v120

    const/16 v121, 0x6a

    const-string v122, "linen"

    move-object/from16 v117, v0

    invoke-direct/range {v117 .. v122}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->Linen:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v112, v0

    const-string v114, "Magenta"

    const/16 v115, 0x6a

    const/16 v117, 0x6b

    const-string v118, "magenta"

    move-object/from16 v113, v0

    invoke-direct/range {v113 .. v118}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->Magenta:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v113, v0

    const-string v120, "Maroon"

    const/16 v121, 0x6b

    const/high16 v2, -0x800000    # Float.NEGATIVE_INFINITY

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v122

    const/16 v123, 0x6c

    const-string v124, "maroon"

    move-object/from16 v119, v0

    invoke-direct/range {v119 .. v124}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->Maroon:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v114, v0

    const-string v126, "MediumAquamarine"

    const/16 v127, 0x6c

    const v2, -0x993256

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v128

    const/16 v129, 0x6d

    const-string v130, "medAquamarine"

    move-object/from16 v125, v0

    invoke-direct/range {v125 .. v130}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->MediumAquamarine:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v115, v0

    const-string v117, "MediumBlue"

    const/16 v118, 0x6d

    const v2, -0xffff33

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v119

    const/16 v120, 0x6e

    const-string v121, "medBlue"

    move-object/from16 v116, v0

    invoke-direct/range {v116 .. v121}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->MediumBlue:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v116, v0

    const-string v123, "MediumOrchid"

    const/16 v124, 0x6e

    const v2, -0x45aa2d

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v125

    const/16 v126, 0x6f

    const-string v127, "medOrchid"

    move-object/from16 v122, v0

    invoke-direct/range {v122 .. v127}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->MediumOrchid:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v117, v0

    const-string v129, "MediumPurple"

    const/16 v130, 0x6f

    const v2, -0x6c8f25

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v131

    const/16 v132, 0x70

    const-string v133, "medPurple"

    move-object/from16 v128, v0

    invoke-direct/range {v128 .. v133}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->MediumPurple:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v118, v0

    const-string v120, "MediumSeaGreen"

    const/16 v121, 0x70

    const v2, -0xc34c8f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v122

    const/16 v123, 0x71

    const-string v124, "medSeaGreen"

    move-object/from16 v119, v0

    invoke-direct/range {v119 .. v124}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->MediumSeaGreen:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v119, v0

    const-string v126, "MediumSlateBlue"

    const/16 v127, 0x71

    const v2, -0x849712

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v128

    const/16 v129, 0x72

    const-string v130, "medSlateBlue"

    move-object/from16 v125, v0

    invoke-direct/range {v125 .. v130}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->MediumSlateBlue:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v120, v0

    const-string v132, "MediumSpringGreen"

    const/16 v133, 0x72

    const v2, -0xff0566

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v134

    const/16 v135, 0x73

    const-string v136, "medSpringGreen"

    move-object/from16 v131, v0

    invoke-direct/range {v131 .. v136}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->MediumSpringGreen:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v121, v0

    const-string v123, "MediumTurquoise"

    const/16 v124, 0x73

    const v2, -0xb72e34

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v125

    const/16 v126, 0x74

    const-string v127, "medTurquoise"

    move-object/from16 v122, v0

    invoke-direct/range {v122 .. v127}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->MediumTurquoise:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v122, v0

    const-string v129, "MediumVioletRed"

    const/16 v130, 0x74

    const v2, -0x38ea7b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v131

    const/16 v132, 0x75

    const-string v133, "medVioletRed"

    move-object/from16 v128, v0

    invoke-direct/range {v128 .. v133}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->MediumVioletRed:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v123, v0

    const-string v135, "MidnightBlue"

    const/16 v136, 0x75

    const v2, -0xe6e690

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v137

    const/16 v138, 0x76

    const-string v139, "midnightBlue"

    move-object/from16 v134, v0

    invoke-direct/range {v134 .. v139}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->MidnightBlue:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v124, v0

    const-string v126, "MintCream"

    const/16 v127, 0x76

    const v2, -0xa0006

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v128

    const/16 v129, 0x77

    const-string v130, "mintCream"

    move-object/from16 v125, v0

    invoke-direct/range {v125 .. v130}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->MintCream:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v125, v0

    const-string v132, "MistyRose"

    const/16 v133, 0x77

    const/16 v2, -0x1b1f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v134

    const/16 v135, 0x78

    const-string v136, "mistyRose"

    move-object/from16 v131, v0

    invoke-direct/range {v131 .. v136}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->MistyRose:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v126, v0

    const-string v138, "Moccasin"

    const/16 v139, 0x78

    const/16 v2, -0x1b4b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v140

    const/16 v141, 0x79

    const-string v142, "moccasin"

    move-object/from16 v137, v0

    invoke-direct/range {v137 .. v142}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->Moccasin:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v127, v0

    const-string v129, "NavajoWhite"

    const/16 v130, 0x79

    const/16 v2, -0x2153

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v131

    const/16 v132, 0x7a

    const-string v133, "navajoWhite"

    move-object/from16 v128, v0

    invoke-direct/range {v128 .. v133}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->NavajoWhite:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v128, v0

    const-string v135, "Navy"

    const/16 v136, 0x7a

    const v2, -0xffff80

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v137

    const/16 v138, 0x7b

    const-string v139, "navy"

    move-object/from16 v134, v0

    invoke-direct/range {v134 .. v139}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->Navy:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v129, v0

    const-string v141, "OldLace"

    const/16 v142, 0x7b

    const v2, -0x20a1a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v143

    const/16 v144, 0x7c

    const-string v145, "oldLace"

    move-object/from16 v140, v0

    invoke-direct/range {v140 .. v145}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->OldLace:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v130, v0

    const-string v132, "Olive"

    const/16 v133, 0x7c

    const v2, -0x7f8000

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v134

    const/16 v135, 0x7d

    const-string v136, "olive"

    move-object/from16 v131, v0

    invoke-direct/range {v131 .. v136}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->Olive:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v131, v0

    const-string v138, "OliveDrab"

    const/16 v139, 0x7d

    const v2, -0x9471dd

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v140

    const/16 v141, 0x7e

    const-string v142, "oliveDrab"

    move-object/from16 v137, v0

    invoke-direct/range {v137 .. v142}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->OliveDrab:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v132, v0

    const-string v144, "Orange"

    const/16 v145, 0x7e

    const/16 v2, -0x5b00

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v146

    const/16 v147, 0x7f

    const-string v148, "orange"

    move-object/from16 v143, v0

    invoke-direct/range {v143 .. v148}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->Orange:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v133, v0

    const-string v135, "OrangeRed"

    const/16 v136, 0x7f

    const v2, -0xbb00

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v137

    const/16 v138, 0x80

    const-string v139, "orangeRed"

    move-object/from16 v134, v0

    invoke-direct/range {v134 .. v139}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->OrangeRed:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v134, v0

    const-string v141, "Orchid"

    const/16 v142, 0x80

    const v2, -0x258f2a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v143

    const/16 v144, 0x81

    const-string v145, "orchid"

    move-object/from16 v140, v0

    invoke-direct/range {v140 .. v145}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->Orchid:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v135, v0

    const-string v147, "PaleGoldenrod"

    const/16 v148, 0x81

    const v2, -0x111756

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v149

    const/16 v150, 0x82

    const-string v151, "paleGoldenrod"

    move-object/from16 v146, v0

    invoke-direct/range {v146 .. v151}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->PaleGoldenrod:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v136, v0

    const-string v138, "PaleGreen"

    const/16 v139, 0x82

    const v2, -0x670468

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v140

    const/16 v141, 0x83

    const-string v142, "paleGreen"

    move-object/from16 v137, v0

    invoke-direct/range {v137 .. v142}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->PaleGreen:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v137, v0

    const-string v144, "PaleTurquoise"

    const/16 v145, 0x83

    const v2, -0x501112

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v146

    const/16 v147, 0x84

    const-string v148, "paleTurquoise"

    move-object/from16 v143, v0

    invoke-direct/range {v143 .. v148}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->PaleTurquoise:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v138, v0

    const-string v150, "PaleVioletRed"

    const/16 v151, 0x84

    const v2, -0x248f6d

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v152

    const/16 v153, 0x85

    const-string v154, "paleVioletRed"

    move-object/from16 v149, v0

    invoke-direct/range {v149 .. v154}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->PaleVioletRed:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v139, v0

    const-string v141, "PapayaWhip"

    const/16 v142, 0x85

    const/16 v2, -0x102b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v143

    const/16 v144, 0x86

    const-string v145, "papayaWhip"

    move-object/from16 v140, v0

    invoke-direct/range {v140 .. v145}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->PapayaWhip:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v140, v0

    const-string v147, "PeachPuff"

    const/16 v148, 0x86

    const/16 v2, -0x2547

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v149

    const/16 v150, 0x87

    const-string v151, "peachPuff"

    move-object/from16 v146, v0

    invoke-direct/range {v146 .. v151}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->PeachPuff:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v141, v0

    const-string v153, "Peru"

    const/16 v154, 0x87

    const v2, -0x327ac1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v155

    const/16 v156, 0x88

    const-string v157, "peru"

    move-object/from16 v152, v0

    invoke-direct/range {v152 .. v157}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->Peru:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v142, v0

    const-string v144, "Pink"

    const/16 v145, 0x88

    const/16 v2, -0x3f35

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v146

    const/16 v147, 0x89

    const-string v148, "pink"

    move-object/from16 v143, v0

    invoke-direct/range {v143 .. v148}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->Pink:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v143, v0

    const-string v150, "Plum"

    const/16 v151, 0x89

    const v2, -0x225f23

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v152

    const/16 v153, 0x8a

    const-string v154, "plum"

    move-object/from16 v149, v0

    invoke-direct/range {v149 .. v154}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->Plum:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v144, v0

    const-string v156, "PowderBlue"

    const/16 v157, 0x8a

    const v2, -0x4f1f1a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v158

    const/16 v159, 0x8b

    const-string v160, "powderBlue"

    move-object/from16 v155, v0

    invoke-direct/range {v155 .. v160}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->PowderBlue:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v145, v0

    const-string v147, "Purple"

    const/16 v148, 0x8b

    const v2, -0x7fff80

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v149

    const/16 v150, 0x8c

    const-string v151, "purple"

    move-object/from16 v146, v0

    invoke-direct/range {v146 .. v151}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->Purple:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v146, v0

    const-string v153, "Red"

    const/16 v154, 0x8c

    const/high16 v2, -0x10000

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v155

    const/16 v156, 0x8d

    const-string v157, "red"

    move-object/from16 v152, v0

    invoke-direct/range {v152 .. v157}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->Red:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v147, v0

    const-string v159, "RosyBrown"

    const/16 v160, 0x8d

    const v2, -0x437071

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v161

    const/16 v162, 0x8e

    const-string v163, "rosyBrown"

    move-object/from16 v158, v0

    invoke-direct/range {v158 .. v163}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->RosyBrown:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v148, v0

    const-string v150, "RoyalBlue"

    const/16 v151, 0x8e

    const v2, -0xbe961f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v152

    const/16 v153, 0x8f

    const-string v154, "royalBlue"

    move-object/from16 v149, v0

    invoke-direct/range {v149 .. v154}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->RoyalBlue:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v149, v0

    const-string v156, "SaddleBrown"

    const/16 v157, 0x8f

    const v2, -0x74baed

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v158

    const/16 v159, 0x90

    const-string v160, "saddleBrown"

    move-object/from16 v155, v0

    invoke-direct/range {v155 .. v160}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->SaddleBrown:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v150, v0

    const-string v162, "Salmon"

    const/16 v163, 0x90

    const v2, -0x57f8e

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v164

    const/16 v165, 0x91

    const-string v166, "salmon"

    move-object/from16 v161, v0

    invoke-direct/range {v161 .. v166}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->Salmon:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v151, v0

    const-string v153, "SandyBrown"

    const/16 v154, 0x91

    const v2, -0xb5ba0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v155

    const/16 v156, 0x92

    const-string v157, "sandyBrown"

    move-object/from16 v152, v0

    invoke-direct/range {v152 .. v157}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->SandyBrown:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v152, v0

    const-string v159, "SeaGreen"

    const/16 v160, 0x92

    const v2, -0xd174a9

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v161

    const/16 v162, 0x93

    const-string v163, "seaGreen"

    move-object/from16 v158, v0

    invoke-direct/range {v158 .. v163}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->SeaGreen:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v153, v0

    const-string v165, "SeaShell"

    const/16 v166, 0x93

    const/16 v2, -0xa12

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v167

    const/16 v168, 0x94

    const-string v169, "seaShell"

    move-object/from16 v164, v0

    invoke-direct/range {v164 .. v169}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->SeaShell:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v154, v0

    const-string v156, "Sienna"

    const/16 v157, 0x94

    const v2, -0x5fadd3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v158

    const/16 v159, 0x95

    const-string v160, "sienna"

    move-object/from16 v155, v0

    invoke-direct/range {v155 .. v160}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->Sienna:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v155, v0

    const-string v162, "Silver"

    const/16 v163, 0x95

    const v2, -0x3f3f40

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v164

    const/16 v165, 0x96

    const-string v166, "silver"

    move-object/from16 v161, v0

    invoke-direct/range {v161 .. v166}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->Silver:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v156, v0

    const-string v168, "SkyBlue"

    const/16 v169, 0x96

    const v2, -0x783115

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v170

    const/16 v171, 0x97

    const-string v172, "skyBlue"

    move-object/from16 v167, v0

    invoke-direct/range {v167 .. v172}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->SkyBlue:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v157, v0

    const-string v159, "SlateBlue"

    const/16 v160, 0x97

    const v2, -0x95a533

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v161

    const/16 v162, 0x98

    const-string v163, "slateBlue"

    move-object/from16 v158, v0

    invoke-direct/range {v158 .. v163}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->SlateBlue:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v158, v0

    const-string v165, "SlateGray"

    const/16 v166, 0x98

    const v2, -0x8f7f70

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v167

    const/16 v168, 0x99

    const-string v169, "slateGray"

    move-object/from16 v164, v0

    invoke-direct/range {v164 .. v169}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->SlateGray:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v159, v0

    const-string v171, "Snow"

    const/16 v172, 0x99

    const/16 v2, -0x506

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v173

    const/16 v174, 0x9a

    const-string v175, "snow"

    move-object/from16 v170, v0

    invoke-direct/range {v170 .. v175}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->Snow:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v160, v0

    const-string v162, "SpringGreen"

    const/16 v163, 0x9a

    const v2, -0xff0081

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v164

    const/16 v165, 0x9b

    const-string v166, "springGreen"

    move-object/from16 v161, v0

    invoke-direct/range {v161 .. v166}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->SpringGreen:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v161, v0

    const-string v168, "SteelBlue"

    const/16 v169, 0x9b

    const v2, -0xb97d4c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v170

    const/16 v171, 0x9c

    const-string v172, "steelBlue"

    move-object/from16 v167, v0

    invoke-direct/range {v167 .. v172}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->SteelBlue:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v162, v0

    const-string v174, "Tan"

    const/16 v175, 0x9c

    const v2, -0x2d4b74

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v176

    const/16 v177, 0x9d

    const-string v178, "tan"

    move-object/from16 v173, v0

    invoke-direct/range {v173 .. v178}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->Tan:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v163, v0

    const-string v165, "Teal"

    const/16 v166, 0x9d

    const v2, -0xff7f80

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v167

    const/16 v168, 0x9e

    const-string v169, "teal"

    move-object/from16 v164, v0

    invoke-direct/range {v164 .. v169}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->Teal:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v164, v0

    const-string v171, "Thistle"

    const/16 v172, 0x9e

    const v2, -0x274028

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v173

    const/16 v174, 0x9f

    const-string v175, "thistle"

    move-object/from16 v170, v0

    invoke-direct/range {v170 .. v175}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->Thistle:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v165, v0

    const-string v177, "Tomato"

    const/16 v178, 0x9f

    const v2, -0x9cb9

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v179

    const/16 v180, 0xa0

    const-string v181, "tomato"

    move-object/from16 v176, v0

    invoke-direct/range {v176 .. v181}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->Tomato:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v166, v0

    const-string v168, "Turquoise"

    const/16 v169, 0xa0

    const v2, -0xbf1f30

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v170

    const/16 v171, 0xa1

    const-string v172, "turquoise"

    move-object/from16 v167, v0

    invoke-direct/range {v167 .. v172}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->Turquoise:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v167, v0

    const-string v174, "Violet"

    const/16 v175, 0xa1

    const v2, -0x117d12

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v176

    const/16 v177, 0xa2

    const-string v178, "violet"

    move-object/from16 v173, v0

    invoke-direct/range {v173 .. v178}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->Violet:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v168, v0

    const-string v188, "Wheat"

    const/16 v189, 0xa2

    const v2, -0xa214d

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v190

    const/16 v191, 0xa3

    const-string v192, "wheat"

    move-object/from16 v187, v0

    invoke-direct/range {v187 .. v192}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->Wheat:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v169, v0

    const-string v171, "White"

    const/16 v172, 0xa3

    const/16 v174, 0xa4

    const-string v175, "white"

    move-object/from16 v170, v0

    move-object/from16 v173, v1

    invoke-direct/range {v170 .. v175}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->White:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v170, v0

    const-string v177, "WhiteSmoke"

    const/16 v178, 0xa4

    const v2, -0xa0a0b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v179

    const/16 v180, 0xa5

    const-string v181, "whiteSmoke"

    move-object/from16 v176, v0

    invoke-direct/range {v176 .. v181}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->WhiteSmoke:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v171, v0

    const-string v188, "Yellow"

    const/16 v189, 0xa5

    const/16 v2, -0x100

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v190

    const/16 v191, 0xa6

    const-string v192, "yellow"

    move-object/from16 v187, v0

    invoke-direct/range {v187 .. v192}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->Yellow:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v172, v0

    const-string v174, "YellowGreen"

    const/16 v175, 0xa6

    const v2, -0x6532ce

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v176

    const/16 v177, 0xa7

    const-string v178, "yellowGreen"

    move-object/from16 v173, v0

    invoke-direct/range {v173 .. v178}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->YellowGreen:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v173, v0

    const-string v175, "ButtonFace"

    const/16 v176, 0xa7

    const/16 v178, 0xa8

    const/16 v179, 0x0

    move-object/from16 v174, v0

    move-object/from16 v177, v182

    invoke-direct/range {v174 .. v179}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->ButtonFace:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v174, v0

    const-string v176, "ButtonHighlight"

    const/16 v177, 0xa8

    const/16 v179, 0xa9

    const/16 v180, 0x0

    move-object/from16 v175, v0

    move-object/from16 v178, v1

    invoke-direct/range {v175 .. v180}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->ButtonHighlight:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v175, v0

    const-string v188, "ButtonShadow"

    const/16 v189, 0xa9

    const v1, -0x5f5f60

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v190

    const/16 v191, 0xaa

    const/16 v192, 0x0

    move-object/from16 v187, v0

    invoke-direct/range {v187 .. v192}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->ButtonShadow:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v176, v0

    const-string v194, "GradientActiveCaption"

    const/16 v195, 0xaa

    const v1, -0x462e16

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v196

    const/16 v197, 0xab

    const-string v198, "gradientActiveCaption"

    move-object/from16 v193, v0

    invoke-direct/range {v193 .. v198}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->GradientActiveCaption:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v177, v0

    const-string v188, "GradientInactiveCaption"

    const/16 v189, 0xab

    const v1, -0x281b0e

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v190

    const/16 v191, 0xac

    const-string v192, "gradientInactiveCaption"

    move-object/from16 v187, v0

    invoke-direct/range {v187 .. v192}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->GradientInactiveCaption:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v178, v0

    const-string v180, "MenuBar"

    const/16 v181, 0xac

    const/16 v183, 0xad

    const-string v184, "menuBar"

    move-object/from16 v179, v0

    invoke-direct/range {v179 .. v184}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->MenuBar:Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    move-object/from16 v179, v0

    const-string v184, "MenuHighlight"

    const/16 v185, 0xad

    const/16 v187, 0xae

    const-string v188, "menuHighlight"

    move-object/from16 v183, v0

    invoke-direct/range {v183 .. v188}, Lorg/apache/poi/sl/usermodel/PresetColor;-><init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->MenuHighlight:Lorg/apache/poi/sl/usermodel/PresetColor;

    filled-new-array/range {v6 .. v179}, [Lorg/apache/poi/sl/usermodel/PresetColor;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->$VALUES:[Lorg/apache/poi/sl/usermodel/PresetColor;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->lookupOoxmlId:Ljava/util/Map;

    invoke-static {}, Lorg/apache/poi/sl/usermodel/PresetColor;->values()[Lorg/apache/poi/sl/usermodel/PresetColor;

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_1

    aget-object v3, v0, v2

    iget-object v4, v3, Lorg/apache/poi/sl/usermodel/PresetColor;->ooxmlId:Ljava/lang/String;

    if-eqz v4, :cond_0

    sget-object v5, Lorg/apache/poi/sl/usermodel/PresetColor;->lookupOoxmlId:Ljava/util/Map;

    invoke-interface {v5, v4, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/Integer;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Integer;",
            "I",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    if-nez p3, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    new-instance p1, Ljava/awt/Color;

    invoke-virtual {p3}, Ljava/lang/Integer;->intValue()I

    move-result p2

    const/4 p3, 0x1

    invoke-direct {p1, p2, p3}, Ljava/awt/Color;-><init>(IZ)V

    :goto_0
    iput-object p1, p0, Lorg/apache/poi/sl/usermodel/PresetColor;->color:Ljava/awt/Color;

    iput p4, p0, Lorg/apache/poi/sl/usermodel/PresetColor;->nativeId:I

    iput-object p5, p0, Lorg/apache/poi/sl/usermodel/PresetColor;->ooxmlId:Ljava/lang/String;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lorg/apache/poi/sl/usermodel/PresetColor;
    .locals 1

    const-class v0, Lorg/apache/poi/sl/usermodel/PresetColor;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lorg/apache/poi/sl/usermodel/PresetColor;

    return-object p0
.end method

.method public static valueOfNativeId(I)Lorg/apache/poi/sl/usermodel/PresetColor;
    .locals 2

    invoke-static {}, Lorg/apache/poi/sl/usermodel/PresetColor;->values()[Lorg/apache/poi/sl/usermodel/PresetColor;

    move-result-object v0

    if-lez p0, :cond_0

    array-length v1, v0

    if-gt p0, v1, :cond_0

    add-int/lit8 p0, p0, -0x1

    aget-object p0, v0, p0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return-object p0
.end method

.method public static valueOfOoxmlId(Ljava/lang/String;)Lorg/apache/poi/sl/usermodel/PresetColor;
    .locals 1

    sget-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->lookupOoxmlId:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lorg/apache/poi/sl/usermodel/PresetColor;

    return-object p0
.end method

.method public static values()[Lorg/apache/poi/sl/usermodel/PresetColor;
    .locals 1

    sget-object v0, Lorg/apache/poi/sl/usermodel/PresetColor;->$VALUES:[Lorg/apache/poi/sl/usermodel/PresetColor;

    invoke-virtual {v0}, [Lorg/apache/poi/sl/usermodel/PresetColor;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lorg/apache/poi/sl/usermodel/PresetColor;

    return-object v0
.end method

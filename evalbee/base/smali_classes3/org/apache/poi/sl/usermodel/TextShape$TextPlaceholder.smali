.class public final enum Lorg/apache/poi/sl/usermodel/TextShape$TextPlaceholder;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/poi/sl/usermodel/TextShape;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "TextPlaceholder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lorg/apache/poi/sl/usermodel/TextShape$TextPlaceholder;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lorg/apache/poi/sl/usermodel/TextShape$TextPlaceholder;

.field public static final enum BODY:Lorg/apache/poi/sl/usermodel/TextShape$TextPlaceholder;

.field public static final enum CENTER_BODY:Lorg/apache/poi/sl/usermodel/TextShape$TextPlaceholder;

.field public static final enum CENTER_TITLE:Lorg/apache/poi/sl/usermodel/TextShape$TextPlaceholder;

.field public static final enum HALF_BODY:Lorg/apache/poi/sl/usermodel/TextShape$TextPlaceholder;

.field public static final enum NOTES:Lorg/apache/poi/sl/usermodel/TextShape$TextPlaceholder;

.field public static final enum OTHER:Lorg/apache/poi/sl/usermodel/TextShape$TextPlaceholder;

.field public static final enum QUARTER_BODY:Lorg/apache/poi/sl/usermodel/TextShape$TextPlaceholder;

.field public static final enum TITLE:Lorg/apache/poi/sl/usermodel/TextShape$TextPlaceholder;


# direct methods
.method public static constructor <clinit>()V
    .locals 10

    new-instance v0, Lorg/apache/poi/sl/usermodel/TextShape$TextPlaceholder;

    const-string v1, "TITLE"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/sl/usermodel/TextShape$TextPlaceholder;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/sl/usermodel/TextShape$TextPlaceholder;->TITLE:Lorg/apache/poi/sl/usermodel/TextShape$TextPlaceholder;

    new-instance v1, Lorg/apache/poi/sl/usermodel/TextShape$TextPlaceholder;

    const-string v2, "BODY"

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Lorg/apache/poi/sl/usermodel/TextShape$TextPlaceholder;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lorg/apache/poi/sl/usermodel/TextShape$TextPlaceholder;->BODY:Lorg/apache/poi/sl/usermodel/TextShape$TextPlaceholder;

    new-instance v2, Lorg/apache/poi/sl/usermodel/TextShape$TextPlaceholder;

    const-string v3, "CENTER_TITLE"

    const/4 v4, 0x2

    invoke-direct {v2, v3, v4}, Lorg/apache/poi/sl/usermodel/TextShape$TextPlaceholder;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lorg/apache/poi/sl/usermodel/TextShape$TextPlaceholder;->CENTER_TITLE:Lorg/apache/poi/sl/usermodel/TextShape$TextPlaceholder;

    new-instance v3, Lorg/apache/poi/sl/usermodel/TextShape$TextPlaceholder;

    const-string v4, "CENTER_BODY"

    const/4 v5, 0x3

    invoke-direct {v3, v4, v5}, Lorg/apache/poi/sl/usermodel/TextShape$TextPlaceholder;-><init>(Ljava/lang/String;I)V

    sput-object v3, Lorg/apache/poi/sl/usermodel/TextShape$TextPlaceholder;->CENTER_BODY:Lorg/apache/poi/sl/usermodel/TextShape$TextPlaceholder;

    new-instance v4, Lorg/apache/poi/sl/usermodel/TextShape$TextPlaceholder;

    const-string v5, "HALF_BODY"

    const/4 v6, 0x4

    invoke-direct {v4, v5, v6}, Lorg/apache/poi/sl/usermodel/TextShape$TextPlaceholder;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lorg/apache/poi/sl/usermodel/TextShape$TextPlaceholder;->HALF_BODY:Lorg/apache/poi/sl/usermodel/TextShape$TextPlaceholder;

    new-instance v5, Lorg/apache/poi/sl/usermodel/TextShape$TextPlaceholder;

    const-string v6, "QUARTER_BODY"

    const/4 v7, 0x5

    invoke-direct {v5, v6, v7}, Lorg/apache/poi/sl/usermodel/TextShape$TextPlaceholder;-><init>(Ljava/lang/String;I)V

    sput-object v5, Lorg/apache/poi/sl/usermodel/TextShape$TextPlaceholder;->QUARTER_BODY:Lorg/apache/poi/sl/usermodel/TextShape$TextPlaceholder;

    new-instance v6, Lorg/apache/poi/sl/usermodel/TextShape$TextPlaceholder;

    const-string v7, "NOTES"

    const/4 v8, 0x6

    invoke-direct {v6, v7, v8}, Lorg/apache/poi/sl/usermodel/TextShape$TextPlaceholder;-><init>(Ljava/lang/String;I)V

    sput-object v6, Lorg/apache/poi/sl/usermodel/TextShape$TextPlaceholder;->NOTES:Lorg/apache/poi/sl/usermodel/TextShape$TextPlaceholder;

    new-instance v7, Lorg/apache/poi/sl/usermodel/TextShape$TextPlaceholder;

    const-string v8, "OTHER"

    const/4 v9, 0x7

    invoke-direct {v7, v8, v9}, Lorg/apache/poi/sl/usermodel/TextShape$TextPlaceholder;-><init>(Ljava/lang/String;I)V

    sput-object v7, Lorg/apache/poi/sl/usermodel/TextShape$TextPlaceholder;->OTHER:Lorg/apache/poi/sl/usermodel/TextShape$TextPlaceholder;

    filled-new-array/range {v0 .. v7}, [Lorg/apache/poi/sl/usermodel/TextShape$TextPlaceholder;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/sl/usermodel/TextShape$TextPlaceholder;->$VALUES:[Lorg/apache/poi/sl/usermodel/TextShape$TextPlaceholder;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lorg/apache/poi/sl/usermodel/TextShape$TextPlaceholder;
    .locals 1

    const-class v0, Lorg/apache/poi/sl/usermodel/TextShape$TextPlaceholder;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lorg/apache/poi/sl/usermodel/TextShape$TextPlaceholder;

    return-object p0
.end method

.method public static values()[Lorg/apache/poi/sl/usermodel/TextShape$TextPlaceholder;
    .locals 1

    sget-object v0, Lorg/apache/poi/sl/usermodel/TextShape$TextPlaceholder;->$VALUES:[Lorg/apache/poi/sl/usermodel/TextShape$TextPlaceholder;

    invoke-virtual {v0}, [Lorg/apache/poi/sl/usermodel/TextShape$TextPlaceholder;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lorg/apache/poi/sl/usermodel/TextShape$TextPlaceholder;

    return-object v0
.end method

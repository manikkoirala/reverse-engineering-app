.class public interface abstract Lorg/apache/poi/sl/usermodel/LineDecoration;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/poi/sl/usermodel/LineDecoration$DecorationSize;,
        Lorg/apache/poi/sl/usermodel/LineDecoration$DecorationShape;
    }
.end annotation


# virtual methods
.method public abstract getHeadLength()Lorg/apache/poi/sl/usermodel/LineDecoration$DecorationSize;
.end method

.method public abstract getHeadShape()Lorg/apache/poi/sl/usermodel/LineDecoration$DecorationShape;
.end method

.method public abstract getHeadWidth()Lorg/apache/poi/sl/usermodel/LineDecoration$DecorationSize;
.end method

.method public abstract getTailLength()Lorg/apache/poi/sl/usermodel/LineDecoration$DecorationSize;
.end method

.method public abstract getTailShape()Lorg/apache/poi/sl/usermodel/LineDecoration$DecorationShape;
.end method

.method public abstract getTailWidth()Lorg/apache/poi/sl/usermodel/LineDecoration$DecorationSize;
.end method

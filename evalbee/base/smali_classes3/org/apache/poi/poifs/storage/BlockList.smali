.class public interface abstract Lorg/apache/poi/poifs/storage/BlockList;
.super Ljava/lang/Object;
.source "SourceFile"


# virtual methods
.method public abstract blockCount()I
.end method

.method public abstract fetchBlocks(II)[Lorg/apache/poi/poifs/storage/ListManagedBlock;
.end method

.method public abstract remove(I)Lorg/apache/poi/poifs/storage/ListManagedBlock;
.end method

.method public abstract setBAT(Lorg/apache/poi/poifs/storage/BlockAllocationTableReader;)V
.end method

.method public abstract zap(I)V
.end method

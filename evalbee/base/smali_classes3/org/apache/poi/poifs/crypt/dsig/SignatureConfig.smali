.class public Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig$SignatureConfigurable;
    }
.end annotation


# static fields
.field private static final LOG:Lorg/apache/poi/util/POILogger;


# instance fields
.field private canonicalizationMethod:Ljava/lang/String;

.field private digestAlgo:Lorg/apache/poi/poifs/crypt/HashAlgorithm;

.field private executionTime:Ljava/util/Date;

.field private includeEntireCertificateChain:Z

.field private includeIssuerSerial:Z

.field private includeKeyValue:Z

.field private key:Ljava/security/PrivateKey;

.field private keyInfoFactory:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal<",
            "Ljavax/xml/crypto/dsig/keyinfo/KeyInfoFactory;",
            ">;"
        }
    .end annotation
.end field

.field namespacePrefixes:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private opcPackage:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal<",
            "Lorg/apache/poi/openxml4j/opc/OPCPackage;",
            ">;"
        }
    .end annotation
.end field

.field private packageSignatureId:Ljava/lang/String;

.field private provider:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal<",
            "Ljava/security/Provider;",
            ">;"
        }
    .end annotation
.end field

.field private proxyUrl:Ljava/lang/String;

.field private revocationDataService:Lorg/apache/poi/poifs/crypt/dsig/services/RevocationDataService;

.field private signatureDescription:Ljava/lang/String;

.field private signatureFacets:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lorg/apache/poi/poifs/crypt/dsig/facets/SignatureFacet;",
            ">;"
        }
    .end annotation
.end field

.field private signatureFactory:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal<",
            "Ljavax/xml/crypto/dsig/XMLSignatureFactory;",
            ">;"
        }
    .end annotation
.end field

.field signatureMarshalListener:Lorg/w3c/dom/events/EventListener;

.field private signaturePolicyService:Lorg/apache/poi/poifs/crypt/dsig/services/SignaturePolicyService;

.field private signingCertificateChain:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/security/cert/X509Certificate;",
            ">;"
        }
    .end annotation
.end field

.field private tspDigestAlgo:Lorg/apache/poi/poifs/crypt/HashAlgorithm;

.field private tspOldProtocol:Z

.field private tspPass:Ljava/lang/String;

.field private tspRequestPolicy:Ljava/lang/String;

.field private tspService:Lorg/apache/poi/poifs/crypt/dsig/services/TimeStampService;

.field private tspUrl:Ljava/lang/String;

.field private tspUser:Ljava/lang/String;

.field private tspValidator:Lorg/apache/poi/poifs/crypt/dsig/services/TimeStampServiceValidator;

.field private uriDereferencer:Ljavax/xml/crypto/URIDereferencer;

.field private userAgent:Ljava/lang/String;

.field private xadesCanonicalizationMethod:Ljava/lang/String;

.field private xadesDigestAlgo:Lorg/apache/poi/poifs/crypt/HashAlgorithm;

.field private xadesIssuerNameNoReverseOrder:Z

.field private xadesRole:Ljava/lang/String;

.field private xadesSignatureId:Ljava/lang/String;

.field private xadesSignaturePolicyImplied:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    const-class v0, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;

    invoke-static {v0}, Lorg/apache/poi/util/POILogFactory;->getLogger(Ljava/lang/Class;)Lorg/apache/poi/util/POILogger;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->LOG:Lorg/apache/poi/util/POILogger;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/ThreadLocal;

    invoke-direct {v0}, Ljava/lang/ThreadLocal;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->opcPackage:Ljava/lang/ThreadLocal;

    new-instance v0, Ljava/lang/ThreadLocal;

    invoke-direct {v0}, Ljava/lang/ThreadLocal;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->signatureFactory:Ljava/lang/ThreadLocal;

    new-instance v0, Ljava/lang/ThreadLocal;

    invoke-direct {v0}, Ljava/lang/ThreadLocal;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->keyInfoFactory:Ljava/lang/ThreadLocal;

    new-instance v0, Ljava/lang/ThreadLocal;

    invoke-direct {v0}, Ljava/lang/ThreadLocal;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->provider:Ljava/lang/ThreadLocal;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->signatureFacets:Ljava/util/List;

    sget-object v0, Lorg/apache/poi/poifs/crypt/HashAlgorithm;->sha1:Lorg/apache/poi/poifs/crypt/HashAlgorithm;

    iput-object v0, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->digestAlgo:Lorg/apache/poi/poifs/crypt/HashAlgorithm;

    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->executionTime:Ljava/util/Date;

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->uriDereferencer:Ljavax/xml/crypto/URIDereferencer;

    const-string v1, "http://www.w3.org/TR/2001/REC-xml-c14n-20010315"

    iput-object v1, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->canonicalizationMethod:Ljava/lang/String;

    const/4 v1, 0x1

    iput-boolean v1, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->includeEntireCertificateChain:Z

    const/4 v2, 0x0

    iput-boolean v2, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->includeIssuerSerial:Z

    iput-boolean v2, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->includeKeyValue:Z

    new-instance v3, Lorg/apache/poi/poifs/crypt/dsig/services/TSPTimeStampService;

    invoke-direct {v3}, Lorg/apache/poi/poifs/crypt/dsig/services/TSPTimeStampService;-><init>()V

    iput-object v3, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->tspService:Lorg/apache/poi/poifs/crypt/dsig/services/TimeStampService;

    iput-boolean v2, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->tspOldProtocol:Z

    iput-object v0, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->tspDigestAlgo:Lorg/apache/poi/poifs/crypt/HashAlgorithm;

    const-string v2, "1.3.6.1.4.1.13762.3"

    iput-object v2, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->tspRequestPolicy:Ljava/lang/String;

    const-string v2, "POI XmlSign Service TSP Client"

    iput-object v2, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->userAgent:Ljava/lang/String;

    iput-object v0, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->xadesDigestAlgo:Lorg/apache/poi/poifs/crypt/HashAlgorithm;

    iput-object v0, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->xadesRole:Ljava/lang/String;

    const-string v2, "idSignedProperties"

    iput-object v2, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->xadesSignatureId:Ljava/lang/String;

    iput-boolean v1, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->xadesSignaturePolicyImplied:Z

    const-string v2, "http://www.w3.org/2001/10/xml-exc-c14n#"

    iput-object v2, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->xadesCanonicalizationMethod:Ljava/lang/String;

    iput-boolean v1, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->xadesIssuerNameNoReverseOrder:Z

    const-string v1, "idPackageSignature"

    iput-object v1, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->packageSignatureId:Ljava/lang/String;

    const-string v1, "Office OpenXML Document"

    iput-object v1, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->signatureDescription:Ljava/lang/String;

    iput-object v0, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->signatureMarshalListener:Lorg/w3c/dom/events/EventListener;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->namespacePrefixes:Ljava/util/Map;

    return-void
.end method

.method public static getDigestMethodUri(Lorg/apache/poi/poifs/crypt/HashAlgorithm;)Ljava/lang/String;
    .locals 3

    .line 2
    sget-object v0, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig$1;->$SwitchMap$org$apache$poi$poifs$crypt$HashAlgorithm:[I

    invoke-virtual {p0}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_5

    const/4 v1, 0x2

    if-eq v0, v1, :cond_4

    const/4 v1, 0x3

    if-eq v0, v1, :cond_3

    const/4 v1, 0x4

    if-eq v0, v1, :cond_2

    const/4 v1, 0x5

    if-eq v0, v1, :cond_1

    const/4 v1, 0x7

    if-ne v0, v1, :cond_0

    const-string p0, "http://www.w3.org/2001/04/xmlenc#ripemd160"

    return-object p0

    :cond_0
    new-instance v0, Lorg/apache/poi/EncryptedDocumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Hash algorithm "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p0, " not supported for signing."

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Lorg/apache/poi/EncryptedDocumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    const-string p0, "http://www.w3.org/2001/04/xmlenc#sha512"

    return-object p0

    :cond_2
    const-string p0, "http://www.w3.org/2001/04/xmldsig-more#sha384"

    return-object p0

    :cond_3
    const-string p0, "http://www.w3.org/2001/04/xmlenc#sha256"

    return-object p0

    :cond_4
    const-string p0, "http://www.w3.org/2001/04/xmldsig-more#sha224"

    return-object p0

    :cond_5
    const-string p0, "http://www.w3.org/2000/09/xmldsig#sha1"

    return-object p0
.end method

.method public static nvl(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;TT;)TT;"
        }
    .end annotation

    if-nez p0, :cond_0

    move-object p0, p1

    :cond_0
    return-object p0
.end method


# virtual methods
.method public addSignatureFacet(Lorg/apache/poi/poifs/crypt/dsig/facets/SignatureFacet;)V
    .locals 1

    iget-object v0, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->signatureFacets:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public getCanonicalizationMethod()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->canonicalizationMethod:Ljava/lang/String;

    return-object v0
.end method

.method public getDigestAlgo()Lorg/apache/poi/poifs/crypt/HashAlgorithm;
    .locals 1

    iget-object v0, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->digestAlgo:Lorg/apache/poi/poifs/crypt/HashAlgorithm;

    return-object v0
.end method

.method public getDigestMethodUri()Ljava/lang/String;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->getDigestAlgo()Lorg/apache/poi/poifs/crypt/HashAlgorithm;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->getDigestMethodUri(Lorg/apache/poi/poifs/crypt/HashAlgorithm;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getExecutionTime()Ljava/util/Date;
    .locals 1

    iget-object v0, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->executionTime:Ljava/util/Date;

    return-object v0
.end method

.method public getHashMagic()[B
    .locals 3

    sget-object v0, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig$1;->$SwitchMap$org$apache$poi$poifs$crypt$HashAlgorithm:[I

    invoke-virtual {p0}, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->getDigestAlgo()Lorg/apache/poi/poifs/crypt/HashAlgorithm;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/16 v1, 0xd

    const/16 v2, 0x11

    packed-switch v0, :pswitch_data_0

    new-instance v0, Lorg/apache/poi/EncryptedDocumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Hash algorithm "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->getDigestAlgo()Lorg/apache/poi/poifs/crypt/HashAlgorithm;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v2, " not supported for signing."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/poi/EncryptedDocumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    new-array v0, v1, [B

    fill-array-data v0, :array_0

    goto :goto_0

    :pswitch_1
    new-array v0, v1, [B

    fill-array-data v0, :array_1

    goto :goto_0

    :pswitch_2
    new-array v0, v2, [B

    fill-array-data v0, :array_2

    goto :goto_0

    :pswitch_3
    new-array v0, v2, [B

    fill-array-data v0, :array_3

    goto :goto_0

    :pswitch_4
    new-array v0, v2, [B

    fill-array-data v0, :array_4

    goto :goto_0

    :pswitch_5
    new-array v0, v2, [B

    fill-array-data v0, :array_5

    goto :goto_0

    :pswitch_6
    new-array v0, v1, [B

    fill-array-data v0, :array_6

    :goto_0
    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :array_0
    .array-data 1
        0x30t
        0x1ft
        0x30t
        0x7t
        0x6t
        0x5t
        0x2bt
        0x24t
        0x3t
        0x2t
        0x1t
        0x4t
        0x14t
    .end array-data

    nop

    :array_1
    .array-data 1
        0x30t
        0x1bt
        0x30t
        0x7t
        0x6t
        0x5t
        0x2bt
        0x24t
        0x3t
        0x2t
        0x2t
        0x4t
        0x10t
    .end array-data

    nop

    :array_2
    .array-data 1
        0x30t
        0x4ft
        0x30t
        0xbt
        0x6t
        0x9t
        0x60t
        -0x7at
        0x48t
        0x1t
        0x65t
        0x3t
        0x4t
        0x2t
        0x3t
        0x4t
        0x40t
    .end array-data

    nop

    :array_3
    .array-data 1
        0x30t
        0x3ft
        0x30t
        0xbt
        0x6t
        0x9t
        0x60t
        -0x7at
        0x48t
        0x1t
        0x65t
        0x3t
        0x4t
        0x2t
        0x2t
        0x4t
        0x30t
    .end array-data

    nop

    :array_4
    .array-data 1
        0x30t
        0x2ft
        0x30t
        0xbt
        0x6t
        0x9t
        0x60t
        -0x7at
        0x48t
        0x1t
        0x65t
        0x3t
        0x4t
        0x2t
        0x1t
        0x4t
        0x20t
    .end array-data

    nop

    :array_5
    .array-data 1
        0x30t
        0x2bt
        0x30t
        0xbt
        0x6t
        0x9t
        0x60t
        -0x7at
        0x48t
        0x1t
        0x65t
        0x3t
        0x4t
        0x2t
        0x4t
        0x4t
        0x1ct
    .end array-data

    nop

    :array_6
    .array-data 1
        0x30t
        0x1ft
        0x30t
        0x7t
        0x6t
        0x5t
        0x2bt
        0xet
        0x3t
        0x2t
        0x1at
        0x4t
        0x14t
    .end array-data
.end method

.method public getKey()Ljava/security/PrivateKey;
    .locals 1

    iget-object v0, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->key:Ljava/security/PrivateKey;

    return-object v0
.end method

.method public getKeyInfoFactory()Ljavax/xml/crypto/dsig/keyinfo/KeyInfoFactory;
    .locals 2

    iget-object v0, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->keyInfoFactory:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljavax/xml/crypto/dsig/keyinfo/KeyInfoFactory;

    if-nez v0, :cond_0

    const-string v0, "DOM"

    invoke-virtual {p0}, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->getProvider()Ljava/security/Provider;

    move-result-object v1

    invoke-static {v0, v1}, Ljavax/xml/crypto/dsig/keyinfo/KeyInfoFactory;->getInstance(Ljava/lang/String;Ljava/security/Provider;)Ljavax/xml/crypto/dsig/keyinfo/KeyInfoFactory;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->setKeyInfoFactory(Ljavax/xml/crypto/dsig/keyinfo/KeyInfoFactory;)V

    :cond_0
    return-object v0
.end method

.method public getNamespacePrefixes()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->namespacePrefixes:Ljava/util/Map;

    return-object v0
.end method

.method public getOpcPackage()Lorg/apache/poi/openxml4j/opc/OPCPackage;
    .locals 1

    iget-object v0, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->opcPackage:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/openxml4j/opc/OPCPackage;

    return-object v0
.end method

.method public getPackageSignatureId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->packageSignatureId:Ljava/lang/String;

    return-object v0
.end method

.method public getProvider()Ljava/security/Provider;
    .locals 7

    iget-object v0, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->provider:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/security/Provider;

    if-nez v0, :cond_1

    const-string v1, "jsr105Provider"

    invoke-static {v1}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "org.apache.jcp.xml.dsig.internal.dom.XMLDSigRI"

    const-string v3, "org.jcp.xml.dsig.internal.dom.XMLDSigRI"

    filled-new-array {v1, v2, v3}, [Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    :goto_0
    const/4 v3, 0x3

    if-ge v2, v3, :cond_1

    aget-object v3, v1, v2

    if-nez v3, :cond_0

    goto :goto_1

    :cond_0
    :try_start_0
    invoke-static {v3}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/security/Provider;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v4

    goto :goto_2

    :catch_0
    sget-object v4, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->LOG:Lorg/apache/poi/util/POILogger;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "XMLDsig-Provider \'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "\' can\'t be found - trying next."

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    filled-new-array {v3}, [Ljava/lang/Object;

    move-result-object v3

    const/4 v5, 0x1

    invoke-virtual {v4, v5, v3}, Lorg/apache/poi/util/POILogger;->log(I[Ljava/lang/Object;)V

    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    :goto_2
    if-eqz v0, :cond_2

    return-object v0

    :cond_2
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "JRE doesn\'t support default xml signature provider - set jsr105Provider system property!"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getProxyUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->proxyUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getRevocationDataService()Lorg/apache/poi/poifs/crypt/dsig/services/RevocationDataService;
    .locals 1

    iget-object v0, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->revocationDataService:Lorg/apache/poi/poifs/crypt/dsig/services/RevocationDataService;

    return-object v0
.end method

.method public getSignatureDescription()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->signatureDescription:Ljava/lang/String;

    return-object v0
.end method

.method public getSignatureFacets()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lorg/apache/poi/poifs/crypt/dsig/facets/SignatureFacet;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->signatureFacets:Ljava/util/List;

    return-object v0
.end method

.method public getSignatureFactory()Ljavax/xml/crypto/dsig/XMLSignatureFactory;
    .locals 2

    iget-object v0, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->signatureFactory:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljavax/xml/crypto/dsig/XMLSignatureFactory;

    if-nez v0, :cond_0

    const-string v0, "DOM"

    invoke-virtual {p0}, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->getProvider()Ljava/security/Provider;

    move-result-object v1

    invoke-static {v0, v1}, Ljavax/xml/crypto/dsig/XMLSignatureFactory;->getInstance(Ljava/lang/String;Ljava/security/Provider;)Ljavax/xml/crypto/dsig/XMLSignatureFactory;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->setSignatureFactory(Ljavax/xml/crypto/dsig/XMLSignatureFactory;)V

    :cond_0
    return-object v0
.end method

.method public getSignatureMarshalListener()Lorg/w3c/dom/events/EventListener;
    .locals 1

    iget-object v0, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->signatureMarshalListener:Lorg/w3c/dom/events/EventListener;

    return-object v0
.end method

.method public getSignatureMethodUri()Ljava/lang/String;
    .locals 3

    sget-object v0, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig$1;->$SwitchMap$org$apache$poi$poifs$crypt$HashAlgorithm:[I

    invoke-virtual {p0}, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->getDigestAlgo()Lorg/apache/poi/poifs/crypt/HashAlgorithm;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_5

    const/4 v1, 0x2

    if-eq v0, v1, :cond_4

    const/4 v1, 0x3

    if-eq v0, v1, :cond_3

    const/4 v1, 0x4

    if-eq v0, v1, :cond_2

    const/4 v1, 0x5

    if-eq v0, v1, :cond_1

    const/4 v1, 0x7

    if-ne v0, v1, :cond_0

    const-string v0, "http://www.w3.org/2001/04/xmldsig-more#rsa-ripemd160"

    return-object v0

    :cond_0
    new-instance v0, Lorg/apache/poi/EncryptedDocumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Hash algorithm "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->getDigestAlgo()Lorg/apache/poi/poifs/crypt/HashAlgorithm;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v2, " not supported for signing."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/poi/EncryptedDocumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    const-string v0, "http://www.w3.org/2001/04/xmldsig-more#rsa-sha512"

    return-object v0

    :cond_2
    const-string v0, "http://www.w3.org/2001/04/xmldsig-more#rsa-sha384"

    return-object v0

    :cond_3
    const-string v0, "http://www.w3.org/2001/04/xmldsig-more#rsa-sha256"

    return-object v0

    :cond_4
    const-string v0, "http://www.w3.org/2001/04/xmldsig-more#rsa-sha224"

    return-object v0

    :cond_5
    const-string v0, "http://www.w3.org/2000/09/xmldsig#rsa-sha1"

    return-object v0
.end method

.method public getSignaturePolicyService()Lorg/apache/poi/poifs/crypt/dsig/services/SignaturePolicyService;
    .locals 1

    iget-object v0, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->signaturePolicyService:Lorg/apache/poi/poifs/crypt/dsig/services/SignaturePolicyService;

    return-object v0
.end method

.method public getSigningCertificateChain()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/security/cert/X509Certificate;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->signingCertificateChain:Ljava/util/List;

    return-object v0
.end method

.method public getTspDigestAlgo()Lorg/apache/poi/poifs/crypt/HashAlgorithm;
    .locals 2

    iget-object v0, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->tspDigestAlgo:Lorg/apache/poi/poifs/crypt/HashAlgorithm;

    iget-object v1, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->digestAlgo:Lorg/apache/poi/poifs/crypt/HashAlgorithm;

    invoke-static {v0, v1}, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->nvl(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/poifs/crypt/HashAlgorithm;

    return-object v0
.end method

.method public getTspPass()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->tspPass:Ljava/lang/String;

    return-object v0
.end method

.method public getTspRequestPolicy()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->tspRequestPolicy:Ljava/lang/String;

    return-object v0
.end method

.method public getTspService()Lorg/apache/poi/poifs/crypt/dsig/services/TimeStampService;
    .locals 1

    iget-object v0, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->tspService:Lorg/apache/poi/poifs/crypt/dsig/services/TimeStampService;

    return-object v0
.end method

.method public getTspUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->tspUrl:Ljava/lang/String;

    return-object v0
.end method

.method public getTspUser()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->tspUser:Ljava/lang/String;

    return-object v0
.end method

.method public getTspValidator()Lorg/apache/poi/poifs/crypt/dsig/services/TimeStampServiceValidator;
    .locals 1

    iget-object v0, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->tspValidator:Lorg/apache/poi/poifs/crypt/dsig/services/TimeStampServiceValidator;

    return-object v0
.end method

.method public getUriDereferencer()Ljavax/xml/crypto/URIDereferencer;
    .locals 1

    iget-object v0, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->uriDereferencer:Ljavax/xml/crypto/URIDereferencer;

    return-object v0
.end method

.method public getUserAgent()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->userAgent:Ljava/lang/String;

    return-object v0
.end method

.method public getXadesCanonicalizationMethod()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->xadesCanonicalizationMethod:Ljava/lang/String;

    return-object v0
.end method

.method public getXadesDigestAlgo()Lorg/apache/poi/poifs/crypt/HashAlgorithm;
    .locals 2

    iget-object v0, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->xadesDigestAlgo:Lorg/apache/poi/poifs/crypt/HashAlgorithm;

    iget-object v1, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->digestAlgo:Lorg/apache/poi/poifs/crypt/HashAlgorithm;

    invoke-static {v0, v1}, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->nvl(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/poifs/crypt/HashAlgorithm;

    return-object v0
.end method

.method public getXadesRole()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->xadesRole:Ljava/lang/String;

    return-object v0
.end method

.method public getXadesSignatureId()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->xadesSignatureId:Ljava/lang/String;

    const-string v1, "idSignedProperties"

    invoke-static {v0, v1}, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->nvl(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public init(Z)V
    .locals 3

    iget-object v0, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->opcPackage:Ljava/lang/ThreadLocal;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->uriDereferencer:Ljavax/xml/crypto/URIDereferencer;

    if-nez v0, :cond_0

    new-instance v0, Lorg/apache/poi/poifs/crypt/dsig/OOXMLURIDereferencer;

    invoke-direct {v0}, Lorg/apache/poi/poifs/crypt/dsig/OOXMLURIDereferencer;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->uriDereferencer:Ljavax/xml/crypto/URIDereferencer;

    :cond_0
    iget-object v0, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->uriDereferencer:Ljavax/xml/crypto/URIDereferencer;

    instance-of v1, v0, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig$SignatureConfigurable;

    if-eqz v1, :cond_1

    check-cast v0, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig$SignatureConfigurable;

    invoke-interface {v0, p0}, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig$SignatureConfigurable;->setSignatureConfig(Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;)V

    :cond_1
    iget-object v0, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->namespacePrefixes:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->namespacePrefixes:Ljava/util/Map;

    const-string v1, "http://schemas.openxmlformats.org/package/2006/digital-signature"

    const-string v2, "mdssi"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->namespacePrefixes:Ljava/util/Map;

    const-string v1, "http://uri.etsi.org/01903/v1.3.2#"

    const-string v2, "xd"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_2
    if-eqz p1, :cond_3

    return-void

    :cond_3
    iget-object p1, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->signatureMarshalListener:Lorg/w3c/dom/events/EventListener;

    if-nez p1, :cond_4

    new-instance p1, Lorg/apache/poi/poifs/crypt/dsig/SignatureMarshalListener;

    invoke-direct {p1}, Lorg/apache/poi/poifs/crypt/dsig/SignatureMarshalListener;-><init>()V

    iput-object p1, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->signatureMarshalListener:Lorg/w3c/dom/events/EventListener;

    :cond_4
    iget-object p1, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->signatureMarshalListener:Lorg/w3c/dom/events/EventListener;

    instance-of v0, p1, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig$SignatureConfigurable;

    if-eqz v0, :cond_5

    check-cast p1, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig$SignatureConfigurable;

    invoke-interface {p1, p0}, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig$SignatureConfigurable;->setSignatureConfig(Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;)V

    :cond_5
    iget-object p1, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->tspService:Lorg/apache/poi/poifs/crypt/dsig/services/TimeStampService;

    if-eqz p1, :cond_6

    invoke-interface {p1, p0}, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig$SignatureConfigurable;->setSignatureConfig(Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;)V

    :cond_6
    iget-object p1, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->signatureFacets:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result p1

    if-eqz p1, :cond_7

    new-instance p1, Lorg/apache/poi/poifs/crypt/dsig/facets/OOXMLSignatureFacet;

    invoke-direct {p1}, Lorg/apache/poi/poifs/crypt/dsig/facets/OOXMLSignatureFacet;-><init>()V

    invoke-virtual {p0, p1}, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->addSignatureFacet(Lorg/apache/poi/poifs/crypt/dsig/facets/SignatureFacet;)V

    new-instance p1, Lorg/apache/poi/poifs/crypt/dsig/facets/KeyInfoSignatureFacet;

    invoke-direct {p1}, Lorg/apache/poi/poifs/crypt/dsig/facets/KeyInfoSignatureFacet;-><init>()V

    invoke-virtual {p0, p1}, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->addSignatureFacet(Lorg/apache/poi/poifs/crypt/dsig/facets/SignatureFacet;)V

    new-instance p1, Lorg/apache/poi/poifs/crypt/dsig/facets/XAdESSignatureFacet;

    invoke-direct {p1}, Lorg/apache/poi/poifs/crypt/dsig/facets/XAdESSignatureFacet;-><init>()V

    invoke-virtual {p0, p1}, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->addSignatureFacet(Lorg/apache/poi/poifs/crypt/dsig/facets/SignatureFacet;)V

    new-instance p1, Lorg/apache/poi/poifs/crypt/dsig/facets/Office2010SignatureFacet;

    invoke-direct {p1}, Lorg/apache/poi/poifs/crypt/dsig/facets/Office2010SignatureFacet;-><init>()V

    invoke-virtual {p0, p1}, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->addSignatureFacet(Lorg/apache/poi/poifs/crypt/dsig/facets/SignatureFacet;)V

    :cond_7
    iget-object p1, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->signatureFacets:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/poi/poifs/crypt/dsig/facets/SignatureFacet;

    invoke-virtual {v0, p0}, Lorg/apache/poi/poifs/crypt/dsig/facets/SignatureFacet;->setSignatureConfig(Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;)V

    goto :goto_0

    :cond_8
    return-void

    :cond_9
    new-instance p1, Lorg/apache/poi/EncryptedDocumentException;

    const-string v0, "opcPackage is null"

    invoke-direct {p1, v0}, Lorg/apache/poi/EncryptedDocumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public isIncludeEntireCertificateChain()Z
    .locals 1

    iget-boolean v0, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->includeEntireCertificateChain:Z

    return v0
.end method

.method public isIncludeIssuerSerial()Z
    .locals 1

    iget-boolean v0, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->includeIssuerSerial:Z

    return v0
.end method

.method public isIncludeKeyValue()Z
    .locals 1

    iget-boolean v0, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->includeKeyValue:Z

    return v0
.end method

.method public isTspOldProtocol()Z
    .locals 1

    iget-boolean v0, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->tspOldProtocol:Z

    return v0
.end method

.method public isXadesIssuerNameNoReverseOrder()Z
    .locals 1

    iget-boolean v0, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->xadesIssuerNameNoReverseOrder:Z

    return v0
.end method

.method public isXadesSignaturePolicyImplied()Z
    .locals 1

    iget-boolean v0, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->xadesSignaturePolicyImplied:Z

    return v0
.end method

.method public setCanonicalizationMethod(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->canonicalizationMethod:Ljava/lang/String;

    return-void
.end method

.method public setDigestAlgo(Lorg/apache/poi/poifs/crypt/HashAlgorithm;)V
    .locals 0

    iput-object p1, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->digestAlgo:Lorg/apache/poi/poifs/crypt/HashAlgorithm;

    return-void
.end method

.method public setExecutionTime(Ljava/util/Date;)V
    .locals 0

    iput-object p1, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->executionTime:Ljava/util/Date;

    return-void
.end method

.method public setIncludeEntireCertificateChain(Z)V
    .locals 0

    iput-boolean p1, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->includeEntireCertificateChain:Z

    return-void
.end method

.method public setIncludeIssuerSerial(Z)V
    .locals 0

    iput-boolean p1, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->includeIssuerSerial:Z

    return-void
.end method

.method public setIncludeKeyValue(Z)V
    .locals 0

    iput-boolean p1, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->includeKeyValue:Z

    return-void
.end method

.method public setKey(Ljava/security/PrivateKey;)V
    .locals 0

    iput-object p1, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->key:Ljava/security/PrivateKey;

    return-void
.end method

.method public setKeyInfoFactory(Ljavax/xml/crypto/dsig/keyinfo/KeyInfoFactory;)V
    .locals 1

    iget-object v0, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->keyInfoFactory:Ljava/lang/ThreadLocal;

    invoke-virtual {v0, p1}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public setNamespacePrefixes(Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->namespacePrefixes:Ljava/util/Map;

    return-void
.end method

.method public setOpcPackage(Lorg/apache/poi/openxml4j/opc/OPCPackage;)V
    .locals 1

    iget-object v0, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->opcPackage:Ljava/lang/ThreadLocal;

    invoke-virtual {v0, p1}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public setPackageSignatureId(Ljava/lang/String;)V
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "xmldsig-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->nvl(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->packageSignatureId:Ljava/lang/String;

    return-void
.end method

.method public setProxyUrl(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->proxyUrl:Ljava/lang/String;

    return-void
.end method

.method public setRevocationDataService(Lorg/apache/poi/poifs/crypt/dsig/services/RevocationDataService;)V
    .locals 0

    iput-object p1, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->revocationDataService:Lorg/apache/poi/poifs/crypt/dsig/services/RevocationDataService;

    return-void
.end method

.method public setSignatureDescription(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->signatureDescription:Ljava/lang/String;

    return-void
.end method

.method public setSignatureFacets(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lorg/apache/poi/poifs/crypt/dsig/facets/SignatureFacet;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->signatureFacets:Ljava/util/List;

    return-void
.end method

.method public setSignatureFactory(Ljavax/xml/crypto/dsig/XMLSignatureFactory;)V
    .locals 1

    iget-object v0, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->signatureFactory:Ljava/lang/ThreadLocal;

    invoke-virtual {v0, p1}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    return-void
.end method

.method public setSignatureMarshalListener(Lorg/w3c/dom/events/EventListener;)V
    .locals 0

    iput-object p1, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->signatureMarshalListener:Lorg/w3c/dom/events/EventListener;

    return-void
.end method

.method public setSignaturePolicyService(Lorg/apache/poi/poifs/crypt/dsig/services/SignaturePolicyService;)V
    .locals 0

    iput-object p1, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->signaturePolicyService:Lorg/apache/poi/poifs/crypt/dsig/services/SignaturePolicyService;

    return-void
.end method

.method public setSigningCertificateChain(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/security/cert/X509Certificate;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->signingCertificateChain:Ljava/util/List;

    return-void
.end method

.method public setTspDigestAlgo(Lorg/apache/poi/poifs/crypt/HashAlgorithm;)V
    .locals 0

    iput-object p1, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->tspDigestAlgo:Lorg/apache/poi/poifs/crypt/HashAlgorithm;

    return-void
.end method

.method public setTspOldProtocol(Z)V
    .locals 0

    iput-boolean p1, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->tspOldProtocol:Z

    return-void
.end method

.method public setTspPass(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->tspPass:Ljava/lang/String;

    return-void
.end method

.method public setTspRequestPolicy(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->tspRequestPolicy:Ljava/lang/String;

    return-void
.end method

.method public setTspService(Lorg/apache/poi/poifs/crypt/dsig/services/TimeStampService;)V
    .locals 0

    iput-object p1, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->tspService:Lorg/apache/poi/poifs/crypt/dsig/services/TimeStampService;

    return-void
.end method

.method public setTspUrl(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->tspUrl:Ljava/lang/String;

    return-void
.end method

.method public setTspUser(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->tspUser:Ljava/lang/String;

    return-void
.end method

.method public setTspValidator(Lorg/apache/poi/poifs/crypt/dsig/services/TimeStampServiceValidator;)V
    .locals 0

    iput-object p1, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->tspValidator:Lorg/apache/poi/poifs/crypt/dsig/services/TimeStampServiceValidator;

    return-void
.end method

.method public setUriDereferencer(Ljavax/xml/crypto/URIDereferencer;)V
    .locals 0

    iput-object p1, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->uriDereferencer:Ljavax/xml/crypto/URIDereferencer;

    return-void
.end method

.method public setUserAgent(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->userAgent:Ljava/lang/String;

    return-void
.end method

.method public setXadesCanonicalizationMethod(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->xadesCanonicalizationMethod:Ljava/lang/String;

    return-void
.end method

.method public setXadesDigestAlgo(Lorg/apache/poi/poifs/crypt/HashAlgorithm;)V
    .locals 0

    iput-object p1, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->xadesDigestAlgo:Lorg/apache/poi/poifs/crypt/HashAlgorithm;

    return-void
.end method

.method public setXadesIssuerNameNoReverseOrder(Z)V
    .locals 0

    iput-boolean p1, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->xadesIssuerNameNoReverseOrder:Z

    return-void
.end method

.method public setXadesRole(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->xadesRole:Ljava/lang/String;

    return-void
.end method

.method public setXadesSignatureId(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->xadesSignatureId:Ljava/lang/String;

    return-void
.end method

.method public setXadesSignaturePolicyImplied(Z)V
    .locals 0

    iput-boolean p1, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->xadesSignaturePolicyImplied:Z

    return-void
.end method

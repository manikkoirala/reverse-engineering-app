.class public Lorg/apache/poi/poifs/crypt/dsig/facets/XAdESXLSignatureFacet;
.super Lorg/apache/poi/poifs/crypt/dsig/facets/SignatureFacet;
.source "SourceFile"


# static fields
.field private static final LOG:Lorg/apache/poi/util/POILogger;


# instance fields
.field private final certificateFactory:Ljava/security/cert/CertificateFactory;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    const-class v0, Lorg/apache/poi/poifs/crypt/dsig/facets/XAdESXLSignatureFacet;

    invoke-static {v0}, Lorg/apache/poi/util/POILogFactory;->getLogger(Ljava/lang/Class;)Lorg/apache/poi/util/POILogger;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/poifs/crypt/dsig/facets/XAdESXLSignatureFacet;->LOG:Lorg/apache/poi/util/POILogger;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    invoke-direct {p0}, Lorg/apache/poi/poifs/crypt/dsig/facets/SignatureFacet;-><init>()V

    :try_start_0
    const-string v0, "X.509"

    invoke-static {v0}, Ljava/security/cert/CertificateFactory;->getInstance(Ljava/lang/String;)Ljava/security/cert/CertificateFactory;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/poifs/crypt/dsig/facets/XAdESXLSignatureFacet;->certificateFactory:Ljava/security/cert/CertificateFactory;
    :try_end_0
    .catch Ljava/security/cert/CertificateException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "X509 JCA error: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method private createRevocationValues(Lorg/etsi/uri/x01903/v13/RevocationValuesType;Lorg/apache/poi/poifs/crypt/dsig/services/RevocationData;)V
    .locals 4

    invoke-virtual {p2}, Lorg/apache/poi/poifs/crypt/dsig/services/RevocationData;->hasCRLs()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Lorg/etsi/uri/x01903/v13/RevocationValuesType;->addNewCRLValues()Lorg/etsi/uri/x01903/v13/CRLValuesType;

    move-result-object v0

    invoke-virtual {p2}, Lorg/apache/poi/poifs/crypt/dsig/services/RevocationData;->getCRLs()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [B

    invoke-interface {v0}, Lorg/etsi/uri/x01903/v13/CRLValuesType;->addNewEncapsulatedCRLValue()Lorg/etsi/uri/x01903/v13/EncapsulatedPKIDataType;

    move-result-object v3

    invoke-interface {v3, v2}, Lorg/apache/xmlbeans/XmlBase64Binary;->setByteArrayValue([B)V

    goto :goto_0

    :cond_0
    invoke-virtual {p2}, Lorg/apache/poi/poifs/crypt/dsig/services/RevocationData;->hasOCSPs()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Lorg/etsi/uri/x01903/v13/RevocationValuesType;->addNewOCSPValues()Lorg/etsi/uri/x01903/v13/OCSPValuesType;

    move-result-object p1

    invoke-virtual {p2}, Lorg/apache/poi/poifs/crypt/dsig/services/RevocationData;->getOCSPs()Ljava/util/List;

    move-result-object p2

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_1
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    invoke-interface {p1}, Lorg/etsi/uri/x01903/v13/OCSPValuesType;->addNewEncapsulatedOCSPValue()Lorg/etsi/uri/x01903/v13/EncapsulatedPKIDataType;

    move-result-object v1

    invoke-interface {v1, v0}, Lorg/apache/xmlbeans/XmlBase64Binary;->setByteArrayValue([B)V

    goto :goto_1

    :cond_1
    return-void
.end method

.method private createValidationData(Lorg/apache/poi/poifs/crypt/dsig/services/RevocationData;)Lorg/etsi/uri/x01903/v14/ValidationDataType;
    .locals 2

    invoke-static {}, Lorg/etsi/uri/x01903/v14/ValidationDataType$Factory;->newInstance()Lorg/etsi/uri/x01903/v14/ValidationDataType;

    move-result-object v0

    invoke-interface {v0}, Lorg/etsi/uri/x01903/v14/ValidationDataType;->addNewRevocationValues()Lorg/etsi/uri/x01903/v13/RevocationValuesType;

    move-result-object v1

    invoke-direct {p0, v1, p1}, Lorg/apache/poi/poifs/crypt/dsig/facets/XAdESXLSignatureFacet;->createRevocationValues(Lorg/etsi/uri/x01903/v13/RevocationValuesType;Lorg/apache/poi/poifs/crypt/dsig/services/RevocationData;)V

    return-object v0
.end method

.method private createXAdESTimeStamp(Ljava/util/List;Lorg/apache/poi/poifs/crypt/dsig/services/RevocationData;)Lorg/etsi/uri/x01903/v13/XAdESTimeStampType;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lorg/w3c/dom/Node;",
            ">;",
            "Lorg/apache/poi/poifs/crypt/dsig/services/RevocationData;",
            ")",
            "Lorg/etsi/uri/x01903/v13/XAdESTimeStampType;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lorg/apache/poi/poifs/crypt/dsig/facets/SignatureFacet;->signatureConfig:Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;

    invoke-virtual {v0}, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->getXadesCanonicalizationMethod()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lorg/apache/poi/poifs/crypt/dsig/facets/XAdESXLSignatureFacet;->getC14nValue(Ljava/util/List;Ljava/lang/String;)[B

    move-result-object p1

    invoke-direct {p0, p1, p2}, Lorg/apache/poi/poifs/crypt/dsig/facets/XAdESXLSignatureFacet;->createXAdESTimeStamp([BLorg/apache/poi/poifs/crypt/dsig/services/RevocationData;)Lorg/etsi/uri/x01903/v13/XAdESTimeStampType;

    move-result-object p1

    return-object p1
.end method

.method private createXAdESTimeStamp([BLorg/apache/poi/poifs/crypt/dsig/services/RevocationData;)Lorg/etsi/uri/x01903/v13/XAdESTimeStampType;
    .locals 2

    .line 2
    :try_start_0
    iget-object v0, p0, Lorg/apache/poi/poifs/crypt/dsig/facets/SignatureFacet;->signatureConfig:Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;

    invoke-virtual {v0}, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->getTspService()Lorg/apache/poi/poifs/crypt/dsig/services/TimeStampService;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lorg/apache/poi/poifs/crypt/dsig/services/TimeStampService;->timeStamp([BLorg/apache/poi/poifs/crypt/dsig/services/RevocationData;)[B

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    invoke-static {}, Lorg/etsi/uri/x01903/v13/XAdESTimeStampType$Factory;->newInstance()Lorg/etsi/uri/x01903/v13/XAdESTimeStampType;

    move-result-object p2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "time-stamp-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p2, v0}, Lorg/etsi/uri/x01903/v13/GenericTimeStampType;->setId(Ljava/lang/String;)V

    invoke-interface {p2}, Lorg/etsi/uri/x01903/v13/GenericTimeStampType;->addNewCanonicalizationMethod()Lorg/w3/x2000/x09/xmldsig/CanonicalizationMethodType;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/poi/poifs/crypt/dsig/facets/SignatureFacet;->signatureConfig:Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;

    invoke-virtual {v1}, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->getXadesCanonicalizationMethod()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/w3/x2000/x09/xmldsig/CanonicalizationMethodType;->setAlgorithm(Ljava/lang/String;)V

    invoke-interface {p2}, Lorg/etsi/uri/x01903/v13/GenericTimeStampType;->addNewEncapsulatedTimeStamp()Lorg/etsi/uri/x01903/v13/EncapsulatedPKIDataType;

    move-result-object v0

    invoke-interface {v0, p1}, Lorg/apache/xmlbeans/XmlBase64Binary;->setByteArrayValue([B)V

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "time-stamp-token-"

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-interface {v0, p1}, Lorg/etsi/uri/x01903/v13/EncapsulatedPKIDataType;->setId(Ljava/lang/String;)V

    return-object p2

    :catch_0
    move-exception p1

    new-instance p2, Ljava/lang/RuntimeException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "error while creating a time-stamp: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p2, v0, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw p2
.end method

.method public static getC14nValue(Ljava/util/List;Ljava/lang/String;)[B
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lorg/w3c/dom/Node;",
            ">;",
            "Ljava/lang/String;",
            ")[B"
        }
    .end annotation

    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    :try_start_0
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/w3c/dom/Node;

    invoke-static {p1}, Lorg/apache/xml/security/c14n/Canonicalizer;->getInstance(Ljava/lang/String;)Lorg/apache/xml/security/c14n/Canonicalizer;

    move-result-object v2

    invoke-virtual {v2, v1}, Lorg/apache/xml/security/c14n/Canonicalizer;->canonicalizeSubtree(Lorg/w3c/dom/Node;)[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/OutputStream;->write([B)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object p0

    return-object p0

    :catch_0
    move-exception p0

    new-instance p1, Ljava/lang/RuntimeException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "c14n error: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0, p0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw p1

    :catch_1
    move-exception p0

    throw p0
.end method

.method private getCrlNumber(Ljava/security/cert/X509CRL;)Ljava/math/BigInteger;
    .locals 3

    sget-object v0, Lorg/bouncycastle/asn1/x509/Extension;->cRLNumber:Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;

    invoke-virtual {v0}, Lorg/bouncycastle/asn1/ASN1ObjectIdentifier;->getId()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/security/cert/X509Extension;->getExtensionValue(Ljava/lang/String;)[B

    move-result-object p1

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return-object v0

    :cond_0
    :try_start_0
    new-instance v1, Lorg/bouncycastle/asn1/ASN1InputStream;

    invoke-direct {v1, p1}, Lorg/bouncycastle/asn1/ASN1InputStream;-><init>([B)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    :try_start_1
    invoke-virtual {v1}, Lorg/bouncycastle/asn1/ASN1InputStream;->readObject()Lorg/bouncycastle/asn1/ASN1Primitive;

    move-result-object p1

    check-cast p1, Lorg/bouncycastle/asn1/ASN1OctetString;

    invoke-virtual {p1}, Lorg/bouncycastle/asn1/ASN1OctetString;->getOctets()[B

    move-result-object p1

    new-instance v2, Lorg/bouncycastle/asn1/ASN1InputStream;

    invoke-direct {v2, p1}, Lorg/bouncycastle/asn1/ASN1InputStream;-><init>([B)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    invoke-virtual {v2}, Lorg/bouncycastle/asn1/ASN1InputStream;->readObject()Lorg/bouncycastle/asn1/ASN1Primitive;

    move-result-object p1

    check-cast p1, Lorg/bouncycastle/asn1/ASN1Integer;

    invoke-virtual {p1}, Lorg/bouncycastle/asn1/ASN1Integer;->getPositiveValue()Ljava/math/BigInteger;

    move-result-object p1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    invoke-static {v2}, Lorg/apache/poi/util/IOUtils;->closeQuietly(Ljava/io/Closeable;)V

    invoke-static {v1}, Lorg/apache/poi/util/IOUtils;->closeQuietly(Ljava/io/Closeable;)V

    return-object p1

    :catchall_0
    move-exception p1

    move-object v0, v2

    goto :goto_0

    :catchall_1
    move-exception p1

    goto :goto_0

    :catchall_2
    move-exception p1

    move-object v1, v0

    :goto_0
    invoke-static {v0}, Lorg/apache/poi/util/IOUtils;->closeQuietly(Ljava/io/Closeable;)V

    invoke-static {v1}, Lorg/apache/poi/util/IOUtils;->closeQuietly(Ljava/io/Closeable;)V

    throw p1
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    :catch_0
    move-exception p1

    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "I/O error: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
.end method


# virtual methods
.method public postSign(Lorg/w3c/dom/Document;)V
    .locals 20

    move-object/from16 v1, p0

    move-object/from16 v0, p1

    sget-object v2, Lorg/apache/poi/poifs/crypt/dsig/facets/XAdESXLSignatureFacet;->LOG:Lorg/apache/poi/util/POILogger;

    const-string v3, "XAdES-X-L post sign phase"

    filled-new-array {v3}, [Ljava/lang/Object;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v2, v4, v3}, Lorg/apache/poi/util/POILogger;->log(I[Ljava/lang/Object;)V

    const-string v3, "http://uri.etsi.org/01903/v1.3.2#"

    const-string v5, "QualifyingProperties"

    invoke-interface {v0, v3, v5}, Lorg/w3c/dom/Document;->getElementsByTagNameNS(Ljava/lang/String;Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v3

    invoke-interface {v3}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v5

    if-ne v5, v4, :cond_b

    const/4 v5, 0x0

    :try_start_0
    invoke-interface {v3, v5}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v6

    sget-object v7, Lorg/apache/poi/POIXMLTypeLoader;->DEFAULT_XML_OPTIONS:Lorg/apache/xmlbeans/XmlOptions;

    invoke-static {v6, v7}, Lorg/etsi/uri/x01903/v13/QualifyingPropertiesDocument$Factory;->parse(Lorg/w3c/dom/Node;Lorg/apache/xmlbeans/XmlOptions;)Lorg/etsi/uri/x01903/v13/QualifyingPropertiesDocument;

    move-result-object v6
    :try_end_0
    .catch Lorg/apache/xmlbeans/XmlException; {:try_start_0 .. :try_end_0} :catch_3

    invoke-interface {v6}, Lorg/etsi/uri/x01903/v13/QualifyingPropertiesDocument;->getQualifyingProperties()Lorg/etsi/uri/x01903/v13/QualifyingPropertiesType;

    move-result-object v6

    invoke-interface {v6}, Lorg/etsi/uri/x01903/v13/QualifyingPropertiesType;->getUnsignedProperties()Lorg/etsi/uri/x01903/v13/UnsignedPropertiesType;

    move-result-object v7

    if-nez v7, :cond_0

    invoke-interface {v6}, Lorg/etsi/uri/x01903/v13/QualifyingPropertiesType;->addNewUnsignedProperties()Lorg/etsi/uri/x01903/v13/UnsignedPropertiesType;

    move-result-object v7

    :cond_0
    invoke-interface {v7}, Lorg/etsi/uri/x01903/v13/UnsignedPropertiesType;->getUnsignedSignatureProperties()Lorg/etsi/uri/x01903/v13/UnsignedSignaturePropertiesType;

    move-result-object v8

    if-nez v8, :cond_1

    invoke-interface {v7}, Lorg/etsi/uri/x01903/v13/UnsignedPropertiesType;->addNewUnsignedSignatureProperties()Lorg/etsi/uri/x01903/v13/UnsignedSignaturePropertiesType;

    move-result-object v8

    :cond_1
    const-string v7, "http://www.w3.org/2000/09/xmldsig#"

    const-string v9, "SignatureValue"

    invoke-interface {v0, v7, v9}, Lorg/w3c/dom/Document;->getElementsByTagNameNS(Ljava/lang/String;Ljava/lang/String;)Lorg/w3c/dom/NodeList;

    move-result-object v7

    invoke-interface {v7}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v9

    if-ne v9, v4, :cond_a

    new-instance v9, Lorg/apache/poi/poifs/crypt/dsig/services/RevocationData;

    invoke-direct {v9}, Lorg/apache/poi/poifs/crypt/dsig/services/RevocationData;-><init>()V

    const-string v10, "creating XAdES-T time-stamp"

    filled-new-array {v10}, [Ljava/lang/Object;

    move-result-object v10

    invoke-virtual {v2, v4, v10}, Lorg/apache/poi/util/POILogger;->log(I[Ljava/lang/Object;)V

    invoke-interface {v7, v5}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-direct {v1, v2, v9}, Lorg/apache/poi/poifs/crypt/dsig/facets/XAdESXLSignatureFacet;->createXAdESTimeStamp(Ljava/util/List;Lorg/apache/poi/poifs/crypt/dsig/services/RevocationData;)Lorg/etsi/uri/x01903/v13/XAdESTimeStampType;

    move-result-object v2

    invoke-interface {v8}, Lorg/etsi/uri/x01903/v13/UnsignedSignaturePropertiesType;->addNewSignatureTimeStamp()Lorg/etsi/uri/x01903/v13/XAdESTimeStampType;

    move-result-object v10

    invoke-interface {v10, v2}, Lorg/apache/xmlbeans/XmlObject;->set(Lorg/apache/xmlbeans/XmlObject;)Lorg/apache/xmlbeans/XmlObject;

    invoke-virtual {v9}, Lorg/apache/poi/poifs/crypt/dsig/services/RevocationData;->hasRevocationDataEntries()Z

    move-result v10

    if-eqz v10, :cond_2

    invoke-direct {v1, v9}, Lorg/apache/poi/poifs/crypt/dsig/facets/XAdESXLSignatureFacet;->createValidationData(Lorg/apache/poi/poifs/crypt/dsig/services/RevocationData;)Lorg/etsi/uri/x01903/v14/ValidationDataType;

    move-result-object v9

    invoke-static {v8, v9}, Lorg/apache/poi/poifs/crypt/dsig/facets/XAdESSignatureFacet;->insertXChild(Lorg/apache/xmlbeans/XmlObject;Lorg/apache/xmlbeans/XmlObject;)V

    :cond_2
    iget-object v9, v1, Lorg/apache/poi/poifs/crypt/dsig/facets/SignatureFacet;->signatureConfig:Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;

    invoke-virtual {v9}, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->getRevocationDataService()Lorg/apache/poi/poifs/crypt/dsig/services/RevocationDataService;

    move-result-object v9

    if-nez v9, :cond_3

    return-void

    :cond_3
    invoke-interface {v8}, Lorg/etsi/uri/x01903/v13/UnsignedSignaturePropertiesType;->addNewCompleteCertificateRefs()Lorg/etsi/uri/x01903/v13/CompleteCertificateRefsType;

    move-result-object v9

    invoke-interface {v9}, Lorg/etsi/uri/x01903/v13/CompleteCertificateRefsType;->addNewCertRefs()Lorg/etsi/uri/x01903/v13/CertIDListType;

    move-result-object v10

    iget-object v11, v1, Lorg/apache/poi/poifs/crypt/dsig/facets/SignatureFacet;->signatureConfig:Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;

    invoke-virtual {v11}, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->getSigningCertificateChain()Ljava/util/List;

    move-result-object v11

    invoke-interface {v11}, Ljava/util/List;->size()I

    move-result v12

    if-le v12, v4, :cond_4

    invoke-interface {v11, v4, v12}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v12

    invoke-interface {v12}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :goto_0
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_4

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/security/cert/X509Certificate;

    invoke-interface {v10}, Lorg/etsi/uri/x01903/v13/CertIDListType;->addNewCert()Lorg/etsi/uri/x01903/v13/CertIDType;

    move-result-object v14

    iget-object v15, v1, Lorg/apache/poi/poifs/crypt/dsig/facets/SignatureFacet;->signatureConfig:Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;

    invoke-static {v14, v15, v5, v13}, Lorg/apache/poi/poifs/crypt/dsig/facets/XAdESSignatureFacet;->setCertID(Lorg/etsi/uri/x01903/v13/CertIDType;Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;ZLjava/security/cert/X509Certificate;)V

    goto :goto_0

    :cond_4
    invoke-interface {v8}, Lorg/etsi/uri/x01903/v13/UnsignedSignaturePropertiesType;->addNewCompleteRevocationRefs()Lorg/etsi/uri/x01903/v13/CompleteRevocationRefsType;

    move-result-object v10

    iget-object v12, v1, Lorg/apache/poi/poifs/crypt/dsig/facets/SignatureFacet;->signatureConfig:Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;

    invoke-virtual {v12}, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->getRevocationDataService()Lorg/apache/poi/poifs/crypt/dsig/services/RevocationDataService;

    move-result-object v12

    invoke-interface {v12, v11}, Lorg/apache/poi/poifs/crypt/dsig/services/RevocationDataService;->getRevocationData(Ljava/util/List;)Lorg/apache/poi/poifs/crypt/dsig/services/RevocationData;

    move-result-object v12

    invoke-virtual {v12}, Lorg/apache/poi/poifs/crypt/dsig/services/RevocationData;->hasCRLs()Z

    move-result v13

    const-string v14, "Z"

    if-eqz v13, :cond_5

    invoke-interface {v10}, Lorg/etsi/uri/x01903/v13/CompleteRevocationRefsType;->addNewCRLRefs()Lorg/etsi/uri/x01903/v13/CRLRefsType;

    move-result-object v13

    invoke-interface {v10, v13}, Lorg/etsi/uri/x01903/v13/CompleteRevocationRefsType;->setCRLRefs(Lorg/etsi/uri/x01903/v13/CRLRefsType;)V

    invoke-virtual {v12}, Lorg/apache/poi/poifs/crypt/dsig/services/RevocationData;->getCRLs()Ljava/util/List;

    move-result-object v15

    invoke-interface {v15}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v15

    :goto_1
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v16

    if-eqz v16, :cond_5

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v16

    move-object/from16 v4, v16

    check-cast v4, [B

    invoke-interface {v13}, Lorg/etsi/uri/x01903/v13/CRLRefsType;->addNewCRLRef()Lorg/etsi/uri/x01903/v13/CRLRefType;

    move-result-object v16

    :try_start_1
    iget-object v5, v1, Lorg/apache/poi/poifs/crypt/dsig/facets/XAdESXLSignatureFacet;->certificateFactory:Ljava/security/cert/CertificateFactory;

    move-object/from16 v17, v13

    new-instance v13, Ljava/io/ByteArrayInputStream;

    invoke-direct {v13, v4}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-virtual {v5, v13}, Ljava/security/cert/CertificateFactory;->generateCRL(Ljava/io/InputStream;)Ljava/security/cert/CRL;

    move-result-object v5

    check-cast v5, Ljava/security/cert/X509CRL;
    :try_end_1
    .catch Ljava/security/cert/CRLException; {:try_start_1 .. :try_end_1} :catch_0

    invoke-interface/range {v16 .. v16}, Lorg/etsi/uri/x01903/v13/CRLRefType;->addNewCRLIdentifier()Lorg/etsi/uri/x01903/v13/CRLIdentifierType;

    move-result-object v13

    invoke-virtual {v5}, Ljava/security/cert/X509CRL;->getIssuerDN()Ljava/security/Principal;

    move-result-object v18

    move-object/from16 v19, v15

    invoke-interface/range {v18 .. v18}, Ljava/security/Principal;->getName()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v18, v3

    const-string v3, ","

    const-string v0, ", "

    invoke-virtual {v15, v3, v0}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v13, v0}, Lorg/etsi/uri/x01903/v13/CRLIdentifierType;->setIssuer(Ljava/lang/String;)V

    invoke-static {v14}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v0

    sget-object v3, Ljava/util/Locale;->ROOT:Ljava/util/Locale;

    invoke-static {v0, v3}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;Ljava/util/Locale;)Ljava/util/Calendar;

    move-result-object v0

    invoke-virtual {v5}, Ljava/security/cert/X509CRL;->getThisUpdate()Ljava/util/Date;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    invoke-interface {v13, v0}, Lorg/etsi/uri/x01903/v13/CRLIdentifierType;->setIssueTime(Ljava/util/Calendar;)V

    invoke-direct {v1, v5}, Lorg/apache/poi/poifs/crypt/dsig/facets/XAdESXLSignatureFacet;->getCrlNumber(Ljava/security/cert/X509CRL;)Ljava/math/BigInteger;

    move-result-object v0

    invoke-interface {v13, v0}, Lorg/etsi/uri/x01903/v13/CRLIdentifierType;->setNumber(Ljava/math/BigInteger;)V

    invoke-interface/range {v16 .. v16}, Lorg/etsi/uri/x01903/v13/CRLRefType;->addNewDigestAlgAndValue()Lorg/etsi/uri/x01903/v13/DigestAlgAndValueType;

    move-result-object v0

    iget-object v3, v1, Lorg/apache/poi/poifs/crypt/dsig/facets/SignatureFacet;->signatureConfig:Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;

    invoke-virtual {v3}, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->getDigestAlgo()Lorg/apache/poi/poifs/crypt/HashAlgorithm;

    move-result-object v3

    invoke-static {v0, v4, v3}, Lorg/apache/poi/poifs/crypt/dsig/facets/XAdESSignatureFacet;->setDigestAlgAndValue(Lorg/etsi/uri/x01903/v13/DigestAlgAndValueType;[BLorg/apache/poi/poifs/crypt/HashAlgorithm;)V

    move-object/from16 v0, p1

    move-object/from16 v13, v17

    move-object/from16 v3, v18

    move-object/from16 v15, v19

    const/4 v4, 0x1

    const/4 v5, 0x0

    goto :goto_1

    :catch_0
    move-exception v0

    new-instance v2, Ljava/lang/RuntimeException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "CRL parse error: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2

    :cond_5
    move-object/from16 v18, v3

    invoke-virtual {v12}, Lorg/apache/poi/poifs/crypt/dsig/services/RevocationData;->hasOCSPs()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v10}, Lorg/etsi/uri/x01903/v13/CompleteRevocationRefsType;->addNewOCSPRefs()Lorg/etsi/uri/x01903/v13/OCSPRefsType;

    move-result-object v0

    invoke-virtual {v12}, Lorg/apache/poi/poifs/crypt/dsig/services/RevocationData;->getOCSPs()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_7

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [B

    :try_start_2
    invoke-interface {v0}, Lorg/etsi/uri/x01903/v13/OCSPRefsType;->addNewOCSPRef()Lorg/etsi/uri/x01903/v13/OCSPRefType;

    move-result-object v5

    invoke-interface {v5}, Lorg/etsi/uri/x01903/v13/OCSPRefType;->addNewDigestAlgAndValue()Lorg/etsi/uri/x01903/v13/DigestAlgAndValueType;

    move-result-object v13

    iget-object v15, v1, Lorg/apache/poi/poifs/crypt/dsig/facets/SignatureFacet;->signatureConfig:Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;

    invoke-virtual {v15}, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->getDigestAlgo()Lorg/apache/poi/poifs/crypt/HashAlgorithm;

    move-result-object v15

    invoke-static {v13, v4, v15}, Lorg/apache/poi/poifs/crypt/dsig/facets/XAdESSignatureFacet;->setDigestAlgAndValue(Lorg/etsi/uri/x01903/v13/DigestAlgAndValueType;[BLorg/apache/poi/poifs/crypt/HashAlgorithm;)V

    invoke-interface {v5}, Lorg/etsi/uri/x01903/v13/OCSPRefType;->addNewOCSPIdentifier()Lorg/etsi/uri/x01903/v13/OCSPIdentifierType;

    move-result-object v5

    new-instance v13, Lorg/bouncycastle/cert/ocsp/OCSPResp;

    invoke-direct {v13, v4}, Lorg/bouncycastle/cert/ocsp/OCSPResp;-><init>([B)V

    invoke-virtual {v13}, Lorg/bouncycastle/cert/ocsp/OCSPResp;->getResponseObject()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/bouncycastle/cert/ocsp/BasicOCSPResp;

    invoke-static {v14}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v13

    sget-object v15, Ljava/util/Locale;->ROOT:Ljava/util/Locale;

    invoke-static {v13, v15}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;Ljava/util/Locale;)Ljava/util/Calendar;

    move-result-object v13

    invoke-virtual {v4}, Lorg/bouncycastle/cert/ocsp/BasicOCSPResp;->getProducedAt()Ljava/util/Date;

    move-result-object v15

    invoke-virtual {v13, v15}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    invoke-interface {v5, v13}, Lorg/etsi/uri/x01903/v13/OCSPIdentifierType;->setProducedAt(Ljava/util/Calendar;)V

    invoke-interface {v5}, Lorg/etsi/uri/x01903/v13/OCSPIdentifierType;->addNewResponderID()Lorg/etsi/uri/x01903/v13/ResponderIDType;

    move-result-object v5

    invoke-virtual {v4}, Lorg/bouncycastle/cert/ocsp/BasicOCSPResp;->getResponderId()Lorg/bouncycastle/cert/ocsp/RespID;

    move-result-object v4

    invoke-virtual {v4}, Lorg/bouncycastle/cert/ocsp/RespID;->toASN1Primitive()Lorg/bouncycastle/asn1/ocsp/ResponderID;

    move-result-object v4

    invoke-virtual {v4}, Lorg/bouncycastle/asn1/ocsp/ResponderID;->toASN1Primitive()Lorg/bouncycastle/asn1/ASN1Primitive;

    move-result-object v4

    check-cast v4, Lorg/bouncycastle/asn1/DERTaggedObject;

    invoke-virtual {v4}, Lorg/bouncycastle/asn1/DERTaggedObject;->getTagNo()I

    move-result v13

    const/4 v15, 0x2

    if-ne v15, v13, :cond_6

    invoke-virtual {v4}, Lorg/bouncycastle/asn1/DERTaggedObject;->getObject()Lorg/bouncycastle/asn1/ASN1Primitive;

    move-result-object v4

    check-cast v4, Lorg/bouncycastle/asn1/ASN1OctetString;

    invoke-virtual {v4}, Lorg/bouncycastle/asn1/ASN1OctetString;->getOctets()[B

    move-result-object v4

    invoke-interface {v5, v4}, Lorg/etsi/uri/x01903/v13/ResponderIDType;->setByKey([B)V

    goto :goto_2

    :cond_6
    invoke-virtual {v4}, Lorg/bouncycastle/asn1/DERTaggedObject;->getObject()Lorg/bouncycastle/asn1/ASN1Primitive;

    move-result-object v4

    invoke-static {v4}, Lorg/bouncycastle/asn1/x500/X500Name;->getInstance(Ljava/lang/Object;)Lorg/bouncycastle/asn1/x500/X500Name;

    move-result-object v4

    invoke-virtual {v4}, Lorg/bouncycastle/asn1/x500/X500Name;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v5, v4}, Lorg/etsi/uri/x01903/v13/ResponderIDType;->setByName(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_2

    :catch_1
    move-exception v0

    new-instance v2, Ljava/lang/RuntimeException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "OCSP decoding error: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2

    :cond_7
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    const/4 v3, 0x0

    invoke-interface {v7, v3}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v4

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {v2}, Lorg/apache/xmlbeans/XmlTokenSource;->getDomNode()Lorg/w3c/dom/Node;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {v9}, Lorg/apache/xmlbeans/XmlTokenSource;->getDomNode()Lorg/w3c/dom/Node;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {v10}, Lorg/apache/xmlbeans/XmlTokenSource;->getDomNode()Lorg/w3c/dom/Node;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v2, Lorg/apache/poi/poifs/crypt/dsig/services/RevocationData;

    invoke-direct {v2}, Lorg/apache/poi/poifs/crypt/dsig/services/RevocationData;-><init>()V

    sget-object v3, Lorg/apache/poi/poifs/crypt/dsig/facets/XAdESXLSignatureFacet;->LOG:Lorg/apache/poi/util/POILogger;

    const-string v4, "creating XAdES-X time-stamp"

    filled-new-array {v4}, [Ljava/lang/Object;

    move-result-object v4

    const/4 v5, 0x1

    invoke-virtual {v3, v5, v4}, Lorg/apache/poi/util/POILogger;->log(I[Ljava/lang/Object;)V

    invoke-direct {v1, v0, v2}, Lorg/apache/poi/poifs/crypt/dsig/facets/XAdESXLSignatureFacet;->createXAdESTimeStamp(Ljava/util/List;Lorg/apache/poi/poifs/crypt/dsig/services/RevocationData;)Lorg/etsi/uri/x01903/v13/XAdESTimeStampType;

    move-result-object v0

    invoke-virtual {v2}, Lorg/apache/poi/poifs/crypt/dsig/services/RevocationData;->hasRevocationDataEntries()Z

    move-result v3

    if-eqz v3, :cond_8

    invoke-direct {v1, v2}, Lorg/apache/poi/poifs/crypt/dsig/facets/XAdESXLSignatureFacet;->createValidationData(Lorg/apache/poi/poifs/crypt/dsig/services/RevocationData;)Lorg/etsi/uri/x01903/v14/ValidationDataType;

    move-result-object v2

    invoke-static {v8, v2}, Lorg/apache/poi/poifs/crypt/dsig/facets/XAdESSignatureFacet;->insertXChild(Lorg/apache/xmlbeans/XmlObject;Lorg/apache/xmlbeans/XmlObject;)V

    :cond_8
    invoke-interface {v8}, Lorg/etsi/uri/x01903/v13/UnsignedSignaturePropertiesType;->addNewSigAndRefsTimeStamp()Lorg/etsi/uri/x01903/v13/XAdESTimeStampType;

    move-result-object v2

    invoke-interface {v2, v0}, Lorg/apache/xmlbeans/XmlObject;->set(Lorg/apache/xmlbeans/XmlObject;)Lorg/apache/xmlbeans/XmlObject;

    invoke-interface {v8}, Lorg/etsi/uri/x01903/v13/UnsignedSignaturePropertiesType;->addNewCertificateValues()Lorg/etsi/uri/x01903/v13/CertificateValuesType;

    move-result-object v0

    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_9

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/security/cert/X509Certificate;

    invoke-interface {v0}, Lorg/etsi/uri/x01903/v13/CertificateValuesType;->addNewEncapsulatedX509Certificate()Lorg/etsi/uri/x01903/v13/EncapsulatedPKIDataType;

    move-result-object v4

    :try_start_3
    invoke-virtual {v3}, Ljava/security/cert/Certificate;->getEncoded()[B

    move-result-object v3

    invoke-interface {v4, v3}, Lorg/apache/xmlbeans/XmlBase64Binary;->setByteArrayValue([B)V
    :try_end_3
    .catch Ljava/security/cert/CertificateEncodingException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_3

    :catch_2
    move-exception v0

    new-instance v2, Ljava/lang/RuntimeException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "certificate encoding error: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2

    :cond_9
    invoke-interface {v8}, Lorg/etsi/uri/x01903/v13/UnsignedSignaturePropertiesType;->addNewRevocationValues()Lorg/etsi/uri/x01903/v13/RevocationValuesType;

    move-result-object v0

    invoke-direct {v1, v0, v12}, Lorg/apache/poi/poifs/crypt/dsig/facets/XAdESXLSignatureFacet;->createRevocationValues(Lorg/etsi/uri/x01903/v13/RevocationValuesType;Lorg/apache/poi/poifs/crypt/dsig/services/RevocationData;)V

    invoke-interface {v6}, Lorg/apache/xmlbeans/XmlTokenSource;->getDomNode()Lorg/w3c/dom/Node;

    move-result-object v0

    move-object/from16 v2, p1

    const/4 v3, 0x1

    invoke-interface {v2, v0, v3}, Lorg/w3c/dom/Document;->importNode(Lorg/w3c/dom/Node;Z)Lorg/w3c/dom/Node;

    move-result-object v0

    move-object/from16 v2, v18

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v4

    invoke-interface {v4}, Lorg/w3c/dom/Node;->getParentNode()Lorg/w3c/dom/Node;

    move-result-object v4

    invoke-interface {v2, v3}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v2

    invoke-interface {v4, v0, v2}, Lorg/w3c/dom/Node;->replaceChild(Lorg/w3c/dom/Node;Lorg/w3c/dom/Node;)Lorg/w3c/dom/Node;

    return-void

    :cond_a
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v2, "SignatureValue is not set."

    invoke-direct {v0, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :catch_3
    move-exception v0

    new-instance v2, Ljavax/xml/crypto/MarshalException;

    invoke-direct {v2, v0}, Ljavax/xml/crypto/MarshalException;-><init>(Ljava/lang/Throwable;)V

    throw v2

    :cond_b
    new-instance v0, Ljavax/xml/crypto/MarshalException;

    const-string v2, "no XAdES-BES extension present"

    invoke-direct {v0, v2}, Ljavax/xml/crypto/MarshalException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.class public Lorg/apache/poi/poifs/crypt/dsig/SignatureInfo$SignaturePart;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/poi/poifs/crypt/dsig/SignatureInfo;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "SignaturePart"
.end annotation


# instance fields
.field private certChain:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/security/cert/X509Certificate;",
            ">;"
        }
    .end annotation
.end field

.field private final signaturePart:Lorg/apache/poi/openxml4j/opc/PackagePart;

.field private signer:Ljava/security/cert/X509Certificate;

.field final synthetic this$0:Lorg/apache/poi/poifs/crypt/dsig/SignatureInfo;


# direct methods
.method private constructor <init>(Lorg/apache/poi/poifs/crypt/dsig/SignatureInfo;Lorg/apache/poi/openxml4j/opc/PackagePart;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureInfo$SignaturePart;->this$0:Lorg/apache/poi/poifs/crypt/dsig/SignatureInfo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureInfo$SignaturePart;->signaturePart:Lorg/apache/poi/openxml4j/opc/PackagePart;

    return-void
.end method

.method public synthetic constructor <init>(Lorg/apache/poi/poifs/crypt/dsig/SignatureInfo;Lorg/apache/poi/openxml4j/opc/PackagePart;Lorg/apache/poi/poifs/crypt/dsig/SignatureInfo$1;)V
    .locals 0

    .line 2
    invoke-direct {p0, p1, p2}, Lorg/apache/poi/poifs/crypt/dsig/SignatureInfo$SignaturePart;-><init>(Lorg/apache/poi/poifs/crypt/dsig/SignatureInfo;Lorg/apache/poi/openxml4j/opc/PackagePart;)V

    return-void
.end method


# virtual methods
.method public getCertChain()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/security/cert/X509Certificate;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureInfo$SignaturePart;->certChain:Ljava/util/List;

    return-object v0
.end method

.method public getPackagePart()Lorg/apache/poi/openxml4j/opc/PackagePart;
    .locals 1

    iget-object v0, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureInfo$SignaturePart;->signaturePart:Lorg/apache/poi/openxml4j/opc/PackagePart;

    return-object v0
.end method

.method public getSignatureDocument()Lorg/w3/x2000/x09/xmldsig/SignatureDocument;
    .locals 2

    iget-object v0, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureInfo$SignaturePart;->signaturePart:Lorg/apache/poi/openxml4j/opc/PackagePart;

    invoke-virtual {v0}, Lorg/apache/poi/openxml4j/opc/PackagePart;->getInputStream()Ljava/io/InputStream;

    move-result-object v0

    sget-object v1, Lorg/apache/poi/POIXMLTypeLoader;->DEFAULT_XML_OPTIONS:Lorg/apache/xmlbeans/XmlOptions;

    invoke-static {v0, v1}, Lorg/w3/x2000/x09/xmldsig/SignatureDocument$Factory;->parse(Ljava/io/InputStream;Lorg/apache/xmlbeans/XmlOptions;)Lorg/w3/x2000/x09/xmldsig/SignatureDocument;

    move-result-object v0

    return-object v0
.end method

.method public getSigner()Ljava/security/cert/X509Certificate;
    .locals 1

    iget-object v0, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureInfo$SignaturePart;->signer:Ljava/security/cert/X509Certificate;

    return-object v0
.end method

.method public validate()Z
    .locals 9

    new-instance v0, Lorg/apache/poi/poifs/crypt/dsig/KeyInfoKeySelector;

    invoke-direct {v0}, Lorg/apache/poi/poifs/crypt/dsig/KeyInfoKeySelector;-><init>()V

    const/4 v1, 0x7

    :try_start_0
    iget-object v2, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureInfo$SignaturePart;->signaturePart:Lorg/apache/poi/openxml4j/opc/PackagePart;

    invoke-virtual {v2}, Lorg/apache/poi/openxml4j/opc/PackagePart;->getInputStream()Ljava/io/InputStream;

    move-result-object v2

    invoke-static {v2}, Lorg/apache/poi/util/DocumentHelper;->readDocument(Ljava/io/InputStream;)Lorg/w3c/dom/Document;

    move-result-object v2

    invoke-static {}, Ljavax/xml/xpath/XPathFactory;->newInstance()Ljavax/xml/xpath/XPathFactory;

    move-result-object v3

    invoke-virtual {v3}, Ljavax/xml/xpath/XPathFactory;->newXPath()Ljavax/xml/xpath/XPath;

    move-result-object v3

    const-string v4, "//*[@Id]"

    invoke-interface {v3, v4}, Ljavax/xml/xpath/XPath;->compile(Ljava/lang/String;)Ljavax/xml/xpath/XPathExpression;

    move-result-object v3

    sget-object v4, Ljavax/xml/xpath/XPathConstants;->NODESET:Ljavax/xml/namespace/QName;

    invoke-interface {v3, v2, v4}, Ljavax/xml/xpath/XPathExpression;->evaluate(Ljava/lang/Object;Ljavax/xml/namespace/QName;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/w3c/dom/NodeList;

    invoke-interface {v3}, Lorg/w3c/dom/NodeList;->getLength()I

    move-result v4

    const/4 v5, 0x0

    :goto_0
    if-ge v5, v4, :cond_0

    invoke-interface {v3, v5}, Lorg/w3c/dom/NodeList;->item(I)Lorg/w3c/dom/Node;

    move-result-object v6

    check-cast v6, Lorg/w3c/dom/Element;

    const-string v7, "Id"

    const/4 v8, 0x1

    invoke-interface {v6, v7, v8}, Lorg/w3c/dom/Element;->setIdAttribute(Ljava/lang/String;Z)V

    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    :cond_0
    new-instance v3, Ljavax/xml/crypto/dsig/dom/DOMValidateContext;

    invoke-direct {v3, v0, v2}, Ljavax/xml/crypto/dsig/dom/DOMValidateContext;-><init>(Ljavax/xml/crypto/KeySelector;Lorg/w3c/dom/Node;)V

    const-string v2, "org.jcp.xml.dsig.validateManifests"

    sget-object v4, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v3, v2, v4}, Ljavax/xml/crypto/dsig/dom/DOMValidateContext;->setProperty(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v2, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureInfo$SignaturePart;->this$0:Lorg/apache/poi/poifs/crypt/dsig/SignatureInfo;

    invoke-static {v2}, Lorg/apache/poi/poifs/crypt/dsig/SignatureInfo;->access$000(Lorg/apache/poi/poifs/crypt/dsig/SignatureInfo;)Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->getUriDereferencer()Ljavax/xml/crypto/URIDereferencer;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljavax/xml/crypto/dsig/dom/DOMValidateContext;->setURIDereferencer(Ljavax/xml/crypto/URIDereferencer;)V

    iget-object v2, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureInfo$SignaturePart;->this$0:Lorg/apache/poi/poifs/crypt/dsig/SignatureInfo;

    invoke-static {v2, v3}, Lorg/apache/poi/poifs/crypt/dsig/SignatureInfo;->access$100(Lorg/apache/poi/poifs/crypt/dsig/SignatureInfo;Ljavax/xml/crypto/dsig/XMLValidateContext;)V

    iget-object v2, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureInfo$SignaturePart;->this$0:Lorg/apache/poi/poifs/crypt/dsig/SignatureInfo;

    invoke-static {v2}, Lorg/apache/poi/poifs/crypt/dsig/SignatureInfo;->access$000(Lorg/apache/poi/poifs/crypt/dsig/SignatureInfo;)Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->getSignatureFactory()Ljavax/xml/crypto/dsig/XMLSignatureFactory;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljavax/xml/crypto/dsig/XMLSignatureFactory;->unmarshalXMLSignature(Ljavax/xml/crypto/dsig/XMLValidateContext;)Ljavax/xml/crypto/dsig/XMLSignature;

    move-result-object v2

    invoke-interface {v2}, Ljavax/xml/crypto/dsig/XMLSignature;->getSignedInfo()Ljavax/xml/crypto/dsig/SignedInfo;

    move-result-object v4

    invoke-interface {v4}, Ljavax/xml/crypto/dsig/SignedInfo;->getReferences()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljavax/xml/crypto/dsig/Reference;

    invoke-static {v5}, Lorg/apache/poi/poifs/crypt/dsig/facets/SignatureFacet;->brokenJvmWorkaround(Ljavax/xml/crypto/dsig/Reference;)V

    goto :goto_1

    :cond_1
    invoke-interface {v2}, Ljavax/xml/crypto/dsig/XMLSignature;->getObjects()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljavax/xml/crypto/dsig/XMLObject;

    invoke-interface {v5}, Ljavax/xml/crypto/dsig/XMLObject;->getContent()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_3
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljavax/xml/crypto/XMLStructure;

    instance-of v7, v6, Ljavax/xml/crypto/dsig/Manifest;

    if-eqz v7, :cond_3

    check-cast v6, Ljavax/xml/crypto/dsig/Manifest;

    invoke-interface {v6}, Ljavax/xml/crypto/dsig/Manifest;->getReferences()Ljava/util/List;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljavax/xml/crypto/dsig/Reference;

    invoke-static {v7}, Lorg/apache/poi/poifs/crypt/dsig/facets/SignatureFacet;->brokenJvmWorkaround(Ljavax/xml/crypto/dsig/Reference;)V

    goto :goto_2

    :cond_4
    invoke-interface {v2, v3}, Ljavax/xml/crypto/dsig/XMLSignature;->validate(Ljavax/xml/crypto/dsig/XMLValidateContext;)Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-virtual {v0}, Lorg/apache/poi/poifs/crypt/dsig/KeyInfoKeySelector;->getSigner()Ljava/security/cert/X509Certificate;

    move-result-object v3

    iput-object v3, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureInfo$SignaturePart;->signer:Ljava/security/cert/X509Certificate;

    invoke-virtual {v0}, Lorg/apache/poi/poifs/crypt/dsig/KeyInfoKeySelector;->getCertChain()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureInfo$SignaturePart;->certChain:Ljava/util/List;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Lorg/xml/sax/SAXException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljavax/xml/xpath/XPathExpressionException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljavax/xml/crypto/MarshalException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljavax/xml/crypto/dsig/XMLSignatureException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_5
    return v2

    :catch_0
    move-exception v0

    invoke-static {}, Lorg/apache/poi/poifs/crypt/dsig/SignatureInfo;->access$200()Lorg/apache/poi/util/POILogger;

    move-result-object v2

    const-string v3, "error in validating the signature"

    filled-new-array {v3, v0}, [Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v2, v1, v4}, Lorg/apache/poi/util/POILogger;->log(I[Ljava/lang/Object;)V

    new-instance v1, Lorg/apache/poi/EncryptedDocumentException;

    invoke-direct {v1, v3, v0}, Lorg/apache/poi/EncryptedDocumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    :catch_1
    move-exception v0

    invoke-static {}, Lorg/apache/poi/poifs/crypt/dsig/SignatureInfo;->access$200()Lorg/apache/poi/util/POILogger;

    move-result-object v2

    const-string v3, "error in unmarshalling the signature"

    filled-new-array {v3, v0}, [Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v2, v1, v4}, Lorg/apache/poi/util/POILogger;->log(I[Ljava/lang/Object;)V

    new-instance v1, Lorg/apache/poi/EncryptedDocumentException;

    invoke-direct {v1, v3, v0}, Lorg/apache/poi/EncryptedDocumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    :catch_2
    move-exception v0

    invoke-static {}, Lorg/apache/poi/poifs/crypt/dsig/SignatureInfo;->access$200()Lorg/apache/poi/util/POILogger;

    move-result-object v2

    const-string v3, "error in searching document with xpath expression"

    filled-new-array {v3, v0}, [Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v2, v1, v4}, Lorg/apache/poi/util/POILogger;->log(I[Ljava/lang/Object;)V

    new-instance v1, Lorg/apache/poi/EncryptedDocumentException;

    invoke-direct {v1, v3, v0}, Lorg/apache/poi/EncryptedDocumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    :catch_3
    move-exception v0

    invoke-static {}, Lorg/apache/poi/poifs/crypt/dsig/SignatureInfo;->access$200()Lorg/apache/poi/util/POILogger;

    move-result-object v2

    const-string v3, "error in parsing document"

    filled-new-array {v3, v0}, [Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v2, v1, v4}, Lorg/apache/poi/util/POILogger;->log(I[Ljava/lang/Object;)V

    new-instance v1, Lorg/apache/poi/EncryptedDocumentException;

    invoke-direct {v1, v3, v0}, Lorg/apache/poi/EncryptedDocumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    :catch_4
    move-exception v0

    invoke-static {}, Lorg/apache/poi/poifs/crypt/dsig/SignatureInfo;->access$200()Lorg/apache/poi/util/POILogger;

    move-result-object v2

    const-string v3, "error in reading document"

    filled-new-array {v3, v0}, [Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v2, v1, v4}, Lorg/apache/poi/util/POILogger;->log(I[Ljava/lang/Object;)V

    new-instance v1, Lorg/apache/poi/EncryptedDocumentException;

    invoke-direct {v1, v3, v0}, Lorg/apache/poi/EncryptedDocumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

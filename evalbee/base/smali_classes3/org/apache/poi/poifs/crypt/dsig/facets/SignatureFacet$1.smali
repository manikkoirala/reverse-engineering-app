.class final Lorg/apache/poi/poifs/crypt/dsig/facets/SignatureFacet$1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/security/PrivilegedAction;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/poi/poifs/crypt/dsig/facets/SignatureFacet;->brokenJvmWorkaround(Ljavax/xml/crypto/dsig/Reference;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/security/PrivilegedAction<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic val$bcProv:Ljava/security/Provider;

.field final synthetic val$digestMethod:Ljavax/xml/crypto/dsig/DigestMethod;

.field final synthetic val$reference:Ljavax/xml/crypto/dsig/Reference;


# direct methods
.method public constructor <init>(Ljavax/xml/crypto/dsig/DigestMethod;Ljava/security/Provider;Ljavax/xml/crypto/dsig/Reference;)V
    .locals 0

    iput-object p1, p0, Lorg/apache/poi/poifs/crypt/dsig/facets/SignatureFacet$1;->val$digestMethod:Ljavax/xml/crypto/dsig/DigestMethod;

    iput-object p2, p0, Lorg/apache/poi/poifs/crypt/dsig/facets/SignatureFacet$1;->val$bcProv:Ljava/security/Provider;

    iput-object p3, p0, Lorg/apache/poi/poifs/crypt/dsig/facets/SignatureFacet$1;->val$reference:Ljavax/xml/crypto/dsig/Reference;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic run()Ljava/lang/Object;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lorg/apache/poi/poifs/crypt/dsig/facets/SignatureFacet$1;->run()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method public run()Ljava/lang/Void;
    .locals 4
    .annotation build Lorg/apache/poi/util/SuppressForbidden;
        value = "Workaround for a bug, needs access to private JDK members (may fail in Java 9): https://bugzilla.redhat.com/show_bug.cgi?id=1155012"
    .end annotation

    .line 2
    :try_start_0
    const-class v0, Lorg/apache/jcp/xml/dsig/internal/dom/DOMDigestMethod;

    const-string v1, "getMessageDigestAlgorithm"

    const/4 v2, 0x0

    new-array v3, v2, [Ljava/lang/Class;

    invoke-virtual {v0, v1, v3}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/reflect/AccessibleObject;->setAccessible(Z)V

    iget-object v3, p0, Lorg/apache/poi/poifs/crypt/dsig/facets/SignatureFacet$1;->val$digestMethod:Ljavax/xml/crypto/dsig/DigestMethod;

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v3, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v2, p0, Lorg/apache/poi/poifs/crypt/dsig/facets/SignatureFacet$1;->val$bcProv:Ljava/security/Provider;

    invoke-static {v0, v2}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;Ljava/security/Provider;)Ljava/security/MessageDigest;

    move-result-object v0

    const-class v2, Lorg/apache/jcp/xml/dsig/internal/dom/DOMReference;

    const-string v3, "md"

    invoke-virtual {v2, v3}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/reflect/AccessibleObject;->setAccessible(Z)V

    iget-object v1, p0, Lorg/apache/poi/poifs/crypt/dsig/facets/SignatureFacet$1;->val$reference:Ljavax/xml/crypto/dsig/Reference;

    invoke-virtual {v2, v1, v0}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-static {}, Lorg/apache/poi/poifs/crypt/dsig/facets/SignatureFacet;->access$000()Lorg/apache/poi/util/POILogger;

    move-result-object v1

    const-string v2, "Can\'t overwrite message digest (workaround for https://bugzilla.redhat.com/show_bug.cgi?id=1155012)"

    filled-new-array {v2, v0}, [Ljava/lang/Object;

    move-result-object v0

    const/4 v2, 0x5

    invoke-virtual {v1, v2, v0}, Lorg/apache/poi/util/POILogger;->log(I[Ljava/lang/Object;)V

    :goto_0
    const/4 v0, 0x0

    return-object v0
.end method

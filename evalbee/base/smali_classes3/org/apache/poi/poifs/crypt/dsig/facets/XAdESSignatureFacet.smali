.class public Lorg/apache/poi/poifs/crypt/dsig/facets/XAdESSignatureFacet;
.super Lorg/apache/poi/poifs/crypt/dsig/facets/SignatureFacet;
.source "SourceFile"


# static fields
.field private static final LOG:Lorg/apache/poi/util/POILogger;

.field private static final XADES_TYPE:Ljava/lang/String; = "http://uri.etsi.org/01903#SignedProperties"


# instance fields
.field private dataObjectFormatMimeTypes:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    const-class v0, Lorg/apache/poi/poifs/crypt/dsig/facets/XAdESSignatureFacet;

    invoke-static {v0}, Lorg/apache/poi/util/POILogFactory;->getLogger(Ljava/lang/Class;)Lorg/apache/poi/util/POILogger;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/poifs/crypt/dsig/facets/XAdESSignatureFacet;->LOG:Lorg/apache/poi/util/POILogger;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lorg/apache/poi/poifs/crypt/dsig/facets/SignatureFacet;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/poi/poifs/crypt/dsig/facets/XAdESSignatureFacet;->dataObjectFormatMimeTypes:Ljava/util/Map;

    return-void
.end method

.method public static insertXChild(Lorg/apache/xmlbeans/XmlObject;Lorg/apache/xmlbeans/XmlObject;)V
    .locals 0

    invoke-interface {p0}, Lorg/apache/xmlbeans/XmlTokenSource;->newCursor()Lorg/apache/xmlbeans/XmlCursor;

    move-result-object p0

    invoke-interface {p0}, Lorg/apache/xmlbeans/XmlCursor;->toEndToken()Lorg/apache/xmlbeans/XmlCursor$TokenType;

    invoke-interface {p1}, Lorg/apache/xmlbeans/XmlTokenSource;->newCursor()Lorg/apache/xmlbeans/XmlCursor;

    move-result-object p1

    invoke-interface {p1}, Lorg/apache/xmlbeans/XmlCursor;->toNextToken()Lorg/apache/xmlbeans/XmlCursor$TokenType;

    invoke-interface {p1, p0}, Lorg/apache/xmlbeans/XmlCursor;->moveXml(Lorg/apache/xmlbeans/XmlCursor;)Z

    invoke-interface {p1}, Lorg/apache/xmlbeans/XmlCursor;->dispose()V

    invoke-interface {p0}, Lorg/apache/xmlbeans/XmlCursor;->dispose()V

    return-void
.end method

.method public static setCertID(Lorg/etsi/uri/x01903/v13/CertIDType;Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;ZLjava/security/cert/X509Certificate;)V
    .locals 3

    invoke-interface {p0}, Lorg/etsi/uri/x01903/v13/CertIDType;->addNewIssuerSerial()Lorg/w3/x2000/x09/xmldsig/X509IssuerSerialType;

    move-result-object v0

    if-eqz p2, :cond_0

    invoke-virtual {p3}, Ljava/security/cert/X509Certificate;->getIssuerDN()Ljava/security/Principal;

    move-result-object p2

    invoke-interface {p2}, Ljava/security/Principal;->getName()Ljava/lang/String;

    move-result-object p2

    const-string v1, ","

    const-string v2, ", "

    invoke-virtual {p2, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object p2

    goto :goto_0

    :cond_0
    invoke-virtual {p3}, Ljava/security/cert/X509Certificate;->getIssuerX500Principal()Ljavax/security/auth/x500/X500Principal;

    move-result-object p2

    invoke-virtual {p2}, Ljavax/security/auth/x500/X500Principal;->toString()Ljava/lang/String;

    move-result-object p2

    :goto_0
    invoke-interface {v0, p2}, Lorg/w3/x2000/x09/xmldsig/X509IssuerSerialType;->setX509IssuerName(Ljava/lang/String;)V

    invoke-virtual {p3}, Ljava/security/cert/X509Certificate;->getSerialNumber()Ljava/math/BigInteger;

    move-result-object p2

    invoke-interface {v0, p2}, Lorg/w3/x2000/x09/xmldsig/X509IssuerSerialType;->setX509SerialNumber(Ljava/math/BigInteger;)V

    :try_start_0
    invoke-virtual {p3}, Ljava/security/cert/Certificate;->getEncoded()[B

    move-result-object p2
    :try_end_0
    .catch Ljava/security/cert/CertificateEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    invoke-interface {p0}, Lorg/etsi/uri/x01903/v13/CertIDType;->addNewCertDigest()Lorg/etsi/uri/x01903/v13/DigestAlgAndValueType;

    move-result-object p0

    invoke-virtual {p1}, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->getXadesDigestAlgo()Lorg/apache/poi/poifs/crypt/HashAlgorithm;

    move-result-object p1

    invoke-static {p0, p2, p1}, Lorg/apache/poi/poifs/crypt/dsig/facets/XAdESSignatureFacet;->setDigestAlgAndValue(Lorg/etsi/uri/x01903/v13/DigestAlgAndValueType;[BLorg/apache/poi/poifs/crypt/HashAlgorithm;)V

    return-void

    :catch_0
    move-exception p0

    new-instance p1, Ljava/lang/RuntimeException;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string p3, "certificate encoding error: "

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2, p0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw p1
.end method

.method public static setDigestAlgAndValue(Lorg/etsi/uri/x01903/v13/DigestAlgAndValueType;[BLorg/apache/poi/poifs/crypt/HashAlgorithm;)V
    .locals 2

    invoke-interface {p0}, Lorg/etsi/uri/x01903/v13/DigestAlgAndValueType;->addNewDigestMethod()Lorg/w3/x2000/x09/xmldsig/DigestMethodType;

    move-result-object v0

    invoke-static {p2}, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->getDigestMethodUri(Lorg/apache/poi/poifs/crypt/HashAlgorithm;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/w3/x2000/x09/xmldsig/DigestMethodType;->setAlgorithm(Ljava/lang/String;)V

    invoke-static {p2}, Lorg/apache/poi/poifs/crypt/CryptoFunctions;->getMessageDigest(Lorg/apache/poi/poifs/crypt/HashAlgorithm;)Ljava/security/MessageDigest;

    move-result-object p2

    invoke-virtual {p2, p1}, Ljava/security/MessageDigest;->digest([B)[B

    move-result-object p1

    invoke-interface {p0, p1}, Lorg/etsi/uri/x01903/v13/DigestAlgAndValueType;->setDigestValue([B)V

    return-void
.end method


# virtual methods
.method public addMimeType(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lorg/apache/poi/poifs/crypt/dsig/facets/XAdESSignatureFacet;->dataObjectFormatMimeTypes:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public preSign(Lorg/w3c/dom/Document;Ljava/util/List;Ljava/util/List;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/w3c/dom/Document;",
            "Ljava/util/List<",
            "Ljavax/xml/crypto/dsig/Reference;",
            ">;",
            "Ljava/util/List<",
            "Ljavax/xml/crypto/dsig/XMLObject;",
            ">;)V"
        }
    .end annotation

    sget-object v0, Lorg/apache/poi/poifs/crypt/dsig/facets/XAdESSignatureFacet;->LOG:Lorg/apache/poi/util/POILogger;

    const-string v1, "preSign"

    filled-new-array {v1}, [Ljava/lang/Object;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lorg/apache/poi/util/POILogger;->log(I[Ljava/lang/Object;)V

    invoke-static {}, Lorg/etsi/uri/x01903/v13/QualifyingPropertiesDocument$Factory;->newInstance()Lorg/etsi/uri/x01903/v13/QualifyingPropertiesDocument;

    move-result-object v0

    invoke-interface {v0}, Lorg/etsi/uri/x01903/v13/QualifyingPropertiesDocument;->addNewQualifyingProperties()Lorg/etsi/uri/x01903/v13/QualifyingPropertiesType;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "#"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, p0, Lorg/apache/poi/poifs/crypt/dsig/facets/SignatureFacet;->signatureConfig:Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;

    invoke-virtual {v4}, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->getPackageSignatureId()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lorg/etsi/uri/x01903/v13/QualifyingPropertiesType;->setTarget(Ljava/lang/String;)V

    invoke-interface {v0}, Lorg/etsi/uri/x01903/v13/QualifyingPropertiesType;->addNewSignedProperties()Lorg/etsi/uri/x01903/v13/SignedPropertiesType;

    move-result-object v1

    iget-object v4, p0, Lorg/apache/poi/poifs/crypt/dsig/facets/SignatureFacet;->signatureConfig:Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;

    invoke-virtual {v4}, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->getXadesSignatureId()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v1, v4}, Lorg/etsi/uri/x01903/v13/SignedPropertiesType;->setId(Ljava/lang/String;)V

    invoke-interface {v1}, Lorg/etsi/uri/x01903/v13/SignedPropertiesType;->addNewSignedSignatureProperties()Lorg/etsi/uri/x01903/v13/SignedSignaturePropertiesType;

    move-result-object v4

    const-string v5, "Z"

    invoke-static {v5}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v5

    sget-object v6, Ljava/util/Locale;->ROOT:Ljava/util/Locale;

    invoke-static {v5, v6}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;Ljava/util/Locale;)Ljava/util/Calendar;

    move-result-object v5

    iget-object v6, p0, Lorg/apache/poi/poifs/crypt/dsig/facets/SignatureFacet;->signatureConfig:Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;

    invoke-virtual {v6}, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->getExecutionTime()Ljava/util/Date;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/util/Calendar;->setTime(Ljava/util/Date;)V

    const/16 v6, 0xe

    invoke-virtual {v5, v6}, Ljava/util/Calendar;->clear(I)V

    invoke-interface {v4, v5}, Lorg/etsi/uri/x01903/v13/SignedSignaturePropertiesType;->setSigningTime(Ljava/util/Calendar;)V

    iget-object v5, p0, Lorg/apache/poi/poifs/crypt/dsig/facets/SignatureFacet;->signatureConfig:Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;

    invoke-virtual {v5}, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->getSigningCertificateChain()Ljava/util/List;

    move-result-object v5

    if-eqz v5, :cond_4

    iget-object v5, p0, Lorg/apache/poi/poifs/crypt/dsig/facets/SignatureFacet;->signatureConfig:Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;

    invoke-virtual {v5}, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->getSigningCertificateChain()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_4

    invoke-interface {v4}, Lorg/etsi/uri/x01903/v13/SignedSignaturePropertiesType;->addNewSigningCertificate()Lorg/etsi/uri/x01903/v13/CertIDListType;

    move-result-object v5

    invoke-interface {v5}, Lorg/etsi/uri/x01903/v13/CertIDListType;->addNewCert()Lorg/etsi/uri/x01903/v13/CertIDType;

    move-result-object v5

    iget-object v6, p0, Lorg/apache/poi/poifs/crypt/dsig/facets/SignatureFacet;->signatureConfig:Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;

    invoke-virtual {v6}, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->getSigningCertificateChain()Ljava/util/List;

    move-result-object v6

    const/4 v7, 0x0

    invoke-interface {v6, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/security/cert/X509Certificate;

    iget-object v7, p0, Lorg/apache/poi/poifs/crypt/dsig/facets/SignatureFacet;->signatureConfig:Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;

    invoke-virtual {v7}, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->isXadesIssuerNameNoReverseOrder()Z

    move-result v8

    invoke-static {v5, v7, v8, v6}, Lorg/apache/poi/poifs/crypt/dsig/facets/XAdESSignatureFacet;->setCertID(Lorg/etsi/uri/x01903/v13/CertIDType;Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;ZLjava/security/cert/X509Certificate;)V

    iget-object v5, p0, Lorg/apache/poi/poifs/crypt/dsig/facets/SignatureFacet;->signatureConfig:Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;

    invoke-virtual {v5}, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->getXadesRole()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_0

    invoke-virtual {v5}, Ljava/lang/String;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_0

    invoke-interface {v4}, Lorg/etsi/uri/x01903/v13/SignedSignaturePropertiesType;->addNewSignerRole()Lorg/etsi/uri/x01903/v13/SignerRoleType;

    move-result-object v6

    invoke-interface {v4, v6}, Lorg/etsi/uri/x01903/v13/SignedSignaturePropertiesType;->setSignerRole(Lorg/etsi/uri/x01903/v13/SignerRoleType;)V

    invoke-interface {v6}, Lorg/etsi/uri/x01903/v13/SignerRoleType;->addNewClaimedRoles()Lorg/etsi/uri/x01903/v13/ClaimedRolesListType;

    move-result-object v6

    invoke-interface {v6}, Lorg/etsi/uri/x01903/v13/ClaimedRolesListType;->addNewClaimedRole()Lorg/etsi/uri/x01903/v13/AnyType;

    move-result-object v6

    invoke-static {}, Lorg/apache/xmlbeans/XmlString$Factory;->newInstance()Lorg/apache/xmlbeans/XmlString;

    move-result-object v7

    invoke-interface {v7, v5}, Lorg/apache/xmlbeans/XmlAnySimpleType;->setStringValue(Ljava/lang/String;)V

    invoke-static {v6, v7}, Lorg/apache/poi/poifs/crypt/dsig/facets/XAdESSignatureFacet;->insertXChild(Lorg/apache/xmlbeans/XmlObject;Lorg/apache/xmlbeans/XmlObject;)V

    :cond_0
    iget-object v5, p0, Lorg/apache/poi/poifs/crypt/dsig/facets/SignatureFacet;->signatureConfig:Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;

    invoke-virtual {v5}, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->getSignaturePolicyService()Lorg/apache/poi/poifs/crypt/dsig/services/SignaturePolicyService;

    move-result-object v5

    if-eqz v5, :cond_1

    invoke-interface {v4}, Lorg/etsi/uri/x01903/v13/SignedSignaturePropertiesType;->addNewSignaturePolicyIdentifier()Lorg/etsi/uri/x01903/v13/SignaturePolicyIdentifierType;

    move-result-object v4

    invoke-interface {v4}, Lorg/etsi/uri/x01903/v13/SignaturePolicyIdentifierType;->addNewSignaturePolicyId()Lorg/etsi/uri/x01903/v13/SignaturePolicyIdType;

    move-result-object v4

    invoke-interface {v4}, Lorg/etsi/uri/x01903/v13/SignaturePolicyIdType;->addNewSigPolicyId()Lorg/etsi/uri/x01903/v13/ObjectIdentifierType;

    move-result-object v6

    invoke-interface {v5}, Lorg/apache/poi/poifs/crypt/dsig/services/SignaturePolicyService;->getSignaturePolicyDescription()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, v7}, Lorg/etsi/uri/x01903/v13/ObjectIdentifierType;->setDescription(Ljava/lang/String;)V

    invoke-interface {v6}, Lorg/etsi/uri/x01903/v13/ObjectIdentifierType;->addNewIdentifier()Lorg/etsi/uri/x01903/v13/IdentifierType;

    move-result-object v6

    invoke-interface {v5}, Lorg/apache/poi/poifs/crypt/dsig/services/SignaturePolicyService;->getSignaturePolicyIdentifier()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, v7}, Lorg/etsi/uri/x01903/v13/IdentifierType;->setStringValue(Ljava/lang/String;)V

    invoke-interface {v5}, Lorg/apache/poi/poifs/crypt/dsig/services/SignaturePolicyService;->getSignaturePolicyDocument()[B

    move-result-object v6

    invoke-interface {v4}, Lorg/etsi/uri/x01903/v13/SignaturePolicyIdType;->addNewSigPolicyHash()Lorg/etsi/uri/x01903/v13/DigestAlgAndValueType;

    move-result-object v7

    iget-object v8, p0, Lorg/apache/poi/poifs/crypt/dsig/facets/SignatureFacet;->signatureConfig:Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;

    invoke-virtual {v8}, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->getDigestAlgo()Lorg/apache/poi/poifs/crypt/HashAlgorithm;

    move-result-object v8

    invoke-static {v7, v6, v8}, Lorg/apache/poi/poifs/crypt/dsig/facets/XAdESSignatureFacet;->setDigestAlgAndValue(Lorg/etsi/uri/x01903/v13/DigestAlgAndValueType;[BLorg/apache/poi/poifs/crypt/HashAlgorithm;)V

    invoke-interface {v5}, Lorg/apache/poi/poifs/crypt/dsig/services/SignaturePolicyService;->getSignaturePolicyDownloadUrl()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_2

    invoke-interface {v4}, Lorg/etsi/uri/x01903/v13/SignaturePolicyIdType;->addNewSigPolicyQualifiers()Lorg/etsi/uri/x01903/v13/SigPolicyQualifiersListType;

    move-result-object v4

    invoke-interface {v4}, Lorg/etsi/uri/x01903/v13/SigPolicyQualifiersListType;->addNewSigPolicyQualifier()Lorg/etsi/uri/x01903/v13/AnyType;

    move-result-object v4

    invoke-static {}, Lorg/apache/xmlbeans/XmlString$Factory;->newInstance()Lorg/apache/xmlbeans/XmlString;

    move-result-object v6

    invoke-interface {v6, v5}, Lorg/apache/xmlbeans/XmlAnySimpleType;->setStringValue(Ljava/lang/String;)V

    invoke-static {v4, v6}, Lorg/apache/poi/poifs/crypt/dsig/facets/XAdESSignatureFacet;->insertXChild(Lorg/apache/xmlbeans/XmlObject;Lorg/apache/xmlbeans/XmlObject;)V

    goto :goto_0

    :cond_1
    iget-object v5, p0, Lorg/apache/poi/poifs/crypt/dsig/facets/SignatureFacet;->signatureConfig:Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;

    invoke-virtual {v5}, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->isXadesSignaturePolicyImplied()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v4}, Lorg/etsi/uri/x01903/v13/SignedSignaturePropertiesType;->addNewSignaturePolicyIdentifier()Lorg/etsi/uri/x01903/v13/SignaturePolicyIdentifierType;

    move-result-object v4

    invoke-interface {v4}, Lorg/etsi/uri/x01903/v13/SignaturePolicyIdentifierType;->addNewSignaturePolicyImplied()Lorg/apache/xmlbeans/XmlObject;

    :cond_2
    :goto_0
    iget-object v4, p0, Lorg/apache/poi/poifs/crypt/dsig/facets/XAdESSignatureFacet;->dataObjectFormatMimeTypes:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_3

    invoke-interface {v1}, Lorg/etsi/uri/x01903/v13/SignedPropertiesType;->addNewSignedDataObjectProperties()Lorg/etsi/uri/x01903/v13/SignedDataObjectPropertiesType;

    move-result-object v1

    invoke-interface {v1}, Lorg/etsi/uri/x01903/v13/SignedDataObjectPropertiesType;->getDataObjectFormatList()Ljava/util/List;

    move-result-object v1

    iget-object v4, p0, Lorg/apache/poi/poifs/crypt/dsig/facets/XAdESSignatureFacet;->dataObjectFormatMimeTypes:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/Map$Entry;

    invoke-static {}, Lorg/etsi/uri/x01903/v13/DataObjectFormatType$Factory;->newInstance()Lorg/etsi/uri/x01903/v13/DataObjectFormatType;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {v5}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, v7}, Lorg/etsi/uri/x01903/v13/DataObjectFormatType;->setObjectReference(Ljava/lang/String;)V

    invoke-interface {v5}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-interface {v6, v5}, Lorg/etsi/uri/x01903/v13/DataObjectFormatType;->setMimeType(Ljava/lang/String;)V

    invoke-interface {v1, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v0}, Lorg/apache/xmlbeans/XmlTokenSource;->getDomNode()Lorg/w3c/dom/Node;

    move-result-object v0

    check-cast v0, Lorg/w3c/dom/Element;

    invoke-interface {p1, v0, v2}, Lorg/w3c/dom/Document;->importNode(Lorg/w3c/dom/Node;Z)Lorg/w3c/dom/Node;

    move-result-object p1

    check-cast p1, Lorg/w3c/dom/Element;

    new-instance v0, Ljavax/xml/crypto/dom/DOMStructure;

    invoke-direct {v0, p1}, Ljavax/xml/crypto/dom/DOMStructure;-><init>(Lorg/w3c/dom/Node;)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0}, Lorg/apache/poi/poifs/crypt/dsig/facets/SignatureFacet;->getSignatureFactory()Ljavax/xml/crypto/dsig/XMLSignatureFactory;

    move-result-object p1

    const/4 v0, 0x0

    invoke-virtual {p1, v1, v0, v0, v0}, Ljavax/xml/crypto/dsig/XMLSignatureFactory;->newXMLObject(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljavax/xml/crypto/dsig/XMLObject;

    move-result-object p1

    invoke-interface {p3, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    const-string p1, "http://www.w3.org/TR/2001/REC-xml-c14n-20010315"

    invoke-virtual {p0, p1}, Lorg/apache/poi/poifs/crypt/dsig/facets/SignatureFacet;->newTransform(Ljava/lang/String;)Ljavax/xml/crypto/dsig/Transform;

    move-result-object p1

    invoke-interface {v6, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p3, p0, Lorg/apache/poi/poifs/crypt/dsig/facets/SignatureFacet;->signatureConfig:Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;

    invoke-virtual {p3}, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->getXadesSignatureId()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const-string v7, "http://uri.etsi.org/01903#SignedProperties"

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object v4, p0

    invoke-virtual/range {v4 .. v9}, Lorg/apache/poi/poifs/crypt/dsig/facets/SignatureFacet;->newReference(Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;[B)Ljavax/xml/crypto/dsig/Reference;

    move-result-object p1

    invoke-interface {p2, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void

    :cond_4
    new-instance p1, Ljava/lang/RuntimeException;

    const-string p2, "no signing certificate chain available"

    invoke-direct {p1, p2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.class Lorg/apache/poi/poifs/crypt/dsig/SignatureInfo$1$1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/poi/poifs/crypt/dsig/SignatureInfo$1;->iterator()Ljava/util/Iterator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Iterator<",
        "Lorg/apache/poi/poifs/crypt/dsig/SignatureInfo$SignaturePart;",
        ">;"
    }
.end annotation


# instance fields
.field pkg:Lorg/apache/poi/openxml4j/opc/OPCPackage;

.field sigOrigRels:Ljava/util/Iterator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Iterator<",
            "Lorg/apache/poi/openxml4j/opc/PackageRelationship;",
            ">;"
        }
    .end annotation
.end field

.field sigPart:Lorg/apache/poi/openxml4j/opc/PackagePart;

.field sigRels:Ljava/util/Iterator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Iterator<",
            "Lorg/apache/poi/openxml4j/opc/PackageRelationship;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$1:Lorg/apache/poi/poifs/crypt/dsig/SignatureInfo$1;


# direct methods
.method public constructor <init>(Lorg/apache/poi/poifs/crypt/dsig/SignatureInfo$1;)V
    .locals 1

    iput-object p1, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureInfo$1$1;->this$1:Lorg/apache/poi/poifs/crypt/dsig/SignatureInfo$1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iget-object p1, p1, Lorg/apache/poi/poifs/crypt/dsig/SignatureInfo$1;->this$0:Lorg/apache/poi/poifs/crypt/dsig/SignatureInfo;

    invoke-static {p1}, Lorg/apache/poi/poifs/crypt/dsig/SignatureInfo;->access$000(Lorg/apache/poi/poifs/crypt/dsig/SignatureInfo;)Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;

    move-result-object p1

    invoke-virtual {p1}, Lorg/apache/poi/poifs/crypt/dsig/SignatureConfig;->getOpcPackage()Lorg/apache/poi/openxml4j/opc/OPCPackage;

    move-result-object p1

    iput-object p1, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureInfo$1$1;->pkg:Lorg/apache/poi/openxml4j/opc/OPCPackage;

    const-string v0, "http://schemas.openxmlformats.org/package/2006/relationships/digital-signature/origin"

    invoke-virtual {p1, v0}, Lorg/apache/poi/openxml4j/opc/OPCPackage;->getRelationshipsByType(Ljava/lang/String;)Lorg/apache/poi/openxml4j/opc/PackageRelationshipCollection;

    move-result-object p1

    invoke-virtual {p1}, Lorg/apache/poi/openxml4j/opc/PackageRelationshipCollection;->iterator()Ljava/util/Iterator;

    move-result-object p1

    iput-object p1, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureInfo$1$1;->sigOrigRels:Ljava/util/Iterator;

    const/4 p1, 0x0

    iput-object p1, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureInfo$1$1;->sigRels:Ljava/util/Iterator;

    iput-object p1, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureInfo$1$1;->sigPart:Lorg/apache/poi/openxml4j/opc/PackagePart;

    return-void
.end method


# virtual methods
.method public hasNext()Z
    .locals 4

    :goto_0
    iget-object v0, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureInfo$1$1;->sigRels:Ljava/util/Iterator;

    const/4 v1, 0x1

    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_1

    :cond_0
    return v1

    :cond_1
    :goto_1
    iget-object v0, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureInfo$1$1;->sigOrigRels:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x0

    return v0

    :cond_2
    iget-object v0, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureInfo$1$1;->pkg:Lorg/apache/poi/openxml4j/opc/OPCPackage;

    iget-object v2, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureInfo$1$1;->sigOrigRels:Ljava/util/Iterator;

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/poi/openxml4j/opc/PackageRelationship;

    invoke-virtual {v0, v2}, Lorg/apache/poi/openxml4j/opc/OPCPackage;->getPart(Lorg/apache/poi/openxml4j/opc/PackageRelationship;)Lorg/apache/poi/openxml4j/opc/PackagePart;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureInfo$1$1;->sigPart:Lorg/apache/poi/openxml4j/opc/PackagePart;

    invoke-static {}, Lorg/apache/poi/poifs/crypt/dsig/SignatureInfo;->access$200()Lorg/apache/poi/util/POILogger;

    move-result-object v0

    const-string v2, "Digital Signature Origin part"

    iget-object v3, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureInfo$1$1;->sigPart:Lorg/apache/poi/openxml4j/opc/PackagePart;

    filled-new-array {v2, v3}, [Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/apache/poi/util/POILogger;->log(I[Ljava/lang/Object;)V

    :try_start_0
    iget-object v0, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureInfo$1$1;->sigPart:Lorg/apache/poi/openxml4j/opc/PackagePart;

    const-string v1, "http://schemas.openxmlformats.org/package/2006/relationships/digital-signature/signature"

    invoke-virtual {v0, v1}, Lorg/apache/poi/openxml4j/opc/PackagePart;->getRelationshipsByType(Ljava/lang/String;)Lorg/apache/poi/openxml4j/opc/PackageRelationshipCollection;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/poi/openxml4j/opc/PackageRelationshipCollection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureInfo$1$1;->sigRels:Ljava/util/Iterator;
    :try_end_0
    .catch Lorg/apache/poi/openxml4j/exceptions/InvalidFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-static {}, Lorg/apache/poi/poifs/crypt/dsig/SignatureInfo;->access$200()Lorg/apache/poi/util/POILogger;

    move-result-object v1

    const-string v2, "Reference to signature is invalid."

    filled-new-array {v2, v0}, [Ljava/lang/Object;

    move-result-object v0

    const/4 v2, 0x5

    invoke-virtual {v1, v2, v0}, Lorg/apache/poi/util/POILogger;->log(I[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public bridge synthetic next()Ljava/lang/Object;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lorg/apache/poi/poifs/crypt/dsig/SignatureInfo$1$1;->next()Lorg/apache/poi/poifs/crypt/dsig/SignatureInfo$SignaturePart;

    move-result-object v0

    return-object v0
.end method

.method public next()Lorg/apache/poi/poifs/crypt/dsig/SignatureInfo$SignaturePart;
    .locals 6

    .line 2
    const/4 v0, 0x0

    move-object v1, v0

    :cond_0
    :try_start_0
    invoke-virtual {p0}, Lorg/apache/poi/poifs/crypt/dsig/SignatureInfo$1$1;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureInfo$1$1;->sigPart:Lorg/apache/poi/openxml4j/opc/PackagePart;

    iget-object v3, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureInfo$1$1;->sigRels:Ljava/util/Iterator;

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/poi/openxml4j/opc/PackageRelationship;

    invoke-virtual {v2, v3}, Lorg/apache/poi/openxml4j/opc/PackagePart;->getRelatedPart(Lorg/apache/poi/openxml4j/opc/PackageRelationship;)Lorg/apache/poi/openxml4j/opc/PackagePart;

    move-result-object v1

    invoke-static {}, Lorg/apache/poi/poifs/crypt/dsig/SignatureInfo;->access$200()Lorg/apache/poi/util/POILogger;

    move-result-object v2

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const-string v4, "XML Signature part"

    const/4 v5, 0x0

    aput-object v4, v3, v5

    const/4 v4, 0x1

    aput-object v1, v3, v4

    invoke-virtual {v2, v4, v3}, Lorg/apache/poi/util/POILogger;->log(I[Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    new-instance v2, Ljava/util/NoSuchElementException;

    invoke-direct {v2}, Ljava/util/NoSuchElementException;-><init>()V

    throw v2
    :try_end_0
    .catch Lorg/apache/poi/openxml4j/exceptions/InvalidFormatException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v2

    invoke-static {}, Lorg/apache/poi/poifs/crypt/dsig/SignatureInfo;->access$200()Lorg/apache/poi/util/POILogger;

    move-result-object v3

    const-string v4, "Reference to signature is invalid."

    filled-new-array {v4, v2}, [Ljava/lang/Object;

    move-result-object v2

    const/4 v4, 0x5

    invoke-virtual {v3, v4, v2}, Lorg/apache/poi/util/POILogger;->log(I[Ljava/lang/Object;)V

    :goto_0
    iget-object v2, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureInfo$1$1;->sigPart:Lorg/apache/poi/openxml4j/opc/PackagePart;

    if-eqz v2, :cond_0

    new-instance v2, Lorg/apache/poi/poifs/crypt/dsig/SignatureInfo$SignaturePart;

    iget-object v3, p0, Lorg/apache/poi/poifs/crypt/dsig/SignatureInfo$1$1;->this$1:Lorg/apache/poi/poifs/crypt/dsig/SignatureInfo$1;

    iget-object v3, v3, Lorg/apache/poi/poifs/crypt/dsig/SignatureInfo$1;->this$0:Lorg/apache/poi/poifs/crypt/dsig/SignatureInfo;

    invoke-direct {v2, v3, v1, v0}, Lorg/apache/poi/poifs/crypt/dsig/SignatureInfo$SignaturePart;-><init>(Lorg/apache/poi/poifs/crypt/dsig/SignatureInfo;Lorg/apache/poi/openxml4j/opc/PackagePart;Lorg/apache/poi/poifs/crypt/dsig/SignatureInfo$1;)V

    return-object v2
.end method

.method public remove()V
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.class public final enum Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/poi/hssf/util/HSSFColor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "HSSFColorPredefined"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

.field public static final enum AQUA:Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

.field public static final enum AUTOMATIC:Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

.field public static final enum BLACK:Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

.field public static final enum BLUE:Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

.field public static final enum BLUE_GREY:Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

.field public static final enum BRIGHT_GREEN:Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

.field public static final enum BROWN:Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

.field public static final enum CORAL:Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

.field public static final enum CORNFLOWER_BLUE:Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

.field public static final enum DARK_BLUE:Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

.field public static final enum DARK_GREEN:Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

.field public static final enum DARK_RED:Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

.field public static final enum DARK_TEAL:Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

.field public static final enum DARK_YELLOW:Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

.field public static final enum GOLD:Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

.field public static final enum GREEN:Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

.field public static final enum GREY_25_PERCENT:Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

.field public static final enum GREY_40_PERCENT:Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

.field public static final enum GREY_50_PERCENT:Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

.field public static final enum GREY_80_PERCENT:Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

.field public static final enum INDIGO:Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

.field public static final enum LAVENDER:Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

.field public static final enum LEMON_CHIFFON:Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

.field public static final enum LIGHT_BLUE:Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

.field public static final enum LIGHT_CORNFLOWER_BLUE:Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

.field public static final enum LIGHT_GREEN:Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

.field public static final enum LIGHT_ORANGE:Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

.field public static final enum LIGHT_TURQUOISE:Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

.field public static final enum LIGHT_YELLOW:Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

.field public static final enum LIME:Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

.field public static final enum MAROON:Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

.field public static final enum OLIVE_GREEN:Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

.field public static final enum ORANGE:Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

.field public static final enum ORCHID:Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

.field public static final enum PALE_BLUE:Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

.field public static final enum PINK:Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

.field public static final enum PLUM:Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

.field public static final enum RED:Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

.field public static final enum ROSE:Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

.field public static final enum ROYAL_BLUE:Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

.field public static final enum SEA_GREEN:Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

.field public static final enum SKY_BLUE:Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

.field public static final enum TAN:Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

.field public static final enum TEAL:Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

.field public static final enum TURQUOISE:Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

.field public static final enum VIOLET:Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

.field public static final enum WHITE:Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

.field public static final enum YELLOW:Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;


# instance fields
.field private color:Lorg/apache/poi/hssf/util/HSSFColor;


# direct methods
.method public static constructor <clinit>()V
    .locals 68

    new-instance v7, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

    move-object v6, v7

    const-string v1, "BLACK"

    const/4 v2, 0x0

    const/16 v3, 0x8

    const/4 v4, -0x1

    const/4 v5, 0x0

    move-object v0, v7

    invoke-direct/range {v0 .. v5}, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;-><init>(Ljava/lang/String;IIII)V

    sput-object v7, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;->BLACK:Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

    new-instance v0, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

    move-object v7, v0

    const-string v9, "BROWN"

    const/4 v10, 0x1

    const/16 v11, 0x3c

    const/4 v12, -0x1

    const v13, 0x993300

    move-object v8, v0

    invoke-direct/range {v8 .. v13}, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;-><init>(Ljava/lang/String;IIII)V

    sput-object v0, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;->BROWN:Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

    new-instance v0, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

    move-object v8, v0

    const-string v15, "OLIVE_GREEN"

    const/16 v16, 0x2

    const/16 v17, 0x3b

    const/16 v18, -0x1

    const v19, 0x333300

    move-object v14, v0

    invoke-direct/range {v14 .. v19}, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;-><init>(Ljava/lang/String;IIII)V

    sput-object v0, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;->OLIVE_GREEN:Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

    new-instance v0, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

    move-object v9, v0

    const-string v21, "DARK_GREEN"

    const/16 v22, 0x3

    const/16 v23, 0x3a

    const/16 v24, -0x1

    const/16 v25, 0x3300

    move-object/from16 v20, v0

    invoke-direct/range {v20 .. v25}, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;-><init>(Ljava/lang/String;IIII)V

    sput-object v0, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;->DARK_GREEN:Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

    new-instance v0, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

    move-object v10, v0

    const-string v12, "DARK_TEAL"

    const/4 v13, 0x4

    const/16 v14, 0x38

    const/4 v15, -0x1

    const/16 v16, 0x3366

    move-object v11, v0

    invoke-direct/range {v11 .. v16}, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;-><init>(Ljava/lang/String;IIII)V

    sput-object v0, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;->DARK_TEAL:Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

    new-instance v0, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

    move-object v11, v0

    const-string v18, "DARK_BLUE"

    const/16 v19, 0x5

    const/16 v20, 0x12

    const/16 v21, 0x20

    const/16 v22, 0x80

    move-object/from16 v17, v0

    invoke-direct/range {v17 .. v22}, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;-><init>(Ljava/lang/String;IIII)V

    sput-object v0, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;->DARK_BLUE:Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

    new-instance v0, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

    move-object v12, v0

    const-string v24, "INDIGO"

    const/16 v25, 0x6

    const/16 v26, 0x3e

    const/16 v27, -0x1

    const v28, 0x333399

    move-object/from16 v23, v0

    invoke-direct/range {v23 .. v28}, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;-><init>(Ljava/lang/String;IIII)V

    sput-object v0, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;->INDIGO:Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

    new-instance v0, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

    move-object v13, v0

    const-string v15, "GREY_80_PERCENT"

    const/16 v16, 0x7

    const/16 v17, 0x3f

    const/16 v18, -0x1

    const v19, 0x333333

    move-object v14, v0

    invoke-direct/range {v14 .. v19}, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;-><init>(Ljava/lang/String;IIII)V

    sput-object v0, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;->GREY_80_PERCENT:Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

    new-instance v0, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

    move-object v14, v0

    const-string v21, "ORANGE"

    const/16 v22, 0x8

    const/16 v23, 0x35

    const/16 v24, -0x1

    const v25, 0xff6600

    move-object/from16 v20, v0

    invoke-direct/range {v20 .. v25}, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;-><init>(Ljava/lang/String;IIII)V

    sput-object v0, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;->ORANGE:Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

    new-instance v0, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

    move-object v15, v0

    const-string v27, "DARK_YELLOW"

    const/16 v28, 0x9

    const/16 v29, 0x13

    const/16 v30, -0x1

    const v31, 0x808000

    move-object/from16 v26, v0

    invoke-direct/range {v26 .. v31}, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;-><init>(Ljava/lang/String;IIII)V

    sput-object v0, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;->DARK_YELLOW:Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

    new-instance v0, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

    move-object/from16 v16, v0

    const-string v18, "GREEN"

    const/16 v19, 0xa

    const/16 v20, 0x11

    const/16 v21, -0x1

    const v22, 0x8000

    move-object/from16 v17, v0

    invoke-direct/range {v17 .. v22}, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;-><init>(Ljava/lang/String;IIII)V

    sput-object v0, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;->GREEN:Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

    new-instance v0, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

    move-object/from16 v17, v0

    const-string v24, "TEAL"

    const/16 v25, 0xb

    const/16 v26, 0x15

    const/16 v27, 0x26

    const v28, 0x8080

    move-object/from16 v23, v0

    invoke-direct/range {v23 .. v28}, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;-><init>(Ljava/lang/String;IIII)V

    sput-object v0, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;->TEAL:Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

    new-instance v0, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

    move-object/from16 v18, v0

    const-string v30, "BLUE"

    const/16 v31, 0xc

    const/16 v32, 0xc

    const/16 v33, 0x27

    const/16 v34, 0xff

    move-object/from16 v29, v0

    invoke-direct/range {v29 .. v34}, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;-><init>(Ljava/lang/String;IIII)V

    sput-object v0, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;->BLUE:Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

    new-instance v0, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

    move-object/from16 v19, v0

    const-string v21, "BLUE_GREY"

    const/16 v22, 0xd

    const/16 v23, 0x36

    const/16 v24, -0x1

    const v25, 0x666699

    move-object/from16 v20, v0

    invoke-direct/range {v20 .. v25}, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;-><init>(Ljava/lang/String;IIII)V

    sput-object v0, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;->BLUE_GREY:Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

    new-instance v0, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

    move-object/from16 v20, v0

    const-string v27, "GREY_50_PERCENT"

    const/16 v28, 0xe

    const/16 v29, 0x17

    const/16 v30, -0x1

    const v31, 0x808080

    move-object/from16 v26, v0

    invoke-direct/range {v26 .. v31}, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;-><init>(Ljava/lang/String;IIII)V

    sput-object v0, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;->GREY_50_PERCENT:Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

    new-instance v0, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

    move-object/from16 v21, v0

    const-string v33, "RED"

    const/16 v34, 0xf

    const/16 v35, 0xa

    const/16 v36, -0x1

    const/high16 v37, 0xff0000

    move-object/from16 v32, v0

    invoke-direct/range {v32 .. v37}, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;-><init>(Ljava/lang/String;IIII)V

    sput-object v0, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;->RED:Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

    new-instance v0, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

    move-object/from16 v22, v0

    const-string v24, "LIGHT_ORANGE"

    const/16 v25, 0x10

    const/16 v26, 0x34

    const/16 v27, -0x1

    const v28, 0xff9900

    move-object/from16 v23, v0

    invoke-direct/range {v23 .. v28}, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;-><init>(Ljava/lang/String;IIII)V

    sput-object v0, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;->LIGHT_ORANGE:Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

    new-instance v0, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

    move-object/from16 v23, v0

    const-string v30, "LIME"

    const/16 v31, 0x11

    const/16 v32, 0x32

    const/16 v33, -0x1

    const v34, 0x99cc00

    move-object/from16 v29, v0

    invoke-direct/range {v29 .. v34}, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;-><init>(Ljava/lang/String;IIII)V

    sput-object v0, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;->LIME:Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

    new-instance v0, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

    move-object/from16 v24, v0

    const-string v36, "SEA_GREEN"

    const/16 v37, 0x12

    const/16 v38, 0x39

    const/16 v39, -0x1

    const v40, 0x339966

    move-object/from16 v35, v0

    invoke-direct/range {v35 .. v40}, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;-><init>(Ljava/lang/String;IIII)V

    sput-object v0, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;->SEA_GREEN:Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

    new-instance v0, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

    move-object/from16 v25, v0

    const-string v27, "AQUA"

    const/16 v28, 0x13

    const/16 v29, 0x31

    const/16 v30, -0x1

    const v31, 0x33cccc

    move-object/from16 v26, v0

    invoke-direct/range {v26 .. v31}, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;-><init>(Ljava/lang/String;IIII)V

    sput-object v0, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;->AQUA:Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

    new-instance v0, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

    move-object/from16 v26, v0

    const-string v33, "LIGHT_BLUE"

    const/16 v34, 0x14

    const/16 v35, 0x30

    const/16 v36, -0x1

    const v37, 0x3366ff

    move-object/from16 v32, v0

    invoke-direct/range {v32 .. v37}, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;-><init>(Ljava/lang/String;IIII)V

    sput-object v0, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;->LIGHT_BLUE:Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

    new-instance v0, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

    move-object/from16 v27, v0

    const-string v39, "VIOLET"

    const/16 v40, 0x15

    const/16 v41, 0x14

    const/16 v42, 0x24

    const v43, 0x800080

    move-object/from16 v38, v0

    invoke-direct/range {v38 .. v43}, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;-><init>(Ljava/lang/String;IIII)V

    sput-object v0, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;->VIOLET:Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

    new-instance v0, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

    move-object/from16 v28, v0

    const-string v30, "GREY_40_PERCENT"

    const/16 v31, 0x16

    const/16 v32, 0x37

    const/16 v33, -0x1

    const v34, 0x969696

    move-object/from16 v29, v0

    invoke-direct/range {v29 .. v34}, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;-><init>(Ljava/lang/String;IIII)V

    sput-object v0, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;->GREY_40_PERCENT:Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

    new-instance v0, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

    move-object/from16 v29, v0

    const-string v36, "PINK"

    const/16 v37, 0x17

    const/16 v38, 0xe

    const/16 v39, 0x21

    const v40, 0xff00ff

    move-object/from16 v35, v0

    invoke-direct/range {v35 .. v40}, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;-><init>(Ljava/lang/String;IIII)V

    sput-object v0, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;->PINK:Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

    new-instance v0, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

    move-object/from16 v30, v0

    const-string v42, "GOLD"

    const/16 v43, 0x18

    const/16 v44, 0x33

    const/16 v45, -0x1

    const v46, 0xffcc00

    move-object/from16 v41, v0

    invoke-direct/range {v41 .. v46}, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;-><init>(Ljava/lang/String;IIII)V

    sput-object v0, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;->GOLD:Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

    new-instance v0, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

    move-object/from16 v31, v0

    const-string v33, "YELLOW"

    const/16 v34, 0x19

    const/16 v35, 0xd

    const/16 v36, 0x22

    const v37, 0xffff00

    move-object/from16 v32, v0

    invoke-direct/range {v32 .. v37}, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;-><init>(Ljava/lang/String;IIII)V

    sput-object v0, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;->YELLOW:Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

    new-instance v0, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

    move-object/from16 v32, v0

    const-string v39, "BRIGHT_GREEN"

    const/16 v40, 0x1a

    const/16 v41, 0xb

    const/16 v42, -0x1

    const v43, 0xff00

    move-object/from16 v38, v0

    invoke-direct/range {v38 .. v43}, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;-><init>(Ljava/lang/String;IIII)V

    sput-object v0, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;->BRIGHT_GREEN:Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

    new-instance v0, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

    move-object/from16 v33, v0

    const-string v45, "TURQUOISE"

    const/16 v46, 0x1b

    const/16 v47, 0xf

    const/16 v48, 0x23

    const v49, 0xffff

    move-object/from16 v44, v0

    invoke-direct/range {v44 .. v49}, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;-><init>(Ljava/lang/String;IIII)V

    sput-object v0, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;->TURQUOISE:Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

    new-instance v0, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

    move-object/from16 v34, v0

    const-string v36, "DARK_RED"

    const/16 v37, 0x1c

    const/16 v38, 0x10

    const/16 v39, 0x25

    const/high16 v40, 0x800000

    move-object/from16 v35, v0

    invoke-direct/range {v35 .. v40}, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;-><init>(Ljava/lang/String;IIII)V

    sput-object v0, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;->DARK_RED:Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

    new-instance v0, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

    move-object/from16 v35, v0

    const-string v42, "SKY_BLUE"

    const/16 v43, 0x1d

    const/16 v44, 0x28

    const/16 v45, -0x1

    const v46, 0xccff

    move-object/from16 v41, v0

    invoke-direct/range {v41 .. v46}, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;-><init>(Ljava/lang/String;IIII)V

    sput-object v0, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;->SKY_BLUE:Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

    new-instance v0, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

    move-object/from16 v36, v0

    const-string v48, "PLUM"

    const/16 v49, 0x1e

    const/16 v50, 0x3d

    const/16 v51, 0x19

    const v52, 0x993366

    move-object/from16 v47, v0

    invoke-direct/range {v47 .. v52}, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;-><init>(Ljava/lang/String;IIII)V

    sput-object v0, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;->PLUM:Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

    new-instance v0, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

    move-object/from16 v37, v0

    const-string v39, "GREY_25_PERCENT"

    const/16 v40, 0x1f

    const/16 v41, 0x16

    const/16 v42, -0x1

    const v43, 0xc0c0c0

    move-object/from16 v38, v0

    invoke-direct/range {v38 .. v43}, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;-><init>(Ljava/lang/String;IIII)V

    sput-object v0, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;->GREY_25_PERCENT:Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

    new-instance v0, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

    move-object/from16 v38, v0

    const-string v45, "ROSE"

    const/16 v46, 0x20

    const/16 v47, 0x2d

    const/16 v48, -0x1

    const v49, 0xff99cc

    move-object/from16 v44, v0

    invoke-direct/range {v44 .. v49}, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;-><init>(Ljava/lang/String;IIII)V

    sput-object v0, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;->ROSE:Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

    new-instance v0, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

    move-object/from16 v39, v0

    const-string v51, "LIGHT_YELLOW"

    const/16 v52, 0x21

    const/16 v53, 0x2b

    const/16 v54, -0x1

    const v55, 0xffff99

    move-object/from16 v50, v0

    invoke-direct/range {v50 .. v55}, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;-><init>(Ljava/lang/String;IIII)V

    sput-object v0, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;->LIGHT_YELLOW:Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

    new-instance v0, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

    move-object/from16 v40, v0

    const-string v42, "LIGHT_GREEN"

    const/16 v43, 0x22

    const/16 v44, 0x2a

    const/16 v45, -0x1

    const v46, 0xccffcc

    move-object/from16 v41, v0

    invoke-direct/range {v41 .. v46}, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;-><init>(Ljava/lang/String;IIII)V

    sput-object v0, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;->LIGHT_GREEN:Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

    new-instance v0, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

    move-object/from16 v41, v0

    const-string v48, "LIGHT_TURQUOISE"

    const/16 v49, 0x23

    const/16 v50, 0x29

    const/16 v51, 0x1b

    const v52, 0xccffff

    move-object/from16 v47, v0

    invoke-direct/range {v47 .. v52}, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;-><init>(Ljava/lang/String;IIII)V

    sput-object v0, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;->LIGHT_TURQUOISE:Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

    new-instance v0, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

    move-object/from16 v42, v0

    const-string v54, "PALE_BLUE"

    const/16 v55, 0x24

    const/16 v56, 0x2c

    const/16 v57, -0x1

    const v58, 0x99ccff

    move-object/from16 v53, v0

    invoke-direct/range {v53 .. v58}, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;-><init>(Ljava/lang/String;IIII)V

    sput-object v0, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;->PALE_BLUE:Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

    new-instance v0, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

    move-object/from16 v43, v0

    const-string v45, "LAVENDER"

    const/16 v46, 0x25

    const/16 v47, 0x2e

    const/16 v48, -0x1

    const v49, 0xcc99ff

    move-object/from16 v44, v0

    invoke-direct/range {v44 .. v49}, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;-><init>(Ljava/lang/String;IIII)V

    sput-object v0, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;->LAVENDER:Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

    new-instance v0, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

    move-object/from16 v44, v0

    const-string v51, "WHITE"

    const/16 v52, 0x26

    const/16 v53, 0x9

    const/16 v54, -0x1

    const v55, 0xffffff

    move-object/from16 v50, v0

    invoke-direct/range {v50 .. v55}, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;-><init>(Ljava/lang/String;IIII)V

    sput-object v0, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;->WHITE:Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

    new-instance v0, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

    move-object/from16 v45, v0

    const-string v57, "CORNFLOWER_BLUE"

    const/16 v58, 0x27

    const/16 v59, 0x18

    const/16 v60, -0x1

    const v61, 0x9999ff

    move-object/from16 v56, v0

    invoke-direct/range {v56 .. v61}, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;-><init>(Ljava/lang/String;IIII)V

    sput-object v0, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;->CORNFLOWER_BLUE:Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

    new-instance v0, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

    move-object/from16 v46, v0

    const-string v48, "LEMON_CHIFFON"

    const/16 v49, 0x28

    const/16 v50, 0x1a

    const/16 v51, -0x1

    const v52, 0xffffcc

    move-object/from16 v47, v0

    invoke-direct/range {v47 .. v52}, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;-><init>(Ljava/lang/String;IIII)V

    sput-object v0, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;->LEMON_CHIFFON:Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

    new-instance v0, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

    move-object/from16 v47, v0

    const-string v54, "MAROON"

    const/16 v55, 0x29

    const/16 v56, 0x19

    const/16 v57, -0x1

    const/high16 v58, 0x7f0000

    move-object/from16 v53, v0

    invoke-direct/range {v53 .. v58}, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;-><init>(Ljava/lang/String;IIII)V

    sput-object v0, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;->MAROON:Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

    new-instance v0, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

    move-object/from16 v48, v0

    const-string v60, "ORCHID"

    const/16 v61, 0x2a

    const/16 v62, 0x1c

    const/16 v63, -0x1

    const v64, 0x660066

    move-object/from16 v59, v0

    invoke-direct/range {v59 .. v64}, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;-><init>(Ljava/lang/String;IIII)V

    sput-object v0, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;->ORCHID:Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

    new-instance v0, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

    move-object/from16 v49, v0

    const-string v51, "CORAL"

    const/16 v52, 0x2b

    const/16 v53, 0x1d

    const/16 v54, -0x1

    const v55, 0xff8080

    move-object/from16 v50, v0

    invoke-direct/range {v50 .. v55}, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;-><init>(Ljava/lang/String;IIII)V

    sput-object v0, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;->CORAL:Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

    new-instance v0, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

    move-object/from16 v50, v0

    const-string v57, "ROYAL_BLUE"

    const/16 v58, 0x2c

    const/16 v59, 0x1e

    const/16 v60, -0x1

    const/16 v61, 0x66cc

    move-object/from16 v56, v0

    invoke-direct/range {v56 .. v61}, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;-><init>(Ljava/lang/String;IIII)V

    sput-object v0, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;->ROYAL_BLUE:Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

    new-instance v0, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

    move-object/from16 v51, v0

    const-string v63, "LIGHT_CORNFLOWER_BLUE"

    const/16 v64, 0x2d

    const/16 v65, 0x1f

    const/16 v66, -0x1

    const v67, 0xccccff

    move-object/from16 v62, v0

    invoke-direct/range {v62 .. v67}, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;-><init>(Ljava/lang/String;IIII)V

    sput-object v0, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;->LIGHT_CORNFLOWER_BLUE:Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

    new-instance v0, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

    move-object/from16 v52, v0

    const-string v54, "TAN"

    const/16 v55, 0x2e

    const/16 v56, 0x2f

    const/16 v57, -0x1

    const v58, 0xffcc99

    move-object/from16 v53, v0

    invoke-direct/range {v53 .. v58}, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;-><init>(Ljava/lang/String;IIII)V

    sput-object v0, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;->TAN:Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

    new-instance v0, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

    move-object/from16 v53, v0

    const-string v60, "AUTOMATIC"

    const/16 v61, 0x2f

    const/16 v62, 0x40

    const/16 v63, -0x1

    const/16 v64, 0x0

    move-object/from16 v59, v0

    invoke-direct/range {v59 .. v64}, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;-><init>(Ljava/lang/String;IIII)V

    sput-object v0, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;->AUTOMATIC:Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

    filled-new-array/range {v6 .. v53}, [Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;->$VALUES:[Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IIII)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(III)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    new-instance p1, Lorg/apache/poi/hssf/util/HSSFColor;

    new-instance p2, Ljava/awt/Color;

    invoke-direct {p2, p5}, Ljava/awt/Color;-><init>(I)V

    invoke-direct {p1, p3, p4, p2}, Lorg/apache/poi/hssf/util/HSSFColor;-><init>(IILjava/awt/Color;)V

    iput-object p1, p0, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;->color:Lorg/apache/poi/hssf/util/HSSFColor;

    return-void
.end method

.method public static synthetic access$100(Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;)Lorg/apache/poi/hssf/util/HSSFColor;
    .locals 0

    iget-object p0, p0, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;->color:Lorg/apache/poi/hssf/util/HSSFColor;

    return-object p0
.end method

.method public static valueOf(Ljava/lang/String;)Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;
    .locals 1

    const-class v0, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

    return-object p0
.end method

.method public static values()[Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;
    .locals 1

    sget-object v0, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;->$VALUES:[Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

    invoke-virtual {v0}, [Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;

    return-object v0
.end method


# virtual methods
.method public getColor()Lorg/apache/poi/hssf/util/HSSFColor;
    .locals 4

    new-instance v0, Lorg/apache/poi/hssf/util/HSSFColor;

    invoke-virtual {p0}, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;->getIndex()S

    move-result v1

    invoke-virtual {p0}, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;->getIndex2()S

    move-result v2

    iget-object v3, p0, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;->color:Lorg/apache/poi/hssf/util/HSSFColor;

    invoke-static {v3}, Lorg/apache/poi/hssf/util/HSSFColor;->access$000(Lorg/apache/poi/hssf/util/HSSFColor;)Ljava/awt/Color;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/poi/hssf/util/HSSFColor;-><init>(IILjava/awt/Color;)V

    return-object v0
.end method

.method public getHexString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;->color:Lorg/apache/poi/hssf/util/HSSFColor;

    invoke-virtual {v0}, Lorg/apache/poi/hssf/util/HSSFColor;->getHexString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getIndex()S
    .locals 1

    iget-object v0, p0, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;->color:Lorg/apache/poi/hssf/util/HSSFColor;

    invoke-virtual {v0}, Lorg/apache/poi/hssf/util/HSSFColor;->getIndex()S

    move-result v0

    return v0
.end method

.method public getIndex2()S
    .locals 1

    iget-object v0, p0, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;->color:Lorg/apache/poi/hssf/util/HSSFColor;

    invoke-virtual {v0}, Lorg/apache/poi/hssf/util/HSSFColor;->getIndex2()S

    move-result v0

    return v0
.end method

.method public getTriplet()[S
    .locals 1

    iget-object v0, p0, Lorg/apache/poi/hssf/util/HSSFColor$HSSFColorPredefined;->color:Lorg/apache/poi/hssf/util/HSSFColor;

    invoke-virtual {v0}, Lorg/apache/poi/hssf/util/HSSFColor;->getTriplet()[S

    move-result-object v0

    return-object v0
.end method

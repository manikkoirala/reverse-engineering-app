.class final Lorg/apache/poi/ss/formula/SheetRangeEvaluator;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/poi/ss/formula/SheetRange;


# instance fields
.field private final _firstSheetIndex:I

.field private final _lastSheetIndex:I

.field private _sheetEvaluators:[Lorg/apache/poi/ss/formula/SheetRefEvaluator;


# direct methods
.method public constructor <init>(II[Lorg/apache/poi/ss/formula/SheetRefEvaluator;)V
    .locals 3

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "."

    if-ltz p1, :cond_1

    if-lt p2, p1, :cond_0

    iput p1, p0, Lorg/apache/poi/ss/formula/SheetRangeEvaluator;->_firstSheetIndex:I

    iput p2, p0, Lorg/apache/poi/ss/formula/SheetRangeEvaluator;->_lastSheetIndex:I

    invoke-virtual {p3}, [Lorg/apache/poi/ss/formula/SheetRefEvaluator;->clone()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, [Lorg/apache/poi/ss/formula/SheetRefEvaluator;

    iput-object p1, p0, Lorg/apache/poi/ss/formula/SheetRangeEvaluator;->_sheetEvaluators:[Lorg/apache/poi/ss/formula/SheetRefEvaluator;

    return-void

    :cond_0
    new-instance p3, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid lastSheetIndex: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p2, " for firstSheetIndex: "

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p3, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p3

    :cond_1
    new-instance p2, Ljava/lang/IllegalArgumentException;

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Invalid firstSheetIndex: "

    invoke-virtual {p3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p2
.end method

.method public constructor <init>(ILorg/apache/poi/ss/formula/SheetRefEvaluator;)V
    .locals 0

    .line 2
    filled-new-array {p2}, [Lorg/apache/poi/ss/formula/SheetRefEvaluator;

    move-result-object p2

    invoke-direct {p0, p1, p1, p2}, Lorg/apache/poi/ss/formula/SheetRangeEvaluator;-><init>(II[Lorg/apache/poi/ss/formula/SheetRefEvaluator;)V

    return-void
.end method


# virtual methods
.method public getEvalForCell(III)Lorg/apache/poi/ss/formula/eval/ValueEval;
    .locals 0

    invoke-virtual {p0, p1}, Lorg/apache/poi/ss/formula/SheetRangeEvaluator;->getSheetEvaluator(I)Lorg/apache/poi/ss/formula/SheetRefEvaluator;

    move-result-object p1

    invoke-virtual {p1, p2, p3}, Lorg/apache/poi/ss/formula/SheetRefEvaluator;->getEvalForCell(II)Lorg/apache/poi/ss/formula/eval/ValueEval;

    move-result-object p1

    return-object p1
.end method

.method public getFirstSheetIndex()I
    .locals 1

    iget v0, p0, Lorg/apache/poi/ss/formula/SheetRangeEvaluator;->_firstSheetIndex:I

    return v0
.end method

.method public getLastSheetIndex()I
    .locals 1

    iget v0, p0, Lorg/apache/poi/ss/formula/SheetRangeEvaluator;->_lastSheetIndex:I

    return v0
.end method

.method public getSheetEvaluator(I)Lorg/apache/poi/ss/formula/SheetRefEvaluator;
    .locals 3

    iget v0, p0, Lorg/apache/poi/ss/formula/SheetRangeEvaluator;->_firstSheetIndex:I

    if-lt p1, v0, :cond_0

    iget v1, p0, Lorg/apache/poi/ss/formula/SheetRangeEvaluator;->_lastSheetIndex:I

    if-gt p1, v1, :cond_0

    iget-object v1, p0, Lorg/apache/poi/ss/formula/SheetRangeEvaluator;->_sheetEvaluators:[Lorg/apache/poi/ss/formula/SheetRefEvaluator;

    sub-int/2addr p1, v0

    aget-object p1, v1, p1

    return-object p1

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid SheetIndex: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, " - Outside range "

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget p1, p0, Lorg/apache/poi/ss/formula/SheetRangeEvaluator;->_firstSheetIndex:I

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, " : "

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget p1, p0, Lorg/apache/poi/ss/formula/SheetRangeEvaluator;->_lastSheetIndex:I

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getSheetName(I)Ljava/lang/String;
    .locals 0

    invoke-virtual {p0, p1}, Lorg/apache/poi/ss/formula/SheetRangeEvaluator;->getSheetEvaluator(I)Lorg/apache/poi/ss/formula/SheetRefEvaluator;

    move-result-object p1

    invoke-virtual {p1}, Lorg/apache/poi/ss/formula/SheetRefEvaluator;->getSheetName()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getSheetNameRange()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget v1, p0, Lorg/apache/poi/ss/formula/SheetRangeEvaluator;->_firstSheetIndex:I

    invoke-virtual {p0, v1}, Lorg/apache/poi/ss/formula/SheetRangeEvaluator;->getSheetName(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lorg/apache/poi/ss/formula/SheetRangeEvaluator;->_firstSheetIndex:I

    iget v2, p0, Lorg/apache/poi/ss/formula/SheetRangeEvaluator;->_lastSheetIndex:I

    if-eq v1, v2, :cond_0

    const/16 v1, 0x3a

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget v1, p0, Lorg/apache/poi/ss/formula/SheetRangeEvaluator;->_lastSheetIndex:I

    invoke-virtual {p0, v1}, Lorg/apache/poi/ss/formula/SheetRangeEvaluator;->getSheetName(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.class public Lorg/apache/poi/ss/format/CellDateFormatter;
.super Lorg/apache/poi/ss/format/CellFormatter;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/poi/ss/format/CellDateFormatter$DatePartHandler;
    }
.end annotation


# static fields
.field private static SIMPLE_DATE:Lorg/apache/poi/ss/format/CellDateFormatter;


# instance fields
.field private final EXCEL_EPOCH_CAL:Ljava/util/Calendar;

.field private amPmUpper:Z

.field private final dateFmt:Ljava/text/DateFormat;

.field private sFmt:Ljava/lang/String;

.field private showAmPm:Z

.field private showM:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .line 1
    invoke-static {}, Lorg/apache/poi/util/LocaleUtil;->getUserLocale()Ljava/util/Locale;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lorg/apache/poi/ss/format/CellDateFormatter;-><init>(Ljava/util/Locale;Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Ljava/util/Locale;Ljava/lang/String;)V
    .locals 3

    .line 2
    invoke-direct {p0, p2}, Lorg/apache/poi/ss/format/CellFormatter;-><init>(Ljava/lang/String;)V

    const/4 v0, 0x0

    const/4 v1, 0x1

    const/16 v2, 0x770

    invoke-static {v2, v0, v1}, Lorg/apache/poi/util/LocaleUtil;->getLocaleCalendar(III)Ljava/util/Calendar;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/poi/ss/format/CellDateFormatter;->EXCEL_EPOCH_CAL:Ljava/util/Calendar;

    new-instance v0, Lorg/apache/poi/ss/format/CellDateFormatter$DatePartHandler;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lorg/apache/poi/ss/format/CellDateFormatter$DatePartHandler;-><init>(Lorg/apache/poi/ss/format/CellDateFormatter;Lorg/apache/poi/ss/format/CellDateFormatter$1;)V

    sget-object v1, Lorg/apache/poi/ss/format/CellFormatType;->DATE:Lorg/apache/poi/ss/format/CellFormatType;

    invoke-static {p2, v1, v0}, Lorg/apache/poi/ss/format/CellFormatPart;->parseFormat(Ljava/lang/String;Lorg/apache/poi/ss/format/CellFormatType;Lorg/apache/poi/ss/format/CellFormatPart$PartHandler;)Ljava/lang/StringBuffer;

    move-result-object p2

    invoke-virtual {v0, p2}, Lorg/apache/poi/ss/format/CellDateFormatter$DatePartHandler;->finish(Ljava/lang/StringBuffer;)V

    invoke-virtual {p2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object p2

    const-string v0, "((y)(?!y))(?<!yy)"

    const-string v1, "yy"

    invoke-virtual {p2, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    new-instance v0, Ljava/text/SimpleDateFormat;

    invoke-direct {v0, p2, p1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    iput-object v0, p0, Lorg/apache/poi/ss/format/CellDateFormatter;->dateFmt:Ljava/text/DateFormat;

    invoke-static {}, Lorg/apache/poi/util/LocaleUtil;->getUserTimeZone()Ljava/util/TimeZone;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/text/DateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    return-void
.end method

.method public static synthetic access$002(Lorg/apache/poi/ss/format/CellDateFormatter;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, Lorg/apache/poi/ss/format/CellDateFormatter;->sFmt:Ljava/lang/String;

    return-object p1
.end method

.method public static synthetic access$100(Lorg/apache/poi/ss/format/CellDateFormatter;)Z
    .locals 0

    iget-boolean p0, p0, Lorg/apache/poi/ss/format/CellDateFormatter;->showAmPm:Z

    return p0
.end method

.method public static synthetic access$102(Lorg/apache/poi/ss/format/CellDateFormatter;Z)Z
    .locals 0

    iput-boolean p1, p0, Lorg/apache/poi/ss/format/CellDateFormatter;->showAmPm:Z

    return p1
.end method

.method public static synthetic access$200(Lorg/apache/poi/ss/format/CellDateFormatter;)Z
    .locals 0

    iget-boolean p0, p0, Lorg/apache/poi/ss/format/CellDateFormatter;->showM:Z

    return p0
.end method

.method public static synthetic access$202(Lorg/apache/poi/ss/format/CellDateFormatter;Z)Z
    .locals 0

    iput-boolean p1, p0, Lorg/apache/poi/ss/format/CellDateFormatter;->showM:Z

    return p1
.end method

.method public static synthetic access$302(Lorg/apache/poi/ss/format/CellDateFormatter;Z)Z
    .locals 0

    iput-boolean p1, p0, Lorg/apache/poi/ss/format/CellDateFormatter;->amPmUpper:Z

    return p1
.end method


# virtual methods
.method public formatValue(Ljava/lang/StringBuffer;Ljava/lang/Object;)V
    .locals 18

    move-object/from16 v1, p0

    move-object/from16 v0, p1

    if-nez p2, :cond_0

    const-wide/16 v2, 0x0

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    goto :goto_0

    :cond_0
    move-object/from16 v2, p2

    :goto_0
    instance-of v3, v2, Ljava/lang/Number;

    const-wide/16 v4, 0x3e8

    if-eqz v3, :cond_2

    check-cast v2, Ljava/lang/Number;

    invoke-virtual {v2}, Ljava/lang/Number;->longValue()J

    move-result-wide v2

    const-wide/16 v6, 0x0

    cmp-long v6, v2, v6

    if-nez v6, :cond_1

    iget-object v2, v1, Lorg/apache/poi/ss/format/CellDateFormatter;->EXCEL_EPOCH_CAL:Ljava/util/Calendar;

    invoke-virtual {v2}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v2

    goto :goto_1

    :cond_1
    iget-object v6, v1, Lorg/apache/poi/ss/format/CellDateFormatter;->EXCEL_EPOCH_CAL:Ljava/util/Calendar;

    invoke-virtual {v6}, Ljava/util/Calendar;->clone()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/Calendar;

    div-long v7, v2, v4

    long-to-int v7, v7

    const/16 v8, 0xd

    invoke-virtual {v6, v8, v7}, Ljava/util/Calendar;->add(II)V

    rem-long/2addr v2, v4

    long-to-int v2, v2

    const/16 v3, 0xe

    invoke-virtual {v6, v3, v2}, Ljava/util/Calendar;->add(II)V

    invoke-virtual {v6}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v2

    :cond_2
    :goto_1
    iget-object v3, v1, Lorg/apache/poi/ss/format/CellDateFormatter;->dateFmt:Ljava/text/DateFormat;

    invoke-virtual {v3, v2}, Ljava/text/Format;->formatToCharacterIterator(Ljava/lang/Object;)Ljava/text/AttributedCharacterIterator;

    move-result-object v3

    invoke-interface {v3}, Ljava/text/CharacterIterator;->first()C

    invoke-interface {v3}, Ljava/text/CharacterIterator;->first()C

    move-result v6

    const/4 v7, 0x0

    move v8, v7

    move v9, v8

    :goto_2
    const v10, 0xffff

    if-eq v6, v10, :cond_8

    sget-object v10, Ljava/text/DateFormat$Field;->MILLISECOND:Ljava/text/DateFormat$Field;

    invoke-interface {v3, v10}, Ljava/text/AttributedCharacterIterator;->getAttribute(Ljava/text/AttributedCharacterIterator$Attribute;)Ljava/lang/Object;

    move-result-object v10

    const/4 v11, 0x1

    if-eqz v10, :cond_3

    if-nez v8, :cond_7

    move-object v6, v2

    check-cast v6, Ljava/util/Date;

    invoke-virtual/range {p1 .. p1}, Ljava/lang/StringBuffer;->length()I

    move-result v8

    new-instance v10, Ljava/util/Formatter;

    sget-object v12, Ljava/util/Locale;->ROOT:Ljava/util/Locale;

    invoke-direct {v10, v0, v12}, Ljava/util/Formatter;-><init>(Ljava/lang/Appendable;Ljava/util/Locale;)V

    :try_start_0
    invoke-virtual {v6}, Ljava/util/Date;->getTime()J

    move-result-wide v12

    rem-long/2addr v12, v4

    iget-object v6, v1, Lorg/apache/poi/ss/format/CellFormatter;->locale:Ljava/util/Locale;

    iget-object v14, v1, Lorg/apache/poi/ss/format/CellDateFormatter;->sFmt:Ljava/lang/String;

    new-array v15, v11, [Ljava/lang/Object;

    long-to-double v12, v12

    const-wide v16, 0x408f400000000000L    # 1000.0

    div-double v12, v12, v16

    invoke-static {v12, v13}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v12

    aput-object v12, v15, v7

    invoke-virtual {v10, v6, v14, v15}, Ljava/util/Formatter;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/util/Formatter;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v10}, Ljava/util/Formatter;->close()V

    add-int/lit8 v6, v8, 0x2

    invoke-virtual {v0, v8, v6}, Ljava/lang/StringBuffer;->delete(II)Ljava/lang/StringBuffer;

    move v8, v11

    goto :goto_4

    :catchall_0
    move-exception v0

    invoke-virtual {v10}, Ljava/util/Formatter;->close()V

    throw v0

    :cond_3
    sget-object v10, Ljava/text/DateFormat$Field;->AM_PM:Ljava/text/DateFormat$Field;

    invoke-interface {v3, v10}, Ljava/text/AttributedCharacterIterator;->getAttribute(Ljava/text/AttributedCharacterIterator$Attribute;)Ljava/lang/Object;

    move-result-object v10

    if-eqz v10, :cond_6

    if-nez v9, :cond_7

    iget-boolean v9, v1, Lorg/apache/poi/ss/format/CellDateFormatter;->showAmPm:Z

    if-eqz v9, :cond_5

    iget-boolean v9, v1, Lorg/apache/poi/ss/format/CellDateFormatter;->amPmUpper:Z

    if-eqz v9, :cond_4

    invoke-static {v6}, Ljava/lang/Character;->toUpperCase(C)C

    move-result v6

    invoke-virtual {v0, v6}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    iget-boolean v6, v1, Lorg/apache/poi/ss/format/CellDateFormatter;->showM:Z

    if-eqz v6, :cond_5

    const/16 v6, 0x4d

    goto :goto_3

    :cond_4
    invoke-static {v6}, Ljava/lang/Character;->toLowerCase(C)C

    move-result v6

    invoke-virtual {v0, v6}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    iget-boolean v6, v1, Lorg/apache/poi/ss/format/CellDateFormatter;->showM:Z

    if-eqz v6, :cond_5

    const/16 v6, 0x6d

    :goto_3
    invoke-virtual {v0, v6}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    :cond_5
    move v9, v11

    goto :goto_4

    :cond_6
    invoke-virtual {v0, v6}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    :cond_7
    :goto_4
    invoke-interface {v3}, Ljava/text/CharacterIterator;->next()C

    move-result v6

    goto/16 :goto_2

    :cond_8
    return-void
.end method

.method public simpleValue(Ljava/lang/StringBuffer;Ljava/lang/Object;)V
    .locals 3

    const-class v0, Lorg/apache/poi/ss/format/CellDateFormatter;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lorg/apache/poi/ss/format/CellDateFormatter;->SIMPLE_DATE:Lorg/apache/poi/ss/format/CellDateFormatter;

    if-eqz v1, :cond_0

    iget-object v1, v1, Lorg/apache/poi/ss/format/CellDateFormatter;->EXCEL_EPOCH_CAL:Ljava/util/Calendar;

    iget-object v2, p0, Lorg/apache/poi/ss/format/CellDateFormatter;->EXCEL_EPOCH_CAL:Ljava/util/Calendar;

    invoke-virtual {v1, v2}, Ljava/util/Calendar;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    new-instance v1, Lorg/apache/poi/ss/format/CellDateFormatter;

    const-string v2, "mm/d/y"

    invoke-direct {v1, v2}, Lorg/apache/poi/ss/format/CellDateFormatter;-><init>(Ljava/lang/String;)V

    sput-object v1, Lorg/apache/poi/ss/format/CellDateFormatter;->SIMPLE_DATE:Lorg/apache/poi/ss/format/CellDateFormatter;

    :cond_1
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    sget-object v0, Lorg/apache/poi/ss/format/CellDateFormatter;->SIMPLE_DATE:Lorg/apache/poi/ss/format/CellDateFormatter;

    invoke-virtual {v0, p1, p2}, Lorg/apache/poi/ss/format/CellDateFormatter;->formatValue(Ljava/lang/StringBuffer;Ljava/lang/Object;)V

    return-void

    :catchall_0
    move-exception p1

    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw p1
.end method

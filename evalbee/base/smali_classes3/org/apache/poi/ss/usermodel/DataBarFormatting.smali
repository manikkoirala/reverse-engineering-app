.class public interface abstract Lorg/apache/poi/ss/usermodel/DataBarFormatting;
.super Ljava/lang/Object;
.source "SourceFile"


# virtual methods
.method public abstract getColor()Lorg/apache/poi/ss/usermodel/Color;
.end method

.method public abstract getMaxThreshold()Lorg/apache/poi/ss/usermodel/ConditionalFormattingThreshold;
.end method

.method public abstract getMinThreshold()Lorg/apache/poi/ss/usermodel/ConditionalFormattingThreshold;
.end method

.method public abstract getWidthMax()I
.end method

.method public abstract getWidthMin()I
.end method

.method public abstract isIconOnly()Z
.end method

.method public abstract isLeftToRight()Z
.end method

.method public abstract setColor(Lorg/apache/poi/ss/usermodel/Color;)V
.end method

.method public abstract setIconOnly(Z)V
.end method

.method public abstract setLeftToRight(Z)V
.end method

.method public abstract setWidthMax(I)V
.end method

.method public abstract setWidthMin(I)V
.end method

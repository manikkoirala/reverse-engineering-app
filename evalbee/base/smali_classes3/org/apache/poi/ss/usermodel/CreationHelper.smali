.class public interface abstract Lorg/apache/poi/ss/usermodel/CreationHelper;
.super Ljava/lang/Object;
.source "SourceFile"


# virtual methods
.method public abstract createAreaReference(Ljava/lang/String;)Lorg/apache/poi/ss/util/AreaReference;
.end method

.method public abstract createAreaReference(Lorg/apache/poi/ss/util/CellReference;Lorg/apache/poi/ss/util/CellReference;)Lorg/apache/poi/ss/util/AreaReference;
.end method

.method public abstract createClientAnchor()Lorg/apache/poi/ss/usermodel/ClientAnchor;
.end method

.method public abstract createDataFormat()Lorg/apache/poi/ss/usermodel/DataFormat;
.end method

.method public abstract createExtendedColor()Lorg/apache/poi/ss/usermodel/ExtendedColor;
.end method

.method public abstract createFormulaEvaluator()Lorg/apache/poi/ss/usermodel/FormulaEvaluator;
.end method

.method public abstract createHyperlink(Lorg/apache/poi/common/usermodel/HyperlinkType;)Lorg/apache/poi/ss/usermodel/Hyperlink;
.end method

.method public abstract createRichTextString(Ljava/lang/String;)Lorg/apache/poi/ss/usermodel/RichTextString;
.end method

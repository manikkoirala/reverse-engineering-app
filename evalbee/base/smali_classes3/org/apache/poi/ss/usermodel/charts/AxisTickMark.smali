.class public final enum Lorg/apache/poi/ss/usermodel/charts/AxisTickMark;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lorg/apache/poi/ss/usermodel/charts/AxisTickMark;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lorg/apache/poi/ss/usermodel/charts/AxisTickMark;

.field public static final enum CROSS:Lorg/apache/poi/ss/usermodel/charts/AxisTickMark;

.field public static final enum IN:Lorg/apache/poi/ss/usermodel/charts/AxisTickMark;

.field public static final enum NONE:Lorg/apache/poi/ss/usermodel/charts/AxisTickMark;

.field public static final enum OUT:Lorg/apache/poi/ss/usermodel/charts/AxisTickMark;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    new-instance v0, Lorg/apache/poi/ss/usermodel/charts/AxisTickMark;

    const-string v1, "NONE"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/ss/usermodel/charts/AxisTickMark;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/ss/usermodel/charts/AxisTickMark;->NONE:Lorg/apache/poi/ss/usermodel/charts/AxisTickMark;

    new-instance v1, Lorg/apache/poi/ss/usermodel/charts/AxisTickMark;

    const-string v2, "CROSS"

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Lorg/apache/poi/ss/usermodel/charts/AxisTickMark;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lorg/apache/poi/ss/usermodel/charts/AxisTickMark;->CROSS:Lorg/apache/poi/ss/usermodel/charts/AxisTickMark;

    new-instance v2, Lorg/apache/poi/ss/usermodel/charts/AxisTickMark;

    const-string v3, "IN"

    const/4 v4, 0x2

    invoke-direct {v2, v3, v4}, Lorg/apache/poi/ss/usermodel/charts/AxisTickMark;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lorg/apache/poi/ss/usermodel/charts/AxisTickMark;->IN:Lorg/apache/poi/ss/usermodel/charts/AxisTickMark;

    new-instance v3, Lorg/apache/poi/ss/usermodel/charts/AxisTickMark;

    const-string v4, "OUT"

    const/4 v5, 0x3

    invoke-direct {v3, v4, v5}, Lorg/apache/poi/ss/usermodel/charts/AxisTickMark;-><init>(Ljava/lang/String;I)V

    sput-object v3, Lorg/apache/poi/ss/usermodel/charts/AxisTickMark;->OUT:Lorg/apache/poi/ss/usermodel/charts/AxisTickMark;

    filled-new-array {v0, v1, v2, v3}, [Lorg/apache/poi/ss/usermodel/charts/AxisTickMark;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/ss/usermodel/charts/AxisTickMark;->$VALUES:[Lorg/apache/poi/ss/usermodel/charts/AxisTickMark;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lorg/apache/poi/ss/usermodel/charts/AxisTickMark;
    .locals 1

    const-class v0, Lorg/apache/poi/ss/usermodel/charts/AxisTickMark;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lorg/apache/poi/ss/usermodel/charts/AxisTickMark;

    return-object p0
.end method

.method public static values()[Lorg/apache/poi/ss/usermodel/charts/AxisTickMark;
    .locals 1

    sget-object v0, Lorg/apache/poi/ss/usermodel/charts/AxisTickMark;->$VALUES:[Lorg/apache/poi/ss/usermodel/charts/AxisTickMark;

    invoke-virtual {v0}, [Lorg/apache/poi/ss/usermodel/charts/AxisTickMark;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lorg/apache/poi/ss/usermodel/charts/AxisTickMark;

    return-object v0
.end method

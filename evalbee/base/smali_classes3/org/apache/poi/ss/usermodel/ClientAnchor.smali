.class public interface abstract Lorg/apache/poi/ss/usermodel/ClientAnchor;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/poi/ss/usermodel/ClientAnchor$AnchorType;
    }
.end annotation


# virtual methods
.method public abstract getAnchorType()Lorg/apache/poi/ss/usermodel/ClientAnchor$AnchorType;
.end method

.method public abstract getCol1()S
.end method

.method public abstract getCol2()S
.end method

.method public abstract getDx1()I
.end method

.method public abstract getDx2()I
.end method

.method public abstract getDy1()I
.end method

.method public abstract getDy2()I
.end method

.method public abstract getRow1()I
.end method

.method public abstract getRow2()I
.end method

.method public abstract setAnchorType(Lorg/apache/poi/ss/usermodel/ClientAnchor$AnchorType;)V
.end method

.method public abstract setCol1(I)V
.end method

.method public abstract setCol2(I)V
.end method

.method public abstract setDx1(I)V
.end method

.method public abstract setDx2(I)V
.end method

.method public abstract setDy1(I)V
.end method

.method public abstract setDy2(I)V
.end method

.method public abstract setRow1(I)V
.end method

.method public abstract setRow2(I)V
.end method

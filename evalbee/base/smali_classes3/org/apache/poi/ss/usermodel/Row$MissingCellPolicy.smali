.class public final enum Lorg/apache/poi/ss/usermodel/Row$MissingCellPolicy;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/poi/ss/usermodel/Row;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "MissingCellPolicy"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lorg/apache/poi/ss/usermodel/Row$MissingCellPolicy;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lorg/apache/poi/ss/usermodel/Row$MissingCellPolicy;

.field public static final enum CREATE_NULL_AS_BLANK:Lorg/apache/poi/ss/usermodel/Row$MissingCellPolicy;

.field public static final enum RETURN_BLANK_AS_NULL:Lorg/apache/poi/ss/usermodel/Row$MissingCellPolicy;

.field public static final enum RETURN_NULL_AND_BLANK:Lorg/apache/poi/ss/usermodel/Row$MissingCellPolicy;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    new-instance v0, Lorg/apache/poi/ss/usermodel/Row$MissingCellPolicy;

    const-string v1, "RETURN_NULL_AND_BLANK"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/ss/usermodel/Row$MissingCellPolicy;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/ss/usermodel/Row$MissingCellPolicy;->RETURN_NULL_AND_BLANK:Lorg/apache/poi/ss/usermodel/Row$MissingCellPolicy;

    new-instance v1, Lorg/apache/poi/ss/usermodel/Row$MissingCellPolicy;

    const-string v2, "RETURN_BLANK_AS_NULL"

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Lorg/apache/poi/ss/usermodel/Row$MissingCellPolicy;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lorg/apache/poi/ss/usermodel/Row$MissingCellPolicy;->RETURN_BLANK_AS_NULL:Lorg/apache/poi/ss/usermodel/Row$MissingCellPolicy;

    new-instance v2, Lorg/apache/poi/ss/usermodel/Row$MissingCellPolicy;

    const-string v3, "CREATE_NULL_AS_BLANK"

    const/4 v4, 0x2

    invoke-direct {v2, v3, v4}, Lorg/apache/poi/ss/usermodel/Row$MissingCellPolicy;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lorg/apache/poi/ss/usermodel/Row$MissingCellPolicy;->CREATE_NULL_AS_BLANK:Lorg/apache/poi/ss/usermodel/Row$MissingCellPolicy;

    filled-new-array {v0, v1, v2}, [Lorg/apache/poi/ss/usermodel/Row$MissingCellPolicy;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/ss/usermodel/Row$MissingCellPolicy;->$VALUES:[Lorg/apache/poi/ss/usermodel/Row$MissingCellPolicy;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lorg/apache/poi/ss/usermodel/Row$MissingCellPolicy;
    .locals 1

    const-class v0, Lorg/apache/poi/ss/usermodel/Row$MissingCellPolicy;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lorg/apache/poi/ss/usermodel/Row$MissingCellPolicy;

    return-object p0
.end method

.method public static values()[Lorg/apache/poi/ss/usermodel/Row$MissingCellPolicy;
    .locals 1

    sget-object v0, Lorg/apache/poi/ss/usermodel/Row$MissingCellPolicy;->$VALUES:[Lorg/apache/poi/ss/usermodel/Row$MissingCellPolicy;

    invoke-virtual {v0}, [Lorg/apache/poi/ss/usermodel/Row$MissingCellPolicy;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lorg/apache/poi/ss/usermodel/Row$MissingCellPolicy;

    return-object v0
.end method

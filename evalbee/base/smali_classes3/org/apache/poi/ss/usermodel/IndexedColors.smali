.class public final enum Lorg/apache/poi/ss/usermodel/IndexedColors;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lorg/apache/poi/ss/usermodel/IndexedColors;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lorg/apache/poi/ss/usermodel/IndexedColors;

.field public static final enum AQUA:Lorg/apache/poi/ss/usermodel/IndexedColors;

.field public static final enum AUTOMATIC:Lorg/apache/poi/ss/usermodel/IndexedColors;

.field public static final enum BLACK:Lorg/apache/poi/ss/usermodel/IndexedColors;

.field public static final enum BLACK1:Lorg/apache/poi/ss/usermodel/IndexedColors;

.field public static final enum BLUE:Lorg/apache/poi/ss/usermodel/IndexedColors;

.field public static final enum BLUE1:Lorg/apache/poi/ss/usermodel/IndexedColors;

.field public static final enum BLUE_GREY:Lorg/apache/poi/ss/usermodel/IndexedColors;

.field public static final enum BRIGHT_GREEN:Lorg/apache/poi/ss/usermodel/IndexedColors;

.field public static final enum BRIGHT_GREEN1:Lorg/apache/poi/ss/usermodel/IndexedColors;

.field public static final enum BROWN:Lorg/apache/poi/ss/usermodel/IndexedColors;

.field public static final enum CORAL:Lorg/apache/poi/ss/usermodel/IndexedColors;

.field public static final enum CORNFLOWER_BLUE:Lorg/apache/poi/ss/usermodel/IndexedColors;

.field public static final enum DARK_BLUE:Lorg/apache/poi/ss/usermodel/IndexedColors;

.field public static final enum DARK_GREEN:Lorg/apache/poi/ss/usermodel/IndexedColors;

.field public static final enum DARK_RED:Lorg/apache/poi/ss/usermodel/IndexedColors;

.field public static final enum DARK_TEAL:Lorg/apache/poi/ss/usermodel/IndexedColors;

.field public static final enum DARK_YELLOW:Lorg/apache/poi/ss/usermodel/IndexedColors;

.field public static final enum GOLD:Lorg/apache/poi/ss/usermodel/IndexedColors;

.field public static final enum GREEN:Lorg/apache/poi/ss/usermodel/IndexedColors;

.field public static final enum GREY_25_PERCENT:Lorg/apache/poi/ss/usermodel/IndexedColors;

.field public static final enum GREY_40_PERCENT:Lorg/apache/poi/ss/usermodel/IndexedColors;

.field public static final enum GREY_50_PERCENT:Lorg/apache/poi/ss/usermodel/IndexedColors;

.field public static final enum GREY_80_PERCENT:Lorg/apache/poi/ss/usermodel/IndexedColors;

.field public static final enum INDIGO:Lorg/apache/poi/ss/usermodel/IndexedColors;

.field public static final enum LAVENDER:Lorg/apache/poi/ss/usermodel/IndexedColors;

.field public static final enum LEMON_CHIFFON:Lorg/apache/poi/ss/usermodel/IndexedColors;

.field public static final enum LIGHT_BLUE:Lorg/apache/poi/ss/usermodel/IndexedColors;

.field public static final enum LIGHT_CORNFLOWER_BLUE:Lorg/apache/poi/ss/usermodel/IndexedColors;

.field public static final enum LIGHT_GREEN:Lorg/apache/poi/ss/usermodel/IndexedColors;

.field public static final enum LIGHT_ORANGE:Lorg/apache/poi/ss/usermodel/IndexedColors;

.field public static final enum LIGHT_TURQUOISE:Lorg/apache/poi/ss/usermodel/IndexedColors;

.field public static final enum LIGHT_TURQUOISE1:Lorg/apache/poi/ss/usermodel/IndexedColors;

.field public static final enum LIGHT_YELLOW:Lorg/apache/poi/ss/usermodel/IndexedColors;

.field public static final enum LIME:Lorg/apache/poi/ss/usermodel/IndexedColors;

.field public static final enum MAROON:Lorg/apache/poi/ss/usermodel/IndexedColors;

.field public static final enum OLIVE_GREEN:Lorg/apache/poi/ss/usermodel/IndexedColors;

.field public static final enum ORANGE:Lorg/apache/poi/ss/usermodel/IndexedColors;

.field public static final enum ORCHID:Lorg/apache/poi/ss/usermodel/IndexedColors;

.field public static final enum PALE_BLUE:Lorg/apache/poi/ss/usermodel/IndexedColors;

.field public static final enum PINK:Lorg/apache/poi/ss/usermodel/IndexedColors;

.field public static final enum PINK1:Lorg/apache/poi/ss/usermodel/IndexedColors;

.field public static final enum PLUM:Lorg/apache/poi/ss/usermodel/IndexedColors;

.field public static final enum RED:Lorg/apache/poi/ss/usermodel/IndexedColors;

.field public static final enum RED1:Lorg/apache/poi/ss/usermodel/IndexedColors;

.field public static final enum ROSE:Lorg/apache/poi/ss/usermodel/IndexedColors;

.field public static final enum ROYAL_BLUE:Lorg/apache/poi/ss/usermodel/IndexedColors;

.field public static final enum SEA_GREEN:Lorg/apache/poi/ss/usermodel/IndexedColors;

.field public static final enum SKY_BLUE:Lorg/apache/poi/ss/usermodel/IndexedColors;

.field public static final enum TAN:Lorg/apache/poi/ss/usermodel/IndexedColors;

.field public static final enum TEAL:Lorg/apache/poi/ss/usermodel/IndexedColors;

.field public static final enum TURQUOISE:Lorg/apache/poi/ss/usermodel/IndexedColors;

.field public static final enum TURQUOISE1:Lorg/apache/poi/ss/usermodel/IndexedColors;

.field public static final enum VIOLET:Lorg/apache/poi/ss/usermodel/IndexedColors;

.field public static final enum WHITE:Lorg/apache/poi/ss/usermodel/IndexedColors;

.field public static final enum WHITE1:Lorg/apache/poi/ss/usermodel/IndexedColors;

.field public static final enum YELLOW:Lorg/apache/poi/ss/usermodel/IndexedColors;

.field public static final enum YELLOW1:Lorg/apache/poi/ss/usermodel/IndexedColors;

.field private static final _values:[Lorg/apache/poi/ss/usermodel/IndexedColors;


# instance fields
.field public final index:S


# direct methods
.method public static constructor <clinit>()V
    .locals 62

    new-instance v1, Lorg/apache/poi/ss/usermodel/IndexedColors;

    move-object v0, v1

    const-string v2, "BLACK1"

    const/4 v15, 0x0

    invoke-direct {v1, v2, v15, v15}, Lorg/apache/poi/ss/usermodel/IndexedColors;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lorg/apache/poi/ss/usermodel/IndexedColors;->BLACK1:Lorg/apache/poi/ss/usermodel/IndexedColors;

    new-instance v2, Lorg/apache/poi/ss/usermodel/IndexedColors;

    move-object v1, v2

    const-string v3, "WHITE1"

    const/4 v4, 0x1

    invoke-direct {v2, v3, v4, v4}, Lorg/apache/poi/ss/usermodel/IndexedColors;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lorg/apache/poi/ss/usermodel/IndexedColors;->WHITE1:Lorg/apache/poi/ss/usermodel/IndexedColors;

    new-instance v3, Lorg/apache/poi/ss/usermodel/IndexedColors;

    move-object v2, v3

    const-string v4, "RED1"

    const/4 v5, 0x2

    invoke-direct {v3, v4, v5, v5}, Lorg/apache/poi/ss/usermodel/IndexedColors;-><init>(Ljava/lang/String;II)V

    sput-object v3, Lorg/apache/poi/ss/usermodel/IndexedColors;->RED1:Lorg/apache/poi/ss/usermodel/IndexedColors;

    new-instance v4, Lorg/apache/poi/ss/usermodel/IndexedColors;

    move-object v3, v4

    const-string v5, "BRIGHT_GREEN1"

    const/4 v6, 0x3

    invoke-direct {v4, v5, v6, v6}, Lorg/apache/poi/ss/usermodel/IndexedColors;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lorg/apache/poi/ss/usermodel/IndexedColors;->BRIGHT_GREEN1:Lorg/apache/poi/ss/usermodel/IndexedColors;

    new-instance v5, Lorg/apache/poi/ss/usermodel/IndexedColors;

    move-object v4, v5

    const-string v6, "BLUE1"

    const/4 v7, 0x4

    invoke-direct {v5, v6, v7, v7}, Lorg/apache/poi/ss/usermodel/IndexedColors;-><init>(Ljava/lang/String;II)V

    sput-object v5, Lorg/apache/poi/ss/usermodel/IndexedColors;->BLUE1:Lorg/apache/poi/ss/usermodel/IndexedColors;

    new-instance v6, Lorg/apache/poi/ss/usermodel/IndexedColors;

    move-object v5, v6

    const-string v7, "YELLOW1"

    const/4 v8, 0x5

    invoke-direct {v6, v7, v8, v8}, Lorg/apache/poi/ss/usermodel/IndexedColors;-><init>(Ljava/lang/String;II)V

    sput-object v6, Lorg/apache/poi/ss/usermodel/IndexedColors;->YELLOW1:Lorg/apache/poi/ss/usermodel/IndexedColors;

    new-instance v7, Lorg/apache/poi/ss/usermodel/IndexedColors;

    move-object v6, v7

    const-string v8, "PINK1"

    const/4 v9, 0x6

    invoke-direct {v7, v8, v9, v9}, Lorg/apache/poi/ss/usermodel/IndexedColors;-><init>(Ljava/lang/String;II)V

    sput-object v7, Lorg/apache/poi/ss/usermodel/IndexedColors;->PINK1:Lorg/apache/poi/ss/usermodel/IndexedColors;

    new-instance v8, Lorg/apache/poi/ss/usermodel/IndexedColors;

    move-object v7, v8

    const-string v9, "TURQUOISE1"

    const/4 v10, 0x7

    invoke-direct {v8, v9, v10, v10}, Lorg/apache/poi/ss/usermodel/IndexedColors;-><init>(Ljava/lang/String;II)V

    sput-object v8, Lorg/apache/poi/ss/usermodel/IndexedColors;->TURQUOISE1:Lorg/apache/poi/ss/usermodel/IndexedColors;

    new-instance v9, Lorg/apache/poi/ss/usermodel/IndexedColors;

    move-object v8, v9

    const-string v10, "BLACK"

    const/16 v11, 0x8

    invoke-direct {v9, v10, v11, v11}, Lorg/apache/poi/ss/usermodel/IndexedColors;-><init>(Ljava/lang/String;II)V

    sput-object v9, Lorg/apache/poi/ss/usermodel/IndexedColors;->BLACK:Lorg/apache/poi/ss/usermodel/IndexedColors;

    new-instance v10, Lorg/apache/poi/ss/usermodel/IndexedColors;

    move-object v9, v10

    const-string v11, "WHITE"

    const/16 v12, 0x9

    invoke-direct {v10, v11, v12, v12}, Lorg/apache/poi/ss/usermodel/IndexedColors;-><init>(Ljava/lang/String;II)V

    sput-object v10, Lorg/apache/poi/ss/usermodel/IndexedColors;->WHITE:Lorg/apache/poi/ss/usermodel/IndexedColors;

    new-instance v11, Lorg/apache/poi/ss/usermodel/IndexedColors;

    move-object v10, v11

    const-string v12, "RED"

    const/16 v13, 0xa

    invoke-direct {v11, v12, v13, v13}, Lorg/apache/poi/ss/usermodel/IndexedColors;-><init>(Ljava/lang/String;II)V

    sput-object v11, Lorg/apache/poi/ss/usermodel/IndexedColors;->RED:Lorg/apache/poi/ss/usermodel/IndexedColors;

    new-instance v12, Lorg/apache/poi/ss/usermodel/IndexedColors;

    move-object v11, v12

    const-string v13, "BRIGHT_GREEN"

    const/16 v14, 0xb

    invoke-direct {v12, v13, v14, v14}, Lorg/apache/poi/ss/usermodel/IndexedColors;-><init>(Ljava/lang/String;II)V

    sput-object v12, Lorg/apache/poi/ss/usermodel/IndexedColors;->BRIGHT_GREEN:Lorg/apache/poi/ss/usermodel/IndexedColors;

    new-instance v13, Lorg/apache/poi/ss/usermodel/IndexedColors;

    move-object v12, v13

    const-string v14, "BLUE"

    const/16 v15, 0xc

    invoke-direct {v13, v14, v15, v15}, Lorg/apache/poi/ss/usermodel/IndexedColors;-><init>(Ljava/lang/String;II)V

    sput-object v13, Lorg/apache/poi/ss/usermodel/IndexedColors;->BLUE:Lorg/apache/poi/ss/usermodel/IndexedColors;

    new-instance v14, Lorg/apache/poi/ss/usermodel/IndexedColors;

    move-object v13, v14

    const-string v15, "YELLOW"

    move-object/from16 v57, v0

    const/16 v0, 0xd

    invoke-direct {v14, v15, v0, v0}, Lorg/apache/poi/ss/usermodel/IndexedColors;-><init>(Ljava/lang/String;II)V

    sput-object v14, Lorg/apache/poi/ss/usermodel/IndexedColors;->YELLOW:Lorg/apache/poi/ss/usermodel/IndexedColors;

    new-instance v0, Lorg/apache/poi/ss/usermodel/IndexedColors;

    move-object v14, v0

    const-string v15, "PINK"

    move-object/from16 v58, v1

    const/16 v1, 0xe

    invoke-direct {v0, v15, v1, v1}, Lorg/apache/poi/ss/usermodel/IndexedColors;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/ss/usermodel/IndexedColors;->PINK:Lorg/apache/poi/ss/usermodel/IndexedColors;

    new-instance v0, Lorg/apache/poi/ss/usermodel/IndexedColors;

    const/16 v59, 0x0

    move-object v15, v0

    const-string v1, "TURQUOISE"

    move-object/from16 v60, v2

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2, v2}, Lorg/apache/poi/ss/usermodel/IndexedColors;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/ss/usermodel/IndexedColors;->TURQUOISE:Lorg/apache/poi/ss/usermodel/IndexedColors;

    new-instance v0, Lorg/apache/poi/ss/usermodel/IndexedColors;

    move-object/from16 v16, v0

    const-string v1, "DARK_RED"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2, v2}, Lorg/apache/poi/ss/usermodel/IndexedColors;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/ss/usermodel/IndexedColors;->DARK_RED:Lorg/apache/poi/ss/usermodel/IndexedColors;

    new-instance v0, Lorg/apache/poi/ss/usermodel/IndexedColors;

    move-object/from16 v17, v0

    const-string v1, "GREEN"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2, v2}, Lorg/apache/poi/ss/usermodel/IndexedColors;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/ss/usermodel/IndexedColors;->GREEN:Lorg/apache/poi/ss/usermodel/IndexedColors;

    new-instance v0, Lorg/apache/poi/ss/usermodel/IndexedColors;

    move-object/from16 v18, v0

    const-string v1, "DARK_BLUE"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2, v2}, Lorg/apache/poi/ss/usermodel/IndexedColors;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/ss/usermodel/IndexedColors;->DARK_BLUE:Lorg/apache/poi/ss/usermodel/IndexedColors;

    new-instance v0, Lorg/apache/poi/ss/usermodel/IndexedColors;

    move-object/from16 v19, v0

    const-string v1, "DARK_YELLOW"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2, v2}, Lorg/apache/poi/ss/usermodel/IndexedColors;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/ss/usermodel/IndexedColors;->DARK_YELLOW:Lorg/apache/poi/ss/usermodel/IndexedColors;

    new-instance v0, Lorg/apache/poi/ss/usermodel/IndexedColors;

    move-object/from16 v20, v0

    const-string v1, "VIOLET"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2, v2}, Lorg/apache/poi/ss/usermodel/IndexedColors;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/ss/usermodel/IndexedColors;->VIOLET:Lorg/apache/poi/ss/usermodel/IndexedColors;

    new-instance v0, Lorg/apache/poi/ss/usermodel/IndexedColors;

    move-object/from16 v21, v0

    const-string v1, "TEAL"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v2, v2}, Lorg/apache/poi/ss/usermodel/IndexedColors;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/ss/usermodel/IndexedColors;->TEAL:Lorg/apache/poi/ss/usermodel/IndexedColors;

    new-instance v0, Lorg/apache/poi/ss/usermodel/IndexedColors;

    move-object/from16 v22, v0

    const/16 v1, 0x16

    const/16 v2, 0x16

    move-object/from16 v61, v3

    const-string v3, "GREY_25_PERCENT"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/ss/usermodel/IndexedColors;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/ss/usermodel/IndexedColors;->GREY_25_PERCENT:Lorg/apache/poi/ss/usermodel/IndexedColors;

    new-instance v0, Lorg/apache/poi/ss/usermodel/IndexedColors;

    move-object/from16 v23, v0

    const/16 v1, 0x17

    const/16 v2, 0x17

    const-string v3, "GREY_50_PERCENT"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/ss/usermodel/IndexedColors;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/ss/usermodel/IndexedColors;->GREY_50_PERCENT:Lorg/apache/poi/ss/usermodel/IndexedColors;

    new-instance v0, Lorg/apache/poi/ss/usermodel/IndexedColors;

    move-object/from16 v24, v0

    const/16 v1, 0x18

    const/16 v2, 0x18

    const-string v3, "CORNFLOWER_BLUE"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/ss/usermodel/IndexedColors;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/ss/usermodel/IndexedColors;->CORNFLOWER_BLUE:Lorg/apache/poi/ss/usermodel/IndexedColors;

    new-instance v0, Lorg/apache/poi/ss/usermodel/IndexedColors;

    move-object/from16 v25, v0

    const/16 v1, 0x19

    const/16 v2, 0x19

    const-string v3, "MAROON"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/ss/usermodel/IndexedColors;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/ss/usermodel/IndexedColors;->MAROON:Lorg/apache/poi/ss/usermodel/IndexedColors;

    new-instance v0, Lorg/apache/poi/ss/usermodel/IndexedColors;

    move-object/from16 v26, v0

    const/16 v1, 0x1a

    const/16 v2, 0x1a

    const-string v3, "LEMON_CHIFFON"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/ss/usermodel/IndexedColors;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/ss/usermodel/IndexedColors;->LEMON_CHIFFON:Lorg/apache/poi/ss/usermodel/IndexedColors;

    new-instance v0, Lorg/apache/poi/ss/usermodel/IndexedColors;

    move-object/from16 v27, v0

    const/16 v1, 0x1b

    const/16 v2, 0x1b

    const-string v3, "LIGHT_TURQUOISE1"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/ss/usermodel/IndexedColors;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/ss/usermodel/IndexedColors;->LIGHT_TURQUOISE1:Lorg/apache/poi/ss/usermodel/IndexedColors;

    new-instance v0, Lorg/apache/poi/ss/usermodel/IndexedColors;

    move-object/from16 v28, v0

    const/16 v1, 0x1c

    const/16 v2, 0x1c

    const-string v3, "ORCHID"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/ss/usermodel/IndexedColors;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/ss/usermodel/IndexedColors;->ORCHID:Lorg/apache/poi/ss/usermodel/IndexedColors;

    new-instance v0, Lorg/apache/poi/ss/usermodel/IndexedColors;

    move-object/from16 v29, v0

    const/16 v1, 0x1d

    const/16 v2, 0x1d

    const-string v3, "CORAL"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/ss/usermodel/IndexedColors;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/ss/usermodel/IndexedColors;->CORAL:Lorg/apache/poi/ss/usermodel/IndexedColors;

    new-instance v0, Lorg/apache/poi/ss/usermodel/IndexedColors;

    move-object/from16 v30, v0

    const/16 v1, 0x1e

    const/16 v2, 0x1e

    const-string v3, "ROYAL_BLUE"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/ss/usermodel/IndexedColors;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/ss/usermodel/IndexedColors;->ROYAL_BLUE:Lorg/apache/poi/ss/usermodel/IndexedColors;

    new-instance v0, Lorg/apache/poi/ss/usermodel/IndexedColors;

    move-object/from16 v31, v0

    const/16 v1, 0x1f

    const/16 v2, 0x1f

    const-string v3, "LIGHT_CORNFLOWER_BLUE"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/ss/usermodel/IndexedColors;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/ss/usermodel/IndexedColors;->LIGHT_CORNFLOWER_BLUE:Lorg/apache/poi/ss/usermodel/IndexedColors;

    new-instance v0, Lorg/apache/poi/ss/usermodel/IndexedColors;

    move-object/from16 v32, v0

    const/16 v1, 0x20

    const/16 v2, 0x28

    const-string v3, "SKY_BLUE"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/ss/usermodel/IndexedColors;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/ss/usermodel/IndexedColors;->SKY_BLUE:Lorg/apache/poi/ss/usermodel/IndexedColors;

    new-instance v0, Lorg/apache/poi/ss/usermodel/IndexedColors;

    move-object/from16 v33, v0

    const/16 v1, 0x21

    const/16 v2, 0x29

    const-string v3, "LIGHT_TURQUOISE"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/ss/usermodel/IndexedColors;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/ss/usermodel/IndexedColors;->LIGHT_TURQUOISE:Lorg/apache/poi/ss/usermodel/IndexedColors;

    new-instance v0, Lorg/apache/poi/ss/usermodel/IndexedColors;

    move-object/from16 v34, v0

    const/16 v1, 0x22

    const/16 v2, 0x2a

    const-string v3, "LIGHT_GREEN"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/ss/usermodel/IndexedColors;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/ss/usermodel/IndexedColors;->LIGHT_GREEN:Lorg/apache/poi/ss/usermodel/IndexedColors;

    new-instance v0, Lorg/apache/poi/ss/usermodel/IndexedColors;

    move-object/from16 v35, v0

    const/16 v1, 0x23

    const/16 v2, 0x2b

    const-string v3, "LIGHT_YELLOW"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/ss/usermodel/IndexedColors;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/ss/usermodel/IndexedColors;->LIGHT_YELLOW:Lorg/apache/poi/ss/usermodel/IndexedColors;

    new-instance v0, Lorg/apache/poi/ss/usermodel/IndexedColors;

    move-object/from16 v36, v0

    const/16 v1, 0x24

    const/16 v2, 0x2c

    const-string v3, "PALE_BLUE"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/ss/usermodel/IndexedColors;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/ss/usermodel/IndexedColors;->PALE_BLUE:Lorg/apache/poi/ss/usermodel/IndexedColors;

    new-instance v0, Lorg/apache/poi/ss/usermodel/IndexedColors;

    move-object/from16 v37, v0

    const/16 v1, 0x25

    const/16 v2, 0x2d

    const-string v3, "ROSE"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/ss/usermodel/IndexedColors;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/ss/usermodel/IndexedColors;->ROSE:Lorg/apache/poi/ss/usermodel/IndexedColors;

    new-instance v0, Lorg/apache/poi/ss/usermodel/IndexedColors;

    move-object/from16 v38, v0

    const/16 v1, 0x26

    const/16 v2, 0x2e

    const-string v3, "LAVENDER"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/ss/usermodel/IndexedColors;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/ss/usermodel/IndexedColors;->LAVENDER:Lorg/apache/poi/ss/usermodel/IndexedColors;

    new-instance v0, Lorg/apache/poi/ss/usermodel/IndexedColors;

    move-object/from16 v39, v0

    const/16 v1, 0x27

    const/16 v2, 0x2f

    const-string v3, "TAN"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/ss/usermodel/IndexedColors;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/ss/usermodel/IndexedColors;->TAN:Lorg/apache/poi/ss/usermodel/IndexedColors;

    new-instance v0, Lorg/apache/poi/ss/usermodel/IndexedColors;

    move-object/from16 v40, v0

    const/16 v1, 0x28

    const/16 v2, 0x30

    const-string v3, "LIGHT_BLUE"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/ss/usermodel/IndexedColors;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/ss/usermodel/IndexedColors;->LIGHT_BLUE:Lorg/apache/poi/ss/usermodel/IndexedColors;

    new-instance v0, Lorg/apache/poi/ss/usermodel/IndexedColors;

    move-object/from16 v41, v0

    const/16 v1, 0x29

    const/16 v2, 0x31

    const-string v3, "AQUA"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/ss/usermodel/IndexedColors;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/ss/usermodel/IndexedColors;->AQUA:Lorg/apache/poi/ss/usermodel/IndexedColors;

    new-instance v0, Lorg/apache/poi/ss/usermodel/IndexedColors;

    move-object/from16 v42, v0

    const/16 v1, 0x2a

    const/16 v2, 0x32

    const-string v3, "LIME"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/ss/usermodel/IndexedColors;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/ss/usermodel/IndexedColors;->LIME:Lorg/apache/poi/ss/usermodel/IndexedColors;

    new-instance v0, Lorg/apache/poi/ss/usermodel/IndexedColors;

    move-object/from16 v43, v0

    const/16 v1, 0x2b

    const/16 v2, 0x33

    const-string v3, "GOLD"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/ss/usermodel/IndexedColors;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/ss/usermodel/IndexedColors;->GOLD:Lorg/apache/poi/ss/usermodel/IndexedColors;

    new-instance v0, Lorg/apache/poi/ss/usermodel/IndexedColors;

    move-object/from16 v44, v0

    const/16 v1, 0x2c

    const/16 v2, 0x34

    const-string v3, "LIGHT_ORANGE"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/ss/usermodel/IndexedColors;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/ss/usermodel/IndexedColors;->LIGHT_ORANGE:Lorg/apache/poi/ss/usermodel/IndexedColors;

    new-instance v0, Lorg/apache/poi/ss/usermodel/IndexedColors;

    move-object/from16 v45, v0

    const/16 v1, 0x2d

    const/16 v2, 0x35

    const-string v3, "ORANGE"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/ss/usermodel/IndexedColors;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/ss/usermodel/IndexedColors;->ORANGE:Lorg/apache/poi/ss/usermodel/IndexedColors;

    new-instance v0, Lorg/apache/poi/ss/usermodel/IndexedColors;

    move-object/from16 v46, v0

    const/16 v1, 0x2e

    const/16 v2, 0x36

    const-string v3, "BLUE_GREY"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/ss/usermodel/IndexedColors;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/ss/usermodel/IndexedColors;->BLUE_GREY:Lorg/apache/poi/ss/usermodel/IndexedColors;

    new-instance v0, Lorg/apache/poi/ss/usermodel/IndexedColors;

    move-object/from16 v47, v0

    const/16 v1, 0x2f

    const/16 v2, 0x37

    const-string v3, "GREY_40_PERCENT"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/ss/usermodel/IndexedColors;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/ss/usermodel/IndexedColors;->GREY_40_PERCENT:Lorg/apache/poi/ss/usermodel/IndexedColors;

    new-instance v0, Lorg/apache/poi/ss/usermodel/IndexedColors;

    move-object/from16 v48, v0

    const/16 v1, 0x30

    const/16 v2, 0x38

    const-string v3, "DARK_TEAL"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/ss/usermodel/IndexedColors;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/ss/usermodel/IndexedColors;->DARK_TEAL:Lorg/apache/poi/ss/usermodel/IndexedColors;

    new-instance v0, Lorg/apache/poi/ss/usermodel/IndexedColors;

    move-object/from16 v49, v0

    const/16 v1, 0x31

    const/16 v2, 0x39

    const-string v3, "SEA_GREEN"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/ss/usermodel/IndexedColors;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/ss/usermodel/IndexedColors;->SEA_GREEN:Lorg/apache/poi/ss/usermodel/IndexedColors;

    new-instance v0, Lorg/apache/poi/ss/usermodel/IndexedColors;

    move-object/from16 v50, v0

    const/16 v1, 0x32

    const/16 v2, 0x3a

    const-string v3, "DARK_GREEN"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/ss/usermodel/IndexedColors;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/ss/usermodel/IndexedColors;->DARK_GREEN:Lorg/apache/poi/ss/usermodel/IndexedColors;

    new-instance v0, Lorg/apache/poi/ss/usermodel/IndexedColors;

    move-object/from16 v51, v0

    const/16 v1, 0x33

    const/16 v2, 0x3b

    const-string v3, "OLIVE_GREEN"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/ss/usermodel/IndexedColors;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/ss/usermodel/IndexedColors;->OLIVE_GREEN:Lorg/apache/poi/ss/usermodel/IndexedColors;

    new-instance v0, Lorg/apache/poi/ss/usermodel/IndexedColors;

    move-object/from16 v52, v0

    const/16 v1, 0x34

    const/16 v2, 0x3c

    const-string v3, "BROWN"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/ss/usermodel/IndexedColors;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/ss/usermodel/IndexedColors;->BROWN:Lorg/apache/poi/ss/usermodel/IndexedColors;

    new-instance v0, Lorg/apache/poi/ss/usermodel/IndexedColors;

    move-object/from16 v53, v0

    const/16 v1, 0x35

    const/16 v2, 0x3d

    const-string v3, "PLUM"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/ss/usermodel/IndexedColors;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/ss/usermodel/IndexedColors;->PLUM:Lorg/apache/poi/ss/usermodel/IndexedColors;

    new-instance v0, Lorg/apache/poi/ss/usermodel/IndexedColors;

    move-object/from16 v54, v0

    const/16 v1, 0x36

    const/16 v2, 0x3e

    const-string v3, "INDIGO"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/ss/usermodel/IndexedColors;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/ss/usermodel/IndexedColors;->INDIGO:Lorg/apache/poi/ss/usermodel/IndexedColors;

    new-instance v0, Lorg/apache/poi/ss/usermodel/IndexedColors;

    move-object/from16 v55, v0

    const/16 v1, 0x37

    const/16 v2, 0x3f

    const-string v3, "GREY_80_PERCENT"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/ss/usermodel/IndexedColors;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/ss/usermodel/IndexedColors;->GREY_80_PERCENT:Lorg/apache/poi/ss/usermodel/IndexedColors;

    new-instance v0, Lorg/apache/poi/ss/usermodel/IndexedColors;

    move-object/from16 v56, v0

    const/16 v1, 0x38

    const/16 v2, 0x40

    const-string v3, "AUTOMATIC"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/poi/ss/usermodel/IndexedColors;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/ss/usermodel/IndexedColors;->AUTOMATIC:Lorg/apache/poi/ss/usermodel/IndexedColors;

    move-object/from16 v0, v57

    move-object/from16 v1, v58

    move-object/from16 v2, v60

    move-object/from16 v3, v61

    filled-new-array/range {v0 .. v56}, [Lorg/apache/poi/ss/usermodel/IndexedColors;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/ss/usermodel/IndexedColors;->$VALUES:[Lorg/apache/poi/ss/usermodel/IndexedColors;

    const/16 v0, 0x41

    new-array v0, v0, [Lorg/apache/poi/ss/usermodel/IndexedColors;

    sput-object v0, Lorg/apache/poi/ss/usermodel/IndexedColors;->_values:[Lorg/apache/poi/ss/usermodel/IndexedColors;

    invoke-static {}, Lorg/apache/poi/ss/usermodel/IndexedColors;->values()[Lorg/apache/poi/ss/usermodel/IndexedColors;

    move-result-object v0

    array-length v1, v0

    move/from16 v15, v59

    :goto_0
    if-ge v15, v1, :cond_0

    aget-object v2, v0, v15

    sget-object v3, Lorg/apache/poi/ss/usermodel/IndexedColors;->_values:[Lorg/apache/poi/ss/usermodel/IndexedColors;

    iget-short v4, v2, Lorg/apache/poi/ss/usermodel/IndexedColors;->index:S

    aput-object v2, v3, v4

    add-int/lit8 v15, v15, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    int-to-short p1, p3

    iput-short p1, p0, Lorg/apache/poi/ss/usermodel/IndexedColors;->index:S

    return-void
.end method

.method public static fromInt(I)Lorg/apache/poi/ss/usermodel/IndexedColors;
    .locals 3

    const-string v0, "Illegal IndexedColor index: "

    if-ltz p0, :cond_1

    sget-object v1, Lorg/apache/poi/ss/usermodel/IndexedColors;->_values:[Lorg/apache/poi/ss/usermodel/IndexedColors;

    array-length v2, v1

    if-ge p0, v2, :cond_1

    aget-object v1, v1, p0

    if-eqz v1, :cond_0

    return-object v1

    :cond_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v1, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v1, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public static valueOf(Ljava/lang/String;)Lorg/apache/poi/ss/usermodel/IndexedColors;
    .locals 1

    const-class v0, Lorg/apache/poi/ss/usermodel/IndexedColors;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lorg/apache/poi/ss/usermodel/IndexedColors;

    return-object p0
.end method

.method public static values()[Lorg/apache/poi/ss/usermodel/IndexedColors;
    .locals 1

    sget-object v0, Lorg/apache/poi/ss/usermodel/IndexedColors;->$VALUES:[Lorg/apache/poi/ss/usermodel/IndexedColors;

    invoke-virtual {v0}, [Lorg/apache/poi/ss/usermodel/IndexedColors;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lorg/apache/poi/ss/usermodel/IndexedColors;

    return-object v0
.end method


# virtual methods
.method public getIndex()S
    .locals 1

    iget-short v0, p0, Lorg/apache/poi/ss/usermodel/IndexedColors;->index:S

    return v0
.end method

.class public interface abstract Lorg/apache/poi/ss/usermodel/BorderFormatting;
.super Ljava/lang/Object;
.source "SourceFile"


# virtual methods
.method public abstract getBorderBottom()S
.end method

.method public abstract getBorderBottomEnum()Lorg/apache/poi/ss/usermodel/BorderStyle;
.end method

.method public abstract getBorderDiagonal()S
.end method

.method public abstract getBorderDiagonalEnum()Lorg/apache/poi/ss/usermodel/BorderStyle;
.end method

.method public abstract getBorderHorizontalEnum()Lorg/apache/poi/ss/usermodel/BorderStyle;
.end method

.method public abstract getBorderLeft()S
.end method

.method public abstract getBorderLeftEnum()Lorg/apache/poi/ss/usermodel/BorderStyle;
.end method

.method public abstract getBorderRight()S
.end method

.method public abstract getBorderRightEnum()Lorg/apache/poi/ss/usermodel/BorderStyle;
.end method

.method public abstract getBorderTop()S
.end method

.method public abstract getBorderTopEnum()Lorg/apache/poi/ss/usermodel/BorderStyle;
.end method

.method public abstract getBorderVerticalEnum()Lorg/apache/poi/ss/usermodel/BorderStyle;
.end method

.method public abstract getBottomBorderColor()S
.end method

.method public abstract getBottomBorderColorColor()Lorg/apache/poi/ss/usermodel/Color;
.end method

.method public abstract getDiagonalBorderColor()S
.end method

.method public abstract getDiagonalBorderColorColor()Lorg/apache/poi/ss/usermodel/Color;
.end method

.method public abstract getHorizontalBorderColor()S
.end method

.method public abstract getHorizontalBorderColorColor()Lorg/apache/poi/ss/usermodel/Color;
.end method

.method public abstract getLeftBorderColor()S
.end method

.method public abstract getLeftBorderColorColor()Lorg/apache/poi/ss/usermodel/Color;
.end method

.method public abstract getRightBorderColor()S
.end method

.method public abstract getRightBorderColorColor()Lorg/apache/poi/ss/usermodel/Color;
.end method

.method public abstract getTopBorderColor()S
.end method

.method public abstract getTopBorderColorColor()Lorg/apache/poi/ss/usermodel/Color;
.end method

.method public abstract getVerticalBorderColor()S
.end method

.method public abstract getVerticalBorderColorColor()Lorg/apache/poi/ss/usermodel/Color;
.end method

.method public abstract setBorderBottom(Lorg/apache/poi/ss/usermodel/BorderStyle;)V
.end method

.method public abstract setBorderBottom(S)V
.end method

.method public abstract setBorderDiagonal(Lorg/apache/poi/ss/usermodel/BorderStyle;)V
.end method

.method public abstract setBorderDiagonal(S)V
.end method

.method public abstract setBorderHorizontal(Lorg/apache/poi/ss/usermodel/BorderStyle;)V
.end method

.method public abstract setBorderLeft(Lorg/apache/poi/ss/usermodel/BorderStyle;)V
.end method

.method public abstract setBorderLeft(S)V
.end method

.method public abstract setBorderRight(Lorg/apache/poi/ss/usermodel/BorderStyle;)V
.end method

.method public abstract setBorderRight(S)V
.end method

.method public abstract setBorderTop(Lorg/apache/poi/ss/usermodel/BorderStyle;)V
.end method

.method public abstract setBorderTop(S)V
.end method

.method public abstract setBorderVertical(Lorg/apache/poi/ss/usermodel/BorderStyle;)V
.end method

.method public abstract setBottomBorderColor(Lorg/apache/poi/ss/usermodel/Color;)V
.end method

.method public abstract setBottomBorderColor(S)V
.end method

.method public abstract setDiagonalBorderColor(Lorg/apache/poi/ss/usermodel/Color;)V
.end method

.method public abstract setDiagonalBorderColor(S)V
.end method

.method public abstract setHorizontalBorderColor(Lorg/apache/poi/ss/usermodel/Color;)V
.end method

.method public abstract setHorizontalBorderColor(S)V
.end method

.method public abstract setLeftBorderColor(Lorg/apache/poi/ss/usermodel/Color;)V
.end method

.method public abstract setLeftBorderColor(S)V
.end method

.method public abstract setRightBorderColor(Lorg/apache/poi/ss/usermodel/Color;)V
.end method

.method public abstract setRightBorderColor(S)V
.end method

.method public abstract setTopBorderColor(Lorg/apache/poi/ss/usermodel/Color;)V
.end method

.method public abstract setTopBorderColor(S)V
.end method

.method public abstract setVerticalBorderColor(Lorg/apache/poi/ss/usermodel/Color;)V
.end method

.method public abstract setVerticalBorderColor(S)V
.end method

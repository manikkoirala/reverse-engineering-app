.class public final enum Lorg/apache/poi/ss/usermodel/FillPatternType;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lorg/apache/poi/ss/usermodel/FillPatternType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lorg/apache/poi/ss/usermodel/FillPatternType;

.field public static final enum ALT_BARS:Lorg/apache/poi/ss/usermodel/FillPatternType;

.field public static final enum BIG_SPOTS:Lorg/apache/poi/ss/usermodel/FillPatternType;

.field public static final enum BRICKS:Lorg/apache/poi/ss/usermodel/FillPatternType;

.field public static final enum DIAMONDS:Lorg/apache/poi/ss/usermodel/FillPatternType;

.field public static final enum FINE_DOTS:Lorg/apache/poi/ss/usermodel/FillPatternType;

.field public static final enum LEAST_DOTS:Lorg/apache/poi/ss/usermodel/FillPatternType;

.field public static final enum LESS_DOTS:Lorg/apache/poi/ss/usermodel/FillPatternType;

.field public static final enum NO_FILL:Lorg/apache/poi/ss/usermodel/FillPatternType;

.field public static final enum SOLID_FOREGROUND:Lorg/apache/poi/ss/usermodel/FillPatternType;

.field public static final enum SPARSE_DOTS:Lorg/apache/poi/ss/usermodel/FillPatternType;

.field public static final enum SQUARES:Lorg/apache/poi/ss/usermodel/FillPatternType;

.field public static final enum THICK_BACKWARD_DIAG:Lorg/apache/poi/ss/usermodel/FillPatternType;

.field public static final enum THICK_FORWARD_DIAG:Lorg/apache/poi/ss/usermodel/FillPatternType;

.field public static final enum THICK_HORZ_BANDS:Lorg/apache/poi/ss/usermodel/FillPatternType;

.field public static final enum THICK_VERT_BANDS:Lorg/apache/poi/ss/usermodel/FillPatternType;

.field public static final enum THIN_BACKWARD_DIAG:Lorg/apache/poi/ss/usermodel/FillPatternType;

.field public static final enum THIN_FORWARD_DIAG:Lorg/apache/poi/ss/usermodel/FillPatternType;

.field public static final enum THIN_HORZ_BANDS:Lorg/apache/poi/ss/usermodel/FillPatternType;

.field public static final enum THIN_VERT_BANDS:Lorg/apache/poi/ss/usermodel/FillPatternType;

.field private static final length:I


# instance fields
.field private final code:S


# direct methods
.method public static constructor <clinit>()V
    .locals 22

    new-instance v1, Lorg/apache/poi/ss/usermodel/FillPatternType;

    move-object v0, v1

    const-string v2, "NO_FILL"

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3, v3}, Lorg/apache/poi/ss/usermodel/FillPatternType;-><init>(Ljava/lang/String;II)V

    sput-object v1, Lorg/apache/poi/ss/usermodel/FillPatternType;->NO_FILL:Lorg/apache/poi/ss/usermodel/FillPatternType;

    new-instance v2, Lorg/apache/poi/ss/usermodel/FillPatternType;

    move-object v1, v2

    const-string v3, "SOLID_FOREGROUND"

    const/4 v4, 0x1

    invoke-direct {v2, v3, v4, v4}, Lorg/apache/poi/ss/usermodel/FillPatternType;-><init>(Ljava/lang/String;II)V

    sput-object v2, Lorg/apache/poi/ss/usermodel/FillPatternType;->SOLID_FOREGROUND:Lorg/apache/poi/ss/usermodel/FillPatternType;

    new-instance v3, Lorg/apache/poi/ss/usermodel/FillPatternType;

    move-object v2, v3

    const-string v4, "FINE_DOTS"

    const/4 v5, 0x2

    invoke-direct {v3, v4, v5, v5}, Lorg/apache/poi/ss/usermodel/FillPatternType;-><init>(Ljava/lang/String;II)V

    sput-object v3, Lorg/apache/poi/ss/usermodel/FillPatternType;->FINE_DOTS:Lorg/apache/poi/ss/usermodel/FillPatternType;

    new-instance v4, Lorg/apache/poi/ss/usermodel/FillPatternType;

    move-object v3, v4

    const-string v5, "ALT_BARS"

    const/4 v6, 0x3

    invoke-direct {v4, v5, v6, v6}, Lorg/apache/poi/ss/usermodel/FillPatternType;-><init>(Ljava/lang/String;II)V

    sput-object v4, Lorg/apache/poi/ss/usermodel/FillPatternType;->ALT_BARS:Lorg/apache/poi/ss/usermodel/FillPatternType;

    new-instance v5, Lorg/apache/poi/ss/usermodel/FillPatternType;

    move-object v4, v5

    const-string v6, "SPARSE_DOTS"

    const/4 v7, 0x4

    invoke-direct {v5, v6, v7, v7}, Lorg/apache/poi/ss/usermodel/FillPatternType;-><init>(Ljava/lang/String;II)V

    sput-object v5, Lorg/apache/poi/ss/usermodel/FillPatternType;->SPARSE_DOTS:Lorg/apache/poi/ss/usermodel/FillPatternType;

    new-instance v6, Lorg/apache/poi/ss/usermodel/FillPatternType;

    move-object v5, v6

    const-string v7, "THICK_HORZ_BANDS"

    const/4 v8, 0x5

    invoke-direct {v6, v7, v8, v8}, Lorg/apache/poi/ss/usermodel/FillPatternType;-><init>(Ljava/lang/String;II)V

    sput-object v6, Lorg/apache/poi/ss/usermodel/FillPatternType;->THICK_HORZ_BANDS:Lorg/apache/poi/ss/usermodel/FillPatternType;

    new-instance v7, Lorg/apache/poi/ss/usermodel/FillPatternType;

    move-object v6, v7

    const-string v8, "THICK_VERT_BANDS"

    const/4 v9, 0x6

    invoke-direct {v7, v8, v9, v9}, Lorg/apache/poi/ss/usermodel/FillPatternType;-><init>(Ljava/lang/String;II)V

    sput-object v7, Lorg/apache/poi/ss/usermodel/FillPatternType;->THICK_VERT_BANDS:Lorg/apache/poi/ss/usermodel/FillPatternType;

    new-instance v8, Lorg/apache/poi/ss/usermodel/FillPatternType;

    move-object v7, v8

    const-string v9, "THICK_BACKWARD_DIAG"

    const/4 v10, 0x7

    invoke-direct {v8, v9, v10, v10}, Lorg/apache/poi/ss/usermodel/FillPatternType;-><init>(Ljava/lang/String;II)V

    sput-object v8, Lorg/apache/poi/ss/usermodel/FillPatternType;->THICK_BACKWARD_DIAG:Lorg/apache/poi/ss/usermodel/FillPatternType;

    new-instance v9, Lorg/apache/poi/ss/usermodel/FillPatternType;

    move-object v8, v9

    const-string v10, "THICK_FORWARD_DIAG"

    const/16 v11, 0x8

    invoke-direct {v9, v10, v11, v11}, Lorg/apache/poi/ss/usermodel/FillPatternType;-><init>(Ljava/lang/String;II)V

    sput-object v9, Lorg/apache/poi/ss/usermodel/FillPatternType;->THICK_FORWARD_DIAG:Lorg/apache/poi/ss/usermodel/FillPatternType;

    new-instance v10, Lorg/apache/poi/ss/usermodel/FillPatternType;

    move-object v9, v10

    const-string v11, "BIG_SPOTS"

    const/16 v12, 0x9

    invoke-direct {v10, v11, v12, v12}, Lorg/apache/poi/ss/usermodel/FillPatternType;-><init>(Ljava/lang/String;II)V

    sput-object v10, Lorg/apache/poi/ss/usermodel/FillPatternType;->BIG_SPOTS:Lorg/apache/poi/ss/usermodel/FillPatternType;

    new-instance v11, Lorg/apache/poi/ss/usermodel/FillPatternType;

    move-object v10, v11

    const-string v12, "BRICKS"

    const/16 v13, 0xa

    invoke-direct {v11, v12, v13, v13}, Lorg/apache/poi/ss/usermodel/FillPatternType;-><init>(Ljava/lang/String;II)V

    sput-object v11, Lorg/apache/poi/ss/usermodel/FillPatternType;->BRICKS:Lorg/apache/poi/ss/usermodel/FillPatternType;

    new-instance v12, Lorg/apache/poi/ss/usermodel/FillPatternType;

    move-object v11, v12

    const-string v13, "THIN_HORZ_BANDS"

    const/16 v14, 0xb

    invoke-direct {v12, v13, v14, v14}, Lorg/apache/poi/ss/usermodel/FillPatternType;-><init>(Ljava/lang/String;II)V

    sput-object v12, Lorg/apache/poi/ss/usermodel/FillPatternType;->THIN_HORZ_BANDS:Lorg/apache/poi/ss/usermodel/FillPatternType;

    new-instance v13, Lorg/apache/poi/ss/usermodel/FillPatternType;

    move-object v12, v13

    const-string v14, "THIN_VERT_BANDS"

    const/16 v15, 0xc

    invoke-direct {v13, v14, v15, v15}, Lorg/apache/poi/ss/usermodel/FillPatternType;-><init>(Ljava/lang/String;II)V

    sput-object v13, Lorg/apache/poi/ss/usermodel/FillPatternType;->THIN_VERT_BANDS:Lorg/apache/poi/ss/usermodel/FillPatternType;

    new-instance v14, Lorg/apache/poi/ss/usermodel/FillPatternType;

    move-object v13, v14

    const-string v15, "THIN_BACKWARD_DIAG"

    move-object/from16 v19, v0

    const/16 v0, 0xd

    invoke-direct {v14, v15, v0, v0}, Lorg/apache/poi/ss/usermodel/FillPatternType;-><init>(Ljava/lang/String;II)V

    sput-object v14, Lorg/apache/poi/ss/usermodel/FillPatternType;->THIN_BACKWARD_DIAG:Lorg/apache/poi/ss/usermodel/FillPatternType;

    new-instance v0, Lorg/apache/poi/ss/usermodel/FillPatternType;

    move-object v14, v0

    const-string v15, "THIN_FORWARD_DIAG"

    move-object/from16 v20, v1

    const/16 v1, 0xe

    invoke-direct {v0, v15, v1, v1}, Lorg/apache/poi/ss/usermodel/FillPatternType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/ss/usermodel/FillPatternType;->THIN_FORWARD_DIAG:Lorg/apache/poi/ss/usermodel/FillPatternType;

    new-instance v0, Lorg/apache/poi/ss/usermodel/FillPatternType;

    move-object v15, v0

    const-string v1, "SQUARES"

    move-object/from16 v21, v2

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2, v2}, Lorg/apache/poi/ss/usermodel/FillPatternType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/ss/usermodel/FillPatternType;->SQUARES:Lorg/apache/poi/ss/usermodel/FillPatternType;

    new-instance v0, Lorg/apache/poi/ss/usermodel/FillPatternType;

    move-object/from16 v16, v0

    const-string v1, "DIAMONDS"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2, v2}, Lorg/apache/poi/ss/usermodel/FillPatternType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/ss/usermodel/FillPatternType;->DIAMONDS:Lorg/apache/poi/ss/usermodel/FillPatternType;

    new-instance v0, Lorg/apache/poi/ss/usermodel/FillPatternType;

    move-object/from16 v17, v0

    const-string v1, "LESS_DOTS"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2, v2}, Lorg/apache/poi/ss/usermodel/FillPatternType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/ss/usermodel/FillPatternType;->LESS_DOTS:Lorg/apache/poi/ss/usermodel/FillPatternType;

    new-instance v0, Lorg/apache/poi/ss/usermodel/FillPatternType;

    move-object/from16 v18, v0

    const-string v1, "LEAST_DOTS"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2, v2}, Lorg/apache/poi/ss/usermodel/FillPatternType;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lorg/apache/poi/ss/usermodel/FillPatternType;->LEAST_DOTS:Lorg/apache/poi/ss/usermodel/FillPatternType;

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    move-object/from16 v2, v21

    filled-new-array/range {v0 .. v18}, [Lorg/apache/poi/ss/usermodel/FillPatternType;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/ss/usermodel/FillPatternType;->$VALUES:[Lorg/apache/poi/ss/usermodel/FillPatternType;

    invoke-static {}, Lorg/apache/poi/ss/usermodel/FillPatternType;->values()[Lorg/apache/poi/ss/usermodel/FillPatternType;

    move-result-object v0

    array-length v0, v0

    sput v0, Lorg/apache/poi/ss/usermodel/FillPatternType;->length:I

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    int-to-short p1, p3

    iput-short p1, p0, Lorg/apache/poi/ss/usermodel/FillPatternType;->code:S

    return-void
.end method

.method public static forInt(I)Lorg/apache/poi/ss/usermodel/FillPatternType;
    .locals 3

    if-ltz p0, :cond_0

    sget v0, Lorg/apache/poi/ss/usermodel/FillPatternType;->length:I

    if-gt p0, v0, :cond_0

    invoke-static {}, Lorg/apache/poi/ss/usermodel/FillPatternType;->values()[Lorg/apache/poi/ss/usermodel/FillPatternType;

    move-result-object v0

    aget-object p0, v0, p0

    return-object p0

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid FillPatternType code: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static valueOf(Ljava/lang/String;)Lorg/apache/poi/ss/usermodel/FillPatternType;
    .locals 1

    const-class v0, Lorg/apache/poi/ss/usermodel/FillPatternType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lorg/apache/poi/ss/usermodel/FillPatternType;

    return-object p0
.end method

.method public static values()[Lorg/apache/poi/ss/usermodel/FillPatternType;
    .locals 1

    sget-object v0, Lorg/apache/poi/ss/usermodel/FillPatternType;->$VALUES:[Lorg/apache/poi/ss/usermodel/FillPatternType;

    invoke-virtual {v0}, [Lorg/apache/poi/ss/usermodel/FillPatternType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lorg/apache/poi/ss/usermodel/FillPatternType;

    return-object v0
.end method


# virtual methods
.method public getCode()S
    .locals 1

    iget-short v0, p0, Lorg/apache/poi/ss/usermodel/FillPatternType;->code:S

    return v0
.end method

.class public interface abstract Lorg/apache/poi/ss/usermodel/ConditionalFormattingRule;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/poi/ss/usermodel/DifferentialStyleProvider;


# virtual methods
.method public abstract createBorderFormatting()Lorg/apache/poi/ss/usermodel/BorderFormatting;
.end method

.method public abstract createFontFormatting()Lorg/apache/poi/ss/usermodel/FontFormatting;
.end method

.method public abstract createPatternFormatting()Lorg/apache/poi/ss/usermodel/PatternFormatting;
.end method

.method public abstract getBorderFormatting()Lorg/apache/poi/ss/usermodel/BorderFormatting;
.end method

.method public abstract getColorScaleFormatting()Lorg/apache/poi/ss/usermodel/ColorScaleFormatting;
.end method

.method public abstract getComparisonOperation()B
.end method

.method public abstract getConditionFilterType()Lorg/apache/poi/ss/usermodel/ConditionFilterType;
.end method

.method public abstract getConditionType()Lorg/apache/poi/ss/usermodel/ConditionType;
.end method

.method public abstract getDataBarFormatting()Lorg/apache/poi/ss/usermodel/DataBarFormatting;
.end method

.method public abstract getFilterConfiguration()Lorg/apache/poi/ss/usermodel/ConditionFilterData;
.end method

.method public abstract getFontFormatting()Lorg/apache/poi/ss/usermodel/FontFormatting;
.end method

.method public abstract getFormula1()Ljava/lang/String;
.end method

.method public abstract getFormula2()Ljava/lang/String;
.end method

.method public abstract getMultiStateFormatting()Lorg/apache/poi/ss/usermodel/IconMultiStateFormatting;
.end method

.method public abstract getNumberFormat()Lorg/apache/poi/ss/usermodel/ExcelNumberFormat;
.end method

.method public abstract getPatternFormatting()Lorg/apache/poi/ss/usermodel/PatternFormatting;
.end method

.method public abstract getPriority()I
.end method

.method public abstract getStopIfTrue()Z
.end method

.class public interface abstract Lorg/apache/poi/ss/usermodel/ObjectData;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/poi/ss/usermodel/SimpleShape;


# virtual methods
.method public abstract getDirectory()Lorg/apache/poi/poifs/filesystem/DirectoryEntry;
.end method

.method public abstract getFileName()Ljava/lang/String;
.end method

.method public abstract getOLE2ClassName()Ljava/lang/String;
.end method

.method public abstract getObjectData()[B
.end method

.method public abstract getPictureData()Lorg/apache/poi/ss/usermodel/PictureData;
.end method

.method public abstract hasDirectoryEntry()Z
.end method

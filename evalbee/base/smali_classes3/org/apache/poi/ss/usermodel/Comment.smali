.class public interface abstract Lorg/apache/poi/ss/usermodel/Comment;
.super Ljava/lang/Object;
.source "SourceFile"


# virtual methods
.method public abstract getAddress()Lorg/apache/poi/ss/util/CellAddress;
.end method

.method public abstract getAuthor()Ljava/lang/String;
.end method

.method public abstract getClientAnchor()Lorg/apache/poi/ss/usermodel/ClientAnchor;
.end method

.method public abstract getColumn()I
.end method

.method public abstract getRow()I
.end method

.method public abstract getString()Lorg/apache/poi/ss/usermodel/RichTextString;
.end method

.method public abstract isVisible()Z
.end method

.method public abstract setAddress(II)V
.end method

.method public abstract setAddress(Lorg/apache/poi/ss/util/CellAddress;)V
.end method

.method public abstract setAuthor(Ljava/lang/String;)V
.end method

.method public abstract setColumn(I)V
.end method

.method public abstract setRow(I)V
.end method

.method public abstract setString(Lorg/apache/poi/ss/usermodel/RichTextString;)V
.end method

.method public abstract setVisible(Z)V
.end method

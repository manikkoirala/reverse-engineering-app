.class public interface abstract Lorg/apache/poi/ss/usermodel/ConditionalFormattingThreshold;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/poi/ss/usermodel/ConditionalFormattingThreshold$RangeType;
    }
.end annotation


# virtual methods
.method public abstract getFormula()Ljava/lang/String;
.end method

.method public abstract getRangeType()Lorg/apache/poi/ss/usermodel/ConditionalFormattingThreshold$RangeType;
.end method

.method public abstract getValue()Ljava/lang/Double;
.end method

.method public abstract setFormula(Ljava/lang/String;)V
.end method

.method public abstract setRangeType(Lorg/apache/poi/ss/usermodel/ConditionalFormattingThreshold$RangeType;)V
.end method

.method public abstract setValue(Ljava/lang/Double;)V
.end method

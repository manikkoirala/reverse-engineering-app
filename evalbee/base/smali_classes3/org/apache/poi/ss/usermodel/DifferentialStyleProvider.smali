.class public interface abstract Lorg/apache/poi/ss/usermodel/DifferentialStyleProvider;
.super Ljava/lang/Object;
.source "SourceFile"


# virtual methods
.method public abstract getBorderFormatting()Lorg/apache/poi/ss/usermodel/BorderFormatting;
.end method

.method public abstract getFontFormatting()Lorg/apache/poi/ss/usermodel/FontFormatting;
.end method

.method public abstract getNumberFormat()Lorg/apache/poi/ss/usermodel/ExcelNumberFormat;
.end method

.method public abstract getPatternFormatting()Lorg/apache/poi/ss/usermodel/PatternFormatting;
.end method

.method public abstract getStripeSize()I
.end method

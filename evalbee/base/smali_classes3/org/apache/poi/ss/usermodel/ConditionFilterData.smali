.class public interface abstract Lorg/apache/poi/ss/usermodel/ConditionFilterData;
.super Ljava/lang/Object;
.source "SourceFile"


# virtual methods
.method public abstract getAboveAverage()Z
.end method

.method public abstract getBottom()Z
.end method

.method public abstract getEqualAverage()Z
.end method

.method public abstract getPercent()Z
.end method

.method public abstract getRank()J
.end method

.method public abstract getStdDev()I
.end method

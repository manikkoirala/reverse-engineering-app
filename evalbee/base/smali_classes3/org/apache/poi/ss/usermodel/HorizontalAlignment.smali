.class public final enum Lorg/apache/poi/ss/usermodel/HorizontalAlignment;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lorg/apache/poi/ss/usermodel/HorizontalAlignment;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lorg/apache/poi/ss/usermodel/HorizontalAlignment;

.field public static final enum CENTER:Lorg/apache/poi/ss/usermodel/HorizontalAlignment;

.field public static final enum CENTER_SELECTION:Lorg/apache/poi/ss/usermodel/HorizontalAlignment;

.field public static final enum DISTRIBUTED:Lorg/apache/poi/ss/usermodel/HorizontalAlignment;

.field public static final enum FILL:Lorg/apache/poi/ss/usermodel/HorizontalAlignment;

.field public static final enum GENERAL:Lorg/apache/poi/ss/usermodel/HorizontalAlignment;

.field public static final enum JUSTIFY:Lorg/apache/poi/ss/usermodel/HorizontalAlignment;

.field public static final enum LEFT:Lorg/apache/poi/ss/usermodel/HorizontalAlignment;

.field public static final enum RIGHT:Lorg/apache/poi/ss/usermodel/HorizontalAlignment;


# direct methods
.method public static constructor <clinit>()V
    .locals 10

    new-instance v0, Lorg/apache/poi/ss/usermodel/HorizontalAlignment;

    const-string v1, "GENERAL"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lorg/apache/poi/ss/usermodel/HorizontalAlignment;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/poi/ss/usermodel/HorizontalAlignment;->GENERAL:Lorg/apache/poi/ss/usermodel/HorizontalAlignment;

    new-instance v1, Lorg/apache/poi/ss/usermodel/HorizontalAlignment;

    const-string v2, "LEFT"

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Lorg/apache/poi/ss/usermodel/HorizontalAlignment;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lorg/apache/poi/ss/usermodel/HorizontalAlignment;->LEFT:Lorg/apache/poi/ss/usermodel/HorizontalAlignment;

    new-instance v2, Lorg/apache/poi/ss/usermodel/HorizontalAlignment;

    const-string v3, "CENTER"

    const/4 v4, 0x2

    invoke-direct {v2, v3, v4}, Lorg/apache/poi/ss/usermodel/HorizontalAlignment;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lorg/apache/poi/ss/usermodel/HorizontalAlignment;->CENTER:Lorg/apache/poi/ss/usermodel/HorizontalAlignment;

    new-instance v3, Lorg/apache/poi/ss/usermodel/HorizontalAlignment;

    const-string v4, "RIGHT"

    const/4 v5, 0x3

    invoke-direct {v3, v4, v5}, Lorg/apache/poi/ss/usermodel/HorizontalAlignment;-><init>(Ljava/lang/String;I)V

    sput-object v3, Lorg/apache/poi/ss/usermodel/HorizontalAlignment;->RIGHT:Lorg/apache/poi/ss/usermodel/HorizontalAlignment;

    new-instance v4, Lorg/apache/poi/ss/usermodel/HorizontalAlignment;

    const-string v5, "FILL"

    const/4 v6, 0x4

    invoke-direct {v4, v5, v6}, Lorg/apache/poi/ss/usermodel/HorizontalAlignment;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lorg/apache/poi/ss/usermodel/HorizontalAlignment;->FILL:Lorg/apache/poi/ss/usermodel/HorizontalAlignment;

    new-instance v5, Lorg/apache/poi/ss/usermodel/HorizontalAlignment;

    const-string v6, "JUSTIFY"

    const/4 v7, 0x5

    invoke-direct {v5, v6, v7}, Lorg/apache/poi/ss/usermodel/HorizontalAlignment;-><init>(Ljava/lang/String;I)V

    sput-object v5, Lorg/apache/poi/ss/usermodel/HorizontalAlignment;->JUSTIFY:Lorg/apache/poi/ss/usermodel/HorizontalAlignment;

    new-instance v6, Lorg/apache/poi/ss/usermodel/HorizontalAlignment;

    const-string v7, "CENTER_SELECTION"

    const/4 v8, 0x6

    invoke-direct {v6, v7, v8}, Lorg/apache/poi/ss/usermodel/HorizontalAlignment;-><init>(Ljava/lang/String;I)V

    sput-object v6, Lorg/apache/poi/ss/usermodel/HorizontalAlignment;->CENTER_SELECTION:Lorg/apache/poi/ss/usermodel/HorizontalAlignment;

    new-instance v7, Lorg/apache/poi/ss/usermodel/HorizontalAlignment;

    const-string v8, "DISTRIBUTED"

    const/4 v9, 0x7

    invoke-direct {v7, v8, v9}, Lorg/apache/poi/ss/usermodel/HorizontalAlignment;-><init>(Ljava/lang/String;I)V

    sput-object v7, Lorg/apache/poi/ss/usermodel/HorizontalAlignment;->DISTRIBUTED:Lorg/apache/poi/ss/usermodel/HorizontalAlignment;

    filled-new-array/range {v0 .. v7}, [Lorg/apache/poi/ss/usermodel/HorizontalAlignment;

    move-result-object v0

    sput-object v0, Lorg/apache/poi/ss/usermodel/HorizontalAlignment;->$VALUES:[Lorg/apache/poi/ss/usermodel/HorizontalAlignment;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static forInt(I)Lorg/apache/poi/ss/usermodel/HorizontalAlignment;
    .locals 3

    if-ltz p0, :cond_0

    invoke-static {}, Lorg/apache/poi/ss/usermodel/HorizontalAlignment;->values()[Lorg/apache/poi/ss/usermodel/HorizontalAlignment;

    move-result-object v0

    array-length v0, v0

    if-ge p0, v0, :cond_0

    invoke-static {}, Lorg/apache/poi/ss/usermodel/HorizontalAlignment;->values()[Lorg/apache/poi/ss/usermodel/HorizontalAlignment;

    move-result-object v0

    aget-object p0, v0, p0

    return-object p0

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid HorizontalAlignment code: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static valueOf(Ljava/lang/String;)Lorg/apache/poi/ss/usermodel/HorizontalAlignment;
    .locals 1

    const-class v0, Lorg/apache/poi/ss/usermodel/HorizontalAlignment;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lorg/apache/poi/ss/usermodel/HorizontalAlignment;

    return-object p0
.end method

.method public static values()[Lorg/apache/poi/ss/usermodel/HorizontalAlignment;
    .locals 1

    sget-object v0, Lorg/apache/poi/ss/usermodel/HorizontalAlignment;->$VALUES:[Lorg/apache/poi/ss/usermodel/HorizontalAlignment;

    invoke-virtual {v0}, [Lorg/apache/poi/ss/usermodel/HorizontalAlignment;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lorg/apache/poi/ss/usermodel/HorizontalAlignment;

    return-object v0
.end method


# virtual methods
.method public getCode()S
    .locals 1

    invoke-virtual {p0}, Ljava/lang/Enum;->ordinal()I

    move-result v0

    int-to-short v0, v0

    return v0
.end method

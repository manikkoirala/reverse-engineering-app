.class public interface abstract Lorg/apache/poi/ss/usermodel/ColorScaleFormatting;
.super Ljava/lang/Object;
.source "SourceFile"


# virtual methods
.method public abstract createThreshold()Lorg/apache/poi/ss/usermodel/ConditionalFormattingThreshold;
.end method

.method public abstract getColors()[Lorg/apache/poi/ss/usermodel/Color;
.end method

.method public abstract getNumControlPoints()I
.end method

.method public abstract getThresholds()[Lorg/apache/poi/ss/usermodel/ConditionalFormattingThreshold;
.end method

.method public abstract setColors([Lorg/apache/poi/ss/usermodel/Color;)V
.end method

.method public abstract setNumControlPoints(I)V
.end method

.method public abstract setThresholds([Lorg/apache/poi/ss/usermodel/ConditionalFormattingThreshold;)V
.end method

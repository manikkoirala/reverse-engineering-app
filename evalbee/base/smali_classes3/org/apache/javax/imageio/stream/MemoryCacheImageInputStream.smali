.class public Lorg/apache/javax/imageio/stream/MemoryCacheImageInputStream;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field protected bitOffset:I

.field private cache:Lorg/apache/javax/imageio/stream/MemoryCache;

.field protected flushedPos:J

.field private stream:Ljava/io/InputStream;

.field protected streamPos:J


# direct methods
.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lorg/apache/javax/imageio/stream/MemoryCacheImageInputStream;->flushedPos:J

    new-instance v0, Lorg/apache/javax/imageio/stream/MemoryCache;

    invoke-direct {v0}, Lorg/apache/javax/imageio/stream/MemoryCache;-><init>()V

    iput-object v0, p0, Lorg/apache/javax/imageio/stream/MemoryCacheImageInputStream;->cache:Lorg/apache/javax/imageio/stream/MemoryCache;

    if-eqz p1, :cond_0

    iput-object p1, p0, Lorg/apache/javax/imageio/stream/MemoryCacheImageInputStream;->stream:Ljava/io/InputStream;

    return-void

    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "stream == null!"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public read()I
    .locals 8

    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/javax/imageio/stream/MemoryCacheImageInputStream;->bitOffset:I

    iget-object v0, p0, Lorg/apache/javax/imageio/stream/MemoryCacheImageInputStream;->cache:Lorg/apache/javax/imageio/stream/MemoryCache;

    iget-object v1, p0, Lorg/apache/javax/imageio/stream/MemoryCacheImageInputStream;->stream:Ljava/io/InputStream;

    iget-wide v2, p0, Lorg/apache/javax/imageio/stream/MemoryCacheImageInputStream;->streamPos:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    invoke-virtual {v0, v1, v2, v3}, Lorg/apache/javax/imageio/stream/MemoryCache;->loadFromStream(Ljava/io/InputStream;J)J

    move-result-wide v0

    iget-wide v2, p0, Lorg/apache/javax/imageio/stream/MemoryCacheImageInputStream;->streamPos:J

    add-long v6, v2, v4

    cmp-long v0, v0, v6

    if-ltz v0, :cond_0

    iget-object v0, p0, Lorg/apache/javax/imageio/stream/MemoryCacheImageInputStream;->cache:Lorg/apache/javax/imageio/stream/MemoryCache;

    add-long/2addr v4, v2

    iput-wide v4, p0, Lorg/apache/javax/imageio/stream/MemoryCacheImageInputStream;->streamPos:J

    invoke-virtual {v0, v2, v3}, Lorg/apache/javax/imageio/stream/MemoryCache;->read(J)I

    move-result v0

    return v0

    :cond_0
    const/4 v0, -0x1

    return v0
.end method

.method public readBits(I)J
    .locals 9

    if-ltz p1, :cond_4

    const/16 v0, 0x40

    if-gt p1, v0, :cond_4

    const-wide/16 v1, 0x0

    if-nez p1, :cond_0

    return-wide v1

    :cond_0
    iget v3, p0, Lorg/apache/javax/imageio/stream/MemoryCacheImageInputStream;->bitOffset:I

    add-int v4, p1, v3

    add-int/2addr v3, p1

    and-int/lit8 v3, v3, 0x7

    :goto_0
    if-lez v4, :cond_2

    invoke-virtual {p0}, Lorg/apache/javax/imageio/stream/MemoryCacheImageInputStream;->read()I

    move-result v5

    const/4 v6, -0x1

    if-eq v5, v6, :cond_1

    const/16 v6, 0x8

    shl-long/2addr v1, v6

    int-to-long v5, v5

    or-long/2addr v1, v5

    add-int/lit8 v4, v4, -0x8

    goto :goto_0

    :cond_1
    new-instance p1, Ljava/io/EOFException;

    invoke-direct {p1}, Ljava/io/EOFException;-><init>()V

    throw p1

    :cond_2
    if-eqz v3, :cond_3

    iget-wide v5, p0, Lorg/apache/javax/imageio/stream/MemoryCacheImageInputStream;->streamPos:J

    const-wide/16 v7, 0x1

    sub-long/2addr v5, v7

    invoke-virtual {p0, v5, v6}, Lorg/apache/javax/imageio/stream/MemoryCacheImageInputStream;->seek(J)V

    :cond_3
    iput v3, p0, Lorg/apache/javax/imageio/stream/MemoryCacheImageInputStream;->bitOffset:I

    neg-int v3, v4

    ushr-long/2addr v1, v3

    const-wide/16 v3, -0x1

    sub-int/2addr v0, p1

    ushr-long/2addr v3, v0

    and-long v0, v1, v3

    return-wide v0

    :cond_4
    new-instance p1, Ljava/lang/IllegalArgumentException;

    invoke-direct {p1}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw p1
.end method

.method public seek(J)V
    .locals 2

    iget-wide v0, p0, Lorg/apache/javax/imageio/stream/MemoryCacheImageInputStream;->flushedPos:J

    cmp-long v0, p1, v0

    if-ltz v0, :cond_0

    iput-wide p1, p0, Lorg/apache/javax/imageio/stream/MemoryCacheImageInputStream;->streamPos:J

    const/4 p1, 0x0

    iput p1, p0, Lorg/apache/javax/imageio/stream/MemoryCacheImageInputStream;->bitOffset:I

    return-void

    :cond_0
    new-instance p1, Ljava/lang/IndexOutOfBoundsException;

    const-string p2, "pos < flushedPos!"

    invoke-direct {p1, p2}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

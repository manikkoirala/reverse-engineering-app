.class Lorg/apache/pdfbox/text/PDFTextStreamEngine;
.super Lorg/apache/pdfbox/contentstream/PDFStreamEngine;
.source "SourceFile"


# instance fields
.field private final glyphList:Lorg/apache/pdfbox/pdmodel/font/encoding/GlyphList;

.field private legacyCTM:Lorg/apache/pdfbox/util/Matrix;

.field private pageRotation:I

.field private pageSize:Lorg/apache/pdfbox/pdmodel/common/PDRectangle;


# direct methods
.method public constructor <init>()V
    .locals 3

    invoke-direct {p0}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;-><init>()V

    new-instance v0, Lorg/apache/pdfbox/contentstream/operator/text/BeginText;

    invoke-direct {v0}, Lorg/apache/pdfbox/contentstream/operator/text/BeginText;-><init>()V

    invoke-virtual {p0, v0}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->addOperator(Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;)V

    new-instance v0, Lorg/apache/pdfbox/contentstream/operator/state/Concatenate;

    invoke-direct {v0}, Lorg/apache/pdfbox/contentstream/operator/state/Concatenate;-><init>()V

    invoke-virtual {p0, v0}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->addOperator(Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;)V

    new-instance v0, Lorg/apache/pdfbox/contentstream/operator/DrawObject;

    invoke-direct {v0}, Lorg/apache/pdfbox/contentstream/operator/DrawObject;-><init>()V

    invoke-virtual {p0, v0}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->addOperator(Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;)V

    new-instance v0, Lorg/apache/pdfbox/contentstream/operator/text/EndText;

    invoke-direct {v0}, Lorg/apache/pdfbox/contentstream/operator/text/EndText;-><init>()V

    invoke-virtual {p0, v0}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->addOperator(Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;)V

    new-instance v0, Lorg/apache/pdfbox/contentstream/operator/state/SetGraphicsStateParameters;

    invoke-direct {v0}, Lorg/apache/pdfbox/contentstream/operator/state/SetGraphicsStateParameters;-><init>()V

    invoke-virtual {p0, v0}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->addOperator(Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;)V

    new-instance v0, Lorg/apache/pdfbox/contentstream/operator/state/Save;

    invoke-direct {v0}, Lorg/apache/pdfbox/contentstream/operator/state/Save;-><init>()V

    invoke-virtual {p0, v0}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->addOperator(Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;)V

    new-instance v0, Lorg/apache/pdfbox/contentstream/operator/state/Restore;

    invoke-direct {v0}, Lorg/apache/pdfbox/contentstream/operator/state/Restore;-><init>()V

    invoke-virtual {p0, v0}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->addOperator(Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;)V

    new-instance v0, Lorg/apache/pdfbox/contentstream/operator/text/NextLine;

    invoke-direct {v0}, Lorg/apache/pdfbox/contentstream/operator/text/NextLine;-><init>()V

    invoke-virtual {p0, v0}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->addOperator(Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;)V

    new-instance v0, Lorg/apache/pdfbox/contentstream/operator/text/SetCharSpacing;

    invoke-direct {v0}, Lorg/apache/pdfbox/contentstream/operator/text/SetCharSpacing;-><init>()V

    invoke-virtual {p0, v0}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->addOperator(Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;)V

    new-instance v0, Lorg/apache/pdfbox/contentstream/operator/text/MoveText;

    invoke-direct {v0}, Lorg/apache/pdfbox/contentstream/operator/text/MoveText;-><init>()V

    invoke-virtual {p0, v0}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->addOperator(Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;)V

    new-instance v0, Lorg/apache/pdfbox/contentstream/operator/text/MoveTextSetLeading;

    invoke-direct {v0}, Lorg/apache/pdfbox/contentstream/operator/text/MoveTextSetLeading;-><init>()V

    invoke-virtual {p0, v0}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->addOperator(Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;)V

    new-instance v0, Lorg/apache/pdfbox/contentstream/operator/text/SetFontAndSize;

    invoke-direct {v0}, Lorg/apache/pdfbox/contentstream/operator/text/SetFontAndSize;-><init>()V

    invoke-virtual {p0, v0}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->addOperator(Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;)V

    new-instance v0, Lorg/apache/pdfbox/contentstream/operator/text/ShowText;

    invoke-direct {v0}, Lorg/apache/pdfbox/contentstream/operator/text/ShowText;-><init>()V

    invoke-virtual {p0, v0}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->addOperator(Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;)V

    new-instance v0, Lorg/apache/pdfbox/contentstream/operator/text/ShowTextAdjusted;

    invoke-direct {v0}, Lorg/apache/pdfbox/contentstream/operator/text/ShowTextAdjusted;-><init>()V

    invoke-virtual {p0, v0}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->addOperator(Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;)V

    new-instance v0, Lorg/apache/pdfbox/contentstream/operator/text/SetTextLeading;

    invoke-direct {v0}, Lorg/apache/pdfbox/contentstream/operator/text/SetTextLeading;-><init>()V

    invoke-virtual {p0, v0}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->addOperator(Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;)V

    new-instance v0, Lorg/apache/pdfbox/contentstream/operator/state/SetMatrix;

    invoke-direct {v0}, Lorg/apache/pdfbox/contentstream/operator/state/SetMatrix;-><init>()V

    invoke-virtual {p0, v0}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->addOperator(Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;)V

    new-instance v0, Lorg/apache/pdfbox/contentstream/operator/text/SetTextRenderingMode;

    invoke-direct {v0}, Lorg/apache/pdfbox/contentstream/operator/text/SetTextRenderingMode;-><init>()V

    invoke-virtual {p0, v0}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->addOperator(Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;)V

    new-instance v0, Lorg/apache/pdfbox/contentstream/operator/text/SetTextRise;

    invoke-direct {v0}, Lorg/apache/pdfbox/contentstream/operator/text/SetTextRise;-><init>()V

    invoke-virtual {p0, v0}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->addOperator(Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;)V

    new-instance v0, Lorg/apache/pdfbox/contentstream/operator/text/SetWordSpacing;

    invoke-direct {v0}, Lorg/apache/pdfbox/contentstream/operator/text/SetWordSpacing;-><init>()V

    invoke-virtual {p0, v0}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->addOperator(Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;)V

    new-instance v0, Lorg/apache/pdfbox/contentstream/operator/text/SetTextHorizontalScaling;

    invoke-direct {v0}, Lorg/apache/pdfbox/contentstream/operator/text/SetTextHorizontalScaling;-><init>()V

    invoke-virtual {p0, v0}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->addOperator(Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;)V

    new-instance v0, Lorg/apache/pdfbox/contentstream/operator/text/ShowTextLine;

    invoke-direct {v0}, Lorg/apache/pdfbox/contentstream/operator/text/ShowTextLine;-><init>()V

    invoke-virtual {p0, v0}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->addOperator(Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;)V

    new-instance v0, Lorg/apache/pdfbox/contentstream/operator/text/ShowTextLineAndSpace;

    invoke-direct {v0}, Lorg/apache/pdfbox/contentstream/operator/text/ShowTextLineAndSpace;-><init>()V

    invoke-virtual {p0, v0}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->addOperator(Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;)V

    invoke-static {}, Lorg/apache/pdfbox/util/PDFBoxResourceLoader;->isReady()Z

    move-result v0

    const-class v1, Lorg/apache/pdfbox/pdmodel/font/encoding/GlyphList;

    const-string v2, "org/apache/pdfbox/resources/glyphlist/additional.txt"

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/ClassLoader;->getResourceAsStream(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v0

    new-instance v1, Lorg/apache/pdfbox/pdmodel/font/encoding/GlyphList;

    invoke-static {}, Lorg/apache/pdfbox/pdmodel/font/encoding/GlyphList;->getAdobeGlyphList()Lorg/apache/pdfbox/pdmodel/font/encoding/GlyphList;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lorg/apache/pdfbox/pdmodel/font/encoding/GlyphList;-><init>(Lorg/apache/pdfbox/pdmodel/font/encoding/GlyphList;Ljava/io/InputStream;)V

    iput-object v1, p0, Lorg/apache/pdfbox/text/PDFTextStreamEngine;->glyphList:Lorg/apache/pdfbox/pdmodel/font/encoding/GlyphList;

    return-void
.end method


# virtual methods
.method public processPage(Lorg/apache/pdfbox/pdmodel/PDPage;)V
    .locals 1

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/PDPage;->getRotation()I

    move-result v0

    iput v0, p0, Lorg/apache/pdfbox/text/PDFTextStreamEngine;->pageRotation:I

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/PDPage;->getCropBox()Lorg/apache/pdfbox/pdmodel/common/PDRectangle;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/pdfbox/text/PDFTextStreamEngine;->pageSize:Lorg/apache/pdfbox/pdmodel/common/PDRectangle;

    invoke-super {p0, p1}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->processPage(Lorg/apache/pdfbox/pdmodel/PDPage;)V

    return-void
.end method

.method public processTextPosition(Lorg/apache/pdfbox/text/TextPosition;)V
    .locals 0

    return-void
.end method

.method public showGlyph(Lorg/apache/pdfbox/util/Matrix;Lorg/apache/pdfbox/pdmodel/font/PDFont;ILjava/lang/String;Lorg/apache/pdfbox/util/Vector;)V
    .locals 18

    move-object/from16 v1, p0

    move-object/from16 v14, p2

    move/from16 v2, p3

    invoke-virtual/range {p0 .. p0}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->getGraphicsState()Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;

    move-result-object v0

    iget-object v3, v1, Lorg/apache/pdfbox/text/PDFTextStreamEngine;->legacyCTM:Lorg/apache/pdfbox/util/Matrix;

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->getTextState()Lorg/apache/pdfbox/pdmodel/graphics/state/PDTextState;

    move-result-object v4

    invoke-virtual {v4}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDTextState;->getFontSize()F

    move-result v15

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->getTextState()Lorg/apache/pdfbox/pdmodel/graphics/state/PDTextState;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDTextState;->getHorizontalScaling()F

    move-result v0

    const/high16 v4, 0x42c80000    # 100.0f

    div-float/2addr v0, v4

    invoke-virtual/range {p0 .. p0}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->getTextMatrix()Lorg/apache/pdfbox/util/Matrix;

    move-result-object v5

    invoke-virtual/range {p2 .. p2}, Lorg/apache/pdfbox/pdmodel/font/PDFont;->getBoundingBox()Lorg/apache/fontbox/util/BoundingBox;

    move-result-object v6

    invoke-virtual {v6}, Lorg/apache/fontbox/util/BoundingBox;->getHeight()F

    move-result v6

    const/high16 v7, 0x40000000    # 2.0f

    div-float/2addr v6, v7

    invoke-virtual/range {p2 .. p2}, Lorg/apache/pdfbox/pdmodel/font/PDFont;->getFontMatrix()Lorg/apache/pdfbox/util/Matrix;

    move-result-object v7

    const-wide/16 v8, 0x0

    float-to-double v10, v6

    invoke-virtual {v7, v8, v9, v10, v11}, Lorg/apache/pdfbox/util/Matrix;->transformPoint(DD)Landroid/graphics/PointF;

    move-result-object v6

    iget v6, v6, Landroid/graphics/PointF;->y:F

    invoke-virtual/range {p5 .. p5}, Lorg/apache/pdfbox/util/Vector;->getX()F

    move-result v7

    mul-float/2addr v7, v15

    mul-float/2addr v7, v0

    const/4 v8, 0x0

    invoke-static {v7, v8}, Lorg/apache/pdfbox/util/Matrix;->getTranslateInstance(FF)Lorg/apache/pdfbox/util/Matrix;

    move-result-object v0

    invoke-virtual {v0, v5}, Lorg/apache/pdfbox/util/Matrix;->multiply(Lorg/apache/pdfbox/util/Matrix;)Lorg/apache/pdfbox/util/Matrix;

    move-result-object v0

    invoke-virtual {v0, v3}, Lorg/apache/pdfbox/util/Matrix;->multiply(Lorg/apache/pdfbox/util/Matrix;)Lorg/apache/pdfbox/util/Matrix;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/pdfbox/util/Matrix;->getTranslateX()F

    move-result v7

    invoke-virtual {v0}, Lorg/apache/pdfbox/util/Matrix;->getTranslateY()F

    move-result v9

    invoke-virtual/range {p1 .. p1}, Lorg/apache/pdfbox/util/Matrix;->getTranslateX()F

    move-result v0

    sub-float v10, v7, v0

    invoke-virtual/range {p1 .. p1}, Lorg/apache/pdfbox/util/Matrix;->getScalingFactorY()F

    move-result v0

    mul-float v11, v6, v0

    invoke-virtual/range {p0 .. p0}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->getGraphicsState()Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->getTextState()Lorg/apache/pdfbox/pdmodel/graphics/state/PDTextState;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDTextState;->getFontSize()F

    move-result v5

    invoke-virtual/range {p0 .. p0}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->getGraphicsState()Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->getTextState()Lorg/apache/pdfbox/pdmodel/graphics/state/PDTextState;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDTextState;->getHorizontalScaling()F

    move-result v0

    div-float v4, v0, v4

    instance-of v0, v14, Lorg/apache/pdfbox/pdmodel/font/PDType3Font;

    const/high16 v6, 0x3f800000    # 1.0f

    if-eqz v0, :cond_0

    invoke-virtual/range {p2 .. p2}, Lorg/apache/pdfbox/pdmodel/font/PDFont;->getFontMatrix()Lorg/apache/pdfbox/util/Matrix;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/pdfbox/util/Matrix;->getScaleX()F

    move-result v0

    div-float v0, v6, v0

    goto :goto_0

    :cond_0
    const v0, 0x3a83126f    # 0.001f

    :goto_0
    move v12, v0

    :try_start_0
    invoke-virtual/range {p2 .. p2}, Lorg/apache/pdfbox/pdmodel/font/PDFont;->getSpaceWidth()F

    move-result v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    mul-float/2addr v0, v12

    goto :goto_1

    :catchall_0
    move-exception v0

    move-object v13, v0

    const-string v0, "PdfBoxAndroid"

    invoke-virtual {v13}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-static {v0, v6, v13}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move v0, v8

    :goto_1
    cmpl-float v6, v0, v8

    if-nez v6, :cond_1

    invoke-virtual/range {p2 .. p2}, Lorg/apache/pdfbox/pdmodel/font/PDFont;->getAverageFontWidth()F

    move-result v0

    mul-float/2addr v0, v12

    const v6, 0x3f4ccccd    # 0.8f

    mul-float/2addr v0, v6

    :cond_1
    cmpl-float v6, v0, v8

    if-nez v6, :cond_2

    const/high16 v6, 0x3f800000    # 1.0f

    goto :goto_2

    :cond_2
    move v6, v0

    :goto_2
    mul-float/2addr v6, v5

    mul-float/2addr v6, v4

    invoke-virtual/range {p1 .. p1}, Lorg/apache/pdfbox/util/Matrix;->getScalingFactorX()F

    move-result v0

    mul-float/2addr v6, v0

    invoke-virtual {v3}, Lorg/apache/pdfbox/util/Matrix;->getScalingFactorX()F

    move-result v0

    mul-float/2addr v0, v6

    iget-object v3, v1, Lorg/apache/pdfbox/text/PDFTextStreamEngine;->glyphList:Lorg/apache/pdfbox/pdmodel/font/encoding/GlyphList;

    invoke-virtual {v14, v2, v3}, Lorg/apache/pdfbox/pdmodel/font/PDFont;->toUnicode(ILorg/apache/pdfbox/pdmodel/font/encoding/GlyphList;)Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_4

    instance-of v3, v14, Lorg/apache/pdfbox/pdmodel/font/PDSimpleFont;

    if-eqz v3, :cond_3

    int-to-char v3, v2

    new-instance v4, Ljava/lang/String;

    const/4 v5, 0x1

    new-array v5, v5, [C

    const/4 v6, 0x0

    aput-char v3, v5, v6

    invoke-direct {v4, v5}, Ljava/lang/String;-><init>([C)V

    move-object v12, v4

    goto :goto_3

    :cond_3
    return-void

    :cond_4
    move-object v12, v3

    :goto_3
    new-instance v13, Lorg/apache/pdfbox/text/TextPosition;

    iget v3, v1, Lorg/apache/pdfbox/text/PDFTextStreamEngine;->pageRotation:I

    iget-object v4, v1, Lorg/apache/pdfbox/text/PDFTextStreamEngine;->pageSize:Lorg/apache/pdfbox/pdmodel/common/PDRectangle;

    invoke-virtual {v4}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->getWidth()F

    move-result v4

    iget-object v5, v1, Lorg/apache/pdfbox/text/PDFTextStreamEngine;->pageSize:Lorg/apache/pdfbox/pdmodel/common/PDRectangle;

    invoke-virtual {v5}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->getHeight()F

    move-result v5

    filled-new-array/range {p3 .. p3}, [I

    move-result-object v16

    invoke-virtual/range {p1 .. p1}, Lorg/apache/pdfbox/util/Matrix;->getScalingFactorX()F

    move-result v2

    mul-float/2addr v2, v15

    float-to-int v8, v2

    move-object v2, v13

    move-object/from16 v6, p1

    move/from16 v17, v8

    move v8, v9

    move v9, v11

    move v11, v0

    move-object v0, v13

    move-object/from16 v13, v16

    move-object/from16 v14, p2

    move/from16 v16, v17

    invoke-direct/range {v2 .. v16}, Lorg/apache/pdfbox/text/TextPosition;-><init>(IFFLorg/apache/pdfbox/util/Matrix;FFFFFLjava/lang/String;[ILorg/apache/pdfbox/pdmodel/font/PDFont;FI)V

    invoke-virtual {v1, v0}, Lorg/apache/pdfbox/text/PDFTextStreamEngine;->processTextPosition(Lorg/apache/pdfbox/text/TextPosition;)V

    return-void
.end method

.method public showText([B)V
    .locals 1

    invoke-virtual {p0}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->getGraphicsState()Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->getCurrentTransformationMatrix()Lorg/apache/pdfbox/util/Matrix;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/pdfbox/util/Matrix;->clone()Lorg/apache/pdfbox/util/Matrix;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/pdfbox/text/PDFTextStreamEngine;->legacyCTM:Lorg/apache/pdfbox/util/Matrix;

    invoke-super {p0, p1}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->showText([B)V

    return-void
.end method

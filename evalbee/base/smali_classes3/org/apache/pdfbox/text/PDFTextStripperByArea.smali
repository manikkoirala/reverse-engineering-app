.class public Lorg/apache/pdfbox/text/PDFTextStripperByArea;
.super Lorg/apache/pdfbox/text/PDFTextStripper;
.source "SourceFile"


# instance fields
.field private final regionArea:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Landroid/graphics/RectF;",
            ">;"
        }
    .end annotation
.end field

.field private final regionCharacterList:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/util/Vector<",
            "Ljava/util/List<",
            "Lorg/apache/pdfbox/text/TextPosition;",
            ">;>;>;"
        }
    .end annotation
.end field

.field private final regionText:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/io/StringWriter;",
            ">;"
        }
    .end annotation
.end field

.field private final regions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lorg/apache/pdfbox/text/PDFTextStripper;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/apache/pdfbox/text/PDFTextStripperByArea;->regions:Ljava/util/List;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/pdfbox/text/PDFTextStripperByArea;->regionArea:Ljava/util/Map;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/pdfbox/text/PDFTextStripperByArea;->regionCharacterList:Ljava/util/Map;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/pdfbox/text/PDFTextStripperByArea;->regionText:Ljava/util/Map;

    return-void
.end method


# virtual methods
.method public addRegion(Ljava/lang/String;Landroid/graphics/RectF;)V
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/text/PDFTextStripperByArea;->regions:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lorg/apache/pdfbox/text/PDFTextStripperByArea;->regionArea:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public extractRegions(Lorg/apache/pdfbox/pdmodel/PDPage;)V
    .locals 4

    iget-object v0, p0, Lorg/apache/pdfbox/text/PDFTextStripperByArea;->regions:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lorg/apache/pdfbox/text/PDFTextStripper;->getCurrentPageNo()I

    move-result v1

    invoke-virtual {p0, v1}, Lorg/apache/pdfbox/text/PDFTextStripper;->setStartPage(I)V

    invoke-virtual {p0}, Lorg/apache/pdfbox/text/PDFTextStripper;->getCurrentPageNo()I

    move-result v1

    invoke-virtual {p0, v1}, Lorg/apache/pdfbox/text/PDFTextStripper;->setEndPage(I)V

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    new-instance v2, Ljava/util/Vector;

    invoke-direct {v2}, Ljava/util/Vector;-><init>()V

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v2, v3}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    iget-object v3, p0, Lorg/apache/pdfbox/text/PDFTextStripperByArea;->regionCharacterList:Ljava/util/Map;

    invoke-interface {v3, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v2, p0, Lorg/apache/pdfbox/text/PDFTextStripperByArea;->regionText:Ljava/util/Map;

    new-instance v3, Ljava/io/StringWriter;

    invoke-direct {v3}, Ljava/io/StringWriter;-><init>()V

    invoke-interface {v2, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/PDPage;->getStream()Lorg/apache/pdfbox/pdmodel/common/PDStream;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/text/PDFTextStripper;->processPage(Lorg/apache/pdfbox/pdmodel/PDPage;)V

    :cond_1
    return-void
.end method

.method public getRegions()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/pdfbox/text/PDFTextStripperByArea;->regions:Ljava/util/List;

    return-object v0
.end method

.method public getTextForRegion(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/text/PDFTextStripperByArea;->regionText:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/io/StringWriter;

    invoke-virtual {p1}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public processTextPosition(Lorg/apache/pdfbox/text/TextPosition;)V
    .locals 5

    iget-object v0, p0, Lorg/apache/pdfbox/text/PDFTextStripperByArea;->regionArea:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iget-object v2, p0, Lorg/apache/pdfbox/text/PDFTextStripperByArea;->regionArea:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/RectF;

    invoke-virtual {p1}, Lorg/apache/pdfbox/text/TextPosition;->getX()F

    move-result v3

    invoke-virtual {p1}, Lorg/apache/pdfbox/text/TextPosition;->getY()F

    move-result v4

    invoke-virtual {v2, v3, v4}, Landroid/graphics/RectF;->contains(FF)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lorg/apache/pdfbox/text/PDFTextStripperByArea;->regionCharacterList:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Vector;

    iput-object v1, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->charactersByArticle:Ljava/util/Vector;

    invoke-super {p0, p1}, Lorg/apache/pdfbox/text/PDFTextStripper;->processTextPosition(Lorg/apache/pdfbox/text/TextPosition;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public writePage()V
    .locals 3

    iget-object v0, p0, Lorg/apache/pdfbox/text/PDFTextStripperByArea;->regionArea:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iget-object v2, p0, Lorg/apache/pdfbox/text/PDFTextStripperByArea;->regionCharacterList:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Vector;

    iput-object v2, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->charactersByArticle:Ljava/util/Vector;

    iget-object v2, p0, Lorg/apache/pdfbox/text/PDFTextStripperByArea;->regionText:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/io/Writer;

    iput-object v1, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->output:Ljava/io/Writer;

    invoke-super {p0}, Lorg/apache/pdfbox/text/PDFTextStripper;->writePage()V

    goto :goto_0

    :cond_0
    return-void
.end method

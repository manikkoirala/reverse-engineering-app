.class public Lorg/apache/pdfbox/text/TextPositionComparator;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator<",
        "Lorg/apache/pdfbox/text/TextPosition;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 0

    .line 1
    check-cast p1, Lorg/apache/pdfbox/text/TextPosition;

    check-cast p2, Lorg/apache/pdfbox/text/TextPosition;

    invoke-virtual {p0, p1, p2}, Lorg/apache/pdfbox/text/TextPositionComparator;->compare(Lorg/apache/pdfbox/text/TextPosition;Lorg/apache/pdfbox/text/TextPosition;)I

    move-result p1

    return p1
.end method

.method public compare(Lorg/apache/pdfbox/text/TextPosition;Lorg/apache/pdfbox/text/TextPosition;)I
    .locals 10

    .line 2
    invoke-virtual {p1}, Lorg/apache/pdfbox/text/TextPosition;->getDir()F

    move-result v0

    invoke-virtual {p2}, Lorg/apache/pdfbox/text/TextPosition;->getDir()F

    move-result v1

    cmpg-float v0, v0, v1

    const/4 v1, -0x1

    if-gez v0, :cond_0

    return v1

    :cond_0
    invoke-virtual {p1}, Lorg/apache/pdfbox/text/TextPosition;->getDir()F

    move-result v0

    invoke-virtual {p2}, Lorg/apache/pdfbox/text/TextPosition;->getDir()F

    move-result v2

    cmpl-float v0, v0, v2

    const/4 v2, 0x1

    if-lez v0, :cond_1

    return v2

    :cond_1
    invoke-virtual {p1}, Lorg/apache/pdfbox/text/TextPosition;->getXDirAdj()F

    move-result v0

    invoke-virtual {p2}, Lorg/apache/pdfbox/text/TextPosition;->getXDirAdj()F

    move-result v3

    invoke-virtual {p1}, Lorg/apache/pdfbox/text/TextPosition;->getYDirAdj()F

    move-result v4

    invoke-virtual {p2}, Lorg/apache/pdfbox/text/TextPosition;->getYDirAdj()F

    move-result v5

    invoke-virtual {p1}, Lorg/apache/pdfbox/text/TextPosition;->getHeightDir()F

    move-result p1

    sub-float p1, v4, p1

    invoke-virtual {p2}, Lorg/apache/pdfbox/text/TextPosition;->getHeightDir()F

    move-result p2

    sub-float p2, v5, p2

    sub-float v6, v4, v5

    invoke-static {v6}, Ljava/lang/Math;->abs(F)F

    move-result v6

    float-to-double v6, v6

    const-wide v8, 0x3fb999999999999aL    # 0.1

    cmpg-double v6, v6, v8

    if-ltz v6, :cond_5

    cmpl-float p1, v5, p1

    if-ltz p1, :cond_2

    cmpg-float p1, v5, v4

    if-lez p1, :cond_5

    :cond_2
    cmpl-float p1, v4, p2

    if-ltz p1, :cond_3

    cmpg-float p1, v4, v5

    if-gtz p1, :cond_3

    goto :goto_0

    :cond_3
    cmpg-float p1, v4, v5

    if-gez p1, :cond_4

    return v1

    :cond_4
    return v2

    :cond_5
    :goto_0
    cmpg-float p1, v0, v3

    if-gez p1, :cond_6

    return v1

    :cond_6
    cmpl-float p1, v0, v3

    if-lez p1, :cond_7

    return v2

    :cond_7
    const/4 p1, 0x0

    return p1
.end method

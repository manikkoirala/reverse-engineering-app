.class public Lorg/apache/pdfbox/text/PDFMarkedContentExtractor;
.super Lorg/apache/pdfbox/text/PDFTextStreamEngine;
.source "SourceFile"


# instance fields
.field private characterListMapping:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lorg/apache/pdfbox/text/TextPosition;",
            ">;>;"
        }
    .end annotation
.end field

.field private currentMarkedContents:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack<",
            "Lorg/apache/pdfbox/pdmodel/documentinterchange/markedcontent/PDMarkedContent;",
            ">;"
        }
    .end annotation
.end field

.field private markedContents:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lorg/apache/pdfbox/pdmodel/documentinterchange/markedcontent/PDMarkedContent;",
            ">;"
        }
    .end annotation
.end field

.field private suppressDuplicateOverlappingText:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/text/PDFMarkedContentExtractor;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .line 2
    invoke-direct {p0}, Lorg/apache/pdfbox/text/PDFTextStreamEngine;-><init>()V

    const/4 p1, 0x1

    iput-boolean p1, p0, Lorg/apache/pdfbox/text/PDFMarkedContentExtractor;->suppressDuplicateOverlappingText:Z

    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lorg/apache/pdfbox/text/PDFMarkedContentExtractor;->markedContents:Ljava/util/List;

    new-instance p1, Ljava/util/Stack;

    invoke-direct {p1}, Ljava/util/Stack;-><init>()V

    iput-object p1, p0, Lorg/apache/pdfbox/text/PDFMarkedContentExtractor;->currentMarkedContents:Ljava/util/Stack;

    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    iput-object p1, p0, Lorg/apache/pdfbox/text/PDFMarkedContentExtractor;->characterListMapping:Ljava/util/Map;

    new-instance p1, Lorg/apache/pdfbox/contentstream/operator/markedcontent/BeginMarkedContentSequenceWithProperties;

    invoke-direct {p1}, Lorg/apache/pdfbox/contentstream/operator/markedcontent/BeginMarkedContentSequenceWithProperties;-><init>()V

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->addOperator(Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;)V

    new-instance p1, Lorg/apache/pdfbox/contentstream/operator/markedcontent/BeginMarkedContentSequence;

    invoke-direct {p1}, Lorg/apache/pdfbox/contentstream/operator/markedcontent/BeginMarkedContentSequence;-><init>()V

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->addOperator(Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;)V

    new-instance p1, Lorg/apache/pdfbox/contentstream/operator/markedcontent/EndMarkedContentSequence;

    invoke-direct {p1}, Lorg/apache/pdfbox/contentstream/operator/markedcontent/EndMarkedContentSequence;-><init>()V

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->addOperator(Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;)V

    return-void
.end method

.method private within(FFF)Z
    .locals 1

    sub-float v0, p1, p3

    cmpl-float v0, p2, v0

    if-lez v0, :cond_0

    add-float/2addr p1, p3

    cmpg-float p1, p2, p1

    if-gez p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method


# virtual methods
.method public beginMarkedContentSequence(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSDictionary;)V
    .locals 0

    invoke-static {p1, p2}, Lorg/apache/pdfbox/pdmodel/documentinterchange/markedcontent/PDMarkedContent;->create(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSDictionary;)Lorg/apache/pdfbox/pdmodel/documentinterchange/markedcontent/PDMarkedContent;

    move-result-object p1

    iget-object p2, p0, Lorg/apache/pdfbox/text/PDFMarkedContentExtractor;->currentMarkedContents:Ljava/util/Stack;

    invoke-virtual {p2}, Ljava/util/AbstractCollection;->isEmpty()Z

    move-result p2

    if-eqz p2, :cond_0

    iget-object p2, p0, Lorg/apache/pdfbox/text/PDFMarkedContentExtractor;->markedContents:Ljava/util/List;

    invoke-interface {p2, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    iget-object p2, p0, Lorg/apache/pdfbox/text/PDFMarkedContentExtractor;->currentMarkedContents:Ljava/util/Stack;

    invoke-virtual {p2}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lorg/apache/pdfbox/pdmodel/documentinterchange/markedcontent/PDMarkedContent;

    if-eqz p2, :cond_1

    invoke-virtual {p2, p1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/markedcontent/PDMarkedContent;->addMarkedContent(Lorg/apache/pdfbox/pdmodel/documentinterchange/markedcontent/PDMarkedContent;)V

    :cond_1
    :goto_0
    iget-object p2, p0, Lorg/apache/pdfbox/text/PDFMarkedContentExtractor;->currentMarkedContents:Ljava/util/Stack;

    invoke-virtual {p2, p1}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public endMarkedContentSequence()V
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/text/PDFMarkedContentExtractor;->currentMarkedContents:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/AbstractCollection;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/pdfbox/text/PDFMarkedContentExtractor;->currentMarkedContents:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    :cond_0
    return-void
.end method

.method public getMarkedContents()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lorg/apache/pdfbox/pdmodel/documentinterchange/markedcontent/PDMarkedContent;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/pdfbox/text/PDFMarkedContentExtractor;->markedContents:Ljava/util/List;

    return-object v0
.end method

.method public bridge synthetic processPage(Lorg/apache/pdfbox/pdmodel/PDPage;)V
    .locals 0

    invoke-super {p0, p1}, Lorg/apache/pdfbox/text/PDFTextStreamEngine;->processPage(Lorg/apache/pdfbox/pdmodel/PDPage;)V

    return-void
.end method

.method public processTextPosition(Lorg/apache/pdfbox/text/TextPosition;)V
    .locals 10

    iget-boolean v0, p0, Lorg/apache/pdfbox/text/PDFMarkedContentExtractor;->suppressDuplicateOverlappingText:Z

    const/4 v1, 0x1

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lorg/apache/pdfbox/text/TextPosition;->getUnicode()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lorg/apache/pdfbox/text/TextPosition;->getX()F

    move-result v2

    invoke-virtual {p1}, Lorg/apache/pdfbox/text/TextPosition;->getY()F

    move-result v3

    iget-object v4, p0, Lorg/apache/pdfbox/text/PDFMarkedContentExtractor;->characterListMapping:Ljava/util/Map;

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/List;

    if-nez v4, :cond_0

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iget-object v5, p0, Lorg/apache/pdfbox/text/PDFMarkedContentExtractor;->characterListMapping:Ljava/util/Map;

    invoke-interface {v5, v0, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    invoke-virtual {p1}, Lorg/apache/pdfbox/text/TextPosition;->getWidth()F

    move-result v5

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v5, v0

    const/high16 v0, 0x40400000    # 3.0f

    div-float/2addr v5, v0

    invoke-interface {v4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    const/4 v7, 0x0

    if-eqz v6, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lorg/apache/pdfbox/text/TextPosition;

    invoke-virtual {v6}, Lorg/apache/pdfbox/text/TextPosition;->getUnicode()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6}, Lorg/apache/pdfbox/text/TextPosition;->getX()F

    move-result v9

    invoke-virtual {v6}, Lorg/apache/pdfbox/text/TextPosition;->getY()F

    move-result v6

    if-eqz v8, :cond_1

    invoke-direct {p0, v9, v2, v5}, Lorg/apache/pdfbox/text/PDFMarkedContentExtractor;->within(FFF)Z

    move-result v8

    if-eqz v8, :cond_1

    invoke-direct {p0, v6, v3, v5}, Lorg/apache/pdfbox/text/PDFMarkedContentExtractor;->within(FFF)Z

    move-result v6

    if-eqz v6, :cond_1

    move v0, v1

    goto :goto_0

    :cond_2
    move v0, v7

    :goto_0
    if-nez v0, :cond_4

    invoke-interface {v4, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_3
    move v7, v1

    :cond_4
    if-eqz v7, :cond_8

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_6

    :cond_5
    :goto_1
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_6
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    sub-int/2addr v2, v1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/pdfbox/text/TextPosition;

    invoke-virtual {p1}, Lorg/apache/pdfbox/text/TextPosition;->isDiacritic()Z

    move-result v3

    if-eqz v3, :cond_7

    invoke-virtual {v2, p1}, Lorg/apache/pdfbox/text/TextPosition;->contains(Lorg/apache/pdfbox/text/TextPosition;)Z

    move-result v3

    if-eqz v3, :cond_7

    invoke-virtual {v2, p1}, Lorg/apache/pdfbox/text/TextPosition;->mergeDiacritic(Lorg/apache/pdfbox/text/TextPosition;)V

    goto :goto_2

    :cond_7
    invoke-virtual {v2}, Lorg/apache/pdfbox/text/TextPosition;->isDiacritic()Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-virtual {p1, v2}, Lorg/apache/pdfbox/text/TextPosition;->contains(Lorg/apache/pdfbox/text/TextPosition;)Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-virtual {p1, v2}, Lorg/apache/pdfbox/text/TextPosition;->mergeDiacritic(Lorg/apache/pdfbox/text/TextPosition;)V

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    sub-int/2addr v2, v1

    invoke-interface {v0, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    goto :goto_1

    :goto_2
    iget-object v0, p0, Lorg/apache/pdfbox/text/PDFMarkedContentExtractor;->currentMarkedContents:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/AbstractCollection;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_8

    iget-object v0, p0, Lorg/apache/pdfbox/text/PDFMarkedContentExtractor;->currentMarkedContents:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/pdmodel/documentinterchange/markedcontent/PDMarkedContent;

    invoke-virtual {v0, p1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/markedcontent/PDMarkedContent;->addText(Lorg/apache/pdfbox/text/TextPosition;)V

    :cond_8
    return-void
.end method

.method public xobject(Lorg/apache/pdfbox/pdmodel/graphics/PDXObject;)V
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/text/PDFMarkedContentExtractor;->currentMarkedContents:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/AbstractCollection;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/pdfbox/text/PDFMarkedContentExtractor;->currentMarkedContents:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/pdmodel/documentinterchange/markedcontent/PDMarkedContent;

    invoke-virtual {v0, p1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/markedcontent/PDMarkedContent;->addXObject(Lorg/apache/pdfbox/pdmodel/graphics/PDXObject;)V

    :cond_0
    return-void
.end method

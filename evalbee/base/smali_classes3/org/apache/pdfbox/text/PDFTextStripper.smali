.class public Lorg/apache/pdfbox/text/PDFTextStripper;
.super Lorg/apache/pdfbox/text/PDFTextStreamEngine;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/pdfbox/text/PDFTextStripper$PositionWrapper;,
        Lorg/apache/pdfbox/text/PDFTextStripper$WordWithTextPositions;,
        Lorg/apache/pdfbox/text/PDFTextStripper$LineItem;
    }
.end annotation


# static fields
.field private static final END_OF_LAST_TEXT_X_RESET_VALUE:F = -1.0f

.field private static final EXPECTED_START_OF_NEXT_WORD_X_RESET_VALUE:F = -3.4028235E38f

.field private static final LAST_WORD_SPACING_RESET_VALUE:F = -1.0f

.field private static final LIST_ITEM_EXPRESSIONS:[Ljava/lang/String;

.field private static final MAX_HEIGHT_FOR_LINE_RESET_VALUE:F = -1.0f

.field private static final MAX_Y_FOR_LINE_RESET_VALUE:F = -3.4028235E38f

.field private static final MIN_Y_TOP_FOR_LINE_RESET_VALUE:F = 3.4028235E38f

.field private static defaultDropThreshold:F = 2.5f

.field private static defaultIndentThreshold:F = 2.0f

.field private static final useCustomQuickSort:Z


# instance fields
.field protected final LINE_SEPARATOR:Ljava/lang/String;

.field private addMoreFormatting:Z

.field private articleEnd:Ljava/lang/String;

.field private articleStart:Ljava/lang/String;

.field private averageCharTolerance:F

.field private characterListMapping:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/util/TreeMap<",
            "Ljava/lang/Float;",
            "Ljava/util/TreeSet<",
            "Ljava/lang/Float;",
            ">;>;>;"
        }
    .end annotation
.end field

.field protected charactersByArticle:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector<",
            "Ljava/util/List<",
            "Lorg/apache/pdfbox/text/TextPosition;",
            ">;>;"
        }
    .end annotation
.end field

.field private currentPageNo:I

.field protected document:Lorg/apache/pdfbox/pdmodel/PDDocument;

.field private dropThreshold:F

.field private endBookmark:Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineItem;

.field private endBookmarkPageNumber:I

.field private endPage:I

.field private inParagraph:Z

.field private indentThreshold:F

.field private lineSeparator:Ljava/lang/String;

.field private listOfPatterns:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/util/regex/Pattern;",
            ">;"
        }
    .end annotation
.end field

.field protected output:Ljava/io/Writer;

.field private pageArticles:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDThreadBead;",
            ">;"
        }
    .end annotation
.end field

.field private pageEnd:Ljava/lang/String;

.field private pageStart:Ljava/lang/String;

.field private paragraphEnd:Ljava/lang/String;

.field private paragraphStart:Ljava/lang/String;

.field private shouldSeparateByBeads:Z

.field private sortByPosition:Z

.field private spacingTolerance:F

.field private startBookmark:Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineItem;

.field private startBookmarkPageNumber:I

.field private startPage:I

.field private suppressDuplicateOverlappingText:Z

.field private wordSeparator:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 11

    const/4 v0, 0x0

    :try_start_0
    const-class v1, Lorg/apache/pdfbox/text/PDFTextStripper;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, ".indent"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ".drop"

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0
    :try_end_1
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_0
    move-object v2, v0

    :catch_1
    :goto_0
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_0

    :try_start_2
    invoke-static {v2}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v1

    sput v1, Lorg/apache/pdfbox/text/PDFTextStripper;->defaultIndentThreshold:F
    :try_end_2
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_2

    :catch_2
    :cond_0
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_1

    :try_start_3
    invoke-static {v0}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v0

    sput v0, Lorg/apache/pdfbox/text/PDFTextStripper;->defaultDropThreshold:F
    :try_end_3
    .catch Ljava/lang/NumberFormatException; {:try_start_3 .. :try_end_3} :catch_3

    :catch_3
    :cond_1
    const/4 v0, 0x1

    sput-boolean v0, Lorg/apache/pdfbox/text/PDFTextStripper;->useCustomQuickSort:Z

    const-string v1, "\\."

    const-string v2, "\\d+\\."

    const-string v3, "\\[\\d+\\]"

    const-string v4, "\\d+\\)"

    const-string v5, "[A-Z]\\."

    const-string v6, "[a-z]\\."

    const-string v7, "[A-Z]\\)"

    const-string v8, "[a-z]\\)"

    const-string v9, "[IVXL]+\\."

    const-string v10, "[ivxl]+\\."

    filled-new-array/range {v1 .. v10}, [Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lorg/apache/pdfbox/text/PDFTextStripper;->LIST_ITEM_EXPRESSIONS:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    invoke-direct {p0}, Lorg/apache/pdfbox/text/PDFTextStreamEngine;-><init>()V

    const-string v0, "line.separator"

    invoke-static {v0}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->LINE_SEPARATOR:Ljava/lang/String;

    iput-object v0, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->lineSeparator:Ljava/lang/String;

    const-string v1, " "

    iput-object v1, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->wordSeparator:Ljava/lang/String;

    const-string v1, ""

    iput-object v1, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->paragraphStart:Ljava/lang/String;

    iput-object v1, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->paragraphEnd:Ljava/lang/String;

    iput-object v1, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->pageStart:Ljava/lang/String;

    iput-object v0, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->pageEnd:Ljava/lang/String;

    iput-object v1, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->articleStart:Ljava/lang/String;

    iput-object v1, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->articleEnd:Ljava/lang/String;

    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->currentPageNo:I

    const/4 v1, 0x1

    iput v1, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->startPage:I

    const v2, 0x7fffffff

    iput v2, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->endPage:I

    const/4 v2, 0x0

    iput-object v2, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->startBookmark:Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineItem;

    const/4 v3, -0x1

    iput v3, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->startBookmarkPageNumber:I

    iput-object v2, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->endBookmark:Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineItem;

    iput v3, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->endBookmarkPageNumber:I

    iput-boolean v1, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->suppressDuplicateOverlappingText:Z

    iput-boolean v1, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->shouldSeparateByBeads:Z

    iput-boolean v0, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->sortByPosition:Z

    iput-boolean v0, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->addMoreFormatting:Z

    sget v0, Lorg/apache/pdfbox/text/PDFTextStripper;->defaultIndentThreshold:F

    iput v0, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->indentThreshold:F

    sget v0, Lorg/apache/pdfbox/text/PDFTextStripper;->defaultDropThreshold:F

    iput v0, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->dropThreshold:F

    const/high16 v0, 0x3f000000    # 0.5f

    iput v0, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->spacingTolerance:F

    const v0, 0x3e99999a    # 0.3f

    iput v0, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->averageCharTolerance:F

    iput-object v2, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->pageArticles:Ljava/util/List;

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->charactersByArticle:Ljava/util/Vector;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->characterListMapping:Ljava/util/Map;

    iput-object v2, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->listOfPatterns:Ljava/util/List;

    return-void
.end method

.method private createWord(Ljava/lang/String;Ljava/util/List;)Lorg/apache/pdfbox/text/PDFTextStripper$WordWithTextPositions;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lorg/apache/pdfbox/text/TextPosition;",
            ">;)",
            "Lorg/apache/pdfbox/text/PDFTextStripper$WordWithTextPositions;"
        }
    .end annotation

    new-instance v0, Lorg/apache/pdfbox/text/PDFTextStripper$WordWithTextPositions;

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/text/PDFTextStripper;->normalizeWord(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1, p2}, Lorg/apache/pdfbox/text/PDFTextStripper$WordWithTextPositions;-><init>(Ljava/lang/String;Ljava/util/List;)V

    return-object v0
.end method

.method private handleLineSeparation(Lorg/apache/pdfbox/text/PDFTextStripper$PositionWrapper;Lorg/apache/pdfbox/text/PDFTextStripper$PositionWrapper;Lorg/apache/pdfbox/text/PDFTextStripper$PositionWrapper;F)Lorg/apache/pdfbox/text/PDFTextStripper$PositionWrapper;
    .locals 0

    invoke-virtual {p1}, Lorg/apache/pdfbox/text/PDFTextStripper$PositionWrapper;->setLineStart()V

    invoke-direct {p0, p1, p2, p3, p4}, Lorg/apache/pdfbox/text/PDFTextStripper;->isParagraphSeparation(Lorg/apache/pdfbox/text/PDFTextStripper$PositionWrapper;Lorg/apache/pdfbox/text/PDFTextStripper$PositionWrapper;Lorg/apache/pdfbox/text/PDFTextStripper$PositionWrapper;F)V

    invoke-virtual {p1}, Lorg/apache/pdfbox/text/PDFTextStripper$PositionWrapper;->isParagraphStart()Z

    move-result p3

    if-eqz p3, :cond_1

    invoke-virtual {p2}, Lorg/apache/pdfbox/text/PDFTextStripper$PositionWrapper;->isArticleStart()Z

    move-result p2

    if-eqz p2, :cond_0

    invoke-virtual {p0}, Lorg/apache/pdfbox/text/PDFTextStripper;->writeParagraphStart()V

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lorg/apache/pdfbox/text/PDFTextStripper;->writeLineSeparator()V

    invoke-virtual {p0}, Lorg/apache/pdfbox/text/PDFTextStripper;->writeParagraphSeparator()V

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lorg/apache/pdfbox/text/PDFTextStripper;->writeLineSeparator()V

    :goto_0
    return-object p1
.end method

.method private isParagraphSeparation(Lorg/apache/pdfbox/text/PDFTextStripper$PositionWrapper;Lorg/apache/pdfbox/text/PDFTextStripper$PositionWrapper;Lorg/apache/pdfbox/text/PDFTextStripper$PositionWrapper;F)V
    .locals 5

    const/4 v0, 0x1

    if-nez p3, :cond_0

    goto/16 :goto_2

    :cond_0
    invoke-virtual {p1}, Lorg/apache/pdfbox/text/PDFTextStripper$PositionWrapper;->getTextPosition()Lorg/apache/pdfbox/text/TextPosition;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/pdfbox/text/TextPosition;->getYDirAdj()F

    move-result v1

    invoke-virtual {p2}, Lorg/apache/pdfbox/text/PDFTextStripper$PositionWrapper;->getTextPosition()Lorg/apache/pdfbox/text/TextPosition;

    move-result-object p2

    invoke-virtual {p2}, Lorg/apache/pdfbox/text/TextPosition;->getYDirAdj()F

    move-result p2

    sub-float/2addr v1, p2

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result p2

    invoke-virtual {p0}, Lorg/apache/pdfbox/text/PDFTextStripper;->getDropThreshold()F

    move-result v1

    invoke-direct {p0, v1, p4}, Lorg/apache/pdfbox/text/PDFTextStripper;->multiplyFloat(FF)F

    move-result p4

    invoke-virtual {p1}, Lorg/apache/pdfbox/text/PDFTextStripper$PositionWrapper;->getTextPosition()Lorg/apache/pdfbox/text/TextPosition;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/pdfbox/text/TextPosition;->getXDirAdj()F

    move-result v1

    invoke-virtual {p3}, Lorg/apache/pdfbox/text/PDFTextStripper$PositionWrapper;->getTextPosition()Lorg/apache/pdfbox/text/TextPosition;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/pdfbox/text/TextPosition;->getXDirAdj()F

    move-result v2

    sub-float/2addr v1, v2

    invoke-virtual {p0}, Lorg/apache/pdfbox/text/PDFTextStripper;->getIndentThreshold()F

    move-result v2

    invoke-virtual {p1}, Lorg/apache/pdfbox/text/PDFTextStripper$PositionWrapper;->getTextPosition()Lorg/apache/pdfbox/text/TextPosition;

    move-result-object v3

    invoke-virtual {v3}, Lorg/apache/pdfbox/text/TextPosition;->getWidthOfSpace()F

    move-result v3

    invoke-direct {p0, v2, v3}, Lorg/apache/pdfbox/text/PDFTextStripper;->multiplyFloat(FF)F

    move-result v2

    invoke-virtual {p1}, Lorg/apache/pdfbox/text/PDFTextStripper$PositionWrapper;->getTextPosition()Lorg/apache/pdfbox/text/TextPosition;

    move-result-object v3

    invoke-virtual {v3}, Lorg/apache/pdfbox/text/TextPosition;->getWidth()F

    move-result v3

    const/high16 v4, 0x3e800000    # 0.25f

    invoke-direct {p0, v4, v3}, Lorg/apache/pdfbox/text/PDFTextStripper;->multiplyFloat(FF)F

    move-result v3

    cmpl-float p2, p2, p4

    if-lez p2, :cond_1

    goto :goto_2

    :cond_1
    cmpl-float p2, v1, v2

    if-lez p2, :cond_3

    invoke-virtual {p3}, Lorg/apache/pdfbox/text/PDFTextStripper$PositionWrapper;->isParagraphStart()Z

    move-result p2

    if-nez p2, :cond_2

    goto :goto_2

    :cond_2
    :goto_0
    invoke-virtual {p1}, Lorg/apache/pdfbox/text/PDFTextStripper$PositionWrapper;->setHangingIndent()V

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Lorg/apache/pdfbox/text/PDFTextStripper$PositionWrapper;->getTextPosition()Lorg/apache/pdfbox/text/TextPosition;

    move-result-object p2

    invoke-virtual {p2}, Lorg/apache/pdfbox/text/TextPosition;->getWidthOfSpace()F

    move-result p2

    neg-float p2, p2

    cmpg-float p2, v1, p2

    if-gez p2, :cond_4

    invoke-virtual {p3}, Lorg/apache/pdfbox/text/PDFTextStripper$PositionWrapper;->isParagraphStart()Z

    move-result p2

    if-nez p2, :cond_6

    goto :goto_2

    :cond_4
    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result p2

    cmpg-float p2, p2, v3

    if-gez p2, :cond_6

    invoke-virtual {p3}, Lorg/apache/pdfbox/text/PDFTextStripper$PositionWrapper;->isHangingIndent()Z

    move-result p2

    if-eqz p2, :cond_5

    goto :goto_0

    :cond_5
    invoke-virtual {p3}, Lorg/apache/pdfbox/text/PDFTextStripper$PositionWrapper;->isParagraphStart()Z

    move-result p2

    if-eqz p2, :cond_6

    invoke-direct {p0, p3}, Lorg/apache/pdfbox/text/PDFTextStripper;->matchListItemPattern(Lorg/apache/pdfbox/text/PDFTextStripper$PositionWrapper;)Ljava/util/regex/Pattern;

    move-result-object p2

    if-eqz p2, :cond_6

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/text/PDFTextStripper;->matchListItemPattern(Lorg/apache/pdfbox/text/PDFTextStripper$PositionWrapper;)Ljava/util/regex/Pattern;

    move-result-object p3

    if-ne p2, p3, :cond_6

    goto :goto_2

    :cond_6
    :goto_1
    const/4 v0, 0x0

    :goto_2
    if-eqz v0, :cond_7

    invoke-virtual {p1}, Lorg/apache/pdfbox/text/PDFTextStripper$PositionWrapper;->setParagraphStart()V

    :cond_7
    return-void
.end method

.method private matchListItemPattern(Lorg/apache/pdfbox/text/PDFTextStripper$PositionWrapper;)Ljava/util/regex/Pattern;
    .locals 1

    invoke-virtual {p1}, Lorg/apache/pdfbox/text/PDFTextStripper$PositionWrapper;->getTextPosition()Lorg/apache/pdfbox/text/TextPosition;

    move-result-object p1

    invoke-virtual {p1}, Lorg/apache/pdfbox/text/TextPosition;->getUnicode()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0}, Lorg/apache/pdfbox/text/PDFTextStripper;->getListItemPatterns()Ljava/util/List;

    move-result-object v0

    invoke-static {p1, v0}, Lorg/apache/pdfbox/text/PDFTextStripper;->matchPattern(Ljava/lang/String;Ljava/util/List;)Ljava/util/regex/Pattern;

    move-result-object p1

    return-object p1
.end method

.method public static matchPattern(Ljava/lang/String;Ljava/util/List;)Ljava/util/regex/Pattern;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/util/regex/Pattern;",
            ">;)",
            "Ljava/util/regex/Pattern;"
        }
    .end annotation

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/regex/Pattern;

    invoke-virtual {v0, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/regex/Matcher;->matches()Z

    move-result v1

    if-eqz v1, :cond_0

    return-object v0

    :cond_1
    const/4 p0, 0x0

    return-object p0
.end method

.method private multiplyFloat(FF)F
    .locals 0

    mul-float/2addr p1, p2

    const/high16 p2, 0x447a0000    # 1000.0f

    mul-float/2addr p1, p2

    invoke-static {p1}, Ljava/lang/Math;->round(F)I

    move-result p1

    int-to-float p1, p1

    div-float/2addr p1, p2

    return p1
.end method

.method private normalize(Ljava/util/List;ZZ)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lorg/apache/pdfbox/text/PDFTextStripper$LineItem;",
            ">;ZZ)",
            "Ljava/util/List<",
            "Lorg/apache/pdfbox/text/PDFTextStripper$WordWithTextPositions;",
            ">;"
        }
    .end annotation

    new-instance p3, Ljava/util/LinkedList;

    invoke-direct {p3}, Ljava/util/LinkedList;-><init>()V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    if-eqz p2, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p2

    add-int/lit8 p2, p2, -0x1

    :goto_0
    if-ltz p2, :cond_1

    invoke-interface {p1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/pdfbox/text/PDFTextStripper$LineItem;

    invoke-direct {p0, p3, v0, v1, v2}, Lorg/apache/pdfbox/text/PDFTextStripper;->normalizeAdd(Ljava/util/List;Ljava/lang/StringBuilder;Ljava/util/List;Lorg/apache/pdfbox/text/PDFTextStripper$LineItem;)Ljava/lang/StringBuilder;

    move-result-object v0

    add-int/lit8 p2, p2, -0x1

    goto :goto_0

    :cond_0
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lorg/apache/pdfbox/text/PDFTextStripper$LineItem;

    invoke-direct {p0, p3, v0, v1, p2}, Lorg/apache/pdfbox/text/PDFTextStripper;->normalizeAdd(Ljava/util/List;Ljava/lang/StringBuilder;Ljava/util/List;Lorg/apache/pdfbox/text/PDFTextStripper$LineItem;)Ljava/lang/StringBuilder;

    move-result-object v0

    goto :goto_1

    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result p1

    if-lez p1, :cond_2

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1, v1}, Lorg/apache/pdfbox/text/PDFTextStripper;->createWord(Ljava/lang/String;Ljava/util/List;)Lorg/apache/pdfbox/text/PDFTextStripper$WordWithTextPositions;

    move-result-object p1

    invoke-interface {p3, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    return-object p3
.end method

.method private normalizeAdd(Ljava/util/List;Ljava/lang/StringBuilder;Ljava/util/List;Lorg/apache/pdfbox/text/PDFTextStripper$LineItem;)Ljava/lang/StringBuilder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lorg/apache/pdfbox/text/PDFTextStripper$WordWithTextPositions;",
            ">;",
            "Ljava/lang/StringBuilder;",
            "Ljava/util/List<",
            "Lorg/apache/pdfbox/text/TextPosition;",
            ">;",
            "Lorg/apache/pdfbox/text/PDFTextStripper$LineItem;",
            ")",
            "Ljava/lang/StringBuilder;"
        }
    .end annotation

    invoke-virtual {p4}, Lorg/apache/pdfbox/text/PDFTextStripper$LineItem;->isWordSeparator()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    new-instance p4, Ljava/util/ArrayList;

    invoke-direct {p4, p3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-direct {p0, p2, p4}, Lorg/apache/pdfbox/text/PDFTextStripper;->createWord(Ljava/lang/String;Ljava/util/List;)Lorg/apache/pdfbox/text/PDFTextStripper$WordWithTextPositions;

    move-result-object p2

    invoke-interface {p1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {p3}, Ljava/util/List;->clear()V

    goto :goto_0

    :cond_0
    invoke-virtual {p4}, Lorg/apache/pdfbox/text/PDFTextStripper$LineItem;->getTextPosition()Lorg/apache/pdfbox/text/TextPosition;

    move-result-object p1

    invoke-virtual {p1}, Lorg/apache/pdfbox/text/TextPosition;->getUnicode()Ljava/lang/String;

    move-result-object p4

    invoke-virtual {p2, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {p3, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_0
    return-object p2
.end method

.method private normalizeWord(Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    move v3, v2

    :goto_0
    if-ge v2, v0, :cond_6

    invoke-virtual {p1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v4

    const v5, 0xfb00

    if-gt v5, v4, :cond_0

    const v5, 0xfdff

    if-le v4, v5, :cond_1

    :cond_0
    const v5, 0xfe70

    if-gt v5, v4, :cond_5

    const v5, 0xfeff

    if-gt v4, v5, :cond_5

    :cond_1
    if-nez v1, :cond_2

    new-instance v1, Ljava/lang/StringBuilder;

    mul-int/lit8 v5, v0, 0x2

    invoke-direct {v1, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    :cond_2
    invoke-virtual {p1, v3, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const v3, 0xfdf2

    if-ne v4, v3, :cond_4

    if-lez v2, :cond_4

    add-int/lit8 v3, v2, -0x1

    invoke-virtual {p1, v3}, Ljava/lang/String;->charAt(I)C

    move-result v4

    const/16 v5, 0x627

    if-eq v4, v5, :cond_3

    invoke-virtual {p1, v3}, Ljava/lang/String;->charAt(I)C

    move-result v3

    const v4, 0xfe8d

    if-ne v3, v4, :cond_4

    :cond_3
    const-string v3, "\u0644\u0644\u0647"

    goto :goto_1

    :cond_4
    add-int/lit8 v3, v2, 0x1

    invoke-virtual {p1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    sget-object v4, Ljava/text/Normalizer$Form;->NFKC:Ljava/text/Normalizer$Form;

    invoke-static {v3, v4}, Ljava/text/Normalizer;->normalize(Ljava/lang/CharSequence;Ljava/text/Normalizer$Form;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    :goto_1
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v3, v2, 0x1

    :cond_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_6
    if-nez v1, :cond_7

    return-object p1

    :cond_7
    invoke-virtual {p1, v3, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private overlap(FFFF)Z
    .locals 1

    const v0, 0x3dcccccd    # 0.1f

    invoke-direct {p0, p1, p3, v0}, Lorg/apache/pdfbox/text/PDFTextStripper;->within(FFF)Z

    move-result v0

    if-nez v0, :cond_2

    cmpg-float v0, p3, p1

    if-gtz v0, :cond_0

    sub-float p2, p1, p2

    cmpl-float p2, p3, p2

    if-gez p2, :cond_2

    :cond_0
    cmpg-float p2, p1, p3

    if-gtz p2, :cond_1

    sub-float/2addr p3, p4

    cmpl-float p1, p1, p3

    if-ltz p1, :cond_1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    goto :goto_1

    :cond_2
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method

.method private resetEngine()V
    .locals 2

    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->currentPageNo:I

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->document:Lorg/apache/pdfbox/pdmodel/PDDocument;

    iget-object v1, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->charactersByArticle:Ljava/util/Vector;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/util/Vector;->clear()V

    :cond_0
    iget-object v1, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->characterListMapping:Ljava/util/Map;

    if-eqz v1, :cond_1

    invoke-interface {v1}, Ljava/util/Map;->clear()V

    :cond_1
    iput-object v0, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->startBookmark:Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineItem;

    iput-object v0, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->endBookmark:Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineItem;

    return-void
.end method

.method private within(FFF)Z
    .locals 1

    add-float v0, p1, p3

    cmpg-float v0, p2, v0

    if-gez v0, :cond_0

    sub-float/2addr p1, p3

    cmpl-float p1, p2, p1

    if-lez p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method private writeLine(Ljava/util/List;Z)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lorg/apache/pdfbox/text/PDFTextStripper$WordWithTextPositions;",
            ">;Z)V"
        }
    .end annotation

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, p2, :cond_1

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/pdfbox/text/PDFTextStripper$WordWithTextPositions;

    invoke-virtual {v1}, Lorg/apache/pdfbox/text/PDFTextStripper$WordWithTextPositions;->getText()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Lorg/apache/pdfbox/text/PDFTextStripper$WordWithTextPositions;->getTextPositions()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0, v2, v1}, Lorg/apache/pdfbox/text/PDFTextStripper;->writeString(Ljava/lang/String;Ljava/util/List;)V

    add-int/lit8 v1, p2, -0x1

    if-ge v0, v1, :cond_0

    invoke-virtual {p0}, Lorg/apache/pdfbox/text/PDFTextStripper;->writeWordSeparator()V

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method


# virtual methods
.method public endArticle()V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->output:Ljava/io/Writer;

    invoke-virtual {p0}, Lorg/apache/pdfbox/text/PDFTextStripper;->getArticleEnd()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    return-void
.end method

.method public endDocument(Lorg/apache/pdfbox/pdmodel/PDDocument;)V
    .locals 0

    return-void
.end method

.method public endPage(Lorg/apache/pdfbox/pdmodel/PDPage;)V
    .locals 0

    return-void
.end method

.method public getAddMoreFormatting()Z
    .locals 1

    iget-boolean v0, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->addMoreFormatting:Z

    return v0
.end method

.method public getArticleEnd()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->articleEnd:Ljava/lang/String;

    return-object v0
.end method

.method public getArticleStart()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->articleStart:Ljava/lang/String;

    return-object v0
.end method

.method public getAverageCharTolerance()F
    .locals 1

    iget v0, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->averageCharTolerance:F

    return v0
.end method

.method public getCharactersByArticle()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/util/List<",
            "Lorg/apache/pdfbox/text/TextPosition;",
            ">;>;"
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->charactersByArticle:Ljava/util/Vector;

    return-object v0
.end method

.method public getCurrentPageNo()I
    .locals 1

    iget v0, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->currentPageNo:I

    return v0
.end method

.method public getDropThreshold()F
    .locals 1

    iget v0, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->dropThreshold:F

    return v0
.end method

.method public getEndBookmark()Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineItem;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->endBookmark:Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineItem;

    return-object v0
.end method

.method public getEndPage()I
    .locals 1

    iget v0, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->endPage:I

    return v0
.end method

.method public getIndentThreshold()F
    .locals 1

    iget v0, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->indentThreshold:F

    return v0
.end method

.method public getLineSeparator()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->lineSeparator:Ljava/lang/String;

    return-object v0
.end method

.method public getListItemPatterns()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/util/regex/Pattern;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->listOfPatterns:Ljava/util/List;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->listOfPatterns:Ljava/util/List;

    sget-object v0, Lorg/apache/pdfbox/text/PDFTextStripper;->LIST_ITEM_EXPRESSIONS:[Ljava/lang/String;

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    aget-object v3, v0, v2

    invoke-static {v3}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v3

    iget-object v4, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->listOfPatterns:Ljava/util/List;

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->listOfPatterns:Ljava/util/List;

    return-object v0
.end method

.method public getOutput()Ljava/io/Writer;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->output:Ljava/io/Writer;

    return-object v0
.end method

.method public getPageEnd()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->pageEnd:Ljava/lang/String;

    return-object v0
.end method

.method public getPageStart()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->pageStart:Ljava/lang/String;

    return-object v0
.end method

.method public getParagraphEnd()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->paragraphEnd:Ljava/lang/String;

    return-object v0
.end method

.method public getParagraphStart()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->paragraphStart:Ljava/lang/String;

    return-object v0
.end method

.method public getSeparateByBeads()Z
    .locals 1

    iget-boolean v0, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->shouldSeparateByBeads:Z

    return v0
.end method

.method public getSortByPosition()Z
    .locals 1

    iget-boolean v0, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->sortByPosition:Z

    return v0
.end method

.method public getSpacingTolerance()F
    .locals 1

    iget v0, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->spacingTolerance:F

    return v0
.end method

.method public getStartBookmark()Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineItem;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->startBookmark:Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineItem;

    return-object v0
.end method

.method public getStartPage()I
    .locals 1

    iget v0, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->startPage:I

    return v0
.end method

.method public getSuppressDuplicateOverlappingText()Z
    .locals 1

    iget-boolean v0, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->suppressDuplicateOverlappingText:Z

    return v0
.end method

.method public getText(Lorg/apache/pdfbox/pdmodel/PDDocument;)Ljava/lang/String;
    .locals 1

    new-instance v0, Ljava/io/StringWriter;

    invoke-direct {v0}, Ljava/io/StringWriter;-><init>()V

    invoke-virtual {p0, p1, v0}, Lorg/apache/pdfbox/text/PDFTextStripper;->writeText(Lorg/apache/pdfbox/pdmodel/PDDocument;Ljava/io/Writer;)V

    invoke-virtual {v0}, Ljava/io/StringWriter;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getWordSeparator()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->wordSeparator:Ljava/lang/String;

    return-object v0
.end method

.method public processPage(Lorg/apache/pdfbox/pdmodel/PDPage;)V
    .locals 5

    iget v0, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->currentPageNo:I

    iget v1, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->startPage:I

    if-lt v0, v1, :cond_5

    iget v1, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->endPage:I

    if-gt v0, v1, :cond_5

    iget v1, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->startBookmarkPageNumber:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    if-lt v0, v1, :cond_5

    :cond_0
    iget v1, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->endBookmarkPageNumber:I

    if-eq v1, v2, :cond_1

    if-gt v0, v1, :cond_5

    :cond_1
    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/text/PDFTextStripper;->startPage(Lorg/apache/pdfbox/pdmodel/PDPage;)V

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/PDPage;->getThreadBeads()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->pageArticles:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    mul-int/lit8 v0, v0, 0x2

    const/4 v1, 0x1

    add-int/2addr v0, v1

    iget-boolean v2, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->shouldSeparateByBeads:Z

    if-nez v2, :cond_2

    goto :goto_0

    :cond_2
    move v1, v0

    :goto_0
    iget-object v0, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->charactersByArticle:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    iget-object v2, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->charactersByArticle:Ljava/util/Vector;

    invoke-virtual {v2, v1}, Ljava/util/Vector;->setSize(I)V

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v1, :cond_4

    iget-object v3, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->charactersByArticle:Ljava/util/Vector;

    if-ge v1, v0, :cond_3

    invoke-virtual {v3, v2}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->clear()V

    goto :goto_2

    :cond_3
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v3, v2, v4}, Ljava/util/Vector;->set(ILjava/lang/Object;)Ljava/lang/Object;

    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_4
    iget-object v0, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->characterListMapping:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    invoke-super {p0, p1}, Lorg/apache/pdfbox/text/PDFTextStreamEngine;->processPage(Lorg/apache/pdfbox/pdmodel/PDPage;)V

    invoke-virtual {p0}, Lorg/apache/pdfbox/text/PDFTextStripper;->writePage()V

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/text/PDFTextStripper;->endPage(Lorg/apache/pdfbox/pdmodel/PDPage;)V

    :cond_5
    return-void
.end method

.method public processPages(Lorg/apache/pdfbox/pdmodel/PDPageTree;)V
    .locals 3

    iget-object v0, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->startBookmark:Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineItem;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    move-object v0, v1

    goto :goto_0

    :cond_0
    iget-object v2, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->document:Lorg/apache/pdfbox/pdmodel/PDDocument;

    invoke-virtual {v0, v2}, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineItem;->findDestinationPage(Lorg/apache/pdfbox/pdmodel/PDDocument;)Lorg/apache/pdfbox/pdmodel/PDPage;

    move-result-object v0

    :goto_0
    iget-object v2, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->endBookmark:Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineItem;

    if-nez v2, :cond_1

    goto :goto_1

    :cond_1
    iget-object v1, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->document:Lorg/apache/pdfbox/pdmodel/PDDocument;

    invoke-virtual {v2, v1}, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineItem;->findDestinationPage(Lorg/apache/pdfbox/pdmodel/PDDocument;)Lorg/apache/pdfbox/pdmodel/PDPage;

    move-result-object v1

    :goto_1
    if-eqz v0, :cond_2

    if-eqz v1, :cond_2

    iget-object v0, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->startBookmark:Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineItem;

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/common/PDDictionaryWrapper;->getCOSObject()Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->endBookmark:Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineItem;

    invoke-virtual {v1}, Lorg/apache/pdfbox/pdmodel/common/PDDictionaryWrapper;->getCOSObject()Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v1

    if-ne v0, v1, :cond_2

    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->startBookmarkPageNumber:I

    iput v0, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->endBookmarkPageNumber:I

    :cond_2
    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/PDPageTree;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_3
    :goto_2
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/pdmodel/PDPage;

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/PDPage;->getStream()Lorg/apache/pdfbox/pdmodel/common/PDStream;

    move-result-object v1

    iget v2, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->currentPageNo:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->currentPageNo:I

    if-eqz v1, :cond_3

    invoke-virtual {p0, v0}, Lorg/apache/pdfbox/text/PDFTextStripper;->processPage(Lorg/apache/pdfbox/pdmodel/PDPage;)V

    goto :goto_2

    :cond_4
    return-void
.end method

.method public processTextPosition(Lorg/apache/pdfbox/text/TextPosition;)V
    .locals 12

    iget-boolean v0, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->suppressDuplicateOverlappingText:Z

    const/4 v1, 0x0

    const/4 v2, 0x1

    if-eqz v0, :cond_5

    invoke-virtual {p1}, Lorg/apache/pdfbox/text/TextPosition;->getUnicode()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lorg/apache/pdfbox/text/TextPosition;->getX()F

    move-result v3

    invoke-virtual {p1}, Lorg/apache/pdfbox/text/TextPosition;->getY()F

    move-result v4

    iget-object v5, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->characterListMapping:Ljava/util/Map;

    invoke-interface {v5, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/util/TreeMap;

    if-nez v5, :cond_0

    new-instance v5, Ljava/util/TreeMap;

    invoke-direct {v5}, Ljava/util/TreeMap;-><init>()V

    iget-object v6, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->characterListMapping:Ljava/util/Map;

    invoke-interface {v6, v0, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    invoke-virtual {p1}, Lorg/apache/pdfbox/text/TextPosition;->getWidth()F

    move-result v6

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v6, v0

    const/high16 v0, 0x40400000    # 3.0f

    div-float/2addr v6, v0

    sub-float v0, v3, v6

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    add-float v7, v3, v6

    invoke-static {v7}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v7

    invoke-virtual {v5, v0, v7}, Ljava/util/TreeMap;->subMap(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedMap;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/SortedMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/util/TreeSet;

    sub-float v8, v4, v6

    invoke-static {v8}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v8

    add-float v9, v4, v6

    invoke-static {v9}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Ljava/util/TreeSet;->subSet(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedSet;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/Set;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_1

    move v0, v2

    goto :goto_0

    :cond_2
    move v0, v1

    :goto_0
    if-nez v0, :cond_4

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/TreeSet;

    if-nez v0, :cond_3

    new-instance v0, Ljava/util/TreeSet;

    invoke-direct {v0}, Ljava/util/TreeSet;-><init>()V

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-virtual {v5, v3, v0}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_3
    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_4
    move v0, v1

    goto :goto_2

    :cond_5
    :goto_1
    move v0, v2

    :goto_2
    if-eqz v0, :cond_15

    invoke-virtual {p1}, Lorg/apache/pdfbox/text/TextPosition;->getX()F

    move-result v0

    invoke-virtual {p1}, Lorg/apache/pdfbox/text/TextPosition;->getY()F

    move-result v3

    iget-boolean v4, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->shouldSeparateByBeads:Z

    const/4 v5, -0x1

    if-eqz v4, :cond_d

    move v4, v1

    move v6, v5

    move v7, v6

    move v8, v7

    move v9, v8

    :goto_3
    iget-object v10, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->pageArticles:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v10

    if-ge v4, v10, :cond_c

    if-ne v6, v5, :cond_c

    iget-object v10, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->pageArticles:Ljava/util/List;

    invoke-interface {v10, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDThreadBead;

    if-eqz v10, :cond_a

    invoke-virtual {v10}, Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDThreadBead;->getRectangle()Lorg/apache/pdfbox/pdmodel/common/PDRectangle;

    move-result-object v10

    invoke-virtual {v10, v0, v3}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->contains(FF)Z

    move-result v11

    if-eqz v11, :cond_6

    mul-int/lit8 v6, v4, 0x2

    add-int/2addr v6, v2

    goto :goto_4

    :cond_6
    invoke-virtual {v10}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->getLowerLeftX()F

    move-result v11

    cmpg-float v11, v0, v11

    if-ltz v11, :cond_7

    invoke-virtual {v10}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->getUpperRightY()F

    move-result v11

    cmpg-float v11, v3, v11

    if-gez v11, :cond_8

    :cond_7
    if-ne v7, v5, :cond_8

    mul-int/lit8 v7, v4, 0x2

    goto :goto_4

    :cond_8
    invoke-virtual {v10}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->getLowerLeftX()F

    move-result v11

    cmpg-float v11, v0, v11

    if-gez v11, :cond_9

    if-ne v8, v5, :cond_9

    mul-int/lit8 v8, v4, 0x2

    goto :goto_4

    :cond_9
    invoke-virtual {v10}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->getUpperRightY()F

    move-result v10

    cmpg-float v10, v3, v10

    if-gez v10, :cond_b

    if-ne v9, v5, :cond_b

    mul-int/lit8 v9, v4, 0x2

    goto :goto_4

    :cond_a
    move v6, v1

    :cond_b
    :goto_4
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    :cond_c
    move v1, v6

    goto :goto_5

    :cond_d
    move v7, v5

    move v8, v7

    move v9, v8

    :goto_5
    if-eq v1, v5, :cond_e

    goto :goto_6

    :cond_e
    if-eq v7, v5, :cond_f

    move v1, v7

    goto :goto_6

    :cond_f
    if-eq v8, v5, :cond_10

    move v1, v8

    goto :goto_6

    :cond_10
    if-eq v9, v5, :cond_11

    move v1, v9

    goto :goto_6

    :cond_11
    iget-object v0, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->charactersByArticle:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    add-int/lit8 v1, v0, -0x1

    :goto_6
    iget-object v0, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->charactersByArticle:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_13

    :cond_12
    :goto_7
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_8

    :cond_13
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    sub-int/2addr v1, v2

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/pdfbox/text/TextPosition;

    invoke-virtual {p1}, Lorg/apache/pdfbox/text/TextPosition;->isDiacritic()Z

    move-result v3

    if-eqz v3, :cond_14

    invoke-virtual {v1, p1}, Lorg/apache/pdfbox/text/TextPosition;->contains(Lorg/apache/pdfbox/text/TextPosition;)Z

    move-result v3

    if-eqz v3, :cond_14

    invoke-virtual {v1, p1}, Lorg/apache/pdfbox/text/TextPosition;->mergeDiacritic(Lorg/apache/pdfbox/text/TextPosition;)V

    goto :goto_8

    :cond_14
    invoke-virtual {v1}, Lorg/apache/pdfbox/text/TextPosition;->isDiacritic()Z

    move-result v3

    if-eqz v3, :cond_12

    invoke-virtual {p1, v1}, Lorg/apache/pdfbox/text/TextPosition;->contains(Lorg/apache/pdfbox/text/TextPosition;)Z

    move-result v3

    if-eqz v3, :cond_12

    invoke-virtual {p1, v1}, Lorg/apache/pdfbox/text/TextPosition;->mergeDiacritic(Lorg/apache/pdfbox/text/TextPosition;)V

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    sub-int/2addr v1, v2

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    goto :goto_7

    :cond_15
    :goto_8
    return-void
.end method

.method public setAddMoreFormatting(Z)V
    .locals 0

    iput-boolean p1, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->addMoreFormatting:Z

    return-void
.end method

.method public setArticleEnd(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->articleEnd:Ljava/lang/String;

    return-void
.end method

.method public setArticleStart(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->articleStart:Ljava/lang/String;

    return-void
.end method

.method public setAverageCharTolerance(F)V
    .locals 0

    iput p1, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->averageCharTolerance:F

    return-void
.end method

.method public setDropThreshold(F)V
    .locals 0

    iput p1, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->dropThreshold:F

    return-void
.end method

.method public setEndBookmark(Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineItem;)V
    .locals 0

    iput-object p1, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->endBookmark:Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineItem;

    return-void
.end method

.method public setEndPage(I)V
    .locals 0

    iput p1, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->endPage:I

    return-void
.end method

.method public setIndentThreshold(F)V
    .locals 0

    iput p1, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->indentThreshold:F

    return-void
.end method

.method public setLineSeparator(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->lineSeparator:Ljava/lang/String;

    return-void
.end method

.method public setListItemPatterns(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/util/regex/Pattern;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->listOfPatterns:Ljava/util/List;

    return-void
.end method

.method public setPageEnd(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->pageEnd:Ljava/lang/String;

    return-void
.end method

.method public setPageStart(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->pageStart:Ljava/lang/String;

    return-void
.end method

.method public setParagraphEnd(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->paragraphEnd:Ljava/lang/String;

    return-void
.end method

.method public setParagraphStart(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->paragraphStart:Ljava/lang/String;

    return-void
.end method

.method public setShouldSeparateByBeads(Z)V
    .locals 0

    iput-boolean p1, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->shouldSeparateByBeads:Z

    return-void
.end method

.method public setSortByPosition(Z)V
    .locals 0

    iput-boolean p1, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->sortByPosition:Z

    return-void
.end method

.method public setSpacingTolerance(F)V
    .locals 0

    iput p1, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->spacingTolerance:F

    return-void
.end method

.method public setStartBookmark(Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineItem;)V
    .locals 0

    iput-object p1, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->startBookmark:Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineItem;

    return-void
.end method

.method public setStartPage(I)V
    .locals 0

    iput p1, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->startPage:I

    return-void
.end method

.method public setSuppressDuplicateOverlappingText(Z)V
    .locals 0

    iput-boolean p1, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->suppressDuplicateOverlappingText:Z

    return-void
.end method

.method public setWordSeparator(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->wordSeparator:Ljava/lang/String;

    return-void
.end method

.method public startArticle()V
    .locals 1

    .line 1
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lorg/apache/pdfbox/text/PDFTextStripper;->startArticle(Z)V

    return-void
.end method

.method public startArticle(Z)V
    .locals 1

    .line 2
    iget-object p1, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->output:Ljava/io/Writer;

    invoke-virtual {p0}, Lorg/apache/pdfbox/text/PDFTextStripper;->getArticleStart()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    return-void
.end method

.method public startDocument(Lorg/apache/pdfbox/pdmodel/PDDocument;)V
    .locals 0

    return-void
.end method

.method public startPage(Lorg/apache/pdfbox/pdmodel/PDPage;)V
    .locals 0

    return-void
.end method

.method public writeCharacters(Lorg/apache/pdfbox/text/TextPosition;)V
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->output:Ljava/io/Writer;

    invoke-virtual {p1}, Lorg/apache/pdfbox/text/TextPosition;->getUnicode()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    return-void
.end method

.method public writeLineSeparator()V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->output:Ljava/io/Writer;

    invoke-virtual {p0}, Lorg/apache/pdfbox/text/PDFTextStripper;->getLineSeparator()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    return-void
.end method

.method public writePage()V
    .locals 34

    move-object/from16 v0, p0

    iget-object v1, v0, Lorg/apache/pdfbox/text/PDFTextStripper;->charactersByArticle:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v1

    if-lez v1, :cond_0

    invoke-virtual/range {p0 .. p0}, Lorg/apache/pdfbox/text/PDFTextStripper;->writePageStart()V

    :cond_0
    iget-object v1, v0, Lorg/apache/pdfbox/text/PDFTextStripper;->charactersByArticle:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v1

    const/4 v5, 0x0

    move-object v7, v5

    const v8, -0x800001

    const/high16 v9, -0x40800000    # -1.0f

    const/high16 v10, -0x40800000    # -1.0f

    const/high16 v11, -0x40800000    # -1.0f

    const v12, 0x7f7fffff    # Float.MAX_VALUE

    const/4 v13, 0x1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v14

    if-eqz v14, :cond_1f

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/util/List;

    invoke-virtual/range {p0 .. p0}, Lorg/apache/pdfbox/text/PDFTextStripper;->getSortByPosition()Z

    move-result v15

    if-eqz v15, :cond_2

    new-instance v15, Lorg/apache/pdfbox/text/TextPositionComparator;

    invoke-direct {v15}, Lorg/apache/pdfbox/text/TextPositionComparator;-><init>()V

    sget-boolean v16, Lorg/apache/pdfbox/text/PDFTextStripper;->useCustomQuickSort:Z

    if-eqz v16, :cond_1

    invoke-static {v14, v15}, Lorg/apache/pdfbox/util/QuickSort;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    goto :goto_1

    :cond_1
    invoke-static {v14, v15}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    :cond_2
    :goto_1
    invoke-interface {v14}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v15

    const/16 v16, 0x0

    move/from16 v2, v16

    move v3, v2

    :goto_2
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v18

    if-eqz v18, :cond_8

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lorg/apache/pdfbox/text/TextPosition;

    invoke-virtual/range {v18 .. v18}, Lorg/apache/pdfbox/text/TextPosition;->getUnicode()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v20, v1

    move/from16 v6, v16

    :goto_3
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v1

    if-ge v6, v1, :cond_7

    invoke-virtual {v4, v6}, Ljava/lang/String;->charAt(I)C

    move-result v1

    invoke-static {v1}, Ljava/lang/Character;->getDirectionality(C)B

    move-result v1

    move-object/from16 v21, v4

    if-eqz v1, :cond_5

    const/16 v4, 0xe

    if-eq v1, v4, :cond_5

    const/16 v4, 0xf

    if-ne v1, v4, :cond_3

    goto :goto_4

    :cond_3
    const/4 v4, 0x1

    if-eq v1, v4, :cond_4

    const/4 v4, 0x2

    if-eq v1, v4, :cond_4

    const/16 v4, 0x10

    if-eq v1, v4, :cond_4

    const/16 v4, 0x11

    if-ne v1, v4, :cond_6

    :cond_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_5

    :cond_5
    :goto_4
    add-int/lit8 v2, v2, 0x1

    :cond_6
    :goto_5
    add-int/lit8 v6, v6, 0x1

    move-object/from16 v4, v21

    goto :goto_3

    :cond_7
    move-object/from16 v1, v20

    goto :goto_2

    :cond_8
    move-object/from16 v20, v1

    if-le v3, v2, :cond_9

    const/4 v1, 0x1

    goto :goto_6

    :cond_9
    move/from16 v1, v16

    :goto_6
    xor-int/lit8 v2, v1, 0x1

    invoke-virtual {v0, v2}, Lorg/apache/pdfbox/text/PDFTextStripper;->startArticle(Z)V

    if-lez v3, :cond_a

    const/4 v2, 0x1

    goto :goto_7

    :cond_a
    move/from16 v2, v16

    :goto_7
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v14}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    const/high16 v6, -0x40800000    # -1.0f

    const/4 v14, 0x1

    :goto_8
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v15

    if-eqz v15, :cond_1d

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lorg/apache/pdfbox/text/TextPosition;

    move-object/from16 v21, v4

    new-instance v4, Lorg/apache/pdfbox/text/PDFTextStripper$PositionWrapper;

    invoke-direct {v4, v15}, Lorg/apache/pdfbox/text/PDFTextStripper$PositionWrapper;-><init>(Lorg/apache/pdfbox/text/TextPosition;)V

    invoke-virtual {v15}, Lorg/apache/pdfbox/text/TextPosition;->getUnicode()Ljava/lang/String;

    move-result-object v22

    move/from16 v23, v6

    if-eqz v5, :cond_c

    invoke-virtual {v15}, Lorg/apache/pdfbox/text/TextPosition;->getFont()Lorg/apache/pdfbox/pdmodel/font/PDFont;

    move-result-object v6

    invoke-virtual {v5}, Lorg/apache/pdfbox/text/PDFTextStripper$PositionWrapper;->getTextPosition()Lorg/apache/pdfbox/text/TextPosition;

    move-result-object v24

    move/from16 v25, v12

    invoke-virtual/range {v24 .. v24}, Lorg/apache/pdfbox/text/TextPosition;->getFont()Lorg/apache/pdfbox/pdmodel/font/PDFont;

    move-result-object v12

    if-ne v6, v12, :cond_b

    invoke-virtual {v15}, Lorg/apache/pdfbox/text/TextPosition;->getFontSize()F

    move-result v6

    invoke-virtual {v5}, Lorg/apache/pdfbox/text/PDFTextStripper$PositionWrapper;->getTextPosition()Lorg/apache/pdfbox/text/TextPosition;

    move-result-object v12

    invoke-virtual {v12}, Lorg/apache/pdfbox/text/TextPosition;->getFontSize()F

    move-result v12

    cmpl-float v6, v6, v12

    if-eqz v6, :cond_d

    :cond_b
    const/high16 v6, -0x40800000    # -1.0f

    goto :goto_9

    :cond_c
    move/from16 v25, v12

    :cond_d
    move/from16 v6, v23

    :goto_9
    invoke-virtual/range {p0 .. p0}, Lorg/apache/pdfbox/text/PDFTextStripper;->getSortByPosition()Z

    move-result v12

    if-eqz v12, :cond_e

    invoke-virtual {v15}, Lorg/apache/pdfbox/text/TextPosition;->getXDirAdj()F

    move-result v12

    invoke-virtual {v15}, Lorg/apache/pdfbox/text/TextPosition;->getYDirAdj()F

    move-result v23

    invoke-virtual {v15}, Lorg/apache/pdfbox/text/TextPosition;->getWidthDirAdj()F

    move-result v24

    invoke-virtual {v15}, Lorg/apache/pdfbox/text/TextPosition;->getHeightDir()F

    move-result v26

    goto :goto_a

    :cond_e
    invoke-virtual {v15}, Lorg/apache/pdfbox/text/TextPosition;->getX()F

    move-result v12

    invoke-virtual {v15}, Lorg/apache/pdfbox/text/TextPosition;->getY()F

    move-result v23

    invoke-virtual {v15}, Lorg/apache/pdfbox/text/TextPosition;->getWidth()F

    move-result v24

    invoke-virtual {v15}, Lorg/apache/pdfbox/text/TextPosition;->getHeight()F

    move-result v26

    :goto_a
    move-object/from16 v27, v4

    move/from16 v32, v26

    move/from16 v26, v12

    move/from16 v12, v32

    move/from16 v33, v23

    move/from16 v23, v13

    move/from16 v13, v33

    invoke-virtual {v15}, Lorg/apache/pdfbox/text/TextPosition;->getIndividualWidths()[F

    move-result-object v4

    array-length v4, v4

    invoke-virtual {v15}, Lorg/apache/pdfbox/text/TextPosition;->getWidthOfSpace()F

    move-result v28

    const/16 v29, 0x0

    cmpl-float v30, v28, v29

    const/high16 v31, 0x40000000    # 2.0f

    if-eqz v30, :cond_11

    const/high16 v30, 0x7fc00000    # Float.NaN

    cmpl-float v30, v28, v30

    if-nez v30, :cond_f

    goto :goto_b

    :cond_f
    cmpg-float v30, v10, v29

    if-gez v30, :cond_10

    invoke-virtual/range {p0 .. p0}, Lorg/apache/pdfbox/text/PDFTextStripper;->getSpacingTolerance()F

    move-result v10

    mul-float v10, v10, v28

    goto :goto_c

    :cond_10
    add-float v10, v28, v10

    div-float v10, v10, v31

    invoke-virtual/range {p0 .. p0}, Lorg/apache/pdfbox/text/PDFTextStripper;->getSpacingTolerance()F

    move-result v30

    mul-float v10, v10, v30

    goto :goto_c

    :cond_11
    :goto_b
    const v10, 0x7f7fffff    # Float.MAX_VALUE

    :goto_c
    cmpg-float v29, v6, v29

    int-to-float v4, v4

    div-float v4, v24, v4

    if-gez v29, :cond_12

    move v6, v4

    goto :goto_d

    :cond_12
    add-float/2addr v6, v4

    div-float v6, v6, v31

    :goto_d
    invoke-virtual/range {p0 .. p0}, Lorg/apache/pdfbox/text/PDFTextStripper;->getAverageCharTolerance()F

    move-result v4

    mul-float/2addr v4, v6

    const/high16 v19, -0x40800000    # -1.0f

    cmpl-float v29, v9, v19

    if-eqz v29, :cond_14

    cmpl-float v29, v4, v10

    if-lez v29, :cond_13

    add-float/2addr v9, v10

    goto :goto_e

    :cond_13
    add-float/2addr v9, v4

    goto :goto_e

    :cond_14
    const v9, -0x800001

    :goto_e
    if-eqz v5, :cond_17

    if-eqz v14, :cond_15

    invoke-virtual {v5}, Lorg/apache/pdfbox/text/PDFTextStripper$PositionWrapper;->setArticleStart()V

    move/from16 v14, v16

    :cond_15
    invoke-direct {v0, v13, v12, v8, v11}, Lorg/apache/pdfbox/text/PDFTextStripper;->overlap(FFFF)Z

    move-result v4

    if-nez v4, :cond_16

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/pdfbox/text/PDFTextStripper;->normalize(Ljava/util/List;ZZ)Ljava/util/List;

    move-result-object v4

    invoke-direct {v0, v4, v1}, Lorg/apache/pdfbox/text/PDFTextStripper;->writeLine(Ljava/util/List;Z)V

    invoke-interface {v3}, Ljava/util/List;->clear()V

    move-object/from16 v4, v27

    invoke-direct {v0, v4, v5, v7, v11}, Lorg/apache/pdfbox/text/PDFTextStripper;->handleLineSeparation(Lorg/apache/pdfbox/text/PDFTextStripper$PositionWrapper;Lorg/apache/pdfbox/text/PDFTextStripper$PositionWrapper;Lorg/apache/pdfbox/text/PDFTextStripper$PositionWrapper;F)Lorg/apache/pdfbox/text/PDFTextStripper$PositionWrapper;

    move-result-object v7

    move/from16 v11, v19

    const v8, -0x800001

    const v9, -0x800001

    const v17, -0x800001

    const v25, 0x7f7fffff    # Float.MAX_VALUE

    goto :goto_f

    :cond_16
    move-object/from16 v4, v27

    const v17, -0x800001

    :goto_f
    cmpl-float v10, v9, v17

    if-eqz v10, :cond_18

    cmpg-float v9, v9, v26

    if-gez v9, :cond_18

    invoke-virtual {v5}, Lorg/apache/pdfbox/text/PDFTextStripper$PositionWrapper;->getTextPosition()Lorg/apache/pdfbox/text/TextPosition;

    move-result-object v9

    invoke-virtual {v9}, Lorg/apache/pdfbox/text/TextPosition;->getUnicode()Ljava/lang/String;

    move-result-object v9

    if-eqz v9, :cond_18

    invoke-virtual {v5}, Lorg/apache/pdfbox/text/PDFTextStripper$PositionWrapper;->getTextPosition()Lorg/apache/pdfbox/text/TextPosition;

    move-result-object v9

    invoke-virtual {v9}, Lorg/apache/pdfbox/text/TextPosition;->getUnicode()Ljava/lang/String;

    move-result-object v9

    const-string v10, " "

    invoke-virtual {v9, v10}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_18

    invoke-static {}, Lorg/apache/pdfbox/text/PDFTextStripper$LineItem;->getWordSeparator()Lorg/apache/pdfbox/text/PDFTextStripper$LineItem;

    move-result-object v9

    invoke-interface {v3, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_10

    :cond_17
    move-object/from16 v4, v27

    const v17, -0x800001

    :cond_18
    :goto_10
    move/from16 v9, v25

    cmpl-float v10, v13, v8

    if-ltz v10, :cond_19

    move v8, v13

    :cond_19
    add-float v10, v26, v24

    if-eqz v22, :cond_1b

    if-eqz v23, :cond_1a

    if-nez v5, :cond_1a

    invoke-virtual/range {p0 .. p0}, Lorg/apache/pdfbox/text/PDFTextStripper;->writeParagraphStart()V

    :cond_1a
    new-instance v5, Lorg/apache/pdfbox/text/PDFTextStripper$LineItem;

    invoke-direct {v5, v15}, Lorg/apache/pdfbox/text/PDFTextStripper$LineItem;-><init>(Lorg/apache/pdfbox/text/TextPosition;)V

    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1b
    invoke-static {v11, v12}, Ljava/lang/Math;->max(FF)F

    move-result v11

    sub-float/2addr v13, v12

    invoke-static {v9, v13}, Ljava/lang/Math;->min(FF)F

    move-result v12

    if-eqz v23, :cond_1c

    invoke-virtual {v4}, Lorg/apache/pdfbox/text/PDFTextStripper$PositionWrapper;->setParagraphStart()V

    invoke-virtual {v4}, Lorg/apache/pdfbox/text/PDFTextStripper$PositionWrapper;->setLineStart()V

    move-object v7, v4

    move/from16 v13, v16

    goto :goto_11

    :cond_1c
    move/from16 v13, v23

    :goto_11
    move-object v5, v4

    move v9, v10

    move-object/from16 v4, v21

    move/from16 v10, v28

    goto/16 :goto_8

    :cond_1d
    move/from16 v25, v12

    move/from16 v23, v13

    const v17, -0x800001

    const/high16 v19, -0x40800000    # -1.0f

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_1e

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/pdfbox/text/PDFTextStripper;->normalize(Ljava/util/List;ZZ)Ljava/util/List;

    move-result-object v2

    invoke-direct {v0, v2, v1}, Lorg/apache/pdfbox/text/PDFTextStripper;->writeLine(Ljava/util/List;Z)V

    invoke-virtual/range {p0 .. p0}, Lorg/apache/pdfbox/text/PDFTextStripper;->writeParagraphEnd()V

    :cond_1e
    invoke-virtual/range {p0 .. p0}, Lorg/apache/pdfbox/text/PDFTextStripper;->endArticle()V

    move-object/from16 v1, v20

    move/from16 v13, v23

    move/from16 v12, v25

    goto/16 :goto_0

    :cond_1f
    invoke-virtual/range {p0 .. p0}, Lorg/apache/pdfbox/text/PDFTextStripper;->writePageEnd()V

    return-void
.end method

.method public writePageEnd()V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->output:Ljava/io/Writer;

    invoke-virtual {p0}, Lorg/apache/pdfbox/text/PDFTextStripper;->getPageEnd()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    return-void
.end method

.method public writePageStart()V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->output:Ljava/io/Writer;

    invoke-virtual {p0}, Lorg/apache/pdfbox/text/PDFTextStripper;->getPageStart()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    return-void
.end method

.method public writeParagraphEnd()V
    .locals 2

    iget-boolean v0, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->inParagraph:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/pdfbox/text/PDFTextStripper;->writeParagraphStart()V

    :cond_0
    iget-object v0, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->output:Ljava/io/Writer;

    invoke-virtual {p0}, Lorg/apache/pdfbox/text/PDFTextStripper;->getParagraphEnd()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->inParagraph:Z

    return-void
.end method

.method public writeParagraphSeparator()V
    .locals 0

    invoke-virtual {p0}, Lorg/apache/pdfbox/text/PDFTextStripper;->writeParagraphEnd()V

    invoke-virtual {p0}, Lorg/apache/pdfbox/text/PDFTextStripper;->writeParagraphStart()V

    return-void
.end method

.method public writeParagraphStart()V
    .locals 2

    iget-boolean v0, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->inParagraph:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/pdfbox/text/PDFTextStripper;->writeParagraphEnd()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->inParagraph:Z

    :cond_0
    iget-object v0, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->output:Ljava/io/Writer;

    invoke-virtual {p0}, Lorg/apache/pdfbox/text/PDFTextStripper;->getParagraphStart()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->inParagraph:Z

    return-void
.end method

.method public writeString(Ljava/lang/String;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->output:Ljava/io/Writer;

    invoke-virtual {v0, p1}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    return-void
.end method

.method public writeString(Ljava/lang/String;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lorg/apache/pdfbox/text/TextPosition;",
            ">;)V"
        }
    .end annotation

    .line 2
    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/text/PDFTextStripper;->writeString(Ljava/lang/String;)V

    return-void
.end method

.method public writeText(Lorg/apache/pdfbox/pdmodel/PDDocument;Ljava/io/Writer;)V
    .locals 0

    invoke-direct {p0}, Lorg/apache/pdfbox/text/PDFTextStripper;->resetEngine()V

    iput-object p1, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->document:Lorg/apache/pdfbox/pdmodel/PDDocument;

    iput-object p2, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->output:Ljava/io/Writer;

    invoke-virtual {p0}, Lorg/apache/pdfbox/text/PDFTextStripper;->getAddMoreFormatting()Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->lineSeparator:Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->paragraphEnd:Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->pageStart:Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->articleStart:Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->articleEnd:Ljava/lang/String;

    :cond_0
    iget-object p1, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->document:Lorg/apache/pdfbox/pdmodel/PDDocument;

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/text/PDFTextStripper;->startDocument(Lorg/apache/pdfbox/pdmodel/PDDocument;)V

    iget-object p1, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->document:Lorg/apache/pdfbox/pdmodel/PDDocument;

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/PDDocument;->getPages()Lorg/apache/pdfbox/pdmodel/PDPageTree;

    move-result-object p1

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/text/PDFTextStripper;->processPages(Lorg/apache/pdfbox/pdmodel/PDPageTree;)V

    iget-object p1, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->document:Lorg/apache/pdfbox/pdmodel/PDDocument;

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/text/PDFTextStripper;->endDocument(Lorg/apache/pdfbox/pdmodel/PDDocument;)V

    return-void
.end method

.method public writeWordSeparator()V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/text/PDFTextStripper;->output:Ljava/io/Writer;

    invoke-virtual {p0}, Lorg/apache/pdfbox/text/PDFTextStripper;->getWordSeparator()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    return-void
.end method

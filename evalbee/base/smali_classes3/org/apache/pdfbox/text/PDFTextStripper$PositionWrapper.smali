.class final Lorg/apache/pdfbox/text/PDFTextStripper$PositionWrapper;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/pdfbox/text/PDFTextStripper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "PositionWrapper"
.end annotation


# instance fields
.field private isArticleStart:Z

.field private isHangingIndent:Z

.field private isLineStart:Z

.field private isPageBreak:Z

.field private isParagraphStart:Z

.field private position:Lorg/apache/pdfbox/text/TextPosition;


# direct methods
.method public constructor <init>(Lorg/apache/pdfbox/text/TextPosition;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/pdfbox/text/PDFTextStripper$PositionWrapper;->isLineStart:Z

    iput-boolean v0, p0, Lorg/apache/pdfbox/text/PDFTextStripper$PositionWrapper;->isParagraphStart:Z

    iput-boolean v0, p0, Lorg/apache/pdfbox/text/PDFTextStripper$PositionWrapper;->isPageBreak:Z

    iput-boolean v0, p0, Lorg/apache/pdfbox/text/PDFTextStripper$PositionWrapper;->isHangingIndent:Z

    iput-boolean v0, p0, Lorg/apache/pdfbox/text/PDFTextStripper$PositionWrapper;->isArticleStart:Z

    iput-object p1, p0, Lorg/apache/pdfbox/text/PDFTextStripper$PositionWrapper;->position:Lorg/apache/pdfbox/text/TextPosition;

    return-void
.end method


# virtual methods
.method public getTextPosition()Lorg/apache/pdfbox/text/TextPosition;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/text/PDFTextStripper$PositionWrapper;->position:Lorg/apache/pdfbox/text/TextPosition;

    return-object v0
.end method

.method public isArticleStart()Z
    .locals 1

    iget-boolean v0, p0, Lorg/apache/pdfbox/text/PDFTextStripper$PositionWrapper;->isArticleStart:Z

    return v0
.end method

.method public isHangingIndent()Z
    .locals 1

    iget-boolean v0, p0, Lorg/apache/pdfbox/text/PDFTextStripper$PositionWrapper;->isHangingIndent:Z

    return v0
.end method

.method public isLineStart()Z
    .locals 1

    iget-boolean v0, p0, Lorg/apache/pdfbox/text/PDFTextStripper$PositionWrapper;->isLineStart:Z

    return v0
.end method

.method public isPageBreak()Z
    .locals 1

    iget-boolean v0, p0, Lorg/apache/pdfbox/text/PDFTextStripper$PositionWrapper;->isPageBreak:Z

    return v0
.end method

.method public isParagraphStart()Z
    .locals 1

    iget-boolean v0, p0, Lorg/apache/pdfbox/text/PDFTextStripper$PositionWrapper;->isParagraphStart:Z

    return v0
.end method

.method public setArticleStart()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/pdfbox/text/PDFTextStripper$PositionWrapper;->isArticleStart:Z

    return-void
.end method

.method public setHangingIndent()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/pdfbox/text/PDFTextStripper$PositionWrapper;->isHangingIndent:Z

    return-void
.end method

.method public setLineStart()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/pdfbox/text/PDFTextStripper$PositionWrapper;->isLineStart:Z

    return-void
.end method

.method public setPageBreak()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/pdfbox/text/PDFTextStripper$PositionWrapper;->isPageBreak:Z

    return-void
.end method

.method public setParagraphStart()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/pdfbox/text/PDFTextStripper$PositionWrapper;->isParagraphStart:Z

    return-void
.end method

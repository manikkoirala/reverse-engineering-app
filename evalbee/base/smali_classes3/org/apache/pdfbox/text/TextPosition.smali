.class public final Lorg/apache/pdfbox/text/TextPosition;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final DIACRITICS:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final charCodes:[I

.field private final endX:F

.field private final endY:F

.field private final font:Lorg/apache/pdfbox/pdmodel/font/PDFont;

.field private final fontSize:F

.field private final fontSizePt:I

.field private final maxHeight:F

.field private final pageHeight:F

.field private final pageWidth:F

.field private final rotation:I

.field private final textMatrix:Lorg/apache/pdfbox/util/Matrix;

.field private unicode:Ljava/lang/String;

.field private final widthOfSpace:F

.field private widths:[F

.field private final x:F

.field private final y:F


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    invoke-static {}, Lorg/apache/pdfbox/text/TextPosition;->createDiacritics()Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lorg/apache/pdfbox/text/TextPosition;->DIACRITICS:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>(IFFLorg/apache/pdfbox/util/Matrix;FFFFFLjava/lang/String;[ILorg/apache/pdfbox/pdmodel/font/PDFont;FI)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p4, p0, Lorg/apache/pdfbox/text/TextPosition;->textMatrix:Lorg/apache/pdfbox/util/Matrix;

    iput p5, p0, Lorg/apache/pdfbox/text/TextPosition;->endX:F

    iput p6, p0, Lorg/apache/pdfbox/text/TextPosition;->endY:F

    iput p1, p0, Lorg/apache/pdfbox/text/TextPosition;->rotation:I

    iput p7, p0, Lorg/apache/pdfbox/text/TextPosition;->maxHeight:F

    iput p3, p0, Lorg/apache/pdfbox/text/TextPosition;->pageHeight:F

    iput p2, p0, Lorg/apache/pdfbox/text/TextPosition;->pageWidth:F

    const/4 p4, 0x1

    new-array p4, p4, [F

    const/4 p5, 0x0

    aput p8, p4, p5

    iput-object p4, p0, Lorg/apache/pdfbox/text/TextPosition;->widths:[F

    iput p9, p0, Lorg/apache/pdfbox/text/TextPosition;->widthOfSpace:F

    iput-object p10, p0, Lorg/apache/pdfbox/text/TextPosition;->unicode:Ljava/lang/String;

    iput-object p11, p0, Lorg/apache/pdfbox/text/TextPosition;->charCodes:[I

    iput-object p12, p0, Lorg/apache/pdfbox/text/TextPosition;->font:Lorg/apache/pdfbox/pdmodel/font/PDFont;

    iput p13, p0, Lorg/apache/pdfbox/text/TextPosition;->fontSize:F

    iput p14, p0, Lorg/apache/pdfbox/text/TextPosition;->fontSizePt:I

    int-to-float p4, p1

    invoke-direct {p0, p4}, Lorg/apache/pdfbox/text/TextPosition;->getXRot(F)F

    move-result p5

    iput p5, p0, Lorg/apache/pdfbox/text/TextPosition;->x:F

    if-eqz p1, :cond_1

    const/16 p5, 0xb4

    if-ne p1, p5, :cond_0

    goto :goto_0

    :cond_0
    invoke-direct {p0, p4}, Lorg/apache/pdfbox/text/TextPosition;->getYLowerLeftRot(F)F

    move-result p1

    sub-float/2addr p2, p1

    iput p2, p0, Lorg/apache/pdfbox/text/TextPosition;->y:F

    goto :goto_1

    :cond_1
    :goto_0
    invoke-direct {p0, p4}, Lorg/apache/pdfbox/text/TextPosition;->getYLowerLeftRot(F)F

    move-result p1

    sub-float/2addr p3, p1

    iput p3, p0, Lorg/apache/pdfbox/text/TextPosition;->y:F

    :goto_1
    return-void
.end method

.method private combineDiacritic(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Ljava/lang/String;->codePointAt(I)I

    move-result v0

    sget-object v1, Lorg/apache/pdfbox/text/TextPosition;->DIACRITICS:Ljava/util/Map;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    return-object p1

    :cond_0
    sget-object v0, Ljava/text/Normalizer$Form;->NFKC:Ljava/text/Normalizer$Form;

    invoke-static {p1, v0}, Ljava/text/Normalizer;->normalize(Ljava/lang/CharSequence;Ljava/text/Normalizer$Form;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private static createDiacritics()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    const/16 v1, 0x60

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "\u0300"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/16 v1, 0x2cb

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/16 v1, 0x27

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "\u0301"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/16 v1, 0x2b9

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/16 v1, 0x2ca

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/16 v1, 0x5e

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "\u0302"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/16 v1, 0x2c6

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/16 v1, 0x7e

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "\u0303"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/16 v1, 0x2c9

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "\u0304"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/16 v1, 0xb0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "\u030a"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/16 v1, 0x2ba

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "\u030b"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/16 v1, 0x2c7

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "\u030c"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/16 v1, 0x2c8

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "\u030d"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/16 v1, 0x22

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "\u030e"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/16 v1, 0x2bb

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "\u0312"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/16 v1, 0x2bc

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "\u0313"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/16 v1, 0x486

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/16 v1, 0x55a

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/16 v1, 0x2bd

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "\u0314"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/16 v1, 0x485

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/16 v1, 0x559

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/16 v1, 0x2d4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "\u031d"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/16 v1, 0x2d5

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "\u031e"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/16 v1, 0x2d6

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "\u031f"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/16 v1, 0x2d7

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "\u0320"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/16 v1, 0x2b2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "\u0321"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/16 v1, 0x2cc

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "\u0329"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/16 v1, 0x2b7

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "\u032b"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/16 v1, 0x2cd

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "\u0331"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/16 v1, 0x5f

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "\u0332"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/16 v1, 0x204e

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "\u0359"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object v0
.end method

.method private getWidthRot(F)F
    .locals 1

    const/high16 v0, 0x42b40000    # 90.0f

    cmpl-float v0, p1, v0

    if-eqz v0, :cond_1

    const/high16 v0, 0x43870000    # 270.0f

    cmpl-float p1, p1, v0

    if-nez p1, :cond_0

    goto :goto_1

    :cond_0
    iget p1, p0, Lorg/apache/pdfbox/text/TextPosition;->endX:F

    iget-object v0, p0, Lorg/apache/pdfbox/text/TextPosition;->textMatrix:Lorg/apache/pdfbox/util/Matrix;

    invoke-virtual {v0}, Lorg/apache/pdfbox/util/Matrix;->getTranslateX()F

    move-result v0

    :goto_0
    sub-float/2addr p1, v0

    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result p1

    return p1

    :cond_1
    :goto_1
    iget p1, p0, Lorg/apache/pdfbox/text/TextPosition;->endY:F

    iget-object v0, p0, Lorg/apache/pdfbox/text/TextPosition;->textMatrix:Lorg/apache/pdfbox/util/Matrix;

    invoke-virtual {v0}, Lorg/apache/pdfbox/util/Matrix;->getTranslateY()F

    move-result v0

    goto :goto_0
.end method

.method private getXRot(F)F
    .locals 2

    const/4 v0, 0x0

    cmpl-float v1, p1, v0

    if-nez v1, :cond_0

    iget-object p1, p0, Lorg/apache/pdfbox/text/TextPosition;->textMatrix:Lorg/apache/pdfbox/util/Matrix;

    invoke-virtual {p1}, Lorg/apache/pdfbox/util/Matrix;->getTranslateX()F

    move-result p1

    return p1

    :cond_0
    const/high16 v1, 0x42b40000    # 90.0f

    cmpl-float v1, p1, v1

    if-nez v1, :cond_1

    iget-object p1, p0, Lorg/apache/pdfbox/text/TextPosition;->textMatrix:Lorg/apache/pdfbox/util/Matrix;

    invoke-virtual {p1}, Lorg/apache/pdfbox/util/Matrix;->getTranslateY()F

    move-result p1

    return p1

    :cond_1
    const/high16 v1, 0x43340000    # 180.0f

    cmpl-float v1, p1, v1

    if-nez v1, :cond_2

    iget p1, p0, Lorg/apache/pdfbox/text/TextPosition;->pageWidth:F

    iget-object v0, p0, Lorg/apache/pdfbox/text/TextPosition;->textMatrix:Lorg/apache/pdfbox/util/Matrix;

    invoke-virtual {v0}, Lorg/apache/pdfbox/util/Matrix;->getTranslateX()F

    move-result v0

    sub-float/2addr p1, v0

    return p1

    :cond_2
    const/high16 v1, 0x43870000    # 270.0f

    cmpl-float p1, p1, v1

    if-nez p1, :cond_3

    iget p1, p0, Lorg/apache/pdfbox/text/TextPosition;->pageHeight:F

    iget-object v0, p0, Lorg/apache/pdfbox/text/TextPosition;->textMatrix:Lorg/apache/pdfbox/util/Matrix;

    invoke-virtual {v0}, Lorg/apache/pdfbox/util/Matrix;->getTranslateX()F

    move-result v0

    sub-float/2addr p1, v0

    return p1

    :cond_3
    return v0
.end method

.method private getYLowerLeftRot(F)F
    .locals 2

    const/4 v0, 0x0

    cmpl-float v1, p1, v0

    if-nez v1, :cond_0

    iget-object p1, p0, Lorg/apache/pdfbox/text/TextPosition;->textMatrix:Lorg/apache/pdfbox/util/Matrix;

    invoke-virtual {p1}, Lorg/apache/pdfbox/util/Matrix;->getTranslateY()F

    move-result p1

    return p1

    :cond_0
    const/high16 v1, 0x42b40000    # 90.0f

    cmpl-float v1, p1, v1

    if-nez v1, :cond_1

    iget p1, p0, Lorg/apache/pdfbox/text/TextPosition;->pageWidth:F

    iget-object v0, p0, Lorg/apache/pdfbox/text/TextPosition;->textMatrix:Lorg/apache/pdfbox/util/Matrix;

    invoke-virtual {v0}, Lorg/apache/pdfbox/util/Matrix;->getTranslateX()F

    move-result v0

    sub-float/2addr p1, v0

    return p1

    :cond_1
    const/high16 v1, 0x43340000    # 180.0f

    cmpl-float v1, p1, v1

    if-nez v1, :cond_2

    iget p1, p0, Lorg/apache/pdfbox/text/TextPosition;->pageHeight:F

    iget-object v0, p0, Lorg/apache/pdfbox/text/TextPosition;->textMatrix:Lorg/apache/pdfbox/util/Matrix;

    invoke-virtual {v0}, Lorg/apache/pdfbox/util/Matrix;->getTranslateY()F

    move-result v0

    sub-float/2addr p1, v0

    return p1

    :cond_2
    const/high16 v1, 0x43870000    # 270.0f

    cmpl-float p1, p1, v1

    if-nez p1, :cond_3

    iget-object p1, p0, Lorg/apache/pdfbox/text/TextPosition;->textMatrix:Lorg/apache/pdfbox/util/Matrix;

    invoke-virtual {p1}, Lorg/apache/pdfbox/util/Matrix;->getTranslateX()F

    move-result p1

    return p1

    :cond_3
    return v0
.end method

.method private insertDiacritic(ILorg/apache/pdfbox/text/TextPosition;)V
    .locals 5

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lorg/apache/pdfbox/text/TextPosition;->unicode:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v1, v2, p1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lorg/apache/pdfbox/text/TextPosition;->widths:[F

    array-length v3, v1

    add-int/lit8 v3, v3, 0x1

    new-array v3, v3, [F

    invoke-static {v1, v2, v3, v2, p1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v1, p0, Lorg/apache/pdfbox/text/TextPosition;->unicode:Ljava/lang/String;

    invoke-virtual {v1, p1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lorg/apache/pdfbox/text/TextPosition;->widths:[F

    aget v1, v1, p1

    aput v1, v3, p1

    invoke-virtual {p2}, Lorg/apache/pdfbox/text/TextPosition;->getUnicode()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p0, p2}, Lorg/apache/pdfbox/text/TextPosition;->combineDiacritic(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 p2, p1, 0x1

    const/4 v1, 0x0

    aput v1, v3, p2

    iget-object v1, p0, Lorg/apache/pdfbox/text/TextPosition;->unicode:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {v1, p2, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lorg/apache/pdfbox/text/TextPosition;->widths:[F

    add-int/lit8 v2, p1, 0x2

    array-length v4, v1

    sub-int/2addr v4, p1

    add-int/lit8 v4, v4, -0x1

    invoke-static {v1, p2, v3, v2, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lorg/apache/pdfbox/text/TextPosition;->unicode:Ljava/lang/String;

    iput-object v3, p0, Lorg/apache/pdfbox/text/TextPosition;->widths:[F

    return-void
.end method


# virtual methods
.method public contains(Lorg/apache/pdfbox/text/TextPosition;)Z
    .locals 12

    invoke-virtual {p0}, Lorg/apache/pdfbox/text/TextPosition;->getXDirAdj()F

    move-result v0

    float-to-double v0, v0

    invoke-virtual {p0}, Lorg/apache/pdfbox/text/TextPosition;->getXDirAdj()F

    move-result v2

    invoke-virtual {p0}, Lorg/apache/pdfbox/text/TextPosition;->getWidthDirAdj()F

    move-result v3

    add-float/2addr v2, v3

    float-to-double v2, v2

    invoke-virtual {p1}, Lorg/apache/pdfbox/text/TextPosition;->getXDirAdj()F

    move-result v4

    float-to-double v4, v4

    invoke-virtual {p1}, Lorg/apache/pdfbox/text/TextPosition;->getXDirAdj()F

    move-result v6

    invoke-virtual {p1}, Lorg/apache/pdfbox/text/TextPosition;->getWidthDirAdj()F

    move-result v7

    add-float/2addr v6, v7

    float-to-double v6, v6

    cmpg-double v8, v6, v0

    const/4 v9, 0x0

    if-lez v8, :cond_6

    cmpl-double v8, v4, v2

    if-ltz v8, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Lorg/apache/pdfbox/text/TextPosition;->getYDirAdj()F

    move-result v8

    invoke-virtual {p1}, Lorg/apache/pdfbox/text/TextPosition;->getHeightDir()F

    move-result v10

    add-float/2addr v8, v10

    invoke-virtual {p0}, Lorg/apache/pdfbox/text/TextPosition;->getYDirAdj()F

    move-result v10

    cmpg-float v8, v8, v10

    if-ltz v8, :cond_6

    invoke-virtual {p1}, Lorg/apache/pdfbox/text/TextPosition;->getYDirAdj()F

    move-result p1

    invoke-virtual {p0}, Lorg/apache/pdfbox/text/TextPosition;->getYDirAdj()F

    move-result v8

    invoke-virtual {p0}, Lorg/apache/pdfbox/text/TextPosition;->getHeightDir()F

    move-result v10

    add-float/2addr v8, v10

    cmpl-float p1, p1, v8

    if-lez p1, :cond_1

    goto :goto_0

    :cond_1
    cmpl-double p1, v4, v0

    const-wide v10, 0x3fc3333333333333L    # 0.15

    const/4 v8, 0x1

    if-lez p1, :cond_3

    cmpl-double p1, v6, v2

    if-lez p1, :cond_3

    sub-double/2addr v2, v4

    invoke-virtual {p0}, Lorg/apache/pdfbox/text/TextPosition;->getWidthDirAdj()F

    move-result p1

    float-to-double v0, p1

    div-double/2addr v2, v0

    cmpl-double p1, v2, v10

    if-lez p1, :cond_2

    move v9, v8

    :cond_2
    return v9

    :cond_3
    cmpg-double p1, v4, v0

    if-gez p1, :cond_5

    cmpg-double p1, v6, v2

    if-gez p1, :cond_5

    sub-double/2addr v6, v0

    invoke-virtual {p0}, Lorg/apache/pdfbox/text/TextPosition;->getWidthDirAdj()F

    move-result p1

    float-to-double v0, p1

    div-double/2addr v6, v0

    cmpl-double p1, v6, v10

    if-lez p1, :cond_4

    move v9, v8

    :cond_4
    return v9

    :cond_5
    return v8

    :cond_6
    :goto_0
    return v9
.end method

.method public getCharacterCodes()[I
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/text/TextPosition;->charCodes:[I

    return-object v0
.end method

.method public getDir()F
    .locals 7

    iget-object v0, p0, Lorg/apache/pdfbox/text/TextPosition;->textMatrix:Lorg/apache/pdfbox/util/Matrix;

    invoke-virtual {v0}, Lorg/apache/pdfbox/util/Matrix;->getScaleY()F

    move-result v0

    iget-object v1, p0, Lorg/apache/pdfbox/text/TextPosition;->textMatrix:Lorg/apache/pdfbox/util/Matrix;

    invoke-virtual {v1}, Lorg/apache/pdfbox/util/Matrix;->getShearY()F

    move-result v1

    iget-object v2, p0, Lorg/apache/pdfbox/text/TextPosition;->textMatrix:Lorg/apache/pdfbox/util/Matrix;

    invoke-virtual {v2}, Lorg/apache/pdfbox/util/Matrix;->getScaleX()F

    move-result v2

    iget-object v3, p0, Lorg/apache/pdfbox/text/TextPosition;->textMatrix:Lorg/apache/pdfbox/util/Matrix;

    invoke-virtual {v3}, Lorg/apache/pdfbox/util/Matrix;->getShearX()F

    move-result v3

    const/4 v4, 0x0

    cmpl-float v5, v0, v4

    if-lez v5, :cond_0

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v5

    cmpg-float v5, v5, v3

    if-gez v5, :cond_0

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v5

    cmpg-float v5, v5, v0

    if-gez v5, :cond_0

    cmpl-float v5, v3, v4

    if-lez v5, :cond_0

    return v4

    :cond_0
    cmpg-float v5, v0, v4

    if-gez v5, :cond_1

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v5

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v6

    cmpg-float v5, v5, v6

    if-gez v5, :cond_1

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v5

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v6

    cmpg-float v5, v5, v6

    if-gez v5, :cond_1

    cmpg-float v5, v3, v4

    if-gez v5, :cond_1

    const/high16 v0, 0x43340000    # 180.0f

    return v0

    :cond_1
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v5

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v6

    cmpg-float v5, v5, v6

    if-gez v5, :cond_2

    cmpl-float v5, v1, v4

    if-lez v5, :cond_2

    cmpg-float v5, v2, v4

    if-gez v5, :cond_2

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v5

    cmpg-float v5, v5, v1

    if-gez v5, :cond_2

    const/high16 v0, 0x42b40000    # 90.0f

    return v0

    :cond_2
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    cmpg-float v0, v0, v2

    if-gez v0, :cond_3

    cmpg-float v0, v1, v4

    if-gez v0, :cond_3

    cmpl-float v0, v2, v4

    if-lez v0, :cond_3

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v0

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_3

    const/high16 v0, 0x43870000    # 270.0f

    return v0

    :cond_3
    return v4
.end method

.method public getFont()Lorg/apache/pdfbox/pdmodel/font/PDFont;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/text/TextPosition;->font:Lorg/apache/pdfbox/pdmodel/font/PDFont;

    return-object v0
.end method

.method public getFontSize()F
    .locals 1

    iget v0, p0, Lorg/apache/pdfbox/text/TextPosition;->fontSize:F

    return v0
.end method

.method public getFontSizeInPt()F
    .locals 1

    iget v0, p0, Lorg/apache/pdfbox/text/TextPosition;->fontSizePt:I

    int-to-float v0, v0

    return v0
.end method

.method public getHeight()F
    .locals 1

    iget v0, p0, Lorg/apache/pdfbox/text/TextPosition;->maxHeight:F

    return v0
.end method

.method public getHeightDir()F
    .locals 1

    iget v0, p0, Lorg/apache/pdfbox/text/TextPosition;->maxHeight:F

    return v0
.end method

.method public getIndividualWidths()[F
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/text/TextPosition;->widths:[F

    return-object v0
.end method

.method public getTextMatrix()Lorg/apache/pdfbox/util/Matrix;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/text/TextPosition;->textMatrix:Lorg/apache/pdfbox/util/Matrix;

    return-object v0
.end method

.method public getUnicode()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/text/TextPosition;->unicode:Ljava/lang/String;

    return-object v0
.end method

.method public getWidth()F
    .locals 1

    iget v0, p0, Lorg/apache/pdfbox/text/TextPosition;->rotation:I

    int-to-float v0, v0

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/text/TextPosition;->getWidthRot(F)F

    move-result v0

    return v0
.end method

.method public getWidthDirAdj()F
    .locals 1

    invoke-virtual {p0}, Lorg/apache/pdfbox/text/TextPosition;->getDir()F

    move-result v0

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/text/TextPosition;->getWidthRot(F)F

    move-result v0

    return v0
.end method

.method public getWidthOfSpace()F
    .locals 1

    iget v0, p0, Lorg/apache/pdfbox/text/TextPosition;->widthOfSpace:F

    return v0
.end method

.method public getX()F
    .locals 1

    iget v0, p0, Lorg/apache/pdfbox/text/TextPosition;->x:F

    return v0
.end method

.method public getXDirAdj()F
    .locals 1

    invoke-virtual {p0}, Lorg/apache/pdfbox/text/TextPosition;->getDir()F

    move-result v0

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/text/TextPosition;->getXRot(F)F

    move-result v0

    return v0
.end method

.method public getXScale()F
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/text/TextPosition;->textMatrix:Lorg/apache/pdfbox/util/Matrix;

    invoke-virtual {v0}, Lorg/apache/pdfbox/util/Matrix;->getScalingFactorX()F

    move-result v0

    return v0
.end method

.method public getY()F
    .locals 1

    iget v0, p0, Lorg/apache/pdfbox/text/TextPosition;->y:F

    return v0
.end method

.method public getYDirAdj()F
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/text/TextPosition;->getDir()F

    move-result v0

    const/4 v1, 0x0

    cmpl-float v1, v0, v1

    if-eqz v1, :cond_1

    const/high16 v1, 0x43340000    # 180.0f

    cmpl-float v1, v0, v1

    if-nez v1, :cond_0

    goto :goto_1

    :cond_0
    iget v1, p0, Lorg/apache/pdfbox/text/TextPosition;->pageWidth:F

    :goto_0
    invoke-direct {p0, v0}, Lorg/apache/pdfbox/text/TextPosition;->getYLowerLeftRot(F)F

    move-result v0

    sub-float/2addr v1, v0

    return v1

    :cond_1
    :goto_1
    iget v1, p0, Lorg/apache/pdfbox/text/TextPosition;->pageHeight:F

    goto :goto_0
.end method

.method public getYScale()F
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/text/TextPosition;->textMatrix:Lorg/apache/pdfbox/util/Matrix;

    invoke-virtual {v0}, Lorg/apache/pdfbox/util/Matrix;->getScalingFactorY()F

    move-result v0

    return v0
.end method

.method public isDiacritic()Z
    .locals 4

    invoke-virtual {p0}, Lorg/apache/pdfbox/text/TextPosition;->getUnicode()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-eq v1, v3, :cond_0

    return v2

    :cond_0
    invoke-virtual {v0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    invoke-static {v0}, Ljava/lang/Character;->getType(C)I

    move-result v0

    const/4 v1, 0x6

    if-eq v0, v1, :cond_1

    const/16 v1, 0x1b

    if-eq v0, v1, :cond_1

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    :cond_1
    move v2, v3

    :cond_2
    return v2
.end method

.method public mergeDiacritic(Lorg/apache/pdfbox/text/TextPosition;)V
    .locals 12

    invoke-virtual {p1}, Lorg/apache/pdfbox/text/TextPosition;->getUnicode()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    return-void

    :cond_0
    invoke-virtual {p1}, Lorg/apache/pdfbox/text/TextPosition;->getXDirAdj()F

    move-result v0

    iget-object v2, p1, Lorg/apache/pdfbox/text/TextPosition;->widths:[F

    const/4 v3, 0x0

    aget v2, v2, v3

    add-float/2addr v2, v0

    invoke-virtual {p0}, Lorg/apache/pdfbox/text/TextPosition;->getXDirAdj()F

    move-result v4

    iget-object v5, p0, Lorg/apache/pdfbox/text/TextPosition;->unicode:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    move v6, v4

    move v4, v3

    :goto_0
    if-ge v3, v5, :cond_7

    if-nez v4, :cond_7

    iget-object v7, p0, Lorg/apache/pdfbox/text/TextPosition;->widths:[F

    aget v8, v7, v3

    add-float v9, v6, v8

    cmpg-float v10, v0, v6

    if-gez v10, :cond_3

    cmpg-float v11, v2, v9

    if-gtz v11, :cond_3

    if-nez v3, :cond_1

    :goto_1
    goto :goto_2

    :cond_1
    sub-float v4, v2, v6

    div-float/2addr v4, v8

    sub-float v8, v6, v0

    add-int/lit8 v9, v3, -0x1

    aget v7, v7, v9

    div-float/2addr v8, v7

    cmpl-float v4, v4, v8

    if-ltz v4, :cond_2

    goto :goto_1

    :cond_2
    invoke-direct {p0, v9, p1}, Lorg/apache/pdfbox/text/TextPosition;->insertDiacritic(ILorg/apache/pdfbox/text/TextPosition;)V

    goto :goto_3

    :cond_3
    if-gez v10, :cond_4

    cmpl-float v7, v2, v9

    if-lez v7, :cond_4

    :goto_2
    invoke-direct {p0, v3, p1}, Lorg/apache/pdfbox/text/TextPosition;->insertDiacritic(ILorg/apache/pdfbox/text/TextPosition;)V

    :goto_3
    move v4, v1

    goto :goto_4

    :cond_4
    cmpl-float v7, v0, v6

    if-ltz v7, :cond_5

    cmpg-float v8, v2, v9

    if-gtz v8, :cond_5

    goto :goto_2

    :cond_5
    if-ltz v7, :cond_6

    cmpl-float v7, v2, v9

    if-lez v7, :cond_6

    add-int/lit8 v7, v5, -0x1

    if-ne v3, v7, :cond_6

    goto :goto_2

    :cond_6
    :goto_4
    iget-object v7, p0, Lorg/apache/pdfbox/text/TextPosition;->widths:[F

    aget v7, v7, v3

    add-float/2addr v6, v7

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_7
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lorg/apache/pdfbox/text/TextPosition;->getUnicode()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

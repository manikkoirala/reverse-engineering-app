.class final Lorg/apache/pdfbox/text/PDFTextStripper$WordWithTextPositions;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/pdfbox/text/PDFTextStripper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "WordWithTextPositions"
.end annotation


# instance fields
.field protected text:Ljava/lang/String;

.field protected textPositions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lorg/apache/pdfbox/text/TextPosition;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lorg/apache/pdfbox/text/TextPosition;",
            ">;)V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lorg/apache/pdfbox/text/PDFTextStripper$WordWithTextPositions;->text:Ljava/lang/String;

    iput-object p2, p0, Lorg/apache/pdfbox/text/PDFTextStripper$WordWithTextPositions;->textPositions:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public getText()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/text/PDFTextStripper$WordWithTextPositions;->text:Ljava/lang/String;

    return-object v0
.end method

.method public getTextPositions()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lorg/apache/pdfbox/text/TextPosition;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/pdfbox/text/PDFTextStripper$WordWithTextPositions;->textPositions:Ljava/util/List;

    return-object v0
.end method

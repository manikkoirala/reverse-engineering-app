.class public Lorg/apache/pdfbox/pdfwriter/COSWriter;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/pdfbox/cos/ICOSVisitor;
.implements Ljava/io/Closeable;


# static fields
.field public static final ARRAY_CLOSE:[B

.field public static final ARRAY_OPEN:[B

.field public static final COMMENT:[B

.field public static final DICT_CLOSE:[B

.field public static final DICT_OPEN:[B

.field public static final ENDOBJ:[B

.field public static final ENDSTREAM:[B

.field public static final EOF:[B

.field public static final GARBAGE:[B

.field public static final OBJ:[B

.field public static final REFERENCE:[B

.field public static final SPACE:[B

.field public static final STARTXREF:[B

.field public static final STREAM:[B

.field public static final TRAILER:[B

.field public static final VERSION:[B

.field public static final XREF:[B

.field public static final XREF_FREE:[B

.field public static final XREF_USED:[B


# instance fields
.field private final actualsAdded:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lorg/apache/pdfbox/cos/COSBase;",
            ">;"
        }
    .end annotation
.end field

.field private byteRangeLength:J

.field private byteRangeOffset:J

.field private currentObjectKey:Lorg/apache/pdfbox/cos/COSObjectKey;

.field private fdfDocument:Lorg/apache/pdfbox/pdmodel/fdf/FDFDocument;

.field private final formatDecimal:Ljava/text/NumberFormat;

.field private final formatXrefGeneration:Ljava/text/NumberFormat;

.field private final formatXrefOffset:Ljava/text/NumberFormat;

.field private incrementalInput:Ljava/io/InputStream;

.field private incrementalOutput:Ljava/io/OutputStream;

.field private incrementalUpdate:Z

.field private final keyObject:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lorg/apache/pdfbox/cos/COSObjectKey;",
            "Lorg/apache/pdfbox/cos/COSBase;",
            ">;"
        }
    .end annotation
.end field

.field private number:J

.field private final objectKeys:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lorg/apache/pdfbox/cos/COSBase;",
            "Lorg/apache/pdfbox/cos/COSObjectKey;",
            ">;"
        }
    .end annotation
.end field

.field private final objectsToWrite:Ljava/util/Deque;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Deque<",
            "Lorg/apache/pdfbox/cos/COSBase;",
            ">;"
        }
    .end annotation
.end field

.field private final objectsToWriteSet:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lorg/apache/pdfbox/cos/COSBase;",
            ">;"
        }
    .end annotation
.end field

.field private output:Ljava/io/OutputStream;

.field private pdDocument:Lorg/apache/pdfbox/pdmodel/PDDocument;

.field private reachedSignature:Z

.field private signatureInterface:Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/SignatureInterface;

.field private signatureLength:J

.field private signatureOffset:J

.field private standardOutput:Lorg/apache/pdfbox/pdfwriter/COSStandardOutputStream;

.field private startxref:J

.field private willEncrypt:Z

.field private final writtenObjects:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lorg/apache/pdfbox/cos/COSBase;",
            ">;"
        }
    .end annotation
.end field

.field private final xRefEntries:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lorg/apache/pdfbox/pdfwriter/COSWriterXRefEntry;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    sget-object v0, Lorg/apache/pdfbox/util/Charsets;->US_ASCII:Ljava/nio/charset/Charset;

    const-string v1, "<<"

    invoke-virtual {v1, v0}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v1

    sput-object v1, Lorg/apache/pdfbox/pdfwriter/COSWriter;->DICT_OPEN:[B

    const-string v1, ">>"

    invoke-virtual {v1, v0}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v1

    sput-object v1, Lorg/apache/pdfbox/pdfwriter/COSWriter;->DICT_CLOSE:[B

    const/4 v1, 0x1

    new-array v2, v1, [B

    const/16 v3, 0x20

    const/4 v4, 0x0

    aput-byte v3, v2, v4

    sput-object v2, Lorg/apache/pdfbox/pdfwriter/COSWriter;->SPACE:[B

    new-array v1, v1, [B

    const/16 v2, 0x25

    aput-byte v2, v1, v4

    sput-object v1, Lorg/apache/pdfbox/pdfwriter/COSWriter;->COMMENT:[B

    const-string v1, "PDF-1.4"

    invoke-virtual {v1, v0}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v1

    sput-object v1, Lorg/apache/pdfbox/pdfwriter/COSWriter;->VERSION:[B

    const/4 v1, 0x4

    new-array v1, v1, [B

    fill-array-data v1, :array_0

    sput-object v1, Lorg/apache/pdfbox/pdfwriter/COSWriter;->GARBAGE:[B

    const-string v1, "%%EOF"

    invoke-virtual {v1, v0}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v1

    sput-object v1, Lorg/apache/pdfbox/pdfwriter/COSWriter;->EOF:[B

    const-string v1, "R"

    invoke-virtual {v1, v0}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v1

    sput-object v1, Lorg/apache/pdfbox/pdfwriter/COSWriter;->REFERENCE:[B

    const-string v1, "xref"

    invoke-virtual {v1, v0}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v1

    sput-object v1, Lorg/apache/pdfbox/pdfwriter/COSWriter;->XREF:[B

    const-string v1, "f"

    invoke-virtual {v1, v0}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v1

    sput-object v1, Lorg/apache/pdfbox/pdfwriter/COSWriter;->XREF_FREE:[B

    const-string v1, "n"

    invoke-virtual {v1, v0}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v1

    sput-object v1, Lorg/apache/pdfbox/pdfwriter/COSWriter;->XREF_USED:[B

    const-string v1, "trailer"

    invoke-virtual {v1, v0}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v1

    sput-object v1, Lorg/apache/pdfbox/pdfwriter/COSWriter;->TRAILER:[B

    const-string v1, "startxref"

    invoke-virtual {v1, v0}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v1

    sput-object v1, Lorg/apache/pdfbox/pdfwriter/COSWriter;->STARTXREF:[B

    const-string v1, "obj"

    invoke-virtual {v1, v0}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v1

    sput-object v1, Lorg/apache/pdfbox/pdfwriter/COSWriter;->OBJ:[B

    const-string v1, "endobj"

    invoke-virtual {v1, v0}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v1

    sput-object v1, Lorg/apache/pdfbox/pdfwriter/COSWriter;->ENDOBJ:[B

    const-string v1, "["

    invoke-virtual {v1, v0}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v1

    sput-object v1, Lorg/apache/pdfbox/pdfwriter/COSWriter;->ARRAY_OPEN:[B

    const-string v1, "]"

    invoke-virtual {v1, v0}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v1

    sput-object v1, Lorg/apache/pdfbox/pdfwriter/COSWriter;->ARRAY_CLOSE:[B

    const-string v1, "stream"

    invoke-virtual {v1, v0}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v1

    sput-object v1, Lorg/apache/pdfbox/pdfwriter/COSWriter;->STREAM:[B

    const-string v1, "endstream"

    invoke-virtual {v1, v0}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v0

    sput-object v0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->ENDSTREAM:[B

    return-void

    nop

    :array_0
    .array-data 1
        -0xat
        -0x1ct
        -0x4t
        -0x21t
    .end array-data
.end method

.method public constructor <init>(Ljava/io/OutputStream;)V
    .locals 3

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/text/DecimalFormat;

    const-string v1, "0000000000"

    invoke-direct {v0, v1}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->formatXrefOffset:Ljava/text/NumberFormat;

    new-instance v0, Ljava/text/DecimalFormat;

    const-string v1, "00000"

    invoke-direct {v0, v1}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->formatXrefGeneration:Ljava/text/NumberFormat;

    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {v0}, Ljava/text/NumberFormat;->getNumberInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->formatDecimal:Ljava/text/NumberFormat;

    const-wide/16 v1, 0x0

    iput-wide v1, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->startxref:J

    iput-wide v1, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->number:J

    new-instance v1, Ljava/util/Hashtable;

    invoke-direct {v1}, Ljava/util/Hashtable;-><init>()V

    iput-object v1, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->objectKeys:Ljava/util/Map;

    new-instance v1, Ljava/util/Hashtable;

    invoke-direct {v1}, Ljava/util/Hashtable;-><init>()V

    iput-object v1, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->keyObject:Ljava/util/Map;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->xRefEntries:Ljava/util/List;

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->objectsToWriteSet:Ljava/util/Set;

    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    iput-object v1, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->objectsToWrite:Ljava/util/Deque;

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->writtenObjects:Ljava/util/Set;

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->actualsAdded:Ljava/util/Set;

    const/4 v1, 0x0

    iput-object v1, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->currentObjectKey:Lorg/apache/pdfbox/cos/COSObjectKey;

    iput-object v1, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->pdDocument:Lorg/apache/pdfbox/pdmodel/PDDocument;

    iput-object v1, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->fdfDocument:Lorg/apache/pdfbox/pdmodel/fdf/FDFDocument;

    const/4 v1, 0x0

    iput-boolean v1, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->willEncrypt:Z

    iput-boolean v1, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->incrementalUpdate:Z

    iput-boolean v1, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->reachedSignature:Z

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->setOutput(Ljava/io/OutputStream;)V

    new-instance p1, Lorg/apache/pdfbox/pdfwriter/COSStandardOutputStream;

    iget-object v2, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->output:Ljava/io/OutputStream;

    invoke-direct {p1, v2}, Lorg/apache/pdfbox/pdfwriter/COSStandardOutputStream;-><init>(Ljava/io/OutputStream;)V

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->setStandardOutput(Lorg/apache/pdfbox/pdfwriter/COSStandardOutputStream;)V

    const/16 p1, 0xa

    invoke-virtual {v0, p1}, Ljava/text/NumberFormat;->setMaximumFractionDigits(I)V

    invoke-virtual {v0, v1}, Ljava/text/NumberFormat;->setGroupingUsed(Z)V

    return-void
.end method

.method public constructor <init>(Ljava/io/OutputStream;Ljava/io/InputStream;)V
    .locals 5

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/text/DecimalFormat;

    const-string v1, "0000000000"

    invoke-direct {v0, v1}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->formatXrefOffset:Ljava/text/NumberFormat;

    new-instance v0, Ljava/text/DecimalFormat;

    const-string v1, "00000"

    invoke-direct {v0, v1}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->formatXrefGeneration:Ljava/text/NumberFormat;

    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {v0}, Ljava/text/NumberFormat;->getNumberInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->formatDecimal:Ljava/text/NumberFormat;

    const-wide/16 v1, 0x0

    iput-wide v1, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->startxref:J

    iput-wide v1, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->number:J

    new-instance v1, Ljava/util/Hashtable;

    invoke-direct {v1}, Ljava/util/Hashtable;-><init>()V

    iput-object v1, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->objectKeys:Ljava/util/Map;

    new-instance v1, Ljava/util/Hashtable;

    invoke-direct {v1}, Ljava/util/Hashtable;-><init>()V

    iput-object v1, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->keyObject:Ljava/util/Map;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->xRefEntries:Ljava/util/List;

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->objectsToWriteSet:Ljava/util/Set;

    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    iput-object v1, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->objectsToWrite:Ljava/util/Deque;

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->writtenObjects:Ljava/util/Set;

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->actualsAdded:Ljava/util/Set;

    const/4 v1, 0x0

    iput-object v1, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->currentObjectKey:Lorg/apache/pdfbox/cos/COSObjectKey;

    iput-object v1, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->pdDocument:Lorg/apache/pdfbox/pdmodel/PDDocument;

    iput-object v1, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->fdfDocument:Lorg/apache/pdfbox/pdmodel/fdf/FDFDocument;

    const/4 v1, 0x0

    iput-boolean v1, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->willEncrypt:Z

    iput-boolean v1, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->incrementalUpdate:Z

    iput-boolean v1, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->reachedSignature:Z

    new-instance v2, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v2}, Ljava/io/ByteArrayOutputStream;-><init>()V

    invoke-direct {p0, v2}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->setOutput(Ljava/io/OutputStream;)V

    new-instance v2, Lorg/apache/pdfbox/pdfwriter/COSStandardOutputStream;

    iget-object v3, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->output:Ljava/io/OutputStream;

    invoke-virtual {p2}, Ljava/io/InputStream;->available()I

    move-result v4

    invoke-direct {v2, v3, v4}, Lorg/apache/pdfbox/pdfwriter/COSStandardOutputStream;-><init>(Ljava/io/OutputStream;I)V

    invoke-direct {p0, v2}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->setStandardOutput(Lorg/apache/pdfbox/pdfwriter/COSStandardOutputStream;)V

    iput-object p2, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->incrementalInput:Ljava/io/InputStream;

    iput-object p1, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->incrementalOutput:Ljava/io/OutputStream;

    const/4 p1, 0x1

    iput-boolean p1, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->incrementalUpdate:Z

    const/16 p1, 0xa

    invoke-virtual {v0, p1}, Ljava/text/NumberFormat;->setMaximumFractionDigits(I)V

    invoke-virtual {v0, v1}, Ljava/text/NumberFormat;->setGroupingUsed(Z)V

    return-void
.end method

.method private addObjectToWrite(Lorg/apache/pdfbox/cos/COSBase;)V
    .locals 3

    instance-of v0, p1, Lorg/apache/pdfbox/cos/COSObject;

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, Lorg/apache/pdfbox/cos/COSObject;

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSObject;->getObject()Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    goto :goto_0

    :cond_0
    move-object v0, p1

    :goto_0
    iget-object v1, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->writtenObjects:Ljava/util/Set;

    invoke-interface {v1, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    iget-object v1, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->objectsToWriteSet:Ljava/util/Set;

    invoke-interface {v1, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    iget-object v1, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->actualsAdded:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    iget-object v2, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->objectKeys:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/pdfbox/cos/COSObjectKey;

    goto :goto_1

    :cond_1
    move-object v2, v1

    :goto_1
    if-eqz v2, :cond_2

    iget-object v1, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->keyObject:Ljava/util/Map;

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/pdfbox/cos/COSBase;

    :cond_2
    if-eqz v0, :cond_3

    iget-object v2, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->objectKeys:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    instance-of v2, p1, Lorg/apache/pdfbox/cos/COSUpdateInfo;

    if-eqz v2, :cond_3

    move-object v2, p1

    check-cast v2, Lorg/apache/pdfbox/cos/COSUpdateInfo;

    invoke-interface {v2}, Lorg/apache/pdfbox/cos/COSUpdateInfo;->isNeedToBeUpdated()Z

    move-result v2

    if-nez v2, :cond_3

    instance-of v2, v1, Lorg/apache/pdfbox/cos/COSUpdateInfo;

    if-eqz v2, :cond_3

    check-cast v1, Lorg/apache/pdfbox/cos/COSUpdateInfo;

    invoke-interface {v1}, Lorg/apache/pdfbox/cos/COSUpdateInfo;->isNeedToBeUpdated()Z

    move-result v1

    if-nez v1, :cond_3

    return-void

    :cond_3
    iget-object v1, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->objectsToWrite:Ljava/util/Deque;

    invoke-interface {v1, p1}, Ljava/util/Deque;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->objectsToWriteSet:Ljava/util/Set;

    invoke-interface {v1, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    if-eqz v0, :cond_4

    iget-object p1, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->actualsAdded:Ljava/util/Set;

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_4
    return-void
.end method

.method private doWriteSignature()V
    .locals 12

    iget-wide v0, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->signatureOffset:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_5

    iget-wide v0, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->byteRangeOffset:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    goto/16 :goto_2

    :cond_0
    iget-object v0, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->incrementalInput:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->available()I

    move-result v0

    int-to-long v0, v0

    iget-wide v4, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->signatureOffset:J

    iget-wide v6, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->signatureLength:J

    add-long/2addr v6, v4

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->getStandardOutput()Lorg/apache/pdfbox/pdfwriter/COSStandardOutputStream;

    move-result-object v8

    invoke-virtual {v8}, Lorg/apache/pdfbox/pdfwriter/COSStandardOutputStream;->getPos()J

    move-result-wide v8

    iget-wide v10, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->signatureLength:J

    add-long/2addr v10, v0

    sub-long/2addr v8, v10

    iget-wide v10, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->signatureOffset:J

    sub-long/2addr v10, v0

    sub-long/2addr v8, v10

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "0 "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v10, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v4, " "

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v10, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v10, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v4, "]"

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    iget-wide v5, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->byteRangeLength:J

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v7

    int-to-long v7, v7

    sub-long/2addr v5, v7

    cmp-long v2, v5, v2

    if-ltz v2, :cond_4

    iget-object v2, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->output:Ljava/io/OutputStream;

    check-cast v2, Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v2}, Ljava/io/OutputStream;->flush()V

    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v2

    invoke-virtual {v4}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    const/4 v4, 0x0

    move v5, v4

    :goto_0
    int-to-long v6, v5

    iget-wide v8, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->byteRangeLength:J

    cmp-long v8, v6, v8

    if-gez v8, :cond_2

    array-length v8, v3

    if-lt v5, v8, :cond_1

    iget-wide v8, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->byteRangeOffset:J

    add-long/2addr v8, v6

    sub-long/2addr v8, v0

    long-to-int v6, v8

    const/16 v7, 0x20

    aput-byte v7, v2, v6

    goto :goto_1

    :cond_1
    iget-wide v8, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->byteRangeOffset:J

    add-long/2addr v8, v6

    sub-long/2addr v8, v0

    long-to-int v6, v8

    aget-byte v7, v3, v5

    aput-byte v7, v2, v6

    :goto_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    :cond_2
    iget-object v3, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->incrementalInput:Ljava/io/InputStream;

    invoke-static {v3}, Lorg/apache/pdfbox/io/IOUtils;->toByteArray(Ljava/io/InputStream;)[B

    move-result-object v3

    array-length v5, v2

    iget-wide v6, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->signatureLength:J

    long-to-int v6, v6

    sub-int/2addr v5, v6

    new-array v5, v5, [B

    iget-wide v6, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->signatureOffset:J

    sub-long/2addr v6, v0

    long-to-int v0, v6

    invoke-static {v2, v4, v5, v4, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-wide v6, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->signatureLength:J

    long-to-int v1, v6

    add-int/2addr v1, v0

    array-length v8, v2

    sub-int/2addr v8, v0

    long-to-int v6, v6

    sub-int/2addr v8, v6

    invoke-static {v2, v1, v5, v0, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    new-instance v1, Ljava/io/SequenceInputStream;

    new-instance v6, Ljava/io/ByteArrayInputStream;

    invoke-direct {v6, v3}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    new-instance v7, Ljava/io/ByteArrayInputStream;

    invoke-direct {v7, v5}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v1, v6, v7}, Ljava/io/SequenceInputStream;-><init>(Ljava/io/InputStream;Ljava/io/InputStream;)V

    iget-object v5, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->signatureInterface:Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/SignatureInterface;

    invoke-interface {v5, v1}, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/SignatureInterface;->sign(Ljava/io/InputStream;)[B

    move-result-object v1

    new-instance v5, Lorg/apache/pdfbox/cos/COSString;

    invoke-direct {v5, v1}, Lorg/apache/pdfbox/cos/COSString;-><init>([B)V

    invoke-virtual {v5}, Lorg/apache/pdfbox/cos/COSString;->toHexString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v5

    int-to-long v5, v5

    iget-wide v7, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->signatureLength:J

    const-wide/16 v9, 0x2

    sub-long/2addr v7, v9

    cmp-long v5, v5, v7

    if-gtz v5, :cond_3

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    add-int/lit8 v0, v0, 0x1

    array-length v5, v1

    invoke-static {v1, v4, v2, v0, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v0, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->incrementalOutput:Ljava/io/OutputStream;

    invoke-virtual {v0, v3}, Ljava/io/OutputStream;->write([B)V

    iget-object v0, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->incrementalOutput:Ljava/io/OutputStream;

    invoke-virtual {v0, v2}, Ljava/io/OutputStream;->write([B)V

    return-void

    :cond_3
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Can\'t write signature, not enough space"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Can\'t write new ByteRange, not enough space"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_5
    :goto_2
    return-void
.end method

.method private doWriteXRefInc(Lorg/apache/pdfbox/cos/COSDocument;J)V
    .locals 7

    invoke-virtual {p1}, Lorg/apache/pdfbox/cos/COSDocument;->isXRefStream()Z

    move-result v0

    const-wide/16 v1, -0x1

    if-nez v0, :cond_0

    cmp-long v0, p2, v1

    if-eqz v0, :cond_2

    :cond_0
    new-instance v0, Lorg/apache/pdfbox/pdfparser/PDFXRefStream;

    invoke-direct {v0}, Lorg/apache/pdfbox/pdfparser/PDFXRefStream;-><init>()V

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->getXRefEntries()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/pdfbox/pdfwriter/COSWriterXRefEntry;

    invoke-virtual {v0, v4}, Lorg/apache/pdfbox/pdfparser/PDFXRefStream;->addEntry(Lorg/apache/pdfbox/pdfwriter/COSWriterXRefEntry;)V

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Lorg/apache/pdfbox/cos/COSDocument;->getTrailer()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v3

    sget-object v4, Lorg/apache/pdfbox/cos/COSName;->PREV:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v3, v4}, Lorg/apache/pdfbox/cos/COSDictionary;->removeItem(Lorg/apache/pdfbox/cos/COSName;)V

    invoke-virtual {v0, v3}, Lorg/apache/pdfbox/pdfparser/PDFXRefStream;->addTrailerInfo(Lorg/apache/pdfbox/cos/COSDictionary;)V

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->getNumber()J

    move-result-wide v3

    const-wide/16 v5, 0x2

    add-long/2addr v3, v5

    invoke-virtual {v0, v3, v4}, Lorg/apache/pdfbox/pdfparser/PDFXRefStream;->setSize(J)V

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->getStandardOutput()Lorg/apache/pdfbox/pdfwriter/COSStandardOutputStream;

    move-result-object v3

    invoke-virtual {v3}, Lorg/apache/pdfbox/pdfwriter/COSStandardOutputStream;->getPos()J

    move-result-wide v3

    invoke-virtual {p0, v3, v4}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->setStartxref(J)V

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdfparser/PDFXRefStream;->getStream()Lorg/apache/pdfbox/cos/COSStream;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->doWriteObject(Lorg/apache/pdfbox/cos/COSBase;)V

    :cond_2
    invoke-virtual {p1}, Lorg/apache/pdfbox/cos/COSDocument;->isXRefStream()Z

    move-result v0

    if-eqz v0, :cond_3

    cmp-long v0, p2, v1

    if-eqz v0, :cond_5

    :cond_3
    invoke-virtual {p1}, Lorg/apache/pdfbox/cos/COSDocument;->getTrailer()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v3, Lorg/apache/pdfbox/cos/COSName;->PREV:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p1}, Lorg/apache/pdfbox/cos/COSDocument;->getStartXref()J

    move-result-wide v4

    invoke-virtual {v0, v3, v4, v5}, Lorg/apache/pdfbox/cos/COSDictionary;->setLong(Lorg/apache/pdfbox/cos/COSName;J)V

    cmp-long p2, p2, v1

    if-eqz p2, :cond_4

    sget-object p2, Lorg/apache/pdfbox/cos/COSName;->XREF_STM:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, p2}, Lorg/apache/pdfbox/cos/COSDictionary;->removeItem(Lorg/apache/pdfbox/cos/COSName;)V

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->getStartxref()J

    move-result-wide v1

    invoke-virtual {v0, p2, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->setLong(Lorg/apache/pdfbox/cos/COSName;J)V

    :cond_4
    invoke-direct {p0}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->doWriteXRefTable()V

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->doWriteTrailer(Lorg/apache/pdfbox/cos/COSDocument;)V

    :cond_5
    return-void
.end method

.method private doWriteXRefTable()V
    .locals 12

    invoke-static {}, Lorg/apache/pdfbox/pdfwriter/COSWriterXRefEntry;->getNullEntry()Lorg/apache/pdfbox/pdfwriter/COSWriterXRefEntry;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->addXRefEntry(Lorg/apache/pdfbox/pdfwriter/COSWriterXRefEntry;)V

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->getXRefEntries()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->getStandardOutput()Lorg/apache/pdfbox/pdfwriter/COSStandardOutputStream;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdfwriter/COSStandardOutputStream;->getPos()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->setStartxref(J)V

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->getStandardOutput()Lorg/apache/pdfbox/pdfwriter/COSStandardOutputStream;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/pdfwriter/COSWriter;->XREF:[B

    invoke-virtual {v0, v1}, Ljava/io/OutputStream;->write([B)V

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->getStandardOutput()Lorg/apache/pdfbox/pdfwriter/COSStandardOutputStream;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdfwriter/COSStandardOutputStream;->writeEOL()V

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->getXRefEntries()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->getXRefRanges(Ljava/util/List;)[Ljava/lang/Long;

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    move v3, v2

    move v4, v3

    :goto_0
    if-ge v3, v1, :cond_1

    rem-int/lit8 v5, v1, 0x2

    if-nez v5, :cond_1

    aget-object v5, v0, v3

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    add-int/lit8 v7, v3, 0x1

    aget-object v8, v0, v7

    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-direct {p0, v5, v6, v8, v9}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->writeXrefRange(JJ)V

    move v5, v2

    :goto_1
    int-to-long v8, v5

    aget-object v6, v0, v7

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    cmp-long v6, v8, v10

    if-gez v6, :cond_0

    iget-object v6, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->xRefEntries:Ljava/util/List;

    add-int/lit8 v8, v4, 0x1

    invoke-interface {v6, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/pdfbox/pdfwriter/COSWriterXRefEntry;

    invoke-direct {p0, v4}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->writeXrefEntry(Lorg/apache/pdfbox/pdfwriter/COSWriterXRefEntry;)V

    add-int/lit8 v5, v5, 0x1

    move v4, v8

    goto :goto_1

    :cond_0
    add-int/lit8 v3, v3, 0x2

    goto :goto_0

    :cond_1
    return-void
.end method

.method private getObjectKey(Lorg/apache/pdfbox/cos/COSBase;)Lorg/apache/pdfbox/cos/COSObjectKey;
    .locals 5

    instance-of v0, p1, Lorg/apache/pdfbox/cos/COSObject;

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, Lorg/apache/pdfbox/cos/COSObject;

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSObject;->getObject()Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    goto :goto_0

    :cond_0
    move-object v0, p1

    :goto_0
    if-eqz v0, :cond_1

    iget-object v1, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->objectKeys:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/pdfbox/cos/COSObjectKey;

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    if-nez v1, :cond_2

    iget-object v1, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->objectKeys:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/pdfbox/cos/COSObjectKey;

    :cond_2
    if-nez v1, :cond_3

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->getNumber()J

    move-result-wide v1

    const-wide/16 v3, 0x1

    add-long/2addr v1, v3

    invoke-virtual {p0, v1, v2}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->setNumber(J)V

    new-instance v1, Lorg/apache/pdfbox/cos/COSObjectKey;

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->getNumber()J

    move-result-wide v2

    const/4 v4, 0x0

    invoke-direct {v1, v2, v3, v4}, Lorg/apache/pdfbox/cos/COSObjectKey;-><init>(JI)V

    iget-object v2, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->objectKeys:Ljava/util/Map;

    invoke-interface {v2, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    if-eqz v0, :cond_3

    iget-object p1, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->objectKeys:Ljava/util/Map;

    invoke-interface {p1, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_3
    return-object v1
.end method

.method public static getXRefRanges(Ljava/util/List;)[Ljava/lang/Long;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lorg/apache/pdfbox/pdfwriter/COSWriterXRefEntry;",
            ">;)[",
            "Ljava/lang/Long;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    const-wide/16 v2, -0x2

    const-wide/16 v4, 0x1

    move-wide v6, v2

    move-wide v8, v4

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lorg/apache/pdfbox/pdfwriter/COSWriterXRefEntry;

    invoke-virtual {v10}, Lorg/apache/pdfbox/pdfwriter/COSWriterXRefEntry;->getKey()Lorg/apache/pdfbox/cos/COSObjectKey;

    move-result-object v10

    invoke-virtual {v10}, Lorg/apache/pdfbox/cos/COSObjectKey;->getNumber()J

    move-result-wide v10

    long-to-int v10, v10

    int-to-long v10, v10

    add-long v12, v6, v4

    cmp-long v12, v10, v12

    if-nez v12, :cond_0

    add-long/2addr v8, v4

    goto :goto_1

    :cond_0
    cmp-long v12, v6, v2

    if-nez v12, :cond_1

    goto :goto_1

    :cond_1
    sub-long/2addr v6, v8

    add-long/2addr v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-wide v8, v4

    :goto_1
    move-wide v6, v10

    goto :goto_0

    :cond_2
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result p0

    if-lez p0, :cond_3

    sub-long/2addr v6, v8

    add-long/2addr v6, v4

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p0

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p0

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_3
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result p0

    new-array p0, p0, [Ljava/lang/Long;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object p0

    check-cast p0, [Ljava/lang/Long;

    return-object p0
.end method

.method private prepareIncrement(Lorg/apache/pdfbox/pdmodel/PDDocument;)V
    .locals 6

    if-eqz p1, :cond_3

    :try_start_0
    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/PDDocument;->getDocument()Lorg/apache/pdfbox/cos/COSDocument;

    move-result-object p1

    invoke-virtual {p1}, Lorg/apache/pdfbox/cos/COSDocument;->getXrefTable()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const-wide/16 v1, 0x0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/pdfbox/cos/COSObjectKey;

    invoke-virtual {p1, v3}, Lorg/apache/pdfbox/cos/COSDocument;->getObjectFromPool(Lorg/apache/pdfbox/cos/COSObjectKey;)Lorg/apache/pdfbox/cos/COSObject;

    move-result-object v4

    invoke-virtual {v4}, Lorg/apache/pdfbox/cos/COSObject;->getObject()Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v4

    if-eqz v4, :cond_1

    if-eqz v3, :cond_1

    instance-of v5, v4, Lorg/apache/pdfbox/cos/COSNumber;

    if-nez v5, :cond_1

    iget-object v5, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->objectKeys:Ljava/util/Map;

    invoke-interface {v5, v4, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v5, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->keyObject:Ljava/util/Map;

    invoke-interface {v5, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Lorg/apache/pdfbox/cos/COSObjectKey;->getNumber()J

    move-result-wide v3

    cmp-long v5, v3, v1

    if-lez v5, :cond_0

    move-wide v1, v3

    goto :goto_0

    :cond_2
    invoke-virtual {p0, v1, v2}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->setNumber(J)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception p1

    const-string v0, "PdfBoxAndroid"

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_3
    :goto_1
    return-void
.end method

.method private setOutput(Ljava/io/OutputStream;)V
    .locals 0

    iput-object p1, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->output:Ljava/io/OutputStream;

    return-void
.end method

.method private setStandardOutput(Lorg/apache/pdfbox/pdfwriter/COSStandardOutputStream;)V
    .locals 0

    iput-object p1, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->standardOutput:Lorg/apache/pdfbox/pdfwriter/COSStandardOutputStream;

    return-void
.end method

.method public static writeString(Lorg/apache/pdfbox/cos/COSString;Ljava/io/OutputStream;)V
    .locals 1

    .line 1
    invoke-virtual {p0}, Lorg/apache/pdfbox/cos/COSString;->getBytes()[B

    move-result-object v0

    invoke-virtual {p0}, Lorg/apache/pdfbox/cos/COSString;->getForceHexForm()Z

    move-result p0

    invoke-static {v0, p0, p1}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->writeString([BZLjava/io/OutputStream;)V

    return-void
.end method

.method public static writeString([BLjava/io/OutputStream;)V
    .locals 1

    .line 2
    const/4 v0, 0x0

    invoke-static {p0, v0, p1}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->writeString([BZLjava/io/OutputStream;)V

    return-void
.end method

.method private static writeString([BZLjava/io/OutputStream;)V
    .locals 5

    .line 3
    array-length v0, p0

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, v0, :cond_1

    aget-byte v3, p0, v2

    if-gez v3, :cond_0

    move v0, v1

    goto :goto_1

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_4

    if-nez p1, :cond_4

    const/16 p1, 0x28

    invoke-virtual {p2, p1}, Ljava/io/OutputStream;->write(I)V

    array-length v0, p0

    :goto_2
    const/16 v2, 0x29

    if-ge v1, v0, :cond_3

    aget-byte v3, p0, v1

    const/16 v4, 0x5c

    if-eq v3, p1, :cond_2

    if-eq v3, v2, :cond_2

    if-eq v3, v4, :cond_2

    goto :goto_3

    :cond_2
    invoke-virtual {p2, v4}, Ljava/io/OutputStream;->write(I)V

    :goto_3
    invoke-virtual {p2, v3}, Ljava/io/OutputStream;->write(I)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_3
    invoke-virtual {p2, v2}, Ljava/io/OutputStream;->write(I)V

    goto :goto_5

    :cond_4
    const/16 p1, 0x3c

    invoke-virtual {p2, p1}, Ljava/io/OutputStream;->write(I)V

    array-length p1, p0

    :goto_4
    if-ge v1, p1, :cond_5

    aget-byte v0, p0, v1

    invoke-static {v0}, Lorg/apache/pdfbox/util/Hex;->getBytes(B)[B

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/OutputStream;->write([B)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    :cond_5
    const/16 p0, 0x3e

    invoke-virtual {p2, p0}, Ljava/io/OutputStream;->write(I)V

    :goto_5
    return-void
.end method

.method private writeXrefEntry(Lorg/apache/pdfbox/pdfwriter/COSWriterXRefEntry;)V
    .locals 4

    iget-object v0, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->formatXrefOffset:Ljava/text/NumberFormat;

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdfwriter/COSWriterXRefEntry;->getOffset()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->formatXrefGeneration:Ljava/text/NumberFormat;

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdfwriter/COSWriterXRefEntry;->getKey()Lorg/apache/pdfbox/cos/COSObjectKey;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/pdfbox/cos/COSObjectKey;->getGeneration()I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {v1, v2, v3}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->getStandardOutput()Lorg/apache/pdfbox/pdfwriter/COSStandardOutputStream;

    move-result-object v2

    sget-object v3, Lorg/apache/pdfbox/util/Charsets;->ISO_8859_1:Ljava/nio/charset/Charset;

    invoke-virtual {v0, v3}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/io/OutputStream;->write([B)V

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->getStandardOutput()Lorg/apache/pdfbox/pdfwriter/COSStandardOutputStream;

    move-result-object v0

    sget-object v2, Lorg/apache/pdfbox/pdfwriter/COSWriter;->SPACE:[B

    invoke-virtual {v0, v2}, Ljava/io/OutputStream;->write([B)V

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->getStandardOutput()Lorg/apache/pdfbox/pdfwriter/COSStandardOutputStream;

    move-result-object v0

    invoke-virtual {v1, v3}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/OutputStream;->write([B)V

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->getStandardOutput()Lorg/apache/pdfbox/pdfwriter/COSStandardOutputStream;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/io/OutputStream;->write([B)V

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->getStandardOutput()Lorg/apache/pdfbox/pdfwriter/COSStandardOutputStream;

    move-result-object v0

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdfwriter/COSWriterXRefEntry;->isFree()Z

    move-result p1

    if-eqz p1, :cond_0

    sget-object p1, Lorg/apache/pdfbox/pdfwriter/COSWriter;->XREF_FREE:[B

    goto :goto_0

    :cond_0
    sget-object p1, Lorg/apache/pdfbox/pdfwriter/COSWriter;->XREF_USED:[B

    :goto_0
    invoke-virtual {v0, p1}, Ljava/io/OutputStream;->write([B)V

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->getStandardOutput()Lorg/apache/pdfbox/pdfwriter/COSStandardOutputStream;

    move-result-object p1

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdfwriter/COSStandardOutputStream;->writeCRLF()V

    return-void
.end method

.method private writeXrefRange(JJ)V
    .locals 1

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->getStandardOutput()Lorg/apache/pdfbox/pdfwriter/COSStandardOutputStream;

    move-result-object v0

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/io/OutputStream;->write([B)V

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->getStandardOutput()Lorg/apache/pdfbox/pdfwriter/COSStandardOutputStream;

    move-result-object p1

    sget-object p2, Lorg/apache/pdfbox/pdfwriter/COSWriter;->SPACE:[B

    invoke-virtual {p1, p2}, Ljava/io/OutputStream;->write([B)V

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->getStandardOutput()Lorg/apache/pdfbox/pdfwriter/COSStandardOutputStream;

    move-result-object p1

    invoke-static {p3, p4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p2}, Ljava/lang/String;->getBytes()[B

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/io/OutputStream;->write([B)V

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->getStandardOutput()Lorg/apache/pdfbox/pdfwriter/COSStandardOutputStream;

    move-result-object p1

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdfwriter/COSStandardOutputStream;->writeEOL()V

    return-void
.end method


# virtual methods
.method public addXRefEntry(Lorg/apache/pdfbox/pdfwriter/COSWriterXRefEntry;)V
    .locals 1

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->getXRefEntries()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public close()V
    .locals 1

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->getStandardOutput()Lorg/apache/pdfbox/pdfwriter/COSStandardOutputStream;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->getStandardOutput()Lorg/apache/pdfbox/pdfwriter/COSStandardOutputStream;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V

    :cond_0
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->getOutput()Ljava/io/OutputStream;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->getOutput()Ljava/io/OutputStream;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V

    :cond_1
    iget-object v0, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->incrementalOutput:Ljava/io/OutputStream;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V

    :cond_2
    return-void
.end method

.method public doWriteBody(Lorg/apache/pdfbox/cos/COSDocument;)V
    .locals 3

    invoke-virtual {p1}, Lorg/apache/pdfbox/cos/COSDocument;->getTrailer()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object p1

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->ROOT:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p1, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->INFO:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p1, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v1

    check-cast v1, Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v2, Lorg/apache/pdfbox/cos/COSName;->ENCRYPT:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object p1

    check-cast p1, Lorg/apache/pdfbox/cos/COSDictionary;

    if-eqz v0, :cond_0

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->addObjectToWrite(Lorg/apache/pdfbox/cos/COSBase;)V

    :cond_0
    if-eqz v1, :cond_1

    invoke-direct {p0, v1}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->addObjectToWrite(Lorg/apache/pdfbox/cos/COSBase;)V

    :cond_1
    :goto_0
    iget-object v0, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->objectsToWrite:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->size()I

    move-result v0

    if-lez v0, :cond_2

    iget-object v0, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->objectsToWrite:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->removeFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSBase;

    iget-object v1, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->objectsToWriteSet:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    invoke-virtual {p0, v0}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->doWriteObject(Lorg/apache/pdfbox/cos/COSBase;)V

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->willEncrypt:Z

    if-eqz p1, :cond_3

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->addObjectToWrite(Lorg/apache/pdfbox/cos/COSBase;)V

    :cond_3
    :goto_1
    iget-object p1, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->objectsToWrite:Ljava/util/Deque;

    invoke-interface {p1}, Ljava/util/Deque;->size()I

    move-result p1

    if-lez p1, :cond_4

    iget-object p1, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->objectsToWrite:Ljava/util/Deque;

    invoke-interface {p1}, Ljava/util/Deque;->removeFirst()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lorg/apache/pdfbox/cos/COSBase;

    iget-object v0, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->objectsToWriteSet:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->doWriteObject(Lorg/apache/pdfbox/cos/COSBase;)V

    goto :goto_1

    :cond_4
    return-void
.end method

.method public doWriteHeader(Lorg/apache/pdfbox/cos/COSDocument;)V
    .locals 2

    iget-object p1, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->fdfDocument:Lorg/apache/pdfbox/pdmodel/fdf/FDFDocument;

    if-eqz p1, :cond_0

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "%FDF-"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->fdfDocument:Lorg/apache/pdfbox/pdmodel/fdf/FDFDocument;

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/fdf/FDFDocument;->getDocument()Lorg/apache/pdfbox/cos/COSDocument;

    move-result-object v0

    goto :goto_0

    :cond_0
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "%PDF-"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->pdDocument:Lorg/apache/pdfbox/pdmodel/PDDocument;

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/PDDocument;->getDocument()Lorg/apache/pdfbox/cos/COSDocument;

    move-result-object v0

    :goto_0
    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSDocument;->getVersion()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->getStandardOutput()Lorg/apache/pdfbox/pdfwriter/COSStandardOutputStream;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/util/Charsets;->ISO_8859_1:Ljava/nio/charset/Charset;

    invoke-virtual {p1, v1}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/io/OutputStream;->write([B)V

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->getStandardOutput()Lorg/apache/pdfbox/pdfwriter/COSStandardOutputStream;

    move-result-object p1

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdfwriter/COSStandardOutputStream;->writeEOL()V

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->getStandardOutput()Lorg/apache/pdfbox/pdfwriter/COSStandardOutputStream;

    move-result-object p1

    sget-object v0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->COMMENT:[B

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->getStandardOutput()Lorg/apache/pdfbox/pdfwriter/COSStandardOutputStream;

    move-result-object p1

    sget-object v0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->GARBAGE:[B

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->getStandardOutput()Lorg/apache/pdfbox/pdfwriter/COSStandardOutputStream;

    move-result-object p1

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdfwriter/COSStandardOutputStream;->writeEOL()V

    return-void
.end method

.method public doWriteObject(Lorg/apache/pdfbox/cos/COSBase;)V
    .locals 4

    iget-object v0, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->writtenObjects:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    instance-of v0, p1, Lorg/apache/pdfbox/cos/COSDictionary;

    if-eqz v0, :cond_1

    move-object v0, p1

    check-cast v0, Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->TYPE:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getItem(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    instance-of v1, v0, Lorg/apache/pdfbox/cos/COSName;

    if-eqz v1, :cond_1

    check-cast v0, Lorg/apache/pdfbox/cos/COSName;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->SIG:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v1, v0}, Lorg/apache/pdfbox/cos/COSName;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->DOC_TIME_STAMP:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v1, v0}, Lorg/apache/pdfbox/cos/COSName;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->reachedSignature:Z

    :cond_1
    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->getObjectKey(Lorg/apache/pdfbox/cos/COSBase;)Lorg/apache/pdfbox/cos/COSObjectKey;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->currentObjectKey:Lorg/apache/pdfbox/cos/COSObjectKey;

    new-instance v0, Lorg/apache/pdfbox/pdfwriter/COSWriterXRefEntry;

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->getStandardOutput()Lorg/apache/pdfbox/pdfwriter/COSStandardOutputStream;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/pdfbox/pdfwriter/COSStandardOutputStream;->getPos()J

    move-result-wide v1

    iget-object v3, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->currentObjectKey:Lorg/apache/pdfbox/cos/COSObjectKey;

    invoke-direct {v0, v1, v2, p1, v3}, Lorg/apache/pdfbox/pdfwriter/COSWriterXRefEntry;-><init>(JLorg/apache/pdfbox/cos/COSBase;Lorg/apache/pdfbox/cos/COSObjectKey;)V

    invoke-virtual {p0, v0}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->addXRefEntry(Lorg/apache/pdfbox/pdfwriter/COSWriterXRefEntry;)V

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->getStandardOutput()Lorg/apache/pdfbox/pdfwriter/COSStandardOutputStream;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->currentObjectKey:Lorg/apache/pdfbox/cos/COSObjectKey;

    invoke-virtual {v1}, Lorg/apache/pdfbox/cos/COSObjectKey;->getNumber()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lorg/apache/pdfbox/util/Charsets;->ISO_8859_1:Ljava/nio/charset/Charset;

    invoke-virtual {v1, v2}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/OutputStream;->write([B)V

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->getStandardOutput()Lorg/apache/pdfbox/pdfwriter/COSStandardOutputStream;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/pdfwriter/COSWriter;->SPACE:[B

    invoke-virtual {v0, v1}, Ljava/io/OutputStream;->write([B)V

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->getStandardOutput()Lorg/apache/pdfbox/pdfwriter/COSStandardOutputStream;

    move-result-object v0

    iget-object v3, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->currentObjectKey:Lorg/apache/pdfbox/cos/COSObjectKey;

    invoke-virtual {v3}, Lorg/apache/pdfbox/cos/COSObjectKey;->getGeneration()I

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/io/OutputStream;->write([B)V

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->getStandardOutput()Lorg/apache/pdfbox/pdfwriter/COSStandardOutputStream;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/io/OutputStream;->write([B)V

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->getStandardOutput()Lorg/apache/pdfbox/pdfwriter/COSStandardOutputStream;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/pdfwriter/COSWriter;->OBJ:[B

    invoke-virtual {v0, v1}, Ljava/io/OutputStream;->write([B)V

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->getStandardOutput()Lorg/apache/pdfbox/pdfwriter/COSStandardOutputStream;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdfwriter/COSStandardOutputStream;->writeEOL()V

    invoke-virtual {p1, p0}, Lorg/apache/pdfbox/cos/COSBase;->accept(Lorg/apache/pdfbox/cos/ICOSVisitor;)Ljava/lang/Object;

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->getStandardOutput()Lorg/apache/pdfbox/pdfwriter/COSStandardOutputStream;

    move-result-object p1

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdfwriter/COSStandardOutputStream;->writeEOL()V

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->getStandardOutput()Lorg/apache/pdfbox/pdfwriter/COSStandardOutputStream;

    move-result-object p1

    sget-object v0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->ENDOBJ:[B

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->getStandardOutput()Lorg/apache/pdfbox/pdfwriter/COSStandardOutputStream;

    move-result-object p1

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdfwriter/COSStandardOutputStream;->writeEOL()V

    return-void
.end method

.method public doWriteTrailer(Lorg/apache/pdfbox/cos/COSDocument;)V
    .locals 7

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->getStandardOutput()Lorg/apache/pdfbox/pdfwriter/COSStandardOutputStream;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/pdfwriter/COSWriter;->TRAILER:[B

    invoke-virtual {v0, v1}, Ljava/io/OutputStream;->write([B)V

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->getStandardOutput()Lorg/apache/pdfbox/pdfwriter/COSStandardOutputStream;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdfwriter/COSStandardOutputStream;->writeEOL()V

    invoke-virtual {p1}, Lorg/apache/pdfbox/cos/COSDocument;->getTrailer()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->getXRefEntries()Ljava/util/List;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->getXRefEntries()Ljava/util/List;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->getXRefEntries()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/pdfbox/pdfwriter/COSWriterXRefEntry;

    sget-object v2, Lorg/apache/pdfbox/cos/COSName;->SIZE:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v1}, Lorg/apache/pdfbox/pdfwriter/COSWriterXRefEntry;->getKey()Lorg/apache/pdfbox/cos/COSObjectKey;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/pdfbox/cos/COSObjectKey;->getNumber()J

    move-result-wide v3

    const-wide/16 v5, 0x1

    add-long/2addr v3, v5

    invoke-virtual {v0, v2, v3, v4}, Lorg/apache/pdfbox/cos/COSDictionary;->setLong(Lorg/apache/pdfbox/cos/COSName;J)V

    iget-boolean v1, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->incrementalUpdate:Z

    if-nez v1, :cond_0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->PREV:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->removeItem(Lorg/apache/pdfbox/cos/COSName;)V

    :cond_0
    invoke-virtual {p1}, Lorg/apache/pdfbox/cos/COSDocument;->isXRefStream()Z

    move-result p1

    if-nez p1, :cond_1

    sget-object p1, Lorg/apache/pdfbox/cos/COSName;->XREF_STM:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->removeItem(Lorg/apache/pdfbox/cos/COSName;)V

    :cond_1
    sget-object p1, Lorg/apache/pdfbox/cos/COSName;->DOC_CHECKSUM:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->removeItem(Lorg/apache/pdfbox/cos/COSName;)V

    invoke-virtual {v0, p0}, Lorg/apache/pdfbox/cos/COSDictionary;->accept(Lorg/apache/pdfbox/cos/ICOSVisitor;)Ljava/lang/Object;

    return-void
.end method

.method public getNumber()J
    .locals 2

    iget-wide v0, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->number:J

    return-wide v0
.end method

.method public getObjectKeys()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Lorg/apache/pdfbox/cos/COSBase;",
            "Lorg/apache/pdfbox/cos/COSObjectKey;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->objectKeys:Ljava/util/Map;

    return-object v0
.end method

.method public getOutput()Ljava/io/OutputStream;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->output:Ljava/io/OutputStream;

    return-object v0
.end method

.method public getStandardOutput()Lorg/apache/pdfbox/pdfwriter/COSStandardOutputStream;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->standardOutput:Lorg/apache/pdfbox/pdfwriter/COSStandardOutputStream;

    return-object v0
.end method

.method public getStartxref()J
    .locals 2

    iget-wide v0, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->startxref:J

    return-wide v0
.end method

.method public getXRefEntries()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lorg/apache/pdfbox/pdfwriter/COSWriterXRefEntry;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->xRefEntries:Ljava/util/List;

    return-object v0
.end method

.method public setNumber(J)V
    .locals 0

    iput-wide p1, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->number:J

    return-void
.end method

.method public setStartxref(J)V
    .locals 0

    iput-wide p1, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->startxref:J

    return-void
.end method

.method public visitFromArray(Lorg/apache/pdfbox/cos/COSArray;)Ljava/lang/Object;
    .locals 4

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->getStandardOutput()Lorg/apache/pdfbox/pdfwriter/COSStandardOutputStream;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/pdfwriter/COSWriter;->ARRAY_OPEN:[B

    invoke-virtual {v0, v1}, Ljava/io/OutputStream;->write([B)V

    invoke-virtual {p1}, Lorg/apache/pdfbox/cos/COSArray;->iterator()Ljava/util/Iterator;

    move-result-object p1

    const/4 v0, 0x0

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/pdfbox/cos/COSBase;

    instance-of v2, v1, Lorg/apache/pdfbox/cos/COSDictionary;

    if-eqz v2, :cond_2

    invoke-virtual {v1}, Lorg/apache/pdfbox/cos/COSBase;->isDirect()Z

    move-result v2

    if-eqz v2, :cond_1

    check-cast v1, Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-virtual {p0, v1}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->visitFromDictionary(Lorg/apache/pdfbox/cos/COSDictionary;)Ljava/lang/Object;

    goto :goto_2

    :cond_1
    :goto_1
    invoke-direct {p0, v1}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->addObjectToWrite(Lorg/apache/pdfbox/cos/COSBase;)V

    invoke-virtual {p0, v1}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->writeReference(Lorg/apache/pdfbox/cos/COSBase;)V

    goto :goto_2

    :cond_2
    instance-of v2, v1, Lorg/apache/pdfbox/cos/COSObject;

    if-eqz v2, :cond_4

    move-object v2, v1

    check-cast v2, Lorg/apache/pdfbox/cos/COSObject;

    invoke-virtual {v2}, Lorg/apache/pdfbox/cos/COSObject;->getObject()Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v2

    instance-of v3, v2, Lorg/apache/pdfbox/cos/COSDictionary;

    if-nez v3, :cond_1

    if-nez v2, :cond_3

    goto :goto_1

    :cond_3
    invoke-virtual {v2, p0}, Lorg/apache/pdfbox/cos/COSBase;->accept(Lorg/apache/pdfbox/cos/ICOSVisitor;)Ljava/lang/Object;

    goto :goto_2

    :cond_4
    if-nez v1, :cond_5

    sget-object v1, Lorg/apache/pdfbox/cos/COSNull;->NULL:Lorg/apache/pdfbox/cos/COSNull;

    invoke-virtual {v1, p0}, Lorg/apache/pdfbox/cos/COSNull;->accept(Lorg/apache/pdfbox/cos/ICOSVisitor;)Ljava/lang/Object;

    goto :goto_2

    :cond_5
    invoke-virtual {v1, p0}, Lorg/apache/pdfbox/cos/COSBase;->accept(Lorg/apache/pdfbox/cos/ICOSVisitor;)Ljava/lang/Object;

    :goto_2
    add-int/lit8 v0, v0, 0x1

    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    rem-int/lit8 v1, v0, 0xa

    if-nez v1, :cond_6

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->getStandardOutput()Lorg/apache/pdfbox/pdfwriter/COSStandardOutputStream;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/pdfbox/pdfwriter/COSStandardOutputStream;->writeEOL()V

    goto :goto_0

    :cond_6
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->getStandardOutput()Lorg/apache/pdfbox/pdfwriter/COSStandardOutputStream;

    move-result-object v1

    sget-object v2, Lorg/apache/pdfbox/pdfwriter/COSWriter;->SPACE:[B

    invoke-virtual {v1, v2}, Ljava/io/OutputStream;->write([B)V

    goto :goto_0

    :cond_7
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->getStandardOutput()Lorg/apache/pdfbox/pdfwriter/COSStandardOutputStream;

    move-result-object p1

    sget-object v0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->ARRAY_CLOSE:[B

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->getStandardOutput()Lorg/apache/pdfbox/pdfwriter/COSStandardOutputStream;

    move-result-object p1

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdfwriter/COSStandardOutputStream;->writeEOL()V

    const/4 p1, 0x0

    return-object p1
.end method

.method public visitFromBoolean(Lorg/apache/pdfbox/cos/COSBoolean;)Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->getStandardOutput()Lorg/apache/pdfbox/pdfwriter/COSStandardOutputStream;

    move-result-object v0

    invoke-virtual {p1, v0}, Lorg/apache/pdfbox/cos/COSBoolean;->writePDF(Ljava/io/OutputStream;)V

    const/4 p1, 0x0

    return-object p1
.end method

.method public visitFromDictionary(Lorg/apache/pdfbox/cos/COSDictionary;)Ljava/lang/Object;
    .locals 6

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->getStandardOutput()Lorg/apache/pdfbox/pdfwriter/COSStandardOutputStream;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/pdfwriter/COSWriter;->DICT_OPEN:[B

    invoke-virtual {v0, v1}, Ljava/io/OutputStream;->write([B)V

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->getStandardOutput()Lorg/apache/pdfbox/pdfwriter/COSStandardOutputStream;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdfwriter/COSStandardOutputStream;->writeEOL()V

    invoke-virtual {p1}, Lorg/apache/pdfbox/cos/COSDictionary;->entrySet()Ljava/util/Set;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/pdfbox/cos/COSBase;

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v2, p0}, Lorg/apache/pdfbox/cos/COSName;->accept(Lorg/apache/pdfbox/cos/ICOSVisitor;)Ljava/lang/Object;

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->getStandardOutput()Lorg/apache/pdfbox/pdfwriter/COSStandardOutputStream;

    move-result-object v2

    sget-object v3, Lorg/apache/pdfbox/pdfwriter/COSWriter;->SPACE:[B

    invoke-virtual {v2, v3}, Ljava/io/OutputStream;->write([B)V

    instance-of v2, v1, Lorg/apache/pdfbox/cos/COSDictionary;

    if-eqz v2, :cond_4

    check-cast v1, Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->XOBJECT:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v1, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->getItem(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    const/4 v2, 0x1

    if-eqz v0, :cond_1

    invoke-virtual {v0, v2}, Lorg/apache/pdfbox/cos/COSBase;->setDirect(Z)V

    :cond_1
    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->RESOURCES:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v1, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->getItem(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0, v2}, Lorg/apache/pdfbox/cos/COSBase;->setDirect(Z)V

    :cond_2
    invoke-virtual {v1}, Lorg/apache/pdfbox/cos/COSBase;->isDirect()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p0, v1}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->visitFromDictionary(Lorg/apache/pdfbox/cos/COSDictionary;)Ljava/lang/Object;

    goto/16 :goto_2

    :cond_3
    :goto_1
    invoke-direct {p0, v1}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->addObjectToWrite(Lorg/apache/pdfbox/cos/COSBase;)V

    invoke-virtual {p0, v1}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->writeReference(Lorg/apache/pdfbox/cos/COSBase;)V

    goto/16 :goto_2

    :cond_4
    instance-of v2, v1, Lorg/apache/pdfbox/cos/COSObject;

    if-eqz v2, :cond_6

    move-object v0, v1

    check-cast v0, Lorg/apache/pdfbox/cos/COSObject;

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSObject;->getObject()Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    instance-of v2, v0, Lorg/apache/pdfbox/cos/COSDictionary;

    if-nez v2, :cond_3

    if-nez v0, :cond_5

    goto :goto_1

    :cond_5
    invoke-virtual {v0, p0}, Lorg/apache/pdfbox/cos/COSBase;->accept(Lorg/apache/pdfbox/cos/ICOSVisitor;)Ljava/lang/Object;

    goto :goto_2

    :cond_6
    iget-boolean v2, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->reachedSignature:Z

    if-eqz v2, :cond_7

    sget-object v2, Lorg/apache/pdfbox/cos/COSName;->CONTENTS:Lorg/apache/pdfbox/cos/COSName;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Lorg/apache/pdfbox/cos/COSName;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->getStandardOutput()Lorg/apache/pdfbox/pdfwriter/COSStandardOutputStream;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdfwriter/COSStandardOutputStream;->getPos()J

    move-result-wide v2

    iput-wide v2, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->signatureOffset:J

    invoke-virtual {v1, p0}, Lorg/apache/pdfbox/cos/COSBase;->accept(Lorg/apache/pdfbox/cos/ICOSVisitor;)Ljava/lang/Object;

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->getStandardOutput()Lorg/apache/pdfbox/pdfwriter/COSStandardOutputStream;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdfwriter/COSStandardOutputStream;->getPos()J

    move-result-wide v0

    iget-wide v2, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->signatureOffset:J

    sub-long/2addr v0, v2

    iput-wide v0, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->signatureLength:J

    goto :goto_2

    :cond_7
    iget-boolean v2, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->reachedSignature:Z

    if-eqz v2, :cond_8

    sget-object v2, Lorg/apache/pdfbox/cos/COSName;->BYTERANGE:Lorg/apache/pdfbox/cos/COSName;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v2, v0}, Lorg/apache/pdfbox/cos/COSName;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->getStandardOutput()Lorg/apache/pdfbox/pdfwriter/COSStandardOutputStream;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdfwriter/COSStandardOutputStream;->getPos()J

    move-result-wide v2

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    iput-wide v2, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->byteRangeOffset:J

    invoke-virtual {v1, p0}, Lorg/apache/pdfbox/cos/COSBase;->accept(Lorg/apache/pdfbox/cos/ICOSVisitor;)Ljava/lang/Object;

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->getStandardOutput()Lorg/apache/pdfbox/pdfwriter/COSStandardOutputStream;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdfwriter/COSStandardOutputStream;->getPos()J

    move-result-wide v0

    sub-long/2addr v0, v4

    iget-wide v2, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->byteRangeOffset:J

    sub-long/2addr v0, v2

    iput-wide v0, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->byteRangeLength:J

    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->reachedSignature:Z

    goto :goto_2

    :cond_8
    invoke-virtual {v1, p0}, Lorg/apache/pdfbox/cos/COSBase;->accept(Lorg/apache/pdfbox/cos/ICOSVisitor;)Ljava/lang/Object;

    :goto_2
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->getStandardOutput()Lorg/apache/pdfbox/pdfwriter/COSStandardOutputStream;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdfwriter/COSStandardOutputStream;->writeEOL()V

    goto/16 :goto_0

    :cond_9
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->getStandardOutput()Lorg/apache/pdfbox/pdfwriter/COSStandardOutputStream;

    move-result-object p1

    sget-object v0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->DICT_CLOSE:[B

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->getStandardOutput()Lorg/apache/pdfbox/pdfwriter/COSStandardOutputStream;

    move-result-object p1

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdfwriter/COSStandardOutputStream;->writeEOL()V

    const/4 p1, 0x0

    return-object p1
.end method

.method public visitFromDocument(Lorg/apache/pdfbox/cos/COSDocument;)Ljava/lang/Object;
    .locals 3

    iget-boolean v0, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->incrementalUpdate:Z

    if-nez v0, :cond_0

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->doWriteHeader(Lorg/apache/pdfbox/cos/COSDocument;)V

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->getStandardOutput()Lorg/apache/pdfbox/pdfwriter/COSStandardOutputStream;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdfwriter/COSStandardOutputStream;->writeCRLF()V

    :goto_0
    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->doWriteBody(Lorg/apache/pdfbox/cos/COSDocument;)V

    invoke-virtual {p1}, Lorg/apache/pdfbox/cos/COSDocument;->getTrailer()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    if-eqz v0, :cond_1

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->XREF_STM:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getLong(Lorg/apache/pdfbox/cos/COSName;)J

    move-result-wide v0

    goto :goto_1

    :cond_1
    const-wide/16 v0, -0x1

    :goto_1
    iget-boolean v2, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->incrementalUpdate:Z

    if-nez v2, :cond_3

    invoke-virtual {p1}, Lorg/apache/pdfbox/cos/COSDocument;->isXRefStream()Z

    move-result v2

    if-eqz v2, :cond_2

    goto :goto_2

    :cond_2
    invoke-direct {p0}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->doWriteXRefTable()V

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->doWriteTrailer(Lorg/apache/pdfbox/cos/COSDocument;)V

    goto :goto_3

    :cond_3
    :goto_2
    invoke-direct {p0, p1, v0, v1}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->doWriteXRefInc(Lorg/apache/pdfbox/cos/COSDocument;J)V

    :goto_3
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->getStandardOutput()Lorg/apache/pdfbox/pdfwriter/COSStandardOutputStream;

    move-result-object p1

    sget-object v0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->STARTXREF:[B

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->getStandardOutput()Lorg/apache/pdfbox/pdfwriter/COSStandardOutputStream;

    move-result-object p1

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdfwriter/COSStandardOutputStream;->writeEOL()V

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->getStandardOutput()Lorg/apache/pdfbox/pdfwriter/COSStandardOutputStream;

    move-result-object p1

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->getStartxref()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/util/Charsets;->ISO_8859_1:Ljava/nio/charset/Charset;

    invoke-virtual {v0, v1}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->getStandardOutput()Lorg/apache/pdfbox/pdfwriter/COSStandardOutputStream;

    move-result-object p1

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdfwriter/COSStandardOutputStream;->writeEOL()V

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->getStandardOutput()Lorg/apache/pdfbox/pdfwriter/COSStandardOutputStream;

    move-result-object p1

    sget-object v0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->EOF:[B

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->getStandardOutput()Lorg/apache/pdfbox/pdfwriter/COSStandardOutputStream;

    move-result-object p1

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdfwriter/COSStandardOutputStream;->writeEOL()V

    iget-boolean p1, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->incrementalUpdate:Z

    if-eqz p1, :cond_4

    invoke-direct {p0}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->doWriteSignature()V

    :cond_4
    const/4 p1, 0x0

    return-object p1
.end method

.method public visitFromFloat(Lorg/apache/pdfbox/cos/COSFloat;)Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->getStandardOutput()Lorg/apache/pdfbox/pdfwriter/COSStandardOutputStream;

    move-result-object v0

    invoke-virtual {p1, v0}, Lorg/apache/pdfbox/cos/COSFloat;->writePDF(Ljava/io/OutputStream;)V

    const/4 p1, 0x0

    return-object p1
.end method

.method public visitFromInt(Lorg/apache/pdfbox/cos/COSInteger;)Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->getStandardOutput()Lorg/apache/pdfbox/pdfwriter/COSStandardOutputStream;

    move-result-object v0

    invoke-virtual {p1, v0}, Lorg/apache/pdfbox/cos/COSInteger;->writePDF(Ljava/io/OutputStream;)V

    const/4 p1, 0x0

    return-object p1
.end method

.method public visitFromName(Lorg/apache/pdfbox/cos/COSName;)Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->getStandardOutput()Lorg/apache/pdfbox/pdfwriter/COSStandardOutputStream;

    move-result-object v0

    invoke-virtual {p1, v0}, Lorg/apache/pdfbox/cos/COSName;->writePDF(Ljava/io/OutputStream;)V

    const/4 p1, 0x0

    return-object p1
.end method

.method public visitFromNull(Lorg/apache/pdfbox/cos/COSNull;)Ljava/lang/Object;
    .locals 0

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->getStandardOutput()Lorg/apache/pdfbox/pdfwriter/COSStandardOutputStream;

    move-result-object p1

    invoke-static {p1}, Lorg/apache/pdfbox/cos/COSNull;->writePDF(Ljava/io/OutputStream;)V

    const/4 p1, 0x0

    return-object p1
.end method

.method public visitFromStream(Lorg/apache/pdfbox/cos/COSStream;)Ljava/lang/Object;
    .locals 8

    iget-boolean v0, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->willEncrypt:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->pdDocument:Lorg/apache/pdfbox/pdmodel/PDDocument;

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/PDDocument;->getEncryption()Lorg/apache/pdfbox/pdmodel/encryption/PDEncryption;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/encryption/PDEncryption;->getSecurityHandler()Lorg/apache/pdfbox/pdmodel/encryption/SecurityHandler;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->currentObjectKey:Lorg/apache/pdfbox/cos/COSObjectKey;

    invoke-virtual {v1}, Lorg/apache/pdfbox/cos/COSObjectKey;->getNumber()J

    move-result-wide v1

    iget-object v3, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->currentObjectKey:Lorg/apache/pdfbox/cos/COSObjectKey;

    invoke-virtual {v3}, Lorg/apache/pdfbox/cos/COSObjectKey;->getGeneration()I

    move-result v3

    invoke-virtual {v0, p1, v1, v2, v3}, Lorg/apache/pdfbox/pdmodel/encryption/SecurityHandler;->encryptStream(Lorg/apache/pdfbox/cos/COSStream;JI)V

    :cond_0
    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->LENGTH:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p1, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v1

    sget-object v2, Lorg/apache/pdfbox/cos/COSName;->TYPE:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->getNameAsString(Lorg/apache/pdfbox/cos/COSName;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lorg/apache/pdfbox/cos/COSBase;->isDirect()Z

    move-result v1

    if-nez v1, :cond_2

    :cond_1
    const-string v1, "XRef"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    :cond_2
    invoke-virtual {p1}, Lorg/apache/pdfbox/cos/COSStream;->getFilteredLength()J

    move-result-wide v1

    invoke-static {v1, v2}, Lorg/apache/pdfbox/cos/COSInteger;->get(J)Lorg/apache/pdfbox/cos/COSInteger;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lorg/apache/pdfbox/cos/COSBase;->setDirect(Z)V

    invoke-virtual {p1, v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    move-object v1, v3

    goto :goto_0

    :cond_3
    new-instance v1, Lorg/apache/pdfbox/cos/COSObject;

    invoke-direct {v1, v3}, Lorg/apache/pdfbox/cos/COSObject;-><init>(Lorg/apache/pdfbox/cos/COSBase;)V

    invoke-virtual {p1, v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    :goto_0
    :try_start_0
    invoke-virtual {p1}, Lorg/apache/pdfbox/cos/COSStream;->getFilteredStream()Ljava/io/InputStream;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->visitFromDictionary(Lorg/apache/pdfbox/cos/COSDictionary;)Ljava/lang/Object;

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->getStandardOutput()Lorg/apache/pdfbox/pdfwriter/COSStandardOutputStream;

    move-result-object p1

    sget-object v2, Lorg/apache/pdfbox/pdfwriter/COSWriter;->STREAM:[B

    invoke-virtual {p1, v2}, Ljava/io/OutputStream;->write([B)V

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->getStandardOutput()Lorg/apache/pdfbox/pdfwriter/COSStandardOutputStream;

    move-result-object p1

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdfwriter/COSStandardOutputStream;->writeCRLF()V

    const/16 p1, 0x400

    new-array v2, p1, [B

    const/4 v4, 0x0

    move v5, v4

    :goto_1
    invoke-virtual {v0, v2, v4, p1}, Ljava/io/InputStream;->read([BII)I

    move-result v6

    const/4 v7, -0x1

    if-eq v6, v7, :cond_4

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->getStandardOutput()Lorg/apache/pdfbox/pdfwriter/COSStandardOutputStream;

    move-result-object v7

    invoke-virtual {v7, v2, v4, v6}, Lorg/apache/pdfbox/pdfwriter/COSStandardOutputStream;->write([BII)V

    add-int/2addr v5, v6

    goto :goto_1

    :cond_4
    if-eqz v1, :cond_5

    int-to-long v4, v5

    invoke-static {v4, v5}, Lorg/apache/pdfbox/cos/COSInteger;->get(J)Lorg/apache/pdfbox/cos/COSInteger;

    move-result-object p1

    invoke-virtual {v1, p1}, Lorg/apache/pdfbox/cos/COSObject;->setObject(Lorg/apache/pdfbox/cos/COSBase;)V

    :cond_5
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->getStandardOutput()Lorg/apache/pdfbox/pdfwriter/COSStandardOutputStream;

    move-result-object p1

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdfwriter/COSStandardOutputStream;->writeCRLF()V

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->getStandardOutput()Lorg/apache/pdfbox/pdfwriter/COSStandardOutputStream;

    move-result-object p1

    sget-object v1, Lorg/apache/pdfbox/pdfwriter/COSWriter;->ENDSTREAM:[B

    invoke-virtual {p1, v1}, Ljava/io/OutputStream;->write([B)V

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->getStandardOutput()Lorg/apache/pdfbox/pdfwriter/COSStandardOutputStream;

    move-result-object p1

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdfwriter/COSStandardOutputStream;->writeEOL()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    return-object v3

    :catchall_0
    move-exception p1

    move-object v3, v0

    goto :goto_2

    :catchall_1
    move-exception p1

    :goto_2
    if-eqz v3, :cond_6

    invoke-virtual {v3}, Ljava/io/InputStream;->close()V

    :cond_6
    throw p1
.end method

.method public visitFromString(Lorg/apache/pdfbox/cos/COSString;)Ljava/lang/Object;
    .locals 4

    iget-boolean v0, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->willEncrypt:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->pdDocument:Lorg/apache/pdfbox/pdmodel/PDDocument;

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/PDDocument;->getEncryption()Lorg/apache/pdfbox/pdmodel/encryption/PDEncryption;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/encryption/PDEncryption;->getSecurityHandler()Lorg/apache/pdfbox/pdmodel/encryption/SecurityHandler;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->currentObjectKey:Lorg/apache/pdfbox/cos/COSObjectKey;

    invoke-virtual {v1}, Lorg/apache/pdfbox/cos/COSObjectKey;->getNumber()J

    move-result-wide v1

    iget-object v3, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->currentObjectKey:Lorg/apache/pdfbox/cos/COSObjectKey;

    invoke-virtual {v3}, Lorg/apache/pdfbox/cos/COSObjectKey;->getGeneration()I

    move-result v3

    invoke-virtual {v0, p1, v1, v2, v3}, Lorg/apache/pdfbox/pdmodel/encryption/SecurityHandler;->encryptString(Lorg/apache/pdfbox/cos/COSString;JI)V

    :cond_0
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->getStandardOutput()Lorg/apache/pdfbox/pdfwriter/COSStandardOutputStream;

    move-result-object v0

    invoke-static {p1, v0}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->writeString(Lorg/apache/pdfbox/cos/COSString;Ljava/io/OutputStream;)V

    const/4 p1, 0x0

    return-object p1
.end method

.method public write(Lorg/apache/pdfbox/cos/COSDocument;)V
    .locals 1

    .line 1
    new-instance v0, Lorg/apache/pdfbox/pdmodel/PDDocument;

    invoke-direct {v0, p1}, Lorg/apache/pdfbox/pdmodel/PDDocument;-><init>(Lorg/apache/pdfbox/cos/COSDocument;)V

    invoke-virtual {p0, v0}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->write(Lorg/apache/pdfbox/pdmodel/PDDocument;)V

    return-void
.end method

.method public write(Lorg/apache/pdfbox/pdmodel/PDDocument;)V
    .locals 1

    .line 2
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->write(Lorg/apache/pdfbox/pdmodel/PDDocument;Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/SignatureInterface;)V

    return-void
.end method

.method public write(Lorg/apache/pdfbox/pdmodel/PDDocument;Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/SignatureInterface;)V
    .locals 7

    .line 3
    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/PDDocument;->getDocumentId()Ljava/lang/Long;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/PDDocument;->getDocumentId()Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    :goto_0
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object p1, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->pdDocument:Lorg/apache/pdfbox/pdmodel/PDDocument;

    iput-object p2, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->signatureInterface:Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/SignatureInterface;

    iget-boolean p2, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->incrementalUpdate:Z

    if-eqz p2, :cond_1

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->prepareIncrement(Lorg/apache/pdfbox/pdmodel/PDDocument;)V

    :cond_1
    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/PDDocument;->isAllSecurityToBeRemoved()Z

    move-result p2

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz p2, :cond_2

    iput-boolean v2, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->willEncrypt:Z

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/PDDocument;->getDocument()Lorg/apache/pdfbox/cos/COSDocument;

    move-result-object p1

    invoke-virtual {p1}, Lorg/apache/pdfbox/cos/COSDocument;->getTrailer()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object p1

    sget-object p2, Lorg/apache/pdfbox/cos/COSName;->ENCRYPT:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p1, p2}, Lorg/apache/pdfbox/cos/COSDictionary;->removeItem(Lorg/apache/pdfbox/cos/COSName;)V

    goto :goto_1

    :cond_2
    iget-object p1, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->pdDocument:Lorg/apache/pdfbox/pdmodel/PDDocument;

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/PDDocument;->getEncryption()Lorg/apache/pdfbox/pdmodel/encryption/PDEncryption;

    move-result-object p1

    if-eqz p1, :cond_3

    iget-object p1, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->pdDocument:Lorg/apache/pdfbox/pdmodel/PDDocument;

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/PDDocument;->getEncryption()Lorg/apache/pdfbox/pdmodel/encryption/PDEncryption;

    move-result-object p1

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/encryption/PDEncryption;->getSecurityHandler()Lorg/apache/pdfbox/pdmodel/encryption/SecurityHandler;

    move-result-object p1

    iget-object p2, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->pdDocument:Lorg/apache/pdfbox/pdmodel/PDDocument;

    invoke-virtual {p1, p2}, Lorg/apache/pdfbox/pdmodel/encryption/SecurityHandler;->prepareDocumentForEncryption(Lorg/apache/pdfbox/pdmodel/PDDocument;)V

    iput-boolean v1, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->willEncrypt:Z

    goto :goto_1

    :cond_3
    iput-boolean v2, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->willEncrypt:Z

    :goto_1
    iget-object p1, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->pdDocument:Lorg/apache/pdfbox/pdmodel/PDDocument;

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/PDDocument;->getDocument()Lorg/apache/pdfbox/cos/COSDocument;

    move-result-object p1

    invoke-virtual {p1}, Lorg/apache/pdfbox/cos/COSDocument;->getTrailer()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object p2

    sget-object v3, Lorg/apache/pdfbox/cos/COSName;->ID:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p2, v3}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v3

    check-cast v3, Lorg/apache/pdfbox/cos/COSArray;

    if-eqz v3, :cond_4

    invoke-virtual {v3}, Lorg/apache/pdfbox/cos/COSArray;->size()I

    move-result v4

    const/4 v5, 0x2

    if-ne v4, v5, :cond_4

    move v1, v2

    :cond_4
    if-nez v1, :cond_5

    iget-boolean v4, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->incrementalUpdate:Z

    if-eqz v4, :cond_9

    :cond_5
    :try_start_0
    const-string v4, "MD5"

    invoke-static {v4}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v4
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    sget-object v5, Lorg/apache/pdfbox/util/Charsets;->ISO_8859_1:Ljava/nio/charset/Charset;

    invoke-virtual {v0, v5}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/security/MessageDigest;->update([B)V

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->INFO:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p2, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSDictionary;

    if-eqz v0, :cond_6

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSDictionary;->getValues()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_6

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/apache/pdfbox/cos/COSBase;

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    sget-object v6, Lorg/apache/pdfbox/util/Charsets;->ISO_8859_1:Ljava/nio/charset/Charset;

    invoke-virtual {v5, v6}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/security/MessageDigest;->update([B)V

    goto :goto_2

    :cond_6
    if-eqz v1, :cond_7

    new-instance v0, Lorg/apache/pdfbox/cos/COSString;

    invoke-virtual {v4}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v2

    invoke-direct {v0, v2}, Lorg/apache/pdfbox/cos/COSString;-><init>([B)V

    goto :goto_3

    :cond_7
    invoke-virtual {v3, v2}, Lorg/apache/pdfbox/cos/COSArray;->get(I)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSString;

    :goto_3
    if-eqz v1, :cond_8

    move-object v1, v0

    goto :goto_4

    :cond_8
    new-instance v1, Lorg/apache/pdfbox/cos/COSString;

    invoke-virtual {v4}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v2

    invoke-direct {v1, v2}, Lorg/apache/pdfbox/cos/COSString;-><init>([B)V

    :goto_4
    new-instance v2, Lorg/apache/pdfbox/cos/COSArray;

    invoke-direct {v2}, Lorg/apache/pdfbox/cos/COSArray;-><init>()V

    invoke-virtual {v2, v0}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    invoke-virtual {v2, v1}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->ID:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p2, v0, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    :cond_9
    invoke-virtual {p1, p0}, Lorg/apache/pdfbox/cos/COSDocument;->accept(Lorg/apache/pdfbox/cos/ICOSVisitor;)Ljava/lang/Object;

    return-void

    :catch_0
    move-exception p1

    new-instance p2, Ljava/lang/RuntimeException;

    invoke-direct {p2, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw p2
.end method

.method public write(Lorg/apache/pdfbox/pdmodel/fdf/FDFDocument;)V
    .locals 1

    .line 4
    iput-object p1, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->fdfDocument:Lorg/apache/pdfbox/pdmodel/fdf/FDFDocument;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->willEncrypt:Z

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/fdf/FDFDocument;->getDocument()Lorg/apache/pdfbox/cos/COSDocument;

    move-result-object p1

    invoke-virtual {p1, p0}, Lorg/apache/pdfbox/cos/COSDocument;->accept(Lorg/apache/pdfbox/cos/ICOSVisitor;)Ljava/lang/Object;

    return-void
.end method

.method public writeReference(Lorg/apache/pdfbox/cos/COSBase;)V
    .locals 3

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->getObjectKey(Lorg/apache/pdfbox/cos/COSBase;)Lorg/apache/pdfbox/cos/COSObjectKey;

    move-result-object p1

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->getStandardOutput()Lorg/apache/pdfbox/pdfwriter/COSStandardOutputStream;

    move-result-object v0

    invoke-virtual {p1}, Lorg/apache/pdfbox/cos/COSObjectKey;->getNumber()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lorg/apache/pdfbox/util/Charsets;->ISO_8859_1:Ljava/nio/charset/Charset;

    invoke-virtual {v1, v2}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/OutputStream;->write([B)V

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->getStandardOutput()Lorg/apache/pdfbox/pdfwriter/COSStandardOutputStream;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/pdfwriter/COSWriter;->SPACE:[B

    invoke-virtual {v0, v1}, Ljava/io/OutputStream;->write([B)V

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->getStandardOutput()Lorg/apache/pdfbox/pdfwriter/COSStandardOutputStream;

    move-result-object v0

    invoke-virtual {p1}, Lorg/apache/pdfbox/cos/COSObjectKey;->getGeneration()I

    move-result p1

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1, v2}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/io/OutputStream;->write([B)V

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->getStandardOutput()Lorg/apache/pdfbox/pdfwriter/COSStandardOutputStream;

    move-result-object p1

    invoke-virtual {p1, v1}, Ljava/io/OutputStream;->write([B)V

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->getStandardOutput()Lorg/apache/pdfbox/pdfwriter/COSStandardOutputStream;

    move-result-object p1

    sget-object v0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->REFERENCE:[B

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    return-void
.end method

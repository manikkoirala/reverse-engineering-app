.class public Lorg/apache/pdfbox/pdfwriter/COSWriterXRefEntry;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable<",
        "Lorg/apache/pdfbox/pdfwriter/COSWriterXRefEntry;",
        ">;"
    }
.end annotation


# static fields
.field private static final NULLENTRY:Lorg/apache/pdfbox/pdfwriter/COSWriterXRefEntry;


# instance fields
.field private free:Z

.field private key:Lorg/apache/pdfbox/cos/COSObjectKey;

.field private object:Lorg/apache/pdfbox/cos/COSBase;

.field private offset:J


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    new-instance v0, Lorg/apache/pdfbox/pdfwriter/COSWriterXRefEntry;

    new-instance v1, Lorg/apache/pdfbox/cos/COSObjectKey;

    const v2, 0xffff

    const-wide/16 v3, 0x0

    invoke-direct {v1, v3, v4, v2}, Lorg/apache/pdfbox/cos/COSObjectKey;-><init>(JI)V

    const/4 v2, 0x0

    invoke-direct {v0, v3, v4, v2, v1}, Lorg/apache/pdfbox/pdfwriter/COSWriterXRefEntry;-><init>(JLorg/apache/pdfbox/cos/COSBase;Lorg/apache/pdfbox/cos/COSObjectKey;)V

    sput-object v0, Lorg/apache/pdfbox/pdfwriter/COSWriterXRefEntry;->NULLENTRY:Lorg/apache/pdfbox/pdfwriter/COSWriterXRefEntry;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/pdfwriter/COSWriterXRefEntry;->setFree(Z)V

    return-void
.end method

.method public constructor <init>(JLorg/apache/pdfbox/cos/COSBase;Lorg/apache/pdfbox/cos/COSObjectKey;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/pdfbox/pdfwriter/COSWriterXRefEntry;->free:Z

    invoke-virtual {p0, p1, p2}, Lorg/apache/pdfbox/pdfwriter/COSWriterXRefEntry;->setOffset(J)V

    invoke-direct {p0, p3}, Lorg/apache/pdfbox/pdfwriter/COSWriterXRefEntry;->setObject(Lorg/apache/pdfbox/cos/COSBase;)V

    invoke-direct {p0, p4}, Lorg/apache/pdfbox/pdfwriter/COSWriterXRefEntry;->setKey(Lorg/apache/pdfbox/cos/COSObjectKey;)V

    return-void
.end method

.method public static getNullEntry()Lorg/apache/pdfbox/pdfwriter/COSWriterXRefEntry;
    .locals 1

    sget-object v0, Lorg/apache/pdfbox/pdfwriter/COSWriterXRefEntry;->NULLENTRY:Lorg/apache/pdfbox/pdfwriter/COSWriterXRefEntry;

    return-object v0
.end method

.method private setKey(Lorg/apache/pdfbox/cos/COSObjectKey;)V
    .locals 0

    iput-object p1, p0, Lorg/apache/pdfbox/pdfwriter/COSWriterXRefEntry;->key:Lorg/apache/pdfbox/cos/COSObjectKey;

    return-void
.end method

.method private setObject(Lorg/apache/pdfbox/cos/COSBase;)V
    .locals 0

    iput-object p1, p0, Lorg/apache/pdfbox/pdfwriter/COSWriterXRefEntry;->object:Lorg/apache/pdfbox/cos/COSBase;

    return-void
.end method


# virtual methods
.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 0

    .line 1
    check-cast p1, Lorg/apache/pdfbox/pdfwriter/COSWriterXRefEntry;

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/pdfwriter/COSWriterXRefEntry;->compareTo(Lorg/apache/pdfbox/pdfwriter/COSWriterXRefEntry;)I

    move-result p1

    return p1
.end method

.method public compareTo(Lorg/apache/pdfbox/pdfwriter/COSWriterXRefEntry;)I
    .locals 5

    .line 2
    const/4 v0, -0x1

    if-eqz p1, :cond_2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfwriter/COSWriterXRefEntry;->getKey()Lorg/apache/pdfbox/cos/COSObjectKey;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/pdfbox/cos/COSObjectKey;->getNumber()J

    move-result-wide v1

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdfwriter/COSWriterXRefEntry;->getKey()Lorg/apache/pdfbox/cos/COSObjectKey;

    move-result-object v3

    invoke-virtual {v3}, Lorg/apache/pdfbox/cos/COSObjectKey;->getNumber()J

    move-result-wide v3

    cmp-long v1, v1, v3

    if-gez v1, :cond_0

    return v0

    :cond_0
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfwriter/COSWriterXRefEntry;->getKey()Lorg/apache/pdfbox/cos/COSObjectKey;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSObjectKey;->getNumber()J

    move-result-wide v0

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdfwriter/COSWriterXRefEntry;->getKey()Lorg/apache/pdfbox/cos/COSObjectKey;

    move-result-object p1

    invoke-virtual {p1}, Lorg/apache/pdfbox/cos/COSObjectKey;->getNumber()J

    move-result-wide v2

    cmp-long p1, v0, v2

    if-lez p1, :cond_1

    const/4 p1, 0x1

    return p1

    :cond_1
    const/4 p1, 0x0

    return p1

    :cond_2
    return v0
.end method

.method public getKey()Lorg/apache/pdfbox/cos/COSObjectKey;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdfwriter/COSWriterXRefEntry;->key:Lorg/apache/pdfbox/cos/COSObjectKey;

    return-object v0
.end method

.method public getObject()Lorg/apache/pdfbox/cos/COSBase;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdfwriter/COSWriterXRefEntry;->object:Lorg/apache/pdfbox/cos/COSBase;

    return-object v0
.end method

.method public getOffset()J
    .locals 2

    iget-wide v0, p0, Lorg/apache/pdfbox/pdfwriter/COSWriterXRefEntry;->offset:J

    return-wide v0
.end method

.method public isFree()Z
    .locals 1

    iget-boolean v0, p0, Lorg/apache/pdfbox/pdfwriter/COSWriterXRefEntry;->free:Z

    return v0
.end method

.method public setFree(Z)V
    .locals 0

    iput-boolean p1, p0, Lorg/apache/pdfbox/pdfwriter/COSWriterXRefEntry;->free:Z

    return-void
.end method

.method public final setOffset(J)V
    .locals 0

    iput-wide p1, p0, Lorg/apache/pdfbox/pdfwriter/COSWriterXRefEntry;->offset:J

    return-void
.end method

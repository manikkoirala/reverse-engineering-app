.class public Lorg/apache/pdfbox/pdfwriter/COSFilterInputStream;
.super Ljava/io/FilterInputStream;
.source "SourceFile"


# instance fields
.field private final byteRange:[I

.field private position:J


# direct methods
.method public constructor <init>(Ljava/io/InputStream;[I)V
    .locals 2

    .line 1
    invoke-direct {p0, p1}, Ljava/io/FilterInputStream;-><init>(Ljava/io/InputStream;)V

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lorg/apache/pdfbox/pdfwriter/COSFilterInputStream;->position:J

    iput-object p2, p0, Lorg/apache/pdfbox/pdfwriter/COSFilterInputStream;->byteRange:[I

    return-void
.end method

.method public constructor <init>([B[I)V
    .locals 2

    .line 2
    new-instance v0, Ljava/io/ByteArrayInputStream;

    invoke-direct {v0, p1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {p0, v0}, Ljava/io/FilterInputStream;-><init>(Ljava/io/InputStream;)V

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lorg/apache/pdfbox/pdfwriter/COSFilterInputStream;->position:J

    iput-object p2, p0, Lorg/apache/pdfbox/pdfwriter/COSFilterInputStream;->byteRange:[I

    return-void
.end method

.method private inRange()Z
    .locals 9

    iget-wide v0, p0, Lorg/apache/pdfbox/pdfwriter/COSFilterInputStream;->position:J

    const/4 v2, 0x0

    move v3, v2

    :goto_0
    iget-object v4, p0, Lorg/apache/pdfbox/pdfwriter/COSFilterInputStream;->byteRange:[I

    array-length v5, v4

    div-int/lit8 v5, v5, 0x2

    if-ge v3, v5, :cond_1

    mul-int/lit8 v5, v3, 0x2

    aget v6, v4, v5

    int-to-long v7, v6

    cmp-long v7, v7, v0

    if-gtz v7, :cond_0

    add-int/lit8 v5, v5, 0x1

    aget v4, v4, v5

    add-int/2addr v6, v4

    int-to-long v4, v6

    cmp-long v4, v4, v0

    if-lez v4, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    return v2
.end method

.method private nextAvailable()V
    .locals 4

    :cond_0
    invoke-direct {p0}, Lorg/apache/pdfbox/pdfwriter/COSFilterInputStream;->inRange()Z

    move-result v0

    if-nez v0, :cond_1

    iget-wide v0, p0, Lorg/apache/pdfbox/pdfwriter/COSFilterInputStream;->position:J

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    iput-wide v0, p0, Lorg/apache/pdfbox/pdfwriter/COSFilterInputStream;->position:J

    invoke-super {p0}, Ljava/io/FilterInputStream;->read()I

    move-result v0

    if-gez v0, :cond_0

    :cond_1
    return-void
.end method


# virtual methods
.method public read()I
    .locals 5

    .line 1
    invoke-direct {p0}, Lorg/apache/pdfbox/pdfwriter/COSFilterInputStream;->nextAvailable()V

    invoke-super {p0}, Ljava/io/FilterInputStream;->read()I

    move-result v0

    const/4 v1, -0x1

    if-le v0, v1, :cond_0

    iget-wide v1, p0, Lorg/apache/pdfbox/pdfwriter/COSFilterInputStream;->position:J

    const-wide/16 v3, 0x1

    add-long/2addr v1, v3

    iput-wide v1, p0, Lorg/apache/pdfbox/pdfwriter/COSFilterInputStream;->position:J

    :cond_0
    return v0
.end method

.method public read([B)I
    .locals 2

    .line 2
    array-length v0, p1

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v1, v0}, Lorg/apache/pdfbox/pdfwriter/COSFilterInputStream;->read([BII)I

    move-result p1

    return p1
.end method

.method public read([BII)I
    .locals 4

    .line 3
    if-nez p3, :cond_0

    const/4 p1, 0x0

    return p1

    :cond_0
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfwriter/COSFilterInputStream;->read()I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    return v1

    :cond_1
    int-to-byte v0, v0

    aput-byte v0, p1, p2

    const/4 v0, 0x1

    :goto_0
    if-ge v0, p3, :cond_3

    :try_start_0
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfwriter/COSFilterInputStream;->read()I

    move-result v2

    if-ne v2, v1, :cond_2

    goto :goto_1

    :cond_2
    add-int v3, p2, v0

    int-to-byte v2, v2

    aput-byte v2, p1, v3
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :catch_0
    :cond_3
    :goto_1
    return v0
.end method

.method public toByteArray()[B
    .locals 4

    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    const/16 v1, 0x400

    new-array v1, v1, [B

    :goto_0
    invoke-virtual {p0, v1}, Lorg/apache/pdfbox/pdfwriter/COSFilterInputStream;->read([B)I

    move-result v2

    const/4 v3, -0x1

    if-eq v2, v3, :cond_0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3, v2}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    return-object v0
.end method

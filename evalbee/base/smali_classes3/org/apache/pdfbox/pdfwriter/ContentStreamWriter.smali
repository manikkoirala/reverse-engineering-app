.class public Lorg/apache/pdfbox/pdfwriter/ContentStreamWriter;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final EOL:[B

.field public static final SPACE:[B


# instance fields
.field private output:Ljava/io/OutputStream;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    const/4 v0, 0x1

    new-array v1, v0, [B

    const/16 v2, 0x20

    const/4 v3, 0x0

    aput-byte v2, v1, v3

    sput-object v1, Lorg/apache/pdfbox/pdfwriter/ContentStreamWriter;->SPACE:[B

    new-array v0, v0, [B

    const/16 v1, 0xa

    aput-byte v1, v0, v3

    sput-object v0, Lorg/apache/pdfbox/pdfwriter/ContentStreamWriter;->EOL:[B

    return-void
.end method

.method public constructor <init>(Ljava/io/OutputStream;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lorg/apache/pdfbox/pdfwriter/ContentStreamWriter;->output:Ljava/io/OutputStream;

    return-void
.end method

.method private writeObject(Ljava/lang/Object;)V
    .locals 6

    instance-of v0, p1, Lorg/apache/pdfbox/cos/COSString;

    if-eqz v0, :cond_0

    check-cast p1, Lorg/apache/pdfbox/cos/COSString;

    iget-object v0, p0, Lorg/apache/pdfbox/pdfwriter/ContentStreamWriter;->output:Ljava/io/OutputStream;

    invoke-static {p1, v0}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->writeString(Lorg/apache/pdfbox/cos/COSString;Ljava/io/OutputStream;)V

    goto/16 :goto_3

    :cond_0
    instance-of v0, p1, Lorg/apache/pdfbox/cos/COSFloat;

    if-eqz v0, :cond_1

    check-cast p1, Lorg/apache/pdfbox/cos/COSFloat;

    iget-object v0, p0, Lorg/apache/pdfbox/pdfwriter/ContentStreamWriter;->output:Ljava/io/OutputStream;

    invoke-virtual {p1, v0}, Lorg/apache/pdfbox/cos/COSFloat;->writePDF(Ljava/io/OutputStream;)V

    goto/16 :goto_3

    :cond_1
    instance-of v0, p1, Lorg/apache/pdfbox/cos/COSInteger;

    if-eqz v0, :cond_2

    check-cast p1, Lorg/apache/pdfbox/cos/COSInteger;

    iget-object v0, p0, Lorg/apache/pdfbox/pdfwriter/ContentStreamWriter;->output:Ljava/io/OutputStream;

    invoke-virtual {p1, v0}, Lorg/apache/pdfbox/cos/COSInteger;->writePDF(Ljava/io/OutputStream;)V

    goto/16 :goto_3

    :cond_2
    instance-of v0, p1, Lorg/apache/pdfbox/cos/COSBoolean;

    if-eqz v0, :cond_3

    check-cast p1, Lorg/apache/pdfbox/cos/COSBoolean;

    iget-object v0, p0, Lorg/apache/pdfbox/pdfwriter/ContentStreamWriter;->output:Ljava/io/OutputStream;

    invoke-virtual {p1, v0}, Lorg/apache/pdfbox/cos/COSBoolean;->writePDF(Ljava/io/OutputStream;)V

    goto/16 :goto_3

    :cond_3
    instance-of v0, p1, Lorg/apache/pdfbox/cos/COSName;

    if-eqz v0, :cond_4

    check-cast p1, Lorg/apache/pdfbox/cos/COSName;

    iget-object v0, p0, Lorg/apache/pdfbox/pdfwriter/ContentStreamWriter;->output:Ljava/io/OutputStream;

    invoke-virtual {p1, v0}, Lorg/apache/pdfbox/cos/COSName;->writePDF(Ljava/io/OutputStream;)V

    goto/16 :goto_3

    :cond_4
    instance-of v0, p1, Lorg/apache/pdfbox/cos/COSArray;

    if-eqz v0, :cond_6

    check-cast p1, Lorg/apache/pdfbox/cos/COSArray;

    iget-object v0, p0, Lorg/apache/pdfbox/pdfwriter/ContentStreamWriter;->output:Ljava/io/OutputStream;

    sget-object v1, Lorg/apache/pdfbox/pdfwriter/COSWriter;->ARRAY_OPEN:[B

    invoke-virtual {v0, v1}, Ljava/io/OutputStream;->write([B)V

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1}, Lorg/apache/pdfbox/cos/COSArray;->size()I

    move-result v1

    if-ge v0, v1, :cond_5

    invoke-virtual {p1, v0}, Lorg/apache/pdfbox/cos/COSArray;->get(I)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v1

    invoke-direct {p0, v1}, Lorg/apache/pdfbox/pdfwriter/ContentStreamWriter;->writeObject(Ljava/lang/Object;)V

    iget-object v1, p0, Lorg/apache/pdfbox/pdfwriter/ContentStreamWriter;->output:Ljava/io/OutputStream;

    sget-object v2, Lorg/apache/pdfbox/pdfwriter/ContentStreamWriter;->SPACE:[B

    invoke-virtual {v1, v2}, Ljava/io/OutputStream;->write([B)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_5
    iget-object p1, p0, Lorg/apache/pdfbox/pdfwriter/ContentStreamWriter;->output:Ljava/io/OutputStream;

    sget-object v0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->ARRAY_CLOSE:[B

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    goto/16 :goto_3

    :cond_6
    instance-of v0, p1, Lorg/apache/pdfbox/cos/COSDictionary;

    if-eqz v0, :cond_9

    check-cast p1, Lorg/apache/pdfbox/cos/COSDictionary;

    iget-object v0, p0, Lorg/apache/pdfbox/pdfwriter/ContentStreamWriter;->output:Ljava/io/OutputStream;

    sget-object v1, Lorg/apache/pdfbox/pdfwriter/COSWriter;->DICT_OPEN:[B

    invoke-virtual {v0, v1}, Ljava/io/OutputStream;->write([B)V

    invoke-virtual {p1}, Lorg/apache/pdfbox/cos/COSDictionary;->entrySet()Ljava/util/Set;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_7
    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_7

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    invoke-direct {p0, v1}, Lorg/apache/pdfbox/pdfwriter/ContentStreamWriter;->writeObject(Ljava/lang/Object;)V

    iget-object v1, p0, Lorg/apache/pdfbox/pdfwriter/ContentStreamWriter;->output:Ljava/io/OutputStream;

    sget-object v2, Lorg/apache/pdfbox/pdfwriter/ContentStreamWriter;->SPACE:[B

    invoke-virtual {v1, v2}, Ljava/io/OutputStream;->write([B)V

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/pdfwriter/ContentStreamWriter;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lorg/apache/pdfbox/pdfwriter/ContentStreamWriter;->output:Ljava/io/OutputStream;

    invoke-virtual {v0, v2}, Ljava/io/OutputStream;->write([B)V

    goto :goto_1

    :cond_8
    iget-object p1, p0, Lorg/apache/pdfbox/pdfwriter/ContentStreamWriter;->output:Ljava/io/OutputStream;

    sget-object v0, Lorg/apache/pdfbox/pdfwriter/COSWriter;->DICT_CLOSE:[B

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    iget-object p1, p0, Lorg/apache/pdfbox/pdfwriter/ContentStreamWriter;->output:Ljava/io/OutputStream;

    sget-object v0, Lorg/apache/pdfbox/pdfwriter/ContentStreamWriter;->SPACE:[B

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    goto/16 :goto_3

    :cond_9
    instance-of v0, p1, Lorg/apache/pdfbox/contentstream/operator/Operator;

    if-eqz v0, :cond_c

    check-cast p1, Lorg/apache/pdfbox/contentstream/operator/Operator;

    invoke-virtual {p1}, Lorg/apache/pdfbox/contentstream/operator/Operator;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "BI"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const-string v2, "ISO-8859-1"

    if-eqz v0, :cond_b

    iget-object v0, p0, Lorg/apache/pdfbox/pdfwriter/ContentStreamWriter;->output:Ljava/io/OutputStream;

    invoke-virtual {v1, v2}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/OutputStream;->write([B)V

    invoke-virtual {p1}, Lorg/apache/pdfbox/contentstream/operator/Operator;->getImageParameters()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSDictionary;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_a

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v3}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v4

    iget-object v5, p0, Lorg/apache/pdfbox/pdfwriter/ContentStreamWriter;->output:Ljava/io/OutputStream;

    invoke-virtual {v3, v5}, Lorg/apache/pdfbox/cos/COSName;->writePDF(Ljava/io/OutputStream;)V

    iget-object v3, p0, Lorg/apache/pdfbox/pdfwriter/ContentStreamWriter;->output:Ljava/io/OutputStream;

    sget-object v5, Lorg/apache/pdfbox/pdfwriter/ContentStreamWriter;->SPACE:[B

    invoke-virtual {v3, v5}, Ljava/io/OutputStream;->write([B)V

    invoke-direct {p0, v4}, Lorg/apache/pdfbox/pdfwriter/ContentStreamWriter;->writeObject(Ljava/lang/Object;)V

    iget-object v3, p0, Lorg/apache/pdfbox/pdfwriter/ContentStreamWriter;->output:Ljava/io/OutputStream;

    sget-object v4, Lorg/apache/pdfbox/pdfwriter/ContentStreamWriter;->EOL:[B

    invoke-virtual {v3, v4}, Ljava/io/OutputStream;->write([B)V

    goto :goto_2

    :cond_a
    iget-object v0, p0, Lorg/apache/pdfbox/pdfwriter/ContentStreamWriter;->output:Ljava/io/OutputStream;

    const-string v1, "ID"

    invoke-virtual {v1, v2}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/OutputStream;->write([B)V

    iget-object v0, p0, Lorg/apache/pdfbox/pdfwriter/ContentStreamWriter;->output:Ljava/io/OutputStream;

    sget-object v1, Lorg/apache/pdfbox/pdfwriter/ContentStreamWriter;->EOL:[B

    invoke-virtual {v0, v1}, Ljava/io/OutputStream;->write([B)V

    iget-object v0, p0, Lorg/apache/pdfbox/pdfwriter/ContentStreamWriter;->output:Ljava/io/OutputStream;

    invoke-virtual {p1}, Lorg/apache/pdfbox/contentstream/operator/Operator;->getImageData()[B

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/io/OutputStream;->write([B)V

    goto :goto_3

    :cond_b
    iget-object v0, p0, Lorg/apache/pdfbox/pdfwriter/ContentStreamWriter;->output:Ljava/io/OutputStream;

    invoke-virtual {p1}, Lorg/apache/pdfbox/contentstream/operator/Operator;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1, v2}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/io/OutputStream;->write([B)V

    iget-object p1, p0, Lorg/apache/pdfbox/pdfwriter/ContentStreamWriter;->output:Ljava/io/OutputStream;

    sget-object v0, Lorg/apache/pdfbox/pdfwriter/ContentStreamWriter;->EOL:[B

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    :goto_3
    return-void

    :cond_c
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Error:Unknown type in content stream:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public writeTokens(Ljava/util/List;)V
    .locals 2

    .line 1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v1, v0}, Lorg/apache/pdfbox/pdfwriter/ContentStreamWriter;->writeTokens(Ljava/util/List;II)V

    return-void
.end method

.method public writeTokens(Ljava/util/List;II)V
    .locals 2

    .line 2
    :goto_0
    if-ge p2, p3, :cond_0

    invoke-interface {p1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/pdfwriter/ContentStreamWriter;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lorg/apache/pdfbox/pdfwriter/ContentStreamWriter;->output:Ljava/io/OutputStream;

    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Ljava/io/OutputStream;->write(I)V

    add-int/lit8 p2, p2, 0x1

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lorg/apache/pdfbox/pdfwriter/ContentStreamWriter;->output:Ljava/io/OutputStream;

    invoke-virtual {p1}, Ljava/io/OutputStream;->flush()V

    return-void
.end method

.class public interface abstract Lorg/apache/pdfbox/cos/ICOSVisitor;
.super Ljava/lang/Object;
.source "SourceFile"


# virtual methods
.method public abstract visitFromArray(Lorg/apache/pdfbox/cos/COSArray;)Ljava/lang/Object;
.end method

.method public abstract visitFromBoolean(Lorg/apache/pdfbox/cos/COSBoolean;)Ljava/lang/Object;
.end method

.method public abstract visitFromDictionary(Lorg/apache/pdfbox/cos/COSDictionary;)Ljava/lang/Object;
.end method

.method public abstract visitFromDocument(Lorg/apache/pdfbox/cos/COSDocument;)Ljava/lang/Object;
.end method

.method public abstract visitFromFloat(Lorg/apache/pdfbox/cos/COSFloat;)Ljava/lang/Object;
.end method

.method public abstract visitFromInt(Lorg/apache/pdfbox/cos/COSInteger;)Ljava/lang/Object;
.end method

.method public abstract visitFromName(Lorg/apache/pdfbox/cos/COSName;)Ljava/lang/Object;
.end method

.method public abstract visitFromNull(Lorg/apache/pdfbox/cos/COSNull;)Ljava/lang/Object;
.end method

.method public abstract visitFromStream(Lorg/apache/pdfbox/cos/COSStream;)Ljava/lang/Object;
.end method

.method public abstract visitFromString(Lorg/apache/pdfbox/cos/COSString;)Ljava/lang/Object;
.end method

.class public final Lorg/apache/pdfbox/cos/COSName;
.super Lorg/apache/pdfbox/cos/COSBase;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lorg/apache/pdfbox/cos/COSBase;",
        "Ljava/lang/Comparable<",
        "Lorg/apache/pdfbox/cos/COSName;",
        ">;"
    }
.end annotation


# static fields
.field public static final A:Lorg/apache/pdfbox/cos/COSName;

.field public static final AA:Lorg/apache/pdfbox/cos/COSName;

.field public static final ACRO_FORM:Lorg/apache/pdfbox/cos/COSName;

.field public static final ACTUAL_TEXT:Lorg/apache/pdfbox/cos/COSName;

.field public static final ADBE_PKCS7_DETACHED:Lorg/apache/pdfbox/cos/COSName;

.field public static final ADBE_PKCS7_SHA1:Lorg/apache/pdfbox/cos/COSName;

.field public static final ADBE_X509_RSA_SHA1:Lorg/apache/pdfbox/cos/COSName;

.field public static final ADOBE_PPKLITE:Lorg/apache/pdfbox/cos/COSName;

.field public static final AESV3:Lorg/apache/pdfbox/cos/COSName;

.field public static final AFTER:Lorg/apache/pdfbox/cos/COSName;

.field public static final AIS:Lorg/apache/pdfbox/cos/COSName;

.field public static final ALPHA:Lorg/apache/pdfbox/cos/COSName;

.field public static final ALT:Lorg/apache/pdfbox/cos/COSName;

.field public static final ALTERNATE:Lorg/apache/pdfbox/cos/COSName;

.field public static final ANNOT:Lorg/apache/pdfbox/cos/COSName;

.field public static final ANNOTS:Lorg/apache/pdfbox/cos/COSName;

.field public static final ANTI_ALIAS:Lorg/apache/pdfbox/cos/COSName;

.field public static final AP:Lorg/apache/pdfbox/cos/COSName;

.field public static final APP:Lorg/apache/pdfbox/cos/COSName;

.field public static final AP_REF:Lorg/apache/pdfbox/cos/COSName;

.field public static final ARTIFACT:Lorg/apache/pdfbox/cos/COSName;

.field public static final ART_BOX:Lorg/apache/pdfbox/cos/COSName;

.field public static final AS:Lorg/apache/pdfbox/cos/COSName;

.field public static final ASCENT:Lorg/apache/pdfbox/cos/COSName;

.field public static final ASCII85_DECODE:Lorg/apache/pdfbox/cos/COSName;

.field public static final ASCII85_DECODE_ABBREVIATION:Lorg/apache/pdfbox/cos/COSName;

.field public static final ASCII_HEX_DECODE:Lorg/apache/pdfbox/cos/COSName;

.field public static final ASCII_HEX_DECODE_ABBREVIATION:Lorg/apache/pdfbox/cos/COSName;

.field public static final ATTACHED:Lorg/apache/pdfbox/cos/COSName;

.field public static final AUTHOR:Lorg/apache/pdfbox/cos/COSName;

.field public static final AVG_WIDTH:Lorg/apache/pdfbox/cos/COSName;

.field public static final B:Lorg/apache/pdfbox/cos/COSName;

.field public static final BACKGROUND:Lorg/apache/pdfbox/cos/COSName;

.field public static final BASE_ENCODING:Lorg/apache/pdfbox/cos/COSName;

.field public static final BASE_FONT:Lorg/apache/pdfbox/cos/COSName;

.field public static final BASE_STATE:Lorg/apache/pdfbox/cos/COSName;

.field public static final BBOX:Lorg/apache/pdfbox/cos/COSName;

.field public static final BC:Lorg/apache/pdfbox/cos/COSName;

.field public static final BEFORE:Lorg/apache/pdfbox/cos/COSName;

.field public static final BG:Lorg/apache/pdfbox/cos/COSName;

.field public static final BITS_PER_COMPONENT:Lorg/apache/pdfbox/cos/COSName;

.field public static final BITS_PER_COORDINATE:Lorg/apache/pdfbox/cos/COSName;

.field public static final BITS_PER_FLAG:Lorg/apache/pdfbox/cos/COSName;

.field public static final BITS_PER_SAMPLE:Lorg/apache/pdfbox/cos/COSName;

.field public static final BLACK_IS_1:Lorg/apache/pdfbox/cos/COSName;

.field public static final BLACK_POINT:Lorg/apache/pdfbox/cos/COSName;

.field public static final BLEED_BOX:Lorg/apache/pdfbox/cos/COSName;

.field public static final BM:Lorg/apache/pdfbox/cos/COSName;

.field public static final BOUNDS:Lorg/apache/pdfbox/cos/COSName;

.field public static final BPC:Lorg/apache/pdfbox/cos/COSName;

.field public static final BS:Lorg/apache/pdfbox/cos/COSName;

.field public static final BTN:Lorg/apache/pdfbox/cos/COSName;

.field public static final BYTERANGE:Lorg/apache/pdfbox/cos/COSName;

.field public static final C:Lorg/apache/pdfbox/cos/COSName;

.field public static final C0:Lorg/apache/pdfbox/cos/COSName;

.field public static final C1:Lorg/apache/pdfbox/cos/COSName;

.field public static final CA:Lorg/apache/pdfbox/cos/COSName;

.field public static final CALGRAY:Lorg/apache/pdfbox/cos/COSName;

.field public static final CALRGB:Lorg/apache/pdfbox/cos/COSName;

.field public static final CAP_HEIGHT:Lorg/apache/pdfbox/cos/COSName;

.field public static final CATALOG:Lorg/apache/pdfbox/cos/COSName;

.field public static final CA_NS:Lorg/apache/pdfbox/cos/COSName;

.field public static final CCITTFAX_DECODE:Lorg/apache/pdfbox/cos/COSName;

.field public static final CCITTFAX_DECODE_ABBREVIATION:Lorg/apache/pdfbox/cos/COSName;

.field public static final CENTER_WINDOW:Lorg/apache/pdfbox/cos/COSName;

.field public static final CF:Lorg/apache/pdfbox/cos/COSName;

.field public static final CFM:Lorg/apache/pdfbox/cos/COSName;

.field public static final CH:Lorg/apache/pdfbox/cos/COSName;

.field public static final CHAR_PROCS:Lorg/apache/pdfbox/cos/COSName;

.field public static final CHAR_SET:Lorg/apache/pdfbox/cos/COSName;

.field public static final CICI_SIGNIT:Lorg/apache/pdfbox/cos/COSName;

.field public static final CIDSYSTEMINFO:Lorg/apache/pdfbox/cos/COSName;

.field public static final CID_FONT_TYPE0:Lorg/apache/pdfbox/cos/COSName;

.field public static final CID_FONT_TYPE2:Lorg/apache/pdfbox/cos/COSName;

.field public static final CID_SET:Lorg/apache/pdfbox/cos/COSName;

.field public static final CID_TO_GID_MAP:Lorg/apache/pdfbox/cos/COSName;

.field public static final CLR_F:Lorg/apache/pdfbox/cos/COSName;

.field public static final CLR_FF:Lorg/apache/pdfbox/cos/COSName;

.field public static final CMAP:Lorg/apache/pdfbox/cos/COSName;

.field public static final CMAPNAME:Lorg/apache/pdfbox/cos/COSName;

.field public static final CMYK:Lorg/apache/pdfbox/cos/COSName;

.field public static final COLORANTS:Lorg/apache/pdfbox/cos/COSName;

.field public static final COLORS:Lorg/apache/pdfbox/cos/COSName;

.field public static final COLORSPACE:Lorg/apache/pdfbox/cos/COSName;

.field public static final COLOR_BURN:Lorg/apache/pdfbox/cos/COSName;

.field public static final COLOR_DODGE:Lorg/apache/pdfbox/cos/COSName;

.field public static final COLUMNS:Lorg/apache/pdfbox/cos/COSName;

.field public static final COMPATIBLE:Lorg/apache/pdfbox/cos/COSName;

.field public static final COMPONENTS:Lorg/apache/pdfbox/cos/COSName;

.field public static final CONTACT_INFO:Lorg/apache/pdfbox/cos/COSName;

.field public static final CONTENTS:Lorg/apache/pdfbox/cos/COSName;

.field public static final COORDS:Lorg/apache/pdfbox/cos/COSName;

.field public static final COUNT:Lorg/apache/pdfbox/cos/COSName;

.field public static final CREATION_DATE:Lorg/apache/pdfbox/cos/COSName;

.field public static final CREATOR:Lorg/apache/pdfbox/cos/COSName;

.field public static final CROP_BOX:Lorg/apache/pdfbox/cos/COSName;

.field public static final CRYPT:Lorg/apache/pdfbox/cos/COSName;

.field public static final CS:Lorg/apache/pdfbox/cos/COSName;

.field public static final D:Lorg/apache/pdfbox/cos/COSName;

.field public static final DA:Lorg/apache/pdfbox/cos/COSName;

.field public static final DARKEN:Lorg/apache/pdfbox/cos/COSName;

.field public static final DATE:Lorg/apache/pdfbox/cos/COSName;

.field public static final DCT_DECODE:Lorg/apache/pdfbox/cos/COSName;

.field public static final DCT_DECODE_ABBREVIATION:Lorg/apache/pdfbox/cos/COSName;

.field public static final DECODE:Lorg/apache/pdfbox/cos/COSName;

.field public static final DECODE_PARMS:Lorg/apache/pdfbox/cos/COSName;

.field public static final DEFAULT:Lorg/apache/pdfbox/cos/COSName;

.field public static final DEFAULT_CMYK:Lorg/apache/pdfbox/cos/COSName;

.field public static final DEFAULT_GRAY:Lorg/apache/pdfbox/cos/COSName;

.field public static final DEFAULT_RGB:Lorg/apache/pdfbox/cos/COSName;

.field public static final DESC:Lorg/apache/pdfbox/cos/COSName;

.field public static final DESCENDANT_FONTS:Lorg/apache/pdfbox/cos/COSName;

.field public static final DESCENT:Lorg/apache/pdfbox/cos/COSName;

.field public static final DEST:Lorg/apache/pdfbox/cos/COSName;

.field public static final DESTS:Lorg/apache/pdfbox/cos/COSName;

.field public static final DEST_OUTPUT_PROFILE:Lorg/apache/pdfbox/cos/COSName;

.field public static final DEVICECMYK:Lorg/apache/pdfbox/cos/COSName;

.field public static final DEVICEGRAY:Lorg/apache/pdfbox/cos/COSName;

.field public static final DEVICEN:Lorg/apache/pdfbox/cos/COSName;

.field public static final DEVICERGB:Lorg/apache/pdfbox/cos/COSName;

.field public static final DI:Lorg/apache/pdfbox/cos/COSName;

.field public static final DIFFERENCE:Lorg/apache/pdfbox/cos/COSName;

.field public static final DIFFERENCES:Lorg/apache/pdfbox/cos/COSName;

.field public static final DIGEST_METHOD:Lorg/apache/pdfbox/cos/COSName;

.field public static final DIGEST_RIPEMD160:Lorg/apache/pdfbox/cos/COSName;

.field public static final DIGEST_SHA1:Lorg/apache/pdfbox/cos/COSName;

.field public static final DIGEST_SHA256:Lorg/apache/pdfbox/cos/COSName;

.field public static final DIGEST_SHA384:Lorg/apache/pdfbox/cos/COSName;

.field public static final DIGEST_SHA512:Lorg/apache/pdfbox/cos/COSName;

.field public static final DIRECTION:Lorg/apache/pdfbox/cos/COSName;

.field public static final DISPLAY_DOC_TITLE:Lorg/apache/pdfbox/cos/COSName;

.field public static final DL:Lorg/apache/pdfbox/cos/COSName;

.field public static final DM:Lorg/apache/pdfbox/cos/COSName;

.field public static final DOC:Lorg/apache/pdfbox/cos/COSName;

.field public static final DOC_CHECKSUM:Lorg/apache/pdfbox/cos/COSName;

.field public static final DOC_TIME_STAMP:Lorg/apache/pdfbox/cos/COSName;

.field public static final DOMAIN:Lorg/apache/pdfbox/cos/COSName;

.field public static final DOS:Lorg/apache/pdfbox/cos/COSName;

.field public static final DP:Lorg/apache/pdfbox/cos/COSName;

.field public static final DR:Lorg/apache/pdfbox/cos/COSName;

.field public static final DS:Lorg/apache/pdfbox/cos/COSName;

.field public static final DUPLEX:Lorg/apache/pdfbox/cos/COSName;

.field public static final DUR:Lorg/apache/pdfbox/cos/COSName;

.field public static final DV:Lorg/apache/pdfbox/cos/COSName;

.field public static final DW:Lorg/apache/pdfbox/cos/COSName;

.field public static final DW2:Lorg/apache/pdfbox/cos/COSName;

.field public static final E:Lorg/apache/pdfbox/cos/COSName;

.field public static final EARLY_CHANGE:Lorg/apache/pdfbox/cos/COSName;

.field public static final EF:Lorg/apache/pdfbox/cos/COSName;

.field public static final EMBEDDED_FDFS:Lorg/apache/pdfbox/cos/COSName;

.field public static final EMBEDDED_FILES:Lorg/apache/pdfbox/cos/COSName;

.field public static final EMPTY:Lorg/apache/pdfbox/cos/COSName;

.field public static final ENCODE:Lorg/apache/pdfbox/cos/COSName;

.field public static final ENCODED_BYTE_ALIGN:Lorg/apache/pdfbox/cos/COSName;

.field public static final ENCODING:Lorg/apache/pdfbox/cos/COSName;

.field public static final ENCODING_90MS_RKSJ_H:Lorg/apache/pdfbox/cos/COSName;

.field public static final ENCODING_90MS_RKSJ_V:Lorg/apache/pdfbox/cos/COSName;

.field public static final ENCODING_ETEN_B5_H:Lorg/apache/pdfbox/cos/COSName;

.field public static final ENCODING_ETEN_B5_V:Lorg/apache/pdfbox/cos/COSName;

.field public static final ENCRYPT:Lorg/apache/pdfbox/cos/COSName;

.field public static final ENCRYPT_META_DATA:Lorg/apache/pdfbox/cos/COSName;

.field public static final END_OF_LINE:Lorg/apache/pdfbox/cos/COSName;

.field public static final ENTRUST_PPKEF:Lorg/apache/pdfbox/cos/COSName;

.field public static final EXCLUSION:Lorg/apache/pdfbox/cos/COSName;

.field public static final EXTEND:Lorg/apache/pdfbox/cos/COSName;

.field public static final EXTENDS:Lorg/apache/pdfbox/cos/COSName;

.field public static final EXT_G_STATE:Lorg/apache/pdfbox/cos/COSName;

.field public static final F:Lorg/apache/pdfbox/cos/COSName;

.field public static final FB:Lorg/apache/pdfbox/cos/COSName;

.field public static final FDF:Lorg/apache/pdfbox/cos/COSName;

.field public static final FF:Lorg/apache/pdfbox/cos/COSName;

.field public static final FIELDS:Lorg/apache/pdfbox/cos/COSName;

.field public static final FILESPEC:Lorg/apache/pdfbox/cos/COSName;

.field public static final FILTER:Lorg/apache/pdfbox/cos/COSName;

.field public static final FIRST:Lorg/apache/pdfbox/cos/COSName;

.field public static final FIRST_CHAR:Lorg/apache/pdfbox/cos/COSName;

.field public static final FIT_WINDOW:Lorg/apache/pdfbox/cos/COSName;

.field public static final FL:Lorg/apache/pdfbox/cos/COSName;

.field public static final FLAGS:Lorg/apache/pdfbox/cos/COSName;

.field public static final FLATE_DECODE:Lorg/apache/pdfbox/cos/COSName;

.field public static final FLATE_DECODE_ABBREVIATION:Lorg/apache/pdfbox/cos/COSName;

.field public static final FONT:Lorg/apache/pdfbox/cos/COSName;

.field public static final FONT_BBOX:Lorg/apache/pdfbox/cos/COSName;

.field public static final FONT_DESC:Lorg/apache/pdfbox/cos/COSName;

.field public static final FONT_FAMILY:Lorg/apache/pdfbox/cos/COSName;

.field public static final FONT_FILE:Lorg/apache/pdfbox/cos/COSName;

.field public static final FONT_FILE2:Lorg/apache/pdfbox/cos/COSName;

.field public static final FONT_FILE3:Lorg/apache/pdfbox/cos/COSName;

.field public static final FONT_MATRIX:Lorg/apache/pdfbox/cos/COSName;

.field public static final FONT_NAME:Lorg/apache/pdfbox/cos/COSName;

.field public static final FONT_STRETCH:Lorg/apache/pdfbox/cos/COSName;

.field public static final FONT_WEIGHT:Lorg/apache/pdfbox/cos/COSName;

.field public static final FORM:Lorg/apache/pdfbox/cos/COSName;

.field public static final FORMTYPE:Lorg/apache/pdfbox/cos/COSName;

.field public static final FRM:Lorg/apache/pdfbox/cos/COSName;

.field public static final FT:Lorg/apache/pdfbox/cos/COSName;

.field public static final FUNCTION:Lorg/apache/pdfbox/cos/COSName;

.field public static final FUNCTIONS:Lorg/apache/pdfbox/cos/COSName;

.field public static final FUNCTION_TYPE:Lorg/apache/pdfbox/cos/COSName;

.field public static final F_DECODE_PARMS:Lorg/apache/pdfbox/cos/COSName;

.field public static final F_FILTER:Lorg/apache/pdfbox/cos/COSName;

.field public static final G:Lorg/apache/pdfbox/cos/COSName;

.field public static final GAMMA:Lorg/apache/pdfbox/cos/COSName;

.field public static final GROUP:Lorg/apache/pdfbox/cos/COSName;

.field public static final GTS_PDFA1:Lorg/apache/pdfbox/cos/COSName;

.field public static final H:Lorg/apache/pdfbox/cos/COSName;

.field public static final HARD_LIGHT:Lorg/apache/pdfbox/cos/COSName;

.field public static final HEIGHT:Lorg/apache/pdfbox/cos/COSName;

.field public static final HIDE_MENUBAR:Lorg/apache/pdfbox/cos/COSName;

.field public static final HIDE_TOOLBAR:Lorg/apache/pdfbox/cos/COSName;

.field public static final HIDE_WINDOWUI:Lorg/apache/pdfbox/cos/COSName;

.field public static final I:Lorg/apache/pdfbox/cos/COSName;

.field public static final IC:Lorg/apache/pdfbox/cos/COSName;

.field public static final ICCBASED:Lorg/apache/pdfbox/cos/COSName;

.field public static final ID:Lorg/apache/pdfbox/cos/COSName;

.field public static final IDENTITY:Lorg/apache/pdfbox/cos/COSName;

.field public static final IDENTITY_H:Lorg/apache/pdfbox/cos/COSName;

.field public static final ID_TREE:Lorg/apache/pdfbox/cos/COSName;

.field public static final IF:Lorg/apache/pdfbox/cos/COSName;

.field public static final IM:Lorg/apache/pdfbox/cos/COSName;

.field public static final IMAGE:Lorg/apache/pdfbox/cos/COSName;

.field public static final IMAGE_MASK:Lorg/apache/pdfbox/cos/COSName;

.field public static final INDEX:Lorg/apache/pdfbox/cos/COSName;

.field public static final INDEXED:Lorg/apache/pdfbox/cos/COSName;

.field public static final INFO:Lorg/apache/pdfbox/cos/COSName;

.field public static final INTERPOLATE:Lorg/apache/pdfbox/cos/COSName;

.field public static final ITALIC_ANGLE:Lorg/apache/pdfbox/cos/COSName;

.field public static final JAVA_SCRIPT:Lorg/apache/pdfbox/cos/COSName;

.field public static final JBIG2_DECODE:Lorg/apache/pdfbox/cos/COSName;

.field public static final JBIG2_GLOBALS:Lorg/apache/pdfbox/cos/COSName;

.field public static final JPX_DECODE:Lorg/apache/pdfbox/cos/COSName;

.field public static final K:Lorg/apache/pdfbox/cos/COSName;

.field public static final KEYWORDS:Lorg/apache/pdfbox/cos/COSName;

.field public static final KIDS:Lorg/apache/pdfbox/cos/COSName;

.field public static final L:Lorg/apache/pdfbox/cos/COSName;

.field public static final LAB:Lorg/apache/pdfbox/cos/COSName;

.field public static final LANG:Lorg/apache/pdfbox/cos/COSName;

.field public static final LAST:Lorg/apache/pdfbox/cos/COSName;

.field public static final LAST_CHAR:Lorg/apache/pdfbox/cos/COSName;

.field public static final LAST_MODIFIED:Lorg/apache/pdfbox/cos/COSName;

.field public static final LC:Lorg/apache/pdfbox/cos/COSName;

.field public static final LEADING:Lorg/apache/pdfbox/cos/COSName;

.field public static final LEGAL_ATTESTATION:Lorg/apache/pdfbox/cos/COSName;

.field public static final LENGTH:Lorg/apache/pdfbox/cos/COSName;

.field public static final LENGTH1:Lorg/apache/pdfbox/cos/COSName;

.field public static final LENGTH2:Lorg/apache/pdfbox/cos/COSName;

.field public static final LIGHTEN:Lorg/apache/pdfbox/cos/COSName;

.field public static final LIMITS:Lorg/apache/pdfbox/cos/COSName;

.field public static final LJ:Lorg/apache/pdfbox/cos/COSName;

.field public static final LOCATION:Lorg/apache/pdfbox/cos/COSName;

.field public static final LUMINOSITY:Lorg/apache/pdfbox/cos/COSName;

.field public static final LW:Lorg/apache/pdfbox/cos/COSName;

.field public static final LZW_DECODE:Lorg/apache/pdfbox/cos/COSName;

.field public static final LZW_DECODE_ABBREVIATION:Lorg/apache/pdfbox/cos/COSName;

.field public static final M:Lorg/apache/pdfbox/cos/COSName;

.field public static final MAC:Lorg/apache/pdfbox/cos/COSName;

.field public static final MAC_ROMAN_ENCODING:Lorg/apache/pdfbox/cos/COSName;

.field public static final MARK_INFO:Lorg/apache/pdfbox/cos/COSName;

.field public static final MASK:Lorg/apache/pdfbox/cos/COSName;

.field public static final MATRIX:Lorg/apache/pdfbox/cos/COSName;

.field public static final MAX_LEN:Lorg/apache/pdfbox/cos/COSName;

.field public static final MAX_WIDTH:Lorg/apache/pdfbox/cos/COSName;

.field public static final MCID:Lorg/apache/pdfbox/cos/COSName;

.field public static final MDP:Lorg/apache/pdfbox/cos/COSName;

.field public static final MEDIA_BOX:Lorg/apache/pdfbox/cos/COSName;

.field public static final METADATA:Lorg/apache/pdfbox/cos/COSName;

.field public static final MISSING_WIDTH:Lorg/apache/pdfbox/cos/COSName;

.field public static final MK:Lorg/apache/pdfbox/cos/COSName;

.field public static final ML:Lorg/apache/pdfbox/cos/COSName;

.field public static final MM_TYPE1:Lorg/apache/pdfbox/cos/COSName;

.field public static final MOD_DATE:Lorg/apache/pdfbox/cos/COSName;

.field public static final MULTIPLY:Lorg/apache/pdfbox/cos/COSName;

.field public static final N:Lorg/apache/pdfbox/cos/COSName;

.field public static final NAME:Lorg/apache/pdfbox/cos/COSName;

.field public static final NAMES:Lorg/apache/pdfbox/cos/COSName;

.field public static final NEED_APPEARANCES:Lorg/apache/pdfbox/cos/COSName;

.field public static final NEXT:Lorg/apache/pdfbox/cos/COSName;

.field public static final NM:Lorg/apache/pdfbox/cos/COSName;

.field public static final NONE:Lorg/apache/pdfbox/cos/COSName;

.field public static final NON_EFONT_NO_WARN:Lorg/apache/pdfbox/cos/COSName;

.field public static final NON_FULL_SCREEN_PAGE_MODE:Lorg/apache/pdfbox/cos/COSName;

.field public static final NORMAL:Lorg/apache/pdfbox/cos/COSName;

.field public static final NUMS:Lorg/apache/pdfbox/cos/COSName;

.field public static final O:Lorg/apache/pdfbox/cos/COSName;

.field public static final OBJ:Lorg/apache/pdfbox/cos/COSName;

.field public static final OBJ_STM:Lorg/apache/pdfbox/cos/COSName;

.field public static final OC:Lorg/apache/pdfbox/cos/COSName;

.field public static final OCG:Lorg/apache/pdfbox/cos/COSName;

.field public static final OCGS:Lorg/apache/pdfbox/cos/COSName;

.field public static final OCPROPERTIES:Lorg/apache/pdfbox/cos/COSName;

.field public static final OE:Lorg/apache/pdfbox/cos/COSName;

.field public static final OFF:Lorg/apache/pdfbox/cos/COSName;

.field public static final ON:Lorg/apache/pdfbox/cos/COSName;

.field public static final OP:Lorg/apache/pdfbox/cos/COSName;

.field public static final OPEN_ACTION:Lorg/apache/pdfbox/cos/COSName;

.field public static final OPEN_TYPE:Lorg/apache/pdfbox/cos/COSName;

.field public static final OPM:Lorg/apache/pdfbox/cos/COSName;

.field public static final OPT:Lorg/apache/pdfbox/cos/COSName;

.field public static final OP_NS:Lorg/apache/pdfbox/cos/COSName;

.field public static final ORDER:Lorg/apache/pdfbox/cos/COSName;

.field public static final ORDERING:Lorg/apache/pdfbox/cos/COSName;

.field public static final OS:Lorg/apache/pdfbox/cos/COSName;

.field public static final OUTLINES:Lorg/apache/pdfbox/cos/COSName;

.field public static final OUTPUT_CONDITION:Lorg/apache/pdfbox/cos/COSName;

.field public static final OUTPUT_CONDITION_IDENTIFIER:Lorg/apache/pdfbox/cos/COSName;

.field public static final OUTPUT_INTENT:Lorg/apache/pdfbox/cos/COSName;

.field public static final OUTPUT_INTENTS:Lorg/apache/pdfbox/cos/COSName;

.field public static final OVERLAY:Lorg/apache/pdfbox/cos/COSName;

.field public static final P:Lorg/apache/pdfbox/cos/COSName;

.field public static final PAGE:Lorg/apache/pdfbox/cos/COSName;

.field public static final PAGES:Lorg/apache/pdfbox/cos/COSName;

.field public static final PAGE_LABELS:Lorg/apache/pdfbox/cos/COSName;

.field public static final PAGE_LAYOUT:Lorg/apache/pdfbox/cos/COSName;

.field public static final PAGE_MODE:Lorg/apache/pdfbox/cos/COSName;

.field public static final PAINT_TYPE:Lorg/apache/pdfbox/cos/COSName;

.field public static final PARAMS:Lorg/apache/pdfbox/cos/COSName;

.field public static final PARENT:Lorg/apache/pdfbox/cos/COSName;

.field public static final PARENT_TREE:Lorg/apache/pdfbox/cos/COSName;

.field public static final PARENT_TREE_NEXT_KEY:Lorg/apache/pdfbox/cos/COSName;

.field public static final PATTERN:Lorg/apache/pdfbox/cos/COSName;

.field public static final PATTERN_TYPE:Lorg/apache/pdfbox/cos/COSName;

.field public static final PDF_DOC_ENCODING:Lorg/apache/pdfbox/cos/COSName;

.field public static final PERMS:Lorg/apache/pdfbox/cos/COSName;

.field public static final PG:Lorg/apache/pdfbox/cos/COSName;

.field public static final PREDICTOR:Lorg/apache/pdfbox/cos/COSName;

.field public static final PREV:Lorg/apache/pdfbox/cos/COSName;

.field public static final PRE_RELEASE:Lorg/apache/pdfbox/cos/COSName;

.field public static final PRINT_AREA:Lorg/apache/pdfbox/cos/COSName;

.field public static final PRINT_CLIP:Lorg/apache/pdfbox/cos/COSName;

.field public static final PRINT_SCALING:Lorg/apache/pdfbox/cos/COSName;

.field public static final PROCESS:Lorg/apache/pdfbox/cos/COSName;

.field public static final PROC_SET:Lorg/apache/pdfbox/cos/COSName;

.field public static final PRODUCER:Lorg/apache/pdfbox/cos/COSName;

.field public static final PROPERTIES:Lorg/apache/pdfbox/cos/COSName;

.field public static final PROP_BUILD:Lorg/apache/pdfbox/cos/COSName;

.field public static final PS:Lorg/apache/pdfbox/cos/COSName;

.field public static final PUB_SEC:Lorg/apache/pdfbox/cos/COSName;

.field public static final Q:Lorg/apache/pdfbox/cos/COSName;

.field public static final R:Lorg/apache/pdfbox/cos/COSName;

.field public static final RANGE:Lorg/apache/pdfbox/cos/COSName;

.field public static final REASON:Lorg/apache/pdfbox/cos/COSName;

.field public static final REASONS:Lorg/apache/pdfbox/cos/COSName;

.field public static final RECIPIENTS:Lorg/apache/pdfbox/cos/COSName;

.field public static final RECT:Lorg/apache/pdfbox/cos/COSName;

.field public static final REGISTRY:Lorg/apache/pdfbox/cos/COSName;

.field public static final REGISTRY_NAME:Lorg/apache/pdfbox/cos/COSName;

.field public static final RENAME:Lorg/apache/pdfbox/cos/COSName;

.field public static final RESOURCES:Lorg/apache/pdfbox/cos/COSName;

.field public static final RGB:Lorg/apache/pdfbox/cos/COSName;

.field public static final RI:Lorg/apache/pdfbox/cos/COSName;

.field public static final ROLE_MAP:Lorg/apache/pdfbox/cos/COSName;

.field public static final ROOT:Lorg/apache/pdfbox/cos/COSName;

.field public static final ROTATE:Lorg/apache/pdfbox/cos/COSName;

.field public static final ROWS:Lorg/apache/pdfbox/cos/COSName;

.field public static final RUN_LENGTH_DECODE:Lorg/apache/pdfbox/cos/COSName;

.field public static final RUN_LENGTH_DECODE_ABBREVIATION:Lorg/apache/pdfbox/cos/COSName;

.field public static final RV:Lorg/apache/pdfbox/cos/COSName;

.field public static final S:Lorg/apache/pdfbox/cos/COSName;

.field public static final SA:Lorg/apache/pdfbox/cos/COSName;

.field public static final SCREEN:Lorg/apache/pdfbox/cos/COSName;

.field public static final SE:Lorg/apache/pdfbox/cos/COSName;

.field public static final SEPARATION:Lorg/apache/pdfbox/cos/COSName;

.field public static final SET_F:Lorg/apache/pdfbox/cos/COSName;

.field public static final SET_FF:Lorg/apache/pdfbox/cos/COSName;

.field public static final SHADING:Lorg/apache/pdfbox/cos/COSName;

.field public static final SHADING_TYPE:Lorg/apache/pdfbox/cos/COSName;

.field public static final SIG:Lorg/apache/pdfbox/cos/COSName;

.field public static final SIG_FLAGS:Lorg/apache/pdfbox/cos/COSName;

.field public static final SIZE:Lorg/apache/pdfbox/cos/COSName;

.field public static final SM:Lorg/apache/pdfbox/cos/COSName;

.field public static final SMASK:Lorg/apache/pdfbox/cos/COSName;

.field public static final SOFT_LIGHT:Lorg/apache/pdfbox/cos/COSName;

.field public static final SS:Lorg/apache/pdfbox/cos/COSName;

.field public static final ST:Lorg/apache/pdfbox/cos/COSName;

.field public static final STANDARD_ENCODING:Lorg/apache/pdfbox/cos/COSName;

.field public static final STATUS:Lorg/apache/pdfbox/cos/COSName;

.field public static final STD_CF:Lorg/apache/pdfbox/cos/COSName;

.field public static final STEM_H:Lorg/apache/pdfbox/cos/COSName;

.field public static final STEM_V:Lorg/apache/pdfbox/cos/COSName;

.field public static final STM_F:Lorg/apache/pdfbox/cos/COSName;

.field public static final STRUCT_PARENT:Lorg/apache/pdfbox/cos/COSName;

.field public static final STRUCT_PARENTS:Lorg/apache/pdfbox/cos/COSName;

.field public static final STRUCT_TREE_ROOT:Lorg/apache/pdfbox/cos/COSName;

.field public static final STR_F:Lorg/apache/pdfbox/cos/COSName;

.field public static final SUBJ:Lorg/apache/pdfbox/cos/COSName;

.field public static final SUBJECT:Lorg/apache/pdfbox/cos/COSName;

.field public static final SUBTYPE:Lorg/apache/pdfbox/cos/COSName;

.field public static final SUB_FILTER:Lorg/apache/pdfbox/cos/COSName;

.field public static final SUPPLEMENT:Lorg/apache/pdfbox/cos/COSName;

.field public static final SV:Lorg/apache/pdfbox/cos/COSName;

.field public static final SW:Lorg/apache/pdfbox/cos/COSName;

.field public static final T:Lorg/apache/pdfbox/cos/COSName;

.field public static final TARGET:Lorg/apache/pdfbox/cos/COSName;

.field public static final TEMPLATES:Lorg/apache/pdfbox/cos/COSName;

.field public static final THREADS:Lorg/apache/pdfbox/cos/COSName;

.field public static final TI:Lorg/apache/pdfbox/cos/COSName;

.field public static final TILING_TYPE:Lorg/apache/pdfbox/cos/COSName;

.field public static final TIME_STAMP:Lorg/apache/pdfbox/cos/COSName;

.field public static final TITLE:Lorg/apache/pdfbox/cos/COSName;

.field public static final TK:Lorg/apache/pdfbox/cos/COSName;

.field public static final TM:Lorg/apache/pdfbox/cos/COSName;

.field public static final TO_UNICODE:Lorg/apache/pdfbox/cos/COSName;

.field public static final TR:Lorg/apache/pdfbox/cos/COSName;

.field public static final TRANS:Lorg/apache/pdfbox/cos/COSName;

.field public static final TRANSPARENCY:Lorg/apache/pdfbox/cos/COSName;

.field public static final TRAPPED:Lorg/apache/pdfbox/cos/COSName;

.field public static final TREF:Lorg/apache/pdfbox/cos/COSName;

.field public static final TRIM_BOX:Lorg/apache/pdfbox/cos/COSName;

.field public static final TRUE_TYPE:Lorg/apache/pdfbox/cos/COSName;

.field public static final TRUSTED_MODE:Lorg/apache/pdfbox/cos/COSName;

.field public static final TU:Lorg/apache/pdfbox/cos/COSName;

.field public static final TX:Lorg/apache/pdfbox/cos/COSName;

.field public static final TYPE:Lorg/apache/pdfbox/cos/COSName;

.field public static final TYPE0:Lorg/apache/pdfbox/cos/COSName;

.field public static final TYPE1:Lorg/apache/pdfbox/cos/COSName;

.field public static final TYPE3:Lorg/apache/pdfbox/cos/COSName;

.field public static final U:Lorg/apache/pdfbox/cos/COSName;

.field public static final UE:Lorg/apache/pdfbox/cos/COSName;

.field public static final UF:Lorg/apache/pdfbox/cos/COSName;

.field public static final UNCHANGED:Lorg/apache/pdfbox/cos/COSName;

.field public static final UNIX:Lorg/apache/pdfbox/cos/COSName;

.field public static final URI:Lorg/apache/pdfbox/cos/COSName;

.field public static final URL:Lorg/apache/pdfbox/cos/COSName;

.field public static final V:Lorg/apache/pdfbox/cos/COSName;

.field public static final VERISIGN_PPKVS:Lorg/apache/pdfbox/cos/COSName;

.field public static final VERSION:Lorg/apache/pdfbox/cos/COSName;

.field public static final VERTICES_PER_ROW:Lorg/apache/pdfbox/cos/COSName;

.field public static final VIEWER_PREFERENCES:Lorg/apache/pdfbox/cos/COSName;

.field public static final VIEW_AREA:Lorg/apache/pdfbox/cos/COSName;

.field public static final VIEW_CLIP:Lorg/apache/pdfbox/cos/COSName;

.field public static final W:Lorg/apache/pdfbox/cos/COSName;

.field public static final W2:Lorg/apache/pdfbox/cos/COSName;

.field public static final WHITE_POINT:Lorg/apache/pdfbox/cos/COSName;

.field public static final WIDTH:Lorg/apache/pdfbox/cos/COSName;

.field public static final WIDTHS:Lorg/apache/pdfbox/cos/COSName;

.field public static final WIN_ANSI_ENCODING:Lorg/apache/pdfbox/cos/COSName;

.field public static final XFA:Lorg/apache/pdfbox/cos/COSName;

.field public static final XHEIGHT:Lorg/apache/pdfbox/cos/COSName;

.field public static final XOBJECT:Lorg/apache/pdfbox/cos/COSName;

.field public static final XREF:Lorg/apache/pdfbox/cos/COSName;

.field public static final XREF_STM:Lorg/apache/pdfbox/cos/COSName;

.field public static final X_STEP:Lorg/apache/pdfbox/cos/COSName;

.field public static final Y_STEP:Lorg/apache/pdfbox/cos/COSName;

.field private static commonNameMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lorg/apache/pdfbox/cos/COSName;",
            ">;"
        }
    .end annotation
.end field

.field private static nameMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lorg/apache/pdfbox/cos/COSName;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final hashCode:I

.field private final name:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    const/16 v1, 0x2000

    invoke-direct {v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;-><init>(I)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->nameMap:Ljava/util/Map;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->commonNameMap:Ljava/util/Map;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "A"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->A:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "AA"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->AA:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "AcroForm"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->ACRO_FORM:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "ActualText"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->ACTUAL_TEXT:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "adbe.pkcs7.detached"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->ADBE_PKCS7_DETACHED:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "adbe.pkcs7.sha1"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->ADBE_PKCS7_SHA1:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "adbe.x509.rsa_sha1"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->ADBE_X509_RSA_SHA1:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Adobe.PPKLite"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->ADOBE_PPKLITE:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "AESV3"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->AESV3:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "After"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->AFTER:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "AIS"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->AIS:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Alt"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->ALT:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Alpha"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->ALPHA:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Alternate"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->ALTERNATE:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Annot"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->ANNOT:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Annots"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->ANNOTS:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "AntiAlias"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->ANTI_ALIAS:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "AP"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->AP:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "APRef"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->AP_REF:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "App"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->APP:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "ArtBox"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->ART_BOX:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Artifact"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->ARTIFACT:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "AS"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->AS:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Ascent"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->ASCENT:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "ASCIIHexDecode"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->ASCII_HEX_DECODE:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "AHx"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->ASCII_HEX_DECODE_ABBREVIATION:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "ASCII85Decode"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->ASCII85_DECODE:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "A85"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->ASCII85_DECODE_ABBREVIATION:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Attached"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->ATTACHED:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Author"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->AUTHOR:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "AvgWidth"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->AVG_WIDTH:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "B"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->B:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Background"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->BACKGROUND:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "BaseEncoding"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->BASE_ENCODING:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "BaseFont"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->BASE_FONT:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "BaseState"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->BASE_STATE:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "BBox"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->BBOX:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "BC"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->BC:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Before"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->BEFORE:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "BG"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->BG:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "BitsPerComponent"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->BITS_PER_COMPONENT:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "BitsPerCoordinate"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->BITS_PER_COORDINATE:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "BitsPerFlag"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->BITS_PER_FLAG:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "BitsPerSample"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->BITS_PER_SAMPLE:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "BlackIs1"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->BLACK_IS_1:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "BlackPoint"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->BLACK_POINT:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "BleedBox"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->BLEED_BOX:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "BM"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->BM:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Bounds"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->BOUNDS:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "BPC"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->BPC:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "BS"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->BS:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Btn"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->BTN:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "ByteRange"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->BYTERANGE:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "C"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->C:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "C0"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->C0:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "C1"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->C1:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "CA"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->CA:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "ca"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->CA_NS:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "CalGray"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->CALGRAY:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "CalRGB"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->CALRGB:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "CapHeight"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->CAP_HEIGHT:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Catalog"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->CATALOG:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "CCITTFaxDecode"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->CCITTFAX_DECODE:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "CCF"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->CCITTFAX_DECODE_ABBREVIATION:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "CenterWindow"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->CENTER_WINDOW:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "CF"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->CF:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "CFM"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->CFM:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Ch"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->CH:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "CharProcs"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->CHAR_PROCS:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "CharSet"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->CHAR_SET:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "CICI.SignIt"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->CICI_SIGNIT:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "CIDFontType0"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->CID_FONT_TYPE0:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "CIDFontType2"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->CID_FONT_TYPE2:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "CIDToGIDMap"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->CID_TO_GID_MAP:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "CIDSet"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->CID_SET:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "CIDSystemInfo"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->CIDSYSTEMINFO:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "ClrF"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->CLR_F:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "ClrFf"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->CLR_FF:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "CMap"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->CMAP:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "CMapName"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->CMAPNAME:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "CMYK"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->CMYK:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "ColorBurn"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->COLOR_BURN:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "ColorDodge"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->COLOR_DODGE:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Colorants"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->COLORANTS:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Colors"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->COLORS:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "ColorSpace"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->COLORSPACE:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Columns"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->COLUMNS:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Compatible"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->COMPATIBLE:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Components"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->COMPONENTS:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "ContactInfo"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->CONTACT_INFO:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Contents"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->CONTENTS:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Coords"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->COORDS:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Count"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->COUNT:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "CreationDate"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->CREATION_DATE:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Creator"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->CREATOR:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "CropBox"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->CROP_BOX:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Crypt"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->CRYPT:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "CS"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->CS:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "D"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->D:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "DA"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->DA:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Darken"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->DARKEN:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Date"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->DATE:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "DCTDecode"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->DCT_DECODE:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "DCT"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->DCT_DECODE_ABBREVIATION:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Decode"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->DECODE:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "DecodeParms"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->DECODE_PARMS:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "default"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->DEFAULT:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "DefaultCMYK"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->DEFAULT_CMYK:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "DefaultGray"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->DEFAULT_GRAY:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "DefaultRGB"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->DEFAULT_RGB:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Desc"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->DESC:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "DescendantFonts"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->DESCENDANT_FONTS:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Descent"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->DESCENT:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Dest"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->DEST:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "DestOutputProfile"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->DEST_OUTPUT_PROFILE:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Dests"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->DESTS:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "DeviceCMYK"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->DEVICECMYK:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "DeviceGray"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->DEVICEGRAY:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "DeviceN"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->DEVICEN:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "DeviceRGB"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->DEVICERGB:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Di"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->DI:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Difference"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->DIFFERENCE:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Differences"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->DIFFERENCES:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "DigestMethod"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->DIGEST_METHOD:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "RIPEMD160"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->DIGEST_RIPEMD160:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "SHA1"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->DIGEST_SHA1:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "SHA256"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->DIGEST_SHA256:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "SHA384"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->DIGEST_SHA384:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "SHA512"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->DIGEST_SHA512:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Direction"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->DIRECTION:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "DisplayDocTitle"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->DISPLAY_DOC_TITLE:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "DL"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->DL:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Dm"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->DM:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Doc"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->DOC:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "DocChecksum"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->DOC_CHECKSUM:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "DocTimeStamp"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->DOC_TIME_STAMP:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Domain"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->DOMAIN:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "DOS"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->DOS:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "DP"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->DP:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "DR"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->DR:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "DS"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->DS:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Duplex"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->DUPLEX:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Dur"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->DUR:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "DV"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->DV:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "DW"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->DW:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "DW2"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->DW2:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "E"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->E:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "EarlyChange"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->EARLY_CHANGE:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "EF"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->EF:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "EmbeddedFDFs"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->EMBEDDED_FDFS:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "EmbeddedFiles"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->EMBEDDED_FILES:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, ""

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->EMPTY:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Encode"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->ENCODE:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "EncodedByteAlign"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->ENCODED_BYTE_ALIGN:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Encoding"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->ENCODING:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "90ms-RKSJ-H"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->ENCODING_90MS_RKSJ_H:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "90ms-RKSJ-V"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->ENCODING_90MS_RKSJ_V:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "ETen-B5-H"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->ENCODING_ETEN_B5_H:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "ETen-B5-V"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->ENCODING_ETEN_B5_V:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Encrypt"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->ENCRYPT:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "EncryptMetadata"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->ENCRYPT_META_DATA:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "EndOfLine"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->END_OF_LINE:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Entrust.PPKEF"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->ENTRUST_PPKEF:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Exclusion"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->EXCLUSION:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "ExtGState"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->EXT_G_STATE:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Extend"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->EXTEND:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Extends"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->EXTENDS:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "F"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->F:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "FDecodeParms"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->F_DECODE_PARMS:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "FFilter"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->F_FILTER:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "FB"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->FB:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "FDF"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->FDF:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Ff"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->FF:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Fields"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->FIELDS:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Filespec"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->FILESPEC:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Filter"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->FILTER:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "First"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->FIRST:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "FirstChar"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->FIRST_CHAR:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "FitWindow"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->FIT_WINDOW:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "FL"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->FL:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Flags"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->FLAGS:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "FlateDecode"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->FLATE_DECODE:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Fl"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->FLATE_DECODE_ABBREVIATION:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Font"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->FONT:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "FontBBox"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->FONT_BBOX:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "FontDescriptor"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->FONT_DESC:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "FontFamily"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->FONT_FAMILY:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "FontFile"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->FONT_FILE:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "FontFile2"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->FONT_FILE2:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "FontFile3"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->FONT_FILE3:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "FontMatrix"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->FONT_MATRIX:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "FontName"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->FONT_NAME:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "FontStretch"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->FONT_STRETCH:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "FontWeight"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->FONT_WEIGHT:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Form"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->FORM:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "FormType"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->FORMTYPE:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "FRM"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->FRM:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "FT"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->FT:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Function"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->FUNCTION:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "FunctionType"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->FUNCTION_TYPE:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Functions"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->FUNCTIONS:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "G"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->G:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Gamma"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->GAMMA:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Group"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->GROUP:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "GTS_PDFA1"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->GTS_PDFA1:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "H"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->H:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "HardLight"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->HARD_LIGHT:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Height"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->HEIGHT:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "HideMenubar"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->HIDE_MENUBAR:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "HideToolbar"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->HIDE_TOOLBAR:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "HideWindowUI"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->HIDE_WINDOWUI:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "I"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->I:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "IC"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->IC:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "ICCBased"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->ICCBASED:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "ID"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->ID:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "IDTree"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->ID_TREE:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Identity"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->IDENTITY:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Identity-H"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->IDENTITY_H:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "IF"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->IF:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "IM"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->IM:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Image"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->IMAGE:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "ImageMask"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->IMAGE_MASK:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Index"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->INDEX:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Indexed"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->INDEXED:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Info"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->INFO:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Interpolate"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->INTERPOLATE:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "ItalicAngle"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->ITALIC_ANGLE:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "JavaScript"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->JAVA_SCRIPT:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "JBIG2Decode"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->JBIG2_DECODE:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "JBIG2Globals"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->JBIG2_GLOBALS:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "JPXDecode"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->JPX_DECODE:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "K"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->K:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Keywords"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->KEYWORDS:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Kids"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->KIDS:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "L"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->L:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Lab"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->LAB:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Lang"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->LANG:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Last"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->LAST:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "LastChar"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->LAST_CHAR:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "LastModified"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->LAST_MODIFIED:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "LC"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->LC:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Leading"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->LEADING:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "LegalAttestation"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->LEGAL_ATTESTATION:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Length"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->LENGTH:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Length1"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->LENGTH1:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Length2"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->LENGTH2:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Lighten"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->LIGHTEN:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Limits"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->LIMITS:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "LJ"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->LJ:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Location"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->LOCATION:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Luminosity"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->LUMINOSITY:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "LW"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->LW:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "LZWDecode"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->LZW_DECODE:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "LZW"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->LZW_DECODE_ABBREVIATION:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "M"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->M:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Mac"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->MAC:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "MacRomanEncoding"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->MAC_ROMAN_ENCODING:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "MarkInfo"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->MARK_INFO:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Mask"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->MASK:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Matrix"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->MATRIX:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "MaxLen"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->MAX_LEN:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "MaxWidth"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->MAX_WIDTH:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "MCID"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->MCID:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "MDP"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->MDP:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "MediaBox"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->MEDIA_BOX:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Metadata"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->METADATA:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "MissingWidth"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->MISSING_WIDTH:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "MK"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->MK:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "ML"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->ML:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "MMType1"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->MM_TYPE1:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "ModDate"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->MOD_DATE:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Multiply"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->MULTIPLY:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "N"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->N:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Name"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->NAME:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Names"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->NAMES:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "NeedAppearances"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->NEED_APPEARANCES:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Next"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->NEXT:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "NM"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->NM:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "NonEFontNoWarn"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->NON_EFONT_NO_WARN:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "NonFullScreenPageMode"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->NON_FULL_SCREEN_PAGE_MODE:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "None"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->NONE:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Normal"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->NORMAL:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Nums"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->NUMS:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "O"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->O:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Obj"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->OBJ:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "ObjStm"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->OBJ_STM:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "OC"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->OC:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "OCG"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->OCG:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "OCGs"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->OCGS:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "OCProperties"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->OCPROPERTIES:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "OE"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->OE:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "OFF"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->OFF:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "ON"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->ON:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "OP"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->OP:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "op"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->OP_NS:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "OpenAction"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->OPEN_ACTION:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "OpenType"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->OPEN_TYPE:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "OPM"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->OPM:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Opt"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->OPT:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Order"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->ORDER:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Ordering"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->ORDERING:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "OS"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->OS:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Outlines"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->OUTLINES:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "OutputCondition"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->OUTPUT_CONDITION:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "OutputConditionIdentifier"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->OUTPUT_CONDITION_IDENTIFIER:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "OutputIntent"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->OUTPUT_INTENT:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "OutputIntents"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->OUTPUT_INTENTS:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Overlay"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->OVERLAY:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "P"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->P:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Page"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->PAGE:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "PageLabels"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->PAGE_LABELS:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "PageLayout"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->PAGE_LAYOUT:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "PageMode"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->PAGE_MODE:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Pages"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->PAGES:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "PaintType"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->PAINT_TYPE:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Params"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->PARAMS:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Parent"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->PARENT:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "ParentTree"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->PARENT_TREE:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "ParentTreeNextKey"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->PARENT_TREE_NEXT_KEY:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Pattern"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->PATTERN:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "PatternType"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->PATTERN_TYPE:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "PDFDocEncoding"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->PDF_DOC_ENCODING:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Perms"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->PERMS:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Pg"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->PG:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "PreRelease"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->PRE_RELEASE:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Predictor"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->PREDICTOR:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Prev"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->PREV:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "PrintArea"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->PRINT_AREA:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "PrintClip"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->PRINT_CLIP:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "PrintScaling"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->PRINT_SCALING:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "ProcSet"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->PROC_SET:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Process"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->PROCESS:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Producer"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->PRODUCER:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Prop_Build"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->PROP_BUILD:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Properties"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->PROPERTIES:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "PS"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->PS:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "PubSec"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->PUB_SEC:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Q"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->Q:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "R"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->R:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Range"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->RANGE:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Reason"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->REASON:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Reasons"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->REASONS:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Recipients"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->RECIPIENTS:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Rect"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->RECT:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Registry"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->REGISTRY:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "RegistryName"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->REGISTRY_NAME:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Rename"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->RENAME:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Resources"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->RESOURCES:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "RGB"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->RGB:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "RI"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->RI:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "RoleMap"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->ROLE_MAP:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Root"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->ROOT:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Rotate"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->ROTATE:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Rows"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->ROWS:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "RunLengthDecode"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->RUN_LENGTH_DECODE:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "RL"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->RUN_LENGTH_DECODE_ABBREVIATION:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "RV"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->RV:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "S"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->S:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "SA"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->SA:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Screen"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->SCREEN:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "SE"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->SE:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Separation"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->SEPARATION:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "SetF"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->SET_F:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "SetFf"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->SET_FF:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Shading"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->SHADING:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "ShadingType"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->SHADING_TYPE:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Sig"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->SIG:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "SigFlags"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->SIG_FLAGS:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Size"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->SIZE:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "SM"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->SM:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "SMask"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->SMASK:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "SoftLight"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->SOFT_LIGHT:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "SS"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->SS:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "St"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->ST:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "StandardEncoding"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->STANDARD_ENCODING:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Status"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->STATUS:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "StdCF"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->STD_CF:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "StemH"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->STEM_H:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "StemV"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->STEM_V:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "StmF"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->STM_F:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "StrF"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->STR_F:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "StructParent"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->STRUCT_PARENT:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "StructParents"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->STRUCT_PARENTS:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "StructTreeRoot"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->STRUCT_TREE_ROOT:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "SubFilter"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->SUB_FILTER:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Subj"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->SUBJ:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Subject"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->SUBJECT:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Subtype"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->SUBTYPE:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Supplement"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->SUPPLEMENT:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "SV"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->SV:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "SW"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->SW:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "T"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->T:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Target"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->TARGET:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Templates"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->TEMPLATES:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Threads"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->THREADS:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "TI"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->TI:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "TilingType"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->TILING_TYPE:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "TimeStamp"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->TIME_STAMP:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Title"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->TITLE:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "TK"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->TK:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "TM"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->TM:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "ToUnicode"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->TO_UNICODE:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "TR"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->TR:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Trapped"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->TRAPPED:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Trans"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->TRANS:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Transparency"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->TRANSPARENCY:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "TRef"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->TREF:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "TrimBox"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->TRIM_BOX:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "TrueType"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->TRUE_TYPE:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "TrustedMode"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->TRUSTED_MODE:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "TU"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->TU:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Tx"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->TX:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Type"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->TYPE:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Type0"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->TYPE0:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Type1"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->TYPE1:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Type3"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->TYPE3:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "U"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->U:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "UE"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->UE:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "UF"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->UF:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Unchanged"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->UNCHANGED:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Unix"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->UNIX:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "URI"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->URI:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "URL"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->URL:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "V"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->V:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "VeriSign.PPKVS"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->VERISIGN_PPKVS:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Version"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->VERSION:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "VerticesPerRow"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->VERTICES_PER_ROW:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "ViewArea"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->VIEW_AREA:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "ViewClip"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->VIEW_CLIP:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "ViewerPreferences"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->VIEWER_PREFERENCES:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "W"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->W:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "W2"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->W2:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "WhitePoint"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->WHITE_POINT:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Width"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->WIDTH:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Widths"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->WIDTHS:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "WinAnsiEncoding"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->WIN_ANSI_ENCODING:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "XFA"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->XFA:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "XStep"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->X_STEP:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "XHeight"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->XHEIGHT:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "XObject"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->XOBJECT:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "XRef"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->XREF:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "XRefStm"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->XREF_STM:Lorg/apache/pdfbox/cos/COSName;

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "YStep"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSName;->Y_STEP:Lorg/apache/pdfbox/cos/COSName;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;)V
    .locals 1

    .line 1
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;Z)V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Z)V
    .locals 0

    .line 2
    invoke-direct {p0}, Lorg/apache/pdfbox/cos/COSBase;-><init>()V

    iput-object p1, p0, Lorg/apache/pdfbox/cos/COSName;->name:Ljava/lang/String;

    if-eqz p2, :cond_0

    sget-object p2, Lorg/apache/pdfbox/cos/COSName;->commonNameMap:Ljava/util/Map;

    goto :goto_0

    :cond_0
    sget-object p2, Lorg/apache/pdfbox/cos/COSName;->nameMap:Ljava/util/Map;

    :goto_0
    invoke-interface {p2, p1, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result p1

    iput p1, p0, Lorg/apache/pdfbox/cos/COSName;->hashCode:I

    return-void
.end method

.method public static declared-synchronized clearResources()V
    .locals 2

    const-class v0, Lorg/apache/pdfbox/cos/COSName;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->nameMap:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->clear()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public static getPDFName(Ljava/lang/String;)Lorg/apache/pdfbox/cos/COSName;
    .locals 2

    if-eqz p0, :cond_0

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->commonNameMap:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSName;

    if-nez v0, :cond_1

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->nameMap:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSName;

    if-nez v0, :cond_1

    new-instance v0, Lorg/apache/pdfbox/cos/COSName;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lorg/apache/pdfbox/cos/COSName;-><init>(Ljava/lang/String;Z)V

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :cond_1
    :goto_0
    return-object v0
.end method


# virtual methods
.method public accept(Lorg/apache/pdfbox/cos/ICOSVisitor;)Ljava/lang/Object;
    .locals 0

    invoke-interface {p1, p0}, Lorg/apache/pdfbox/cos/ICOSVisitor;->visitFromName(Lorg/apache/pdfbox/cos/COSName;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 0

    .line 1
    check-cast p1, Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/cos/COSName;->compareTo(Lorg/apache/pdfbox/cos/COSName;)I

    move-result p1

    return p1
.end method

.method public compareTo(Lorg/apache/pdfbox/cos/COSName;)I
    .locals 1

    .line 2
    iget-object v0, p0, Lorg/apache/pdfbox/cos/COSName;->name:Ljava/lang/String;

    iget-object p1, p1, Lorg/apache/pdfbox/cos/COSName;->name:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result p1

    return p1
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    instance-of v0, p1, Lorg/apache/pdfbox/cos/COSName;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/pdfbox/cos/COSName;->name:Ljava/lang/String;

    check-cast p1, Lorg/apache/pdfbox/cos/COSName;

    iget-object p1, p1, Lorg/apache/pdfbox/cos/COSName;->name:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/cos/COSName;->name:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    iget v0, p0, Lorg/apache/pdfbox/cos/COSName;->hashCode:I

    return v0
.end method

.method public isEmpty()Z
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/cos/COSName;->name:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "COSName{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lorg/apache/pdfbox/cos/COSName;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writePDF(Ljava/io/OutputStream;)V
    .locals 5

    const/16 v0, 0x2f

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write(I)V

    invoke-virtual {p0}, Lorg/apache/pdfbox/cos/COSName;->getName()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/util/Charsets;->US_ASCII:Ljava/nio/charset/Charset;

    invoke-virtual {v0, v1}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_5

    aget-byte v3, v0, v2

    add-int/lit16 v3, v3, 0x100

    rem-int/lit16 v3, v3, 0x100

    const/16 v4, 0x41

    if-lt v3, v4, :cond_0

    const/16 v4, 0x5a

    if-le v3, v4, :cond_4

    :cond_0
    const/16 v4, 0x61

    if-lt v3, v4, :cond_1

    const/16 v4, 0x7a

    if-le v3, v4, :cond_4

    :cond_1
    const/16 v4, 0x30

    if-lt v3, v4, :cond_2

    const/16 v4, 0x39

    if-le v3, v4, :cond_4

    :cond_2
    const/16 v4, 0x2b

    if-eq v3, v4, :cond_4

    const/16 v4, 0x2d

    if-eq v3, v4, :cond_4

    const/16 v4, 0x5f

    if-eq v3, v4, :cond_4

    const/16 v4, 0x40

    if-eq v3, v4, :cond_4

    const/16 v4, 0x2a

    if-eq v3, v4, :cond_4

    const/16 v4, 0x24

    if-eq v3, v4, :cond_4

    const/16 v4, 0x3b

    if-eq v3, v4, :cond_4

    const/16 v4, 0x2e

    if-ne v3, v4, :cond_3

    goto :goto_1

    :cond_3
    const/16 v4, 0x23

    invoke-virtual {p1, v4}, Ljava/io/OutputStream;->write(I)V

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    filled-new-array {v3}, [Ljava/lang/Object;

    move-result-object v3

    const-string v4, "%02X"

    invoke-static {v4, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lorg/apache/pdfbox/util/Charsets;->US_ASCII:Ljava/nio/charset/Charset;

    invoke-virtual {v3, v4}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/io/OutputStream;->write([B)V

    goto :goto_2

    :cond_4
    :goto_1
    invoke-virtual {p1, v3}, Ljava/io/OutputStream;->write(I)V

    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_5
    return-void
.end method

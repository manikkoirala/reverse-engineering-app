.class public final Lorg/apache/pdfbox/cos/COSInteger;
.super Lorg/apache/pdfbox/cos/COSNumber;
.source "SourceFile"


# static fields
.field private static final HIGH:I = 0x100

.field private static final LOW:I = -0x64

.field public static final ONE:Lorg/apache/pdfbox/cos/COSInteger;

.field private static final STATIC:[Lorg/apache/pdfbox/cos/COSInteger;

.field public static final THREE:Lorg/apache/pdfbox/cos/COSInteger;

.field public static final TWO:Lorg/apache/pdfbox/cos/COSInteger;

.field public static final ZERO:Lorg/apache/pdfbox/cos/COSInteger;


# instance fields
.field private final value:J


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    const/16 v0, 0x165

    new-array v0, v0, [Lorg/apache/pdfbox/cos/COSInteger;

    sput-object v0, Lorg/apache/pdfbox/cos/COSInteger;->STATIC:[Lorg/apache/pdfbox/cos/COSInteger;

    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, Lorg/apache/pdfbox/cos/COSInteger;->get(J)Lorg/apache/pdfbox/cos/COSInteger;

    move-result-object v0

    sput-object v0, Lorg/apache/pdfbox/cos/COSInteger;->ZERO:Lorg/apache/pdfbox/cos/COSInteger;

    const-wide/16 v0, 0x1

    invoke-static {v0, v1}, Lorg/apache/pdfbox/cos/COSInteger;->get(J)Lorg/apache/pdfbox/cos/COSInteger;

    move-result-object v0

    sput-object v0, Lorg/apache/pdfbox/cos/COSInteger;->ONE:Lorg/apache/pdfbox/cos/COSInteger;

    const-wide/16 v0, 0x2

    invoke-static {v0, v1}, Lorg/apache/pdfbox/cos/COSInteger;->get(J)Lorg/apache/pdfbox/cos/COSInteger;

    move-result-object v0

    sput-object v0, Lorg/apache/pdfbox/cos/COSInteger;->TWO:Lorg/apache/pdfbox/cos/COSInteger;

    const-wide/16 v0, 0x3

    invoke-static {v0, v1}, Lorg/apache/pdfbox/cos/COSInteger;->get(J)Lorg/apache/pdfbox/cos/COSInteger;

    move-result-object v0

    sput-object v0, Lorg/apache/pdfbox/cos/COSInteger;->THREE:Lorg/apache/pdfbox/cos/COSInteger;

    return-void
.end method

.method private constructor <init>(J)V
    .locals 0

    invoke-direct {p0}, Lorg/apache/pdfbox/cos/COSNumber;-><init>()V

    iput-wide p1, p0, Lorg/apache/pdfbox/cos/COSInteger;->value:J

    return-void
.end method

.method public static get(J)Lorg/apache/pdfbox/cos/COSInteger;
    .locals 3

    const-wide/16 v0, -0x64

    cmp-long v0, v0, p0

    if-gtz v0, :cond_1

    const-wide/16 v0, 0x100

    cmp-long v0, p0, v0

    if-gtz v0, :cond_1

    long-to-int v0, p0

    add-int/lit8 v0, v0, 0x64

    sget-object v1, Lorg/apache/pdfbox/cos/COSInteger;->STATIC:[Lorg/apache/pdfbox/cos/COSInteger;

    aget-object v2, v1, v0

    if-nez v2, :cond_0

    new-instance v2, Lorg/apache/pdfbox/cos/COSInteger;

    invoke-direct {v2, p0, p1}, Lorg/apache/pdfbox/cos/COSInteger;-><init>(J)V

    aput-object v2, v1, v0

    :cond_0
    aget-object p0, v1, v0

    return-object p0

    :cond_1
    new-instance v0, Lorg/apache/pdfbox/cos/COSInteger;

    invoke-direct {v0, p0, p1}, Lorg/apache/pdfbox/cos/COSInteger;-><init>(J)V

    return-object v0
.end method


# virtual methods
.method public accept(Lorg/apache/pdfbox/cos/ICOSVisitor;)Ljava/lang/Object;
    .locals 0

    invoke-interface {p1, p0}, Lorg/apache/pdfbox/cos/ICOSVisitor;->visitFromInt(Lorg/apache/pdfbox/cos/COSInteger;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public doubleValue()D
    .locals 2

    iget-wide v0, p0, Lorg/apache/pdfbox/cos/COSInteger;->value:J

    long-to-double v0, v0

    return-wide v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    instance-of v0, p1, Lorg/apache/pdfbox/cos/COSInteger;

    if-eqz v0, :cond_0

    check-cast p1, Lorg/apache/pdfbox/cos/COSInteger;

    invoke-virtual {p1}, Lorg/apache/pdfbox/cos/COSInteger;->intValue()I

    move-result p1

    invoke-virtual {p0}, Lorg/apache/pdfbox/cos/COSInteger;->intValue()I

    move-result v0

    if-ne p1, v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public floatValue()F
    .locals 2

    iget-wide v0, p0, Lorg/apache/pdfbox/cos/COSInteger;->value:J

    long-to-float v0, v0

    return v0
.end method

.method public hashCode()I
    .locals 4

    iget-wide v0, p0, Lorg/apache/pdfbox/cos/COSInteger;->value:J

    const/16 v2, 0x20

    shr-long v2, v0, v2

    xor-long/2addr v0, v2

    long-to-int v0, v0

    return v0
.end method

.method public intValue()I
    .locals 2

    iget-wide v0, p0, Lorg/apache/pdfbox/cos/COSInteger;->value:J

    long-to-int v0, v0

    return v0
.end method

.method public longValue()J
    .locals 2

    iget-wide v0, p0, Lorg/apache/pdfbox/cos/COSInteger;->value:J

    return-wide v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "COSInt{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lorg/apache/pdfbox/cos/COSInteger;->value:J

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writePDF(Ljava/io/OutputStream;)V
    .locals 2

    iget-wide v0, p0, Lorg/apache/pdfbox/cos/COSInteger;->value:J

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    const-string v1, "ISO-8859-1"

    invoke-virtual {v0, v1}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    return-void
.end method

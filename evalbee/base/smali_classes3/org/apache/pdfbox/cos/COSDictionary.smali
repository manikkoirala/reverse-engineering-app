.class public Lorg/apache/pdfbox/cos/COSDictionary;
.super Lorg/apache/pdfbox/cos/COSBase;
.source "SourceFile"

# interfaces
.implements Lorg/apache/pdfbox/cos/COSUpdateInfo;


# static fields
.field private static final PATH_SEPARATOR:Ljava/lang/String; = "/"


# instance fields
.field protected items:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lorg/apache/pdfbox/cos/COSName;",
            "Lorg/apache/pdfbox/cos/COSBase;",
            ">;"
        }
    .end annotation
.end field

.field private needToBeUpdated:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Lorg/apache/pdfbox/cos/COSBase;-><init>()V

    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/pdfbox/cos/COSDictionary;->items:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>(Lorg/apache/pdfbox/cos/COSDictionary;)V
    .locals 1

    .line 2
    invoke-direct {p0}, Lorg/apache/pdfbox/cos/COSBase;-><init>()V

    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/pdfbox/cos/COSDictionary;->items:Ljava/util/Map;

    iget-object p1, p1, Lorg/apache/pdfbox/cos/COSDictionary;->items:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    return-void
.end method


# virtual methods
.method public accept(Lorg/apache/pdfbox/cos/ICOSVisitor;)Ljava/lang/Object;
    .locals 0

    invoke-interface {p1, p0}, Lorg/apache/pdfbox/cos/ICOSVisitor;->visitFromDictionary(Lorg/apache/pdfbox/cos/COSDictionary;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public addAll(Lorg/apache/pdfbox/cos/COSDictionary;)V
    .locals 3

    invoke-virtual {p1}, Lorg/apache/pdfbox/cos/COSDictionary;->entrySet()Ljava/util/Set;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v1}, Lorg/apache/pdfbox/cos/COSName;->getName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Size"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lorg/apache/pdfbox/cos/COSDictionary;->items:Ljava/util/Map;

    invoke-static {v2}, Lorg/apache/pdfbox/cos/COSName;->getPDFName(Ljava/lang/String;)Lorg/apache/pdfbox/cos/COSName;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    :cond_1
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/pdfbox/cos/COSName;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSBase;

    invoke-virtual {p0, v1, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    goto :goto_0

    :cond_2
    return-void
.end method

.method public asUnmodifiableDictionary()Lorg/apache/pdfbox/cos/COSDictionary;
    .locals 1

    new-instance v0, Lorg/apache/pdfbox/cos/UnmodifiableCOSDictionary;

    invoke-direct {v0, p0}, Lorg/apache/pdfbox/cos/UnmodifiableCOSDictionary;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    return-object v0
.end method

.method public clear()V
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/cos/COSDictionary;->items:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    return-void
.end method

.method public containsKey(Ljava/lang/String;)Z
    .locals 0

    .line 1
    invoke-static {p1}, Lorg/apache/pdfbox/cos/COSName;->getPDFName(Ljava/lang/String;)Lorg/apache/pdfbox/cos/COSName;

    move-result-object p1

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->containsKey(Lorg/apache/pdfbox/cos/COSName;)Z

    move-result p1

    return p1
.end method

.method public containsKey(Lorg/apache/pdfbox/cos/COSName;)Z
    .locals 1

    .line 2
    iget-object v0, p0, Lorg/apache/pdfbox/cos/COSDictionary;->items:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public containsValue(Ljava/lang/Object;)Z
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/cos/COSDictionary;->items:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsValue(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    instance-of v1, p1, Lorg/apache/pdfbox/cos/COSObject;

    if-eqz v1, :cond_0

    iget-object v0, p0, Lorg/apache/pdfbox/cos/COSDictionary;->items:Ljava/util/Map;

    check-cast p1, Lorg/apache/pdfbox/cos/COSObject;

    invoke-virtual {p1}, Lorg/apache/pdfbox/cos/COSObject;->getObject()Lorg/apache/pdfbox/cos/COSBase;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Map;->containsValue(Ljava/lang/Object;)Z

    move-result v0

    :cond_0
    return v0
.end method

.method public entrySet()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Ljava/util/Map$Entry<",
            "Lorg/apache/pdfbox/cos/COSName;",
            "Lorg/apache/pdfbox/cos/COSBase;",
            ">;>;"
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/pdfbox/cos/COSDictionary;->items:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public getBoolean(Ljava/lang/String;Z)Z
    .locals 0

    .line 1
    invoke-static {p1}, Lorg/apache/pdfbox/cos/COSName;->getPDFName(Ljava/lang/String;)Lorg/apache/pdfbox/cos/COSName;

    move-result-object p1

    invoke-virtual {p0, p1, p2}, Lorg/apache/pdfbox/cos/COSDictionary;->getBoolean(Lorg/apache/pdfbox/cos/COSName;Z)Z

    move-result p1

    return p1
.end method

.method public getBoolean(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSName;Z)Z
    .locals 0

    .line 2
    invoke-virtual {p0, p1, p2}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object p1

    instance-of p2, p1, Lorg/apache/pdfbox/cos/COSBoolean;

    if-eqz p2, :cond_0

    check-cast p1, Lorg/apache/pdfbox/cos/COSBoolean;

    invoke-virtual {p1}, Lorg/apache/pdfbox/cos/COSBoolean;->getValue()Z

    move-result p3

    :cond_0
    return p3
.end method

.method public getBoolean(Lorg/apache/pdfbox/cos/COSName;Z)Z
    .locals 1

    .line 3
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, p2}, Lorg/apache/pdfbox/cos/COSDictionary;->getBoolean(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSName;Z)Z

    move-result p1

    return p1
.end method

.method public getCOSName(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSName;
    .locals 1

    .line 1
    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object p1

    instance-of v0, p1, Lorg/apache/pdfbox/cos/COSName;

    if-eqz v0, :cond_0

    check-cast p1, Lorg/apache/pdfbox/cos/COSName;

    return-object p1

    :cond_0
    const/4 p1, 0x0

    return-object p1
.end method

.method public getCOSName(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSName;
    .locals 1

    .line 2
    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object p1

    instance-of v0, p1, Lorg/apache/pdfbox/cos/COSName;

    if-eqz v0, :cond_0

    check-cast p1, Lorg/apache/pdfbox/cos/COSName;

    return-object p1

    :cond_0
    return-object p2
.end method

.method public getDate(Ljava/lang/String;)Ljava/util/Calendar;
    .locals 0

    .line 1
    invoke-static {p1}, Lorg/apache/pdfbox/cos/COSName;->getPDFName(Ljava/lang/String;)Lorg/apache/pdfbox/cos/COSName;

    move-result-object p1

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDate(Lorg/apache/pdfbox/cos/COSName;)Ljava/util/Calendar;

    move-result-object p1

    return-object p1
.end method

.method public getDate(Ljava/lang/String;Ljava/util/Calendar;)Ljava/util/Calendar;
    .locals 0

    .line 2
    invoke-static {p1}, Lorg/apache/pdfbox/cos/COSName;->getPDFName(Ljava/lang/String;)Lorg/apache/pdfbox/cos/COSName;

    move-result-object p1

    invoke-virtual {p0, p1, p2}, Lorg/apache/pdfbox/cos/COSDictionary;->getDate(Lorg/apache/pdfbox/cos/COSName;Ljava/util/Calendar;)Ljava/util/Calendar;

    move-result-object p1

    return-object p1
.end method

.method public getDate(Lorg/apache/pdfbox/cos/COSName;)Ljava/util/Calendar;
    .locals 0

    .line 3
    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object p1

    check-cast p1, Lorg/apache/pdfbox/cos/COSString;

    invoke-static {p1}, Lorg/apache/pdfbox/util/DateConverter;->toCalendar(Lorg/apache/pdfbox/cos/COSString;)Ljava/util/Calendar;

    move-result-object p1

    return-object p1
.end method

.method public getDate(Lorg/apache/pdfbox/cos/COSName;Ljava/util/Calendar;)Ljava/util/Calendar;
    .locals 0

    .line 4
    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDate(Lorg/apache/pdfbox/cos/COSName;)Ljava/util/Calendar;

    move-result-object p1

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    move-object p2, p1

    :goto_0
    return-object p2
.end method

.method public getDictionaryObject(Ljava/lang/String;)Lorg/apache/pdfbox/cos/COSBase;
    .locals 0

    .line 1
    invoke-static {p1}, Lorg/apache/pdfbox/cos/COSName;->getPDFName(Ljava/lang/String;)Lorg/apache/pdfbox/cos/COSName;

    move-result-object p1

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object p1

    return-object p1
.end method

.method public getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;
    .locals 1

    .line 2
    iget-object v0, p0, Lorg/apache/pdfbox/cos/COSDictionary;->items:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lorg/apache/pdfbox/cos/COSBase;

    instance-of v0, p1, Lorg/apache/pdfbox/cos/COSObject;

    if-eqz v0, :cond_0

    check-cast p1, Lorg/apache/pdfbox/cos/COSObject;

    invoke-virtual {p1}, Lorg/apache/pdfbox/cos/COSObject;->getObject()Lorg/apache/pdfbox/cos/COSBase;

    move-result-object p1

    :cond_0
    instance-of v0, p1, Lorg/apache/pdfbox/cos/COSNull;

    if-eqz v0, :cond_1

    const/4 p1, 0x0

    :cond_1
    return-object p1
.end method

.method public getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;
    .locals 0

    .line 3
    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object p1

    if-nez p1, :cond_0

    if-eqz p2, :cond_0

    invoke-virtual {p0, p2}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object p1

    :cond_0
    return-object p1
.end method

.method public getDictionaryObject([Ljava/lang/String;)Lorg/apache/pdfbox/cos/COSBase;
    .locals 3

    .line 4
    const/4 v0, 0x0

    const/4 v1, 0x0

    :goto_0
    array-length v2, p1

    if-ge v1, v2, :cond_0

    if-nez v0, :cond_0

    aget-object v0, p1, v1

    invoke-static {v0}, Lorg/apache/pdfbox/cos/COSName;->getPDFName(Ljava/lang/String;)Lorg/apache/pdfbox/cos/COSName;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method public getEmbeddedDate(Ljava/lang/String;Ljava/lang/String;)Ljava/util/Calendar;
    .locals 1

    .line 1
    invoke-static {p2}, Lorg/apache/pdfbox/cos/COSName;->getPDFName(Ljava/lang/String;)Lorg/apache/pdfbox/cos/COSName;

    move-result-object p2

    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->getEmbeddedDate(Ljava/lang/String;Lorg/apache/pdfbox/cos/COSName;Ljava/util/Calendar;)Ljava/util/Calendar;

    move-result-object p1

    return-object p1
.end method

.method public getEmbeddedDate(Ljava/lang/String;Ljava/lang/String;Ljava/util/Calendar;)Ljava/util/Calendar;
    .locals 0

    .line 2
    invoke-static {p2}, Lorg/apache/pdfbox/cos/COSName;->getPDFName(Ljava/lang/String;)Lorg/apache/pdfbox/cos/COSName;

    move-result-object p2

    invoke-virtual {p0, p1, p2, p3}, Lorg/apache/pdfbox/cos/COSDictionary;->getEmbeddedDate(Ljava/lang/String;Lorg/apache/pdfbox/cos/COSName;Ljava/util/Calendar;)Ljava/util/Calendar;

    move-result-object p1

    return-object p1
.end method

.method public getEmbeddedDate(Ljava/lang/String;Lorg/apache/pdfbox/cos/COSName;)Ljava/util/Calendar;
    .locals 1

    .line 3
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->getEmbeddedDate(Ljava/lang/String;Lorg/apache/pdfbox/cos/COSName;Ljava/util/Calendar;)Ljava/util/Calendar;

    move-result-object p1

    return-object p1
.end method

.method public getEmbeddedDate(Ljava/lang/String;Lorg/apache/pdfbox/cos/COSName;Ljava/util/Calendar;)Ljava/util/Calendar;
    .locals 0

    .line 4
    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Ljava/lang/String;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object p1

    check-cast p1, Lorg/apache/pdfbox/cos/COSDictionary;

    if-eqz p1, :cond_0

    invoke-virtual {p1, p2, p3}, Lorg/apache/pdfbox/cos/COSDictionary;->getDate(Lorg/apache/pdfbox/cos/COSName;Ljava/util/Calendar;)Ljava/util/Calendar;

    move-result-object p3

    :cond_0
    return-object p3
.end method

.method public getEmbeddedInt(Ljava/lang/String;Ljava/lang/String;)I
    .locals 0

    .line 1
    invoke-static {p2}, Lorg/apache/pdfbox/cos/COSName;->getPDFName(Ljava/lang/String;)Lorg/apache/pdfbox/cos/COSName;

    move-result-object p2

    invoke-virtual {p0, p1, p2}, Lorg/apache/pdfbox/cos/COSDictionary;->getEmbeddedInt(Ljava/lang/String;Lorg/apache/pdfbox/cos/COSName;)I

    move-result p1

    return p1
.end method

.method public getEmbeddedInt(Ljava/lang/String;Ljava/lang/String;I)I
    .locals 0

    .line 2
    invoke-static {p2}, Lorg/apache/pdfbox/cos/COSName;->getPDFName(Ljava/lang/String;)Lorg/apache/pdfbox/cos/COSName;

    move-result-object p2

    invoke-virtual {p0, p1, p2, p3}, Lorg/apache/pdfbox/cos/COSDictionary;->getEmbeddedInt(Ljava/lang/String;Lorg/apache/pdfbox/cos/COSName;I)I

    move-result p1

    return p1
.end method

.method public getEmbeddedInt(Ljava/lang/String;Lorg/apache/pdfbox/cos/COSName;)I
    .locals 1

    .line 3
    const/4 v0, -0x1

    invoke-virtual {p0, p1, p2, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->getEmbeddedInt(Ljava/lang/String;Lorg/apache/pdfbox/cos/COSName;I)I

    move-result p1

    return p1
.end method

.method public getEmbeddedInt(Ljava/lang/String;Lorg/apache/pdfbox/cos/COSName;I)I
    .locals 0

    .line 4
    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Ljava/lang/String;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object p1

    check-cast p1, Lorg/apache/pdfbox/cos/COSDictionary;

    if-eqz p1, :cond_0

    invoke-virtual {p1, p2, p3}, Lorg/apache/pdfbox/cos/COSDictionary;->getInt(Lorg/apache/pdfbox/cos/COSName;I)I

    move-result p3

    :cond_0
    return p3
.end method

.method public getEmbeddedString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .line 1
    invoke-static {p2}, Lorg/apache/pdfbox/cos/COSName;->getPDFName(Ljava/lang/String;)Lorg/apache/pdfbox/cos/COSName;

    move-result-object p2

    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->getEmbeddedString(Ljava/lang/String;Lorg/apache/pdfbox/cos/COSName;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getEmbeddedString(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 2
    invoke-static {p2}, Lorg/apache/pdfbox/cos/COSName;->getPDFName(Ljava/lang/String;)Lorg/apache/pdfbox/cos/COSName;

    move-result-object p2

    invoke-virtual {p0, p1, p2, p3}, Lorg/apache/pdfbox/cos/COSDictionary;->getEmbeddedString(Ljava/lang/String;Lorg/apache/pdfbox/cos/COSName;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getEmbeddedString(Ljava/lang/String;Lorg/apache/pdfbox/cos/COSName;)Ljava/lang/String;
    .locals 1

    .line 3
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->getEmbeddedString(Ljava/lang/String;Lorg/apache/pdfbox/cos/COSName;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getEmbeddedString(Ljava/lang/String;Lorg/apache/pdfbox/cos/COSName;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 4
    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Ljava/lang/String;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object p1

    check-cast p1, Lorg/apache/pdfbox/cos/COSDictionary;

    if-eqz p1, :cond_0

    invoke-virtual {p1, p2, p3}, Lorg/apache/pdfbox/cos/COSDictionary;->getString(Lorg/apache/pdfbox/cos/COSName;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p3

    :cond_0
    return-object p3
.end method

.method public getFlag(Lorg/apache/pdfbox/cos/COSName;I)Z
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->getInt(Lorg/apache/pdfbox/cos/COSName;I)I

    move-result p1

    and-int/2addr p1, p2

    if-ne p1, p2, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public getFloat(Ljava/lang/String;)F
    .locals 1

    .line 1
    invoke-static {p1}, Lorg/apache/pdfbox/cos/COSName;->getPDFName(Ljava/lang/String;)Lorg/apache/pdfbox/cos/COSName;

    move-result-object p1

    const/high16 v0, -0x40800000    # -1.0f

    invoke-virtual {p0, p1, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->getFloat(Lorg/apache/pdfbox/cos/COSName;F)F

    move-result p1

    return p1
.end method

.method public getFloat(Ljava/lang/String;F)F
    .locals 0

    .line 2
    invoke-static {p1}, Lorg/apache/pdfbox/cos/COSName;->getPDFName(Ljava/lang/String;)Lorg/apache/pdfbox/cos/COSName;

    move-result-object p1

    invoke-virtual {p0, p1, p2}, Lorg/apache/pdfbox/cos/COSDictionary;->getFloat(Lorg/apache/pdfbox/cos/COSName;F)F

    move-result p1

    return p1
.end method

.method public getFloat(Lorg/apache/pdfbox/cos/COSName;)F
    .locals 1

    .line 3
    const/high16 v0, -0x40800000    # -1.0f

    invoke-virtual {p0, p1, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->getFloat(Lorg/apache/pdfbox/cos/COSName;F)F

    move-result p1

    return p1
.end method

.method public getFloat(Lorg/apache/pdfbox/cos/COSName;F)F
    .locals 1

    .line 4
    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object p1

    instance-of v0, p1, Lorg/apache/pdfbox/cos/COSNumber;

    if-eqz v0, :cond_0

    check-cast p1, Lorg/apache/pdfbox/cos/COSNumber;

    invoke-virtual {p1}, Lorg/apache/pdfbox/cos/COSNumber;->floatValue()F

    move-result p2

    :cond_0
    return p2
.end method

.method public getInt(Ljava/lang/String;)I
    .locals 1

    .line 1
    invoke-static {p1}, Lorg/apache/pdfbox/cos/COSName;->getPDFName(Ljava/lang/String;)Lorg/apache/pdfbox/cos/COSName;

    move-result-object p1

    const/4 v0, -0x1

    invoke-virtual {p0, p1, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->getInt(Lorg/apache/pdfbox/cos/COSName;I)I

    move-result p1

    return p1
.end method

.method public getInt(Ljava/lang/String;I)I
    .locals 0

    .line 2
    invoke-static {p1}, Lorg/apache/pdfbox/cos/COSName;->getPDFName(Ljava/lang/String;)Lorg/apache/pdfbox/cos/COSName;

    move-result-object p1

    invoke-virtual {p0, p1, p2}, Lorg/apache/pdfbox/cos/COSDictionary;->getInt(Lorg/apache/pdfbox/cos/COSName;I)I

    move-result p1

    return p1
.end method

.method public getInt(Lorg/apache/pdfbox/cos/COSName;)I
    .locals 1

    .line 3
    const/4 v0, -0x1

    invoke-virtual {p0, p1, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->getInt(Lorg/apache/pdfbox/cos/COSName;I)I

    move-result p1

    return p1
.end method

.method public getInt(Lorg/apache/pdfbox/cos/COSName;I)I
    .locals 1

    .line 4
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, p2}, Lorg/apache/pdfbox/cos/COSDictionary;->getInt(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSName;I)I

    move-result p1

    return p1
.end method

.method public getInt(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSName;)I
    .locals 1

    .line 5
    const/4 v0, -0x1

    invoke-virtual {p0, p1, p2, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->getInt(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSName;I)I

    move-result p1

    return p1
.end method

.method public getInt(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSName;I)I
    .locals 0

    .line 6
    invoke-virtual {p0, p1, p2}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object p1

    instance-of p2, p1, Lorg/apache/pdfbox/cos/COSNumber;

    if-eqz p2, :cond_0

    check-cast p1, Lorg/apache/pdfbox/cos/COSNumber;

    invoke-virtual {p1}, Lorg/apache/pdfbox/cos/COSNumber;->intValue()I

    move-result p3

    :cond_0
    return p3
.end method

.method public getInt([Ljava/lang/String;I)I
    .locals 1

    .line 7
    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject([Ljava/lang/String;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object p1

    instance-of v0, p1, Lorg/apache/pdfbox/cos/COSNumber;

    if-eqz v0, :cond_0

    check-cast p1, Lorg/apache/pdfbox/cos/COSNumber;

    invoke-virtual {p1}, Lorg/apache/pdfbox/cos/COSNumber;->intValue()I

    move-result p2

    :cond_0
    return p2
.end method

.method public getItem(Ljava/lang/String;)Lorg/apache/pdfbox/cos/COSBase;
    .locals 0

    .line 1
    invoke-static {p1}, Lorg/apache/pdfbox/cos/COSName;->getPDFName(Ljava/lang/String;)Lorg/apache/pdfbox/cos/COSName;

    move-result-object p1

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->getItem(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object p1

    return-object p1
.end method

.method public getItem(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;
    .locals 1

    .line 2
    iget-object v0, p0, Lorg/apache/pdfbox/cos/COSDictionary;->items:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lorg/apache/pdfbox/cos/COSBase;

    return-object p1
.end method

.method public getKeyForValue(Ljava/lang/Object;)Lorg/apache/pdfbox/cos/COSName;
    .locals 4

    iget-object v0, p0, Lorg/apache/pdfbox/cos/COSDictionary;->items:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    instance-of v3, v2, Lorg/apache/pdfbox/cos/COSObject;

    if-eqz v3, :cond_0

    check-cast v2, Lorg/apache/pdfbox/cos/COSObject;

    invoke-virtual {v2}, Lorg/apache/pdfbox/cos/COSObject;->getObject()Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_1
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lorg/apache/pdfbox/cos/COSName;

    return-object p1

    :cond_2
    const/4 p1, 0x0

    return-object p1
.end method

.method public getLong(Ljava/lang/String;)J
    .locals 2

    .line 1
    invoke-static {p1}, Lorg/apache/pdfbox/cos/COSName;->getPDFName(Ljava/lang/String;)Lorg/apache/pdfbox/cos/COSName;

    move-result-object p1

    const-wide/16 v0, -0x1

    invoke-virtual {p0, p1, v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getLong(Lorg/apache/pdfbox/cos/COSName;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public getLong(Ljava/lang/String;J)J
    .locals 0

    .line 2
    invoke-static {p1}, Lorg/apache/pdfbox/cos/COSName;->getPDFName(Ljava/lang/String;)Lorg/apache/pdfbox/cos/COSName;

    move-result-object p1

    invoke-virtual {p0, p1, p2, p3}, Lorg/apache/pdfbox/cos/COSDictionary;->getLong(Lorg/apache/pdfbox/cos/COSName;J)J

    move-result-wide p1

    return-wide p1
.end method

.method public getLong(Lorg/apache/pdfbox/cos/COSName;)J
    .locals 2

    .line 3
    const-wide/16 v0, -0x1

    invoke-virtual {p0, p1, v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getLong(Lorg/apache/pdfbox/cos/COSName;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public getLong(Lorg/apache/pdfbox/cos/COSName;J)J
    .locals 1

    .line 4
    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object p1

    instance-of v0, p1, Lorg/apache/pdfbox/cos/COSNumber;

    if-eqz v0, :cond_0

    check-cast p1, Lorg/apache/pdfbox/cos/COSNumber;

    invoke-virtual {p1}, Lorg/apache/pdfbox/cos/COSNumber;->longValue()J

    move-result-wide p2

    :cond_0
    return-wide p2
.end method

.method public getLong([Ljava/lang/String;J)J
    .locals 1

    .line 5
    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject([Ljava/lang/String;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object p1

    instance-of v0, p1, Lorg/apache/pdfbox/cos/COSNumber;

    if-eqz v0, :cond_0

    check-cast p1, Lorg/apache/pdfbox/cos/COSNumber;

    invoke-virtual {p1}, Lorg/apache/pdfbox/cos/COSNumber;->longValue()J

    move-result-wide p2

    :cond_0
    return-wide p2
.end method

.method public getNameAsString(Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 1
    invoke-static {p1}, Lorg/apache/pdfbox/cos/COSName;->getPDFName(Ljava/lang/String;)Lorg/apache/pdfbox/cos/COSName;

    move-result-object p1

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->getNameAsString(Lorg/apache/pdfbox/cos/COSName;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getNameAsString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 2
    invoke-static {p1}, Lorg/apache/pdfbox/cos/COSName;->getPDFName(Ljava/lang/String;)Lorg/apache/pdfbox/cos/COSName;

    move-result-object p1

    invoke-virtual {p0, p1, p2}, Lorg/apache/pdfbox/cos/COSDictionary;->getNameAsString(Lorg/apache/pdfbox/cos/COSName;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getNameAsString(Lorg/apache/pdfbox/cos/COSName;)Ljava/lang/String;
    .locals 1

    .line 3
    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object p1

    instance-of v0, p1, Lorg/apache/pdfbox/cos/COSName;

    if-eqz v0, :cond_0

    check-cast p1, Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p1}, Lorg/apache/pdfbox/cos/COSName;->getName()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_0
    instance-of v0, p1, Lorg/apache/pdfbox/cos/COSString;

    if-eqz v0, :cond_1

    check-cast p1, Lorg/apache/pdfbox/cos/COSString;

    invoke-virtual {p1}, Lorg/apache/pdfbox/cos/COSString;->getString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    :goto_0
    return-object p1
.end method

.method public getNameAsString(Lorg/apache/pdfbox/cos/COSName;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 4
    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->getNameAsString(Lorg/apache/pdfbox/cos/COSName;)Ljava/lang/String;

    move-result-object p1

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    move-object p2, p1

    :goto_0
    return-object p2
.end method

.method public getObjectFromPath(Ljava/lang/String;)Lorg/apache/pdfbox/cos/COSBase;
    .locals 6

    const-string v0, "/"

    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object p1

    array-length v0, p1

    const/4 v1, 0x0

    move-object v2, p0

    :goto_0
    if-ge v1, v0, :cond_2

    aget-object v3, p1, v1

    instance-of v4, v2, Lorg/apache/pdfbox/cos/COSArray;

    if-eqz v4, :cond_0

    const-string v4, "\\["

    const-string v5, ""

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "\\]"

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    check-cast v2, Lorg/apache/pdfbox/cos/COSArray;

    invoke-virtual {v2, v3}, Lorg/apache/pdfbox/cos/COSArray;->getObject(I)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v2

    goto :goto_1

    :cond_0
    instance-of v4, v2, Lorg/apache/pdfbox/cos/COSDictionary;

    if-eqz v4, :cond_1

    check-cast v2, Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-virtual {v2, v3}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Ljava/lang/String;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v2

    :cond_1
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    return-object v2
.end method

.method public getString(Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 1
    invoke-static {p1}, Lorg/apache/pdfbox/cos/COSName;->getPDFName(Ljava/lang/String;)Lorg/apache/pdfbox/cos/COSName;

    move-result-object p1

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->getString(Lorg/apache/pdfbox/cos/COSName;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 2
    invoke-static {p1}, Lorg/apache/pdfbox/cos/COSName;->getPDFName(Ljava/lang/String;)Lorg/apache/pdfbox/cos/COSName;

    move-result-object p1

    invoke-virtual {p0, p1, p2}, Lorg/apache/pdfbox/cos/COSDictionary;->getString(Lorg/apache/pdfbox/cos/COSName;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getString(Lorg/apache/pdfbox/cos/COSName;)Ljava/lang/String;
    .locals 1

    .line 3
    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object p1

    instance-of v0, p1, Lorg/apache/pdfbox/cos/COSString;

    if-eqz v0, :cond_0

    check-cast p1, Lorg/apache/pdfbox/cos/COSString;

    invoke-virtual {p1}, Lorg/apache/pdfbox/cos/COSString;->getString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return-object p1
.end method

.method public getString(Lorg/apache/pdfbox/cos/COSName;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .line 4
    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->getString(Lorg/apache/pdfbox/cos/COSName;)Ljava/lang/String;

    move-result-object p1

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    move-object p2, p1

    :goto_0
    return-object p2
.end method

.method public getValues()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection<",
            "Lorg/apache/pdfbox/cos/COSBase;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/pdfbox/cos/COSDictionary;->items:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method public isNeedToBeUpdated()Z
    .locals 1

    iget-boolean v0, p0, Lorg/apache/pdfbox/cos/COSDictionary;->needToBeUpdated:Z

    return v0
.end method

.method public keySet()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lorg/apache/pdfbox/cos/COSName;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/pdfbox/cos/COSDictionary;->items:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public mergeInto(Lorg/apache/pdfbox/cos/COSDictionary;)V
    .locals 2

    invoke-virtual {p1}, Lorg/apache/pdfbox/cos/COSDictionary;->entrySet()Ljava/util/Set;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getItem(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/pdfbox/cos/COSName;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSBase;

    invoke-virtual {p0, v1, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public removeItem(Lorg/apache/pdfbox/cos/COSName;)V
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/cos/COSDictionary;->items:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public setBoolean(Ljava/lang/String;Z)V
    .locals 0

    .line 1
    invoke-static {p1}, Lorg/apache/pdfbox/cos/COSName;->getPDFName(Ljava/lang/String;)Lorg/apache/pdfbox/cos/COSName;

    move-result-object p1

    invoke-static {p2}, Lorg/apache/pdfbox/cos/COSBoolean;->getBoolean(Z)Lorg/apache/pdfbox/cos/COSBoolean;

    move-result-object p2

    invoke-virtual {p0, p1, p2}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    return-void
.end method

.method public setBoolean(Lorg/apache/pdfbox/cos/COSName;Z)V
    .locals 0

    .line 2
    invoke-static {p2}, Lorg/apache/pdfbox/cos/COSBoolean;->getBoolean(Z)Lorg/apache/pdfbox/cos/COSBoolean;

    move-result-object p2

    invoke-virtual {p0, p1, p2}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    return-void
.end method

.method public setDate(Ljava/lang/String;Ljava/util/Calendar;)V
    .locals 0

    .line 1
    invoke-static {p1}, Lorg/apache/pdfbox/cos/COSName;->getPDFName(Ljava/lang/String;)Lorg/apache/pdfbox/cos/COSName;

    move-result-object p1

    invoke-virtual {p0, p1, p2}, Lorg/apache/pdfbox/cos/COSDictionary;->setDate(Lorg/apache/pdfbox/cos/COSName;Ljava/util/Calendar;)V

    return-void
.end method

.method public setDate(Lorg/apache/pdfbox/cos/COSName;Ljava/util/Calendar;)V
    .locals 0

    .line 2
    invoke-static {p2}, Lorg/apache/pdfbox/util/DateConverter;->toString(Ljava/util/Calendar;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p0, p1, p2}, Lorg/apache/pdfbox/cos/COSDictionary;->setString(Lorg/apache/pdfbox/cos/COSName;Ljava/lang/String;)V

    return-void
.end method

.method public setEmbeddedDate(Ljava/lang/String;Ljava/lang/String;Ljava/util/Calendar;)V
    .locals 0

    .line 1
    invoke-static {p2}, Lorg/apache/pdfbox/cos/COSName;->getPDFName(Ljava/lang/String;)Lorg/apache/pdfbox/cos/COSName;

    move-result-object p2

    invoke-virtual {p0, p1, p2, p3}, Lorg/apache/pdfbox/cos/COSDictionary;->setEmbeddedDate(Ljava/lang/String;Lorg/apache/pdfbox/cos/COSName;Ljava/util/Calendar;)V

    return-void
.end method

.method public setEmbeddedDate(Ljava/lang/String;Lorg/apache/pdfbox/cos/COSName;Ljava/util/Calendar;)V
    .locals 1

    .line 2
    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Ljava/lang/String;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSDictionary;

    if-nez v0, :cond_0

    if-eqz p3, :cond_0

    new-instance v0, Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-direct {v0}, Lorg/apache/pdfbox/cos/COSDictionary;-><init>()V

    invoke-virtual {p0, p1, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Ljava/lang/String;Lorg/apache/pdfbox/cos/COSBase;)V

    :cond_0
    if-eqz v0, :cond_1

    invoke-virtual {v0, p2, p3}, Lorg/apache/pdfbox/cos/COSDictionary;->setDate(Lorg/apache/pdfbox/cos/COSName;Ljava/util/Calendar;)V

    :cond_1
    return-void
.end method

.method public setEmbeddedInt(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 0

    .line 1
    invoke-static {p2}, Lorg/apache/pdfbox/cos/COSName;->getPDFName(Ljava/lang/String;)Lorg/apache/pdfbox/cos/COSName;

    move-result-object p2

    invoke-virtual {p0, p1, p2, p3}, Lorg/apache/pdfbox/cos/COSDictionary;->setEmbeddedInt(Ljava/lang/String;Lorg/apache/pdfbox/cos/COSName;I)V

    return-void
.end method

.method public setEmbeddedInt(Ljava/lang/String;Lorg/apache/pdfbox/cos/COSName;I)V
    .locals 1

    .line 2
    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Ljava/lang/String;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSDictionary;

    if-nez v0, :cond_0

    new-instance v0, Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-direct {v0}, Lorg/apache/pdfbox/cos/COSDictionary;-><init>()V

    invoke-virtual {p0, p1, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Ljava/lang/String;Lorg/apache/pdfbox/cos/COSBase;)V

    :cond_0
    invoke-virtual {v0, p2, p3}, Lorg/apache/pdfbox/cos/COSDictionary;->setInt(Lorg/apache/pdfbox/cos/COSName;I)V

    return-void
.end method

.method public setEmbeddedString(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-static {p2}, Lorg/apache/pdfbox/cos/COSName;->getPDFName(Ljava/lang/String;)Lorg/apache/pdfbox/cos/COSName;

    move-result-object p2

    invoke-virtual {p0, p1, p2, p3}, Lorg/apache/pdfbox/cos/COSDictionary;->setEmbeddedString(Ljava/lang/String;Lorg/apache/pdfbox/cos/COSName;Ljava/lang/String;)V

    return-void
.end method

.method public setEmbeddedString(Ljava/lang/String;Lorg/apache/pdfbox/cos/COSName;Ljava/lang/String;)V
    .locals 1

    .line 2
    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Ljava/lang/String;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSDictionary;

    if-nez v0, :cond_0

    if-eqz p3, :cond_0

    new-instance v0, Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-direct {v0}, Lorg/apache/pdfbox/cos/COSDictionary;-><init>()V

    invoke-virtual {p0, p1, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Ljava/lang/String;Lorg/apache/pdfbox/cos/COSBase;)V

    :cond_0
    if-eqz v0, :cond_1

    invoke-virtual {v0, p2, p3}, Lorg/apache/pdfbox/cos/COSDictionary;->setString(Lorg/apache/pdfbox/cos/COSName;Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method public setFlag(Lorg/apache/pdfbox/cos/COSName;IZ)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->getInt(Lorg/apache/pdfbox/cos/COSName;I)I

    move-result v0

    if-eqz p3, :cond_0

    or-int/2addr p2, v0

    goto :goto_0

    :cond_0
    not-int p2, p2

    and-int/2addr p2, v0

    :goto_0
    invoke-virtual {p0, p1, p2}, Lorg/apache/pdfbox/cos/COSDictionary;->setInt(Lorg/apache/pdfbox/cos/COSName;I)V

    return-void
.end method

.method public setFloat(Ljava/lang/String;F)V
    .locals 0

    .line 1
    invoke-static {p1}, Lorg/apache/pdfbox/cos/COSName;->getPDFName(Ljava/lang/String;)Lorg/apache/pdfbox/cos/COSName;

    move-result-object p1

    invoke-virtual {p0, p1, p2}, Lorg/apache/pdfbox/cos/COSDictionary;->setFloat(Lorg/apache/pdfbox/cos/COSName;F)V

    return-void
.end method

.method public setFloat(Lorg/apache/pdfbox/cos/COSName;F)V
    .locals 1

    .line 2
    new-instance v0, Lorg/apache/pdfbox/cos/COSFloat;

    invoke-direct {v0, p2}, Lorg/apache/pdfbox/cos/COSFloat;-><init>(F)V

    invoke-virtual {p0, p1, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    return-void
.end method

.method public setInt(Ljava/lang/String;I)V
    .locals 0

    .line 1
    invoke-static {p1}, Lorg/apache/pdfbox/cos/COSName;->getPDFName(Ljava/lang/String;)Lorg/apache/pdfbox/cos/COSName;

    move-result-object p1

    invoke-virtual {p0, p1, p2}, Lorg/apache/pdfbox/cos/COSDictionary;->setInt(Lorg/apache/pdfbox/cos/COSName;I)V

    return-void
.end method

.method public setInt(Lorg/apache/pdfbox/cos/COSName;I)V
    .locals 2

    .line 2
    int-to-long v0, p2

    invoke-static {v0, v1}, Lorg/apache/pdfbox/cos/COSInteger;->get(J)Lorg/apache/pdfbox/cos/COSInteger;

    move-result-object p2

    invoke-virtual {p0, p1, p2}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    return-void
.end method

.method public setItem(Ljava/lang/String;Lorg/apache/pdfbox/cos/COSBase;)V
    .locals 0

    .line 1
    invoke-static {p1}, Lorg/apache/pdfbox/cos/COSName;->getPDFName(Ljava/lang/String;)Lorg/apache/pdfbox/cos/COSName;

    move-result-object p1

    invoke-virtual {p0, p1, p2}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    return-void
.end method

.method public setItem(Ljava/lang/String;Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)V
    .locals 0

    .line 2
    invoke-static {p1}, Lorg/apache/pdfbox/cos/COSName;->getPDFName(Ljava/lang/String;)Lorg/apache/pdfbox/cos/COSName;

    move-result-object p1

    invoke-virtual {p0, p1, p2}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)V

    return-void
.end method

.method public setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V
    .locals 1

    .line 3
    if-nez p2, :cond_0

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->removeItem(Lorg/apache/pdfbox/cos/COSName;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lorg/apache/pdfbox/cos/COSDictionary;->items:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_0
    return-void
.end method

.method public setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)V
    .locals 0

    .line 4
    if-eqz p2, :cond_0

    invoke-interface {p2}, Lorg/apache/pdfbox/pdmodel/common/COSObjectable;->getCOSObject()Lorg/apache/pdfbox/cos/COSBase;

    move-result-object p2

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    :goto_0
    invoke-virtual {p0, p1, p2}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    return-void
.end method

.method public setLong(Ljava/lang/String;J)V
    .locals 0

    .line 1
    invoke-static {p1}, Lorg/apache/pdfbox/cos/COSName;->getPDFName(Ljava/lang/String;)Lorg/apache/pdfbox/cos/COSName;

    move-result-object p1

    invoke-virtual {p0, p1, p2, p3}, Lorg/apache/pdfbox/cos/COSDictionary;->setLong(Lorg/apache/pdfbox/cos/COSName;J)V

    return-void
.end method

.method public setLong(Lorg/apache/pdfbox/cos/COSName;J)V
    .locals 0

    .line 2
    invoke-static {p2, p3}, Lorg/apache/pdfbox/cos/COSInteger;->get(J)Lorg/apache/pdfbox/cos/COSInteger;

    move-result-object p2

    invoke-virtual {p0, p1, p2}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    return-void
.end method

.method public setName(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-static {p1}, Lorg/apache/pdfbox/cos/COSName;->getPDFName(Ljava/lang/String;)Lorg/apache/pdfbox/cos/COSName;

    move-result-object p1

    invoke-virtual {p0, p1, p2}, Lorg/apache/pdfbox/cos/COSDictionary;->setName(Lorg/apache/pdfbox/cos/COSName;Ljava/lang/String;)V

    return-void
.end method

.method public setName(Lorg/apache/pdfbox/cos/COSName;Ljava/lang/String;)V
    .locals 0

    .line 2
    if-eqz p2, :cond_0

    invoke-static {p2}, Lorg/apache/pdfbox/cos/COSName;->getPDFName(Ljava/lang/String;)Lorg/apache/pdfbox/cos/COSName;

    move-result-object p2

    goto :goto_0

    :cond_0
    const/4 p2, 0x0

    :goto_0
    invoke-virtual {p0, p1, p2}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    return-void
.end method

.method public setNeedToBeUpdated(Z)V
    .locals 0

    iput-boolean p1, p0, Lorg/apache/pdfbox/cos/COSDictionary;->needToBeUpdated:Z

    return-void
.end method

.method public setString(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-static {p1}, Lorg/apache/pdfbox/cos/COSName;->getPDFName(Ljava/lang/String;)Lorg/apache/pdfbox/cos/COSName;

    move-result-object p1

    invoke-virtual {p0, p1, p2}, Lorg/apache/pdfbox/cos/COSDictionary;->setString(Lorg/apache/pdfbox/cos/COSName;Ljava/lang/String;)V

    return-void
.end method

.method public setString(Lorg/apache/pdfbox/cos/COSName;Ljava/lang/String;)V
    .locals 1

    .line 2
    if-eqz p2, :cond_0

    new-instance v0, Lorg/apache/pdfbox/cos/COSString;

    invoke-direct {v0, p2}, Lorg/apache/pdfbox/cos/COSString;-><init>(Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, p1, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    return-void
.end method

.method public size()I
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/cos/COSDictionary;->items:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "COSDictionary{"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lorg/apache/pdfbox/cos/COSDictionary;->items:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/pdfbox/cos/COSName;

    const-string v3, "("

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v3, ":"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {p0, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    :cond_0
    const-string v2, "<null>"

    :goto_1
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, ") "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_1
    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

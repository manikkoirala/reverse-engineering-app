.class public Lorg/apache/pdfbox/cos/COSObject;
.super Lorg/apache/pdfbox/cos/COSBase;
.source "SourceFile"

# interfaces
.implements Lorg/apache/pdfbox/cos/COSUpdateInfo;


# instance fields
.field private baseObject:Lorg/apache/pdfbox/cos/COSBase;

.field private generationNumber:I

.field private needToBeUpdated:Z

.field private objectNumber:J


# direct methods
.method public constructor <init>(Lorg/apache/pdfbox/cos/COSBase;)V
    .locals 0

    invoke-direct {p0}, Lorg/apache/pdfbox/cos/COSBase;-><init>()V

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/cos/COSObject;->setObject(Lorg/apache/pdfbox/cos/COSBase;)V

    return-void
.end method


# virtual methods
.method public accept(Lorg/apache/pdfbox/cos/ICOSVisitor;)Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lorg/apache/pdfbox/cos/COSObject;->getObject()Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/pdfbox/cos/COSObject;->getObject()Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/pdfbox/cos/COSBase;->accept(Lorg/apache/pdfbox/cos/ICOSVisitor;)Ljava/lang/Object;

    move-result-object p1

    goto :goto_0

    :cond_0
    sget-object v0, Lorg/apache/pdfbox/cos/COSNull;->NULL:Lorg/apache/pdfbox/cos/COSNull;

    invoke-virtual {v0, p1}, Lorg/apache/pdfbox/cos/COSNull;->accept(Lorg/apache/pdfbox/cos/ICOSVisitor;)Ljava/lang/Object;

    move-result-object p1

    :goto_0
    return-object p1
.end method

.method public getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/cos/COSObject;->baseObject:Lorg/apache/pdfbox/cos/COSBase;

    instance-of v1, v0, Lorg/apache/pdfbox/cos/COSDictionary;

    if-eqz v1, :cond_0

    check-cast v0, Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-virtual {v0, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return-object p1
.end method

.method public getGenerationNumber()I
    .locals 1

    iget v0, p0, Lorg/apache/pdfbox/cos/COSObject;->generationNumber:I

    return v0
.end method

.method public getItem(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/cos/COSObject;->baseObject:Lorg/apache/pdfbox/cos/COSBase;

    instance-of v1, v0, Lorg/apache/pdfbox/cos/COSDictionary;

    if-eqz v1, :cond_0

    check-cast v0, Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-virtual {v0, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->getItem(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return-object p1
.end method

.method public getObject()Lorg/apache/pdfbox/cos/COSBase;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/cos/COSObject;->baseObject:Lorg/apache/pdfbox/cos/COSBase;

    return-object v0
.end method

.method public getObjectNumber()J
    .locals 2

    iget-wide v0, p0, Lorg/apache/pdfbox/cos/COSObject;->objectNumber:J

    return-wide v0
.end method

.method public isNeedToBeUpdated()Z
    .locals 1

    iget-boolean v0, p0, Lorg/apache/pdfbox/cos/COSObject;->needToBeUpdated:Z

    return v0
.end method

.method public setGenerationNumber(I)V
    .locals 0

    iput p1, p0, Lorg/apache/pdfbox/cos/COSObject;->generationNumber:I

    return-void
.end method

.method public setNeedToBeUpdated(Z)V
    .locals 0

    iput-boolean p1, p0, Lorg/apache/pdfbox/cos/COSObject;->needToBeUpdated:Z

    return-void
.end method

.method public final setObject(Lorg/apache/pdfbox/cos/COSBase;)V
    .locals 0

    iput-object p1, p0, Lorg/apache/pdfbox/cos/COSObject;->baseObject:Lorg/apache/pdfbox/cos/COSBase;

    return-void
.end method

.method public setObjectNumber(J)V
    .locals 0

    iput-wide p1, p0, Lorg/apache/pdfbox/cos/COSObject;->objectNumber:J

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "COSObject{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v1, p0, Lorg/apache/pdfbox/cos/COSObject;->objectNumber:J

    invoke-static {v1, v2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lorg/apache/pdfbox/cos/COSObject;->generationNumber:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

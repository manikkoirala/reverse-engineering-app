.class public final Lorg/apache/pdfbox/cos/COSBoolean;
.super Lorg/apache/pdfbox/cos/COSBase;
.source "SourceFile"


# static fields
.field public static final FALSE:Lorg/apache/pdfbox/cos/COSBoolean;

.field public static final FALSE_BYTES:[B

.field public static final TRUE:Lorg/apache/pdfbox/cos/COSBoolean;

.field public static final TRUE_BYTES:[B


# instance fields
.field private final value:Z


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    const/4 v0, 0x4

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, Lorg/apache/pdfbox/cos/COSBoolean;->TRUE_BYTES:[B

    const/4 v0, 0x5

    new-array v0, v0, [B

    fill-array-data v0, :array_1

    sput-object v0, Lorg/apache/pdfbox/cos/COSBoolean;->FALSE_BYTES:[B

    new-instance v0, Lorg/apache/pdfbox/cos/COSBoolean;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSBoolean;-><init>(Z)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSBoolean;->TRUE:Lorg/apache/pdfbox/cos/COSBoolean;

    new-instance v0, Lorg/apache/pdfbox/cos/COSBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSBoolean;-><init>(Z)V

    sput-object v0, Lorg/apache/pdfbox/cos/COSBoolean;->FALSE:Lorg/apache/pdfbox/cos/COSBoolean;

    return-void

    nop

    :array_0
    .array-data 1
        0x74t
        0x72t
        0x75t
        0x65t
    .end array-data

    :array_1
    .array-data 1
        0x66t
        0x61t
        0x6ct
        0x73t
        0x65t
    .end array-data
.end method

.method private constructor <init>(Z)V
    .locals 0

    invoke-direct {p0}, Lorg/apache/pdfbox/cos/COSBase;-><init>()V

    iput-boolean p1, p0, Lorg/apache/pdfbox/cos/COSBoolean;->value:Z

    return-void
.end method

.method public static getBoolean(Ljava/lang/Boolean;)Lorg/apache/pdfbox/cos/COSBoolean;
    .locals 0

    .line 1
    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p0

    invoke-static {p0}, Lorg/apache/pdfbox/cos/COSBoolean;->getBoolean(Z)Lorg/apache/pdfbox/cos/COSBoolean;

    move-result-object p0

    return-object p0
.end method

.method public static getBoolean(Z)Lorg/apache/pdfbox/cos/COSBoolean;
    .locals 0

    .line 2
    if-eqz p0, :cond_0

    sget-object p0, Lorg/apache/pdfbox/cos/COSBoolean;->TRUE:Lorg/apache/pdfbox/cos/COSBoolean;

    goto :goto_0

    :cond_0
    sget-object p0, Lorg/apache/pdfbox/cos/COSBoolean;->FALSE:Lorg/apache/pdfbox/cos/COSBoolean;

    :goto_0
    return-object p0
.end method


# virtual methods
.method public accept(Lorg/apache/pdfbox/cos/ICOSVisitor;)Ljava/lang/Object;
    .locals 0

    invoke-interface {p1, p0}, Lorg/apache/pdfbox/cos/ICOSVisitor;->visitFromBoolean(Lorg/apache/pdfbox/cos/COSBoolean;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public getValue()Z
    .locals 1

    iget-boolean v0, p0, Lorg/apache/pdfbox/cos/COSBoolean;->value:Z

    return v0
.end method

.method public getValueAsObject()Ljava/lang/Boolean;
    .locals 1

    iget-boolean v0, p0, Lorg/apache/pdfbox/cos/COSBoolean;->value:Z

    if-eqz v0, :cond_0

    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    goto :goto_0

    :cond_0
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    :goto_0
    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    iget-boolean v0, p0, Lorg/apache/pdfbox/cos/COSBoolean;->value:Z

    invoke-static {v0}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writePDF(Ljava/io/OutputStream;)V
    .locals 1

    iget-boolean v0, p0, Lorg/apache/pdfbox/cos/COSBoolean;->value:Z

    if-eqz v0, :cond_0

    sget-object v0, Lorg/apache/pdfbox/cos/COSBoolean;->TRUE_BYTES:[B

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    goto :goto_0

    :cond_0
    sget-object v0, Lorg/apache/pdfbox/cos/COSBoolean;->FALSE_BYTES:[B

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write([B)V

    :goto_0
    return-void
.end method

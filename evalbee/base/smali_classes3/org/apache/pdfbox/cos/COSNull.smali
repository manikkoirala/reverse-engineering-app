.class public final Lorg/apache/pdfbox/cos/COSNull;
.super Lorg/apache/pdfbox/cos/COSBase;
.source "SourceFile"


# static fields
.field public static final NULL:Lorg/apache/pdfbox/cos/COSNull;

.field public static final NULL_BYTES:[B


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x4

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, Lorg/apache/pdfbox/cos/COSNull;->NULL_BYTES:[B

    new-instance v0, Lorg/apache/pdfbox/cos/COSNull;

    invoke-direct {v0}, Lorg/apache/pdfbox/cos/COSNull;-><init>()V

    sput-object v0, Lorg/apache/pdfbox/cos/COSNull;->NULL:Lorg/apache/pdfbox/cos/COSNull;

    return-void

    :array_0
    .array-data 1
        0x6et
        0x75t
        0x6ct
        0x6ct
    .end array-data
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lorg/apache/pdfbox/cos/COSBase;-><init>()V

    return-void
.end method

.method public static writePDF(Ljava/io/OutputStream;)V
    .locals 1

    sget-object v0, Lorg/apache/pdfbox/cos/COSNull;->NULL_BYTES:[B

    invoke-virtual {p0, v0}, Ljava/io/OutputStream;->write([B)V

    return-void
.end method


# virtual methods
.method public accept(Lorg/apache/pdfbox/cos/ICOSVisitor;)Ljava/lang/Object;
    .locals 0

    invoke-interface {p1, p0}, Lorg/apache/pdfbox/cos/ICOSVisitor;->visitFromNull(Lorg/apache/pdfbox/cos/COSNull;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    const-string v0, "COSNull{}"

    return-object v0
.end method

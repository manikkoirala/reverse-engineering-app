.class public Lorg/apache/pdfbox/cos/COSDocument;
.super Lorg/apache/pdfbox/cos/COSBase;
.source "SourceFile"

# interfaces
.implements Ljava/io/Closeable;


# instance fields
.field private closed:Z

.field private isDecrypted:Z

.field private isXRefStream:Z

.field private final objectPool:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lorg/apache/pdfbox/cos/COSObjectKey;",
            "Lorg/apache/pdfbox/cos/COSObject;",
            ">;"
        }
    .end annotation
.end field

.field private final scratchDirectory:Ljava/io/File;

.field private startXref:J

.field private trailer:Lorg/apache/pdfbox/cos/COSDictionary;

.field private final useScratchFile:Z

.field private version:F

.field private warnMissingClose:Z

.field private final xrefTable:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lorg/apache/pdfbox/cos/COSObjectKey;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/cos/COSDocument;-><init>(Z)V

    return-void
.end method

.method public constructor <init>(Ljava/io/File;Z)V
    .locals 1

    .line 2
    invoke-direct {p0}, Lorg/apache/pdfbox/cos/COSBase;-><init>()V

    const v0, 0x3fb33333    # 1.4f

    iput v0, p0, Lorg/apache/pdfbox/cos/COSDocument;->version:F

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/pdfbox/cos/COSDocument;->objectPool:Ljava/util/Map;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/pdfbox/cos/COSDocument;->xrefTable:Ljava/util/Map;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/pdfbox/cos/COSDocument;->warnMissingClose:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/pdfbox/cos/COSDocument;->isDecrypted:Z

    iput-boolean v0, p0, Lorg/apache/pdfbox/cos/COSDocument;->closed:Z

    iput-object p1, p0, Lorg/apache/pdfbox/cos/COSDocument;->scratchDirectory:Ljava/io/File;

    iput-boolean p2, p0, Lorg/apache/pdfbox/cos/COSDocument;->useScratchFile:Z

    return-void
.end method

.method public constructor <init>(Z)V
    .locals 1

    .line 3
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1}, Lorg/apache/pdfbox/cos/COSDocument;-><init>(Ljava/io/File;Z)V

    return-void
.end method


# virtual methods
.method public accept(Lorg/apache/pdfbox/cos/ICOSVisitor;)Ljava/lang/Object;
    .locals 0

    invoke-interface {p1, p0}, Lorg/apache/pdfbox/cos/ICOSVisitor;->visitFromDocument(Lorg/apache/pdfbox/cos/COSDocument;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public addXRefTable(Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Lorg/apache/pdfbox/cos/COSObjectKey;",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/pdfbox/cos/COSDocument;->xrefTable:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    return-void
.end method

.method public close()V
    .locals 3

    iget-boolean v0, p0, Lorg/apache/pdfbox/cos/COSDocument;->closed:Z

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lorg/apache/pdfbox/cos/COSDocument;->getObjects()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/pdfbox/cos/COSObject;

    invoke-virtual {v1}, Lorg/apache/pdfbox/cos/COSObject;->getObject()Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v1

    instance-of v2, v1, Lorg/apache/pdfbox/cos/COSStream;

    if-eqz v2, :cond_0

    check-cast v1, Lorg/apache/pdfbox/cos/COSStream;

    invoke-virtual {v1}, Lorg/apache/pdfbox/cos/COSStream;->close()V

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/pdfbox/cos/COSDocument;->closed:Z

    :cond_2
    return-void
.end method

.method public createCOSStream()Lorg/apache/pdfbox/cos/COSStream;
    .locals 3

    .line 1
    new-instance v0, Lorg/apache/pdfbox/cos/COSStream;

    iget-boolean v1, p0, Lorg/apache/pdfbox/cos/COSDocument;->useScratchFile:Z

    iget-object v2, p0, Lorg/apache/pdfbox/cos/COSDocument;->scratchDirectory:Ljava/io/File;

    invoke-direct {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSStream;-><init>(ZLjava/io/File;)V

    return-object v0
.end method

.method public createCOSStream(Lorg/apache/pdfbox/cos/COSDictionary;)Lorg/apache/pdfbox/cos/COSStream;
    .locals 3

    .line 2
    new-instance v0, Lorg/apache/pdfbox/cos/COSStream;

    iget-boolean v1, p0, Lorg/apache/pdfbox/cos/COSDocument;->useScratchFile:Z

    iget-object v2, p0, Lorg/apache/pdfbox/cos/COSDocument;->scratchDirectory:Ljava/io/File;

    invoke-direct {v0, p1, v1, v2}, Lorg/apache/pdfbox/cos/COSStream;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;ZLjava/io/File;)V

    return-object v0
.end method

.method public dereferenceObjectStreams()V
    .locals 10

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->OBJ_STM:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p0, v0}, Lorg/apache/pdfbox/cos/COSDocument;->getObjectsByType(Lorg/apache/pdfbox/cos/COSName;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/pdfbox/cos/COSObject;

    invoke-virtual {v1}, Lorg/apache/pdfbox/cos/COSObject;->getObject()Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v2

    check-cast v2, Lorg/apache/pdfbox/cos/COSStream;

    new-instance v3, Lorg/apache/pdfbox/pdfparser/PDFObjectStreamParser;

    invoke-direct {v3, v2, p0}, Lorg/apache/pdfbox/pdfparser/PDFObjectStreamParser;-><init>(Lorg/apache/pdfbox/cos/COSStream;Lorg/apache/pdfbox/cos/COSDocument;)V

    :try_start_0
    invoke-virtual {v3}, Lorg/apache/pdfbox/pdfparser/PDFObjectStreamParser;->parse()V

    invoke-virtual {v3}, Lorg/apache/pdfbox/pdfparser/PDFObjectStreamParser;->getObjects()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/pdfbox/cos/COSObject;

    new-instance v5, Lorg/apache/pdfbox/cos/COSObjectKey;

    invoke-direct {v5, v4}, Lorg/apache/pdfbox/cos/COSObjectKey;-><init>(Lorg/apache/pdfbox/cos/COSObject;)V

    iget-object v6, p0, Lorg/apache/pdfbox/cos/COSDocument;->objectPool:Ljava/util/Map;

    invoke-interface {v6, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    if-eqz v6, :cond_1

    iget-object v6, p0, Lorg/apache/pdfbox/cos/COSDocument;->objectPool:Ljava/util/Map;

    invoke-interface {v6, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lorg/apache/pdfbox/cos/COSObject;

    invoke-virtual {v6}, Lorg/apache/pdfbox/cos/COSObject;->getObject()Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v6

    if-eqz v6, :cond_1

    iget-object v6, p0, Lorg/apache/pdfbox/cos/COSDocument;->xrefTable:Ljava/util/Map;

    invoke-interface {v6, v5}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    iget-object v6, p0, Lorg/apache/pdfbox/cos/COSDocument;->xrefTable:Ljava/util/Map;

    invoke-interface {v6, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Long;

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-virtual {v1}, Lorg/apache/pdfbox/cos/COSObject;->getObjectNumber()J

    move-result-wide v8

    neg-long v8, v8

    cmp-long v6, v6, v8

    if-nez v6, :cond_0

    :cond_1
    invoke-virtual {p0, v5}, Lorg/apache/pdfbox/cos/COSDocument;->getObjectFromPool(Lorg/apache/pdfbox/cos/COSObjectKey;)Lorg/apache/pdfbox/cos/COSObject;

    move-result-object v5

    invoke-virtual {v4}, Lorg/apache/pdfbox/cos/COSObject;->getObject()Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v4

    invoke-virtual {v5, v4}, Lorg/apache/pdfbox/cos/COSObject;->setObject(Lorg/apache/pdfbox/cos/COSBase;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :cond_2
    invoke-virtual {v3}, Lorg/apache/pdfbox/pdfparser/BaseParser;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v3}, Lorg/apache/pdfbox/pdfparser/BaseParser;->close()V

    throw v0

    :cond_3
    return-void
.end method

.method public finalize()V
    .locals 2

    iget-boolean v0, p0, Lorg/apache/pdfbox/cos/COSDocument;->closed:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lorg/apache/pdfbox/cos/COSDocument;->warnMissingClose:Z

    if-eqz v0, :cond_0

    const-string v0, "PdfBoxAndroid"

    const-string v1, "Warning: You did not close a PDF Document"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-virtual {p0}, Lorg/apache/pdfbox/cos/COSDocument;->close()V

    :cond_1
    return-void
.end method

.method public getCatalog()Lorg/apache/pdfbox/cos/COSObject;
    .locals 2

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->CATALOG:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p0, v0}, Lorg/apache/pdfbox/cos/COSDocument;->getObjectByType(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSObject;

    move-result-object v0

    if-eqz v0, :cond_0

    return-object v0

    :cond_0
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Catalog cannot be found"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getDocumentID()Lorg/apache/pdfbox/cos/COSArray;
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/cos/COSDocument;->getTrailer()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->ID:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSArray;

    return-object v0
.end method

.method public getEncryptionDictionary()Lorg/apache/pdfbox/cos/COSDictionary;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/cos/COSDocument;->trailer:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->ENCRYPT:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSDictionary;

    return-object v0
.end method

.method public getObjectByType(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSObject;
    .locals 5

    const-string v0, "PdfBoxAndroid"

    iget-object v1, p0, Lorg/apache/pdfbox/cos/COSDocument;->objectPool:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/pdfbox/cos/COSObject;

    invoke-virtual {v2}, Lorg/apache/pdfbox/cos/COSObject;->getObject()Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v3

    instance-of v4, v3, Lorg/apache/pdfbox/cos/COSDictionary;

    if-eqz v4, :cond_0

    :try_start_0
    check-cast v3, Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v4, Lorg/apache/pdfbox/cos/COSName;->TYPE:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v3, v4}, Lorg/apache/pdfbox/cos/COSDictionary;->getItem(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v3

    instance-of v4, v3, Lorg/apache/pdfbox/cos/COSName;

    if-eqz v4, :cond_1

    check-cast v3, Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v3, p1}, Lorg/apache/pdfbox/cos/COSName;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    return-object v2

    :cond_1
    if-eqz v3, :cond_0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Expected a /Name object after /Type, got \'"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v3, "\' instead"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v2

    invoke-virtual {v2}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :cond_2
    const/4 p1, 0x0

    return-object p1
.end method

.method public getObjectFromPool(Lorg/apache/pdfbox/cos/COSObjectKey;)Lorg/apache/pdfbox/cos/COSObject;
    .locals 4

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    iget-object v1, p0, Lorg/apache/pdfbox/cos/COSDocument;->objectPool:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/pdfbox/cos/COSObject;

    goto :goto_0

    :cond_0
    move-object v1, v0

    :goto_0
    if-nez v1, :cond_1

    new-instance v1, Lorg/apache/pdfbox/cos/COSObject;

    invoke-direct {v1, v0}, Lorg/apache/pdfbox/cos/COSObject;-><init>(Lorg/apache/pdfbox/cos/COSBase;)V

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lorg/apache/pdfbox/cos/COSObjectKey;->getNumber()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lorg/apache/pdfbox/cos/COSObject;->setObjectNumber(J)V

    invoke-virtual {p1}, Lorg/apache/pdfbox/cos/COSObjectKey;->getGeneration()I

    move-result v0

    invoke-virtual {v1, v0}, Lorg/apache/pdfbox/cos/COSObject;->setGenerationNumber(I)V

    iget-object v0, p0, Lorg/apache/pdfbox/cos/COSDocument;->objectPool:Ljava/util/Map;

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    return-object v1
.end method

.method public getObjects()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lorg/apache/pdfbox/cos/COSObject;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lorg/apache/pdfbox/cos/COSDocument;->objectPool:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method public getObjectsByType(Ljava/lang/String;)Ljava/util/List;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Lorg/apache/pdfbox/cos/COSObject;",
            ">;"
        }
    .end annotation

    .line 1
    invoke-static {p1}, Lorg/apache/pdfbox/cos/COSName;->getPDFName(Ljava/lang/String;)Lorg/apache/pdfbox/cos/COSName;

    move-result-object p1

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/cos/COSDocument;->getObjectsByType(Lorg/apache/pdfbox/cos/COSName;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public getObjectsByType(Lorg/apache/pdfbox/cos/COSName;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/pdfbox/cos/COSName;",
            ")",
            "Ljava/util/List<",
            "Lorg/apache/pdfbox/cos/COSObject;",
            ">;"
        }
    .end annotation

    .line 2
    const-string v0, "PdfBoxAndroid"

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iget-object v2, p0, Lorg/apache/pdfbox/cos/COSDocument;->objectPool:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/pdfbox/cos/COSObject;

    invoke-virtual {v3}, Lorg/apache/pdfbox/cos/COSObject;->getObject()Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v4

    instance-of v5, v4, Lorg/apache/pdfbox/cos/COSDictionary;

    if-eqz v5, :cond_0

    :try_start_0
    check-cast v4, Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v5, Lorg/apache/pdfbox/cos/COSName;->TYPE:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v4, v5}, Lorg/apache/pdfbox/cos/COSDictionary;->getItem(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v4

    instance-of v5, v4, Lorg/apache/pdfbox/cos/COSName;

    if-eqz v5, :cond_1

    check-cast v4, Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v4, p1}, Lorg/apache/pdfbox/cos/COSName;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    if-eqz v4, :cond_0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Expected a /Name object after /Type, got \'"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v4, "\' instead"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v3

    invoke-virtual {v3}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :cond_2
    return-object v1
.end method

.method public getSignatureDictionaries()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lorg/apache/pdfbox/cos/COSDictionary;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lorg/apache/pdfbox/cos/COSDocument;->getSignatureFields(Z)Ljava/util/List;

    move-result-object v0

    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v3, Lorg/apache/pdfbox/cos/COSName;->V:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v2, v3}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v2

    if-eqz v2, :cond_0

    check-cast v2, Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-object v1
.end method

.method public getSignatureFields(Z)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Ljava/util/List<",
            "Lorg/apache/pdfbox/cos/COSDictionary;",
            ">;"
        }
    .end annotation

    invoke-virtual {p0}, Lorg/apache/pdfbox/cos/COSDocument;->getCatalog()Lorg/apache/pdfbox/cos/COSObject;

    move-result-object v0

    if-eqz v0, :cond_3

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->ACRO_FORM:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSObject;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSDictionary;

    if-eqz v0, :cond_3

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->FIELDS:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSArray;

    if-eqz v0, :cond_3

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSArray;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/pdfbox/cos/COSObject;

    sget-object v3, Lorg/apache/pdfbox/cos/COSName;->SIG:Lorg/apache/pdfbox/cos/COSName;

    sget-object v4, Lorg/apache/pdfbox/cos/COSName;->FT:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v2, v4}, Lorg/apache/pdfbox/cos/COSObject;->getItem(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v4

    invoke-virtual {v3, v4}, Lorg/apache/pdfbox/cos/COSName;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    sget-object v3, Lorg/apache/pdfbox/cos/COSName;->V:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v2, v3}, Lorg/apache/pdfbox/cos/COSObject;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v3

    if-eqz v3, :cond_1

    if-nez p1, :cond_0

    :cond_1
    new-instance v3, Lorg/apache/pdfbox/cos/COSObjectKey;

    invoke-direct {v3, v2}, Lorg/apache/pdfbox/cos/COSObjectKey;-><init>(Lorg/apache/pdfbox/cos/COSObject;)V

    invoke-virtual {v2}, Lorg/apache/pdfbox/cos/COSObject;->getObject()Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v2

    check-cast v2, Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-interface {v1, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_2
    new-instance p1, Ljava/util/LinkedList;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/util/LinkedList;-><init>(Ljava/util/Collection;)V

    return-object p1

    :cond_3
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public getStartXref()J
    .locals 2

    iget-wide v0, p0, Lorg/apache/pdfbox/cos/COSDocument;->startXref:J

    return-wide v0
.end method

.method public getTrailer()Lorg/apache/pdfbox/cos/COSDictionary;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/cos/COSDocument;->trailer:Lorg/apache/pdfbox/cos/COSDictionary;

    return-object v0
.end method

.method public getVersion()F
    .locals 1

    iget v0, p0, Lorg/apache/pdfbox/cos/COSDocument;->version:F

    return v0
.end method

.method public getXrefTable()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Lorg/apache/pdfbox/cos/COSObjectKey;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/pdfbox/cos/COSDocument;->xrefTable:Ljava/util/Map;

    return-object v0
.end method

.method public isClosed()Z
    .locals 1

    iget-boolean v0, p0, Lorg/apache/pdfbox/cos/COSDocument;->closed:Z

    return v0
.end method

.method public isDecrypted()Z
    .locals 1

    iget-boolean v0, p0, Lorg/apache/pdfbox/cos/COSDocument;->isDecrypted:Z

    return v0
.end method

.method public isEncrypted()Z
    .locals 3

    iget-object v0, p0, Lorg/apache/pdfbox/cos/COSDocument;->trailer:Lorg/apache/pdfbox/cos/COSDictionary;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    sget-object v2, Lorg/apache/pdfbox/cos/COSName;->ENCRYPT:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    move v1, v0

    :cond_0
    return v1
.end method

.method public isXRefStream()Z
    .locals 1

    iget-boolean v0, p0, Lorg/apache/pdfbox/cos/COSDocument;->isXRefStream:Z

    return v0
.end method

.method public print()V
    .locals 3

    iget-object v0, p0, Lorg/apache/pdfbox/cos/COSDocument;->objectPool:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/pdfbox/cos/COSObject;

    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v2, v1}, Ljava/io/PrintStream;->println(Ljava/lang/Object;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public removeObject(Lorg/apache/pdfbox/cos/COSObjectKey;)Lorg/apache/pdfbox/cos/COSObject;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/cos/COSDocument;->objectPool:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lorg/apache/pdfbox/cos/COSObject;

    return-object p1
.end method

.method public setDecrypted()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/pdfbox/cos/COSDocument;->isDecrypted:Z

    return-void
.end method

.method public setDocumentID(Lorg/apache/pdfbox/cos/COSArray;)V
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/cos/COSDocument;->getTrailer()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->ID:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    return-void
.end method

.method public setEncryptionDictionary(Lorg/apache/pdfbox/cos/COSDictionary;)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/cos/COSDocument;->trailer:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->ENCRYPT:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    return-void
.end method

.method public setIsXRefStream(Z)V
    .locals 0

    iput-boolean p1, p0, Lorg/apache/pdfbox/cos/COSDocument;->isXRefStream:Z

    return-void
.end method

.method public setStartXref(J)V
    .locals 0

    iput-wide p1, p0, Lorg/apache/pdfbox/cos/COSDocument;->startXref:J

    return-void
.end method

.method public setTrailer(Lorg/apache/pdfbox/cos/COSDictionary;)V
    .locals 0

    iput-object p1, p0, Lorg/apache/pdfbox/cos/COSDocument;->trailer:Lorg/apache/pdfbox/cos/COSDictionary;

    return-void
.end method

.method public setVersion(F)V
    .locals 0

    iput p1, p0, Lorg/apache/pdfbox/cos/COSDocument;->version:F

    return-void
.end method

.method public setWarnMissingClose(Z)V
    .locals 0

    iput-boolean p1, p0, Lorg/apache/pdfbox/cos/COSDocument;->warnMissingClose:Z

    return-void
.end method

.class public abstract Lorg/apache/pdfbox/cos/COSBase;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/pdfbox/pdmodel/common/COSObjectable;


# instance fields
.field private direct:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract accept(Lorg/apache/pdfbox/cos/ICOSVisitor;)Ljava/lang/Object;
.end method

.method public getCOSObject()Lorg/apache/pdfbox/cos/COSBase;
    .locals 0

    return-object p0
.end method

.method public isDirect()Z
    .locals 1

    iget-boolean v0, p0, Lorg/apache/pdfbox/cos/COSBase;->direct:Z

    return v0
.end method

.method public setDirect(Z)V
    .locals 0

    iput-boolean p1, p0, Lorg/apache/pdfbox/cos/COSBase;->direct:Z

    return-void
.end method

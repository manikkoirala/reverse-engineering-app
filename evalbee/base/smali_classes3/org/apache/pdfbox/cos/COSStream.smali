.class public Lorg/apache/pdfbox/cos/COSStream;
.super Lorg/apache/pdfbox/cos/COSDictionary;
.source "SourceFile"

# interfaces
.implements Ljava/io/Closeable;


# static fields
.field private static final BUFFER_SIZE:I = 0x4000


# instance fields
.field private final buffer:Lorg/apache/pdfbox/io/RandomAccess;

.field private decodeResult:Lorg/apache/pdfbox/filter/DecodeResult;

.field private filteredStream:Lorg/apache/pdfbox/io/RandomAccessFileOutputStream;

.field private scratchFile:Ljava/io/File;

.field private unFilteredStream:Lorg/apache/pdfbox/io/RandomAccessFileOutputStream;


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 1
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lorg/apache/pdfbox/cos/COSStream;-><init>(ZLjava/io/File;)V

    return-void
.end method

.method public constructor <init>(Lorg/apache/pdfbox/cos/COSDictionary;)V
    .locals 2

    .line 2
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lorg/apache/pdfbox/cos/COSStream;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;ZLjava/io/File;)V

    return-void
.end method

.method public constructor <init>(Lorg/apache/pdfbox/cos/COSDictionary;ZLjava/io/File;)V
    .locals 0

    .line 3
    invoke-direct {p0, p1}, Lorg/apache/pdfbox/cos/COSDictionary;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    if-eqz p2, :cond_0

    invoke-direct {p0, p3}, Lorg/apache/pdfbox/cos/COSStream;->createScratchFile(Ljava/io/File;)Lorg/apache/pdfbox/io/RandomAccess;

    move-result-object p1

    goto :goto_0

    :cond_0
    new-instance p1, Lorg/apache/pdfbox/io/RandomAccessBuffer;

    invoke-direct {p1}, Lorg/apache/pdfbox/io/RandomAccessBuffer;-><init>()V

    :goto_0
    iput-object p1, p0, Lorg/apache/pdfbox/cos/COSStream;->buffer:Lorg/apache/pdfbox/io/RandomAccess;

    return-void
.end method

.method public constructor <init>(ZLjava/io/File;)V
    .locals 0

    .line 4
    invoke-direct {p0}, Lorg/apache/pdfbox/cos/COSDictionary;-><init>()V

    if-eqz p1, :cond_0

    invoke-direct {p0, p2}, Lorg/apache/pdfbox/cos/COSStream;->createScratchFile(Ljava/io/File;)Lorg/apache/pdfbox/io/RandomAccess;

    move-result-object p1

    goto :goto_0

    :cond_0
    new-instance p1, Lorg/apache/pdfbox/io/RandomAccessBuffer;

    invoke-direct {p1}, Lorg/apache/pdfbox/io/RandomAccessBuffer;-><init>()V

    :goto_0
    iput-object p1, p0, Lorg/apache/pdfbox/cos/COSStream;->buffer:Lorg/apache/pdfbox/io/RandomAccess;

    return-void
.end method

.method private attemptDecode(JJLorg/apache/pdfbox/filter/Filter;I)V
    .locals 9

    const/4 v0, 0x0

    :try_start_0
    new-instance v1, Ljava/io/BufferedInputStream;

    new-instance v8, Lorg/apache/pdfbox/io/RandomAccessFileInputStream;

    iget-object v3, p0, Lorg/apache/pdfbox/cos/COSStream;->buffer:Lorg/apache/pdfbox/io/RandomAccess;

    move-object v2, v8

    move-wide v4, p1

    move-wide v6, p3

    invoke-direct/range {v2 .. v7}, Lorg/apache/pdfbox/io/RandomAccessFileInputStream;-><init>(Lorg/apache/pdfbox/io/RandomAccess;JJ)V

    const/16 p1, 0x4000

    invoke-direct {v1, v8, p1}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    iget-object p1, p0, Lorg/apache/pdfbox/cos/COSStream;->unFilteredStream:Lorg/apache/pdfbox/io/RandomAccessFileOutputStream;

    invoke-static {p1}, Lorg/apache/pdfbox/io/IOUtils;->closeQuietly(Ljava/io/Closeable;)V

    new-instance p1, Lorg/apache/pdfbox/io/RandomAccessFileOutputStream;

    iget-object p2, p0, Lorg/apache/pdfbox/cos/COSStream;->buffer:Lorg/apache/pdfbox/io/RandomAccess;

    invoke-direct {p1, p2}, Lorg/apache/pdfbox/io/RandomAccessFileOutputStream;-><init>(Lorg/apache/pdfbox/io/RandomAccess;)V

    iput-object p1, p0, Lorg/apache/pdfbox/cos/COSStream;->unFilteredStream:Lorg/apache/pdfbox/io/RandomAccessFileOutputStream;

    invoke-virtual {p5, v1, p1, p0, p6}, Lorg/apache/pdfbox/filter/Filter;->decode(Ljava/io/InputStream;Ljava/io/OutputStream;Lorg/apache/pdfbox/cos/COSDictionary;I)Lorg/apache/pdfbox/filter/DecodeResult;

    move-result-object p1

    iput-object p1, p0, Lorg/apache/pdfbox/cos/COSStream;->decodeResult:Lorg/apache/pdfbox/filter/DecodeResult;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-static {v1}, Lorg/apache/pdfbox/io/IOUtils;->closeQuietly(Ljava/io/Closeable;)V

    return-void

    :catchall_0
    move-exception p1

    move-object v0, v1

    goto :goto_0

    :catchall_1
    move-exception p1

    :goto_0
    invoke-static {v0}, Lorg/apache/pdfbox/io/IOUtils;->closeQuietly(Ljava/io/Closeable;)V

    throw p1
.end method

.method private createScratchFile(Ljava/io/File;)Lorg/apache/pdfbox/io/RandomAccess;
    .locals 2

    :try_start_0
    const-string v0, "PDFBox"

    const/4 v1, 0x0

    invoke-static {v0, v1, p1}, Ljava/io/File;->createTempFile(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Ljava/io/File;

    move-result-object p1

    iput-object p1, p0, Lorg/apache/pdfbox/cos/COSStream;->scratchFile:Ljava/io/File;

    new-instance v0, Lorg/apache/pdfbox/io/RandomAccessFile;

    const-string v1, "rw"

    invoke-direct {v0, p1, v1}, Lorg/apache/pdfbox/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception p1

    const-string v0, "PdfBoxAndroid"

    const-string v1, "Can\'t create temp file, using memory buffer instead"

    invoke-static {v0, v1, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    new-instance p1, Lorg/apache/pdfbox/io/RandomAccessBuffer;

    invoke-direct {p1}, Lorg/apache/pdfbox/io/RandomAccessBuffer;-><init>()V

    return-object p1
.end method

.method private doDecode()V
    .locals 4

    .line 1
    iget-object v0, p0, Lorg/apache/pdfbox/cos/COSStream;->filteredStream:Lorg/apache/pdfbox/io/RandomAccessFileOutputStream;

    iput-object v0, p0, Lorg/apache/pdfbox/cos/COSStream;->unFilteredStream:Lorg/apache/pdfbox/io/RandomAccessFileOutputStream;

    invoke-virtual {p0}, Lorg/apache/pdfbox/cos/COSStream;->getFilters()Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    if-nez v0, :cond_0

    sget-object v0, Lorg/apache/pdfbox/filter/DecodeResult;->DEFAULT:Lorg/apache/pdfbox/filter/DecodeResult;

    iput-object v0, p0, Lorg/apache/pdfbox/cos/COSStream;->decodeResult:Lorg/apache/pdfbox/filter/DecodeResult;

    goto :goto_1

    :cond_0
    instance-of v1, v0, Lorg/apache/pdfbox/cos/COSName;

    const/4 v2, 0x0

    if-eqz v1, :cond_1

    check-cast v0, Lorg/apache/pdfbox/cos/COSName;

    invoke-direct {p0, v0, v2}, Lorg/apache/pdfbox/cos/COSStream;->doDecode(Lorg/apache/pdfbox/cos/COSName;I)V

    goto :goto_1

    :cond_1
    instance-of v1, v0, Lorg/apache/pdfbox/cos/COSArray;

    if-eqz v1, :cond_3

    check-cast v0, Lorg/apache/pdfbox/cos/COSArray;

    :goto_0
    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSArray;->size()I

    move-result v1

    if-ge v2, v1, :cond_2

    invoke-virtual {v0, v2}, Lorg/apache/pdfbox/cos/COSArray;->get(I)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v1

    check-cast v1, Lorg/apache/pdfbox/cos/COSName;

    invoke-direct {p0, v1, v2}, Lorg/apache/pdfbox/cos/COSStream;->doDecode(Lorg/apache/pdfbox/cos/COSName;I)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    :goto_1
    return-void

    :cond_3
    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error: Unknown filter type:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private doDecode(Lorg/apache/pdfbox/cos/COSName;I)V
    .locals 24

    .line 2
    move-object/from16 v8, p0

    sget-object v0, Lorg/apache/pdfbox/filter/FilterFactory;->INSTANCE:Lorg/apache/pdfbox/filter/FilterFactory;

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/filter/FilterFactory;->getFilter(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/filter/Filter;

    move-result-object v9

    iget-object v0, v8, Lorg/apache/pdfbox/cos/COSStream;->unFilteredStream:Lorg/apache/pdfbox/io/RandomAccessFileOutputStream;

    invoke-virtual {v0}, Lorg/apache/pdfbox/io/RandomAccessFileOutputStream;->getPosition()J

    move-result-wide v10

    iget-object v0, v8, Lorg/apache/pdfbox/cos/COSStream;->unFilteredStream:Lorg/apache/pdfbox/io/RandomAccessFileOutputStream;

    invoke-virtual {v0}, Lorg/apache/pdfbox/io/RandomAccessFileOutputStream;->getLength()J

    move-result-wide v0

    iget-object v2, v8, Lorg/apache/pdfbox/cos/COSStream;->unFilteredStream:Lorg/apache/pdfbox/io/RandomAccessFileOutputStream;

    invoke-virtual {v2}, Lorg/apache/pdfbox/io/RandomAccessFileOutputStream;->getLengthWritten()J

    move-result-wide v12

    const-wide/16 v14, 0x0

    cmp-long v2, v0, v14

    const/16 v16, 0x1

    const/4 v3, 0x0

    if-nez v2, :cond_0

    cmp-long v2, v12, v14

    if-nez v2, :cond_0

    iget-object v0, v8, Lorg/apache/pdfbox/cos/COSStream;->unFilteredStream:Lorg/apache/pdfbox/io/RandomAccessFileOutputStream;

    invoke-static {v0}, Lorg/apache/pdfbox/io/IOUtils;->closeQuietly(Ljava/io/Closeable;)V

    new-instance v0, Lorg/apache/pdfbox/io/RandomAccessFileOutputStream;

    iget-object v1, v8, Lorg/apache/pdfbox/cos/COSStream;->buffer:Lorg/apache/pdfbox/io/RandomAccess;

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/io/RandomAccessFileOutputStream;-><init>(Lorg/apache/pdfbox/io/RandomAccess;)V

    iput-object v0, v8, Lorg/apache/pdfbox/cos/COSStream;->unFilteredStream:Lorg/apache/pdfbox/io/RandomAccessFileOutputStream;

    goto :goto_4

    :cond_0
    const/16 v17, 0x0

    move-wide/from16 v18, v0

    move-object v0, v3

    move/from16 v7, v17

    move/from16 v20, v7

    :goto_0
    cmp-long v1, v18, v14

    const-wide/16 v21, 0x1

    const/4 v6, 0x5

    if-lez v1, :cond_1

    if-nez v20, :cond_1

    if-ge v7, v6, :cond_1

    move-object/from16 v1, p0

    move-wide v2, v10

    move-wide/from16 v4, v18

    move-object v6, v9

    move/from16 v23, v7

    move/from16 v7, p2

    :try_start_0
    invoke-direct/range {v1 .. v7}, Lorg/apache/pdfbox/cos/COSStream;->attemptDecode(JJLorg/apache/pdfbox/filter/Filter;I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move/from16 v20, v16

    goto :goto_1

    :catch_0
    move-exception v0

    sub-long v18, v18, v21

    :goto_1
    add-int/lit8 v7, v23, 0x1

    goto :goto_0

    :cond_1
    if-nez v20, :cond_2

    move/from16 v14, v17

    :goto_2
    if-nez v20, :cond_2

    if-ge v14, v6, :cond_2

    move-object/from16 v1, p0

    move-wide v2, v10

    move-wide v4, v12

    move v15, v6

    move-object v6, v9

    move/from16 v7, p2

    :try_start_1
    invoke-direct/range {v1 .. v7}, Lorg/apache/pdfbox/cos/COSStream;->attemptDecode(JJLorg/apache/pdfbox/filter/Filter;I)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    move/from16 v20, v16

    goto :goto_3

    :catch_1
    move-exception v0

    sub-long v12, v12, v21

    :goto_3
    add-int/lit8 v14, v14, 0x1

    move v6, v15

    goto :goto_2

    :cond_2
    move-object v3, v0

    move/from16 v16, v20

    :goto_4
    if-nez v16, :cond_4

    if-nez v3, :cond_3

    goto :goto_5

    :cond_3
    throw v3

    :cond_4
    :goto_5
    return-void
.end method

.method private doEncode()V
    .locals 3

    .line 1
    iget-object v0, p0, Lorg/apache/pdfbox/cos/COSStream;->unFilteredStream:Lorg/apache/pdfbox/io/RandomAccessFileOutputStream;

    iput-object v0, p0, Lorg/apache/pdfbox/cos/COSStream;->filteredStream:Lorg/apache/pdfbox/io/RandomAccessFileOutputStream;

    invoke-virtual {p0}, Lorg/apache/pdfbox/cos/COSStream;->getFilters()Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    if-nez v0, :cond_0

    goto :goto_1

    :cond_0
    instance-of v1, v0, Lorg/apache/pdfbox/cos/COSName;

    if-eqz v1, :cond_1

    check-cast v0, Lorg/apache/pdfbox/cos/COSName;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lorg/apache/pdfbox/cos/COSStream;->doEncode(Lorg/apache/pdfbox/cos/COSName;I)V

    goto :goto_1

    :cond_1
    instance-of v1, v0, Lorg/apache/pdfbox/cos/COSArray;

    if-eqz v1, :cond_2

    check-cast v0, Lorg/apache/pdfbox/cos/COSArray;

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSArray;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    :goto_0
    if-ltz v1, :cond_2

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSArray;->get(I)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v2

    check-cast v2, Lorg/apache/pdfbox/cos/COSName;

    invoke-direct {p0, v2, v1}, Lorg/apache/pdfbox/cos/COSStream;->doEncode(Lorg/apache/pdfbox/cos/COSName;I)V

    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    :cond_2
    :goto_1
    return-void
.end method

.method private doEncode(Lorg/apache/pdfbox/cos/COSName;I)V
    .locals 8

    .line 2
    sget-object v0, Lorg/apache/pdfbox/filter/FilterFactory;->INSTANCE:Lorg/apache/pdfbox/filter/FilterFactory;

    invoke-virtual {v0, p1}, Lorg/apache/pdfbox/filter/FilterFactory;->getFilter(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/filter/Filter;

    move-result-object p1

    new-instance v0, Ljava/io/BufferedInputStream;

    new-instance v7, Lorg/apache/pdfbox/io/RandomAccessFileInputStream;

    iget-object v2, p0, Lorg/apache/pdfbox/cos/COSStream;->buffer:Lorg/apache/pdfbox/io/RandomAccess;

    iget-object v1, p0, Lorg/apache/pdfbox/cos/COSStream;->filteredStream:Lorg/apache/pdfbox/io/RandomAccessFileOutputStream;

    invoke-virtual {v1}, Lorg/apache/pdfbox/io/RandomAccessFileOutputStream;->getPosition()J

    move-result-wide v3

    iget-object v1, p0, Lorg/apache/pdfbox/cos/COSStream;->filteredStream:Lorg/apache/pdfbox/io/RandomAccessFileOutputStream;

    invoke-virtual {v1}, Lorg/apache/pdfbox/io/RandomAccessFileOutputStream;->getLength()J

    move-result-wide v5

    move-object v1, v7

    invoke-direct/range {v1 .. v6}, Lorg/apache/pdfbox/io/RandomAccessFileInputStream;-><init>(Lorg/apache/pdfbox/io/RandomAccess;JJ)V

    const/16 v1, 0x4000

    invoke-direct {v0, v7, v1}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;I)V

    iget-object v1, p0, Lorg/apache/pdfbox/cos/COSStream;->filteredStream:Lorg/apache/pdfbox/io/RandomAccessFileOutputStream;

    invoke-static {v1}, Lorg/apache/pdfbox/io/IOUtils;->closeQuietly(Ljava/io/Closeable;)V

    new-instance v1, Lorg/apache/pdfbox/io/RandomAccessFileOutputStream;

    iget-object v2, p0, Lorg/apache/pdfbox/cos/COSStream;->buffer:Lorg/apache/pdfbox/io/RandomAccess;

    invoke-direct {v1, v2}, Lorg/apache/pdfbox/io/RandomAccessFileOutputStream;-><init>(Lorg/apache/pdfbox/io/RandomAccess;)V

    iput-object v1, p0, Lorg/apache/pdfbox/cos/COSStream;->filteredStream:Lorg/apache/pdfbox/io/RandomAccessFileOutputStream;

    invoke-virtual {p1, v0, v1, p0, p2}, Lorg/apache/pdfbox/filter/Filter;->encode(Ljava/io/InputStream;Ljava/io/OutputStream;Lorg/apache/pdfbox/cos/COSDictionary;I)V

    invoke-static {v0}, Lorg/apache/pdfbox/io/IOUtils;->closeQuietly(Ljava/io/Closeable;)V

    return-void
.end method


# virtual methods
.method public accept(Lorg/apache/pdfbox/cos/ICOSVisitor;)Ljava/lang/Object;
    .locals 0

    invoke-interface {p1, p0}, Lorg/apache/pdfbox/cos/ICOSVisitor;->visitFromStream(Lorg/apache/pdfbox/cos/COSStream;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public close()V
    .locals 3

    iget-object v0, p0, Lorg/apache/pdfbox/cos/COSStream;->buffer:Lorg/apache/pdfbox/io/RandomAccess;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lorg/apache/pdfbox/io/SequentialRead;->close()V

    :cond_0
    iget-object v0, p0, Lorg/apache/pdfbox/cos/COSStream;->filteredStream:Lorg/apache/pdfbox/io/RandomAccessFileOutputStream;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V

    :cond_1
    iget-object v0, p0, Lorg/apache/pdfbox/cos/COSStream;->unFilteredStream:Lorg/apache/pdfbox/io/RandomAccessFileOutputStream;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V

    :cond_2
    iget-object v0, p0, Lorg/apache/pdfbox/cos/COSStream;->scratchFile:Ljava/io/File;

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lorg/apache/pdfbox/cos/COSStream;->scratchFile:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move-result v0

    if-eqz v0, :cond_3

    goto :goto_0

    :cond_3
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Can\'t delete the temporary scratch file "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lorg/apache/pdfbox/cos/COSStream;->scratchFile:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    :goto_0
    return-void
.end method

.method public createFilteredStream()Ljava/io/OutputStream;
    .locals 3

    .line 1
    iget-object v0, p0, Lorg/apache/pdfbox/cos/COSStream;->unFilteredStream:Lorg/apache/pdfbox/io/RandomAccessFileOutputStream;

    invoke-static {v0}, Lorg/apache/pdfbox/io/IOUtils;->closeQuietly(Ljava/io/Closeable;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/pdfbox/cos/COSStream;->unFilteredStream:Lorg/apache/pdfbox/io/RandomAccessFileOutputStream;

    iget-object v0, p0, Lorg/apache/pdfbox/cos/COSStream;->filteredStream:Lorg/apache/pdfbox/io/RandomAccessFileOutputStream;

    invoke-static {v0}, Lorg/apache/pdfbox/io/IOUtils;->closeQuietly(Ljava/io/Closeable;)V

    new-instance v0, Lorg/apache/pdfbox/io/RandomAccessFileOutputStream;

    iget-object v1, p0, Lorg/apache/pdfbox/cos/COSStream;->buffer:Lorg/apache/pdfbox/io/RandomAccess;

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/io/RandomAccessFileOutputStream;-><init>(Lorg/apache/pdfbox/io/RandomAccess;)V

    iput-object v0, p0, Lorg/apache/pdfbox/cos/COSStream;->filteredStream:Lorg/apache/pdfbox/io/RandomAccessFileOutputStream;

    new-instance v0, Ljava/io/BufferedOutputStream;

    iget-object v1, p0, Lorg/apache/pdfbox/cos/COSStream;->filteredStream:Lorg/apache/pdfbox/io/RandomAccessFileOutputStream;

    const/16 v2, 0x4000

    invoke-direct {v0, v1, v2}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;I)V

    return-object v0
.end method

.method public createFilteredStream(Lorg/apache/pdfbox/cos/COSBase;)Ljava/io/OutputStream;
    .locals 2

    .line 2
    invoke-virtual {p0}, Lorg/apache/pdfbox/cos/COSStream;->createFilteredStream()Ljava/io/OutputStream;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/pdfbox/cos/COSStream;->filteredStream:Lorg/apache/pdfbox/io/RandomAccessFileOutputStream;

    invoke-virtual {v1, p1}, Lorg/apache/pdfbox/io/RandomAccessFileOutputStream;->setExpectedLength(Lorg/apache/pdfbox/cos/COSBase;)V

    return-object v0
.end method

.method public createUnfilteredStream()Ljava/io/OutputStream;
    .locals 3

    iget-object v0, p0, Lorg/apache/pdfbox/cos/COSStream;->filteredStream:Lorg/apache/pdfbox/io/RandomAccessFileOutputStream;

    invoke-static {v0}, Lorg/apache/pdfbox/io/IOUtils;->closeQuietly(Ljava/io/Closeable;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/pdfbox/cos/COSStream;->filteredStream:Lorg/apache/pdfbox/io/RandomAccessFileOutputStream;

    iget-object v0, p0, Lorg/apache/pdfbox/cos/COSStream;->unFilteredStream:Lorg/apache/pdfbox/io/RandomAccessFileOutputStream;

    invoke-static {v0}, Lorg/apache/pdfbox/io/IOUtils;->closeQuietly(Ljava/io/Closeable;)V

    new-instance v0, Lorg/apache/pdfbox/io/RandomAccessFileOutputStream;

    iget-object v1, p0, Lorg/apache/pdfbox/cos/COSStream;->buffer:Lorg/apache/pdfbox/io/RandomAccess;

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/io/RandomAccessFileOutputStream;-><init>(Lorg/apache/pdfbox/io/RandomAccess;)V

    iput-object v0, p0, Lorg/apache/pdfbox/cos/COSStream;->unFilteredStream:Lorg/apache/pdfbox/io/RandomAccessFileOutputStream;

    new-instance v0, Ljava/io/BufferedOutputStream;

    iget-object v1, p0, Lorg/apache/pdfbox/cos/COSStream;->unFilteredStream:Lorg/apache/pdfbox/io/RandomAccessFileOutputStream;

    const/16 v2, 0x4000

    invoke-direct {v0, v1, v2}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;I)V

    return-object v0
.end method

.method public getDecodeResult()Lorg/apache/pdfbox/filter/DecodeResult;
    .locals 5

    iget-object v0, p0, Lorg/apache/pdfbox/cos/COSStream;->unFilteredStream:Lorg/apache/pdfbox/io/RandomAccessFileOutputStream;

    if-nez v0, :cond_0

    invoke-direct {p0}, Lorg/apache/pdfbox/cos/COSStream;->doDecode()V

    :cond_0
    iget-object v0, p0, Lorg/apache/pdfbox/cos/COSStream;->unFilteredStream:Lorg/apache/pdfbox/io/RandomAccessFileOutputStream;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lorg/apache/pdfbox/cos/COSStream;->decodeResult:Lorg/apache/pdfbox/filter/DecodeResult;

    if-nez v0, :cond_1

    goto :goto_0

    :cond_1
    return-object v0

    :cond_2
    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lorg/apache/pdfbox/cos/COSStream;->getFilters()Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v1

    if-eqz v1, :cond_5

    const-string v2, " - filter: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    instance-of v2, v1, Lorg/apache/pdfbox/cos/COSName;

    if-nez v2, :cond_4

    instance-of v2, v1, Lorg/apache/pdfbox/cos/COSArray;

    if-eqz v2, :cond_5

    check-cast v1, Lorg/apache/pdfbox/cos/COSArray;

    const/4 v2, 0x0

    :goto_1
    invoke-virtual {v1}, Lorg/apache/pdfbox/cos/COSArray;->size()I

    move-result v3

    if-ge v2, v3, :cond_5

    invoke-virtual {v1}, Lorg/apache/pdfbox/cos/COSArray;->size()I

    move-result v3

    const/4 v4, 0x1

    if-le v3, v4, :cond_3

    const-string v3, ", "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_3
    invoke-virtual {v1, v2}, Lorg/apache/pdfbox/cos/COSArray;->get(I)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v3

    check-cast v3, Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v3}, Lorg/apache/pdfbox/cos/COSName;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_4
    check-cast v1, Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v1}, Lorg/apache/pdfbox/cos/COSName;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_5
    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->SUBTYPE:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getNameAsString(Lorg/apache/pdfbox/cos/COSName;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/io/IOException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " stream was not read"

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2
.end method

.method public getFilteredLength()J
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/cos/COSStream;->filteredStream:Lorg/apache/pdfbox/io/RandomAccessFileOutputStream;

    if-nez v0, :cond_0

    invoke-direct {p0}, Lorg/apache/pdfbox/cos/COSStream;->doEncode()V

    :cond_0
    iget-object v0, p0, Lorg/apache/pdfbox/cos/COSStream;->filteredStream:Lorg/apache/pdfbox/io/RandomAccessFileOutputStream;

    invoke-virtual {v0}, Lorg/apache/pdfbox/io/RandomAccessFileOutputStream;->getLength()J

    move-result-wide v0

    return-wide v0
.end method

.method public getFilteredStream()Ljava/io/InputStream;
    .locals 7

    iget-object v0, p0, Lorg/apache/pdfbox/cos/COSStream;->buffer:Lorg/apache/pdfbox/io/RandomAccess;

    invoke-interface {v0}, Lorg/apache/pdfbox/io/RandomAccessRead;->isClosed()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lorg/apache/pdfbox/cos/COSStream;->filteredStream:Lorg/apache/pdfbox/io/RandomAccessFileOutputStream;

    if-nez v0, :cond_0

    invoke-direct {p0}, Lorg/apache/pdfbox/cos/COSStream;->doEncode()V

    :cond_0
    iget-object v0, p0, Lorg/apache/pdfbox/cos/COSStream;->filteredStream:Lorg/apache/pdfbox/io/RandomAccessFileOutputStream;

    invoke-virtual {v0}, Lorg/apache/pdfbox/io/RandomAccessFileOutputStream;->getPosition()J

    move-result-wide v3

    iget-object v0, p0, Lorg/apache/pdfbox/cos/COSStream;->filteredStream:Lorg/apache/pdfbox/io/RandomAccessFileOutputStream;

    invoke-virtual {v0}, Lorg/apache/pdfbox/io/RandomAccessFileOutputStream;->getLengthWritten()J

    move-result-wide v5

    new-instance v0, Lorg/apache/pdfbox/io/RandomAccessFileInputStream;

    iget-object v2, p0, Lorg/apache/pdfbox/cos/COSStream;->buffer:Lorg/apache/pdfbox/io/RandomAccess;

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lorg/apache/pdfbox/io/RandomAccessFileInputStream;-><init>(Lorg/apache/pdfbox/io/RandomAccess;JJ)V

    new-instance v1, Ljava/io/BufferedInputStream;

    const/16 v2, 0x4000

    invoke-direct {v1, v0, v2}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;I)V

    return-object v1

    :cond_1
    new-instance v0, Ljava/io/IOException;

    const-string v1, "COSStream has been closed and cannot be read. Perhaps its enclosing PDDocument has been closed?"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getFilters()Lorg/apache/pdfbox/cos/COSBase;
    .locals 1

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->FILTER:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p0, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    return-object v0
.end method

.method public getUnfilteredStream()Ljava/io/InputStream;
    .locals 7

    iget-object v0, p0, Lorg/apache/pdfbox/cos/COSStream;->buffer:Lorg/apache/pdfbox/io/RandomAccess;

    invoke-interface {v0}, Lorg/apache/pdfbox/io/RandomAccessRead;->isClosed()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lorg/apache/pdfbox/cos/COSStream;->unFilteredStream:Lorg/apache/pdfbox/io/RandomAccessFileOutputStream;

    if-nez v0, :cond_0

    invoke-direct {p0}, Lorg/apache/pdfbox/cos/COSStream;->doDecode()V

    :cond_0
    iget-object v0, p0, Lorg/apache/pdfbox/cos/COSStream;->unFilteredStream:Lorg/apache/pdfbox/io/RandomAccessFileOutputStream;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lorg/apache/pdfbox/io/RandomAccessFileOutputStream;->getPosition()J

    move-result-wide v3

    iget-object v0, p0, Lorg/apache/pdfbox/cos/COSStream;->unFilteredStream:Lorg/apache/pdfbox/io/RandomAccessFileOutputStream;

    invoke-virtual {v0}, Lorg/apache/pdfbox/io/RandomAccessFileOutputStream;->getLengthWritten()J

    move-result-wide v5

    new-instance v0, Lorg/apache/pdfbox/io/RandomAccessFileInputStream;

    iget-object v2, p0, Lorg/apache/pdfbox/cos/COSStream;->buffer:Lorg/apache/pdfbox/io/RandomAccess;

    move-object v1, v0

    invoke-direct/range {v1 .. v6}, Lorg/apache/pdfbox/io/RandomAccessFileInputStream;-><init>(Lorg/apache/pdfbox/io/RandomAccess;JJ)V

    new-instance v1, Ljava/io/BufferedInputStream;

    const/16 v2, 0x4000

    invoke-direct {v1, v0, v2}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;I)V

    goto :goto_0

    :cond_1
    new-instance v1, Ljava/io/ByteArrayInputStream;

    const/4 v0, 0x0

    new-array v0, v0, [B

    invoke-direct {v1, v0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    :goto_0
    return-object v1

    :cond_2
    new-instance v0, Ljava/io/IOException;

    const-string v1, "COSStream has been closed and cannot be read. Perhaps its enclosing PDDocument has been closed?"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setFilters(Lorg/apache/pdfbox/cos/COSBase;)V
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/cos/COSStream;->unFilteredStream:Lorg/apache/pdfbox/io/RandomAccessFileOutputStream;

    if-nez v0, :cond_0

    invoke-direct {p0}, Lorg/apache/pdfbox/cos/COSStream;->doDecode()V

    :cond_0
    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->FILTER:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p0, v0, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    iget-object p1, p0, Lorg/apache/pdfbox/cos/COSStream;->filteredStream:Lorg/apache/pdfbox/io/RandomAccessFileOutputStream;

    invoke-static {p1}, Lorg/apache/pdfbox/io/IOUtils;->closeQuietly(Ljava/io/Closeable;)V

    const/4 p1, 0x0

    iput-object p1, p0, Lorg/apache/pdfbox/cos/COSStream;->filteredStream:Lorg/apache/pdfbox/io/RandomAccessFileOutputStream;

    return-void
.end method

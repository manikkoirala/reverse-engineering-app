.class public Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver$XRefType;,
        Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver$XrefTrailerObj;
    }
.end annotation


# instance fields
.field private final bytePosToXrefMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver$XrefTrailerObj;",
            ">;"
        }
    .end annotation
.end field

.field private curXrefTrailerObj:Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver$XrefTrailerObj;

.field private resolvedXrefTrailer:Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver$XrefTrailerObj;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver;->bytePosToXrefMap:Ljava/util/Map;

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver;->curXrefTrailerObj:Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver$XrefTrailerObj;

    iput-object v0, p0, Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver;->resolvedXrefTrailer:Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver$XrefTrailerObj;

    return-void
.end method


# virtual methods
.method public getContainedObjectNumbers(I)Ljava/util/Set;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/Set<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver;->resolvedXrefTrailer:Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver$XrefTrailerObj;

    if-nez v0, :cond_0

    const/4 p1, 0x0

    return-object p1

    :cond_0
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    neg-int p1, p1

    int-to-long v1, p1

    iget-object p1, p0, Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver;->resolvedXrefTrailer:Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver$XrefTrailerObj;

    invoke-static {p1}, Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver$XrefTrailerObj;->access$200(Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver$XrefTrailerObj;)Ljava/util/Map;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_1
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    cmp-long v4, v4, v1

    if-nez v4, :cond_1

    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/pdfbox/cos/COSObjectKey;

    invoke-virtual {v3}, Lorg/apache/pdfbox/cos/COSObjectKey;->getNumber()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    return-object v0
.end method

.method public getCurrentTrailer()Lorg/apache/pdfbox/cos/COSDictionary;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver;->curXrefTrailerObj:Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver$XrefTrailerObj;

    iget-object v0, v0, Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver$XrefTrailerObj;->trailer:Lorg/apache/pdfbox/cos/COSDictionary;

    return-object v0
.end method

.method public final getFirstTrailer()Lorg/apache/pdfbox/cos/COSDictionary;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver;->bytePosToXrefMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    :cond_0
    iget-object v0, p0, Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver;->bytePosToXrefMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    new-instance v1, Ljava/util/TreeSet;

    invoke-direct {v1, v0}, Ljava/util/TreeSet;-><init>(Ljava/util/Collection;)V

    iget-object v0, p0, Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver;->bytePosToXrefMap:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/SortedSet;->first()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver$XrefTrailerObj;

    iget-object v0, v0, Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver$XrefTrailerObj;->trailer:Lorg/apache/pdfbox/cos/COSDictionary;

    return-object v0
.end method

.method public final getLastTrailer()Lorg/apache/pdfbox/cos/COSDictionary;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver;->bytePosToXrefMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    :cond_0
    iget-object v0, p0, Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver;->bytePosToXrefMap:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    new-instance v1, Ljava/util/TreeSet;

    invoke-direct {v1, v0}, Ljava/util/TreeSet;-><init>(Ljava/util/Collection;)V

    iget-object v0, p0, Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver;->bytePosToXrefMap:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/SortedSet;->last()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver$XrefTrailerObj;

    iget-object v0, v0, Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver$XrefTrailerObj;->trailer:Lorg/apache/pdfbox/cos/COSDictionary;

    return-object v0
.end method

.method public getTrailer()Lorg/apache/pdfbox/cos/COSDictionary;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver;->resolvedXrefTrailer:Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver$XrefTrailerObj;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    iget-object v0, v0, Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver$XrefTrailerObj;->trailer:Lorg/apache/pdfbox/cos/COSDictionary;

    :goto_0
    return-object v0
.end method

.method public getXrefTable()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Lorg/apache/pdfbox/cos/COSObjectKey;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver;->resolvedXrefTrailer:Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver$XrefTrailerObj;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    invoke-static {v0}, Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver$XrefTrailerObj;->access$200(Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver$XrefTrailerObj;)Ljava/util/Map;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public getXrefType()Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver$XRefType;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver;->resolvedXrefTrailer:Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver$XrefTrailerObj;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    invoke-static {v0}, Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver$XrefTrailerObj;->access$100(Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver$XrefTrailerObj;)Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver$XRefType;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public nextXrefObj(JLorg/apache/pdfbox/pdfparser/XrefTrailerResolver$XRefType;)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver;->bytePosToXrefMap:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    new-instance p2, Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver$XrefTrailerObj;

    const/4 v1, 0x0

    invoke-direct {p2, p0, v1}, Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver$XrefTrailerObj;-><init>(Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver;Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver$1;)V

    iput-object p2, p0, Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver;->curXrefTrailerObj:Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver$XrefTrailerObj;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object p1, p0, Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver;->curXrefTrailerObj:Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver$XrefTrailerObj;

    invoke-static {p1, p3}, Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver$XrefTrailerObj;->access$102(Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver$XrefTrailerObj;Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver$XRefType;)Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver$XRefType;

    return-void
.end method

.method public setStartxref(J)V
    .locals 5

    iget-object v0, p0, Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver;->resolvedXrefTrailer:Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver$XrefTrailerObj;

    const-string v1, "PdfBoxAndroid"

    if-eqz v0, :cond_0

    const-string p1, "Method must be called only ones with last startxref value."

    invoke-static {v1, p1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    new-instance v0, Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver$XrefTrailerObj;

    const/4 v2, 0x0

    invoke-direct {v0, p0, v2}, Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver$XrefTrailerObj;-><init>(Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver;Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver$1;)V

    iput-object v0, p0, Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver;->resolvedXrefTrailer:Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver$XrefTrailerObj;

    new-instance v2, Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-direct {v2}, Lorg/apache/pdfbox/cos/COSDictionary;-><init>()V

    iput-object v2, v0, Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver$XrefTrailerObj;->trailer:Lorg/apache/pdfbox/cos/COSDictionary;

    iget-object v0, p0, Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver;->bytePosToXrefMap:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver$XrefTrailerObj;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Did not found XRef object at specified startxref position "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v1, p1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object p1, p0, Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver;->bytePosToXrefMap:Ljava/util/Map;

    invoke-interface {p1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object p1

    invoke-interface {v2, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    invoke-static {v2}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    goto :goto_1

    :cond_1
    iget-object v3, p0, Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver;->resolvedXrefTrailer:Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver$XrefTrailerObj;

    invoke-static {v0}, Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver$XrefTrailerObj;->access$100(Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver$XrefTrailerObj;)Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver$XRefType;

    move-result-object v4

    invoke-static {v3, v4}, Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver$XrefTrailerObj;->access$102(Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver$XrefTrailerObj;Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver$XRefType;)Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver$XRefType;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-interface {v2, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    iget-object p1, v0, Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver$XrefTrailerObj;->trailer:Lorg/apache/pdfbox/cos/COSDictionary;

    if-eqz p1, :cond_5

    sget-object p2, Lorg/apache/pdfbox/cos/COSName;->PREV:Lorg/apache/pdfbox/cos/COSName;

    const-wide/16 v3, -0x1

    invoke-virtual {p1, p2, v3, v4}, Lorg/apache/pdfbox/cos/COSDictionary;->getLong(Lorg/apache/pdfbox/cos/COSName;J)J

    move-result-wide p1

    cmp-long v0, p1, v3

    if-nez v0, :cond_3

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver;->bytePosToXrefMap:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver$XrefTrailerObj;

    if-nez v0, :cond_4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Did not found XRef object pointed to by \'Prev\' key at position "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v1, p1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_4
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-interface {v2, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result p1

    iget-object p2, p0, Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver;->bytePosToXrefMap:Ljava/util/Map;

    invoke-interface {p2}, Ljava/util/Map;->size()I

    move-result p2

    if-lt p1, p2, :cond_2

    :cond_5
    :goto_0
    invoke-static {v2}, Ljava/util/Collections;->reverse(Ljava/util/List;)V

    :goto_1
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_2
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_7

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/lang/Long;

    iget-object v0, p0, Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver;->bytePosToXrefMap:Ljava/util/Map;

    invoke-interface {v0, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver$XrefTrailerObj;

    iget-object v0, p2, Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver$XrefTrailerObj;->trailer:Lorg/apache/pdfbox/cos/COSDictionary;

    if-eqz v0, :cond_6

    iget-object v1, p0, Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver;->resolvedXrefTrailer:Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver$XrefTrailerObj;

    iget-object v1, v1, Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver$XrefTrailerObj;->trailer:Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-virtual {v1, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->addAll(Lorg/apache/pdfbox/cos/COSDictionary;)V

    :cond_6
    iget-object v0, p0, Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver;->resolvedXrefTrailer:Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver$XrefTrailerObj;

    invoke-static {v0}, Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver$XrefTrailerObj;->access$200(Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver$XrefTrailerObj;)Ljava/util/Map;

    move-result-object v0

    invoke-static {p2}, Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver$XrefTrailerObj;->access$200(Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver$XrefTrailerObj;)Ljava/util/Map;

    move-result-object p2

    invoke-interface {v0, p2}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    goto :goto_2

    :cond_7
    return-void
.end method

.method public setTrailer(Lorg/apache/pdfbox/cos/COSDictionary;)V
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver;->curXrefTrailerObj:Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver$XrefTrailerObj;

    if-nez v0, :cond_0

    const-string p1, "PdfBoxAndroid"

    const-string v0, "Cannot add trailer because XRef start was not signalled."

    invoke-static {p1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    iput-object p1, v0, Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver$XrefTrailerObj;->trailer:Lorg/apache/pdfbox/cos/COSDictionary;

    return-void
.end method

.method public setXRef(Lorg/apache/pdfbox/cos/COSObjectKey;J)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver;->curXrefTrailerObj:Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver$XrefTrailerObj;

    if-nez v0, :cond_0

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string p3, "Cannot add XRef entry for \'"

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lorg/apache/pdfbox/cos/COSObjectKey;->getNumber()J

    move-result-wide v0

    invoke-virtual {p2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string p1, "\' because XRef start was not signalled."

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string p2, "PdfBoxAndroid"

    invoke-static {p2, p1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    invoke-static {v0}, Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver$XrefTrailerObj;->access$200(Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver$XrefTrailerObj;)Ljava/util/Map;

    move-result-object v0

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.class public Lorg/apache/pdfbox/pdfparser/PDFXrefStreamParser;
.super Lorg/apache/pdfbox/pdfparser/BaseParser;
.source "SourceFile"


# instance fields
.field private stream:Lorg/apache/pdfbox/cos/COSStream;

.field private xrefTrailerResolver:Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver;


# direct methods
.method public constructor <init>(Lorg/apache/pdfbox/cos/COSStream;Lorg/apache/pdfbox/cos/COSDocument;Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver;)V
    .locals 1

    invoke-virtual {p1}, Lorg/apache/pdfbox/cos/COSStream;->getUnfilteredStream()Ljava/io/InputStream;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/pdfparser/BaseParser;-><init>(Ljava/io/InputStream;)V

    iput-object p2, p0, Lorg/apache/pdfbox/pdfparser/BaseParser;->document:Lorg/apache/pdfbox/cos/COSDocument;

    iput-object p1, p0, Lorg/apache/pdfbox/pdfparser/PDFXrefStreamParser;->stream:Lorg/apache/pdfbox/cos/COSStream;

    iput-object p3, p0, Lorg/apache/pdfbox/pdfparser/PDFXrefStreamParser;->xrefTrailerResolver:Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver;

    return-void
.end method


# virtual methods
.method public parse()V
    .locals 15

    :try_start_0
    iget-object v0, p0, Lorg/apache/pdfbox/pdfparser/PDFXrefStreamParser;->stream:Lorg/apache/pdfbox/cos/COSStream;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->W:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSArray;

    iget-object v1, p0, Lorg/apache/pdfbox/pdfparser/PDFXrefStreamParser;->stream:Lorg/apache/pdfbox/cos/COSStream;

    sget-object v2, Lorg/apache/pdfbox/cos/COSName;->INDEX:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v1

    check-cast v1, Lorg/apache/pdfbox/cos/COSArray;

    if-nez v1, :cond_0

    new-instance v1, Lorg/apache/pdfbox/cos/COSArray;

    invoke-direct {v1}, Lorg/apache/pdfbox/cos/COSArray;-><init>()V

    sget-object v2, Lorg/apache/pdfbox/cos/COSInteger;->ZERO:Lorg/apache/pdfbox/cos/COSInteger;

    invoke-virtual {v1, v2}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    iget-object v2, p0, Lorg/apache/pdfbox/pdfparser/PDFXrefStreamParser;->stream:Lorg/apache/pdfbox/cos/COSStream;

    sget-object v3, Lorg/apache/pdfbox/cos/COSName;->SIZE:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v2, v3}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v2

    invoke-virtual {v1, v2}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    :cond_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v1}, Lorg/apache/pdfbox/cos/COSArray;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    const/4 v4, 0x0

    if-eqz v3, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/pdfbox/cos/COSInteger;

    invoke-virtual {v3}, Lorg/apache/pdfbox/cos/COSInteger;->longValue()J

    move-result-wide v5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/pdfbox/cos/COSInteger;

    invoke-virtual {v3}, Lorg/apache/pdfbox/cos/COSInteger;->intValue()I

    move-result v3

    :goto_0
    if-ge v4, v3, :cond_1

    int-to-long v7, v4

    add-long/2addr v7, v5

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_2
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    invoke-virtual {v0, v4}, Lorg/apache/pdfbox/cos/COSArray;->getInt(I)I

    move-result v2

    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Lorg/apache/pdfbox/cos/COSArray;->getInt(I)I

    move-result v5

    const/4 v6, 0x2

    invoke-virtual {v0, v6}, Lorg/apache/pdfbox/cos/COSArray;->getInt(I)I

    move-result v0

    add-int v7, v2, v5

    add-int/2addr v7, v0

    :goto_1
    iget-object v8, p0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    invoke-virtual {v8}, Ljava/io/InputStream;->available()I

    move-result v8

    if-lez v8, :cond_9

    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_9

    new-array v8, v7, [B

    iget-object v9, p0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    invoke-virtual {v9, v8}, Lorg/apache/pdfbox/io/PushBackInputStream;->read([B)I

    move v9, v4

    move v10, v9

    :goto_2
    if-ge v9, v2, :cond_3

    aget-byte v11, v8, v9

    and-int/lit16 v11, v11, 0xff

    sub-int v12, v2, v9

    sub-int/2addr v12, v3

    mul-int/lit8 v12, v12, 0x8

    shl-int/2addr v11, v12

    add-int/2addr v10, v11

    add-int/lit8 v9, v9, 0x1

    goto :goto_2

    :cond_3
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Long;

    if-eq v10, v3, :cond_6

    if-eq v10, v6, :cond_4

    goto :goto_1

    :cond_4
    move v10, v4

    move v11, v10

    :goto_3
    if-ge v10, v5, :cond_5

    add-int v12, v10, v2

    aget-byte v12, v8, v12

    and-int/lit16 v12, v12, 0xff

    sub-int v13, v5, v10

    sub-int/2addr v13, v3

    mul-int/lit8 v13, v13, 0x8

    shl-int/2addr v12, v13

    add-int/2addr v11, v12

    add-int/lit8 v10, v10, 0x1

    goto :goto_3

    :cond_5
    new-instance v8, Lorg/apache/pdfbox/cos/COSObjectKey;

    invoke-virtual {v9}, Ljava/lang/Long;->longValue()J

    move-result-wide v9

    invoke-direct {v8, v9, v10, v4}, Lorg/apache/pdfbox/cos/COSObjectKey;-><init>(JI)V

    iget-object v9, p0, Lorg/apache/pdfbox/pdfparser/PDFXrefStreamParser;->xrefTrailerResolver:Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver;

    neg-int v10, v11

    int-to-long v10, v10

    invoke-virtual {v9, v8, v10, v11}, Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver;->setXRef(Lorg/apache/pdfbox/cos/COSObjectKey;J)V

    goto :goto_1

    :cond_6
    move v10, v4

    move v11, v10

    :goto_4
    if-ge v10, v5, :cond_7

    add-int v12, v10, v2

    aget-byte v12, v8, v12

    and-int/lit16 v12, v12, 0xff

    sub-int v13, v5, v10

    sub-int/2addr v13, v3

    mul-int/lit8 v13, v13, 0x8

    shl-int/2addr v12, v13

    add-int/2addr v11, v12

    add-int/lit8 v10, v10, 0x1

    goto :goto_4

    :cond_7
    move v10, v4

    move v12, v10

    :goto_5
    if-ge v10, v0, :cond_8

    add-int v13, v10, v2

    add-int/2addr v13, v5

    aget-byte v13, v8, v13

    and-int/lit16 v13, v13, 0xff

    sub-int v14, v0, v10

    sub-int/2addr v14, v3

    mul-int/lit8 v14, v14, 0x8

    shl-int/2addr v13, v14

    add-int/2addr v12, v13

    add-int/lit8 v10, v10, 0x1

    goto :goto_5

    :cond_8
    new-instance v8, Lorg/apache/pdfbox/cos/COSObjectKey;

    invoke-virtual {v9}, Ljava/lang/Long;->longValue()J

    move-result-wide v9

    invoke-direct {v8, v9, v10, v12}, Lorg/apache/pdfbox/cos/COSObjectKey;-><init>(JI)V

    iget-object v9, p0, Lorg/apache/pdfbox/pdfparser/PDFXrefStreamParser;->xrefTrailerResolver:Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver;

    int-to-long v10, v11

    invoke-virtual {v9, v8, v10, v11}, Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver;->setXRef(Lorg/apache/pdfbox/cos/COSObjectKey;J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/16 :goto_1

    :cond_9
    iget-object v0, p0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    return-void

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    throw v0
.end method

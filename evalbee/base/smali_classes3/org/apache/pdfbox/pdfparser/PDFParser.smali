.class public Lorg/apache/pdfbox/pdfparser/PDFParser;
.super Lorg/apache/pdfbox/pdfparser/COSParser;
.source "SourceFile"


# static fields
.field private static final EMPTY_INPUT_STREAM:Ljava/io/InputStream;


# instance fields
.field private accessPermission:Lorg/apache/pdfbox/pdmodel/encryption/AccessPermission;

.field private keyAlias:Ljava/lang/String;

.field private keyStoreInputStream:Ljava/io/InputStream;

.field private password:Ljava/lang/String;

.field private final raStream:Lorg/apache/pdfbox/io/RandomAccessBufferedFileInputStream;

.field private tempPDFFile:Ljava/io/File;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/io/ByteArrayInputStream;

    const/4 v1, 0x0

    new-array v1, v1, [B

    invoke-direct {v0, v1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    sput-object v0, Lorg/apache/pdfbox/pdfparser/PDFParser;->EMPTY_INPUT_STREAM:Ljava/io/InputStream;

    return-void
.end method

.method public constructor <init>(Ljava/io/File;)V
    .locals 2

    .line 1
    const-string v0, ""

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lorg/apache/pdfbox/pdfparser/PDFParser;-><init>(Ljava/io/File;Ljava/lang/String;Z)V

    return-void
.end method

.method public constructor <init>(Ljava/io/File;Ljava/lang/String;)V
    .locals 1

    .line 2
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/pdfbox/pdfparser/PDFParser;-><init>(Ljava/io/File;Ljava/lang/String;Z)V

    return-void
.end method

.method public constructor <init>(Ljava/io/File;Ljava/lang/String;Ljava/io/InputStream;Ljava/lang/String;)V
    .locals 6

    .line 3
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lorg/apache/pdfbox/pdfparser/PDFParser;-><init>(Ljava/io/File;Ljava/lang/String;Ljava/io/InputStream;Ljava/lang/String;Z)V

    return-void
.end method

.method public constructor <init>(Ljava/io/File;Ljava/lang/String;Ljava/io/InputStream;Ljava/lang/String;Z)V
    .locals 2

    .line 4
    sget-object v0, Lorg/apache/pdfbox/pdfparser/PDFParser;->EMPTY_INPUT_STREAM:Ljava/io/InputStream;

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/pdfparser/COSParser;-><init>(Ljava/io/InputStream;)V

    const-string v0, ""

    iput-object v0, p0, Lorg/apache/pdfbox/pdfparser/PDFParser;->password:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/pdfbox/pdfparser/PDFParser;->keyStoreInputStream:Ljava/io/InputStream;

    iput-object v0, p0, Lorg/apache/pdfbox/pdfparser/PDFParser;->keyAlias:Ljava/lang/String;

    invoke-virtual {p1}, Ljava/io/File;->length()J

    move-result-wide v0

    iput-wide v0, p0, Lorg/apache/pdfbox/pdfparser/COSParser;->fileLen:J

    new-instance v0, Lorg/apache/pdfbox/io/RandomAccessBufferedFileInputStream;

    invoke-direct {v0, p1}, Lorg/apache/pdfbox/io/RandomAccessBufferedFileInputStream;-><init>(Ljava/io/File;)V

    iput-object v0, p0, Lorg/apache/pdfbox/pdfparser/PDFParser;->raStream:Lorg/apache/pdfbox/io/RandomAccessBufferedFileInputStream;

    iput-object p2, p0, Lorg/apache/pdfbox/pdfparser/PDFParser;->password:Ljava/lang/String;

    iput-object p3, p0, Lorg/apache/pdfbox/pdfparser/PDFParser;->keyStoreInputStream:Ljava/io/InputStream;

    iput-object p4, p0, Lorg/apache/pdfbox/pdfparser/PDFParser;->keyAlias:Ljava/lang/String;

    invoke-direct {p0, p5}, Lorg/apache/pdfbox/pdfparser/PDFParser;->init(Z)V

    return-void
.end method

.method public constructor <init>(Ljava/io/File;Ljava/lang/String;Z)V
    .locals 6

    .line 5
    const/4 v3, 0x0

    const/4 v4, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v5, p3

    invoke-direct/range {v0 .. v5}, Lorg/apache/pdfbox/pdfparser/PDFParser;-><init>(Ljava/io/File;Ljava/lang/String;Ljava/io/InputStream;Ljava/lang/String;Z)V

    return-void
.end method

.method public constructor <init>(Ljava/io/File;Z)V
    .locals 1

    .line 6
    const-string v0, ""

    invoke-direct {p0, p1, v0, p2}, Lorg/apache/pdfbox/pdfparser/PDFParser;-><init>(Ljava/io/File;Ljava/lang/String;Z)V

    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 2

    .line 7
    const-string v0, ""

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lorg/apache/pdfbox/pdfparser/PDFParser;-><init>(Ljava/io/InputStream;Ljava/lang/String;Z)V

    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;Ljava/lang/String;)V
    .locals 1

    .line 8
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/pdfbox/pdfparser/PDFParser;-><init>(Ljava/io/InputStream;Ljava/lang/String;Z)V

    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;Ljava/lang/String;Ljava/io/InputStream;Ljava/lang/String;)V
    .locals 6

    .line 9
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lorg/apache/pdfbox/pdfparser/PDFParser;-><init>(Ljava/io/InputStream;Ljava/lang/String;Ljava/io/InputStream;Ljava/lang/String;Z)V

    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;Ljava/lang/String;Ljava/io/InputStream;Ljava/lang/String;Z)V
    .locals 2

    .line 10
    sget-object v0, Lorg/apache/pdfbox/pdfparser/PDFParser;->EMPTY_INPUT_STREAM:Ljava/io/InputStream;

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/pdfparser/COSParser;-><init>(Ljava/io/InputStream;)V

    const-string v0, ""

    iput-object v0, p0, Lorg/apache/pdfbox/pdfparser/PDFParser;->password:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/pdfbox/pdfparser/PDFParser;->keyStoreInputStream:Ljava/io/InputStream;

    iput-object v0, p0, Lorg/apache/pdfbox/pdfparser/PDFParser;->keyAlias:Ljava/lang/String;

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/pdfparser/COSParser;->createTmpFile(Ljava/io/InputStream;)Ljava/io/File;

    move-result-object p1

    iput-object p1, p0, Lorg/apache/pdfbox/pdfparser/PDFParser;->tempPDFFile:Ljava/io/File;

    invoke-virtual {p1}, Ljava/io/File;->length()J

    move-result-wide v0

    iput-wide v0, p0, Lorg/apache/pdfbox/pdfparser/COSParser;->fileLen:J

    new-instance p1, Lorg/apache/pdfbox/io/RandomAccessBufferedFileInputStream;

    iget-object v0, p0, Lorg/apache/pdfbox/pdfparser/PDFParser;->tempPDFFile:Ljava/io/File;

    invoke-direct {p1, v0}, Lorg/apache/pdfbox/io/RandomAccessBufferedFileInputStream;-><init>(Ljava/io/File;)V

    iput-object p1, p0, Lorg/apache/pdfbox/pdfparser/PDFParser;->raStream:Lorg/apache/pdfbox/io/RandomAccessBufferedFileInputStream;

    iput-object p2, p0, Lorg/apache/pdfbox/pdfparser/PDFParser;->password:Ljava/lang/String;

    iput-object p3, p0, Lorg/apache/pdfbox/pdfparser/PDFParser;->keyStoreInputStream:Ljava/io/InputStream;

    iput-object p4, p0, Lorg/apache/pdfbox/pdfparser/PDFParser;->keyAlias:Ljava/lang/String;

    invoke-direct {p0, p5}, Lorg/apache/pdfbox/pdfparser/PDFParser;->init(Z)V

    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;Ljava/lang/String;Z)V
    .locals 6

    .line 11
    const/4 v3, 0x0

    const/4 v4, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v5, p3

    invoke-direct/range {v0 .. v5}, Lorg/apache/pdfbox/pdfparser/PDFParser;-><init>(Ljava/io/InputStream;Ljava/lang/String;Ljava/io/InputStream;Ljava/lang/String;Z)V

    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;Z)V
    .locals 1

    .line 12
    const-string v0, ""

    invoke-direct {p0, p1, v0, p2}, Lorg/apache/pdfbox/pdfparser/PDFParser;-><init>(Ljava/io/InputStream;Ljava/lang/String;Z)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 2

    .line 13
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const/4 p1, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, v0, p1, v1}, Lorg/apache/pdfbox/pdfparser/PDFParser;-><init>(Ljava/io/File;Ljava/lang/String;Z)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Z)V
    .locals 1

    .line 14
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const/4 p1, 0x0

    invoke-direct {p0, v0, p1, p2}, Lorg/apache/pdfbox/pdfparser/PDFParser;-><init>(Ljava/io/File;Ljava/lang/String;Z)V

    return-void
.end method

.method private deleteTempFile()V
    .locals 5

    const-string v0, "\' can\'t be deleted"

    const-string v1, "Temporary file \'"

    const-string v2, "PdfBoxAndroid"

    iget-object v3, p0, Lorg/apache/pdfbox/pdfparser/PDFParser;->tempPDFFile:Ljava/io/File;

    if-eqz v3, :cond_0

    :try_start_0
    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    move-result v3

    if-nez v3, :cond_0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, p0, Lorg/apache/pdfbox/pdfparser/PDFParser;->tempPDFFile:Ljava/io/File;

    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lorg/apache/pdfbox/pdfparser/PDFParser;->tempPDFFile:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_0
    :goto_0
    return-void
.end method

.method private init(Z)V
    .locals 3

    const-string v0, "org.apache.pdfbox.pdfparser.nonSequentialPDFParser.eofLookupRange"

    invoke-static {v0}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    :try_start_0
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p0, v1}, Lorg/apache/pdfbox/pdfparser/COSParser;->setEOFLookupRange(I)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "System property org.apache.pdfbox.pdfparser.nonSequentialPDFParser.eofLookupRange does not contain an integer value, but: \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "\'"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "PdfBoxAndroid"

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    new-instance v0, Lorg/apache/pdfbox/cos/COSDocument;

    invoke-direct {v0, p1}, Lorg/apache/pdfbox/cos/COSDocument;-><init>(Z)V

    iput-object v0, p0, Lorg/apache/pdfbox/pdfparser/BaseParser;->document:Lorg/apache/pdfbox/cos/COSDocument;

    new-instance p1, Lorg/apache/pdfbox/io/PushBackInputStream;

    iget-object v0, p0, Lorg/apache/pdfbox/pdfparser/PDFParser;->raStream:Lorg/apache/pdfbox/io/RandomAccessBufferedFileInputStream;

    const/16 v1, 0x1000

    invoke-direct {p1, v0, v1}, Lorg/apache/pdfbox/io/PushBackInputStream;-><init>(Ljava/io/InputStream;I)V

    iput-object p1, p0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    return-void
.end method

.method private parseDictionaryRecursive(Lorg/apache/pdfbox/cos/COSObject;)V
    .locals 2

    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lorg/apache/pdfbox/pdfparser/COSParser;->parseObjectDynamically(Lorg/apache/pdfbox/cos/COSObject;Z)Lorg/apache/pdfbox/cos/COSBase;

    invoke-virtual {p1}, Lorg/apache/pdfbox/cos/COSObject;->getObject()Lorg/apache/pdfbox/cos/COSBase;

    move-result-object p1

    check-cast p1, Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-virtual {p1}, Lorg/apache/pdfbox/cos/COSDictionary;->getValues()Ljava/util/Collection;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSBase;

    instance-of v1, v0, Lorg/apache/pdfbox/cos/COSObject;

    if-eqz v1, :cond_0

    check-cast v0, Lorg/apache/pdfbox/cos/COSObject;

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSObject;->getObject()Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/pdfparser/PDFParser;->parseDictionaryRecursive(Lorg/apache/pdfbox/cos/COSObject;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method private prepareDecryption()V
    .locals 5

    iget-object v0, p0, Lorg/apache/pdfbox/pdfparser/BaseParser;->document:Lorg/apache/pdfbox/cos/COSDocument;

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSDocument;->getTrailer()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->ENCRYPT:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getItem(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    if-eqz v0, :cond_2

    instance-of v1, v0, Lorg/apache/pdfbox/cos/COSNull;

    if-nez v1, :cond_2

    instance-of v1, v0, Lorg/apache/pdfbox/cos/COSObject;

    if-eqz v1, :cond_0

    check-cast v0, Lorg/apache/pdfbox/cos/COSObject;

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/pdfparser/PDFParser;->parseDictionaryRecursive(Lorg/apache/pdfbox/cos/COSObject;)V

    :cond_0
    :try_start_0
    new-instance v0, Lorg/apache/pdfbox/pdmodel/encryption/PDEncryption;

    iget-object v1, p0, Lorg/apache/pdfbox/pdfparser/BaseParser;->document:Lorg/apache/pdfbox/cos/COSDocument;

    invoke-virtual {v1}, Lorg/apache/pdfbox/cos/COSDocument;->getEncryptionDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/pdmodel/encryption/PDEncryption;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    iget-object v1, p0, Lorg/apache/pdfbox/pdfparser/PDFParser;->keyStoreInputStream:Ljava/io/InputStream;

    if-eqz v1, :cond_1

    const-string v1, "PKCS12"

    invoke-static {v1}, Ljava/security/KeyStore;->getInstance(Ljava/lang/String;)Ljava/security/KeyStore;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/pdfbox/pdfparser/PDFParser;->keyStoreInputStream:Ljava/io/InputStream;

    iget-object v3, p0, Lorg/apache/pdfbox/pdfparser/PDFParser;->password:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->toCharArray()[C

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/security/KeyStore;->load(Ljava/io/InputStream;[C)V

    new-instance v2, Lorg/apache/pdfbox/pdmodel/encryption/PublicKeyDecryptionMaterial;

    iget-object v3, p0, Lorg/apache/pdfbox/pdfparser/PDFParser;->keyAlias:Ljava/lang/String;

    iget-object v4, p0, Lorg/apache/pdfbox/pdfparser/PDFParser;->password:Ljava/lang/String;

    invoke-direct {v2, v1, v3, v4}, Lorg/apache/pdfbox/pdmodel/encryption/PublicKeyDecryptionMaterial;-><init>(Ljava/security/KeyStore;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    new-instance v2, Lorg/apache/pdfbox/pdmodel/encryption/StandardDecryptionMaterial;

    iget-object v1, p0, Lorg/apache/pdfbox/pdfparser/PDFParser;->password:Ljava/lang/String;

    invoke-direct {v2, v1}, Lorg/apache/pdfbox/pdmodel/encryption/StandardDecryptionMaterial;-><init>(Ljava/lang/String;)V

    :goto_0
    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/encryption/PDEncryption;->getSecurityHandler()Lorg/apache/pdfbox/pdmodel/encryption/SecurityHandler;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/pdfbox/pdfparser/COSParser;->securityHandler:Lorg/apache/pdfbox/pdmodel/encryption/SecurityHandler;

    iget-object v3, p0, Lorg/apache/pdfbox/pdfparser/BaseParser;->document:Lorg/apache/pdfbox/cos/COSDocument;

    invoke-virtual {v3}, Lorg/apache/pdfbox/cos/COSDocument;->getDocumentID()Lorg/apache/pdfbox/cos/COSArray;

    move-result-object v3

    invoke-virtual {v1, v0, v3, v2}, Lorg/apache/pdfbox/pdmodel/encryption/SecurityHandler;->prepareForDecryption(Lorg/apache/pdfbox/pdmodel/encryption/PDEncryption;Lorg/apache/pdfbox/cos/COSArray;Lorg/apache/pdfbox/pdmodel/encryption/DecryptionMaterial;)V

    iget-object v0, p0, Lorg/apache/pdfbox/pdfparser/COSParser;->securityHandler:Lorg/apache/pdfbox/pdmodel/encryption/SecurityHandler;

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/encryption/SecurityHandler;->getCurrentAccessPermission()Lorg/apache/pdfbox/pdmodel/encryption/AccessPermission;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/pdfbox/pdfparser/PDFParser;->accessPermission:Lorg/apache/pdfbox/pdmodel/encryption/AccessPermission;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, ") while creating security handler for decryption"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    :catch_1
    move-exception v0

    throw v0

    :cond_2
    :goto_1
    return-void
.end method


# virtual methods
.method public getPDDocument()Lorg/apache/pdfbox/pdmodel/PDDocument;
    .locals 3

    new-instance v0, Lorg/apache/pdfbox/pdmodel/PDDocument;

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfparser/COSParser;->getDocument()Lorg/apache/pdfbox/cos/COSDocument;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/pdfbox/pdfparser/PDFParser;->accessPermission:Lorg/apache/pdfbox/pdmodel/encryption/AccessPermission;

    invoke-direct {v0, v1, p0, v2}, Lorg/apache/pdfbox/pdmodel/PDDocument;-><init>(Lorg/apache/pdfbox/cos/COSDocument;Lorg/apache/pdfbox/pdfparser/BaseParser;Lorg/apache/pdfbox/pdmodel/encryption/AccessPermission;)V

    return-object v0
.end method

.method public initialParse()V
    .locals 6

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfparser/COSParser;->getStartxrefOffset()J

    move-result-wide v0

    const-wide/16 v2, -0x1

    cmp-long v2, v0, v2

    const/4 v3, 0x0

    if-lez v2, :cond_0

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdfparser/COSParser;->parseXref(J)Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfparser/COSParser;->isLenient()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfparser/COSParser;->rebuildTrailer()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    goto :goto_0

    :cond_1
    move-object v0, v3

    :goto_0
    invoke-direct {p0}, Lorg/apache/pdfbox/pdfparser/PDFParser;->prepareDecryption()V

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSDictionary;->getValues()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    const/4 v4, 0x0

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/pdfbox/cos/COSBase;

    instance-of v5, v2, Lorg/apache/pdfbox/cos/COSObject;

    if-eqz v5, :cond_2

    check-cast v2, Lorg/apache/pdfbox/cos/COSObject;

    invoke-virtual {p0, v2, v4}, Lorg/apache/pdfbox/pdfparser/COSParser;->parseObjectDynamically(Lorg/apache/pdfbox/cos/COSObject;Z)Lorg/apache/pdfbox/cos/COSBase;

    goto :goto_1

    :cond_3
    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->ROOT:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getItem(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSObject;

    if-eqz v0, :cond_5

    invoke-virtual {p0, v0, v4}, Lorg/apache/pdfbox/pdfparser/COSParser;->parseObjectDynamically(Lorg/apache/pdfbox/cos/COSObject;Z)Lorg/apache/pdfbox/cos/COSBase;

    iget-object v0, p0, Lorg/apache/pdfbox/pdfparser/BaseParser;->document:Lorg/apache/pdfbox/cos/COSDocument;

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSDocument;->getCatalog()Lorg/apache/pdfbox/cos/COSObject;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSObject;->getObject()Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v1

    instance-of v1, v1, Lorg/apache/pdfbox/cos/COSDictionary;

    if-eqz v1, :cond_4

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSObject;->getObject()Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-virtual {p0, v0, v3}, Lorg/apache/pdfbox/pdfparser/COSParser;->parseDictObjects(Lorg/apache/pdfbox/cos/COSDictionary;[Lorg/apache/pdfbox/cos/COSName;)V

    iget-object v0, p0, Lorg/apache/pdfbox/pdfparser/BaseParser;->document:Lorg/apache/pdfbox/cos/COSDocument;

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSDocument;->setDecrypted()V

    :cond_4
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/pdfbox/pdfparser/COSParser;->initialParseDone:Z

    return-void

    :cond_5
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Missing root object specification in trailer."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public parse()V
    .locals 2

    :try_start_0
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfparser/COSParser;->parsePDFHeader()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfparser/COSParser;->parseFDFHeader()Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Error: Header doesn\'t contain versioninfo"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    :goto_0
    iget-boolean v0, p0, Lorg/apache/pdfbox/pdfparser/COSParser;->initialParseDone:Z

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfparser/PDFParser;->initialParse()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_2
    iget-object v0, p0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    invoke-static {v0}, Lorg/apache/pdfbox/io/IOUtils;->closeQuietly(Ljava/io/Closeable;)V

    iget-object v0, p0, Lorg/apache/pdfbox/pdfparser/PDFParser;->keyStoreInputStream:Ljava/io/InputStream;

    invoke-static {v0}, Lorg/apache/pdfbox/io/IOUtils;->closeQuietly(Ljava/io/Closeable;)V

    invoke-direct {p0}, Lorg/apache/pdfbox/pdfparser/PDFParser;->deleteTempFile()V

    return-void

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    invoke-static {v1}, Lorg/apache/pdfbox/io/IOUtils;->closeQuietly(Ljava/io/Closeable;)V

    iget-object v1, p0, Lorg/apache/pdfbox/pdfparser/PDFParser;->keyStoreInputStream:Ljava/io/InputStream;

    invoke-static {v1}, Lorg/apache/pdfbox/io/IOUtils;->closeQuietly(Ljava/io/Closeable;)V

    invoke-direct {p0}, Lorg/apache/pdfbox/pdfparser/PDFParser;->deleteTempFile()V

    iget-object v1, p0, Lorg/apache/pdfbox/pdfparser/BaseParser;->document:Lorg/apache/pdfbox/cos/COSDocument;

    if-eqz v1, :cond_3

    :try_start_1
    invoke-virtual {v1}, Lorg/apache/pdfbox/cos/COSDocument;->close()V

    const/4 v1, 0x0

    iput-object v1, p0, Lorg/apache/pdfbox/pdfparser/BaseParser;->document:Lorg/apache/pdfbox/cos/COSDocument;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    :cond_3
    throw v0
.end method

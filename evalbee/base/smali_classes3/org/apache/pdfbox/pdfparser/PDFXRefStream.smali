.class public Lorg/apache/pdfbox/pdfparser/PDFXRefStream;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/pdfbox/pdfparser/PDFXRef;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/pdfbox/pdfparser/PDFXRefStream$FreeReference;,
        Lorg/apache/pdfbox/pdfparser/PDFXRefStream$NormalReference;,
        Lorg/apache/pdfbox/pdfparser/PDFXRefStream$ObjectStreamReference;
    }
.end annotation


# static fields
.field private static final ENTRY_FREE:I = 0x0

.field private static final ENTRY_NORMAL:I = 0x1

.field private static final ENTRY_OBJSTREAM:I = 0x2


# instance fields
.field private final objectNumbers:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private size:J

.field private final stream:Lorg/apache/pdfbox/cos/COSStream;

.field private final streamData:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lorg/apache/pdfbox/pdfparser/PDFXRefStream;->size:J

    new-instance v0, Lorg/apache/pdfbox/cos/COSStream;

    new-instance v1, Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-direct {v1}, Lorg/apache/pdfbox/cos/COSDictionary;-><init>()V

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSStream;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    iput-object v0, p0, Lorg/apache/pdfbox/pdfparser/PDFXRefStream;->stream:Lorg/apache/pdfbox/cos/COSStream;

    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0}, Ljava/util/TreeMap;-><init>()V

    iput-object v0, p0, Lorg/apache/pdfbox/pdfparser/PDFXRefStream;->streamData:Ljava/util/Map;

    new-instance v0, Ljava/util/TreeSet;

    invoke-direct {v0}, Ljava/util/TreeSet;-><init>()V

    iput-object v0, p0, Lorg/apache/pdfbox/pdfparser/PDFXRefStream;->objectNumbers:Ljava/util/Set;

    return-void
.end method

.method private getIndexEntry()Ljava/util/List;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iget-object v1, p0, Lorg/apache/pdfbox/pdfparser/PDFXRefStream;->objectNumbers:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    const/4 v2, 0x0

    move-object v3, v2

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    const-wide/16 v5, 0x1

    if-nez v2, :cond_1

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    move-object v2, v4

    :cond_1
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v9

    add-long/2addr v7, v9

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v9

    cmp-long v7, v7, v9

    if-nez v7, :cond_2

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    add-long/2addr v7, v5

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    :cond_2
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v9

    add-long/2addr v7, v9

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v9

    cmp-long v7, v7, v9

    if-gez v7, :cond_0

    invoke-virtual {v0, v2}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v0, v3}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    move-object v3, v2

    move-object v2, v4

    goto :goto_0

    :cond_3
    invoke-virtual {v0, v2}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v0, v3}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    return-object v0
.end method

.method private getSizeEntry()J
    .locals 2

    iget-wide v0, p0, Lorg/apache/pdfbox/pdfparser/PDFXRefStream;->size:J

    return-wide v0
.end method

.method private getWEntry()[I
    .locals 12

    const/4 v0, 0x3

    new-array v1, v0, [J

    iget-object v2, p0, Lorg/apache/pdfbox/pdfparser/PDFXRefStream;->streamData:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    const-wide/16 v4, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x1

    if-eqz v3, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    instance-of v8, v3, Lorg/apache/pdfbox/pdfparser/PDFXRefStream$FreeReference;

    const/4 v9, 0x2

    if-eqz v8, :cond_0

    check-cast v3, Lorg/apache/pdfbox/pdfparser/PDFXRefStream$FreeReference;

    aget-wide v10, v1, v6

    invoke-static {v10, v11, v4, v5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v4

    aput-wide v4, v1, v6

    aget-wide v4, v1, v7

    iget-wide v10, v3, Lorg/apache/pdfbox/pdfparser/PDFXRefStream$FreeReference;->nextFree:J

    invoke-static {v4, v5, v10, v11}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v4

    aput-wide v4, v1, v7

    aget-wide v4, v1, v9

    iget v3, v3, Lorg/apache/pdfbox/pdfparser/PDFXRefStream$FreeReference;->nextGenNumber:I

    int-to-long v6, v3

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v3

    aput-wide v3, v1, v9

    goto :goto_0

    :cond_0
    instance-of v4, v3, Lorg/apache/pdfbox/pdfparser/PDFXRefStream$NormalReference;

    if-eqz v4, :cond_1

    check-cast v3, Lorg/apache/pdfbox/pdfparser/PDFXRefStream$NormalReference;

    aget-wide v4, v1, v6

    const-wide/16 v10, 0x1

    invoke-static {v4, v5, v10, v11}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v4

    aput-wide v4, v1, v6

    aget-wide v4, v1, v7

    iget-wide v10, v3, Lorg/apache/pdfbox/pdfparser/PDFXRefStream$NormalReference;->offset:J

    invoke-static {v4, v5, v10, v11}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v4

    aput-wide v4, v1, v7

    aget-wide v4, v1, v9

    iget v3, v3, Lorg/apache/pdfbox/pdfparser/PDFXRefStream$NormalReference;->genNumber:I

    int-to-long v6, v3

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v3

    aput-wide v3, v1, v9

    goto :goto_0

    :cond_1
    instance-of v4, v3, Lorg/apache/pdfbox/pdfparser/PDFXRefStream$ObjectStreamReference;

    if-eqz v4, :cond_2

    check-cast v3, Lorg/apache/pdfbox/pdfparser/PDFXRefStream$ObjectStreamReference;

    aget-wide v4, v1, v6

    const-wide/16 v10, 0x2

    invoke-static {v4, v5, v10, v11}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v4

    aput-wide v4, v1, v6

    aget-wide v4, v1, v7

    iget-wide v10, v3, Lorg/apache/pdfbox/pdfparser/PDFXRefStream$ObjectStreamReference;->offset:J

    invoke-static {v4, v5, v10, v11}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v4

    aput-wide v4, v1, v7

    aget-wide v4, v1, v9

    iget-wide v6, v3, Lorg/apache/pdfbox/pdfparser/PDFXRefStream$ObjectStreamReference;->objectNumberOfObjectStream:J

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v3

    aput-wide v3, v1, v9

    goto :goto_0

    :cond_2
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "unexpected reference type"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    new-array v2, v0, [I

    :goto_1
    if-ge v6, v0, :cond_5

    :goto_2
    aget-wide v8, v1, v6

    cmp-long v3, v8, v4

    if-lez v3, :cond_4

    aget v3, v2, v6

    add-int/2addr v3, v7

    aput v3, v2, v6

    const/16 v3, 0x8

    shr-long/2addr v8, v3

    aput-wide v8, v1, v6

    goto :goto_2

    :cond_4
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    :cond_5
    return-object v2
.end method

.method private writeNumber(Ljava/io/OutputStream;JI)V
    .locals 5

    new-array v0, p4, [B

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, p4, :cond_0

    const-wide/16 v3, 0xff

    and-long/2addr v3, p2

    long-to-int v3, v3

    int-to-byte v3, v3

    aput-byte v3, v0, v2

    const/16 v3, 0x8

    shr-long/2addr p2, v3

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    :goto_1
    if-ge v1, p4, :cond_1

    sub-int p2, p4, v1

    add-int/lit8 p2, p2, -0x1

    aget-byte p2, v0, p2

    invoke-virtual {p1, p2}, Ljava/io/OutputStream;->write(I)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_1
    return-void
.end method

.method private writeStreamData(Ljava/io/OutputStream;[I)V
    .locals 7

    iget-object v0, p0, Lorg/apache/pdfbox/pdfparser/PDFXRefStream;->streamData:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    instance-of v2, v1, Lorg/apache/pdfbox/pdfparser/PDFXRefStream$FreeReference;

    const/4 v3, 0x2

    const/4 v4, 0x1

    const/4 v5, 0x0

    if-eqz v2, :cond_0

    check-cast v1, Lorg/apache/pdfbox/pdfparser/PDFXRefStream$FreeReference;

    aget v2, p2, v5

    const-wide/16 v5, 0x0

    invoke-direct {p0, p1, v5, v6, v2}, Lorg/apache/pdfbox/pdfparser/PDFXRefStream;->writeNumber(Ljava/io/OutputStream;JI)V

    iget-wide v5, v1, Lorg/apache/pdfbox/pdfparser/PDFXRefStream$FreeReference;->nextFree:J

    aget v2, p2, v4

    invoke-direct {p0, p1, v5, v6, v2}, Lorg/apache/pdfbox/pdfparser/PDFXRefStream;->writeNumber(Ljava/io/OutputStream;JI)V

    iget v1, v1, Lorg/apache/pdfbox/pdfparser/PDFXRefStream$FreeReference;->nextGenNumber:I

    int-to-long v1, v1

    aget v3, p2, v3

    :goto_1
    invoke-direct {p0, p1, v1, v2, v3}, Lorg/apache/pdfbox/pdfparser/PDFXRefStream;->writeNumber(Ljava/io/OutputStream;JI)V

    goto :goto_0

    :cond_0
    instance-of v2, v1, Lorg/apache/pdfbox/pdfparser/PDFXRefStream$NormalReference;

    if-eqz v2, :cond_1

    check-cast v1, Lorg/apache/pdfbox/pdfparser/PDFXRefStream$NormalReference;

    aget v2, p2, v5

    const-wide/16 v5, 0x1

    invoke-direct {p0, p1, v5, v6, v2}, Lorg/apache/pdfbox/pdfparser/PDFXRefStream;->writeNumber(Ljava/io/OutputStream;JI)V

    iget-wide v5, v1, Lorg/apache/pdfbox/pdfparser/PDFXRefStream$NormalReference;->offset:J

    aget v2, p2, v4

    invoke-direct {p0, p1, v5, v6, v2}, Lorg/apache/pdfbox/pdfparser/PDFXRefStream;->writeNumber(Ljava/io/OutputStream;JI)V

    iget v1, v1, Lorg/apache/pdfbox/pdfparser/PDFXRefStream$NormalReference;->genNumber:I

    int-to-long v1, v1

    aget v3, p2, v3

    goto :goto_1

    :cond_1
    instance-of v2, v1, Lorg/apache/pdfbox/pdfparser/PDFXRefStream$ObjectStreamReference;

    if-eqz v2, :cond_2

    check-cast v1, Lorg/apache/pdfbox/pdfparser/PDFXRefStream$ObjectStreamReference;

    aget v2, p2, v5

    const-wide/16 v5, 0x2

    invoke-direct {p0, p1, v5, v6, v2}, Lorg/apache/pdfbox/pdfparser/PDFXRefStream;->writeNumber(Ljava/io/OutputStream;JI)V

    iget-wide v5, v1, Lorg/apache/pdfbox/pdfparser/PDFXRefStream$ObjectStreamReference;->offset:J

    aget v2, p2, v4

    invoke-direct {p0, p1, v5, v6, v2}, Lorg/apache/pdfbox/pdfparser/PDFXRefStream;->writeNumber(Ljava/io/OutputStream;JI)V

    iget-wide v1, v1, Lorg/apache/pdfbox/pdfparser/PDFXRefStream$ObjectStreamReference;->objectNumberOfObjectStream:J

    aget v3, p2, v3

    goto :goto_1

    :cond_2
    new-instance p1, Ljava/lang/RuntimeException;

    const-string p2, "unexpected reference type"

    invoke-direct {p1, p2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_3
    invoke-virtual {p1}, Ljava/io/OutputStream;->flush()V

    invoke-virtual {p1}, Ljava/io/OutputStream;->close()V

    return-void
.end method


# virtual methods
.method public addEntry(Lorg/apache/pdfbox/pdfwriter/COSWriterXRefEntry;)V
    .locals 4

    iget-object v0, p0, Lorg/apache/pdfbox/pdfparser/PDFXRefStream;->objectNumbers:Ljava/util/Set;

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdfwriter/COSWriterXRefEntry;->getKey()Lorg/apache/pdfbox/cos/COSObjectKey;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/pdfbox/cos/COSObjectKey;->getNumber()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdfwriter/COSWriterXRefEntry;->isFree()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lorg/apache/pdfbox/pdfparser/PDFXRefStream$FreeReference;

    invoke-direct {v0, p0}, Lorg/apache/pdfbox/pdfparser/PDFXRefStream$FreeReference;-><init>(Lorg/apache/pdfbox/pdfparser/PDFXRefStream;)V

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdfwriter/COSWriterXRefEntry;->getKey()Lorg/apache/pdfbox/cos/COSObjectKey;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/pdfbox/cos/COSObjectKey;->getGeneration()I

    move-result v1

    iput v1, v0, Lorg/apache/pdfbox/pdfparser/PDFXRefStream$FreeReference;->nextGenNumber:I

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdfwriter/COSWriterXRefEntry;->getKey()Lorg/apache/pdfbox/cos/COSObjectKey;

    move-result-object p1

    invoke-virtual {p1}, Lorg/apache/pdfbox/cos/COSObjectKey;->getNumber()J

    move-result-wide v1

    iput-wide v1, v0, Lorg/apache/pdfbox/pdfparser/PDFXRefStream$FreeReference;->nextFree:J

    iget-object p1, p0, Lorg/apache/pdfbox/pdfparser/PDFXRefStream;->streamData:Ljava/util/Map;

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {p1, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    new-instance v0, Lorg/apache/pdfbox/pdfparser/PDFXRefStream$NormalReference;

    invoke-direct {v0, p0}, Lorg/apache/pdfbox/pdfparser/PDFXRefStream$NormalReference;-><init>(Lorg/apache/pdfbox/pdfparser/PDFXRefStream;)V

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdfwriter/COSWriterXRefEntry;->getKey()Lorg/apache/pdfbox/cos/COSObjectKey;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/pdfbox/cos/COSObjectKey;->getGeneration()I

    move-result v1

    iput v1, v0, Lorg/apache/pdfbox/pdfparser/PDFXRefStream$NormalReference;->genNumber:I

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdfwriter/COSWriterXRefEntry;->getOffset()J

    move-result-wide v1

    iput-wide v1, v0, Lorg/apache/pdfbox/pdfparser/PDFXRefStream$NormalReference;->offset:J

    iget-object v1, p0, Lorg/apache/pdfbox/pdfparser/PDFXRefStream;->streamData:Ljava/util/Map;

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdfwriter/COSWriterXRefEntry;->getKey()Lorg/apache/pdfbox/cos/COSObjectKey;

    move-result-object p1

    invoke-virtual {p1}, Lorg/apache/pdfbox/cos/COSObjectKey;->getNumber()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p1

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_0
    return-void
.end method

.method public addTrailerInfo(Lorg/apache/pdfbox/cos/COSDictionary;)V
    .locals 3

    invoke-virtual {p1}, Lorg/apache/pdfbox/cos/COSDictionary;->entrySet()Ljava/util/Set;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/pdfbox/cos/COSName;

    sget-object v2, Lorg/apache/pdfbox/cos/COSName;->INFO:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v2, v1}, Lorg/apache/pdfbox/cos/COSName;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    sget-object v2, Lorg/apache/pdfbox/cos/COSName;->ROOT:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v2, v1}, Lorg/apache/pdfbox/cos/COSName;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    sget-object v2, Lorg/apache/pdfbox/cos/COSName;->ENCRYPT:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v2, v1}, Lorg/apache/pdfbox/cos/COSName;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    sget-object v2, Lorg/apache/pdfbox/cos/COSName;->ID:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v2, v1}, Lorg/apache/pdfbox/cos/COSName;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    sget-object v2, Lorg/apache/pdfbox/cos/COSName;->PREV:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v2, v1}, Lorg/apache/pdfbox/cos/COSName;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_1
    iget-object v2, p0, Lorg/apache/pdfbox/pdfparser/PDFXRefStream;->stream:Lorg/apache/pdfbox/cos/COSStream;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSBase;

    invoke-virtual {v2, v1, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    goto :goto_0

    :cond_2
    return-void
.end method

.method public getObject(I)Lorg/apache/pdfbox/cos/COSObject;
    .locals 0

    const/4 p1, 0x0

    return-object p1
.end method

.method public getStream()Lorg/apache/pdfbox/cos/COSStream;
    .locals 5

    iget-object v0, p0, Lorg/apache/pdfbox/pdfparser/PDFXRefStream;->stream:Lorg/apache/pdfbox/cos/COSStream;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->TYPE:Lorg/apache/pdfbox/cos/COSName;

    sget-object v2, Lorg/apache/pdfbox/cos/COSName;->XREF:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    iget-wide v0, p0, Lorg/apache/pdfbox/pdfparser/PDFXRefStream;->size:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_3

    iget-object v0, p0, Lorg/apache/pdfbox/pdfparser/PDFXRefStream;->stream:Lorg/apache/pdfbox/cos/COSStream;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->SIZE:Lorg/apache/pdfbox/cos/COSName;

    invoke-direct {p0}, Lorg/apache/pdfbox/pdfparser/PDFXRefStream;->getSizeEntry()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lorg/apache/pdfbox/cos/COSDictionary;->setLong(Lorg/apache/pdfbox/cos/COSName;J)V

    iget-object v0, p0, Lorg/apache/pdfbox/pdfparser/PDFXRefStream;->stream:Lorg/apache/pdfbox/cos/COSStream;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->FLATE_DECODE:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSStream;->setFilters(Lorg/apache/pdfbox/cos/COSBase;)V

    invoke-direct {p0}, Lorg/apache/pdfbox/pdfparser/PDFXRefStream;->getIndexEntry()Ljava/util/List;

    move-result-object v0

    new-instance v1, Lorg/apache/pdfbox/cos/COSArray;

    invoke-direct {v1}, Lorg/apache/pdfbox/cos/COSArray;-><init>()V

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v2, v3}, Lorg/apache/pdfbox/cos/COSInteger;->get(J)Lorg/apache/pdfbox/cos/COSInteger;

    move-result-object v2

    invoke-virtual {v1, v2}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lorg/apache/pdfbox/pdfparser/PDFXRefStream;->stream:Lorg/apache/pdfbox/cos/COSStream;

    sget-object v2, Lorg/apache/pdfbox/cos/COSName;->INDEX:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v2, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    invoke-direct {p0}, Lorg/apache/pdfbox/pdfparser/PDFXRefStream;->getWEntry()[I

    move-result-object v0

    new-instance v1, Lorg/apache/pdfbox/cos/COSArray;

    invoke-direct {v1}, Lorg/apache/pdfbox/cos/COSArray;-><init>()V

    const/4 v2, 0x0

    :goto_1
    array-length v3, v0

    if-ge v2, v3, :cond_1

    aget v3, v0, v2

    int-to-long v3, v3

    invoke-static {v3, v4}, Lorg/apache/pdfbox/cos/COSInteger;->get(J)Lorg/apache/pdfbox/cos/COSInteger;

    move-result-object v3

    invoke-virtual {v1, v3}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_1
    iget-object v2, p0, Lorg/apache/pdfbox/pdfparser/PDFXRefStream;->stream:Lorg/apache/pdfbox/cos/COSStream;

    sget-object v3, Lorg/apache/pdfbox/cos/COSName;->W:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v2, v3, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    iget-object v1, p0, Lorg/apache/pdfbox/pdfparser/PDFXRefStream;->stream:Lorg/apache/pdfbox/cos/COSStream;

    invoke-virtual {v1}, Lorg/apache/pdfbox/cos/COSStream;->createUnfilteredStream()Ljava/io/OutputStream;

    move-result-object v1

    invoke-direct {p0, v1, v0}, Lorg/apache/pdfbox/pdfparser/PDFXRefStream;->writeStreamData(Ljava/io/OutputStream;[I)V

    iget-object v0, p0, Lorg/apache/pdfbox/pdfparser/PDFXRefStream;->stream:Lorg/apache/pdfbox/cos/COSStream;

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSDictionary;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/pdfbox/cos/COSName;

    iget-object v2, p0, Lorg/apache/pdfbox/pdfparser/PDFXRefStream;->stream:Lorg/apache/pdfbox/cos/COSStream;

    invoke-virtual {v2, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lorg/apache/pdfbox/cos/COSBase;->setDirect(Z)V

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lorg/apache/pdfbox/pdfparser/PDFXRefStream;->stream:Lorg/apache/pdfbox/cos/COSStream;

    return-object v0

    :cond_3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "size is not set in xrefstream"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setSize(J)V
    .locals 0

    iput-wide p1, p0, Lorg/apache/pdfbox/pdfparser/PDFXRefStream;->size:J

    return-void
.end method

.class public Lorg/apache/pdfbox/pdfparser/PDFStreamParser;
.super Lorg/apache/pdfbox/pdfparser/BaseParser;
.source "SourceFile"


# static fields
.field private static final MAX_BIN_CHAR_TEST_LENGTH:I = 0xa


# instance fields
.field private final binCharTestArr:[B

.field private final streamObjects:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 1

    .line 1
    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdfparser/BaseParser;-><init>(Ljava/io/InputStream;)V

    new-instance p1, Ljava/util/ArrayList;

    const/16 v0, 0x64

    invoke-direct {p1, v0}, Ljava/util/ArrayList;-><init>(I)V

    iput-object p1, p0, Lorg/apache/pdfbox/pdfparser/PDFStreamParser;->streamObjects:Ljava/util/List;

    const/16 p1, 0xa

    new-array p1, p1, [B

    iput-object p1, p0, Lorg/apache/pdfbox/pdfparser/PDFStreamParser;->binCharTestArr:[B

    return-void
.end method

.method public constructor <init>(Lorg/apache/pdfbox/cos/COSStream;)V
    .locals 0

    .line 2
    invoke-virtual {p1}, Lorg/apache/pdfbox/cos/COSStream;->getUnfilteredStream()Ljava/io/InputStream;

    move-result-object p1

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdfparser/PDFStreamParser;-><init>(Ljava/io/InputStream;)V

    return-void
.end method

.method public constructor <init>(Lorg/apache/pdfbox/pdmodel/common/PDStream;)V
    .locals 0

    .line 3
    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/common/PDStream;->createInputStream()Ljava/io/InputStream;

    move-result-object p1

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdfparser/PDFStreamParser;-><init>(Ljava/io/InputStream;)V

    return-void
.end method

.method public static synthetic access$000(Lorg/apache/pdfbox/pdfparser/PDFStreamParser;)Ljava/lang/Object;
    .locals 0

    invoke-direct {p0}, Lorg/apache/pdfbox/pdfparser/PDFStreamParser;->parseNextToken()Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method private hasNextSpaceOrReturn()Z
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    invoke-virtual {v0}, Lorg/apache/pdfbox/io/PushBackInputStream;->peek()I

    move-result v0

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/pdfparser/PDFStreamParser;->isSpaceOrReturn(I)Z

    move-result v0

    return v0
.end method

.method private hasNoFollowingBinData(Ljava/io/PushbackInputStream;)Z
    .locals 12

    iget-object v0, p0, Lorg/apache/pdfbox/pdfparser/PDFStreamParser;->binCharTestArr:[B

    const/4 v1, 0x0

    const/16 v2, 0xa

    invoke-virtual {p1, v0, v1, v2}, Ljava/io/PushbackInputStream;->read([BII)I

    move-result v0

    const/4 v3, 0x1

    if-lez v0, :cond_8

    const/4 v4, -0x1

    move v5, v1

    move v6, v4

    move v7, v6

    :goto_0
    if-ge v5, v0, :cond_5

    iget-object v8, p0, Lorg/apache/pdfbox/pdfparser/PDFStreamParser;->binCharTestArr:[B

    aget-byte v8, v8, v5

    const/16 v9, 0x9

    if-lt v8, v9, :cond_4

    const/16 v10, 0xd

    const/16 v11, 0x20

    if-le v8, v2, :cond_0

    if-ge v8, v11, :cond_0

    if-eq v8, v10, :cond_0

    goto :goto_2

    :cond_0
    if-ne v6, v4, :cond_1

    if-eq v8, v9, :cond_1

    if-eq v8, v11, :cond_1

    if-eq v8, v2, :cond_1

    if-eq v8, v10, :cond_1

    move v6, v5

    goto :goto_1

    :cond_1
    if-eq v6, v4, :cond_3

    if-ne v7, v4, :cond_3

    if-eq v8, v9, :cond_2

    if-eq v8, v11, :cond_2

    if-eq v8, v2, :cond_2

    if-ne v8, v10, :cond_3

    :cond_2
    move v7, v5

    :cond_3
    :goto_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    :cond_4
    :goto_2
    move v3, v1

    :cond_5
    if-ne v0, v2, :cond_7

    if-eq v6, v4, :cond_6

    if-ne v7, v4, :cond_6

    goto :goto_3

    :cond_6
    move v2, v7

    :goto_3
    if-eq v2, v4, :cond_7

    if-eq v6, v4, :cond_7

    sub-int/2addr v2, v6

    const/4 v4, 0x3

    if-le v2, v4, :cond_7

    move v3, v1

    :cond_7
    iget-object v2, p0, Lorg/apache/pdfbox/pdfparser/PDFStreamParser;->binCharTestArr:[B

    invoke-virtual {p1, v2, v1, v0}, Ljava/io/PushbackInputStream;->unread([BII)V

    :cond_8
    if-nez v3, :cond_9

    const-string p1, "PdfBoxAndroid"

    const-string v0, "ignoring \'EI\' assumed to be in the middle of inline image"

    invoke-static {p1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_9
    return v3
.end method

.method private hasPrecedingAscii85Data(Ljava/io/ByteArrayOutputStream;)Z
    .locals 6

    invoke-virtual {p1}, Ljava/io/ByteArrayOutputStream;->size()I

    move-result v0

    const/4 v1, 0x0

    const/16 v2, 0x46

    if-ge v0, v2, :cond_0

    return v1

    :cond_0
    invoke-virtual {p1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object p1

    array-length v0, p1

    const/4 v3, 0x1

    sub-int/2addr v0, v3

    :goto_0
    array-length v4, p1

    sub-int/2addr v4, v2

    if-lt v0, v4, :cond_3

    aget-byte v4, p1, v0

    const/16 v5, 0x21

    if-lt v4, v5, :cond_2

    const/16 v5, 0x75

    if-le v4, v5, :cond_1

    goto :goto_1

    :cond_1
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_2
    :goto_1
    return v1

    :cond_3
    return v3
.end method

.method private isSpaceOrReturn(I)Z
    .locals 1

    const/16 v0, 0xa

    if-eq p1, v0, :cond_1

    const/16 v0, 0xd

    if-eq p1, v0, :cond_1

    const/16 v0, 0x20

    if-ne p1, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method

.method private parseNextToken()Ljava/lang/Object;
    .locals 7

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfparser/BaseParser;->skipSpaces()V

    iget-object v0, p0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    invoke-virtual {v0}, Lorg/apache/pdfbox/io/PushBackInputStream;->peek()I

    move-result v0

    int-to-byte v1, v0

    const/4 v2, -0x1

    const/4 v3, 0x0

    if-ne v1, v2, :cond_0

    return-object v3

    :cond_0
    int-to-char v0, v0

    sparse-switch v0, :sswitch_data_0

    packed-switch v0, :pswitch_data_0

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfparser/PDFStreamParser;->readOperator()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_1

    goto/16 :goto_6

    :cond_1
    invoke-static {v0}, Lorg/apache/pdfbox/contentstream/operator/Operator;->getOperator(Ljava/lang/String;)Lorg/apache/pdfbox/contentstream/operator/Operator;

    move-result-object v3

    goto/16 :goto_6

    :pswitch_0
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfparser/BaseParser;->parseCOSName()Lorg/apache/pdfbox/cos/COSName;

    move-result-object v3

    goto/16 :goto_6

    :sswitch_0
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfparser/BaseParser;->readString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "null"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    goto :goto_0

    :sswitch_1
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfparser/BaseParser;->readString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "true"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    sget-object v3, Lorg/apache/pdfbox/cos/COSBoolean;->TRUE:Lorg/apache/pdfbox/cos/COSBoolean;

    goto/16 :goto_6

    :cond_2
    const-string v1, "false"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    sget-object v3, Lorg/apache/pdfbox/cos/COSBoolean;->FALSE:Lorg/apache/pdfbox/cos/COSBoolean;

    goto/16 :goto_6

    :sswitch_2
    iget-object v0, p0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    invoke-virtual {v0}, Lorg/apache/pdfbox/io/PushBackInputStream;->read()I

    :goto_0
    sget-object v3, Lorg/apache/pdfbox/cos/COSNull;->NULL:Lorg/apache/pdfbox/cos/COSNull;

    goto/16 :goto_6

    :sswitch_3
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfparser/BaseParser;->parseCOSArray()Lorg/apache/pdfbox/cos/COSArray;

    move-result-object v3

    goto/16 :goto_6

    :sswitch_4
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfparser/BaseParser;->readString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "R"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance v0, Lorg/apache/pdfbox/cos/COSObject;

    invoke-direct {v0, v3}, Lorg/apache/pdfbox/cos/COSObject;-><init>(Lorg/apache/pdfbox/cos/COSBase;)V

    :cond_3
    :goto_1
    move-object v3, v0

    goto/16 :goto_6

    :sswitch_5
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    invoke-virtual {v1}, Lorg/apache/pdfbox/io/PushBackInputStream;->read()I

    move-result v1

    int-to-char v1, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    invoke-virtual {v1}, Lorg/apache/pdfbox/io/PushBackInputStream;->read()I

    move-result v1

    int-to-char v1, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ID"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfparser/BaseParser;->isWhitespace()Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    invoke-virtual {v2}, Lorg/apache/pdfbox/io/PushBackInputStream;->read()I

    :cond_4
    iget-object v2, p0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    invoke-virtual {v2}, Lorg/apache/pdfbox/io/PushBackInputStream;->read()I

    move-result v2

    iget-object v3, p0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    invoke-virtual {v3}, Lorg/apache/pdfbox/io/PushBackInputStream;->read()I

    move-result v3

    :goto_2
    const/16 v4, 0x45

    if-ne v2, v4, :cond_5

    const/16 v4, 0x49

    if-ne v3, v4, :cond_5

    invoke-direct {p0}, Lorg/apache/pdfbox/pdfparser/PDFStreamParser;->hasNextSpaceOrReturn()Z

    move-result v4

    if-eqz v4, :cond_5

    iget-object v4, p0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    invoke-direct {p0, v4}, Lorg/apache/pdfbox/pdfparser/PDFStreamParser;->hasNoFollowingBinData(Ljava/io/PushbackInputStream;)Z

    move-result v4

    if-nez v4, :cond_6

    :cond_5
    iget-object v4, p0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    invoke-virtual {v4}, Lorg/apache/pdfbox/io/PushBackInputStream;->isEOF()Z

    move-result v4

    if-nez v4, :cond_6

    invoke-virtual {v0, v2}, Ljava/io/ByteArrayOutputStream;->write(I)V

    iget-object v2, p0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    invoke-virtual {v2}, Lorg/apache/pdfbox/io/PushBackInputStream;->read()I

    move-result v2

    move v6, v3

    move v3, v2

    move v2, v6

    goto :goto_2

    :cond_6
    invoke-static {v1}, Lorg/apache/pdfbox/contentstream/operator/Operator;->getOperator(Ljava/lang/String;)Lorg/apache/pdfbox/contentstream/operator/Operator;

    move-result-object v3

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    invoke-virtual {v3, v0}, Lorg/apache/pdfbox/contentstream/operator/Operator;->setImageData([B)V

    goto/16 :goto_6

    :cond_7
    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error: Expected operator \'ID\' actual=\'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "\'"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    :sswitch_6
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfparser/BaseParser;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/pdfbox/contentstream/operator/Operator;->getOperator(Ljava/lang/String;)Lorg/apache/pdfbox/contentstream/operator/Operator;

    move-result-object v3

    const-string v1, "BI"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_e

    new-instance v0, Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-direct {v0}, Lorg/apache/pdfbox/cos/COSDictionary;-><init>()V

    invoke-virtual {v3, v0}, Lorg/apache/pdfbox/contentstream/operator/Operator;->setImageParameters(Lorg/apache/pdfbox/cos/COSDictionary;)V

    :goto_3
    invoke-direct {p0}, Lorg/apache/pdfbox/pdfparser/PDFStreamParser;->parseNextToken()Ljava/lang/Object;

    move-result-object v1

    instance-of v2, v1, Lorg/apache/pdfbox/cos/COSName;

    if-eqz v2, :cond_8

    invoke-direct {p0}, Lorg/apache/pdfbox/pdfparser/PDFStreamParser;->parseNextToken()Ljava/lang/Object;

    move-result-object v2

    check-cast v1, Lorg/apache/pdfbox/cos/COSName;

    check-cast v2, Lorg/apache/pdfbox/cos/COSBase;

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    goto :goto_3

    :cond_8
    check-cast v1, Lorg/apache/pdfbox/contentstream/operator/Operator;

    invoke-virtual {v1}, Lorg/apache/pdfbox/contentstream/operator/Operator;->getImageData()[B

    move-result-object v0

    invoke-virtual {v3, v0}, Lorg/apache/pdfbox/contentstream/operator/Operator;->setImageData([B)V

    goto/16 :goto_6

    :sswitch_7
    iget-object v0, p0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    invoke-virtual {v0}, Lorg/apache/pdfbox/io/PushBackInputStream;->read()I

    move-result v0

    iget-object v1, p0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    invoke-virtual {v1}, Lorg/apache/pdfbox/io/PushBackInputStream;->peek()I

    move-result v1

    int-to-char v1, v1

    iget-object v2, p0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    invoke-virtual {v2, v0}, Lorg/apache/pdfbox/io/PushBackInputStream;->unread(I)V

    const/16 v0, 0x3c

    if-ne v1, v0, :cond_d

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfparser/BaseParser;->parseCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfparser/BaseParser;->skipSpaces()V

    iget-object v1, p0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    invoke-virtual {v1}, Lorg/apache/pdfbox/io/PushBackInputStream;->peek()I

    move-result v1

    int-to-char v1, v1

    const/16 v2, 0x73

    if-ne v1, v2, :cond_3

    invoke-virtual {p0, v0}, Lorg/apache/pdfbox/pdfparser/BaseParser;->parseCOSStream(Lorg/apache/pdfbox/cos/COSDictionary;)Lorg/apache/pdfbox/cos/COSStream;

    move-result-object v0

    goto/16 :goto_1

    :pswitch_1
    :sswitch_8
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    iget-object v2, p0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    invoke-virtual {v2}, Lorg/apache/pdfbox/io/PushBackInputStream;->read()I

    const/4 v2, 0x0

    const/16 v3, 0x2e

    if-eq v0, v3, :cond_c

    const/4 v0, 0x1

    :cond_9
    :goto_4
    iget-object v4, p0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    invoke-virtual {v4}, Lorg/apache/pdfbox/io/PushBackInputStream;->peek()I

    move-result v4

    int-to-char v4, v4

    invoke-static {v4}, Ljava/lang/Character;->isDigit(C)Z

    move-result v5

    if-nez v5, :cond_b

    if-eqz v0, :cond_a

    if-ne v4, v3, :cond_a

    goto :goto_5

    :cond_a
    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/pdfbox/cos/COSNumber;->get(Ljava/lang/String;)Lorg/apache/pdfbox/cos/COSNumber;

    move-result-object v3

    goto :goto_6

    :cond_b
    :goto_5
    invoke-virtual {v1, v4}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    iget-object v5, p0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    invoke-virtual {v5}, Lorg/apache/pdfbox/io/PushBackInputStream;->read()I

    if-eqz v0, :cond_9

    if-ne v4, v3, :cond_9

    :cond_c
    move v0, v2

    goto :goto_4

    :cond_d
    :sswitch_9
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfparser/BaseParser;->parseCOSString()Lorg/apache/pdfbox/cos/COSString;

    move-result-object v3

    :cond_e
    :goto_6
    return-object v3

    :sswitch_data_0
    .sparse-switch
        0x28 -> :sswitch_9
        0x2b -> :sswitch_8
        0x3c -> :sswitch_7
        0x42 -> :sswitch_6
        0x49 -> :sswitch_5
        0x52 -> :sswitch_4
        0x5b -> :sswitch_3
        0x5d -> :sswitch_2
        0x66 -> :sswitch_1
        0x6e -> :sswitch_0
        0x74 -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x2d
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public getTokenIterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    new-instance v0, Lorg/apache/pdfbox/pdfparser/PDFStreamParser$1;

    invoke-direct {v0, p0}, Lorg/apache/pdfbox/pdfparser/PDFStreamParser$1;-><init>(Lorg/apache/pdfbox/pdfparser/PDFStreamParser;)V

    return-object v0
.end method

.method public getTokens()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/pdfbox/pdfparser/PDFStreamParser;->streamObjects:Ljava/util/List;

    return-object v0
.end method

.method public parse()V
    .locals 2

    :goto_0
    :try_start_0
    invoke-direct {p0}, Lorg/apache/pdfbox/pdfparser/PDFStreamParser;->parseNextToken()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lorg/apache/pdfbox/pdfparser/PDFStreamParser;->streamObjects:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    return-void

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    throw v0
.end method

.method public readOperator()Ljava/lang/String;
    .locals 5

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfparser/BaseParser;->skipSpaces()V

    new-instance v0, Ljava/lang/StringBuffer;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Ljava/lang/StringBuffer;-><init>(I)V

    :goto_0
    iget-object v1, p0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    invoke-virtual {v1}, Lorg/apache/pdfbox/io/PushBackInputStream;->peek()I

    move-result v1

    :goto_1
    const/4 v2, -0x1

    if-eq v1, v2, :cond_3

    invoke-virtual {p0, v1}, Lorg/apache/pdfbox/pdfparser/BaseParser;->isWhitespace(I)Z

    move-result v2

    if-nez v2, :cond_3

    invoke-virtual {p0, v1}, Lorg/apache/pdfbox/pdfparser/BaseParser;->isClosing(I)Z

    move-result v2

    if-nez v2, :cond_3

    const/16 v2, 0x5b

    if-eq v1, v2, :cond_3

    const/16 v2, 0x3c

    if-eq v1, v2, :cond_3

    const/16 v2, 0x28

    if-eq v1, v2, :cond_3

    const/16 v2, 0x2f

    if-eq v1, v2, :cond_3

    const/16 v2, 0x30

    if-lt v1, v2, :cond_0

    const/16 v3, 0x39

    if-le v1, v3, :cond_3

    :cond_0
    iget-object v1, p0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    invoke-virtual {v1}, Lorg/apache/pdfbox/io/PushBackInputStream;->read()I

    move-result v1

    int-to-char v1, v1

    iget-object v3, p0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    invoke-virtual {v3}, Lorg/apache/pdfbox/io/PushBackInputStream;->peek()I

    move-result v3

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    const/16 v4, 0x64

    if-ne v1, v4, :cond_2

    if-eq v3, v2, :cond_1

    const/16 v1, 0x31

    if-ne v3, v1, :cond_2

    :cond_1
    iget-object v1, p0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    invoke-virtual {v1}, Lorg/apache/pdfbox/io/PushBackInputStream;->read()I

    move-result v1

    int-to-char v1, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_0

    :cond_2
    move v1, v3

    goto :goto_1

    :cond_3
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

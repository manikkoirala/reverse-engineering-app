.class Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver$XrefTrailerObj;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "XrefTrailerObj"
.end annotation


# instance fields
.field final synthetic this$0:Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver;

.field protected trailer:Lorg/apache/pdfbox/cos/COSDictionary;

.field private final xrefTable:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lorg/apache/pdfbox/cos/COSObjectKey;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private xrefType:Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver$XRefType;


# direct methods
.method private constructor <init>(Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver$XrefTrailerObj;->this$0:Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 p1, 0x0

    iput-object p1, p0, Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver$XrefTrailerObj;->trailer:Lorg/apache/pdfbox/cos/COSDictionary;

    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    iput-object p1, p0, Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver$XrefTrailerObj;->xrefTable:Ljava/util/Map;

    sget-object p1, Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver$XRefType;->TABLE:Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver$XRefType;

    iput-object p1, p0, Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver$XrefTrailerObj;->xrefType:Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver$XRefType;

    return-void
.end method

.method public synthetic constructor <init>(Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver;Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver$1;)V
    .locals 0

    .line 2
    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver$XrefTrailerObj;-><init>(Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver;)V

    return-void
.end method

.method public static synthetic access$100(Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver$XrefTrailerObj;)Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver$XRefType;
    .locals 0

    iget-object p0, p0, Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver$XrefTrailerObj;->xrefType:Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver$XRefType;

    return-object p0
.end method

.method public static synthetic access$102(Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver$XrefTrailerObj;Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver$XRefType;)Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver$XRefType;
    .locals 0

    iput-object p1, p0, Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver$XrefTrailerObj;->xrefType:Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver$XRefType;

    return-object p1
.end method

.method public static synthetic access$200(Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver$XrefTrailerObj;)Ljava/util/Map;
    .locals 0

    iget-object p0, p0, Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver$XrefTrailerObj;->xrefTable:Ljava/util/Map;

    return-object p0
.end method

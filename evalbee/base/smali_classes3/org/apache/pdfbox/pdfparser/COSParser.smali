.class public Lorg/apache/pdfbox/pdfparser/COSParser;
.super Lorg/apache/pdfbox/pdfparser/BaseParser;
.source "SourceFile"


# static fields
.field private static final DEFAULT_TRAIL_BYTECOUNT:I = 0x800

.field protected static final EOF_MARKER:[C

.field private static final FDF_DEFAULT_VERSION:Ljava/lang/String; = "1.0"

.field private static final FDF_HEADER:Ljava/lang/String; = "%FDF-"

.field private static final MINIMUM_SEARCH_OFFSET:J = 0x6L

.field protected static final OBJ_MARKER:[C

.field private static final PDF_DEFAULT_VERSION:Ljava/lang/String; = "1.4"

.field private static final PDF_HEADER:Ljava/lang/String; = "%PDF-"

.field private static final STARTXREF:[C

.field private static final STREAMCOPYBUFLEN:I = 0x2000

.field public static final SYSPROP_EOFLOOKUPRANGE:Ljava/lang/String; = "org.apache.pdfbox.pdfparser.nonSequentialPDFParser.eofLookupRange"

.field public static final SYSPROP_PARSEMINIMAL:Ljava/lang/String; = "org.apache.pdfbox.pdfparser.nonSequentialPDFParser.parseMinimal"

.field public static final TMP_FILE_PREFIX:Ljava/lang/String; = "tmpPDF"

.field private static final X:I = 0x78

.field private static final XREF_STREAM:[C

.field private static final XREF_TABLE:[C


# instance fields
.field private bfSearchCOSObjectKeyOffsets:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lorg/apache/pdfbox/cos/COSObjectKey;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private bfSearchXRefStreamsOffsets:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private bfSearchXRefTablesOffsets:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field protected fileLen:J

.field private inGetLength:Z

.field protected initialParseDone:Z

.field private isLenient:Z

.field private final parseMinimalCatalog:Z

.field private readTrailBytes:I

.field protected securityHandler:Lorg/apache/pdfbox/pdmodel/encryption/SecurityHandler;

.field private final streamCopyBuf:[B

.field private trailerOffset:J

.field protected xrefTrailerResolver:Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    const/4 v0, 0x4

    new-array v0, v0, [C

    fill-array-data v0, :array_0

    sput-object v0, Lorg/apache/pdfbox/pdfparser/COSParser;->XREF_TABLE:[C

    const/4 v0, 0x5

    new-array v1, v0, [C

    fill-array-data v1, :array_1

    sput-object v1, Lorg/apache/pdfbox/pdfparser/COSParser;->XREF_STREAM:[C

    const/16 v1, 0x9

    new-array v1, v1, [C

    fill-array-data v1, :array_2

    sput-object v1, Lorg/apache/pdfbox/pdfparser/COSParser;->STARTXREF:[C

    new-array v0, v0, [C

    fill-array-data v0, :array_3

    sput-object v0, Lorg/apache/pdfbox/pdfparser/COSParser;->EOF_MARKER:[C

    const/4 v0, 0x3

    new-array v0, v0, [C

    fill-array-data v0, :array_4

    sput-object v0, Lorg/apache/pdfbox/pdfparser/COSParser;->OBJ_MARKER:[C

    return-void

    nop

    :array_0
    .array-data 2
        0x78s
        0x72s
        0x65s
        0x66s
    .end array-data

    :array_1
    .array-data 2
        0x2fs
        0x58s
        0x52s
        0x65s
        0x66s
    .end array-data

    nop

    :array_2
    .array-data 2
        0x73s
        0x74s
        0x61s
        0x72s
        0x74s
        0x78s
        0x72s
        0x65s
        0x66s
    .end array-data

    nop

    :array_3
    .array-data 2
        0x25s
        0x25s
        0x45s
        0x4fs
        0x46s
    .end array-data

    nop

    :array_4
    .array-data 2
        0x6fs
        0x62s
        0x6as
    .end array-data
.end method

.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 2

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdfparser/BaseParser;-><init>(Ljava/io/InputStream;)V

    const/4 p1, 0x1

    iput-boolean p1, p0, Lorg/apache/pdfbox/pdfparser/COSParser;->isLenient:Z

    const/4 p1, 0x0

    iput-boolean p1, p0, Lorg/apache/pdfbox/pdfparser/COSParser;->initialParseDone:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/pdfbox/pdfparser/COSParser;->bfSearchCOSObjectKeyOffsets:Ljava/util/Map;

    iput-object v0, p0, Lorg/apache/pdfbox/pdfparser/COSParser;->bfSearchXRefTablesOffsets:Ljava/util/List;

    iput-object v0, p0, Lorg/apache/pdfbox/pdfparser/COSParser;->bfSearchXRefStreamsOffsets:Ljava/util/List;

    iput-object v0, p0, Lorg/apache/pdfbox/pdfparser/COSParser;->securityHandler:Lorg/apache/pdfbox/pdmodel/encryption/SecurityHandler;

    const/16 v0, 0x800

    iput v0, p0, Lorg/apache/pdfbox/pdfparser/COSParser;->readTrailBytes:I

    const-string v0, "org.apache.pdfbox.pdfparser.nonSequentialPDFParser.parseMinimal"

    invoke-static {v0}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "true"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, Lorg/apache/pdfbox/pdfparser/COSParser;->parseMinimalCatalog:Z

    new-instance v0, Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver;

    invoke-direct {v0}, Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver;-><init>()V

    iput-object v0, p0, Lorg/apache/pdfbox/pdfparser/COSParser;->xrefTrailerResolver:Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver;

    iput-boolean p1, p0, Lorg/apache/pdfbox/pdfparser/COSParser;->inGetLength:Z

    const/16 p1, 0x2000

    new-array p1, p1, [B

    iput-object p1, p0, Lorg/apache/pdfbox/pdfparser/COSParser;->streamCopyBuf:[B

    return-void
.end method

.method private addExcludedToList([Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSDictionary;Ljava/util/Set;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Lorg/apache/pdfbox/cos/COSName;",
            "Lorg/apache/pdfbox/cos/COSDictionary;",
            "Ljava/util/Set<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    if-eqz p1, :cond_1

    array-length v0, p1

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_1

    aget-object v2, p1, v1

    invoke-virtual {p2, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->getItem(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v2

    instance-of v3, v2, Lorg/apache/pdfbox/cos/COSObject;

    if-eqz v3, :cond_0

    check-cast v2, Lorg/apache/pdfbox/cos/COSObject;

    invoke-direct {p0, v2}, Lorg/apache/pdfbox/pdfparser/COSParser;->getObjectId(Lorg/apache/pdfbox/cos/COSObject;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {p3, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method private addNewToList(Ljava/util/Queue;Ljava/util/Collection;Ljava/util/Set;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Queue<",
            "Lorg/apache/pdfbox/cos/COSBase;",
            ">;",
            "Ljava/util/Collection<",
            "Lorg/apache/pdfbox/cos/COSBase;",
            ">;",
            "Ljava/util/Set<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .line 1
    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSBase;

    invoke-direct {p0, p1, v0, p3}, Lorg/apache/pdfbox/pdfparser/COSParser;->addNewToList(Ljava/util/Queue;Lorg/apache/pdfbox/cos/COSBase;Ljava/util/Set;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private addNewToList(Ljava/util/Queue;Lorg/apache/pdfbox/cos/COSBase;Ljava/util/Set;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Queue<",
            "Lorg/apache/pdfbox/cos/COSBase;",
            ">;",
            "Lorg/apache/pdfbox/cos/COSBase;",
            "Ljava/util/Set<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .line 2
    instance-of v0, p2, Lorg/apache/pdfbox/cos/COSObject;

    if-eqz v0, :cond_0

    move-object v0, p2

    check-cast v0, Lorg/apache/pdfbox/cos/COSObject;

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/pdfparser/COSParser;->getObjectId(Lorg/apache/pdfbox/cos/COSObject;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {p3, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result p3

    if-nez p3, :cond_0

    return-void

    :cond_0
    invoke-interface {p1, p2}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method private bfSearchForObjects()V
    .locals 16

    move-object/from16 v0, p0

    iget-object v1, v0, Lorg/apache/pdfbox/pdfparser/COSParser;->bfSearchCOSObjectKeyOffsets:Ljava/util/Map;

    if-nez v1, :cond_4

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, v0, Lorg/apache/pdfbox/pdfparser/COSParser;->bfSearchCOSObjectKeyOffsets:Ljava/util/Map;

    iget-object v1, v0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    invoke-virtual {v1}, Lorg/apache/pdfbox/io/PushBackInputStream;->getOffset()J

    move-result-wide v1

    const-string v3, " obj"

    invoke-virtual {v3}, Ljava/lang/String;->toCharArray()[C

    move-result-object v3

    const-wide/16 v4, 0x6

    move-wide v6, v4

    :goto_0
    iget-object v8, v0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    invoke-virtual {v8, v6, v7}, Lorg/apache/pdfbox/io/PushBackInputStream;->seek(J)V

    invoke-virtual {v0, v3}, Lorg/apache/pdfbox/pdfparser/BaseParser;->isString([C)Z

    move-result v8

    const-wide/16 v9, 0x1

    if-eqz v8, :cond_2

    sub-long v11, v6, v9

    iget-object v8, v0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    invoke-virtual {v8, v11, v12}, Lorg/apache/pdfbox/io/PushBackInputStream;->seek(J)V

    iget-object v8, v0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    invoke-virtual {v8}, Lorg/apache/pdfbox/io/PushBackInputStream;->peek()I

    move-result v8

    const/16 v13, 0x2f

    if-le v8, v13, :cond_2

    const/16 v13, 0x3a

    if-ge v8, v13, :cond_2

    add-int/lit8 v8, v8, -0x30

    sub-long/2addr v11, v9

    iget-object v13, v0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    invoke-virtual {v13, v11, v12}, Lorg/apache/pdfbox/io/PushBackInputStream;->seek(J)V

    invoke-virtual/range {p0 .. p0}, Lorg/apache/pdfbox/pdfparser/BaseParser;->isSpace()Z

    move-result v13

    if-eqz v13, :cond_2

    :goto_1
    cmp-long v13, v11, v4

    if-lez v13, :cond_0

    invoke-virtual/range {p0 .. p0}, Lorg/apache/pdfbox/pdfparser/BaseParser;->isSpace()Z

    move-result v13

    if-eqz v13, :cond_0

    iget-object v13, v0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    sub-long/2addr v11, v9

    invoke-virtual {v13, v11, v12}, Lorg/apache/pdfbox/io/PushBackInputStream;->seek(J)V

    goto :goto_1

    :cond_0
    const/4 v13, 0x0

    move v14, v13

    :goto_2
    cmp-long v15, v11, v4

    if-lez v15, :cond_1

    invoke-virtual/range {p0 .. p0}, Lorg/apache/pdfbox/pdfparser/BaseParser;->isDigit()Z

    move-result v15

    if-eqz v15, :cond_1

    iget-object v15, v0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    sub-long/2addr v11, v9

    invoke-virtual {v15, v11, v12}, Lorg/apache/pdfbox/io/PushBackInputStream;->seek(J)V

    add-int/lit8 v14, v14, 0x1

    goto :goto_2

    :cond_1
    if-lez v14, :cond_2

    iget-object v15, v0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    invoke-virtual {v15}, Lorg/apache/pdfbox/io/PushBackInputStream;->read()I

    iget-object v15, v0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    invoke-virtual {v15, v14}, Lorg/apache/pdfbox/io/PushBackInputStream;->readFully(I)[B

    move-result-object v14

    new-instance v15, Ljava/lang/String;

    array-length v4, v14

    const-string v5, "ISO-8859-1"

    invoke-direct {v15, v14, v13, v4, v5}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V

    :try_start_0
    invoke-static {v15}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v4
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_3

    :catch_0
    const/4 v4, 0x0

    :goto_3
    if-eqz v4, :cond_2

    iget-object v5, v0, Lorg/apache/pdfbox/pdfparser/COSParser;->bfSearchCOSObjectKeyOffsets:Ljava/util/Map;

    new-instance v13, Lorg/apache/pdfbox/cos/COSObjectKey;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v14

    invoke-direct {v13, v14, v15, v8}, Lorg/apache/pdfbox/cos/COSObjectKey;-><init>(JI)V

    add-long/2addr v11, v9

    invoke-static {v11, v12}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v5, v13, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_2
    add-long/2addr v6, v9

    iget-object v4, v0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    invoke-virtual {v4}, Lorg/apache/pdfbox/io/PushBackInputStream;->isEOF()Z

    move-result v4

    if-eqz v4, :cond_3

    iget-object v3, v0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    invoke-virtual {v3, v1, v2}, Lorg/apache/pdfbox/io/PushBackInputStream;->seek(J)V

    goto :goto_4

    :cond_3
    const-wide/16 v4, 0x6

    goto/16 :goto_0

    :cond_4
    :goto_4
    return-void
.end method

.method private bfSearchForXRef(JZ)J
    .locals 10

    if-nez p3, :cond_0

    invoke-direct {p0}, Lorg/apache/pdfbox/pdfparser/COSParser;->bfSearchForXRefTables()V

    :cond_0
    invoke-direct {p0}, Lorg/apache/pdfbox/pdfparser/COSParser;->bfSearchForXRefStreams()V

    const-wide/16 v0, -0x1

    if-nez p3, :cond_1

    iget-object p3, p0, Lorg/apache/pdfbox/pdfparser/COSParser;->bfSearchXRefTablesOffsets:Ljava/util/List;

    if-eqz p3, :cond_1

    invoke-direct {p0, p3, p1, p2}, Lorg/apache/pdfbox/pdfparser/COSParser;->searchNearestValue(Ljava/util/List;J)J

    move-result-wide v2

    goto :goto_0

    :cond_1
    move-wide v2, v0

    :goto_0
    iget-object p3, p0, Lorg/apache/pdfbox/pdfparser/COSParser;->bfSearchXRefStreamsOffsets:Ljava/util/List;

    if-eqz p3, :cond_2

    invoke-direct {p0, p3, p1, p2}, Lorg/apache/pdfbox/pdfparser/COSParser;->searchNearestValue(Ljava/util/List;J)J

    move-result-wide v4

    goto :goto_1

    :cond_2
    move-wide v4, v0

    :goto_1
    cmp-long p3, v2, v0

    if-lez p3, :cond_4

    cmp-long v6, v4, v0

    if-lez v6, :cond_4

    sub-long v0, p1, v2

    sub-long/2addr p1, v4

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(J)J

    move-result-wide v6

    invoke-static {p1, p2}, Ljava/lang/Math;->abs(J)J

    move-result-wide v8

    cmp-long p3, v6, v8

    if-lez p3, :cond_3

    iget-object p3, p0, Lorg/apache/pdfbox/pdfparser/COSParser;->bfSearchXRefStreamsOffsets:Ljava/util/List;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {p3, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move-wide v0, p1

    goto :goto_2

    :cond_3
    iget-object p1, p0, Lorg/apache/pdfbox/pdfparser/COSParser;->bfSearchXRefTablesOffsets:Ljava/util/List;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    invoke-interface {p1, p2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_4
    if-lez p3, :cond_5

    iget-object p1, p0, Lorg/apache/pdfbox/pdfparser/COSParser;->bfSearchXRefTablesOffsets:Ljava/util/List;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    invoke-interface {p1, p2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move-wide v0, v2

    goto :goto_2

    :cond_5
    cmp-long p1, v4, v0

    if-lez p1, :cond_6

    iget-object p1, p0, Lorg/apache/pdfbox/pdfparser/COSParser;->bfSearchXRefStreamsOffsets:Ljava/util/List;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object p2

    invoke-interface {p1, p2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move-wide v0, v4

    :cond_6
    :goto_2
    return-wide v0
.end method

.method private bfSearchForXRefStreams()V
    .locals 19

    move-object/from16 v0, p0

    iget-object v1, v0, Lorg/apache/pdfbox/pdfparser/COSParser;->bfSearchXRefStreamsOffsets:Ljava/util/List;

    if-nez v1, :cond_8

    new-instance v1, Ljava/util/Vector;

    invoke-direct {v1}, Ljava/util/Vector;-><init>()V

    iput-object v1, v0, Lorg/apache/pdfbox/pdfparser/COSParser;->bfSearchXRefStreamsOffsets:Ljava/util/List;

    iget-object v1, v0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    invoke-virtual {v1}, Lorg/apache/pdfbox/io/PushBackInputStream;->getOffset()J

    move-result-wide v1

    iget-object v3, v0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    const-wide/16 v4, 0x6

    invoke-virtual {v3, v4, v5}, Lorg/apache/pdfbox/io/PushBackInputStream;->seek(J)V

    const-string v3, " obj"

    invoke-virtual {v3}, Ljava/lang/String;->toCharArray()[C

    move-result-object v3

    :goto_0
    iget-object v6, v0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    invoke-virtual {v6}, Lorg/apache/pdfbox/io/PushBackInputStream;->isEOF()Z

    move-result v6

    if-nez v6, :cond_7

    sget-object v6, Lorg/apache/pdfbox/pdfparser/COSParser;->XREF_STREAM:[C

    invoke-virtual {v0, v6}, Lorg/apache/pdfbox/pdfparser/BaseParser;->isString([C)Z

    move-result v6

    if-eqz v6, :cond_6

    iget-object v6, v0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    invoke-virtual {v6}, Lorg/apache/pdfbox/io/PushBackInputStream;->getOffset()J

    move-result-wide v6

    const/4 v12, 0x1

    const/4 v13, 0x0

    const-wide/16 v14, -0x1

    :goto_1
    const/16 v10, 0x1e

    if-ge v12, v10, :cond_4

    if-nez v13, :cond_4

    mul-int/lit8 v10, v12, 0xa

    int-to-long v8, v10

    sub-long v8, v6, v8

    const-wide/16 v16, 0x0

    cmp-long v10, v8, v16

    if-lez v10, :cond_3

    iget-object v10, v0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    invoke-virtual {v10, v8, v9}, Lorg/apache/pdfbox/io/PushBackInputStream;->seek(J)V

    const/4 v10, 0x0

    :goto_2
    const/16 v11, 0xa

    if-ge v10, v11, :cond_3

    invoke-virtual {v0, v3}, Lorg/apache/pdfbox/pdfparser/BaseParser;->isString([C)Z

    move-result v11

    const-wide/16 v17, 0x1

    if-eqz v11, :cond_2

    sub-long v8, v8, v17

    iget-object v10, v0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    invoke-virtual {v10, v8, v9}, Lorg/apache/pdfbox/io/PushBackInputStream;->seek(J)V

    iget-object v10, v0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    invoke-virtual {v10}, Lorg/apache/pdfbox/io/PushBackInputStream;->peek()I

    move-result v10

    invoke-virtual {v0, v10}, Lorg/apache/pdfbox/pdfparser/BaseParser;->isDigit(I)Z

    move-result v10

    if-eqz v10, :cond_1

    sub-long v8, v8, v17

    iget-object v10, v0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    invoke-virtual {v10, v8, v9}, Lorg/apache/pdfbox/io/PushBackInputStream;->seek(J)V

    invoke-virtual/range {p0 .. p0}, Lorg/apache/pdfbox/pdfparser/BaseParser;->isSpace()Z

    move-result v10

    if-eqz v10, :cond_1

    iget-object v10, v0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    sub-long v8, v8, v17

    invoke-virtual {v10, v8, v9}, Lorg/apache/pdfbox/io/PushBackInputStream;->seek(J)V

    const/4 v10, 0x0

    :goto_3
    cmp-long v11, v8, v4

    if-lez v11, :cond_0

    invoke-virtual/range {p0 .. p0}, Lorg/apache/pdfbox/pdfparser/BaseParser;->isDigit()Z

    move-result v11

    if-eqz v11, :cond_0

    iget-object v11, v0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    sub-long v8, v8, v17

    invoke-virtual {v11, v8, v9}, Lorg/apache/pdfbox/io/PushBackInputStream;->seek(J)V

    add-int/lit8 v10, v10, 0x1

    goto :goto_3

    :cond_0
    if-lez v10, :cond_1

    iget-object v8, v0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    invoke-virtual {v8}, Lorg/apache/pdfbox/io/PushBackInputStream;->read()I

    iget-object v8, v0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    invoke-virtual {v8}, Lorg/apache/pdfbox/io/PushBackInputStream;->getOffset()J

    move-result-wide v8

    move-wide v14, v8

    :cond_1
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Fixed reference for xref stream "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v9, " -> "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    const-string v9, "PdfBoxAndroid"

    invoke-static {v9, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v13, 0x1

    goto :goto_4

    :cond_2
    add-long v8, v8, v17

    iget-object v11, v0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    invoke-virtual {v11}, Lorg/apache/pdfbox/io/PushBackInputStream;->read()I

    add-int/lit8 v10, v10, 0x1

    goto :goto_2

    :cond_3
    :goto_4
    add-int/lit8 v12, v12, 0x1

    goto/16 :goto_1

    :cond_4
    const-wide/16 v8, -0x1

    cmp-long v8, v14, v8

    if-lez v8, :cond_5

    iget-object v8, v0, Lorg/apache/pdfbox/pdfparser/COSParser;->bfSearchXRefStreamsOffsets:Ljava/util/List;

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-interface {v8, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_5
    iget-object v8, v0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    const-wide/16 v9, 0x5

    add-long/2addr v6, v9

    invoke-virtual {v8, v6, v7}, Lorg/apache/pdfbox/io/PushBackInputStream;->seek(J)V

    :cond_6
    iget-object v6, v0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    invoke-virtual {v6}, Lorg/apache/pdfbox/io/PushBackInputStream;->read()I

    goto/16 :goto_0

    :cond_7
    iget-object v3, v0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    invoke-virtual {v3, v1, v2}, Lorg/apache/pdfbox/io/PushBackInputStream;->seek(J)V

    :cond_8
    return-void
.end method

.method private bfSearchForXRefTables()V
    .locals 7

    iget-object v0, p0, Lorg/apache/pdfbox/pdfparser/COSParser;->bfSearchXRefTablesOffsets:Ljava/util/List;

    if-nez v0, :cond_3

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lorg/apache/pdfbox/pdfparser/COSParser;->bfSearchXRefTablesOffsets:Ljava/util/List;

    iget-object v0, p0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    invoke-virtual {v0}, Lorg/apache/pdfbox/io/PushBackInputStream;->getOffset()J

    move-result-wide v0

    iget-object v2, p0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    const-wide/16 v3, 0x6

    invoke-virtual {v2, v3, v4}, Lorg/apache/pdfbox/io/PushBackInputStream;->seek(J)V

    :goto_0
    iget-object v2, p0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    invoke-virtual {v2}, Lorg/apache/pdfbox/io/PushBackInputStream;->isEOF()Z

    move-result v2

    if-nez v2, :cond_2

    sget-object v2, Lorg/apache/pdfbox/pdfparser/COSParser;->XREF_TABLE:[C

    invoke-virtual {p0, v2}, Lorg/apache/pdfbox/pdfparser/BaseParser;->isString([C)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    invoke-virtual {v2}, Lorg/apache/pdfbox/io/PushBackInputStream;->getOffset()J

    move-result-wide v2

    iget-object v4, p0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    const-wide/16 v5, 0x1

    sub-long v5, v2, v5

    invoke-virtual {v4, v5, v6}, Lorg/apache/pdfbox/io/PushBackInputStream;->seek(J)V

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfparser/BaseParser;->isWhitespace()Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lorg/apache/pdfbox/pdfparser/COSParser;->bfSearchXRefTablesOffsets:Ljava/util/List;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    iget-object v4, p0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    const-wide/16 v5, 0x4

    add-long/2addr v2, v5

    invoke-virtual {v4, v2, v3}, Lorg/apache/pdfbox/io/PushBackInputStream;->seek(J)V

    :cond_1
    iget-object v2, p0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    invoke-virtual {v2}, Lorg/apache/pdfbox/io/PushBackInputStream;->read()I

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    invoke-virtual {v2, v0, v1}, Lorg/apache/pdfbox/io/PushBackInputStream;->seek(J)V

    :cond_3
    return-void
.end method

.method private calculateXRefFixedOffset(JZ)J
    .locals 8

    const-wide/16 v0, 0x0

    cmp-long v2, p1, v0

    const-string v3, "PdfBoxAndroid"

    if-gez v2, :cond_0

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid object offset "

    invoke-virtual {p3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string p1, " when searching for a xref table/stream"

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_0
    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v3, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-wide v0

    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lorg/apache/pdfbox/pdfparser/COSParser;->bfSearchForXRef(JZ)J

    move-result-wide v4

    const-wide/16 v6, -0x1

    cmp-long p3, v4, v6

    if-lez p3, :cond_1

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Fixed reference for xref table/stream "

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string p1, " -> "

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v3, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-wide v4

    :cond_1
    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Can\'t find the object axref table/stream at offset "

    invoke-virtual {p3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method private checkObjectKeys(Lorg/apache/pdfbox/cos/COSObjectKey;J)Z
    .locals 6

    const-wide/16 v0, 0x6

    cmp-long v0, p2, v0

    const/4 v1, 0x0

    if-gez v0, :cond_0

    return v1

    :cond_0
    invoke-virtual {p1}, Lorg/apache/pdfbox/cos/COSObjectKey;->getNumber()J

    move-result-wide v2

    invoke-virtual {p1}, Lorg/apache/pdfbox/cos/COSObjectKey;->getGeneration()I

    move-result p1

    iget-object v0, p0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    invoke-virtual {v0}, Lorg/apache/pdfbox/io/PushBackInputStream;->getOffset()J

    move-result-wide v4

    iget-object v0, p0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    invoke-virtual {v0, p2, p3}, Lorg/apache/pdfbox/io/PushBackInputStream;->seek(J)V

    invoke-direct {p0, v2, v3, p1}, Lorg/apache/pdfbox/pdfparser/COSParser;->createObjectString(JI)Ljava/lang/String;

    move-result-object p1

    :try_start_0
    const-string p2, "ISO-8859-1"

    invoke-virtual {p1, p2}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object p1

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/pdfparser/BaseParser;->isString([B)Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    invoke-virtual {p1, v4, v5}, Lorg/apache/pdfbox/io/PushBackInputStream;->seek(J)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object p1, p0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    invoke-virtual {p1, v4, v5}, Lorg/apache/pdfbox/io/PushBackInputStream;->seek(J)V

    const/4 p1, 0x1

    return p1

    :catchall_0
    move-exception p1

    iget-object p2, p0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    invoke-virtual {p2, v4, v5}, Lorg/apache/pdfbox/io/PushBackInputStream;->seek(J)V

    throw p1

    :catch_0
    :cond_1
    iget-object p1, p0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    invoke-virtual {p1, v4, v5}, Lorg/apache/pdfbox/io/PushBackInputStream;->seek(J)V

    return v1
.end method

.method private checkXRefOffset(J)J
    .locals 4

    iget-boolean v0, p0, Lorg/apache/pdfbox/pdfparser/COSParser;->isLenient:Z

    if-nez v0, :cond_0

    return-wide p1

    :cond_0
    iget-object v0, p0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    invoke-virtual {v0, p1, p2}, Lorg/apache/pdfbox/io/PushBackInputStream;->seek(J)V

    iget-object v0, p0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    invoke-virtual {v0}, Lorg/apache/pdfbox/io/PushBackInputStream;->peek()I

    move-result v0

    const/16 v1, 0x78

    if-ne v0, v1, :cond_1

    sget-object v0, Lorg/apache/pdfbox/pdfparser/COSParser;->XREF_TABLE:[C

    invoke-virtual {p0, v0}, Lorg/apache/pdfbox/pdfparser/BaseParser;->isString([C)Z

    move-result v0

    if-eqz v0, :cond_1

    return-wide p1

    :cond_1
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-lez v0, :cond_2

    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/pdfbox/pdfparser/COSParser;->checkXRefStreamOffset(JZ)J

    move-result-wide v0

    const-wide/16 v2, -0x1

    cmp-long v2, v0, v2

    if-lez v2, :cond_2

    return-wide v0

    :cond_2
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/pdfbox/pdfparser/COSParser;->calculateXRefFixedOffset(JZ)J

    move-result-wide p1

    return-wide p1
.end method

.method private checkXRefStreamOffset(JZ)J
    .locals 3

    iget-boolean v0, p0, Lorg/apache/pdfbox/pdfparser/COSParser;->isLenient:Z

    if-eqz v0, :cond_3

    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    const-wide/16 v1, 0x1

    sub-long v1, p1, v1

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/io/PushBackInputStream;->seek(J)V

    iget-object v0, p0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    invoke-virtual {v0}, Lorg/apache/pdfbox/io/PushBackInputStream;->read()I

    move-result v0

    invoke-virtual {p0, v0}, Lorg/apache/pdfbox/pdfparser/BaseParser;->isWhitespace(I)Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_1

    iget-object v0, p0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    invoke-virtual {v0}, Lorg/apache/pdfbox/io/PushBackInputStream;->peek()I

    move-result v0

    const/16 v2, 0x2f

    if-le v0, v2, :cond_1

    const/16 v2, 0x3a

    if-ge v0, v2, :cond_1

    :try_start_0
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfparser/BaseParser;->readObjectNumber()I

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfparser/BaseParser;->readGenerationNumber()I

    sget-object v0, Lorg/apache/pdfbox/pdfparser/COSParser;->OBJ_MARKER:[C

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdfparser/BaseParser;->readExpectedString([CZ)V

    iget-object v0, p0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    invoke-virtual {v0, p1, p2}, Lorg/apache/pdfbox/io/PushBackInputStream;->seek(J)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-wide p1

    :catch_0
    iget-object v0, p0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    invoke-virtual {v0, p1, p2}, Lorg/apache/pdfbox/io/PushBackInputStream;->seek(J)V

    :cond_1
    if-eqz p3, :cond_2

    const-wide/16 p1, -0x1

    goto :goto_0

    :cond_2
    invoke-direct {p0, p1, p2, v1}, Lorg/apache/pdfbox/pdfparser/COSParser;->calculateXRefFixedOffset(JZ)J

    move-result-wide p1

    :cond_3
    :goto_0
    return-wide p1
.end method

.method private checkXrefOffsets()V
    .locals 9

    iget-boolean v0, p0, Lorg/apache/pdfbox/pdfparser/COSParser;->isLenient:Z

    if-nez v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lorg/apache/pdfbox/pdfparser/COSParser;->xrefTrailerResolver:Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver;

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver;->getXrefTable()Ljava/util/Map;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    const-string v3, "PdfBoxAndroid"

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/pdfbox/cos/COSObjectKey;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    const-wide/16 v7, 0x0

    cmp-long v5, v5, v7

    if-ltz v5, :cond_1

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    invoke-direct {p0, v4, v5, v6}, Lorg/apache/pdfbox/pdfparser/COSParser;->checkObjectKeys(Lorg/apache/pdfbox/cos/COSObjectKey;J)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v1, "Stop checking xref offsets as at least one couldn\'t be dereferenced"

    invoke-static {v3, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x1

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_3

    invoke-direct {p0}, Lorg/apache/pdfbox/pdfparser/COSParser;->bfSearchForObjects()V

    iget-object v1, p0, Lorg/apache/pdfbox/pdfparser/COSParser;->bfSearchCOSObjectKeyOffsets:Ljava/util/Map;

    if-eqz v1, :cond_3

    invoke-interface {v1}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, "Replaced read xref table with the results of a brute force search"

    invoke-static {v3, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lorg/apache/pdfbox/pdfparser/COSParser;->bfSearchCOSObjectKeyOffsets:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    :cond_3
    return-void
.end method

.method private createObjectString(JI)Ljava/lang/String;
    .locals 1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, " "

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {p3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, " obj"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private getLength(Lorg/apache/pdfbox/cos/COSBase;)Lorg/apache/pdfbox/cos/COSNumber;
    .locals 4

    if-nez p1, :cond_0

    const/4 p1, 0x0

    return-object p1

    :cond_0
    iget-boolean v0, p0, Lorg/apache/pdfbox/pdfparser/COSParser;->inGetLength:Z

    if-nez v0, :cond_6

    const/4 v0, 0x0

    const/4 v1, 0x1

    :try_start_0
    iput-boolean v1, p0, Lorg/apache/pdfbox/pdfparser/COSParser;->inGetLength:Z

    instance-of v2, p1, Lorg/apache/pdfbox/cos/COSNumber;

    if-eqz v2, :cond_1

    :goto_0
    check-cast p1, Lorg/apache/pdfbox/cos/COSNumber;

    goto :goto_2

    :cond_1
    instance-of v2, p1, Lorg/apache/pdfbox/cos/COSObject;

    if-eqz v2, :cond_5

    check-cast p1, Lorg/apache/pdfbox/cos/COSObject;

    invoke-virtual {p1}, Lorg/apache/pdfbox/cos/COSObject;->getObject()Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v2

    if-nez v2, :cond_3

    iget-object v2, p0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    invoke-virtual {v2}, Lorg/apache/pdfbox/io/PushBackInputStream;->getOffset()J

    move-result-wide v2

    invoke-virtual {p0, p1, v1}, Lorg/apache/pdfbox/pdfparser/COSParser;->parseObjectDynamically(Lorg/apache/pdfbox/cos/COSObject;Z)Lorg/apache/pdfbox/cos/COSBase;

    iget-object v1, p0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    invoke-virtual {v1, v2, v3}, Lorg/apache/pdfbox/io/PushBackInputStream;->seek(J)V

    invoke-virtual {p1}, Lorg/apache/pdfbox/cos/COSObject;->getObject()Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v1

    if-eqz v1, :cond_2

    goto :goto_1

    :cond_2
    new-instance p1, Ljava/io/IOException;

    const-string v1, "Length object content was not read."

    invoke-direct {p1, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_3
    :goto_1
    invoke-virtual {p1}, Lorg/apache/pdfbox/cos/COSObject;->getObject()Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v1

    instance-of v1, v1, Lorg/apache/pdfbox/cos/COSNumber;

    if-eqz v1, :cond_4

    invoke-virtual {p1}, Lorg/apache/pdfbox/cos/COSObject;->getObject()Lorg/apache/pdfbox/cos/COSBase;

    move-result-object p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :goto_2
    iput-boolean v0, p0, Lorg/apache/pdfbox/pdfparser/COSParser;->inGetLength:Z

    return-object p1

    :cond_4
    :try_start_1
    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Wrong type of referenced length object "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lorg/apache/pdfbox/cos/COSObject;->getObject()Lorg/apache/pdfbox/cos/COSBase;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v1, p1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_5
    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Wrong type of length object: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v1, p1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception p1

    iput-boolean v0, p0, Lorg/apache/pdfbox/pdfparser/COSParser;->inGetLength:Z

    throw p1

    :cond_6
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Loop while reading length from "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private getObjectId(Lorg/apache/pdfbox/cos/COSObject;)J
    .locals 4

    invoke-virtual {p1}, Lorg/apache/pdfbox/cos/COSObject;->getObjectNumber()J

    move-result-wide v0

    const/16 v2, 0x20

    shl-long/2addr v0, v2

    invoke-virtual {p1}, Lorg/apache/pdfbox/cos/COSObject;->getGenerationNumber()I

    move-result p1

    int-to-long v2, p1

    or-long/2addr v0, v2

    return-wide v0
.end method

.method private parseHeader(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 7

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfparser/BaseParser;->readLine()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    const/4 v2, 0x0

    if-nez v1, :cond_1

    :cond_0
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfparser/BaseParser;->readLine()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_0

    invoke-virtual {v0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v1

    invoke-static {v1}, Ljava/lang/Character;->isDigit(C)Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_1
    invoke-virtual {v0, p1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    const-wide/16 v3, 0x0

    if-nez v1, :cond_2

    iget-object p1, p0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    invoke-virtual {p1, v3, v4}, Lorg/apache/pdfbox/io/PushBackInputStream;->seek(J)V

    return v2

    :cond_2
    invoke-virtual {v0, p1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    if-lez v1, :cond_3

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    invoke-virtual {v0, v1, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    :cond_3
    invoke-virtual {v0, p1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    const-string v5, "PdfBoxAndroid"

    if-eqz v1, :cond_4

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, "\\d.\\d"

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_4

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x3

    if-ge v1, v2, :cond_5

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "No version found, set to "

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, " as default."

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v5, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_4
    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x3

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v6

    invoke-virtual {v0, v1, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\n"

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result p1

    add-int/lit8 p1, p1, 0x3

    invoke-virtual {v0, v2, p1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    iget-object p1, p0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    const-string v1, "ISO-8859-1"

    invoke-virtual {p2, v1}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object p2

    invoke-virtual {p1, p2}, Lorg/apache/pdfbox/io/PushBackInputStream;->unread([B)V

    :cond_5
    :goto_0
    const/4 p1, 0x1

    const/high16 p2, -0x40800000    # -1.0f

    :try_start_0
    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    array-length v2, v1

    const/4 v6, 0x2

    if-ne v2, v6, :cond_6

    aget-object v1, v1, p1

    invoke-static {v1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result p2
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    const-string v2, "Can\'t parse the header version."

    invoke-static {v5, v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_6
    :goto_1
    const/4 v1, 0x0

    cmpg-float v1, p2, v1

    if-ltz v1, :cond_7

    iget-object v0, p0, Lorg/apache/pdfbox/pdfparser/BaseParser;->document:Lorg/apache/pdfbox/cos/COSDocument;

    invoke-virtual {v0, p2}, Lorg/apache/pdfbox/cos/COSDocument;->setVersion(F)V

    iget-object p2, p0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    invoke-virtual {p2, v3, v4}, Lorg/apache/pdfbox/io/PushBackInputStream;->seek(J)V

    return p1

    :cond_7
    new-instance p1, Ljava/io/IOException;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Error getting header version: "

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private parseXrefObjStream(JZ)J
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfparser/BaseParser;->readObjectNumber()I

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfparser/BaseParser;->readGenerationNumber()I

    sget-object v0, Lorg/apache/pdfbox/pdfparser/COSParser;->OBJ_MARKER:[C

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdfparser/BaseParser;->readExpectedString([CZ)V

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfparser/BaseParser;->parseCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/apache/pdfbox/pdfparser/COSParser;->parseCOSStream(Lorg/apache/pdfbox/cos/COSDictionary;)Lorg/apache/pdfbox/cos/COSStream;

    move-result-object v1

    long-to-int p1, p1

    int-to-long p1, p1

    invoke-virtual {p0, v1, p1, p2, p3}, Lorg/apache/pdfbox/pdfparser/COSParser;->parseXrefStream(Lorg/apache/pdfbox/cos/COSStream;JZ)V

    invoke-virtual {v1}, Lorg/apache/pdfbox/cos/COSStream;->close()V

    sget-object p1, Lorg/apache/pdfbox/cos/COSName;->PREV:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->getLong(Lorg/apache/pdfbox/cos/COSName;)J

    move-result-wide p1

    return-wide p1
.end method

.method private searchNearestValue(Ljava/util/List;J)J
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/Long;",
            ">;J)J"
        }
    .end annotation

    move-object/from16 v0, p1

    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v1

    const-wide/16 v2, -0x1

    const/4 v4, -0x1

    const/4 v5, 0x0

    move-wide v7, v2

    move v6, v4

    :goto_0
    if-ge v5, v1, :cond_2

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Long;

    invoke-virtual {v9}, Ljava/lang/Long;->longValue()J

    move-result-wide v9

    sub-long v9, p2, v9

    cmp-long v11, v7, v2

    if-eqz v11, :cond_0

    invoke-static {v7, v8}, Ljava/lang/Math;->abs(J)J

    move-result-wide v11

    invoke-static {v9, v10}, Ljava/lang/Math;->abs(J)J

    move-result-wide v13

    cmp-long v11, v11, v13

    if-lez v11, :cond_1

    :cond_0
    move v6, v5

    move-wide v7, v9

    :cond_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    :cond_2
    if-le v6, v4, :cond_3

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    :cond_3
    return-wide v2
.end method

.method private validateStreamLength(J)Z
    .locals 7

    iget-object v0, p0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    invoke-virtual {v0}, Lorg/apache/pdfbox/io/PushBackInputStream;->getOffset()J

    move-result-wide v0

    add-long/2addr p1, v0

    iget-wide v2, p0, Lorg/apache/pdfbox/pdfparser/COSParser;->fileLen:J

    cmp-long v2, p1, v2

    const-string v3, " but expected "

    const-string v4, "PdfBoxAndroid"

    const/4 v5, 0x0

    if-lez v2, :cond_0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "The end of the stream is out of range, using workaround to read the stream, found "

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v4, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_0
    iget-object v2, p0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    invoke-virtual {v2, p1, p2}, Lorg/apache/pdfbox/io/PushBackInputStream;->seek(J)V

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfparser/BaseParser;->skipSpaces()V

    sget-object v2, Lorg/apache/pdfbox/pdfparser/BaseParser;->ENDSTREAM:[B

    invoke-virtual {p0, v2}, Lorg/apache/pdfbox/pdfparser/BaseParser;->isString([B)Z

    move-result v2

    if-nez v2, :cond_1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "The end of the stream doesn\'t point to the correct offset, using workaround to read the stream, found "

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v4, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    const/4 p1, 0x1

    move v5, p1

    :goto_0
    iget-object p1, p0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    invoke-virtual {p1, v0, v1}, Lorg/apache/pdfbox/io/PushBackInputStream;->seek(J)V

    :goto_1
    return v5
.end method


# virtual methods
.method public createTmpFile(Ljava/io/InputStream;)Ljava/io/File;
    .locals 3

    const/4 v0, 0x0

    :try_start_0
    const-string v1, "tmpPDF"

    const-string v2, ".pdf"

    invoke-static {v1, v2}, Ljava/io/File;->createTempFile(Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    new-instance v2, Ljava/io/FileOutputStream;

    invoke-direct {v2, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    invoke-static {p1, v2}, Lorg/apache/pdfbox/io/IOUtils;->copy(Ljava/io/InputStream;Ljava/io/OutputStream;)J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-static {p1}, Lorg/apache/pdfbox/io/IOUtils;->closeQuietly(Ljava/io/Closeable;)V

    invoke-static {v2}, Lorg/apache/pdfbox/io/IOUtils;->closeQuietly(Ljava/io/Closeable;)V

    return-object v1

    :catchall_0
    move-exception v0

    goto :goto_0

    :catchall_1
    move-exception v1

    move-object v2, v0

    move-object v0, v1

    :goto_0
    invoke-static {p1}, Lorg/apache/pdfbox/io/IOUtils;->closeQuietly(Ljava/io/Closeable;)V

    invoke-static {v2}, Lorg/apache/pdfbox/io/IOUtils;->closeQuietly(Ljava/io/Closeable;)V

    throw v0
.end method

.method public getDocument()Lorg/apache/pdfbox/cos/COSDocument;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdfparser/BaseParser;->document:Lorg/apache/pdfbox/cos/COSDocument;

    if-eqz v0, :cond_0

    return-object v0

    :cond_0
    new-instance v0, Ljava/io/IOException;

    const-string v1, "You must call parse() before calling getDocument()"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final getStartxrefOffset()J
    .locals 10

    const-wide/16 v0, 0x0

    :try_start_0
    iget-wide v2, p0, Lorg/apache/pdfbox/pdfparser/COSParser;->fileLen:J

    iget v4, p0, Lorg/apache/pdfbox/pdfparser/COSParser;->readTrailBytes:I

    int-to-long v5, v4

    cmp-long v5, v2, v5

    if-gez v5, :cond_0

    long-to-int v4, v2

    :cond_0
    new-array v5, v4, [B

    int-to-long v6, v4

    sub-long/2addr v2, v6

    iget-object v6, p0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    invoke-virtual {v6, v2, v3}, Lorg/apache/pdfbox/io/PushBackInputStream;->seek(J)V

    const/4 v6, 0x0

    :goto_0
    if-ge v6, v4, :cond_2

    iget-object v7, p0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    sub-int v8, v4, v6

    invoke-virtual {v7, v5, v6, v8}, Lorg/apache/pdfbox/io/PushBackInputStream;->read([BII)I

    move-result v7

    const/4 v9, 0x1

    if-lt v7, v9, :cond_1

    add-int/2addr v6, v7

    goto :goto_0

    :cond_1
    new-instance v2, Ljava/io/IOException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "No more bytes to read for trailing buffer, but expected: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_2
    iget-object v6, p0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    invoke-virtual {v6, v0, v1}, Lorg/apache/pdfbox/io/PushBackInputStream;->seek(J)V

    sget-object v0, Lorg/apache/pdfbox/pdfparser/COSParser;->EOF_MARKER:[C

    invoke-virtual {p0, v0, v5, v4}, Lorg/apache/pdfbox/pdfparser/COSParser;->lastIndexOf([C[BI)I

    move-result v1

    const-string v6, "PdfBoxAndroid"

    if-gez v1, :cond_4

    iget-boolean v1, p0, Lorg/apache/pdfbox/pdfparser/COSParser;->isLenient:Z

    const-string v7, "\'"

    const-string v8, "Missing end of file marker \'"

    if-eqz v1, :cond_3

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v8, Ljava/lang/String;

    invoke-direct {v8, v0}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v6, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_3
    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v3, Ljava/lang/String;

    invoke-direct {v3, v0}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_4
    move v4, v1

    :goto_1
    sget-object v0, Lorg/apache/pdfbox/pdfparser/COSParser;->STARTXREF:[C

    invoke-virtual {p0, v0, v5, v4}, Lorg/apache/pdfbox/pdfparser/COSParser;->lastIndexOf([C[BI)I

    move-result v0

    int-to-long v4, v0

    add-long/2addr v2, v4

    if-gez v0, :cond_6

    iget-boolean v0, p0, Lorg/apache/pdfbox/pdfparser/COSParser;->isLenient:Z

    if-eqz v0, :cond_5

    const-string v0, "Can\'t find offset for startxref"

    invoke-static {v6, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-wide/16 v0, -0x1

    return-wide v0

    :cond_5
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Missing \'startxref\' marker."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_6
    return-wide v2

    :catchall_0
    move-exception v2

    iget-object v3, p0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    invoke-virtual {v3, v0, v1}, Lorg/apache/pdfbox/io/PushBackInputStream;->seek(J)V

    throw v2
.end method

.method public isLenient()Z
    .locals 1

    iget-boolean v0, p0, Lorg/apache/pdfbox/pdfparser/COSParser;->isLenient:Z

    return v0
.end method

.method public lastIndexOf([C[BI)I
    .locals 4

    array-length v0, p1

    add-int/lit8 v0, v0, -0x1

    aget-char v1, p1, v0

    :goto_0
    move v2, v0

    :cond_0
    :goto_1
    const/4 v3, -0x1

    add-int/2addr p3, v3

    if-ltz p3, :cond_3

    aget-byte v3, p2, p3

    if-ne v3, v1, :cond_2

    add-int/lit8 v2, v2, -0x1

    if-gez v2, :cond_1

    return p3

    :cond_1
    aget-char v1, p1, v2

    goto :goto_1

    :cond_2
    if-ge v2, v0, :cond_0

    aget-char v1, p1, v0

    goto :goto_0

    :cond_3
    return v3
.end method

.method public parseCOSStream(Lorg/apache/pdfbox/cos/COSDictionary;)Lorg/apache/pdfbox/cos/COSStream;
    .locals 11

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/pdfparser/BaseParser;->createCOSStream(Lorg/apache/pdfbox/cos/COSDictionary;)Lorg/apache/pdfbox/cos/COSStream;

    move-result-object v0

    const/4 v1, 0x0

    :try_start_0
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfparser/BaseParser;->readString()Ljava/lang/String;

    iget-object v2, p0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    :goto_0
    invoke-virtual {v2}, Lorg/apache/pdfbox/io/PushBackInputStream;->read()I

    move-result v2

    const/16 v3, 0x20

    if-ne v2, v3, :cond_0

    iget-object v2, p0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    goto :goto_0

    :cond_0
    const/16 v3, 0xd

    const/16 v4, 0xa

    if-ne v2, v3, :cond_1

    iget-object v2, p0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    invoke-virtual {v2}, Lorg/apache/pdfbox/io/PushBackInputStream;->read()I

    move-result v2

    if-eq v2, v4, :cond_2

    iget-object v3, p0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    :goto_1
    invoke-virtual {v3, v2}, Lorg/apache/pdfbox/io/PushBackInputStream;->unread(I)V

    goto :goto_2

    :cond_1
    if-eq v2, v4, :cond_2

    iget-object v3, p0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    goto :goto_1

    :cond_2
    :goto_2
    sget-object v2, Lorg/apache/pdfbox/cos/COSName;->LENGTH:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->getItem(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object p1

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdfparser/COSParser;->getLength(Lorg/apache/pdfbox/cos/COSBase;)Lorg/apache/pdfbox/cos/COSNumber;

    move-result-object p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const-string v2, "PdfBoxAndroid"

    if-nez p1, :cond_4

    :try_start_1
    iget-boolean v3, p0, Lorg/apache/pdfbox/pdfparser/COSParser;->isLenient:Z

    if-eqz v3, :cond_3

    const-string v3, "The stream doesn\'t provide any stream length, using fallback readUntilEnd"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    :cond_3
    new-instance p1, Ljava/io/IOException;

    const-string v0, "Missing length for stream."

    invoke-direct {p1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_4
    :goto_3
    const/4 v3, 0x1

    const/4 v4, 0x0

    if-eqz p1, :cond_8

    invoke-virtual {p1}, Lorg/apache/pdfbox/cos/COSNumber;->longValue()J

    move-result-wide v5

    invoke-direct {p0, v5, v6}, Lorg/apache/pdfbox/pdfparser/COSParser;->validateStreamLength(J)Z

    move-result v5

    if-eqz v5, :cond_8

    invoke-virtual {v0, p1}, Lorg/apache/pdfbox/cos/COSStream;->createFilteredStream(Lorg/apache/pdfbox/cos/COSBase;)Ljava/io/OutputStream;

    move-result-object v1

    invoke-virtual {p1}, Lorg/apache/pdfbox/cos/COSNumber;->longValue()J

    move-result-wide v5

    move p1, v4

    :goto_4
    const-wide/16 v7, 0x0

    cmp-long v7, v5, v7

    if-lez v7, :cond_7

    iget-object v7, p0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    iget-object v8, p0, Lorg/apache/pdfbox/pdfparser/COSParser;->streamCopyBuf:[B

    const-wide/16 v9, 0x2000

    cmp-long v9, v5, v9

    if-lez v9, :cond_5

    const/16 v9, 0x2000

    goto :goto_5

    :cond_5
    long-to-int v9, v5

    :goto_5
    invoke-virtual {v7, v8, v4, v9}, Lorg/apache/pdfbox/io/PushBackInputStream;->read([BII)I

    move-result v7

    if-gtz v7, :cond_6

    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V

    iget-object v5, p0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    invoke-virtual {v5, p1}, Lorg/apache/pdfbox/io/PushBackInputStream;->unread(I)V

    goto :goto_6

    :cond_6
    iget-object v8, p0, Lorg/apache/pdfbox/pdfparser/COSParser;->streamCopyBuf:[B

    invoke-virtual {v1, v8, v4, v7}, Ljava/io/OutputStream;->write([BII)V

    int-to-long v8, v7

    sub-long/2addr v5, v8

    add-int/2addr p1, v7

    goto :goto_4

    :cond_7
    move v3, v4

    :cond_8
    :goto_6
    if-eqz v3, :cond_9

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSStream;->createFilteredStream()Ljava/io/OutputStream;

    move-result-object v1

    new-instance p1, Lorg/apache/pdfbox/pdfparser/EndstreamOutputStream;

    invoke-direct {p1, v1}, Lorg/apache/pdfbox/pdfparser/EndstreamOutputStream;-><init>(Ljava/io/OutputStream;)V

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/pdfparser/BaseParser;->readUntilEndStream(Ljava/io/OutputStream;)V

    :cond_9
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfparser/BaseParser;->readString()Ljava/lang/String;

    move-result-object p1

    const-string v3, "endobj"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_a

    iget-boolean v3, p0, Lorg/apache/pdfbox/pdfparser/COSParser;->isLenient:Z

    if-eqz v3, :cond_a

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "stream ends with \'endobj\' instead of \'endstream\' at offset "

    invoke-virtual {p1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    invoke-virtual {v3}, Lorg/apache/pdfbox/io/PushBackInputStream;->getOffset()J

    move-result-wide v3

    invoke-virtual {p1, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v2, p1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object p1, p0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    sget-object v2, Lorg/apache/pdfbox/pdfparser/BaseParser;->ENDOBJ:[B

    invoke-virtual {p1, v2}, Lorg/apache/pdfbox/io/PushBackInputStream;->unread([B)V

    goto :goto_7

    :cond_a
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const-string v5, "endstream"

    const/16 v6, 0x9

    if-le v3, v6, :cond_b

    :try_start_2
    iget-boolean v3, p0, Lorg/apache/pdfbox/pdfparser/COSParser;->isLenient:Z

    if-eqz v3, :cond_b

    invoke-virtual {p1, v4, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_b

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "stream ends with \'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "\' instead of \'endstream\' at offset "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, p0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    invoke-virtual {v4}, Lorg/apache/pdfbox/io/PushBackInputStream;->getOffset()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    invoke-virtual {p1, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object p1

    const-string v3, "ISO-8859-1"

    invoke-virtual {p1, v3}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object p1

    invoke-virtual {v2, p1}, Lorg/apache/pdfbox/io/PushBackInputStream;->unread([B)V

    goto :goto_7

    :cond_b
    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-eqz v2, :cond_d

    :goto_7
    if-eqz v1, :cond_c

    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V

    :cond_c
    return-object v0

    :cond_d
    :try_start_3
    new-instance v0, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error reading stream, expected=\'endstream\' actual=\'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "\' at offset "

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p1, p0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    invoke-virtual {p1}, Lorg/apache/pdfbox/io/PushBackInputStream;->getOffset()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :catchall_0
    move-exception p1

    if-eqz v1, :cond_e

    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V

    :cond_e
    throw p1
.end method

.method public varargs parseDictObjects(Lorg/apache/pdfbox/cos/COSDictionary;[Lorg/apache/pdfbox/cos/COSName;)V
    .locals 13

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    new-instance v1, Ljava/util/TreeMap;

    invoke-direct {v1}, Ljava/util/TreeMap;-><init>()V

    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    invoke-direct {p0, p2, p1, v2}, Lorg/apache/pdfbox/pdfparser/COSParser;->addExcludedToList([Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSDictionary;Ljava/util/Set;)V

    invoke-virtual {p1}, Lorg/apache/pdfbox/cos/COSDictionary;->getValues()Ljava/util/Collection;

    move-result-object p1

    invoke-direct {p0, v0, p1, v3}, Lorg/apache/pdfbox/pdfparser/COSParser;->addNewToList(Ljava/util/Queue;Ljava/util/Collection;Ljava/util/Set;)V

    :cond_0
    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result p1

    if-eqz p1, :cond_1

    invoke-virtual {v1}, Ljava/util/AbstractMap;->isEmpty()Z

    move-result p1

    if-nez p1, :cond_9

    :cond_1
    :goto_0
    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lorg/apache/pdfbox/cos/COSBase;

    const/4 p2, 0x0

    if-eqz p1, :cond_8

    instance-of v4, p1, Lorg/apache/pdfbox/cos/COSDictionary;

    if-eqz v4, :cond_2

    check-cast p1, Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-virtual {p1}, Lorg/apache/pdfbox/cos/COSDictionary;->getValues()Ljava/util/Collection;

    move-result-object p1

    invoke-direct {p0, v0, p1, v3}, Lorg/apache/pdfbox/pdfparser/COSParser;->addNewToList(Ljava/util/Queue;Ljava/util/Collection;Ljava/util/Set;)V

    goto :goto_0

    :cond_2
    instance-of v4, p1, Lorg/apache/pdfbox/cos/COSArray;

    if-eqz v4, :cond_3

    check-cast p1, Lorg/apache/pdfbox/cos/COSArray;

    invoke-virtual {p1}, Lorg/apache/pdfbox/cos/COSArray;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lorg/apache/pdfbox/cos/COSBase;

    invoke-direct {p0, v0, p2, v3}, Lorg/apache/pdfbox/pdfparser/COSParser;->addNewToList(Ljava/util/Queue;Lorg/apache/pdfbox/cos/COSBase;Ljava/util/Set;)V

    goto :goto_1

    :cond_3
    instance-of v4, p1, Lorg/apache/pdfbox/cos/COSObject;

    if-eqz v4, :cond_1

    check-cast p1, Lorg/apache/pdfbox/cos/COSObject;

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdfparser/COSParser;->getObjectId(Lorg/apache/pdfbox/cos/COSObject;)J

    move-result-wide v4

    new-instance v6, Lorg/apache/pdfbox/cos/COSObjectKey;

    invoke-virtual {p1}, Lorg/apache/pdfbox/cos/COSObject;->getObjectNumber()J

    move-result-wide v7

    invoke-virtual {p1}, Lorg/apache/pdfbox/cos/COSObject;->getGenerationNumber()I

    move-result v9

    invoke-direct {v6, v7, v8, v9}, Lorg/apache/pdfbox/cos/COSObjectKey;-><init>(JI)V

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v2, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    iget-object v4, p0, Lorg/apache/pdfbox/pdfparser/COSParser;->xrefTrailerResolver:Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver;

    invoke-virtual {v4}, Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver;->getXrefTable()Ljava/util/Map;

    move-result-object v4

    invoke-interface {v4, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    if-eqz v4, :cond_7

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    const-wide/16 v9, 0x0

    cmp-long v5, v7, v9

    if-eqz v5, :cond_7

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    cmp-long v5, v7, v9

    if-lez v5, :cond_4

    invoke-static {p1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    invoke-virtual {v1, v4, p1}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_4
    iget-object v5, p0, Lorg/apache/pdfbox/pdfparser/COSParser;->xrefTrailerResolver:Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver;

    invoke-virtual {v5}, Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver;->getXrefTable()Ljava/util/Map;

    move-result-object v5

    new-instance v7, Lorg/apache/pdfbox/cos/COSObjectKey;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v11

    neg-long v11, v11

    long-to-int v4, v11

    int-to-long v11, v4

    invoke-direct {v7, v11, v12, p2}, Lorg/apache/pdfbox/cos/COSObjectKey;-><init>(JI)V

    invoke-interface {v5, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Ljava/lang/Long;

    if-eqz p2, :cond_6

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    cmp-long v4, v4, v9

    if-lez v4, :cond_6

    invoke-virtual {v1, p2}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/List;

    if-nez v4, :cond_5

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v1, p2, v4}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_5
    invoke-interface {v4, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :cond_6
    new-instance p1, Ljava/io/IOException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Invalid object stream xref object reference for key \'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, "\': "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_7
    iget-object p1, p0, Lorg/apache/pdfbox/pdfparser/BaseParser;->document:Lorg/apache/pdfbox/cos/COSDocument;

    invoke-virtual {p1, v6}, Lorg/apache/pdfbox/cos/COSDocument;->getObjectFromPool(Lorg/apache/pdfbox/cos/COSObjectKey;)Lorg/apache/pdfbox/cos/COSObject;

    move-result-object p1

    sget-object p2, Lorg/apache/pdfbox/cos/COSNull;->NULL:Lorg/apache/pdfbox/cos/COSNull;

    invoke-virtual {p1, p2}, Lorg/apache/pdfbox/cos/COSObject;->setObject(Lorg/apache/pdfbox/cos/COSBase;)V

    goto/16 :goto_0

    :cond_8
    invoke-virtual {v1}, Ljava/util/AbstractMap;->isEmpty()Z

    move-result p1

    if-eqz p1, :cond_a

    :cond_9
    return-void

    :cond_a
    invoke-virtual {v1}, Ljava/util/TreeMap;->firstKey()Ljava/lang/Object;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/util/TreeMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_2
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/pdfbox/cos/COSObject;

    invoke-virtual {p0, v4, p2}, Lorg/apache/pdfbox/pdfparser/COSParser;->parseObjectDynamically(Lorg/apache/pdfbox/cos/COSObject;Z)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v5

    invoke-virtual {v4, v5}, Lorg/apache/pdfbox/cos/COSObject;->setObject(Lorg/apache/pdfbox/cos/COSBase;)V

    invoke-direct {p0, v0, v5, v3}, Lorg/apache/pdfbox/pdfparser/COSParser;->addNewToList(Ljava/util/Queue;Lorg/apache/pdfbox/cos/COSBase;Ljava/util/Set;)V

    invoke-direct {p0, v4}, Lorg/apache/pdfbox/pdfparser/COSParser;->getObjectId(Lorg/apache/pdfbox/cos/COSObject;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v2, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_2
.end method

.method public parseFDFHeader()Z
    .locals 2

    const-string v0, "%FDF-"

    const-string v1, "1.0"

    invoke-direct {p0, v0, v1}, Lorg/apache/pdfbox/pdfparser/COSParser;->parseHeader(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public parseObjectDynamically(JIZ)Lorg/apache/pdfbox/cos/COSBase;
    .locals 16

    .line 1
    move-object/from16 v0, p0

    move/from16 v1, p3

    new-instance v2, Lorg/apache/pdfbox/cos/COSObjectKey;

    move-wide/from16 v5, p1

    invoke-direct {v2, v5, v6, v1}, Lorg/apache/pdfbox/cos/COSObjectKey;-><init>(JI)V

    iget-object v3, v0, Lorg/apache/pdfbox/pdfparser/BaseParser;->document:Lorg/apache/pdfbox/cos/COSDocument;

    invoke-virtual {v3, v2}, Lorg/apache/pdfbox/cos/COSDocument;->getObjectFromPool(Lorg/apache/pdfbox/cos/COSObjectKey;)Lorg/apache/pdfbox/cos/COSObject;

    move-result-object v9

    invoke-virtual {v9}, Lorg/apache/pdfbox/cos/COSObject;->getObject()Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v3

    if-nez v3, :cond_c

    iget-object v3, v0, Lorg/apache/pdfbox/pdfparser/COSParser;->xrefTrailerResolver:Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver;

    invoke-virtual {v3}, Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver;->getXrefTable()Ljava/util/Map;

    move-result-object v3

    invoke-interface {v3, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    move-object v10, v3

    check-cast v10, Ljava/lang/Long;

    const-wide/16 v3, 0x0

    const-string v11, ":"

    if-eqz p4, :cond_1

    if-eqz v10, :cond_0

    invoke-virtual {v10}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    cmp-long v7, v7, v3

    if-lez v7, :cond_0

    goto :goto_0

    :cond_0
    new-instance v1, Ljava/io/IOException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Object must be defined and must not be compressed object: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Lorg/apache/pdfbox/cos/COSObjectKey;->getNumber()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Lorg/apache/pdfbox/cos/COSObjectKey;->getGeneration()I

    move-result v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_1
    :goto_0
    if-nez v10, :cond_2

    sget-object v1, Lorg/apache/pdfbox/cos/COSNull;->NULL:Lorg/apache/pdfbox/cos/COSNull;

    invoke-virtual {v9, v1}, Lorg/apache/pdfbox/cos/COSObject;->setObject(Lorg/apache/pdfbox/cos/COSBase;)V

    goto/16 :goto_4

    :cond_2
    invoke-virtual {v10}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    cmp-long v3, v7, v3

    const/4 v4, 0x1

    if-lez v3, :cond_a

    iget-object v3, v0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    invoke-virtual {v10}, Ljava/lang/Long;->longValue()J

    move-result-wide v7

    invoke-virtual {v3, v7, v8}, Lorg/apache/pdfbox/io/PushBackInputStream;->seek(J)V

    invoke-virtual/range {p0 .. p0}, Lorg/apache/pdfbox/pdfparser/BaseParser;->readObjectNumber()I

    move-result v3

    int-to-long v12, v3

    invoke-virtual/range {p0 .. p0}, Lorg/apache/pdfbox/pdfparser/BaseParser;->readGenerationNumber()I

    move-result v14

    sget-object v3, Lorg/apache/pdfbox/pdfparser/COSParser;->OBJ_MARKER:[C

    invoke-virtual {v0, v3, v4}, Lorg/apache/pdfbox/pdfparser/BaseParser;->readExpectedString([CZ)V

    invoke-virtual {v2}, Lorg/apache/pdfbox/cos/COSObjectKey;->getNumber()J

    move-result-wide v3

    cmp-long v3, v12, v3

    if-nez v3, :cond_9

    invoke-virtual {v2}, Lorg/apache/pdfbox/cos/COSObjectKey;->getGeneration()I

    move-result v3

    if-ne v14, v3, :cond_9

    invoke-virtual/range {p0 .. p0}, Lorg/apache/pdfbox/pdfparser/BaseParser;->skipSpaces()V

    invoke-virtual/range {p0 .. p0}, Lorg/apache/pdfbox/pdfparser/BaseParser;->parseDirObject()Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v2

    invoke-virtual/range {p0 .. p0}, Lorg/apache/pdfbox/pdfparser/BaseParser;->readString()Ljava/lang/String;

    move-result-object v15

    const-string v3, "stream"

    invoke-virtual {v15, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    const-string v7, "endobj"

    if-eqz v3, :cond_5

    iget-object v3, v0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    const-string v4, "ISO-8859-1"

    invoke-virtual {v15, v4}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v4

    invoke-virtual {v3, v4}, Lorg/apache/pdfbox/io/PushBackInputStream;->unread([B)V

    iget-object v3, v0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    const/16 v4, 0x20

    invoke-virtual {v3, v4}, Lorg/apache/pdfbox/io/PushBackInputStream;->unread(I)V

    instance-of v3, v2, Lorg/apache/pdfbox/cos/COSDictionary;

    if-eqz v3, :cond_4

    check-cast v2, Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-virtual {v0, v2}, Lorg/apache/pdfbox/pdfparser/COSParser;->parseCOSStream(Lorg/apache/pdfbox/cos/COSDictionary;)Lorg/apache/pdfbox/cos/COSStream;

    move-result-object v2

    iget-object v3, v0, Lorg/apache/pdfbox/pdfparser/COSParser;->securityHandler:Lorg/apache/pdfbox/pdmodel/encryption/SecurityHandler;

    if-eqz v3, :cond_3

    move-object/from16 p4, v7

    int-to-long v7, v1

    move-object v4, v2

    move-wide/from16 v5, p1

    move-object/from16 v1, p4

    invoke-virtual/range {v3 .. v8}, Lorg/apache/pdfbox/pdmodel/encryption/SecurityHandler;->decryptStream(Lorg/apache/pdfbox/cos/COSStream;JJ)V

    goto :goto_1

    :cond_3
    move-object v1, v7

    :goto_1
    invoke-virtual/range {p0 .. p0}, Lorg/apache/pdfbox/pdfparser/BaseParser;->skipSpaces()V

    invoke-virtual/range {p0 .. p0}, Lorg/apache/pdfbox/pdfparser/BaseParser;->readLine()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v15, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_7

    const-string v3, "endstream"

    invoke-virtual {v15, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_7

    const/16 v3, 0x9

    invoke-virtual {v15, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_7

    invoke-virtual/range {p0 .. p0}, Lorg/apache/pdfbox/pdfparser/BaseParser;->readLine()Ljava/lang/String;

    move-result-object v15

    goto :goto_2

    :cond_4
    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Stream not preceded by dictionary (offset: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v3, ")."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_5
    iget-object v3, v0, Lorg/apache/pdfbox/pdfparser/COSParser;->securityHandler:Lorg/apache/pdfbox/pdmodel/encryption/SecurityHandler;

    if-eqz v3, :cond_6

    move-object/from16 p4, v7

    int-to-long v7, v1

    move-object v4, v2

    move-wide/from16 v5, p1

    move-object/from16 v1, p4

    invoke-virtual/range {v3 .. v8}, Lorg/apache/pdfbox/pdmodel/encryption/SecurityHandler;->decrypt(Lorg/apache/pdfbox/cos/COSBase;JJ)V

    goto :goto_2

    :cond_6
    move-object v1, v7

    :cond_7
    :goto_2
    invoke-virtual {v9, v2}, Lorg/apache/pdfbox/cos/COSObject;->setObject(Lorg/apache/pdfbox/cos/COSBase;)V

    invoke-virtual {v15, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_c

    iget-boolean v1, v0, Lorg/apache/pdfbox/pdfparser/COSParser;->isLenient:Z

    const-string v2, "\'"

    const-string v3, " does not end with \'endobj\' but with \'"

    const-string v4, ") at offset "

    const-string v5, "Object ("

    if-eqz v1, :cond_8

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "PdfBoxAndroid"

    invoke-static {v2, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_4

    :cond_8
    new-instance v1, Ljava/io/IOException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_9
    new-instance v1, Ljava/io/IOException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "XREF for "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Lorg/apache/pdfbox/cos/COSObjectKey;->getNumber()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Lorg/apache/pdfbox/cos/COSObjectKey;->getGeneration()I

    move-result v2

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, " points to wrong object: "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_a
    invoke-virtual {v10}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    neg-long v1, v1

    long-to-int v1, v1

    int-to-long v2, v1

    const/4 v5, 0x0

    invoke-virtual {v0, v2, v3, v5, v4}, Lorg/apache/pdfbox/pdfparser/COSParser;->parseObjectDynamically(JIZ)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v2

    instance-of v3, v2, Lorg/apache/pdfbox/cos/COSStream;

    if-eqz v3, :cond_c

    new-instance v3, Lorg/apache/pdfbox/pdfparser/PDFObjectStreamParser;

    check-cast v2, Lorg/apache/pdfbox/cos/COSStream;

    iget-object v4, v0, Lorg/apache/pdfbox/pdfparser/BaseParser;->document:Lorg/apache/pdfbox/cos/COSDocument;

    invoke-direct {v3, v2, v4}, Lorg/apache/pdfbox/pdfparser/PDFObjectStreamParser;-><init>(Lorg/apache/pdfbox/cos/COSStream;Lorg/apache/pdfbox/cos/COSDocument;)V

    invoke-virtual {v3}, Lorg/apache/pdfbox/pdfparser/PDFObjectStreamParser;->parse()V

    invoke-virtual {v3}, Lorg/apache/pdfbox/pdfparser/BaseParser;->close()V

    iget-object v2, v0, Lorg/apache/pdfbox/pdfparser/COSParser;->xrefTrailerResolver:Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver;

    invoke-virtual {v2, v1}, Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver;->getContainedObjectNumbers(I)Ljava/util/Set;

    move-result-object v1

    invoke-virtual {v3}, Lorg/apache/pdfbox/pdfparser/PDFObjectStreamParser;->getObjects()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_b
    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_c

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/pdfbox/cos/COSObject;

    new-instance v4, Lorg/apache/pdfbox/cos/COSObjectKey;

    invoke-direct {v4, v3}, Lorg/apache/pdfbox/cos/COSObjectKey;-><init>(Lorg/apache/pdfbox/cos/COSObject;)V

    invoke-virtual {v4}, Lorg/apache/pdfbox/cos/COSObjectKey;->getNumber()J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-interface {v1, v5}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_b

    iget-object v5, v0, Lorg/apache/pdfbox/pdfparser/BaseParser;->document:Lorg/apache/pdfbox/cos/COSDocument;

    invoke-virtual {v5, v4}, Lorg/apache/pdfbox/cos/COSDocument;->getObjectFromPool(Lorg/apache/pdfbox/cos/COSObjectKey;)Lorg/apache/pdfbox/cos/COSObject;

    move-result-object v4

    invoke-virtual {v3}, Lorg/apache/pdfbox/cos/COSObject;->getObject()Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v3

    invoke-virtual {v4, v3}, Lorg/apache/pdfbox/cos/COSObject;->setObject(Lorg/apache/pdfbox/cos/COSBase;)V

    goto :goto_3

    :cond_c
    :goto_4
    invoke-virtual {v9}, Lorg/apache/pdfbox/cos/COSObject;->getObject()Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v1

    return-object v1
.end method

.method public final parseObjectDynamically(Lorg/apache/pdfbox/cos/COSObject;Z)Lorg/apache/pdfbox/cos/COSBase;
    .locals 2

    .line 2
    invoke-virtual {p1}, Lorg/apache/pdfbox/cos/COSObject;->getObjectNumber()J

    move-result-wide v0

    invoke-virtual {p1}, Lorg/apache/pdfbox/cos/COSObject;->getGenerationNumber()I

    move-result p1

    invoke-virtual {p0, v0, v1, p1, p2}, Lorg/apache/pdfbox/pdfparser/COSParser;->parseObjectDynamically(JIZ)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object p1

    return-object p1
.end method

.method public parsePDFHeader()Z
    .locals 2

    const-string v0, "%PDF-"

    const-string v1, "1.4"

    invoke-direct {p0, v0, v1}, Lorg/apache/pdfbox/pdfparser/COSParser;->parseHeader(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public parseStartXref()J
    .locals 2

    sget-object v0, Lorg/apache/pdfbox/pdfparser/COSParser;->STARTXREF:[C

    invoke-virtual {p0, v0}, Lorg/apache/pdfbox/pdfparser/BaseParser;->isString([C)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfparser/BaseParser;->readString()Ljava/lang/String;

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfparser/BaseParser;->skipSpaces()V

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfparser/BaseParser;->readLong()J

    move-result-wide v0

    goto :goto_0

    :cond_0
    const-wide/16 v0, -0x1

    :goto_0
    return-wide v0
.end method

.method public parseTrailer()Z
    .locals 6

    iget-object v0, p0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    invoke-virtual {v0}, Lorg/apache/pdfbox/io/PushBackInputStream;->peek()I

    move-result v0

    const/16 v1, 0x74

    const/4 v2, 0x0

    if-eq v0, v1, :cond_0

    return v2

    :cond_0
    iget-object v0, p0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    invoke-virtual {v0}, Lorg/apache/pdfbox/io/PushBackInputStream;->getOffset()J

    move-result-wide v0

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfparser/BaseParser;->readLine()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    const-string v5, "trailer"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    invoke-virtual {v3, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v2, p0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    const/4 v3, 0x7

    int-to-long v3, v3

    add-long/2addr v0, v3

    invoke-virtual {v2, v0, v1}, Lorg/apache/pdfbox/io/PushBackInputStream;->seek(J)V

    goto :goto_0

    :cond_1
    return v2

    :cond_2
    :goto_0
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfparser/BaseParser;->skipSpaces()V

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfparser/BaseParser;->parseCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/pdfbox/pdfparser/COSParser;->xrefTrailerResolver:Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver;

    invoke-virtual {v1, v0}, Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver;->setTrailer(Lorg/apache/pdfbox/cos/COSDictionary;)V

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfparser/BaseParser;->skipSpaces()V

    const/4 v0, 0x1

    return v0
.end method

.method public parseXref(J)Lorg/apache/pdfbox/cos/COSDictionary;
    .locals 17

    move-object/from16 v0, p0

    iget-object v1, v0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    move-wide/from16 v2, p1

    invoke-virtual {v1, v2, v3}, Lorg/apache/pdfbox/io/PushBackInputStream;->seek(J)V

    const-wide/16 v1, 0x0

    invoke-virtual/range {p0 .. p0}, Lorg/apache/pdfbox/pdfparser/COSParser;->parseStartXref()J

    move-result-wide v3

    invoke-static {v1, v2, v3, v4}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Lorg/apache/pdfbox/pdfparser/COSParser;->checkXRefOffset(J)J

    move-result-wide v3

    const-wide/16 v5, -0x1

    cmp-long v7, v3, v5

    if-lez v7, :cond_0

    move-wide v1, v3

    :cond_0
    iget-object v3, v0, Lorg/apache/pdfbox/pdfparser/BaseParser;->document:Lorg/apache/pdfbox/cos/COSDocument;

    invoke-virtual {v3, v1, v2}, Lorg/apache/pdfbox/cos/COSDocument;->setStartXref(J)V

    move-wide v3, v1

    :cond_1
    :goto_0
    cmp-long v7, v3, v5

    const/4 v8, 0x1

    const/4 v9, 0x0

    if-lez v7, :cond_b

    iget-object v7, v0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    invoke-virtual {v7, v3, v4}, Lorg/apache/pdfbox/io/PushBackInputStream;->seek(J)V

    invoke-virtual/range {p0 .. p0}, Lorg/apache/pdfbox/pdfparser/BaseParser;->skipSpaces()V

    iget-object v7, v0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    invoke-virtual {v7}, Lorg/apache/pdfbox/io/PushBackInputStream;->peek()I

    move-result v7

    const/16 v10, 0x78

    if-ne v7, v10, :cond_a

    invoke-virtual {v0, v3, v4}, Lorg/apache/pdfbox/pdfparser/COSParser;->parseXrefTable(J)Z

    iget-object v7, v0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    invoke-virtual {v7}, Lorg/apache/pdfbox/io/PushBackInputStream;->getOffset()J

    move-result-wide v7

    iput-wide v7, v0, Lorg/apache/pdfbox/pdfparser/COSParser;->trailerOffset:J

    :goto_1
    iget-boolean v7, v0, Lorg/apache/pdfbox/pdfparser/COSParser;->isLenient:Z

    const-string v8, "PdfBoxAndroid"

    if-eqz v7, :cond_3

    iget-object v7, v0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    invoke-virtual {v7}, Lorg/apache/pdfbox/io/PushBackInputStream;->peek()I

    move-result v7

    const/16 v10, 0x74

    if-eq v7, v10, :cond_3

    iget-object v7, v0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    invoke-virtual {v7}, Lorg/apache/pdfbox/io/PushBackInputStream;->getOffset()J

    move-result-wide v10

    iget-wide v12, v0, Lorg/apache/pdfbox/pdfparser/COSParser;->trailerOffset:J

    cmp-long v7, v10, v12

    if-nez v7, :cond_2

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Expected trailer object at position "

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v10, v0, Lorg/apache/pdfbox/pdfparser/COSParser;->trailerOffset:J

    invoke-virtual {v7, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string v10, ", keep trying"

    invoke-virtual {v7, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v8, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    invoke-virtual/range {p0 .. p0}, Lorg/apache/pdfbox/pdfparser/BaseParser;->readLine()Ljava/lang/String;

    goto :goto_1

    :cond_3
    invoke-virtual/range {p0 .. p0}, Lorg/apache/pdfbox/pdfparser/COSParser;->parseTrailer()Z

    move-result v7

    if-eqz v7, :cond_9

    iget-object v7, v0, Lorg/apache/pdfbox/pdfparser/COSParser;->xrefTrailerResolver:Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver;

    invoke-virtual {v7}, Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver;->getCurrentTrailer()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v7

    sget-object v10, Lorg/apache/pdfbox/cos/COSName;->XREF_STM:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v7, v10}, Lorg/apache/pdfbox/cos/COSDictionary;->containsKey(Lorg/apache/pdfbox/cos/COSName;)Z

    move-result v11

    if-eqz v11, :cond_7

    invoke-virtual {v7, v10}, Lorg/apache/pdfbox/cos/COSDictionary;->getInt(Lorg/apache/pdfbox/cos/COSName;)I

    move-result v11

    int-to-long v12, v11

    invoke-direct {v0, v12, v13, v9}, Lorg/apache/pdfbox/pdfparser/COSParser;->checkXRefStreamOffset(JZ)J

    move-result-wide v14

    cmp-long v16, v14, v5

    if-lez v16, :cond_4

    cmp-long v12, v14, v12

    if-eqz v12, :cond_4

    long-to-int v11, v14

    invoke-virtual {v7, v10, v11}, Lorg/apache/pdfbox/cos/COSDictionary;->setInt(Lorg/apache/pdfbox/cos/COSName;I)V

    :cond_4
    if-lez v11, :cond_5

    iget-object v8, v0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    int-to-long v10, v11

    invoke-virtual {v8, v10, v11}, Lorg/apache/pdfbox/io/PushBackInputStream;->seek(J)V

    invoke-virtual/range {p0 .. p0}, Lorg/apache/pdfbox/pdfparser/BaseParser;->skipSpaces()V

    invoke-direct {v0, v3, v4, v9}, Lorg/apache/pdfbox/pdfparser/COSParser;->parseXrefObjStream(JZ)J

    goto :goto_2

    :cond_5
    iget-boolean v3, v0, Lorg/apache/pdfbox/pdfparser/COSParser;->isLenient:Z

    const-string v4, "Skipped XRef stream due to a corrupt offset:"

    if-eqz v3, :cond_6

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v8, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    :cond_6
    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_7
    :goto_2
    sget-object v3, Lorg/apache/pdfbox/cos/COSName;->PREV:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v7, v3}, Lorg/apache/pdfbox/cos/COSDictionary;->getInt(Lorg/apache/pdfbox/cos/COSName;)I

    move-result v4

    int-to-long v8, v4

    cmp-long v4, v8, v5

    if-lez v4, :cond_8

    invoke-direct {v0, v8, v9}, Lorg/apache/pdfbox/pdfparser/COSParser;->checkXRefOffset(J)J

    move-result-wide v10

    cmp-long v4, v10, v5

    if-lez v4, :cond_8

    cmp-long v4, v10, v8

    if-eqz v4, :cond_8

    invoke-virtual {v7, v3, v10, v11}, Lorg/apache/pdfbox/cos/COSDictionary;->setLong(Lorg/apache/pdfbox/cos/COSName;J)V

    move-wide v3, v10

    goto/16 :goto_0

    :cond_8
    move-wide v3, v8

    goto/16 :goto_0

    :cond_9
    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Expected trailer object at position: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, v0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    invoke-virtual {v3}, Lorg/apache/pdfbox/io/PushBackInputStream;->getOffset()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_a
    invoke-direct {v0, v3, v4, v8}, Lorg/apache/pdfbox/pdfparser/COSParser;->parseXrefObjStream(JZ)J

    move-result-wide v3

    cmp-long v7, v3, v5

    if-lez v7, :cond_1

    invoke-direct {v0, v3, v4}, Lorg/apache/pdfbox/pdfparser/COSParser;->checkXRefOffset(J)J

    move-result-wide v7

    cmp-long v9, v7, v5

    if-lez v9, :cond_1

    cmp-long v9, v7, v3

    if-eqz v9, :cond_1

    iget-object v3, v0, Lorg/apache/pdfbox/pdfparser/COSParser;->xrefTrailerResolver:Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver;

    invoke-virtual {v3}, Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver;->getCurrentTrailer()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v3

    sget-object v4, Lorg/apache/pdfbox/cos/COSName;->PREV:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v3, v4, v7, v8}, Lorg/apache/pdfbox/cos/COSDictionary;->setLong(Lorg/apache/pdfbox/cos/COSName;J)V

    move-wide v3, v7

    goto/16 :goto_0

    :cond_b
    iget-object v3, v0, Lorg/apache/pdfbox/pdfparser/COSParser;->xrefTrailerResolver:Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver;

    invoke-virtual {v3, v1, v2}, Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver;->setStartxref(J)V

    iget-object v1, v0, Lorg/apache/pdfbox/pdfparser/COSParser;->xrefTrailerResolver:Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver;

    invoke-virtual {v1}, Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver;->getTrailer()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v1

    iget-object v2, v0, Lorg/apache/pdfbox/pdfparser/BaseParser;->document:Lorg/apache/pdfbox/cos/COSDocument;

    invoke-virtual {v2, v1}, Lorg/apache/pdfbox/cos/COSDocument;->setTrailer(Lorg/apache/pdfbox/cos/COSDictionary;)V

    iget-object v2, v0, Lorg/apache/pdfbox/pdfparser/BaseParser;->document:Lorg/apache/pdfbox/cos/COSDocument;

    sget-object v3, Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver$XRefType;->STREAM:Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver$XRefType;

    iget-object v4, v0, Lorg/apache/pdfbox/pdfparser/COSParser;->xrefTrailerResolver:Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver;

    invoke-virtual {v4}, Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver;->getXrefType()Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver$XRefType;

    move-result-object v4

    if-ne v3, v4, :cond_c

    goto :goto_3

    :cond_c
    move v8, v9

    :goto_3
    invoke-virtual {v2, v8}, Lorg/apache/pdfbox/cos/COSDocument;->setIsXRefStream(Z)V

    invoke-direct/range {p0 .. p0}, Lorg/apache/pdfbox/pdfparser/COSParser;->checkXrefOffsets()V

    iget-object v2, v0, Lorg/apache/pdfbox/pdfparser/BaseParser;->document:Lorg/apache/pdfbox/cos/COSDocument;

    iget-object v3, v0, Lorg/apache/pdfbox/pdfparser/COSParser;->xrefTrailerResolver:Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver;

    invoke-virtual {v3}, Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver;->getXrefTable()Ljava/util/Map;

    move-result-object v3

    invoke-virtual {v2, v3}, Lorg/apache/pdfbox/cos/COSDocument;->addXRefTable(Ljava/util/Map;)V

    return-object v1
.end method

.method public parseXrefStream(Lorg/apache/pdfbox/cos/COSStream;JZ)V
    .locals 1

    if-eqz p4, :cond_0

    iget-object p4, p0, Lorg/apache/pdfbox/pdfparser/COSParser;->xrefTrailerResolver:Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver;

    sget-object v0, Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver$XRefType;->STREAM:Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver$XRefType;

    invoke-virtual {p4, p2, p3, v0}, Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver;->nextXrefObj(JLorg/apache/pdfbox/pdfparser/XrefTrailerResolver$XRefType;)V

    iget-object p2, p0, Lorg/apache/pdfbox/pdfparser/COSParser;->xrefTrailerResolver:Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver;

    invoke-virtual {p2, p1}, Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver;->setTrailer(Lorg/apache/pdfbox/cos/COSDictionary;)V

    :cond_0
    new-instance p2, Lorg/apache/pdfbox/pdfparser/PDFXrefStreamParser;

    iget-object p3, p0, Lorg/apache/pdfbox/pdfparser/BaseParser;->document:Lorg/apache/pdfbox/cos/COSDocument;

    iget-object p4, p0, Lorg/apache/pdfbox/pdfparser/COSParser;->xrefTrailerResolver:Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver;

    invoke-direct {p2, p1, p3, p4}, Lorg/apache/pdfbox/pdfparser/PDFXrefStreamParser;-><init>(Lorg/apache/pdfbox/cos/COSStream;Lorg/apache/pdfbox/cos/COSDocument;Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver;)V

    invoke-virtual {p2}, Lorg/apache/pdfbox/pdfparser/PDFXrefStreamParser;->parse()V

    invoke-virtual {p2}, Lorg/apache/pdfbox/pdfparser/BaseParser;->close()V

    return-void
.end method

.method public parseXrefTable(J)Z
    .locals 10

    iget-object v0, p0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    invoke-virtual {v0}, Lorg/apache/pdfbox/io/PushBackInputStream;->peek()I

    move-result v0

    const/16 v1, 0x78

    const/4 v2, 0x0

    if-eq v0, v1, :cond_0

    return v2

    :cond_0
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfparser/BaseParser;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    const-string v1, "xref"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    return v2

    :cond_1
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfparser/BaseParser;->readString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ISO-8859-1"

    invoke-virtual {v0, v1}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v1

    iget-object v3, p0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    array-length v4, v1

    invoke-virtual {v3, v1, v2, v4}, Lorg/apache/pdfbox/io/PushBackInputStream;->unread([BII)V

    iget-object v1, p0, Lorg/apache/pdfbox/pdfparser/COSParser;->xrefTrailerResolver:Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver;

    sget-object v3, Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver$XRefType;->TABLE:Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver$XRefType;

    invoke-virtual {v1, p1, p2, v3}, Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver;->nextXrefObj(JLorg/apache/pdfbox/pdfparser/XrefTrailerResolver$XRefType;)V

    const-string p1, "trailer"

    invoke-virtual {v0, p1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result p1

    const-string p2, "PdfBoxAndroid"

    if-eqz p1, :cond_2

    const-string p1, "skipping empty xref table"

    invoke-static {p2, p1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    return v2

    :cond_2
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfparser/BaseParser;->readObjectNumber()I

    move-result p1

    int-to-long v0, p1

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfparser/BaseParser;->readLong()J

    move-result-wide v3

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfparser/BaseParser;->skipSpaces()V

    move p1, v2

    :goto_0
    int-to-long v5, p1

    cmp-long v5, v5, v3

    const/4 v6, 0x1

    if-gez v5, :cond_8

    iget-object v5, p0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    invoke-virtual {v5}, Lorg/apache/pdfbox/io/PushBackInputStream;->isEOF()Z

    move-result v5

    if-nez v5, :cond_8

    iget-object v5, p0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    invoke-virtual {v5}, Lorg/apache/pdfbox/io/PushBackInputStream;->peek()I

    move-result v5

    int-to-char v5, v5

    invoke-virtual {p0, v5}, Lorg/apache/pdfbox/pdfparser/BaseParser;->isEndOfName(C)Z

    move-result v5

    if-eqz v5, :cond_3

    goto/16 :goto_2

    :cond_3
    iget-object v5, p0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    invoke-virtual {v5}, Lorg/apache/pdfbox/io/PushBackInputStream;->peek()I

    move-result v5

    const/16 v7, 0x74

    if-ne v5, v7, :cond_4

    goto/16 :goto_2

    :cond_4
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfparser/BaseParser;->readLine()Ljava/lang/String;

    move-result-object v5

    const-string v7, "\\s"

    invoke-virtual {v5, v7}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    array-length v8, v7

    const/4 v9, 0x3

    if-ge v8, v9, :cond_5

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "invalid xref line: "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p2, p1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    :cond_5
    array-length v5, v7

    sub-int/2addr v5, v6

    aget-object v5, v7, v5

    const-string v8, "n"

    invoke-virtual {v5, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    :try_start_0
    aget-object v5, v7, v2

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    aget-object v6, v7, v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    new-instance v7, Lorg/apache/pdfbox/cos/COSObjectKey;

    invoke-direct {v7, v0, v1, v6}, Lorg/apache/pdfbox/cos/COSObjectKey;-><init>(JI)V

    iget-object v6, p0, Lorg/apache/pdfbox/pdfparser/COSParser;->xrefTrailerResolver:Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver;

    int-to-long v8, v5

    invoke-virtual {v6, v7, v8, v9}, Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver;->setXRef(Lorg/apache/pdfbox/cos/COSObjectKey;J)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception p1

    new-instance p2, Ljava/io/IOException;

    invoke-direct {p2, p1}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw p2

    :cond_6
    const/4 v5, 0x2

    aget-object v5, v7, v5

    const-string v6, "f"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_7

    :goto_1
    const-wide/16 v5, 0x1

    add-long/2addr v0, v5

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfparser/BaseParser;->skipSpaces()V

    add-int/lit8 p1, p1, 0x1

    goto/16 :goto_0

    :cond_7
    new-instance p1, Ljava/io/IOException;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Corrupt XRefTable Entry - ObjID:"

    invoke-virtual {p2, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_8
    :goto_2
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfparser/BaseParser;->skipSpaces()V

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfparser/BaseParser;->isDigit()Z

    move-result p1

    if-nez p1, :cond_2

    return v6
.end method

.method public final rebuildTrailer()Lorg/apache/pdfbox/cos/COSDictionary;
    .locals 7

    invoke-direct {p0}, Lorg/apache/pdfbox/pdfparser/COSParser;->bfSearchForObjects()V

    iget-object v0, p0, Lorg/apache/pdfbox/pdfparser/COSParser;->bfSearchCOSObjectKeyOffsets:Ljava/util/Map;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lorg/apache/pdfbox/pdfparser/COSParser;->xrefTrailerResolver:Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver;

    sget-object v1, Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver$XRefType;->TABLE:Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver$XRefType;

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3, v1}, Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver;->nextXrefObj(JLorg/apache/pdfbox/pdfparser/XrefTrailerResolver$XRefType;)V

    iget-object v0, p0, Lorg/apache/pdfbox/pdfparser/COSParser;->bfSearchCOSObjectKeyOffsets:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/pdfbox/cos/COSObjectKey;

    iget-object v4, p0, Lorg/apache/pdfbox/pdfparser/COSParser;->xrefTrailerResolver:Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver;

    iget-object v5, p0, Lorg/apache/pdfbox/pdfparser/COSParser;->bfSearchCOSObjectKeyOffsets:Ljava/util/Map;

    invoke-interface {v5, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Long;

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    invoke-virtual {v4, v1, v5, v6}, Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver;->setXRef(Lorg/apache/pdfbox/cos/COSObjectKey;J)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lorg/apache/pdfbox/pdfparser/COSParser;->xrefTrailerResolver:Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver;

    invoke-virtual {v0, v2, v3}, Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver;->setStartxref(J)V

    iget-object v0, p0, Lorg/apache/pdfbox/pdfparser/COSParser;->xrefTrailerResolver:Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver;

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdfparser/XrefTrailerResolver;->getTrailer()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfparser/COSParser;->getDocument()Lorg/apache/pdfbox/cos/COSDocument;

    move-result-object v1

    invoke-virtual {v1, v0}, Lorg/apache/pdfbox/cos/COSDocument;->setTrailer(Lorg/apache/pdfbox/cos/COSDictionary;)V

    iget-object v1, p0, Lorg/apache/pdfbox/pdfparser/COSParser;->bfSearchCOSObjectKeyOffsets:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/pdfbox/cos/COSObjectKey;

    iget-object v3, p0, Lorg/apache/pdfbox/pdfparser/COSParser;->bfSearchCOSObjectKeyOffsets:Ljava/util/Map;

    invoke-interface {v3, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    iget-object v4, p0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    invoke-virtual {v4, v5, v6}, Lorg/apache/pdfbox/io/PushBackInputStream;->seek(J)V

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfparser/BaseParser;->readObjectNumber()I

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfparser/BaseParser;->readGenerationNumber()I

    sget-object v3, Lorg/apache/pdfbox/pdfparser/COSParser;->OBJ_MARKER:[C

    const/4 v4, 0x1

    invoke-virtual {p0, v3, v4}, Lorg/apache/pdfbox/pdfparser/BaseParser;->readExpectedString([CZ)V

    :try_start_0
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfparser/BaseParser;->parseCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v3

    if-eqz v3, :cond_1

    sget-object v4, Lorg/apache/pdfbox/cos/COSName;->CATALOG:Lorg/apache/pdfbox/cos/COSName;

    sget-object v5, Lorg/apache/pdfbox/cos/COSName;->TYPE:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v3, v5}, Lorg/apache/pdfbox/cos/COSDictionary;->getCOSName(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSName;

    move-result-object v5

    invoke-virtual {v4, v5}, Lorg/apache/pdfbox/cos/COSName;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    sget-object v3, Lorg/apache/pdfbox/cos/COSName;->ROOT:Lorg/apache/pdfbox/cos/COSName;

    iget-object v4, p0, Lorg/apache/pdfbox/pdfparser/BaseParser;->document:Lorg/apache/pdfbox/cos/COSDocument;

    :goto_2
    invoke-virtual {v4, v2}, Lorg/apache/pdfbox/cos/COSDocument;->getObjectFromPool(Lorg/apache/pdfbox/cos/COSObjectKey;)Lorg/apache/pdfbox/cos/COSObject;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    goto :goto_1

    :cond_2
    sget-object v4, Lorg/apache/pdfbox/cos/COSName;->TITLE:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v3, v4}, Lorg/apache/pdfbox/cos/COSDictionary;->containsKey(Lorg/apache/pdfbox/cos/COSName;)Z

    move-result v4

    if-nez v4, :cond_3

    sget-object v4, Lorg/apache/pdfbox/cos/COSName;->AUTHOR:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v3, v4}, Lorg/apache/pdfbox/cos/COSDictionary;->containsKey(Lorg/apache/pdfbox/cos/COSName;)Z

    move-result v4

    if-nez v4, :cond_3

    sget-object v4, Lorg/apache/pdfbox/cos/COSName;->SUBJECT:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v3, v4}, Lorg/apache/pdfbox/cos/COSDictionary;->containsKey(Lorg/apache/pdfbox/cos/COSName;)Z

    move-result v4

    if-nez v4, :cond_3

    sget-object v4, Lorg/apache/pdfbox/cos/COSName;->KEYWORDS:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v3, v4}, Lorg/apache/pdfbox/cos/COSDictionary;->containsKey(Lorg/apache/pdfbox/cos/COSName;)Z

    move-result v4

    if-nez v4, :cond_3

    sget-object v4, Lorg/apache/pdfbox/cos/COSName;->CREATOR:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v3, v4}, Lorg/apache/pdfbox/cos/COSDictionary;->containsKey(Lorg/apache/pdfbox/cos/COSName;)Z

    move-result v4

    if-nez v4, :cond_3

    sget-object v4, Lorg/apache/pdfbox/cos/COSName;->PRODUCER:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v3, v4}, Lorg/apache/pdfbox/cos/COSDictionary;->containsKey(Lorg/apache/pdfbox/cos/COSName;)Z

    move-result v4

    if-nez v4, :cond_3

    sget-object v4, Lorg/apache/pdfbox/cos/COSName;->CREATION_DATE:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v3, v4}, Lorg/apache/pdfbox/cos/COSDictionary;->containsKey(Lorg/apache/pdfbox/cos/COSName;)Z

    move-result v3

    if-eqz v3, :cond_1

    :cond_3
    sget-object v3, Lorg/apache/pdfbox/cos/COSName;->INFO:Lorg/apache/pdfbox/cos/COSName;

    iget-object v4, p0, Lorg/apache/pdfbox/pdfparser/BaseParser;->document:Lorg/apache/pdfbox/cos/COSDocument;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    :catch_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Skipped object "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v2, ", either it\'s corrupt or not a dictionary"

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "PdfBoxAndroid"

    invoke-static {v3, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    :cond_4
    const/4 v0, 0x0

    :cond_5
    return-object v0
.end method

.method public setEOFLookupRange(I)V
    .locals 1

    const/16 v0, 0xf

    if-le p1, v0, :cond_0

    iput p1, p0, Lorg/apache/pdfbox/pdfparser/COSParser;->readTrailBytes:I

    :cond_0
    return-void
.end method

.method public setLenient(Z)V
    .locals 1

    iget-boolean v0, p0, Lorg/apache/pdfbox/pdfparser/COSParser;->initialParseDone:Z

    if-nez v0, :cond_0

    iput-boolean p1, p0, Lorg/apache/pdfbox/pdfparser/COSParser;->isLenient:Z

    return-void

    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Cannot change leniency after parsing"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.class public Lorg/apache/pdfbox/pdfparser/FDFParser;
.super Lorg/apache/pdfbox/pdfparser/COSParser;
.source "SourceFile"


# static fields
.field private static final EMPTY_INPUT_STREAM:Ljava/io/InputStream;


# instance fields
.field private final raStream:Lorg/apache/pdfbox/io/RandomAccessBufferedFileInputStream;

.field private tempPDFFile:Ljava/io/File;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/io/ByteArrayInputStream;

    const/4 v1, 0x0

    new-array v1, v1, [B

    invoke-direct {v0, v1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    sput-object v0, Lorg/apache/pdfbox/pdfparser/FDFParser;->EMPTY_INPUT_STREAM:Ljava/io/InputStream;

    return-void
.end method

.method public constructor <init>(Ljava/io/File;)V
    .locals 2

    .line 1
    sget-object v0, Lorg/apache/pdfbox/pdfparser/FDFParser;->EMPTY_INPUT_STREAM:Ljava/io/InputStream;

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/pdfparser/COSParser;-><init>(Ljava/io/InputStream;)V

    invoke-virtual {p1}, Ljava/io/File;->length()J

    move-result-wide v0

    iput-wide v0, p0, Lorg/apache/pdfbox/pdfparser/COSParser;->fileLen:J

    new-instance v0, Lorg/apache/pdfbox/io/RandomAccessBufferedFileInputStream;

    invoke-direct {v0, p1}, Lorg/apache/pdfbox/io/RandomAccessBufferedFileInputStream;-><init>(Ljava/io/File;)V

    iput-object v0, p0, Lorg/apache/pdfbox/pdfparser/FDFParser;->raStream:Lorg/apache/pdfbox/io/RandomAccessBufferedFileInputStream;

    invoke-direct {p0}, Lorg/apache/pdfbox/pdfparser/FDFParser;->init()V

    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 2

    .line 2
    sget-object v0, Lorg/apache/pdfbox/pdfparser/FDFParser;->EMPTY_INPUT_STREAM:Ljava/io/InputStream;

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/pdfparser/COSParser;-><init>(Ljava/io/InputStream;)V

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/pdfparser/COSParser;->createTmpFile(Ljava/io/InputStream;)Ljava/io/File;

    move-result-object p1

    iput-object p1, p0, Lorg/apache/pdfbox/pdfparser/FDFParser;->tempPDFFile:Ljava/io/File;

    invoke-virtual {p1}, Ljava/io/File;->length()J

    move-result-wide v0

    iput-wide v0, p0, Lorg/apache/pdfbox/pdfparser/COSParser;->fileLen:J

    new-instance p1, Lorg/apache/pdfbox/io/RandomAccessBufferedFileInputStream;

    iget-object v0, p0, Lorg/apache/pdfbox/pdfparser/FDFParser;->tempPDFFile:Ljava/io/File;

    invoke-direct {p1, v0}, Lorg/apache/pdfbox/io/RandomAccessBufferedFileInputStream;-><init>(Ljava/io/File;)V

    iput-object p1, p0, Lorg/apache/pdfbox/pdfparser/FDFParser;->raStream:Lorg/apache/pdfbox/io/RandomAccessBufferedFileInputStream;

    invoke-direct {p0}, Lorg/apache/pdfbox/pdfparser/FDFParser;->init()V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .line 3
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/pdfparser/FDFParser;-><init>(Ljava/io/File;)V

    return-void
.end method

.method private deleteTempFile()V
    .locals 5

    const-string v0, "\' can\'t be deleted"

    const-string v1, "Temporary file \'"

    const-string v2, "PdfBoxAndroid"

    iget-object v3, p0, Lorg/apache/pdfbox/pdfparser/FDFParser;->tempPDFFile:Ljava/io/File;

    if-eqz v3, :cond_0

    :try_start_0
    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    move-result v3

    if-nez v3, :cond_0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v4, p0, Lorg/apache/pdfbox/pdfparser/FDFParser;->tempPDFFile:Ljava/io/File;

    invoke-virtual {v4}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lorg/apache/pdfbox/pdfparser/FDFParser;->tempPDFFile:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_0
    :goto_0
    return-void
.end method

.method private init()V
    .locals 3

    const-string v0, "org.apache.pdfbox.pdfparser.nonSequentialPDFParser.eofLookupRange"

    invoke-static {v0}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    :try_start_0
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p0, v1}, Lorg/apache/pdfbox/pdfparser/COSParser;->setEOFLookupRange(I)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "System property org.apache.pdfbox.pdfparser.nonSequentialPDFParser.eofLookupRange does not contain an integer value, but: \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "\'"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "PdfBoxAndroid"

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    new-instance v0, Lorg/apache/pdfbox/cos/COSDocument;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSDocument;-><init>(Z)V

    iput-object v0, p0, Lorg/apache/pdfbox/pdfparser/BaseParser;->document:Lorg/apache/pdfbox/cos/COSDocument;

    new-instance v0, Lorg/apache/pdfbox/io/PushBackInputStream;

    iget-object v1, p0, Lorg/apache/pdfbox/pdfparser/FDFParser;->raStream:Lorg/apache/pdfbox/io/RandomAccessBufferedFileInputStream;

    const/16 v2, 0x1000

    invoke-direct {v0, v1, v2}, Lorg/apache/pdfbox/io/PushBackInputStream;-><init>(Ljava/io/InputStream;I)V

    iput-object v0, p0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    return-void
.end method

.method private initialParse()V
    .locals 5

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfparser/COSParser;->getStartxrefOffset()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdfparser/COSParser;->parseXref(J)Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfparser/COSParser;->rebuildTrailer()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    :goto_0
    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSDictionary;->getValues()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    const/4 v3, 0x0

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/pdfbox/cos/COSBase;

    instance-of v4, v2, Lorg/apache/pdfbox/cos/COSObject;

    if-eqz v4, :cond_1

    check-cast v2, Lorg/apache/pdfbox/cos/COSObject;

    invoke-virtual {p0, v2, v3}, Lorg/apache/pdfbox/pdfparser/COSParser;->parseObjectDynamically(Lorg/apache/pdfbox/cos/COSObject;Z)Lorg/apache/pdfbox/cos/COSBase;

    goto :goto_1

    :cond_2
    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->ROOT:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getItem(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSObject;

    if-eqz v0, :cond_4

    invoke-virtual {p0, v0, v3}, Lorg/apache/pdfbox/pdfparser/COSParser;->parseObjectDynamically(Lorg/apache/pdfbox/cos/COSObject;Z)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    instance-of v1, v0, Lorg/apache/pdfbox/cos/COSDictionary;

    if-eqz v1, :cond_3

    check-cast v0, Lorg/apache/pdfbox/cos/COSDictionary;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdfparser/COSParser;->parseDictObjects(Lorg/apache/pdfbox/cos/COSDictionary;[Lorg/apache/pdfbox/cos/COSName;)V

    :cond_3
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/pdfbox/pdfparser/COSParser;->initialParseDone:Z

    return-void

    :cond_4
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Missing root object specification in trailer."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public getFDFDocument()Lorg/apache/pdfbox/pdmodel/fdf/FDFDocument;
    .locals 2

    new-instance v0, Lorg/apache/pdfbox/pdmodel/fdf/FDFDocument;

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfparser/COSParser;->getDocument()Lorg/apache/pdfbox/cos/COSDocument;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/pdmodel/fdf/FDFDocument;-><init>(Lorg/apache/pdfbox/cos/COSDocument;)V

    return-object v0
.end method

.method public parse()V
    .locals 2

    :try_start_0
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfparser/COSParser;->parseFDFHeader()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lorg/apache/pdfbox/pdfparser/FDFParser;->initialParse()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    invoke-static {v0}, Lorg/apache/pdfbox/io/IOUtils;->closeQuietly(Ljava/io/Closeable;)V

    invoke-direct {p0}, Lorg/apache/pdfbox/pdfparser/FDFParser;->deleteTempFile()V

    return-void

    :cond_0
    :try_start_1
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Error: Header doesn\'t contain versioninfo"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    invoke-static {v1}, Lorg/apache/pdfbox/io/IOUtils;->closeQuietly(Ljava/io/Closeable;)V

    invoke-direct {p0}, Lorg/apache/pdfbox/pdfparser/FDFParser;->deleteTempFile()V

    iget-object v1, p0, Lorg/apache/pdfbox/pdfparser/BaseParser;->document:Lorg/apache/pdfbox/cos/COSDocument;

    if-eqz v1, :cond_1

    :try_start_2
    invoke-virtual {v1}, Lorg/apache/pdfbox/cos/COSDocument;->close()V

    const/4 v1, 0x0

    iput-object v1, p0, Lorg/apache/pdfbox/pdfparser/BaseParser;->document:Lorg/apache/pdfbox/cos/COSDocument;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    :cond_1
    throw v0
.end method

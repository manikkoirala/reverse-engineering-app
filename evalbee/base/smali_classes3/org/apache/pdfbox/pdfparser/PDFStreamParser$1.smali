.class Lorg/apache/pdfbox/pdfparser/PDFStreamParser$1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/pdfbox/pdfparser/PDFStreamParser;->getTokenIterator()Ljava/util/Iterator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Iterator<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lorg/apache/pdfbox/pdfparser/PDFStreamParser;

.field private token:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Lorg/apache/pdfbox/pdfparser/PDFStreamParser;)V
    .locals 0

    iput-object p1, p0, Lorg/apache/pdfbox/pdfparser/PDFStreamParser$1;->this$0:Lorg/apache/pdfbox/pdfparser/PDFStreamParser;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private tryNext()V
    .locals 2

    :try_start_0
    iget-object v0, p0, Lorg/apache/pdfbox/pdfparser/PDFStreamParser$1;->token:Ljava/lang/Object;

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/pdfbox/pdfparser/PDFStreamParser$1;->this$0:Lorg/apache/pdfbox/pdfparser/PDFStreamParser;

    invoke-static {v0}, Lorg/apache/pdfbox/pdfparser/PDFStreamParser;->access$000(Lorg/apache/pdfbox/pdfparser/PDFStreamParser;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/pdfbox/pdfparser/PDFStreamParser$1;->token:Ljava/lang/Object;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method


# virtual methods
.method public hasNext()Z
    .locals 1

    invoke-direct {p0}, Lorg/apache/pdfbox/pdfparser/PDFStreamParser$1;->tryNext()V

    iget-object v0, p0, Lorg/apache/pdfbox/pdfparser/PDFStreamParser$1;->token:Ljava/lang/Object;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public next()Ljava/lang/Object;
    .locals 2

    invoke-direct {p0}, Lorg/apache/pdfbox/pdfparser/PDFStreamParser$1;->tryNext()V

    iget-object v0, p0, Lorg/apache/pdfbox/pdfparser/PDFStreamParser$1;->token:Ljava/lang/Object;

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    iput-object v1, p0, Lorg/apache/pdfbox/pdfparser/PDFStreamParser$1;->token:Ljava/lang/Object;

    return-object v0

    :cond_0
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0
.end method

.method public remove()V
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.class public Lorg/apache/pdfbox/pdfparser/VisualSignatureParser;
.super Lorg/apache/pdfbox/pdfparser/BaseParser;
.source "SourceFile"


# direct methods
.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 0

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdfparser/BaseParser;-><init>(Ljava/io/InputStream;)V

    return-void
.end method

.method private parseObject()Z
    .locals 6

    :goto_0
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfparser/BaseParser;->skipSpaces()V

    iget-object v0, p0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    invoke-virtual {v0}, Lorg/apache/pdfbox/io/PushBackInputStream;->peek()I

    move-result v0

    int-to-char v0, v0

    const/16 v1, 0x65

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfparser/BaseParser;->readString()Ljava/lang/String;

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    invoke-virtual {v1}, Lorg/apache/pdfbox/io/PushBackInputStream;->isEOF()Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_1

    goto/16 :goto_5

    :cond_1
    const/16 v1, 0x78

    const/4 v3, 0x1

    if-ne v0, v1, :cond_2

    return v3

    :cond_2
    const/16 v1, 0x73

    const/16 v4, 0x74

    if-eq v0, v4, :cond_b

    if-ne v0, v1, :cond_3

    goto/16 :goto_3

    :cond_3
    invoke-virtual {p0, v2}, Lorg/apache/pdfbox/pdfparser/BaseParser;->parseObjectKey(Z)Lorg/apache/pdfbox/cos/COSObjectKey;

    move-result-object v0

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfparser/BaseParser;->skipSpaces()V

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfparser/BaseParser;->parseDirObject()Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfparser/BaseParser;->readString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "stream"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    iget-object v5, p0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    invoke-virtual {v4}, Ljava/lang/String;->getBytes()[B

    move-result-object v4

    invoke-virtual {v5, v4}, Lorg/apache/pdfbox/io/PushBackInputStream;->unread([B)V

    iget-object v4, p0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    const/16 v5, 0x20

    invoke-virtual {v4, v5}, Lorg/apache/pdfbox/io/PushBackInputStream;->unread(I)V

    instance-of v4, v1, Lorg/apache/pdfbox/cos/COSDictionary;

    if-eqz v4, :cond_4

    check-cast v1, Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-virtual {p0, v1}, Lorg/apache/pdfbox/pdfparser/BaseParser;->parseCOSStream(Lorg/apache/pdfbox/cos/COSDictionary;)Lorg/apache/pdfbox/cos/COSStream;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfparser/BaseParser;->readString()Ljava/lang/String;

    move-result-object v4

    goto :goto_1

    :cond_4
    new-instance v0, Ljava/io/IOException;

    const-string v1, "stream not preceded by dictionary"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_5
    :goto_1
    iget-object v5, p0, Lorg/apache/pdfbox/pdfparser/BaseParser;->document:Lorg/apache/pdfbox/cos/COSDocument;

    invoke-virtual {v5, v0}, Lorg/apache/pdfbox/cos/COSDocument;->getObjectFromPool(Lorg/apache/pdfbox/cos/COSObjectKey;)Lorg/apache/pdfbox/cos/COSObject;

    move-result-object v0

    instance-of v5, v1, Lorg/apache/pdfbox/cos/COSUpdateInfo;

    if-eqz v5, :cond_6

    move-object v5, v1

    check-cast v5, Lorg/apache/pdfbox/cos/COSUpdateInfo;

    invoke-interface {v5, v3}, Lorg/apache/pdfbox/cos/COSUpdateInfo;->setNeedToBeUpdated(Z)V

    :cond_6
    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSObject;->setObject(Lorg/apache/pdfbox/cos/COSBase;)V

    const-string v0, "endobj"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_a

    invoke-virtual {v4, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    iget-object v0, p0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    const/4 v1, 0x6

    invoke-virtual {v4, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/io/PushBackInputStream;->unread([B)V

    goto :goto_2

    :cond_7
    iget-object v1, p0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    invoke-virtual {v1}, Lorg/apache/pdfbox/io/PushBackInputStream;->isEOF()Z

    move-result v1

    if-nez v1, :cond_a

    :try_start_0
    invoke-static {v4}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    iget-object v1, p0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    sget-object v3, Lorg/apache/pdfbox/pdfwriter/COSWriter;->SPACE:[B

    invoke-virtual {v1, v3}, Lorg/apache/pdfbox/io/PushBackInputStream;->unread([B)V

    iget-object v1, p0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    invoke-virtual {v4}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    invoke-virtual {v1, v3}, Lorg/apache/pdfbox/io/PushBackInputStream;->unread([B)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    :catch_0
    move-exception v1

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfparser/BaseParser;->readString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_a

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfparser/BaseParser;->isClosing()Z

    move-result v5

    if-eqz v5, :cond_8

    iget-object v5, p0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    invoke-virtual {v5}, Lorg/apache/pdfbox/io/PushBackInputStream;->read()I

    :cond_8
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfparser/BaseParser;->skipSpaces()V

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfparser/BaseParser;->readString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    goto :goto_2

    :cond_9
    new-instance v0, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "expected=\'endobj\' firstReadAttempt=\'"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "\' "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, "secondReadAttempt=\'"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, p0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0

    :cond_a
    :goto_2
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfparser/BaseParser;->skipSpaces()V

    goto :goto_5

    :cond_b
    :goto_3
    if-ne v0, v4, :cond_c

    return v3

    :cond_c
    if-ne v0, v1, :cond_f

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfparser/BaseParser;->skipToNextObj()V

    const-string v0, "%%EOF"

    invoke-virtual {p0, v0}, Lorg/apache/pdfbox/pdfparser/VisualSignatureParser;->readExpectedStringUntilEOL(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_e

    iget-object v0, p0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    invoke-virtual {v0}, Lorg/apache/pdfbox/io/PushBackInputStream;->isEOF()Z

    move-result v0

    if-eqz v0, :cond_d

    goto :goto_4

    :cond_d
    new-instance v0, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "expected=\'%%EOF\' actual=\'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\' next="

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfparser/BaseParser;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " next="

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfparser/BaseParser;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_e
    :goto_4
    move v2, v3

    :cond_f
    :goto_5
    return v2
.end method


# virtual methods
.method public getDocument()Lorg/apache/pdfbox/cos/COSDocument;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdfparser/BaseParser;->document:Lorg/apache/pdfbox/cos/COSDocument;

    if-eqz v0, :cond_0

    return-object v0

    :cond_0
    new-instance v0, Ljava/io/IOException;

    const-string v1, "You must call parse() before calling getDocument()"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public parse()V
    .locals 4

    new-instance v0, Lorg/apache/pdfbox/cos/COSDocument;

    invoke-direct {v0}, Lorg/apache/pdfbox/cos/COSDocument;-><init>()V

    iput-object v0, p0, Lorg/apache/pdfbox/pdfparser/BaseParser;->document:Lorg/apache/pdfbox/cos/COSDocument;

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfparser/BaseParser;->skipToNextObj()V

    const/4 v0, 0x0

    :goto_0
    if-nez v0, :cond_2

    :try_start_0
    iget-object v1, p0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    invoke-virtual {v1}, Lorg/apache/pdfbox/io/PushBackInputStream;->isEOF()Z

    move-result v1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    if-eqz v1, :cond_0

    goto :goto_2

    :cond_0
    :try_start_1
    invoke-direct {p0}, Lorg/apache/pdfbox/pdfparser/VisualSignatureParser;->parseObject()Z

    move-result v0
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    :try_start_2
    const-string v2, "PdfBoxAndroid"

    const-string v3, "Parsing Error, Skipping Object"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfparser/BaseParser;->skipToNextObj()V

    :goto_1
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdfparser/BaseParser;->skipSpaces()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    :catch_1
    move-exception v1

    if-eqz v0, :cond_1

    goto :goto_2

    :cond_1
    throw v1

    :cond_2
    :goto_2
    return-void
.end method

.method public readExpectedStringUntilEOL(Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    :goto_0
    iget-object v0, p0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    invoke-virtual {v0}, Lorg/apache/pdfbox/io/PushBackInputStream;->read()I

    move-result v0

    invoke-virtual {p0, v0}, Lorg/apache/pdfbox/pdfparser/BaseParser;->isWhitespace(I)Z

    move-result v1

    const/4 v2, -0x1

    if-eqz v1, :cond_0

    if-eq v0, v2, :cond_0

    goto :goto_0

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const/4 v3, 0x0

    :goto_1
    invoke-virtual {p0, v0}, Lorg/apache/pdfbox/pdfparser/BaseParser;->isEOL(I)Z

    move-result v4

    if-nez v4, :cond_2

    if-eq v0, v2, :cond_2

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v4

    if-ge v3, v4, :cond_2

    int-to-char v0, v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {p1, v3}, Ljava/lang/String;->charAt(I)C

    move-result v4

    if-ne v4, v0, :cond_1

    add-int/lit8 v3, v3, 0x1

    iget-object v0, p0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    invoke-virtual {v0}, Lorg/apache/pdfbox/io/PushBackInputStream;->read()I

    move-result v0

    goto :goto_1

    :cond_1
    iget-object v0, p0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "ISO-8859-1"

    invoke-virtual {v2, v3}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v2

    invoke-virtual {v0, v2}, Lorg/apache/pdfbox/io/PushBackInputStream;->unread([B)V

    new-instance v0, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error: Expected to read \'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "\' instead started reading \'"

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "\'"

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    :goto_2
    invoke-virtual {p0, v0}, Lorg/apache/pdfbox/pdfparser/BaseParser;->isEOL(I)Z

    move-result p1

    if-eqz p1, :cond_3

    if-eq v0, v2, :cond_3

    iget-object p1, p0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    invoke-virtual {p1}, Lorg/apache/pdfbox/io/PushBackInputStream;->read()I

    move-result v0

    goto :goto_2

    :cond_3
    if-eq v0, v2, :cond_4

    iget-object p1, p0, Lorg/apache/pdfbox/pdfparser/BaseParser;->pdfSource:Lorg/apache/pdfbox/io/PushBackInputStream;

    invoke-virtual {p1, v0}, Lorg/apache/pdfbox/io/PushBackInputStream;->unread(I)V

    :cond_4
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

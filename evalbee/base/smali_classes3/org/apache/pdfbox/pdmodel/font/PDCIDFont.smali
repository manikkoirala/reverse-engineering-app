.class public abstract Lorg/apache/pdfbox/pdmodel/font/PDCIDFont;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/pdfbox/pdmodel/common/COSObjectable;
.implements Lorg/apache/pdfbox/pdmodel/font/PDFontLike;


# instance fields
.field private defaultWidth:F

.field protected final dict:Lorg/apache/pdfbox/cos/COSDictionary;

.field private dw2:[F

.field private fontDescriptor:Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;

.field protected final parent:Lorg/apache/pdfbox/pdmodel/font/PDType0Font;

.field private final positionVectors:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Lorg/apache/pdfbox/util/Vector;",
            ">;"
        }
    .end annotation
.end field

.field private final verticalDisplacementY:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private widths:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lorg/apache/pdfbox/cos/COSDictionary;Lorg/apache/pdfbox/pdmodel/font/PDType0Font;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFont;->verticalDisplacementY:Ljava/util/Map;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFont;->positionVectors:Ljava/util/Map;

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFont;->dict:Lorg/apache/pdfbox/cos/COSDictionary;

    iput-object p2, p0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFont;->parent:Lorg/apache/pdfbox/pdmodel/font/PDType0Font;

    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/font/PDCIDFont;->readWidths()V

    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/font/PDCIDFont;->readVerticalDisplacements()V

    return-void
.end method

.method private getDefaultPositionVector(I)Lorg/apache/pdfbox/util/Vector;
    .locals 3

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFont;->widths:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFont;->widths:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Float;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/Float;->floatValue()F

    move-result p1

    goto :goto_0

    :cond_0
    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/font/PDCIDFont;->getDefaultWidth()F

    move-result p1

    :goto_0
    new-instance v0, Lorg/apache/pdfbox/util/Vector;

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr p1, v1

    iget-object v1, p0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFont;->dw2:[F

    const/4 v2, 0x0

    aget v1, v1, v2

    invoke-direct {v0, p1, v1}, Lorg/apache/pdfbox/util/Vector;-><init>(FF)V

    return-object v0
.end method

.method private getDefaultWidth()F
    .locals 2

    iget v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFont;->defaultWidth:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFont;->dict:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->DW:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSNumber;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSNumber;->floatValue()F

    move-result v0

    goto :goto_0

    :cond_0
    const/high16 v0, 0x447a0000    # 1000.0f

    :goto_0
    iput v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFont;->defaultWidth:F

    :cond_1
    iget v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFont;->defaultWidth:F

    return v0
.end method

.method private readVerticalDisplacements()V
    .locals 14

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFont;->dict:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->DW2:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSArray;

    const/4 v1, 0x2

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-eqz v0, :cond_0

    new-array v1, v1, [F

    iput-object v1, p0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFont;->dw2:[F

    invoke-virtual {v0, v2}, Lorg/apache/pdfbox/cos/COSArray;->get(I)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v4

    check-cast v4, Lorg/apache/pdfbox/cos/COSNumber;

    invoke-virtual {v4}, Lorg/apache/pdfbox/cos/COSNumber;->floatValue()F

    move-result v4

    aput v4, v1, v2

    iget-object v1, p0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFont;->dw2:[F

    invoke-virtual {v0, v3}, Lorg/apache/pdfbox/cos/COSArray;->get(I)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSNumber;

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSNumber;->floatValue()F

    move-result v0

    aput v0, v1, v3

    goto :goto_0

    :cond_0
    new-array v0, v1, [F

    fill-array-data v0, :array_0

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFont;->dw2:[F

    :goto_0
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFont;->dict:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->W2:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSArray;

    if-eqz v0, :cond_3

    move v1, v2

    :goto_1
    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSArray;->size()I

    move-result v4

    if-ge v1, v4, :cond_3

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSArray;->get(I)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v4

    check-cast v4, Lorg/apache/pdfbox/cos/COSNumber;

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSArray;->get(I)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v5

    instance-of v6, v5, Lorg/apache/pdfbox/cos/COSArray;

    if-eqz v6, :cond_1

    check-cast v5, Lorg/apache/pdfbox/cos/COSArray;

    move v6, v2

    :goto_2
    invoke-virtual {v5}, Lorg/apache/pdfbox/cos/COSArray;->size()I

    move-result v7

    if-ge v6, v7, :cond_2

    invoke-virtual {v4}, Lorg/apache/pdfbox/cos/COSNumber;->intValue()I

    move-result v7

    add-int/2addr v7, v6

    invoke-virtual {v5, v6}, Lorg/apache/pdfbox/cos/COSArray;->get(I)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v8

    check-cast v8, Lorg/apache/pdfbox/cos/COSNumber;

    add-int/lit8 v6, v6, 0x1

    invoke-virtual {v5, v6}, Lorg/apache/pdfbox/cos/COSArray;->get(I)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v9

    check-cast v9, Lorg/apache/pdfbox/cos/COSNumber;

    add-int/2addr v6, v3

    invoke-virtual {v5, v6}, Lorg/apache/pdfbox/cos/COSArray;->get(I)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v10

    check-cast v10, Lorg/apache/pdfbox/cos/COSNumber;

    iget-object v11, p0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFont;->verticalDisplacementY:Ljava/util/Map;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v8}, Lorg/apache/pdfbox/cos/COSNumber;->floatValue()F

    move-result v8

    invoke-static {v8}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v8

    invoke-interface {v11, v12, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v8, p0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFont;->positionVectors:Ljava/util/Map;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    new-instance v11, Lorg/apache/pdfbox/util/Vector;

    invoke-virtual {v9}, Lorg/apache/pdfbox/cos/COSNumber;->floatValue()F

    move-result v9

    invoke-virtual {v10}, Lorg/apache/pdfbox/cos/COSNumber;->floatValue()F

    move-result v10

    invoke-direct {v11, v9, v10}, Lorg/apache/pdfbox/util/Vector;-><init>(FF)V

    invoke-interface {v8, v7, v11}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/2addr v6, v3

    goto :goto_2

    :cond_1
    invoke-virtual {v4}, Lorg/apache/pdfbox/cos/COSNumber;->intValue()I

    move-result v4

    check-cast v5, Lorg/apache/pdfbox/cos/COSNumber;

    invoke-virtual {v5}, Lorg/apache/pdfbox/cos/COSNumber;->intValue()I

    move-result v5

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSArray;->get(I)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v6

    check-cast v6, Lorg/apache/pdfbox/cos/COSNumber;

    add-int/2addr v1, v3

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSArray;->get(I)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v7

    check-cast v7, Lorg/apache/pdfbox/cos/COSNumber;

    add-int/2addr v1, v3

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSArray;->get(I)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v8

    check-cast v8, Lorg/apache/pdfbox/cos/COSNumber;

    :goto_3
    if-gt v4, v5, :cond_2

    iget-object v9, p0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFont;->verticalDisplacementY:Ljava/util/Map;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v6}, Lorg/apache/pdfbox/cos/COSNumber;->floatValue()F

    move-result v11

    invoke-static {v11}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v11

    invoke-interface {v9, v10, v11}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v9, p0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFont;->positionVectors:Ljava/util/Map;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    new-instance v11, Lorg/apache/pdfbox/util/Vector;

    invoke-virtual {v7}, Lorg/apache/pdfbox/cos/COSNumber;->floatValue()F

    move-result v12

    invoke-virtual {v8}, Lorg/apache/pdfbox/cos/COSNumber;->floatValue()F

    move-result v13

    invoke-direct {v11, v12, v13}, Lorg/apache/pdfbox/util/Vector;-><init>(FF)V

    invoke-interface {v9, v10, v11}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    :cond_2
    add-int/2addr v1, v3

    goto/16 :goto_1

    :cond_3
    return-void

    :array_0
    .array-data 4
        0x445c0000    # 880.0f
        -0x3b860000    # -1000.0f
    .end array-data
.end method

.method private readWidths()V
    .locals 11

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFont;->widths:Ljava/util/Map;

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFont;->dict:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->W:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSArray;

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSArray;->size()I

    move-result v1

    const/4 v2, 0x0

    move v3, v2

    :goto_0
    if-ge v3, v1, :cond_3

    add-int/lit8 v4, v3, 0x1

    invoke-virtual {v0, v3}, Lorg/apache/pdfbox/cos/COSArray;->getObject(I)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v3

    check-cast v3, Lorg/apache/pdfbox/cos/COSNumber;

    add-int/lit8 v5, v4, 0x1

    invoke-virtual {v0, v4}, Lorg/apache/pdfbox/cos/COSArray;->getObject(I)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v4

    instance-of v6, v4, Lorg/apache/pdfbox/cos/COSArray;

    if-eqz v6, :cond_1

    check-cast v4, Lorg/apache/pdfbox/cos/COSArray;

    invoke-virtual {v3}, Lorg/apache/pdfbox/cos/COSNumber;->intValue()I

    move-result v3

    invoke-virtual {v4}, Lorg/apache/pdfbox/cos/COSArray;->size()I

    move-result v6

    move v7, v2

    :goto_1
    if-ge v7, v6, :cond_0

    invoke-virtual {v4, v7}, Lorg/apache/pdfbox/cos/COSArray;->get(I)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v8

    check-cast v8, Lorg/apache/pdfbox/cos/COSNumber;

    iget-object v9, p0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFont;->widths:Ljava/util/Map;

    add-int v10, v3, v7

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v8}, Lorg/apache/pdfbox/cos/COSNumber;->floatValue()F

    move-result v8

    invoke-static {v8}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v8

    invoke-interface {v9, v10, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    :cond_0
    move v3, v5

    goto :goto_0

    :cond_1
    check-cast v4, Lorg/apache/pdfbox/cos/COSNumber;

    add-int/lit8 v6, v5, 0x1

    invoke-virtual {v0, v5}, Lorg/apache/pdfbox/cos/COSArray;->getObject(I)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v5

    check-cast v5, Lorg/apache/pdfbox/cos/COSNumber;

    invoke-virtual {v3}, Lorg/apache/pdfbox/cos/COSNumber;->intValue()I

    move-result v3

    invoke-virtual {v4}, Lorg/apache/pdfbox/cos/COSNumber;->intValue()I

    move-result v4

    invoke-virtual {v5}, Lorg/apache/pdfbox/cos/COSNumber;->floatValue()F

    move-result v5

    :goto_2
    if-gt v3, v4, :cond_2

    iget-object v7, p0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFont;->widths:Ljava/util/Map;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v9

    invoke-interface {v7, v8, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_2
    move v3, v6

    goto :goto_0

    :cond_3
    return-void
.end method


# virtual methods
.method public abstract codeToCID(I)I
.end method

.method public abstract codeToGID(I)I
.end method

.method public abstract encode(I)[B
.end method

.method public getAverageFontWidth()F
    .locals 10

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFont;->dict:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->W:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSArray;

    const/4 v1, 0x0

    if-eqz v0, :cond_2

    const/4 v2, 0x0

    move v4, v1

    move v5, v4

    move v3, v2

    :goto_0
    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSArray;->size()I

    move-result v6

    if-ge v3, v6, :cond_3

    add-int/lit8 v6, v3, 0x1

    invoke-virtual {v0, v3}, Lorg/apache/pdfbox/cos/COSArray;->getObject(I)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v3

    check-cast v3, Lorg/apache/pdfbox/cos/COSNumber;

    invoke-virtual {v0, v6}, Lorg/apache/pdfbox/cos/COSArray;->getObject(I)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v3

    instance-of v7, v3, Lorg/apache/pdfbox/cos/COSArray;

    const/high16 v8, 0x3f800000    # 1.0f

    if-eqz v7, :cond_0

    check-cast v3, Lorg/apache/pdfbox/cos/COSArray;

    move v7, v2

    :goto_1
    invoke-virtual {v3}, Lorg/apache/pdfbox/cos/COSArray;->size()I

    move-result v9

    if-ge v7, v9, :cond_1

    invoke-virtual {v3, v7}, Lorg/apache/pdfbox/cos/COSArray;->get(I)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v9

    check-cast v9, Lorg/apache/pdfbox/cos/COSNumber;

    invoke-virtual {v9}, Lorg/apache/pdfbox/cos/COSNumber;->floatValue()F

    move-result v9

    add-float/2addr v4, v9

    add-float/2addr v5, v8

    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    :cond_0
    add-int/lit8 v6, v6, 0x1

    invoke-virtual {v0, v6}, Lorg/apache/pdfbox/cos/COSArray;->getObject(I)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v3

    check-cast v3, Lorg/apache/pdfbox/cos/COSNumber;

    invoke-virtual {v3}, Lorg/apache/pdfbox/cos/COSNumber;->floatValue()F

    move-result v7

    cmpl-float v7, v7, v1

    if-lez v7, :cond_1

    invoke-virtual {v3}, Lorg/apache/pdfbox/cos/COSNumber;->floatValue()F

    move-result v3

    add-float/2addr v4, v3

    add-float/2addr v5, v8

    :cond_1
    add-int/lit8 v3, v6, 0x1

    goto :goto_0

    :cond_2
    move v4, v1

    move v5, v4

    :cond_3
    div-float/2addr v4, v5

    cmpg-float v0, v4, v1

    if-gtz v0, :cond_4

    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/font/PDCIDFont;->getDefaultWidth()F

    move-result v4

    :cond_4
    return v4
.end method

.method public getBaseFont()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFont;->dict:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->BASE_FONT:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getNameAsString(Lorg/apache/pdfbox/cos/COSName;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public abstract getBoundingBox()Lorg/apache/fontbox/util/BoundingBox;
.end method

.method public bridge synthetic getCOSObject()Lorg/apache/pdfbox/cos/COSBase;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDCIDFont;->getCOSObject()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    return-object v0
.end method

.method public getCOSObject()Lorg/apache/pdfbox/cos/COSDictionary;
    .locals 1

    .line 2
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFont;->dict:Lorg/apache/pdfbox/cos/COSDictionary;

    return-object v0
.end method

.method public getFontDescriptor()Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFont;->fontDescriptor:Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFont;->dict:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->FONT_DESC:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSDictionary;

    if-eqz v0, :cond_0

    new-instance v1, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;

    invoke-direct {v1, v0}, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    iput-object v1, p0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFont;->fontDescriptor:Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;

    :cond_0
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFont;->fontDescriptor:Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;

    return-object v0
.end method

.method public abstract getFontMatrix()Lorg/apache/pdfbox/util/Matrix;
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDCIDFont;->getBaseFont()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getParent()Lorg/apache/pdfbox/pdmodel/font/PDType0Font;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFont;->parent:Lorg/apache/pdfbox/pdmodel/font/PDType0Font;

    return-object v0
.end method

.method public getPositionVector(I)Lorg/apache/pdfbox/util/Vector;
    .locals 2

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/pdmodel/font/PDCIDFont;->codeToCID(I)I

    move-result p1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFont;->positionVectors:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/util/Vector;

    if-eqz v0, :cond_0

    return-object v0

    :cond_0
    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/font/PDCIDFont;->getDefaultPositionVector(I)Lorg/apache/pdfbox/util/Vector;

    move-result-object p1

    return-object p1
.end method

.method public getVerticalDisplacementVectorY(I)F
    .locals 1

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/pdmodel/font/PDCIDFont;->codeToCID(I)I

    move-result p1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFont;->verticalDisplacementY:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Float;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/Float;->floatValue()F

    move-result p1

    return p1

    :cond_0
    iget-object p1, p0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFont;->dw2:[F

    const/4 v0, 0x1

    aget p1, p1, v0

    return p1
.end method

.method public getWidth(I)F
    .locals 3

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/pdmodel/font/PDCIDFont;->codeToCID(I)I

    move-result v0

    iget-object v1, p0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFont;->widths:Ljava/util/Map;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object p1, p0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFont;->widths:Ljava/util/Map;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Float;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/Float;->floatValue()F

    move-result p1

    return p1

    :cond_0
    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/font/PDCIDFont;->getDefaultWidth()F

    move-result p1

    return p1

    :cond_1
    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/pdmodel/font/PDCIDFont;->getWidthFromFont(I)F

    move-result p1

    return p1
.end method

.method public abstract getWidthFromFont(I)F
.end method

.method public abstract isEmbedded()Z
.end method

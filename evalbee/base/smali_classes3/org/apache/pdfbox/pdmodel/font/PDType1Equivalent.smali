.class public interface abstract Lorg/apache/pdfbox/pdmodel/font/PDType1Equivalent;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/pdfbox/pdmodel/font/PDFontLike;


# virtual methods
.method public abstract codeToName(I)Ljava/lang/String;
.end method

.method public abstract getName()Ljava/lang/String;
.end method

.method public abstract getPath(Ljava/lang/String;)Landroid/graphics/Path;
.end method

.method public abstract getType1Equivalent()Lorg/apache/fontbox/ttf/Type1Equivalent;
.end method

.class public Lorg/apache/pdfbox/pdmodel/font/PDType0Font;
.super Lorg/apache/pdfbox/pdmodel/font/PDFont;
.source "SourceFile"


# instance fields
.field private cMap:Lorg/apache/fontbox/cmap/CMap;

.field private cMapUCS2:Lorg/apache/fontbox/cmap/CMap;

.field private final descendantFont:Lorg/apache/pdfbox/pdmodel/font/PDCIDFont;

.field private embedder:Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType2Embedder;

.field private isCMapPredefined:Z


# direct methods
.method public constructor <init>(Lorg/apache/pdfbox/cos/COSDictionary;)V
    .locals 1

    .line 1
    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/font/PDFont;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    iget-object p1, p0, Lorg/apache/pdfbox/pdmodel/font/PDFont;->dict:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->DESCENDANT_FONTS:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p1, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object p1

    check-cast p1, Lorg/apache/pdfbox/cos/COSArray;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lorg/apache/pdfbox/cos/COSArray;->getObject(I)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object p1

    check-cast p1, Lorg/apache/pdfbox/cos/COSDictionary;

    if-eqz p1, :cond_0

    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/font/PDType0Font;->readEncoding()V

    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/font/PDType0Font;->fetchCMapUCS2()V

    invoke-static {p1, p0}, Lorg/apache/pdfbox/pdmodel/font/PDFontFactory;->createDescendantFont(Lorg/apache/pdfbox/cos/COSDictionary;Lorg/apache/pdfbox/pdmodel/font/PDType0Font;)Lorg/apache/pdfbox/pdmodel/font/PDCIDFont;

    move-result-object p1

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/font/PDType0Font;->descendantFont:Lorg/apache/pdfbox/pdmodel/font/PDCIDFont;

    return-void

    :cond_0
    new-instance p1, Ljava/io/IOException;

    const-string v0, "Missing descendant font dictionary"

    invoke-direct {p1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private constructor <init>(Lorg/apache/pdfbox/pdmodel/PDDocument;Ljava/io/InputStream;Z)V
    .locals 7

    .line 2
    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/font/PDFont;-><init>()V

    new-instance v6, Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType2Embedder;

    iget-object v2, p0, Lorg/apache/pdfbox/pdmodel/font/PDFont;->dict:Lorg/apache/pdfbox/cos/COSDictionary;

    move-object v0, v6

    move-object v1, p1

    move-object v3, p2

    move v4, p3

    move-object v5, p0

    invoke-direct/range {v0 .. v5}, Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType2Embedder;-><init>(Lorg/apache/pdfbox/pdmodel/PDDocument;Lorg/apache/pdfbox/cos/COSDictionary;Ljava/io/InputStream;ZLorg/apache/pdfbox/pdmodel/font/PDType0Font;)V

    iput-object v6, p0, Lorg/apache/pdfbox/pdmodel/font/PDType0Font;->embedder:Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType2Embedder;

    invoke-virtual {v6}, Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType2Embedder;->getCIDFont()Lorg/apache/pdfbox/pdmodel/font/PDCIDFont;

    move-result-object p1

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/font/PDType0Font;->descendantFont:Lorg/apache/pdfbox/pdmodel/font/PDCIDFont;

    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/font/PDType0Font;->readEncoding()V

    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/font/PDType0Font;->fetchCMapUCS2()V

    return-void
.end method

.method private fetchCMapUCS2()V
    .locals 3

    iget-boolean v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDType0Font;->isCMapPredefined:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDFont;->dict:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->ENCODING:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    instance-of v1, v0, Lorg/apache/pdfbox/cos/COSName;

    if-eqz v1, :cond_0

    check-cast v0, Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSName;->getName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_1

    const-string v1, "Identity-H"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "Identity-V"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-static {v0}, Lorg/apache/pdfbox/pdmodel/font/CMapManager;->getPredefinedCMap(Ljava/lang/String;)Lorg/apache/fontbox/cmap/CMap;

    move-result-object v0

    if-eqz v0, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Lorg/apache/fontbox/cmap/CMap;->getRegistry()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "-"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lorg/apache/fontbox/cmap/CMap;->getOrdering()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "-UCS2"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/pdfbox/pdmodel/font/CMapManager;->getPredefinedCMap(Ljava/lang/String;)Lorg/apache/fontbox/cmap/CMap;

    move-result-object v0

    if-eqz v0, :cond_1

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDType0Font;->cMapUCS2:Lorg/apache/fontbox/cmap/CMap;

    :cond_1
    return-void
.end method

.method public static load(Lorg/apache/pdfbox/pdmodel/PDDocument;Ljava/io/File;)Lorg/apache/pdfbox/pdmodel/font/PDType0Font;
    .locals 2

    .line 1
    new-instance v0, Lorg/apache/pdfbox/pdmodel/font/PDType0Font;

    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    const/4 p1, 0x1

    invoke-direct {v0, p0, v1, p1}, Lorg/apache/pdfbox/pdmodel/font/PDType0Font;-><init>(Lorg/apache/pdfbox/pdmodel/PDDocument;Ljava/io/InputStream;Z)V

    return-object v0
.end method

.method public static load(Lorg/apache/pdfbox/pdmodel/PDDocument;Ljava/io/InputStream;)Lorg/apache/pdfbox/pdmodel/font/PDType0Font;
    .locals 2

    .line 2
    new-instance v0, Lorg/apache/pdfbox/pdmodel/font/PDType0Font;

    const/4 v1, 0x1

    invoke-direct {v0, p0, p1, v1}, Lorg/apache/pdfbox/pdmodel/font/PDType0Font;-><init>(Lorg/apache/pdfbox/pdmodel/PDDocument;Ljava/io/InputStream;Z)V

    return-object v0
.end method

.method public static load(Lorg/apache/pdfbox/pdmodel/PDDocument;Ljava/io/InputStream;Z)Lorg/apache/pdfbox/pdmodel/font/PDType0Font;
    .locals 1

    .line 3
    new-instance v0, Lorg/apache/pdfbox/pdmodel/font/PDType0Font;

    invoke-direct {v0, p0, p1, p2}, Lorg/apache/pdfbox/pdmodel/font/PDType0Font;-><init>(Lorg/apache/pdfbox/pdmodel/PDDocument;Ljava/io/InputStream;Z)V

    return-object v0
.end method

.method private readEncoding()V
    .locals 3

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDFont;->dict:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->ENCODING:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    instance-of v1, v0, Lorg/apache/pdfbox/cos/COSName;

    const-string v2, "Missing required CMap"

    if-eqz v1, :cond_1

    check-cast v0, Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSName;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/pdfbox/pdmodel/font/CMapManager;->getPredefinedCMap(Ljava/lang/String;)Lorg/apache/fontbox/cmap/CMap;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDType0Font;->cMap:Lorg/apache/fontbox/cmap/CMap;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDType0Font;->isCMapPredefined:Z

    goto :goto_0

    :cond_0
    new-instance v0, Ljava/io/IOException;

    invoke-direct {v0, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    if-eqz v0, :cond_3

    invoke-virtual {p0, v0}, Lorg/apache/pdfbox/pdmodel/font/PDFont;->readCMap(Lorg/apache/pdfbox/cos/COSBase;)Lorg/apache/fontbox/cmap/CMap;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDType0Font;->cMap:Lorg/apache/fontbox/cmap/CMap;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lorg/apache/fontbox/cmap/CMap;->hasCIDMappings()Z

    move-result v0

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Invalid Encoding CMap in font "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDType0Font;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "PdfBoxAndroid"

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    new-instance v0, Ljava/io/IOException;

    invoke-direct {v0, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    :goto_0
    return-void
.end method


# virtual methods
.method public addToSubset(I)V
    .locals 1

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDType0Font;->willBeSubset()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDType0Font;->embedder:Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType2Embedder;

    invoke-virtual {v0, p1}, Lorg/apache/pdfbox/pdmodel/font/TrueTypeEmbedder;->addToSubset(I)V

    return-void

    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "This font was created with subsetting disabled"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public codeToCID(I)I
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDType0Font;->descendantFont:Lorg/apache/pdfbox/pdmodel/font/PDCIDFont;

    invoke-virtual {v0, p1}, Lorg/apache/pdfbox/pdmodel/font/PDCIDFont;->codeToCID(I)I

    move-result p1

    return p1
.end method

.method public codeToGID(I)I
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDType0Font;->descendantFont:Lorg/apache/pdfbox/pdmodel/font/PDCIDFont;

    invoke-virtual {v0, p1}, Lorg/apache/pdfbox/pdmodel/font/PDCIDFont;->codeToGID(I)I

    move-result p1

    return p1
.end method

.method public encode(I)[B
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDType0Font;->descendantFont:Lorg/apache/pdfbox/pdmodel/font/PDCIDFont;

    invoke-virtual {v0, p1}, Lorg/apache/pdfbox/pdmodel/font/PDCIDFont;->encode(I)[B

    move-result-object p1

    return-object p1
.end method

.method public getAverageFontWidth()F
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDType0Font;->descendantFont:Lorg/apache/pdfbox/pdmodel/font/PDCIDFont;

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/font/PDCIDFont;->getAverageFontWidth()F

    move-result v0

    return v0
.end method

.method public getBaseFont()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDFont;->dict:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->BASE_FONT:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getNameAsString(Lorg/apache/pdfbox/cos/COSName;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getBoundingBox()Lorg/apache/fontbox/util/BoundingBox;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDType0Font;->descendantFont:Lorg/apache/pdfbox/pdmodel/font/PDCIDFont;

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/font/PDCIDFont;->getBoundingBox()Lorg/apache/fontbox/util/BoundingBox;

    move-result-object v0

    return-object v0
.end method

.method public getCMap()Lorg/apache/fontbox/cmap/CMap;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDType0Font;->cMap:Lorg/apache/fontbox/cmap/CMap;

    return-object v0
.end method

.method public getCMapUCS2()Lorg/apache/fontbox/cmap/CMap;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDType0Font;->cMapUCS2:Lorg/apache/fontbox/cmap/CMap;

    return-object v0
.end method

.method public getDescendantFont()Lorg/apache/pdfbox/pdmodel/font/PDCIDFont;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDType0Font;->descendantFont:Lorg/apache/pdfbox/pdmodel/font/PDCIDFont;

    return-object v0
.end method

.method public getDisplacement(I)Lorg/apache/pdfbox/util/Vector;
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDType0Font;->isVertical()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lorg/apache/pdfbox/util/Vector;

    iget-object v1, p0, Lorg/apache/pdfbox/pdmodel/font/PDType0Font;->descendantFont:Lorg/apache/pdfbox/pdmodel/font/PDCIDFont;

    invoke-virtual {v1, p1}, Lorg/apache/pdfbox/pdmodel/font/PDCIDFont;->getVerticalDisplacementVectorY(I)F

    move-result p1

    const/high16 v1, 0x447a0000    # 1000.0f

    div-float/2addr p1, v1

    const/4 v1, 0x0

    invoke-direct {v0, v1, p1}, Lorg/apache/pdfbox/util/Vector;-><init>(FF)V

    return-object v0

    :cond_0
    invoke-super {p0, p1}, Lorg/apache/pdfbox/pdmodel/font/PDFont;->getDisplacement(I)Lorg/apache/pdfbox/util/Vector;

    move-result-object p1

    return-object p1
.end method

.method public getFontDescriptor()Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDType0Font;->descendantFont:Lorg/apache/pdfbox/pdmodel/font/PDCIDFont;

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/font/PDCIDFont;->getFontDescriptor()Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;

    move-result-object v0

    return-object v0
.end method

.method public getFontMatrix()Lorg/apache/pdfbox/util/Matrix;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDType0Font;->descendantFont:Lorg/apache/pdfbox/pdmodel/font/PDCIDFont;

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/font/PDCIDFont;->getFontMatrix()Lorg/apache/pdfbox/util/Matrix;

    move-result-object v0

    return-object v0
.end method

.method public getHeight(I)F
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDType0Font;->descendantFont:Lorg/apache/pdfbox/pdmodel/font/PDCIDFont;

    invoke-interface {v0, p1}, Lorg/apache/pdfbox/pdmodel/font/PDFontLike;->getHeight(I)F

    move-result p1

    return p1
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDType0Font;->getBaseFont()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPositionVector(I)Lorg/apache/pdfbox/util/Vector;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDType0Font;->descendantFont:Lorg/apache/pdfbox/pdmodel/font/PDCIDFont;

    invoke-virtual {v0, p1}, Lorg/apache/pdfbox/pdmodel/font/PDCIDFont;->getPositionVector(I)Lorg/apache/pdfbox/util/Vector;

    move-result-object p1

    const v0, -0x457ced91    # -0.001f

    invoke-virtual {p1, v0}, Lorg/apache/pdfbox/util/Vector;->scale(F)Lorg/apache/pdfbox/util/Vector;

    move-result-object p1

    return-object p1
.end method

.method public getWidth(I)F
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDType0Font;->descendantFont:Lorg/apache/pdfbox/pdmodel/font/PDCIDFont;

    invoke-virtual {v0, p1}, Lorg/apache/pdfbox/pdmodel/font/PDCIDFont;->getWidth(I)F

    move-result p1

    return p1
.end method

.method public getWidthFromFont(I)F
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDType0Font;->descendantFont:Lorg/apache/pdfbox/pdmodel/font/PDCIDFont;

    invoke-virtual {v0, p1}, Lorg/apache/pdfbox/pdmodel/font/PDCIDFont;->getWidthFromFont(I)F

    move-result p1

    return p1
.end method

.method public isDamaged()Z
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDType0Font;->descendantFont:Lorg/apache/pdfbox/pdmodel/font/PDCIDFont;

    invoke-interface {v0}, Lorg/apache/pdfbox/pdmodel/font/PDFontLike;->isDamaged()Z

    move-result v0

    return v0
.end method

.method public isEmbedded()Z
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDType0Font;->descendantFont:Lorg/apache/pdfbox/pdmodel/font/PDCIDFont;

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/font/PDCIDFont;->isEmbedded()Z

    move-result v0

    return v0
.end method

.method public isStandard14()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isVertical()Z
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDType0Font;->cMap:Lorg/apache/fontbox/cmap/CMap;

    invoke-virtual {v0}, Lorg/apache/fontbox/cmap/CMap;->getWMode()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method public readCode(Ljava/io/InputStream;)I
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDType0Font;->cMap:Lorg/apache/fontbox/cmap/CMap;

    invoke-virtual {v0, p1}, Lorg/apache/fontbox/cmap/CMap;->readCode(Ljava/io/InputStream;)I

    move-result p1

    return p1
.end method

.method public subset()V
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDType0Font;->willBeSubset()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDType0Font;->embedder:Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType2Embedder;

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/font/TrueTypeEmbedder;->subset()V

    return-void

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "This font was created with subsetting disabled"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDType0Font;->getDescendantFont()Lorg/apache/pdfbox/pdmodel/font/PDCIDFont;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDType0Font;->getDescendantFont()Lorg/apache/pdfbox/pdmodel/font/PDCIDFont;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDType0Font;->getBaseFont()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public toUnicode(I)Ljava/lang/String;
    .locals 1

    invoke-super {p0, p1}, Lorg/apache/pdfbox/pdmodel/font/PDFont;->toUnicode(I)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    return-object v0

    :cond_0
    iget-boolean v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDType0Font;->isCMapPredefined:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDType0Font;->cMapUCS2:Lorg/apache/fontbox/cmap/CMap;

    if-eqz v0, :cond_1

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/pdmodel/font/PDType0Font;->codeToCID(I)I

    move-result p1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDType0Font;->cMapUCS2:Lorg/apache/fontbox/cmap/CMap;

    invoke-virtual {v0, p1}, Lorg/apache/fontbox/cmap/CMap;->toUnicode(I)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_1
    const/4 p1, 0x0

    return-object p1
.end method

.method public willBeSubset()Z
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDType0Font;->embedder:Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType2Embedder;

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/font/TrueTypeEmbedder;->needsSubset()Z

    move-result v0

    return v0
.end method

.class public final Lorg/apache/pdfbox/pdmodel/font/ExternalFonts;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/pdfbox/pdmodel/font/ExternalFonts$DefaultFontProvider;
    }
.end annotation


# static fields
.field private static final cidFallbackFont:Lorg/apache/fontbox/cff/CFFCIDFont;

.field private static fontProvider:Lorg/apache/pdfbox/pdmodel/font/FontProvider;

.field private static final substitutes:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private static final ttfFallbackFont:Lorg/apache/fontbox/ttf/TrueTypeFont;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    :try_start_0
    const-string v0, "org/apache/pdfbox/resources/ttf/LiberationSans-Regular.ttf"

    invoke-static {}, Lorg/apache/pdfbox/util/PDFBoxResourceLoader;->isReady()Z

    move-result v1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    const-class v2, Lorg/apache/pdfbox/pdmodel/font/ExternalFonts;

    const-string v3, "Error loading resource: "

    if-eqz v1, :cond_1

    :try_start_1
    invoke-static {v0}, Lorg/apache/pdfbox/util/PDFBoxResourceLoader;->getStream(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    new-instance v0, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/ClassLoader;->getResource(Ljava/lang/String;)Ljava/net/URL;

    move-result-object v1

    if-eqz v1, :cond_7

    invoke-virtual {v1}, Ljava/net/URL;->openStream()Ljava/io/InputStream;

    move-result-object v1

    :goto_0
    new-instance v4, Lorg/apache/fontbox/ttf/TTFParser;

    invoke-direct {v4}, Lorg/apache/fontbox/ttf/TTFParser;-><init>()V

    invoke-virtual {v4, v1}, Lorg/apache/fontbox/ttf/TTFParser;->parse(Ljava/io/InputStream;)Lorg/apache/fontbox/ttf/TrueTypeFont;

    move-result-object v1

    sput-object v1, Lorg/apache/pdfbox/pdmodel/font/ExternalFonts;->ttfFallbackFont:Lorg/apache/fontbox/ttf/TrueTypeFont;

    const-string v1, "org/apache/pdfbox/resources/otf/AdobeBlank.otf"

    invoke-static {}, Lorg/apache/pdfbox/util/PDFBoxResourceLoader;->isReady()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-static {v1}, Lorg/apache/pdfbox/util/PDFBoxResourceLoader;->getStream(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v0

    if-eqz v0, :cond_2

    goto :goto_1

    :cond_2
    new-instance v0, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    invoke-virtual {v2}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/ClassLoader;->getResource(Ljava/lang/String;)Ljava/net/URL;

    move-result-object v1

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/net/URL;->openStream()Ljava/io/InputStream;

    move-result-object v0

    :goto_1
    invoke-static {v0}, Lorg/apache/pdfbox/io/IOUtils;->toByteArray(Ljava/io/InputStream;)[B

    move-result-object v0

    new-instance v1, Lorg/apache/fontbox/cff/CFFParser;

    invoke-direct {v1}, Lorg/apache/fontbox/cff/CFFParser;-><init>()V

    invoke-virtual {v1, v0}, Lorg/apache/fontbox/cff/CFFParser;->parse([B)Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/fontbox/cff/CFFCIDFont;

    sput-object v0, Lorg/apache/pdfbox/pdmodel/font/ExternalFonts;->cidFallbackFont:Lorg/apache/fontbox/cff/CFFCIDFont;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lorg/apache/pdfbox/pdmodel/font/ExternalFonts;->substitutes:Ljava/util/Map;

    const-string v1, "LiberationMono"

    const-string v2, "NimbusMonL-Regu"

    const-string v3, "CourierNew"

    const-string v4, "CourierNewPSMT"

    const-string v5, "DroidSansMono"

    filled-new-array {v3, v4, v1, v2, v5}, [Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    const-string v2, "Courier"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "LiberationMono-Bold"

    const-string v2, "NimbusMonL-Bold"

    const-string v3, "CourierNewPS-BoldMT"

    const-string v4, "CourierNew-Bold"

    filled-new-array {v3, v4, v1, v2, v5}, [Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    const-string v2, "Courier-Bold"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "LiberationMono-Italic"

    const-string v2, "NimbusMonL-ReguObli"

    const-string v3, "CourierNewPS-ItalicMT"

    const-string v4, "CourierNew-Italic"

    filled-new-array {v3, v4, v1, v2, v5}, [Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    const-string v2, "Courier-Oblique"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "LiberationMono-BoldItalic"

    const-string v2, "NimbusMonL-BoldObli"

    const-string v3, "CourierNewPS-BoldItalicMT"

    const-string v4, "CourierNew-BoldItalic"

    filled-new-array {v3, v4, v1, v2, v5}, [Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    const-string v2, "Courier-BoldOblique"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "NimbusSanL-Regu"

    const-string v2, "Roboto-Regular"

    const-string v3, "ArialMT"

    const-string v4, "Arial"

    const-string v5, "LiberationSans"

    filled-new-array {v3, v4, v5, v1, v2}, [Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    const-string v2, "Helvetica"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "NimbusSanL-Bold"

    const-string v2, "Roboto-Bold"

    const-string v3, "Arial-BoldMT"

    const-string v4, "Arial-Bold"

    const-string v5, "LiberationSans-Bold"

    filled-new-array {v3, v4, v5, v1, v2}, [Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    const-string v2, "Helvetica-Bold"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v3, "Arial-ItalicMT"

    const-string v4, "Arial-Italic"

    const-string v5, "Helvetica-Italic"

    const-string v6, "LiberationSans-Italic"

    const-string v7, "NimbusSanL-ReguItal"

    const-string v8, "Roboto-Italic"

    filled-new-array/range {v3 .. v8}, [Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    const-string v2, "Helvetica-Oblique"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "NimbusSanL-BoldItal"

    const-string v2, "Roboto-BoldItalic"

    const-string v3, "Arial-BoldItalicMT"

    const-string v4, "Helvetica-BoldItalic"

    const-string v5, "LiberationSans-BoldItalic"

    filled-new-array {v3, v4, v5, v1, v2}, [Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    const-string v2, "Helvetica-BoldOblique"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v3, "TimesNewRomanPSMT"

    const-string v4, "TimesNewRoman"

    const-string v5, "TimesNewRomanPS"

    const-string v6, "LiberationSerif"

    const-string v7, "NimbusRomNo9L-Regu"

    const-string v8, "DroidSerif-Regular"

    filled-new-array/range {v3 .. v8}, [Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    const-string v2, "Times-Roman"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v3, "TimesNewRomanPS-BoldMT"

    const-string v4, "TimesNewRomanPS-Bold"

    const-string v5, "TimesNewRoman-Bold"

    const-string v6, "LiberationSerif-Bold"

    const-string v7, "NimbusRomNo9L-Medi"

    const-string v8, "DroidSerif-Bold"

    filled-new-array/range {v3 .. v8}, [Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    const-string v2, "Times-Bold"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v3, "TimesNewRomanPS-ItalicMT"

    const-string v4, "TimesNewRomanPS-Italic"

    const-string v5, "TimesNewRoman-Italic"

    const-string v6, "LiberationSerif-Italic"

    const-string v7, "NimbusRomNo9L-ReguItal"

    const-string v8, "DroidSerif-Italic"

    filled-new-array/range {v3 .. v8}, [Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    const-string v2, "Times-Italic"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v3, "TimesNewRomanPS-BoldItalicMT"

    const-string v4, "TimesNewRomanPS-BoldItalic"

    const-string v5, "TimesNewRoman-BoldItalic"

    const-string v6, "LiberationSerif-BoldItalic"

    const-string v7, "NimbusRomNo9L-MediItal"

    const-string v8, "DroidSerif-BoldItalic"

    filled-new-array/range {v3 .. v8}, [Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    const-string v2, "Times-BoldItalic"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "SymbolMT"

    const-string v2, "StandardSymL"

    filled-new-array {v1, v2}, [Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    const-string v2, "Symbol"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "ZapfDingbatsITC"

    const-string v2, "Dingbats"

    filled-new-array {v1, v2}, [Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    const-string v2, "ZapfDingbats"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "AdobeMingStd-Light"

    filled-new-array {v1}, [Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    const-string v2, "$Adobe-CNS1"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "KozMinPr6N-Regular"

    filled-new-array {v1}, [Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    const-string v2, "$Adobe-Japan1"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "AdobeGothicStd-Bold"

    filled-new-array {v1}, [Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    const-string v2, "$Adobe-Korea1"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "AdobeHeitiStd-Regular"

    filled-new-array {v1}, [Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    const-string v2, "$Adobe-GB1"

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Lorg/apache/pdfbox/pdmodel/font/Standard14Fonts;->getNames()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_4
    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    sget-object v2, Lorg/apache/pdfbox/pdmodel/font/ExternalFonts;->substitutes:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    invoke-static {v1}, Lorg/apache/pdfbox/pdmodel/font/Standard14Fonts;->getMappedFontName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lorg/apache/pdfbox/pdmodel/font/ExternalFonts;->copySubstitutes(Ljava/lang/String;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v2, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    :cond_5
    return-void

    :cond_6
    :try_start_2
    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_7
    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static addSubstitute(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    sget-object v0, Lorg/apache/pdfbox/pdmodel/font/ExternalFonts;->substitutes:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v0, p0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/util/List;

    invoke-interface {p0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method private static copySubstitutes(Ljava/lang/String;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    sget-object v1, Lorg/apache/pdfbox/pdmodel/font/ExternalFonts;->substitutes:Ljava/util/Map;

    invoke-interface {v1, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/util/Collection;

    invoke-direct {v0, p0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method public static getCFFCIDFont(Ljava/lang/String;)Lorg/apache/fontbox/cff/CFFCIDFont;
    .locals 1

    invoke-static {p0}, Lorg/apache/pdfbox/pdmodel/font/ExternalFonts;->getCFFFont(Ljava/lang/String;)Lorg/apache/fontbox/cff/CFFFont;

    move-result-object p0

    instance-of v0, p0, Lorg/apache/fontbox/cff/CFFCIDFont;

    if-eqz v0, :cond_0

    check-cast p0, Lorg/apache/fontbox/cff/CFFCIDFont;

    return-object p0

    :cond_0
    const/4 p0, 0x0

    return-object p0
.end method

.method public static getCFFCIDFontFallback(Ljava/lang/String;Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;)Lorg/apache/fontbox/cff/CFFCIDFont;
    .locals 1

    if-eqz p0, :cond_1

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "$"

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Lorg/apache/pdfbox/pdmodel/font/ExternalFonts;->getSubstitutes(Ljava/lang/String;)Ljava/util/List;

    move-result-object p0

    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :cond_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result p1

    if-eqz p1, :cond_1

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    invoke-static {}, Lorg/apache/pdfbox/pdmodel/font/ExternalFonts;->getProvider()Lorg/apache/pdfbox/pdmodel/font/FontProvider;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/pdfbox/pdmodel/font/FontProvider;->getCFFFont(Ljava/lang/String;)Lorg/apache/fontbox/cff/CFFFont;

    move-result-object p1

    instance-of v0, p1, Lorg/apache/fontbox/cff/CFFCIDFont;

    if-eqz v0, :cond_0

    check-cast p1, Lorg/apache/fontbox/cff/CFFCIDFont;

    return-object p1

    :cond_1
    sget-object p0, Lorg/apache/pdfbox/pdmodel/font/ExternalFonts;->cidFallbackFont:Lorg/apache/fontbox/cff/CFFCIDFont;

    return-object p0
.end method

.method private static getCFFFont(Ljava/lang/String;)Lorg/apache/fontbox/cff/CFFFont;
    .locals 3

    invoke-static {}, Lorg/apache/pdfbox/pdmodel/font/ExternalFonts;->getProvider()Lorg/apache/pdfbox/pdmodel/font/FontProvider;

    move-result-object v0

    invoke-virtual {v0, p0}, Lorg/apache/pdfbox/pdmodel/font/FontProvider;->getCFFFont(Ljava/lang/String;)Lorg/apache/fontbox/cff/CFFFont;

    move-result-object v0

    if-nez v0, :cond_2

    invoke-static {p0}, Lorg/apache/pdfbox/pdmodel/font/ExternalFonts;->getSubstitutes(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {}, Lorg/apache/pdfbox/pdmodel/font/ExternalFonts;->getProvider()Lorg/apache/pdfbox/pdmodel/font/FontProvider;

    move-result-object v2

    invoke-virtual {v2, v1}, Lorg/apache/pdfbox/pdmodel/font/FontProvider;->getCFFFont(Ljava/lang/String;)Lorg/apache/fontbox/cff/CFFFont;

    move-result-object v1

    if-eqz v1, :cond_0

    return-object v1

    :cond_1
    invoke-static {}, Lorg/apache/pdfbox/pdmodel/font/ExternalFonts;->getProvider()Lorg/apache/pdfbox/pdmodel/font/FontProvider;

    move-result-object v0

    invoke-static {p0}, Lorg/apache/pdfbox/pdmodel/font/ExternalFonts;->windowsToPs(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, p0}, Lorg/apache/pdfbox/pdmodel/font/FontProvider;->getCFFFont(Ljava/lang/String;)Lorg/apache/fontbox/cff/CFFFont;

    move-result-object v0

    :cond_2
    return-object v0
.end method

.method public static getCFFType1Font(Ljava/lang/String;)Lorg/apache/fontbox/cff/CFFType1Font;
    .locals 1

    invoke-static {p0}, Lorg/apache/pdfbox/pdmodel/font/ExternalFonts;->getCFFFont(Ljava/lang/String;)Lorg/apache/fontbox/cff/CFFFont;

    move-result-object p0

    instance-of v0, p0, Lorg/apache/fontbox/cff/CFFType1Font;

    if-eqz v0, :cond_0

    check-cast p0, Lorg/apache/fontbox/cff/CFFType1Font;

    return-object p0

    :cond_0
    const/4 p0, 0x0

    return-object p0
.end method

.method private static getFallbackFontName(Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;)Ljava/lang/String;
    .locals 6

    if-eqz p0, :cond_b

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->getFontName()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->getFontName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v2, "bold"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "black"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "heavy"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    move v1, v0

    :cond_1
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->isFixedPitch()Z

    move-result v0

    const-string v2, "-Oblique"

    const-string v3, "-BoldOblique"

    const-string v4, "-Bold"

    if-eqz v0, :cond_4

    const-string v0, "Courier"

    if-eqz v1, :cond_2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->isItalic()Z

    move-result v5

    if-eqz v5, :cond_2

    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    :goto_0
    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    :cond_2
    if-eqz v1, :cond_3

    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    :goto_1
    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_2
    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_5

    :cond_3
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->isItalic()Z

    move-result p0

    if-eqz p0, :cond_c

    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    :goto_3
    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    :cond_4
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->isSerif()Z

    move-result v0

    if-eqz v0, :cond_8

    const-string v0, "Times"

    if-eqz v1, :cond_5

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->isItalic()Z

    move-result v2

    if-eqz v2, :cond_5

    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "-BoldItalic"

    :goto_4
    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    :cond_5
    if-eqz v1, :cond_6

    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    goto :goto_1

    :cond_6
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->isItalic()Z

    move-result p0

    if-eqz p0, :cond_7

    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "-Italic"

    goto :goto_4

    :cond_7
    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "-Roman"

    goto :goto_4

    :cond_8
    const-string v0, "Helvetica"

    if-eqz v1, :cond_9

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->isItalic()Z

    move-result v5

    if-eqz v5, :cond_9

    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    goto :goto_0

    :cond_9
    if-eqz v1, :cond_a

    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    goto :goto_1

    :cond_a
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->isItalic()Z

    move-result p0

    if-eqz p0, :cond_c

    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    goto :goto_3

    :cond_b
    const-string v0, "Times-Roman"

    :cond_c
    :goto_5
    return-object v0
.end method

.method public static getProvider()Lorg/apache/pdfbox/pdmodel/font/FontProvider;
    .locals 1

    sget-object v0, Lorg/apache/pdfbox/pdmodel/font/ExternalFonts;->fontProvider:Lorg/apache/pdfbox/pdmodel/font/FontProvider;

    if-nez v0, :cond_0

    invoke-static {}, Lorg/apache/pdfbox/pdmodel/font/ExternalFonts$DefaultFontProvider;->access$000()Lorg/apache/pdfbox/pdmodel/font/FontProvider;

    move-result-object v0

    sput-object v0, Lorg/apache/pdfbox/pdmodel/font/ExternalFonts;->fontProvider:Lorg/apache/pdfbox/pdmodel/font/FontProvider;

    :cond_0
    sget-object v0, Lorg/apache/pdfbox/pdmodel/font/ExternalFonts;->fontProvider:Lorg/apache/pdfbox/pdmodel/font/FontProvider;

    return-object v0
.end method

.method private static getSubstitutes(Ljava/lang/String;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    sget-object v0, Lorg/apache/pdfbox/pdmodel/font/ExternalFonts;->substitutes:Ljava/util/Map;

    const-string v1, " "

    const-string v2, ""

    invoke-virtual {p0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/util/List;

    if-eqz p0, :cond_0

    return-object p0

    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method public static getTrueTypeFallbackFont(Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;)Lorg/apache/fontbox/ttf/TrueTypeFont;
    .locals 2

    invoke-static {p0}, Lorg/apache/pdfbox/pdmodel/font/ExternalFonts;->getFallbackFontName(Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;)Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Lorg/apache/pdfbox/pdmodel/font/ExternalFonts;->getTrueTypeFont(Ljava/lang/String;)Lorg/apache/fontbox/ttf/TrueTypeFont;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "No TTF fallback font for \'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p0, "\'"

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    const-string v0, "PdfBoxAndroid"

    invoke-static {v0, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    sget-object p0, Lorg/apache/pdfbox/pdmodel/font/ExternalFonts;->ttfFallbackFont:Lorg/apache/fontbox/ttf/TrueTypeFont;

    return-object p0

    :cond_0
    return-object v0
.end method

.method public static getTrueTypeFont(Ljava/lang/String;)Lorg/apache/fontbox/ttf/TrueTypeFont;
    .locals 3

    invoke-static {}, Lorg/apache/pdfbox/pdmodel/font/ExternalFonts;->getProvider()Lorg/apache/pdfbox/pdmodel/font/FontProvider;

    move-result-object v0

    invoke-virtual {v0, p0}, Lorg/apache/pdfbox/pdmodel/font/FontProvider;->getTrueTypeFont(Ljava/lang/String;)Lorg/apache/fontbox/ttf/TrueTypeFont;

    move-result-object v0

    if-nez v0, :cond_2

    invoke-static {p0}, Lorg/apache/pdfbox/pdmodel/font/ExternalFonts;->getSubstitutes(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {}, Lorg/apache/pdfbox/pdmodel/font/ExternalFonts;->getProvider()Lorg/apache/pdfbox/pdmodel/font/FontProvider;

    move-result-object v2

    invoke-virtual {v2, v1}, Lorg/apache/pdfbox/pdmodel/font/FontProvider;->getTrueTypeFont(Ljava/lang/String;)Lorg/apache/fontbox/ttf/TrueTypeFont;

    move-result-object v1

    if-eqz v1, :cond_0

    return-object v1

    :cond_1
    invoke-static {}, Lorg/apache/pdfbox/pdmodel/font/ExternalFonts;->getProvider()Lorg/apache/pdfbox/pdmodel/font/FontProvider;

    move-result-object v0

    invoke-static {p0}, Lorg/apache/pdfbox/pdmodel/font/ExternalFonts;->windowsToPs(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, p0}, Lorg/apache/pdfbox/pdmodel/font/FontProvider;->getTrueTypeFont(Ljava/lang/String;)Lorg/apache/fontbox/ttf/TrueTypeFont;

    move-result-object v0

    :cond_2
    return-object v0
.end method

.method public static getType1EquivalentFont(Ljava/lang/String;)Lorg/apache/fontbox/ttf/Type1Equivalent;
    .locals 1

    invoke-static {p0}, Lorg/apache/pdfbox/pdmodel/font/ExternalFonts;->getType1Font(Ljava/lang/String;)Lorg/apache/fontbox/type1/Type1Font;

    move-result-object v0

    if-eqz v0, :cond_0

    return-object v0

    :cond_0
    invoke-static {p0}, Lorg/apache/pdfbox/pdmodel/font/ExternalFonts;->getCFFType1Font(Ljava/lang/String;)Lorg/apache/fontbox/cff/CFFType1Font;

    move-result-object v0

    if-eqz v0, :cond_1

    return-object v0

    :cond_1
    invoke-static {p0}, Lorg/apache/pdfbox/pdmodel/font/ExternalFonts;->getTrueTypeFont(Ljava/lang/String;)Lorg/apache/fontbox/ttf/TrueTypeFont;

    move-result-object p0

    if-eqz p0, :cond_2

    return-object p0

    :cond_2
    const/4 p0, 0x0

    return-object p0
.end method

.method public static getType1FallbackFont(Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;)Lorg/apache/fontbox/ttf/Type1Equivalent;
    .locals 2

    invoke-static {p0}, Lorg/apache/pdfbox/pdmodel/font/ExternalFonts;->getFallbackFontName(Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;)Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Lorg/apache/pdfbox/pdmodel/font/ExternalFonts;->getType1EquivalentFont(Ljava/lang/String;)Lorg/apache/fontbox/ttf/Type1Equivalent;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "No fallback font for \'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p0, "\'"

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    const-string v0, "PdfBoxAndroid"

    invoke-static {v0, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    sget-object p0, Lorg/apache/pdfbox/pdmodel/font/ExternalFonts;->ttfFallbackFont:Lorg/apache/fontbox/ttf/TrueTypeFont;

    return-object p0

    :cond_0
    return-object v0
.end method

.method public static getType1Font(Ljava/lang/String;)Lorg/apache/fontbox/type1/Type1Font;
    .locals 3

    invoke-static {}, Lorg/apache/pdfbox/pdmodel/font/ExternalFonts;->getProvider()Lorg/apache/pdfbox/pdmodel/font/FontProvider;

    move-result-object v0

    invoke-virtual {v0, p0}, Lorg/apache/pdfbox/pdmodel/font/FontProvider;->getType1Font(Ljava/lang/String;)Lorg/apache/fontbox/type1/Type1Font;

    move-result-object v0

    if-nez v0, :cond_2

    invoke-static {p0}, Lorg/apache/pdfbox/pdmodel/font/ExternalFonts;->getSubstitutes(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {}, Lorg/apache/pdfbox/pdmodel/font/ExternalFonts;->getProvider()Lorg/apache/pdfbox/pdmodel/font/FontProvider;

    move-result-object v2

    invoke-virtual {v2, v1}, Lorg/apache/pdfbox/pdmodel/font/FontProvider;->getType1Font(Ljava/lang/String;)Lorg/apache/fontbox/type1/Type1Font;

    move-result-object v1

    if-eqz v1, :cond_0

    return-object v1

    :cond_1
    invoke-static {}, Lorg/apache/pdfbox/pdmodel/font/ExternalFonts;->getProvider()Lorg/apache/pdfbox/pdmodel/font/FontProvider;

    move-result-object v0

    invoke-static {p0}, Lorg/apache/pdfbox/pdmodel/font/ExternalFonts;->windowsToPs(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, p0}, Lorg/apache/pdfbox/pdmodel/font/FontProvider;->getType1Font(Ljava/lang/String;)Lorg/apache/fontbox/type1/Type1Font;

    move-result-object v0

    :cond_2
    return-object v0
.end method

.method public static setProvider(Lorg/apache/pdfbox/pdmodel/font/FontProvider;)V
    .locals 0

    sput-object p0, Lorg/apache/pdfbox/pdmodel/font/ExternalFonts;->fontProvider:Lorg/apache/pdfbox/pdmodel/font/FontProvider;

    return-void
.end method

.method private static windowsToPs(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    const-string v0, ","

    const-string v1, "-"

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

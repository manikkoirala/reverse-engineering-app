.class public Lorg/apache/pdfbox/pdmodel/font/encoding/Type1Encoding;
.super Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;-><init>()V

    return-void
.end method

.method public constructor <init>(Lorg/apache/fontbox/afm/FontMetrics;)V
    .locals 2

    .line 1
    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;-><init>()V

    invoke-virtual {p1}, Lorg/apache/fontbox/afm/FontMetrics;->getCharMetrics()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/fontbox/afm/CharMetric;

    invoke-virtual {v0}, Lorg/apache/fontbox/afm/CharMetric;->getCharacterCode()I

    move-result v1

    invoke-virtual {v0}, Lorg/apache/fontbox/afm/CharMetric;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v1, v0}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public static fromFontBox(Lorg/apache/fontbox/encoding/Encoding;)Lorg/apache/pdfbox/pdmodel/font/encoding/Type1Encoding;
    .locals 3

    invoke-virtual {p0}, Lorg/apache/fontbox/encoding/Encoding;->getCodeToNameMap()Ljava/util/Map;

    move-result-object p0

    new-instance v0, Lorg/apache/pdfbox/pdmodel/font/encoding/Type1Encoding;

    invoke-direct {v0}, Lorg/apache/pdfbox/pdmodel/font/encoding/Type1Encoding;-><init>()V

    invoke-interface {p0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p0

    invoke-interface {p0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v0, v2, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    goto :goto_0

    :cond_0
    return-object v0
.end method


# virtual methods
.method public getCOSObject()Lorg/apache/pdfbox/cos/COSBase;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

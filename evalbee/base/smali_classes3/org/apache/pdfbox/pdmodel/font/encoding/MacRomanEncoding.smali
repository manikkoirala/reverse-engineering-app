.class public Lorg/apache/pdfbox/pdmodel/font/encoding/MacRomanEncoding;
.super Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;
.source "SourceFile"


# static fields
.field public static final INSTANCE:Lorg/apache/pdfbox/pdmodel/font/encoding/MacRomanEncoding;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lorg/apache/pdfbox/pdmodel/font/encoding/MacRomanEncoding;

    invoke-direct {v0}, Lorg/apache/pdfbox/pdmodel/font/encoding/MacRomanEncoding;-><init>()V

    sput-object v0, Lorg/apache/pdfbox/pdmodel/font/encoding/MacRomanEncoding;->INSTANCE:Lorg/apache/pdfbox/pdmodel/font/encoding/MacRomanEncoding;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;-><init>()V

    const/16 v0, 0x41

    const-string v1, "A"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xae

    const-string v1, "AE"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xe7

    const-string v1, "Aacute"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xe5

    const-string v1, "Acircumflex"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x80

    const-string v1, "Adieresis"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xcb

    const-string v1, "Agrave"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x81

    const-string v1, "Aring"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xcc

    const-string v1, "Atilde"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x42

    const-string v1, "B"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x43

    const-string v1, "C"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x82

    const-string v1, "Ccedilla"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x44

    const-string v1, "D"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x45

    const-string v1, "E"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x83

    const-string v1, "Eacute"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xe6

    const-string v1, "Ecircumflex"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xe8

    const-string v1, "Edieresis"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xe9

    const-string v1, "Egrave"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x46

    const-string v1, "F"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x47

    const-string v1, "G"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x48

    const-string v1, "H"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x49

    const-string v1, "I"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xea

    const-string v1, "Iacute"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xeb

    const-string v1, "Icircumflex"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xec

    const-string v1, "Idieresis"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xed

    const-string v1, "Igrave"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x4a

    const-string v1, "J"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x4b

    const-string v1, "K"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x4c

    const-string v1, "L"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x4d

    const-string v1, "M"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x4e

    const-string v1, "N"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x84

    const-string v1, "Ntilde"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x4f

    const-string v1, "O"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xce

    const-string v1, "OE"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xee

    const-string v1, "Oacute"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xef

    const-string v1, "Ocircumflex"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x85

    const-string v1, "Odieresis"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xf1

    const-string v1, "Ograve"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xaf

    const-string v1, "Oslash"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xcd

    const-string v1, "Otilde"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x50

    const-string v1, "P"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x51

    const-string v1, "Q"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x52

    const-string v1, "R"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x53

    const-string v1, "S"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x54

    const-string v1, "T"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x55

    const-string v1, "U"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xf2

    const-string v1, "Uacute"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xf3

    const-string v1, "Ucircumflex"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x86

    const-string v1, "Udieresis"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xf4

    const-string v1, "Ugrave"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x56

    const-string v1, "V"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x57

    const-string v1, "W"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x58

    const-string v1, "X"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x59

    const-string v1, "Y"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xd9

    const-string v1, "Ydieresis"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x5a

    const-string v1, "Z"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x61

    const-string v1, "a"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x87

    const-string v1, "aacute"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x89

    const-string v1, "acircumflex"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xab

    const-string v1, "acute"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x8a

    const-string v1, "adieresis"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xbe

    const-string v1, "ae"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x88

    const-string v1, "agrave"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x26

    const-string v1, "ampersand"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x8c

    const-string v1, "aring"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x5e

    const-string v1, "asciicircum"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x7e

    const-string v1, "asciitilde"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x2a

    const-string v1, "asterisk"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x40

    const-string v1, "at"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x8b

    const-string v1, "atilde"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x62

    const-string v1, "b"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x5c

    const-string v1, "backslash"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x7c

    const-string v1, "bar"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x7b

    const-string v1, "braceleft"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x7d

    const-string v1, "braceright"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x5b

    const-string v1, "bracketleft"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x5d

    const-string v1, "bracketright"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xf9

    const-string v1, "breve"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xa5

    const-string v1, "bullet"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x63

    const-string v1, "c"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xff

    const-string v1, "caron"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x8d

    const-string v1, "ccedilla"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xfc

    const-string v1, "cedilla"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xa2

    const-string v1, "cent"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xf6

    const-string v1, "circumflex"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x3a

    const-string v1, "colon"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x2c

    const-string v1, "comma"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xa9

    const-string v1, "copyright"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xdb

    const-string v1, "currency"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x64

    const-string v1, "d"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xa0

    const-string v1, "dagger"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xe0

    const-string v1, "daggerdbl"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xa1

    const-string v1, "degree"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xac

    const-string v1, "dieresis"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xd6

    const-string v1, "divide"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x24

    const-string v1, "dollar"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xfa

    const-string v1, "dotaccent"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xf5

    const-string v1, "dotlessi"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x65

    const-string v1, "e"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x8e

    const-string v1, "eacute"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x90

    const-string v1, "ecircumflex"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x91

    const-string v1, "edieresis"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x8f

    const-string v1, "egrave"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x38

    const-string v1, "eight"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xc9

    const-string v1, "ellipsis"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xd1

    const-string v1, "emdash"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xd0

    const-string v1, "endash"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x3d

    const-string v1, "equal"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x21

    const-string v1, "exclam"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xc1

    const-string v1, "exclamdown"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x66

    const-string v1, "f"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xde

    const-string v1, "fi"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x35

    const-string v1, "five"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xdf

    const-string v1, "fl"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xc4

    const-string v1, "florin"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x34

    const-string v1, "four"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xda

    const-string v1, "fraction"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x67

    const-string v1, "g"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xa7

    const-string v1, "germandbls"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x60

    const-string v1, "grave"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x3e

    const-string v1, "greater"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xc7

    const-string v1, "guillemotleft"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xc8

    const-string v1, "guillemotright"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xdc

    const-string v1, "guilsinglleft"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xdd

    const-string v1, "guilsinglright"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x68

    const-string v1, "h"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xfd

    const-string v1, "hungarumlaut"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x2d

    const-string v1, "hyphen"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x69

    const-string v1, "i"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x92

    const-string v1, "iacute"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x94

    const-string v1, "icircumflex"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x95

    const-string v1, "idieresis"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x93

    const-string v1, "igrave"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x6a

    const-string v1, "j"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x6b

    const-string v1, "k"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x6c

    const-string v1, "l"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x3c

    const-string v1, "less"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xc2

    const-string v1, "logicalnot"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x6d

    const-string v1, "m"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xf8

    const-string v1, "macron"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xb5

    const-string v1, "mu"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x6e

    const-string v1, "n"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x39

    const-string v1, "nine"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x96

    const-string v1, "ntilde"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x23

    const-string v1, "numbersign"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x6f

    const-string v1, "o"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x97

    const-string v1, "oacute"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x99

    const-string v1, "ocircumflex"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x9a

    const-string v1, "odieresis"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xcf

    const-string v1, "oe"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xfe

    const-string v1, "ogonek"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x98

    const-string v1, "ograve"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x31

    const-string v1, "one"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xbb

    const-string v1, "ordfeminine"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xbc

    const-string v1, "ordmasculine"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xbf

    const-string v1, "oslash"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x9b

    const-string v1, "otilde"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x70

    const-string v1, "p"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xa6

    const-string v1, "paragraph"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x28

    const-string v1, "parenleft"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x29

    const-string v1, "parenright"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x25

    const-string v1, "percent"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x2e

    const-string v1, "period"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xe1

    const-string v1, "periodcentered"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xe4

    const-string v1, "perthousand"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x2b

    const-string v1, "plus"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xb1

    const-string v1, "plusminus"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x71

    const-string v1, "q"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x3f

    const-string v1, "question"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xc0

    const-string v1, "questiondown"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x22

    const-string v1, "quotedbl"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xe3

    const-string v1, "quotedblbase"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xd2

    const-string v1, "quotedblleft"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xd3

    const-string v1, "quotedblright"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xd4

    const-string v1, "quoteleft"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xd5

    const-string v1, "quoteright"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xe2

    const-string v1, "quotesinglbase"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x27

    const-string v1, "quotesingle"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x72

    const-string v1, "r"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xa8

    const-string v1, "registered"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xfb

    const-string v1, "ring"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x73

    const-string v1, "s"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xa4

    const-string v1, "section"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x3b

    const-string v1, "semicolon"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x37

    const-string v1, "seven"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x36

    const-string v1, "six"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x2f

    const-string v1, "slash"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x20

    const-string v1, "space"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xa3

    const-string v1, "sterling"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x74

    const-string v1, "t"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x33

    const-string v1, "three"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xf7

    const-string v1, "tilde"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xaa

    const-string v1, "trademark"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x32

    const-string v1, "two"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x75

    const-string v1, "u"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x9c

    const-string v1, "uacute"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x9e

    const-string v1, "ucircumflex"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x9f

    const-string v1, "udieresis"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x9d

    const-string v1, "ugrave"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x5f

    const-string v1, "underscore"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x76

    const-string v1, "v"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x77

    const-string v1, "w"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x78

    const-string v1, "x"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x79

    const-string v1, "y"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xd8

    const-string v1, "ydieresis"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xb4

    const-string v1, "yen"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x7a

    const-string v1, "z"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x30

    const-string v1, "zero"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    return-void
.end method


# virtual methods
.method public getCOSObject()Lorg/apache/pdfbox/cos/COSBase;
    .locals 1

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->MAC_ROMAN_ENCODING:Lorg/apache/pdfbox/cos/COSName;

    return-object v0
.end method

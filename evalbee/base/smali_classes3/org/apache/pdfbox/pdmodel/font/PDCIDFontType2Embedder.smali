.class final Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType2Embedder;
.super Lorg/apache/pdfbox/pdmodel/font/TrueTypeEmbedder;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType2Embedder$State;
    }
.end annotation


# instance fields
.field private final cidFont:Lorg/apache/pdfbox/cos/COSDictionary;

.field private final dict:Lorg/apache/pdfbox/cos/COSDictionary;

.field private final document:Lorg/apache/pdfbox/pdmodel/PDDocument;

.field private final gidToUni:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final parent:Lorg/apache/pdfbox/pdmodel/font/PDType0Font;


# direct methods
.method public constructor <init>(Lorg/apache/pdfbox/pdmodel/PDDocument;Lorg/apache/pdfbox/cos/COSDictionary;Ljava/io/InputStream;ZLorg/apache/pdfbox/pdmodel/font/PDType0Font;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lorg/apache/pdfbox/pdmodel/font/TrueTypeEmbedder;-><init>(Lorg/apache/pdfbox/pdmodel/PDDocument;Lorg/apache/pdfbox/cos/COSDictionary;Ljava/io/InputStream;Z)V

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType2Embedder;->document:Lorg/apache/pdfbox/pdmodel/PDDocument;

    iput-object p2, p0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType2Embedder;->dict:Lorg/apache/pdfbox/cos/COSDictionary;

    iput-object p5, p0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType2Embedder;->parent:Lorg/apache/pdfbox/pdmodel/font/PDType0Font;

    sget-object p1, Lorg/apache/pdfbox/cos/COSName;->SUBTYPE:Lorg/apache/pdfbox/cos/COSName;

    sget-object p3, Lorg/apache/pdfbox/cos/COSName;->TYPE0:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p2, p1, p3}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    sget-object p1, Lorg/apache/pdfbox/cos/COSName;->BASE_FONT:Lorg/apache/pdfbox/cos/COSName;

    iget-object p3, p0, Lorg/apache/pdfbox/pdmodel/font/TrueTypeEmbedder;->fontDescriptor:Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;

    invoke-virtual {p3}, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->getFontName()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p2, p1, p3}, Lorg/apache/pdfbox/cos/COSDictionary;->setName(Lorg/apache/pdfbox/cos/COSName;Ljava/lang/String;)V

    sget-object p1, Lorg/apache/pdfbox/cos/COSName;->ENCODING:Lorg/apache/pdfbox/cos/COSName;

    sget-object p3, Lorg/apache/pdfbox/cos/COSName;->IDENTITY_H:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p2, p1, p3}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType2Embedder;->createCIDFont()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object p1

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType2Embedder;->cidFont:Lorg/apache/pdfbox/cos/COSDictionary;

    new-instance p3, Lorg/apache/pdfbox/cos/COSArray;

    invoke-direct {p3}, Lorg/apache/pdfbox/cos/COSArray;-><init>()V

    invoke-virtual {p3, p1}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    sget-object p1, Lorg/apache/pdfbox/cos/COSName;->DESCENDANT_FONTS:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p2, p1, p3}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType2Embedder;->gidToUni:Ljava/util/Map;

    iget-object p1, p0, Lorg/apache/pdfbox/pdmodel/font/TrueTypeEmbedder;->ttf:Lorg/apache/fontbox/ttf/TrueTypeFont;

    invoke-virtual {p1}, Lorg/apache/fontbox/ttf/TrueTypeFont;->getMaximumProfile()Lorg/apache/fontbox/ttf/MaximumProfileTable;

    move-result-object p1

    invoke-virtual {p1}, Lorg/apache/fontbox/ttf/MaximumProfileTable;->getNumGlyphs()I

    move-result p1

    const/4 p2, 0x1

    :goto_0
    if-gt p2, p1, :cond_1

    iget-object p3, p0, Lorg/apache/pdfbox/pdmodel/font/TrueTypeEmbedder;->cmap:Lorg/apache/fontbox/ttf/CmapSubtable;

    invoke-virtual {p3, p2}, Lorg/apache/fontbox/ttf/CmapSubtable;->getCharacterCode(I)Ljava/lang/Integer;

    move-result-object p3

    if-eqz p3, :cond_0

    iget-object p4, p0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType2Embedder;->gidToUni:Ljava/util/Map;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p5

    invoke-interface {p4, p5, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    add-int/lit8 p2, p2, 0x1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType2Embedder;->buildToUnicodeCMap(Ljava/util/Map;)V

    return-void
.end method

.method private addNameTag(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/TrueTypeEmbedder;->fontDescriptor:Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->getFontName()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType2Embedder;->dict:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->BASE_FONT:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setName(Lorg/apache/pdfbox/cos/COSName;Ljava/lang/String;)V

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/TrueTypeEmbedder;->fontDescriptor:Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;

    invoke-virtual {v0, p1}, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->setFontName(Ljava/lang/String;)V

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType2Embedder;->cidFont:Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setName(Lorg/apache/pdfbox/cos/COSName;Ljava/lang/String;)V

    return-void
.end method

.method private buildCIDSet(Ljava/util/Map;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    invoke-interface {p1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->max(Ljava/util/Collection;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    div-int/lit8 v0, v0, 0x8

    const/4 v1, 0x1

    add-int/2addr v0, v1

    new-array v0, v0, [B

    invoke-interface {p1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    rem-int/lit8 v3, v2, 0x8

    rsub-int/lit8 v3, v3, 0x7

    shl-int v3, v1, v3

    div-int/lit8 v2, v2, 0x8

    aget-byte v4, v0, v2

    or-int/2addr v3, v4

    int-to-byte v3, v3

    aput-byte v3, v0, v2

    goto :goto_0

    :cond_0
    new-instance p1, Ljava/io/ByteArrayInputStream;

    invoke-direct {p1, v0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    new-instance v0, Lorg/apache/pdfbox/pdmodel/common/PDStream;

    iget-object v1, p0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType2Embedder;->document:Lorg/apache/pdfbox/pdmodel/PDDocument;

    invoke-direct {v0, v1, p1}, Lorg/apache/pdfbox/pdmodel/common/PDStream;-><init>(Lorg/apache/pdfbox/pdmodel/PDDocument;Ljava/io/InputStream;)V

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/common/PDStream;->addCompression()V

    iget-object p1, p0, Lorg/apache/pdfbox/pdmodel/font/TrueTypeEmbedder;->fontDescriptor:Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;

    invoke-virtual {p1, v0}, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->setCIDSet(Lorg/apache/pdfbox/pdmodel/common/PDStream;)V

    return-void
.end method

.method private buildCIDToGIDMap(Ljava/util/Map;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    invoke-interface {p1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Collections;->max(Ljava/util/Collection;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/4 v2, 0x0

    move v3, v2

    :goto_0
    if-gt v3, v1, :cond_1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {p1, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {p1, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    goto :goto_1

    :cond_0
    move v4, v2

    :goto_1
    const/4 v5, 0x2

    new-array v5, v5, [B

    shr-int/lit8 v6, v4, 0x8

    and-int/lit16 v6, v6, 0xff

    int-to-byte v6, v6

    aput-byte v6, v5, v2

    and-int/lit16 v4, v4, 0xff

    int-to-byte v4, v4

    const/4 v6, 0x1

    aput-byte v4, v5, v6

    invoke-virtual {v0, v5}, Ljava/io/OutputStream;->write([B)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    new-instance p1, Ljava/io/ByteArrayInputStream;

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    new-instance v0, Lorg/apache/pdfbox/pdmodel/common/PDStream;

    iget-object v1, p0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType2Embedder;->document:Lorg/apache/pdfbox/pdmodel/PDDocument;

    invoke-direct {v0, v1, p1, v2}, Lorg/apache/pdfbox/pdmodel/common/PDStream;-><init>(Lorg/apache/pdfbox/pdmodel/PDDocument;Ljava/io/InputStream;Z)V

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/common/PDStream;->getStream()Lorg/apache/pdfbox/cos/COSStream;

    move-result-object p1

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->LENGTH1:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/common/PDStream;->getByteArray()[B

    move-result-object v2

    array-length v2, v2

    invoke-virtual {p1, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->setInt(Lorg/apache/pdfbox/cos/COSName;I)V

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/common/PDStream;->addCompression()V

    iget-object p1, p0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType2Embedder;->cidFont:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->CID_TO_GID_MAP:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p1, v1, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)V

    return-void
.end method

.method private buildToUnicodeCMap(Ljava/util/Map;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    new-instance v0, Lorg/apache/pdfbox/pdmodel/font/ToUnicodeWriter;

    invoke-direct {v0}, Lorg/apache/pdfbox/pdmodel/font/ToUnicodeWriter;-><init>()V

    iget-object v1, p0, Lorg/apache/pdfbox/pdmodel/font/TrueTypeEmbedder;->ttf:Lorg/apache/fontbox/ttf/TrueTypeFont;

    invoke-virtual {v1}, Lorg/apache/fontbox/ttf/TrueTypeFont;->getMaximumProfile()Lorg/apache/fontbox/ttf/MaximumProfileTable;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/fontbox/ttf/MaximumProfileTable;->getNumGlyphs()I

    move-result v1

    const/4 v2, 0x0

    const/4 v3, 0x1

    move v5, v2

    move v4, v3

    :goto_0
    if-gt v4, v1, :cond_4

    if-eqz p1, :cond_1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {p1, v6}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    goto :goto_2

    :cond_0
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {p1, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    goto :goto_1

    :cond_1
    move v6, v4

    :goto_1
    iget-object v7, p0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType2Embedder;->gidToUni:Ljava/util/Map;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-interface {v7, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    if-eqz v7, :cond_3

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v8

    const v9, 0xffff

    if-le v8, v9, :cond_2

    move v5, v3

    :cond_2
    new-instance v8, Ljava/lang/String;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    filled-new-array {v7}, [I

    move-result-object v7

    invoke-direct {v8, v7, v2, v3}, Ljava/lang/String;-><init>([III)V

    invoke-virtual {v0, v6, v8}, Lorg/apache/pdfbox/pdmodel/font/ToUnicodeWriter;->add(ILjava/lang/String;)V

    :cond_3
    :goto_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_4
    new-instance p1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {p1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    invoke-virtual {v0, p1}, Lorg/apache/pdfbox/pdmodel/font/ToUnicodeWriter;->writeTo(Ljava/io/OutputStream;)V

    new-instance v0, Ljava/io/ByteArrayInputStream;

    invoke-virtual {p1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    new-instance p1, Lorg/apache/pdfbox/pdmodel/common/PDStream;

    iget-object v1, p0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType2Embedder;->document:Lorg/apache/pdfbox/pdmodel/PDDocument;

    invoke-direct {p1, v1, v0, v2}, Lorg/apache/pdfbox/pdmodel/common/PDStream;-><init>(Lorg/apache/pdfbox/pdmodel/PDDocument;Ljava/io/InputStream;Z)V

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/common/PDStream;->addCompression()V

    if-eqz v5, :cond_5

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType2Embedder;->document:Lorg/apache/pdfbox/pdmodel/PDDocument;

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/PDDocument;->getVersion()F

    move-result v0

    float-to-double v0, v0

    const-wide/high16 v2, 0x3ff8000000000000L    # 1.5

    cmpg-double v0, v0, v2

    if-gez v0, :cond_5

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType2Embedder;->document:Lorg/apache/pdfbox/pdmodel/PDDocument;

    const/high16 v1, 0x3fc00000    # 1.5f

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/pdmodel/PDDocument;->setVersion(F)V

    :cond_5
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType2Embedder;->dict:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->TO_UNICODE:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)V

    return-void
.end method

.method private buildWidths(Ljava/util/Map;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/TrueTypeEmbedder;->ttf:Lorg/apache/fontbox/ttf/TrueTypeFont;

    invoke-virtual {v0}, Lorg/apache/fontbox/ttf/TrueTypeFont;->getHeader()Lorg/apache/fontbox/ttf/HeaderTable;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/fontbox/ttf/HeaderTable;->getUnitsPerEm()I

    move-result v0

    int-to-float v0, v0

    const/high16 v1, 0x447a0000    # 1000.0f

    div-float/2addr v1, v0

    new-instance v0, Lorg/apache/pdfbox/cos/COSArray;

    invoke-direct {v0}, Lorg/apache/pdfbox/cos/COSArray;-><init>()V

    new-instance v2, Lorg/apache/pdfbox/cos/COSArray;

    invoke-direct {v2}, Lorg/apache/pdfbox/cos/COSArray;-><init>()V

    invoke-interface {p1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    const/4 v4, -0x1

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {p1, v6}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    goto :goto_0

    :cond_0
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {p1, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    iget-object v7, p0, Lorg/apache/pdfbox/pdmodel/font/TrueTypeEmbedder;->ttf:Lorg/apache/fontbox/ttf/TrueTypeFont;

    invoke-virtual {v7}, Lorg/apache/fontbox/ttf/TrueTypeFont;->getHorizontalMetrics()Lorg/apache/fontbox/ttf/HorizontalMetricsTable;

    move-result-object v7

    invoke-virtual {v7, v6}, Lorg/apache/fontbox/ttf/HorizontalMetricsTable;->getAdvanceWidth(I)I

    move-result v6

    int-to-float v6, v6

    mul-float/2addr v6, v1

    add-int/lit8 v7, v5, -0x1

    if-eq v4, v7, :cond_1

    new-instance v2, Lorg/apache/pdfbox/cos/COSArray;

    invoke-direct {v2}, Lorg/apache/pdfbox/cos/COSArray;-><init>()V

    int-to-long v7, v5

    invoke-static {v7, v8}, Lorg/apache/pdfbox/cos/COSInteger;->get(J)Lorg/apache/pdfbox/cos/COSInteger;

    move-result-object v4

    invoke-virtual {v0, v4}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    invoke-virtual {v0, v2}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    :cond_1
    invoke-static {v6}, Ljava/lang/Math;->round(F)I

    move-result v4

    int-to-long v6, v4

    invoke-static {v6, v7}, Lorg/apache/pdfbox/cos/COSInteger;->get(J)Lorg/apache/pdfbox/cos/COSInteger;

    move-result-object v4

    invoke-virtual {v2, v4}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    move v4, v5

    goto :goto_0

    :cond_2
    iget-object p1, p0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType2Embedder;->cidFont:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->W:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p1, v1, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    return-void
.end method

.method private buildWidths(Lorg/apache/pdfbox/cos/COSDictionary;)V
    .locals 5

    .line 2
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/TrueTypeEmbedder;->ttf:Lorg/apache/fontbox/ttf/TrueTypeFont;

    invoke-virtual {v0}, Lorg/apache/fontbox/ttf/TrueTypeFont;->getNumberOfGlyphs()I

    move-result v0

    mul-int/lit8 v1, v0, 0x2

    new-array v1, v1, [I

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_0

    mul-int/lit8 v3, v2, 0x2

    aput v2, v1, v3

    add-int/lit8 v3, v3, 0x1

    iget-object v4, p0, Lorg/apache/pdfbox/pdmodel/font/TrueTypeEmbedder;->ttf:Lorg/apache/fontbox/ttf/TrueTypeFont;

    invoke-virtual {v4}, Lorg/apache/fontbox/ttf/TrueTypeFont;->getHorizontalMetrics()Lorg/apache/fontbox/ttf/HorizontalMetricsTable;

    move-result-object v4

    invoke-virtual {v4, v2}, Lorg/apache/fontbox/ttf/HorizontalMetricsTable;->getAdvanceWidth(I)I

    move-result v4

    aput v4, v1, v3

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->W:Lorg/apache/pdfbox/cos/COSName;

    invoke-direct {p0, v1}, Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType2Embedder;->getWidths([I)Lorg/apache/pdfbox/cos/COSArray;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    return-void
.end method

.method private createCIDFont()Lorg/apache/pdfbox/cos/COSDictionary;
    .locals 4

    new-instance v0, Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-direct {v0}, Lorg/apache/pdfbox/cos/COSDictionary;-><init>()V

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->TYPE:Lorg/apache/pdfbox/cos/COSName;

    sget-object v2, Lorg/apache/pdfbox/cos/COSName;->FONT:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->SUBTYPE:Lorg/apache/pdfbox/cos/COSName;

    sget-object v2, Lorg/apache/pdfbox/cos/COSName;->CID_FONT_TYPE2:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->BASE_FONT:Lorg/apache/pdfbox/cos/COSName;

    iget-object v2, p0, Lorg/apache/pdfbox/pdmodel/font/TrueTypeEmbedder;->fontDescriptor:Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;

    invoke-virtual {v2}, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->getFontName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->setName(Lorg/apache/pdfbox/cos/COSName;Ljava/lang/String;)V

    const-string v1, "Identity"

    const/4 v2, 0x0

    const-string v3, "Adobe"

    invoke-direct {p0, v3, v1, v2}, Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType2Embedder;->toCIDSystemInfo(Ljava/lang/String;Ljava/lang/String;I)Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v1

    sget-object v2, Lorg/apache/pdfbox/cos/COSName;->CIDSYSTEMINFO:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v2, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->FONT_DESC:Lorg/apache/pdfbox/cos/COSName;

    iget-object v2, p0, Lorg/apache/pdfbox/pdmodel/font/TrueTypeEmbedder;->fontDescriptor:Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;

    invoke-virtual {v2}, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->getCOSObject()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType2Embedder;->buildWidths(Lorg/apache/pdfbox/cos/COSDictionary;)V

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->CID_TO_GID_MAP:Lorg/apache/pdfbox/cos/COSName;

    sget-object v2, Lorg/apache/pdfbox/cos/COSName;->IDENTITY:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    return-object v0
.end method

.method private getWidths([I)Lorg/apache/pdfbox/cos/COSArray;
    .locals 19

    move-object/from16 v0, p1

    array-length v1, v0

    if-eqz v1, :cond_d

    move-object/from16 v1, p0

    iget-object v2, v1, Lorg/apache/pdfbox/pdmodel/font/TrueTypeEmbedder;->ttf:Lorg/apache/fontbox/ttf/TrueTypeFont;

    invoke-virtual {v2}, Lorg/apache/fontbox/ttf/TrueTypeFont;->getHeader()Lorg/apache/fontbox/ttf/HeaderTable;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/fontbox/ttf/HeaderTable;->getUnitsPerEm()I

    move-result v2

    int-to-float v2, v2

    const/high16 v3, 0x447a0000    # 1000.0f

    div-float/2addr v3, v2

    const/4 v2, 0x0

    aget v2, v0, v2

    int-to-long v4, v2

    const/4 v2, 0x1

    aget v6, v0, v2

    int-to-float v6, v6

    mul-float/2addr v6, v3

    invoke-static {v6}, Ljava/lang/Math;->round(F)I

    move-result v6

    int-to-long v6, v6

    new-instance v8, Lorg/apache/pdfbox/cos/COSArray;

    invoke-direct {v8}, Lorg/apache/pdfbox/cos/COSArray;-><init>()V

    invoke-static {v4, v5}, Lorg/apache/pdfbox/cos/COSInteger;->get(J)Lorg/apache/pdfbox/cos/COSInteger;

    move-result-object v9

    invoke-virtual {v8, v9}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    sget-object v9, Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType2Embedder$State;->FIRST:Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType2Embedder$State;

    const/4 v11, 0x0

    const/4 v12, 0x2

    :goto_0
    array-length v13, v0

    if-ge v12, v13, :cond_9

    aget v13, v0, v12

    int-to-long v14, v13

    add-int/lit8 v13, v12, 0x1

    aget v13, v0, v13

    int-to-float v13, v13

    mul-float/2addr v13, v3

    invoke-static {v13}, Ljava/lang/Math;->round(F)I

    move-result v13

    move-object/from16 v16, v11

    int-to-long v10, v13

    sget-object v13, Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType2Embedder$1;->$SwitchMap$org$apache$pdfbox$pdmodel$font$PDCIDFontType2Embedder$State:[I

    invoke-virtual {v9}, Ljava/lang/Enum;->ordinal()I

    move-result v17

    aget v13, v13, v17

    const-wide/16 v17, 0x1

    if-eq v13, v2, :cond_6

    const/4 v2, 0x2

    if-eq v13, v2, :cond_3

    const/4 v2, 0x3

    if-eq v13, v2, :cond_0

    goto :goto_1

    :cond_0
    add-long v17, v4, v17

    cmp-long v2, v14, v17

    if-nez v2, :cond_1

    cmp-long v2, v10, v6

    if-eqz v2, :cond_2

    :cond_1
    invoke-static {v4, v5}, Lorg/apache/pdfbox/cos/COSInteger;->get(J)Lorg/apache/pdfbox/cos/COSInteger;

    move-result-object v2

    invoke-virtual {v8, v2}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    invoke-static {v6, v7}, Lorg/apache/pdfbox/cos/COSInteger;->get(J)Lorg/apache/pdfbox/cos/COSInteger;

    move-result-object v2

    invoke-virtual {v8, v2}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    invoke-static {v14, v15}, Lorg/apache/pdfbox/cos/COSInteger;->get(J)Lorg/apache/pdfbox/cos/COSInteger;

    move-result-object v2

    invoke-virtual {v8, v2}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    sget-object v2, Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType2Embedder$State;->FIRST:Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType2Embedder$State;

    move-object v9, v2

    :cond_2
    :goto_1
    move-object/from16 v13, v16

    goto/16 :goto_4

    :cond_3
    add-long v17, v4, v17

    cmp-long v2, v14, v17

    if-nez v2, :cond_4

    cmp-long v13, v10, v6

    if-nez v13, :cond_4

    sget-object v2, Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType2Embedder$State;->SERIAL:Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType2Embedder$State;

    move-object/from16 v13, v16

    invoke-virtual {v8, v13}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    invoke-static {v4, v5}, Lorg/apache/pdfbox/cos/COSInteger;->get(J)Lorg/apache/pdfbox/cos/COSInteger;

    move-result-object v4

    goto :goto_2

    :cond_4
    move-object/from16 v13, v16

    if-nez v2, :cond_5

    invoke-static {v6, v7}, Lorg/apache/pdfbox/cos/COSInteger;->get(J)Lorg/apache/pdfbox/cos/COSInteger;

    move-result-object v2

    invoke-virtual {v13, v2}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    goto :goto_4

    :cond_5
    sget-object v2, Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType2Embedder$State;->FIRST:Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType2Embedder$State;

    invoke-static {v6, v7}, Lorg/apache/pdfbox/cos/COSInteger;->get(J)Lorg/apache/pdfbox/cos/COSInteger;

    move-result-object v4

    invoke-virtual {v13, v4}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    invoke-virtual {v8, v13}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    invoke-static {v14, v15}, Lorg/apache/pdfbox/cos/COSInteger;->get(J)Lorg/apache/pdfbox/cos/COSInteger;

    move-result-object v4

    :goto_2
    invoke-virtual {v8, v4}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    goto :goto_3

    :cond_6
    move-object/from16 v13, v16

    add-long v4, v4, v17

    cmp-long v2, v14, v4

    if-nez v2, :cond_7

    cmp-long v4, v10, v6

    if-nez v4, :cond_7

    sget-object v2, Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType2Embedder$State;->SERIAL:Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType2Embedder$State;

    :goto_3
    move-object v9, v2

    goto :goto_4

    :cond_7
    if-nez v2, :cond_8

    sget-object v2, Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType2Embedder$State;->BRACKET:Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType2Embedder$State;

    new-instance v4, Lorg/apache/pdfbox/cos/COSArray;

    invoke-direct {v4}, Lorg/apache/pdfbox/cos/COSArray;-><init>()V

    invoke-static {v6, v7}, Lorg/apache/pdfbox/cos/COSInteger;->get(J)Lorg/apache/pdfbox/cos/COSInteger;

    move-result-object v5

    invoke-virtual {v4, v5}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    move-object v9, v2

    move-object v13, v4

    goto :goto_4

    :cond_8
    new-instance v2, Lorg/apache/pdfbox/cos/COSArray;

    invoke-direct {v2}, Lorg/apache/pdfbox/cos/COSArray;-><init>()V

    invoke-static {v6, v7}, Lorg/apache/pdfbox/cos/COSInteger;->get(J)Lorg/apache/pdfbox/cos/COSInteger;

    move-result-object v4

    invoke-virtual {v2, v4}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    invoke-virtual {v8, v2}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    invoke-static {v14, v15}, Lorg/apache/pdfbox/cos/COSInteger;->get(J)Lorg/apache/pdfbox/cos/COSInteger;

    move-result-object v4

    invoke-virtual {v8, v4}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    move-object v13, v2

    :goto_4
    add-int/lit8 v12, v12, 0x2

    move-wide v6, v10

    move-object v11, v13

    move-wide v4, v14

    const/4 v2, 0x1

    goto/16 :goto_0

    :cond_9
    move-object v13, v11

    sget-object v0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType2Embedder$1;->$SwitchMap$org$apache$pdfbox$pdmodel$font$PDCIDFontType2Embedder$State:[I

    invoke-virtual {v9}, Ljava/lang/Enum;->ordinal()I

    move-result v2

    aget v0, v0, v2

    const/4 v2, 0x1

    if-eq v0, v2, :cond_c

    const/4 v2, 0x2

    if-eq v0, v2, :cond_b

    const/4 v2, 0x3

    if-eq v0, v2, :cond_a

    goto :goto_6

    :cond_a
    invoke-static {v4, v5}, Lorg/apache/pdfbox/cos/COSInteger;->get(J)Lorg/apache/pdfbox/cos/COSInteger;

    move-result-object v0

    invoke-virtual {v8, v0}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    invoke-static {v6, v7}, Lorg/apache/pdfbox/cos/COSInteger;->get(J)Lorg/apache/pdfbox/cos/COSInteger;

    move-result-object v0

    goto :goto_5

    :cond_b
    invoke-static {v6, v7}, Lorg/apache/pdfbox/cos/COSInteger;->get(J)Lorg/apache/pdfbox/cos/COSInteger;

    move-result-object v0

    invoke-virtual {v13, v0}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    invoke-virtual {v8, v13}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    goto :goto_6

    :cond_c
    new-instance v0, Lorg/apache/pdfbox/cos/COSArray;

    invoke-direct {v0}, Lorg/apache/pdfbox/cos/COSArray;-><init>()V

    invoke-static {v6, v7}, Lorg/apache/pdfbox/cos/COSInteger;->get(J)Lorg/apache/pdfbox/cos/COSInteger;

    move-result-object v2

    invoke-virtual {v0, v2}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    :goto_5
    invoke-virtual {v8, v0}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    :goto_6
    return-object v8

    :cond_d
    move-object/from16 v1, p0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v2, "length of widths must be > 0"

    invoke-direct {v0, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private toCIDSystemInfo(Ljava/lang/String;Ljava/lang/String;I)Lorg/apache/pdfbox/cos/COSDictionary;
    .locals 2

    new-instance v0, Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-direct {v0}, Lorg/apache/pdfbox/cos/COSDictionary;-><init>()V

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->REGISTRY:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setString(Lorg/apache/pdfbox/cos/COSName;Ljava/lang/String;)V

    sget-object p1, Lorg/apache/pdfbox/cos/COSName;->ORDERING:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, p1, p2}, Lorg/apache/pdfbox/cos/COSDictionary;->setString(Lorg/apache/pdfbox/cos/COSName;Ljava/lang/String;)V

    sget-object p1, Lorg/apache/pdfbox/cos/COSName;->SUPPLEMENT:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, p1, p3}, Lorg/apache/pdfbox/cos/COSDictionary;->setInt(Lorg/apache/pdfbox/cos/COSName;I)V

    return-object v0
.end method


# virtual methods
.method public buildSubset(Ljava/io/InputStream;Ljava/lang/String;Ljava/util/Map;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/InputStream;",
            "Ljava/lang/String;",
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-interface {p3}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/pdmodel/font/TrueTypeEmbedder;->buildFontFile2(Ljava/io/InputStream;)V

    invoke-direct {p0, p2}, Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType2Embedder;->addNameTag(Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType2Embedder;->buildWidths(Ljava/util/Map;)V

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType2Embedder;->buildCIDToGIDMap(Ljava/util/Map;)V

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType2Embedder;->buildCIDSet(Ljava/util/Map;)V

    invoke-direct {p0, p3}, Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType2Embedder;->buildToUnicodeCMap(Ljava/util/Map;)V

    return-void
.end method

.method public getCIDFont()Lorg/apache/pdfbox/pdmodel/font/PDCIDFont;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType2Embedder;->cidFont:Lorg/apache/pdfbox/cos/COSDictionary;

    iget-object v1, p0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType2Embedder;->parent:Lorg/apache/pdfbox/pdmodel/font/PDType0Font;

    invoke-static {v0, v1}, Lorg/apache/pdfbox/pdmodel/font/PDFontFactory;->createDescendantFont(Lorg/apache/pdfbox/cos/COSDictionary;Lorg/apache/pdfbox/pdmodel/font/PDType0Font;)Lorg/apache/pdfbox/pdmodel/font/PDCIDFont;

    move-result-object v0

    return-object v0
.end method

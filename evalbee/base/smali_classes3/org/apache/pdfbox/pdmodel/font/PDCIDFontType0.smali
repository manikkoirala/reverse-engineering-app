.class public Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType0;
.super Lorg/apache/pdfbox/pdmodel/font/PDCIDFont;
.source "SourceFile"


# instance fields
.field private avgWidth:Ljava/lang/Float;

.field private final cidFont:Lorg/apache/fontbox/cff/CFFCIDFont;

.field private fontMatrix:Lorg/apache/pdfbox/util/Matrix;

.field private fontMatrixTransform:Lorg/apache/pdfbox/util/awt/AffineTransform;

.field private final glyphHeights:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private final isDamaged:Z

.field private final isEmbedded:Z

.field private final t1Font:Lorg/apache/fontbox/cff/CFFType1Font;


# direct methods
.method public constructor <init>(Lorg/apache/pdfbox/cos/COSDictionary;Lorg/apache/pdfbox/pdmodel/font/PDType0Font;)V
    .locals 6

    invoke-direct {p0, p1, p2}, Lorg/apache/pdfbox/pdmodel/font/PDCIDFont;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;Lorg/apache/pdfbox/pdmodel/font/PDType0Font;)V

    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType0;->glyphHeights:Ljava/util/Map;

    const/4 p1, 0x0

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType0;->avgWidth:Ljava/lang/Float;

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDCIDFont;->getFontDescriptor()Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;

    move-result-object p2

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->getFontFile3()Lorg/apache/pdfbox/pdmodel/common/PDStream;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/common/PDStream;->createInputStream()Ljava/io/InputStream;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/pdfbox/io/IOUtils;->toByteArray(Ljava/io/InputStream;)[B

    move-result-object v0

    goto :goto_0

    :cond_0
    move-object v0, p1

    :goto_0
    const/4 v1, 0x1

    const-string v2, "PdfBoxAndroid"

    const/4 v3, 0x0

    if-eqz v0, :cond_1

    array-length v4, v0

    if-lez v4, :cond_1

    aget-byte v4, v0, v3

    and-int/lit16 v4, v4, 0xff

    const/16 v5, 0x25

    if-ne v4, v5, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unsupported: Type1 font instead of CFF in "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->getFontName()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-static {v2, p2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_1
    move-object v0, p1

    move p2, v1

    goto :goto_3

    :cond_1
    if-eqz v0, :cond_2

    new-instance v4, Lorg/apache/fontbox/cff/CFFParser;

    invoke-direct {v4}, Lorg/apache/fontbox/cff/CFFParser;-><init>()V

    :try_start_0
    invoke-virtual {v4, v0}, Lorg/apache/fontbox/cff/CFFParser;->parse([B)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/fontbox/cff/CFFFont;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    :catch_0
    move-exception v0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Can\'t read the embedded CFF font "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->getFontName()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-static {v2, p2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    :cond_2
    move-object v0, p1

    :goto_2
    move p2, v3

    :goto_3
    if-eqz v0, :cond_4

    instance-of p2, v0, Lorg/apache/fontbox/cff/CFFCIDFont;

    if-eqz p2, :cond_3

    check-cast v0, Lorg/apache/fontbox/cff/CFFCIDFont;

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType0;->cidFont:Lorg/apache/fontbox/cff/CFFCIDFont;

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType0;->t1Font:Lorg/apache/fontbox/cff/CFFType1Font;

    goto :goto_4

    :cond_3
    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType0;->cidFont:Lorg/apache/fontbox/cff/CFFCIDFont;

    check-cast v0, Lorg/apache/fontbox/cff/CFFType1Font;

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType0;->t1Font:Lorg/apache/fontbox/cff/CFFType1Font;

    :goto_4
    iput-boolean v1, p0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType0;->isEmbedded:Z

    iput-boolean v3, p0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType0;->isDamaged:Z

    goto/16 :goto_7

    :cond_4
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDCIDFont;->getBaseFont()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/pdfbox/pdmodel/font/ExternalFonts;->getCFFCIDFont(Ljava/lang/String;)Lorg/apache/fontbox/cff/CFFCIDFont;

    move-result-object v0

    if-eqz v0, :cond_5

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType0;->cidFont:Lorg/apache/fontbox/cff/CFFCIDFont;

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType0;->t1Font:Lorg/apache/fontbox/cff/CFFType1Font;

    goto/16 :goto_6

    :cond_5
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFont;->dict:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->CIDSYSTEMINFO:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSDictionary;

    if-eqz v0, :cond_6

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->REGISTRY:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getNameAsString(Lorg/apache/pdfbox/cos/COSName;)Ljava/lang/String;

    move-result-object v1

    sget-object v4, Lorg/apache/pdfbox/cos/COSName;->ORDERING:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v4}, Lorg/apache/pdfbox/cos/COSDictionary;->getNameAsString(Lorg/apache/pdfbox/cos/COSName;)Ljava/lang/String;

    move-result-object v0

    if-eqz v1, :cond_6

    if-eqz v0, :cond_6

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "-"

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_5

    :cond_6
    move-object v0, p1

    :goto_5
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDCIDFont;->getFontDescriptor()Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;

    move-result-object v1

    invoke-static {v0, v1}, Lorg/apache/pdfbox/pdmodel/font/ExternalFonts;->getCFFCIDFontFallback(Ljava/lang/String;Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;)Lorg/apache/fontbox/cff/CFFCIDFont;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType0;->cidFont:Lorg/apache/fontbox/cff/CFFCIDFont;

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType0;->t1Font:Lorg/apache/fontbox/cff/CFFType1Font;

    invoke-virtual {v0}, Lorg/apache/fontbox/cff/CFFFont;->getName()Ljava/lang/String;

    move-result-object p1

    const-string v0, "AdobeBlank"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_7

    if-nez p2, :cond_8

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Missing CID-keyed font "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDCIDFont;->getBaseFont()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v2, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_6

    :cond_7
    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Using fallback for CID-keyed font "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDCIDFont;->getBaseFont()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v2, p1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_8
    :goto_6
    iput-boolean v3, p0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType0;->isEmbedded:Z

    iput-boolean p2, p0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType0;->isDamaged:Z

    :goto_7
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType0;->getFontMatrix()Lorg/apache/pdfbox/util/Matrix;

    move-result-object p1

    invoke-virtual {p1}, Lorg/apache/pdfbox/util/Matrix;->createAffineTransform()Lorg/apache/pdfbox/util/awt/AffineTransform;

    move-result-object p1

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType0;->fontMatrixTransform:Lorg/apache/pdfbox/util/awt/AffineTransform;

    const-wide v0, 0x408f400000000000L    # 1000.0

    invoke-virtual {p1, v0, v1, v0, v1}, Lorg/apache/pdfbox/util/awt/AffineTransform;->scale(DD)V

    return-void
.end method

.method private getAverageCharacterWidth()F
    .locals 1

    const/high16 v0, 0x43fa0000    # 500.0f

    return v0
.end method


# virtual methods
.method public codeToCID(I)I
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFont;->parent:Lorg/apache/pdfbox/pdmodel/font/PDType0Font;

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/font/PDType0Font;->getCMap()Lorg/apache/fontbox/cmap/CMap;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/fontbox/cmap/CMap;->toCID(I)I

    move-result p1

    return p1
.end method

.method public codeToGID(I)I
    .locals 1

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType0;->codeToCID(I)I

    move-result p1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType0;->cidFont:Lorg/apache/fontbox/cff/CFFCIDFont;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lorg/apache/fontbox/cff/CFFFont;->getCharset()Lorg/apache/fontbox/cff/CFFCharset;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/fontbox/cff/CFFCharset;->getGIDForCID(I)I

    move-result p1

    :cond_0
    return p1
.end method

.method public encode(I)[B
    .locals 0

    new-instance p1, Ljava/lang/UnsupportedOperationException;

    invoke-direct {p1}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw p1
.end method

.method public getAverageFontWidth()F
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType0;->avgWidth:Ljava/lang/Float;

    if-nez v0, :cond_0

    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType0;->getAverageCharacterWidth()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType0;->avgWidth:Ljava/lang/Float;

    :cond_0
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType0;->avgWidth:Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    return v0
.end method

.method public getBoundingBox()Lorg/apache/fontbox/util/BoundingBox;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType0;->cidFont:Lorg/apache/fontbox/cff/CFFCIDFont;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lorg/apache/fontbox/cff/CFFFont;->getFontBBox()Lorg/apache/fontbox/util/BoundingBox;

    move-result-object v0

    return-object v0

    :cond_0
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType0;->t1Font:Lorg/apache/fontbox/cff/CFFType1Font;

    invoke-virtual {v0}, Lorg/apache/fontbox/cff/CFFFont;->getFontBBox()Lorg/apache/fontbox/util/BoundingBox;

    move-result-object v0

    return-object v0
.end method

.method public getCFFFont()Lorg/apache/fontbox/cff/CFFFont;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType0;->cidFont:Lorg/apache/fontbox/cff/CFFCIDFont;

    if-eqz v0, :cond_0

    return-object v0

    :cond_0
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType0;->t1Font:Lorg/apache/fontbox/cff/CFFType1Font;

    return-object v0
.end method

.method public getFontMatrix()Lorg/apache/pdfbox/util/Matrix;
    .locals 18

    move-object/from16 v0, p0

    iget-object v1, v0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType0;->fontMatrix:Lorg/apache/pdfbox/util/Matrix;

    if-nez v1, :cond_2

    iget-object v1, v0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType0;->cidFont:Lorg/apache/fontbox/cff/CFFCIDFont;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lorg/apache/fontbox/cff/CFFCIDFont;->getFontMatrix()Ljava/util/List;

    move-result-object v1

    goto :goto_0

    :cond_0
    iget-object v1, v0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType0;->t1Font:Lorg/apache/fontbox/cff/CFFType1Font;

    invoke-virtual {v1}, Lorg/apache/fontbox/cff/CFFType1Font;->getFontMatrix()Ljava/util/List;

    move-result-object v1

    :goto_0
    if-eqz v1, :cond_1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    const/4 v3, 0x6

    if-ne v2, v3, :cond_1

    new-instance v2, Lorg/apache/pdfbox/util/Matrix;

    const/4 v3, 0x0

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Number;

    invoke-virtual {v3}, Ljava/lang/Number;->floatValue()F

    move-result v5

    const/4 v3, 0x1

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Number;

    invoke-virtual {v3}, Ljava/lang/Number;->floatValue()F

    move-result v6

    const/4 v3, 0x2

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Number;

    invoke-virtual {v3}, Ljava/lang/Number;->floatValue()F

    move-result v7

    const/4 v3, 0x3

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Number;

    invoke-virtual {v3}, Ljava/lang/Number;->floatValue()F

    move-result v8

    const/4 v3, 0x4

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Number;

    invoke-virtual {v3}, Ljava/lang/Number;->floatValue()F

    move-result v9

    const/4 v3, 0x5

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Number;

    invoke-virtual {v1}, Ljava/lang/Number;->floatValue()F

    move-result v10

    move-object v4, v2

    invoke-direct/range {v4 .. v10}, Lorg/apache/pdfbox/util/Matrix;-><init>(FFFFFF)V

    iput-object v2, v0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType0;->fontMatrix:Lorg/apache/pdfbox/util/Matrix;

    goto :goto_1

    :cond_1
    new-instance v1, Lorg/apache/pdfbox/util/Matrix;

    const v12, 0x3a83126f    # 0.001f

    const/4 v13, 0x0

    const/4 v14, 0x0

    const v15, 0x3a83126f    # 0.001f

    const/16 v16, 0x0

    const/16 v17, 0x0

    move-object v11, v1

    invoke-direct/range {v11 .. v17}, Lorg/apache/pdfbox/util/Matrix;-><init>(FFFFFF)V

    iput-object v1, v0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType0;->fontMatrix:Lorg/apache/pdfbox/util/Matrix;

    :cond_2
    :goto_1
    iget-object v1, v0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType0;->fontMatrix:Lorg/apache/pdfbox/util/Matrix;

    return-object v1
.end method

.method public getHeight(I)F
    .locals 3

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType0;->codeToCID(I)I

    move-result p1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType0;->glyphHeights:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType0;->getType2CharString(I)Lorg/apache/fontbox/cff/Type2CharString;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/fontbox/cff/Type1CharString;->getBounds()Landroid/graphics/RectF;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v0

    iget-object v1, p0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType0;->glyphHeights:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-interface {v1, p1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public getType2CharString(I)Lorg/apache/fontbox/cff/Type2CharString;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType0;->cidFont:Lorg/apache/fontbox/cff/CFFCIDFont;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lorg/apache/fontbox/cff/CFFCIDFont;->getType2CharString(I)Lorg/apache/fontbox/cff/CIDKeyedType2CharString;

    move-result-object p1

    return-object p1

    :cond_0
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType0;->t1Font:Lorg/apache/fontbox/cff/CFFType1Font;

    invoke-virtual {v0, p1}, Lorg/apache/fontbox/cff/CFFType1Font;->getType2CharString(I)Lorg/apache/fontbox/cff/Type2CharString;

    move-result-object p1

    return-object p1
.end method

.method public getWidthFromFont(I)F
    .locals 2

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType0;->codeToCID(I)I

    move-result p1

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType0;->getType2CharString(I)Lorg/apache/fontbox/cff/Type2CharString;

    move-result-object p1

    invoke-virtual {p1}, Lorg/apache/fontbox/cff/Type1CharString;->getWidth()I

    move-result p1

    new-instance v0, Landroid/graphics/PointF;

    int-to-float p1, p1

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Landroid/graphics/PointF;-><init>(FF)V

    iget-object p1, p0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType0;->fontMatrixTransform:Lorg/apache/pdfbox/util/awt/AffineTransform;

    invoke-virtual {p1, v0, v0}, Lorg/apache/pdfbox/util/awt/AffineTransform;->transform(Landroid/graphics/PointF;Landroid/graphics/PointF;)Landroid/graphics/PointF;

    iget p1, v0, Landroid/graphics/PointF;->x:F

    return p1
.end method

.method public isDamaged()Z
    .locals 1

    iget-boolean v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType0;->isDamaged:Z

    return v0
.end method

.method public isEmbedded()Z
    .locals 1

    iget-boolean v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType0;->isEmbedded:Z

    return v0
.end method

.class public interface abstract Lorg/apache/pdfbox/pdmodel/font/PDFontLike;
.super Ljava/lang/Object;
.source "SourceFile"


# virtual methods
.method public abstract getAverageFontWidth()F
.end method

.method public abstract getBoundingBox()Lorg/apache/fontbox/util/BoundingBox;
.end method

.method public abstract getFontDescriptor()Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;
.end method

.method public abstract getFontMatrix()Lorg/apache/pdfbox/util/Matrix;
.end method

.method public abstract getHeight(I)F
.end method

.method public abstract getName()Ljava/lang/String;
.end method

.method public abstract getPositionVector(I)Lorg/apache/pdfbox/util/Vector;
.end method

.method public abstract getWidth(I)F
.end method

.method public abstract getWidthFromFont(I)F
.end method

.method public abstract isDamaged()Z
.end method

.method public abstract isEmbedded()Z
.end method

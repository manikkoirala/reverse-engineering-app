.class abstract Lorg/apache/pdfbox/pdmodel/font/TrueTypeEmbedder;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/pdfbox/pdmodel/font/Subsetter;


# static fields
.field private static final BASE25:Ljava/lang/String; = "BCDEFGHIJKLMNOPQRSTUVWXYZ"

.field private static final ITALIC:I = 0x1

.field private static final OBLIQUE:I = 0x100


# instance fields
.field protected final cmap:Lorg/apache/fontbox/ttf/CmapSubtable;

.field private final document:Lorg/apache/pdfbox/pdmodel/PDDocument;

.field private final embedSubset:Z

.field protected fontDescriptor:Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;

.field private final subsetCodePoints:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field protected ttf:Lorg/apache/fontbox/ttf/TrueTypeFont;


# direct methods
.method public constructor <init>(Lorg/apache/pdfbox/pdmodel/PDDocument;Lorg/apache/pdfbox/cos/COSDictionary;Ljava/io/InputStream;Z)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/TrueTypeEmbedder;->subsetCodePoints:Ljava/util/Set;

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/font/TrueTypeEmbedder;->document:Lorg/apache/pdfbox/pdmodel/PDDocument;

    iput-boolean p4, p0, Lorg/apache/pdfbox/pdmodel/font/TrueTypeEmbedder;->embedSubset:Z

    invoke-virtual {p0, p3}, Lorg/apache/pdfbox/pdmodel/font/TrueTypeEmbedder;->buildFontFile2(Ljava/io/InputStream;)V

    sget-object p1, Lorg/apache/pdfbox/cos/COSName;->BASE_FONT:Lorg/apache/pdfbox/cos/COSName;

    iget-object p3, p0, Lorg/apache/pdfbox/pdmodel/font/TrueTypeEmbedder;->ttf:Lorg/apache/fontbox/ttf/TrueTypeFont;

    invoke-virtual {p3}, Lorg/apache/fontbox/ttf/TrueTypeFont;->getName()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p2, p1, p3}, Lorg/apache/pdfbox/cos/COSDictionary;->setName(Lorg/apache/pdfbox/cos/COSName;Ljava/lang/String;)V

    iget-object p1, p0, Lorg/apache/pdfbox/pdmodel/font/TrueTypeEmbedder;->ttf:Lorg/apache/fontbox/ttf/TrueTypeFont;

    invoke-virtual {p1}, Lorg/apache/fontbox/ttf/TrueTypeFont;->getUnicodeCmap()Lorg/apache/fontbox/ttf/CmapSubtable;

    move-result-object p1

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/font/TrueTypeEmbedder;->cmap:Lorg/apache/fontbox/ttf/CmapSubtable;

    return-void
.end method

.method private createFontDescriptor(Lorg/apache/fontbox/ttf/TrueTypeFont;)Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;
    .locals 8

    new-instance v0, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;

    invoke-direct {v0}, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;-><init>()V

    invoke-virtual {p1}, Lorg/apache/fontbox/ttf/TrueTypeFont;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->setFontName(Ljava/lang/String;)V

    invoke-virtual {p1}, Lorg/apache/fontbox/ttf/TrueTypeFont;->getOS2Windows()Lorg/apache/fontbox/ttf/OS2WindowsMetricsTable;

    move-result-object v1

    invoke-virtual {p1}, Lorg/apache/fontbox/ttf/TrueTypeFont;->getPostScript()Lorg/apache/fontbox/ttf/PostScriptTable;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/fontbox/ttf/PostScriptTable;->getIsFixedPitch()J

    move-result-wide v3

    const-wide/16 v5, 0x0

    cmp-long v3, v3, v5

    const/4 v4, 0x0

    const/4 v5, 0x1

    if-gtz v3, :cond_1

    invoke-virtual {p1}, Lorg/apache/fontbox/ttf/TrueTypeFont;->getHorizontalHeader()Lorg/apache/fontbox/ttf/HorizontalHeaderTable;

    move-result-object v3

    invoke-virtual {v3}, Lorg/apache/fontbox/ttf/HorizontalHeaderTable;->getNumberOfHMetrics()I

    move-result v3

    if-ne v3, v5, :cond_0

    goto :goto_0

    :cond_0
    move v3, v4

    goto :goto_1

    :cond_1
    :goto_0
    move v3, v5

    :goto_1
    invoke-virtual {v0, v3}, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->setFixedPitch(Z)V

    invoke-virtual {v1}, Lorg/apache/fontbox/ttf/OS2WindowsMetricsTable;->getFsSelection()I

    move-result v3

    and-int/lit8 v6, v3, 0x1

    if-eq v6, v3, :cond_3

    and-int/lit16 v6, v3, 0x100

    if-ne v6, v3, :cond_2

    goto :goto_2

    :cond_2
    move v3, v4

    goto :goto_3

    :cond_3
    :goto_2
    move v3, v5

    :goto_3
    invoke-virtual {v0, v3}, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->setItalic(Z)V

    invoke-virtual {v1}, Lorg/apache/fontbox/ttf/OS2WindowsMetricsTable;->getFamilyClass()I

    move-result v3

    if-eq v3, v5, :cond_5

    const/4 v6, 0x7

    if-eq v3, v6, :cond_5

    const/16 v6, 0xa

    if-eq v3, v6, :cond_4

    const/4 v6, 0x3

    if-eq v3, v6, :cond_5

    const/4 v6, 0x4

    if-eq v3, v6, :cond_5

    const/4 v6, 0x5

    if-eq v3, v6, :cond_5

    goto :goto_4

    :cond_4
    invoke-virtual {v0, v5}, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->setScript(Z)V

    goto :goto_4

    :cond_5
    invoke-virtual {v0, v5}, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->setSerif(Z)V

    :goto_4
    invoke-virtual {v1}, Lorg/apache/fontbox/ttf/OS2WindowsMetricsTable;->getWeightClass()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {v0, v3}, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->setFontWeight(F)V

    invoke-virtual {v0, v5}, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->setSymbolic(Z)V

    invoke-virtual {v0, v4}, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->setNonSymbolic(Z)V

    invoke-virtual {v2}, Lorg/apache/fontbox/ttf/PostScriptTable;->getItalicAngle()F

    move-result v2

    invoke-virtual {v0, v2}, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->setItalicAngle(F)V

    invoke-virtual {p1}, Lorg/apache/fontbox/ttf/TrueTypeFont;->getHeader()Lorg/apache/fontbox/ttf/HeaderTable;

    move-result-object v2

    new-instance v3, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;

    invoke-direct {v3}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;-><init>()V

    invoke-virtual {v2}, Lorg/apache/fontbox/ttf/HeaderTable;->getUnitsPerEm()I

    move-result v4

    int-to-float v4, v4

    const/high16 v5, 0x447a0000    # 1000.0f

    div-float/2addr v5, v4

    invoke-virtual {v2}, Lorg/apache/fontbox/ttf/HeaderTable;->getXMin()S

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v4, v5

    invoke-virtual {v3, v4}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->setLowerLeftX(F)V

    invoke-virtual {v2}, Lorg/apache/fontbox/ttf/HeaderTable;->getYMin()S

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v4, v5

    invoke-virtual {v3, v4}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->setLowerLeftY(F)V

    invoke-virtual {v2}, Lorg/apache/fontbox/ttf/HeaderTable;->getXMax()S

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v4, v5

    invoke-virtual {v3, v4}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->setUpperRightX(F)V

    invoke-virtual {v2}, Lorg/apache/fontbox/ttf/HeaderTable;->getYMax()S

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v2, v5

    invoke-virtual {v3, v2}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->setUpperRightY(F)V

    invoke-virtual {v0, v3}, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->setFontBoundingBox(Lorg/apache/pdfbox/pdmodel/common/PDRectangle;)V

    invoke-virtual {p1}, Lorg/apache/fontbox/ttf/TrueTypeFont;->getHorizontalHeader()Lorg/apache/fontbox/ttf/HorizontalHeaderTable;

    move-result-object p1

    invoke-virtual {p1}, Lorg/apache/fontbox/ttf/HorizontalHeaderTable;->getAscender()S

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v2, v5

    invoke-virtual {v0, v2}, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->setAscent(F)V

    invoke-virtual {p1}, Lorg/apache/fontbox/ttf/HorizontalHeaderTable;->getDescender()S

    move-result p1

    int-to-float p1, p1

    mul-float/2addr p1, v5

    invoke-virtual {v0, p1}, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->setDescent(F)V

    invoke-virtual {v1}, Lorg/apache/fontbox/ttf/OS2WindowsMetricsTable;->getVersion()I

    move-result p1

    int-to-double v2, p1

    const-wide v6, 0x3ff3333333333333L    # 1.2

    cmpl-double p1, v2, v6

    if-ltz p1, :cond_6

    invoke-virtual {v1}, Lorg/apache/fontbox/ttf/OS2WindowsMetricsTable;->getCapHeight()I

    move-result p1

    int-to-float p1, p1

    div-float/2addr p1, v5

    invoke-virtual {v0, p1}, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->setCapHeight(F)V

    invoke-virtual {v1}, Lorg/apache/fontbox/ttf/OS2WindowsMetricsTable;->getHeight()I

    move-result p1

    int-to-float p1, p1

    goto :goto_5

    :cond_6
    invoke-virtual {v1}, Lorg/apache/fontbox/ttf/OS2WindowsMetricsTable;->getTypoAscender()I

    move-result p1

    invoke-virtual {v1}, Lorg/apache/fontbox/ttf/OS2WindowsMetricsTable;->getTypoDescender()I

    move-result v2

    add-int/2addr p1, v2

    int-to-float p1, p1

    div-float/2addr p1, v5

    invoke-virtual {v0, p1}, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->setCapHeight(F)V

    invoke-virtual {v1}, Lorg/apache/fontbox/ttf/OS2WindowsMetricsTable;->getTypoAscender()I

    move-result p1

    int-to-float p1, p1

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr p1, v1

    :goto_5
    div-float/2addr p1, v5

    invoke-virtual {v0, p1}, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->setXHeight(F)V

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->getFontBoundingBox()Lorg/apache/pdfbox/pdmodel/common/PDRectangle;

    move-result-object p1

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->getWidth()F

    move-result p1

    const v1, 0x3e051eb8    # 0.13f

    mul-float/2addr p1, v1

    invoke-virtual {v0, p1}, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->setStemV(F)V

    return-object v0
.end method

.method private isEmbeddingPermitted(Lorg/apache/fontbox/ttf/TrueTypeFont;)Z
    .locals 3

    invoke-virtual {p1}, Lorg/apache/fontbox/ttf/TrueTypeFont;->getOS2Windows()Lorg/apache/fontbox/ttf/OS2WindowsMetricsTable;

    move-result-object v0

    const/4 v1, 0x1

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lorg/apache/fontbox/ttf/TrueTypeFont;->getOS2Windows()Lorg/apache/fontbox/ttf/OS2WindowsMetricsTable;

    move-result-object p1

    invoke-virtual {p1}, Lorg/apache/fontbox/ttf/OS2WindowsMetricsTable;->getFsType()S

    move-result p1

    and-int/lit8 p1, p1, 0x8

    and-int/lit8 v0, p1, 0x1

    const/4 v2, 0x0

    if-ne v0, v1, :cond_0

    return v2

    :cond_0
    const/16 v0, 0x200

    and-int/2addr p1, v0

    if-ne p1, v0, :cond_1

    return v2

    :cond_1
    return v1
.end method

.method private isSubsettingPermitted(Lorg/apache/fontbox/ttf/TrueTypeFont;)Z
    .locals 1

    invoke-virtual {p1}, Lorg/apache/fontbox/ttf/TrueTypeFont;->getOS2Windows()Lorg/apache/fontbox/ttf/OS2WindowsMetricsTable;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lorg/apache/fontbox/ttf/TrueTypeFont;->getOS2Windows()Lorg/apache/fontbox/ttf/OS2WindowsMetricsTable;

    move-result-object p1

    invoke-virtual {p1}, Lorg/apache/fontbox/ttf/OS2WindowsMetricsTable;->getFsType()S

    move-result p1

    const/16 v0, 0x100

    and-int/2addr p1, v0

    if-ne p1, v0, :cond_0

    const/4 p1, 0x0

    return p1

    :cond_0
    const/4 p1, 0x1

    return p1
.end method


# virtual methods
.method public addToSubset(I)V
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/TrueTypeEmbedder;->subsetCodePoints:Ljava/util/Set;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public buildFontFile2(Ljava/io/InputStream;)V
    .locals 3

    new-instance v0, Lorg/apache/pdfbox/pdmodel/common/PDStream;

    iget-object v1, p0, Lorg/apache/pdfbox/pdmodel/font/TrueTypeEmbedder;->document:Lorg/apache/pdfbox/pdmodel/PDDocument;

    const/4 v2, 0x0

    invoke-direct {v0, v1, p1, v2}, Lorg/apache/pdfbox/pdmodel/common/PDStream;-><init>(Lorg/apache/pdfbox/pdmodel/PDDocument;Ljava/io/InputStream;Z)V

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/common/PDStream;->getStream()Lorg/apache/pdfbox/cos/COSStream;

    move-result-object p1

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->LENGTH1:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/common/PDStream;->getByteArray()[B

    move-result-object v2

    array-length v2, v2

    invoke-virtual {p1, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->setInt(Lorg/apache/pdfbox/cos/COSName;I)V

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/common/PDStream;->addCompression()V

    :try_start_0
    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/common/PDStream;->createInputStream()Ljava/io/InputStream;

    move-result-object p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    new-instance v1, Lorg/apache/fontbox/ttf/TTFParser;

    invoke-direct {v1}, Lorg/apache/fontbox/ttf/TTFParser;-><init>()V

    invoke-virtual {v1, p1}, Lorg/apache/fontbox/ttf/TTFParser;->parseEmbedded(Ljava/io/InputStream;)Lorg/apache/fontbox/ttf/TrueTypeFont;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/pdfbox/pdmodel/font/TrueTypeEmbedder;->ttf:Lorg/apache/fontbox/ttf/TrueTypeFont;

    invoke-direct {p0, v1}, Lorg/apache/pdfbox/pdmodel/font/TrueTypeEmbedder;->isEmbeddingPermitted(Lorg/apache/fontbox/ttf/TrueTypeFont;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lorg/apache/pdfbox/pdmodel/font/TrueTypeEmbedder;->fontDescriptor:Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;

    if-nez v1, :cond_0

    iget-object v1, p0, Lorg/apache/pdfbox/pdmodel/font/TrueTypeEmbedder;->ttf:Lorg/apache/fontbox/ttf/TrueTypeFont;

    invoke-direct {p0, v1}, Lorg/apache/pdfbox/pdmodel/font/TrueTypeEmbedder;->createFontDescriptor(Lorg/apache/fontbox/ttf/TrueTypeFont;)Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/pdfbox/pdmodel/font/TrueTypeEmbedder;->fontDescriptor:Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_0
    invoke-static {p1}, Lorg/apache/pdfbox/io/IOUtils;->closeQuietly(Ljava/io/Closeable;)V

    iget-object p1, p0, Lorg/apache/pdfbox/pdmodel/font/TrueTypeEmbedder;->fontDescriptor:Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;

    invoke-virtual {p1, v0}, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->setFontFile2(Lorg/apache/pdfbox/pdmodel/common/PDStream;)V

    return-void

    :cond_1
    :try_start_2
    new-instance v0, Ljava/io/IOException;

    const-string v1, "This font does not permit embedding"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catchall_0
    move-exception v0

    goto :goto_0

    :catchall_1
    move-exception v0

    const/4 p1, 0x0

    :goto_0
    invoke-static {p1}, Lorg/apache/pdfbox/io/IOUtils;->closeQuietly(Ljava/io/Closeable;)V

    throw v0
.end method

.method public abstract buildSubset(Ljava/io/InputStream;Ljava/lang/String;Ljava/util/Map;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/InputStream;",
            "Ljava/lang/String;",
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation
.end method

.method public getFontDescriptor()Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/TrueTypeEmbedder;->fontDescriptor:Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;

    return-object v0
.end method

.method public getTag(Ljava/util/Map;)Ljava/lang/String;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    invoke-interface {p1}, Ljava/util/Map;->hashCode()I

    move-result p1

    int-to-long v0, p1

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    :goto_0
    const-wide/16 v2, 0x19

    div-long v4, v0, v2

    rem-long/2addr v0, v2

    long-to-int v0, v0

    const-string v1, "BCDEFGHIJKLMNOPQRSTUVWXYZ"

    invoke-virtual {v1, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-wide/16 v0, 0x0

    cmp-long v0, v4, v0

    const/4 v1, 0x6

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-lt v0, v1, :cond_0

    goto :goto_1

    :cond_0
    move-wide v0, v4

    goto :goto_0

    :cond_1
    :goto_1
    invoke-virtual {p1}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-ge v0, v1, :cond_2

    const/4 v0, 0x0

    const/16 v2, 0x41

    invoke-virtual {p1, v0, v2}, Ljava/lang/StringBuilder;->insert(IC)Ljava/lang/StringBuilder;

    goto :goto_1

    :cond_2
    const/16 v0, 0x2b

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public getTrueTypeFont()Lorg/apache/fontbox/ttf/TrueTypeFont;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/TrueTypeEmbedder;->ttf:Lorg/apache/fontbox/ttf/TrueTypeFont;

    return-object v0
.end method

.method public needsSubset()Z
    .locals 1

    iget-boolean v0, p0, Lorg/apache/pdfbox/pdmodel/font/TrueTypeEmbedder;->embedSubset:Z

    return v0
.end method

.method public subset()V
    .locals 4

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/TrueTypeEmbedder;->ttf:Lorg/apache/fontbox/ttf/TrueTypeFont;

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/pdmodel/font/TrueTypeEmbedder;->isSubsettingPermitted(Lorg/apache/fontbox/ttf/TrueTypeFont;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lorg/apache/pdfbox/pdmodel/font/TrueTypeEmbedder;->embedSubset:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    const-string v1, "head"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string v1, "hhea"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string v1, "loca"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string v1, "maxp"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string v1, "cvt "

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string v1, "prep"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string v1, "glyf"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string v1, "hmtx"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string v1, "fpgm"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string v1, "gasp"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v1, Lorg/apache/fontbox/ttf/TTFSubsetter;

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/TrueTypeEmbedder;->getTrueTypeFont()Lorg/apache/fontbox/ttf/TrueTypeFont;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lorg/apache/fontbox/ttf/TTFSubsetter;-><init>(Lorg/apache/fontbox/ttf/TrueTypeFont;Ljava/util/List;)V

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/TrueTypeEmbedder;->subsetCodePoints:Ljava/util/Set;

    invoke-virtual {v1, v0}, Lorg/apache/fontbox/ttf/TTFSubsetter;->addAll(Ljava/util/Set;)V

    invoke-virtual {v1}, Lorg/apache/fontbox/ttf/TTFSubsetter;->getGIDMap()Ljava/util/Map;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/apache/pdfbox/pdmodel/font/TrueTypeEmbedder;->getTag(Ljava/util/Map;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lorg/apache/fontbox/ttf/TTFSubsetter;->setPrefix(Ljava/lang/String;)V

    new-instance v3, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v3}, Ljava/io/ByteArrayOutputStream;-><init>()V

    invoke-virtual {v1, v3}, Lorg/apache/fontbox/ttf/TTFSubsetter;->writeToStream(Ljava/io/OutputStream;)V

    new-instance v1, Ljava/io/ByteArrayInputStream;

    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-virtual {p0, v1, v2, v0}, Lorg/apache/pdfbox/pdmodel/font/TrueTypeEmbedder;->buildSubset(Ljava/io/InputStream;Ljava/lang/String;Ljava/util/Map;)V

    return-void

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Subsetting is disabled"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    new-instance v0, Ljava/io/IOException;

    const-string v1, "This font does not permit subsetting"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

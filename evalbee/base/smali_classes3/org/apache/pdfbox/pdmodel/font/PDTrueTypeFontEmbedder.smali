.class final Lorg/apache/pdfbox/pdmodel/font/PDTrueTypeFontEmbedder;
.super Lorg/apache/pdfbox/pdmodel/font/TrueTypeEmbedder;
.source "SourceFile"


# instance fields
.field private final fontEncoding:Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;


# direct methods
.method public constructor <init>(Lorg/apache/pdfbox/pdmodel/PDDocument;Lorg/apache/pdfbox/cos/COSDictionary;Ljava/io/InputStream;)V
    .locals 2

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lorg/apache/pdfbox/pdmodel/font/TrueTypeEmbedder;-><init>(Lorg/apache/pdfbox/pdmodel/PDDocument;Lorg/apache/pdfbox/cos/COSDictionary;Ljava/io/InputStream;Z)V

    sget-object p1, Lorg/apache/pdfbox/cos/COSName;->SUBTYPE:Lorg/apache/pdfbox/cos/COSName;

    sget-object p3, Lorg/apache/pdfbox/cos/COSName;->TRUE_TYPE:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p2, p1, p3}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    new-instance p1, Lorg/apache/pdfbox/pdmodel/font/encoding/WinAnsiEncoding;

    invoke-direct {p1}, Lorg/apache/pdfbox/pdmodel/font/encoding/WinAnsiEncoding;-><init>()V

    invoke-static {}, Lorg/apache/pdfbox/pdmodel/font/encoding/GlyphList;->getAdobeGlyphList()Lorg/apache/pdfbox/pdmodel/font/encoding/GlyphList;

    move-result-object p3

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/font/PDTrueTypeFontEmbedder;->fontEncoding:Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->ENCODING:Lorg/apache/pdfbox/cos/COSName;

    invoke-interface {p1}, Lorg/apache/pdfbox/pdmodel/common/COSObjectable;->getCOSObject()Lorg/apache/pdfbox/cos/COSBase;

    move-result-object p1

    invoke-virtual {p2, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    iget-object p1, p0, Lorg/apache/pdfbox/pdmodel/font/TrueTypeEmbedder;->fontDescriptor:Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;

    invoke-virtual {p1, v0}, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->setSymbolic(Z)V

    iget-object p1, p0, Lorg/apache/pdfbox/pdmodel/font/TrueTypeEmbedder;->fontDescriptor:Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->setNonSymbolic(Z)V

    sget-object p1, Lorg/apache/pdfbox/cos/COSName;->FONT_DESC:Lorg/apache/pdfbox/cos/COSName;

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/TrueTypeEmbedder;->fontDescriptor:Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;

    invoke-virtual {p2, p1, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)V

    invoke-direct {p0, p2, p3}, Lorg/apache/pdfbox/pdmodel/font/PDTrueTypeFontEmbedder;->setWidths(Lorg/apache/pdfbox/cos/COSDictionary;Lorg/apache/pdfbox/pdmodel/font/encoding/GlyphList;)V

    return-void
.end method

.method private setWidths(Lorg/apache/pdfbox/cos/COSDictionary;Lorg/apache/pdfbox/pdmodel/font/encoding/GlyphList;)V
    .locals 10

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/TrueTypeEmbedder;->ttf:Lorg/apache/fontbox/ttf/TrueTypeFont;

    invoke-virtual {v0}, Lorg/apache/fontbox/ttf/TrueTypeFont;->getHeader()Lorg/apache/fontbox/ttf/HeaderTable;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/fontbox/ttf/HeaderTable;->getUnitsPerEm()I

    move-result v0

    int-to-float v0, v0

    const/high16 v1, 0x447a0000    # 1000.0f

    div-float/2addr v1, v0

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/TrueTypeEmbedder;->ttf:Lorg/apache/fontbox/ttf/TrueTypeFont;

    invoke-virtual {v0}, Lorg/apache/fontbox/ttf/TrueTypeFont;->getHorizontalMetrics()Lorg/apache/fontbox/ttf/HorizontalMetricsTable;

    move-result-object v0

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDTrueTypeFontEmbedder;->getFontEncoding()Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->getCodeToNameMap()Ljava/util/Map;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-static {v3}, Ljava/util/Collections;->min(Ljava/util/Collection;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-interface {v2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v4

    invoke-static {v4}, Ljava/util/Collections;->max(Ljava/util/Collection;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    new-instance v5, Ljava/util/ArrayList;

    sub-int v6, v4, v3

    add-int/lit8 v6, v6, 0x1

    invoke-direct {v5, v6}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v7, 0x0

    move v8, v7

    :goto_0
    if-ge v8, v6, :cond_0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-interface {v5, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    :cond_0
    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/Map$Entry;

    invoke-interface {v6}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Integer;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    invoke-interface {v6}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/String;

    if-lt v8, v3, :cond_1

    if-gt v8, v4, :cond_1

    invoke-virtual {p2, v9}, Lorg/apache/pdfbox/pdmodel/font/encoding/GlyphList;->toUnicode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8, v7}, Ljava/lang/String;->codePointAt(I)I

    move-result v8

    iget-object v9, p0, Lorg/apache/pdfbox/pdmodel/font/TrueTypeEmbedder;->cmap:Lorg/apache/fontbox/ttf/CmapSubtable;

    invoke-virtual {v9, v8}, Lorg/apache/fontbox/ttf/CmapSubtable;->getGlyphId(I)I

    move-result v8

    invoke-interface {v6}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    sub-int/2addr v6, v3

    invoke-virtual {v0, v8}, Lorg/apache/fontbox/ttf/HorizontalMetricsTable;->getAdvanceWidth(I)I

    move-result v8

    int-to-float v8, v8

    mul-float/2addr v8, v1

    invoke-static {v8}, Ljava/lang/Math;->round(F)I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-interface {v5, v6, v8}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_2
    sget-object p2, Lorg/apache/pdfbox/cos/COSName;->FIRST_CHAR:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p1, p2, v3}, Lorg/apache/pdfbox/cos/COSDictionary;->setInt(Lorg/apache/pdfbox/cos/COSName;I)V

    sget-object p2, Lorg/apache/pdfbox/cos/COSName;->LAST_CHAR:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p1, p2, v4}, Lorg/apache/pdfbox/cos/COSDictionary;->setInt(Lorg/apache/pdfbox/cos/COSName;I)V

    sget-object p2, Lorg/apache/pdfbox/cos/COSName;->WIDTHS:Lorg/apache/pdfbox/cos/COSName;

    invoke-static {v5}, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;->converterToCOSArray(Ljava/util/List;)Lorg/apache/pdfbox/cos/COSArray;

    move-result-object v0

    invoke-virtual {p1, p2, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    return-void
.end method


# virtual methods
.method public buildSubset(Ljava/io/InputStream;Ljava/lang/String;Ljava/util/Map;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/InputStream;",
            "Ljava/lang/String;",
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    new-instance p1, Ljava/lang/UnsupportedOperationException;

    invoke-direct {p1}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw p1
.end method

.method public getFontEncoding()Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDTrueTypeFontEmbedder;->fontEncoding:Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;

    return-object v0
.end method

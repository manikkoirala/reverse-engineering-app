.class public final Lorg/apache/pdfbox/pdmodel/font/PDType3CharProc;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/pdfbox/pdmodel/common/COSObjectable;
.implements Lorg/apache/pdfbox/contentstream/PDContentStream;


# instance fields
.field private final charStream:Lorg/apache/pdfbox/cos/COSStream;

.field private final font:Lorg/apache/pdfbox/pdmodel/font/PDType3Font;


# direct methods
.method public constructor <init>(Lorg/apache/pdfbox/pdmodel/font/PDType3Font;Lorg/apache/pdfbox/cos/COSStream;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/font/PDType3CharProc;->font:Lorg/apache/pdfbox/pdmodel/font/PDType3Font;

    iput-object p2, p0, Lorg/apache/pdfbox/pdmodel/font/PDType3CharProc;->charStream:Lorg/apache/pdfbox/cos/COSStream;

    return-void
.end method


# virtual methods
.method public getBBox()Lorg/apache/pdfbox/pdmodel/common/PDRectangle;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDType3CharProc;->font:Lorg/apache/pdfbox/pdmodel/font/PDType3Font;

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/font/PDType3Font;->getFontBBox()Lorg/apache/pdfbox/pdmodel/common/PDRectangle;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getCOSObject()Lorg/apache/pdfbox/cos/COSBase;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDType3CharProc;->getCOSObject()Lorg/apache/pdfbox/cos/COSStream;

    move-result-object v0

    return-object v0
.end method

.method public getCOSObject()Lorg/apache/pdfbox/cos/COSStream;
    .locals 1

    .line 2
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDType3CharProc;->charStream:Lorg/apache/pdfbox/cos/COSStream;

    return-object v0
.end method

.method public getContentStream()Lorg/apache/pdfbox/cos/COSStream;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDType3CharProc;->charStream:Lorg/apache/pdfbox/cos/COSStream;

    return-object v0
.end method

.method public getFont()Lorg/apache/pdfbox/pdmodel/font/PDType3Font;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDType3CharProc;->font:Lorg/apache/pdfbox/pdmodel/font/PDType3Font;

    return-object v0
.end method

.method public getMatrix()Lorg/apache/pdfbox/util/Matrix;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDType3CharProc;->font:Lorg/apache/pdfbox/pdmodel/font/PDType3Font;

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/font/PDType3Font;->getFontMatrix()Lorg/apache/pdfbox/util/Matrix;

    move-result-object v0

    return-object v0
.end method

.method public getResources()Lorg/apache/pdfbox/pdmodel/PDResources;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDType3CharProc;->font:Lorg/apache/pdfbox/pdmodel/font/PDType3Font;

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/font/PDType3Font;->getResources()Lorg/apache/pdfbox/pdmodel/PDResources;

    move-result-object v0

    return-object v0
.end method

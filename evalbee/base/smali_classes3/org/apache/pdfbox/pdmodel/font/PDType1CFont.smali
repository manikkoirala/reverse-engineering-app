.class public Lorg/apache/pdfbox/pdmodel/font/PDType1CFont;
.super Lorg/apache/pdfbox/pdmodel/font/PDSimpleFont;
.source "SourceFile"

# interfaces
.implements Lorg/apache/pdfbox/pdmodel/font/PDType1Equivalent;


# instance fields
.field private avgWidth:Ljava/lang/Float;

.field private final cffFont:Lorg/apache/fontbox/cff/CFFType1Font;

.field private fontMatrix:Lorg/apache/pdfbox/util/Matrix;

.field private final fontMatrixTransform:Lorg/apache/pdfbox/util/awt/AffineTransform;

.field private final glyphHeights:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private final isDamaged:Z

.field private final isEmbedded:Z

.field private final type1Equivalent:Lorg/apache/fontbox/ttf/Type1Equivalent;


# direct methods
.method public constructor <init>(Lorg/apache/pdfbox/cos/COSDictionary;)V
    .locals 6

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/font/PDSimpleFont;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/font/PDType1CFont;->glyphHeights:Ljava/util/Map;

    const/4 p1, 0x0

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/font/PDType1CFont;->avgWidth:Ljava/lang/Float;

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDFont;->getFontDescriptor()Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;

    move-result-object v0

    const-string v1, "PdfBoxAndroid"

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->getFontFile3()Lorg/apache/pdfbox/pdmodel/common/PDStream;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/common/PDStream;->createInputStream()Ljava/io/InputStream;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/pdfbox/io/IOUtils;->toByteArray(Ljava/io/InputStream;)[B

    move-result-object v0

    array-length v2, v0

    if-nez v2, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid data for embedded Type1C font "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDType1CFont;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    move-object v0, p1

    :cond_1
    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz v0, :cond_2

    :try_start_0
    new-instance v4, Lorg/apache/fontbox/cff/CFFParser;

    invoke-direct {v4}, Lorg/apache/fontbox/cff/CFFParser;-><init>()V

    invoke-virtual {v4, v0}, Lorg/apache/fontbox/cff/CFFParser;->parse([B)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/fontbox/cff/CFFType1Font;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-object p1, v0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Can\'t read the embedded Type1C font "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDType1CFont;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move v0, v2

    goto :goto_1

    :cond_2
    :goto_0
    move v0, v3

    :goto_1
    iput-boolean v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDType1CFont;->isDamaged:Z

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/font/PDType1CFont;->cffFont:Lorg/apache/fontbox/cff/CFFType1Font;

    if-eqz p1, :cond_3

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/font/PDType1CFont;->type1Equivalent:Lorg/apache/fontbox/ttf/Type1Equivalent;

    iput-boolean v2, p0, Lorg/apache/pdfbox/pdmodel/font/PDType1CFont;->isEmbedded:Z

    goto :goto_3

    :cond_3
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDType1CFont;->getBaseFont()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lorg/apache/pdfbox/pdmodel/font/ExternalFonts;->getType1EquivalentFont(Ljava/lang/String;)Lorg/apache/fontbox/ttf/Type1Equivalent;

    move-result-object p1

    if-eqz p1, :cond_4

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/font/PDType1CFont;->type1Equivalent:Lorg/apache/fontbox/ttf/Type1Equivalent;

    goto :goto_2

    :cond_4
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDFont;->getFontDescriptor()Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;

    move-result-object p1

    invoke-static {p1}, Lorg/apache/pdfbox/pdmodel/font/ExternalFonts;->getType1FallbackFont(Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;)Lorg/apache/fontbox/ttf/Type1Equivalent;

    move-result-object p1

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/font/PDType1CFont;->type1Equivalent:Lorg/apache/fontbox/ttf/Type1Equivalent;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Using fallback font "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {p1}, Lorg/apache/fontbox/ttf/Type1Equivalent;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, " for "

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDType1CFont;->getBaseFont()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v1, p1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_2
    iput-boolean v3, p0, Lorg/apache/pdfbox/pdmodel/font/PDType1CFont;->isEmbedded:Z

    :goto_3
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDSimpleFont;->readEncoding()V

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDType1CFont;->getFontMatrix()Lorg/apache/pdfbox/util/Matrix;

    move-result-object p1

    invoke-virtual {p1}, Lorg/apache/pdfbox/util/Matrix;->createAffineTransform()Lorg/apache/pdfbox/util/awt/AffineTransform;

    move-result-object p1

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/font/PDType1CFont;->fontMatrixTransform:Lorg/apache/pdfbox/util/awt/AffineTransform;

    const-wide v0, 0x408f400000000000L    # 1000.0

    invoke-virtual {p1, v0, v1, v0, v1}, Lorg/apache/pdfbox/util/awt/AffineTransform;->scale(DD)V

    return-void
.end method

.method private getAverageCharacterWidth()F
    .locals 1

    const/high16 v0, 0x43fa0000    # 500.0f

    return v0
.end method


# virtual methods
.method public codeToName(I)Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDSimpleFont;->getEncoding()Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->getName(I)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public encode(I)[B
    .locals 1

    new-instance p1, Ljava/lang/UnsupportedOperationException;

    const-string v0, "Not implemented: Type1C"

    invoke-direct {p1, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public getAverageFontWidth()F
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDType1CFont;->avgWidth:Ljava/lang/Float;

    if-nez v0, :cond_0

    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/font/PDType1CFont;->getAverageCharacterWidth()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDType1CFont;->avgWidth:Ljava/lang/Float;

    :cond_0
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDType1CFont;->avgWidth:Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    return v0
.end method

.method public getBaseFont()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDFont;->dict:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->BASE_FONT:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getNameAsString(Lorg/apache/pdfbox/cos/COSName;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getBoundingBox()Lorg/apache/fontbox/util/BoundingBox;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDType1CFont;->type1Equivalent:Lorg/apache/fontbox/ttf/Type1Equivalent;

    invoke-interface {v0}, Lorg/apache/fontbox/ttf/Type1Equivalent;->getFontBBox()Lorg/apache/fontbox/util/BoundingBox;

    move-result-object v0

    return-object v0
.end method

.method public getCFFType1Font()Lorg/apache/fontbox/cff/CFFType1Font;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDType1CFont;->cffFont:Lorg/apache/fontbox/cff/CFFType1Font;

    return-object v0
.end method

.method public getFontMatrix()Lorg/apache/pdfbox/util/Matrix;
    .locals 10

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDType1CFont;->fontMatrix:Lorg/apache/pdfbox/util/Matrix;

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDType1CFont;->cffFont:Lorg/apache/fontbox/cff/CFFType1Font;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lorg/apache/fontbox/cff/CFFType1Font;->getFontMatrix()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x6

    if-ne v1, v2, :cond_0

    new-instance v1, Lorg/apache/pdfbox/util/Matrix;

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Number;

    invoke-virtual {v2}, Ljava/lang/Number;->floatValue()F

    move-result v4

    const/4 v2, 0x1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Number;

    invoke-virtual {v2}, Ljava/lang/Number;->floatValue()F

    move-result v5

    const/4 v2, 0x2

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Number;

    invoke-virtual {v2}, Ljava/lang/Number;->floatValue()F

    move-result v6

    const/4 v2, 0x3

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Number;

    invoke-virtual {v2}, Ljava/lang/Number;->floatValue()F

    move-result v7

    const/4 v2, 0x4

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Number;

    invoke-virtual {v2}, Ljava/lang/Number;->floatValue()F

    move-result v8

    const/4 v2, 0x5

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->floatValue()F

    move-result v9

    move-object v3, v1

    invoke-direct/range {v3 .. v9}, Lorg/apache/pdfbox/util/Matrix;-><init>(FFFFFF)V

    iput-object v1, p0, Lorg/apache/pdfbox/pdmodel/font/PDType1CFont;->fontMatrix:Lorg/apache/pdfbox/util/Matrix;

    return-object v1

    :cond_0
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDType1CFont;->fontMatrix:Lorg/apache/pdfbox/util/Matrix;

    return-object v0
.end method

.method public getHeight(I)F
    .locals 3

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/pdmodel/font/PDType1CFont;->codeToName(I)Ljava/lang/String;

    move-result-object p1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDType1CFont;->glyphHeights:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDType1CFont;->cffFont:Lorg/apache/fontbox/cff/CFFType1Font;

    invoke-virtual {v0, p1}, Lorg/apache/fontbox/cff/CFFType1Font;->getType1CharString(Ljava/lang/String;)Lorg/apache/fontbox/cff/Type1CharString;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/fontbox/cff/Type1CharString;->getBounds()Landroid/graphics/RectF;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v0

    iget-object v1, p0, Lorg/apache/pdfbox/pdmodel/font/PDType1CFont;->glyphHeights:Ljava/util/Map;

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-interface {v1, p1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDType1CFont;->getBaseFont()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPath(Ljava/lang/String;)Landroid/graphics/Path;
    .locals 1

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDType1CFont;->isEmbedded()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, ".notdef"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDType1CFont;->isEmbedded()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDSimpleFont;->isStandard14()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance p1, Landroid/graphics/Path;

    invoke-direct {p1}, Landroid/graphics/Path;-><init>()V

    return-object p1

    :cond_0
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDType1CFont;->type1Equivalent:Lorg/apache/fontbox/ttf/Type1Equivalent;

    invoke-interface {v0, p1}, Lorg/apache/fontbox/ttf/Type1Equivalent;->getPath(Ljava/lang/String;)Landroid/graphics/Path;

    move-result-object p1

    return-object p1
.end method

.method public getStringWidth(Ljava/lang/String;)F
    .locals 4

    const/4 v0, 0x0

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v1, v2, :cond_0

    invoke-virtual {p1, v1}, Ljava/lang/String;->codePointAt(I)I

    move-result v2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDSimpleFont;->getGlyphList()Lorg/apache/pdfbox/pdmodel/font/encoding/GlyphList;

    move-result-object v3

    invoke-virtual {v3, v2}, Lorg/apache/pdfbox/pdmodel/font/encoding/GlyphList;->codePointToName(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lorg/apache/pdfbox/pdmodel/font/PDType1CFont;->cffFont:Lorg/apache/fontbox/cff/CFFType1Font;

    invoke-virtual {v3, v2}, Lorg/apache/fontbox/cff/CFFType1Font;->getType1CharString(Ljava/lang/String;)Lorg/apache/fontbox/cff/Type1CharString;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/fontbox/cff/Type1CharString;->getWidth()I

    move-result v2

    int-to-float v2, v2

    add-float/2addr v0, v2

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return v0
.end method

.method public getType1Equivalent()Lorg/apache/fontbox/ttf/Type1Equivalent;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDType1CFont;->type1Equivalent:Lorg/apache/fontbox/ttf/Type1Equivalent;

    return-object v0
.end method

.method public getWidthFromFont(I)F
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDFont;->getStandard14AFM()Lorg/apache/fontbox/afm/FontMetrics;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/pdmodel/font/PDSimpleFont;->getStandard14Width(I)F

    move-result p1

    return p1

    :cond_0
    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/pdmodel/font/PDType1CFont;->codeToName(I)Ljava/lang/String;

    move-result-object p1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDType1CFont;->type1Equivalent:Lorg/apache/fontbox/ttf/Type1Equivalent;

    invoke-interface {v0, p1}, Lorg/apache/fontbox/ttf/Type1Equivalent;->getWidth(Ljava/lang/String;)F

    move-result p1

    new-instance v0, Landroid/graphics/PointF;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Landroid/graphics/PointF;-><init>(FF)V

    iget-object p1, p0, Lorg/apache/pdfbox/pdmodel/font/PDType1CFont;->fontMatrixTransform:Lorg/apache/pdfbox/util/awt/AffineTransform;

    invoke-virtual {p1, v0, v0}, Lorg/apache/pdfbox/util/awt/AffineTransform;->transform(Landroid/graphics/PointF;Landroid/graphics/PointF;)Landroid/graphics/PointF;

    iget p1, v0, Landroid/graphics/PointF;->x:F

    return p1
.end method

.method public isDamaged()Z
    .locals 1

    iget-boolean v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDType1CFont;->isDamaged:Z

    return v0
.end method

.method public isEmbedded()Z
    .locals 1

    iget-boolean v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDType1CFont;->isEmbedded:Z

    return v0
.end method

.method public readCode(Ljava/io/InputStream;)I
    .locals 0

    invoke-virtual {p1}, Ljava/io/InputStream;->read()I

    move-result p1

    return p1
.end method

.method public readEncodingFromFont()Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDType1CFont;->type1Equivalent:Lorg/apache/fontbox/ttf/Type1Equivalent;

    invoke-interface {v0}, Lorg/apache/fontbox/ttf/Type1Equivalent;->getEncoding()Lorg/apache/fontbox/encoding/Encoding;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/pdfbox/pdmodel/font/encoding/Type1Encoding;->fromFontBox(Lorg/apache/fontbox/encoding/Encoding;)Lorg/apache/pdfbox/pdmodel/font/encoding/Type1Encoding;

    move-result-object v0

    return-object v0
.end method

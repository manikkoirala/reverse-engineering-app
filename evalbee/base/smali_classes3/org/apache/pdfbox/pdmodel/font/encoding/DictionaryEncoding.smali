.class public Lorg/apache/pdfbox/pdmodel/font/encoding/DictionaryEncoding;
.super Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;
.source "SourceFile"


# instance fields
.field private final baseEncoding:Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;

.field private final differences:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final encoding:Lorg/apache/pdfbox/cos/COSDictionary;


# direct methods
.method public constructor <init>(Lorg/apache/pdfbox/cos/COSDictionary;ZLorg/apache/pdfbox/pdmodel/font/encoding/Encoding;)V
    .locals 3

    .line 1
    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/encoding/DictionaryEncoding;->differences:Ljava/util/Map;

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/font/encoding/DictionaryEncoding;->encoding:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->BASE_ENCODING:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p1, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->containsKey(Lorg/apache/pdfbox/cos/COSName;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p1, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->getCOSName(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSName;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->getInstance(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    if-nez v0, :cond_3

    if-eqz p2, :cond_1

    sget-object p3, Lorg/apache/pdfbox/pdmodel/font/encoding/StandardEncoding;->INSTANCE:Lorg/apache/pdfbox/pdmodel/font/encoding/StandardEncoding;

    goto :goto_1

    :cond_1
    if-eqz p3, :cond_2

    goto :goto_1

    :cond_2
    sget-object p3, Lorg/apache/pdfbox/pdmodel/font/encoding/StandardEncoding;->INSTANCE:Lorg/apache/pdfbox/pdmodel/font/encoding/StandardEncoding;

    const-string p2, "PdfBoxAndroid"

    const-string v0, "Built-in encoding required for symbolic font, using standard encoding"

    invoke-static {p2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_3
    move-object p3, v0

    :goto_1
    iput-object p3, p0, Lorg/apache/pdfbox/pdmodel/font/encoding/DictionaryEncoding;->baseEncoding:Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;

    iget-object p2, p0, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->codeToName:Ljava/util/Map;

    iget-object v0, p3, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->codeToName:Ljava/util/Map;

    invoke-interface {p2, v0}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    iget-object p2, p0, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->names:Ljava/util/Set;

    iget-object p3, p3, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->names:Ljava/util/Set;

    invoke-interface {p2, p3}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    sget-object p2, Lorg/apache/pdfbox/cos/COSName;->DIFFERENCES:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p1, p2}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object p1

    check-cast p1, Lorg/apache/pdfbox/cos/COSArray;

    const/4 p2, -0x1

    const/4 p3, 0x0

    :goto_2
    if-eqz p1, :cond_6

    invoke-virtual {p1}, Lorg/apache/pdfbox/cos/COSArray;->size()I

    move-result v0

    if-ge p3, v0, :cond_6

    invoke-virtual {p1, p3}, Lorg/apache/pdfbox/cos/COSArray;->getObject(I)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    instance-of v1, v0, Lorg/apache/pdfbox/cos/COSNumber;

    if-eqz v1, :cond_4

    check-cast v0, Lorg/apache/pdfbox/cos/COSNumber;

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSNumber;->intValue()I

    move-result p2

    goto :goto_3

    :cond_4
    instance-of v1, v0, Lorg/apache/pdfbox/cos/COSName;

    if-eqz v1, :cond_5

    check-cast v0, Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSName;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, p2, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    iget-object v1, p0, Lorg/apache/pdfbox/pdmodel/font/encoding/DictionaryEncoding;->differences:Ljava/util/Map;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSName;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 p2, p2, 0x1

    :cond_5
    :goto_3
    add-int/lit8 p3, p3, 0x1

    goto :goto_2

    :cond_6
    return-void
.end method

.method public constructor <init>(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSArray;)V
    .locals 3

    .line 2
    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/encoding/DictionaryEncoding;->differences:Ljava/util/Map;

    new-instance v0, Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-direct {v0}, Lorg/apache/pdfbox/cos/COSDictionary;-><init>()V

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/encoding/DictionaryEncoding;->encoding:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->NAME:Lorg/apache/pdfbox/cos/COSName;

    sget-object v2, Lorg/apache/pdfbox/cos/COSName;->ENCODING:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->DIFFERENCES:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p2}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    sget-object p2, Lorg/apache/pdfbox/cos/COSName;->STANDARD_ENCODING:Lorg/apache/pdfbox/cos/COSName;

    if-eq p1, p2, :cond_0

    sget-object p2, Lorg/apache/pdfbox/cos/COSName;->BASE_ENCODING:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, p2, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    :cond_0
    invoke-static {p1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->getInstance(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;

    move-result-object p2

    iput-object p2, p0, Lorg/apache/pdfbox/pdmodel/font/encoding/DictionaryEncoding;->baseEncoding:Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;

    iget-object p2, p0, Lorg/apache/pdfbox/pdmodel/font/encoding/DictionaryEncoding;->baseEncoding:Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;

    if-eqz p2, :cond_1

    return-void

    :cond_1
    new-instance p2, Ljava/lang/IllegalArgumentException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Invalid encoding: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p2
.end method


# virtual methods
.method public getBaseEncoding()Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/encoding/DictionaryEncoding;->baseEncoding:Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;

    return-object v0
.end method

.method public getCOSObject()Lorg/apache/pdfbox/cos/COSBase;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/encoding/DictionaryEncoding;->encoding:Lorg/apache/pdfbox/cos/COSDictionary;

    return-object v0
.end method

.method public getDifferences()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/encoding/DictionaryEncoding;->differences:Ljava/util/Map;

    return-object v0
.end method

.class public abstract Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/pdfbox/pdmodel/common/COSObjectable;


# instance fields
.field protected final codeToName:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field protected final names:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->codeToName:Ljava/util/Map;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->names:Ljava/util/Set;

    return-void
.end method

.method public static getInstance(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;
    .locals 1

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->STANDARD_ENCODING:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, p0}, Lorg/apache/pdfbox/cos/COSName;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object p0, Lorg/apache/pdfbox/pdmodel/font/encoding/StandardEncoding;->INSTANCE:Lorg/apache/pdfbox/pdmodel/font/encoding/StandardEncoding;

    return-object p0

    :cond_0
    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->WIN_ANSI_ENCODING:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, p0}, Lorg/apache/pdfbox/cos/COSName;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object p0, Lorg/apache/pdfbox/pdmodel/font/encoding/WinAnsiEncoding;->INSTANCE:Lorg/apache/pdfbox/pdmodel/font/encoding/WinAnsiEncoding;

    return-object p0

    :cond_1
    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->MAC_ROMAN_ENCODING:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, p0}, Lorg/apache/pdfbox/cos/COSName;->equals(Ljava/lang/Object;)Z

    move-result p0

    if-eqz p0, :cond_2

    sget-object p0, Lorg/apache/pdfbox/pdmodel/font/encoding/MacRomanEncoding;->INSTANCE:Lorg/apache/pdfbox/pdmodel/font/encoding/MacRomanEncoding;

    return-object p0

    :cond_2
    const/4 p0, 0x0

    return-object p0
.end method


# virtual methods
.method public add(ILjava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->codeToName:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object p1, p0, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->names:Ljava/util/Set;

    invoke-interface {p1, p2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public contains(I)Z
    .locals 1

    .line 1
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->codeToName:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public contains(Ljava/lang/String;)Z
    .locals 1

    .line 2
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->names:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public getCodeToNameMap()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->codeToName:Ljava/util/Map;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public getName(I)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->codeToName:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    if-eqz p1, :cond_0

    return-object p1

    :cond_0
    const-string p1, ".notdef"

    return-object p1
.end method

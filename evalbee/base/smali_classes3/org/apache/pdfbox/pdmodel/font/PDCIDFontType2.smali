.class public Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType2;
.super Lorg/apache/pdfbox/pdmodel/font/PDCIDFont;
.source "SourceFile"


# instance fields
.field private final cid2gid:[I

.field private final cmap:Lorg/apache/fontbox/ttf/CmapSubtable;

.field private fontMatrix:Lorg/apache/pdfbox/util/Matrix;

.field private final gid2cid:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final hasIdentityCid2Gid:Z

.field private final isDamaged:Z

.field private final isEmbedded:Z

.field private final ttf:Lorg/apache/fontbox/ttf/TrueTypeFont;


# direct methods
.method public constructor <init>(Lorg/apache/pdfbox/cos/COSDictionary;Lorg/apache/pdfbox/pdmodel/font/PDType0Font;)V
    .locals 6

    const-string v0, "Could not read embedded OTF for font "

    const-string v1, "Could not read embedded TTF for font "

    invoke-direct {p0, p1, p2}, Lorg/apache/pdfbox/pdmodel/font/PDCIDFont;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;Lorg/apache/pdfbox/pdmodel/font/PDType0Font;)V

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDCIDFont;->getFontDescriptor()Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;

    move-result-object p1

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->getFontFile2()Lorg/apache/pdfbox/pdmodel/common/PDStream;

    move-result-object p2

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->getFontFile3()Lorg/apache/pdfbox/pdmodel/common/PDStream;

    move-result-object v2

    if-nez p2, :cond_0

    if-nez v2, :cond_0

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->getFontFile()Lorg/apache/pdfbox/pdmodel/common/PDStream;

    move-result-object p2

    :cond_0
    const/4 p1, 0x0

    const-string v3, "PdfBoxAndroid"

    const/4 v4, 0x1

    const/4 v5, 0x0

    if-eqz p2, :cond_1

    :try_start_0
    new-instance v0, Lorg/apache/fontbox/ttf/TTFParser;

    invoke-direct {v0, v4}, Lorg/apache/fontbox/ttf/TTFParser;-><init>(Z)V

    invoke-virtual {p2}, Lorg/apache/pdfbox/pdmodel/common/PDStream;->createInputStream()Ljava/io/InputStream;

    move-result-object p2

    invoke-virtual {v0, p2}, Lorg/apache/fontbox/ttf/TTFParser;->parse(Ljava/io/InputStream;)Lorg/apache/fontbox/ttf/TrueTypeFont;

    move-result-object v5
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_3

    :catch_0
    move-exception p2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    goto :goto_0

    :catch_1
    move-exception p2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    :goto_0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDCIDFont;->getBaseFont()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-static {v3, v0, p2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move p2, v4

    goto :goto_4

    :cond_1
    if-eqz v2, :cond_3

    :try_start_1
    new-instance p2, Lorg/apache/fontbox/ttf/OTFParser;

    invoke-direct {p2, v4}, Lorg/apache/fontbox/ttf/OTFParser;-><init>(Z)V

    invoke-virtual {v2}, Lorg/apache/pdfbox/pdmodel/common/PDStream;->createInputStream()Ljava/io/InputStream;

    move-result-object v1

    invoke-virtual {p2, v1}, Lorg/apache/fontbox/ttf/OTFParser;->parse(Ljava/io/InputStream;)Lorg/apache/fontbox/ttf/OpenTypeFont;

    move-result-object v5

    invoke-virtual {v5}, Lorg/apache/fontbox/ttf/OpenTypeFont;->isPostScript()Z

    move-result p2

    if-nez p2, :cond_2

    invoke-virtual {v5}, Lorg/apache/fontbox/ttf/OpenTypeFont;->hasLayoutTables()Z

    move-result p2

    if-eqz p2, :cond_3

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "OpenType Layout tables used in font "

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDCIDFont;->getBaseFont()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " are not implemented in PDFBox and will be ignored"

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-static {v3, p2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    :cond_2
    new-instance p2, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Not implemented: OpenType font with CFF table "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDCIDFont;->getBaseFont()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p2, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p2
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    :catch_2
    move-exception p2

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    goto :goto_2

    :catch_3
    move-exception p2

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    :goto_2
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDCIDFont;->getBaseFont()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_3
    :goto_3
    move p2, p1

    :goto_4
    if-eqz v5, :cond_4

    move v0, v4

    goto :goto_5

    :cond_4
    move v0, p1

    :goto_5
    iput-boolean v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType2;->isEmbedded:Z

    iput-boolean p2, p0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType2;->isDamaged:Z

    if-nez v5, :cond_6

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDCIDFont;->getBaseFont()Ljava/lang/String;

    move-result-object p2

    invoke-static {p2}, Lorg/apache/pdfbox/pdmodel/font/ExternalFonts;->getTrueTypeFont(Ljava/lang/String;)Lorg/apache/fontbox/ttf/TrueTypeFont;

    move-result-object v5

    if-eqz v5, :cond_5

    goto :goto_6

    :cond_5
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDCIDFont;->getFontDescriptor()Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;

    move-result-object p2

    invoke-static {p2}, Lorg/apache/pdfbox/pdmodel/font/ExternalFonts;->getTrueTypeFallbackFont(Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;)Lorg/apache/fontbox/ttf/TrueTypeFont;

    move-result-object v5

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Using fallback font \'"

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v0, "\' for \'"

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDCIDFont;->getBaseFont()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "\'"

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-static {v3, p2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_6
    :goto_6
    iput-object v5, p0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType2;->ttf:Lorg/apache/fontbox/ttf/TrueTypeFont;

    invoke-virtual {v5, p1}, Lorg/apache/fontbox/ttf/TrueTypeFont;->getUnicodeCmap(Z)Lorg/apache/fontbox/ttf/CmapSubtable;

    move-result-object p2

    iput-object p2, p0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType2;->cmap:Lorg/apache/fontbox/ttf/CmapSubtable;

    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType2;->readCIDToGIDMap()[I

    move-result-object p2

    iput-object p2, p0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType2;->cid2gid:[I

    invoke-direct {p0, p2}, Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType2;->invert([I)Ljava/util/Map;

    move-result-object p2

    iput-object p2, p0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType2;->gid2cid:Ljava/util/Map;

    iget-object p2, p0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFont;->dict:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->CID_TO_GID_MAP:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p2, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object p2

    instance-of v0, p2, Lorg/apache/pdfbox/cos/COSName;

    if-eqz v0, :cond_7

    check-cast p2, Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p2}, Lorg/apache/pdfbox/cos/COSName;->getName()Ljava/lang/String;

    move-result-object p2

    const-string v0, "Identity"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_7

    move p1, v4

    :cond_7
    iput-boolean p1, p0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType2;->hasIdentityCid2Gid:Z

    return-void
.end method

.method private invert([I)Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([I)",
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    if-nez p1, :cond_0

    const/4 p1, 0x0

    return-object p1

    :cond_0
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    const/4 v1, 0x0

    :goto_0
    array-length v2, p1

    if-ge v1, v2, :cond_1

    aget v2, p1, v1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method private readCIDToGIDMap()[I
    .locals 7

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFont;->dict:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->CID_TO_GID_MAP:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    instance-of v1, v0, Lorg/apache/pdfbox/cos/COSStream;

    if-eqz v1, :cond_0

    check-cast v0, Lorg/apache/pdfbox/cos/COSStream;

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSStream;->getUnfilteredStream()Ljava/io/InputStream;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/pdfbox/io/IOUtils;->toByteArray(Ljava/io/InputStream;)[B

    move-result-object v1

    invoke-static {v0}, Lorg/apache/pdfbox/io/IOUtils;->closeQuietly(Ljava/io/Closeable;)V

    array-length v0, v1

    div-int/lit8 v0, v0, 0x2

    new-array v2, v0, [I

    const/4 v3, 0x0

    move v4, v3

    :goto_0
    if-ge v3, v0, :cond_1

    aget-byte v5, v1, v4

    and-int/lit16 v5, v5, 0xff

    shl-int/lit8 v5, v5, 0x8

    add-int/lit8 v6, v4, 0x1

    aget-byte v6, v1, v6

    and-int/lit16 v6, v6, 0xff

    or-int/2addr v5, v6

    aput v5, v2, v3

    add-int/lit8 v4, v4, 0x2

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :cond_1
    return-object v2
.end method


# virtual methods
.method public codeToCID(I)I
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFont;->parent:Lorg/apache/pdfbox/pdmodel/font/PDType0Font;

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/font/PDType0Font;->getCMap()Lorg/apache/fontbox/cmap/CMap;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/fontbox/cmap/CMap;->hasCIDMappings()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lorg/apache/fontbox/cmap/CMap;->hasUnicodeMappings()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0, p1}, Lorg/apache/fontbox/cmap/CMap;->toUnicode(I)Ljava/lang/String;

    move-result-object p1

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Ljava/lang/String;->codePointAt(I)I

    move-result p1

    return p1

    :cond_0
    invoke-virtual {v0, p1}, Lorg/apache/fontbox/cmap/CMap;->toCID(I)I

    move-result p1

    return p1
.end method

.method public codeToGID(I)I
    .locals 4

    iget-boolean v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType2;->isEmbedded:Z

    const/4 v1, 0x0

    if-nez v0, :cond_6

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFont;->parent:Lorg/apache/pdfbox/pdmodel/font/PDType0Font;

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/font/PDType0Font;->getCMapUCS2()Lorg/apache/fontbox/cmap/CMap;

    move-result-object v0

    const/4 v2, 0x1

    if-eqz v0, :cond_0

    move v0, v2

    goto :goto_0

    :cond_0
    move v0, v1

    :goto_0
    iget-object v3, p0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType2;->cid2gid:[I

    if-eqz v3, :cond_1

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType2;->codeToCID(I)I

    move-result p1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType2;->cid2gid:[I

    aget p1, v0, p1

    return p1

    :cond_1
    iget-boolean v3, p0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType2;->hasIdentityCid2Gid:Z

    if-nez v3, :cond_5

    if-nez v0, :cond_2

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFont;->parent:Lorg/apache/pdfbox/pdmodel/font/PDType0Font;

    invoke-virtual {v0, p1}, Lorg/apache/pdfbox/pdmodel/font/PDType0Font;->toUnicode(I)Ljava/lang/String;

    move-result-object v0

    const-string v3, "PdfBoxAndroid"

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Failed to find a character mapping for "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, " in "

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDCIDFont;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v3, p1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    return v1

    :cond_3
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result p1

    if-le p1, v2, :cond_4

    const-string p1, "Trying to map multi-byte character using \'cmap\', result will be poor"

    invoke-static {v3, p1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    iget-object p1, p0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType2;->cmap:Lorg/apache/fontbox/ttf/CmapSubtable;

    invoke-virtual {v0, v1}, Ljava/lang/String;->codePointAt(I)I

    move-result v0

    invoke-virtual {p1, v0}, Lorg/apache/fontbox/ttf/CmapSubtable;->getGlyphId(I)I

    move-result p1

    return p1

    :cond_5
    :goto_1
    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType2;->codeToCID(I)I

    move-result p1

    return p1

    :cond_6
    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType2;->codeToCID(I)I

    move-result p1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType2;->cid2gid:[I

    if-eqz v0, :cond_8

    array-length v2, v0

    if-ge p1, v2, :cond_7

    aget p1, v0, p1

    return p1

    :cond_7
    return v1

    :cond_8
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType2;->ttf:Lorg/apache/fontbox/ttf/TrueTypeFont;

    invoke-virtual {v0}, Lorg/apache/fontbox/ttf/TrueTypeFont;->getNumberOfGlyphs()I

    move-result v0

    if-ge p1, v0, :cond_9

    return p1

    :cond_9
    return v1
.end method

.method public encode(I)[B
    .locals 3

    iget-boolean v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType2;->isEmbedded:Z

    const/4 v1, 0x0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFont;->parent:Lorg/apache/pdfbox/pdmodel/font/PDType0Font;

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/font/PDType0Font;->getCMap()Lorg/apache/fontbox/cmap/CMap;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/fontbox/cmap/CMap;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v2, "Identity-"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    const/4 v2, -0x1

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType2;->cmap:Lorg/apache/fontbox/ttf/CmapSubtable;

    if-eqz v0, :cond_1

    invoke-virtual {v0, p1}, Lorg/apache/fontbox/ttf/CmapSubtable;->getGlyphId(I)I

    move-result v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFont;->parent:Lorg/apache/pdfbox/pdmodel/font/PDType0Font;

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/font/PDType0Font;->getCMapUCS2()Lorg/apache/fontbox/cmap/CMap;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFont;->parent:Lorg/apache/pdfbox/pdmodel/font/PDType0Font;

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/font/PDType0Font;->getCMapUCS2()Lorg/apache/fontbox/cmap/CMap;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/fontbox/cmap/CMap;->toCID(I)I

    move-result v0

    goto :goto_0

    :cond_1
    move v0, v2

    :goto_0
    if-ne v0, v2, :cond_3

    move v0, v1

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType2;->cmap:Lorg/apache/fontbox/ttf/CmapSubtable;

    invoke-virtual {v0, p1}, Lorg/apache/fontbox/ttf/CmapSubtable;->getGlyphId(I)I

    move-result v0

    :cond_3
    :goto_1
    if-eqz v0, :cond_4

    const/4 p1, 0x2

    new-array p1, p1, [B

    shr-int/lit8 v2, v0, 0x8

    and-int/lit16 v2, v2, 0xff

    int-to-byte v2, v2

    aput-byte v2, p1, v1

    and-int/lit16 v0, v0, 0xff

    int-to-byte v0, v0

    const/4 v1, 0x1

    aput-byte v0, p1, v1

    return-object p1

    :cond_4
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDCIDFont;->getName()Ljava/lang/String;

    move-result-object v1

    filled-new-array {p1, v1}, [Ljava/lang/Object;

    move-result-object p1

    const-string v1, "No glyph for U+%04X in font %s"

    invoke-static {v1, p1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getBoundingBox()Lorg/apache/fontbox/util/BoundingBox;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType2;->ttf:Lorg/apache/fontbox/ttf/TrueTypeFont;

    invoke-virtual {v0}, Lorg/apache/fontbox/ttf/TrueTypeFont;->getFontBBox()Lorg/apache/fontbox/util/BoundingBox;

    move-result-object v0

    return-object v0
.end method

.method public getFontMatrix()Lorg/apache/pdfbox/util/Matrix;
    .locals 8

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType2;->fontMatrix:Lorg/apache/pdfbox/util/Matrix;

    if-nez v0, :cond_0

    new-instance v0, Lorg/apache/pdfbox/util/Matrix;

    const v2, 0x3a83126f    # 0.001f

    const/4 v3, 0x0

    const/4 v4, 0x0

    const v5, 0x3a83126f    # 0.001f

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v1, v0

    invoke-direct/range {v1 .. v7}, Lorg/apache/pdfbox/util/Matrix;-><init>(FFFFFF)V

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType2;->fontMatrix:Lorg/apache/pdfbox/util/Matrix;

    :cond_0
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType2;->fontMatrix:Lorg/apache/pdfbox/util/Matrix;

    return-object v0
.end method

.method public getHeight(I)F
    .locals 1

    iget-object p1, p0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType2;->ttf:Lorg/apache/fontbox/ttf/TrueTypeFont;

    invoke-virtual {p1}, Lorg/apache/fontbox/ttf/TrueTypeFont;->getHorizontalHeader()Lorg/apache/fontbox/ttf/HorizontalHeaderTable;

    move-result-object p1

    invoke-virtual {p1}, Lorg/apache/fontbox/ttf/HorizontalHeaderTable;->getAscender()S

    move-result p1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType2;->ttf:Lorg/apache/fontbox/ttf/TrueTypeFont;

    invoke-virtual {v0}, Lorg/apache/fontbox/ttf/TrueTypeFont;->getHorizontalHeader()Lorg/apache/fontbox/ttf/HorizontalHeaderTable;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/fontbox/ttf/HorizontalHeaderTable;->getDescender()S

    move-result v0

    neg-int v0, v0

    add-int/2addr p1, v0

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType2;->ttf:Lorg/apache/fontbox/ttf/TrueTypeFont;

    invoke-virtual {v0}, Lorg/apache/fontbox/ttf/TrueTypeFont;->getUnitsPerEm()I

    move-result v0

    div-int/2addr p1, v0

    int-to-float p1, p1

    return p1
.end method

.method public getTrueTypeFont()Lorg/apache/fontbox/ttf/TrueTypeFont;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType2;->ttf:Lorg/apache/fontbox/ttf/TrueTypeFont;

    return-object v0
.end method

.method public getWidthFromFont(I)F
    .locals 2

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType2;->codeToGID(I)I

    move-result p1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType2;->ttf:Lorg/apache/fontbox/ttf/TrueTypeFont;

    invoke-virtual {v0, p1}, Lorg/apache/fontbox/ttf/TrueTypeFont;->getAdvanceWidth(I)I

    move-result p1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType2;->ttf:Lorg/apache/fontbox/ttf/TrueTypeFont;

    invoke-virtual {v0}, Lorg/apache/fontbox/ttf/TrueTypeFont;->getUnitsPerEm()I

    move-result v0

    const/16 v1, 0x3e8

    if-eq v0, v1, :cond_0

    int-to-float p1, p1

    const/high16 v1, 0x447a0000    # 1000.0f

    int-to-float v0, v0

    div-float/2addr v1, v0

    mul-float/2addr p1, v1

    float-to-int p1, p1

    :cond_0
    int-to-float p1, p1

    return p1
.end method

.method public isDamaged()Z
    .locals 1

    iget-boolean v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType2;->isDamaged:Z

    return v0
.end method

.method public isEmbedded()Z
    .locals 1

    iget-boolean v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType2;->isEmbedded:Z

    return v0
.end method

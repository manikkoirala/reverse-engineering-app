.class public Lorg/apache/pdfbox/pdmodel/font/encoding/MacOSRomanEncoding;
.super Lorg/apache/pdfbox/pdmodel/font/encoding/MacRomanEncoding;
.source "SourceFile"


# static fields
.field public static final INSTANCE:Lorg/apache/pdfbox/pdmodel/font/encoding/MacOSRomanEncoding;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lorg/apache/pdfbox/pdmodel/font/encoding/MacOSRomanEncoding;

    invoke-direct {v0}, Lorg/apache/pdfbox/pdmodel/font/encoding/MacOSRomanEncoding;-><init>()V

    sput-object v0, Lorg/apache/pdfbox/pdmodel/font/encoding/MacOSRomanEncoding;->INSTANCE:Lorg/apache/pdfbox/pdmodel/font/encoding/MacOSRomanEncoding;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/font/encoding/MacRomanEncoding;-><init>()V

    const/16 v0, 0xff

    const-string v1, "notequal"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x104

    const-string v1, "infinity"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x106

    const-string v1, "lessequal"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x107

    const-string v1, "greaterequal"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x10a

    const-string v1, "partialdiff"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x10b

    const-string v1, "summation"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x10e

    const-string v1, "product"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x10f

    const-string v1, "pi"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x110

    const-string v1, "integral"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x113

    const-string v1, "Omega"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x12f

    const-string v1, "radical"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x131

    const-string v1, "approxequal"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x132

    const-string v1, "Delta"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x147

    const-string v1, "lozenge"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x14d

    const-string v1, "Euro"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x168

    const-string v1, "apple"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    return-void
.end method


# virtual methods
.method public getCOSObject()Lorg/apache/pdfbox/cos/COSBase;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

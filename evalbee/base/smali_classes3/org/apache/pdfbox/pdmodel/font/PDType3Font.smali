.class public Lorg/apache/pdfbox/pdmodel/font/PDType3Font;
.super Lorg/apache/pdfbox/pdmodel/font/PDSimpleFont;
.source "SourceFile"


# instance fields
.field private charProcs:Lorg/apache/pdfbox/cos/COSDictionary;

.field private fontMatrix:Lorg/apache/pdfbox/util/Matrix;

.field private resources:Lorg/apache/pdfbox/pdmodel/PDResources;


# direct methods
.method public constructor <init>(Lorg/apache/pdfbox/cos/COSDictionary;)V
    .locals 0

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/font/PDSimpleFont;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDSimpleFont;->readEncoding()V

    return-void
.end method


# virtual methods
.method public encode(I)[B
    .locals 1

    new-instance p1, Ljava/lang/UnsupportedOperationException;

    const-string v0, "Not implemented: Type3"

    invoke-direct {p1, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public getBoundingBox()Lorg/apache/fontbox/util/BoundingBox;
    .locals 5

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDType3Font;->getFontBBox()Lorg/apache/pdfbox/pdmodel/common/PDRectangle;

    move-result-object v0

    new-instance v1, Lorg/apache/fontbox/util/BoundingBox;

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->getLowerLeftX()F

    move-result v2

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->getLowerLeftY()F

    move-result v3

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->getWidth()F

    move-result v4

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->getHeight()F

    move-result v0

    invoke-direct {v1, v2, v3, v4, v0}, Lorg/apache/fontbox/util/BoundingBox;-><init>(FFFF)V

    return-object v1
.end method

.method public getCharProc(I)Lorg/apache/pdfbox/pdmodel/font/PDType3CharProc;
    .locals 1

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDSimpleFont;->getEncoding()Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->getName(I)Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDType3Font;->getCharProcs()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    invoke-static {p1}, Lorg/apache/pdfbox/cos/COSName;->getPDFName(Ljava/lang/String;)Lorg/apache/pdfbox/cos/COSName;

    move-result-object p1

    invoke-virtual {v0, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object p1

    check-cast p1, Lorg/apache/pdfbox/cos/COSStream;

    new-instance v0, Lorg/apache/pdfbox/pdmodel/font/PDType3CharProc;

    invoke-direct {v0, p0, p1}, Lorg/apache/pdfbox/pdmodel/font/PDType3CharProc;-><init>(Lorg/apache/pdfbox/pdmodel/font/PDType3Font;Lorg/apache/pdfbox/cos/COSStream;)V

    return-object v0

    :cond_0
    const/4 p1, 0x0

    return-object p1
.end method

.method public getCharProcs()Lorg/apache/pdfbox/cos/COSDictionary;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDType3Font;->charProcs:Lorg/apache/pdfbox/cos/COSDictionary;

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDFont;->dict:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->CHAR_PROCS:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSDictionary;

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDType3Font;->charProcs:Lorg/apache/pdfbox/cos/COSDictionary;

    :cond_0
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDType3Font;->charProcs:Lorg/apache/pdfbox/cos/COSDictionary;

    return-object v0
.end method

.method public getDisplacement(I)Lorg/apache/pdfbox/util/Vector;
    .locals 3

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDType3Font;->getFontMatrix()Lorg/apache/pdfbox/util/Matrix;

    move-result-object v0

    new-instance v1, Lorg/apache/pdfbox/util/Vector;

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/pdmodel/font/PDType3Font;->getWidth(I)F

    move-result p1

    const/4 v2, 0x0

    invoke-direct {v1, p1, v2}, Lorg/apache/pdfbox/util/Vector;-><init>(FF)V

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/util/Matrix;->transform(Lorg/apache/pdfbox/util/Vector;)Lorg/apache/pdfbox/util/Vector;

    move-result-object p1

    return-object p1
.end method

.method public getFontBBox()Lorg/apache/pdfbox/pdmodel/common/PDRectangle;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDFont;->dict:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->FONT_BBOX:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSArray;

    if-eqz v0, :cond_0

    new-instance v1, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;

    invoke-direct {v1, v0}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;-><init>(Lorg/apache/pdfbox/cos/COSArray;)V

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return-object v1
.end method

.method public getFontMatrix()Lorg/apache/pdfbox/util/Matrix;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDType3Font;->fontMatrix:Lorg/apache/pdfbox/util/Matrix;

    if-nez v0, :cond_1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDFont;->dict:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->FONT_MATRIX:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSArray;

    if-eqz v0, :cond_0

    new-instance v1, Lorg/apache/pdfbox/util/Matrix;

    invoke-direct {v1, v0}, Lorg/apache/pdfbox/util/Matrix;-><init>(Lorg/apache/pdfbox/cos/COSArray;)V

    iput-object v1, p0, Lorg/apache/pdfbox/pdmodel/font/PDType3Font;->fontMatrix:Lorg/apache/pdfbox/util/Matrix;

    goto :goto_0

    :cond_0
    invoke-super {p0}, Lorg/apache/pdfbox/pdmodel/font/PDFont;->getFontMatrix()Lorg/apache/pdfbox/util/Matrix;

    move-result-object v0

    return-object v0

    :cond_1
    :goto_0
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDType3Font;->fontMatrix:Lorg/apache/pdfbox/util/Matrix;

    return-object v0
.end method

.method public getHeight(I)F
    .locals 3

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDFont;->getFontDescriptor()Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;

    move-result-object p1

    const/4 v0, 0x0

    if-eqz p1, :cond_4

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->getFontBoundingBox()Lorg/apache/pdfbox/pdmodel/common/PDRectangle;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->getHeight()F

    move-result v1

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    goto :goto_0

    :cond_0
    move v1, v0

    :goto_0
    cmpl-float v2, v1, v0

    if-nez v2, :cond_1

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->getCapHeight()F

    move-result v1

    :cond_1
    cmpl-float v2, v1, v0

    if-nez v2, :cond_2

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->getAscent()F

    move-result v1

    :cond_2
    cmpl-float v2, v1, v0

    if-nez v2, :cond_3

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->getXHeight()F

    move-result v1

    cmpl-float v0, v1, v0

    if-lez v0, :cond_3

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->getDescent()F

    move-result p1

    sub-float/2addr v1, p1

    :cond_3
    return v1

    :cond_4
    return v0
.end method

.method public getName()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDFont;->dict:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->NAME:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getNameAsString(Lorg/apache/pdfbox/cos/COSName;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getResources()Lorg/apache/pdfbox/pdmodel/PDResources;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDType3Font;->resources:Lorg/apache/pdfbox/pdmodel/PDResources;

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDFont;->dict:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->RESOURCES:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSDictionary;

    if-eqz v0, :cond_0

    new-instance v1, Lorg/apache/pdfbox/pdmodel/PDResources;

    invoke-direct {v1, v0}, Lorg/apache/pdfbox/pdmodel/PDResources;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    iput-object v1, p0, Lorg/apache/pdfbox/pdmodel/font/PDType3Font;->resources:Lorg/apache/pdfbox/pdmodel/PDResources;

    :cond_0
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDType3Font;->resources:Lorg/apache/pdfbox/pdmodel/PDResources;

    return-object v0
.end method

.method public getWidth(I)F
    .locals 4

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDFont;->dict:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->FIRST_CHAR:Lorg/apache/pdfbox/cos/COSName;

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->getInt(Lorg/apache/pdfbox/cos/COSName;I)I

    move-result v0

    iget-object v1, p0, Lorg/apache/pdfbox/pdmodel/font/PDFont;->dict:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v3, Lorg/apache/pdfbox/cos/COSName;->LAST_CHAR:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v1, v3, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->getInt(Lorg/apache/pdfbox/cos/COSName;I)I

    move-result v1

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDFont;->getWidths()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_0

    if-lt p1, v0, :cond_0

    if-gt p1, v1, :cond_0

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDFont;->getWidths()Ljava/util/List;

    move-result-object v1

    sub-int/2addr p1, v0

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->floatValue()F

    move-result p1

    return p1

    :cond_0
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDFont;->getFontDescriptor()Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->getMissingWidth()F

    move-result p1

    return p1

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "No width for glyph "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, " in font "

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDType3Font;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v0, "PdfBoxAndroid"

    invoke-static {v0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 p1, 0x0

    return p1
.end method

.method public getWidthFromFont(I)F
    .locals 1

    new-instance p1, Ljava/lang/UnsupportedOperationException;

    const-string v0, "not suppported"

    invoke-direct {p1, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public isDamaged()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public isEmbedded()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public isFontSymbolic()Ljava/lang/Boolean;
    .locals 1

    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    return-object v0
.end method

.method public readCode(Ljava/io/InputStream;)I
    .locals 0

    invoke-virtual {p1}, Ljava/io/InputStream;->read()I

    move-result p1

    return p1
.end method

.method public readEncodingFromFont()Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;
    .locals 2

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "not supported for Type 3 fonts"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

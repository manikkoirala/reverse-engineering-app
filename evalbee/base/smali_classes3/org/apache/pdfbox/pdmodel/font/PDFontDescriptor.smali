.class public final Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/pdfbox/pdmodel/common/COSObjectable;


# static fields
.field private static final FLAG_ALL_CAP:I = 0x10000

.field private static final FLAG_FIXED_PITCH:I = 0x1

.field private static final FLAG_FORCE_BOLD:I = 0x40000

.field private static final FLAG_ITALIC:I = 0x40

.field private static final FLAG_NON_SYMBOLIC:I = 0x20

.field private static final FLAG_SCRIPT:I = 0x8

.field private static final FLAG_SERIF:I = 0x2

.field private static final FLAG_SMALL_CAP:I = 0x20000

.field private static final FLAG_SYMBOLIC:I = 0x4


# instance fields
.field private capHeight:F

.field private final dic:Lorg/apache/pdfbox/cos/COSDictionary;

.field private flags:I

.field private xHeight:F


# direct methods
.method public constructor <init>()V
    .locals 3

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/high16 v0, -0x800000    # Float.NEGATIVE_INFINITY

    iput v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->xHeight:F

    iput v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->capHeight:F

    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->flags:I

    new-instance v0, Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-direct {v0}, Lorg/apache/pdfbox/cos/COSDictionary;-><init>()V

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->dic:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->TYPE:Lorg/apache/pdfbox/cos/COSName;

    sget-object v2, Lorg/apache/pdfbox/cos/COSName;->FONT_DESC:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    return-void
.end method

.method public constructor <init>(Lorg/apache/pdfbox/cos/COSDictionary;)V
    .locals 1

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/high16 v0, -0x800000    # Float.NEGATIVE_INFINITY

    iput v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->xHeight:F

    iput v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->capHeight:F

    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->flags:I

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->dic:Lorg/apache/pdfbox/cos/COSDictionary;

    return-void
.end method

.method private isFlagBitOn(I)Z
    .locals 1

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->getFlags()I

    move-result v0

    and-int/2addr p1, v0

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method private setFlagBit(IZ)V
    .locals 1

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->getFlags()I

    move-result v0

    if-eqz p2, :cond_0

    or-int/2addr p1, v0

    goto :goto_0

    :cond_0
    xor-int/lit8 p1, p1, -0x1

    and-int/2addr p1, v0

    :goto_0
    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->setFlags(I)V

    return-void
.end method


# virtual methods
.method public getAscent()F
    .locals 3

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->dic:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->ASCENT:Lorg/apache/pdfbox/cos/COSName;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->getFloat(Lorg/apache/pdfbox/cos/COSName;F)F

    move-result v0

    return v0
.end method

.method public getAverageWidth()F
    .locals 3

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->dic:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->AVG_WIDTH:Lorg/apache/pdfbox/cos/COSName;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->getFloat(Lorg/apache/pdfbox/cos/COSName;F)F

    move-result v0

    return v0
.end method

.method public bridge synthetic getCOSObject()Lorg/apache/pdfbox/cos/COSBase;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->getCOSObject()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    return-object v0
.end method

.method public getCOSObject()Lorg/apache/pdfbox/cos/COSDictionary;
    .locals 1

    .line 2
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->dic:Lorg/apache/pdfbox/cos/COSDictionary;

    return-object v0
.end method

.method public getCapHeight()F
    .locals 3

    iget v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->capHeight:F

    const/high16 v1, -0x800000    # Float.NEGATIVE_INFINITY

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->dic:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->CAP_HEIGHT:Lorg/apache/pdfbox/cos/COSName;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->getFloat(Lorg/apache/pdfbox/cos/COSName;F)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iput v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->capHeight:F

    :cond_0
    iget v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->capHeight:F

    return v0
.end method

.method public getCharSet()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->dic:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->CHAR_SET:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSString;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSString;->getString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public getDescent()F
    .locals 3

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->dic:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->DESCENT:Lorg/apache/pdfbox/cos/COSName;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->getFloat(Lorg/apache/pdfbox/cos/COSName;F)F

    move-result v0

    return v0
.end method

.method public getFlags()I
    .locals 3

    iget v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->flags:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->dic:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->FLAGS:Lorg/apache/pdfbox/cos/COSName;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->getInt(Lorg/apache/pdfbox/cos/COSName;I)I

    move-result v0

    iput v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->flags:I

    :cond_0
    iget v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->flags:I

    return v0
.end method

.method public getFontBoundingBox()Lorg/apache/pdfbox/pdmodel/common/PDRectangle;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->dic:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->FONT_BBOX:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSArray;

    if-eqz v0, :cond_0

    new-instance v1, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;

    invoke-direct {v1, v0}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;-><init>(Lorg/apache/pdfbox/cos/COSArray;)V

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return-object v1
.end method

.method public getFontFamily()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->dic:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->FONT_FAMILY:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSString;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSString;->getString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public getFontFile()Lorg/apache/pdfbox/pdmodel/common/PDStream;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->dic:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->FONT_FILE:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    instance-of v1, v0, Lorg/apache/pdfbox/cos/COSStream;

    if-eqz v1, :cond_0

    new-instance v1, Lorg/apache/pdfbox/pdmodel/common/PDStream;

    check-cast v0, Lorg/apache/pdfbox/cos/COSStream;

    invoke-direct {v1, v0}, Lorg/apache/pdfbox/pdmodel/common/PDStream;-><init>(Lorg/apache/pdfbox/cos/COSStream;)V

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return-object v1
.end method

.method public getFontFile2()Lorg/apache/pdfbox/pdmodel/common/PDStream;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->dic:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->FONT_FILE2:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    instance-of v1, v0, Lorg/apache/pdfbox/cos/COSStream;

    if-eqz v1, :cond_0

    new-instance v1, Lorg/apache/pdfbox/pdmodel/common/PDStream;

    check-cast v0, Lorg/apache/pdfbox/cos/COSStream;

    invoke-direct {v1, v0}, Lorg/apache/pdfbox/pdmodel/common/PDStream;-><init>(Lorg/apache/pdfbox/cos/COSStream;)V

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return-object v1
.end method

.method public getFontFile3()Lorg/apache/pdfbox/pdmodel/common/PDStream;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->dic:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->FONT_FILE3:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    instance-of v1, v0, Lorg/apache/pdfbox/cos/COSStream;

    if-eqz v1, :cond_0

    new-instance v1, Lorg/apache/pdfbox/pdmodel/common/PDStream;

    check-cast v0, Lorg/apache/pdfbox/cos/COSStream;

    invoke-direct {v1, v0}, Lorg/apache/pdfbox/pdmodel/common/PDStream;-><init>(Lorg/apache/pdfbox/cos/COSStream;)V

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return-object v1
.end method

.method public getFontName()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->dic:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->FONT_NAME:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSName;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSName;->getName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public getFontStretch()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->dic:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->FONT_STRETCH:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSName;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSName;->getName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public getFontWeight()F
    .locals 3

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->dic:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->FONT_WEIGHT:Lorg/apache/pdfbox/cos/COSName;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->getFloat(Lorg/apache/pdfbox/cos/COSName;F)F

    move-result v0

    return v0
.end method

.method public getItalicAngle()F
    .locals 3

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->dic:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->ITALIC_ANGLE:Lorg/apache/pdfbox/cos/COSName;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->getFloat(Lorg/apache/pdfbox/cos/COSName;F)F

    move-result v0

    return v0
.end method

.method public getLeading()F
    .locals 3

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->dic:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->LEADING:Lorg/apache/pdfbox/cos/COSName;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->getFloat(Lorg/apache/pdfbox/cos/COSName;F)F

    move-result v0

    return v0
.end method

.method public getMaxWidth()F
    .locals 3

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->dic:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->MAX_WIDTH:Lorg/apache/pdfbox/cos/COSName;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->getFloat(Lorg/apache/pdfbox/cos/COSName;F)F

    move-result v0

    return v0
.end method

.method public getMissingWidth()F
    .locals 3

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->dic:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->MISSING_WIDTH:Lorg/apache/pdfbox/cos/COSName;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->getFloat(Lorg/apache/pdfbox/cos/COSName;F)F

    move-result v0

    return v0
.end method

.method public getStemH()F
    .locals 3

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->dic:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->STEM_H:Lorg/apache/pdfbox/cos/COSName;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->getFloat(Lorg/apache/pdfbox/cos/COSName;F)F

    move-result v0

    return v0
.end method

.method public getStemV()F
    .locals 3

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->dic:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->STEM_V:Lorg/apache/pdfbox/cos/COSName;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->getFloat(Lorg/apache/pdfbox/cos/COSName;F)F

    move-result v0

    return v0
.end method

.method public getXHeight()F
    .locals 3

    iget v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->xHeight:F

    const/high16 v1, -0x800000    # Float.NEGATIVE_INFINITY

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->dic:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->XHEIGHT:Lorg/apache/pdfbox/cos/COSName;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->getFloat(Lorg/apache/pdfbox/cos/COSName;F)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iput v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->xHeight:F

    :cond_0
    iget v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->xHeight:F

    return v0
.end method

.method public hasWidths()Z
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->dic:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->WIDTHS:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->containsKey(Lorg/apache/pdfbox/cos/COSName;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->dic:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->MISSING_WIDTH:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->containsKey(Lorg/apache/pdfbox/cos/COSName;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public isAllCap()Z
    .locals 1

    const/high16 v0, 0x10000

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->isFlagBitOn(I)Z

    move-result v0

    return v0
.end method

.method public isFixedPitch()Z
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->isFlagBitOn(I)Z

    move-result v0

    return v0
.end method

.method public isForceBold()Z
    .locals 1

    const/high16 v0, 0x40000

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->isFlagBitOn(I)Z

    move-result v0

    return v0
.end method

.method public isItalic()Z
    .locals 1

    const/16 v0, 0x40

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->isFlagBitOn(I)Z

    move-result v0

    return v0
.end method

.method public isNonSymbolic()Z
    .locals 1

    const/16 v0, 0x20

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->isFlagBitOn(I)Z

    move-result v0

    return v0
.end method

.method public isScript()Z
    .locals 1

    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->isFlagBitOn(I)Z

    move-result v0

    return v0
.end method

.method public isSerif()Z
    .locals 1

    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->isFlagBitOn(I)Z

    move-result v0

    return v0
.end method

.method public isSmallCap()Z
    .locals 1

    const/high16 v0, 0x20000

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->isFlagBitOn(I)Z

    move-result v0

    return v0
.end method

.method public isSymbolic()Z
    .locals 1

    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->isFlagBitOn(I)Z

    move-result v0

    return v0
.end method

.method public setAllCap(Z)V
    .locals 1

    const/high16 v0, 0x10000

    invoke-direct {p0, v0, p1}, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->setFlagBit(IZ)V

    return-void
.end method

.method public setAscent(F)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->dic:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->ASCENT:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setFloat(Lorg/apache/pdfbox/cos/COSName;F)V

    return-void
.end method

.method public setAverageWidth(F)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->dic:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->AVG_WIDTH:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setFloat(Lorg/apache/pdfbox/cos/COSName;F)V

    return-void
.end method

.method public setCIDSet(Lorg/apache/pdfbox/pdmodel/common/PDStream;)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->dic:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->CID_SET:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)V

    return-void
.end method

.method public setCapHeight(F)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->dic:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->CAP_HEIGHT:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setFloat(Lorg/apache/pdfbox/cos/COSName;F)V

    iput p1, p0, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->capHeight:F

    return-void
.end method

.method public setCharacterSet(Ljava/lang/String;)V
    .locals 2

    if-eqz p1, :cond_0

    new-instance v0, Lorg/apache/pdfbox/cos/COSString;

    invoke-direct {v0, p1}, Lorg/apache/pdfbox/cos/COSString;-><init>(Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iget-object p1, p0, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->dic:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->CHAR_SET:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p1, v1, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    return-void
.end method

.method public setDescent(F)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->dic:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->DESCENT:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setFloat(Lorg/apache/pdfbox/cos/COSName;F)V

    return-void
.end method

.method public setFixedPitch(Z)V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0, p1}, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->setFlagBit(IZ)V

    return-void
.end method

.method public setFlags(I)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->dic:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->FLAGS:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setInt(Lorg/apache/pdfbox/cos/COSName;I)V

    iput p1, p0, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->flags:I

    return-void
.end method

.method public setFontBoundingBox(Lorg/apache/pdfbox/pdmodel/common/PDRectangle;)V
    .locals 2

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->getCOSArray()Lorg/apache/pdfbox/cos/COSArray;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->dic:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->FONT_BBOX:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    return-void
.end method

.method public setFontFamily(Ljava/lang/String;)V
    .locals 2

    if-eqz p1, :cond_0

    new-instance v0, Lorg/apache/pdfbox/cos/COSString;

    invoke-direct {v0, p1}, Lorg/apache/pdfbox/cos/COSString;-><init>(Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iget-object p1, p0, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->dic:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->FONT_FAMILY:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p1, v1, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    return-void
.end method

.method public setFontFile(Lorg/apache/pdfbox/pdmodel/common/PDStream;)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->dic:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->FONT_FILE:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)V

    return-void
.end method

.method public setFontFile2(Lorg/apache/pdfbox/pdmodel/common/PDStream;)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->dic:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->FONT_FILE2:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)V

    return-void
.end method

.method public setFontFile3(Lorg/apache/pdfbox/pdmodel/common/PDStream;)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->dic:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->FONT_FILE3:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)V

    return-void
.end method

.method public setFontName(Ljava/lang/String;)V
    .locals 2

    if-eqz p1, :cond_0

    invoke-static {p1}, Lorg/apache/pdfbox/cos/COSName;->getPDFName(Ljava/lang/String;)Lorg/apache/pdfbox/cos/COSName;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->dic:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->FONT_NAME:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    return-void
.end method

.method public setFontStretch(Ljava/lang/String;)V
    .locals 2

    if-eqz p1, :cond_0

    invoke-static {p1}, Lorg/apache/pdfbox/cos/COSName;->getPDFName(Ljava/lang/String;)Lorg/apache/pdfbox/cos/COSName;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->dic:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->FONT_STRETCH:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    return-void
.end method

.method public setFontWeight(F)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->dic:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->FONT_WEIGHT:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setFloat(Lorg/apache/pdfbox/cos/COSName;F)V

    return-void
.end method

.method public setForceBold(Z)V
    .locals 1

    const/high16 v0, 0x40000

    invoke-direct {p0, v0, p1}, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->setFlagBit(IZ)V

    return-void
.end method

.method public setItalic(Z)V
    .locals 1

    const/16 v0, 0x40

    invoke-direct {p0, v0, p1}, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->setFlagBit(IZ)V

    return-void
.end method

.method public setItalicAngle(F)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->dic:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->ITALIC_ANGLE:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setFloat(Lorg/apache/pdfbox/cos/COSName;F)V

    return-void
.end method

.method public setLeading(F)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->dic:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->LEADING:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setFloat(Lorg/apache/pdfbox/cos/COSName;F)V

    return-void
.end method

.method public setMaxWidth(F)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->dic:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->MAX_WIDTH:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setFloat(Lorg/apache/pdfbox/cos/COSName;F)V

    return-void
.end method

.method public setMissingWidth(F)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->dic:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->MISSING_WIDTH:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setFloat(Lorg/apache/pdfbox/cos/COSName;F)V

    return-void
.end method

.method public setNonSymbolic(Z)V
    .locals 1

    const/16 v0, 0x20

    invoke-direct {p0, v0, p1}, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->setFlagBit(IZ)V

    return-void
.end method

.method public setScript(Z)V
    .locals 1

    const/16 v0, 0x8

    invoke-direct {p0, v0, p1}, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->setFlagBit(IZ)V

    return-void
.end method

.method public setSerif(Z)V
    .locals 1

    const/4 v0, 0x2

    invoke-direct {p0, v0, p1}, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->setFlagBit(IZ)V

    return-void
.end method

.method public setSmallCap(Z)V
    .locals 1

    const/high16 v0, 0x20000

    invoke-direct {p0, v0, p1}, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->setFlagBit(IZ)V

    return-void
.end method

.method public setStemH(F)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->dic:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->STEM_H:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setFloat(Lorg/apache/pdfbox/cos/COSName;F)V

    return-void
.end method

.method public setStemV(F)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->dic:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->STEM_V:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setFloat(Lorg/apache/pdfbox/cos/COSName;F)V

    return-void
.end method

.method public setSymbolic(Z)V
    .locals 1

    const/4 v0, 0x4

    invoke-direct {p0, v0, p1}, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->setFlagBit(IZ)V

    return-void
.end method

.method public setXHeight(F)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->dic:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->XHEIGHT:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setFloat(Lorg/apache/pdfbox/cos/COSName;F)V

    iput p1, p0, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->xHeight:F

    return-void
.end method

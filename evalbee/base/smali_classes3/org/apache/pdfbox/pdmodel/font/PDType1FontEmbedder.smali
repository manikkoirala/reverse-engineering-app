.class Lorg/apache/pdfbox/pdmodel/font/PDType1FontEmbedder;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final fontEncoding:Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;

.field private final metrics:Lorg/apache/fontbox/afm/FontMetrics;

.field private final type1:Lorg/apache/fontbox/type1/Type1Font;


# direct methods
.method public constructor <init>(Lorg/apache/pdfbox/pdmodel/PDDocument;Lorg/apache/pdfbox/cos/COSDictionary;Ljava/io/InputStream;Ljava/io/InputStream;)V
    .locals 7

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->SUBTYPE:Lorg/apache/pdfbox/cos/COSName;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->TYPE1:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p2, v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    new-instance v0, Lorg/apache/fontbox/afm/AFMParser;

    invoke-direct {v0, p3}, Lorg/apache/fontbox/afm/AFMParser;-><init>(Ljava/io/InputStream;)V

    invoke-virtual {v0}, Lorg/apache/fontbox/afm/AFMParser;->parse()Lorg/apache/fontbox/afm/FontMetrics;

    move-result-object p3

    iput-object p3, p0, Lorg/apache/pdfbox/pdmodel/font/PDType1FontEmbedder;->metrics:Lorg/apache/fontbox/afm/FontMetrics;

    invoke-direct {p0, p3}, Lorg/apache/pdfbox/pdmodel/font/PDType1FontEmbedder;->encodingFromAFM(Lorg/apache/fontbox/afm/FontMetrics;)Lorg/apache/pdfbox/pdmodel/font/encoding/DictionaryEncoding;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDType1FontEmbedder;->fontEncoding:Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;

    invoke-static {p3}, Lorg/apache/pdfbox/pdmodel/font/PDType1FontEmbedder;->buildFontDescriptor(Lorg/apache/fontbox/afm/FontMetrics;)Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;

    move-result-object p3

    invoke-static {p4}, Lorg/apache/pdfbox/io/IOUtils;->toByteArray(Ljava/io/InputStream;)[B

    move-result-object p4

    new-instance v0, Lorg/apache/fontbox/pfb/PfbParser;

    new-instance v1, Ljava/io/ByteArrayInputStream;

    invoke-direct {v1, p4}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v0, v1}, Lorg/apache/fontbox/pfb/PfbParser;-><init>(Ljava/io/InputStream;)V

    new-instance v1, Ljava/io/ByteArrayInputStream;

    invoke-direct {v1, p4}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-static {v1}, Lorg/apache/fontbox/type1/Type1Font;->createWithPFB(Ljava/io/InputStream;)Lorg/apache/fontbox/type1/Type1Font;

    move-result-object p4

    iput-object p4, p0, Lorg/apache/pdfbox/pdmodel/font/PDType1FontEmbedder;->type1:Lorg/apache/fontbox/type1/Type1Font;

    new-instance p4, Lorg/apache/pdfbox/pdmodel/common/PDStream;

    invoke-virtual {v0}, Lorg/apache/fontbox/pfb/PfbParser;->getInputStream()Ljava/io/InputStream;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {p4, p1, v1, v2}, Lorg/apache/pdfbox/pdmodel/common/PDStream;-><init>(Lorg/apache/pdfbox/pdmodel/PDDocument;Ljava/io/InputStream;Z)V

    invoke-virtual {p4}, Lorg/apache/pdfbox/pdmodel/common/PDStream;->getStream()Lorg/apache/pdfbox/cos/COSStream;

    move-result-object p1

    invoke-virtual {v0}, Lorg/apache/fontbox/pfb/PfbParser;->size()I

    move-result v1

    const-string v3, "Length"

    invoke-virtual {p1, v3, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->setInt(Ljava/lang/String;I)V

    move p1, v2

    :goto_0
    invoke-virtual {v0}, Lorg/apache/fontbox/pfb/PfbParser;->getLengths()[I

    move-result-object v1

    array-length v1, v1

    if-ge p1, v1, :cond_0

    invoke-virtual {p4}, Lorg/apache/pdfbox/pdmodel/common/PDStream;->getStream()Lorg/apache/pdfbox/cos/COSStream;

    move-result-object v1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v5, p1, 0x1

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0}, Lorg/apache/fontbox/pfb/PfbParser;->getLengths()[I

    move-result-object v6

    aget p1, v6, p1

    invoke-virtual {v1, v4, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setInt(Ljava/lang/String;I)V

    move p1, v5

    goto :goto_0

    :cond_0
    invoke-virtual {p4}, Lorg/apache/pdfbox/pdmodel/common/PDStream;->addCompression()V

    invoke-virtual {p3, p4}, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->setFontFile(Lorg/apache/pdfbox/pdmodel/common/PDStream;)V

    sget-object p1, Lorg/apache/pdfbox/cos/COSName;->FONT_DESC:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p2, p1, p3}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)V

    sget-object p1, Lorg/apache/pdfbox/cos/COSName;->BASE_FONT:Lorg/apache/pdfbox/cos/COSName;

    iget-object p3, p0, Lorg/apache/pdfbox/pdmodel/font/PDType1FontEmbedder;->metrics:Lorg/apache/fontbox/afm/FontMetrics;

    invoke-virtual {p3}, Lorg/apache/fontbox/afm/FontMetrics;->getFontName()Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p2, p1, p3}, Lorg/apache/pdfbox/cos/COSDictionary;->setName(Lorg/apache/pdfbox/cos/COSName;Ljava/lang/String;)V

    iget-object p1, p0, Lorg/apache/pdfbox/pdmodel/font/PDType1FontEmbedder;->metrics:Lorg/apache/fontbox/afm/FontMetrics;

    invoke-virtual {p1}, Lorg/apache/fontbox/afm/FontMetrics;->getCharMetrics()Ljava/util/List;

    move-result-object p1

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDType1FontEmbedder;->getFontEncoding()Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;

    new-instance p3, Ljava/util/ArrayList;

    const/16 p4, 0x100

    invoke-direct {p3, p4}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    move v0, v2

    :goto_1
    if-ge v0, p4, :cond_1

    const/16 v1, 0xfa

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {p3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    const/16 p4, 0xff

    move v0, p4

    move v1, v2

    :cond_2
    :goto_2
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/fontbox/afm/CharMetric;

    invoke-virtual {v3}, Lorg/apache/fontbox/afm/CharMetric;->getCharacterCode()I

    move-result v4

    if-lez v4, :cond_2

    invoke-static {v0, v4}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {v1, v4}, Ljava/lang/Math;->max(II)I

    move-result v1

    invoke-virtual {v3}, Lorg/apache/fontbox/afm/CharMetric;->getWx()F

    move-result v5

    const/4 v6, 0x0

    cmpl-float v5, v5, v6

    if-lez v5, :cond_2

    invoke-virtual {v3}, Lorg/apache/fontbox/afm/CharMetric;->getWx()F

    move-result v3

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {p3, v4, v3}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    :cond_3
    sget-object p1, Lorg/apache/pdfbox/cos/COSName;->FIRST_CHAR:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p2, p1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->setInt(Lorg/apache/pdfbox/cos/COSName;I)V

    sget-object p1, Lorg/apache/pdfbox/cos/COSName;->LAST_CHAR:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p2, p1, p4}, Lorg/apache/pdfbox/cos/COSDictionary;->setInt(Lorg/apache/pdfbox/cos/COSName;I)V

    sget-object p1, Lorg/apache/pdfbox/cos/COSName;->WIDTHS:Lorg/apache/pdfbox/cos/COSName;

    invoke-static {p3}, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;->converterToCOSArray(Ljava/util/List;)Lorg/apache/pdfbox/cos/COSArray;

    move-result-object p3

    invoke-virtual {p2, p1, p3}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    return-void
.end method

.method public static buildFontDescriptor(Lorg/apache/fontbox/afm/FontMetrics;)Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;
    .locals 3

    invoke-virtual {p0}, Lorg/apache/fontbox/afm/FontMetrics;->getEncodingScheme()Ljava/lang/String;

    move-result-object v0

    const-string v1, "FontSpecific"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    new-instance v1, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;

    invoke-direct {v1}, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;-><init>()V

    invoke-virtual {p0}, Lorg/apache/fontbox/afm/FontMetrics;->getFontName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->setFontName(Ljava/lang/String;)V

    invoke-virtual {p0}, Lorg/apache/fontbox/afm/FontMetrics;->getFamilyName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->setFontFamily(Ljava/lang/String;)V

    xor-int/lit8 v2, v0, 0x1

    invoke-virtual {v1, v2}, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->setNonSymbolic(Z)V

    invoke-virtual {v1, v0}, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->setSymbolic(Z)V

    new-instance v0, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;

    invoke-virtual {p0}, Lorg/apache/fontbox/afm/FontMetrics;->getFontBBox()Lorg/apache/fontbox/util/BoundingBox;

    move-result-object v2

    invoke-direct {v0, v2}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;-><init>(Lorg/apache/fontbox/util/BoundingBox;)V

    invoke-virtual {v1, v0}, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->setFontBoundingBox(Lorg/apache/pdfbox/pdmodel/common/PDRectangle;)V

    invoke-virtual {p0}, Lorg/apache/fontbox/afm/FontMetrics;->getItalicAngle()F

    move-result v0

    invoke-virtual {v1, v0}, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->setItalicAngle(F)V

    invoke-virtual {p0}, Lorg/apache/fontbox/afm/FontMetrics;->getAscender()F

    move-result v0

    invoke-virtual {v1, v0}, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->setAscent(F)V

    invoke-virtual {p0}, Lorg/apache/fontbox/afm/FontMetrics;->getDescender()F

    move-result v0

    invoke-virtual {v1, v0}, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->setDescent(F)V

    invoke-virtual {p0}, Lorg/apache/fontbox/afm/FontMetrics;->getCapHeight()F

    move-result v0

    invoke-virtual {v1, v0}, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->setCapHeight(F)V

    invoke-virtual {p0}, Lorg/apache/fontbox/afm/FontMetrics;->getXHeight()F

    move-result v0

    invoke-virtual {v1, v0}, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->setXHeight(F)V

    invoke-virtual {p0}, Lorg/apache/fontbox/afm/FontMetrics;->getAverageCharacterWidth()F

    move-result v0

    invoke-virtual {v1, v0}, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->setAverageWidth(F)V

    invoke-virtual {p0}, Lorg/apache/fontbox/afm/FontMetrics;->getCharacterSet()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v1, p0}, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->setCharacterSet(Ljava/lang/String;)V

    const/4 p0, 0x0

    invoke-virtual {v1, p0}, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->setStemV(F)V

    return-object v1
.end method

.method private encodingFromAFM(Lorg/apache/fontbox/afm/FontMetrics;)Lorg/apache/pdfbox/pdmodel/font/encoding/DictionaryEncoding;
    .locals 3

    new-instance v0, Lorg/apache/pdfbox/pdmodel/font/encoding/Type1Encoding;

    invoke-direct {v0, p1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Type1Encoding;-><init>(Lorg/apache/fontbox/afm/FontMetrics;)V

    new-instance p1, Lorg/apache/pdfbox/cos/COSArray;

    invoke-direct {p1}, Lorg/apache/pdfbox/cos/COSArray;-><init>()V

    sget-object v1, Lorg/apache/pdfbox/cos/COSInteger;->ZERO:Lorg/apache/pdfbox/cos/COSInteger;

    invoke-virtual {p1, v1}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    const/4 v1, 0x0

    :goto_0
    const/16 v2, 0x100

    if-ge v1, v2, :cond_0

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->getName(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lorg/apache/pdfbox/cos/COSName;->getPDFName(Ljava/lang/String;)Lorg/apache/pdfbox/cos/COSName;

    move-result-object v2

    invoke-virtual {p1, v2}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    const-string v0, "germandbls"

    invoke-static {v0}, Lorg/apache/pdfbox/cos/COSName;->getPDFName(Ljava/lang/String;)Lorg/apache/pdfbox/cos/COSName;

    move-result-object v0

    const/16 v1, 0xe0

    invoke-virtual {p1, v1, v0}, Lorg/apache/pdfbox/cos/COSArray;->set(ILorg/apache/pdfbox/cos/COSBase;)V

    const-string v0, "adieresis"

    invoke-static {v0}, Lorg/apache/pdfbox/cos/COSName;->getPDFName(Ljava/lang/String;)Lorg/apache/pdfbox/cos/COSName;

    move-result-object v0

    const/16 v1, 0xe5

    invoke-virtual {p1, v1, v0}, Lorg/apache/pdfbox/cos/COSArray;->set(ILorg/apache/pdfbox/cos/COSBase;)V

    const-string v0, "odieresis"

    invoke-static {v0}, Lorg/apache/pdfbox/cos/COSName;->getPDFName(Ljava/lang/String;)Lorg/apache/pdfbox/cos/COSName;

    move-result-object v0

    const/16 v1, 0xf7

    invoke-virtual {p1, v1, v0}, Lorg/apache/pdfbox/cos/COSArray;->set(ILorg/apache/pdfbox/cos/COSBase;)V

    const-string v0, "udieresis"

    invoke-static {v0}, Lorg/apache/pdfbox/cos/COSName;->getPDFName(Ljava/lang/String;)Lorg/apache/pdfbox/cos/COSName;

    move-result-object v0

    const/16 v1, 0xfd

    invoke-virtual {p1, v1, v0}, Lorg/apache/pdfbox/cos/COSArray;->set(ILorg/apache/pdfbox/cos/COSBase;)V

    const-string v0, "Adieresis"

    invoke-static {v0}, Lorg/apache/pdfbox/cos/COSName;->getPDFName(Ljava/lang/String;)Lorg/apache/pdfbox/cos/COSName;

    move-result-object v0

    const/16 v1, 0xc5

    invoke-virtual {p1, v1, v0}, Lorg/apache/pdfbox/cos/COSArray;->set(ILorg/apache/pdfbox/cos/COSBase;)V

    const-string v0, "Odieresis"

    invoke-static {v0}, Lorg/apache/pdfbox/cos/COSName;->getPDFName(Ljava/lang/String;)Lorg/apache/pdfbox/cos/COSName;

    move-result-object v0

    const/16 v1, 0xd7

    invoke-virtual {p1, v1, v0}, Lorg/apache/pdfbox/cos/COSArray;->set(ILorg/apache/pdfbox/cos/COSBase;)V

    const-string v0, "Udieresis"

    invoke-static {v0}, Lorg/apache/pdfbox/cos/COSName;->getPDFName(Ljava/lang/String;)Lorg/apache/pdfbox/cos/COSName;

    move-result-object v0

    const/16 v1, 0xdd

    invoke-virtual {p1, v1, v0}, Lorg/apache/pdfbox/cos/COSArray;->set(ILorg/apache/pdfbox/cos/COSBase;)V

    new-instance v0, Lorg/apache/pdfbox/pdmodel/font/encoding/DictionaryEncoding;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->STANDARD_ENCODING:Lorg/apache/pdfbox/cos/COSName;

    invoke-direct {v0, v1, p1}, Lorg/apache/pdfbox/pdmodel/font/encoding/DictionaryEncoding;-><init>(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSArray;)V

    return-object v0
.end method


# virtual methods
.method public getFontEncoding()Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDType1FontEmbedder;->fontEncoding:Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;

    return-object v0
.end method

.method public getFontMetrics()Lorg/apache/fontbox/afm/FontMetrics;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDType1FontEmbedder;->metrics:Lorg/apache/fontbox/afm/FontMetrics;

    return-object v0
.end method

.method public getType1Font()Lorg/apache/fontbox/type1/Type1Font;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDType1FontEmbedder;->type1:Lorg/apache/fontbox/type1/Type1Font;

    return-object v0
.end method

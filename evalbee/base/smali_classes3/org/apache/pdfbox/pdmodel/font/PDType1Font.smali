.class public Lorg/apache/pdfbox/pdmodel/font/PDType1Font;
.super Lorg/apache/pdfbox/pdmodel/font/PDSimpleFont;
.source "SourceFile"

# interfaces
.implements Lorg/apache/pdfbox/pdmodel/font/PDType1Equivalent;


# static fields
.field private static final ALT_NAMES:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final COURIER:Lorg/apache/pdfbox/pdmodel/font/PDType1Font;

.field public static final COURIER_BOLD:Lorg/apache/pdfbox/pdmodel/font/PDType1Font;

.field public static final COURIER_BOLD_OBLIQUE:Lorg/apache/pdfbox/pdmodel/font/PDType1Font;

.field public static final COURIER_OBLIQUE:Lorg/apache/pdfbox/pdmodel/font/PDType1Font;

.field public static final HELVETICA:Lorg/apache/pdfbox/pdmodel/font/PDType1Font;

.field public static final HELVETICA_BOLD:Lorg/apache/pdfbox/pdmodel/font/PDType1Font;

.field public static final HELVETICA_BOLD_OBLIQUE:Lorg/apache/pdfbox/pdmodel/font/PDType1Font;

.field public static final HELVETICA_OBLIQUE:Lorg/apache/pdfbox/pdmodel/font/PDType1Font;

.field private static final PFB_START_MARKER:I = 0x80

.field public static final SYMBOL:Lorg/apache/pdfbox/pdmodel/font/PDType1Font;

.field public static final TIMES_BOLD:Lorg/apache/pdfbox/pdmodel/font/PDType1Font;

.field public static final TIMES_BOLD_ITALIC:Lorg/apache/pdfbox/pdmodel/font/PDType1Font;

.field public static final TIMES_ITALIC:Lorg/apache/pdfbox/pdmodel/font/PDType1Font;

.field public static final TIMES_ROMAN:Lorg/apache/pdfbox/pdmodel/font/PDType1Font;

.field public static final ZAPF_DINGBATS:Lorg/apache/pdfbox/pdmodel/font/PDType1Font;


# instance fields
.field private fontMatrix:Lorg/apache/pdfbox/util/Matrix;

.field private invertedEncoding:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final isDamaged:Z

.field private final isEmbedded:Z

.field private final type1Equivalent:Lorg/apache/fontbox/ttf/Type1Equivalent;

.field private final type1font:Lorg/apache/fontbox/type1/Type1Font;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lorg/apache/pdfbox/pdmodel/font/PDType1Font;->ALT_NAMES:Ljava/util/Map;

    const-string v1, "ff"

    const-string v2, "f_f"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "ffi"

    const-string v2, "f_f_i"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "ffl"

    const-string v2, "f_f_l"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "fi"

    const-string v2, "f_i"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "fl"

    const-string v2, "f_l"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "st"

    const-string v2, "s_t"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "IJ"

    const-string v2, "I_J"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "ij"

    const-string v2, "i_j"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "ellipsis"

    const-string v2, "elipsis"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lorg/apache/pdfbox/pdmodel/font/PDType1Font;

    const-string v1, "Times-Roman"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/pdmodel/font/PDType1Font;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/pdmodel/font/PDType1Font;->TIMES_ROMAN:Lorg/apache/pdfbox/pdmodel/font/PDType1Font;

    new-instance v0, Lorg/apache/pdfbox/pdmodel/font/PDType1Font;

    const-string v1, "Times-Bold"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/pdmodel/font/PDType1Font;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/pdmodel/font/PDType1Font;->TIMES_BOLD:Lorg/apache/pdfbox/pdmodel/font/PDType1Font;

    new-instance v0, Lorg/apache/pdfbox/pdmodel/font/PDType1Font;

    const-string v1, "Times-Italic"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/pdmodel/font/PDType1Font;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/pdmodel/font/PDType1Font;->TIMES_ITALIC:Lorg/apache/pdfbox/pdmodel/font/PDType1Font;

    new-instance v0, Lorg/apache/pdfbox/pdmodel/font/PDType1Font;

    const-string v1, "Times-BoldItalic"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/pdmodel/font/PDType1Font;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/pdmodel/font/PDType1Font;->TIMES_BOLD_ITALIC:Lorg/apache/pdfbox/pdmodel/font/PDType1Font;

    new-instance v0, Lorg/apache/pdfbox/pdmodel/font/PDType1Font;

    const-string v1, "Helvetica"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/pdmodel/font/PDType1Font;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/pdmodel/font/PDType1Font;->HELVETICA:Lorg/apache/pdfbox/pdmodel/font/PDType1Font;

    new-instance v0, Lorg/apache/pdfbox/pdmodel/font/PDType1Font;

    const-string v1, "Helvetica-Bold"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/pdmodel/font/PDType1Font;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/pdmodel/font/PDType1Font;->HELVETICA_BOLD:Lorg/apache/pdfbox/pdmodel/font/PDType1Font;

    new-instance v0, Lorg/apache/pdfbox/pdmodel/font/PDType1Font;

    const-string v1, "Helvetica-Oblique"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/pdmodel/font/PDType1Font;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/pdmodel/font/PDType1Font;->HELVETICA_OBLIQUE:Lorg/apache/pdfbox/pdmodel/font/PDType1Font;

    new-instance v0, Lorg/apache/pdfbox/pdmodel/font/PDType1Font;

    const-string v1, "Helvetica-BoldOblique"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/pdmodel/font/PDType1Font;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/pdmodel/font/PDType1Font;->HELVETICA_BOLD_OBLIQUE:Lorg/apache/pdfbox/pdmodel/font/PDType1Font;

    new-instance v0, Lorg/apache/pdfbox/pdmodel/font/PDType1Font;

    const-string v1, "Courier"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/pdmodel/font/PDType1Font;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/pdmodel/font/PDType1Font;->COURIER:Lorg/apache/pdfbox/pdmodel/font/PDType1Font;

    new-instance v0, Lorg/apache/pdfbox/pdmodel/font/PDType1Font;

    const-string v1, "Courier-Bold"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/pdmodel/font/PDType1Font;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/pdmodel/font/PDType1Font;->COURIER_BOLD:Lorg/apache/pdfbox/pdmodel/font/PDType1Font;

    new-instance v0, Lorg/apache/pdfbox/pdmodel/font/PDType1Font;

    const-string v1, "Courier-Oblique"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/pdmodel/font/PDType1Font;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/pdmodel/font/PDType1Font;->COURIER_OBLIQUE:Lorg/apache/pdfbox/pdmodel/font/PDType1Font;

    new-instance v0, Lorg/apache/pdfbox/pdmodel/font/PDType1Font;

    const-string v1, "Courier-BoldOblique"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/pdmodel/font/PDType1Font;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/pdmodel/font/PDType1Font;->COURIER_BOLD_OBLIQUE:Lorg/apache/pdfbox/pdmodel/font/PDType1Font;

    new-instance v0, Lorg/apache/pdfbox/pdmodel/font/PDType1Font;

    const-string v1, "Symbol"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/pdmodel/font/PDType1Font;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/pdmodel/font/PDType1Font;->SYMBOL:Lorg/apache/pdfbox/pdmodel/font/PDType1Font;

    new-instance v0, Lorg/apache/pdfbox/pdmodel/font/PDType1Font;

    const-string v1, "ZapfDingbats"

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/pdmodel/font/PDType1Font;-><init>(Ljava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/pdmodel/font/PDType1Font;->ZAPF_DINGBATS:Lorg/apache/pdfbox/pdmodel/font/PDType1Font;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;)V
    .locals 3

    .line 1
    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/font/PDSimpleFont;-><init>(Ljava/lang/String;)V

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDFont;->dict:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->SUBTYPE:Lorg/apache/pdfbox/cos/COSName;

    sget-object v2, Lorg/apache/pdfbox/cos/COSName;->TYPE1:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDFont;->dict:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->BASE_FONT:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setName(Lorg/apache/pdfbox/cos/COSName;Ljava/lang/String;)V

    new-instance p1, Lorg/apache/pdfbox/pdmodel/font/encoding/WinAnsiEncoding;

    invoke-direct {p1}, Lorg/apache/pdfbox/pdmodel/font/encoding/WinAnsiEncoding;-><init>()V

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/font/PDSimpleFont;->encoding:Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;

    iget-object p1, p0, Lorg/apache/pdfbox/pdmodel/font/PDFont;->dict:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->ENCODING:Lorg/apache/pdfbox/cos/COSName;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->WIN_ANSI_ENCODING:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p1, v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    const/4 p1, 0x0

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/font/PDType1Font;->type1font:Lorg/apache/fontbox/type1/Type1Font;

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDType1Font;->getBaseFont()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lorg/apache/pdfbox/pdmodel/font/ExternalFonts;->getType1EquivalentFont(Ljava/lang/String;)Lorg/apache/fontbox/ttf/Type1Equivalent;

    move-result-object p1

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/font/PDType1Font;->type1Equivalent:Lorg/apache/fontbox/ttf/Type1Equivalent;

    const/4 p1, 0x0

    iput-boolean p1, p0, Lorg/apache/pdfbox/pdmodel/font/PDType1Font;->isEmbedded:Z

    iput-boolean p1, p0, Lorg/apache/pdfbox/pdmodel/font/PDType1Font;->isDamaged:Z

    return-void
.end method

.method public constructor <init>(Lorg/apache/pdfbox/cos/COSDictionary;)V
    .locals 9

    .line 2
    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/font/PDSimpleFont;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDFont;->getFontDescriptor()Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;

    move-result-object p1

    const-string v0, "PdfBoxAndroid"

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x0

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->getFontFile3()Lorg/apache/pdfbox/pdmodel/common/PDStream;

    move-result-object v4

    if-nez v4, :cond_1

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->getFontFile()Lorg/apache/pdfbox/pdmodel/common/PDStream;

    move-result-object v4

    if-eqz v4, :cond_2

    :try_start_0
    invoke-virtual {v4}, Lorg/apache/pdfbox/pdmodel/common/PDStream;->getStream()Lorg/apache/pdfbox/cos/COSStream;

    move-result-object v5

    sget-object v6, Lorg/apache/pdfbox/cos/COSName;->LENGTH1:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v5, v6}, Lorg/apache/pdfbox/cos/COSDictionary;->getInt(Lorg/apache/pdfbox/cos/COSName;)I

    move-result v6

    sget-object v7, Lorg/apache/pdfbox/cos/COSName;->LENGTH2:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v5, v7}, Lorg/apache/pdfbox/cos/COSDictionary;->getInt(Lorg/apache/pdfbox/cos/COSName;)I

    move-result v5

    invoke-virtual {v4}, Lorg/apache/pdfbox/pdmodel/common/PDStream;->getByteArray()[B

    move-result-object v4

    invoke-direct {p0, v4, v6}, Lorg/apache/pdfbox/pdmodel/font/PDType1Font;->repairLength1([BI)I

    move-result v6

    array-length v7, v4

    if-lez v7, :cond_0

    aget-byte v7, v4, v3

    and-int/lit16 v7, v7, 0xff

    const/16 v8, 0x80

    if-ne v7, v8, :cond_0

    invoke-static {v4}, Lorg/apache/fontbox/type1/Type1Font;->createWithPFB([B)Lorg/apache/fontbox/type1/Type1Font;

    move-result-object p1

    :goto_0
    move-object v2, p1

    goto :goto_2

    :cond_0
    invoke-static {v4, v3, v6}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object v7

    add-int v8, v6, v5

    invoke-static {v4, v6, v8}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object v4

    if-lez v6, :cond_2

    if-lez v5, :cond_2

    invoke-static {v7, v4}, Lorg/apache/fontbox/type1/Type1Font;->createWithSegments([B[B)Lorg/apache/fontbox/type1/Type1Font;

    move-result-object p1
    :try_end_0
    .catch Lorg/apache/fontbox/type1/DamagedFontException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Can\'t read the embedded Type1 font "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->getFontName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    :catch_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Can\'t read damaged embedded Type1 font "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->getFontName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_1
    move p1, v1

    goto :goto_3

    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Use PDType1CFont for FontFile3"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_2
    :goto_2
    move p1, v3

    :goto_3
    if-eqz v2, :cond_3

    goto :goto_4

    :cond_3
    move v1, v3

    :goto_4
    iput-boolean v1, p0, Lorg/apache/pdfbox/pdmodel/font/PDType1Font;->isEmbedded:Z

    iput-boolean p1, p0, Lorg/apache/pdfbox/pdmodel/font/PDType1Font;->isDamaged:Z

    if-nez v2, :cond_4

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDType1Font;->getBaseFont()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lorg/apache/pdfbox/pdmodel/font/ExternalFonts;->getType1Font(Ljava/lang/String;)Lorg/apache/fontbox/type1/Type1Font;

    move-result-object v2

    :cond_4
    iput-object v2, p0, Lorg/apache/pdfbox/pdmodel/font/PDType1Font;->type1font:Lorg/apache/fontbox/type1/Type1Font;

    if-eqz v2, :cond_5

    iput-object v2, p0, Lorg/apache/pdfbox/pdmodel/font/PDType1Font;->type1Equivalent:Lorg/apache/fontbox/ttf/Type1Equivalent;

    goto :goto_5

    :cond_5
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDType1Font;->getBaseFont()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lorg/apache/pdfbox/pdmodel/font/ExternalFonts;->getType1EquivalentFont(Ljava/lang/String;)Lorg/apache/fontbox/ttf/Type1Equivalent;

    move-result-object p1

    if-eqz p1, :cond_6

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/font/PDType1Font;->type1Equivalent:Lorg/apache/fontbox/ttf/Type1Equivalent;

    goto :goto_5

    :cond_6
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDFont;->getFontDescriptor()Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;

    move-result-object p1

    invoke-static {p1}, Lorg/apache/pdfbox/pdmodel/font/ExternalFonts;->getType1FallbackFont(Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;)Lorg/apache/fontbox/ttf/Type1Equivalent;

    move-result-object p1

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/font/PDType1Font;->type1Equivalent:Lorg/apache/fontbox/ttf/Type1Equivalent;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Using fallback font "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {p1}, Lorg/apache/fontbox/ttf/Type1Equivalent;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, " for "

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDType1Font;->getBaseFont()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_5
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDSimpleFont;->readEncoding()V

    return-void
.end method

.method public constructor <init>(Lorg/apache/pdfbox/pdmodel/PDDocument;Ljava/io/InputStream;Ljava/io/InputStream;)V
    .locals 2

    .line 3
    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/font/PDSimpleFont;-><init>()V

    new-instance v0, Lorg/apache/pdfbox/pdmodel/font/PDType1FontEmbedder;

    iget-object v1, p0, Lorg/apache/pdfbox/pdmodel/font/PDFont;->dict:Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-direct {v0, p1, v1, p2, p3}, Lorg/apache/pdfbox/pdmodel/font/PDType1FontEmbedder;-><init>(Lorg/apache/pdfbox/pdmodel/PDDocument;Lorg/apache/pdfbox/cos/COSDictionary;Ljava/io/InputStream;Ljava/io/InputStream;)V

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/font/PDType1FontEmbedder;->getFontEncoding()Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;

    move-result-object p1

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/font/PDSimpleFont;->encoding:Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/font/PDType1FontEmbedder;->getType1Font()Lorg/apache/fontbox/type1/Type1Font;

    move-result-object p1

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/font/PDType1Font;->type1font:Lorg/apache/fontbox/type1/Type1Font;

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/font/PDType1FontEmbedder;->getType1Font()Lorg/apache/fontbox/type1/Type1Font;

    move-result-object p1

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/font/PDType1Font;->type1Equivalent:Lorg/apache/fontbox/ttf/Type1Equivalent;

    const/4 p1, 0x1

    iput-boolean p1, p0, Lorg/apache/pdfbox/pdmodel/font/PDType1Font;->isEmbedded:Z

    const/4 p1, 0x0

    iput-boolean p1, p0, Lorg/apache/pdfbox/pdmodel/font/PDType1Font;->isDamaged:Z

    return-void
.end method

.method private getInvertedEncoding()Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDType1Font;->invertedEncoding:Ljava/util/Map;

    if-eqz v0, :cond_0

    return-object v0

    :cond_0
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDType1Font;->invertedEncoding:Ljava/util/Map;

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDSimpleFont;->encoding:Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->getCodeToNameMap()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_1
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    iget-object v2, p0, Lorg/apache/pdfbox/pdmodel/font/PDType1Font;->invertedEncoding:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lorg/apache/pdfbox/pdmodel/font/PDType1Font;->invertedEncoding:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v2, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDType1Font;->invertedEncoding:Ljava/util/Map;

    return-object v0
.end method

.method private getNameInFont(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDType1Font;->isEmbedded()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDType1Font;->type1Equivalent:Lorg/apache/fontbox/ttf/Type1Equivalent;

    invoke-interface {v0, p1}, Lorg/apache/fontbox/ttf/Type1Equivalent;->hasGlyph(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    sget-object v0, Lorg/apache/pdfbox/pdmodel/font/PDType1Font;->ALT_NAMES:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v1, ".notdef"

    if-eqz v0, :cond_1

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lorg/apache/pdfbox/pdmodel/font/PDType1Font;->type1Equivalent:Lorg/apache/fontbox/ttf/Type1Equivalent;

    invoke-interface {v2, v0}, Lorg/apache/fontbox/ttf/Type1Equivalent;->hasGlyph(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    return-object v0

    :cond_1
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDSimpleFont;->getGlyphList()Lorg/apache/pdfbox/pdmodel/font/encoding/GlyphList;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/pdfbox/pdmodel/font/encoding/GlyphList;->toUnicode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_2

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Ljava/lang/String;->codePointAt(I)I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    filled-new-array {p1}, [Ljava/lang/Object;

    move-result-object p1

    const-string v0, "uni%04X"

    invoke-static {v0, p1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDType1Font;->type1Equivalent:Lorg/apache/fontbox/ttf/Type1Equivalent;

    invoke-interface {v0, p1}, Lorg/apache/fontbox/ttf/Type1Equivalent;->hasGlyph(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    return-object p1

    :cond_2
    return-object v1

    :cond_3
    :goto_0
    return-object p1
.end method

.method private repairLength1([BI)I
    .locals 4

    add-int/lit8 v0, p2, -0x4

    const/4 v1, 0x0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    :goto_0
    if-lez v0, :cond_2

    add-int/lit8 v1, v0, 0x0

    aget-byte v1, p1, v1

    const/16 v2, 0x65

    if-ne v1, v2, :cond_1

    add-int/lit8 v1, v0, 0x1

    aget-byte v1, p1, v1

    const/16 v3, 0x78

    if-ne v1, v3, :cond_1

    add-int/lit8 v1, v0, 0x2

    aget-byte v1, p1, v1

    if-ne v1, v2, :cond_1

    add-int/lit8 v1, v0, 0x3

    aget-byte v1, p1, v1

    const/16 v2, 0x63

    if-ne v1, v2, :cond_1

    add-int/lit8 v0, v0, 0x4

    :goto_1
    if-ge v0, p2, :cond_2

    aget-byte v1, p1, v0

    const/16 v2, 0xd

    if-eq v1, v2, :cond_0

    const/16 v2, 0xa

    if-eq v1, v2, :cond_0

    const/16 v2, 0x20

    if-ne v1, v2, :cond_2

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_2
    sub-int p1, p2, v0

    if-eqz p1, :cond_3

    if-lez v0, :cond_3

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string p2, "Ignored invalid Length1 for Type 1 font "

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDType1Font;->getName()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string p2, "PdfBoxAndroid"

    invoke-static {p2, p1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    return v0

    :cond_3
    return p2
.end method


# virtual methods
.method public codeToName(I)Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDSimpleFont;->getEncoding()Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->getName(I)Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/font/PDType1Font;->getNameInFont(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public encode(I)[B
    .locals 4

    const/16 v0, 0xff

    if-gt p1, v0, :cond_1

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDSimpleFont;->getGlyphList()Lorg/apache/pdfbox/pdmodel/font/encoding/GlyphList;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/pdfbox/pdmodel/font/encoding/GlyphList;->codePointToName(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/pdmodel/font/PDType1Font;->getNameInFont(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/font/PDType1Font;->getInvertedEncoding()Ljava/util/Map;

    move-result-object v2

    const-string v3, ".notdef"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lorg/apache/pdfbox/pdmodel/font/PDType1Font;->type1Equivalent:Lorg/apache/fontbox/ttf/Type1Equivalent;

    invoke-interface {v3, v1}, Lorg/apache/fontbox/ttf/Type1Equivalent;->hasGlyph(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    const/4 v0, 0x1

    new-array v0, v0, [B

    const/4 v1, 0x0

    int-to-byte p1, p1

    aput-byte p1, v0, v1

    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDType1Font;->getName()Ljava/lang/String;

    move-result-object v1

    filled-new-array {p1, v1}, [Ljava/lang/Object;

    move-result-object p1

    const-string v1, "No glyph for U+%04X in font %s"

    invoke-static {v1, p1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "This font type only supports 8-bit code points"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public getAverageFontWidth()F
    .locals 1

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDFont;->getStandard14AFM()Lorg/apache/fontbox/afm/FontMetrics;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDFont;->getStandard14AFM()Lorg/apache/fontbox/afm/FontMetrics;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/fontbox/afm/FontMetrics;->getAverageCharacterWidth()F

    move-result v0

    return v0

    :cond_0
    invoke-super {p0}, Lorg/apache/pdfbox/pdmodel/font/PDFont;->getAverageFontWidth()F

    move-result v0

    return v0
.end method

.method public getBaseFont()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDFont;->dict:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->BASE_FONT:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getNameAsString(Lorg/apache/pdfbox/cos/COSName;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getBoundingBox()Lorg/apache/fontbox/util/BoundingBox;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDType1Font;->type1Equivalent:Lorg/apache/fontbox/ttf/Type1Equivalent;

    invoke-interface {v0}, Lorg/apache/fontbox/ttf/Type1Equivalent;->getFontBBox()Lorg/apache/fontbox/util/BoundingBox;

    move-result-object v0

    return-object v0
.end method

.method public getFontMatrix()Lorg/apache/pdfbox/util/Matrix;
    .locals 10

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDType1Font;->fontMatrix:Lorg/apache/pdfbox/util/Matrix;

    if-nez v0, :cond_2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDType1Font;->type1font:Lorg/apache/fontbox/type1/Type1Font;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lorg/apache/fontbox/type1/Type1Font;->getFontMatrix()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x6

    if-ne v1, v2, :cond_0

    new-instance v1, Lorg/apache/pdfbox/util/Matrix;

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Number;

    invoke-virtual {v2}, Ljava/lang/Number;->floatValue()F

    move-result v4

    const/4 v2, 0x1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Number;

    invoke-virtual {v2}, Ljava/lang/Number;->floatValue()F

    move-result v5

    const/4 v2, 0x2

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Number;

    invoke-virtual {v2}, Ljava/lang/Number;->floatValue()F

    move-result v6

    const/4 v2, 0x3

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Number;

    invoke-virtual {v2}, Ljava/lang/Number;->floatValue()F

    move-result v7

    const/4 v2, 0x4

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Number;

    invoke-virtual {v2}, Ljava/lang/Number;->floatValue()F

    move-result v8

    const/4 v2, 0x5

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->floatValue()F

    move-result v9

    move-object v3, v1

    invoke-direct/range {v3 .. v9}, Lorg/apache/pdfbox/util/Matrix;-><init>(FFFFFF)V

    iput-object v1, p0, Lorg/apache/pdfbox/pdmodel/font/PDType1Font;->fontMatrix:Lorg/apache/pdfbox/util/Matrix;

    goto :goto_0

    :cond_0
    invoke-super {p0}, Lorg/apache/pdfbox/pdmodel/font/PDFont;->getFontMatrix()Lorg/apache/pdfbox/util/Matrix;

    move-result-object v0

    return-object v0

    :cond_1
    sget-object v0, Lorg/apache/pdfbox/pdmodel/font/PDFont;->DEFAULT_FONT_MATRIX:Lorg/apache/pdfbox/util/Matrix;

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDType1Font;->fontMatrix:Lorg/apache/pdfbox/util/Matrix;

    :cond_2
    :goto_0
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDType1Font;->fontMatrix:Lorg/apache/pdfbox/util/Matrix;

    return-object v0
.end method

.method public getHeight(I)F
    .locals 2

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/pdmodel/font/PDType1Font;->codeToName(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDFont;->getStandard14AFM()Lorg/apache/fontbox/afm/FontMetrics;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDSimpleFont;->getEncoding()Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->getName(I)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDFont;->getStandard14AFM()Lorg/apache/fontbox/afm/FontMetrics;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/fontbox/afm/FontMetrics;->getCharacterHeight(Ljava/lang/String;)F

    move-result p1

    return p1

    :cond_0
    new-instance p1, Landroid/graphics/RectF;

    invoke-direct {p1}, Landroid/graphics/RectF;-><init>()V

    iget-object v1, p0, Lorg/apache/pdfbox/pdmodel/font/PDType1Font;->type1Equivalent:Lorg/apache/fontbox/ttf/Type1Equivalent;

    invoke-interface {v1, v0}, Lorg/apache/fontbox/ttf/Type1Equivalent;->getPath(Ljava/lang/String;)Landroid/graphics/Path;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Landroid/graphics/Path;->computeBounds(Landroid/graphics/RectF;Z)V

    invoke-virtual {p1}, Landroid/graphics/RectF;->height()F

    move-result p1

    return p1
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDType1Font;->getBaseFont()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPath(Ljava/lang/String;)Landroid/graphics/Path;
    .locals 1

    const-string v0, ".notdef"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDType1Font;->isEmbedded:Z

    if-nez v0, :cond_0

    new-instance p1, Landroid/graphics/Path;

    invoke-direct {p1}, Landroid/graphics/Path;-><init>()V

    return-object p1

    :cond_0
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDType1Font;->type1Equivalent:Lorg/apache/fontbox/ttf/Type1Equivalent;

    invoke-interface {v0, p1}, Lorg/apache/fontbox/ttf/Type1Equivalent;->getPath(Ljava/lang/String;)Landroid/graphics/Path;

    move-result-object p1

    return-object p1
.end method

.method public getType1Equivalent()Lorg/apache/fontbox/ttf/Type1Equivalent;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDType1Font;->type1Equivalent:Lorg/apache/fontbox/ttf/Type1Equivalent;

    return-object v0
.end method

.method public getType1Font()Lorg/apache/fontbox/type1/Type1Font;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDType1Font;->type1font:Lorg/apache/fontbox/type1/Type1Font;

    return-object v0
.end method

.method public getWidthFromFont(I)F
    .locals 2

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/pdmodel/font/PDType1Font;->codeToName(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDFont;->getStandard14AFM()Lorg/apache/fontbox/afm/FontMetrics;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/pdmodel/font/PDSimpleFont;->getStandard14Width(I)F

    move-result p1

    return p1

    :cond_0
    iget-object p1, p0, Lorg/apache/pdfbox/pdmodel/font/PDType1Font;->type1Equivalent:Lorg/apache/fontbox/ttf/Type1Equivalent;

    invoke-interface {p1, v0}, Lorg/apache/fontbox/ttf/Type1Equivalent;->getWidth(Ljava/lang/String;)F

    move-result p1

    return p1
.end method

.method public isDamaged()Z
    .locals 1

    iget-boolean v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDType1Font;->isDamaged:Z

    return v0
.end method

.method public isEmbedded()Z
    .locals 1

    iget-boolean v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDType1Font;->isEmbedded:Z

    return v0
.end method

.method public readCode(Ljava/io/InputStream;)I
    .locals 0

    invoke-virtual {p1}, Ljava/io/InputStream;->read()I

    move-result p1

    return p1
.end method

.method public readEncodingFromFont()Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDFont;->getStandard14AFM()Lorg/apache/fontbox/afm/FontMetrics;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v0, Lorg/apache/pdfbox/pdmodel/font/encoding/Type1Encoding;

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDFont;->getStandard14AFM()Lorg/apache/fontbox/afm/FontMetrics;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Type1Encoding;-><init>(Lorg/apache/fontbox/afm/FontMetrics;)V

    return-object v0

    :cond_0
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDType1Font;->type1Equivalent:Lorg/apache/fontbox/ttf/Type1Equivalent;

    invoke-interface {v0}, Lorg/apache/fontbox/ttf/Type1Equivalent;->getEncoding()Lorg/apache/fontbox/encoding/Encoding;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDType1Font;->type1Equivalent:Lorg/apache/fontbox/ttf/Type1Equivalent;

    invoke-interface {v0}, Lorg/apache/fontbox/ttf/Type1Equivalent;->getEncoding()Lorg/apache/fontbox/encoding/Encoding;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/pdfbox/pdmodel/font/encoding/Type1Encoding;->fromFontBox(Lorg/apache/fontbox/encoding/Encoding;)Lorg/apache/pdfbox/pdmodel/font/encoding/Type1Encoding;

    move-result-object v0

    return-object v0

    :cond_1
    sget-object v0, Lorg/apache/pdfbox/pdmodel/font/encoding/StandardEncoding;->INSTANCE:Lorg/apache/pdfbox/pdmodel/font/encoding/StandardEncoding;

    return-object v0
.end method

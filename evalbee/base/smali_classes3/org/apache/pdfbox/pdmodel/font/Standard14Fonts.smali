.class Lorg/apache/pdfbox/pdmodel/font/Standard14Fonts;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final STANDARD14_AFM_MAP:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lorg/apache/fontbox/afm/FontMetrics;",
            ">;"
        }
    .end annotation
.end field

.field private static final STANDARD_14_MAPPING:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static final STANDARD_14_NAMES:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 14

    const-string v0, "Helvetica-Oblique"

    const-string v1, "Helvetica-BoldOblique"

    const-string v2, "Helvetica-Bold"

    const-string v3, "Helvetica"

    const-string v4, "Courier-Oblique"

    const-string v5, "Courier-BoldOblique"

    const-string v6, "Courier-Bold"

    const-string v7, "Times-Roman"

    const-string v8, "Times-Italic"

    const-string v9, "Times-BoldItalic"

    const-string v10, "Times-Bold"

    const-string v11, "Courier"

    const-string v12, "Symbol"

    new-instance v13, Ljava/util/HashSet;

    invoke-direct {v13}, Ljava/util/HashSet;-><init>()V

    sput-object v13, Lorg/apache/pdfbox/pdmodel/font/Standard14Fonts;->STANDARD_14_NAMES:Ljava/util/Set;

    new-instance v13, Ljava/util/HashMap;

    invoke-direct {v13}, Ljava/util/HashMap;-><init>()V

    sput-object v13, Lorg/apache/pdfbox/pdmodel/font/Standard14Fonts;->STANDARD_14_MAPPING:Ljava/util/Map;

    :try_start_0
    new-instance v13, Ljava/util/HashMap;

    invoke-direct {v13}, Ljava/util/HashMap;-><init>()V

    sput-object v13, Lorg/apache/pdfbox/pdmodel/font/Standard14Fonts;->STANDARD14_AFM_MAP:Ljava/util/Map;

    invoke-static {v6}, Lorg/apache/pdfbox/pdmodel/font/Standard14Fonts;->addAFM(Ljava/lang/String;)V

    invoke-static {v5}, Lorg/apache/pdfbox/pdmodel/font/Standard14Fonts;->addAFM(Ljava/lang/String;)V

    invoke-static {v11}, Lorg/apache/pdfbox/pdmodel/font/Standard14Fonts;->addAFM(Ljava/lang/String;)V

    invoke-static {v4}, Lorg/apache/pdfbox/pdmodel/font/Standard14Fonts;->addAFM(Ljava/lang/String;)V

    invoke-static {v3}, Lorg/apache/pdfbox/pdmodel/font/Standard14Fonts;->addAFM(Ljava/lang/String;)V

    invoke-static {v2}, Lorg/apache/pdfbox/pdmodel/font/Standard14Fonts;->addAFM(Ljava/lang/String;)V

    invoke-static {v1}, Lorg/apache/pdfbox/pdmodel/font/Standard14Fonts;->addAFM(Ljava/lang/String;)V

    invoke-static {v0}, Lorg/apache/pdfbox/pdmodel/font/Standard14Fonts;->addAFM(Ljava/lang/String;)V

    invoke-static {v12}, Lorg/apache/pdfbox/pdmodel/font/Standard14Fonts;->addAFM(Ljava/lang/String;)V

    invoke-static {v10}, Lorg/apache/pdfbox/pdmodel/font/Standard14Fonts;->addAFM(Ljava/lang/String;)V

    invoke-static {v9}, Lorg/apache/pdfbox/pdmodel/font/Standard14Fonts;->addAFM(Ljava/lang/String;)V

    invoke-static {v8}, Lorg/apache/pdfbox/pdmodel/font/Standard14Fonts;->addAFM(Ljava/lang/String;)V

    invoke-static {v7}, Lorg/apache/pdfbox/pdmodel/font/Standard14Fonts;->addAFM(Ljava/lang/String;)V

    const-string v13, "ZapfDingbats"

    invoke-static {v13}, Lorg/apache/pdfbox/pdmodel/font/Standard14Fonts;->addAFM(Ljava/lang/String;)V

    const-string v13, "CourierCourierNew"

    invoke-static {v13, v11}, Lorg/apache/pdfbox/pdmodel/font/Standard14Fonts;->addAFM(Ljava/lang/String;Ljava/lang/String;)V

    const-string v13, "CourierNew"

    invoke-static {v13, v11}, Lorg/apache/pdfbox/pdmodel/font/Standard14Fonts;->addAFM(Ljava/lang/String;Ljava/lang/String;)V

    const-string v11, "CourierNew,Italic"

    invoke-static {v11, v4}, Lorg/apache/pdfbox/pdmodel/font/Standard14Fonts;->addAFM(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "CourierNew,Bold"

    invoke-static {v4, v6}, Lorg/apache/pdfbox/pdmodel/font/Standard14Fonts;->addAFM(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "CourierNew,BoldItalic"

    invoke-static {v4, v5}, Lorg/apache/pdfbox/pdmodel/font/Standard14Fonts;->addAFM(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "Arial"

    invoke-static {v4, v3}, Lorg/apache/pdfbox/pdmodel/font/Standard14Fonts;->addAFM(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "Arial,Italic"

    invoke-static {v3, v0}, Lorg/apache/pdfbox/pdmodel/font/Standard14Fonts;->addAFM(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "Arial,Bold"

    invoke-static {v0, v2}, Lorg/apache/pdfbox/pdmodel/font/Standard14Fonts;->addAFM(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "Arial,BoldItalic"

    invoke-static {v0, v1}, Lorg/apache/pdfbox/pdmodel/font/Standard14Fonts;->addAFM(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "TimesNewRoman"

    invoke-static {v0, v7}, Lorg/apache/pdfbox/pdmodel/font/Standard14Fonts;->addAFM(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "TimesNewRoman,Italic"

    invoke-static {v0, v8}, Lorg/apache/pdfbox/pdmodel/font/Standard14Fonts;->addAFM(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "TimesNewRoman,Bold"

    invoke-static {v0, v10}, Lorg/apache/pdfbox/pdmodel/font/Standard14Fonts;->addAFM(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "TimesNewRoman,BoldItalic"

    invoke-static {v0, v9}, Lorg/apache/pdfbox/pdmodel/font/Standard14Fonts;->addAFM(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "Symbol,Italic"

    invoke-static {v0, v12}, Lorg/apache/pdfbox/pdmodel/font/Standard14Fonts;->addAFM(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "Symbol,Bold"

    invoke-static {v0, v12}, Lorg/apache/pdfbox/pdmodel/font/Standard14Fonts;->addAFM(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "Symbol,BoldItalic"

    invoke-static {v0, v12}, Lorg/apache/pdfbox/pdmodel/font/Standard14Fonts;->addAFM(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "Times"

    invoke-static {v0, v7}, Lorg/apache/pdfbox/pdmodel/font/Standard14Fonts;->addAFM(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "Times,Italic"

    invoke-static {v0, v8}, Lorg/apache/pdfbox/pdmodel/font/Standard14Fonts;->addAFM(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "Times,Bold"

    invoke-static {v0, v10}, Lorg/apache/pdfbox/pdmodel/font/Standard14Fonts;->addAFM(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "Times,BoldItalic"

    invoke-static {v0, v9}, Lorg/apache/pdfbox/pdmodel/font/Standard14Fonts;->addAFM(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static addAFM(Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-static {p0, p0}, Lorg/apache/pdfbox/pdmodel/font/Standard14Fonts;->addAFM(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private static addAFM(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .line 2
    sget-object v0, Lorg/apache/pdfbox/pdmodel/font/Standard14Fonts;->STANDARD_14_NAMES:Ljava/util/Set;

    invoke-interface {v0, p0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v0, Lorg/apache/pdfbox/pdmodel/font/Standard14Fonts;->STANDARD_14_MAPPING:Ljava/util/Map;

    invoke-interface {v0, p0, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lorg/apache/pdfbox/pdmodel/font/Standard14Fonts;->STANDARD14_AFM_MAP:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, p0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "org/apache/pdfbox/resources/afm/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, ".afm"

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {}, Lorg/apache/pdfbox/util/PDFBoxResourceLoader;->isReady()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {p1}, Lorg/apache/pdfbox/util/PDFBoxResourceLoader;->getStream(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object p1

    goto :goto_0

    :cond_1
    const-class v1, Lorg/apache/pdfbox/pdmodel/font/PDType1Font;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/ClassLoader;->getResource(Ljava/lang/String;)Ljava/net/URL;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/net/URL;->openStream()Ljava/io/InputStream;

    move-result-object p1

    :goto_0
    :try_start_0
    new-instance v1, Lorg/apache/fontbox/afm/AFMParser;

    invoke-direct {v1, p1}, Lorg/apache/fontbox/afm/AFMParser;-><init>(Ljava/io/InputStream;)V

    invoke-virtual {v1}, Lorg/apache/fontbox/afm/AFMParser;->parse()Lorg/apache/fontbox/afm/FontMetrics;

    move-result-object v1

    invoke-interface {v0, p0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p1}, Ljava/io/InputStream;->close()V

    return-void

    :catchall_0
    move-exception p0

    invoke-virtual {p1}, Ljava/io/InputStream;->close()V

    throw p0

    :cond_2
    new-instance p0, Ljava/io/IOException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, " not found"

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static containsName(Ljava/lang/String;)Z
    .locals 1

    sget-object v0, Lorg/apache/pdfbox/pdmodel/font/Standard14Fonts;->STANDARD_14_NAMES:Ljava/util/Set;

    invoke-interface {v0, p0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result p0

    return p0
.end method

.method public static getAFM(Ljava/lang/String;)Lorg/apache/fontbox/afm/FontMetrics;
    .locals 1

    sget-object v0, Lorg/apache/pdfbox/pdmodel/font/Standard14Fonts;->STANDARD14_AFM_MAP:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lorg/apache/fontbox/afm/FontMetrics;

    return-object p0
.end method

.method public static getMappedFontName(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    sget-object v0, Lorg/apache/pdfbox/pdmodel/font/Standard14Fonts;->STANDARD_14_MAPPING:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Ljava/lang/String;

    return-object p0
.end method

.method public static getNames()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    sget-object v0, Lorg/apache/pdfbox/pdmodel/font/Standard14Fonts;->STANDARD_14_NAMES:Ljava/util/Set;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.class public abstract Lorg/apache/pdfbox/pdmodel/font/PDFont;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/pdfbox/pdmodel/common/COSObjectable;
.implements Lorg/apache/pdfbox/pdmodel/font/PDFontLike;


# static fields
.field protected static final DEFAULT_FONT_MATRIX:Lorg/apache/pdfbox/util/Matrix;


# instance fields
.field private final afmStandard14:Lorg/apache/fontbox/afm/FontMetrics;

.field private avgFontWidth:F

.field protected final dict:Lorg/apache/pdfbox/cos/COSDictionary;

.field private fontDescriptor:Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;

.field private fontWidthOfSpace:F

.field private final toUnicodeCMap:Lorg/apache/fontbox/cmap/CMap;

.field private widths:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 8

    new-instance v7, Lorg/apache/pdfbox/util/Matrix;

    const v1, 0x3a83126f    # 0.001f

    const/4 v2, 0x0

    const/4 v3, 0x0

    const v4, 0x3a83126f    # 0.001f

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lorg/apache/pdfbox/util/Matrix;-><init>(FFFFFF)V

    sput-object v7, Lorg/apache/pdfbox/pdmodel/font/PDFont;->DEFAULT_FONT_MATRIX:Lorg/apache/pdfbox/util/Matrix;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDFont;->fontWidthOfSpace:F

    new-instance v0, Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-direct {v0}, Lorg/apache/pdfbox/cos/COSDictionary;-><init>()V

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDFont;->dict:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->TYPE:Lorg/apache/pdfbox/cos/COSName;

    sget-object v2, Lorg/apache/pdfbox/cos/COSName;->FONT:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDFont;->toUnicodeCMap:Lorg/apache/fontbox/cmap/CMap;

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDFont;->fontDescriptor:Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDFont;->afmStandard14:Lorg/apache/fontbox/afm/FontMetrics;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 3

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDFont;->fontWidthOfSpace:F

    new-instance v0, Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-direct {v0}, Lorg/apache/pdfbox/cos/COSDictionary;-><init>()V

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDFont;->dict:Lorg/apache/pdfbox/cos/COSDictionary;

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDFont;->toUnicodeCMap:Lorg/apache/fontbox/cmap/CMap;

    invoke-static {p1}, Lorg/apache/pdfbox/pdmodel/font/Standard14Fonts;->getAFM(Ljava/lang/String;)Lorg/apache/fontbox/afm/FontMetrics;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDFont;->afmStandard14:Lorg/apache/fontbox/afm/FontMetrics;

    if-eqz v0, :cond_0

    invoke-static {v0}, Lorg/apache/pdfbox/pdmodel/font/PDType1FontEmbedder;->buildFontDescriptor(Lorg/apache/fontbox/afm/FontMetrics;)Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;

    move-result-object p1

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/font/PDFont;->fontDescriptor:Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;

    return-void

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "No AFM for font "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public constructor <init>(Lorg/apache/pdfbox/cos/COSDictionary;)V
    .locals 3

    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDFont;->fontWidthOfSpace:F

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/font/PDFont;->dict:Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDFont;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/pdfbox/pdmodel/font/Standard14Fonts;->getAFM(Ljava/lang/String;)Lorg/apache/fontbox/afm/FontMetrics;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDFont;->afmStandard14:Lorg/apache/fontbox/afm/FontMetrics;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->FONT_DESC:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p1, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v1

    check-cast v1, Lorg/apache/pdfbox/cos/COSDictionary;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    new-instance v0, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    :goto_0
    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDFont;->fontDescriptor:Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;

    goto :goto_1

    :cond_0
    if-eqz v0, :cond_1

    invoke-static {v0}, Lorg/apache/pdfbox/pdmodel/font/PDType1FontEmbedder;->buildFontDescriptor(Lorg/apache/fontbox/afm/FontMetrics;)Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;

    move-result-object v0

    goto :goto_0

    :cond_1
    iput-object v2, p0, Lorg/apache/pdfbox/pdmodel/font/PDFont;->fontDescriptor:Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;

    :goto_1
    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->TO_UNICODE:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p1, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object p1

    if-eqz p1, :cond_2

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/pdmodel/font/PDFont;->readCMap(Lorg/apache/pdfbox/cos/COSBase;)Lorg/apache/fontbox/cmap/CMap;

    move-result-object p1

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/font/PDFont;->toUnicodeCMap:Lorg/apache/fontbox/cmap/CMap;

    if-eqz p1, :cond_3

    invoke-virtual {p1}, Lorg/apache/fontbox/cmap/CMap;->hasUnicodeMappings()Z

    move-result p1

    if-nez p1, :cond_3

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Invalid ToUnicode CMap in font "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDFont;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v0, "PdfBoxAndroid"

    invoke-static {v0, p1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    :cond_2
    iput-object v2, p0, Lorg/apache/pdfbox/pdmodel/font/PDFont;->toUnicodeCMap:Lorg/apache/fontbox/cmap/CMap;

    :cond_3
    :goto_2
    return-void
.end method


# virtual methods
.method public abstract addToSubset(I)V
.end method

.method public abstract encode(I)[B
.end method

.method public final encode(Ljava/lang/String;)[B
    .locals 4

    .line 1
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v1, v2, :cond_0

    invoke-virtual {p1, v1}, Ljava/lang/String;->codePointAt(I)I

    move-result v2

    invoke-virtual {p0, v2}, Lorg/apache/pdfbox/pdmodel/font/PDFont;->encode(I)[B

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/io/OutputStream;->write([B)V

    invoke-static {v2}, Ljava/lang/Character;->charCount(I)I

    move-result v2

    add-int/2addr v1, v2

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object p1

    return-object p1
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    instance-of v0, p1, Lorg/apache/pdfbox/pdmodel/font/PDFont;

    if-eqz v0, :cond_0

    check-cast p1, Lorg/apache/pdfbox/pdmodel/font/PDFont;

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/font/PDFont;->getCOSObject()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object p1

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDFont;->getCOSObject()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    if-ne p1, v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public getAverageFontWidth()F
    .locals 7

    iget v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDFont;->avgFontWidth:F

    const/4 v1, 0x0

    cmpl-float v2, v0, v1

    if-eqz v2, :cond_0

    goto :goto_2

    :cond_0
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDFont;->dict:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v2, Lorg/apache/pdfbox/cos/COSName;->WIDTHS:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSArray;

    if-eqz v0, :cond_2

    const/4 v2, 0x0

    move v3, v1

    move v4, v3

    :goto_0
    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSArray;->size()I

    move-result v5

    if-ge v2, v5, :cond_3

    invoke-virtual {v0, v2}, Lorg/apache/pdfbox/cos/COSArray;->getObject(I)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v5

    check-cast v5, Lorg/apache/pdfbox/cos/COSNumber;

    invoke-virtual {v5}, Lorg/apache/pdfbox/cos/COSNumber;->floatValue()F

    move-result v6

    cmpl-float v6, v6, v1

    if-lez v6, :cond_1

    invoke-virtual {v5}, Lorg/apache/pdfbox/cos/COSNumber;->floatValue()F

    move-result v5

    add-float/2addr v3, v5

    const/high16 v5, 0x3f800000    # 1.0f

    add-float/2addr v4, v5

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    move v3, v1

    move v4, v3

    :cond_3
    cmpl-float v0, v3, v1

    if-lez v0, :cond_4

    div-float/2addr v3, v4

    move v0, v3

    goto :goto_1

    :cond_4
    move v0, v1

    :goto_1
    iput v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDFont;->avgFontWidth:F

    :goto_2
    return v0
.end method

.method public abstract getBoundingBox()Lorg/apache/fontbox/util/BoundingBox;
.end method

.method public bridge synthetic getCOSObject()Lorg/apache/pdfbox/cos/COSBase;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDFont;->getCOSObject()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    return-object v0
.end method

.method public getCOSObject()Lorg/apache/pdfbox/cos/COSDictionary;
    .locals 1

    .line 2
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDFont;->dict:Lorg/apache/pdfbox/cos/COSDictionary;

    return-object v0
.end method

.method public getDisplacement(I)Lorg/apache/pdfbox/util/Vector;
    .locals 2

    new-instance v0, Lorg/apache/pdfbox/util/Vector;

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/pdmodel/font/PDFont;->getWidth(I)F

    move-result p1

    const/high16 v1, 0x447a0000    # 1000.0f

    div-float/2addr p1, v1

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lorg/apache/pdfbox/util/Vector;-><init>(FF)V

    return-object v0
.end method

.method public getFontDescriptor()Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDFont;->fontDescriptor:Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;

    return-object v0
.end method

.method public getFontMatrix()Lorg/apache/pdfbox/util/Matrix;
    .locals 1

    sget-object v0, Lorg/apache/pdfbox/pdmodel/font/PDFont;->DEFAULT_FONT_MATRIX:Lorg/apache/pdfbox/util/Matrix;

    return-object v0
.end method

.method public abstract getName()Ljava/lang/String;
.end method

.method public getPositionVector(I)Lorg/apache/pdfbox/util/Vector;
    .locals 1

    new-instance p1, Ljava/lang/UnsupportedOperationException;

    const-string v0, "Horizontal fonts have no position vector"

    invoke-direct {p1, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public getSpaceWidth()F
    .locals 3

    iget v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDFont;->fontWidthOfSpace:F

    const/high16 v1, -0x40800000    # -1.0f

    cmpl-float v0, v0, v1

    if-nez v0, :cond_2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDFont;->dict:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->TO_UNICODE:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDFont;->toUnicodeCMap:Lorg/apache/fontbox/cmap/CMap;

    invoke-virtual {v0}, Lorg/apache/fontbox/cmap/CMap;->getSpaceMapping()I

    move-result v0

    const/4 v1, -0x1

    if-le v0, v1, :cond_1

    invoke-virtual {p0, v0}, Lorg/apache/pdfbox/pdmodel/font/PDFont;->getWidth(I)F

    move-result v0

    :goto_0
    iput v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDFont;->fontWidthOfSpace:F

    goto :goto_1

    :cond_0
    const/16 v0, 0x20

    invoke-virtual {p0, v0}, Lorg/apache/pdfbox/pdmodel/font/PDFont;->getWidth(I)F

    move-result v0

    goto :goto_0

    :cond_1
    :goto_1
    iget v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDFont;->fontWidthOfSpace:F

    const/4 v1, 0x0

    cmpg-float v0, v0, v1

    if-gtz v0, :cond_2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDFont;->getAverageFontWidth()F

    move-result v0

    iput v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDFont;->fontWidthOfSpace:F
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    :catch_0
    move-exception v0

    const-string v1, "PdfBoxAndroid"

    const-string v2, "Can\'t determine the width of the space character, assuming 250"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const/high16 v0, 0x437a0000    # 250.0f

    iput v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDFont;->fontWidthOfSpace:F

    :cond_2
    :goto_2
    iget v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDFont;->fontWidthOfSpace:F

    return v0
.end method

.method public final getStandard14AFM()Lorg/apache/fontbox/afm/FontMetrics;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDFont;->afmStandard14:Lorg/apache/fontbox/afm/FontMetrics;

    return-object v0
.end method

.method public getStringWidth(Ljava/lang/String;)F
    .locals 2

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/pdmodel/font/PDFont;->encode(Ljava/lang/String;)[B

    move-result-object p1

    new-instance v0, Ljava/io/ByteArrayInputStream;

    invoke-direct {v0, p1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    const/4 p1, 0x0

    :goto_0
    invoke-virtual {v0}, Ljava/io/ByteArrayInputStream;->available()I

    move-result v1

    if-lez v1, :cond_0

    invoke-virtual {p0, v0}, Lorg/apache/pdfbox/pdmodel/font/PDFont;->readCode(Ljava/io/InputStream;)I

    move-result v1

    invoke-virtual {p0, v1}, Lorg/apache/pdfbox/pdmodel/font/PDFont;->getWidth(I)F

    move-result v1

    add-float/2addr p1, v1

    goto :goto_0

    :cond_0
    return p1
.end method

.method public getSubType()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDFont;->dict:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->SUBTYPE:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getNameAsString(Lorg/apache/pdfbox/cos/COSName;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getType()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDFont;->dict:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->TYPE:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getNameAsString(Lorg/apache/pdfbox/cos/COSName;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getWidth(I)F
    .locals 4

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDFont;->dict:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->WIDTHS:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->containsKey(Lorg/apache/pdfbox/cos/COSName;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDFont;->dict:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->MISSING_WIDTH:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->containsKey(Lorg/apache/pdfbox/cos/COSName;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDFont;->dict:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->FIRST_CHAR:Lorg/apache/pdfbox/cos/COSName;

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->getInt(Lorg/apache/pdfbox/cos/COSName;I)I

    move-result v0

    iget-object v1, p0, Lorg/apache/pdfbox/pdmodel/font/PDFont;->dict:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v3, Lorg/apache/pdfbox/cos/COSName;->LAST_CHAR:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v1, v3, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->getInt(Lorg/apache/pdfbox/cos/COSName;I)I

    move-result v1

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDFont;->getWidths()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_1

    if-lt p1, v0, :cond_1

    if-gt p1, v1, :cond_1

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDFont;->getWidths()Ljava/util/List;

    move-result-object v1

    sub-int/2addr p1, v0

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->floatValue()F

    move-result p1

    return p1

    :cond_1
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDFont;->getFontDescriptor()Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->getMissingWidth()F

    move-result p1

    return p1

    :cond_2
    invoke-interface {p0, p1}, Lorg/apache/pdfbox/pdmodel/font/PDFontLike;->getWidthFromFont(I)F

    move-result p1

    return p1
.end method

.method public final getWidths()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDFont;->widths:Ljava/util/List;

    if-nez v0, :cond_1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDFont;->dict:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->WIDTHS:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSArray;

    if-eqz v0, :cond_0

    invoke-static {v0}, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;->convertIntegerCOSArrayToList(Lorg/apache/pdfbox/cos/COSArray;)Ljava/util/List;

    move-result-object v0

    goto :goto_0

    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDFont;->widths:Ljava/util/List;

    :cond_1
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDFont;->widths:Ljava/util/List;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDFont;->getCOSObject()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method

.method public abstract isDamaged()Z
.end method

.method public abstract isEmbedded()Z
.end method

.method public isStandard14()Z
    .locals 1

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDFont;->isEmbedded()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    return v0

    :cond_0
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDFont;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/pdfbox/pdmodel/font/Standard14Fonts;->containsName(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public abstract isVertical()Z
.end method

.method public final readCMap(Lorg/apache/pdfbox/cos/COSBase;)Lorg/apache/fontbox/cmap/CMap;
    .locals 1

    instance-of v0, p1, Lorg/apache/pdfbox/cos/COSName;

    if-eqz v0, :cond_0

    check-cast p1, Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p1}, Lorg/apache/pdfbox/cos/COSName;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lorg/apache/pdfbox/pdmodel/font/CMapManager;->getPredefinedCMap(Ljava/lang/String;)Lorg/apache/fontbox/cmap/CMap;

    move-result-object p1

    return-object p1

    :cond_0
    instance-of v0, p1, Lorg/apache/pdfbox/cos/COSStream;

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    :try_start_0
    check-cast p1, Lorg/apache/pdfbox/cos/COSStream;

    invoke-virtual {p1}, Lorg/apache/pdfbox/cos/COSStream;->getUnfilteredStream()Ljava/io/InputStream;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/pdfbox/pdmodel/font/CMapManager;->parseCMap(Ljava/io/InputStream;)Lorg/apache/fontbox/cmap/CMap;

    move-result-object p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-static {v0}, Lorg/apache/pdfbox/io/IOUtils;->closeQuietly(Ljava/io/Closeable;)V

    return-object p1

    :catchall_0
    move-exception p1

    invoke-static {v0}, Lorg/apache/pdfbox/io/IOUtils;->closeQuietly(Ljava/io/Closeable;)V

    throw p1

    :cond_1
    new-instance p1, Ljava/io/IOException;

    const-string v0, "Expected Name or Stream"

    invoke-direct {p1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public abstract readCode(Ljava/io/InputStream;)I
.end method

.method public final setFontDescriptor(Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;)V
    .locals 0

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/font/PDFont;->fontDescriptor:Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;

    return-void
.end method

.method public abstract subset()V
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDFont;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public toUnicode(I)Ljava/lang/String;
    .locals 3

    .line 1
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDFont;->toUnicodeCMap:Lorg/apache/fontbox/cmap/CMap;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lorg/apache/fontbox/cmap/CMap;->getName()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDFont;->toUnicodeCMap:Lorg/apache/fontbox/cmap/CMap;

    invoke-virtual {v0}, Lorg/apache/fontbox/cmap/CMap;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Identity-"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/String;

    const/4 v1, 0x1

    new-array v1, v1, [C

    const/4 v2, 0x0

    int-to-char p1, p1

    aput-char p1, v1, v2

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>([C)V

    return-object v0

    :cond_0
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDFont;->toUnicodeCMap:Lorg/apache/fontbox/cmap/CMap;

    invoke-virtual {v0, p1}, Lorg/apache/fontbox/cmap/CMap;->toUnicode(I)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_1
    const/4 p1, 0x0

    return-object p1
.end method

.method public toUnicode(ILorg/apache/pdfbox/pdmodel/font/encoding/GlyphList;)Ljava/lang/String;
    .locals 0

    .line 2
    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/pdmodel/font/PDFont;->toUnicode(I)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public abstract willBeSubset()Z
.end method

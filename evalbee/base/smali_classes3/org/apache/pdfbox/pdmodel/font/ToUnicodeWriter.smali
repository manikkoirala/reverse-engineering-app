.class final Lorg/apache/pdfbox/pdmodel/font/ToUnicodeWriter;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final cidToUnicode:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private wMode:I


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0}, Ljava/util/TreeMap;-><init>()V

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/ToUnicodeWriter;->cidToUnicode:Ljava/util/Map;

    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/pdfbox/pdmodel/font/ToUnicodeWriter;->wMode:I

    return-void
.end method

.method private stringToHex(Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lorg/apache/pdfbox/util/Charsets;->UTF_16BE:Ljava/nio/charset/Charset;

    invoke-virtual {p1, v1}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object p1

    array-length v1, p1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    aget-byte v3, p1, v2

    invoke-static {v3}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v3

    filled-new-array {v3}, [Ljava/lang/Object;

    move-result-object v3

    const-string v4, "%02X"

    invoke-static {v4, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private toHex(I)Ljava/lang/String;
    .locals 1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    filled-new-array {p1}, [Ljava/lang/Object;

    move-result-object p1

    const-string v0, "%04X"

    invoke-static {v0, p1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private writeLine(Ljava/io/BufferedWriter;Ljava/lang/String;)V
    .locals 0

    invoke-virtual {p1, p2}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    const/16 p2, 0xa

    invoke-virtual {p1, p2}, Ljava/io/BufferedWriter;->write(I)V

    return-void
.end method


# virtual methods
.method public add(ILjava/lang/String;)V
    .locals 1

    if-ltz p1, :cond_1

    const v0, 0xffff

    if-gt p1, v0, :cond_1

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/ToUnicodeWriter;->cidToUnicode:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void

    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Text is null or empty"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "CID is not valid"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public setWMode(I)V
    .locals 0

    iput p1, p0, Lorg/apache/pdfbox/pdmodel/font/ToUnicodeWriter;->wMode:I

    return-void
.end method

.method public writeTo(Ljava/io/OutputStream;)V
    .locals 12

    new-instance v0, Ljava/io/BufferedWriter;

    new-instance v1, Ljava/io/OutputStreamWriter;

    sget-object v2, Lorg/apache/pdfbox/util/Charsets;->US_ASCII:Ljava/nio/charset/Charset;

    invoke-direct {v1, p1, v2}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;Ljava/nio/charset/Charset;)V

    invoke-direct {v0, v1}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V

    const-string p1, "/CIDInit /ProcSet findresource begin"

    invoke-direct {p0, v0, p1}, Lorg/apache/pdfbox/pdmodel/font/ToUnicodeWriter;->writeLine(Ljava/io/BufferedWriter;Ljava/lang/String;)V

    const-string p1, "12 dict begin\n"

    invoke-direct {p0, v0, p1}, Lorg/apache/pdfbox/pdmodel/font/ToUnicodeWriter;->writeLine(Ljava/io/BufferedWriter;Ljava/lang/String;)V

    const-string p1, "begincmap"

    invoke-direct {p0, v0, p1}, Lorg/apache/pdfbox/pdmodel/font/ToUnicodeWriter;->writeLine(Ljava/io/BufferedWriter;Ljava/lang/String;)V

    const-string p1, "/CIDSystemInfo"

    invoke-direct {p0, v0, p1}, Lorg/apache/pdfbox/pdmodel/font/ToUnicodeWriter;->writeLine(Ljava/io/BufferedWriter;Ljava/lang/String;)V

    const-string p1, "<< /Registry ()"

    invoke-direct {p0, v0, p1}, Lorg/apache/pdfbox/pdmodel/font/ToUnicodeWriter;->writeLine(Ljava/io/BufferedWriter;Ljava/lang/String;)V

    const-string p1, "/Ordering ()"

    invoke-direct {p0, v0, p1}, Lorg/apache/pdfbox/pdmodel/font/ToUnicodeWriter;->writeLine(Ljava/io/BufferedWriter;Ljava/lang/String;)V

    const-string p1, "/Supplement "

    invoke-direct {p0, v0, p1}, Lorg/apache/pdfbox/pdmodel/font/ToUnicodeWriter;->writeLine(Ljava/io/BufferedWriter;Ljava/lang/String;)V

    const-string p1, ">> def\n"

    invoke-direct {p0, v0, p1}, Lorg/apache/pdfbox/pdmodel/font/ToUnicodeWriter;->writeLine(Ljava/io/BufferedWriter;Ljava/lang/String;)V

    const-string p1, "/CMapName /Adobe-Identity-UCS def"

    invoke-direct {p0, v0, p1}, Lorg/apache/pdfbox/pdmodel/font/ToUnicodeWriter;->writeLine(Ljava/io/BufferedWriter;Ljava/lang/String;)V

    const-string p1, "/CMapType 2 def\n"

    invoke-direct {p0, v0, p1}, Lorg/apache/pdfbox/pdmodel/font/ToUnicodeWriter;->writeLine(Ljava/io/BufferedWriter;Ljava/lang/String;)V

    iget p1, p0, Lorg/apache/pdfbox/pdmodel/font/ToUnicodeWriter;->wMode:I

    if-eqz p1, :cond_0

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "/WMode /"

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lorg/apache/pdfbox/pdmodel/font/ToUnicodeWriter;->wMode:I

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, " def"

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, v0, p1}, Lorg/apache/pdfbox/pdmodel/font/ToUnicodeWriter;->writeLine(Ljava/io/BufferedWriter;Ljava/lang/String;)V

    :cond_0
    const-string p1, "1 begincodespacerange"

    invoke-direct {p0, v0, p1}, Lorg/apache/pdfbox/pdmodel/font/ToUnicodeWriter;->writeLine(Ljava/io/BufferedWriter;Ljava/lang/String;)V

    const-string p1, "<0000> <FFFF>"

    invoke-direct {p0, v0, p1}, Lorg/apache/pdfbox/pdmodel/font/ToUnicodeWriter;->writeLine(Ljava/io/BufferedWriter;Ljava/lang/String;)V

    const-string p1, "endcodespacerange\n"

    invoke-direct {p0, v0, p1}, Lorg/apache/pdfbox/pdmodel/font/ToUnicodeWriter;->writeLine(Ljava/io/BufferedWriter;Ljava/lang/String;)V

    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iget-object v3, p0, Lorg/apache/pdfbox/pdmodel/font/ToUnicodeWriter;->cidToUnicode:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    const/4 v4, -0x1

    const/4 v5, 0x0

    move-object v6, v5

    move v5, v4

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    const/4 v8, 0x0

    if-eqz v7, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/util/Map$Entry;

    invoke-interface {v7}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Integer;

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v9

    invoke-interface {v7}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    const/4 v10, 0x1

    add-int/2addr v4, v10

    if-ne v9, v4, :cond_1

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {v6, v8, v4}, Ljava/lang/String;->codePointCount(II)I

    move-result v4

    if-ne v4, v10, :cond_1

    invoke-virtual {v7, v8}, Ljava/lang/String;->codePointAt(I)I

    move-result v4

    invoke-virtual {v6, v8}, Ljava/lang/String;->codePointAt(I)I

    move-result v11

    add-int/2addr v11, v10

    if-ne v4, v11, :cond_1

    invoke-virtual {v6, v8}, Ljava/lang/String;->codePointAt(I)I

    move-result v4

    add-int/2addr v4, v10

    sub-int v6, v9, v5

    rsub-int v6, v6, 0xff

    if-gt v4, v6, :cond_1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v4

    sub-int/2addr v4, v10

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v1, v4, v6}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_1
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {p1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {v2, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v5, v9

    :goto_1
    move-object v6, v7

    move v4, v9

    goto :goto_0

    :cond_2
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    int-to-double v3, v3

    const-wide/high16 v5, 0x4059000000000000L    # 100.0

    div-double/2addr v3, v5

    invoke-static {v3, v4}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v3

    double-to-int v3, v3

    move v4, v8

    :goto_2
    if-ge v4, v3, :cond_5

    add-int/lit8 v5, v3, -0x1

    const/16 v6, 0x64

    if-ne v4, v5, :cond_3

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v5

    rem-int/lit8 v6, v5, 0x64

    :cond_3
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v7, " beginbfrange\n"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    move v5, v8

    :goto_3
    if-ge v5, v6, :cond_4

    mul-int/lit8 v7, v4, 0x64

    add-int/2addr v7, v5

    const/16 v9, 0x3c

    invoke-virtual {v0, v9}, Ljava/io/BufferedWriter;->write(I)V

    invoke-interface {p1, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Integer;

    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v10

    invoke-direct {p0, v10}, Lorg/apache/pdfbox/pdmodel/font/ToUnicodeWriter;->toHex(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v10}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    const-string v10, "> "

    invoke-virtual {v0, v10}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    invoke-virtual {v0, v9}, Ljava/io/BufferedWriter;->write(I)V

    invoke-interface {v1, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Integer;

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v9

    invoke-direct {p0, v9}, Lorg/apache/pdfbox/pdmodel/font/ToUnicodeWriter;->toHex(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v9}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    invoke-virtual {v0, v10}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    const-string v9, "<"

    invoke-virtual {v0, v9}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    invoke-interface {v2, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-direct {p0, v7}, Lorg/apache/pdfbox/pdmodel/font/ToUnicodeWriter;->stringToHex(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    const-string v7, ">\n"

    invoke-virtual {v0, v7}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    add-int/lit8 v5, v5, 0x1

    goto :goto_3

    :cond_4
    const-string v5, "endbfrange\n"

    invoke-direct {p0, v0, v5}, Lorg/apache/pdfbox/pdmodel/font/ToUnicodeWriter;->writeLine(Ljava/io/BufferedWriter;Ljava/lang/String;)V

    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    :cond_5
    const-string p1, "endcmap"

    invoke-direct {p0, v0, p1}, Lorg/apache/pdfbox/pdmodel/font/ToUnicodeWriter;->writeLine(Ljava/io/BufferedWriter;Ljava/lang/String;)V

    const-string p1, "CMapName currentdict /CMap defineresource pop"

    invoke-direct {p0, v0, p1}, Lorg/apache/pdfbox/pdmodel/font/ToUnicodeWriter;->writeLine(Ljava/io/BufferedWriter;Ljava/lang/String;)V

    const-string p1, "end"

    invoke-direct {p0, v0, p1}, Lorg/apache/pdfbox/pdmodel/font/ToUnicodeWriter;->writeLine(Ljava/io/BufferedWriter;Ljava/lang/String;)V

    invoke-direct {p0, v0, p1}, Lorg/apache/pdfbox/pdmodel/font/ToUnicodeWriter;->writeLine(Ljava/io/BufferedWriter;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/BufferedWriter;->flush()V

    return-void
.end method

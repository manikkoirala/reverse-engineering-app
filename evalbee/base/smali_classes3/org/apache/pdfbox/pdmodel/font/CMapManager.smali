.class final Lorg/apache/pdfbox/pdmodel/font/CMapManager;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field protected static cMapCache:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lorg/apache/fontbox/cmap/CMap;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lorg/apache/pdfbox/pdmodel/font/CMapManager;->cMapCache:Ljava/util/Map;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getPredefinedCMap(Ljava/lang/String;)Lorg/apache/fontbox/cmap/CMap;
    .locals 2

    sget-object v0, Lorg/apache/pdfbox/pdmodel/font/CMapManager;->cMapCache:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/fontbox/cmap/CMap;

    if-eqz v0, :cond_0

    return-object v0

    :cond_0
    new-instance v0, Lorg/apache/fontbox/cmap/CMapParser;

    invoke-direct {v0}, Lorg/apache/fontbox/cmap/CMapParser;-><init>()V

    invoke-virtual {v0, p0}, Lorg/apache/fontbox/cmap/CMapParser;->parsePredefined(Ljava/lang/String;)Lorg/apache/fontbox/cmap/CMap;

    move-result-object p0

    sget-object v0, Lorg/apache/pdfbox/pdmodel/font/CMapManager;->cMapCache:Ljava/util/Map;

    invoke-virtual {p0}, Lorg/apache/fontbox/cmap/CMap;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object p0
.end method

.method public static parseCMap(Ljava/io/InputStream;)Lorg/apache/fontbox/cmap/CMap;
    .locals 1

    if-eqz p0, :cond_0

    new-instance v0, Lorg/apache/fontbox/cmap/CMapParser;

    invoke-direct {v0}, Lorg/apache/fontbox/cmap/CMapParser;-><init>()V

    invoke-virtual {v0, p0}, Lorg/apache/fontbox/cmap/CMapParser;->parse(Ljava/io/InputStream;)Lorg/apache/fontbox/cmap/CMap;

    move-result-object p0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return-object p0
.end method

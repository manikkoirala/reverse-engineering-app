.class public Lorg/apache/pdfbox/pdmodel/font/encoding/WinAnsiEncoding;
.super Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;
.source "SourceFile"


# static fields
.field public static final INSTANCE:Lorg/apache/pdfbox/pdmodel/font/encoding/WinAnsiEncoding;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lorg/apache/pdfbox/pdmodel/font/encoding/WinAnsiEncoding;

    invoke-direct {v0}, Lorg/apache/pdfbox/pdmodel/font/encoding/WinAnsiEncoding;-><init>()V

    sput-object v0, Lorg/apache/pdfbox/pdmodel/font/encoding/WinAnsiEncoding;->INSTANCE:Lorg/apache/pdfbox/pdmodel/font/encoding/WinAnsiEncoding;

    return-void
.end method

.method public constructor <init>()V
    .locals 7

    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;-><init>()V

    const/16 v0, 0x41

    const-string v1, "A"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xc6

    const-string v1, "AE"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xc1

    const-string v1, "Aacute"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xc2

    const-string v1, "Acircumflex"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xc4

    const-string v1, "Adieresis"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xc0

    const-string v1, "Agrave"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xc5

    const-string v1, "Aring"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xc3

    const-string v1, "Atilde"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x42

    const-string v1, "B"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x43

    const-string v1, "C"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xc7

    const-string v1, "Ccedilla"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x44

    const-string v1, "D"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x45

    const-string v1, "E"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xc9

    const-string v1, "Eacute"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xca

    const-string v1, "Ecircumflex"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xcb

    const-string v1, "Edieresis"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xc8

    const-string v1, "Egrave"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xd0

    const-string v1, "Eth"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x80

    const-string v1, "Euro"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x46

    const-string v1, "F"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x47

    const-string v1, "G"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x48

    const-string v1, "H"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x49

    const-string v1, "I"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xcd

    const-string v1, "Iacute"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xce

    const-string v1, "Icircumflex"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xcf

    const-string v1, "Idieresis"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xcc

    const-string v1, "Igrave"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x4a

    const-string v1, "J"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x4b

    const-string v1, "K"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x4c

    const-string v1, "L"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x4d

    const-string v1, "M"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x4e

    const-string v1, "N"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xd1

    const-string v1, "Ntilde"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x4f

    const-string v1, "O"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x8c

    const-string v1, "OE"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xd3

    const-string v1, "Oacute"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xd4

    const-string v1, "Ocircumflex"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xd6

    const-string v1, "Odieresis"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xd2

    const-string v1, "Ograve"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xd8

    const-string v1, "Oslash"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xd5

    const-string v1, "Otilde"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x50

    const-string v1, "P"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x51

    const-string v1, "Q"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x52

    const-string v1, "R"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x53

    const-string v1, "S"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x8a

    const-string v1, "Scaron"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x54

    const-string v1, "T"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xde

    const-string v1, "Thorn"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x55

    const-string v1, "U"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xda

    const-string v1, "Uacute"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xdb

    const-string v1, "Ucircumflex"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xdc

    const-string v1, "Udieresis"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xd9

    const-string v1, "Ugrave"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x56

    const-string v1, "V"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x57

    const-string v1, "W"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x58

    const-string v1, "X"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x59

    const-string v1, "Y"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xdd

    const-string v1, "Yacute"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x9f

    const-string v1, "Ydieresis"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x5a

    const-string v1, "Z"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x8e

    const-string v1, "Zcaron"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x61

    const-string v1, "a"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xe1

    const-string v1, "aacute"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xe2

    const-string v1, "acircumflex"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xb4

    const-string v1, "acute"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xe4

    const-string v1, "adieresis"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xe6

    const-string v1, "ae"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xe0

    const-string v1, "agrave"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x26

    const-string v1, "ampersand"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xe5

    const-string v1, "aring"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x5e

    const-string v1, "asciicircum"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x7e

    const-string v1, "asciitilde"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x2a

    const-string v1, "asterisk"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x40

    const-string v1, "at"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xe3

    const-string v1, "atilde"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x62

    const-string v1, "b"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x5c

    const-string v1, "backslash"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x7c

    const-string v1, "bar"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x7b

    const-string v1, "braceleft"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x7d

    const-string v1, "braceright"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x5b

    const-string v1, "bracketleft"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x5d

    const-string v1, "bracketright"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xa6

    const-string v1, "brokenbar"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x95

    const-string v1, "bullet"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x63

    const-string v2, "c"

    invoke-virtual {p0, v0, v2}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xe7

    const-string v2, "ccedilla"

    invoke-virtual {p0, v0, v2}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xb8

    const-string v2, "cedilla"

    invoke-virtual {p0, v0, v2}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xa2

    const-string v2, "cent"

    invoke-virtual {p0, v0, v2}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x88

    const-string v2, "circumflex"

    invoke-virtual {p0, v0, v2}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x3a

    const-string v2, "colon"

    invoke-virtual {p0, v0, v2}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x2c

    const-string v2, "comma"

    invoke-virtual {p0, v0, v2}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xa9

    const-string v2, "copyright"

    invoke-virtual {p0, v0, v2}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xa4

    const-string v2, "currency"

    invoke-virtual {p0, v0, v2}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x64

    const-string v2, "d"

    invoke-virtual {p0, v0, v2}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x86

    const-string v2, "dagger"

    invoke-virtual {p0, v0, v2}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x87

    const-string v2, "daggerdbl"

    invoke-virtual {p0, v0, v2}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xb0

    const-string v2, "degree"

    invoke-virtual {p0, v0, v2}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xa8

    const-string v2, "dieresis"

    invoke-virtual {p0, v0, v2}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xf7

    const-string v2, "divide"

    invoke-virtual {p0, v0, v2}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x24

    const-string v2, "dollar"

    invoke-virtual {p0, v0, v2}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x65

    const-string v2, "e"

    invoke-virtual {p0, v0, v2}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xe9

    const-string v2, "eacute"

    invoke-virtual {p0, v0, v2}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xea

    const-string v2, "ecircumflex"

    invoke-virtual {p0, v0, v2}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xeb

    const-string v2, "edieresis"

    invoke-virtual {p0, v0, v2}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xe8

    const-string v2, "egrave"

    invoke-virtual {p0, v0, v2}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x38

    const-string v2, "eight"

    invoke-virtual {p0, v0, v2}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x85

    const-string v2, "ellipsis"

    invoke-virtual {p0, v0, v2}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x97

    const-string v2, "emdash"

    invoke-virtual {p0, v0, v2}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x96

    const-string v2, "endash"

    invoke-virtual {p0, v0, v2}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x3d

    const-string v2, "equal"

    invoke-virtual {p0, v0, v2}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xf0

    const-string v2, "eth"

    invoke-virtual {p0, v0, v2}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const-string v0, "exclam"

    const/16 v2, 0x21

    invoke-virtual {p0, v2, v0}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xa1

    const-string v3, "exclamdown"

    invoke-virtual {p0, v0, v3}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x66

    const-string v3, "f"

    invoke-virtual {p0, v0, v3}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x35

    const-string v3, "five"

    invoke-virtual {p0, v0, v3}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x83

    const-string v3, "florin"

    invoke-virtual {p0, v0, v3}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x34

    const-string v3, "four"

    invoke-virtual {p0, v0, v3}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x67

    const-string v3, "g"

    invoke-virtual {p0, v0, v3}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xdf

    const-string v3, "germandbls"

    invoke-virtual {p0, v0, v3}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x60

    const-string v3, "grave"

    invoke-virtual {p0, v0, v3}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x3e

    const-string v3, "greater"

    invoke-virtual {p0, v0, v3}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xab

    const-string v3, "guillemotleft"

    invoke-virtual {p0, v0, v3}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xbb

    const-string v3, "guillemotright"

    invoke-virtual {p0, v0, v3}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x8b

    const-string v3, "guilsinglleft"

    invoke-virtual {p0, v0, v3}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x9b

    const-string v3, "guilsinglright"

    invoke-virtual {p0, v0, v3}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x68

    const-string v3, "h"

    invoke-virtual {p0, v0, v3}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x2d

    const-string v3, "hyphen"

    invoke-virtual {p0, v0, v3}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x69

    const-string v4, "i"

    invoke-virtual {p0, v0, v4}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xed

    const-string v4, "iacute"

    invoke-virtual {p0, v0, v4}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xee

    const-string v4, "icircumflex"

    invoke-virtual {p0, v0, v4}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xef

    const-string v4, "idieresis"

    invoke-virtual {p0, v0, v4}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xec

    const-string v4, "igrave"

    invoke-virtual {p0, v0, v4}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x6a

    const-string v4, "j"

    invoke-virtual {p0, v0, v4}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x6b

    const-string v4, "k"

    invoke-virtual {p0, v0, v4}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x6c

    const-string v4, "l"

    invoke-virtual {p0, v0, v4}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x3c

    const-string v4, "less"

    invoke-virtual {p0, v0, v4}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xac

    const-string v4, "logicalnot"

    invoke-virtual {p0, v0, v4}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x6d

    const-string v4, "m"

    invoke-virtual {p0, v0, v4}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xaf

    const-string v4, "macron"

    invoke-virtual {p0, v0, v4}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xb5

    const-string v4, "mu"

    invoke-virtual {p0, v0, v4}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xd7

    const-string v4, "multiply"

    invoke-virtual {p0, v0, v4}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x6e

    const-string v4, "n"

    invoke-virtual {p0, v0, v4}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x39

    const-string v4, "nine"

    invoke-virtual {p0, v0, v4}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xf1

    const-string v4, "ntilde"

    invoke-virtual {p0, v0, v4}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x23

    const-string v4, "numbersign"

    invoke-virtual {p0, v0, v4}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x6f

    const-string v4, "o"

    invoke-virtual {p0, v0, v4}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xf3

    const-string v4, "oacute"

    invoke-virtual {p0, v0, v4}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xf4

    const-string v4, "ocircumflex"

    invoke-virtual {p0, v0, v4}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xf6

    const-string v4, "odieresis"

    invoke-virtual {p0, v0, v4}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x9c

    const-string v4, "oe"

    invoke-virtual {p0, v0, v4}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xf2

    const-string v4, "ograve"

    invoke-virtual {p0, v0, v4}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x31

    const-string v4, "one"

    invoke-virtual {p0, v0, v4}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xbd

    const-string v4, "onehalf"

    invoke-virtual {p0, v0, v4}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xbc

    const-string v4, "onequarter"

    invoke-virtual {p0, v0, v4}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xb9

    const-string v4, "onesuperior"

    invoke-virtual {p0, v0, v4}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xaa

    const-string v4, "ordfeminine"

    invoke-virtual {p0, v0, v4}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xba

    const-string v4, "ordmasculine"

    invoke-virtual {p0, v0, v4}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xf8

    const-string v4, "oslash"

    invoke-virtual {p0, v0, v4}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xf5

    const-string v4, "otilde"

    invoke-virtual {p0, v0, v4}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x70

    const-string v4, "p"

    invoke-virtual {p0, v0, v4}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xb6

    const-string v4, "paragraph"

    invoke-virtual {p0, v0, v4}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x28

    const-string v4, "parenleft"

    invoke-virtual {p0, v0, v4}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x29

    const-string v4, "parenright"

    invoke-virtual {p0, v0, v4}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x25

    const-string v4, "percent"

    invoke-virtual {p0, v0, v4}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x2e

    const-string v4, "period"

    invoke-virtual {p0, v0, v4}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xb7

    const-string v4, "periodcentered"

    invoke-virtual {p0, v0, v4}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x89

    const-string v4, "perthousand"

    invoke-virtual {p0, v0, v4}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x2b

    const-string v4, "plus"

    invoke-virtual {p0, v0, v4}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xb1

    const-string v4, "plusminus"

    invoke-virtual {p0, v0, v4}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x71

    const-string v4, "q"

    invoke-virtual {p0, v0, v4}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x3f

    const-string v4, "question"

    invoke-virtual {p0, v0, v4}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xbf

    const-string v4, "questiondown"

    invoke-virtual {p0, v0, v4}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x22

    const-string v4, "quotedbl"

    invoke-virtual {p0, v0, v4}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x84

    const-string v4, "quotedblbase"

    invoke-virtual {p0, v0, v4}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x93

    const-string v4, "quotedblleft"

    invoke-virtual {p0, v0, v4}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x94

    const-string v4, "quotedblright"

    invoke-virtual {p0, v0, v4}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x91

    const-string v4, "quoteleft"

    invoke-virtual {p0, v0, v4}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x92

    const-string v4, "quoteright"

    invoke-virtual {p0, v0, v4}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x82

    const-string v4, "quotesinglbase"

    invoke-virtual {p0, v0, v4}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x27

    const-string v4, "quotesingle"

    invoke-virtual {p0, v0, v4}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x72

    const-string v4, "r"

    invoke-virtual {p0, v0, v4}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xae

    const-string v4, "registered"

    invoke-virtual {p0, v0, v4}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x73

    const-string v4, "s"

    invoke-virtual {p0, v0, v4}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x9a

    const-string v4, "scaron"

    invoke-virtual {p0, v0, v4}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xa7

    const-string v4, "section"

    invoke-virtual {p0, v0, v4}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x3b

    const-string v4, "semicolon"

    invoke-virtual {p0, v0, v4}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x37

    const-string v4, "seven"

    invoke-virtual {p0, v0, v4}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x36

    const-string v4, "six"

    invoke-virtual {p0, v0, v4}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x2f

    const-string v4, "slash"

    invoke-virtual {p0, v0, v4}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x20

    const-string v4, "space"

    invoke-virtual {p0, v0, v4}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xa3

    const-string v5, "sterling"

    invoke-virtual {p0, v0, v5}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x74

    const-string v5, "t"

    invoke-virtual {p0, v0, v5}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xfe

    const-string v5, "thorn"

    invoke-virtual {p0, v0, v5}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x33

    const-string v5, "three"

    invoke-virtual {p0, v0, v5}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xbe

    const-string v5, "threequarters"

    invoke-virtual {p0, v0, v5}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xb3

    const-string v5, "threesuperior"

    invoke-virtual {p0, v0, v5}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x98

    const-string v5, "tilde"

    invoke-virtual {p0, v0, v5}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x99

    const-string v5, "trademark"

    invoke-virtual {p0, v0, v5}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x32

    const-string v5, "two"

    invoke-virtual {p0, v0, v5}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xb2

    const-string v5, "twosuperior"

    invoke-virtual {p0, v0, v5}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x75

    const-string v5, "u"

    invoke-virtual {p0, v0, v5}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xfa

    const-string v5, "uacute"

    invoke-virtual {p0, v0, v5}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xfb

    const-string v5, "ucircumflex"

    invoke-virtual {p0, v0, v5}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xfc

    const-string v5, "udieresis"

    invoke-virtual {p0, v0, v5}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xf9

    const-string v5, "ugrave"

    invoke-virtual {p0, v0, v5}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x5f

    const-string v5, "underscore"

    invoke-virtual {p0, v0, v5}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x76

    const-string v5, "v"

    invoke-virtual {p0, v0, v5}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x77

    const-string v5, "w"

    invoke-virtual {p0, v0, v5}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x78

    const-string v5, "x"

    invoke-virtual {p0, v0, v5}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x79

    const-string v5, "y"

    invoke-virtual {p0, v0, v5}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xfd

    const-string v5, "yacute"

    invoke-virtual {p0, v0, v5}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const-string v0, "ydieresis"

    const/16 v5, 0xff

    invoke-virtual {p0, v5, v0}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xa5

    const-string v6, "yen"

    invoke-virtual {p0, v0, v6}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x7a

    const-string v6, "z"

    invoke-virtual {p0, v0, v6}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x9e

    const-string v6, "zcaron"

    invoke-virtual {p0, v0, v6}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0x30

    const-string v6, "zero"

    invoke-virtual {p0, v0, v6}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xa0

    invoke-virtual {p0, v0, v4}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    const/16 v0, 0xad

    invoke-virtual {p0, v0, v3}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    :goto_0
    if-gt v2, v5, :cond_1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->codeToName:Ljava/util/Map;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0, v2, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->add(ILjava/lang/String;)V

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method


# virtual methods
.method public getCOSObject()Lorg/apache/pdfbox/cos/COSBase;
    .locals 1

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->WIN_ANSI_ENCODING:Lorg/apache/pdfbox/cos/COSName;

    return-object v0
.end method

.class public Lorg/apache/pdfbox/pdmodel/font/PDTrueTypeFont;
.super Lorg/apache/pdfbox/pdmodel/font/PDSimpleFont;
.source "SourceFile"


# static fields
.field private static final INVERTED_MACOS_ROMAN:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final START_RANGE_F000:I = 0xf000

.field private static final START_RANGE_F100:I = 0xf100

.field private static final START_RANGE_F200:I = 0xf200


# instance fields
.field private cmapInitialized:Z

.field private cmapMacRoman:Lorg/apache/fontbox/ttf/CmapSubtable;

.field private cmapWinSymbol:Lorg/apache/fontbox/ttf/CmapSubtable;

.field private cmapWinUnicode:Lorg/apache/fontbox/ttf/CmapSubtable;

.field private final isDamaged:Z

.field private final isEmbedded:Z

.field private final ttf:Lorg/apache/fontbox/ttf/TrueTypeFont;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lorg/apache/pdfbox/pdmodel/font/PDTrueTypeFont;->INVERTED_MACOS_ROMAN:Ljava/util/Map;

    sget-object v0, Lorg/apache/pdfbox/pdmodel/font/encoding/MacOSRomanEncoding;->INSTANCE:Lorg/apache/pdfbox/pdmodel/font/encoding/MacOSRomanEncoding;

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->getCodeToNameMap()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    sget-object v2, Lorg/apache/pdfbox/pdmodel/font/PDTrueTypeFont;->INVERTED_MACOS_ROMAN:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v2, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_1
    return-void
.end method

.method public constructor <init>(Lorg/apache/pdfbox/cos/COSDictionary;)V
    .locals 6

    .line 1
    const-string v0, "Could not read embedded TTF for font "

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/font/PDSimpleFont;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    const/4 p1, 0x0

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/font/PDTrueTypeFont;->cmapWinUnicode:Lorg/apache/fontbox/ttf/CmapSubtable;

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/font/PDTrueTypeFont;->cmapWinSymbol:Lorg/apache/fontbox/ttf/CmapSubtable;

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/font/PDTrueTypeFont;->cmapMacRoman:Lorg/apache/fontbox/ttf/CmapSubtable;

    const/4 v1, 0x0

    iput-boolean v1, p0, Lorg/apache/pdfbox/pdmodel/font/PDTrueTypeFont;->cmapInitialized:Z

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDFont;->getFontDescriptor()Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;

    move-result-object v2

    const-string v3, "PdfBoxAndroid"

    const/4 v4, 0x1

    if-eqz v2, :cond_0

    invoke-super {p0}, Lorg/apache/pdfbox/pdmodel/font/PDFont;->getFontDescriptor()Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->getFontFile2()Lorg/apache/pdfbox/pdmodel/common/PDStream;

    move-result-object v2

    if-eqz v2, :cond_0

    :try_start_0
    new-instance v5, Lorg/apache/fontbox/ttf/TTFParser;

    invoke-direct {v5, v4}, Lorg/apache/fontbox/ttf/TTFParser;-><init>(Z)V

    invoke-virtual {v2}, Lorg/apache/pdfbox/pdmodel/common/PDStream;->createInputStream()Ljava/io/InputStream;

    move-result-object v2

    invoke-virtual {v5, v2}, Lorg/apache/fontbox/ttf/TTFParser;->parse(Ljava/io/InputStream;)Lorg/apache/fontbox/ttf/TrueTypeFont;

    move-result-object p1
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v2

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    goto :goto_0

    :catch_1
    move-exception v2

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    :goto_0
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDTrueTypeFont;->getBaseFont()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move v0, v4

    goto :goto_2

    :cond_0
    :goto_1
    move v0, v1

    :goto_2
    if-eqz p1, :cond_1

    move v1, v4

    :cond_1
    iput-boolean v1, p0, Lorg/apache/pdfbox/pdmodel/font/PDTrueTypeFont;->isEmbedded:Z

    iput-boolean v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDTrueTypeFont;->isDamaged:Z

    if-nez p1, :cond_2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDTrueTypeFont;->getBaseFont()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lorg/apache/pdfbox/pdmodel/font/ExternalFonts;->getTrueTypeFont(Ljava/lang/String;)Lorg/apache/fontbox/ttf/TrueTypeFont;

    move-result-object p1

    if-nez p1, :cond_2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDFont;->getFontDescriptor()Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;

    move-result-object p1

    invoke-static {p1}, Lorg/apache/pdfbox/pdmodel/font/ExternalFonts;->getTrueTypeFallbackFont(Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;)Lorg/apache/fontbox/ttf/TrueTypeFont;

    move-result-object p1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Using fallback font \'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, "\' for \'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDTrueTypeFont;->getBaseFont()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/font/PDTrueTypeFont;->ttf:Lorg/apache/fontbox/ttf/TrueTypeFont;

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDSimpleFont;->readEncoding()V

    return-void
.end method

.method private constructor <init>(Lorg/apache/pdfbox/pdmodel/PDDocument;Ljava/io/InputStream;)V
    .locals 3

    .line 2
    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/font/PDSimpleFont;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDTrueTypeFont;->cmapWinUnicode:Lorg/apache/fontbox/ttf/CmapSubtable;

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDTrueTypeFont;->cmapWinSymbol:Lorg/apache/fontbox/ttf/CmapSubtable;

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDTrueTypeFont;->cmapMacRoman:Lorg/apache/fontbox/ttf/CmapSubtable;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDTrueTypeFont;->cmapInitialized:Z

    new-instance v1, Lorg/apache/pdfbox/pdmodel/font/PDTrueTypeFontEmbedder;

    iget-object v2, p0, Lorg/apache/pdfbox/pdmodel/font/PDFont;->dict:Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-direct {v1, p1, v2, p2}, Lorg/apache/pdfbox/pdmodel/font/PDTrueTypeFontEmbedder;-><init>(Lorg/apache/pdfbox/pdmodel/PDDocument;Lorg/apache/pdfbox/cos/COSDictionary;Ljava/io/InputStream;)V

    invoke-virtual {v1}, Lorg/apache/pdfbox/pdmodel/font/PDTrueTypeFontEmbedder;->getFontEncoding()Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;

    move-result-object p1

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/font/PDSimpleFont;->encoding:Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;

    invoke-virtual {v1}, Lorg/apache/pdfbox/pdmodel/font/TrueTypeEmbedder;->getTrueTypeFont()Lorg/apache/fontbox/ttf/TrueTypeFont;

    move-result-object p1

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/font/PDTrueTypeFont;->ttf:Lorg/apache/fontbox/ttf/TrueTypeFont;

    invoke-virtual {v1}, Lorg/apache/pdfbox/pdmodel/font/TrueTypeEmbedder;->getFontDescriptor()Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;

    move-result-object p1

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/pdmodel/font/PDFont;->setFontDescriptor(Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;)V

    const/4 p1, 0x1

    iput-boolean p1, p0, Lorg/apache/pdfbox/pdmodel/font/PDTrueTypeFont;->isEmbedded:Z

    iput-boolean v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDTrueTypeFont;->isDamaged:Z

    invoke-static {}, Lorg/apache/pdfbox/pdmodel/font/encoding/GlyphList;->getAdobeGlyphList()Lorg/apache/pdfbox/pdmodel/font/encoding/GlyphList;

    move-result-object p1

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/font/PDSimpleFont;->glyphList:Lorg/apache/pdfbox/pdmodel/font/encoding/GlyphList;

    return-void
.end method

.method private extractCmapTable()V
    .locals 7

    iget-boolean v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDTrueTypeFont;->cmapInitialized:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDTrueTypeFont;->ttf:Lorg/apache/fontbox/ttf/TrueTypeFont;

    invoke-virtual {v0}, Lorg/apache/fontbox/ttf/TrueTypeFont;->getCmap()Lorg/apache/fontbox/ttf/CmapTable;

    move-result-object v0

    const/4 v1, 0x1

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lorg/apache/fontbox/ttf/CmapTable;->getCmaps()[Lorg/apache/fontbox/ttf/CmapSubtable;

    move-result-object v0

    array-length v2, v0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_4

    aget-object v4, v0, v3

    invoke-virtual {v4}, Lorg/apache/fontbox/ttf/CmapSubtable;->getPlatformId()I

    move-result v5

    const/4 v6, 0x3

    if-ne v6, v5, :cond_2

    invoke-virtual {v4}, Lorg/apache/fontbox/ttf/CmapSubtable;->getPlatformEncodingId()I

    move-result v5

    if-ne v1, v5, :cond_1

    iput-object v4, p0, Lorg/apache/pdfbox/pdmodel/font/PDTrueTypeFont;->cmapWinUnicode:Lorg/apache/fontbox/ttf/CmapSubtable;

    goto :goto_1

    :cond_1
    invoke-virtual {v4}, Lorg/apache/fontbox/ttf/CmapSubtable;->getPlatformEncodingId()I

    move-result v5

    if-nez v5, :cond_3

    iput-object v4, p0, Lorg/apache/pdfbox/pdmodel/font/PDTrueTypeFont;->cmapWinSymbol:Lorg/apache/fontbox/ttf/CmapSubtable;

    goto :goto_1

    :cond_2
    invoke-virtual {v4}, Lorg/apache/fontbox/ttf/CmapSubtable;->getPlatformId()I

    move-result v5

    if-ne v1, v5, :cond_3

    invoke-virtual {v4}, Lorg/apache/fontbox/ttf/CmapSubtable;->getPlatformEncodingId()I

    move-result v5

    if-nez v5, :cond_3

    iput-object v4, p0, Lorg/apache/pdfbox/pdmodel/font/PDTrueTypeFont;->cmapMacRoman:Lorg/apache/fontbox/ttf/CmapSubtable;

    :cond_3
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_4
    iput-boolean v1, p0, Lorg/apache/pdfbox/pdmodel/font/PDTrueTypeFont;->cmapInitialized:Z

    return-void
.end method

.method public static loadTTF(Lorg/apache/pdfbox/pdmodel/PDDocument;Ljava/io/File;)Lorg/apache/pdfbox/pdmodel/font/PDTrueTypeFont;
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1
    new-instance v0, Lorg/apache/pdfbox/pdmodel/font/PDTrueTypeFont;

    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v0, p0, v1}, Lorg/apache/pdfbox/pdmodel/font/PDTrueTypeFont;-><init>(Lorg/apache/pdfbox/pdmodel/PDDocument;Ljava/io/InputStream;)V

    return-object v0
.end method

.method public static loadTTF(Lorg/apache/pdfbox/pdmodel/PDDocument;Ljava/io/InputStream;)Lorg/apache/pdfbox/pdmodel/font/PDTrueTypeFont;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 2
    new-instance v0, Lorg/apache/pdfbox/pdmodel/font/PDTrueTypeFont;

    invoke-direct {v0, p0, p1}, Lorg/apache/pdfbox/pdmodel/font/PDTrueTypeFont;-><init>(Lorg/apache/pdfbox/pdmodel/PDDocument;Ljava/io/InputStream;)V

    return-object v0
.end method


# virtual methods
.method public codeToGID(I)I
    .locals 3

    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/font/PDTrueTypeFont;->extractCmapTable()V

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDSimpleFont;->isSymbolic()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_3

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDSimpleFont;->encoding:Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;

    invoke-virtual {v0, p1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->getName(I)Ljava/lang/String;

    move-result-object v0

    const-string v2, ".notdef"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    return v1

    :cond_0
    iget-object v2, p0, Lorg/apache/pdfbox/pdmodel/font/PDTrueTypeFont;->cmapWinUnicode:Lorg/apache/fontbox/ttf/CmapSubtable;

    if-eqz v2, :cond_1

    invoke-static {}, Lorg/apache/pdfbox/pdmodel/font/encoding/GlyphList;->getAdobeGlyphList()Lorg/apache/pdfbox/pdmodel/font/encoding/GlyphList;

    move-result-object v2

    invoke-virtual {v2, v0}, Lorg/apache/pdfbox/pdmodel/font/encoding/GlyphList;->toUnicode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v2, v1}, Ljava/lang/String;->codePointAt(I)I

    move-result v1

    iget-object v2, p0, Lorg/apache/pdfbox/pdmodel/font/PDTrueTypeFont;->cmapWinUnicode:Lorg/apache/fontbox/ttf/CmapSubtable;

    invoke-virtual {v2, v1}, Lorg/apache/fontbox/ttf/CmapSubtable;->getGlyphId(I)I

    move-result v1

    :cond_1
    if-nez v1, :cond_2

    iget-object v2, p0, Lorg/apache/pdfbox/pdmodel/font/PDTrueTypeFont;->cmapMacRoman:Lorg/apache/fontbox/ttf/CmapSubtable;

    if-eqz v2, :cond_2

    sget-object v2, Lorg/apache/pdfbox/pdmodel/font/PDTrueTypeFont;->INVERTED_MACOS_ROMAN:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    if-eqz v2, :cond_2

    iget-object v1, p0, Lorg/apache/pdfbox/pdmodel/font/PDTrueTypeFont;->cmapMacRoman:Lorg/apache/fontbox/ttf/CmapSubtable;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v1, v2}, Lorg/apache/fontbox/ttf/CmapSubtable;->getGlyphId(I)I

    move-result v1

    :cond_2
    if-nez v1, :cond_7

    iget-object v1, p0, Lorg/apache/pdfbox/pdmodel/font/PDTrueTypeFont;->ttf:Lorg/apache/fontbox/ttf/TrueTypeFont;

    invoke-virtual {v1, v0}, Lorg/apache/fontbox/ttf/TrueTypeFont;->nameToGID(Ljava/lang/String;)I

    move-result v1

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDTrueTypeFont;->cmapWinSymbol:Lorg/apache/fontbox/ttf/CmapSubtable;

    if-eqz v0, :cond_6

    invoke-virtual {v0, p1}, Lorg/apache/fontbox/ttf/CmapSubtable;->getGlyphId(I)I

    move-result v1

    if-ltz p1, :cond_6

    const/16 v0, 0xff

    if-gt p1, v0, :cond_6

    if-nez v1, :cond_4

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDTrueTypeFont;->cmapWinSymbol:Lorg/apache/fontbox/ttf/CmapSubtable;

    const v1, 0xf000

    add-int/2addr v1, p1

    invoke-virtual {v0, v1}, Lorg/apache/fontbox/ttf/CmapSubtable;->getGlyphId(I)I

    move-result v1

    :cond_4
    if-nez v1, :cond_5

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDTrueTypeFont;->cmapWinSymbol:Lorg/apache/fontbox/ttf/CmapSubtable;

    const v1, 0xf100

    add-int/2addr v1, p1

    invoke-virtual {v0, v1}, Lorg/apache/fontbox/ttf/CmapSubtable;->getGlyphId(I)I

    move-result v0

    move v1, v0

    :cond_5
    if-nez v1, :cond_6

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDTrueTypeFont;->cmapWinSymbol:Lorg/apache/fontbox/ttf/CmapSubtable;

    const v1, 0xf200

    add-int/2addr v1, p1

    invoke-virtual {v0, v1}, Lorg/apache/fontbox/ttf/CmapSubtable;->getGlyphId(I)I

    move-result v1

    :cond_6
    if-nez v1, :cond_7

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDTrueTypeFont;->cmapMacRoman:Lorg/apache/fontbox/ttf/CmapSubtable;

    if-eqz v0, :cond_7

    invoke-virtual {v0, p1}, Lorg/apache/fontbox/ttf/CmapSubtable;->getGlyphId(I)I

    move-result v1

    :cond_7
    :goto_0
    if-nez v1, :cond_8

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Can\'t map code "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, " in font "

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDTrueTypeFont;->getBaseFont()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v0, "PdfBoxAndroid"

    invoke-static {v0, p1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_8
    return v1
.end method

.method public encode(I)[B
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDSimpleFont;->getEncoding()Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;

    move-result-object v0

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDSimpleFont;->getGlyphList()Lorg/apache/pdfbox/pdmodel/font/encoding/GlyphList;

    move-result-object v1

    invoke-virtual {v1, p1}, Lorg/apache/pdfbox/pdmodel/font/encoding/GlyphList;->codePointToName(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->contains(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/pdmodel/font/PDTrueTypeFont;->codeToGID(I)I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    new-array v0, v0, [B

    const/4 v1, 0x0

    int-to-byte p1, p1

    aput-byte p1, v0, v1

    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    filled-new-array {p1}, [Ljava/lang/Object;

    move-result-object p1

    const-string v1, "U+%04X is not available in this font\'s Encoding"

    invoke-static {v1, p1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "This font type only supports 8-bit code points"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public getBaseFont()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDFont;->dict:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->BASE_FONT:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getNameAsString(Lorg/apache/pdfbox/cos/COSName;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getBoundingBox()Lorg/apache/fontbox/util/BoundingBox;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDTrueTypeFont;->ttf:Lorg/apache/fontbox/ttf/TrueTypeFont;

    invoke-virtual {v0}, Lorg/apache/fontbox/ttf/TrueTypeFont;->getFontBBox()Lorg/apache/fontbox/util/BoundingBox;

    move-result-object v0

    return-object v0
.end method

.method public getHeight(I)F
    .locals 1

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/pdmodel/font/PDTrueTypeFont;->codeToGID(I)I

    move-result p1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDTrueTypeFont;->ttf:Lorg/apache/fontbox/ttf/TrueTypeFont;

    invoke-virtual {v0}, Lorg/apache/fontbox/ttf/TrueTypeFont;->getGlyph()Lorg/apache/fontbox/ttf/GlyphTable;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/fontbox/ttf/GlyphTable;->getGlyph(I)Lorg/apache/fontbox/ttf/GlyphData;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lorg/apache/fontbox/ttf/GlyphData;->getBoundingBox()Lorg/apache/fontbox/util/BoundingBox;

    move-result-object p1

    invoke-virtual {p1}, Lorg/apache/fontbox/util/BoundingBox;->getHeight()F

    move-result p1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDTrueTypeFont;->getBaseFont()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTrueTypeFont()Lorg/apache/fontbox/ttf/TrueTypeFont;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDTrueTypeFont;->ttf:Lorg/apache/fontbox/ttf/TrueTypeFont;

    return-object v0
.end method

.method public getWidthFromFont(I)F
    .locals 3

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDFont;->getStandard14AFM()Lorg/apache/fontbox/afm/FontMetrics;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDSimpleFont;->getEncoding()Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/pdmodel/font/PDSimpleFont;->getStandard14Width(I)F

    move-result p1

    return p1

    :cond_0
    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/pdmodel/font/PDTrueTypeFont;->codeToGID(I)I

    move-result p1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDTrueTypeFont;->ttf:Lorg/apache/fontbox/ttf/TrueTypeFont;

    invoke-virtual {v0, p1}, Lorg/apache/fontbox/ttf/TrueTypeFont;->getAdvanceWidth(I)I

    move-result p1

    int-to-float p1, p1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDTrueTypeFont;->ttf:Lorg/apache/fontbox/ttf/TrueTypeFont;

    invoke-virtual {v0}, Lorg/apache/fontbox/ttf/TrueTypeFont;->getUnitsPerEm()I

    move-result v0

    int-to-float v0, v0

    const/high16 v1, 0x447a0000    # 1000.0f

    cmpl-float v2, v0, v1

    if-eqz v2, :cond_1

    div-float/2addr v1, v0

    mul-float/2addr p1, v1

    :cond_1
    return p1
.end method

.method public isDamaged()Z
    .locals 1

    iget-boolean v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDTrueTypeFont;->isDamaged:Z

    return v0
.end method

.method public isEmbedded()Z
    .locals 1

    iget-boolean v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDTrueTypeFont;->isEmbedded:Z

    return v0
.end method

.method public readCode(Ljava/io/InputStream;)I
    .locals 0

    invoke-virtual {p1}, Ljava/io/InputStream;->read()I

    move-result p1

    return p1
.end method

.method public readEncodingFromFont()Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.class public abstract Lorg/apache/pdfbox/pdmodel/font/PDSimpleFont;
.super Lorg/apache/pdfbox/pdmodel/font/PDFont;
.source "SourceFile"


# instance fields
.field protected encoding:Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;

.field protected glyphList:Lorg/apache/pdfbox/pdmodel/font/encoding/GlyphList;

.field private isSymbolic:Ljava/lang/Boolean;

.field private final noUnicode:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/font/PDFont;-><init>()V

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDSimpleFont;->noUnicode:Ljava/util/Set;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .line 2
    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/font/PDFont;-><init>(Ljava/lang/String;)V

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDSimpleFont;->noUnicode:Ljava/util/Set;

    sget-object v0, Lorg/apache/pdfbox/pdmodel/font/encoding/WinAnsiEncoding;->INSTANCE:Lorg/apache/pdfbox/pdmodel/font/encoding/WinAnsiEncoding;

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDSimpleFont;->encoding:Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;

    const-string v0, "ZapfDingbats"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    invoke-static {}, Lorg/apache/pdfbox/pdmodel/font/encoding/GlyphList;->getZapfDingbats()Lorg/apache/pdfbox/pdmodel/font/encoding/GlyphList;

    move-result-object p1

    goto :goto_0

    :cond_0
    invoke-static {}, Lorg/apache/pdfbox/pdmodel/font/encoding/GlyphList;->getAdobeGlyphList()Lorg/apache/pdfbox/pdmodel/font/encoding/GlyphList;

    move-result-object p1

    :goto_0
    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/font/PDSimpleFont;->glyphList:Lorg/apache/pdfbox/pdmodel/font/encoding/GlyphList;

    return-void
.end method

.method public constructor <init>(Lorg/apache/pdfbox/cos/COSDictionary;)V
    .locals 0

    .line 3
    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/font/PDFont;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    new-instance p1, Ljava/util/HashSet;

    invoke-direct {p1}, Ljava/util/HashSet;-><init>()V

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/font/PDSimpleFont;->noUnicode:Ljava/util/Set;

    return-void
.end method


# virtual methods
.method public addToSubset(I)V
    .locals 0

    new-instance p1, Ljava/lang/UnsupportedOperationException;

    invoke-direct {p1}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw p1
.end method

.method public getEncoding()Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDSimpleFont;->encoding:Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;

    return-object v0
.end method

.method public getGlyphList()Lorg/apache/pdfbox/pdmodel/font/encoding/GlyphList;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDSimpleFont;->glyphList:Lorg/apache/pdfbox/pdmodel/font/encoding/GlyphList;

    return-object v0
.end method

.method public final getStandard14Width(I)F
    .locals 1

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDFont;->getStandard14AFM()Lorg/apache/fontbox/afm/FontMetrics;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDSimpleFont;->getEncoding()Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->getName(I)Ljava/lang/String;

    move-result-object p1

    const-string v0, ".notdef"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/high16 p1, 0x437a0000    # 250.0f

    return p1

    :cond_0
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDFont;->getStandard14AFM()Lorg/apache/fontbox/afm/FontMetrics;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/fontbox/afm/FontMetrics;->getCharacterWidth(Ljava/lang/String;)F

    move-result p1

    return p1

    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "No AFM"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public final getSymbolicFlag()Ljava/lang/Boolean;
    .locals 1

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDFont;->getFontDescriptor()Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDFont;->getFontDescriptor()Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->isSymbolic()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public isFontSymbolic()Ljava/lang/Boolean;
    .locals 3

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDSimpleFont;->getSymbolicFlag()Ljava/lang/Boolean;

    move-result-object v0

    if-eqz v0, :cond_0

    return-object v0

    :cond_0
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDSimpleFont;->isStandard14()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDFont;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Symbol"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDFont;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "ZapfDingbats"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    :goto_0
    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_3
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDSimpleFont;->encoding:Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;

    if-nez v0, :cond_5

    instance-of v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDTrueTypeFont;

    if-eqz v0, :cond_4

    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    return-object v0

    :cond_4
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "PDFBox bug: encoding should not be null!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_5
    instance-of v1, v0, Lorg/apache/pdfbox/pdmodel/font/encoding/WinAnsiEncoding;

    if-nez v1, :cond_c

    instance-of v1, v0, Lorg/apache/pdfbox/pdmodel/font/encoding/MacRomanEncoding;

    if-nez v1, :cond_c

    instance-of v1, v0, Lorg/apache/pdfbox/pdmodel/font/encoding/StandardEncoding;

    if-eqz v1, :cond_6

    goto :goto_3

    :cond_6
    instance-of v1, v0, Lorg/apache/pdfbox/pdmodel/font/encoding/DictionaryEncoding;

    if-eqz v1, :cond_b

    check-cast v0, Lorg/apache/pdfbox/pdmodel/font/encoding/DictionaryEncoding;

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/font/encoding/DictionaryEncoding;->getDifferences()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_7
    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_a

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const-string v2, ".notdef"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    goto :goto_2

    :cond_8
    sget-object v2, Lorg/apache/pdfbox/pdmodel/font/encoding/WinAnsiEncoding;->INSTANCE:Lorg/apache/pdfbox/pdmodel/font/encoding/WinAnsiEncoding;

    invoke-virtual {v2, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->contains(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_9

    sget-object v2, Lorg/apache/pdfbox/pdmodel/font/encoding/MacRomanEncoding;->INSTANCE:Lorg/apache/pdfbox/pdmodel/font/encoding/MacRomanEncoding;

    invoke-virtual {v2, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->contains(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_9

    sget-object v2, Lorg/apache/pdfbox/pdmodel/font/encoding/StandardEncoding;->INSTANCE:Lorg/apache/pdfbox/pdmodel/font/encoding/StandardEncoding;

    invoke-virtual {v2, v1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->contains(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_7

    :cond_9
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    return-object v0

    :cond_a
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    return-object v0

    :cond_b
    const/4 v0, 0x0

    return-object v0

    :cond_c
    :goto_3
    sget-object v0, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    return-object v0
.end method

.method public isStandard14()Z
    .locals 1

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDSimpleFont;->getEncoding()Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;

    move-result-object v0

    instance-of v0, v0, Lorg/apache/pdfbox/pdmodel/font/encoding/DictionaryEncoding;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDSimpleFont;->getEncoding()Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/pdmodel/font/encoding/DictionaryEncoding;

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/font/encoding/DictionaryEncoding;->getDifferences()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x0

    return v0

    :cond_0
    invoke-super {p0}, Lorg/apache/pdfbox/pdmodel/font/PDFont;->isStandard14()Z

    move-result v0

    return v0
.end method

.method public final isSymbolic()Z
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDSimpleFont;->isSymbolic:Ljava/lang/Boolean;

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDSimpleFont;->isFontSymbolic()Ljava/lang/Boolean;

    move-result-object v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    :goto_0
    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDSimpleFont;->isSymbolic:Ljava/lang/Boolean;

    :cond_1
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDSimpleFont;->isSymbolic:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public isVertical()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final readEncoding()V
    .locals 7

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDFont;->dict:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->ENCODING:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    if-eqz v0, :cond_5

    instance-of v1, v0, Lorg/apache/pdfbox/cos/COSName;

    if-eqz v1, :cond_0

    check-cast v0, Lorg/apache/pdfbox/cos/COSName;

    invoke-static {v0}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->getInstance(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/pdfbox/pdmodel/font/PDSimpleFont;->encoding:Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;

    if-nez v1, :cond_6

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown encoding: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSName;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "PdfBoxAndroid"

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    :cond_0
    instance-of v1, v0, Lorg/apache/pdfbox/cos/COSDictionary;

    if-eqz v1, :cond_6

    check-cast v0, Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDSimpleFont;->getSymbolicFlag()Ljava/lang/Boolean;

    move-result-object v1

    const/4 v2, 0x1

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_1

    move v3, v2

    goto :goto_0

    :cond_1
    const/4 v3, 0x0

    :goto_0
    sget-object v4, Lorg/apache/pdfbox/cos/COSName;->BASE_ENCODING:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v4}, Lorg/apache/pdfbox/cos/COSDictionary;->containsKey(Lorg/apache/pdfbox/cos/COSName;)Z

    move-result v5

    const/4 v6, 0x0

    if-nez v5, :cond_2

    if-eqz v3, :cond_2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDSimpleFont;->readEncodingFromFont()Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;

    move-result-object v3

    goto :goto_1

    :cond_2
    move-object v3, v6

    :goto_1
    if-nez v1, :cond_3

    sget-object v1, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    :cond_3
    if-nez v3, :cond_4

    invoke-virtual {v0, v4}, Lorg/apache/pdfbox/cos/COSDictionary;->containsKey(Lorg/apache/pdfbox/cos/COSName;)Z

    move-result v4

    if-nez v4, :cond_4

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_4

    iput-object v6, p0, Lorg/apache/pdfbox/pdmodel/font/PDSimpleFont;->encoding:Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;

    goto :goto_3

    :cond_4
    new-instance v4, Lorg/apache/pdfbox/pdmodel/font/encoding/DictionaryEncoding;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    xor-int/2addr v1, v2

    invoke-direct {v4, v0, v1, v3}, Lorg/apache/pdfbox/pdmodel/font/encoding/DictionaryEncoding;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;ZLorg/apache/pdfbox/pdmodel/font/encoding/Encoding;)V

    iput-object v4, p0, Lorg/apache/pdfbox/pdmodel/font/PDSimpleFont;->encoding:Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;

    goto :goto_3

    :cond_5
    :goto_2
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDSimpleFont;->readEncodingFromFont()Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDSimpleFont;->encoding:Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;

    :cond_6
    :goto_3
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDSimpleFont;->encoding:Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;

    if-nez v0, :cond_7

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDSimpleFont;->getSymbolicFlag()Ljava/lang/Boolean;

    move-result-object v0

    if-eqz v0, :cond_7

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDSimpleFont;->getSymbolicFlag()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_7

    sget-object v0, Lorg/apache/pdfbox/pdmodel/font/encoding/StandardEncoding;->INSTANCE:Lorg/apache/pdfbox/pdmodel/font/encoding/StandardEncoding;

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDSimpleFont;->encoding:Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;

    :cond_7
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDSimpleFont;->encoding:Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;

    const-string v1, "ZapfDingbats"

    if-nez v0, :cond_8

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDSimpleFont;->isStandard14()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDFont;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v2, "Symbol"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_8

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDFont;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_8

    sget-object v0, Lorg/apache/pdfbox/pdmodel/font/encoding/StandardEncoding;->INSTANCE:Lorg/apache/pdfbox/pdmodel/font/encoding/StandardEncoding;

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDSimpleFont;->encoding:Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;

    :cond_8
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDFont;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-static {}, Lorg/apache/pdfbox/pdmodel/font/encoding/GlyphList;->getZapfDingbats()Lorg/apache/pdfbox/pdmodel/font/encoding/GlyphList;

    move-result-object v0

    goto :goto_4

    :cond_9
    invoke-static {}, Lorg/apache/pdfbox/pdmodel/font/encoding/GlyphList;->getAdobeGlyphList()Lorg/apache/pdfbox/pdmodel/font/encoding/GlyphList;

    move-result-object v0

    :goto_4
    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDSimpleFont;->glyphList:Lorg/apache/pdfbox/pdmodel/font/encoding/GlyphList;

    return-void
.end method

.method public abstract readEncodingFromFont()Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;
.end method

.method public subset()V
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public toUnicode(I)Ljava/lang/String;
    .locals 1

    .line 1
    invoke-static {}, Lorg/apache/pdfbox/pdmodel/font/encoding/GlyphList;->getAdobeGlyphList()Lorg/apache/pdfbox/pdmodel/font/encoding/GlyphList;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lorg/apache/pdfbox/pdmodel/font/PDSimpleFont;->toUnicode(ILorg/apache/pdfbox/pdmodel/font/encoding/GlyphList;)Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method public toUnicode(ILorg/apache/pdfbox/pdmodel/font/encoding/GlyphList;)Ljava/lang/String;
    .locals 4

    .line 2
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDSimpleFont;->glyphList:Lorg/apache/pdfbox/pdmodel/font/encoding/GlyphList;

    invoke-static {}, Lorg/apache/pdfbox/pdmodel/font/encoding/GlyphList;->getAdobeGlyphList()Lorg/apache/pdfbox/pdmodel/font/encoding/GlyphList;

    move-result-object v1

    if-ne v0, v1, :cond_0

    goto :goto_0

    :cond_0
    iget-object p2, p0, Lorg/apache/pdfbox/pdmodel/font/PDSimpleFont;->glyphList:Lorg/apache/pdfbox/pdmodel/font/encoding/GlyphList;

    :goto_0
    invoke-super {p0, p1}, Lorg/apache/pdfbox/pdmodel/font/PDFont;->toUnicode(I)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    return-object v0

    :cond_1
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/font/PDSimpleFont;->encoding:Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;

    const/4 v1, 0x0

    if-eqz v0, :cond_2

    invoke-virtual {v0, p1}, Lorg/apache/pdfbox/pdmodel/font/encoding/Encoding;->getName(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lorg/apache/pdfbox/pdmodel/font/encoding/GlyphList;->toUnicode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p2

    if-eqz p2, :cond_3

    return-object p2

    :cond_2
    move-object v0, v1

    :cond_3
    iget-object p2, p0, Lorg/apache/pdfbox/pdmodel/font/PDSimpleFont;->noUnicode:Ljava/util/Set;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {p2, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result p2

    if-nez p2, :cond_5

    iget-object p2, p0, Lorg/apache/pdfbox/pdmodel/font/PDSimpleFont;->noUnicode:Ljava/util/Set;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {p2, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    const-string p2, "PdfBoxAndroid"

    if-eqz v0, :cond_4

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "No Unicode mapping for "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " ("

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, ") in font "

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDFont;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_1

    :cond_4
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "No Unicode mapping for character code "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, " in font "

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/font/PDFont;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    :goto_1
    invoke-static {p2, p1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_5
    return-object v1
.end method

.method public willBeSubset()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

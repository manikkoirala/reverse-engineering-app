.class public Lorg/apache/pdfbox/pdmodel/PDDocument;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Closeable;


# instance fields
.field private accessPermission:Lorg/apache/pdfbox/pdmodel/encryption/AccessPermission;

.field private allSecurityToBeRemoved:Z

.field private final document:Lorg/apache/pdfbox/cos/COSDocument;

.field private documentCatalog:Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;

.field private documentId:Ljava/lang/Long;

.field private documentInformation:Lorg/apache/pdfbox/pdmodel/PDDocumentInformation;

.field private encryption:Lorg/apache/pdfbox/pdmodel/encryption/PDEncryption;

.field private final fontsToSubset:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Lorg/apache/pdfbox/pdmodel/font/PDFont;",
            ">;"
        }
    .end annotation
.end field

.field private incrementalFile:Ljava/io/File;

.field private final parser:Lorg/apache/pdfbox/pdfparser/BaseParser;

.field private signInterface:Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/SignatureInterface;


# direct methods
.method public constructor <init>()V
    .locals 4

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDDocument;->fontsToSubset:Ljava/util/Set;

    new-instance v0, Lorg/apache/pdfbox/cos/COSDocument;

    invoke-direct {v0}, Lorg/apache/pdfbox/cos/COSDocument;-><init>()V

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDDocument;->document:Lorg/apache/pdfbox/cos/COSDocument;

    const/4 v1, 0x0

    iput-object v1, p0, Lorg/apache/pdfbox/pdmodel/PDDocument;->parser:Lorg/apache/pdfbox/pdfparser/BaseParser;

    new-instance v1, Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-direct {v1}, Lorg/apache/pdfbox/cos/COSDictionary;-><init>()V

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDocument;->setTrailer(Lorg/apache/pdfbox/cos/COSDictionary;)V

    new-instance v0, Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-direct {v0}, Lorg/apache/pdfbox/cos/COSDictionary;-><init>()V

    sget-object v2, Lorg/apache/pdfbox/cos/COSName;->ROOT:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v1, v2, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->TYPE:Lorg/apache/pdfbox/cos/COSName;

    sget-object v2, Lorg/apache/pdfbox/cos/COSName;->CATALOG:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    sget-object v2, Lorg/apache/pdfbox/cos/COSName;->VERSION:Lorg/apache/pdfbox/cos/COSName;

    const-string v3, "1.4"

    invoke-static {v3}, Lorg/apache/pdfbox/cos/COSName;->getPDFName(Ljava/lang/String;)Lorg/apache/pdfbox/cos/COSName;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    new-instance v2, Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-direct {v2}, Lorg/apache/pdfbox/cos/COSDictionary;-><init>()V

    sget-object v3, Lorg/apache/pdfbox/cos/COSName;->PAGES:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v3, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    invoke-virtual {v2, v1, v3}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    new-instance v0, Lorg/apache/pdfbox/cos/COSArray;

    invoke-direct {v0}, Lorg/apache/pdfbox/cos/COSArray;-><init>()V

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->KIDS:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v2, v1, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->COUNT:Lorg/apache/pdfbox/cos/COSName;

    sget-object v1, Lorg/apache/pdfbox/cos/COSInteger;->ZERO:Lorg/apache/pdfbox/cos/COSInteger;

    invoke-virtual {v2, v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    return-void
.end method

.method public constructor <init>(Lorg/apache/pdfbox/cos/COSDocument;)V
    .locals 1

    .line 2
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lorg/apache/pdfbox/pdmodel/PDDocument;-><init>(Lorg/apache/pdfbox/cos/COSDocument;Lorg/apache/pdfbox/pdfparser/BaseParser;)V

    return-void
.end method

.method public constructor <init>(Lorg/apache/pdfbox/cos/COSDocument;Lorg/apache/pdfbox/pdfparser/BaseParser;)V
    .locals 1

    .line 3
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/pdfbox/pdmodel/PDDocument;-><init>(Lorg/apache/pdfbox/cos/COSDocument;Lorg/apache/pdfbox/pdfparser/BaseParser;Lorg/apache/pdfbox/pdmodel/encryption/AccessPermission;)V

    return-void
.end method

.method public constructor <init>(Lorg/apache/pdfbox/cos/COSDocument;Lorg/apache/pdfbox/pdfparser/BaseParser;Lorg/apache/pdfbox/pdmodel/encryption/AccessPermission;)V
    .locals 1

    .line 4
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDDocument;->fontsToSubset:Ljava/util/Set;

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/PDDocument;->document:Lorg/apache/pdfbox/cos/COSDocument;

    iput-object p2, p0, Lorg/apache/pdfbox/pdmodel/PDDocument;->parser:Lorg/apache/pdfbox/pdfparser/BaseParser;

    iput-object p3, p0, Lorg/apache/pdfbox/pdmodel/PDDocument;->accessPermission:Lorg/apache/pdfbox/pdmodel/encryption/AccessPermission;

    return-void
.end method

.method public static load(Ljava/io/File;)Lorg/apache/pdfbox/pdmodel/PDDocument;
    .locals 2

    .line 1
    const-string v0, ""

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/PDDocument;->load(Ljava/io/File;Ljava/lang/String;Z)Lorg/apache/pdfbox/pdmodel/PDDocument;

    move-result-object p0

    return-object p0
.end method

.method public static load(Ljava/io/File;Ljava/lang/String;)Lorg/apache/pdfbox/pdmodel/PDDocument;
    .locals 2

    .line 2
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-static {p0, p1, v0, v0, v1}, Lorg/apache/pdfbox/pdmodel/PDDocument;->load(Ljava/io/File;Ljava/lang/String;Ljava/io/InputStream;Ljava/lang/String;Z)Lorg/apache/pdfbox/pdmodel/PDDocument;

    move-result-object p0

    return-object p0
.end method

.method public static load(Ljava/io/File;Ljava/lang/String;Ljava/io/InputStream;Ljava/lang/String;)Lorg/apache/pdfbox/pdmodel/PDDocument;
    .locals 1

    .line 3
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, p3, v0}, Lorg/apache/pdfbox/pdmodel/PDDocument;->load(Ljava/io/File;Ljava/lang/String;Ljava/io/InputStream;Ljava/lang/String;Z)Lorg/apache/pdfbox/pdmodel/PDDocument;

    move-result-object p0

    return-object p0
.end method

.method public static load(Ljava/io/File;Ljava/lang/String;Ljava/io/InputStream;Ljava/lang/String;Z)Lorg/apache/pdfbox/pdmodel/PDDocument;
    .locals 7

    .line 4
    new-instance v6, Lorg/apache/pdfbox/pdfparser/PDFParser;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lorg/apache/pdfbox/pdfparser/PDFParser;-><init>(Ljava/io/File;Ljava/lang/String;Ljava/io/InputStream;Ljava/lang/String;Z)V

    invoke-virtual {v6}, Lorg/apache/pdfbox/pdfparser/PDFParser;->parse()V

    invoke-virtual {v6}, Lorg/apache/pdfbox/pdfparser/PDFParser;->getPDDocument()Lorg/apache/pdfbox/pdmodel/PDDocument;

    move-result-object p1

    iput-object p0, p1, Lorg/apache/pdfbox/pdmodel/PDDocument;->incrementalFile:Ljava/io/File;

    return-object p1
.end method

.method public static load(Ljava/io/File;Ljava/lang/String;Z)Lorg/apache/pdfbox/pdmodel/PDDocument;
    .locals 1

    .line 5
    const/4 v0, 0x0

    invoke-static {p0, p1, v0, v0, p2}, Lorg/apache/pdfbox/pdmodel/PDDocument;->load(Ljava/io/File;Ljava/lang/String;Ljava/io/InputStream;Ljava/lang/String;Z)Lorg/apache/pdfbox/pdmodel/PDDocument;

    move-result-object p0

    return-object p0
.end method

.method public static load(Ljava/io/File;Z)Lorg/apache/pdfbox/pdmodel/PDDocument;
    .locals 2

    .line 6
    const-string v0, ""

    const/4 v1, 0x0

    invoke-static {p0, v0, v1, v1, p1}, Lorg/apache/pdfbox/pdmodel/PDDocument;->load(Ljava/io/File;Ljava/lang/String;Ljava/io/InputStream;Ljava/lang/String;Z)Lorg/apache/pdfbox/pdmodel/PDDocument;

    move-result-object p0

    return-object p0
.end method

.method public static load(Ljava/io/InputStream;)Lorg/apache/pdfbox/pdmodel/PDDocument;
    .locals 3

    .line 7
    const/4 v0, 0x0

    const/4 v1, 0x0

    const-string v2, ""

    invoke-static {p0, v2, v0, v0, v1}, Lorg/apache/pdfbox/pdmodel/PDDocument;->load(Ljava/io/InputStream;Ljava/lang/String;Ljava/io/InputStream;Ljava/lang/String;Z)Lorg/apache/pdfbox/pdmodel/PDDocument;

    move-result-object p0

    return-object p0
.end method

.method public static load(Ljava/io/InputStream;Ljava/lang/String;)Lorg/apache/pdfbox/pdmodel/PDDocument;
    .locals 1

    .line 8
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lorg/apache/pdfbox/pdmodel/PDDocument;->load(Ljava/io/InputStream;Ljava/lang/String;Z)Lorg/apache/pdfbox/pdmodel/PDDocument;

    move-result-object p0

    return-object p0
.end method

.method public static load(Ljava/io/InputStream;Ljava/lang/String;Ljava/io/InputStream;Ljava/lang/String;)Lorg/apache/pdfbox/pdmodel/PDDocument;
    .locals 1

    .line 9
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, p3, v0}, Lorg/apache/pdfbox/pdmodel/PDDocument;->load(Ljava/io/InputStream;Ljava/lang/String;Ljava/io/InputStream;Ljava/lang/String;Z)Lorg/apache/pdfbox/pdmodel/PDDocument;

    move-result-object p0

    return-object p0
.end method

.method public static load(Ljava/io/InputStream;Ljava/lang/String;Ljava/io/InputStream;Ljava/lang/String;Z)Lorg/apache/pdfbox/pdmodel/PDDocument;
    .locals 7

    .line 10
    new-instance v6, Lorg/apache/pdfbox/pdfparser/PDFParser;

    move-object v0, v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lorg/apache/pdfbox/pdfparser/PDFParser;-><init>(Ljava/io/InputStream;Ljava/lang/String;Ljava/io/InputStream;Ljava/lang/String;Z)V

    invoke-virtual {v6}, Lorg/apache/pdfbox/pdfparser/PDFParser;->parse()V

    invoke-virtual {v6}, Lorg/apache/pdfbox/pdfparser/PDFParser;->getPDDocument()Lorg/apache/pdfbox/pdmodel/PDDocument;

    move-result-object p0

    return-object p0
.end method

.method public static load(Ljava/io/InputStream;Ljava/lang/String;Z)Lorg/apache/pdfbox/pdmodel/PDDocument;
    .locals 1

    .line 11
    const/4 p2, 0x0

    const/4 v0, 0x0

    invoke-static {p0, p1, p2, p2, v0}, Lorg/apache/pdfbox/pdmodel/PDDocument;->load(Ljava/io/InputStream;Ljava/lang/String;Ljava/io/InputStream;Ljava/lang/String;Z)Lorg/apache/pdfbox/pdmodel/PDDocument;

    move-result-object p0

    return-object p0
.end method

.method public static load(Ljava/io/InputStream;Z)Lorg/apache/pdfbox/pdmodel/PDDocument;
    .locals 2

    .line 12
    const-string v0, ""

    const/4 v1, 0x0

    invoke-static {p0, v0, v1, v1, p1}, Lorg/apache/pdfbox/pdmodel/PDDocument;->load(Ljava/io/InputStream;Ljava/lang/String;Ljava/io/InputStream;Ljava/lang/String;Z)Lorg/apache/pdfbox/pdmodel/PDDocument;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public addPage(Lorg/apache/pdfbox/pdmodel/PDPage;)V
    .locals 1

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/PDDocument;->getPages()Lorg/apache/pdfbox/pdmodel/PDPageTree;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/pdfbox/pdmodel/PDPageTree;->add(Lorg/apache/pdfbox/pdmodel/PDPage;)V

    return-void
.end method

.method public addSignature(Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/PDSignature;Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/SignatureInterface;)V
    .locals 1

    .line 1
    new-instance v0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/SignatureOptions;

    invoke-direct {v0}, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/SignatureOptions;-><init>()V

    invoke-virtual {p0, p1, p2, v0}, Lorg/apache/pdfbox/pdmodel/PDDocument;->addSignature(Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/PDSignature;Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/SignatureInterface;Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/SignatureOptions;)V

    return-void
.end method

.method public addSignature(Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/PDSignature;Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/SignatureInterface;Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/SignatureOptions;)V
    .locals 17

    .line 2
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual/range {p3 .. p3}, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/SignatureOptions;->getPreferedSignatureSize()I

    move-result v2

    if-lez v2, :cond_0

    new-array v2, v2, [B

    invoke-virtual {v1, v2}, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/PDSignature;->setContents([B)V

    goto :goto_0

    :cond_0
    const/16 v2, 0x2500

    new-array v2, v2, [B

    invoke-virtual {v1, v2}, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/PDSignature;->setContents([B)V

    :goto_0
    const/4 v2, 0x0

    const v3, 0x3b9aca00

    filled-new-array {v2, v3, v3, v3}, [I

    move-result-object v3

    invoke-virtual {v1, v3}, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/PDSignature;->setByteRange([I)V

    move-object/from16 v3, p2

    iput-object v3, v0, Lorg/apache/pdfbox/pdmodel/PDDocument;->signInterface:Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/SignatureInterface;

    invoke-virtual/range {p0 .. p0}, Lorg/apache/pdfbox/pdmodel/PDDocument;->getDocumentCatalog()Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;

    move-result-object v3

    invoke-virtual {v3}, Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;->getPages()Lorg/apache/pdfbox/pdmodel/PDPageTree;

    move-result-object v4

    invoke-virtual {v4}, Lorg/apache/pdfbox/pdmodel/PDPageTree;->getCount()I

    move-result v4

    if-eqz v4, :cond_12

    invoke-virtual/range {p3 .. p3}, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/SignatureOptions;->getPage()I

    move-result v5

    invoke-static {v5, v2}, Ljava/lang/Math;->max(II)I

    move-result v5

    const/4 v6, 0x1

    sub-int/2addr v4, v6

    invoke-static {v5, v4}, Ljava/lang/Math;->min(II)I

    move-result v4

    invoke-virtual {v3}, Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;->getPages()Lorg/apache/pdfbox/pdmodel/PDPageTree;

    move-result-object v5

    invoke-virtual {v5, v4}, Lorg/apache/pdfbox/pdmodel/PDPageTree;->get(I)Lorg/apache/pdfbox/pdmodel/PDPage;

    move-result-object v4

    invoke-virtual {v3}, Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;->getAcroForm()Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;

    move-result-object v5

    invoke-virtual {v3}, Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;->getCOSObject()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v7

    invoke-virtual {v7, v6}, Lorg/apache/pdfbox/cos/COSDictionary;->setNeedToBeUpdated(Z)V

    if-nez v5, :cond_1

    new-instance v5, Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;

    invoke-direct {v5, v0}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;-><init>(Lorg/apache/pdfbox/pdmodel/PDDocument;)V

    invoke-virtual {v3, v5}, Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;->setAcroForm(Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;)V

    goto :goto_1

    :cond_1
    invoke-virtual {v5}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v3

    invoke-virtual {v3, v6}, Lorg/apache/pdfbox/cos/COSDictionary;->setNeedToBeUpdated(Z)V

    :goto_1
    invoke-virtual {v4}, Lorg/apache/pdfbox/pdmodel/PDPage;->getAnnotations()Ljava/util/List;

    move-result-object v3

    invoke-virtual {v5}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;->getFields()Ljava/util/List;

    move-result-object v7

    if-nez v7, :cond_2

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v5, v7}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;->setFields(Ljava/util/List;)V

    :cond_2
    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    const/4 v8, 0x0

    move-object v9, v8

    :cond_3
    :goto_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_4

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;

    instance-of v11, v10, Lorg/apache/pdfbox/pdmodel/interactive/form/PDSignatureField;

    if-eqz v11, :cond_3

    check-cast v10, Lorg/apache/pdfbox/pdmodel/interactive/form/PDSignatureField;

    invoke-virtual {v10}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDSignatureField;->getSignature()Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/PDSignature;

    move-result-object v11

    if-eqz v11, :cond_3

    invoke-virtual {v11}, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/PDSignature;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v11

    invoke-virtual/range {p1 .. p1}, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/PDSignature;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_3

    move-object v9, v10

    goto :goto_2

    :cond_4
    if-nez v9, :cond_5

    new-instance v9, Lorg/apache/pdfbox/pdmodel/interactive/form/PDSignatureField;

    invoke-direct {v9, v5}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDSignatureField;-><init>(Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;)V

    invoke-virtual {v9, v1}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDSignatureField;->setSignature(Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/PDSignature;)V

    invoke-virtual {v9}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->getWidget()Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotationWidget;

    move-result-object v1

    invoke-virtual {v1, v4}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotation;->setPage(Lorg/apache/pdfbox/pdmodel/PDPage;)V

    :cond_5
    invoke-virtual {v5}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;->getFields()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v5}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v7

    invoke-virtual {v7, v6}, Lorg/apache/pdfbox/cos/COSBase;->setDirect(Z)V

    invoke-virtual {v5, v6}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;->setSignaturesExist(Z)V

    invoke-virtual {v5, v6}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;->setAppendOnly(Z)V

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_6
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_7

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;

    instance-of v11, v10, Lorg/apache/pdfbox/pdmodel/interactive/form/PDSignatureField;

    if-eqz v11, :cond_6

    check-cast v10, Lorg/apache/pdfbox/pdmodel/interactive/form/PDSignatureField;

    invoke-virtual {v10}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->getCOSObject()Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v10

    invoke-virtual {v9}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->getCOSObject()Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_6

    invoke-virtual {v9}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v7

    invoke-virtual {v7, v6}, Lorg/apache/pdfbox/cos/COSDictionary;->setNeedToBeUpdated(Z)V

    move v7, v6

    goto :goto_3

    :cond_7
    move v7, v2

    :goto_3
    if-nez v7, :cond_8

    invoke-interface {v1, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_8
    invoke-virtual/range {p3 .. p3}, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/SignatureOptions;->getVisualSignature()Lorg/apache/pdfbox/cos/COSDocument;

    move-result-object v10

    if-nez v10, :cond_9

    invoke-virtual {v9}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->getWidget()Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotationWidget;

    move-result-object v2

    new-instance v10, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;

    invoke-direct {v10}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;-><init>()V

    invoke-virtual {v2, v10}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotation;->setRectangle(Lorg/apache/pdfbox/pdmodel/common/PDRectangle;)V

    invoke-virtual {v5, v8}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;->setDefaultResources(Lorg/apache/pdfbox/pdmodel/PDResources;)V

    new-instance v2, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAppearanceDictionary;

    invoke-direct {v2}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAppearanceDictionary;-><init>()V

    invoke-virtual/range {p0 .. p0}, Lorg/apache/pdfbox/pdmodel/PDDocument;->getDocument()Lorg/apache/pdfbox/cos/COSDocument;

    move-result-object v5

    invoke-virtual {v5}, Lorg/apache/pdfbox/cos/COSDocument;->createCOSStream()Lorg/apache/pdfbox/cos/COSStream;

    move-result-object v5

    invoke-virtual {v5}, Lorg/apache/pdfbox/cos/COSStream;->createUnfilteredStream()Ljava/io/OutputStream;

    new-instance v8, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAppearanceStream;

    invoke-direct {v8, v5}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAppearanceStream;-><init>(Lorg/apache/pdfbox/cos/COSStream;)V

    invoke-virtual {v8}, Lorg/apache/pdfbox/pdmodel/graphics/PDXObject;->getCOSObject()Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v5

    check-cast v5, Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v10, Lorg/apache/pdfbox/cos/COSName;->SUBTYPE:Lorg/apache/pdfbox/cos/COSName;

    sget-object v11, Lorg/apache/pdfbox/cos/COSName;->FORM:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v5, v10, v11}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    sget-object v10, Lorg/apache/pdfbox/cos/COSName;->BBOX:Lorg/apache/pdfbox/cos/COSName;

    new-instance v11, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;

    invoke-direct {v11}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;-><init>()V

    invoke-virtual {v5, v10, v11}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)V

    invoke-virtual {v2, v8}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAppearanceDictionary;->setNormalAppearance(Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAppearanceStream;)V

    invoke-virtual {v2}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAppearanceDictionary;->getCOSObject()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v5

    invoke-virtual {v5, v6}, Lorg/apache/pdfbox/cos/COSBase;->setDirect(Z)V

    invoke-virtual {v9}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->getWidget()Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotationWidget;

    move-result-object v5

    invoke-virtual {v5, v2}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotation;->setAppearance(Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAppearanceDictionary;)V

    goto/16 :goto_6

    :cond_9
    invoke-virtual {v10}, Lorg/apache/pdfbox/cos/COSDocument;->getObjects()Ljava/util/List;

    move-result-object v8

    invoke-virtual {v5}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v5

    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    move v10, v6

    move v11, v10

    :goto_4
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_e

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lorg/apache/pdfbox/cos/COSObject;

    if-nez v10, :cond_a

    if-nez v11, :cond_a

    goto/16 :goto_5

    :cond_a
    invoke-virtual {v12}, Lorg/apache/pdfbox/cos/COSObject;->getObject()Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v12

    instance-of v13, v12, Lorg/apache/pdfbox/cos/COSDictionary;

    if-eqz v13, :cond_d

    check-cast v12, Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v13, Lorg/apache/pdfbox/cos/COSName;->FT:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v12, v13}, Lorg/apache/pdfbox/cos/COSDictionary;->getItem(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v13

    sget-object v14, Lorg/apache/pdfbox/cos/COSName;->TYPE:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v12, v14}, Lorg/apache/pdfbox/cos/COSDictionary;->getItem(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v14

    sget-object v15, Lorg/apache/pdfbox/cos/COSName;->AP:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v12, v15}, Lorg/apache/pdfbox/cos/COSDictionary;->getItem(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v16

    if-eqz v10, :cond_b

    sget-object v2, Lorg/apache/pdfbox/cos/COSName;->ANNOT:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v2, v14}, Lorg/apache/pdfbox/cos/COSName;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_b

    sget-object v2, Lorg/apache/pdfbox/cos/COSName;->RECT:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v12, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->getItem(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v2

    check-cast v2, Lorg/apache/pdfbox/cos/COSArray;

    new-instance v10, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;

    invoke-direct {v10, v2}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;-><init>(Lorg/apache/pdfbox/cos/COSArray;)V

    invoke-virtual {v9}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->getWidget()Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotationWidget;

    move-result-object v2

    invoke-virtual {v2, v10}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotation;->setRectangle(Lorg/apache/pdfbox/pdmodel/common/PDRectangle;)V

    const/4 v10, 0x0

    :cond_b
    if-eqz v11, :cond_d

    sget-object v2, Lorg/apache/pdfbox/cos/COSName;->SIG:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v2, v13}, Lorg/apache/pdfbox/cos/COSName;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_d

    if-eqz v16, :cond_d

    new-instance v2, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAppearanceDictionary;

    invoke-virtual {v12, v15}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v11

    check-cast v11, Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-direct {v2, v11}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAppearanceDictionary;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    invoke-virtual {v2}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAppearanceDictionary;->getCOSObject()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v11

    invoke-virtual {v11, v6}, Lorg/apache/pdfbox/cos/COSBase;->setDirect(Z)V

    invoke-virtual {v9}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->getWidget()Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotationWidget;

    move-result-object v11

    invoke-virtual {v11, v2}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotation;->setAppearance(Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAppearanceDictionary;)V

    sget-object v2, Lorg/apache/pdfbox/cos/COSName;->DR:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v12, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->getItem(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v11

    check-cast v11, Lorg/apache/pdfbox/cos/COSDictionary;

    if-eqz v11, :cond_c

    invoke-virtual {v11, v6}, Lorg/apache/pdfbox/cos/COSBase;->setDirect(Z)V

    invoke-virtual {v11, v6}, Lorg/apache/pdfbox/cos/COSDictionary;->setNeedToBeUpdated(Z)V

    invoke-virtual {v5, v2, v11}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    :cond_c
    const/4 v11, 0x0

    :cond_d
    const/4 v2, 0x0

    goto/16 :goto_4

    :cond_e
    :goto_5
    if-nez v10, :cond_11

    if-nez v11, :cond_11

    :goto_6
    instance-of v2, v3, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;

    if-eqz v2, :cond_f

    instance-of v2, v1, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;

    if-eqz v2, :cond_f

    move-object v2, v3

    check-cast v2, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;

    invoke-virtual {v2}, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;->toList()Lorg/apache/pdfbox/cos/COSArray;

    move-result-object v2

    check-cast v1, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;

    invoke-virtual {v1}, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;->toList()Lorg/apache/pdfbox/cos/COSArray;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_f

    if-nez v7, :cond_10

    :cond_f
    invoke-virtual {v9}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->getWidget()Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotationWidget;

    move-result-object v1

    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_10
    invoke-virtual {v4}, Lorg/apache/pdfbox/pdmodel/PDPage;->getCOSObject()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v1

    invoke-interface {v1, v6}, Lorg/apache/pdfbox/cos/COSUpdateInfo;->setNeedToBeUpdated(Z)V

    return-void

    :cond_11
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Template is missing required objects"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_12
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Cannot sign an empty document"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public addSignatureField(Ljava/util/List;Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/SignatureInterface;Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/SignatureOptions;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lorg/apache/pdfbox/pdmodel/interactive/form/PDSignatureField;",
            ">;",
            "Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/SignatureInterface;",
            "Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/SignatureOptions;",
            ")V"
        }
    .end annotation

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/PDDocument;->getDocumentCatalog()Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;->getCOSObject()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {v1, v2}, Lorg/apache/pdfbox/cos/COSUpdateInfo;->setNeedToBeUpdated(Z)V

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;->getAcroForm()Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;

    move-result-object v1

    if-nez v1, :cond_0

    new-instance v1, Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;

    invoke-direct {v1, p0}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;-><init>(Lorg/apache/pdfbox/pdmodel/PDDocument;)V

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;->setAcroForm(Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;)V

    :cond_0
    invoke-virtual {v1}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    invoke-virtual {v0, v2}, Lorg/apache/pdfbox/cos/COSBase;->setDirect(Z)V

    invoke-virtual {v0, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->setNeedToBeUpdated(Z)V

    invoke-virtual {v1}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;->isSignaturesExist()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {v1, v2}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;->setSignaturesExist(Z)V

    :cond_1
    invoke-virtual {v1}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;->getFields()Ljava/util/List;

    move-result-object v0

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_2
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/pdfbox/pdmodel/interactive/form/PDSignatureField;

    invoke-virtual {v1}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v3

    invoke-virtual {v3, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->setNeedToBeUpdated(Z)V

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;

    instance-of v5, v4, Lorg/apache/pdfbox/pdmodel/interactive/form/PDSignatureField;

    if-eqz v5, :cond_3

    invoke-virtual {v4}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->getCOSObject()Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v4

    invoke-virtual {v1}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->getCOSObject()Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-virtual {v1}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v3

    invoke-virtual {v3, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->setNeedToBeUpdated(Z)V

    move v3, v2

    goto :goto_1

    :cond_4
    const/4 v3, 0x0

    :goto_1
    if-nez v3, :cond_5

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_5
    invoke-virtual {v1}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDSignatureField;->getSignature()Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/PDSignature;

    move-result-object v3

    if-eqz v3, :cond_2

    invoke-virtual {v1}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v3

    invoke-virtual {v3, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->setNeedToBeUpdated(Z)V

    invoke-virtual {v1}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDSignatureField;->getSignature()Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/PDSignature;

    move-result-object v1

    invoke-virtual {p0, v1, p2, p3}, Lorg/apache/pdfbox/pdmodel/PDDocument;->addSignature(Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/PDSignature;Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/SignatureInterface;Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/SignatureOptions;)V

    goto :goto_0

    :cond_6
    return-void
.end method

.method public close()V
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDDocument;->document:Lorg/apache/pdfbox/cos/COSDocument;

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSDocument;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDDocument;->document:Lorg/apache/pdfbox/cos/COSDocument;

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSDocument;->close()V

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDDocument;->parser:Lorg/apache/pdfbox/pdfparser/BaseParser;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdfparser/BaseParser;->close()V

    :cond_0
    return-void
.end method

.method public getCurrentAccessPermission()Lorg/apache/pdfbox/pdmodel/encryption/AccessPermission;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDDocument;->accessPermission:Lorg/apache/pdfbox/pdmodel/encryption/AccessPermission;

    if-nez v0, :cond_0

    invoke-static {}, Lorg/apache/pdfbox/pdmodel/encryption/AccessPermission;->getOwnerAccessPermission()Lorg/apache/pdfbox/pdmodel/encryption/AccessPermission;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDDocument;->accessPermission:Lorg/apache/pdfbox/pdmodel/encryption/AccessPermission;

    :cond_0
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDDocument;->accessPermission:Lorg/apache/pdfbox/pdmodel/encryption/AccessPermission;

    return-object v0
.end method

.method public getDocument()Lorg/apache/pdfbox/cos/COSDocument;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDDocument;->document:Lorg/apache/pdfbox/cos/COSDocument;

    return-object v0
.end method

.method public getDocumentCatalog()Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDDocument;->documentCatalog:Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;

    if-nez v0, :cond_1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDDocument;->document:Lorg/apache/pdfbox/cos/COSDocument;

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSDocument;->getTrailer()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->ROOT:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    instance-of v1, v0, Lorg/apache/pdfbox/cos/COSDictionary;

    if-eqz v1, :cond_0

    new-instance v1, Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;

    check-cast v0, Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-direct {v1, p0, v0}, Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;-><init>(Lorg/apache/pdfbox/pdmodel/PDDocument;Lorg/apache/pdfbox/cos/COSDictionary;)V

    iput-object v1, p0, Lorg/apache/pdfbox/pdmodel/PDDocument;->documentCatalog:Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;

    goto :goto_0

    :cond_0
    new-instance v0, Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;

    invoke-direct {v0, p0}, Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;-><init>(Lorg/apache/pdfbox/pdmodel/PDDocument;)V

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDDocument;->documentCatalog:Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;

    :cond_1
    :goto_0
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDDocument;->documentCatalog:Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;

    return-object v0
.end method

.method public getDocumentId()Ljava/lang/Long;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDDocument;->documentId:Ljava/lang/Long;

    return-object v0
.end method

.method public getDocumentInformation()Lorg/apache/pdfbox/pdmodel/PDDocumentInformation;
    .locals 3

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDDocument;->documentInformation:Lorg/apache/pdfbox/pdmodel/PDDocumentInformation;

    if-nez v0, :cond_1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDDocument;->document:Lorg/apache/pdfbox/cos/COSDocument;

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSDocument;->getTrailer()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->INFO:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v2

    check-cast v2, Lorg/apache/pdfbox/cos/COSDictionary;

    if-nez v2, :cond_0

    new-instance v2, Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-direct {v2}, Lorg/apache/pdfbox/cos/COSDictionary;-><init>()V

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    :cond_0
    new-instance v0, Lorg/apache/pdfbox/pdmodel/PDDocumentInformation;

    invoke-direct {v0, v2}, Lorg/apache/pdfbox/pdmodel/PDDocumentInformation;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDDocument;->documentInformation:Lorg/apache/pdfbox/pdmodel/PDDocumentInformation;

    :cond_1
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDDocument;->documentInformation:Lorg/apache/pdfbox/pdmodel/PDDocumentInformation;

    return-object v0
.end method

.method public getEncryption()Lorg/apache/pdfbox/pdmodel/encryption/PDEncryption;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDDocument;->encryption:Lorg/apache/pdfbox/pdmodel/encryption/PDEncryption;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/PDDocument;->isEncrypted()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lorg/apache/pdfbox/pdmodel/encryption/PDEncryption;

    iget-object v1, p0, Lorg/apache/pdfbox/pdmodel/PDDocument;->document:Lorg/apache/pdfbox/cos/COSDocument;

    invoke-virtual {v1}, Lorg/apache/pdfbox/cos/COSDocument;->getEncryptionDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/pdmodel/encryption/PDEncryption;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDDocument;->encryption:Lorg/apache/pdfbox/pdmodel/encryption/PDEncryption;

    :cond_0
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDDocument;->encryption:Lorg/apache/pdfbox/pdmodel/encryption/PDEncryption;

    return-object v0
.end method

.method public getFontsToSubset()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Lorg/apache/pdfbox/pdmodel/font/PDFont;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDDocument;->fontsToSubset:Ljava/util/Set;

    return-object v0
.end method

.method public getLastSignatureDictionary()Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/PDSignature;
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/PDDocument;->getSignatureDictionaries()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_0

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/PDSignature;

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public getNumberOfPages()I
    .locals 1

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/PDDocument;->getDocumentCatalog()Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;->getPages()Lorg/apache/pdfbox/pdmodel/PDPageTree;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/PDPageTree;->getCount()I

    move-result v0

    return v0
.end method

.method public getPage(I)Lorg/apache/pdfbox/pdmodel/PDPage;
    .locals 1

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/PDDocument;->getDocumentCatalog()Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;->getPages()Lorg/apache/pdfbox/pdmodel/PDPageTree;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/pdfbox/pdmodel/PDPageTree;->get(I)Lorg/apache/pdfbox/pdmodel/PDPage;

    move-result-object p1

    return-object p1
.end method

.method public getPages()Lorg/apache/pdfbox/pdmodel/PDPageTree;
    .locals 1

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/PDDocument;->getDocumentCatalog()Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;->getPages()Lorg/apache/pdfbox/pdmodel/PDPageTree;

    move-result-object v0

    return-object v0
.end method

.method public getSignatureDictionaries()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/PDSignature;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDDocument;->document:Lorg/apache/pdfbox/cos/COSDocument;

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSDocument;->getSignatureDictionaries()Ljava/util/List;

    move-result-object v0

    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/pdfbox/cos/COSDictionary;

    new-instance v3, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/PDSignature;

    invoke-direct {v3, v2}, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/PDSignature;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method public getSignatureFields()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lorg/apache/pdfbox/pdmodel/interactive/form/PDSignatureField;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/PDDocument;->getDocumentCatalog()Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;->getAcroForm()Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v2, p0, Lorg/apache/pdfbox/pdmodel/PDDocument;->document:Lorg/apache/pdfbox/cos/COSDocument;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lorg/apache/pdfbox/cos/COSDocument;->getSignatureFields(Z)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/pdfbox/cos/COSDictionary;

    new-instance v4, Lorg/apache/pdfbox/pdmodel/interactive/form/PDSignatureField;

    const/4 v5, 0x0

    invoke-direct {v4, v1, v3, v5}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDSignatureField;-><init>(Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;Lorg/apache/pdfbox/cos/COSDictionary;Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;)V

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method public getVersion()F
    .locals 4

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/PDDocument;->getDocument()Lorg/apache/pdfbox/cos/COSDocument;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSDocument;->getVersion()F

    move-result v0

    const v1, 0x3fb33333    # 1.4f

    cmpl-float v1, v0, v1

    if-ltz v1, :cond_1

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/PDDocument;->getDocumentCatalog()Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;->getVersion()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    :try_start_0
    invoke-static {v1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v1
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    const-string v2, "PdfBoxAndroid"

    const-string v3, "Can\'t extract the version number of the document catalog."

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_0
    const/high16 v1, -0x40800000    # -1.0f

    :goto_0
    invoke-static {v1, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    :cond_1
    return v0
.end method

.method public importPage(Lorg/apache/pdfbox/pdmodel/PDPage;)Lorg/apache/pdfbox/pdmodel/PDPage;
    .locals 5

    new-instance v0, Lorg/apache/pdfbox/pdmodel/PDPage;

    new-instance v1, Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/PDPage;->getCOSObject()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v2

    invoke-direct {v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/pdmodel/PDPage;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    const/4 v1, 0x0

    :try_start_0
    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/PDPage;->getStream()Lorg/apache/pdfbox/pdmodel/common/PDStream;

    move-result-object p1

    if-eqz p1, :cond_0

    new-instance v2, Lorg/apache/pdfbox/pdmodel/common/PDStream;

    iget-object v3, p0, Lorg/apache/pdfbox/pdmodel/PDDocument;->document:Lorg/apache/pdfbox/cos/COSDocument;

    invoke-virtual {v3}, Lorg/apache/pdfbox/cos/COSDocument;->createCOSStream()Lorg/apache/pdfbox/cos/COSStream;

    move-result-object v3

    invoke-direct {v2, v3}, Lorg/apache/pdfbox/pdmodel/common/PDStream;-><init>(Lorg/apache/pdfbox/cos/COSStream;)V

    invoke-virtual {v2}, Lorg/apache/pdfbox/pdmodel/common/PDStream;->addCompression()V

    invoke-virtual {v0, v2}, Lorg/apache/pdfbox/pdmodel/PDPage;->setContents(Lorg/apache/pdfbox/pdmodel/common/PDStream;)V

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/common/PDStream;->createInputStream()Ljava/io/InputStream;

    move-result-object p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    :try_start_1
    invoke-virtual {v2}, Lorg/apache/pdfbox/pdmodel/common/PDStream;->createOutputStream()Ljava/io/OutputStream;

    move-result-object v1

    invoke-static {p1, v1}, Lorg/apache/pdfbox/io/IOUtils;->copy(Ljava/io/InputStream;Ljava/io/OutputStream;)J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object v4, v1

    move-object v1, p1

    move-object p1, v4

    goto :goto_0

    :catchall_0
    move-exception v0

    move-object v4, v1

    move-object v1, p1

    move-object p1, v4

    goto :goto_1

    :cond_0
    move-object p1, v1

    :goto_0
    :try_start_2
    invoke-virtual {p0, v0}, Lorg/apache/pdfbox/pdmodel/PDDocument;->addPage(Lorg/apache/pdfbox/pdmodel/PDPage;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Ljava/io/OutputStream;->close()V

    :cond_2
    return-object v0

    :catchall_1
    move-exception v0

    goto :goto_1

    :catchall_2
    move-exception v0

    move-object p1, v1

    :goto_1
    if-eqz v1, :cond_3

    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    :cond_3
    if-eqz p1, :cond_4

    invoke-virtual {p1}, Ljava/io/OutputStream;->close()V

    :cond_4
    throw v0
.end method

.method public isAllSecurityToBeRemoved()Z
    .locals 1

    iget-boolean v0, p0, Lorg/apache/pdfbox/pdmodel/PDDocument;->allSecurityToBeRemoved:Z

    return v0
.end method

.method public isEncrypted()Z
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDDocument;->document:Lorg/apache/pdfbox/cos/COSDocument;

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSDocument;->isEncrypted()Z

    move-result v0

    return v0
.end method

.method public protect(Lorg/apache/pdfbox/pdmodel/encryption/ProtectionPolicy;)V
    .locals 3

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/PDDocument;->isEncrypted()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Lorg/apache/pdfbox/pdmodel/encryption/PDEncryption;

    invoke-direct {v0}, Lorg/apache/pdfbox/pdmodel/encryption/PDEncryption;-><init>()V

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDDocument;->encryption:Lorg/apache/pdfbox/pdmodel/encryption/PDEncryption;

    :cond_0
    sget-object v0, Lorg/apache/pdfbox/pdmodel/encryption/SecurityHandlerFactory;->INSTANCE:Lorg/apache/pdfbox/pdmodel/encryption/SecurityHandlerFactory;

    invoke-virtual {v0, p1}, Lorg/apache/pdfbox/pdmodel/encryption/SecurityHandlerFactory;->newSecurityHandlerForPolicy(Lorg/apache/pdfbox/pdmodel/encryption/ProtectionPolicy;)Lorg/apache/pdfbox/pdmodel/encryption/SecurityHandler;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/PDDocument;->getEncryption()Lorg/apache/pdfbox/pdmodel/encryption/PDEncryption;

    move-result-object p1

    invoke-virtual {p1, v0}, Lorg/apache/pdfbox/pdmodel/encryption/PDEncryption;->setSecurityHandler(Lorg/apache/pdfbox/pdmodel/encryption/SecurityHandler;)V

    return-void

    :cond_1
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "No security handler for policy "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public removePage(I)V
    .locals 1

    .line 1
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/PDDocument;->getPages()Lorg/apache/pdfbox/pdmodel/PDPageTree;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/pdfbox/pdmodel/PDPageTree;->remove(I)V

    return-void
.end method

.method public removePage(Lorg/apache/pdfbox/pdmodel/PDPage;)V
    .locals 1

    .line 2
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/PDDocument;->getPages()Lorg/apache/pdfbox/pdmodel/PDPageTree;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/pdfbox/pdmodel/PDPageTree;->remove(Lorg/apache/pdfbox/pdmodel/PDPage;)V

    return-void
.end method

.method public save(Ljava/io/File;)V
    .locals 1

    .line 1
    new-instance v0, Ljava/io/FileOutputStream;

    invoke-direct {v0, p1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-virtual {p0, v0}, Lorg/apache/pdfbox/pdmodel/PDDocument;->save(Ljava/io/OutputStream;)V

    return-void
.end method

.method public save(Ljava/io/OutputStream;)V
    .locals 2

    .line 2
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDDocument;->document:Lorg/apache/pdfbox/cos/COSDocument;

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSDocument;->isClosed()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDDocument;->fontsToSubset:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/pdfbox/pdmodel/font/PDFont;

    invoke-virtual {v1}, Lorg/apache/pdfbox/pdmodel/font/PDFont;->subset()V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDDocument;->fontsToSubset:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    new-instance v0, Lorg/apache/pdfbox/pdfwriter/COSWriter;

    invoke-direct {v0, p1}, Lorg/apache/pdfbox/pdfwriter/COSWriter;-><init>(Ljava/io/OutputStream;)V

    :try_start_0
    invoke-virtual {v0, p0}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->write(Lorg/apache/pdfbox/pdmodel/PDDocument;)V

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->close()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->close()V

    return-void

    :catchall_0
    move-exception p1

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->close()V

    throw p1

    :cond_1
    new-instance p1, Ljava/io/IOException;

    const-string v0, "Cannot save a document which has been closed"

    invoke-direct {p1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public save(Ljava/lang/String;)V
    .locals 1

    .line 3
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lorg/apache/pdfbox/pdmodel/PDDocument;->save(Ljava/io/File;)V

    return-void
.end method

.method public saveIncremental(Ljava/io/OutputStream;)V
    .locals 3

    new-instance v0, Lorg/apache/pdfbox/io/RandomAccessBufferedFileInputStream;

    iget-object v1, p0, Lorg/apache/pdfbox/pdmodel/PDDocument;->incrementalFile:Ljava/io/File;

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/io/RandomAccessBufferedFileInputStream;-><init>(Ljava/io/File;)V

    const/4 v1, 0x0

    :try_start_0
    new-instance v2, Lorg/apache/pdfbox/pdfwriter/COSWriter;

    invoke-direct {v2, p1, v0}, Lorg/apache/pdfbox/pdfwriter/COSWriter;-><init>(Ljava/io/OutputStream;Ljava/io/InputStream;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    iget-object p1, p0, Lorg/apache/pdfbox/pdmodel/PDDocument;->signInterface:Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/SignatureInterface;

    invoke-virtual {v2, p0, p1}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->write(Lorg/apache/pdfbox/pdmodel/PDDocument;Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/SignatureInterface;)V

    invoke-virtual {v2}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->close()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-virtual {v2}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->close()V

    return-void

    :catchall_0
    move-exception p1

    move-object v1, v2

    goto :goto_0

    :catchall_1
    move-exception p1

    :goto_0
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->close()V

    :cond_0
    throw p1
.end method

.method public setAllSecurityToBeRemoved(Z)V
    .locals 0

    iput-boolean p1, p0, Lorg/apache/pdfbox/pdmodel/PDDocument;->allSecurityToBeRemoved:Z

    return-void
.end method

.method public setDocumentId(Ljava/lang/Long;)V
    .locals 0

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/PDDocument;->documentId:Ljava/lang/Long;

    return-void
.end method

.method public setDocumentInformation(Lorg/apache/pdfbox/pdmodel/PDDocumentInformation;)V
    .locals 2

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/PDDocument;->documentInformation:Lorg/apache/pdfbox/pdmodel/PDDocumentInformation;

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDDocument;->document:Lorg/apache/pdfbox/cos/COSDocument;

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSDocument;->getTrailer()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->INFO:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/PDDocumentInformation;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    return-void
.end method

.method public setEncryptionDictionary(Lorg/apache/pdfbox/pdmodel/encryption/PDEncryption;)V
    .locals 0

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/PDDocument;->encryption:Lorg/apache/pdfbox/pdmodel/encryption/PDEncryption;

    return-void
.end method

.method public setVersion(F)V
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/PDDocument;->getVersion()F

    move-result v0

    cmpl-float v1, p1, v0

    if-nez v1, :cond_0

    return-void

    :cond_0
    cmpg-float v0, p1, v0

    if-gez v0, :cond_1

    const-string p1, "PdfBoxAndroid"

    const-string v0, "It\'s not allowed to downgrade the version of a pdf."

    invoke-static {p1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_1
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/PDDocument;->getDocument()Lorg/apache/pdfbox/cos/COSDocument;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSDocument;->getVersion()F

    move-result v0

    const v1, 0x3fb33333    # 1.4f

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/PDDocument;->getDocumentCatalog()Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;->setVersion(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/PDDocument;->getDocument()Lorg/apache/pdfbox/cos/COSDocument;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/pdfbox/cos/COSDocument;->setVersion(F)V

    :goto_0
    return-void
.end method

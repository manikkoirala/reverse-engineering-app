.class public Lorg/apache/pdfbox/pdmodel/PDDocumentNameDictionary;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/pdfbox/pdmodel/common/COSObjectable;


# instance fields
.field private catalog:Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;

.field private nameDictionary:Lorg/apache/pdfbox/cos/COSDictionary;


# direct methods
.method public constructor <init>(Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;)V
    .locals 3

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;->getCOSObject()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->NAMES:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    if-eqz v0, :cond_0

    check-cast v0, Lorg/apache/pdfbox/cos/COSDictionary;

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDDocumentNameDictionary;->nameDictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    goto :goto_0

    :cond_0
    new-instance v0, Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-direct {v0}, Lorg/apache/pdfbox/cos/COSDictionary;-><init>()V

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDDocumentNameDictionary;->nameDictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;->getCOSObject()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    iget-object v2, p0, Lorg/apache/pdfbox/pdmodel/PDDocumentNameDictionary;->nameDictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    :goto_0
    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/PDDocumentNameDictionary;->catalog:Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;

    return-void
.end method

.method public constructor <init>(Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;Lorg/apache/pdfbox/cos/COSDictionary;)V
    .locals 0

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/PDDocumentNameDictionary;->catalog:Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;

    iput-object p2, p0, Lorg/apache/pdfbox/pdmodel/PDDocumentNameDictionary;->nameDictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    return-void
.end method


# virtual methods
.method public getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDDocumentNameDictionary;->nameDictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    return-object v0
.end method

.method public getCOSObject()Lorg/apache/pdfbox/cos/COSBase;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDDocumentNameDictionary;->nameDictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    return-object v0
.end method

.method public getDests()Lorg/apache/pdfbox/pdmodel/PDDestinationNameTreeNode;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDDocumentNameDictionary;->nameDictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->DESTS:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSDictionary;

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDDocumentNameDictionary;->catalog:Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;->getCOSObject()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSDictionary;

    :cond_0
    if-eqz v0, :cond_1

    new-instance v1, Lorg/apache/pdfbox/pdmodel/PDDestinationNameTreeNode;

    invoke-direct {v1, v0}, Lorg/apache/pdfbox/pdmodel/PDDestinationNameTreeNode;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    return-object v1
.end method

.method public getEmbeddedFiles()Lorg/apache/pdfbox/pdmodel/PDEmbeddedFilesNameTreeNode;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDDocumentNameDictionary;->nameDictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->EMBEDDED_FILES:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSDictionary;

    if-eqz v0, :cond_0

    new-instance v1, Lorg/apache/pdfbox/pdmodel/PDEmbeddedFilesNameTreeNode;

    invoke-direct {v1, v0}, Lorg/apache/pdfbox/pdmodel/PDEmbeddedFilesNameTreeNode;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return-object v1
.end method

.method public getJavaScript()Lorg/apache/pdfbox/pdmodel/PDJavascriptNameTreeNode;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDDocumentNameDictionary;->nameDictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->JAVA_SCRIPT:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSDictionary;

    if-eqz v0, :cond_0

    new-instance v1, Lorg/apache/pdfbox/pdmodel/PDJavascriptNameTreeNode;

    invoke-direct {v1, v0}, Lorg/apache/pdfbox/pdmodel/PDJavascriptNameTreeNode;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return-object v1
.end method

.method public setDests(Lorg/apache/pdfbox/pdmodel/PDDestinationNameTreeNode;)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDDocumentNameDictionary;->nameDictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->DESTS:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)V

    iget-object p1, p0, Lorg/apache/pdfbox/pdmodel/PDDocumentNameDictionary;->catalog:Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;->getCOSObject()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object p1

    const/4 v0, 0x0

    invoke-virtual {p1, v1, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)V

    return-void
.end method

.method public setEmbeddedFiles(Lorg/apache/pdfbox/pdmodel/PDEmbeddedFilesNameTreeNode;)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDDocumentNameDictionary;->nameDictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->EMBEDDED_FILES:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)V

    return-void
.end method

.method public setJavascript(Lorg/apache/pdfbox/pdmodel/PDJavascriptNameTreeNode;)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDDocumentNameDictionary;->nameDictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->JAVA_SCRIPT:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)V

    return-void
.end method

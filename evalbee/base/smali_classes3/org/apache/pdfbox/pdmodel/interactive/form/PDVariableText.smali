.class public abstract Lorg/apache/pdfbox/pdmodel/interactive/form/PDVariableText;
.super Lorg/apache/pdfbox/pdmodel/interactive/form/PDField;
.source "SourceFile"


# static fields
.field public static final QUADDING_CENTERED:I = 0x1

.field public static final QUADDING_LEFT:I = 0x0

.field public static final QUADDING_RIGHT:I = 0x2


# direct methods
.method public constructor <init>(Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDField;-><init>(Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;)V

    return-void
.end method

.method public constructor <init>(Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;Lorg/apache/pdfbox/cos/COSDictionary;Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;)V
    .locals 0

    .line 2
    invoke-direct {p0, p1, p2, p3}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDField;-><init>(Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;Lorg/apache/pdfbox/cos/COSDictionary;Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;)V

    return-void
.end method


# virtual methods
.method public getDefaultAppearance()Ljava/lang/String;
    .locals 1

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->DA:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p0, v0}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->getInheritableAttribute(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSString;

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSString;->getString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultStyleString()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->DS:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSString;

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSString;->getString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getQ()I
    .locals 1

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->Q:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p0, v0}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->getInheritableAttribute(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSNumber;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSNumber;->intValue()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public getRichTextValue()Ljava/lang/String;
    .locals 1

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->RV:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p0, v0}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->getInheritableAttribute(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->getAsTextStream(Lorg/apache/pdfbox/cos/COSBase;)Lorg/apache/pdfbox/pdmodel/common/PDTextStream;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/common/PDTextStream;->getAsString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, ""

    return-object v0
.end method

.method public getRichTextValueAsStream()Lorg/apache/pdfbox/pdmodel/common/PDTextStream;
    .locals 1

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->RV:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p0, v0}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->getInheritableAttribute(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->getAsTextStream(Lorg/apache/pdfbox/cos/COSBase;)Lorg/apache/pdfbox/pdmodel/common/PDTextStream;

    move-result-object v0

    return-object v0
.end method

.method public setDefaultAppearance(Ljava/lang/String;)V
    .locals 2

    if-eqz p1, :cond_0

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->DA:Lorg/apache/pdfbox/cos/COSName;

    new-instance v1, Lorg/apache/pdfbox/cos/COSString;

    invoke-direct {v1, p1}, Lorg/apache/pdfbox/cos/COSString;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->setInheritableAttribute(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    goto :goto_0

    :cond_0
    sget-object p1, Lorg/apache/pdfbox/cos/COSName;->DA:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->removeInheritableAttribute(Lorg/apache/pdfbox/cos/COSName;)V

    :goto_0
    return-void
.end method

.method public setDefaultStyleString(Ljava/lang/String;)V
    .locals 3

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->DS:Lorg/apache/pdfbox/cos/COSName;

    new-instance v2, Lorg/apache/pdfbox/cos/COSString;

    invoke-direct {v2, p1}, Lorg/apache/pdfbox/cos/COSString;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object p1

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->DS:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p1, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->removeItem(Lorg/apache/pdfbox/cos/COSName;)V

    :goto_0
    return-void
.end method

.method public setQ(I)V
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->Q:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setInt(Lorg/apache/pdfbox/cos/COSName;I)V

    return-void
.end method

.method public setRichTextValue(Ljava/lang/String;)V
    .locals 3

    .line 1
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->RV:Lorg/apache/pdfbox/cos/COSName;

    new-instance v2, Lorg/apache/pdfbox/cos/COSString;

    invoke-direct {v2, p1}, Lorg/apache/pdfbox/cos/COSString;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object p1

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->RV:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p1, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->removeItem(Lorg/apache/pdfbox/cos/COSName;)V

    :goto_0
    return-void
.end method

.method public setRichTextValue(Lorg/apache/pdfbox/pdmodel/common/PDTextStream;)V
    .locals 2

    .line 2
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->RV:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/common/PDTextStream;->getCOSObject()Lorg/apache/pdfbox/cos/COSBase;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object p1

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->RV:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p1, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->removeItem(Lorg/apache/pdfbox/cos/COSName;)V

    :goto_0
    return-void
.end method

.class public Lorg/apache/pdfbox/pdmodel/interactive/measurement/PDRectlinearMeasureDictionary;
.super Lorg/apache/pdfbox/pdmodel/interactive/measurement/PDMeasureDictionary;
.source "SourceFile"


# static fields
.field public static final SUBTYPE:Ljava/lang/String; = "RL"


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/interactive/measurement/PDMeasureDictionary;-><init>()V

    const-string v0, "RL"

    invoke-virtual {p0, v0}, Lorg/apache/pdfbox/pdmodel/interactive/measurement/PDMeasureDictionary;->setSubtype(Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Lorg/apache/pdfbox/cos/COSDictionary;)V
    .locals 0

    .line 2
    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/interactive/measurement/PDMeasureDictionary;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    return-void
.end method


# virtual methods
.method public getAngles()[Lorg/apache/pdfbox/pdmodel/interactive/measurement/PDNumberFormatDictionary;
    .locals 5

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/measurement/PDMeasureDictionary;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    const-string v1, "T"

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Ljava/lang/String;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSArray;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSArray;->size()I

    move-result v1

    new-array v1, v1, [Lorg/apache/pdfbox/pdmodel/interactive/measurement/PDNumberFormatDictionary;

    const/4 v2, 0x0

    :goto_0
    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSArray;->size()I

    move-result v3

    if-ge v2, v3, :cond_0

    invoke-virtual {v0, v2}, Lorg/apache/pdfbox/cos/COSArray;->get(I)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v3

    check-cast v3, Lorg/apache/pdfbox/cos/COSDictionary;

    new-instance v4, Lorg/apache/pdfbox/pdmodel/interactive/measurement/PDNumberFormatDictionary;

    invoke-direct {v4, v3}, Lorg/apache/pdfbox/pdmodel/interactive/measurement/PDNumberFormatDictionary;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    aput-object v4, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    return-object v1

    :cond_1
    const/4 v0, 0x0

    return-object v0
.end method

.method public getAreas()[Lorg/apache/pdfbox/pdmodel/interactive/measurement/PDNumberFormatDictionary;
    .locals 5

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/measurement/PDMeasureDictionary;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->A:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSArray;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSArray;->size()I

    move-result v1

    new-array v1, v1, [Lorg/apache/pdfbox/pdmodel/interactive/measurement/PDNumberFormatDictionary;

    const/4 v2, 0x0

    :goto_0
    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSArray;->size()I

    move-result v3

    if-ge v2, v3, :cond_0

    invoke-virtual {v0, v2}, Lorg/apache/pdfbox/cos/COSArray;->get(I)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v3

    check-cast v3, Lorg/apache/pdfbox/cos/COSDictionary;

    new-instance v4, Lorg/apache/pdfbox/pdmodel/interactive/measurement/PDNumberFormatDictionary;

    invoke-direct {v4, v3}, Lorg/apache/pdfbox/pdmodel/interactive/measurement/PDNumberFormatDictionary;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    aput-object v4, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    return-object v1

    :cond_1
    const/4 v0, 0x0

    return-object v0
.end method

.method public getCYX()F
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/measurement/PDMeasureDictionary;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    const-string v1, "CYX"

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getFloat(Ljava/lang/String;)F

    move-result v0

    return v0
.end method

.method public getChangeXs()[Lorg/apache/pdfbox/pdmodel/interactive/measurement/PDNumberFormatDictionary;
    .locals 5

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/measurement/PDMeasureDictionary;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    const-string v1, "X"

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Ljava/lang/String;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSArray;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSArray;->size()I

    move-result v1

    new-array v1, v1, [Lorg/apache/pdfbox/pdmodel/interactive/measurement/PDNumberFormatDictionary;

    const/4 v2, 0x0

    :goto_0
    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSArray;->size()I

    move-result v3

    if-ge v2, v3, :cond_0

    invoke-virtual {v0, v2}, Lorg/apache/pdfbox/cos/COSArray;->get(I)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v3

    check-cast v3, Lorg/apache/pdfbox/cos/COSDictionary;

    new-instance v4, Lorg/apache/pdfbox/pdmodel/interactive/measurement/PDNumberFormatDictionary;

    invoke-direct {v4, v3}, Lorg/apache/pdfbox/pdmodel/interactive/measurement/PDNumberFormatDictionary;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    aput-object v4, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    return-object v1

    :cond_1
    const/4 v0, 0x0

    return-object v0
.end method

.method public getChangeYs()[Lorg/apache/pdfbox/pdmodel/interactive/measurement/PDNumberFormatDictionary;
    .locals 5

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/measurement/PDMeasureDictionary;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    const-string v1, "Y"

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Ljava/lang/String;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSArray;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSArray;->size()I

    move-result v1

    new-array v1, v1, [Lorg/apache/pdfbox/pdmodel/interactive/measurement/PDNumberFormatDictionary;

    const/4 v2, 0x0

    :goto_0
    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSArray;->size()I

    move-result v3

    if-ge v2, v3, :cond_0

    invoke-virtual {v0, v2}, Lorg/apache/pdfbox/cos/COSArray;->get(I)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v3

    check-cast v3, Lorg/apache/pdfbox/cos/COSDictionary;

    new-instance v4, Lorg/apache/pdfbox/pdmodel/interactive/measurement/PDNumberFormatDictionary;

    invoke-direct {v4, v3}, Lorg/apache/pdfbox/pdmodel/interactive/measurement/PDNumberFormatDictionary;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    aput-object v4, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    return-object v1

    :cond_1
    const/4 v0, 0x0

    return-object v0
.end method

.method public getCoordSystemOrigin()[F
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/measurement/PDMeasureDictionary;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    const-string v1, "O"

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Ljava/lang/String;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSArray;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSArray;->toFloatArray()[F

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public getDistances()[Lorg/apache/pdfbox/pdmodel/interactive/measurement/PDNumberFormatDictionary;
    .locals 5

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/measurement/PDMeasureDictionary;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    const-string v1, "D"

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Ljava/lang/String;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSArray;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSArray;->size()I

    move-result v1

    new-array v1, v1, [Lorg/apache/pdfbox/pdmodel/interactive/measurement/PDNumberFormatDictionary;

    const/4 v2, 0x0

    :goto_0
    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSArray;->size()I

    move-result v3

    if-ge v2, v3, :cond_0

    invoke-virtual {v0, v2}, Lorg/apache/pdfbox/cos/COSArray;->get(I)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v3

    check-cast v3, Lorg/apache/pdfbox/cos/COSDictionary;

    new-instance v4, Lorg/apache/pdfbox/pdmodel/interactive/measurement/PDNumberFormatDictionary;

    invoke-direct {v4, v3}, Lorg/apache/pdfbox/pdmodel/interactive/measurement/PDNumberFormatDictionary;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    aput-object v4, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    return-object v1

    :cond_1
    const/4 v0, 0x0

    return-object v0
.end method

.method public getLineSloaps()[Lorg/apache/pdfbox/pdmodel/interactive/measurement/PDNumberFormatDictionary;
    .locals 5

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/measurement/PDMeasureDictionary;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    const-string v1, "S"

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Ljava/lang/String;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSArray;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSArray;->size()I

    move-result v1

    new-array v1, v1, [Lorg/apache/pdfbox/pdmodel/interactive/measurement/PDNumberFormatDictionary;

    const/4 v2, 0x0

    :goto_0
    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSArray;->size()I

    move-result v3

    if-ge v2, v3, :cond_0

    invoke-virtual {v0, v2}, Lorg/apache/pdfbox/cos/COSArray;->get(I)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v3

    check-cast v3, Lorg/apache/pdfbox/cos/COSDictionary;

    new-instance v4, Lorg/apache/pdfbox/pdmodel/interactive/measurement/PDNumberFormatDictionary;

    invoke-direct {v4, v3}, Lorg/apache/pdfbox/pdmodel/interactive/measurement/PDNumberFormatDictionary;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    aput-object v4, v1, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    return-object v1

    :cond_1
    const/4 v0, 0x0

    return-object v0
.end method

.method public getScaleRatio()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/measurement/PDMeasureDictionary;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->R:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getString(Lorg/apache/pdfbox/cos/COSName;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public setAngles([Lorg/apache/pdfbox/pdmodel/interactive/measurement/PDNumberFormatDictionary;)V
    .locals 4

    new-instance v0, Lorg/apache/pdfbox/cos/COSArray;

    invoke-direct {v0}, Lorg/apache/pdfbox/cos/COSArray;-><init>()V

    array-length v1, p1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    aget-object v3, p1, v2

    invoke-virtual {v0, v3}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/measurement/PDMeasureDictionary;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object p1

    const-string v1, "T"

    invoke-virtual {p1, v1, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Ljava/lang/String;Lorg/apache/pdfbox/cos/COSBase;)V

    return-void
.end method

.method public setAreas([Lorg/apache/pdfbox/pdmodel/interactive/measurement/PDNumberFormatDictionary;)V
    .locals 4

    new-instance v0, Lorg/apache/pdfbox/cos/COSArray;

    invoke-direct {v0}, Lorg/apache/pdfbox/cos/COSArray;-><init>()V

    array-length v1, p1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    aget-object v3, p1, v2

    invoke-virtual {v0, v3}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/measurement/PDMeasureDictionary;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object p1

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->A:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p1, v1, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    return-void
.end method

.method public setCYX(F)V
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/measurement/PDMeasureDictionary;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    const-string v1, "CYX"

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setFloat(Ljava/lang/String;F)V

    return-void
.end method

.method public setChangeXs([Lorg/apache/pdfbox/pdmodel/interactive/measurement/PDNumberFormatDictionary;)V
    .locals 4

    new-instance v0, Lorg/apache/pdfbox/cos/COSArray;

    invoke-direct {v0}, Lorg/apache/pdfbox/cos/COSArray;-><init>()V

    array-length v1, p1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    aget-object v3, p1, v2

    invoke-virtual {v0, v3}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/measurement/PDMeasureDictionary;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object p1

    const-string v1, "X"

    invoke-virtual {p1, v1, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Ljava/lang/String;Lorg/apache/pdfbox/cos/COSBase;)V

    return-void
.end method

.method public setChangeYs([Lorg/apache/pdfbox/pdmodel/interactive/measurement/PDNumberFormatDictionary;)V
    .locals 4

    new-instance v0, Lorg/apache/pdfbox/cos/COSArray;

    invoke-direct {v0}, Lorg/apache/pdfbox/cos/COSArray;-><init>()V

    array-length v1, p1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    aget-object v3, p1, v2

    invoke-virtual {v0, v3}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/measurement/PDMeasureDictionary;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object p1

    const-string v1, "Y"

    invoke-virtual {p1, v1, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Ljava/lang/String;Lorg/apache/pdfbox/cos/COSBase;)V

    return-void
.end method

.method public setCoordSystemOrigin([F)V
    .locals 2

    new-instance v0, Lorg/apache/pdfbox/cos/COSArray;

    invoke-direct {v0}, Lorg/apache/pdfbox/cos/COSArray;-><init>()V

    invoke-virtual {v0, p1}, Lorg/apache/pdfbox/cos/COSArray;->setFloatArray([F)V

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/measurement/PDMeasureDictionary;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object p1

    const-string v1, "O"

    invoke-virtual {p1, v1, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Ljava/lang/String;Lorg/apache/pdfbox/cos/COSBase;)V

    return-void
.end method

.method public setDistances([Lorg/apache/pdfbox/pdmodel/interactive/measurement/PDNumberFormatDictionary;)V
    .locals 4

    new-instance v0, Lorg/apache/pdfbox/cos/COSArray;

    invoke-direct {v0}, Lorg/apache/pdfbox/cos/COSArray;-><init>()V

    array-length v1, p1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    aget-object v3, p1, v2

    invoke-virtual {v0, v3}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/measurement/PDMeasureDictionary;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object p1

    const-string v1, "D"

    invoke-virtual {p1, v1, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Ljava/lang/String;Lorg/apache/pdfbox/cos/COSBase;)V

    return-void
.end method

.method public setLineSloaps([Lorg/apache/pdfbox/pdmodel/interactive/measurement/PDNumberFormatDictionary;)V
    .locals 4

    new-instance v0, Lorg/apache/pdfbox/cos/COSArray;

    invoke-direct {v0}, Lorg/apache/pdfbox/cos/COSArray;-><init>()V

    array-length v1, p1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    aget-object v3, p1, v2

    invoke-virtual {v0, v3}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/measurement/PDMeasureDictionary;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object p1

    const-string v1, "S"

    invoke-virtual {p1, v1, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Ljava/lang/String;Lorg/apache/pdfbox/cos/COSBase;)V

    return-void
.end method

.method public setScaleRatio(Ljava/lang/String;)V
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/measurement/PDMeasureDictionary;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->R:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setString(Lorg/apache/pdfbox/cos/COSName;Ljava/lang/String;)V

    return-void
.end method

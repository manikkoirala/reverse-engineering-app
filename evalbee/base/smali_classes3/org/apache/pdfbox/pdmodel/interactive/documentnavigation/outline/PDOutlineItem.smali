.class public final Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineItem;
.super Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineNode;
.source "SourceFile"


# static fields
.field private static final BOLD_FLAG:I = 0x2

.field private static final ITALIC_FLAG:I = 0x1


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineNode;-><init>()V

    return-void
.end method

.method public constructor <init>(Lorg/apache/pdfbox/cos/COSDictionary;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineNode;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    return-void
.end method


# virtual methods
.method public findDestinationPage(Lorg/apache/pdfbox/pdmodel/PDDocument;)Lorg/apache/pdfbox/pdmodel/PDPage;
    .locals 3

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineItem;->getDestination()Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/destination/PDDestination;

    move-result-object v0

    const/4 v1, 0x0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineItem;->getAction()Lorg/apache/pdfbox/pdmodel/interactive/action/PDAction;

    move-result-object v0

    instance-of v2, v0, Lorg/apache/pdfbox/pdmodel/interactive/action/PDActionGoTo;

    if-eqz v2, :cond_0

    check-cast v0, Lorg/apache/pdfbox/pdmodel/interactive/action/PDActionGoTo;

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/interactive/action/PDActionGoTo;->getDestination()Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/destination/PDDestination;

    move-result-object v0

    goto :goto_0

    :cond_0
    return-object v1

    :cond_1
    :goto_0
    instance-of v2, v0, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/destination/PDNamedDestination;

    if-eqz v2, :cond_3

    check-cast v0, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/destination/PDNamedDestination;

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/PDDocument;->getDocumentCatalog()Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;->getNames()Lorg/apache/pdfbox/pdmodel/PDDocumentNameDictionary;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Lorg/apache/pdfbox/pdmodel/PDDocumentNameDictionary;->getDests()Lorg/apache/pdfbox/pdmodel/PDDestinationNameTreeNode;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/destination/PDNamedDestination;->getNamedDestination()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lorg/apache/pdfbox/pdmodel/common/PDNameTreeNode;->getValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_1

    :cond_2
    return-object v1

    :cond_3
    instance-of v2, v0, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/destination/PDPageDestination;

    if-eqz v2, :cond_5

    :goto_1
    check-cast v0, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/destination/PDPageDestination;

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/destination/PDPageDestination;->getPage()Lorg/apache/pdfbox/pdmodel/PDPage;

    move-result-object v1

    if-nez v1, :cond_4

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/destination/PDPageDestination;->getPageNumber()I

    move-result v0

    const/4 v2, -0x1

    if-eq v0, v2, :cond_4

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p1, v0}, Lorg/apache/pdfbox/pdmodel/PDDocument;->getPage(I)Lorg/apache/pdfbox/pdmodel/PDPage;

    move-result-object v1

    :cond_4
    return-object v1

    :cond_5
    if-nez v0, :cond_6

    return-object v1

    :cond_6
    new-instance p1, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Error: Unknown destination type "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public getAction()Lorg/apache/pdfbox/pdmodel/interactive/action/PDAction;
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/common/PDDictionaryWrapper;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->A:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-static {v0}, Lorg/apache/pdfbox/pdmodel/interactive/action/PDActionFactory;->createAction(Lorg/apache/pdfbox/cos/COSDictionary;)Lorg/apache/pdfbox/pdmodel/interactive/action/PDAction;

    move-result-object v0

    return-object v0
.end method

.method public getDestination()Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/destination/PDDestination;
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/common/PDDictionaryWrapper;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->DEST:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/destination/PDDestination;->create(Lorg/apache/pdfbox/cos/COSBase;)Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/destination/PDDestination;

    move-result-object v0

    return-object v0
.end method

.method public getNextSibling()Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineItem;
    .locals 1

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->NEXT:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p0, v0}, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineNode;->getOutlineItem(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineItem;

    move-result-object v0

    return-object v0
.end method

.method public getPreviousSibling()Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineItem;
    .locals 1

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->PREV:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p0, v0}, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineNode;->getOutlineItem(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineItem;

    move-result-object v0

    return-object v0
.end method

.method public getStructureElement()Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureElement;
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/common/PDDictionaryWrapper;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->SE:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSDictionary;

    if-eqz v0, :cond_0

    new-instance v1, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureElement;

    invoke-direct {v1, v0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureElement;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return-object v1
.end method

.method public getTextColor()Lorg/apache/pdfbox/pdmodel/graphics/color/PDColor;
    .locals 4

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/common/PDDictionaryWrapper;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->C:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSArray;

    if-nez v0, :cond_0

    new-instance v0, Lorg/apache/pdfbox/cos/COSArray;

    invoke-direct {v0}, Lorg/apache/pdfbox/cos/COSArray;-><init>()V

    new-instance v2, Lorg/apache/pdfbox/cos/COSFloat;

    const/4 v3, 0x0

    invoke-direct {v2, v3}, Lorg/apache/pdfbox/cos/COSFloat;-><init>(F)V

    const/4 v3, 0x3

    invoke-virtual {v0, v3, v2}, Lorg/apache/pdfbox/cos/COSArray;->growToSize(ILorg/apache/pdfbox/cos/COSBase;)V

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/common/PDDictionaryWrapper;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v2

    invoke-virtual {v2, v1, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    :cond_0
    new-instance v1, Lorg/apache/pdfbox/pdmodel/graphics/color/PDColor;

    sget-object v2, Lorg/apache/pdfbox/pdmodel/graphics/color/PDDeviceRGB;->INSTANCE:Lorg/apache/pdfbox/pdmodel/graphics/color/PDDeviceRGB;

    invoke-direct {v1, v0, v2}, Lorg/apache/pdfbox/pdmodel/graphics/color/PDColor;-><init>(Lorg/apache/pdfbox/cos/COSArray;Lorg/apache/pdfbox/pdmodel/graphics/color/PDColorSpace;)V

    return-object v1
.end method

.method public getTitle()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/common/PDDictionaryWrapper;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->TITLE:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getString(Lorg/apache/pdfbox/cos/COSName;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public insertSiblingAfter(Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineItem;)V
    .locals 2

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineNode;->requireSingleNode(Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineItem;)V

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineNode;->getParent()Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineNode;

    move-result-object v0

    invoke-virtual {p1, v0}, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineNode;->setParent(Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineNode;)V

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineItem;->getNextSibling()Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineItem;

    move-result-object v1

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineItem;->setNextSibling(Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineNode;)V

    invoke-virtual {p1, p0}, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineItem;->setPreviousSibling(Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineNode;)V

    if-eqz v1, :cond_0

    invoke-virtual {p1, v1}, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineItem;->setNextSibling(Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineNode;)V

    invoke-virtual {v1, p1}, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineItem;->setPreviousSibling(Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineNode;)V

    goto :goto_0

    :cond_0
    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineNode;->getParent()Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineNode;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineNode;->setLastChild(Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineNode;)V

    :cond_1
    :goto_0
    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineNode;->updateParentOpenCountForAddedChild(Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineItem;)V

    return-void
.end method

.method public insertSiblingBefore(Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineItem;)V
    .locals 2

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineNode;->requireSingleNode(Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineItem;)V

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineNode;->getParent()Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineNode;

    move-result-object v0

    invoke-virtual {p1, v0}, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineNode;->setParent(Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineNode;)V

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineItem;->getPreviousSibling()Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineItem;

    move-result-object v1

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineItem;->setPreviousSibling(Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineNode;)V

    invoke-virtual {p1, p0}, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineItem;->setNextSibling(Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineNode;)V

    if-eqz v1, :cond_0

    invoke-virtual {v1, p1}, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineItem;->setNextSibling(Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineNode;)V

    invoke-virtual {p1, v1}, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineItem;->setPreviousSibling(Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineNode;)V

    goto :goto_0

    :cond_0
    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineNode;->getParent()Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineNode;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineNode;->setFirstChild(Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineNode;)V

    :cond_1
    :goto_0
    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineNode;->updateParentOpenCountForAddedChild(Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineItem;)V

    return-void
.end method

.method public isBold()Z
    .locals 3

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/common/PDDictionaryWrapper;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->F:Lorg/apache/pdfbox/cos/COSName;

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->getFlag(Lorg/apache/pdfbox/cos/COSName;I)Z

    move-result v0

    return v0
.end method

.method public isItalic()Z
    .locals 3

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/common/PDDictionaryWrapper;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->F:Lorg/apache/pdfbox/cos/COSName;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->getFlag(Lorg/apache/pdfbox/cos/COSName;I)Z

    move-result v0

    return v0
.end method

.method public setAction(Lorg/apache/pdfbox/pdmodel/interactive/action/PDAction;)V
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/common/PDDictionaryWrapper;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->A:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)V

    return-void
.end method

.method public setBold(Z)V
    .locals 3

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/common/PDDictionaryWrapper;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->F:Lorg/apache/pdfbox/cos/COSName;

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setFlag(Lorg/apache/pdfbox/cos/COSName;IZ)V

    return-void
.end method

.method public setDestination(Lorg/apache/pdfbox/pdmodel/PDPage;)V
    .locals 1

    .line 1
    if-eqz p1, :cond_0

    new-instance v0, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/destination/PDPageXYZDestination;

    invoke-direct {v0}, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/destination/PDPageXYZDestination;-><init>()V

    invoke-virtual {v0, p1}, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/destination/PDPageDestination;->setPage(Lorg/apache/pdfbox/pdmodel/PDPage;)V

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, v0}, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineItem;->setDestination(Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/destination/PDDestination;)V

    return-void
.end method

.method public setDestination(Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/destination/PDDestination;)V
    .locals 2

    .line 2
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/common/PDDictionaryWrapper;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->DEST:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)V

    return-void
.end method

.method public setItalic(Z)V
    .locals 3

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/common/PDDictionaryWrapper;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->F:Lorg/apache/pdfbox/cos/COSName;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setFlag(Lorg/apache/pdfbox/cos/COSName;IZ)V

    return-void
.end method

.method public setNextSibling(Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineNode;)V
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/common/PDDictionaryWrapper;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->NEXT:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)V

    return-void
.end method

.method public setPreviousSibling(Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineNode;)V
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/common/PDDictionaryWrapper;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->PREV:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)V

    return-void
.end method

.method public setStructuredElement(Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureElement;)V
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/common/PDDictionaryWrapper;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->SE:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)V

    return-void
.end method

.method public setTextColor(Lorg/apache/pdfbox/pdmodel/graphics/color/PDColor;)V
    .locals 2

    .line 1
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/common/PDDictionaryWrapper;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->C:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/graphics/color/PDColor;->toCOSArray()Lorg/apache/pdfbox/cos/COSArray;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    return-void
.end method

.method public setTextColor(Lorg/apache/pdfbox/util/awt/AWTColor;)V
    .locals 4

    .line 2
    new-instance v0, Lorg/apache/pdfbox/cos/COSArray;

    invoke-direct {v0}, Lorg/apache/pdfbox/cos/COSArray;-><init>()V

    new-instance v1, Lorg/apache/pdfbox/cos/COSFloat;

    invoke-virtual {p1}, Lorg/apache/pdfbox/util/awt/AWTColor;->getRed()I

    move-result v2

    int-to-float v2, v2

    const/high16 v3, 0x437f0000    # 255.0f

    div-float/2addr v2, v3

    invoke-direct {v1, v2}, Lorg/apache/pdfbox/cos/COSFloat;-><init>(F)V

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    new-instance v1, Lorg/apache/pdfbox/cos/COSFloat;

    invoke-virtual {p1}, Lorg/apache/pdfbox/util/awt/AWTColor;->getGreen()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v3

    invoke-direct {v1, v2}, Lorg/apache/pdfbox/cos/COSFloat;-><init>(F)V

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    new-instance v1, Lorg/apache/pdfbox/cos/COSFloat;

    invoke-virtual {p1}, Lorg/apache/pdfbox/util/awt/AWTColor;->getBlue()I

    move-result p1

    int-to-float p1, p1

    div-float/2addr p1, v3

    invoke-direct {v1, p1}, Lorg/apache/pdfbox/cos/COSFloat;-><init>(F)V

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/common/PDDictionaryWrapper;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object p1

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->C:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p1, v1, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/common/PDDictionaryWrapper;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->TITLE:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setString(Lorg/apache/pdfbox/cos/COSName;Ljava/lang/String;)V

    return-void
.end method

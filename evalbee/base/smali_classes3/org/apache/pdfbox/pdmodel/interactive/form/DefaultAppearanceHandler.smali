.class Lorg/apache/pdfbox/pdmodel/interactive/form/DefaultAppearanceHandler;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private appearanceTokens:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/interactive/form/DefaultAppearanceHandler;->getStreamTokens(Ljava/lang/String;)Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/DefaultAppearanceHandler;->appearanceTokens:Ljava/util/List;

    return-void
.end method

.method private getStreamTokens(Ljava/lang/String;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v0, Ljava/io/ByteArrayInputStream;

    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    new-instance p1, Lorg/apache/pdfbox/pdfparser/PDFStreamParser;

    invoke-direct {p1, v0}, Lorg/apache/pdfbox/pdfparser/PDFStreamParser;-><init>(Ljava/io/InputStream;)V

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdfparser/PDFStreamParser;->parse()V

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdfparser/PDFStreamParser;->getTokens()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdfparser/BaseParser;->close()V

    :cond_0
    return-object v0
.end method


# virtual methods
.method public getFontName()Lorg/apache/pdfbox/cos/COSName;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/DefaultAppearanceHandler;->appearanceTokens:Ljava/util/List;

    const-string v1, "Tf"

    invoke-static {v1}, Lorg/apache/pdfbox/contentstream/operator/Operator;->getOperator(Ljava/lang/String;)Lorg/apache/pdfbox/contentstream/operator/Operator;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    iget-object v1, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/DefaultAppearanceHandler;->appearanceTokens:Ljava/util/List;

    add-int/lit8 v0, v0, -0x2

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSName;

    return-object v0
.end method

.method public getFontSize()F
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/DefaultAppearanceHandler;->appearanceTokens:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/DefaultAppearanceHandler;->appearanceTokens:Ljava/util/List;

    const-string v1, "Tf"

    invoke-static {v1}, Lorg/apache/pdfbox/contentstream/operator/Operator;->getOperator(Ljava/lang/String;)Lorg/apache/pdfbox/contentstream/operator/Operator;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget-object v1, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/DefaultAppearanceHandler;->appearanceTokens:Ljava/util/List;

    add-int/lit8 v0, v0, -0x1

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSNumber;

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSNumber;->floatValue()F

    move-result v0

    return v0

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public getTokens()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/DefaultAppearanceHandler;->appearanceTokens:Ljava/util/List;

    return-object v0
.end method

.method public setFontSize(F)V
    .locals 3

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/DefaultAppearanceHandler;->appearanceTokens:Ljava/util/List;

    const-string v1, "Tf"

    invoke-static {v1}, Lorg/apache/pdfbox/contentstream/operator/Operator;->getOperator(Ljava/lang/String;)Lorg/apache/pdfbox/contentstream/operator/Operator;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget-object v1, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/DefaultAppearanceHandler;->appearanceTokens:Ljava/util/List;

    add-int/lit8 v0, v0, -0x1

    new-instance v2, Lorg/apache/pdfbox/cos/COSFloat;

    invoke-direct {v2, p1}, Lorg/apache/pdfbox/cos/COSFloat;-><init>(F)V

    invoke-interface {v1, v0, v2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-void
.end method

.class public Lorg/apache/pdfbox/pdmodel/interactive/form/PDNonTerminalField;
.super Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;
.source "SourceFile"


# direct methods
.method public constructor <init>(Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;-><init>(Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;)V

    return-void
.end method

.method public constructor <init>(Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;Lorg/apache/pdfbox/cos/COSDictionary;Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;)V
    .locals 0

    .line 2
    invoke-direct {p0, p1, p2, p3}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;-><init>(Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;Lorg/apache/pdfbox/cos/COSDictionary;Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;)V

    return-void
.end method


# virtual methods
.method public getDefaultValue()Ljava/lang/Object;
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->V:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getNameAsString(Lorg/apache/pdfbox/cos/COSName;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getFieldFlags()I
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->FF:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSInteger;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSInteger;->intValue()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public getFieldType()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->FT:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getNameAsString(Lorg/apache/pdfbox/cos/COSName;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getValue()Ljava/lang/Object;
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->V:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getNameAsString(Lorg/apache/pdfbox/cos/COSName;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public setDefaultValue(Ljava/lang/String;)V
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->V:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setString(Lorg/apache/pdfbox/cos/COSName;Ljava/lang/String;)V

    return-void
.end method

.method public setValue(Ljava/lang/String;)V
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->V:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setString(Lorg/apache/pdfbox/cos/COSName;Ljava/lang/String;)V

    return-void
.end method

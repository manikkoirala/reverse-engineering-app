.class public abstract Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/destination/PDPageDestination;
.super Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/destination/PDDestination;
.source "SourceFile"


# instance fields
.field protected array:Lorg/apache/pdfbox/cos/COSArray;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/destination/PDDestination;-><init>()V

    new-instance v0, Lorg/apache/pdfbox/cos/COSArray;

    invoke-direct {v0}, Lorg/apache/pdfbox/cos/COSArray;-><init>()V

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/destination/PDPageDestination;->array:Lorg/apache/pdfbox/cos/COSArray;

    return-void
.end method

.method public constructor <init>(Lorg/apache/pdfbox/cos/COSArray;)V
    .locals 0

    .line 2
    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/destination/PDDestination;-><init>()V

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/destination/PDPageDestination;->array:Lorg/apache/pdfbox/cos/COSArray;

    return-void
.end method


# virtual methods
.method public findPageNumber()I
    .locals 5

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/destination/PDPageDestination;->array:Lorg/apache/pdfbox/cos/COSArray;

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSArray;->size()I

    move-result v0

    if-lez v0, :cond_2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/destination/PDPageDestination;->array:Lorg/apache/pdfbox/cos/COSArray;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSArray;->getObject(I)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    instance-of v1, v0, Lorg/apache/pdfbox/cos/COSNumber;

    if-eqz v1, :cond_0

    check-cast v0, Lorg/apache/pdfbox/cos/COSNumber;

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSNumber;->intValue()I

    move-result v0

    goto :goto_1

    :cond_0
    instance-of v1, v0, Lorg/apache/pdfbox/cos/COSDictionary;

    if-eqz v1, :cond_2

    move-object v1, v0

    :goto_0
    check-cast v1, Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v2, Lorg/apache/pdfbox/cos/COSName;->PARENT:Lorg/apache/pdfbox/cos/COSName;

    sget-object v3, Lorg/apache/pdfbox/cos/COSName;->P:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v1, v2, v3}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-virtual {v1, v2, v3}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v1

    goto :goto_0

    :cond_1
    new-instance v2, Lorg/apache/pdfbox/pdmodel/PDPageTree;

    invoke-direct {v2, v1}, Lorg/apache/pdfbox/pdmodel/PDPageTree;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    new-instance v1, Lorg/apache/pdfbox/pdmodel/PDPage;

    check-cast v0, Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-direct {v1, v0}, Lorg/apache/pdfbox/pdmodel/PDPage;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    invoke-virtual {v2, v1}, Lorg/apache/pdfbox/pdmodel/PDPageTree;->indexOf(Lorg/apache/pdfbox/pdmodel/PDPage;)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    const/4 v0, -0x1

    :goto_1
    return v0
.end method

.method public getCOSArray()Lorg/apache/pdfbox/cos/COSArray;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/destination/PDPageDestination;->array:Lorg/apache/pdfbox/cos/COSArray;

    return-object v0
.end method

.method public getCOSObject()Lorg/apache/pdfbox/cos/COSBase;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/destination/PDPageDestination;->array:Lorg/apache/pdfbox/cos/COSArray;

    return-object v0
.end method

.method public getPage()Lorg/apache/pdfbox/pdmodel/PDPage;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/destination/PDPageDestination;->array:Lorg/apache/pdfbox/cos/COSArray;

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSArray;->size()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/destination/PDPageDestination;->array:Lorg/apache/pdfbox/cos/COSArray;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSArray;->getObject(I)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    instance-of v1, v0, Lorg/apache/pdfbox/cos/COSDictionary;

    if-eqz v1, :cond_0

    new-instance v1, Lorg/apache/pdfbox/pdmodel/PDPage;

    check-cast v0, Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-direct {v1, v0}, Lorg/apache/pdfbox/pdmodel/PDPage;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return-object v1
.end method

.method public getPageNumber()I
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/destination/PDPageDestination;->array:Lorg/apache/pdfbox/cos/COSArray;

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSArray;->size()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/destination/PDPageDestination;->array:Lorg/apache/pdfbox/cos/COSArray;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSArray;->getObject(I)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    instance-of v1, v0, Lorg/apache/pdfbox/cos/COSNumber;

    if-eqz v1, :cond_0

    check-cast v0, Lorg/apache/pdfbox/cos/COSNumber;

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSNumber;->intValue()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, -0x1

    :goto_0
    return v0
.end method

.method public setPage(Lorg/apache/pdfbox/pdmodel/PDPage;)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/destination/PDPageDestination;->array:Lorg/apache/pdfbox/cos/COSArray;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSArray;->set(ILorg/apache/pdfbox/pdmodel/common/COSObjectable;)V

    return-void
.end method

.method public setPageNumber(I)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/destination/PDPageDestination;->array:Lorg/apache/pdfbox/cos/COSArray;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSArray;->set(II)V

    return-void
.end method

.class public final enum Lorg/apache/pdfbox/pdmodel/interactive/viewerpreferences/PDViewerPreferences$DUPLEX;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/pdfbox/pdmodel/interactive/viewerpreferences/PDViewerPreferences;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "DUPLEX"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lorg/apache/pdfbox/pdmodel/interactive/viewerpreferences/PDViewerPreferences$DUPLEX;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lorg/apache/pdfbox/pdmodel/interactive/viewerpreferences/PDViewerPreferences$DUPLEX;

.field public static final enum DuplexFlipLongEdge:Lorg/apache/pdfbox/pdmodel/interactive/viewerpreferences/PDViewerPreferences$DUPLEX;

.field public static final enum DuplexFlipShortEdge:Lorg/apache/pdfbox/pdmodel/interactive/viewerpreferences/PDViewerPreferences$DUPLEX;

.field public static final enum Simplex:Lorg/apache/pdfbox/pdmodel/interactive/viewerpreferences/PDViewerPreferences$DUPLEX;


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    new-instance v0, Lorg/apache/pdfbox/pdmodel/interactive/viewerpreferences/PDViewerPreferences$DUPLEX;

    const-string v1, "Simplex"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lorg/apache/pdfbox/pdmodel/interactive/viewerpreferences/PDViewerPreferences$DUPLEX;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/pdfbox/pdmodel/interactive/viewerpreferences/PDViewerPreferences$DUPLEX;->Simplex:Lorg/apache/pdfbox/pdmodel/interactive/viewerpreferences/PDViewerPreferences$DUPLEX;

    new-instance v1, Lorg/apache/pdfbox/pdmodel/interactive/viewerpreferences/PDViewerPreferences$DUPLEX;

    const-string v2, "DuplexFlipShortEdge"

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Lorg/apache/pdfbox/pdmodel/interactive/viewerpreferences/PDViewerPreferences$DUPLEX;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lorg/apache/pdfbox/pdmodel/interactive/viewerpreferences/PDViewerPreferences$DUPLEX;->DuplexFlipShortEdge:Lorg/apache/pdfbox/pdmodel/interactive/viewerpreferences/PDViewerPreferences$DUPLEX;

    new-instance v2, Lorg/apache/pdfbox/pdmodel/interactive/viewerpreferences/PDViewerPreferences$DUPLEX;

    const-string v3, "DuplexFlipLongEdge"

    const/4 v4, 0x2

    invoke-direct {v2, v3, v4}, Lorg/apache/pdfbox/pdmodel/interactive/viewerpreferences/PDViewerPreferences$DUPLEX;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lorg/apache/pdfbox/pdmodel/interactive/viewerpreferences/PDViewerPreferences$DUPLEX;->DuplexFlipLongEdge:Lorg/apache/pdfbox/pdmodel/interactive/viewerpreferences/PDViewerPreferences$DUPLEX;

    filled-new-array {v0, v1, v2}, [Lorg/apache/pdfbox/pdmodel/interactive/viewerpreferences/PDViewerPreferences$DUPLEX;

    move-result-object v0

    sput-object v0, Lorg/apache/pdfbox/pdmodel/interactive/viewerpreferences/PDViewerPreferences$DUPLEX;->$VALUES:[Lorg/apache/pdfbox/pdmodel/interactive/viewerpreferences/PDViewerPreferences$DUPLEX;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lorg/apache/pdfbox/pdmodel/interactive/viewerpreferences/PDViewerPreferences$DUPLEX;
    .locals 1

    const-class v0, Lorg/apache/pdfbox/pdmodel/interactive/viewerpreferences/PDViewerPreferences$DUPLEX;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lorg/apache/pdfbox/pdmodel/interactive/viewerpreferences/PDViewerPreferences$DUPLEX;

    return-object p0
.end method

.method public static values()[Lorg/apache/pdfbox/pdmodel/interactive/viewerpreferences/PDViewerPreferences$DUPLEX;
    .locals 1

    sget-object v0, Lorg/apache/pdfbox/pdmodel/interactive/viewerpreferences/PDViewerPreferences$DUPLEX;->$VALUES:[Lorg/apache/pdfbox/pdmodel/interactive/viewerpreferences/PDViewerPreferences$DUPLEX;

    invoke-virtual {v0}, [Lorg/apache/pdfbox/pdmodel/interactive/viewerpreferences/PDViewerPreferences$DUPLEX;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lorg/apache/pdfbox/pdmodel/interactive/viewerpreferences/PDViewerPreferences$DUPLEX;

    return-object v0
.end method

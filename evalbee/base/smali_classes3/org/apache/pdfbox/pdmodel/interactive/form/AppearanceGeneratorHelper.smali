.class Lorg/apache/pdfbox/pdmodel/interactive/form/AppearanceGeneratorHelper;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final GLYPH_TO_PDF_SCALE:F = 1000.0f


# instance fields
.field private final acroForm:Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;

.field private final defaultAppearanceHandler:Lorg/apache/pdfbox/pdmodel/interactive/form/DefaultAppearanceHandler;

.field private final parent:Lorg/apache/pdfbox/pdmodel/interactive/form/PDVariableText;

.field private value:Ljava/lang/String;

.field private widgets:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lorg/apache/pdfbox/pdmodel/common/COSObjectable;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;Lorg/apache/pdfbox/pdmodel/interactive/form/PDVariableText;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/AppearanceGeneratorHelper;->widgets:Ljava/util/List;

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/AppearanceGeneratorHelper;->acroForm:Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;

    iput-object p2, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/AppearanceGeneratorHelper;->parent:Lorg/apache/pdfbox/pdmodel/interactive/form/PDVariableText;

    invoke-virtual {p2}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->getKids()Ljava/util/List;

    move-result-object p1

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/AppearanceGeneratorHelper;->widgets:Ljava/util/List;

    if-nez p1, :cond_0

    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/AppearanceGeneratorHelper;->widgets:Ljava/util/List;

    invoke-virtual {p2}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->getWidget()Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotationWidget;

    move-result-object p2

    invoke-interface {p1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    new-instance p1, Lorg/apache/pdfbox/pdmodel/interactive/form/DefaultAppearanceHandler;

    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/interactive/form/AppearanceGeneratorHelper;->getDefaultAppearance()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Lorg/apache/pdfbox/pdmodel/interactive/form/DefaultAppearanceHandler;-><init>(Ljava/lang/String;)V

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/AppearanceGeneratorHelper;->defaultAppearanceHandler:Lorg/apache/pdfbox/pdmodel/interactive/form/DefaultAppearanceHandler;

    return-void
.end method

.method private applyPadding(Lorg/apache/pdfbox/pdmodel/common/PDRectangle;F)Lorg/apache/pdfbox/pdmodel/common/PDRectangle;
    .locals 5

    new-instance v0, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->getLowerLeftX()F

    move-result v1

    add-float/2addr v1, p2

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->getLowerLeftY()F

    move-result v2

    add-float/2addr v2, p2

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->getWidth()F

    move-result v3

    const/high16 v4, 0x40000000    # 2.0f

    mul-float/2addr p2, v4

    sub-float/2addr v3, p2

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->getHeight()F

    move-result p1

    sub-float/2addr p1, p2

    invoke-direct {v0, v1, v2, v3, p1}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;-><init>(FFFF)V

    return-object v0
.end method

.method private calculateFontSize(Lorg/apache/pdfbox/pdmodel/font/PDFont;Lorg/apache/pdfbox/pdmodel/common/PDRectangle;)F
    .locals 5

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/AppearanceGeneratorHelper;->defaultAppearanceHandler:Lorg/apache/pdfbox/pdmodel/interactive/form/DefaultAppearanceHandler;

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/interactive/form/DefaultAppearanceHandler;->getTokens()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    const/high16 v1, 0x41400000    # 12.0f

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/AppearanceGeneratorHelper;->defaultAppearanceHandler:Lorg/apache/pdfbox/pdmodel/interactive/form/DefaultAppearanceHandler;

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/interactive/form/DefaultAppearanceHandler;->getFontSize()F

    move-result v0

    goto :goto_0

    :cond_0
    move v0, v1

    :goto_0
    const/4 v2, 0x0

    cmpl-float v3, v0, v2

    if-nez v3, :cond_1

    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/interactive/form/AppearanceGeneratorHelper;->isMultiLine()Z

    move-result v3

    if-nez v3, :cond_1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/AppearanceGeneratorHelper;->value:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lorg/apache/pdfbox/pdmodel/font/PDFont;->getStringWidth(Ljava/lang/String;)F

    move-result v0

    const/high16 v3, 0x447a0000    # 1000.0f

    div-float/2addr v0, v3

    invoke-virtual {p2}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->getWidth()F

    move-result v4

    div-float/2addr v4, v0

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/font/PDFont;->getFontDescriptor()Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;

    move-result-object p1

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->getFontBoundingBox()Lorg/apache/pdfbox/pdmodel/common/PDRectangle;

    move-result-object p1

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->getHeight()F

    move-result p1

    div-float/2addr p1, v3

    invoke-virtual {p2}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->getHeight()F

    move-result p2

    div-float/2addr p2, p1

    invoke-static {p2, v4}, Ljava/lang/Math;->min(FF)F

    move-result v0

    :cond_1
    cmpl-float p1, v0, v2

    if-nez p1, :cond_2

    goto :goto_1

    :cond_2
    move v1, v0

    :goto_1
    return v1
.end method

.method private calculateHorizontalOffset(Lorg/apache/pdfbox/pdmodel/common/PDRectangle;Lorg/apache/pdfbox/pdmodel/font/PDFont;F)F
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/AppearanceGeneratorHelper;->value:Ljava/lang/String;

    invoke-virtual {p2, v0}, Lorg/apache/pdfbox/pdmodel/font/PDFont;->getStringWidth(Ljava/lang/String;)F

    move-result p2

    const/high16 v0, 0x447a0000    # 1000.0f

    div-float/2addr p2, v0

    mul-float/2addr p2, p3

    iget-object p3, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/AppearanceGeneratorHelper;->parent:Lorg/apache/pdfbox/pdmodel/interactive/form/PDVariableText;

    invoke-virtual {p3}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDVariableText;->getQ()I

    move-result p3

    if-eqz p3, :cond_3

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->getWidth()F

    move-result v0

    cmpl-float v0, p2, v0

    if-lez v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x1

    if-ne p3, v0, :cond_1

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->getLowerLeftX()F

    move-result p3

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->getWidth()F

    move-result p1

    sub-float/2addr p1, p2

    const/high16 p2, 0x40000000    # 2.0f

    div-float/2addr p1, p2

    add-float/2addr p3, p1

    goto :goto_1

    :cond_1
    const/4 v0, 0x2

    if-ne p3, v0, :cond_2

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->getLowerLeftX()F

    move-result p3

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->getWidth()F

    move-result p1

    add-float/2addr p3, p1

    sub-float/2addr p3, p2

    goto :goto_1

    :cond_2
    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->getLowerLeftX()F

    move-result p1

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Unknown justification value, defaulting to left: "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    const-string p3, "PdfBoxAndroid"

    invoke-static {p3, p2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move p3, p1

    goto :goto_1

    :cond_3
    :goto_0
    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->getLowerLeftX()F

    move-result p3

    :goto_1
    return p3
.end method

.method private calculateVerticalOffset(Lorg/apache/pdfbox/pdmodel/common/PDRectangle;Lorg/apache/pdfbox/pdmodel/common/PDRectangle;Lorg/apache/pdfbox/pdmodel/font/PDFont;F)F
    .locals 5

    invoke-direct {p0, p3, p4}, Lorg/apache/pdfbox/pdmodel/interactive/form/AppearanceGeneratorHelper;->getCapHeight(Lorg/apache/pdfbox/pdmodel/font/PDFont;F)F

    move-result v0

    invoke-virtual {p3}, Lorg/apache/pdfbox/pdmodel/font/PDFont;->getBoundingBox()Lorg/apache/fontbox/util/BoundingBox;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/fontbox/util/BoundingBox;->getHeight()F

    move-result v1

    const/high16 v2, 0x447a0000    # 1000.0f

    div-float/2addr v1, v2

    mul-float/2addr v1, p4

    iget-object v3, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/AppearanceGeneratorHelper;->parent:Lorg/apache/pdfbox/pdmodel/interactive/form/PDVariableText;

    instance-of v4, v3, Lorg/apache/pdfbox/pdmodel/interactive/form/PDTextField;

    if-eqz v4, :cond_0

    check-cast v3, Lorg/apache/pdfbox/pdmodel/interactive/form/PDTextField;

    invoke-virtual {v3}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDTextField;->isMultiline()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {p2}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->getUpperRightY()F

    move-result p1

    add-float/2addr p1, v1

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->getHeight()F

    move-result p2

    cmpl-float p2, v0, p2

    if-lez p2, :cond_1

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->getLowerLeftX()F

    move-result p1

    invoke-virtual {p3}, Lorg/apache/pdfbox/pdmodel/font/PDFont;->getFontDescriptor()Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;

    move-result-object p2

    invoke-virtual {p2}, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->getDescent()F

    move-result p2

    div-float/2addr p2, v2

    mul-float/2addr p2, p4

    sub-float/2addr p1, p2

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->getHeight()F

    move-result p2

    sub-float/2addr p2, v0

    const/high16 p3, 0x40000000    # 2.0f

    div-float/2addr p2, p3

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->getLowerLeftX()F

    move-result p1

    add-float/2addr p1, p2

    :goto_0
    return p1
.end method

.method private containsMarkedContent(Ljava/util/List;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/Object;",
            ">;)Z"
        }
    .end annotation

    const-string v0, "BMC"

    invoke-static {v0}, Lorg/apache/pdfbox/contentstream/operator/Operator;->getOperator(Ljava/lang/String;)Lorg/apache/pdfbox/contentstream/operator/Operator;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method private createAppearanceContent(Ljava/util/List;Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotationWidget;Lorg/apache/pdfbox/pdmodel/font/PDFont;Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAppearanceStream;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/Object;",
            ">;",
            "Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotationWidget;",
            "Lorg/apache/pdfbox/pdmodel/font/PDFont;",
            "Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAppearanceStream;",
            ")V"
        }
    .end annotation

    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    new-instance v1, Lorg/apache/pdfbox/pdfwriter/ContentStreamWriter;

    invoke-direct {v1, v0}, Lorg/apache/pdfbox/pdfwriter/ContentStreamWriter;-><init>(Ljava/io/OutputStream;)V

    invoke-virtual {v1, p1}, Lorg/apache/pdfbox/pdfwriter/ContentStreamWriter;->writeTokens(Ljava/util/List;)V

    const-string v1, "/Tx BMC\n"

    const-string v2, "ISO-8859-1"

    invoke-virtual {v1, v2}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/OutputStream;->write([B)V

    invoke-direct {p0, p2, p4}, Lorg/apache/pdfbox/pdmodel/interactive/form/AppearanceGeneratorHelper;->resolveBoundingBox(Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotationWidget;Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAppearanceStream;)Lorg/apache/pdfbox/pdmodel/common/PDRectangle;

    move-result-object p2

    invoke-direct {p0, p2, v0, p3, p1}, Lorg/apache/pdfbox/pdmodel/interactive/form/AppearanceGeneratorHelper;->insertGeneratedAppearance(Lorg/apache/pdfbox/pdmodel/common/PDRectangle;Ljava/io/OutputStream;Lorg/apache/pdfbox/pdmodel/font/PDFont;Ljava/util/List;)V

    const-string p1, "EMC"

    invoke-virtual {p1, v2}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/io/OutputStream;->write([B)V

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->close()V

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object p1

    invoke-direct {p0, p1, p4}, Lorg/apache/pdfbox/pdmodel/interactive/form/AppearanceGeneratorHelper;->writeToStream([BLorg/apache/pdfbox/pdmodel/interactive/annotation/PDAppearanceStream;)V

    return-void
.end method

.method private getCapHeight(Lorg/apache/pdfbox/pdmodel/font/PDFont;F)F
    .locals 3

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/font/PDFont;->getFontDescriptor()Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;

    move-result-object v0

    const/high16 v1, 0x447a0000    # 1000.0f

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->getCapHeight()F

    move-result v0

    const/4 v2, 0x0

    cmpl-float v0, v0, v2

    if-nez v0, :cond_0

    goto :goto_1

    :cond_0
    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/font/PDFont;->getFontDescriptor()Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;

    move-result-object p1

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/font/PDFontDescriptor;->getCapHeight()F

    move-result p1

    div-float/2addr p1, v1

    :goto_0
    mul-float/2addr p1, p2

    return p1

    :cond_1
    :goto_1
    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/font/PDFont;->getBoundingBox()Lorg/apache/fontbox/util/BoundingBox;

    move-result-object p1

    invoke-virtual {p1}, Lorg/apache/fontbox/util/BoundingBox;->getHeight()F

    move-result p1

    div-float/2addr p1, v1

    mul-float/2addr p1, p2

    const p2, 0x3f333333    # 0.7f

    goto :goto_0
.end method

.method private getDefaultAppearance()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/AppearanceGeneratorHelper;->parent:Lorg/apache/pdfbox/pdmodel/interactive/form/PDVariableText;

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDVariableText;->getDefaultAppearance()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getFontAndUpdateResources(Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAppearanceStream;)Lorg/apache/pdfbox/pdmodel/font/PDFont;
    .locals 3

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/graphics/form/PDFormXObject;->getResources()Lorg/apache/pdfbox/pdmodel/PDResources;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/AppearanceGeneratorHelper;->acroForm:Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;

    invoke-virtual {v1}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;->getDefaultResources()Lorg/apache/pdfbox/pdmodel/PDResources;

    move-result-object v1

    if-nez v0, :cond_1

    if-eqz v1, :cond_0

    goto :goto_0

    :cond_0
    new-instance p1, Ljava/io/IOException;

    const-string v0, "Unable to generate field appearance - missing required resources"

    invoke-direct {p1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    :goto_0
    iget-object v2, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/AppearanceGeneratorHelper;->defaultAppearanceHandler:Lorg/apache/pdfbox/pdmodel/interactive/form/DefaultAppearanceHandler;

    invoke-virtual {v2}, Lorg/apache/pdfbox/pdmodel/interactive/form/DefaultAppearanceHandler;->getFontName()Lorg/apache/pdfbox/cos/COSName;

    move-result-object v2

    if-eqz v0, :cond_2

    invoke-virtual {v0, v2}, Lorg/apache/pdfbox/pdmodel/PDResources;->getFont(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/pdmodel/font/PDFont;

    move-result-object p1

    if-eqz p1, :cond_3

    return-object p1

    :cond_2
    new-instance v0, Lorg/apache/pdfbox/pdmodel/PDResources;

    invoke-direct {v0}, Lorg/apache/pdfbox/pdmodel/PDResources;-><init>()V

    invoke-virtual {p1, v0}, Lorg/apache/pdfbox/pdmodel/graphics/form/PDFormXObject;->setResources(Lorg/apache/pdfbox/pdmodel/PDResources;)V

    :cond_3
    if-eqz v1, :cond_4

    invoke-virtual {v1, v2}, Lorg/apache/pdfbox/pdmodel/PDResources;->getFont(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/pdmodel/font/PDFont;

    move-result-object p1

    if-eqz p1, :cond_4

    invoke-virtual {v0, v2, p1}, Lorg/apache/pdfbox/pdmodel/PDResources;->put(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/pdmodel/font/PDFont;)V

    return-object p1

    :cond_4
    invoke-direct {p0, v0, v1, v2}, Lorg/apache/pdfbox/pdmodel/interactive/form/AppearanceGeneratorHelper;->resolveFont(Lorg/apache/pdfbox/pdmodel/PDResources;Lorg/apache/pdfbox/pdmodel/PDResources;Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/pdmodel/font/PDFont;

    move-result-object p1

    if-eqz p1, :cond_5

    invoke-virtual {v0, v2, p1}, Lorg/apache/pdfbox/pdmodel/PDResources;->put(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/pdmodel/font/PDFont;)V

    return-object p1

    :cond_5
    new-instance p1, Ljava/io/IOException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unable to generate field appearance - missing required font resources: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private getLineWidth(Ljava/util/List;)F
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/Object;",
            ">;)F"
        }
    .end annotation

    if-eqz p1, :cond_1

    const-string v0, "BT"

    invoke-static {v0}, Lorg/apache/pdfbox/contentstream/operator/Operator;->getOperator(Ljava/lang/String;)Lorg/apache/pdfbox/contentstream/operator/Operator;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    const-string v1, "w"

    invoke-static {v1}, Lorg/apache/pdfbox/contentstream/operator/Operator;->getOperator(Ljava/lang/String;)Lorg/apache/pdfbox/contentstream/operator/Operator;

    move-result-object v1

    invoke-interface {p1, v1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v1

    if-lez v1, :cond_1

    if-lt v1, v0, :cond_0

    const/4 v2, -0x1

    if-ne v0, v2, :cond_1

    :cond_0
    add-int/lit8 v1, v1, -0x1

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lorg/apache/pdfbox/cos/COSNumber;

    invoke-virtual {p1}, Lorg/apache/pdfbox/cos/COSNumber;->floatValue()F

    move-result p1

    goto :goto_0

    :cond_1
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method private getStreamTokens(Lorg/apache/pdfbox/cos/COSStream;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/pdfbox/cos/COSStream;",
            ")",
            "Ljava/util/List<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    if-eqz p1, :cond_0

    new-instance v0, Lorg/apache/pdfbox/pdfparser/PDFStreamParser;

    invoke-direct {v0, p1}, Lorg/apache/pdfbox/pdfparser/PDFStreamParser;-><init>(Lorg/apache/pdfbox/cos/COSStream;)V

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdfparser/PDFStreamParser;->parse()V

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdfparser/PDFStreamParser;->getTokens()Ljava/util/List;

    move-result-object p1

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdfparser/BaseParser;->close()V

    move-object v0, p1

    :cond_0
    return-object v0
.end method

.method private getStreamTokens(Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAppearanceStream;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAppearanceStream;",
            ")",
            "Ljava/util/List<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .line 2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/graphics/PDXObject;->getCOSStream()Lorg/apache/pdfbox/cos/COSStream;

    move-result-object p1

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/interactive/form/AppearanceGeneratorHelper;->getStreamTokens(Lorg/apache/pdfbox/cos/COSStream;)Ljava/util/List;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method private insertGeneratedAppearance(Lorg/apache/pdfbox/pdmodel/common/PDRectangle;Ljava/io/OutputStream;Lorg/apache/pdfbox/pdmodel/font/PDFont;Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/pdfbox/pdmodel/common/PDRectangle;",
            "Ljava/io/OutputStream;",
            "Lorg/apache/pdfbox/pdmodel/font/PDFont;",
            "Ljava/util/List<",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    new-instance v0, Lorg/apache/pdfbox/pdmodel/interactive/form/AppearancePrimitivesComposer;

    invoke-direct {v0, p2}, Lorg/apache/pdfbox/pdmodel/interactive/form/AppearancePrimitivesComposer;-><init>(Ljava/io/OutputStream;)V

    invoke-direct {p0, p4}, Lorg/apache/pdfbox/pdmodel/interactive/form/AppearanceGeneratorHelper;->getLineWidth(Ljava/util/List;)F

    move-result p4

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-static {v1, p4}, Ljava/lang/Math;->max(FF)F

    move-result v2

    invoke-direct {p0, p1, v2}, Lorg/apache/pdfbox/pdmodel/interactive/form/AppearanceGeneratorHelper;->applyPadding(Lorg/apache/pdfbox/pdmodel/common/PDRectangle;F)Lorg/apache/pdfbox/pdmodel/common/PDRectangle;

    move-result-object p1

    invoke-static {v1, p4}, Ljava/lang/Math;->max(FF)F

    move-result p4

    invoke-direct {p0, p1, p4}, Lorg/apache/pdfbox/pdmodel/interactive/form/AppearanceGeneratorHelper;->applyPadding(Lorg/apache/pdfbox/pdmodel/common/PDRectangle;F)Lorg/apache/pdfbox/pdmodel/common/PDRectangle;

    move-result-object p4

    invoke-virtual {v0, p1}, Lorg/apache/pdfbox/pdmodel/interactive/form/AppearancePrimitivesComposer;->addRect(Lorg/apache/pdfbox/pdmodel/common/PDRectangle;)V

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/interactive/form/AppearancePrimitivesComposer;->clip()V

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/interactive/form/AppearancePrimitivesComposer;->beginText()V

    invoke-direct {p0, p3, p4}, Lorg/apache/pdfbox/pdmodel/interactive/form/AppearanceGeneratorHelper;->calculateFontSize(Lorg/apache/pdfbox/pdmodel/font/PDFont;Lorg/apache/pdfbox/pdmodel/common/PDRectangle;)F

    move-result v1

    iget-object v2, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/AppearanceGeneratorHelper;->defaultAppearanceHandler:Lorg/apache/pdfbox/pdmodel/interactive/form/DefaultAppearanceHandler;

    invoke-virtual {v2}, Lorg/apache/pdfbox/pdmodel/interactive/form/DefaultAppearanceHandler;->getTokens()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/AppearanceGeneratorHelper;->defaultAppearanceHandler:Lorg/apache/pdfbox/pdmodel/interactive/form/DefaultAppearanceHandler;

    invoke-virtual {v2, v1}, Lorg/apache/pdfbox/pdmodel/interactive/form/DefaultAppearanceHandler;->setFontSize(F)V

    new-instance v2, Lorg/apache/pdfbox/pdfwriter/ContentStreamWriter;

    invoke-direct {v2, p2}, Lorg/apache/pdfbox/pdfwriter/ContentStreamWriter;-><init>(Ljava/io/OutputStream;)V

    iget-object p2, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/AppearanceGeneratorHelper;->defaultAppearanceHandler:Lorg/apache/pdfbox/pdmodel/interactive/form/DefaultAppearanceHandler;

    invoke-virtual {p2}, Lorg/apache/pdfbox/pdmodel/interactive/form/DefaultAppearanceHandler;->getTokens()Ljava/util/List;

    move-result-object p2

    invoke-virtual {v2, p2}, Lorg/apache/pdfbox/pdfwriter/ContentStreamWriter;->writeTokens(Ljava/util/List;)V

    :cond_0
    invoke-direct {p0, p1, p4, p3, v1}, Lorg/apache/pdfbox/pdmodel/interactive/form/AppearanceGeneratorHelper;->calculateVerticalOffset(Lorg/apache/pdfbox/pdmodel/common/PDRectangle;Lorg/apache/pdfbox/pdmodel/common/PDRectangle;Lorg/apache/pdfbox/pdmodel/font/PDFont;F)F

    move-result p1

    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/interactive/form/AppearanceGeneratorHelper;->isMultiLine()Z

    move-result p2

    if-nez p2, :cond_1

    invoke-direct {p0, p4, p3, v1}, Lorg/apache/pdfbox/pdmodel/interactive/form/AppearanceGeneratorHelper;->calculateHorizontalOffset(Lorg/apache/pdfbox/pdmodel/common/PDRectangle;Lorg/apache/pdfbox/pdmodel/font/PDFont;F)F

    move-result p2

    invoke-virtual {v0, p2, p1}, Lorg/apache/pdfbox/pdmodel/interactive/form/AppearancePrimitivesComposer;->newLineAtOffset(FF)V

    iget-object p1, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/AppearanceGeneratorHelper;->value:Ljava/lang/String;

    invoke-virtual {v0, p1, p3}, Lorg/apache/pdfbox/pdmodel/interactive/form/AppearancePrimitivesComposer;->showText(Ljava/lang/String;Lorg/apache/pdfbox/pdmodel/font/PDFont;)V

    goto :goto_0

    :cond_1
    invoke-virtual {p4}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->getLowerLeftX()F

    move-result p2

    new-instance v2, Lorg/apache/pdfbox/pdmodel/interactive/form/PlainText;

    iget-object v3, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/AppearanceGeneratorHelper;->value:Ljava/lang/String;

    invoke-direct {v2, v3}, Lorg/apache/pdfbox/pdmodel/interactive/form/PlainText;-><init>(Ljava/lang/String;)V

    new-instance v3, Lorg/apache/pdfbox/pdmodel/interactive/form/AppearanceStyle;

    invoke-direct {v3}, Lorg/apache/pdfbox/pdmodel/interactive/form/AppearanceStyle;-><init>()V

    invoke-virtual {v3, p3}, Lorg/apache/pdfbox/pdmodel/interactive/form/AppearanceStyle;->setFont(Lorg/apache/pdfbox/pdmodel/font/PDFont;)V

    invoke-virtual {v3, v1}, Lorg/apache/pdfbox/pdmodel/interactive/form/AppearanceStyle;->setFontSize(F)V

    invoke-virtual {p3}, Lorg/apache/pdfbox/pdmodel/font/PDFont;->getBoundingBox()Lorg/apache/fontbox/util/BoundingBox;

    move-result-object p3

    invoke-virtual {p3}, Lorg/apache/fontbox/util/BoundingBox;->getHeight()F

    move-result p3

    const/high16 v4, 0x447a0000    # 1000.0f

    div-float/2addr p3, v4

    mul-float/2addr p3, v1

    invoke-virtual {v3, p3}, Lorg/apache/pdfbox/pdmodel/interactive/form/AppearanceStyle;->setLeading(F)V

    new-instance p3, Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter$Builder;

    invoke-direct {p3, v0}, Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter$Builder;-><init>(Lorg/apache/pdfbox/pdmodel/interactive/form/AppearancePrimitivesComposer;)V

    invoke-virtual {p3, v3}, Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter$Builder;->style(Lorg/apache/pdfbox/pdmodel/interactive/form/AppearanceStyle;)Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter$Builder;

    move-result-object p3

    invoke-virtual {p3, v2}, Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter$Builder;->text(Lorg/apache/pdfbox/pdmodel/interactive/form/PlainText;)Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter$Builder;

    move-result-object p3

    invoke-virtual {p4}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->getWidth()F

    move-result p4

    invoke-virtual {p3, p4}, Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter$Builder;->width(F)Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter$Builder;

    move-result-object p3

    const/4 p4, 0x1

    invoke-virtual {p3, p4}, Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter$Builder;->wrapLines(Z)Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter$Builder;

    move-result-object p3

    invoke-virtual {p3, p2, p1}, Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter$Builder;->initialOffset(FF)Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter$Builder;

    move-result-object p1

    iget-object p2, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/AppearanceGeneratorHelper;->parent:Lorg/apache/pdfbox/pdmodel/interactive/form/PDVariableText;

    invoke-virtual {p2}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDVariableText;->getQ()I

    move-result p2

    invoke-virtual {p1, p2}, Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter$Builder;->textAlign(I)Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter$Builder;

    move-result-object p1

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter$Builder;->build()Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter;

    move-result-object p1

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter;->format()V

    :goto_0
    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/interactive/form/AppearancePrimitivesComposer;->endText()V

    return-void
.end method

.method private isMultiLine()Z
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/AppearanceGeneratorHelper;->parent:Lorg/apache/pdfbox/pdmodel/interactive/form/PDVariableText;

    instance-of v1, v0, Lorg/apache/pdfbox/pdmodel/interactive/form/PDTextField;

    if-eqz v1, :cond_0

    check-cast v0, Lorg/apache/pdfbox/pdmodel/interactive/form/PDTextField;

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDTextField;->isMultiline()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private resolveBoundingBox(Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotationWidget;Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAppearanceStream;)Lorg/apache/pdfbox/pdmodel/common/PDRectangle;
    .locals 0

    invoke-virtual {p2}, Lorg/apache/pdfbox/pdmodel/graphics/form/PDFormXObject;->getBBox()Lorg/apache/pdfbox/pdmodel/common/PDRectangle;

    move-result-object p2

    if-nez p2, :cond_0

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotation;->getRectangle()Lorg/apache/pdfbox/pdmodel/common/PDRectangle;

    move-result-object p1

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->createRetranslatedRectangle()Lorg/apache/pdfbox/pdmodel/common/PDRectangle;

    move-result-object p2

    :cond_0
    return-object p2
.end method

.method private resolveFont(Lorg/apache/pdfbox/pdmodel/PDResources;Lorg/apache/pdfbox/pdmodel/PDResources;Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/pdmodel/font/PDFont;
    .locals 4

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/PDResources;->getFontNames()Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p1, v1}, Lorg/apache/pdfbox/pdmodel/PDResources;->getFont(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/pdmodel/font/PDFont;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/pdfbox/pdmodel/font/PDFont;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p3}, Lorg/apache/pdfbox/cos/COSName;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    return-object v1

    :cond_1
    if-eqz p2, :cond_3

    invoke-virtual {p2}, Lorg/apache/pdfbox/pdmodel/PDResources;->getFontNames()Ljava/lang/Iterable;

    move-result-object p1

    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_2
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p2, v0}, Lorg/apache/pdfbox/pdmodel/PDResources;->getFont(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/pdmodel/font/PDFont;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/font/PDFont;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p3}, Lorg/apache/pdfbox/cos/COSName;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    return-object v0

    :cond_3
    const/4 p1, 0x0

    return-object p1
.end method

.method private updateAppearanceContent(Ljava/util/List;Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotationWidget;Lorg/apache/pdfbox/pdmodel/font/PDFont;Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAppearanceStream;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/Object;",
            ">;",
            "Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotationWidget;",
            "Lorg/apache/pdfbox/pdmodel/font/PDFont;",
            "Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAppearanceStream;",
            ")V"
        }
    .end annotation

    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    new-instance v1, Lorg/apache/pdfbox/pdfwriter/ContentStreamWriter;

    invoke-direct {v1, v0}, Lorg/apache/pdfbox/pdfwriter/ContentStreamWriter;-><init>(Ljava/io/OutputStream;)V

    invoke-direct {p0, p2, p4}, Lorg/apache/pdfbox/pdmodel/interactive/form/AppearanceGeneratorHelper;->resolveBoundingBox(Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotationWidget;Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAppearanceStream;)Lorg/apache/pdfbox/pdmodel/common/PDRectangle;

    move-result-object p2

    const-string v2, "BMC"

    invoke-static {v2}, Lorg/apache/pdfbox/contentstream/operator/Operator;->getOperator(Ljava/lang/String;)Lorg/apache/pdfbox/contentstream/operator/Operator;

    move-result-object v2

    invoke-interface {p1, v2}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v2

    const-string v3, "EMC"

    invoke-static {v3}, Lorg/apache/pdfbox/contentstream/operator/Operator;->getOperator(Ljava/lang/String;)Lorg/apache/pdfbox/contentstream/operator/Operator;

    move-result-object v3

    invoke-interface {p1, v3}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v3

    add-int/lit8 v2, v2, 0x1

    const/4 v4, 0x0

    invoke-virtual {v1, p1, v4, v2}, Lorg/apache/pdfbox/pdfwriter/ContentStreamWriter;->writeTokens(Ljava/util/List;II)V

    const-string v2, "\n"

    const-string v4, "ISO-8859-1"

    invoke-virtual {v2, v4}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/io/OutputStream;->write([B)V

    invoke-direct {p0, p2, v0, p3, p1}, Lorg/apache/pdfbox/pdmodel/interactive/form/AppearanceGeneratorHelper;->insertGeneratedAppearance(Lorg/apache/pdfbox/pdmodel/common/PDRectangle;Ljava/io/OutputStream;Lorg/apache/pdfbox/pdmodel/font/PDFont;Ljava/util/List;)V

    const/4 p2, -0x1

    if-eq v3, p2, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p2

    invoke-virtual {v1, p1, v3, p2}, Lorg/apache/pdfbox/pdfwriter/ContentStreamWriter;->writeTokens(Ljava/util/List;II)V

    :cond_0
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->close()V

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object p1

    invoke-direct {p0, p1, p4}, Lorg/apache/pdfbox/pdmodel/interactive/form/AppearanceGeneratorHelper;->writeToStream([BLorg/apache/pdfbox/pdmodel/interactive/annotation/PDAppearanceStream;)V

    return-void
.end method

.method private writeToStream([BLorg/apache/pdfbox/pdmodel/interactive/annotation/PDAppearanceStream;)V
    .locals 0

    invoke-virtual {p2}, Lorg/apache/pdfbox/pdmodel/graphics/PDXObject;->getCOSStream()Lorg/apache/pdfbox/cos/COSStream;

    move-result-object p2

    invoke-virtual {p2}, Lorg/apache/pdfbox/cos/COSStream;->createUnfilteredStream()Ljava/io/OutputStream;

    move-result-object p2

    invoke-virtual {p2, p1}, Ljava/io/OutputStream;->write([B)V

    invoke-virtual {p2}, Ljava/io/OutputStream;->flush()V

    return-void
.end method


# virtual methods
.method public setAppearanceValue(Ljava/lang/String;)V
    .locals 5

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/AppearanceGeneratorHelper;->value:Ljava/lang/String;

    iget-object p1, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/AppearanceGeneratorHelper;->widgets:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/pdmodel/common/COSObjectable;

    instance-of v1, v0, Lorg/apache/pdfbox/pdmodel/interactive/form/PDField;

    const/4 v2, 0x0

    if-eqz v1, :cond_1

    check-cast v0, Lorg/apache/pdfbox/pdmodel/interactive/form/PDField;

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->getWidget()Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotationWidget;

    move-result-object v1

    goto :goto_1

    :cond_1
    move-object v1, v0

    check-cast v1, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotationWidget;

    move-object v0, v2

    :goto_1
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->getActions()Lorg/apache/pdfbox/pdmodel/interactive/action/PDFormFieldAdditionalActions;

    move-result-object v0

    goto :goto_2

    :cond_2
    move-object v0, v2

    :goto_2
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/interactive/action/PDFormFieldAdditionalActions;->getF()Lorg/apache/pdfbox/pdmodel/interactive/action/PDAction;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {v1}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotation;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v3, Lorg/apache/pdfbox/cos/COSName;->AP:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v3}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    if-eqz v0, :cond_0

    :cond_3
    invoke-virtual {v1}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotation;->getAppearance()Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAppearanceDictionary;

    move-result-object v0

    if-nez v0, :cond_4

    new-instance v0, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAppearanceDictionary;

    invoke-direct {v0}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAppearanceDictionary;-><init>()V

    invoke-virtual {v1, v0}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotation;->setAppearance(Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAppearanceDictionary;)V

    :cond_4
    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAppearanceDictionary;->getNormalAppearance()Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAppearanceEntry;

    move-result-object v3

    invoke-virtual {v3}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAppearanceEntry;->isStream()Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-virtual {v3}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAppearanceEntry;->getAppearanceStream()Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAppearanceStream;

    move-result-object v2

    :cond_5
    if-nez v2, :cond_6

    iget-object v2, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/AppearanceGeneratorHelper;->acroForm:Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;

    invoke-virtual {v2}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;->getDocument()Lorg/apache/pdfbox/pdmodel/PDDocument;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/pdfbox/pdmodel/PDDocument;->getDocument()Lorg/apache/pdfbox/cos/COSDocument;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/pdfbox/cos/COSDocument;->createCOSStream()Lorg/apache/pdfbox/cos/COSStream;

    move-result-object v2

    new-instance v3, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAppearanceStream;

    invoke-direct {v3, v2}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAppearanceStream;-><init>(Lorg/apache/pdfbox/cos/COSStream;)V

    invoke-virtual {v1}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotation;->getRectangle()Lorg/apache/pdfbox/pdmodel/common/PDRectangle;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->createRetranslatedRectangle()Lorg/apache/pdfbox/pdmodel/common/PDRectangle;

    move-result-object v2

    invoke-virtual {v3, v2}, Lorg/apache/pdfbox/pdmodel/graphics/form/PDFormXObject;->setBBox(Lorg/apache/pdfbox/pdmodel/common/PDRectangle;)V

    invoke-virtual {v0, v3}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAppearanceDictionary;->setNormalAppearance(Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAppearanceStream;)V

    move-object v2, v3

    :cond_6
    invoke-direct {p0, v2}, Lorg/apache/pdfbox/pdmodel/interactive/form/AppearanceGeneratorHelper;->getStreamTokens(Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAppearanceStream;)Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v2}, Lorg/apache/pdfbox/pdmodel/interactive/form/AppearanceGeneratorHelper;->getFontAndUpdateResources(Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAppearanceStream;)Lorg/apache/pdfbox/pdmodel/font/PDFont;

    move-result-object v3

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/pdmodel/interactive/form/AppearanceGeneratorHelper;->containsMarkedContent(Ljava/util/List;)Z

    move-result v4

    if-nez v4, :cond_7

    invoke-direct {p0, v0, v1, v3, v2}, Lorg/apache/pdfbox/pdmodel/interactive/form/AppearanceGeneratorHelper;->createAppearanceContent(Ljava/util/List;Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotationWidget;Lorg/apache/pdfbox/pdmodel/font/PDFont;Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAppearanceStream;)V

    goto/16 :goto_0

    :cond_7
    invoke-direct {p0, v0, v1, v3, v2}, Lorg/apache/pdfbox/pdmodel/interactive/form/AppearanceGeneratorHelper;->updateAppearanceContent(Ljava/util/List;Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotationWidget;Lorg/apache/pdfbox/pdmodel/font/PDFont;Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAppearanceStream;)V

    goto/16 :goto_0

    :cond_8
    return-void
.end method

.class public abstract Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotation;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/pdfbox/pdmodel/common/COSObjectable;


# static fields
.field public static final FLAG_HIDDEN:I = 0x2

.field public static final FLAG_INVISIBLE:I = 0x1

.field public static final FLAG_LOCKED:I = 0x80

.field public static final FLAG_NO_ROTATE:I = 0x10

.field public static final FLAG_NO_VIEW:I = 0x20

.field public static final FLAG_NO_ZOOM:I = 0x8

.field public static final FLAG_PRINTED:I = 0x4

.field public static final FLAG_READ_ONLY:I = 0x40

.field public static final FLAG_TOGGLE_NO_VIEW:I = 0x100


# instance fields
.field private final dictionary:Lorg/apache/pdfbox/cos/COSDictionary;


# direct methods
.method public constructor <init>()V
    .locals 3

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-direct {v0}, Lorg/apache/pdfbox/cos/COSDictionary;-><init>()V

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotation;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->TYPE:Lorg/apache/pdfbox/cos/COSName;

    sget-object v2, Lorg/apache/pdfbox/cos/COSName;->ANNOT:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    return-void
.end method

.method public constructor <init>(Lorg/apache/pdfbox/cos/COSDictionary;)V
    .locals 2

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotation;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->TYPE:Lorg/apache/pdfbox/cos/COSName;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->ANNOT:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p1, v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    return-void
.end method

.method public static createAnnotation(Lorg/apache/pdfbox/cos/COSBase;)Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotation;
    .locals 3

    instance-of v0, p0, Lorg/apache/pdfbox/cos/COSDictionary;

    if-eqz v0, :cond_e

    check-cast p0, Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->SUBTYPE:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p0, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->getNameAsString(Lorg/apache/pdfbox/cos/COSName;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "FileAttachment"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v0, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotationFileAttachment;

    invoke-direct {v0, p0}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotationFileAttachment;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    goto/16 :goto_3

    :cond_0
    const-string v1, "Line"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance v0, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotationLine;

    invoke-direct {v0, p0}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotationLine;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    goto/16 :goto_3

    :cond_1
    const-string v1, "Link"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    new-instance v0, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotationLink;

    invoke-direct {v0, p0}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotationLink;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    goto/16 :goto_3

    :cond_2
    const-string v2, "Popup"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    new-instance v0, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotationPopup;

    invoke-direct {v0, p0}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotationPopup;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    goto/16 :goto_3

    :cond_3
    const-string v2, "Stamp"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    new-instance v0, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotationRubberStamp;

    invoke-direct {v0, p0}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotationRubberStamp;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    goto/16 :goto_3

    :cond_4
    const-string v2, "Square"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_d

    const-string v2, "Circle"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    goto/16 :goto_2

    :cond_5
    const-string v2, "Text"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    new-instance v0, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotationText;

    invoke-direct {v0, p0}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotationText;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    goto/16 :goto_3

    :cond_6
    const-string v2, "Highlight"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    const-string v2, "Underline"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    const-string v2, "Squiggly"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    const-string v2, "StrikeOut"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    goto/16 :goto_1

    :cond_7
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    new-instance v0, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotationLink;

    invoke-direct {v0, p0}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotationLink;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    goto/16 :goto_3

    :cond_8
    const-string v1, "Widget"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    new-instance v0, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotationWidget;

    invoke-direct {v0, p0}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotationWidget;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    goto :goto_3

    :cond_9
    const-string v1, "FreeText"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_b

    const-string v1, "Polygon"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_b

    const-string v1, "PolyLine"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_b

    const-string v1, "Caret"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_b

    const-string v1, "Ink"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_b

    const-string v1, "Sound"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    goto :goto_0

    :cond_a
    new-instance v1, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotationUnknown;

    invoke-direct {v1, p0}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotationUnknown;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown or unsupported annotation subtype "

    invoke-virtual {p0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    const-string v0, "PdfBoxAndroid"

    invoke-static {v0, p0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v1

    goto :goto_3

    :cond_b
    :goto_0
    new-instance v0, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotationMarkup;

    invoke-direct {v0, p0}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotationMarkup;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    goto :goto_3

    :cond_c
    :goto_1
    new-instance v0, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotationTextMarkup;

    invoke-direct {v0, p0}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotationTextMarkup;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    goto :goto_3

    :cond_d
    :goto_2
    new-instance v0, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotationSquareCircle;

    invoke-direct {v0, p0}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotationSquareCircle;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    :goto_3
    return-object v0

    :cond_e
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Error: Unknown annotation type "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public getAnnotationFlags()I
    .locals 3

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotation;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->F:Lorg/apache/pdfbox/cos/COSName;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->getInt(Lorg/apache/pdfbox/cos/COSName;I)I

    move-result v0

    return v0
.end method

.method public getAnnotationName()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotation;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->NM:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getString(Lorg/apache/pdfbox/cos/COSName;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getAppearance()Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAppearanceDictionary;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotation;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->AP:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSDictionary;

    if-eqz v0, :cond_0

    new-instance v1, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAppearanceDictionary;

    invoke-direct {v1, v0}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAppearanceDictionary;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return-object v1
.end method

.method public getAppearanceState()Lorg/apache/pdfbox/cos/COSName;
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotation;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->AS:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSName;

    if-eqz v0, :cond_0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public getCOSObject()Lorg/apache/pdfbox/cos/COSBase;
    .locals 1

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotation;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    return-object v0
.end method

.method public getColor()Lorg/apache/pdfbox/pdmodel/graphics/color/PDColor;
    .locals 1

    .line 1
    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->C:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p0, v0}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotation;->getColor(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/pdmodel/graphics/color/PDColor;

    move-result-object v0

    return-object v0
.end method

.method public getColor(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/pdmodel/graphics/color/PDColor;
    .locals 3

    .line 2
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotation;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->getItem(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object p1

    instance-of v0, p1, Lorg/apache/pdfbox/cos/COSArray;

    const/4 v1, 0x0

    if-eqz v0, :cond_2

    check-cast p1, Lorg/apache/pdfbox/cos/COSArray;

    invoke-virtual {p1}, Lorg/apache/pdfbox/cos/COSArray;->size()I

    move-result v0

    const/4 v2, 0x1

    if-eq v0, v2, :cond_1

    const/4 v2, 0x3

    if-eq v0, v2, :cond_0

    goto :goto_0

    :cond_0
    sget-object v1, Lorg/apache/pdfbox/pdmodel/graphics/color/PDDeviceRGB;->INSTANCE:Lorg/apache/pdfbox/pdmodel/graphics/color/PDDeviceRGB;

    goto :goto_0

    :cond_1
    sget-object v1, Lorg/apache/pdfbox/pdmodel/graphics/color/PDDeviceGray;->INSTANCE:Lorg/apache/pdfbox/pdmodel/graphics/color/PDDeviceGray;

    :goto_0
    new-instance v0, Lorg/apache/pdfbox/pdmodel/graphics/color/PDColor;

    invoke-direct {v0, p1, v1}, Lorg/apache/pdfbox/pdmodel/graphics/color/PDColor;-><init>(Lorg/apache/pdfbox/cos/COSArray;Lorg/apache/pdfbox/pdmodel/graphics/color/PDColorSpace;)V

    return-object v0

    :cond_2
    return-object v1
.end method

.method public getContents()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotation;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->CONTENTS:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getString(Lorg/apache/pdfbox/cos/COSName;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotation;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    return-object v0
.end method

.method public getModifiedDate()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotation;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->M:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getString(Lorg/apache/pdfbox/cos/COSName;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getNormalAppearanceStream()Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAppearanceStream;
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotation;->getAppearance()Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAppearanceDictionary;

    move-result-object v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return-object v1

    :cond_0
    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAppearanceDictionary;->getNormalAppearance()Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAppearanceEntry;

    move-result-object v0

    if-nez v0, :cond_1

    return-object v1

    :cond_1
    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAppearanceEntry;->isSubDictionary()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotation;->getAppearanceState()Lorg/apache/pdfbox/cos/COSName;

    move-result-object v1

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAppearanceEntry;->getSubDictionary()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAppearanceStream;

    return-object v0

    :cond_2
    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAppearanceEntry;->getAppearanceStream()Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAppearanceStream;

    move-result-object v0

    return-object v0
.end method

.method public getPage()Lorg/apache/pdfbox/pdmodel/PDPage;
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotation;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->P:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSDictionary;

    if-eqz v0, :cond_0

    new-instance v1, Lorg/apache/pdfbox/pdmodel/PDPage;

    invoke-direct {v1, v0}, Lorg/apache/pdfbox/pdmodel/PDPage;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    return-object v1

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public getRectangle()Lorg/apache/pdfbox/pdmodel/common/PDRectangle;
    .locals 3

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotation;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->RECT:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSArray;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSArray;->size()I

    move-result v1

    const/4 v2, 0x4

    if-ne v1, v2, :cond_0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSArray;->get(I)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v1

    instance-of v1, v1, Lorg/apache/pdfbox/cos/COSNumber;

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSArray;->get(I)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v1

    instance-of v1, v1, Lorg/apache/pdfbox/cos/COSNumber;

    if-eqz v1, :cond_0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSArray;->get(I)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v1

    instance-of v1, v1, Lorg/apache/pdfbox/cos/COSNumber;

    if-eqz v1, :cond_0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSArray;->get(I)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v1

    instance-of v1, v1, Lorg/apache/pdfbox/cos/COSNumber;

    if-eqz v1, :cond_0

    new-instance v1, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;

    invoke-direct {v1, v0}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;-><init>(Lorg/apache/pdfbox/cos/COSArray;)V

    goto :goto_0

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v0, " is not a rectangle array, returning null"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "PdfBoxAndroid"

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    const/4 v1, 0x0

    :goto_0
    return-object v1
.end method

.method public getStructParent()I
    .locals 3

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotation;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->STRUCT_PARENT:Lorg/apache/pdfbox/cos/COSName;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->getInt(Lorg/apache/pdfbox/cos/COSName;I)I

    move-result v0

    return v0
.end method

.method public getSubtype()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotation;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->SUBTYPE:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getNameAsString(Lorg/apache/pdfbox/cos/COSName;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public isHidden()Z
    .locals 3

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotation;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->F:Lorg/apache/pdfbox/cos/COSName;

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->getFlag(Lorg/apache/pdfbox/cos/COSName;I)Z

    move-result v0

    return v0
.end method

.method public isInvisible()Z
    .locals 3

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotation;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->F:Lorg/apache/pdfbox/cos/COSName;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->getFlag(Lorg/apache/pdfbox/cos/COSName;I)Z

    move-result v0

    return v0
.end method

.method public isLocked()Z
    .locals 3

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotation;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->F:Lorg/apache/pdfbox/cos/COSName;

    const/16 v2, 0x80

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->getFlag(Lorg/apache/pdfbox/cos/COSName;I)Z

    move-result v0

    return v0
.end method

.method public isNoRotate()Z
    .locals 3

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotation;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->F:Lorg/apache/pdfbox/cos/COSName;

    const/16 v2, 0x10

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->getFlag(Lorg/apache/pdfbox/cos/COSName;I)Z

    move-result v0

    return v0
.end method

.method public isNoView()Z
    .locals 3

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotation;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->F:Lorg/apache/pdfbox/cos/COSName;

    const/16 v2, 0x20

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->getFlag(Lorg/apache/pdfbox/cos/COSName;I)Z

    move-result v0

    return v0
.end method

.method public isNoZoom()Z
    .locals 3

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotation;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->F:Lorg/apache/pdfbox/cos/COSName;

    const/16 v2, 0x8

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->getFlag(Lorg/apache/pdfbox/cos/COSName;I)Z

    move-result v0

    return v0
.end method

.method public isPrinted()Z
    .locals 3

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotation;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->F:Lorg/apache/pdfbox/cos/COSName;

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->getFlag(Lorg/apache/pdfbox/cos/COSName;I)Z

    move-result v0

    return v0
.end method

.method public isReadOnly()Z
    .locals 3

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotation;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->F:Lorg/apache/pdfbox/cos/COSName;

    const/16 v2, 0x40

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->getFlag(Lorg/apache/pdfbox/cos/COSName;I)Z

    move-result v0

    return v0
.end method

.method public isToggleNoView()Z
    .locals 3

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotation;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->F:Lorg/apache/pdfbox/cos/COSName;

    const/16 v2, 0x100

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->getFlag(Lorg/apache/pdfbox/cos/COSName;I)Z

    move-result v0

    return v0
.end method

.method public setAnnotationFlags(I)V
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotation;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->F:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setInt(Lorg/apache/pdfbox/cos/COSName;I)V

    return-void
.end method

.method public setAnnotationName(Ljava/lang/String;)V
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotation;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->NM:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setString(Lorg/apache/pdfbox/cos/COSName;Ljava/lang/String;)V

    return-void
.end method

.method public setAppearance(Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAppearanceDictionary;)V
    .locals 2

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAppearanceDictionary;->getCOSObject()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotation;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->AP:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    return-void
.end method

.method public setAppearanceState(Ljava/lang/String;)V
    .locals 2

    if-nez p1, :cond_0

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotation;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object p1

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->AS:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p1, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->removeItem(Lorg/apache/pdfbox/cos/COSName;)V

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotation;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->AS:Lorg/apache/pdfbox/cos/COSName;

    invoke-static {p1}, Lorg/apache/pdfbox/cos/COSName;->getPDFName(Ljava/lang/String;)Lorg/apache/pdfbox/cos/COSName;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    :goto_0
    return-void
.end method

.method public setColor(Lorg/apache/pdfbox/pdmodel/graphics/color/PDColor;)V
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotation;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->C:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/graphics/color/PDColor;->toCOSArray()Lorg/apache/pdfbox/cos/COSArray;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    return-void
.end method

.method public setContents(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotation;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->CONTENTS:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setString(Lorg/apache/pdfbox/cos/COSName;Ljava/lang/String;)V

    return-void
.end method

.method public setHidden(Z)V
    .locals 3

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotation;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->F:Lorg/apache/pdfbox/cos/COSName;

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setFlag(Lorg/apache/pdfbox/cos/COSName;IZ)V

    return-void
.end method

.method public setInvisible(Z)V
    .locals 3

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotation;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->F:Lorg/apache/pdfbox/cos/COSName;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setFlag(Lorg/apache/pdfbox/cos/COSName;IZ)V

    return-void
.end method

.method public setLocked(Z)V
    .locals 3

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotation;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->F:Lorg/apache/pdfbox/cos/COSName;

    const/16 v2, 0x80

    invoke-virtual {v0, v1, v2, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setFlag(Lorg/apache/pdfbox/cos/COSName;IZ)V

    return-void
.end method

.method public setModifiedDate(Ljava/lang/String;)V
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotation;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->M:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setString(Lorg/apache/pdfbox/cos/COSName;Ljava/lang/String;)V

    return-void
.end method

.method public setNoRotate(Z)V
    .locals 3

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotation;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->F:Lorg/apache/pdfbox/cos/COSName;

    const/16 v2, 0x10

    invoke-virtual {v0, v1, v2, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setFlag(Lorg/apache/pdfbox/cos/COSName;IZ)V

    return-void
.end method

.method public setNoView(Z)V
    .locals 3

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotation;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->F:Lorg/apache/pdfbox/cos/COSName;

    const/16 v2, 0x20

    invoke-virtual {v0, v1, v2, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setFlag(Lorg/apache/pdfbox/cos/COSName;IZ)V

    return-void
.end method

.method public setNoZoom(Z)V
    .locals 3

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotation;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->F:Lorg/apache/pdfbox/cos/COSName;

    const/16 v2, 0x8

    invoke-virtual {v0, v1, v2, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setFlag(Lorg/apache/pdfbox/cos/COSName;IZ)V

    return-void
.end method

.method public setPage(Lorg/apache/pdfbox/pdmodel/PDPage;)V
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotation;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->P:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)V

    return-void
.end method

.method public setPrinted(Z)V
    .locals 3

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotation;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->F:Lorg/apache/pdfbox/cos/COSName;

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v2, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setFlag(Lorg/apache/pdfbox/cos/COSName;IZ)V

    return-void
.end method

.method public setReadOnly(Z)V
    .locals 3

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotation;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->F:Lorg/apache/pdfbox/cos/COSName;

    const/16 v2, 0x40

    invoke-virtual {v0, v1, v2, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setFlag(Lorg/apache/pdfbox/cos/COSName;IZ)V

    return-void
.end method

.method public setRectangle(Lorg/apache/pdfbox/pdmodel/common/PDRectangle;)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotation;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->RECT:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->getCOSArray()Lorg/apache/pdfbox/cos/COSArray;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    return-void
.end method

.method public setStructParent(I)V
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotation;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->STRUCT_PARENT:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setInt(Lorg/apache/pdfbox/cos/COSName;I)V

    return-void
.end method

.method public setToggleNoView(Z)V
    .locals 3

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotation;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->F:Lorg/apache/pdfbox/cos/COSName;

    const/16 v2, 0x100

    invoke-virtual {v0, v1, v2, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setFlag(Lorg/apache/pdfbox/cos/COSName;IZ)V

    return-void
.end method

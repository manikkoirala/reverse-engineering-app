.class public Lorg/apache/pdfbox/pdmodel/interactive/measurement/PDViewportDictionary;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/pdfbox/pdmodel/common/COSObjectable;


# static fields
.field public static final TYPE:Ljava/lang/String; = "Viewport"


# instance fields
.field private viewportDictionary:Lorg/apache/pdfbox/cos/COSDictionary;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-direct {v0}, Lorg/apache/pdfbox/cos/COSDictionary;-><init>()V

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/measurement/PDViewportDictionary;->viewportDictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    return-void
.end method

.method public constructor <init>(Lorg/apache/pdfbox/cos/COSDictionary;)V
    .locals 0

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/interactive/measurement/PDViewportDictionary;->viewportDictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    return-void
.end method


# virtual methods
.method public getBBox()Lorg/apache/pdfbox/pdmodel/common/PDRectangle;
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/measurement/PDViewportDictionary;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    const-string v1, "BBox"

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Ljava/lang/String;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSArray;

    if-eqz v0, :cond_0

    new-instance v1, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;

    invoke-direct {v1, v0}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;-><init>(Lorg/apache/pdfbox/cos/COSArray;)V

    return-object v1

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public getCOSObject()Lorg/apache/pdfbox/cos/COSBase;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/measurement/PDViewportDictionary;->viewportDictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    return-object v0
.end method

.method public getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/measurement/PDViewportDictionary;->viewportDictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    return-object v0
.end method

.method public getMeasure()Lorg/apache/pdfbox/pdmodel/interactive/measurement/PDMeasureDictionary;
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/measurement/PDViewportDictionary;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    const-string v1, "Measure"

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Ljava/lang/String;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSDictionary;

    if-eqz v0, :cond_0

    new-instance v1, Lorg/apache/pdfbox/pdmodel/interactive/measurement/PDMeasureDictionary;

    invoke-direct {v1, v0}, Lorg/apache/pdfbox/pdmodel/interactive/measurement/PDMeasureDictionary;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    return-object v1

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/measurement/PDViewportDictionary;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->NAME:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getNameAsString(Lorg/apache/pdfbox/cos/COSName;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getType()Ljava/lang/String;
    .locals 1

    const-string v0, "Viewport"

    return-object v0
.end method

.method public setBBox(Lorg/apache/pdfbox/pdmodel/common/PDRectangle;)V
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/measurement/PDViewportDictionary;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    const-string v1, "BBox"

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Ljava/lang/String;Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)V

    return-void
.end method

.method public setMeasure(Lorg/apache/pdfbox/pdmodel/interactive/measurement/PDMeasureDictionary;)V
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/measurement/PDViewportDictionary;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    const-string v1, "Measure"

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Ljava/lang/String;Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)V

    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/measurement/PDViewportDictionary;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->NAME:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setName(Lorg/apache/pdfbox/cos/COSName;Ljava/lang/String;)V

    return-void
.end method

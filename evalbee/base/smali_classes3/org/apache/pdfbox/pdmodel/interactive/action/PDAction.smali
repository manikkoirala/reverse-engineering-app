.class public abstract Lorg/apache/pdfbox/pdmodel/interactive/action/PDAction;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/pdfbox/pdmodel/common/PDDestinationOrAction;


# static fields
.field public static final TYPE:Ljava/lang/String; = "Action"


# instance fields
.field protected action:Lorg/apache/pdfbox/cos/COSDictionary;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-direct {v0}, Lorg/apache/pdfbox/cos/COSDictionary;-><init>()V

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/action/PDAction;->action:Lorg/apache/pdfbox/cos/COSDictionary;

    const-string v0, "Action"

    invoke-virtual {p0, v0}, Lorg/apache/pdfbox/pdmodel/interactive/action/PDAction;->setType(Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Lorg/apache/pdfbox/cos/COSDictionary;)V
    .locals 0

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/interactive/action/PDAction;->action:Lorg/apache/pdfbox/cos/COSDictionary;

    return-void
.end method


# virtual methods
.method public getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/action/PDAction;->action:Lorg/apache/pdfbox/cos/COSDictionary;

    return-object v0
.end method

.method public getCOSObject()Lorg/apache/pdfbox/cos/COSBase;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/action/PDAction;->action:Lorg/apache/pdfbox/cos/COSDictionary;

    return-object v0
.end method

.method public getNext()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lorg/apache/pdfbox/pdmodel/interactive/action/PDAction;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/action/PDAction;->action:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->NEXT:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    instance-of v2, v0, Lorg/apache/pdfbox/cos/COSDictionary;

    if-eqz v2, :cond_0

    move-object v2, v0

    check-cast v2, Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-static {v2}, Lorg/apache/pdfbox/pdmodel/interactive/action/PDActionFactory;->createAction(Lorg/apache/pdfbox/cos/COSDictionary;)Lorg/apache/pdfbox/pdmodel/interactive/action/PDAction;

    move-result-object v2

    new-instance v3, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;

    iget-object v4, p0, Lorg/apache/pdfbox/pdmodel/interactive/action/PDAction;->action:Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-direct {v3, v2, v0, v4, v1}, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;-><init>(Ljava/lang/Object;Lorg/apache/pdfbox/cos/COSBase;Lorg/apache/pdfbox/cos/COSDictionary;Lorg/apache/pdfbox/cos/COSName;)V

    goto :goto_1

    :cond_0
    instance-of v1, v0, Lorg/apache/pdfbox/cos/COSArray;

    if-eqz v1, :cond_2

    check-cast v0, Lorg/apache/pdfbox/cos/COSArray;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    const/4 v2, 0x0

    :goto_0
    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSArray;->size()I

    move-result v3

    if-ge v2, v3, :cond_1

    invoke-virtual {v0, v2}, Lorg/apache/pdfbox/cos/COSArray;->getObject(I)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v3

    check-cast v3, Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-static {v3}, Lorg/apache/pdfbox/pdmodel/interactive/action/PDActionFactory;->createAction(Lorg/apache/pdfbox/cos/COSDictionary;)Lorg/apache/pdfbox/pdmodel/interactive/action/PDAction;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    new-instance v3, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;

    invoke-direct {v3, v1, v0}, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;-><init>(Ljava/util/List;Lorg/apache/pdfbox/cos/COSArray;)V

    goto :goto_1

    :cond_2
    const/4 v3, 0x0

    :goto_1
    return-object v3
.end method

.method public getSubType()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/action/PDAction;->action:Lorg/apache/pdfbox/cos/COSDictionary;

    const-string v1, "S"

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getNameAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getType()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/action/PDAction;->action:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->TYPE:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getNameAsString(Lorg/apache/pdfbox/cos/COSName;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public setNext(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "*>;)V"
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/action/PDAction;->action:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->NEXT:Lorg/apache/pdfbox/cos/COSName;

    invoke-static {p1}, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;->converterToCOSArray(Ljava/util/List;)Lorg/apache/pdfbox/cos/COSArray;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    return-void
.end method

.method public setSubType(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/action/PDAction;->action:Lorg/apache/pdfbox/cos/COSDictionary;

    const-string v1, "S"

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setName(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public final setType(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/action/PDAction;->action:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->TYPE:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setName(Lorg/apache/pdfbox/cos/COSName;Ljava/lang/String;)V

    return-void
.end method

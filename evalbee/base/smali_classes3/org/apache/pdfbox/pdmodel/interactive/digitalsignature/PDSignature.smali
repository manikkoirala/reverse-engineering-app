.class public Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/PDSignature;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/pdfbox/pdmodel/common/COSObjectable;


# static fields
.field public static final FILTER_ADOBE_PPKLITE:Lorg/apache/pdfbox/cos/COSName;

.field public static final FILTER_CICI_SIGNIT:Lorg/apache/pdfbox/cos/COSName;

.field public static final FILTER_ENTRUST_PPKEF:Lorg/apache/pdfbox/cos/COSName;

.field public static final FILTER_VERISIGN_PPKVS:Lorg/apache/pdfbox/cos/COSName;

.field public static final SUBFILTER_ADBE_PKCS7_DETACHED:Lorg/apache/pdfbox/cos/COSName;

.field public static final SUBFILTER_ADBE_PKCS7_SHA1:Lorg/apache/pdfbox/cos/COSName;

.field public static final SUBFILTER_ADBE_X509_RSA_SHA1:Lorg/apache/pdfbox/cos/COSName;

.field public static final SUBFILTER_ETSI_CADES_DETACHED:Lorg/apache/pdfbox/cos/COSName;


# instance fields
.field private final dictionary:Lorg/apache/pdfbox/cos/COSDictionary;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->ADOBE_PPKLITE:Lorg/apache/pdfbox/cos/COSName;

    sput-object v0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/PDSignature;->FILTER_ADOBE_PPKLITE:Lorg/apache/pdfbox/cos/COSName;

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->ENTRUST_PPKEF:Lorg/apache/pdfbox/cos/COSName;

    sput-object v0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/PDSignature;->FILTER_ENTRUST_PPKEF:Lorg/apache/pdfbox/cos/COSName;

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->CICI_SIGNIT:Lorg/apache/pdfbox/cos/COSName;

    sput-object v0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/PDSignature;->FILTER_CICI_SIGNIT:Lorg/apache/pdfbox/cos/COSName;

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->VERISIGN_PPKVS:Lorg/apache/pdfbox/cos/COSName;

    sput-object v0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/PDSignature;->FILTER_VERISIGN_PPKVS:Lorg/apache/pdfbox/cos/COSName;

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->ADBE_X509_RSA_SHA1:Lorg/apache/pdfbox/cos/COSName;

    sput-object v0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/PDSignature;->SUBFILTER_ADBE_X509_RSA_SHA1:Lorg/apache/pdfbox/cos/COSName;

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->ADBE_PKCS7_DETACHED:Lorg/apache/pdfbox/cos/COSName;

    sput-object v0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/PDSignature;->SUBFILTER_ADBE_PKCS7_DETACHED:Lorg/apache/pdfbox/cos/COSName;

    const-string v0, "ETSI.CAdES.detached"

    invoke-static {v0}, Lorg/apache/pdfbox/cos/COSName;->getPDFName(Ljava/lang/String;)Lorg/apache/pdfbox/cos/COSName;

    move-result-object v0

    sput-object v0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/PDSignature;->SUBFILTER_ETSI_CADES_DETACHED:Lorg/apache/pdfbox/cos/COSName;

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->ADBE_PKCS7_SHA1:Lorg/apache/pdfbox/cos/COSName;

    sput-object v0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/PDSignature;->SUBFILTER_ADBE_PKCS7_SHA1:Lorg/apache/pdfbox/cos/COSName;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-direct {v0}, Lorg/apache/pdfbox/cos/COSDictionary;-><init>()V

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/PDSignature;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->TYPE:Lorg/apache/pdfbox/cos/COSName;

    sget-object v2, Lorg/apache/pdfbox/cos/COSName;->SIG:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    return-void
.end method

.method public constructor <init>(Lorg/apache/pdfbox/cos/COSDictionary;)V
    .locals 0

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/PDSignature;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    return-void
.end method

.method private getContents(Lorg/apache/pdfbox/pdfwriter/COSFilterInputStream;)[B
    .locals 7

    .line 2
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    const/16 v1, 0x400

    invoke-direct {v0, v1}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    new-array v1, v1, [B

    :goto_0
    invoke-virtual {p1, v1}, Lorg/apache/pdfbox/pdfwriter/COSFilterInputStream;->read([B)I

    move-result v2

    const/4 v3, -0x1

    if-eq v2, v3, :cond_4

    const/4 v3, 0x0

    aget-byte v4, v1, v3

    const/16 v5, 0x3c

    if-eq v4, v5, :cond_2

    const/16 v5, 0x28

    if-ne v4, v5, :cond_0

    goto :goto_1

    :cond_0
    add-int/lit8 v4, v2, -0x1

    aget-byte v5, v1, v4

    const/16 v6, 0x3e

    if-eq v5, v6, :cond_1

    const/16 v6, 0x29

    if-ne v5, v6, :cond_3

    :cond_1
    invoke-virtual {v0, v1, v3, v4}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    goto :goto_0

    :cond_2
    :goto_1
    const/4 v3, 0x1

    :cond_3
    invoke-virtual {v0, v1, v3, v2}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    goto :goto_0

    :cond_4
    invoke-virtual {p1}, Ljava/io/InputStream;->close()V

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lorg/apache/pdfbox/cos/COSString;->parseHex(Ljava/lang/String;)Lorg/apache/pdfbox/cos/COSString;

    move-result-object p1

    invoke-virtual {p1}, Lorg/apache/pdfbox/cos/COSString;->getBytes()[B

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public getByteRange()[I
    .locals 5

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/PDSignature;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->BYTERANGE:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSArray;

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSArray;->size()I

    move-result v1

    new-array v2, v1, [I

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v1, :cond_0

    invoke-virtual {v0, v3}, Lorg/apache/pdfbox/cos/COSArray;->getInt(I)I

    move-result v4

    aput v4, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_0
    return-object v2
.end method

.method public getCOSObject()Lorg/apache/pdfbox/cos/COSBase;
    .locals 1

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/PDSignature;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    return-object v0
.end method

.method public getContactInfo()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/PDSignature;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->CONTACT_INFO:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getString(Lorg/apache/pdfbox/cos/COSName;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getContents(Ljava/io/InputStream;)[B
    .locals 4

    .line 1
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/PDSignature;->getByteRange()[I

    move-result-object v0

    const/4 v1, 0x0

    aget v1, v0, v1

    const/4 v2, 0x1

    aget v3, v0, v2

    add-int/2addr v1, v3

    add-int/2addr v1, v2

    const/4 v2, 0x2

    aget v0, v0, v2

    sub-int/2addr v0, v1

    new-instance v2, Lorg/apache/pdfbox/pdfwriter/COSFilterInputStream;

    filled-new-array {v1, v0}, [I

    move-result-object v0

    invoke-direct {v2, p1, v0}, Lorg/apache/pdfbox/pdfwriter/COSFilterInputStream;-><init>(Ljava/io/InputStream;[I)V

    invoke-direct {p0, v2}, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/PDSignature;->getContents(Lorg/apache/pdfbox/pdfwriter/COSFilterInputStream;)[B

    move-result-object p1

    return-object p1
.end method

.method public getContents([B)[B
    .locals 4

    .line 3
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/PDSignature;->getByteRange()[I

    move-result-object v0

    const/4 v1, 0x0

    aget v1, v0, v1

    const/4 v2, 0x1

    aget v3, v0, v2

    add-int/2addr v1, v3

    add-int/2addr v1, v2

    const/4 v2, 0x2

    aget v0, v0, v2

    sub-int/2addr v0, v1

    new-instance v2, Lorg/apache/pdfbox/pdfwriter/COSFilterInputStream;

    filled-new-array {v1, v0}, [I

    move-result-object v0

    invoke-direct {v2, p1, v0}, Lorg/apache/pdfbox/pdfwriter/COSFilterInputStream;-><init>([B[I)V

    invoke-direct {p0, v2}, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/PDSignature;->getContents(Lorg/apache/pdfbox/pdfwriter/COSFilterInputStream;)[B

    move-result-object p1

    return-object p1
.end method

.method public getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/PDSignature;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    return-object v0
.end method

.method public getFilter()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/PDSignature;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->FILTER:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getNameAsString(Lorg/apache/pdfbox/cos/COSName;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getLocation()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/PDSignature;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->LOCATION:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getString(Lorg/apache/pdfbox/cos/COSName;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/PDSignature;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->NAME:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getString(Lorg/apache/pdfbox/cos/COSName;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPropBuild()Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/PDPropBuild;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/PDSignature;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->PROP_BUILD:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSDictionary;

    if-eqz v0, :cond_0

    new-instance v1, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/PDPropBuild;

    invoke-direct {v1, v0}, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/PDPropBuild;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return-object v1
.end method

.method public getReason()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/PDSignature;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->REASON:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getString(Lorg/apache/pdfbox/cos/COSName;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSignDate()Ljava/util/Calendar;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/PDSignature;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->M:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDate(Lorg/apache/pdfbox/cos/COSName;)Ljava/util/Calendar;

    move-result-object v0

    return-object v0
.end method

.method public getSignedContent(Ljava/io/InputStream;)[B
    .locals 3

    .line 1
    const/4 v0, 0x0

    :try_start_0
    new-instance v1, Lorg/apache/pdfbox/pdfwriter/COSFilterInputStream;

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/PDSignature;->getByteRange()[I

    move-result-object v2

    invoke-direct {v1, p1, v2}, Lorg/apache/pdfbox/pdfwriter/COSFilterInputStream;-><init>(Ljava/io/InputStream;[I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    invoke-virtual {v1}, Lorg/apache/pdfbox/pdfwriter/COSFilterInputStream;->toByteArray()[B

    move-result-object p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    return-object p1

    :catchall_0
    move-exception p1

    move-object v0, v1

    goto :goto_0

    :catchall_1
    move-exception p1

    :goto_0
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    :cond_0
    throw p1
.end method

.method public getSignedContent([B)[B
    .locals 3

    .line 2
    const/4 v0, 0x0

    :try_start_0
    new-instance v1, Lorg/apache/pdfbox/pdfwriter/COSFilterInputStream;

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/PDSignature;->getByteRange()[I

    move-result-object v2

    invoke-direct {v1, p1, v2}, Lorg/apache/pdfbox/pdfwriter/COSFilterInputStream;-><init>([B[I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    invoke-virtual {v1}, Lorg/apache/pdfbox/pdfwriter/COSFilterInputStream;->toByteArray()[B

    move-result-object p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    return-object p1

    :catchall_0
    move-exception p1

    move-object v0, v1

    goto :goto_0

    :catchall_1
    move-exception p1

    :goto_0
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    :cond_0
    throw p1
.end method

.method public getSubFilter()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/PDSignature;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->SUB_FILTER:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getNameAsString(Lorg/apache/pdfbox/cos/COSName;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public setByteRange([I)V
    .locals 5

    array-length v0, p1

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    return-void

    :cond_0
    new-instance v0, Lorg/apache/pdfbox/cos/COSArray;

    invoke-direct {v0}, Lorg/apache/pdfbox/cos/COSArray;-><init>()V

    array-length v1, p1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_1

    aget v3, p1, v2

    int-to-long v3, v3

    invoke-static {v3, v4}, Lorg/apache/pdfbox/cos/COSInteger;->get(J)Lorg/apache/pdfbox/cos/COSInteger;

    move-result-object v3

    invoke-virtual {v0, v3}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    iget-object p1, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/PDSignature;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->BYTERANGE:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p1, v1, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    return-void
.end method

.method public setContactInfo(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/PDSignature;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->CONTACT_INFO:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setString(Lorg/apache/pdfbox/cos/COSName;Ljava/lang/String;)V

    return-void
.end method

.method public setContents([B)V
    .locals 2

    new-instance v0, Lorg/apache/pdfbox/cos/COSString;

    invoke-direct {v0, p1}, Lorg/apache/pdfbox/cos/COSString;-><init>([B)V

    const/4 p1, 0x1

    invoke-virtual {v0, p1}, Lorg/apache/pdfbox/cos/COSString;->setForceHexForm(Z)V

    iget-object p1, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/PDSignature;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->CONTENTS:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p1, v1, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    return-void
.end method

.method public setFilter(Lorg/apache/pdfbox/cos/COSName;)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/PDSignature;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->FILTER:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    return-void
.end method

.method public setLocation(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/PDSignature;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->LOCATION:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setString(Lorg/apache/pdfbox/cos/COSName;Ljava/lang/String;)V

    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/PDSignature;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->NAME:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setString(Lorg/apache/pdfbox/cos/COSName;Ljava/lang/String;)V

    return-void
.end method

.method public setPropBuild(Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/PDPropBuild;)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/PDSignature;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->PROP_BUILD:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)V

    return-void
.end method

.method public setReason(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/PDSignature;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->REASON:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setString(Lorg/apache/pdfbox/cos/COSName;Ljava/lang/String;)V

    return-void
.end method

.method public setSignDate(Ljava/util/Calendar;)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/PDSignature;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->M:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setDate(Lorg/apache/pdfbox/cos/COSName;Ljava/util/Calendar;)V

    return-void
.end method

.method public setSubFilter(Lorg/apache/pdfbox/cos/COSName;)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/PDSignature;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->SUB_FILTER:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    return-void
.end method

.method public setType(Lorg/apache/pdfbox/cos/COSName;)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/PDSignature;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->TYPE:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    return-void
.end method

.class public Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAppearanceEntry;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/pdfbox/pdmodel/common/COSObjectable;


# instance fields
.field private entry:Lorg/apache/pdfbox/cos/COSBase;


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(Lorg/apache/pdfbox/cos/COSBase;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAppearanceEntry;->entry:Lorg/apache/pdfbox/cos/COSBase;

    return-void
.end method


# virtual methods
.method public getAppearanceStream()Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAppearanceStream;
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAppearanceEntry;->isStream()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAppearanceStream;

    iget-object v1, p0, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAppearanceEntry;->entry:Lorg/apache/pdfbox/cos/COSBase;

    check-cast v1, Lorg/apache/pdfbox/cos/COSStream;

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAppearanceStream;-><init>(Lorg/apache/pdfbox/cos/COSStream;)V

    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0
.end method

.method public getCOSObject()Lorg/apache/pdfbox/cos/COSBase;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAppearanceEntry;->entry:Lorg/apache/pdfbox/cos/COSBase;

    return-object v0
.end method

.method public getSubDictionary()Ljava/util/Map;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Lorg/apache/pdfbox/cos/COSName;",
            "Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAppearanceStream;",
            ">;"
        }
    .end annotation

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAppearanceEntry;->isSubDictionary()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAppearanceEntry;->entry:Lorg/apache/pdfbox/cos/COSBase;

    check-cast v0, Lorg/apache/pdfbox/cos/COSDictionary;

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSDictionary;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v3}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v4

    instance-of v5, v4, Lorg/apache/pdfbox/cos/COSStream;

    if-eqz v5, :cond_0

    new-instance v5, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAppearanceStream;

    check-cast v4, Lorg/apache/pdfbox/cos/COSStream;

    invoke-direct {v5, v4}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAppearanceStream;-><init>(Lorg/apache/pdfbox/cos/COSStream;)V

    invoke-interface {v1, v3, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_1
    new-instance v2, Lorg/apache/pdfbox/pdmodel/common/COSDictionaryMap;

    invoke-direct {v2, v1, v0}, Lorg/apache/pdfbox/pdmodel/common/COSDictionaryMap;-><init>(Ljava/util/Map;Lorg/apache/pdfbox/cos/COSDictionary;)V

    return-object v2

    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0
.end method

.method public isStream()Z
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAppearanceEntry;->entry:Lorg/apache/pdfbox/cos/COSBase;

    instance-of v0, v0, Lorg/apache/pdfbox/cos/COSStream;

    return v0
.end method

.method public isSubDictionary()Z
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAppearanceEntry;->entry:Lorg/apache/pdfbox/cos/COSBase;

    instance-of v0, v0, Lorg/apache/pdfbox/cos/COSStream;

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.class public final Lorg/apache/pdfbox/pdmodel/interactive/form/PDXFAResource;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/pdfbox/pdmodel/common/COSObjectable;


# instance fields
.field private xfa:Lorg/apache/pdfbox/cos/COSBase;


# direct methods
.method public constructor <init>(Lorg/apache/pdfbox/cos/COSBase;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/PDXFAResource;->xfa:Lorg/apache/pdfbox/cos/COSBase;

    return-void
.end method


# virtual methods
.method public getBytes()[B
    .locals 10

    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    const/4 v1, 0x0

    :try_start_0
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDXFAResource;->getCOSObject()Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v2

    instance-of v2, v2, Lorg/apache/pdfbox/cos/COSArray;

    const/4 v3, -0x1

    const/16 v4, 0x400

    const/4 v5, 0x0

    if-eqz v2, :cond_2

    new-array v2, v4, [B

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDXFAResource;->getCOSObject()Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v6

    check-cast v6, Lorg/apache/pdfbox/cos/COSArray;

    const/4 v7, 0x1

    :goto_0
    invoke-virtual {v6}, Lorg/apache/pdfbox/cos/COSArray;->size()I

    move-result v8

    if-ge v7, v8, :cond_4

    invoke-virtual {v6, v7}, Lorg/apache/pdfbox/cos/COSArray;->getObject(I)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v8

    instance-of v9, v8, Lorg/apache/pdfbox/cos/COSStream;

    if-eqz v9, :cond_1

    check-cast v8, Lorg/apache/pdfbox/cos/COSStream;

    invoke-virtual {v8}, Lorg/apache/pdfbox/cos/COSStream;->getUnfilteredStream()Ljava/io/InputStream;

    move-result-object v1

    :goto_1
    invoke-virtual {v1, v2, v5, v4}, Ljava/io/InputStream;->read([BII)I

    move-result v8

    if-eq v8, v3, :cond_0

    invoke-virtual {v0, v2, v5, v8}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    goto :goto_1

    :cond_0
    invoke-virtual {v0}, Ljava/io/OutputStream;->flush()V

    :cond_1
    add-int/lit8 v7, v7, 0x2

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/PDXFAResource;->xfa:Lorg/apache/pdfbox/cos/COSBase;

    invoke-virtual {v2}, Lorg/apache/pdfbox/cos/COSBase;->getCOSObject()Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v2

    instance-of v2, v2, Lorg/apache/pdfbox/cos/COSStream;

    if-eqz v2, :cond_4

    new-array v2, v4, [B

    iget-object v6, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/PDXFAResource;->xfa:Lorg/apache/pdfbox/cos/COSBase;

    invoke-virtual {v6}, Lorg/apache/pdfbox/cos/COSBase;->getCOSObject()Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v6

    check-cast v6, Lorg/apache/pdfbox/cos/COSStream;

    invoke-virtual {v6}, Lorg/apache/pdfbox/cos/COSStream;->getUnfilteredStream()Ljava/io/InputStream;

    move-result-object v1

    :goto_2
    invoke-virtual {v1, v2, v5, v4}, Ljava/io/InputStream;->read([BII)I

    move-result v6

    if-eq v6, v3, :cond_3

    invoke-virtual {v0, v2, v5, v6}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    goto :goto_2

    :cond_3
    invoke-virtual {v0}, Ljava/io/OutputStream;->flush()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_4
    if-eqz v1, :cond_5

    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    :cond_5
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->close()V

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    return-object v0

    :catchall_0
    move-exception v2

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    :cond_6
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->close()V

    throw v2
.end method

.method public getCOSObject()Lorg/apache/pdfbox/cos/COSBase;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/PDXFAResource;->xfa:Lorg/apache/pdfbox/cos/COSBase;

    return-object v0
.end method

.method public getDocument()Lorg/w3c/dom/Document;
    .locals 3

    invoke-static {}, Ljavax/xml/parsers/DocumentBuilderFactory;->newInstance()Ljavax/xml/parsers/DocumentBuilderFactory;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljavax/xml/parsers/DocumentBuilderFactory;->setNamespaceAware(Z)V

    invoke-virtual {v0}, Ljavax/xml/parsers/DocumentBuilderFactory;->newDocumentBuilder()Ljavax/xml/parsers/DocumentBuilder;

    move-result-object v0

    new-instance v1, Ljava/io/ByteArrayInputStream;

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDXFAResource;->getBytes()[B

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-virtual {v0, v1}, Ljavax/xml/parsers/DocumentBuilder;->parse(Ljava/io/InputStream;)Lorg/w3c/dom/Document;

    move-result-object v0

    return-object v0
.end method

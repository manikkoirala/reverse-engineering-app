.class Lorg/apache/pdfbox/pdmodel/interactive/form/FieldUtils$KeyValueKeyComparator;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Serializable;
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/pdfbox/pdmodel/interactive/form/FieldUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "KeyValueKeyComparator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/io/Serializable;",
        "Ljava/util/Comparator<",
        "Lorg/apache/pdfbox/pdmodel/interactive/form/FieldUtils$KeyValue;",
        ">;"
    }
.end annotation


# static fields
.field private static final serialVersionUID:J = 0x5d31c43e033872ceL


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 0

    .line 1
    check-cast p1, Lorg/apache/pdfbox/pdmodel/interactive/form/FieldUtils$KeyValue;

    check-cast p2, Lorg/apache/pdfbox/pdmodel/interactive/form/FieldUtils$KeyValue;

    invoke-virtual {p0, p1, p2}, Lorg/apache/pdfbox/pdmodel/interactive/form/FieldUtils$KeyValueKeyComparator;->compare(Lorg/apache/pdfbox/pdmodel/interactive/form/FieldUtils$KeyValue;Lorg/apache/pdfbox/pdmodel/interactive/form/FieldUtils$KeyValue;)I

    move-result p1

    return p1
.end method

.method public compare(Lorg/apache/pdfbox/pdmodel/interactive/form/FieldUtils$KeyValue;Lorg/apache/pdfbox/pdmodel/interactive/form/FieldUtils$KeyValue;)I
    .locals 0

    .line 2
    invoke-static {p1}, Lorg/apache/pdfbox/pdmodel/interactive/form/FieldUtils$KeyValue;->access$000(Lorg/apache/pdfbox/pdmodel/interactive/form/FieldUtils$KeyValue;)Ljava/lang/String;

    move-result-object p1

    invoke-static {p2}, Lorg/apache/pdfbox/pdmodel/interactive/form/FieldUtils$KeyValue;->access$000(Lorg/apache/pdfbox/pdmodel/interactive/form/FieldUtils$KeyValue;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result p1

    return p1
.end method

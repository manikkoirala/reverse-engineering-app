.class public Lorg/apache/pdfbox/pdmodel/interactive/action/PDActionLaunch;
.super Lorg/apache/pdfbox/pdmodel/interactive/action/PDAction;
.source "SourceFile"


# static fields
.field public static final SUB_TYPE:Ljava/lang/String; = "Launch"


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/interactive/action/PDAction;-><init>()V

    const-string v0, "Launch"

    invoke-virtual {p0, v0}, Lorg/apache/pdfbox/pdmodel/interactive/action/PDAction;->setSubType(Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Lorg/apache/pdfbox/cos/COSDictionary;)V
    .locals 0

    .line 2
    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/interactive/action/PDAction;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    return-void
.end method


# virtual methods
.method public getD()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/action/PDAction;->action:Lorg/apache/pdfbox/cos/COSDictionary;

    const-string v1, "D"

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getF()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/action/PDAction;->action:Lorg/apache/pdfbox/cos/COSDictionary;

    const-string v1, "F"

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getFile()Lorg/apache/pdfbox/pdmodel/common/filespecification/PDFileSpecification;
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/action/PDAction;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    const-string v1, "F"

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Ljava/lang/String;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/pdfbox/pdmodel/common/filespecification/PDFileSpecification;->createFS(Lorg/apache/pdfbox/cos/COSBase;)Lorg/apache/pdfbox/pdmodel/common/filespecification/PDFileSpecification;

    move-result-object v0

    return-object v0
.end method

.method public getO()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/action/PDAction;->action:Lorg/apache/pdfbox/cos/COSDictionary;

    const-string v1, "O"

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getP()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/action/PDAction;->action:Lorg/apache/pdfbox/cos/COSDictionary;

    const-string v1, "P"

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getWinLaunchParams()Lorg/apache/pdfbox/pdmodel/interactive/action/PDWindowsLaunchParams;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/action/PDAction;->action:Lorg/apache/pdfbox/cos/COSDictionary;

    const-string v1, "Win"

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Ljava/lang/String;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSDictionary;

    if-eqz v0, :cond_0

    new-instance v1, Lorg/apache/pdfbox/pdmodel/interactive/action/PDWindowsLaunchParams;

    invoke-direct {v1, v0}, Lorg/apache/pdfbox/pdmodel/interactive/action/PDWindowsLaunchParams;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return-object v1
.end method

.method public setD(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/action/PDAction;->action:Lorg/apache/pdfbox/cos/COSDictionary;

    const-string v1, "D"

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setString(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public setF(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/action/PDAction;->action:Lorg/apache/pdfbox/cos/COSDictionary;

    const-string v1, "F"

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setString(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public setFile(Lorg/apache/pdfbox/pdmodel/common/filespecification/PDFileSpecification;)V
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/action/PDAction;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    const-string v1, "F"

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Ljava/lang/String;Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)V

    return-void
.end method

.method public setO(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/action/PDAction;->action:Lorg/apache/pdfbox/cos/COSDictionary;

    const-string v1, "O"

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setString(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public setOpenInNewWindow(Z)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/action/PDAction;->action:Lorg/apache/pdfbox/cos/COSDictionary;

    const-string v1, "NewWindow"

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setBoolean(Ljava/lang/String;Z)V

    return-void
.end method

.method public setP(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/action/PDAction;->action:Lorg/apache/pdfbox/cos/COSDictionary;

    const-string v1, "P"

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setString(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public setWinLaunchParams(Lorg/apache/pdfbox/pdmodel/interactive/action/PDWindowsLaunchParams;)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/action/PDAction;->action:Lorg/apache/pdfbox/cos/COSDictionary;

    const-string v1, "Win"

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Ljava/lang/String;Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)V

    return-void
.end method

.method public shouldOpenInNewWindow()Z
    .locals 3

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/action/PDAction;->action:Lorg/apache/pdfbox/cos/COSDictionary;

    const-string v1, "NewWindow"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

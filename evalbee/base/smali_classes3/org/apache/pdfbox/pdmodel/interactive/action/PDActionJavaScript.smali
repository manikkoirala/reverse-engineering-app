.class public Lorg/apache/pdfbox/pdmodel/interactive/action/PDActionJavaScript;
.super Lorg/apache/pdfbox/pdmodel/interactive/action/PDAction;
.source "SourceFile"


# static fields
.field public static final SUB_TYPE:Ljava/lang/String; = "JavaScript"


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/interactive/action/PDAction;-><init>()V

    const-string v0, "JavaScript"

    invoke-virtual {p0, v0}, Lorg/apache/pdfbox/pdmodel/interactive/action/PDAction;->setSubType(Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .line 2
    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/interactive/action/PDActionJavaScript;-><init>()V

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/pdmodel/interactive/action/PDActionJavaScript;->setAction(Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Lorg/apache/pdfbox/cos/COSDictionary;)V
    .locals 0

    .line 3
    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/interactive/action/PDAction;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    return-void
.end method


# virtual methods
.method public getAction()Lorg/apache/pdfbox/pdmodel/common/PDTextStream;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/action/PDAction;->action:Lorg/apache/pdfbox/cos/COSDictionary;

    const-string v1, "JS"

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Ljava/lang/String;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/pdfbox/pdmodel/common/PDTextStream;->createTextStream(Lorg/apache/pdfbox/cos/COSBase;)Lorg/apache/pdfbox/pdmodel/common/PDTextStream;

    move-result-object v0

    return-object v0
.end method

.method public setAction(Ljava/lang/String;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/action/PDAction;->action:Lorg/apache/pdfbox/cos/COSDictionary;

    const-string v1, "JS"

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setString(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public setAction(Lorg/apache/pdfbox/pdmodel/common/PDTextStream;)V
    .locals 2

    .line 2
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/action/PDAction;->action:Lorg/apache/pdfbox/cos/COSDictionary;

    const-string v1, "JS"

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Ljava/lang/String;Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)V

    return-void
.end method

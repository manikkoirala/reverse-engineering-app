.class public Lorg/apache/pdfbox/pdmodel/interactive/action/PDActionNamed;
.super Lorg/apache/pdfbox/pdmodel/interactive/action/PDAction;
.source "SourceFile"


# static fields
.field public static final SUB_TYPE:Ljava/lang/String; = "Named"


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/interactive/action/PDAction;-><init>()V

    new-instance v0, Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-direct {v0}, Lorg/apache/pdfbox/cos/COSDictionary;-><init>()V

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/action/PDAction;->action:Lorg/apache/pdfbox/cos/COSDictionary;

    const-string v0, "Named"

    invoke-virtual {p0, v0}, Lorg/apache/pdfbox/pdmodel/interactive/action/PDAction;->setSubType(Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Lorg/apache/pdfbox/cos/COSDictionary;)V
    .locals 0

    .line 2
    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/interactive/action/PDAction;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    return-void
.end method


# virtual methods
.method public getN()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/action/PDAction;->action:Lorg/apache/pdfbox/cos/COSDictionary;

    const-string v1, "N"

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getNameAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public setN(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/action/PDAction;->action:Lorg/apache/pdfbox/cos/COSDictionary;

    const-string v1, "N"

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setName(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.class public Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAppearanceDictionary;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/pdfbox/pdmodel/common/COSObjectable;


# instance fields
.field private final dictionary:Lorg/apache/pdfbox/cos/COSDictionary;


# direct methods
.method public constructor <init>()V
    .locals 3

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-direct {v0}, Lorg/apache/pdfbox/cos/COSDictionary;-><init>()V

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAppearanceDictionary;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->N:Lorg/apache/pdfbox/cos/COSName;

    new-instance v2, Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-direct {v2}, Lorg/apache/pdfbox/cos/COSDictionary;-><init>()V

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    return-void
.end method

.method public constructor <init>(Lorg/apache/pdfbox/cos/COSDictionary;)V
    .locals 0

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAppearanceDictionary;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    return-void
.end method


# virtual methods
.method public bridge synthetic getCOSObject()Lorg/apache/pdfbox/cos/COSBase;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAppearanceDictionary;->getCOSObject()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    return-object v0
.end method

.method public getCOSObject()Lorg/apache/pdfbox/cos/COSDictionary;
    .locals 1

    .line 2
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAppearanceDictionary;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    return-object v0
.end method

.method public getDownAppearance()Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAppearanceEntry;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAppearanceDictionary;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->D:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAppearanceDictionary;->getNormalAppearance()Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAppearanceEntry;

    move-result-object v0

    return-object v0

    :cond_0
    new-instance v1, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAppearanceEntry;

    invoke-direct {v1, v0}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAppearanceEntry;-><init>(Lorg/apache/pdfbox/cos/COSBase;)V

    return-object v1
.end method

.method public getNormalAppearance()Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAppearanceEntry;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAppearanceDictionary;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->N:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    :cond_0
    new-instance v1, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAppearanceEntry;

    invoke-direct {v1, v0}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAppearanceEntry;-><init>(Lorg/apache/pdfbox/cos/COSBase;)V

    return-object v1
.end method

.method public getRolloverAppearance()Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAppearanceEntry;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAppearanceDictionary;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->R:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAppearanceDictionary;->getNormalAppearance()Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAppearanceEntry;

    move-result-object v0

    return-object v0

    :cond_0
    new-instance v1, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAppearanceEntry;

    invoke-direct {v1, v0}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAppearanceEntry;-><init>(Lorg/apache/pdfbox/cos/COSBase;)V

    return-object v1
.end method

.method public setDownAppearance(Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAppearanceEntry;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAppearanceDictionary;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->D:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)V

    return-void
.end method

.method public setDownAppearance(Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAppearanceStream;)V
    .locals 2

    .line 2
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAppearanceDictionary;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->D:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)V

    return-void
.end method

.method public setNormalAppearance(Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAppearanceEntry;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAppearanceDictionary;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->N:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)V

    return-void
.end method

.method public setNormalAppearance(Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAppearanceStream;)V
    .locals 2

    .line 2
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAppearanceDictionary;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->N:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)V

    return-void
.end method

.method public setRolloverAppearance(Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAppearanceEntry;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAppearanceDictionary;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->R:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)V

    return-void
.end method

.method public setRolloverAppearance(Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAppearanceStream;)V
    .locals 2

    .line 2
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAppearanceDictionary;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->R:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)V

    return-void
.end method

.class public Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/destination/PDPageFitRectangleDestination;
.super Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/destination/PDPageDestination;
.source "SourceFile"


# static fields
.field protected static final TYPE:Ljava/lang/String; = "FitR"


# direct methods
.method public constructor <init>()V
    .locals 3

    .line 1
    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/destination/PDPageDestination;-><init>()V

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/destination/PDPageDestination;->array:Lorg/apache/pdfbox/cos/COSArray;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSArray;->growToSize(I)V

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/destination/PDPageDestination;->array:Lorg/apache/pdfbox/cos/COSArray;

    const/4 v1, 0x1

    const-string v2, "FitR"

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSArray;->setName(ILjava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Lorg/apache/pdfbox/cos/COSArray;)V
    .locals 0

    .line 2
    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/destination/PDPageDestination;-><init>(Lorg/apache/pdfbox/cos/COSArray;)V

    return-void
.end method


# virtual methods
.method public getBottom()I
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/destination/PDPageDestination;->array:Lorg/apache/pdfbox/cos/COSArray;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSArray;->getInt(I)I

    move-result v0

    return v0
.end method

.method public getLeft()I
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/destination/PDPageDestination;->array:Lorg/apache/pdfbox/cos/COSArray;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSArray;->getInt(I)I

    move-result v0

    return v0
.end method

.method public getRight()I
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/destination/PDPageDestination;->array:Lorg/apache/pdfbox/cos/COSArray;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSArray;->getInt(I)I

    move-result v0

    return v0
.end method

.method public getTop()I
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/destination/PDPageDestination;->array:Lorg/apache/pdfbox/cos/COSArray;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSArray;->getInt(I)I

    move-result v0

    return v0
.end method

.method public setBottom(I)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/destination/PDPageDestination;->array:Lorg/apache/pdfbox/cos/COSArray;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSArray;->growToSize(I)V

    const/4 v0, -0x1

    const/4 v1, 0x3

    if-ne p1, v0, :cond_0

    iget-object p1, p0, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/destination/PDPageDestination;->array:Lorg/apache/pdfbox/cos/COSArray;

    const/4 v0, 0x0

    invoke-virtual {p1, v1, v0}, Lorg/apache/pdfbox/cos/COSArray;->set(ILorg/apache/pdfbox/cos/COSBase;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/destination/PDPageDestination;->array:Lorg/apache/pdfbox/cos/COSArray;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSArray;->setInt(II)V

    :goto_0
    return-void
.end method

.method public setLeft(I)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/destination/PDPageDestination;->array:Lorg/apache/pdfbox/cos/COSArray;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSArray;->growToSize(I)V

    const/4 v0, -0x1

    const/4 v1, 0x2

    if-ne p1, v0, :cond_0

    iget-object p1, p0, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/destination/PDPageDestination;->array:Lorg/apache/pdfbox/cos/COSArray;

    const/4 v0, 0x0

    invoke-virtual {p1, v1, v0}, Lorg/apache/pdfbox/cos/COSArray;->set(ILorg/apache/pdfbox/cos/COSBase;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/destination/PDPageDestination;->array:Lorg/apache/pdfbox/cos/COSArray;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSArray;->setInt(II)V

    :goto_0
    return-void
.end method

.method public setRight(I)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/destination/PDPageDestination;->array:Lorg/apache/pdfbox/cos/COSArray;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSArray;->growToSize(I)V

    const/4 v0, -0x1

    const/4 v1, 0x4

    if-ne p1, v0, :cond_0

    iget-object p1, p0, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/destination/PDPageDestination;->array:Lorg/apache/pdfbox/cos/COSArray;

    const/4 v0, 0x0

    invoke-virtual {p1, v1, v0}, Lorg/apache/pdfbox/cos/COSArray;->set(ILorg/apache/pdfbox/cos/COSBase;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/destination/PDPageDestination;->array:Lorg/apache/pdfbox/cos/COSArray;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSArray;->setInt(II)V

    :goto_0
    return-void
.end method

.method public setTop(I)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/destination/PDPageDestination;->array:Lorg/apache/pdfbox/cos/COSArray;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSArray;->growToSize(I)V

    const/4 v0, -0x1

    const/4 v1, 0x5

    if-ne p1, v0, :cond_0

    iget-object p1, p0, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/destination/PDPageDestination;->array:Lorg/apache/pdfbox/cos/COSArray;

    const/4 v0, 0x0

    invoke-virtual {p1, v1, v0}, Lorg/apache/pdfbox/cos/COSArray;->set(ILorg/apache/pdfbox/cos/COSBase;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/destination/PDPageDestination;->array:Lorg/apache/pdfbox/cos/COSArray;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSArray;->setInt(II)V

    :goto_0
    return-void
.end method

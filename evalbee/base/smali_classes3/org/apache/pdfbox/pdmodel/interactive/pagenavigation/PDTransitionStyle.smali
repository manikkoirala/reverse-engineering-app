.class public final enum Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDTransitionStyle;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDTransitionStyle;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDTransitionStyle;

.field public static final enum Blinds:Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDTransitionStyle;

.field public static final enum Box:Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDTransitionStyle;

.field public static final enum Cover:Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDTransitionStyle;

.field public static final enum Dissolve:Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDTransitionStyle;

.field public static final enum Fade:Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDTransitionStyle;

.field public static final enum Fly:Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDTransitionStyle;

.field public static final enum Glitter:Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDTransitionStyle;

.field public static final enum Push:Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDTransitionStyle;

.field public static final enum R:Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDTransitionStyle;

.field public static final enum Split:Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDTransitionStyle;

.field public static final enum Uncover:Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDTransitionStyle;

.field public static final enum Wipe:Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDTransitionStyle;


# direct methods
.method public static constructor <clinit>()V
    .locals 14

    new-instance v0, Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDTransitionStyle;

    const-string v1, "Split"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDTransitionStyle;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDTransitionStyle;->Split:Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDTransitionStyle;

    new-instance v1, Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDTransitionStyle;

    const-string v2, "Blinds"

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDTransitionStyle;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDTransitionStyle;->Blinds:Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDTransitionStyle;

    new-instance v2, Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDTransitionStyle;

    const-string v3, "Box"

    const/4 v4, 0x2

    invoke-direct {v2, v3, v4}, Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDTransitionStyle;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDTransitionStyle;->Box:Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDTransitionStyle;

    new-instance v3, Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDTransitionStyle;

    const-string v4, "Wipe"

    const/4 v5, 0x3

    invoke-direct {v3, v4, v5}, Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDTransitionStyle;-><init>(Ljava/lang/String;I)V

    sput-object v3, Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDTransitionStyle;->Wipe:Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDTransitionStyle;

    new-instance v4, Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDTransitionStyle;

    const-string v5, "Dissolve"

    const/4 v6, 0x4

    invoke-direct {v4, v5, v6}, Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDTransitionStyle;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDTransitionStyle;->Dissolve:Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDTransitionStyle;

    new-instance v5, Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDTransitionStyle;

    const-string v6, "Glitter"

    const/4 v7, 0x5

    invoke-direct {v5, v6, v7}, Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDTransitionStyle;-><init>(Ljava/lang/String;I)V

    sput-object v5, Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDTransitionStyle;->Glitter:Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDTransitionStyle;

    new-instance v6, Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDTransitionStyle;

    const-string v7, "R"

    const/4 v8, 0x6

    invoke-direct {v6, v7, v8}, Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDTransitionStyle;-><init>(Ljava/lang/String;I)V

    sput-object v6, Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDTransitionStyle;->R:Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDTransitionStyle;

    new-instance v7, Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDTransitionStyle;

    const-string v8, "Fly"

    const/4 v9, 0x7

    invoke-direct {v7, v8, v9}, Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDTransitionStyle;-><init>(Ljava/lang/String;I)V

    sput-object v7, Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDTransitionStyle;->Fly:Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDTransitionStyle;

    new-instance v8, Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDTransitionStyle;

    const-string v9, "Push"

    const/16 v10, 0x8

    invoke-direct {v8, v9, v10}, Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDTransitionStyle;-><init>(Ljava/lang/String;I)V

    sput-object v8, Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDTransitionStyle;->Push:Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDTransitionStyle;

    new-instance v9, Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDTransitionStyle;

    const-string v10, "Cover"

    const/16 v11, 0x9

    invoke-direct {v9, v10, v11}, Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDTransitionStyle;-><init>(Ljava/lang/String;I)V

    sput-object v9, Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDTransitionStyle;->Cover:Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDTransitionStyle;

    new-instance v10, Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDTransitionStyle;

    const-string v11, "Uncover"

    const/16 v12, 0xa

    invoke-direct {v10, v11, v12}, Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDTransitionStyle;-><init>(Ljava/lang/String;I)V

    sput-object v10, Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDTransitionStyle;->Uncover:Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDTransitionStyle;

    new-instance v11, Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDTransitionStyle;

    const-string v12, "Fade"

    const/16 v13, 0xb

    invoke-direct {v11, v12, v13}, Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDTransitionStyle;-><init>(Ljava/lang/String;I)V

    sput-object v11, Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDTransitionStyle;->Fade:Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDTransitionStyle;

    filled-new-array/range {v0 .. v11}, [Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDTransitionStyle;

    move-result-object v0

    sput-object v0, Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDTransitionStyle;->$VALUES:[Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDTransitionStyle;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDTransitionStyle;
    .locals 1

    const-class v0, Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDTransitionStyle;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDTransitionStyle;

    return-object p0
.end method

.method public static values()[Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDTransitionStyle;
    .locals 1

    sget-object v0, Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDTransitionStyle;->$VALUES:[Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDTransitionStyle;

    invoke-virtual {v0}, [Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDTransitionStyle;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDTransitionStyle;

    return-object v0
.end method

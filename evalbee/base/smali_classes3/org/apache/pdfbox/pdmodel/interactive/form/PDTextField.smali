.class public final Lorg/apache/pdfbox/pdmodel/interactive/form/PDTextField;
.super Lorg/apache/pdfbox/pdmodel/interactive/form/PDVariableText;
.source "SourceFile"


# static fields
.field private static final FLAG_COMB:I = 0x1000000

.field private static final FLAG_DO_NOT_SCROLL:I = 0x800000

.field private static final FLAG_DO_NOT_SPELL_CHECK:I = 0x400000

.field private static final FLAG_FILE_SELECT:I = 0x100000

.field private static final FLAG_MULTILINE:I = 0x1000

.field private static final FLAG_PASSWORD:I = 0x2000

.field private static final FLAG_RICH_TEXT:I = 0x2000000


# direct methods
.method public constructor <init>(Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;)V
    .locals 2

    .line 1
    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDVariableText;-><init>(Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;)V

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object p1

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->FT:Lorg/apache/pdfbox/cos/COSName;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->TX:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p1, v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    return-void
.end method

.method public constructor <init>(Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;Lorg/apache/pdfbox/cos/COSDictionary;Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;)V
    .locals 0

    .line 2
    invoke-direct {p0, p1, p2, p3}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDVariableText;-><init>(Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;Lorg/apache/pdfbox/cos/COSDictionary;Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;)V

    return-void
.end method


# virtual methods
.method public doNotScroll()Z
    .locals 3

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->FF:Lorg/apache/pdfbox/cos/COSName;

    const/high16 v2, 0x800000

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->getFlag(Lorg/apache/pdfbox/cos/COSName;I)Z

    move-result v0

    return v0
.end method

.method public doNotSpellCheck()Z
    .locals 3

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->FF:Lorg/apache/pdfbox/cos/COSName;

    const/high16 v2, 0x400000

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->getFlag(Lorg/apache/pdfbox/cos/COSName;I)Z

    move-result v0

    return v0
.end method

.method public bridge synthetic getDefaultValue()Ljava/lang/Object;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDTextField;->getDefaultValue()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultValue()Ljava/lang/String;
    .locals 2

    .line 2
    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->DV:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p0, v0}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->getInheritableAttribute(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    instance-of v1, v0, Lorg/apache/pdfbox/cos/COSString;

    if-eqz v1, :cond_0

    check-cast v0, Lorg/apache/pdfbox/cos/COSString;

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSString;->getString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, ""

    return-object v0
.end method

.method public getMaxLen()I
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->MAX_LEN:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getInt(Lorg/apache/pdfbox/cos/COSName;)I

    move-result v0

    return v0
.end method

.method public bridge synthetic getValue()Ljava/lang/Object;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDTextField;->getValue()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getValue()Ljava/lang/String;
    .locals 1

    .line 2
    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->V:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p0, v0}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->getInheritableAttribute(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->getAsTextStream(Lorg/apache/pdfbox/cos/COSBase;)Lorg/apache/pdfbox/pdmodel/common/PDTextStream;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/common/PDTextStream;->getAsString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, ""

    return-object v0
.end method

.method public getValueAsStream()Lorg/apache/pdfbox/pdmodel/common/PDTextStream;
    .locals 1

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->V:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p0, v0}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->getInheritableAttribute(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    invoke-virtual {p0, v0}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->getAsTextStream(Lorg/apache/pdfbox/cos/COSBase;)Lorg/apache/pdfbox/pdmodel/common/PDTextStream;

    move-result-object v0

    return-object v0
.end method

.method public isComb()Z
    .locals 3

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->FF:Lorg/apache/pdfbox/cos/COSName;

    const/high16 v2, 0x1000000

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->getFlag(Lorg/apache/pdfbox/cos/COSName;I)Z

    move-result v0

    return v0
.end method

.method public isFileSelect()Z
    .locals 3

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->FF:Lorg/apache/pdfbox/cos/COSName;

    const/high16 v2, 0x100000

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->getFlag(Lorg/apache/pdfbox/cos/COSName;I)Z

    move-result v0

    return v0
.end method

.method public isMultiline()Z
    .locals 3

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->FF:Lorg/apache/pdfbox/cos/COSName;

    const/16 v2, 0x1000

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->getFlag(Lorg/apache/pdfbox/cos/COSName;I)Z

    move-result v0

    return v0
.end method

.method public isPassword()Z
    .locals 3

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->FF:Lorg/apache/pdfbox/cos/COSName;

    const/16 v2, 0x2000

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->getFlag(Lorg/apache/pdfbox/cos/COSName;I)Z

    move-result v0

    return v0
.end method

.method public isRichText()Z
    .locals 3

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->FF:Lorg/apache/pdfbox/cos/COSName;

    const/high16 v2, 0x2000000

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->getFlag(Lorg/apache/pdfbox/cos/COSName;I)Z

    move-result v0

    return v0
.end method

.method public setComb(Z)V
    .locals 3

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->FF:Lorg/apache/pdfbox/cos/COSName;

    const/high16 v2, 0x1000000

    invoke-virtual {v0, v1, v2, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setFlag(Lorg/apache/pdfbox/cos/COSName;IZ)V

    return-void
.end method

.method public setDefaultValue(Ljava/lang/String;)V
    .locals 1

    if-eqz p1, :cond_0

    new-instance v0, Lorg/apache/pdfbox/cos/COSString;

    invoke-direct {v0, p1}, Lorg/apache/pdfbox/cos/COSString;-><init>(Ljava/lang/String;)V

    sget-object p1, Lorg/apache/pdfbox/cos/COSName;->DV:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p0, p1, v0}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->setInheritableAttribute(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    goto :goto_0

    :cond_0
    sget-object p1, Lorg/apache/pdfbox/cos/COSName;->DV:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->removeInheritableAttribute(Lorg/apache/pdfbox/cos/COSName;)V

    :goto_0
    return-void
.end method

.method public setDoNotScroll(Z)V
    .locals 3

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->FF:Lorg/apache/pdfbox/cos/COSName;

    const/high16 v2, 0x800000

    invoke-virtual {v0, v1, v2, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setFlag(Lorg/apache/pdfbox/cos/COSName;IZ)V

    return-void
.end method

.method public setDoNotSpellCheck(Z)V
    .locals 3

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->FF:Lorg/apache/pdfbox/cos/COSName;

    const/high16 v2, 0x400000

    invoke-virtual {v0, v1, v2, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setFlag(Lorg/apache/pdfbox/cos/COSName;IZ)V

    return-void
.end method

.method public setFileSelect(Z)V
    .locals 3

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->FF:Lorg/apache/pdfbox/cos/COSName;

    const/high16 v2, 0x100000

    invoke-virtual {v0, v1, v2, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setFlag(Lorg/apache/pdfbox/cos/COSName;IZ)V

    return-void
.end method

.method public setMaxLen(I)V
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->MAX_LEN:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setInt(Lorg/apache/pdfbox/cos/COSName;I)V

    return-void
.end method

.method public setMultiline(Z)V
    .locals 3

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->FF:Lorg/apache/pdfbox/cos/COSName;

    const/16 v2, 0x1000

    invoke-virtual {v0, v1, v2, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setFlag(Lorg/apache/pdfbox/cos/COSName;IZ)V

    return-void
.end method

.method public setPassword(Z)V
    .locals 3

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->FF:Lorg/apache/pdfbox/cos/COSName;

    const/16 v2, 0x2000

    invoke-virtual {v0, v1, v2, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setFlag(Lorg/apache/pdfbox/cos/COSName;IZ)V

    return-void
.end method

.method public setRichText(Z)V
    .locals 3

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->FF:Lorg/apache/pdfbox/cos/COSName;

    const/high16 v2, 0x2000000

    invoke-virtual {v0, v1, v2, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setFlag(Lorg/apache/pdfbox/cos/COSName;IZ)V

    return-void
.end method

.method public setValue(Ljava/lang/String;)V
    .locals 1

    .line 1
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Lorg/apache/pdfbox/cos/COSString;

    invoke-direct {v0, p1}, Lorg/apache/pdfbox/cos/COSString;-><init>(Ljava/lang/String;)V

    sget-object p1, Lorg/apache/pdfbox/cos/COSName;->V:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p0, p1, v0}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->setInheritableAttribute(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    goto :goto_0

    :cond_0
    sget-object p1, Lorg/apache/pdfbox/cos/COSName;->DV:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->removeInheritableAttribute(Lorg/apache/pdfbox/cos/COSName;)V

    :goto_0
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDField;->updateFieldAppearances()V

    return-void
.end method

.method public setValue(Lorg/apache/pdfbox/pdmodel/common/PDTextStream;)V
    .locals 1

    .line 2
    if-eqz p1, :cond_0

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->V:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/common/PDTextStream;->getCOSObject()Lorg/apache/pdfbox/cos/COSBase;

    move-result-object p1

    invoke-virtual {p0, v0, p1}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->setInheritableAttribute(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    goto :goto_0

    :cond_0
    sget-object p1, Lorg/apache/pdfbox/cos/COSName;->V:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->removeInheritableAttribute(Lorg/apache/pdfbox/cos/COSName;)V

    :goto_0
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDField;->updateFieldAppearances()V

    return-void
.end method

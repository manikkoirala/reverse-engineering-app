.class public Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDVisibleSigBuilder;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDFTemplateBuilder;


# instance fields
.field private pdfStructure:Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDFTemplateStructure;


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDFTemplateStructure;

    invoke-direct {v0}, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDFTemplateStructure;-><init>()V

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDVisibleSigBuilder;->pdfStructure:Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDFTemplateStructure;

    const-string v0, "PdfBoxAndroid"

    const-string v1, "PDF Strucure has been Created"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method


# virtual methods
.method public appendRawCommands(Ljava/io/OutputStream;Ljava/lang/String;)V
    .locals 1

    const-string v0, "UTF-8"

    invoke-virtual {p2, v0}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/io/OutputStream;->write([B)V

    invoke-virtual {p1}, Ljava/io/OutputStream;->close()V

    return-void
.end method

.method public closeTemplate(Lorg/apache/pdfbox/pdmodel/PDDocument;)V
    .locals 0

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/PDDocument;->close()V

    iget-object p1, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDVisibleSigBuilder;->pdfStructure:Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDFTemplateStructure;

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDFTemplateStructure;->getTemplate()Lorg/apache/pdfbox/pdmodel/PDDocument;

    move-result-object p1

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/PDDocument;->close()V

    return-void
.end method

.method public createAcroForm(Lorg/apache/pdfbox/pdmodel/PDDocument;)V
    .locals 1

    new-instance v0, Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;

    invoke-direct {v0, p1}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;-><init>(Lorg/apache/pdfbox/pdmodel/PDDocument;)V

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/PDDocument;->getDocumentCatalog()Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;

    move-result-object p1

    invoke-virtual {p1, v0}, Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;->setAcroForm(Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;)V

    iget-object p1, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDVisibleSigBuilder;->pdfStructure:Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDFTemplateStructure;

    invoke-virtual {p1, v0}, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDFTemplateStructure;->setAcroForm(Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;)V

    const-string p1, "PdfBoxAndroid"

    const-string v0, "Acro form page has been created"

    invoke-static {p1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public createAcroFormDictionary(Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;Lorg/apache/pdfbox/pdmodel/interactive/form/PDSignatureField;)V
    .locals 3

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;->getFields()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {p1, v2}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;->setSignaturesExist(Z)V

    invoke-virtual {p1, v2}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;->setAppendOnly(Z)V

    invoke-virtual {v1, v2}, Lorg/apache/pdfbox/cos/COSBase;->setDirect(Z)V

    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string p2, "/sylfaen 0 Tf 0 g"

    invoke-virtual {p1, p2}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;->setDefaultAppearance(Ljava/lang/String;)V

    iget-object p1, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDVisibleSigBuilder;->pdfStructure:Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDFTemplateStructure;

    invoke-virtual {p1, v0}, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDFTemplateStructure;->setAcroFormFields(Ljava/util/List;)V

    iget-object p1, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDVisibleSigBuilder;->pdfStructure:Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDFTemplateStructure;

    invoke-virtual {p1, v1}, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDFTemplateStructure;->setAcroFormDictionary(Lorg/apache/pdfbox/cos/COSDictionary;)V

    const-string p1, "PdfBoxAndroid"

    const-string p2, "AcroForm dictionary has been created"

    invoke-static {p1, p2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public createAffineTransform([B)V
    .locals 14

    new-instance v13, Lorg/apache/pdfbox/util/awt/AffineTransform;

    const/4 v0, 0x0

    aget-byte v0, p1, v0

    int-to-double v1, v0

    const/4 v0, 0x1

    aget-byte v0, p1, v0

    int-to-double v3, v0

    const/4 v0, 0x2

    aget-byte v0, p1, v0

    int-to-double v5, v0

    const/4 v0, 0x3

    aget-byte v0, p1, v0

    int-to-double v7, v0

    const/4 v0, 0x4

    aget-byte v0, p1, v0

    int-to-double v9, v0

    const/4 v0, 0x5

    aget-byte p1, p1, v0

    int-to-double v11, p1

    move-object v0, v13

    invoke-direct/range {v0 .. v12}, Lorg/apache/pdfbox/util/awt/AffineTransform;-><init>(DDDDDD)V

    iget-object p1, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDVisibleSigBuilder;->pdfStructure:Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDFTemplateStructure;

    invoke-virtual {p1, v13}, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDFTemplateStructure;->setAffineTransform(Lorg/apache/pdfbox/util/awt/AffineTransform;)V

    const-string p1, "PdfBoxAndroid"

    const-string v0, "Matrix has been added"

    invoke-static {p1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public createAppearanceDictionary(Lorg/apache/pdfbox/pdmodel/graphics/form/PDFormXObject;Lorg/apache/pdfbox/pdmodel/interactive/form/PDSignatureField;)V
    .locals 3

    new-instance v0, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAppearanceDictionary;

    invoke-direct {v0}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAppearanceDictionary;-><init>()V

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAppearanceDictionary;->getCOSObject()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lorg/apache/pdfbox/cos/COSBase;->setDirect(Z)V

    new-instance v1, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAppearanceStream;

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/graphics/PDXObject;->getCOSStream()Lorg/apache/pdfbox/cos/COSStream;

    move-result-object p1

    invoke-direct {v1, p1}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAppearanceStream;-><init>(Lorg/apache/pdfbox/cos/COSStream;)V

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAppearanceDictionary;->setNormalAppearance(Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAppearanceStream;)V

    invoke-virtual {p2}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->getWidget()Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotationWidget;

    move-result-object p1

    invoke-virtual {p1, v0}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotation;->setAppearance(Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAppearanceDictionary;)V

    iget-object p1, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDVisibleSigBuilder;->pdfStructure:Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDFTemplateStructure;

    invoke-virtual {p1, v0}, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDFTemplateStructure;->setAppearanceDictionary(Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAppearanceDictionary;)V

    const-string p1, "PdfBoxAndroid"

    const-string p2, "PDF appereance Dictionary has been created"

    invoke-static {p1, p2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public createFormaterRectangle([B)V
    .locals 2

    new-instance v0, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;

    invoke-direct {v0}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;-><init>()V

    const/4 v1, 0x0

    aget-byte v1, p1, v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->setUpperRightX(F)V

    const/4 v1, 0x1

    aget-byte v1, p1, v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->setUpperRightY(F)V

    const/4 v1, 0x2

    aget-byte v1, p1, v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->setLowerLeftX(F)V

    const/4 v1, 0x3

    aget-byte p1, p1, v1

    int-to-float p1, p1

    invoke-virtual {v0, p1}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->setLowerLeftY(F)V

    iget-object p1, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDVisibleSigBuilder;->pdfStructure:Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDFTemplateStructure;

    invoke-virtual {p1, v0}, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDFTemplateStructure;->setFormaterRectangle(Lorg/apache/pdfbox/pdmodel/common/PDRectangle;)V

    const-string p1, "PdfBoxAndroid"

    const-string v0, "Formater rectangle has been created"

    invoke-static {p1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public createHolderForm(Lorg/apache/pdfbox/pdmodel/PDResources;Lorg/apache/pdfbox/pdmodel/common/PDStream;Lorg/apache/pdfbox/pdmodel/common/PDRectangle;)V
    .locals 1

    new-instance v0, Lorg/apache/pdfbox/pdmodel/graphics/form/PDFormXObject;

    invoke-direct {v0, p2}, Lorg/apache/pdfbox/pdmodel/graphics/form/PDFormXObject;-><init>(Lorg/apache/pdfbox/pdmodel/common/PDStream;)V

    invoke-virtual {v0, p1}, Lorg/apache/pdfbox/pdmodel/graphics/form/PDFormXObject;->setResources(Lorg/apache/pdfbox/pdmodel/PDResources;)V

    invoke-virtual {v0, p3}, Lorg/apache/pdfbox/pdmodel/graphics/form/PDFormXObject;->setBBox(Lorg/apache/pdfbox/pdmodel/common/PDRectangle;)V

    const/4 p1, 0x1

    invoke-virtual {v0, p1}, Lorg/apache/pdfbox/pdmodel/graphics/form/PDFormXObject;->setFormType(I)V

    iget-object p1, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDVisibleSigBuilder;->pdfStructure:Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDFTemplateStructure;

    invoke-virtual {p1, v0}, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDFTemplateStructure;->setHolderForm(Lorg/apache/pdfbox/pdmodel/graphics/form/PDFormXObject;)V

    const-string p1, "PdfBoxAndroid"

    const-string p2, "Holder form has been created"

    invoke-static {p1, p2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public createHolderFormResources()V
    .locals 2

    new-instance v0, Lorg/apache/pdfbox/pdmodel/PDResources;

    invoke-direct {v0}, Lorg/apache/pdfbox/pdmodel/PDResources;-><init>()V

    iget-object v1, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDVisibleSigBuilder;->pdfStructure:Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDFTemplateStructure;

    invoke-virtual {v1, v0}, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDFTemplateStructure;->setHolderFormResources(Lorg/apache/pdfbox/pdmodel/PDResources;)V

    const-string v0, "PdfBoxAndroid"

    const-string v1, "Holder form resources have been created"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public createHolderFormStream(Lorg/apache/pdfbox/pdmodel/PDDocument;)V
    .locals 1

    new-instance v0, Lorg/apache/pdfbox/pdmodel/common/PDStream;

    invoke-direct {v0, p1}, Lorg/apache/pdfbox/pdmodel/common/PDStream;-><init>(Lorg/apache/pdfbox/pdmodel/PDDocument;)V

    iget-object p1, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDVisibleSigBuilder;->pdfStructure:Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDFTemplateStructure;

    invoke-virtual {p1, v0}, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDFTemplateStructure;->setHolderFormStream(Lorg/apache/pdfbox/pdmodel/common/PDStream;)V

    const-string p1, "PdfBoxAndroid"

    const-string v0, "Holder form Stream has been created"

    invoke-static {p1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public createImageForm(Lorg/apache/pdfbox/pdmodel/PDResources;Lorg/apache/pdfbox/pdmodel/PDResources;Lorg/apache/pdfbox/pdmodel/common/PDStream;Lorg/apache/pdfbox/pdmodel/common/PDRectangle;Lorg/apache/pdfbox/util/awt/AffineTransform;Lorg/apache/pdfbox/pdmodel/graphics/image/PDImageXObject;)V
    .locals 1

    new-instance v0, Lorg/apache/pdfbox/pdmodel/graphics/form/PDFormXObject;

    invoke-direct {v0, p3}, Lorg/apache/pdfbox/pdmodel/graphics/form/PDFormXObject;-><init>(Lorg/apache/pdfbox/pdmodel/common/PDStream;)V

    invoke-virtual {v0, p4}, Lorg/apache/pdfbox/pdmodel/graphics/form/PDFormXObject;->setBBox(Lorg/apache/pdfbox/pdmodel/common/PDRectangle;)V

    invoke-virtual {v0, p5}, Lorg/apache/pdfbox/pdmodel/graphics/form/PDFormXObject;->setMatrix(Lorg/apache/pdfbox/util/awt/AffineTransform;)V

    invoke-virtual {v0, p1}, Lorg/apache/pdfbox/pdmodel/graphics/form/PDFormXObject;->setResources(Lorg/apache/pdfbox/pdmodel/PDResources;)V

    const/4 p3, 0x1

    invoke-virtual {v0, p3}, Lorg/apache/pdfbox/pdmodel/graphics/form/PDFormXObject;->setFormType(I)V

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/PDResources;->getCOSObject()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object p4

    invoke-virtual {p4, p3}, Lorg/apache/pdfbox/cos/COSBase;->setDirect(Z)V

    const-string p3, "n"

    invoke-virtual {p2, v0, p3}, Lorg/apache/pdfbox/pdmodel/PDResources;->add(Lorg/apache/pdfbox/pdmodel/graphics/PDXObject;Ljava/lang/String;)Lorg/apache/pdfbox/cos/COSName;

    move-result-object p2

    const-string p3, "img"

    invoke-virtual {p1, p6, p3}, Lorg/apache/pdfbox/pdmodel/PDResources;->add(Lorg/apache/pdfbox/pdmodel/graphics/PDXObject;Ljava/lang/String;)Lorg/apache/pdfbox/cos/COSName;

    move-result-object p1

    iget-object p3, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDVisibleSigBuilder;->pdfStructure:Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDFTemplateStructure;

    invoke-virtual {p3, v0}, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDFTemplateStructure;->setImageForm(Lorg/apache/pdfbox/pdmodel/graphics/form/PDFormXObject;)V

    iget-object p3, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDVisibleSigBuilder;->pdfStructure:Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDFTemplateStructure;

    invoke-virtual {p3, p2}, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDFTemplateStructure;->setImageFormName(Lorg/apache/pdfbox/cos/COSName;)V

    iget-object p2, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDVisibleSigBuilder;->pdfStructure:Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDFTemplateStructure;

    invoke-virtual {p2, p1}, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDFTemplateStructure;->setImageName(Lorg/apache/pdfbox/cos/COSName;)V

    const-string p1, "PdfBoxAndroid"

    const-string p2, "Created image form"

    invoke-static {p1, p2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public createImageFormResources()V
    .locals 2

    new-instance v0, Lorg/apache/pdfbox/pdmodel/PDResources;

    invoke-direct {v0}, Lorg/apache/pdfbox/pdmodel/PDResources;-><init>()V

    iget-object v1, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDVisibleSigBuilder;->pdfStructure:Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDFTemplateStructure;

    invoke-virtual {v1, v0}, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDFTemplateStructure;->setImageFormResources(Lorg/apache/pdfbox/pdmodel/PDResources;)V

    const-string v0, "PdfBoxAndroid"

    const-string v1, "Created image form Resources"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public createImageFormStream(Lorg/apache/pdfbox/pdmodel/PDDocument;)V
    .locals 1

    new-instance v0, Lorg/apache/pdfbox/pdmodel/common/PDStream;

    invoke-direct {v0, p1}, Lorg/apache/pdfbox/pdmodel/common/PDStream;-><init>(Lorg/apache/pdfbox/pdmodel/PDDocument;)V

    iget-object p1, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDVisibleSigBuilder;->pdfStructure:Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDFTemplateStructure;

    invoke-virtual {p1, v0}, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDFTemplateStructure;->setImageFormStream(Lorg/apache/pdfbox/pdmodel/common/PDStream;)V

    const-string p1, "PdfBoxAndroid"

    const-string v0, "Created image form Stream"

    invoke-static {p1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public createInnerForm(Lorg/apache/pdfbox/pdmodel/PDResources;Lorg/apache/pdfbox/pdmodel/common/PDStream;Lorg/apache/pdfbox/pdmodel/common/PDRectangle;)V
    .locals 1

    new-instance v0, Lorg/apache/pdfbox/pdmodel/graphics/form/PDFormXObject;

    invoke-direct {v0, p2}, Lorg/apache/pdfbox/pdmodel/graphics/form/PDFormXObject;-><init>(Lorg/apache/pdfbox/pdmodel/common/PDStream;)V

    invoke-virtual {v0, p1}, Lorg/apache/pdfbox/pdmodel/graphics/form/PDFormXObject;->setResources(Lorg/apache/pdfbox/pdmodel/PDResources;)V

    invoke-virtual {v0, p3}, Lorg/apache/pdfbox/pdmodel/graphics/form/PDFormXObject;->setBBox(Lorg/apache/pdfbox/pdmodel/common/PDRectangle;)V

    const/4 p1, 0x1

    invoke-virtual {v0, p1}, Lorg/apache/pdfbox/pdmodel/graphics/form/PDFormXObject;->setFormType(I)V

    iget-object p1, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDVisibleSigBuilder;->pdfStructure:Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDFTemplateStructure;

    invoke-virtual {p1, v0}, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDFTemplateStructure;->setInnerForm(Lorg/apache/pdfbox/pdmodel/graphics/form/PDFormXObject;)V

    const-string p1, "PdfBoxAndroid"

    const-string p2, "Another form (inner form - it would be inside holder form) have been created"

    invoke-static {p1, p2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public createInnerFormResource()V
    .locals 2

    new-instance v0, Lorg/apache/pdfbox/pdmodel/PDResources;

    invoke-direct {v0}, Lorg/apache/pdfbox/pdmodel/PDResources;-><init>()V

    iget-object v1, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDVisibleSigBuilder;->pdfStructure:Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDFTemplateStructure;

    invoke-virtual {v1, v0}, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDFTemplateStructure;->setInnerFormResources(Lorg/apache/pdfbox/pdmodel/PDResources;)V

    const-string v0, "PdfBoxAndroid"

    const-string v1, "Resources of another form (inner form - it would be inside holder form)have been created"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public createInnerFormStream(Lorg/apache/pdfbox/pdmodel/PDDocument;)V
    .locals 1

    new-instance v0, Lorg/apache/pdfbox/pdmodel/common/PDStream;

    invoke-direct {v0, p1}, Lorg/apache/pdfbox/pdmodel/common/PDStream;-><init>(Lorg/apache/pdfbox/pdmodel/PDDocument;)V

    iget-object p1, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDVisibleSigBuilder;->pdfStructure:Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDFTemplateStructure;

    invoke-virtual {p1, v0}, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDFTemplateStructure;->setInnterFormStream(Lorg/apache/pdfbox/pdmodel/common/PDStream;)V

    const-string p1, "PdfBoxAndroid"

    const-string v0, "Stream of another form (inner form - it would be inside holder form) has been created"

    invoke-static {p1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public createPage(Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDVisibleSignDesigner;)V
    .locals 3

    new-instance v0, Lorg/apache/pdfbox/pdmodel/PDPage;

    new-instance v1, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDVisibleSignDesigner;->getPageWidth()F

    move-result v2

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDVisibleSignDesigner;->getPageHeight()F

    move-result p1

    invoke-direct {v1, v2, p1}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;-><init>(FF)V

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/pdmodel/PDPage;-><init>(Lorg/apache/pdfbox/pdmodel/common/PDRectangle;)V

    iget-object p1, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDVisibleSigBuilder;->pdfStructure:Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDFTemplateStructure;

    invoke-virtual {p1, v0}, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDFTemplateStructure;->setPage(Lorg/apache/pdfbox/pdmodel/PDPage;)V

    const-string p1, "PdfBoxAndroid"

    const-string v0, "PDF page has been created"

    invoke-static {p1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public createProcSetArray()V
    .locals 2

    new-instance v0, Lorg/apache/pdfbox/cos/COSArray;

    invoke-direct {v0}, Lorg/apache/pdfbox/cos/COSArray;-><init>()V

    const-string v1, "PDF"

    invoke-static {v1}, Lorg/apache/pdfbox/cos/COSName;->getPDFName(Ljava/lang/String;)Lorg/apache/pdfbox/cos/COSName;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    const-string v1, "Text"

    invoke-static {v1}, Lorg/apache/pdfbox/cos/COSName;->getPDFName(Ljava/lang/String;)Lorg/apache/pdfbox/cos/COSName;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    const-string v1, "ImageB"

    invoke-static {v1}, Lorg/apache/pdfbox/cos/COSName;->getPDFName(Ljava/lang/String;)Lorg/apache/pdfbox/cos/COSName;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    const-string v1, "ImageC"

    invoke-static {v1}, Lorg/apache/pdfbox/cos/COSName;->getPDFName(Ljava/lang/String;)Lorg/apache/pdfbox/cos/COSName;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    const-string v1, "ImageI"

    invoke-static {v1}, Lorg/apache/pdfbox/cos/COSName;->getPDFName(Ljava/lang/String;)Lorg/apache/pdfbox/cos/COSName;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    iget-object v1, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDVisibleSigBuilder;->pdfStructure:Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDFTemplateStructure;

    invoke-virtual {v1, v0}, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDFTemplateStructure;->setProcSet(Lorg/apache/pdfbox/cos/COSArray;)V

    const-string v0, "PdfBoxAndroid"

    const-string v1, "ProcSet array has been created"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public createSignature(Lorg/apache/pdfbox/pdmodel/interactive/form/PDSignatureField;Lorg/apache/pdfbox/pdmodel/PDPage;Ljava/lang/String;)V
    .locals 2

    new-instance v0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/PDSignature;

    invoke-direct {v0}, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/PDSignature;-><init>()V

    invoke-virtual {p1, v0}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDSignatureField;->setSignature(Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/PDSignature;)V

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->getWidget()Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotationWidget;

    move-result-object v1

    invoke-virtual {v1, p2}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotation;->setPage(Lorg/apache/pdfbox/pdmodel/PDPage;)V

    invoke-virtual {p2}, Lorg/apache/pdfbox/pdmodel/PDPage;->getAnnotations()Ljava/util/List;

    move-result-object p2

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->getWidget()Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotationWidget;

    move-result-object p1

    invoke-interface {p2, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {v0, p3}, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/PDSignature;->setName(Ljava/lang/String;)V

    const/4 p1, 0x0

    filled-new-array {p1, p1, p1, p1}, [I

    move-result-object p1

    invoke-virtual {v0, p1}, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/PDSignature;->setByteRange([I)V

    const/16 p1, 0x1000

    new-array p1, p1, [B

    invoke-virtual {v0, p1}, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/PDSignature;->setContents([B)V

    iget-object p1, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDVisibleSigBuilder;->pdfStructure:Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDFTemplateStructure;

    invoke-virtual {p1, v0}, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDFTemplateStructure;->setPdSignature(Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/PDSignature;)V

    const-string p1, "PdfBoxAndroid"

    const-string p2, "PDSignatur has been created"

    invoke-static {p1, p2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public createSignatureField(Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;)V
    .locals 1

    new-instance v0, Lorg/apache/pdfbox/pdmodel/interactive/form/PDSignatureField;

    invoke-direct {v0, p1}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDSignatureField;-><init>(Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;)V

    iget-object p1, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDVisibleSigBuilder;->pdfStructure:Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDFTemplateStructure;

    invoke-virtual {p1, v0}, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDFTemplateStructure;->setSignatureField(Lorg/apache/pdfbox/pdmodel/interactive/form/PDSignatureField;)V

    const-string p1, "PdfBoxAndroid"

    const-string v0, "Signature field has been created"

    invoke-static {p1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public createSignatureRectangle(Lorg/apache/pdfbox/pdmodel/interactive/form/PDSignatureField;Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDVisibleSignDesigner;)V
    .locals 3

    new-instance v0, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;

    invoke-direct {v0}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;-><init>()V

    invoke-virtual {p2}, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDVisibleSignDesigner;->getxAxis()F

    move-result v1

    invoke-virtual {p2}, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDVisibleSignDesigner;->getWidth()F

    move-result v2

    add-float/2addr v1, v2

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->setUpperRightX(F)V

    invoke-virtual {p2}, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDVisibleSignDesigner;->getTemplateHeight()F

    move-result v1

    invoke-virtual {p2}, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDVisibleSignDesigner;->getyAxis()F

    move-result v2

    sub-float/2addr v1, v2

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->setUpperRightY(F)V

    invoke-virtual {p2}, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDVisibleSignDesigner;->getTemplateHeight()F

    move-result v1

    invoke-virtual {p2}, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDVisibleSignDesigner;->getyAxis()F

    move-result v2

    sub-float/2addr v1, v2

    invoke-virtual {p2}, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDVisibleSignDesigner;->getHeight()F

    move-result v2

    sub-float/2addr v1, v2

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->setLowerLeftY(F)V

    invoke-virtual {p2}, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDVisibleSignDesigner;->getxAxis()F

    move-result p2

    invoke-virtual {v0, p2}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->setLowerLeftX(F)V

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->getWidget()Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotationWidget;

    move-result-object p1

    invoke-virtual {p1, v0}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotation;->setRectangle(Lorg/apache/pdfbox/pdmodel/common/PDRectangle;)V

    iget-object p1, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDVisibleSigBuilder;->pdfStructure:Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDFTemplateStructure;

    invoke-virtual {p1, v0}, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDFTemplateStructure;->setSignatureRectangle(Lorg/apache/pdfbox/pdmodel/common/PDRectangle;)V

    const-string p1, "PdfBoxAndroid"

    const-string p2, "rectangle of signature has been created"

    invoke-static {p1, p2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public createTemplate(Lorg/apache/pdfbox/pdmodel/PDPage;)V
    .locals 1

    new-instance v0, Lorg/apache/pdfbox/pdmodel/PDDocument;

    invoke-direct {v0}, Lorg/apache/pdfbox/pdmodel/PDDocument;-><init>()V

    invoke-virtual {v0, p1}, Lorg/apache/pdfbox/pdmodel/PDDocument;->addPage(Lorg/apache/pdfbox/pdmodel/PDPage;)V

    iget-object p1, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDVisibleSigBuilder;->pdfStructure:Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDFTemplateStructure;

    invoke-virtual {p1, v0}, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDFTemplateStructure;->setTemplate(Lorg/apache/pdfbox/pdmodel/PDDocument;)V

    return-void
.end method

.method public createVisualSignature(Lorg/apache/pdfbox/pdmodel/PDDocument;)V
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDVisibleSigBuilder;->pdfStructure:Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDFTemplateStructure;

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/PDDocument;->getDocument()Lorg/apache/pdfbox/cos/COSDocument;

    move-result-object p1

    invoke-virtual {v0, p1}, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDFTemplateStructure;->setVisualSignature(Lorg/apache/pdfbox/cos/COSDocument;)V

    const-string p1, "PdfBoxAndroid"

    const-string v0, "Visible signature has been created"

    invoke-static {p1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public createWidgetDictionary(Lorg/apache/pdfbox/pdmodel/interactive/form/PDSignatureField;Lorg/apache/pdfbox/pdmodel/PDResources;)V
    .locals 1

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->getWidget()Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotationWidget;

    move-result-object p1

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotation;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object p1

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->setNeedToBeUpdated(Z)V

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->DR:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p2}, Lorg/apache/pdfbox/pdmodel/PDResources;->getCOSObject()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object p2

    invoke-virtual {p1, v0, p2}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    iget-object p2, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDVisibleSigBuilder;->pdfStructure:Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDFTemplateStructure;

    invoke-virtual {p2, p1}, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDFTemplateStructure;->setWidgetDictionary(Lorg/apache/pdfbox/cos/COSDictionary;)V

    const-string p1, "PdfBoxAndroid"

    const-string p2, "WidgetDictionary has been crated"

    invoke-static {p1, p2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public getStructure()Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDFTemplateStructure;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDVisibleSigBuilder;->pdfStructure:Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDFTemplateStructure;

    return-object v0
.end method

.method public injectAppearanceStreams(Lorg/apache/pdfbox/pdmodel/common/PDStream;Lorg/apache/pdfbox/pdmodel/common/PDStream;Lorg/apache/pdfbox/pdmodel/common/PDStream;Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDVisibleSignDesigner;)V
    .locals 0

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string p2, "q 100 0 0 50 0 0 cm /"

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p5}, Lorg/apache/pdfbox/cos/COSName;->getName()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, " Do Q\n"

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string p5, "q 1 0 0 1 0 0 cm /"

    invoke-virtual {p3, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p6}, Lorg/apache/pdfbox/cos/COSName;->getName()Ljava/lang/String;

    move-result-object p6

    invoke-virtual {p3, p6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p6, " Do Q \n"

    invoke-virtual {p3, p6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    new-instance p6, Ljava/lang/StringBuilder;

    invoke-direct {p6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p6, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p4}, Lorg/apache/pdfbox/cos/COSName;->getName()Ljava/lang/String;

    move-result-object p4

    invoke-virtual {p6, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    iget-object p4, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDVisibleSigBuilder;->pdfStructure:Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDFTemplateStructure;

    invoke-virtual {p4}, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDFTemplateStructure;->getHolderFormStream()Lorg/apache/pdfbox/pdmodel/common/PDStream;

    move-result-object p4

    invoke-virtual {p4}, Lorg/apache/pdfbox/pdmodel/common/PDStream;->createOutputStream()Ljava/io/OutputStream;

    move-result-object p4

    invoke-virtual {p0, p4, p3}, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDVisibleSigBuilder;->appendRawCommands(Ljava/io/OutputStream;Ljava/lang/String;)V

    iget-object p3, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDVisibleSigBuilder;->pdfStructure:Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDFTemplateStructure;

    invoke-virtual {p3}, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDFTemplateStructure;->getInnterFormStream()Lorg/apache/pdfbox/pdmodel/common/PDStream;

    move-result-object p3

    invoke-virtual {p3}, Lorg/apache/pdfbox/pdmodel/common/PDStream;->createOutputStream()Ljava/io/OutputStream;

    move-result-object p3

    invoke-virtual {p0, p3, p2}, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDVisibleSigBuilder;->appendRawCommands(Ljava/io/OutputStream;Ljava/lang/String;)V

    iget-object p2, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDVisibleSigBuilder;->pdfStructure:Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDFTemplateStructure;

    invoke-virtual {p2}, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDFTemplateStructure;->getImageFormStream()Lorg/apache/pdfbox/pdmodel/common/PDStream;

    move-result-object p2

    invoke-virtual {p2}, Lorg/apache/pdfbox/pdmodel/common/PDStream;->createOutputStream()Ljava/io/OutputStream;

    move-result-object p2

    invoke-virtual {p0, p2, p1}, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDVisibleSigBuilder;->appendRawCommands(Ljava/io/OutputStream;Ljava/lang/String;)V

    const-string p1, "PdfBoxAndroid"

    const-string p2, "Injected apereance stream to pdf"

    invoke-static {p1, p2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public injectProcSetArray(Lorg/apache/pdfbox/pdmodel/graphics/form/PDFormXObject;Lorg/apache/pdfbox/pdmodel/PDPage;Lorg/apache/pdfbox/pdmodel/PDResources;Lorg/apache/pdfbox/pdmodel/PDResources;Lorg/apache/pdfbox/pdmodel/PDResources;Lorg/apache/pdfbox/cos/COSArray;)V
    .locals 1

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/graphics/form/PDFormXObject;->getResources()Lorg/apache/pdfbox/pdmodel/PDResources;

    move-result-object p1

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/PDResources;->getCOSObject()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object p1

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->PROC_SET:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p1, v0, p6}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    invoke-virtual {p2}, Lorg/apache/pdfbox/pdmodel/PDPage;->getCOSObject()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object p1

    invoke-virtual {p1, v0, p6}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    invoke-virtual {p3}, Lorg/apache/pdfbox/pdmodel/PDResources;->getCOSObject()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object p1

    invoke-virtual {p1, v0, p6}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    invoke-virtual {p4}, Lorg/apache/pdfbox/pdmodel/PDResources;->getCOSObject()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object p1

    invoke-virtual {p1, v0, p6}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    invoke-virtual {p5}, Lorg/apache/pdfbox/pdmodel/PDResources;->getCOSObject()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object p1

    invoke-virtual {p1, v0, p6}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    const-string p1, "PdfBoxAndroid"

    const-string p2, "inserted ProcSet to PDF"

    invoke-static {p1, p2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public insertInnerFormToHolerResources(Lorg/apache/pdfbox/pdmodel/graphics/form/PDFormXObject;Lorg/apache/pdfbox/pdmodel/PDResources;)V
    .locals 1

    const-string v0, "FRM"

    invoke-virtual {p2, p1, v0}, Lorg/apache/pdfbox/pdmodel/PDResources;->add(Lorg/apache/pdfbox/pdmodel/graphics/PDXObject;Ljava/lang/String;)Lorg/apache/pdfbox/cos/COSName;

    move-result-object p1

    iget-object p2, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDVisibleSigBuilder;->pdfStructure:Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDFTemplateStructure;

    invoke-virtual {p2, p1}, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDFTemplateStructure;->setInnerFormName(Lorg/apache/pdfbox/cos/COSName;)V

    const-string p1, "PdfBoxAndroid"

    const-string p2, "Already inserted inner form  inside holder form"

    invoke-static {p1, p2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.class public Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/PDSeedValueMDP;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private dictionary:Lorg/apache/pdfbox/cos/COSDictionary;


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-direct {v0}, Lorg/apache/pdfbox/cos/COSDictionary;-><init>()V

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/PDSeedValueMDP;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSBase;->setDirect(Z)V

    return-void
.end method

.method public constructor <init>(Lorg/apache/pdfbox/cos/COSDictionary;)V
    .locals 1

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/PDSeedValueMDP;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lorg/apache/pdfbox/cos/COSBase;->setDirect(Z)V

    return-void
.end method


# virtual methods
.method public getCOSObject()Lorg/apache/pdfbox/cos/COSBase;
    .locals 1

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/PDSeedValueMDP;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    return-object v0
.end method

.method public getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/PDSeedValueMDP;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    return-object v0
.end method

.method public getP()I
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/PDSeedValueMDP;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->P:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getInt(Lorg/apache/pdfbox/cos/COSName;)I

    move-result v0

    return v0
.end method

.method public setP(I)V
    .locals 2

    if-ltz p1, :cond_0

    const/4 v0, 0x3

    if-gt p1, v0, :cond_0

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/PDSeedValueMDP;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->P:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setInt(Lorg/apache/pdfbox/cos/COSName;I)V

    return-void

    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Only values between 0 and 3 nare allowed."

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

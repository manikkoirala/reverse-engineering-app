.class Lorg/apache/pdfbox/pdmodel/interactive/form/PlainText$Line;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/pdfbox/pdmodel/interactive/form/PlainText;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Line"
.end annotation


# instance fields
.field private lineWidth:F

.field private words:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lorg/apache/pdfbox/pdmodel/interactive/form/PlainText$Word;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/PlainText$Line;->words:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public addWord(Lorg/apache/pdfbox/pdmodel/interactive/form/PlainText$Word;)V
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/PlainText$Line;->words:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public calculateWidth(Lorg/apache/pdfbox/pdmodel/font/PDFont;F)F
    .locals 5

    const/high16 v0, 0x447a0000    # 1000.0f

    div-float/2addr p2, v0

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/PlainText$Line;->words:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v1, 0x0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/pdfbox/pdmodel/interactive/form/PlainText$Word;

    invoke-virtual {v2}, Lorg/apache/pdfbox/pdmodel/interactive/form/PlainText$Word;->getAttributes()Ljava/text/AttributedString;

    move-result-object v3

    invoke-virtual {v3}, Ljava/text/AttributedString;->getIterator()Ljava/text/AttributedCharacterIterator;

    move-result-object v3

    sget-object v4, Lorg/apache/pdfbox/pdmodel/interactive/form/PlainText$TextAttribute;->WIDTH:Ljava/text/AttributedCharacterIterator$Attribute;

    invoke-interface {v3, v4}, Ljava/text/AttributedCharacterIterator;->getAttribute(Ljava/text/AttributedCharacterIterator$Attribute;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Float;

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v3

    add-float/2addr v1, v3

    invoke-virtual {v2}, Lorg/apache/pdfbox/pdmodel/interactive/form/PlainText$Word;->getText()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/PlainText$Line;->words:Ljava/util/List;

    invoke-interface {v4, v2}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v2

    iget-object v4, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/PlainText$Line;->words:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    if-ne v2, v4, :cond_0

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v3, v2}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-static {v2}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v3, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Lorg/apache/pdfbox/pdmodel/font/PDFont;->getStringWidth(Ljava/lang/String;)F

    move-result v2

    mul-float/2addr v2, p2

    sub-float/2addr v1, v2

    goto :goto_0

    :cond_1
    return v1
.end method

.method public getInterWordSpacing(F)F
    .locals 1

    iget v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/PlainText$Line;->lineWidth:F

    sub-float/2addr p1, v0

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/PlainText$Line;->words:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    int-to-float v0, v0

    div-float/2addr p1, v0

    return p1
.end method

.method public getWidth()F
    .locals 1

    iget v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/PlainText$Line;->lineWidth:F

    return v0
.end method

.method public getWords()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lorg/apache/pdfbox/pdmodel/interactive/form/PlainText$Word;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/PlainText$Line;->words:Ljava/util/List;

    return-object v0
.end method

.method public setWidth(F)V
    .locals 0

    iput p1, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/PlainText$Line;->lineWidth:F

    return-void
.end method

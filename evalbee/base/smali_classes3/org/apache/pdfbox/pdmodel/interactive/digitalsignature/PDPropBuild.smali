.class public Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/PDPropBuild;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/pdfbox/pdmodel/common/COSObjectable;


# instance fields
.field private dictionary:Lorg/apache/pdfbox/cos/COSDictionary;


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-direct {v0}, Lorg/apache/pdfbox/cos/COSDictionary;-><init>()V

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/PDPropBuild;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSBase;->setDirect(Z)V

    return-void
.end method

.method public constructor <init>(Lorg/apache/pdfbox/cos/COSDictionary;)V
    .locals 1

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/PDPropBuild;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lorg/apache/pdfbox/cos/COSBase;->setDirect(Z)V

    return-void
.end method


# virtual methods
.method public getApp()Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/PDPropBuildDataDict;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/PDPropBuild;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->APP:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSDictionary;

    if-eqz v0, :cond_0

    new-instance v1, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/PDPropBuildDataDict;

    invoke-direct {v1, v0}, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/PDPropBuildDataDict;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return-object v1
.end method

.method public getCOSObject()Lorg/apache/pdfbox/cos/COSBase;
    .locals 1

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/PDPropBuild;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    return-object v0
.end method

.method public getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/PDPropBuild;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    return-object v0
.end method

.method public getFilter()Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/PDPropBuildDataDict;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/PDPropBuild;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->FILTER:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSDictionary;

    if-eqz v0, :cond_0

    new-instance v1, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/PDPropBuildDataDict;

    invoke-direct {v1, v0}, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/PDPropBuildDataDict;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return-object v1
.end method

.method public getPubSec()Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/PDPropBuildDataDict;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/PDPropBuild;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->PUB_SEC:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSDictionary;

    if-eqz v0, :cond_0

    new-instance v1, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/PDPropBuildDataDict;

    invoke-direct {v1, v0}, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/PDPropBuildDataDict;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return-object v1
.end method

.method public setPDPropBuildApp(Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/PDPropBuildDataDict;)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/PDPropBuild;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->APP:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)V

    return-void
.end method

.method public setPDPropBuildFilter(Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/PDPropBuildDataDict;)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/PDPropBuild;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->FILTER:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)V

    return-void
.end method

.method public setPDPropBuildPubSec(Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/PDPropBuildDataDict;)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/PDPropBuild;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->PUB_SEC:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)V

    return-void
.end method

.class public abstract Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/pdfbox/pdmodel/common/COSObjectable;


# static fields
.field private static final FIELD_TYPE_BUTTON:Ljava/lang/String; = "Btn"

.field private static final FIELD_TYPE_CHOICE:Ljava/lang/String; = "Ch"

.field private static final FIELD_TYPE_SIGNATURE:Ljava/lang/String; = "Sig"

.field private static final FIELD_TYPE_TEXT:Ljava/lang/String; = "Tx"

.field private static final FLAG_NO_EXPORT:I = 0x4

.field private static final FLAG_READ_ONLY:I = 0x1

.field private static final FLAG_REQUIRED:I = 0x2


# instance fields
.field private acroForm:Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;

.field private dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

.field private parent:Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;


# direct methods
.method public constructor <init>(Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;)V
    .locals 2

    .line 1
    new-instance v0, Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-direct {v0}, Lorg/apache/pdfbox/cos/COSDictionary;-><init>()V

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;-><init>(Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;Lorg/apache/pdfbox/cos/COSDictionary;Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;)V

    return-void
.end method

.method public constructor <init>(Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;Lorg/apache/pdfbox/cos/COSDictionary;Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;)V
    .locals 0

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->acroForm:Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;

    iput-object p2, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    iput-object p3, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->parent:Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;

    return-void
.end method

.method private static createButtonSubType(Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;Lorg/apache/pdfbox/cos/COSDictionary;Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;)Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;
    .locals 2

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->FF:Lorg/apache/pdfbox/cos/COSName;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getInt(Lorg/apache/pdfbox/cos/COSName;I)I

    move-result v0

    const v1, 0x8000

    and-int/2addr v1, v0

    if-eqz v1, :cond_0

    new-instance v0, Lorg/apache/pdfbox/pdmodel/interactive/form/PDRadioButton;

    invoke-direct {v0, p0, p1, p2}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDRadioButton;-><init>(Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;Lorg/apache/pdfbox/cos/COSDictionary;Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;)V

    return-object v0

    :cond_0
    const/high16 v1, 0x10000

    and-int/2addr v0, v1

    if-eqz v0, :cond_1

    new-instance v0, Lorg/apache/pdfbox/pdmodel/interactive/form/PDPushButton;

    invoke-direct {v0, p0, p1, p2}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDPushButton;-><init>(Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;Lorg/apache/pdfbox/cos/COSDictionary;Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;)V

    return-object v0

    :cond_1
    new-instance v0, Lorg/apache/pdfbox/pdmodel/interactive/form/PDCheckbox;

    invoke-direct {v0, p0, p1, p2}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDCheckbox;-><init>(Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;Lorg/apache/pdfbox/cos/COSDictionary;Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;)V

    return-object v0
.end method

.method private static createChoiceSubType(Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;Lorg/apache/pdfbox/cos/COSDictionary;Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;)Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;
    .locals 2

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->FF:Lorg/apache/pdfbox/cos/COSName;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getInt(Lorg/apache/pdfbox/cos/COSName;I)I

    move-result v0

    const/high16 v1, 0x20000

    and-int/2addr v0, v1

    if-eqz v0, :cond_0

    new-instance v0, Lorg/apache/pdfbox/pdmodel/interactive/form/PDComboBox;

    invoke-direct {v0, p0, p1, p2}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDComboBox;-><init>(Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;Lorg/apache/pdfbox/cos/COSDictionary;Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;)V

    return-object v0

    :cond_0
    new-instance v0, Lorg/apache/pdfbox/pdmodel/interactive/form/PDListBox;

    invoke-direct {v0, p0, p1, p2}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDListBox;-><init>(Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;Lorg/apache/pdfbox/cos/COSDictionary;Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;)V

    return-object v0
.end method

.method public static createField(Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;Lorg/apache/pdfbox/cos/COSDictionary;Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;)Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;
    .locals 2

    invoke-static {p1}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->findFieldType(Lorg/apache/pdfbox/cos/COSDictionary;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "Ch"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0, p1, p2}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->createChoiceSubType(Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;Lorg/apache/pdfbox/cos/COSDictionary;Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;)Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;

    move-result-object p0

    return-object p0

    :cond_0
    const-string v1, "Tx"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance v0, Lorg/apache/pdfbox/pdmodel/interactive/form/PDTextField;

    invoke-direct {v0, p0, p1, p2}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDTextField;-><init>(Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;Lorg/apache/pdfbox/cos/COSDictionary;Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;)V

    return-object v0

    :cond_1
    const-string v1, "Sig"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    new-instance v0, Lorg/apache/pdfbox/pdmodel/interactive/form/PDSignatureField;

    invoke-direct {v0, p0, p1, p2}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDSignatureField;-><init>(Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;Lorg/apache/pdfbox/cos/COSDictionary;Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;)V

    return-object v0

    :cond_2
    const-string v1, "Btn"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {p0, p1, p2}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->createButtonSubType(Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;Lorg/apache/pdfbox/cos/COSDictionary;Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;)Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;

    move-result-object p0

    return-object p0

    :cond_3
    new-instance v0, Lorg/apache/pdfbox/pdmodel/interactive/form/PDNonTerminalField;

    invoke-direct {v0, p0, p1, p2}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDNonTerminalField;-><init>(Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;Lorg/apache/pdfbox/cos/COSDictionary;Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;)V

    return-object v0
.end method

.method private static findFieldType(Lorg/apache/pdfbox/cos/COSDictionary;)Ljava/lang/String;
    .locals 3

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->FT:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p0, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->getNameAsString(Lorg/apache/pdfbox/cos/COSName;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->PARENT:Lorg/apache/pdfbox/cos/COSName;

    sget-object v2, Lorg/apache/pdfbox/cos/COSName;->P:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object p0

    check-cast p0, Lorg/apache/pdfbox/cos/COSDictionary;

    if-eqz p0, :cond_0

    invoke-static {p0}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->findFieldType(Lorg/apache/pdfbox/cos/COSDictionary;)Ljava/lang/String;

    move-result-object v0

    :cond_0
    return-object v0
.end method


# virtual methods
.method public findKid([Ljava/lang/String;I)Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;
    .locals 6

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->KIDS:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSArray;

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    const/4 v2, 0x0

    :goto_0
    if-nez v1, :cond_1

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSArray;->size()I

    move-result v3

    if-ge v2, v3, :cond_1

    invoke-virtual {v0, v2}, Lorg/apache/pdfbox/cos/COSArray;->getObject(I)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v3

    check-cast v3, Lorg/apache/pdfbox/cos/COSDictionary;

    aget-object v4, p1, p2

    sget-object v5, Lorg/apache/pdfbox/cos/COSName;->T:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v3, v5}, Lorg/apache/pdfbox/cos/COSDictionary;->getString(Lorg/apache/pdfbox/cos/COSName;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v1, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->acroForm:Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;

    invoke-static {v1, v3, p0}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->createField(Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;Lorg/apache/pdfbox/cos/COSDictionary;Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;)Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;

    move-result-object v1

    array-length v3, p1

    add-int/lit8 v4, p2, 0x1

    if-le v3, v4, :cond_0

    invoke-virtual {v1, p1, v4}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->findKid([Ljava/lang/String;I)Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;

    move-result-object v1

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    return-object v1
.end method

.method public getAcroForm()Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->acroForm:Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;

    return-object v0
.end method

.method public getActions()Lorg/apache/pdfbox/pdmodel/interactive/action/PDFormFieldAdditionalActions;
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->AA:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSDictionary;

    if-eqz v0, :cond_0

    new-instance v1, Lorg/apache/pdfbox/pdmodel/interactive/action/PDFormFieldAdditionalActions;

    invoke-direct {v1, v0}, Lorg/apache/pdfbox/pdmodel/interactive/action/PDFormFieldAdditionalActions;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return-object v1
.end method

.method public getAlternateFieldName()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->TU:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getString(Lorg/apache/pdfbox/cos/COSName;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getAsTextStream(Lorg/apache/pdfbox/cos/COSBase;)Lorg/apache/pdfbox/pdmodel/common/PDTextStream;
    .locals 3

    if-nez p1, :cond_0

    const/4 p1, 0x0

    return-object p1

    :cond_0
    invoke-static {p1}, Lorg/apache/pdfbox/pdmodel/common/PDTextStream;->createTextStream(Lorg/apache/pdfbox/cos/COSBase;)Lorg/apache/pdfbox/pdmodel/common/PDTextStream;

    move-result-object v0

    if-eqz v0, :cond_1

    return-object v0

    :cond_1
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid field value. Unexpected type "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getCOSObject()Lorg/apache/pdfbox/cos/COSBase;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    return-object v0
.end method

.method public abstract getDefaultValue()Ljava/lang/Object;
.end method

.method public getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    return-object v0
.end method

.method public abstract getFieldFlags()I
.end method

.method public abstract getFieldType()Ljava/lang/String;
.end method

.method public getFullyQualifiedName()Ljava/lang/String;
    .locals 3

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->getPartialName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->getParent()Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->getParent()Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->getFullyQualifiedName()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-eqz v1, :cond_2

    if-eqz v0, :cond_1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "."

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_1
    move-object v0, v1

    :cond_2
    :goto_1
    return-object v0
.end method

.method public getInheritableAttribute(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;
    .locals 1

    invoke-virtual {p0, p0, p1}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->getInheritableAttributesNode(Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    :goto_0
    invoke-virtual {v0, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object p1

    return-object p1

    :cond_0
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->getAcroForm()Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    goto :goto_0
.end method

.method public getInheritableAttributesNode(Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;
    .locals 1

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    invoke-virtual {v0, p2}, Lorg/apache/pdfbox/cos/COSDictionary;->containsKey(Lorg/apache/pdfbox/cos/COSName;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-object p1

    :cond_0
    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->getParent()Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;

    move-result-object p1

    if-eqz p1, :cond_1

    invoke-virtual {p0, p1, p2}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->getInheritableAttributesNode(Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;

    :cond_1
    const/4 p1, 0x0

    return-object p1
.end method

.method public getKids()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lorg/apache/pdfbox/pdmodel/common/COSObjectable;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->KIDS:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSArray;

    if-eqz v0, :cond_4

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    const/4 v2, 0x0

    :goto_0
    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSArray;->size()I

    move-result v3

    if-ge v2, v3, :cond_3

    invoke-virtual {v0, v2}, Lorg/apache/pdfbox/cos/COSArray;->getObject(I)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v3

    check-cast v3, Lorg/apache/pdfbox/cos/COSDictionary;

    if-nez v3, :cond_0

    goto :goto_1

    :cond_0
    sget-object v4, Lorg/apache/pdfbox/cos/COSName;->T:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v3, v4}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v4

    if-eqz v4, :cond_1

    iget-object v4, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->acroForm:Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;

    invoke-static {v4, v3, p0}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->createField(Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;Lorg/apache/pdfbox/cos/COSDictionary;Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;)Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;

    move-result-object v3

    if-eqz v3, :cond_2

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    new-instance v4, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotationWidget;

    invoke-direct {v4, v3}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotationWidget;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_3
    new-instance v2, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;

    invoke-direct {v2, v1, v0}, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;-><init>(Ljava/util/List;Lorg/apache/pdfbox/cos/COSArray;)V

    goto :goto_2

    :cond_4
    const/4 v2, 0x0

    :goto_2
    return-object v2
.end method

.method public getMappingName()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->TM:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getString(Lorg/apache/pdfbox/cos/COSName;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getParent()Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->parent:Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;

    return-object v0
.end method

.method public getPartialName()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->T:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getString(Lorg/apache/pdfbox/cos/COSName;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public abstract getValue()Ljava/lang/Object;
.end method

.method public getWidget()Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotationWidget;
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->getKids()Ljava/util/List;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotationWidget;

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotationWidget;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    goto :goto_0

    :cond_0
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotationWidget;

    if-eqz v1, :cond_1

    check-cast v0, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotationWidget;

    goto :goto_0

    :cond_1
    check-cast v0, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->getWidget()Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotationWidget;

    move-result-object v0

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public importFDF(Lorg/apache/pdfbox/pdmodel/fdf/FDFField;)V
    .locals 7

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/fdf/FDFField;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->getFieldFlags()I

    move-result v1

    if-eqz v0, :cond_2

    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_0

    check-cast v0, Ljava/lang/String;

    :goto_0
    invoke-virtual {p0, v0}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->setValue(Ljava/lang/String;)V

    goto :goto_1

    :cond_0
    instance-of v2, v0, Lorg/apache/pdfbox/pdmodel/common/PDTextStream;

    if-eqz v2, :cond_1

    check-cast v0, Lorg/apache/pdfbox/pdmodel/common/PDTextStream;

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/common/PDTextStream;->getAsString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    new-instance p1, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown field type:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_2
    :goto_1
    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/fdf/FDFField;->getFieldFlags()Ljava/lang/Integer;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    :goto_2
    invoke-virtual {p0, v0}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->setFieldFlags(I)V

    goto :goto_3

    :cond_3
    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/fdf/FDFField;->getSetFieldFlags()Ljava/lang/Integer;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    or-int/2addr v1, v0

    invoke-virtual {p0, v1}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->setFieldFlags(I)V

    :cond_4
    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/fdf/FDFField;->getClearFieldFlags()Ljava/lang/Integer;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    not-int v0, v0

    and-int/2addr v0, v1

    goto :goto_2

    :cond_5
    :goto_3
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->getWidget()Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotationWidget;

    move-result-object v0

    if-eqz v0, :cond_8

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotation;->getAnnotationFlags()I

    move-result v1

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/fdf/FDFField;->getWidgetFieldFlags()Ljava/lang/Integer;

    move-result-object v2

    if-eqz v2, :cond_6

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    :goto_4
    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotation;->setAnnotationFlags(I)V

    goto :goto_5

    :cond_6
    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/fdf/FDFField;->getSetWidgetFieldFlags()Ljava/lang/Integer;

    move-result-object v2

    if-eqz v2, :cond_7

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    or-int/2addr v1, v2

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotation;->setAnnotationFlags(I)V

    :cond_7
    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/fdf/FDFField;->getClearWidgetFieldFlags()Ljava/lang/Integer;

    move-result-object v2

    if-eqz v2, :cond_8

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    int-to-long v2, v2

    const-wide v4, 0xffffffffL

    xor-long/2addr v2, v4

    long-to-int v2, v2

    and-int/2addr v1, v2

    goto :goto_4

    :cond_8
    :goto_5
    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/fdf/FDFField;->getKids()Ljava/util/List;

    move-result-object p1

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->getKids()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    :goto_6
    if-eqz p1, :cond_b

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_b

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/pdfbox/pdmodel/fdf/FDFField;

    invoke-virtual {v2}, Lorg/apache/pdfbox/pdmodel/fdf/FDFField;->getPartialFieldName()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_9
    :goto_7
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_a

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/apache/pdfbox/pdmodel/common/COSObjectable;

    instance-of v6, v5, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;

    if-eqz v6, :cond_9

    check-cast v5, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;

    if-eqz v3, :cond_9

    invoke-virtual {v5}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->getPartialName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_9

    invoke-virtual {v5, v2}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->importFDF(Lorg/apache/pdfbox/pdmodel/fdf/FDFField;)V

    goto :goto_7

    :cond_a
    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    :cond_b
    return-void
.end method

.method public isNoExport()Z
    .locals 3

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->FF:Lorg/apache/pdfbox/cos/COSName;

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->getFlag(Lorg/apache/pdfbox/cos/COSName;I)Z

    move-result v0

    return v0
.end method

.method public isReadonly()Z
    .locals 3

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->FF:Lorg/apache/pdfbox/cos/COSName;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->getFlag(Lorg/apache/pdfbox/cos/COSName;I)Z

    move-result v0

    return v0
.end method

.method public isRequired()Z
    .locals 3

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->FF:Lorg/apache/pdfbox/cos/COSName;

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->getFlag(Lorg/apache/pdfbox/cos/COSName;I)Z

    move-result v0

    return v0
.end method

.method public removeInheritableAttribute(Lorg/apache/pdfbox/cos/COSName;)V
    .locals 1

    invoke-virtual {p0, p0, p1}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->getInheritableAttributesNode(Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->removeItem(Lorg/apache/pdfbox/cos/COSName;)V

    :cond_0
    return-void
.end method

.method public setAcroForm(Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;)V
    .locals 0

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->acroForm:Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;

    return-void
.end method

.method public setAlternateFieldName(Ljava/lang/String;)V
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->TU:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setString(Lorg/apache/pdfbox/cos/COSName;Ljava/lang/String;)V

    return-void
.end method

.method public abstract setDefaultValue(Ljava/lang/String;)V
.end method

.method public setFieldFlags(I)V
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->FF:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setInt(Lorg/apache/pdfbox/cos/COSName;I)V

    return-void
.end method

.method public setInheritableAttribute(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V
    .locals 1

    if-nez p2, :cond_0

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->removeInheritableAttribute(Lorg/apache/pdfbox/cos/COSName;)V

    goto :goto_1

    :cond_0
    invoke-virtual {p0, p0, p1}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->getInheritableAttributesNode(Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    :goto_0
    invoke-virtual {v0, p1, p2}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    :goto_1
    return-void
.end method

.method public setKids(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lorg/apache/pdfbox/pdmodel/common/COSObjectable;",
            ">;)V"
        }
    .end annotation

    invoke-static {p1}, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;->converterToCOSArray(Ljava/util/List;)Lorg/apache/pdfbox/cos/COSArray;

    move-result-object p1

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->KIDS:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    return-void
.end method

.method public setMappingName(Ljava/lang/String;)V
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->TM:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setString(Lorg/apache/pdfbox/cos/COSName;Ljava/lang/String;)V

    return-void
.end method

.method public setNoExport(Z)V
    .locals 3

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->FF:Lorg/apache/pdfbox/cos/COSName;

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v2, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setFlag(Lorg/apache/pdfbox/cos/COSName;IZ)V

    return-void
.end method

.method public setParent(Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;)V
    .locals 2

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->parent:Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object p1

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->PARENT:Lorg/apache/pdfbox/cos/COSName;

    iget-object v1, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->parent:Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;

    invoke-virtual {v1}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object p1

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->PARENT:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p1, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->removeItem(Lorg/apache/pdfbox/cos/COSName;)V

    :goto_0
    return-void
.end method

.method public setPartialName(Ljava/lang/String;)V
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->T:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setString(Lorg/apache/pdfbox/cos/COSName;Ljava/lang/String;)V

    return-void
.end method

.method public setReadonly(Z)V
    .locals 3

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->FF:Lorg/apache/pdfbox/cos/COSName;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setFlag(Lorg/apache/pdfbox/cos/COSName;IZ)V

    return-void
.end method

.method public setRequired(Z)V
    .locals 3

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->FF:Lorg/apache/pdfbox/cos/COSName;

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setFlag(Lorg/apache/pdfbox/cos/COSName;IZ)V

    return-void
.end method

.method public abstract setValue(Ljava/lang/String;)V
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v1

    sget-object v2, Lorg/apache/pdfbox/cos/COSName;->V:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.class Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter$Builder;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private appearanceStyle:Lorg/apache/pdfbox/pdmodel/interactive/form/AppearanceStyle;

.field private composer:Lorg/apache/pdfbox/pdmodel/interactive/form/AppearancePrimitivesComposer;

.field private horizontalOffset:F

.field private textAlignment:Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter$TextAlign;

.field private textContent:Lorg/apache/pdfbox/pdmodel/interactive/form/PlainText;

.field private verticalOffset:F

.field private width:F

.field private wrapLines:Z


# direct methods
.method public constructor <init>(Lorg/apache/pdfbox/pdmodel/interactive/form/AppearancePrimitivesComposer;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter$Builder;->wrapLines:Z

    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter$Builder;->width:F

    sget-object v1, Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter$TextAlign;->LEFT:Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter$TextAlign;

    iput-object v1, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter$Builder;->textAlignment:Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter$TextAlign;

    iput v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter$Builder;->horizontalOffset:F

    iput v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter$Builder;->verticalOffset:F

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter$Builder;->composer:Lorg/apache/pdfbox/pdmodel/interactive/form/AppearancePrimitivesComposer;

    return-void
.end method

.method public static synthetic access$100(Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter$Builder;)Lorg/apache/pdfbox/pdmodel/interactive/form/AppearanceStyle;
    .locals 0

    iget-object p0, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter$Builder;->appearanceStyle:Lorg/apache/pdfbox/pdmodel/interactive/form/AppearanceStyle;

    return-object p0
.end method

.method public static synthetic access$200(Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter$Builder;)Z
    .locals 0

    iget-boolean p0, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter$Builder;->wrapLines:Z

    return p0
.end method

.method public static synthetic access$300(Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter$Builder;)F
    .locals 0

    iget p0, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter$Builder;->width:F

    return p0
.end method

.method public static synthetic access$400(Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter$Builder;)Lorg/apache/pdfbox/pdmodel/interactive/form/AppearancePrimitivesComposer;
    .locals 0

    iget-object p0, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter$Builder;->composer:Lorg/apache/pdfbox/pdmodel/interactive/form/AppearancePrimitivesComposer;

    return-object p0
.end method

.method public static synthetic access$500(Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter$Builder;)Lorg/apache/pdfbox/pdmodel/interactive/form/PlainText;
    .locals 0

    iget-object p0, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter$Builder;->textContent:Lorg/apache/pdfbox/pdmodel/interactive/form/PlainText;

    return-object p0
.end method

.method public static synthetic access$600(Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter$Builder;)Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter$TextAlign;
    .locals 0

    iget-object p0, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter$Builder;->textAlignment:Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter$TextAlign;

    return-object p0
.end method

.method public static synthetic access$700(Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter$Builder;)F
    .locals 0

    iget p0, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter$Builder;->horizontalOffset:F

    return p0
.end method

.method public static synthetic access$800(Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter$Builder;)F
    .locals 0

    iget p0, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter$Builder;->verticalOffset:F

    return p0
.end method


# virtual methods
.method public build()Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter;
    .locals 2

    new-instance v0, Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter;-><init>(Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter$Builder;Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter$1;)V

    return-object v0
.end method

.method public initialOffset(FF)Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter$Builder;
    .locals 0

    iput p1, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter$Builder;->horizontalOffset:F

    iput p2, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter$Builder;->verticalOffset:F

    return-object p0
.end method

.method public style(Lorg/apache/pdfbox/pdmodel/interactive/form/AppearanceStyle;)Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter$Builder;
    .locals 0

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter$Builder;->appearanceStyle:Lorg/apache/pdfbox/pdmodel/interactive/form/AppearanceStyle;

    return-object p0
.end method

.method public text(Lorg/apache/pdfbox/pdmodel/interactive/form/PlainText;)Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter$Builder;
    .locals 0

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter$Builder;->textContent:Lorg/apache/pdfbox/pdmodel/interactive/form/PlainText;

    return-object p0
.end method

.method public textAlign(I)Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter$Builder;
    .locals 0

    .line 1
    invoke-static {p1}, Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter$TextAlign;->valueOf(I)Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter$TextAlign;

    move-result-object p1

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter$Builder;->textAlignment:Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter$TextAlign;

    return-object p0
.end method

.method public textAlign(Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter$TextAlign;)Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter$Builder;
    .locals 0

    .line 2
    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter$Builder;->textAlignment:Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter$TextAlign;

    return-object p0
.end method

.method public width(F)Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter$Builder;
    .locals 0

    iput p1, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter$Builder;->width:F

    return-object p0
.end method

.method public wrapLines(Z)Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter$Builder;
    .locals 0

    iput-boolean p1, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter$Builder;->wrapLines:Z

    return-object p0
.end method

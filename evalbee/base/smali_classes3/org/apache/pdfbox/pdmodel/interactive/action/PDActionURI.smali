.class public Lorg/apache/pdfbox/pdmodel/interactive/action/PDActionURI;
.super Lorg/apache/pdfbox/pdmodel/interactive/action/PDAction;
.source "SourceFile"


# static fields
.field public static final SUB_TYPE:Ljava/lang/String; = "URI"


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/interactive/action/PDAction;-><init>()V

    new-instance v0, Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-direct {v0}, Lorg/apache/pdfbox/cos/COSDictionary;-><init>()V

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/action/PDAction;->action:Lorg/apache/pdfbox/cos/COSDictionary;

    const-string v0, "URI"

    invoke-virtual {p0, v0}, Lorg/apache/pdfbox/pdmodel/interactive/action/PDAction;->setSubType(Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Lorg/apache/pdfbox/cos/COSDictionary;)V
    .locals 0

    .line 2
    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/interactive/action/PDAction;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    return-void
.end method


# virtual methods
.method public getS()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/action/PDAction;->action:Lorg/apache/pdfbox/cos/COSDictionary;

    const-string v1, "S"

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getNameAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getURI()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/action/PDAction;->action:Lorg/apache/pdfbox/cos/COSDictionary;

    const-string v1, "URI"

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public setS(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/action/PDAction;->action:Lorg/apache/pdfbox/cos/COSDictionary;

    const-string v1, "S"

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setName(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public setTrackMousePosition(Z)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/action/PDAction;->action:Lorg/apache/pdfbox/cos/COSDictionary;

    const-string v1, "IsMap"

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setBoolean(Ljava/lang/String;Z)V

    return-void
.end method

.method public setURI(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/action/PDAction;->action:Lorg/apache/pdfbox/cos/COSDictionary;

    const-string v1, "URI"

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setString(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public shouldTrackMousePosition()Z
    .locals 3

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/action/PDAction;->action:Lorg/apache/pdfbox/cos/COSDictionary;

    const-string v1, "IsMap"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.class public Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/destination/PDPageFitHeightDestination;
.super Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/destination/PDPageDestination;
.source "SourceFile"


# static fields
.field protected static final TYPE:Ljava/lang/String; = "FitV"

.field protected static final TYPE_BOUNDED:Ljava/lang/String; = "FitBV"


# direct methods
.method public constructor <init>()V
    .locals 3

    .line 1
    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/destination/PDPageDestination;-><init>()V

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/destination/PDPageDestination;->array:Lorg/apache/pdfbox/cos/COSArray;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSArray;->growToSize(I)V

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/destination/PDPageDestination;->array:Lorg/apache/pdfbox/cos/COSArray;

    const/4 v1, 0x1

    const-string v2, "FitV"

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSArray;->setName(ILjava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Lorg/apache/pdfbox/cos/COSArray;)V
    .locals 0

    .line 2
    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/destination/PDPageDestination;-><init>(Lorg/apache/pdfbox/cos/COSArray;)V

    return-void
.end method


# virtual methods
.method public fitBoundingBox()Z
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/destination/PDPageDestination;->array:Lorg/apache/pdfbox/cos/COSArray;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSArray;->getName(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "FitBV"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public getLeft()I
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/destination/PDPageDestination;->array:Lorg/apache/pdfbox/cos/COSArray;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSArray;->getInt(I)I

    move-result v0

    return v0
.end method

.method public setFitBoundingBox(Z)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/destination/PDPageDestination;->array:Lorg/apache/pdfbox/cos/COSArray;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSArray;->growToSize(I)V

    const/4 v0, 0x1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/destination/PDPageDestination;->array:Lorg/apache/pdfbox/cos/COSArray;

    const-string v1, "FitBV"

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/destination/PDPageDestination;->array:Lorg/apache/pdfbox/cos/COSArray;

    const-string v1, "FitV"

    :goto_0
    invoke-virtual {p1, v0, v1}, Lorg/apache/pdfbox/cos/COSArray;->setName(ILjava/lang/String;)V

    return-void
.end method

.method public setLeft(I)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/destination/PDPageDestination;->array:Lorg/apache/pdfbox/cos/COSArray;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSArray;->growToSize(I)V

    const/4 v0, -0x1

    const/4 v1, 0x2

    if-ne p1, v0, :cond_0

    iget-object p1, p0, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/destination/PDPageDestination;->array:Lorg/apache/pdfbox/cos/COSArray;

    const/4 v0, 0x0

    invoke-virtual {p1, v1, v0}, Lorg/apache/pdfbox/cos/COSArray;->set(ILorg/apache/pdfbox/cos/COSBase;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/destination/PDPageDestination;->array:Lorg/apache/pdfbox/cos/COSArray;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSArray;->setInt(II)V

    :goto_0
    return-void
.end method

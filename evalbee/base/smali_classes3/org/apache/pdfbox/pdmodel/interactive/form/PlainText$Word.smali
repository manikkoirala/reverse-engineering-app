.class Lorg/apache/pdfbox/pdmodel/interactive/form/PlainText$Word;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/pdfbox/pdmodel/interactive/form/PlainText;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Word"
.end annotation


# instance fields
.field private attributedString:Ljava/text/AttributedString;

.field private textContent:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/PlainText$Word;->textContent:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getAttributes()Ljava/text/AttributedString;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/PlainText$Word;->attributedString:Ljava/text/AttributedString;

    return-object v0
.end method

.method public getText()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/PlainText$Word;->textContent:Ljava/lang/String;

    return-object v0
.end method

.method public setAttributes(Ljava/text/AttributedString;)V
    .locals 0

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/PlainText$Word;->attributedString:Ljava/text/AttributedString;

    return-void
.end method

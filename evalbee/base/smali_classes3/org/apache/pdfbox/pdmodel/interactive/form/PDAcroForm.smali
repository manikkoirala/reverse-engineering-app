.class public final Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/pdfbox/pdmodel/common/COSObjectable;


# static fields
.field private static final FLAG_APPEND_ONLY:I = 0x2

.field private static final FLAG_SIGNATURES_EXIST:I = 0x1


# instance fields
.field private acroForm:Lorg/apache/pdfbox/cos/COSDictionary;

.field private document:Lorg/apache/pdfbox/pdmodel/PDDocument;

.field private fieldCache:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lorg/apache/pdfbox/pdmodel/PDDocument;)V
    .locals 2

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;->document:Lorg/apache/pdfbox/pdmodel/PDDocument;

    new-instance p1, Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-direct {p1}, Lorg/apache/pdfbox/cos/COSDictionary;-><init>()V

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;->acroForm:Lorg/apache/pdfbox/cos/COSDictionary;

    new-instance p1, Lorg/apache/pdfbox/cos/COSArray;

    invoke-direct {p1}, Lorg/apache/pdfbox/cos/COSArray;-><init>()V

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;->acroForm:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->FIELDS:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    return-void
.end method

.method public constructor <init>(Lorg/apache/pdfbox/pdmodel/PDDocument;Lorg/apache/pdfbox/cos/COSDictionary;)V
    .locals 0

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;->document:Lorg/apache/pdfbox/pdmodel/PDDocument;

    iput-object p2, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;->acroForm:Lorg/apache/pdfbox/cos/COSDictionary;

    return-void
.end method

.method private addFieldAndChildren(Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;",
            "Ljava/util/List<",
            "Lorg/apache/pdfbox/pdmodel/fdf/FDFField;",
            ">;)V"
        }
    .end annotation

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->getValue()Ljava/lang/Object;

    move-result-object v0

    new-instance v1, Lorg/apache/pdfbox/pdmodel/fdf/FDFField;

    invoke-direct {v1}, Lorg/apache/pdfbox/pdmodel/fdf/FDFField;-><init>()V

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->getPartialName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lorg/apache/pdfbox/pdmodel/fdf/FDFField;->setPartialFieldName(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Lorg/apache/pdfbox/pdmodel/fdf/FDFField;->setValue(Ljava/lang/Object;)V

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->getKids()Ljava/util/List;

    move-result-object p1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    if-eqz p1, :cond_1

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/pdfbox/pdmodel/common/COSObjectable;

    check-cast v3, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;

    invoke-direct {p0, v3, v2}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;->addFieldAndChildren(Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;Ljava/util/List;)V

    goto :goto_0

    :cond_0
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result p1

    if-nez p1, :cond_1

    invoke-virtual {v1, v2}, Lorg/apache/pdfbox/pdmodel/fdf/FDFField;->setKids(Ljava/util/List;)V

    :cond_1
    if-nez v0, :cond_2

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result p1

    if-nez p1, :cond_3

    :cond_2
    invoke-interface {p2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_3
    return-void
.end method


# virtual methods
.method public exportFDF()Lorg/apache/pdfbox/pdmodel/fdf/FDFDocument;
    .locals 5

    new-instance v0, Lorg/apache/pdfbox/pdmodel/fdf/FDFDocument;

    invoke-direct {v0}, Lorg/apache/pdfbox/pdmodel/fdf/FDFDocument;-><init>()V

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/fdf/FDFDocument;->getCatalog()Lorg/apache/pdfbox/pdmodel/fdf/FDFCatalog;

    move-result-object v1

    new-instance v2, Lorg/apache/pdfbox/pdmodel/fdf/FDFDictionary;

    invoke-direct {v2}, Lorg/apache/pdfbox/pdmodel/fdf/FDFDictionary;-><init>()V

    invoke-virtual {v1, v2}, Lorg/apache/pdfbox/pdmodel/fdf/FDFCatalog;->setFDF(Lorg/apache/pdfbox/pdmodel/fdf/FDFDictionary;)V

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;->getFields()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;

    invoke-direct {p0, v4, v1}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;->addFieldAndChildren(Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;Ljava/util/List;)V

    goto :goto_0

    :cond_0
    iget-object v3, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;->document:Lorg/apache/pdfbox/pdmodel/PDDocument;

    invoke-virtual {v3}, Lorg/apache/pdfbox/pdmodel/PDDocument;->getDocument()Lorg/apache/pdfbox/cos/COSDocument;

    move-result-object v3

    invoke-virtual {v3}, Lorg/apache/pdfbox/cos/COSDocument;->getDocumentID()Lorg/apache/pdfbox/cos/COSArray;

    move-result-object v3

    invoke-virtual {v2, v3}, Lorg/apache/pdfbox/pdmodel/fdf/FDFDictionary;->setID(Lorg/apache/pdfbox/cos/COSArray;)V

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1

    invoke-virtual {v2, v1}, Lorg/apache/pdfbox/pdmodel/fdf/FDFDictionary;->setFields(Ljava/util/List;)V

    :cond_1
    return-object v0
.end method

.method public getCOSObject()Lorg/apache/pdfbox/cos/COSBase;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;->acroForm:Lorg/apache/pdfbox/cos/COSDictionary;

    return-object v0
.end method

.method public getDefaultAppearance()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->DA:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getItem(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSString;

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSString;->getString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultResources()Lorg/apache/pdfbox/pdmodel/PDResources;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;->acroForm:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->DR:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSDictionary;

    if-eqz v0, :cond_0

    new-instance v1, Lorg/apache/pdfbox/pdmodel/PDResources;

    invoke-direct {v1, v0}, Lorg/apache/pdfbox/pdmodel/PDResources;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return-object v1
.end method

.method public getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;->acroForm:Lorg/apache/pdfbox/cos/COSDictionary;

    return-object v0
.end method

.method public getDocument()Lorg/apache/pdfbox/pdmodel/PDDocument;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;->document:Lorg/apache/pdfbox/pdmodel/PDDocument;

    return-object v0
.end method

.method public getField(Ljava/lang/String;)Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;
    .locals 9

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;->fieldCache:Ljava/util/Map;

    if-eqz v0, :cond_0

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;

    goto :goto_1

    :cond_0
    const-string v0, "\\."

    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;->acroForm:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v2, Lorg/apache/pdfbox/cos/COSName;->FIELDS:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v1

    check-cast v1, Lorg/apache/pdfbox/cos/COSArray;

    const/4 v2, 0x0

    const/4 v3, 0x0

    move v5, v2

    move-object v4, v3

    :goto_0
    invoke-virtual {v1}, Lorg/apache/pdfbox/cos/COSArray;->size()I

    move-result v6

    if-ge v5, v6, :cond_3

    if-nez v4, :cond_3

    invoke-virtual {v1, v5}, Lorg/apache/pdfbox/cos/COSArray;->getObject(I)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v6

    check-cast v6, Lorg/apache/pdfbox/cos/COSDictionary;

    if-eqz v6, :cond_2

    sget-object v7, Lorg/apache/pdfbox/cos/COSName;->T:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v6, v7}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v7

    check-cast v7, Lorg/apache/pdfbox/cos/COSString;

    invoke-virtual {v7}, Lorg/apache/pdfbox/cos/COSString;->getString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_1

    invoke-virtual {v7}, Lorg/apache/pdfbox/cos/COSString;->getString()Ljava/lang/String;

    move-result-object v7

    aget-object v8, v0, v2

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    :cond_1
    invoke-static {p0, v6, v3}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->createField(Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;Lorg/apache/pdfbox/cos/COSDictionary;Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;)Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;

    move-result-object v4

    array-length v6, v0

    const/4 v7, 0x1

    if-le v6, v7, :cond_2

    invoke-virtual {v4, v0, v7}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->findKid([Ljava/lang/String;I)Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;

    move-result-object v6

    if-eqz v6, :cond_2

    move-object v4, v6

    :cond_2
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    :cond_3
    move-object p1, v4

    :goto_1
    return-object p1
.end method

.method public getFields()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;->acroForm:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->FIELDS:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSArray;

    if-nez v0, :cond_0

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    return-object v0

    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    const/4 v2, 0x0

    :goto_0
    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSArray;->size()I

    move-result v3

    if-ge v2, v3, :cond_2

    invoke-virtual {v0, v2}, Lorg/apache/pdfbox/cos/COSArray;->getObject(I)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v3

    check-cast v3, Lorg/apache/pdfbox/cos/COSDictionary;

    if-eqz v3, :cond_1

    const/4 v4, 0x0

    invoke-static {p0, v3, v4}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->createField(Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;Lorg/apache/pdfbox/cos/COSDictionary;Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;)Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    new-instance v2, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;

    invoke-direct {v2, v1, v0}, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;-><init>(Ljava/util/List;Lorg/apache/pdfbox/cos/COSArray;)V

    return-object v2
.end method

.method public getQ()I
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->Q:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSNumber;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSNumber;->intValue()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public getXFA()Lorg/apache/pdfbox/pdmodel/interactive/form/PDXFAResource;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;->acroForm:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->XFA:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v1, Lorg/apache/pdfbox/pdmodel/interactive/form/PDXFAResource;

    invoke-direct {v1, v0}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDXFAResource;-><init>(Lorg/apache/pdfbox/cos/COSBase;)V

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return-object v1
.end method

.method public hasXFA()Z
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;->acroForm:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->XFA:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->containsKey(Lorg/apache/pdfbox/cos/COSName;)Z

    move-result v0

    return v0
.end method

.method public importFDF(Lorg/apache/pdfbox/pdmodel/fdf/FDFDocument;)V
    .locals 2

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/fdf/FDFDocument;->getCatalog()Lorg/apache/pdfbox/pdmodel/fdf/FDFCatalog;

    move-result-object p1

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/fdf/FDFCatalog;->getFDF()Lorg/apache/pdfbox/pdmodel/fdf/FDFDictionary;

    move-result-object p1

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/fdf/FDFDictionary;->getFields()Ljava/util/List;

    move-result-object p1

    if-eqz p1, :cond_1

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/pdmodel/fdf/FDFField;

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/fdf/FDFField;->getPartialFieldName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;->getField(Ljava/lang/String;)Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1, v0}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->importFDF(Lorg/apache/pdfbox/pdmodel/fdf/FDFField;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public isAppendOnly()Z
    .locals 3

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->SIG_FLAGS:Lorg/apache/pdfbox/cos/COSName;

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->getFlag(Lorg/apache/pdfbox/cos/COSName;I)Z

    move-result v0

    return v0
.end method

.method public isCachingFields()Z
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;->fieldCache:Ljava/util/Map;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isNeedAppearances()Z
    .locals 3

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->NEED_APPEARANCES:Lorg/apache/pdfbox/cos/COSName;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->getBoolean(Lorg/apache/pdfbox/cos/COSName;Z)Z

    move-result v0

    return v0
.end method

.method public isSignaturesExist()Z
    .locals 3

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->SIG_FLAGS:Lorg/apache/pdfbox/cos/COSName;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->getFlag(Lorg/apache/pdfbox/cos/COSName;I)Z

    move-result v0

    return v0
.end method

.method public setAppendOnly(Z)V
    .locals 3

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->SIG_FLAGS:Lorg/apache/pdfbox/cos/COSName;

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setFlag(Lorg/apache/pdfbox/cos/COSName;IZ)V

    return-void
.end method

.method public setCacheFields(Z)V
    .locals 3

    if-eqz p1, :cond_0

    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;->fieldCache:Ljava/util/Map;

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;->getFields()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;

    iget-object v1, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;->fieldCache:Ljava/util/Map;

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->getFullyQualifiedName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;->fieldCache:Ljava/util/Map;

    :cond_1
    return-void
.end method

.method public setDefaultAppearance(Ljava/lang/String;)V
    .locals 2

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->DA:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setString(Lorg/apache/pdfbox/cos/COSName;Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object p1

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->DA:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p1, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->removeItem(Lorg/apache/pdfbox/cos/COSName;)V

    :goto_0
    return-void
.end method

.method public setDefaultResources(Lorg/apache/pdfbox/pdmodel/PDResources;)V
    .locals 2

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/PDResources;->getCOSObject()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;->acroForm:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->DR:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    return-void
.end method

.method public setFields(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;->acroForm:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->FIELDS:Lorg/apache/pdfbox/cos/COSName;

    invoke-static {p1}, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;->converterToCOSArray(Ljava/util/List;)Lorg/apache/pdfbox/cos/COSArray;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    return-void
.end method

.method public setNeedAppearances(Ljava/lang/Boolean;)V
    .locals 2

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->NEED_APPEARANCES:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p1

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setBoolean(Lorg/apache/pdfbox/cos/COSName;Z)V

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object p1

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->NEED_APPEARANCES:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p1, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->removeItem(Lorg/apache/pdfbox/cos/COSName;)V

    :goto_0
    return-void
.end method

.method public setQ(I)V
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->Q:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setInt(Lorg/apache/pdfbox/cos/COSName;I)V

    return-void
.end method

.method public setSignaturesExist(Z)V
    .locals 3

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->SIG_FLAGS:Lorg/apache/pdfbox/cos/COSName;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setFlag(Lorg/apache/pdfbox/cos/COSName;IZ)V

    return-void
.end method

.method public setXFA(Lorg/apache/pdfbox/pdmodel/interactive/form/PDXFAResource;)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;->acroForm:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->XFA:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)V

    return-void
.end method

.method public xfaIsDynamic()Z
    .locals 1

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;->hasXFA()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;->getFields()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

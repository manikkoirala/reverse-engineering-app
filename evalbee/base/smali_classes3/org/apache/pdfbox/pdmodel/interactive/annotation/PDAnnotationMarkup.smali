.class public Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotationMarkup;
.super Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotation;
.source "SourceFile"


# static fields
.field public static final RT_GROUP:Ljava/lang/String; = "Group"

.field public static final RT_REPLY:Ljava/lang/String; = "R"

.field public static final SUB_TYPE_CARET:Ljava/lang/String; = "Caret"

.field public static final SUB_TYPE_FREETEXT:Ljava/lang/String; = "FreeText"

.field public static final SUB_TYPE_INK:Ljava/lang/String; = "Ink"

.field public static final SUB_TYPE_POLYGON:Ljava/lang/String; = "Polygon"

.field public static final SUB_TYPE_POLYLINE:Ljava/lang/String; = "PolyLine"

.field public static final SUB_TYPE_SOUND:Ljava/lang/String; = "Sound"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotation;-><init>()V

    return-void
.end method

.method public constructor <init>(Lorg/apache/pdfbox/cos/COSDictionary;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotation;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    return-void
.end method


# virtual methods
.method public getConstantOpacity()F
    .locals 3

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotation;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    const-string v1, "CA"

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->getFloat(Ljava/lang/String;F)F

    move-result v0

    return v0
.end method

.method public getCreationDate()Ljava/util/Calendar;
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotation;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    const-string v1, "CreationDate"

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDate(Ljava/lang/String;)Ljava/util/Calendar;

    move-result-object v0

    return-object v0
.end method

.method public getExternalData()Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDExternalDataDictionary;
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotation;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    const-string v1, "ExData"

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Ljava/lang/String;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    instance-of v1, v0, Lorg/apache/pdfbox/cos/COSDictionary;

    if-eqz v1, :cond_0

    new-instance v1, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDExternalDataDictionary;

    check-cast v0, Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-direct {v1, v0}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDExternalDataDictionary;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    return-object v1

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public getInReplyTo()Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotation;
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotation;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    const-string v1, "IRT"

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Ljava/lang/String;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotation;->createAnnotation(Lorg/apache/pdfbox/cos/COSBase;)Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotation;

    move-result-object v0

    return-object v0
.end method

.method public getIntent()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotation;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    const-string v1, "IT"

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getNameAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPopup()Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotationPopup;
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotation;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    const-string v1, "Popup"

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Ljava/lang/String;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSDictionary;

    if-eqz v0, :cond_0

    new-instance v1, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotationPopup;

    invoke-direct {v1, v0}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotationPopup;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    return-object v1

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public getReplyType()Ljava/lang/String;
    .locals 3

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotation;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    const-string v1, "RT"

    const-string v2, "R"

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->getNameAsString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getRichContents()Lorg/apache/pdfbox/pdmodel/common/PDTextStream;
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotation;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    const-string v1, "RC"

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Ljava/lang/String;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {v0}, Lorg/apache/pdfbox/pdmodel/common/PDTextStream;->createTextStream(Lorg/apache/pdfbox/cos/COSBase;)Lorg/apache/pdfbox/pdmodel/common/PDTextStream;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public getSubject()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotation;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    const-string v1, "Subj"

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTitlePopup()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotation;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    const-string v1, "T"

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public setConstantOpacity(F)V
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotation;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    const-string v1, "CA"

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setFloat(Ljava/lang/String;F)V

    return-void
.end method

.method public setCreationDate(Ljava/util/Calendar;)V
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotation;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    const-string v1, "CreationDate"

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setDate(Ljava/lang/String;Ljava/util/Calendar;)V

    return-void
.end method

.method public setExternalData(Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDExternalDataDictionary;)V
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotation;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    const-string v1, "ExData"

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Ljava/lang/String;Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)V

    return-void
.end method

.method public setInReplyTo(Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotation;)V
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotation;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    const-string v1, "IRT"

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Ljava/lang/String;Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)V

    return-void
.end method

.method public setIntent(Ljava/lang/String;)V
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotation;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    const-string v1, "IT"

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setName(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public setPopup(Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotationPopup;)V
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotation;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    const-string v1, "Popup"

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Ljava/lang/String;Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)V

    return-void
.end method

.method public setReplyType(Ljava/lang/String;)V
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotation;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    const-string v1, "RT"

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setName(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public setRichContents(Lorg/apache/pdfbox/pdmodel/common/PDTextStream;)V
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotation;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    const-string v1, "RC"

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Ljava/lang/String;Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)V

    return-void
.end method

.method public setSubject(Ljava/lang/String;)V
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotation;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    const-string v1, "Subj"

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setString(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public setTitlePopup(Ljava/lang/String;)V
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotation;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    const-string v1, "T"

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setString(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

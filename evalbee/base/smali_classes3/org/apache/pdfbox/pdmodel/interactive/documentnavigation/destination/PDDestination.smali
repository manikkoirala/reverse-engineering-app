.class public abstract Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/destination/PDDestination;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/pdfbox/pdmodel/common/PDDestinationOrAction;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static create(Lorg/apache/pdfbox/cos/COSBase;)Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/destination/PDDestination;
    .locals 3

    if-nez p0, :cond_0

    const/4 p0, 0x0

    goto/16 :goto_4

    :cond_0
    instance-of v0, p0, Lorg/apache/pdfbox/cos/COSArray;

    if-eqz v0, :cond_9

    move-object v0, p0

    check-cast v0, Lorg/apache/pdfbox/cos/COSArray;

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSArray;->size()I

    move-result v1

    const/4 v2, 0x1

    if-le v1, v2, :cond_9

    invoke-virtual {v0, v2}, Lorg/apache/pdfbox/cos/COSArray;->getObject(I)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v1

    instance-of v1, v1, Lorg/apache/pdfbox/cos/COSName;

    if-eqz v1, :cond_9

    invoke-virtual {v0, v2}, Lorg/apache/pdfbox/cos/COSArray;->getObject(I)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object p0

    check-cast p0, Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p0}, Lorg/apache/pdfbox/cos/COSName;->getName()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Fit"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    const-string v2, "FitB"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    goto :goto_2

    :cond_1
    const-string v2, "FitV"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    const-string v2, "FitBV"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto :goto_1

    :cond_2
    const-string v2, "FitR"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    new-instance p0, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/destination/PDPageFitRectangleDestination;

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/destination/PDPageFitRectangleDestination;-><init>(Lorg/apache/pdfbox/cos/COSArray;)V

    goto :goto_4

    :cond_3
    const-string v2, "FitH"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    const-string v2, "FitBH"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    goto :goto_0

    :cond_4
    const-string v2, "XYZ"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    new-instance p0, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/destination/PDPageXYZDestination;

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/destination/PDPageXYZDestination;-><init>(Lorg/apache/pdfbox/cos/COSArray;)V

    goto :goto_4

    :cond_5
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown destination type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/apache/pdfbox/cos/COSName;->getName()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_6
    :goto_0
    new-instance p0, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/destination/PDPageFitWidthDestination;

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/destination/PDPageFitWidthDestination;-><init>(Lorg/apache/pdfbox/cos/COSArray;)V

    goto :goto_4

    :cond_7
    :goto_1
    new-instance p0, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/destination/PDPageFitHeightDestination;

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/destination/PDPageFitHeightDestination;-><init>(Lorg/apache/pdfbox/cos/COSArray;)V

    goto :goto_4

    :cond_8
    :goto_2
    new-instance p0, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/destination/PDPageFitDestination;

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/destination/PDPageFitDestination;-><init>(Lorg/apache/pdfbox/cos/COSArray;)V

    goto :goto_4

    :cond_9
    instance-of v0, p0, Lorg/apache/pdfbox/cos/COSString;

    if-eqz v0, :cond_a

    new-instance v0, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/destination/PDNamedDestination;

    check-cast p0, Lorg/apache/pdfbox/cos/COSString;

    invoke-direct {v0, p0}, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/destination/PDNamedDestination;-><init>(Lorg/apache/pdfbox/cos/COSString;)V

    :goto_3
    move-object p0, v0

    goto :goto_4

    :cond_a
    instance-of v0, p0, Lorg/apache/pdfbox/cos/COSName;

    if-eqz v0, :cond_b

    new-instance v0, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/destination/PDNamedDestination;

    check-cast p0, Lorg/apache/pdfbox/cos/COSName;

    invoke-direct {v0, p0}, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/destination/PDNamedDestination;-><init>(Lorg/apache/pdfbox/cos/COSName;)V

    goto :goto_3

    :goto_4
    return-object p0

    :cond_b
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Error: can\'t convert to Destination "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

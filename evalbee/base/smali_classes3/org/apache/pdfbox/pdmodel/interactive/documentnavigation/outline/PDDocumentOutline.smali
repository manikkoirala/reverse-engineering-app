.class public final Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDDocumentOutline;
.super Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineNode;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 3

    .line 1
    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineNode;-><init>()V

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/common/PDDictionaryWrapper;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->TYPE:Lorg/apache/pdfbox/cos/COSName;

    sget-object v2, Lorg/apache/pdfbox/cos/COSName;->OUTLINES:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v2}, Lorg/apache/pdfbox/cos/COSName;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->setName(Lorg/apache/pdfbox/cos/COSName;Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Lorg/apache/pdfbox/cos/COSDictionary;)V
    .locals 2

    .line 2
    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineNode;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/common/PDDictionaryWrapper;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object p1

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->TYPE:Lorg/apache/pdfbox/cos/COSName;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->OUTLINES:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v1}, Lorg/apache/pdfbox/cos/COSName;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->setName(Lorg/apache/pdfbox/cos/COSName;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public closeNode()V
    .locals 0

    return-void
.end method

.method public isNodeOpen()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public openNode()V
    .locals 0

    return-void
.end method

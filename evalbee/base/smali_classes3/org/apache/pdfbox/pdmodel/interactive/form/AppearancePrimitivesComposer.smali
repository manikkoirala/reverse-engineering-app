.class Lorg/apache/pdfbox/pdmodel/interactive/form/AppearancePrimitivesComposer;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final formatDecimal:Ljava/text/NumberFormat;

.field private inTextMode:Z

.field private final outputstream:Ljava/io/OutputStream;


# direct methods
.method public constructor <init>(Ljava/io/OutputStream;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {v0}, Ljava/text/NumberFormat;->getNumberInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/AppearancePrimitivesComposer;->formatDecimal:Ljava/text/NumberFormat;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/AppearancePrimitivesComposer;->inTextMode:Z

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/AppearancePrimitivesComposer;->outputstream:Ljava/io/OutputStream;

    return-void
.end method

.method private write(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/AppearancePrimitivesComposer;->outputstream:Ljava/io/OutputStream;

    sget-object v1, Lorg/apache/pdfbox/util/Charsets;->US_ASCII:Ljava/nio/charset/Charset;

    invoke-virtual {p1, v1}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/io/OutputStream;->write([B)V

    return-void
.end method

.method private writeOperand(F)V
    .locals 3

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/AppearancePrimitivesComposer;->formatDecimal:Ljava/text/NumberFormat;

    float-to-double v1, p1

    invoke-virtual {v0, v1, v2}, Ljava/text/NumberFormat;->format(D)Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/interactive/form/AppearancePrimitivesComposer;->write(Ljava/lang/String;)V

    iget-object p1, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/AppearancePrimitivesComposer;->outputstream:Ljava/io/OutputStream;

    const/16 v0, 0x20

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write(I)V

    return-void
.end method

.method private writeOperator(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/AppearancePrimitivesComposer;->outputstream:Ljava/io/OutputStream;

    sget-object v1, Lorg/apache/pdfbox/util/Charsets;->US_ASCII:Ljava/nio/charset/Charset;

    invoke-virtual {p1, v1}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/io/OutputStream;->write([B)V

    iget-object p1, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/AppearancePrimitivesComposer;->outputstream:Ljava/io/OutputStream;

    const/16 v0, 0xa

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write(I)V

    return-void
.end method


# virtual methods
.method public addRect(FFFF)V
    .locals 1

    .line 1
    iget-boolean v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/AppearancePrimitivesComposer;->inTextMode:Z

    if-nez v0, :cond_0

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/interactive/form/AppearancePrimitivesComposer;->writeOperand(F)V

    invoke-direct {p0, p2}, Lorg/apache/pdfbox/pdmodel/interactive/form/AppearancePrimitivesComposer;->writeOperand(F)V

    invoke-direct {p0, p3}, Lorg/apache/pdfbox/pdmodel/interactive/form/AppearancePrimitivesComposer;->writeOperand(F)V

    invoke-direct {p0, p4}, Lorg/apache/pdfbox/pdmodel/interactive/form/AppearancePrimitivesComposer;->writeOperand(F)V

    const-string p1, "re"

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/interactive/form/AppearancePrimitivesComposer;->writeOperator(Ljava/lang/String;)V

    return-void

    :cond_0
    new-instance p1, Ljava/io/IOException;

    const-string p2, "Error: addRect is not allowed within a text block."

    invoke-direct {p1, p2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public addRect(Lorg/apache/pdfbox/pdmodel/common/PDRectangle;)V
    .locals 3

    .line 2
    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->getLowerLeftX()F

    move-result v0

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->getLowerLeftY()F

    move-result v1

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->getWidth()F

    move-result v2

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->getHeight()F

    move-result p1

    invoke-virtual {p0, v0, v1, v2, p1}, Lorg/apache/pdfbox/pdmodel/interactive/form/AppearancePrimitivesComposer;->addRect(FFFF)V

    return-void
.end method

.method public beginText()V
    .locals 2

    iget-boolean v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/AppearancePrimitivesComposer;->inTextMode:Z

    if-nez v0, :cond_0

    const-string v0, "BT"

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/pdmodel/interactive/form/AppearancePrimitivesComposer;->writeOperator(Ljava/lang/String;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/AppearancePrimitivesComposer;->inTextMode:Z

    return-void

    :cond_0
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Error: Nested beginText() calls are not allowed."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public clip()V
    .locals 2

    iget-boolean v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/AppearancePrimitivesComposer;->inTextMode:Z

    if-nez v0, :cond_0

    const-string v0, "W"

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/pdmodel/interactive/form/AppearancePrimitivesComposer;->writeOperator(Ljava/lang/String;)V

    const-string v0, "n"

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/pdmodel/interactive/form/AppearancePrimitivesComposer;->writeOperator(Ljava/lang/String;)V

    return-void

    :cond_0
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Error: clip is not allowed within a text block."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public endText()V
    .locals 2

    iget-boolean v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/AppearancePrimitivesComposer;->inTextMode:Z

    if-eqz v0, :cond_0

    const-string v0, "ET"

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/pdmodel/interactive/form/AppearancePrimitivesComposer;->writeOperator(Ljava/lang/String;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/AppearancePrimitivesComposer;->inTextMode:Z

    return-void

    :cond_0
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Error: You must call beginText() before calling endText."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public newLineAtOffset(FF)V
    .locals 0

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/interactive/form/AppearancePrimitivesComposer;->writeOperand(F)V

    invoke-direct {p0, p2}, Lorg/apache/pdfbox/pdmodel/interactive/form/AppearancePrimitivesComposer;->writeOperand(F)V

    const-string p1, "Td"

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/interactive/form/AppearancePrimitivesComposer;->writeOperator(Ljava/lang/String;)V

    return-void
.end method

.method public showText(Ljava/lang/String;Lorg/apache/pdfbox/pdmodel/font/PDFont;)V
    .locals 0

    invoke-virtual {p2, p1}, Lorg/apache/pdfbox/pdmodel/font/PDFont;->encode(Ljava/lang/String;)[B

    move-result-object p1

    iget-object p2, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/AppearancePrimitivesComposer;->outputstream:Ljava/io/OutputStream;

    invoke-static {p1, p2}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->writeString([BLjava/io/OutputStream;)V

    const-string p1, " "

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/interactive/form/AppearancePrimitivesComposer;->write(Ljava/lang/String;)V

    const-string p1, "Tj"

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/interactive/form/AppearancePrimitivesComposer;->writeOperator(Ljava/lang/String;)V

    return-void
.end method

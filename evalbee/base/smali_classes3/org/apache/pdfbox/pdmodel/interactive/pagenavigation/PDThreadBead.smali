.class public Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDThreadBead;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/pdfbox/pdmodel/common/COSObjectable;


# instance fields
.field private bead:Lorg/apache/pdfbox/cos/COSDictionary;


# direct methods
.method public constructor <init>()V
    .locals 3

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-direct {v0}, Lorg/apache/pdfbox/cos/COSDictionary;-><init>()V

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDThreadBead;->bead:Lorg/apache/pdfbox/cos/COSDictionary;

    const-string v1, "Type"

    const-string v2, "Bead"

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->setName(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, p0}, Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDThreadBead;->setNextBead(Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDThreadBead;)V

    invoke-virtual {p0, p0}, Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDThreadBead;->setPreviousBead(Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDThreadBead;)V

    return-void
.end method

.method public constructor <init>(Lorg/apache/pdfbox/cos/COSDictionary;)V
    .locals 0

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDThreadBead;->bead:Lorg/apache/pdfbox/cos/COSDictionary;

    return-void
.end method


# virtual methods
.method public appendBead(Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDThreadBead;)V
    .locals 1

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDThreadBead;->getNextBead()Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDThreadBead;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDThreadBead;->setPreviousBead(Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDThreadBead;)V

    invoke-virtual {p1, v0}, Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDThreadBead;->setNextBead(Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDThreadBead;)V

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDThreadBead;->setNextBead(Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDThreadBead;)V

    invoke-virtual {p1, p0}, Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDThreadBead;->setPreviousBead(Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDThreadBead;)V

    return-void
.end method

.method public getCOSObject()Lorg/apache/pdfbox/cos/COSBase;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDThreadBead;->bead:Lorg/apache/pdfbox/cos/COSDictionary;

    return-object v0
.end method

.method public getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDThreadBead;->bead:Lorg/apache/pdfbox/cos/COSDictionary;

    return-object v0
.end method

.method public getNextBead()Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDThreadBead;
    .locals 3

    new-instance v0, Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDThreadBead;

    iget-object v1, p0, Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDThreadBead;->bead:Lorg/apache/pdfbox/cos/COSDictionary;

    const-string v2, "N"

    invoke-virtual {v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Ljava/lang/String;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v1

    check-cast v1, Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDThreadBead;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    return-object v0
.end method

.method public getPage()Lorg/apache/pdfbox/pdmodel/PDPage;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDThreadBead;->bead:Lorg/apache/pdfbox/cos/COSDictionary;

    const-string v1, "P"

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Ljava/lang/String;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSDictionary;

    if-eqz v0, :cond_0

    new-instance v1, Lorg/apache/pdfbox/pdmodel/PDPage;

    invoke-direct {v1, v0}, Lorg/apache/pdfbox/pdmodel/PDPage;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return-object v1
.end method

.method public getPreviousBead()Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDThreadBead;
    .locals 3

    new-instance v0, Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDThreadBead;

    iget-object v1, p0, Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDThreadBead;->bead:Lorg/apache/pdfbox/cos/COSDictionary;

    const-string v2, "V"

    invoke-virtual {v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Ljava/lang/String;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v1

    check-cast v1, Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDThreadBead;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    return-object v0
.end method

.method public getRectangle()Lorg/apache/pdfbox/pdmodel/common/PDRectangle;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDThreadBead;->bead:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->R:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSArray;

    if-eqz v0, :cond_0

    new-instance v1, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;

    invoke-direct {v1, v0}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;-><init>(Lorg/apache/pdfbox/cos/COSArray;)V

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return-object v1
.end method

.method public getThread()Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDThread;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDThreadBead;->bead:Lorg/apache/pdfbox/cos/COSDictionary;

    const-string v1, "T"

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Ljava/lang/String;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSDictionary;

    if-eqz v0, :cond_0

    new-instance v1, Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDThread;

    invoke-direct {v1, v0}, Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDThread;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return-object v1
.end method

.method public setNextBead(Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDThreadBead;)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDThreadBead;->bead:Lorg/apache/pdfbox/cos/COSDictionary;

    const-string v1, "N"

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Ljava/lang/String;Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)V

    return-void
.end method

.method public setPage(Lorg/apache/pdfbox/pdmodel/PDPage;)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDThreadBead;->bead:Lorg/apache/pdfbox/cos/COSDictionary;

    const-string v1, "P"

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Ljava/lang/String;Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)V

    return-void
.end method

.method public setPreviousBead(Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDThreadBead;)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDThreadBead;->bead:Lorg/apache/pdfbox/cos/COSDictionary;

    const-string v1, "V"

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Ljava/lang/String;Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)V

    return-void
.end method

.method public setRectangle(Lorg/apache/pdfbox/pdmodel/common/PDRectangle;)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDThreadBead;->bead:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->R:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)V

    return-void
.end method

.method public setThread(Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDThread;)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDThreadBead;->bead:Lorg/apache/pdfbox/cos/COSDictionary;

    const-string v1, "T"

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Ljava/lang/String;Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)V

    return-void
.end method

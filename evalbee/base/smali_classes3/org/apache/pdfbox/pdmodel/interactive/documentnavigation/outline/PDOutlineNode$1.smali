.class Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineNode$1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Iterable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineNode;->children()Ljava/lang/Iterable;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Iterable<",
        "Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineItem;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineNode;


# direct methods
.method public constructor <init>(Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineNode;)V
    .locals 0

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineNode$1;->this$0:Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineNode;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public iterator()Ljava/util/Iterator;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator<",
            "Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineItem;",
            ">;"
        }
    .end annotation

    new-instance v0, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineItemIterator;

    iget-object v1, p0, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineNode$1;->this$0:Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineNode;

    invoke-virtual {v1}, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineNode;->getFirstChild()Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineItem;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineItemIterator;-><init>(Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineItem;)V

    return-object v0
.end method

.class public Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDThread;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/pdfbox/pdmodel/common/COSObjectable;


# instance fields
.field private thread:Lorg/apache/pdfbox/cos/COSDictionary;


# direct methods
.method public constructor <init>()V
    .locals 3

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-direct {v0}, Lorg/apache/pdfbox/cos/COSDictionary;-><init>()V

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDThread;->thread:Lorg/apache/pdfbox/cos/COSDictionary;

    const-string v1, "Type"

    const-string v2, "Thread"

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->setName(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Lorg/apache/pdfbox/cos/COSDictionary;)V
    .locals 0

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDThread;->thread:Lorg/apache/pdfbox/cos/COSDictionary;

    return-void
.end method


# virtual methods
.method public getCOSObject()Lorg/apache/pdfbox/cos/COSBase;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDThread;->thread:Lorg/apache/pdfbox/cos/COSDictionary;

    return-object v0
.end method

.method public getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDThread;->thread:Lorg/apache/pdfbox/cos/COSDictionary;

    return-object v0
.end method

.method public getFirstBead()Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDThreadBead;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDThread;->thread:Lorg/apache/pdfbox/cos/COSDictionary;

    const-string v1, "F"

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Ljava/lang/String;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSDictionary;

    if-eqz v0, :cond_0

    new-instance v1, Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDThreadBead;

    invoke-direct {v1, v0}, Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDThreadBead;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return-object v1
.end method

.method public getThreadInfo()Lorg/apache/pdfbox/pdmodel/PDDocumentInformation;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDThread;->thread:Lorg/apache/pdfbox/cos/COSDictionary;

    const-string v1, "I"

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Ljava/lang/String;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSDictionary;

    if-eqz v0, :cond_0

    new-instance v1, Lorg/apache/pdfbox/pdmodel/PDDocumentInformation;

    invoke-direct {v1, v0}, Lorg/apache/pdfbox/pdmodel/PDDocumentInformation;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return-object v1
.end method

.method public setFirstBead(Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDThreadBead;)V
    .locals 2

    if-eqz p1, :cond_0

    invoke-virtual {p1, p0}, Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDThreadBead;->setThread(Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDThread;)V

    :cond_0
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDThread;->thread:Lorg/apache/pdfbox/cos/COSDictionary;

    const-string v1, "F"

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Ljava/lang/String;Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)V

    return-void
.end method

.method public setThreadInfo(Lorg/apache/pdfbox/pdmodel/PDDocumentInformation;)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDThread;->thread:Lorg/apache/pdfbox/cos/COSDictionary;

    const-string v1, "I"

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Ljava/lang/String;Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)V

    return-void
.end method

.class public Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDBorderEffectDictionary;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/pdfbox/pdmodel/common/COSObjectable;


# static fields
.field public static final STYLE_CLOUDY:Ljava/lang/String; = "C"

.field public static final STYLE_SOLID:Ljava/lang/String; = "S"


# instance fields
.field private dictionary:Lorg/apache/pdfbox/cos/COSDictionary;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-direct {v0}, Lorg/apache/pdfbox/cos/COSDictionary;-><init>()V

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDBorderEffectDictionary;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    return-void
.end method

.method public constructor <init>(Lorg/apache/pdfbox/cos/COSDictionary;)V
    .locals 0

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDBorderEffectDictionary;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    return-void
.end method


# virtual methods
.method public getCOSObject()Lorg/apache/pdfbox/cos/COSBase;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDBorderEffectDictionary;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    return-object v0
.end method

.method public getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDBorderEffectDictionary;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    return-object v0
.end method

.method public getIntensity()F
    .locals 3

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDBorderEffectDictionary;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    const-string v1, "I"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->getFloat(Ljava/lang/String;F)F

    move-result v0

    return v0
.end method

.method public getStyle()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDBorderEffectDictionary;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    const-string v1, "S"

    invoke-virtual {v0, v1, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getNameAsString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public setIntensity(F)V
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDBorderEffectDictionary;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    const-string v1, "I"

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setFloat(Ljava/lang/String;F)V

    return-void
.end method

.method public setStyle(Ljava/lang/String;)V
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDBorderEffectDictionary;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    const-string v1, "S"

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setName(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

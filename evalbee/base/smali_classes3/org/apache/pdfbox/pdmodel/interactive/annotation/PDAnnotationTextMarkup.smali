.class public Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotationTextMarkup;
.super Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotationMarkup;
.source "SourceFile"


# static fields
.field public static final SUB_TYPE_HIGHLIGHT:Ljava/lang/String; = "Highlight"

.field public static final SUB_TYPE_SQUIGGLY:Ljava/lang/String; = "Squiggly"

.field public static final SUB_TYPE_STRIKEOUT:Ljava/lang/String; = "StrikeOut"

.field public static final SUB_TYPE_UNDERLINE:Ljava/lang/String; = "Underline"


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotationMarkup;-><init>()V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotationMarkup;-><init>()V

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotationTextMarkup;->setSubtype(Ljava/lang/String;)V

    const/4 p1, 0x0

    new-array p1, p1, [F

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotationTextMarkup;->setQuadPoints([F)V

    return-void
.end method

.method public constructor <init>(Lorg/apache/pdfbox/cos/COSDictionary;)V
    .locals 0

    .line 2
    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotationMarkup;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    return-void
.end method


# virtual methods
.method public getQuadPoints()[F
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotation;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    const-string v1, "QuadPoints"

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Ljava/lang/String;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSArray;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSArray;->toFloatArray()[F

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public getSubtype()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotation;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->SUBTYPE:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getNameAsString(Lorg/apache/pdfbox/cos/COSName;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public setQuadPoints([F)V
    .locals 2

    new-instance v0, Lorg/apache/pdfbox/cos/COSArray;

    invoke-direct {v0}, Lorg/apache/pdfbox/cos/COSArray;-><init>()V

    invoke-virtual {v0, p1}, Lorg/apache/pdfbox/cos/COSArray;->setFloatArray([F)V

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotation;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object p1

    const-string v1, "QuadPoints"

    invoke-virtual {p1, v1, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Ljava/lang/String;Lorg/apache/pdfbox/cos/COSBase;)V

    return-void
.end method

.method public setSubtype(Ljava/lang/String;)V
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotation;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->SUBTYPE:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setName(Lorg/apache/pdfbox/cos/COSName;Ljava/lang/String;)V

    return-void
.end method

.class public final Lorg/apache/pdfbox/pdmodel/interactive/form/AppearanceGenerator;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static generateFieldAppearances(Lorg/apache/pdfbox/pdmodel/interactive/form/PDField;)V
    .locals 4

    instance-of v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/PDVariableText;

    const-string v1, "PdfBoxAndroid"

    if-eqz v0, :cond_2

    new-instance v0, Lorg/apache/pdfbox/pdmodel/interactive/form/AppearanceGeneratorHelper;

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->getAcroForm()Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;

    move-result-object v2

    move-object v3, p0

    check-cast v3, Lorg/apache/pdfbox/pdmodel/interactive/form/PDVariableText;

    invoke-direct {v0, v2, v3}, Lorg/apache/pdfbox/pdmodel/interactive/form/AppearanceGeneratorHelper;-><init>(Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;Lorg/apache/pdfbox/pdmodel/interactive/form/PDVariableText;)V

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->getValue()Ljava/lang/Object;

    move-result-object p0

    instance-of v2, p0, Ljava/lang/String;

    if-eqz v2, :cond_0

    check-cast p0, Ljava/lang/String;

    :goto_0
    invoke-virtual {v0, p0}, Lorg/apache/pdfbox/pdmodel/interactive/form/AppearanceGeneratorHelper;->setAppearanceValue(Ljava/lang/String;)V

    goto :goto_2

    :cond_0
    instance-of v2, p0, Lorg/apache/pdfbox/pdmodel/common/PDTextStream;

    if-eqz v2, :cond_1

    check-cast p0, Lorg/apache/pdfbox/pdmodel/common/PDTextStream;

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/common/PDTextStream;->getAsString()Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    :cond_1
    if-eqz p0, :cond_3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Can\'t generate the appearance for values typed "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p0, "."

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_1

    :cond_2
    const-string p0, "Unable to generate the field appearance."

    :goto_1
    invoke-static {v1, p0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    :goto_2
    return-void
.end method

.class public final Lorg/apache/pdfbox/pdmodel/interactive/form/FieldUtils;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/pdfbox/pdmodel/interactive/form/FieldUtils$KeyValueValueComparator;,
        Lorg/apache/pdfbox/pdmodel/interactive/form/FieldUtils$KeyValueKeyComparator;,
        Lorg/apache/pdfbox/pdmodel/interactive/form/FieldUtils$KeyValue;
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static getItemsFromPair(Lorg/apache/pdfbox/cos/COSBase;I)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/pdfbox/cos/COSBase;",
            "I)",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    check-cast p0, Lorg/apache/pdfbox/cos/COSArray;

    invoke-virtual {p0}, Lorg/apache/pdfbox/cos/COSArray;->size()I

    move-result v1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    invoke-virtual {p0, v2}, Lorg/apache/pdfbox/cos/COSArray;->get(I)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v3

    check-cast v3, Lorg/apache/pdfbox/cos/COSArray;

    invoke-virtual {v3, p1}, Lorg/apache/pdfbox/cos/COSArray;->get(I)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v3

    check-cast v3, Lorg/apache/pdfbox/cos/COSString;

    invoke-virtual {v3}, Lorg/apache/pdfbox/cos/COSString;->getString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method public static getPairableItems(Lorg/apache/pdfbox/cos/COSBase;I)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/pdfbox/cos/COSBase;",
            "I)",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    if-ltz p1, :cond_3

    const/4 v0, 0x1

    if-gt p1, v0, :cond_3

    instance-of v0, p0, Lorg/apache/pdfbox/cos/COSString;

    if-eqz v0, :cond_0

    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    check-cast p0, Lorg/apache/pdfbox/cos/COSString;

    invoke-virtual {p0}, Lorg/apache/pdfbox/cos/COSString;->getString()Ljava/lang/String;

    move-result-object p0

    invoke-interface {p1, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p1

    :cond_0
    instance-of v0, p0, Lorg/apache/pdfbox/cos/COSArray;

    if-eqz v0, :cond_2

    move-object v0, p0

    check-cast v0, Lorg/apache/pdfbox/cos/COSArray;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSArray;->get(I)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v1

    instance-of v1, v1, Lorg/apache/pdfbox/cos/COSString;

    if-eqz v1, :cond_1

    invoke-static {v0}, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;->convertCOSStringCOSArrayToList(Lorg/apache/pdfbox/cos/COSArray;)Ljava/util/List;

    move-result-object p0

    return-object p0

    :cond_1
    invoke-static {p0, p1}, Lorg/apache/pdfbox/pdmodel/interactive/form/FieldUtils;->getItemsFromPair(Lorg/apache/pdfbox/cos/COSBase;I)Ljava/util/List;

    move-result-object p0

    return-object p0

    :cond_2
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object p0

    return-object p0

    :cond_3
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string p1, "Only 0 and 1 are allowed as an index into two-element arrays"

    invoke-direct {p0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method public static sortByKey(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lorg/apache/pdfbox/pdmodel/interactive/form/FieldUtils$KeyValue;",
            ">;)V"
        }
    .end annotation

    new-instance v0, Lorg/apache/pdfbox/pdmodel/interactive/form/FieldUtils$KeyValueKeyComparator;

    invoke-direct {v0}, Lorg/apache/pdfbox/pdmodel/interactive/form/FieldUtils$KeyValueKeyComparator;-><init>()V

    invoke-static {p0, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    return-void
.end method

.method public static sortByValue(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lorg/apache/pdfbox/pdmodel/interactive/form/FieldUtils$KeyValue;",
            ">;)V"
        }
    .end annotation

    new-instance v0, Lorg/apache/pdfbox/pdmodel/interactive/form/FieldUtils$KeyValueValueComparator;

    invoke-direct {v0}, Lorg/apache/pdfbox/pdmodel/interactive/form/FieldUtils$KeyValueValueComparator;-><init>()V

    invoke-static {p0, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    return-void
.end method

.method public static toKeyValueList(Ljava/util/List;Ljava/util/List;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/util/List<",
            "Lorg/apache/pdfbox/pdmodel/interactive/form/FieldUtils$KeyValue;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    const/4 v1, 0x0

    :goto_0
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v2

    if-ge v1, v2, :cond_0

    new-instance v2, Lorg/apache/pdfbox/pdmodel/interactive/form/FieldUtils$KeyValue;

    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-direct {v2, v3, v4}, Lorg/apache/pdfbox/pdmodel/interactive/form/FieldUtils$KeyValue;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-object v0
.end method

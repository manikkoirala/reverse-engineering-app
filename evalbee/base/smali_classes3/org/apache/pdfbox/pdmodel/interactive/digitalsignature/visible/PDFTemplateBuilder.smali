.class public interface abstract Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDFTemplateBuilder;
.super Ljava/lang/Object;
.source "SourceFile"


# virtual methods
.method public abstract closeTemplate(Lorg/apache/pdfbox/pdmodel/PDDocument;)V
.end method

.method public abstract createAcroForm(Lorg/apache/pdfbox/pdmodel/PDDocument;)V
.end method

.method public abstract createAcroFormDictionary(Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;Lorg/apache/pdfbox/pdmodel/interactive/form/PDSignatureField;)V
.end method

.method public abstract createAffineTransform([B)V
.end method

.method public abstract createAppearanceDictionary(Lorg/apache/pdfbox/pdmodel/graphics/form/PDFormXObject;Lorg/apache/pdfbox/pdmodel/interactive/form/PDSignatureField;)V
.end method

.method public abstract createFormaterRectangle([B)V
.end method

.method public abstract createHolderForm(Lorg/apache/pdfbox/pdmodel/PDResources;Lorg/apache/pdfbox/pdmodel/common/PDStream;Lorg/apache/pdfbox/pdmodel/common/PDRectangle;)V
.end method

.method public abstract createHolderFormResources()V
.end method

.method public abstract createHolderFormStream(Lorg/apache/pdfbox/pdmodel/PDDocument;)V
.end method

.method public abstract createImageForm(Lorg/apache/pdfbox/pdmodel/PDResources;Lorg/apache/pdfbox/pdmodel/PDResources;Lorg/apache/pdfbox/pdmodel/common/PDStream;Lorg/apache/pdfbox/pdmodel/common/PDRectangle;Lorg/apache/pdfbox/util/awt/AffineTransform;Lorg/apache/pdfbox/pdmodel/graphics/image/PDImageXObject;)V
.end method

.method public abstract createImageFormResources()V
.end method

.method public abstract createImageFormStream(Lorg/apache/pdfbox/pdmodel/PDDocument;)V
.end method

.method public abstract createInnerForm(Lorg/apache/pdfbox/pdmodel/PDResources;Lorg/apache/pdfbox/pdmodel/common/PDStream;Lorg/apache/pdfbox/pdmodel/common/PDRectangle;)V
.end method

.method public abstract createInnerFormResource()V
.end method

.method public abstract createInnerFormStream(Lorg/apache/pdfbox/pdmodel/PDDocument;)V
.end method

.method public abstract createPage(Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDVisibleSignDesigner;)V
.end method

.method public abstract createProcSetArray()V
.end method

.method public abstract createSignature(Lorg/apache/pdfbox/pdmodel/interactive/form/PDSignatureField;Lorg/apache/pdfbox/pdmodel/PDPage;Ljava/lang/String;)V
.end method

.method public abstract createSignatureField(Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;)V
.end method

.method public abstract createSignatureRectangle(Lorg/apache/pdfbox/pdmodel/interactive/form/PDSignatureField;Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDVisibleSignDesigner;)V
.end method

.method public abstract createTemplate(Lorg/apache/pdfbox/pdmodel/PDPage;)V
.end method

.method public abstract createVisualSignature(Lorg/apache/pdfbox/pdmodel/PDDocument;)V
.end method

.method public abstract createWidgetDictionary(Lorg/apache/pdfbox/pdmodel/interactive/form/PDSignatureField;Lorg/apache/pdfbox/pdmodel/PDResources;)V
.end method

.method public abstract getStructure()Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDFTemplateStructure;
.end method

.method public abstract injectAppearanceStreams(Lorg/apache/pdfbox/pdmodel/common/PDStream;Lorg/apache/pdfbox/pdmodel/common/PDStream;Lorg/apache/pdfbox/pdmodel/common/PDStream;Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDVisibleSignDesigner;)V
.end method

.method public abstract injectProcSetArray(Lorg/apache/pdfbox/pdmodel/graphics/form/PDFormXObject;Lorg/apache/pdfbox/pdmodel/PDPage;Lorg/apache/pdfbox/pdmodel/PDResources;Lorg/apache/pdfbox/pdmodel/PDResources;Lorg/apache/pdfbox/pdmodel/PDResources;Lorg/apache/pdfbox/cos/COSArray;)V
.end method

.method public abstract insertInnerFormToHolerResources(Lorg/apache/pdfbox/pdmodel/graphics/form/PDFormXObject;Lorg/apache/pdfbox/pdmodel/PDResources;)V
.end method

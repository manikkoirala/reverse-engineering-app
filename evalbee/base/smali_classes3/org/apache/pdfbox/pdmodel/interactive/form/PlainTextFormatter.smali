.class Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter$Builder;,
        Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter$TextAlign;
    }
.end annotation


# instance fields
.field private appearanceStyle:Lorg/apache/pdfbox/pdmodel/interactive/form/AppearanceStyle;

.field private final composer:Lorg/apache/pdfbox/pdmodel/interactive/form/AppearancePrimitivesComposer;

.field private horizontalOffset:F

.field private final textAlignment:Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter$TextAlign;

.field private final textContent:Lorg/apache/pdfbox/pdmodel/interactive/form/PlainText;

.field private verticalOffset:F

.field private final width:F

.field private final wrapLines:Z


# direct methods
.method private constructor <init>(Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter$Builder;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter$Builder;->access$100(Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter$Builder;)Lorg/apache/pdfbox/pdmodel/interactive/form/AppearanceStyle;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter;->appearanceStyle:Lorg/apache/pdfbox/pdmodel/interactive/form/AppearanceStyle;

    invoke-static {p1}, Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter$Builder;->access$200(Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter$Builder;)Z

    move-result v0

    iput-boolean v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter;->wrapLines:Z

    invoke-static {p1}, Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter$Builder;->access$300(Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter$Builder;)F

    move-result v0

    iput v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter;->width:F

    invoke-static {p1}, Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter$Builder;->access$400(Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter$Builder;)Lorg/apache/pdfbox/pdmodel/interactive/form/AppearancePrimitivesComposer;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter;->composer:Lorg/apache/pdfbox/pdmodel/interactive/form/AppearancePrimitivesComposer;

    invoke-static {p1}, Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter$Builder;->access$500(Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter$Builder;)Lorg/apache/pdfbox/pdmodel/interactive/form/PlainText;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter;->textContent:Lorg/apache/pdfbox/pdmodel/interactive/form/PlainText;

    invoke-static {p1}, Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter$Builder;->access$600(Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter$Builder;)Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter$TextAlign;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter;->textAlignment:Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter$TextAlign;

    invoke-static {p1}, Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter$Builder;->access$700(Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter$Builder;)F

    move-result v0

    iput v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter;->horizontalOffset:F

    invoke-static {p1}, Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter$Builder;->access$800(Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter$Builder;)F

    move-result p1

    iput p1, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter;->verticalOffset:F

    return-void
.end method

.method public synthetic constructor <init>(Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter$Builder;Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter$1;)V
    .locals 0

    .line 2
    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter;-><init>(Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter$Builder;)V

    return-void
.end method

.method private processLines(Ljava/util/List;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lorg/apache/pdfbox/pdmodel/interactive/form/PlainText$Line;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter;->appearanceStyle:Lorg/apache/pdfbox/pdmodel/interactive/form/AppearanceStyle;

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/interactive/form/AppearanceStyle;->getFont()Lorg/apache/pdfbox/pdmodel/font/PDFont;

    move-result-object v0

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    const/4 v2, 0x0

    move v3, v2

    move v4, v3

    move v5, v4

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lorg/apache/pdfbox/pdmodel/interactive/form/PlainText$Line;

    sget-object v7, Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter$1;->$SwitchMap$org$apache$pdfbox$pdmodel$interactive$form$PlainTextFormatter$TextAlign:[I

    iget-object v8, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter;->textAlignment:Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter$TextAlign;

    invoke-virtual {v8}, Ljava/lang/Enum;->ordinal()I

    move-result v8

    aget v7, v7, v8

    const/4 v8, 0x1

    if-eq v7, v8, :cond_2

    const/4 v9, 0x2

    if-eq v7, v9, :cond_1

    const/4 v9, 0x3

    if-eq v7, v9, :cond_0

    move v4, v2

    goto :goto_1

    :cond_0
    invoke-interface {p1, v6}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v7

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v9

    sub-int/2addr v9, v8

    if-eq v7, v9, :cond_3

    iget v5, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter;->width:F

    invoke-virtual {v6, v5}, Lorg/apache/pdfbox/pdmodel/interactive/form/PlainText$Line;->getInterWordSpacing(F)F

    move-result v5

    goto :goto_1

    :cond_1
    iget v4, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter;->width:F

    invoke-virtual {v6}, Lorg/apache/pdfbox/pdmodel/interactive/form/PlainText$Line;->getWidth()F

    move-result v7

    sub-float/2addr v4, v7

    goto :goto_1

    :cond_2
    iget v4, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter;->width:F

    invoke-virtual {v6}, Lorg/apache/pdfbox/pdmodel/interactive/form/PlainText$Line;->getWidth()F

    move-result v7

    sub-float/2addr v4, v7

    const/high16 v7, 0x40000000    # 2.0f

    div-float/2addr v4, v7

    :cond_3
    :goto_1
    neg-float v3, v3

    add-float/2addr v3, v4

    iget v7, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter;->horizontalOffset:F

    add-float/2addr v3, v7

    invoke-interface {p1, v6}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v7

    if-nez v7, :cond_4

    iget-object v7, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter;->composer:Lorg/apache/pdfbox/pdmodel/interactive/form/AppearancePrimitivesComposer;

    iget v9, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter;->verticalOffset:F

    invoke-virtual {v7, v3, v9}, Lorg/apache/pdfbox/pdmodel/interactive/form/AppearancePrimitivesComposer;->newLineAtOffset(FF)V

    iput v2, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter;->verticalOffset:F

    iput v2, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter;->horizontalOffset:F

    goto :goto_2

    :cond_4
    iget v7, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter;->verticalOffset:F

    iget-object v9, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter;->appearanceStyle:Lorg/apache/pdfbox/pdmodel/interactive/form/AppearanceStyle;

    invoke-virtual {v9}, Lorg/apache/pdfbox/pdmodel/interactive/form/AppearanceStyle;->getLeading()F

    move-result v9

    sub-float/2addr v7, v9

    iput v7, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter;->verticalOffset:F

    iget-object v7, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter;->composer:Lorg/apache/pdfbox/pdmodel/interactive/form/AppearancePrimitivesComposer;

    iget-object v9, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter;->appearanceStyle:Lorg/apache/pdfbox/pdmodel/interactive/form/AppearanceStyle;

    invoke-virtual {v9}, Lorg/apache/pdfbox/pdmodel/interactive/form/AppearanceStyle;->getLeading()F

    move-result v9

    neg-float v9, v9

    invoke-virtual {v7, v3, v9}, Lorg/apache/pdfbox/pdmodel/interactive/form/AppearancePrimitivesComposer;->newLineAtOffset(FF)V

    :goto_2
    invoke-virtual {v6}, Lorg/apache/pdfbox/pdmodel/interactive/form/PlainText$Line;->getWords()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v7, v4

    :cond_5
    :goto_3
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_6

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lorg/apache/pdfbox/pdmodel/interactive/form/PlainText$Word;

    iget-object v10, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter;->composer:Lorg/apache/pdfbox/pdmodel/interactive/form/AppearancePrimitivesComposer;

    invoke-virtual {v9}, Lorg/apache/pdfbox/pdmodel/interactive/form/PlainText$Word;->getText()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11, v0}, Lorg/apache/pdfbox/pdmodel/interactive/form/AppearancePrimitivesComposer;->showText(Ljava/lang/String;Lorg/apache/pdfbox/pdmodel/font/PDFont;)V

    invoke-virtual {v9}, Lorg/apache/pdfbox/pdmodel/interactive/form/PlainText$Word;->getAttributes()Ljava/text/AttributedString;

    move-result-object v10

    invoke-virtual {v10}, Ljava/text/AttributedString;->getIterator()Ljava/text/AttributedCharacterIterator;

    move-result-object v10

    sget-object v11, Lorg/apache/pdfbox/pdmodel/interactive/form/PlainText$TextAttribute;->WIDTH:Ljava/text/AttributedCharacterIterator$Attribute;

    invoke-interface {v10, v11}, Ljava/text/AttributedCharacterIterator;->getAttribute(Ljava/text/AttributedCharacterIterator$Attribute;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Float;

    invoke-virtual {v10}, Ljava/lang/Float;->floatValue()F

    move-result v10

    invoke-interface {v3, v9}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v9

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v11

    sub-int/2addr v11, v8

    if-eq v9, v11, :cond_5

    iget-object v9, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter;->composer:Lorg/apache/pdfbox/pdmodel/interactive/form/AppearancePrimitivesComposer;

    add-float v11, v10, v5

    invoke-virtual {v9, v11, v2}, Lorg/apache/pdfbox/pdmodel/interactive/form/AppearancePrimitivesComposer;->newLineAtOffset(FF)V

    add-float/2addr v7, v10

    add-float/2addr v7, v5

    goto :goto_3

    :cond_6
    move v3, v7

    goto/16 :goto_0

    :cond_7
    iget p1, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter;->horizontalOffset:F

    sub-float/2addr p1, v3

    iput p1, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter;->horizontalOffset:F

    return-void
.end method


# virtual methods
.method public format()V
    .locals 5

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter;->textContent:Lorg/apache/pdfbox/pdmodel/interactive/form/PlainText;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/interactive/form/PlainText;->getParagraphs()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter;->textContent:Lorg/apache/pdfbox/pdmodel/interactive/form/PlainText;

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/interactive/form/PlainText;->getParagraphs()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/pdfbox/pdmodel/interactive/form/PlainText$Paragraph;

    iget-boolean v2, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter;->wrapLines:Z

    if-eqz v2, :cond_0

    iget-object v2, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter;->appearanceStyle:Lorg/apache/pdfbox/pdmodel/interactive/form/AppearanceStyle;

    invoke-virtual {v2}, Lorg/apache/pdfbox/pdmodel/interactive/form/AppearanceStyle;->getFont()Lorg/apache/pdfbox/pdmodel/font/PDFont;

    move-result-object v2

    iget-object v3, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter;->appearanceStyle:Lorg/apache/pdfbox/pdmodel/interactive/form/AppearanceStyle;

    invoke-virtual {v3}, Lorg/apache/pdfbox/pdmodel/interactive/form/AppearanceStyle;->getFontSize()F

    move-result v3

    iget v4, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter;->width:F

    invoke-virtual {v1, v2, v3, v4}, Lorg/apache/pdfbox/pdmodel/interactive/form/PlainText$Paragraph;->getLines(Lorg/apache/pdfbox/pdmodel/font/PDFont;FF)Ljava/util/List;

    move-result-object v1

    invoke-direct {p0, v1}, Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter;->processLines(Ljava/util/List;)V

    goto :goto_0

    :cond_0
    iget-object v2, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter;->composer:Lorg/apache/pdfbox/pdmodel/interactive/form/AppearancePrimitivesComposer;

    invoke-virtual {v1}, Lorg/apache/pdfbox/pdmodel/interactive/form/PlainText$Paragraph;->getText()Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lorg/apache/pdfbox/pdmodel/interactive/form/PlainTextFormatter;->appearanceStyle:Lorg/apache/pdfbox/pdmodel/interactive/form/AppearanceStyle;

    invoke-virtual {v3}, Lorg/apache/pdfbox/pdmodel/interactive/form/AppearanceStyle;->getFont()Lorg/apache/pdfbox/pdmodel/font/PDFont;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Lorg/apache/pdfbox/pdmodel/interactive/form/AppearancePrimitivesComposer;->showText(Ljava/lang/String;Lorg/apache/pdfbox/pdmodel/font/PDFont;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.class public Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDVisibleSignDesigner;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private AffineTransformParams:[B

.field private formaterRectangleParams:[B

.field private imageHeight:Ljava/lang/Float;

.field private imageSizeInPercents:F

.field private imageWidth:Ljava/lang/Float;

.field private pageHeight:F

.field private pageWidth:F

.field private signatureFieldName:Ljava/lang/String;

.field private xAxis:F

.field private yAxis:F


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "sig"

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDVisibleSignDesigner;->signatureFieldName:Ljava/lang/String;

    const/4 v0, 0x4

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDVisibleSignDesigner;->formaterRectangleParams:[B

    const/4 v0, 0x6

    new-array v0, v0, [B

    fill-array-data v0, :array_1

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDVisibleSignDesigner;->AffineTransformParams:[B

    return-void

    :array_0
    .array-data 1
        0x0t
        0x0t
        0x64t
        0x32t
    .end array-data

    :array_1
    .array-data 1
        0x1t
        0x0t
        0x0t
        0x1t
        0x0t
        0x0t
    .end array-data
.end method

.method private calculatePageSize(Lorg/apache/pdfbox/pdmodel/PDDocument;I)V
    .locals 2

    const/4 v0, 0x1

    if-lt p2, v0, :cond_0

    sub-int/2addr p2, v0

    invoke-virtual {p1, p2}, Lorg/apache/pdfbox/pdmodel/PDDocument;->getPage(I)Lorg/apache/pdfbox/pdmodel/PDPage;

    move-result-object p1

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/PDPage;->getMediaBox()Lorg/apache/pdfbox/pdmodel/common/PDRectangle;

    move-result-object p1

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->getHeight()F

    move-result p2

    invoke-direct {p0, p2}, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDVisibleSignDesigner;->pageHeight(F)Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDVisibleSignDesigner;

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->getWidth()F

    move-result p1

    const/4 p2, 0x0

    add-float v0, p1, p2

    iput v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDVisibleSignDesigner;->pageWidth:F

    add-float/2addr p1, p2

    div-float/2addr p2, p1

    const/high16 p1, 0x42c80000    # 100.0f

    sub-float/2addr p1, p2

    iput p1, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDVisibleSignDesigner;->imageSizeInPercents:F

    return-void

    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "First page of pdf is 1, not "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private pageHeight(F)Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDVisibleSignDesigner;
    .locals 0

    iput p1, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDVisibleSignDesigner;->pageHeight:F

    return-object p0
.end method


# virtual methods
.method public affineTransformParams([B)Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDVisibleSignDesigner;
    .locals 0

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDVisibleSignDesigner;->AffineTransformParams:[B

    return-object p0
.end method

.method public coordinates(FF)Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDVisibleSignDesigner;
    .locals 0

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDVisibleSignDesigner;->xAxis(F)Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDVisibleSignDesigner;

    invoke-virtual {p0, p2}, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDVisibleSignDesigner;->yAxis(F)Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDVisibleSignDesigner;

    return-object p0
.end method

.method public formaterRectangleParams([B)Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDVisibleSignDesigner;
    .locals 0

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDVisibleSignDesigner;->formaterRectangleParams:[B

    return-object p0
.end method

.method public getAffineTransformParams()[B
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDVisibleSignDesigner;->AffineTransformParams:[B

    return-object v0
.end method

.method public getFormaterRectangleParams()[B
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDVisibleSignDesigner;->formaterRectangleParams:[B

    return-object v0
.end method

.method public getHeight()F
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDVisibleSignDesigner;->imageHeight:Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    return v0
.end method

.method public getImageSizeInPercents()F
    .locals 1

    iget v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDVisibleSignDesigner;->imageSizeInPercents:F

    return v0
.end method

.method public getPageHeight()F
    .locals 1

    iget v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDVisibleSignDesigner;->pageHeight:F

    return v0
.end method

.method public getPageWidth()F
    .locals 1

    iget v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDVisibleSignDesigner;->pageWidth:F

    return v0
.end method

.method public getSignatureFieldName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDVisibleSignDesigner;->signatureFieldName:Ljava/lang/String;

    return-object v0
.end method

.method public getSignatureText()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "That method is not yet implemented"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getTemplateHeight()F
    .locals 1

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDVisibleSignDesigner;->getPageHeight()F

    move-result v0

    return v0
.end method

.method public getWidth()F
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDVisibleSignDesigner;->imageWidth:Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    return v0
.end method

.method public getxAxis()F
    .locals 1

    iget v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDVisibleSignDesigner;->xAxis:F

    return v0
.end method

.method public getyAxis()F
    .locals 1

    iget v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDVisibleSignDesigner;->yAxis:F

    return v0
.end method

.method public height(F)Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDVisibleSignDesigner;
    .locals 0

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object p1

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDVisibleSignDesigner;->imageHeight:Ljava/lang/Float;

    return-object p0
.end method

.method public imageSizeInPercents(F)V
    .locals 0

    iput p1, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDVisibleSignDesigner;->imageSizeInPercents:F

    return-void
.end method

.method public pageWidth(F)Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDVisibleSignDesigner;
    .locals 0

    iput p1, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDVisibleSignDesigner;->pageWidth:F

    return-object p0
.end method

.method public signatureFieldName(Ljava/lang/String;)Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDVisibleSignDesigner;
    .locals 0

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDVisibleSignDesigner;->signatureFieldName:Ljava/lang/String;

    return-object p0
.end method

.method public signatureText(Ljava/lang/String;)Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDVisibleSignDesigner;
    .locals 1

    new-instance p1, Ljava/lang/UnsupportedOperationException;

    const-string v0, "That method is not yet implemented"

    invoke-direct {p1, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public width(F)Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDVisibleSignDesigner;
    .locals 0

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object p1

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDVisibleSignDesigner;->imageWidth:Ljava/lang/Float;

    return-object p0
.end method

.method public xAxis(F)Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDVisibleSignDesigner;
    .locals 0

    iput p1, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDVisibleSignDesigner;->xAxis:F

    return-object p0
.end method

.method public yAxis(F)Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDVisibleSignDesigner;
    .locals 0

    iput p1, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDVisibleSignDesigner;->yAxis:F

    return-object p0
.end method

.method public zoom(F)Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDVisibleSignDesigner;
    .locals 3

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDVisibleSignDesigner;->imageHeight:Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    iget-object v1, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDVisibleSignDesigner;->imageHeight:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    mul-float/2addr v1, p1

    const/high16 v2, 0x42c80000    # 100.0f

    div-float/2addr v1, v2

    add-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDVisibleSignDesigner;->imageHeight:Ljava/lang/Float;

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDVisibleSignDesigner;->imageWidth:Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    iget-object v1, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDVisibleSignDesigner;->imageWidth:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    mul-float/2addr v1, p1

    div-float/2addr v1, v2

    add-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object p1

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDVisibleSignDesigner;->imageWidth:Ljava/lang/Float;

    return-object p0
.end method

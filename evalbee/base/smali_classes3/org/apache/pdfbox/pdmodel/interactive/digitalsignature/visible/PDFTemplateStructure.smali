.class public Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDFTemplateStructure;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private acroForm:Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;

.field private acroFormDictionary:Lorg/apache/pdfbox/cos/COSDictionary;

.field private acroFormFields:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;",
            ">;"
        }
    .end annotation
.end field

.field private affineTransform:Lorg/apache/pdfbox/util/awt/AffineTransform;

.field private appearanceDictionary:Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAppearanceDictionary;

.field private formaterRectangle:Lorg/apache/pdfbox/pdmodel/common/PDRectangle;

.field private holderForm:Lorg/apache/pdfbox/pdmodel/graphics/form/PDFormXObject;

.field private holderFormResources:Lorg/apache/pdfbox/pdmodel/PDResources;

.field private holderFormStream:Lorg/apache/pdfbox/pdmodel/common/PDStream;

.field private image:Lorg/apache/pdfbox/pdmodel/graphics/image/PDImageXObject;

.field private imageForm:Lorg/apache/pdfbox/pdmodel/graphics/form/PDFormXObject;

.field private imageFormName:Lorg/apache/pdfbox/cos/COSName;

.field private imageFormResources:Lorg/apache/pdfbox/pdmodel/PDResources;

.field private imageFormStream:Lorg/apache/pdfbox/pdmodel/common/PDStream;

.field private imageName:Lorg/apache/pdfbox/cos/COSName;

.field private innerForm:Lorg/apache/pdfbox/pdmodel/graphics/form/PDFormXObject;

.field private innerFormName:Lorg/apache/pdfbox/cos/COSName;

.field private innerFormResources:Lorg/apache/pdfbox/pdmodel/PDResources;

.field private innterFormStream:Lorg/apache/pdfbox/pdmodel/common/PDStream;

.field private page:Lorg/apache/pdfbox/pdmodel/PDPage;

.field private pdSignature:Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/PDSignature;

.field private procSet:Lorg/apache/pdfbox/cos/COSArray;

.field private signatureField:Lorg/apache/pdfbox/pdmodel/interactive/form/PDSignatureField;

.field private singatureRectangle:Lorg/apache/pdfbox/pdmodel/common/PDRectangle;

.field private template:Lorg/apache/pdfbox/pdmodel/PDDocument;

.field private visualSignature:Lorg/apache/pdfbox/cos/COSDocument;

.field private widgetDictionary:Lorg/apache/pdfbox/cos/COSDictionary;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getAcroForm()Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDFTemplateStructure;->acroForm:Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;

    return-object v0
.end method

.method public getAcroFormDictionary()Lorg/apache/pdfbox/cos/COSDictionary;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDFTemplateStructure;->acroFormDictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    return-object v0
.end method

.method public getAcroFormFields()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDFTemplateStructure;->acroFormFields:Ljava/util/List;

    return-object v0
.end method

.method public getAffineTransform()Lorg/apache/pdfbox/util/awt/AffineTransform;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDFTemplateStructure;->affineTransform:Lorg/apache/pdfbox/util/awt/AffineTransform;

    return-object v0
.end method

.method public getAppearanceDictionary()Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAppearanceDictionary;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDFTemplateStructure;->appearanceDictionary:Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAppearanceDictionary;

    return-object v0
.end method

.method public getFormaterRectangle()Lorg/apache/pdfbox/pdmodel/common/PDRectangle;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDFTemplateStructure;->formaterRectangle:Lorg/apache/pdfbox/pdmodel/common/PDRectangle;

    return-object v0
.end method

.method public getHolderForm()Lorg/apache/pdfbox/pdmodel/graphics/form/PDFormXObject;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDFTemplateStructure;->holderForm:Lorg/apache/pdfbox/pdmodel/graphics/form/PDFormXObject;

    return-object v0
.end method

.method public getHolderFormResources()Lorg/apache/pdfbox/pdmodel/PDResources;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDFTemplateStructure;->holderFormResources:Lorg/apache/pdfbox/pdmodel/PDResources;

    return-object v0
.end method

.method public getHolderFormStream()Lorg/apache/pdfbox/pdmodel/common/PDStream;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDFTemplateStructure;->holderFormStream:Lorg/apache/pdfbox/pdmodel/common/PDStream;

    return-object v0
.end method

.method public getImage()Lorg/apache/pdfbox/pdmodel/graphics/image/PDImageXObject;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDFTemplateStructure;->image:Lorg/apache/pdfbox/pdmodel/graphics/image/PDImageXObject;

    return-object v0
.end method

.method public getImageForm()Lorg/apache/pdfbox/pdmodel/graphics/form/PDFormXObject;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDFTemplateStructure;->imageForm:Lorg/apache/pdfbox/pdmodel/graphics/form/PDFormXObject;

    return-object v0
.end method

.method public getImageFormName()Lorg/apache/pdfbox/cos/COSName;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDFTemplateStructure;->imageFormName:Lorg/apache/pdfbox/cos/COSName;

    return-object v0
.end method

.method public getImageFormResources()Lorg/apache/pdfbox/pdmodel/PDResources;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDFTemplateStructure;->imageFormResources:Lorg/apache/pdfbox/pdmodel/PDResources;

    return-object v0
.end method

.method public getImageFormStream()Lorg/apache/pdfbox/pdmodel/common/PDStream;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDFTemplateStructure;->imageFormStream:Lorg/apache/pdfbox/pdmodel/common/PDStream;

    return-object v0
.end method

.method public getImageName()Lorg/apache/pdfbox/cos/COSName;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDFTemplateStructure;->imageName:Lorg/apache/pdfbox/cos/COSName;

    return-object v0
.end method

.method public getInnerForm()Lorg/apache/pdfbox/pdmodel/graphics/form/PDFormXObject;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDFTemplateStructure;->innerForm:Lorg/apache/pdfbox/pdmodel/graphics/form/PDFormXObject;

    return-object v0
.end method

.method public getInnerFormName()Lorg/apache/pdfbox/cos/COSName;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDFTemplateStructure;->innerFormName:Lorg/apache/pdfbox/cos/COSName;

    return-object v0
.end method

.method public getInnerFormResources()Lorg/apache/pdfbox/pdmodel/PDResources;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDFTemplateStructure;->innerFormResources:Lorg/apache/pdfbox/pdmodel/PDResources;

    return-object v0
.end method

.method public getInnterFormStream()Lorg/apache/pdfbox/pdmodel/common/PDStream;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDFTemplateStructure;->innterFormStream:Lorg/apache/pdfbox/pdmodel/common/PDStream;

    return-object v0
.end method

.method public getPage()Lorg/apache/pdfbox/pdmodel/PDPage;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDFTemplateStructure;->page:Lorg/apache/pdfbox/pdmodel/PDPage;

    return-object v0
.end method

.method public getPdSignature()Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/PDSignature;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDFTemplateStructure;->pdSignature:Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/PDSignature;

    return-object v0
.end method

.method public getProcSet()Lorg/apache/pdfbox/cos/COSArray;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDFTemplateStructure;->procSet:Lorg/apache/pdfbox/cos/COSArray;

    return-object v0
.end method

.method public getSignatureField()Lorg/apache/pdfbox/pdmodel/interactive/form/PDSignatureField;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDFTemplateStructure;->signatureField:Lorg/apache/pdfbox/pdmodel/interactive/form/PDSignatureField;

    return-object v0
.end method

.method public getSingatureRectangle()Lorg/apache/pdfbox/pdmodel/common/PDRectangle;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDFTemplateStructure;->singatureRectangle:Lorg/apache/pdfbox/pdmodel/common/PDRectangle;

    return-object v0
.end method

.method public getTemplate()Lorg/apache/pdfbox/pdmodel/PDDocument;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDFTemplateStructure;->template:Lorg/apache/pdfbox/pdmodel/PDDocument;

    return-object v0
.end method

.method public getTemplateAppearanceStream()Ljava/io/ByteArrayInputStream;
    .locals 3

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDFTemplateStructure;->getVisualSignature()Lorg/apache/pdfbox/cos/COSDocument;

    move-result-object v0

    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    new-instance v2, Lorg/apache/pdfbox/pdfwriter/COSWriter;

    invoke-direct {v2, v1}, Lorg/apache/pdfbox/pdfwriter/COSWriter;-><init>(Ljava/io/OutputStream;)V

    invoke-virtual {v2, v0}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->write(Lorg/apache/pdfbox/cos/COSDocument;)V

    new-instance v0, Ljava/io/ByteArrayInputStream;

    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDFTemplateStructure;->getTemplate()Lorg/apache/pdfbox/pdmodel/PDDocument;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/pdfbox/pdmodel/PDDocument;->close()V

    return-object v0
.end method

.method public getVisualSignature()Lorg/apache/pdfbox/cos/COSDocument;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDFTemplateStructure;->visualSignature:Lorg/apache/pdfbox/cos/COSDocument;

    return-object v0
.end method

.method public getWidgetDictionary()Lorg/apache/pdfbox/cos/COSDictionary;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDFTemplateStructure;->widgetDictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    return-object v0
.end method

.method public setAcroForm(Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;)V
    .locals 0

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDFTemplateStructure;->acroForm:Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;

    return-void
.end method

.method public setAcroFormDictionary(Lorg/apache/pdfbox/cos/COSDictionary;)V
    .locals 0

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDFTemplateStructure;->acroFormDictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    return-void
.end method

.method public setAcroFormFields(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDFTemplateStructure;->acroFormFields:Ljava/util/List;

    return-void
.end method

.method public setAffineTransform(Lorg/apache/pdfbox/util/awt/AffineTransform;)V
    .locals 0

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDFTemplateStructure;->affineTransform:Lorg/apache/pdfbox/util/awt/AffineTransform;

    return-void
.end method

.method public setAppearanceDictionary(Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAppearanceDictionary;)V
    .locals 0

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDFTemplateStructure;->appearanceDictionary:Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAppearanceDictionary;

    return-void
.end method

.method public setFormaterRectangle(Lorg/apache/pdfbox/pdmodel/common/PDRectangle;)V
    .locals 0

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDFTemplateStructure;->formaterRectangle:Lorg/apache/pdfbox/pdmodel/common/PDRectangle;

    return-void
.end method

.method public setHolderForm(Lorg/apache/pdfbox/pdmodel/graphics/form/PDFormXObject;)V
    .locals 0

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDFTemplateStructure;->holderForm:Lorg/apache/pdfbox/pdmodel/graphics/form/PDFormXObject;

    return-void
.end method

.method public setHolderFormResources(Lorg/apache/pdfbox/pdmodel/PDResources;)V
    .locals 0

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDFTemplateStructure;->holderFormResources:Lorg/apache/pdfbox/pdmodel/PDResources;

    return-void
.end method

.method public setHolderFormStream(Lorg/apache/pdfbox/pdmodel/common/PDStream;)V
    .locals 0

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDFTemplateStructure;->holderFormStream:Lorg/apache/pdfbox/pdmodel/common/PDStream;

    return-void
.end method

.method public setImage(Lorg/apache/pdfbox/pdmodel/graphics/image/PDImageXObject;)V
    .locals 0

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDFTemplateStructure;->image:Lorg/apache/pdfbox/pdmodel/graphics/image/PDImageXObject;

    return-void
.end method

.method public setImageForm(Lorg/apache/pdfbox/pdmodel/graphics/form/PDFormXObject;)V
    .locals 0

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDFTemplateStructure;->imageForm:Lorg/apache/pdfbox/pdmodel/graphics/form/PDFormXObject;

    return-void
.end method

.method public setImageFormName(Lorg/apache/pdfbox/cos/COSName;)V
    .locals 0

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDFTemplateStructure;->imageFormName:Lorg/apache/pdfbox/cos/COSName;

    return-void
.end method

.method public setImageFormResources(Lorg/apache/pdfbox/pdmodel/PDResources;)V
    .locals 0

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDFTemplateStructure;->imageFormResources:Lorg/apache/pdfbox/pdmodel/PDResources;

    return-void
.end method

.method public setImageFormStream(Lorg/apache/pdfbox/pdmodel/common/PDStream;)V
    .locals 0

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDFTemplateStructure;->imageFormStream:Lorg/apache/pdfbox/pdmodel/common/PDStream;

    return-void
.end method

.method public setImageName(Lorg/apache/pdfbox/cos/COSName;)V
    .locals 0

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDFTemplateStructure;->imageName:Lorg/apache/pdfbox/cos/COSName;

    return-void
.end method

.method public setInnerForm(Lorg/apache/pdfbox/pdmodel/graphics/form/PDFormXObject;)V
    .locals 0

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDFTemplateStructure;->innerForm:Lorg/apache/pdfbox/pdmodel/graphics/form/PDFormXObject;

    return-void
.end method

.method public setInnerFormName(Lorg/apache/pdfbox/cos/COSName;)V
    .locals 0

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDFTemplateStructure;->innerFormName:Lorg/apache/pdfbox/cos/COSName;

    return-void
.end method

.method public setInnerFormResources(Lorg/apache/pdfbox/pdmodel/PDResources;)V
    .locals 0

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDFTemplateStructure;->innerFormResources:Lorg/apache/pdfbox/pdmodel/PDResources;

    return-void
.end method

.method public setInnterFormStream(Lorg/apache/pdfbox/pdmodel/common/PDStream;)V
    .locals 0

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDFTemplateStructure;->innterFormStream:Lorg/apache/pdfbox/pdmodel/common/PDStream;

    return-void
.end method

.method public setPage(Lorg/apache/pdfbox/pdmodel/PDPage;)V
    .locals 0

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDFTemplateStructure;->page:Lorg/apache/pdfbox/pdmodel/PDPage;

    return-void
.end method

.method public setPdSignature(Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/PDSignature;)V
    .locals 0

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDFTemplateStructure;->pdSignature:Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/PDSignature;

    return-void
.end method

.method public setProcSet(Lorg/apache/pdfbox/cos/COSArray;)V
    .locals 0

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDFTemplateStructure;->procSet:Lorg/apache/pdfbox/cos/COSArray;

    return-void
.end method

.method public setSignatureField(Lorg/apache/pdfbox/pdmodel/interactive/form/PDSignatureField;)V
    .locals 0

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDFTemplateStructure;->signatureField:Lorg/apache/pdfbox/pdmodel/interactive/form/PDSignatureField;

    return-void
.end method

.method public setSignatureRectangle(Lorg/apache/pdfbox/pdmodel/common/PDRectangle;)V
    .locals 0

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDFTemplateStructure;->singatureRectangle:Lorg/apache/pdfbox/pdmodel/common/PDRectangle;

    return-void
.end method

.method public setTemplate(Lorg/apache/pdfbox/pdmodel/PDDocument;)V
    .locals 0

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDFTemplateStructure;->template:Lorg/apache/pdfbox/pdmodel/PDDocument;

    return-void
.end method

.method public setVisualSignature(Lorg/apache/pdfbox/cos/COSDocument;)V
    .locals 0

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDFTemplateStructure;->visualSignature:Lorg/apache/pdfbox/cos/COSDocument;

    return-void
.end method

.method public setWidgetDictionary(Lorg/apache/pdfbox/cos/COSDictionary;)V
    .locals 0

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDFTemplateStructure;->widgetDictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    return-void
.end method

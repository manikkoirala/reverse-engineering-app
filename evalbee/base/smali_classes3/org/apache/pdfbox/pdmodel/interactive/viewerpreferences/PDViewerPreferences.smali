.class public Lorg/apache/pdfbox/pdmodel/interactive/viewerpreferences/PDViewerPreferences;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/pdfbox/pdmodel/common/COSObjectable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/pdfbox/pdmodel/interactive/viewerpreferences/PDViewerPreferences$PRINT_SCALING;,
        Lorg/apache/pdfbox/pdmodel/interactive/viewerpreferences/PDViewerPreferences$DUPLEX;,
        Lorg/apache/pdfbox/pdmodel/interactive/viewerpreferences/PDViewerPreferences$BOUNDARY;,
        Lorg/apache/pdfbox/pdmodel/interactive/viewerpreferences/PDViewerPreferences$READING_DIRECTION;,
        Lorg/apache/pdfbox/pdmodel/interactive/viewerpreferences/PDViewerPreferences$NON_FULL_SCREEN_PAGE_MODE;
    }
.end annotation


# static fields
.field public static final BOUNDARY_ART_BOX:Ljava/lang/String; = "ArtBox"

.field public static final BOUNDARY_BLEED_BOX:Ljava/lang/String; = "BleedBox"

.field public static final BOUNDARY_CROP_BOX:Ljava/lang/String; = "CropBox"

.field public static final BOUNDARY_MEDIA_BOX:Ljava/lang/String; = "MediaBox"

.field public static final BOUNDARY_TRIM_BOX:Ljava/lang/String; = "TrimBox"

.field public static final NON_FULL_SCREEN_PAGE_MODE_USE_NONE:Ljava/lang/String; = "UseNone"

.field public static final NON_FULL_SCREEN_PAGE_MODE_USE_OPTIONAL_CONTENT:Ljava/lang/String; = "UseOC"

.field public static final NON_FULL_SCREEN_PAGE_MODE_USE_OUTLINES:Ljava/lang/String; = "UseOutlines"

.field public static final NON_FULL_SCREEN_PAGE_MODE_USE_THUMBS:Ljava/lang/String; = "UseThumbs"

.field public static final READING_DIRECTION_L2R:Ljava/lang/String; = "L2R"

.field public static final READING_DIRECTION_R2L:Ljava/lang/String; = "R2L"


# instance fields
.field private prefs:Lorg/apache/pdfbox/cos/COSDictionary;


# direct methods
.method public constructor <init>(Lorg/apache/pdfbox/cos/COSDictionary;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/interactive/viewerpreferences/PDViewerPreferences;->prefs:Lorg/apache/pdfbox/cos/COSDictionary;

    return-void
.end method


# virtual methods
.method public centerWindow()Z
    .locals 3

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/viewerpreferences/PDViewerPreferences;->prefs:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->CENTER_WINDOW:Lorg/apache/pdfbox/cos/COSName;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->getBoolean(Lorg/apache/pdfbox/cos/COSName;Z)Z

    move-result v0

    return v0
.end method

.method public displayDocTitle()Z
    .locals 3

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/viewerpreferences/PDViewerPreferences;->prefs:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->DISPLAY_DOC_TITLE:Lorg/apache/pdfbox/cos/COSName;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->getBoolean(Lorg/apache/pdfbox/cos/COSName;Z)Z

    move-result v0

    return v0
.end method

.method public fitWindow()Z
    .locals 3

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/viewerpreferences/PDViewerPreferences;->prefs:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->FIT_WINDOW:Lorg/apache/pdfbox/cos/COSName;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->getBoolean(Lorg/apache/pdfbox/cos/COSName;Z)Z

    move-result v0

    return v0
.end method

.method public getCOSObject()Lorg/apache/pdfbox/cos/COSBase;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/viewerpreferences/PDViewerPreferences;->prefs:Lorg/apache/pdfbox/cos/COSDictionary;

    return-object v0
.end method

.method public getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/viewerpreferences/PDViewerPreferences;->prefs:Lorg/apache/pdfbox/cos/COSDictionary;

    return-object v0
.end method

.method public getDuplex()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/viewerpreferences/PDViewerPreferences;->prefs:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->DUPLEX:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getNameAsString(Lorg/apache/pdfbox/cos/COSName;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getNonFullScreenPageMode()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/viewerpreferences/PDViewerPreferences;->prefs:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->NON_FULL_SCREEN_PAGE_MODE:Lorg/apache/pdfbox/cos/COSName;

    sget-object v2, Lorg/apache/pdfbox/pdmodel/interactive/viewerpreferences/PDViewerPreferences$NON_FULL_SCREEN_PAGE_MODE;->UseNone:Lorg/apache/pdfbox/pdmodel/interactive/viewerpreferences/PDViewerPreferences$NON_FULL_SCREEN_PAGE_MODE;

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->getNameAsString(Lorg/apache/pdfbox/cos/COSName;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPrintArea()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/viewerpreferences/PDViewerPreferences;->prefs:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->PRINT_AREA:Lorg/apache/pdfbox/cos/COSName;

    sget-object v2, Lorg/apache/pdfbox/pdmodel/interactive/viewerpreferences/PDViewerPreferences$BOUNDARY;->CropBox:Lorg/apache/pdfbox/pdmodel/interactive/viewerpreferences/PDViewerPreferences$BOUNDARY;

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->getNameAsString(Lorg/apache/pdfbox/cos/COSName;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPrintClip()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/viewerpreferences/PDViewerPreferences;->prefs:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->PRINT_CLIP:Lorg/apache/pdfbox/cos/COSName;

    sget-object v2, Lorg/apache/pdfbox/pdmodel/interactive/viewerpreferences/PDViewerPreferences$BOUNDARY;->CropBox:Lorg/apache/pdfbox/pdmodel/interactive/viewerpreferences/PDViewerPreferences$BOUNDARY;

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->getNameAsString(Lorg/apache/pdfbox/cos/COSName;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPrintScaling()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/viewerpreferences/PDViewerPreferences;->prefs:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->PRINT_SCALING:Lorg/apache/pdfbox/cos/COSName;

    sget-object v2, Lorg/apache/pdfbox/pdmodel/interactive/viewerpreferences/PDViewerPreferences$PRINT_SCALING;->AppDefault:Lorg/apache/pdfbox/pdmodel/interactive/viewerpreferences/PDViewerPreferences$PRINT_SCALING;

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->getNameAsString(Lorg/apache/pdfbox/cos/COSName;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getReadingDirection()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/viewerpreferences/PDViewerPreferences;->prefs:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->DIRECTION:Lorg/apache/pdfbox/cos/COSName;

    sget-object v2, Lorg/apache/pdfbox/pdmodel/interactive/viewerpreferences/PDViewerPreferences$READING_DIRECTION;->L2R:Lorg/apache/pdfbox/pdmodel/interactive/viewerpreferences/PDViewerPreferences$READING_DIRECTION;

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->getNameAsString(Lorg/apache/pdfbox/cos/COSName;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getViewArea()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/viewerpreferences/PDViewerPreferences;->prefs:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->VIEW_AREA:Lorg/apache/pdfbox/cos/COSName;

    sget-object v2, Lorg/apache/pdfbox/pdmodel/interactive/viewerpreferences/PDViewerPreferences$BOUNDARY;->CropBox:Lorg/apache/pdfbox/pdmodel/interactive/viewerpreferences/PDViewerPreferences$BOUNDARY;

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->getNameAsString(Lorg/apache/pdfbox/cos/COSName;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getViewClip()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/viewerpreferences/PDViewerPreferences;->prefs:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->VIEW_CLIP:Lorg/apache/pdfbox/cos/COSName;

    sget-object v2, Lorg/apache/pdfbox/pdmodel/interactive/viewerpreferences/PDViewerPreferences$BOUNDARY;->CropBox:Lorg/apache/pdfbox/pdmodel/interactive/viewerpreferences/PDViewerPreferences$BOUNDARY;

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->getNameAsString(Lorg/apache/pdfbox/cos/COSName;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public hideMenubar()Z
    .locals 3

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/viewerpreferences/PDViewerPreferences;->prefs:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->HIDE_MENUBAR:Lorg/apache/pdfbox/cos/COSName;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->getBoolean(Lorg/apache/pdfbox/cos/COSName;Z)Z

    move-result v0

    return v0
.end method

.method public hideToolbar()Z
    .locals 3

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/viewerpreferences/PDViewerPreferences;->prefs:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->HIDE_TOOLBAR:Lorg/apache/pdfbox/cos/COSName;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->getBoolean(Lorg/apache/pdfbox/cos/COSName;Z)Z

    move-result v0

    return v0
.end method

.method public hideWindowUI()Z
    .locals 3

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/viewerpreferences/PDViewerPreferences;->prefs:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->HIDE_WINDOWUI:Lorg/apache/pdfbox/cos/COSName;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->getBoolean(Lorg/apache/pdfbox/cos/COSName;Z)Z

    move-result v0

    return v0
.end method

.method public setCenterWindow(Z)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/viewerpreferences/PDViewerPreferences;->prefs:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->CENTER_WINDOW:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setBoolean(Lorg/apache/pdfbox/cos/COSName;Z)V

    return-void
.end method

.method public setDisplayDocTitle(Z)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/viewerpreferences/PDViewerPreferences;->prefs:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->DISPLAY_DOC_TITLE:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setBoolean(Lorg/apache/pdfbox/cos/COSName;Z)V

    return-void
.end method

.method public setDuplex(Lorg/apache/pdfbox/pdmodel/interactive/viewerpreferences/PDViewerPreferences$DUPLEX;)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/viewerpreferences/PDViewerPreferences;->prefs:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->DUPLEX:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setName(Lorg/apache/pdfbox/cos/COSName;Ljava/lang/String;)V

    return-void
.end method

.method public setFitWindow(Z)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/viewerpreferences/PDViewerPreferences;->prefs:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->FIT_WINDOW:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setBoolean(Lorg/apache/pdfbox/cos/COSName;Z)V

    return-void
.end method

.method public setHideMenubar(Z)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/viewerpreferences/PDViewerPreferences;->prefs:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->HIDE_MENUBAR:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setBoolean(Lorg/apache/pdfbox/cos/COSName;Z)V

    return-void
.end method

.method public setHideToolbar(Z)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/viewerpreferences/PDViewerPreferences;->prefs:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->HIDE_TOOLBAR:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setBoolean(Lorg/apache/pdfbox/cos/COSName;Z)V

    return-void
.end method

.method public setHideWindowUI(Z)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/viewerpreferences/PDViewerPreferences;->prefs:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->HIDE_WINDOWUI:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setBoolean(Lorg/apache/pdfbox/cos/COSName;Z)V

    return-void
.end method

.method public setNonFullScreenPageMode(Ljava/lang/String;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/viewerpreferences/PDViewerPreferences;->prefs:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->NON_FULL_SCREEN_PAGE_MODE:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setName(Lorg/apache/pdfbox/cos/COSName;Ljava/lang/String;)V

    return-void
.end method

.method public setNonFullScreenPageMode(Lorg/apache/pdfbox/pdmodel/interactive/viewerpreferences/PDViewerPreferences$NON_FULL_SCREEN_PAGE_MODE;)V
    .locals 2

    .line 2
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/viewerpreferences/PDViewerPreferences;->prefs:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->NON_FULL_SCREEN_PAGE_MODE:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setName(Lorg/apache/pdfbox/cos/COSName;Ljava/lang/String;)V

    return-void
.end method

.method public setPrintArea(Ljava/lang/String;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/viewerpreferences/PDViewerPreferences;->prefs:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->PRINT_AREA:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setName(Lorg/apache/pdfbox/cos/COSName;Ljava/lang/String;)V

    return-void
.end method

.method public setPrintArea(Lorg/apache/pdfbox/pdmodel/interactive/viewerpreferences/PDViewerPreferences$BOUNDARY;)V
    .locals 2

    .line 2
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/viewerpreferences/PDViewerPreferences;->prefs:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->PRINT_AREA:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setName(Lorg/apache/pdfbox/cos/COSName;Ljava/lang/String;)V

    return-void
.end method

.method public setPrintClip(Ljava/lang/String;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/viewerpreferences/PDViewerPreferences;->prefs:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->PRINT_CLIP:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setName(Lorg/apache/pdfbox/cos/COSName;Ljava/lang/String;)V

    return-void
.end method

.method public setPrintClip(Lorg/apache/pdfbox/pdmodel/interactive/viewerpreferences/PDViewerPreferences$BOUNDARY;)V
    .locals 2

    .line 2
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/viewerpreferences/PDViewerPreferences;->prefs:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->PRINT_CLIP:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setName(Lorg/apache/pdfbox/cos/COSName;Ljava/lang/String;)V

    return-void
.end method

.method public setPrintScaling(Lorg/apache/pdfbox/pdmodel/interactive/viewerpreferences/PDViewerPreferences$PRINT_SCALING;)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/viewerpreferences/PDViewerPreferences;->prefs:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->PRINT_SCALING:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setName(Lorg/apache/pdfbox/cos/COSName;Ljava/lang/String;)V

    return-void
.end method

.method public setReadingDirection(Ljava/lang/String;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/viewerpreferences/PDViewerPreferences;->prefs:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->DIRECTION:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setName(Lorg/apache/pdfbox/cos/COSName;Ljava/lang/String;)V

    return-void
.end method

.method public setReadingDirection(Lorg/apache/pdfbox/pdmodel/interactive/viewerpreferences/PDViewerPreferences$READING_DIRECTION;)V
    .locals 2

    .line 2
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/viewerpreferences/PDViewerPreferences;->prefs:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->DIRECTION:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setName(Lorg/apache/pdfbox/cos/COSName;Ljava/lang/String;)V

    return-void
.end method

.method public setViewArea(Ljava/lang/String;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/viewerpreferences/PDViewerPreferences;->prefs:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->VIEW_AREA:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setName(Lorg/apache/pdfbox/cos/COSName;Ljava/lang/String;)V

    return-void
.end method

.method public setViewArea(Lorg/apache/pdfbox/pdmodel/interactive/viewerpreferences/PDViewerPreferences$BOUNDARY;)V
    .locals 2

    .line 2
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/viewerpreferences/PDViewerPreferences;->prefs:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->VIEW_AREA:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setName(Lorg/apache/pdfbox/cos/COSName;Ljava/lang/String;)V

    return-void
.end method

.method public setViewClip(Ljava/lang/String;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/viewerpreferences/PDViewerPreferences;->prefs:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->VIEW_CLIP:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setName(Lorg/apache/pdfbox/cos/COSName;Ljava/lang/String;)V

    return-void
.end method

.method public setViewClip(Lorg/apache/pdfbox/pdmodel/interactive/viewerpreferences/PDViewerPreferences$BOUNDARY;)V
    .locals 2

    .line 2
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/viewerpreferences/PDViewerPreferences;->prefs:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->VIEW_CLIP:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setName(Lorg/apache/pdfbox/cos/COSName;Ljava/lang/String;)V

    return-void
.end method

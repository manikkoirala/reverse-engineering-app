.class public Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/destination/PDNamedDestination;
.super Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/destination/PDDestination;
.source "SourceFile"


# instance fields
.field private namedDestination:Lorg/apache/pdfbox/cos/COSBase;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/destination/PDDestination;-><init>()V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/destination/PDDestination;-><init>()V

    new-instance v0, Lorg/apache/pdfbox/cos/COSString;

    invoke-direct {v0, p1}, Lorg/apache/pdfbox/cos/COSString;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/destination/PDNamedDestination;->namedDestination:Lorg/apache/pdfbox/cos/COSBase;

    return-void
.end method

.method public constructor <init>(Lorg/apache/pdfbox/cos/COSName;)V
    .locals 0

    .line 2
    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/destination/PDDestination;-><init>()V

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/destination/PDNamedDestination;->namedDestination:Lorg/apache/pdfbox/cos/COSBase;

    return-void
.end method

.method public constructor <init>(Lorg/apache/pdfbox/cos/COSString;)V
    .locals 0

    .line 3
    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/destination/PDDestination;-><init>()V

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/destination/PDNamedDestination;->namedDestination:Lorg/apache/pdfbox/cos/COSBase;

    return-void
.end method


# virtual methods
.method public getCOSObject()Lorg/apache/pdfbox/cos/COSBase;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/destination/PDNamedDestination;->namedDestination:Lorg/apache/pdfbox/cos/COSBase;

    return-object v0
.end method

.method public getNamedDestination()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/destination/PDNamedDestination;->namedDestination:Lorg/apache/pdfbox/cos/COSBase;

    instance-of v1, v0, Lorg/apache/pdfbox/cos/COSString;

    if-eqz v1, :cond_0

    check-cast v0, Lorg/apache/pdfbox/cos/COSString;

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSString;->getString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    instance-of v1, v0, Lorg/apache/pdfbox/cos/COSName;

    if-eqz v1, :cond_1

    check-cast v0, Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSName;->getName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public setNamedDestination(Ljava/lang/String;)V
    .locals 1

    if-nez p1, :cond_0

    const/4 p1, 0x0

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/destination/PDNamedDestination;->namedDestination:Lorg/apache/pdfbox/cos/COSBase;

    goto :goto_0

    :cond_0
    new-instance v0, Lorg/apache/pdfbox/cos/COSString;

    invoke-direct {v0, p1}, Lorg/apache/pdfbox/cos/COSString;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/destination/PDNamedDestination;->namedDestination:Lorg/apache/pdfbox/cos/COSBase;

    :goto_0
    return-void
.end method

.class public final Lorg/apache/pdfbox/pdmodel/interactive/action/PDActionFactory;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static createAction(Lorg/apache/pdfbox/cos/COSDictionary;)Lorg/apache/pdfbox/pdmodel/interactive/action/PDAction;
    .locals 2

    if-eqz p0, :cond_5

    const-string v0, "S"

    invoke-virtual {p0, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->getNameAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "JavaScript"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v0, Lorg/apache/pdfbox/pdmodel/interactive/action/PDActionJavaScript;

    invoke-direct {v0, p0}, Lorg/apache/pdfbox/pdmodel/interactive/action/PDActionJavaScript;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    goto :goto_0

    :cond_0
    const-string v1, "GoTo"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance v0, Lorg/apache/pdfbox/pdmodel/interactive/action/PDActionGoTo;

    invoke-direct {v0, p0}, Lorg/apache/pdfbox/pdmodel/interactive/action/PDActionGoTo;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    goto :goto_0

    :cond_1
    const-string v1, "Launch"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    new-instance v0, Lorg/apache/pdfbox/pdmodel/interactive/action/PDActionLaunch;

    invoke-direct {v0, p0}, Lorg/apache/pdfbox/pdmodel/interactive/action/PDActionLaunch;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    goto :goto_0

    :cond_2
    const-string v1, "GoToR"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    new-instance v0, Lorg/apache/pdfbox/pdmodel/interactive/action/PDActionRemoteGoTo;

    invoke-direct {v0, p0}, Lorg/apache/pdfbox/pdmodel/interactive/action/PDActionRemoteGoTo;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    goto :goto_0

    :cond_3
    const-string v1, "URI"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    new-instance v0, Lorg/apache/pdfbox/pdmodel/interactive/action/PDActionURI;

    invoke-direct {v0, p0}, Lorg/apache/pdfbox/pdmodel/interactive/action/PDActionURI;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    goto :goto_0

    :cond_4
    const-string v1, "Named"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    new-instance v0, Lorg/apache/pdfbox/pdmodel/interactive/action/PDActionNamed;

    invoke-direct {v0, p0}, Lorg/apache/pdfbox/pdmodel/interactive/action/PDActionNamed;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    goto :goto_0

    :cond_5
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

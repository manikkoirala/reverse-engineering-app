.class public abstract Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineNode;
.super Lorg/apache/pdfbox/pdmodel/common/PDDictionaryWrapper;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/common/PDDictionaryWrapper;-><init>()V

    return-void
.end method

.method public constructor <init>(Lorg/apache/pdfbox/cos/COSDictionary;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/common/PDDictionaryWrapper;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    return-void
.end method

.method private append(Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineItem;)V
    .locals 1

    invoke-virtual {p1, p0}, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineNode;->setParent(Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineNode;)V

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineNode;->hasChildren()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineNode;->setFirstChild(Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineNode;)V

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineNode;->getLastChild()Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineItem;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineItem;->setNextSibling(Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineNode;)V

    invoke-virtual {p1, v0}, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineItem;->setPreviousSibling(Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineNode;)V

    :goto_0
    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineNode;->setLastChild(Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineNode;)V

    return-void
.end method

.method private prepend(Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineItem;)V
    .locals 1

    invoke-virtual {p1, p0}, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineNode;->setParent(Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineNode;)V

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineNode;->hasChildren()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineNode;->setLastChild(Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineNode;)V

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineNode;->getFirstChild()Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineItem;

    move-result-object v0

    invoke-virtual {p1, v0}, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineItem;->setNextSibling(Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineNode;)V

    invoke-virtual {v0, p1}, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineItem;->setPreviousSibling(Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineNode;)V

    :goto_0
    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineNode;->setFirstChild(Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineNode;)V

    return-void
.end method

.method private switchNodeCount()V
    .locals 1

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineNode;->getOpenCount()I

    move-result v0

    neg-int v0, v0

    invoke-virtual {p0, v0}, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineNode;->setOpenCount(I)V

    invoke-virtual {p0, v0}, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineNode;->updateParentOpenCount(I)V

    return-void
.end method


# virtual methods
.method public addFirst(Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineItem;)V
    .locals 0

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineNode;->requireSingleNode(Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineItem;)V

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineNode;->prepend(Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineItem;)V

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineNode;->updateParentOpenCountForAddedChild(Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineItem;)V

    return-void
.end method

.method public addLast(Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineItem;)V
    .locals 0

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineNode;->requireSingleNode(Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineItem;)V

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineNode;->append(Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineItem;)V

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineNode;->updateParentOpenCountForAddedChild(Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineItem;)V

    return-void
.end method

.method public children()Ljava/lang/Iterable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Iterable<",
            "Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineItem;",
            ">;"
        }
    .end annotation

    new-instance v0, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineNode$1;

    invoke-direct {v0, p0}, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineNode$1;-><init>(Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineNode;)V

    return-object v0
.end method

.method public closeNode()V
    .locals 1

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineNode;->isNodeOpen()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineNode;->switchNodeCount()V

    :cond_0
    return-void
.end method

.method public getFirstChild()Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineItem;
    .locals 1

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->FIRST:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p0, v0}, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineNode;->getOutlineItem(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineItem;

    move-result-object v0

    return-object v0
.end method

.method public getLastChild()Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineItem;
    .locals 1

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->LAST:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p0, v0}, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineNode;->getOutlineItem(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineItem;

    move-result-object v0

    return-object v0
.end method

.method public getOpenCount()I
    .locals 3

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/common/PDDictionaryWrapper;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->COUNT:Lorg/apache/pdfbox/cos/COSName;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->getInt(Lorg/apache/pdfbox/cos/COSName;I)I

    move-result v0

    return v0
.end method

.method public getOutlineItem(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineItem;
    .locals 1

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/common/PDDictionaryWrapper;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object p1

    check-cast p1, Lorg/apache/pdfbox/cos/COSDictionary;

    if-eqz p1, :cond_0

    new-instance v0, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineItem;

    invoke-direct {v0, p1}, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineItem;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    return-object v0

    :cond_0
    const/4 p1, 0x0

    return-object p1
.end method

.method public getParent()Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineNode;
    .locals 3

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/common/PDDictionaryWrapper;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->PARENT:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSDictionary;

    if-eqz v0, :cond_1

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->OUTLINES:Lorg/apache/pdfbox/cos/COSName;

    sget-object v2, Lorg/apache/pdfbox/cos/COSName;->TYPE:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->getCOSName(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSName;

    move-result-object v2

    invoke-virtual {v1, v2}, Lorg/apache/pdfbox/cos/COSName;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDDocumentOutline;

    invoke-direct {v1, v0}, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDDocumentOutline;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    return-object v1

    :cond_0
    new-instance v1, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineItem;

    invoke-direct {v1, v0}, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineItem;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    return-object v1

    :cond_1
    const/4 v0, 0x0

    return-object v0
.end method

.method public hasChildren()Z
    .locals 1

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineNode;->getFirstChild()Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineItem;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isNodeOpen()Z
    .locals 1

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineNode;->getOpenCount()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public openNode()V
    .locals 1

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineNode;->isNodeOpen()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineNode;->switchNodeCount()V

    :cond_0
    return-void
.end method

.method public requireSingleNode(Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineItem;)V
    .locals 1

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineItem;->getNextSibling()Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineItem;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineItem;->getPreviousSibling()Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineItem;

    move-result-object p1

    if-nez p1, :cond_0

    return-void

    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "A single node with no siblings is required"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public setFirstChild(Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineNode;)V
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/common/PDDictionaryWrapper;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->FIRST:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)V

    return-void
.end method

.method public setLastChild(Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineNode;)V
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/common/PDDictionaryWrapper;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->LAST:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)V

    return-void
.end method

.method public setOpenCount(I)V
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/common/PDDictionaryWrapper;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->COUNT:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setInt(Lorg/apache/pdfbox/cos/COSName;I)V

    return-void
.end method

.method public setParent(Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineNode;)V
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/common/PDDictionaryWrapper;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->PARENT:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)V

    return-void
.end method

.method public updateParentOpenCount(I)V
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineNode;->getParent()Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineNode;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineNode;->isNodeOpen()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineNode;->getOpenCount()I

    move-result v1

    add-int/2addr v1, p1

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineNode;->setOpenCount(I)V

    invoke-virtual {v0, p1}, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineNode;->updateParentOpenCount(I)V

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineNode;->getOpenCount()I

    move-result v1

    sub-int/2addr v1, p1

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineNode;->setOpenCount(I)V

    :cond_1
    :goto_0
    return-void
.end method

.method public updateParentOpenCountForAddedChild(Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineItem;)V
    .locals 2

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineNode;->isNodeOpen()Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineNode;->getOpenCount()I

    move-result v0

    add-int/2addr v1, v0

    :cond_0
    invoke-virtual {p1, v1}, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineNode;->updateParentOpenCount(I)V

    return-void
.end method

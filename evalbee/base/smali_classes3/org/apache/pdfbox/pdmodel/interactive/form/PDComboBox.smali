.class public final Lorg/apache/pdfbox/pdmodel/interactive/form/PDComboBox;
.super Lorg/apache/pdfbox/pdmodel/interactive/form/PDChoice;
.source "SourceFile"


# static fields
.field private static final FLAG_EDIT:I = 0x40000


# direct methods
.method public constructor <init>(Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDChoice;-><init>(Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;)V

    const/4 p1, 0x1

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDChoice;->setCombo(Z)V

    return-void
.end method

.method public constructor <init>(Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;Lorg/apache/pdfbox/cos/COSDictionary;Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;)V
    .locals 0

    .line 2
    invoke-direct {p0, p1, p2, p3}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDChoice;-><init>(Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;Lorg/apache/pdfbox/cos/COSDictionary;Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;)V

    return-void
.end method


# virtual methods
.method public isEdit()Z
    .locals 3

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->FF:Lorg/apache/pdfbox/cos/COSName;

    const/high16 v2, 0x40000

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->getFlag(Lorg/apache/pdfbox/cos/COSName;I)Z

    move-result v0

    return v0
.end method

.method public setEdit(Z)V
    .locals 3

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->FF:Lorg/apache/pdfbox/cos/COSName;

    const/high16 v2, 0x40000

    invoke-virtual {v0, v1, v2, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setFlag(Lorg/apache/pdfbox/cos/COSName;IZ)V

    return-void
.end method

.method public setValue(Ljava/lang/String;)V
    .locals 2

    if-eqz p1, :cond_2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDComboBox;->isEdit()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDChoice;->getOptions()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    goto :goto_0

    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "The list box does not contain the given value."

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    :goto_0
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->V:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setString(Lorg/apache/pdfbox/cos/COSName;Ljava/lang/String;)V

    const/4 p1, 0x0

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDChoice;->setSelectedOptionsIndex(Ljava/util/List;)V

    goto :goto_1

    :cond_2
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object p1

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->V:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p1, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->removeItem(Lorg/apache/pdfbox/cos/COSName;)V

    :goto_1
    return-void
.end method

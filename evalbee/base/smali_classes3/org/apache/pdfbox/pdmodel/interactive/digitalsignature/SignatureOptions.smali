.class public Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/SignatureOptions;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Closeable;


# instance fields
.field private pageNo:I

.field private preferedSignatureSize:I

.field private visualSignature:Lorg/apache/pdfbox/cos/COSDocument;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/SignatureOptions;->pageNo:I

    return-void
.end method


# virtual methods
.method public close()V
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/SignatureOptions;->visualSignature:Lorg/apache/pdfbox/cos/COSDocument;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSDocument;->close()V

    :cond_0
    return-void
.end method

.method public getPage()I
    .locals 1

    iget v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/SignatureOptions;->pageNo:I

    return v0
.end method

.method public getPreferedSignatureSize()I
    .locals 1

    iget v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/SignatureOptions;->preferedSignatureSize:I

    return v0
.end method

.method public getVisualSignature()Lorg/apache/pdfbox/cos/COSDocument;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/SignatureOptions;->visualSignature:Lorg/apache/pdfbox/cos/COSDocument;

    return-object v0
.end method

.method public setPage(I)V
    .locals 0

    iput p1, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/SignatureOptions;->pageNo:I

    return-void
.end method

.method public setPreferedSignatureSize(I)V
    .locals 0

    if-lez p1, :cond_0

    iput p1, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/SignatureOptions;->preferedSignatureSize:I

    :cond_0
    return-void
.end method

.method public setVisualSignature(Ljava/io/InputStream;)V
    .locals 1

    .line 1
    new-instance v0, Lorg/apache/pdfbox/pdfparser/VisualSignatureParser;

    invoke-direct {v0, p1}, Lorg/apache/pdfbox/pdfparser/VisualSignatureParser;-><init>(Ljava/io/InputStream;)V

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdfparser/VisualSignatureParser;->parse()V

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdfparser/VisualSignatureParser;->getDocument()Lorg/apache/pdfbox/cos/COSDocument;

    move-result-object p1

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/SignatureOptions;->visualSignature:Lorg/apache/pdfbox/cos/COSDocument;

    return-void
.end method

.method public setVisualSignature(Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDVisibleSigProperties;)V
    .locals 0

    .line 2
    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/visible/PDVisibleSigProperties;->getVisibleSignature()Ljava/io/InputStream;

    move-result-object p1

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/SignatureOptions;->setVisualSignature(Ljava/io/InputStream;)V

    return-void
.end method

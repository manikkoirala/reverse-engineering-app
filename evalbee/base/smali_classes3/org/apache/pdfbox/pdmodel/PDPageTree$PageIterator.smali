.class final Lorg/apache/pdfbox/pdmodel/PDPageTree$PageIterator;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/pdfbox/pdmodel/PDPageTree;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "PageIterator"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Iterator<",
        "Lorg/apache/pdfbox/pdmodel/PDPage;",
        ">;"
    }
.end annotation


# instance fields
.field private final queue:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue<",
            "Lorg/apache/pdfbox/cos/COSDictionary;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lorg/apache/pdfbox/pdmodel/PDPageTree;


# direct methods
.method private constructor <init>(Lorg/apache/pdfbox/pdmodel/PDPageTree;Lorg/apache/pdfbox/cos/COSDictionary;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/PDPageTree$PageIterator;->this$0:Lorg/apache/pdfbox/pdmodel/PDPageTree;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance p1, Ljava/util/ArrayDeque;

    invoke-direct {p1}, Ljava/util/ArrayDeque;-><init>()V

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/PDPageTree$PageIterator;->queue:Ljava/util/Queue;

    invoke-direct {p0, p2}, Lorg/apache/pdfbox/pdmodel/PDPageTree$PageIterator;->enqueueKids(Lorg/apache/pdfbox/cos/COSDictionary;)V

    return-void
.end method

.method public synthetic constructor <init>(Lorg/apache/pdfbox/pdmodel/PDPageTree;Lorg/apache/pdfbox/cos/COSDictionary;Lorg/apache/pdfbox/pdmodel/PDPageTree$1;)V
    .locals 0

    .line 2
    invoke-direct {p0, p1, p2}, Lorg/apache/pdfbox/pdmodel/PDPageTree$PageIterator;-><init>(Lorg/apache/pdfbox/pdmodel/PDPageTree;Lorg/apache/pdfbox/cos/COSDictionary;)V

    return-void
.end method

.method private enqueueKids(Lorg/apache/pdfbox/cos/COSDictionary;)V
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDPageTree$PageIterator;->this$0:Lorg/apache/pdfbox/pdmodel/PDPageTree;

    invoke-static {v0, p1}, Lorg/apache/pdfbox/pdmodel/PDPageTree;->access$100(Lorg/apache/pdfbox/pdmodel/PDPageTree;Lorg/apache/pdfbox/cos/COSDictionary;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDPageTree$PageIterator;->this$0:Lorg/apache/pdfbox/pdmodel/PDPageTree;

    invoke-static {v0, p1}, Lorg/apache/pdfbox/pdmodel/PDPageTree;->access$200(Lorg/apache/pdfbox/pdmodel/PDPageTree;Lorg/apache/pdfbox/cos/COSDictionary;)Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/pdmodel/PDPageTree$PageIterator;->enqueueKids(Lorg/apache/pdfbox/cos/COSDictionary;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDPageTree$PageIterator;->queue:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    :cond_1
    return-void
.end method


# virtual methods
.method public hasNext()Z
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDPageTree$PageIterator;->queue:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public bridge synthetic next()Ljava/lang/Object;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/PDPageTree$PageIterator;->next()Lorg/apache/pdfbox/pdmodel/PDPage;

    move-result-object v0

    return-object v0
.end method

.method public next()Lorg/apache/pdfbox/pdmodel/PDPage;
    .locals 4

    .line 2
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDPageTree$PageIterator;->queue:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->TYPE:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getCOSName(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSName;

    move-result-object v1

    sget-object v2, Lorg/apache/pdfbox/cos/COSName;->PAGE:Lorg/apache/pdfbox/cos/COSName;

    if-ne v1, v2, :cond_0

    new-instance v1, Lorg/apache/pdfbox/pdmodel/PDPage;

    invoke-direct {v1, v0}, Lorg/apache/pdfbox/pdmodel/PDPage;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    return-object v1

    :cond_0
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Expected Page but got "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public remove()V
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.class public final Lorg/apache/pdfbox/pdmodel/PDPageContentStream;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Closeable;


# instance fields
.field private final document:Lorg/apache/pdfbox/pdmodel/PDDocument;

.field private final fontStack:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack<",
            "Lorg/apache/pdfbox/pdmodel/font/PDFont;",
            ">;"
        }
    .end annotation
.end field

.field private final formatDecimal:Ljava/text/NumberFormat;

.field private inTextMode:Z

.field private final nonStrokingColorSpaceStack:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack<",
            "Lorg/apache/pdfbox/pdmodel/graphics/color/PDColorSpace;",
            ">;"
        }
    .end annotation
.end field

.field private output:Ljava/io/OutputStream;

.field private resources:Lorg/apache/pdfbox/pdmodel/PDResources;

.field private strokingColorSpaceStack:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack<",
            "Lorg/apache/pdfbox/pdmodel/graphics/color/PDColorSpace;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lorg/apache/pdfbox/pdmodel/PDDocument;Lorg/apache/pdfbox/pdmodel/PDPage;)V
    .locals 2

    .line 1
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, p1, p2, v0, v1}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;-><init>(Lorg/apache/pdfbox/pdmodel/PDDocument;Lorg/apache/pdfbox/pdmodel/PDPage;ZZ)V

    return-void
.end method

.method public constructor <init>(Lorg/apache/pdfbox/pdmodel/PDDocument;Lorg/apache/pdfbox/pdmodel/PDPage;ZZ)V
    .locals 6

    .line 2
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    invoke-direct/range {v0 .. v5}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;-><init>(Lorg/apache/pdfbox/pdmodel/PDDocument;Lorg/apache/pdfbox/pdmodel/PDPage;ZZZ)V

    return-void
.end method

.method public constructor <init>(Lorg/apache/pdfbox/pdmodel/PDDocument;Lorg/apache/pdfbox/pdmodel/PDPage;ZZZ)V
    .locals 5

    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->inTextMode:Z

    new-instance v1, Ljava/util/Stack;

    invoke-direct {v1}, Ljava/util/Stack;-><init>()V

    iput-object v1, p0, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->fontStack:Ljava/util/Stack;

    new-instance v1, Ljava/util/Stack;

    invoke-direct {v1}, Ljava/util/Stack;-><init>()V

    iput-object v1, p0, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->nonStrokingColorSpaceStack:Ljava/util/Stack;

    new-instance v1, Ljava/util/Stack;

    invoke-direct {v1}, Ljava/util/Stack;-><init>()V

    iput-object v1, p0, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->strokingColorSpaceStack:Ljava/util/Stack;

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {v1}, Ljava/text/NumberFormat;->getNumberInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->formatDecimal:Ljava/text/NumberFormat;

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->document:Lorg/apache/pdfbox/pdmodel/PDDocument;

    invoke-virtual {p2}, Lorg/apache/pdfbox/pdmodel/PDPage;->getStream()Lorg/apache/pdfbox/pdmodel/common/PDStream;

    move-result-object v2

    if-eqz v2, :cond_0

    const/4 v3, 0x1

    goto :goto_0

    :cond_0
    move v3, v0

    :goto_0
    if-eqz p3, :cond_5

    if-eqz v3, :cond_5

    new-instance p3, Lorg/apache/pdfbox/pdmodel/common/PDStream;

    invoke-direct {p3, p1}, Lorg/apache/pdfbox/pdmodel/common/PDStream;-><init>(Lorg/apache/pdfbox/pdmodel/PDDocument;)V

    invoke-virtual {v2}, Lorg/apache/pdfbox/pdmodel/common/PDStream;->getStream()Lorg/apache/pdfbox/cos/COSStream;

    move-result-object v3

    instance-of v3, v3, Lorg/apache/pdfbox/pdmodel/common/COSStreamArray;

    if-eqz v3, :cond_1

    invoke-virtual {v2}, Lorg/apache/pdfbox/pdmodel/common/PDStream;->getStream()Lorg/apache/pdfbox/cos/COSStream;

    move-result-object v2

    check-cast v2, Lorg/apache/pdfbox/pdmodel/common/COSStreamArray;

    invoke-virtual {p3}, Lorg/apache/pdfbox/pdmodel/common/PDStream;->getStream()Lorg/apache/pdfbox/cos/COSStream;

    move-result-object v3

    invoke-virtual {v2, v3}, Lorg/apache/pdfbox/pdmodel/common/COSStreamArray;->appendStream(Lorg/apache/pdfbox/cos/COSStream;)V

    goto :goto_1

    :cond_1
    new-instance v3, Lorg/apache/pdfbox/cos/COSArray;

    invoke-direct {v3}, Lorg/apache/pdfbox/cos/COSArray;-><init>()V

    invoke-virtual {v2}, Lorg/apache/pdfbox/pdmodel/common/PDStream;->getCOSObject()Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v2

    invoke-virtual {v3, v2}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    invoke-virtual {p3}, Lorg/apache/pdfbox/pdmodel/common/PDStream;->getCOSObject()Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v2

    invoke-virtual {v3, v2}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    new-instance v2, Lorg/apache/pdfbox/pdmodel/common/COSStreamArray;

    invoke-direct {v2, v3}, Lorg/apache/pdfbox/pdmodel/common/COSStreamArray;-><init>(Lorg/apache/pdfbox/cos/COSArray;)V

    :goto_1
    if-eqz p4, :cond_2

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    sget-object v4, Lorg/apache/pdfbox/cos/COSName;->FLATE_DECODE:Lorg/apache/pdfbox/cos/COSName;

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {p3, v3}, Lorg/apache/pdfbox/pdmodel/common/PDStream;->setFilters(Ljava/util/List;)V

    :cond_2
    if-eqz p5, :cond_4

    new-instance v3, Lorg/apache/pdfbox/pdmodel/common/PDStream;

    invoke-direct {v3, p1}, Lorg/apache/pdfbox/pdmodel/common/PDStream;-><init>(Lorg/apache/pdfbox/pdmodel/PDDocument;)V

    invoke-virtual {v3}, Lorg/apache/pdfbox/pdmodel/common/PDStream;->createOutputStream()Ljava/io/OutputStream;

    move-result-object p1

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->output:Ljava/io/OutputStream;

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->saveGraphicsState()V

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->close()V

    if-eqz p4, :cond_3

    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    sget-object p4, Lorg/apache/pdfbox/cos/COSName;->FLATE_DECODE:Lorg/apache/pdfbox/cos/COSName;

    invoke-interface {p1, p4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {v3, p1}, Lorg/apache/pdfbox/pdmodel/common/PDStream;->setFilters(Ljava/util/List;)V

    :cond_3
    invoke-virtual {v2, v3}, Lorg/apache/pdfbox/pdmodel/common/COSStreamArray;->insertCOSStream(Lorg/apache/pdfbox/pdmodel/common/PDStream;)V

    :cond_4
    new-instance p1, Lorg/apache/pdfbox/pdmodel/common/PDStream;

    invoke-direct {p1, v2}, Lorg/apache/pdfbox/pdmodel/common/PDStream;-><init>(Lorg/apache/pdfbox/cos/COSStream;)V

    invoke-virtual {p2, p1}, Lorg/apache/pdfbox/pdmodel/PDPage;->setContents(Lorg/apache/pdfbox/pdmodel/common/PDStream;)V

    invoke-virtual {p3}, Lorg/apache/pdfbox/pdmodel/common/PDStream;->createOutputStream()Ljava/io/OutputStream;

    move-result-object p1

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->output:Ljava/io/OutputStream;

    if-eqz p5, :cond_8

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->restoreGraphicsState()V

    goto :goto_2

    :cond_5
    if-eqz v3, :cond_6

    const-string p3, "PdfBoxAndroid"

    const-string p5, "You are overwriting an existing content, you should use the append mode"

    invoke-static {p3, p5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_6
    new-instance p3, Lorg/apache/pdfbox/pdmodel/common/PDStream;

    invoke-direct {p3, p1}, Lorg/apache/pdfbox/pdmodel/common/PDStream;-><init>(Lorg/apache/pdfbox/pdmodel/PDDocument;)V

    if-eqz p4, :cond_7

    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    sget-object p4, Lorg/apache/pdfbox/cos/COSName;->FLATE_DECODE:Lorg/apache/pdfbox/cos/COSName;

    invoke-interface {p1, p4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {p3, p1}, Lorg/apache/pdfbox/pdmodel/common/PDStream;->setFilters(Ljava/util/List;)V

    :cond_7
    invoke-virtual {p2, p3}, Lorg/apache/pdfbox/pdmodel/PDPage;->setContents(Lorg/apache/pdfbox/pdmodel/common/PDStream;)V

    invoke-virtual {p3}, Lorg/apache/pdfbox/pdmodel/common/PDStream;->createOutputStream()Ljava/io/OutputStream;

    move-result-object p1

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->output:Ljava/io/OutputStream;

    :cond_8
    :goto_2
    const/16 p1, 0xa

    invoke-virtual {v1, p1}, Ljava/text/NumberFormat;->setMaximumFractionDigits(I)V

    invoke-virtual {v1, v0}, Ljava/text/NumberFormat;->setGroupingUsed(Z)V

    invoke-virtual {p2}, Lorg/apache/pdfbox/pdmodel/PDPage;->getResources()Lorg/apache/pdfbox/pdmodel/PDResources;

    move-result-object p1

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->resources:Lorg/apache/pdfbox/pdmodel/PDResources;

    if-nez p1, :cond_9

    new-instance p1, Lorg/apache/pdfbox/pdmodel/PDResources;

    invoke-direct {p1}, Lorg/apache/pdfbox/pdmodel/PDResources;-><init>()V

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->resources:Lorg/apache/pdfbox/pdmodel/PDResources;

    invoke-virtual {p2, p1}, Lorg/apache/pdfbox/pdmodel/PDPage;->setResources(Lorg/apache/pdfbox/pdmodel/PDResources;)V

    :cond_9
    return-void
.end method

.method private getName(Lorg/apache/pdfbox/pdmodel/graphics/color/PDColorSpace;)Lorg/apache/pdfbox/cos/COSName;
    .locals 1

    instance-of v0, p1, Lorg/apache/pdfbox/pdmodel/graphics/color/PDDeviceGray;

    if-nez v0, :cond_1

    instance-of v0, p1, Lorg/apache/pdfbox/pdmodel/graphics/color/PDDeviceRGB;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->resources:Lorg/apache/pdfbox/pdmodel/PDResources;

    invoke-virtual {v0, p1}, Lorg/apache/pdfbox/pdmodel/PDResources;->add(Lorg/apache/pdfbox/pdmodel/graphics/color/PDColorSpace;)Lorg/apache/pdfbox/cos/COSName;

    move-result-object p1

    return-object p1

    :cond_1
    :goto_0
    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/graphics/color/PDColorSpace;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lorg/apache/pdfbox/cos/COSName;->getPDFName(Ljava/lang/String;)Lorg/apache/pdfbox/cos/COSName;

    move-result-object p1

    return-object p1
.end method

.method private write(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->output:Ljava/io/OutputStream;

    sget-object v1, Lorg/apache/pdfbox/util/Charsets;->US_ASCII:Ljava/nio/charset/Charset;

    invoke-virtual {p1, v1}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/io/OutputStream;->write([B)V

    return-void
.end method

.method private writeAffineTransform(Lorg/apache/pdfbox/util/awt/AffineTransform;)V
    .locals 4

    const/4 v0, 0x6

    new-array v1, v0, [D

    invoke-virtual {p1, v1}, Lorg/apache/pdfbox/util/awt/AffineTransform;->getMatrix([D)V

    const/4 p1, 0x0

    :goto_0
    if-ge p1, v0, :cond_0

    aget-wide v2, v1, p1

    double-to-float v2, v2

    invoke-direct {p0, v2}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->writeOperand(F)V

    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private writeBytes([B)V
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->output:Ljava/io/OutputStream;

    invoke-virtual {v0, p1}, Ljava/io/OutputStream;->write([B)V

    return-void
.end method

.method private writeLine()V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->output:Ljava/io/OutputStream;

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Ljava/io/OutputStream;->write(I)V

    return-void
.end method

.method private writeOperand(F)V
    .locals 3

    .line 1
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->formatDecimal:Ljava/text/NumberFormat;

    float-to-double v1, p1

    invoke-virtual {v0, v1, v2}, Ljava/text/NumberFormat;->format(D)Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->writeOperator(Ljava/lang/String;)V

    iget-object p1, p0, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->output:Ljava/io/OutputStream;

    const/16 v0, 0x20

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write(I)V

    return-void
.end method

.method private writeOperand(I)V
    .locals 3

    .line 2
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->formatDecimal:Ljava/text/NumberFormat;

    int-to-long v1, p1

    invoke-virtual {v0, v1, v2}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->writeOperator(Ljava/lang/String;)V

    iget-object p1, p0, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->output:Ljava/io/OutputStream;

    const/16 v0, 0x20

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write(I)V

    return-void
.end method

.method private writeOperand(Lorg/apache/pdfbox/cos/COSName;)V
    .locals 1

    .line 3
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->output:Ljava/io/OutputStream;

    invoke-virtual {p1, v0}, Lorg/apache/pdfbox/cos/COSName;->writePDF(Ljava/io/OutputStream;)V

    iget-object p1, p0, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->output:Ljava/io/OutputStream;

    const/16 v0, 0x20

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write(I)V

    return-void
.end method

.method private writeOperator(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->output:Ljava/io/OutputStream;

    sget-object v1, Lorg/apache/pdfbox/util/Charsets;->US_ASCII:Ljava/nio/charset/Charset;

    invoke-virtual {p1, v1}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/io/OutputStream;->write([B)V

    iget-object p1, p0, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->output:Ljava/io/OutputStream;

    const/16 v0, 0xa

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write(I)V

    return-void
.end method


# virtual methods
.method public addBezier31(FFFF)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    invoke-virtual {p0, p1, p2, p3, p4}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->curveTo1(FFFF)V

    return-void
.end method

.method public addBezier312(FFFFFF)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    invoke-virtual/range {p0 .. p6}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->curveTo(FFFFFF)V

    return-void
.end method

.method public addBezier32(FFFF)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    invoke-virtual {p0, p1, p2, p3, p4}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->curveTo2(FFFF)V

    return-void
.end method

.method public addLine(FFFF)V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    iget-boolean v0, p0, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->inTextMode:Z

    if-nez v0, :cond_0

    invoke-virtual {p0, p1, p2}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->moveTo(FF)V

    invoke-virtual {p0, p3, p4}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->lineTo(FF)V

    return-void

    :cond_0
    new-instance p1, Ljava/io/IOException;

    const-string p2, "Error: addLine is not allowed within a text block."

    invoke-direct {p1, p2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public addPolygon([F[F)V
    .locals 3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    iget-boolean v0, p0, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->inTextMode:Z

    if-nez v0, :cond_3

    array-length v0, p1

    array-length v1, p2

    if-ne v0, v1, :cond_2

    const/4 v0, 0x0

    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_1

    if-nez v0, :cond_0

    aget v1, p1, v0

    aget v2, p2, v0

    invoke-virtual {p0, v1, v2}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->moveTo(FF)V

    goto :goto_1

    :cond_0
    aget v1, p1, v0

    aget v2, p2, v0

    invoke-virtual {p0, v1, v2}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->lineTo(FF)V

    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->closeSubPath()V

    return-void

    :cond_2
    new-instance p1, Ljava/io/IOException;

    const-string p2, "Error: some points are missing coordinate"

    invoke-direct {p1, p2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_3
    new-instance p1, Ljava/io/IOException;

    const-string p2, "Error: addPolygon is not allowed within a text block."

    invoke-direct {p1, p2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public addRect(FFFF)V
    .locals 1

    iget-boolean v0, p0, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->inTextMode:Z

    if-nez v0, :cond_0

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->writeOperand(F)V

    invoke-direct {p0, p2}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->writeOperand(F)V

    invoke-direct {p0, p3}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->writeOperand(F)V

    invoke-direct {p0, p4}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->writeOperand(F)V

    const-string p1, "re"

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->writeOperator(Ljava/lang/String;)V

    return-void

    :cond_0
    new-instance p1, Ljava/io/IOException;

    const-string p2, "Error: addRect is not allowed within a text block."

    invoke-direct {p1, p2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public appendCOSName(Lorg/apache/pdfbox/cos/COSName;)V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->output:Ljava/io/OutputStream;

    invoke-virtual {p1, v0}, Lorg/apache/pdfbox/cos/COSName;->writePDF(Ljava/io/OutputStream;)V

    return-void
.end method

.method public appendRawCommands(D)V
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->output:Ljava/io/OutputStream;

    iget-object v1, p0, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->formatDecimal:Ljava/text/NumberFormat;

    invoke-virtual {v1, p1, p2}, Ljava/text/NumberFormat;->format(D)Ljava/lang/String;

    move-result-object p1

    sget-object p2, Lorg/apache/pdfbox/util/Charsets;->US_ASCII:Ljava/nio/charset/Charset;

    invoke-virtual {p1, p2}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/io/OutputStream;->write([B)V

    return-void
.end method

.method public appendRawCommands(F)V
    .locals 4
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 2
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->output:Ljava/io/OutputStream;

    iget-object v1, p0, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->formatDecimal:Ljava/text/NumberFormat;

    float-to-double v2, p1

    invoke-virtual {v1, v2, v3}, Ljava/text/NumberFormat;->format(D)Ljava/lang/String;

    move-result-object p1

    sget-object v1, Lorg/apache/pdfbox/util/Charsets;->US_ASCII:Ljava/nio/charset/Charset;

    invoke-virtual {p1, v1}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/io/OutputStream;->write([B)V

    return-void
.end method

.method public appendRawCommands(I)V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 3
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->output:Ljava/io/OutputStream;

    invoke-virtual {v0, p1}, Ljava/io/OutputStream;->write(I)V

    return-void
.end method

.method public appendRawCommands(Ljava/lang/String;)V
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 4
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->output:Ljava/io/OutputStream;

    sget-object v1, Lorg/apache/pdfbox/util/Charsets;->US_ASCII:Ljava/nio/charset/Charset;

    invoke-virtual {p1, v1}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/io/OutputStream;->write([B)V

    return-void
.end method

.method public appendRawCommands([B)V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 5
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->output:Ljava/io/OutputStream;

    invoke-virtual {v0, p1}, Ljava/io/OutputStream;->write([B)V

    return-void
.end method

.method public beginMarkedContent(Lorg/apache/pdfbox/cos/COSName;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->writeOperand(Lorg/apache/pdfbox/cos/COSName;)V

    const-string p1, "BMC"

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->writeOperator(Ljava/lang/String;)V

    return-void
.end method

.method public beginMarkedContent(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/pdmodel/documentinterchange/markedcontent/PDPropertyList;)V
    .locals 0

    .line 2
    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->writeOperand(Lorg/apache/pdfbox/cos/COSName;)V

    iget-object p1, p0, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->resources:Lorg/apache/pdfbox/pdmodel/PDResources;

    invoke-virtual {p1, p2}, Lorg/apache/pdfbox/pdmodel/PDResources;->add(Lorg/apache/pdfbox/pdmodel/documentinterchange/markedcontent/PDPropertyList;)Lorg/apache/pdfbox/cos/COSName;

    move-result-object p1

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->writeOperand(Lorg/apache/pdfbox/cos/COSName;)V

    const-string p1, "BDC"

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->writeOperator(Ljava/lang/String;)V

    return-void
.end method

.method public beginMarkedContentSequence(Lorg/apache/pdfbox/cos/COSName;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1
    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->beginMarkedContent(Lorg/apache/pdfbox/cos/COSName;)V

    return-void
.end method

.method public beginMarkedContentSequence(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSName;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 2
    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->writeOperand(Lorg/apache/pdfbox/cos/COSName;)V

    invoke-direct {p0, p2}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->writeOperand(Lorg/apache/pdfbox/cos/COSName;)V

    const-string p1, "BDC"

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->writeOperator(Ljava/lang/String;)V

    return-void
.end method

.method public beginText()V
    .locals 2

    iget-boolean v0, p0, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->inTextMode:Z

    if-nez v0, :cond_0

    const-string v0, "BT"

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->writeOperator(Ljava/lang/String;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->inTextMode:Z

    return-void

    :cond_0
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Error: Nested beginText() calls are not allowed."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public clip()V
    .locals 2

    iget-boolean v0, p0, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->inTextMode:Z

    if-nez v0, :cond_0

    const-string v0, "W"

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->writeOperator(Ljava/lang/String;)V

    const-string v0, "n"

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->writeOperator(Ljava/lang/String;)V

    return-void

    :cond_0
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Error: clip is not allowed within a text block."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public clipEvenOdd()V
    .locals 2

    iget-boolean v0, p0, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->inTextMode:Z

    if-nez v0, :cond_0

    const-string v0, "W*"

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->writeOperator(Ljava/lang/String;)V

    const-string v0, "n"

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->writeOperator(Ljava/lang/String;)V

    return-void

    :cond_0
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Error: clipEvenOdd is not allowed within a text block."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public clipPath(Landroid/graphics/Path$FillType;)V
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    iget-boolean v0, p0, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->inTextMode:Z

    if-nez v0, :cond_2

    sget-object v0, Landroid/graphics/Path$FillType;->WINDING:Landroid/graphics/Path$FillType;

    const-string v1, "W"

    if-ne p1, v0, :cond_0

    :goto_0
    invoke-direct {p0, v1}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->writeOperator(Ljava/lang/String;)V

    goto :goto_1

    :cond_0
    sget-object v0, Landroid/graphics/Path$FillType;->EVEN_ODD:Landroid/graphics/Path$FillType;

    if-ne p1, v0, :cond_1

    goto :goto_0

    :goto_1
    const-string p1, "n"

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->writeOperator(Ljava/lang/String;)V

    return-void

    :cond_1
    new-instance p1, Ljava/io/IOException;

    const-string v0, "Error: unknown value for winding rule"

    invoke-direct {p1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_2
    new-instance p1, Ljava/io/IOException;

    const-string v0, "Error: clipPath is not allowed within a text block."

    invoke-direct {p1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public close()V
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->output:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V

    return-void
.end method

.method public closeAndStroke()V
    .locals 2

    iget-boolean v0, p0, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->inTextMode:Z

    if-nez v0, :cond_0

    const-string v0, "s"

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->writeOperator(Ljava/lang/String;)V

    return-void

    :cond_0
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Error: closeAndStroke is not allowed within a text block."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public closePath()V
    .locals 2

    iget-boolean v0, p0, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->inTextMode:Z

    if-nez v0, :cond_0

    const-string v0, "h"

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->writeOperator(Ljava/lang/String;)V

    return-void

    :cond_0
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Error: closePath is not allowed within a text block."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public closeSubPath()V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->closePath()V

    return-void
.end method

.method public concatenate2CTM(DDDDDD)V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1
    new-instance v0, Lorg/apache/pdfbox/util/Matrix;

    double-to-float p2, p1

    double-to-float p3, p3

    double-to-float p4, p5

    double-to-float p5, p7

    double-to-float p6, p9

    double-to-float p7, p11

    move-object p1, v0

    invoke-direct/range {p1 .. p7}, Lorg/apache/pdfbox/util/Matrix;-><init>(FFFFFF)V

    invoke-virtual {p0, v0}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->transform(Lorg/apache/pdfbox/util/Matrix;)V

    return-void
.end method

.method public concatenate2CTM(Lorg/apache/pdfbox/util/awt/AffineTransform;)V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 2
    new-instance v0, Lorg/apache/pdfbox/util/Matrix;

    invoke-direct {v0, p1}, Lorg/apache/pdfbox/util/Matrix;-><init>(Lorg/apache/pdfbox/util/awt/AffineTransform;)V

    invoke-virtual {p0, v0}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->transform(Lorg/apache/pdfbox/util/Matrix;)V

    return-void
.end method

.method public curveTo(FFFFFF)V
    .locals 1

    iget-boolean v0, p0, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->inTextMode:Z

    if-nez v0, :cond_0

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->writeOperand(F)V

    invoke-direct {p0, p2}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->writeOperand(F)V

    invoke-direct {p0, p3}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->writeOperand(F)V

    invoke-direct {p0, p4}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->writeOperand(F)V

    invoke-direct {p0, p5}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->writeOperand(F)V

    invoke-direct {p0, p6}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->writeOperand(F)V

    const-string p1, "c"

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->writeOperator(Ljava/lang/String;)V

    return-void

    :cond_0
    new-instance p1, Ljava/io/IOException;

    const-string p2, "Error: curveTo is not allowed within a text block."

    invoke-direct {p1, p2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public curveTo1(FFFF)V
    .locals 1

    iget-boolean v0, p0, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->inTextMode:Z

    if-nez v0, :cond_0

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->writeOperand(F)V

    invoke-direct {p0, p2}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->writeOperand(F)V

    invoke-direct {p0, p3}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->writeOperand(F)V

    invoke-direct {p0, p4}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->writeOperand(F)V

    const-string p1, "y"

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->writeOperator(Ljava/lang/String;)V

    return-void

    :cond_0
    new-instance p1, Ljava/io/IOException;

    const-string p2, "Error: curveTo1 is not allowed within a text block."

    invoke-direct {p1, p2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public curveTo2(FFFF)V
    .locals 1

    iget-boolean v0, p0, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->inTextMode:Z

    if-nez v0, :cond_0

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->writeOperand(F)V

    invoke-direct {p0, p2}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->writeOperand(F)V

    invoke-direct {p0, p3}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->writeOperand(F)V

    invoke-direct {p0, p4}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->writeOperand(F)V

    const-string p1, "v"

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->writeOperator(Ljava/lang/String;)V

    return-void

    :cond_0
    new-instance p1, Ljava/io/IOException;

    const-string p2, "Error: curveTo2 is not allowed within a text block."

    invoke-direct {p1, p2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public drawForm(Lorg/apache/pdfbox/pdmodel/graphics/form/PDFormXObject;)V
    .locals 1

    iget-boolean v0, p0, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->inTextMode:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->resources:Lorg/apache/pdfbox/pdmodel/PDResources;

    invoke-virtual {v0, p1}, Lorg/apache/pdfbox/pdmodel/PDResources;->add(Lorg/apache/pdfbox/pdmodel/graphics/form/PDFormXObject;)Lorg/apache/pdfbox/cos/COSName;

    move-result-object p1

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->writeOperand(Lorg/apache/pdfbox/cos/COSName;)V

    const-string p1, "Do"

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->writeOperator(Ljava/lang/String;)V

    return-void

    :cond_0
    new-instance p1, Ljava/io/IOException;

    const-string v0, "Error: drawForm is not allowed within a text block."

    invoke-direct {p1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public drawImage(Lorg/apache/pdfbox/pdmodel/graphics/image/PDImageXObject;FF)V
    .locals 7

    .line 1
    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/graphics/image/PDImageXObject;->getWidth()I

    move-result v0

    int-to-float v5, v0

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/graphics/image/PDImageXObject;->getHeight()I

    move-result v0

    int-to-float v6, v0

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move v4, p3

    invoke-virtual/range {v1 .. v6}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->drawImage(Lorg/apache/pdfbox/pdmodel/graphics/image/PDImageXObject;FFFF)V

    return-void
.end method

.method public drawImage(Lorg/apache/pdfbox/pdmodel/graphics/image/PDImageXObject;FFFF)V
    .locals 15

    .line 2
    move-object v0, p0

    iget-boolean v1, v0, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->inTextMode:Z

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->saveGraphicsState()V

    new-instance v1, Lorg/apache/pdfbox/util/awt/AffineTransform;

    move/from16 v2, p4

    float-to-double v3, v2

    const-wide/16 v5, 0x0

    const-wide/16 v7, 0x0

    move/from16 v2, p5

    float-to-double v9, v2

    move/from16 v2, p2

    float-to-double v11, v2

    move/from16 v2, p3

    float-to-double v13, v2

    move-object v2, v1

    invoke-direct/range {v2 .. v14}, Lorg/apache/pdfbox/util/awt/AffineTransform;-><init>(DDDDDD)V

    new-instance v2, Lorg/apache/pdfbox/util/Matrix;

    invoke-direct {v2, v1}, Lorg/apache/pdfbox/util/Matrix;-><init>(Lorg/apache/pdfbox/util/awt/AffineTransform;)V

    invoke-virtual {p0, v2}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->transform(Lorg/apache/pdfbox/util/Matrix;)V

    iget-object v1, v0, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->resources:Lorg/apache/pdfbox/pdmodel/PDResources;

    move-object/from16 v2, p1

    invoke-virtual {v1, v2}, Lorg/apache/pdfbox/pdmodel/PDResources;->add(Lorg/apache/pdfbox/pdmodel/graphics/image/PDImageXObject;)Lorg/apache/pdfbox/cos/COSName;

    move-result-object v1

    invoke-direct {p0, v1}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->writeOperand(Lorg/apache/pdfbox/cos/COSName;)V

    const-string v1, "Do"

    invoke-direct {p0, v1}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->writeOperator(Ljava/lang/String;)V

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->restoreGraphicsState()V

    return-void

    :cond_0
    new-instance v1, Ljava/io/IOException;

    const-string v2, "Error: drawImage is not allowed within a text block."

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public drawLine(FFFF)V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    iget-boolean v0, p0, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->inTextMode:Z

    if-nez v0, :cond_0

    invoke-virtual {p0, p1, p2}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->moveTo(FF)V

    invoke-virtual {p0, p3, p4}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->lineTo(FF)V

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->stroke()V

    return-void

    :cond_0
    new-instance p1, Ljava/io/IOException;

    const-string p2, "Error: drawLine is not allowed within a text block."

    invoke-direct {p1, p2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public drawPolygon([F[F)V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    iget-boolean v0, p0, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->inTextMode:Z

    if-nez v0, :cond_0

    invoke-virtual {p0, p1, p2}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->addPolygon([F[F)V

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->stroke()V

    return-void

    :cond_0
    new-instance p1, Ljava/io/IOException;

    const-string p2, "Error: drawPolygon is not allowed within a text block."

    invoke-direct {p1, p2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public drawString(Ljava/lang/String;)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->showText(Ljava/lang/String;)V

    return-void
.end method

.method public drawXObject(Lorg/apache/pdfbox/pdmodel/graphics/PDXObject;FFFF)V
    .locals 14
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1
    new-instance v13, Lorg/apache/pdfbox/util/awt/AffineTransform;

    move/from16 v0, p4

    float-to-double v1, v0

    const-wide/16 v3, 0x0

    const-wide/16 v5, 0x0

    move/from16 v0, p5

    float-to-double v7, v0

    move/from16 v0, p2

    float-to-double v9, v0

    move/from16 v0, p3

    float-to-double v11, v0

    move-object v0, v13

    invoke-direct/range {v0 .. v12}, Lorg/apache/pdfbox/util/awt/AffineTransform;-><init>(DDDDDD)V

    move-object v0, p0

    move-object v1, p1

    invoke-virtual {p0, p1, v13}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->drawXObject(Lorg/apache/pdfbox/pdmodel/graphics/PDXObject;Lorg/apache/pdfbox/util/awt/AffineTransform;)V

    return-void
.end method

.method public drawXObject(Lorg/apache/pdfbox/pdmodel/graphics/PDXObject;Lorg/apache/pdfbox/util/awt/AffineTransform;)V
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 2
    iget-boolean v0, p0, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->inTextMode:Z

    if-nez v0, :cond_1

    instance-of v0, p1, Lorg/apache/pdfbox/pdmodel/graphics/image/PDImageXObject;

    if-eqz v0, :cond_0

    const-string v0, "Im"

    goto :goto_0

    :cond_0
    const-string v0, "Form"

    :goto_0
    iget-object v1, p0, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->resources:Lorg/apache/pdfbox/pdmodel/PDResources;

    invoke-virtual {v1, p1, v0}, Lorg/apache/pdfbox/pdmodel/PDResources;->add(Lorg/apache/pdfbox/pdmodel/graphics/PDXObject;Ljava/lang/String;)Lorg/apache/pdfbox/cos/COSName;

    move-result-object p1

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->saveGraphicsState()V

    new-instance v0, Lorg/apache/pdfbox/util/Matrix;

    invoke-direct {v0, p2}, Lorg/apache/pdfbox/util/Matrix;-><init>(Lorg/apache/pdfbox/util/awt/AffineTransform;)V

    invoke-virtual {p0, v0}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->transform(Lorg/apache/pdfbox/util/Matrix;)V

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->writeOperand(Lorg/apache/pdfbox/cos/COSName;)V

    const-string p1, "Do"

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->writeOperator(Ljava/lang/String;)V

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->restoreGraphicsState()V

    return-void

    :cond_1
    new-instance p1, Ljava/io/IOException;

    const-string p2, "Error: drawXObject is not allowed within a text block."

    invoke-direct {p1, p2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public endMarkedContent()V
    .locals 1

    const-string v0, "EMC"

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->writeOperator(Ljava/lang/String;)V

    return-void
.end method

.method public endMarkedContentSequence()V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->endMarkedContent()V

    return-void
.end method

.method public endText()V
    .locals 2

    iget-boolean v0, p0, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->inTextMode:Z

    if-eqz v0, :cond_0

    const-string v0, "ET"

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->writeOperator(Ljava/lang/String;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->inTextMode:Z

    return-void

    :cond_0
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Error: You must call beginText() before calling endText."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public fill()V
    .locals 2

    .line 1
    iget-boolean v0, p0, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->inTextMode:Z

    if-nez v0, :cond_0

    const-string v0, "f"

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->writeOperator(Ljava/lang/String;)V

    return-void

    :cond_0
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Error: fill is not allowed within a text block."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public fill(Landroid/graphics/Path$FillType;)V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 2
    sget-object v0, Landroid/graphics/Path$FillType;->WINDING:Landroid/graphics/Path$FillType;

    if-ne p1, v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->fill()V

    goto :goto_0

    :cond_0
    sget-object v0, Landroid/graphics/Path$FillType;->EVEN_ODD:Landroid/graphics/Path$FillType;

    if-ne p1, v0, :cond_1

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->fillEvenOdd()V

    :goto_0
    return-void

    :cond_1
    new-instance p1, Ljava/io/IOException;

    const-string v0, "Error: unknown value for winding rule"

    invoke-direct {p1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public fillEvenOdd()V
    .locals 2

    iget-boolean v0, p0, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->inTextMode:Z

    if-nez v0, :cond_0

    const-string v0, "f*"

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->writeOperator(Ljava/lang/String;)V

    return-void

    :cond_0
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Error: fill is not allowed within a text block."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public fillPolygon([F[F)V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    iget-boolean v0, p0, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->inTextMode:Z

    if-nez v0, :cond_0

    invoke-virtual {p0, p1, p2}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->addPolygon([F[F)V

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->fill()V

    return-void

    :cond_0
    new-instance p1, Ljava/io/IOException;

    const-string p2, "Error: fillPolygon is not allowed within a text block."

    invoke-direct {p1, p2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public fillRect(FFFF)V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    iget-boolean v0, p0, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->inTextMode:Z

    if-nez v0, :cond_0

    invoke-virtual {p0, p1, p2, p3, p4}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->addRect(FFFF)V

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->fill()V

    return-void

    :cond_0
    new-instance p1, Ljava/io/IOException;

    const-string p2, "Error: fillRect is not allowed within a text block."

    invoke-direct {p1, p2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public lineTo(FF)V
    .locals 1

    iget-boolean v0, p0, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->inTextMode:Z

    if-nez v0, :cond_0

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->writeOperand(F)V

    invoke-direct {p0, p2}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->writeOperand(F)V

    const-string p1, "l"

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->writeOperator(Ljava/lang/String;)V

    return-void

    :cond_0
    new-instance p1, Ljava/io/IOException;

    const-string p2, "Error: lineTo is not allowed within a text block."

    invoke-direct {p1, p2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public moveTextPositionByAmount(FF)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    invoke-virtual {p0, p1, p2}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->newLineAtOffset(FF)V

    return-void
.end method

.method public moveTo(FF)V
    .locals 1

    iget-boolean v0, p0, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->inTextMode:Z

    if-nez v0, :cond_0

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->writeOperand(F)V

    invoke-direct {p0, p2}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->writeOperand(F)V

    const-string p1, "m"

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->writeOperator(Ljava/lang/String;)V

    return-void

    :cond_0
    new-instance p1, Ljava/io/IOException;

    const-string p2, "Error: moveTo is not allowed within a text block."

    invoke-direct {p1, p2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public newLine()V
    .locals 2

    iget-boolean v0, p0, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->inTextMode:Z

    if-eqz v0, :cond_0

    const-string v0, "T*"

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->writeOperator(Ljava/lang/String;)V

    return-void

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Must call beginText() before newLine()"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public newLineAtOffset(FF)V
    .locals 1

    iget-boolean v0, p0, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->inTextMode:Z

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->writeOperand(F)V

    invoke-direct {p0, p2}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->writeOperand(F)V

    const-string p1, "Td"

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->writeOperator(Ljava/lang/String;)V

    return-void

    :cond_0
    new-instance p1, Ljava/io/IOException;

    const-string p2, "Error: must call beginText() before newLineAtOffset()"

    invoke-direct {p1, p2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public restoreGraphicsState()V
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->fontStack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/AbstractCollection;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->fontStack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    :cond_0
    const-string v0, "Q"

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->writeOperator(Ljava/lang/String;)V

    return-void
.end method

.method public saveGraphicsState()V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->fontStack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/AbstractCollection;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->fontStack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    const-string v0, "q"

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->writeOperator(Ljava/lang/String;)V

    return-void
.end method

.method public setFont(Lorg/apache/pdfbox/pdmodel/font/PDFont;F)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->fontStack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/AbstractCollection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->fontStack:Ljava/util/Stack;

    invoke-virtual {v0, p1}, Ljava/util/AbstractCollection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->fontStack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/AbstractCollection;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, p1, v1}, Ljava/util/Vector;->setElementAt(Ljava/lang/Object;I)V

    :goto_0
    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/font/PDFont;->willBeSubset()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->document:Lorg/apache/pdfbox/pdmodel/PDDocument;

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/PDDocument;->getFontsToSubset()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->document:Lorg/apache/pdfbox/pdmodel/PDDocument;

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/PDDocument;->getFontsToSubset()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_1
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->resources:Lorg/apache/pdfbox/pdmodel/PDResources;

    invoke-virtual {v0, p1}, Lorg/apache/pdfbox/pdmodel/PDResources;->add(Lorg/apache/pdfbox/pdmodel/font/PDFont;)Lorg/apache/pdfbox/cos/COSName;

    move-result-object p1

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->writeOperand(Lorg/apache/pdfbox/cos/COSName;)V

    invoke-direct {p0, p2}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->writeOperand(F)V

    const-string p1, "Tf"

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->writeOperator(Ljava/lang/String;)V

    return-void
.end method

.method public setLeading(D)V
    .locals 0

    double-to-float p1, p1

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->writeOperand(F)V

    const-string p1, "TL"

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->writeOperator(Ljava/lang/String;)V

    return-void
.end method

.method public setLineCapStyle(I)V
    .locals 1

    iget-boolean v0, p0, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->inTextMode:Z

    if-nez v0, :cond_1

    if-ltz p1, :cond_0

    const/4 v0, 0x2

    if-gt p1, v0, :cond_0

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->writeOperand(I)V

    const-string p1, "J"

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->writeOperator(Ljava/lang/String;)V

    return-void

    :cond_0
    new-instance p1, Ljava/io/IOException;

    const-string v0, "Error: unknown value for line cap style"

    invoke-direct {p1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    new-instance p1, Ljava/io/IOException;

    const-string v0, "Error: setLineCapStyle is not allowed within a text block."

    invoke-direct {p1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public setLineDashPattern([FF)V
    .locals 3

    iget-boolean v0, p0, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->inTextMode:Z

    if-nez v0, :cond_1

    const-string v0, "["

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->write(Ljava/lang/String;)V

    array-length v0, p1

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    aget v2, p1, v1

    invoke-direct {p0, v2}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->writeOperand(F)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    const-string p1, "] "

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->write(Ljava/lang/String;)V

    invoke-direct {p0, p2}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->writeOperand(F)V

    const-string p1, "d"

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->writeOperator(Ljava/lang/String;)V

    return-void

    :cond_1
    new-instance p1, Ljava/io/IOException;

    const-string p2, "Error: setLineDashPattern is not allowed within a text block."

    invoke-direct {p1, p2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public setLineJoinStyle(I)V
    .locals 1

    iget-boolean v0, p0, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->inTextMode:Z

    if-nez v0, :cond_1

    if-ltz p1, :cond_0

    const/4 v0, 0x2

    if-gt p1, v0, :cond_0

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->writeOperand(I)V

    const-string p1, "j"

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->writeOperator(Ljava/lang/String;)V

    return-void

    :cond_0
    new-instance p1, Ljava/io/IOException;

    const-string v0, "Error: unknown value for line join style"

    invoke-direct {p1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    new-instance p1, Ljava/io/IOException;

    const-string v0, "Error: setLineJoinStyle is not allowed within a text block."

    invoke-direct {p1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public setLineWidth(F)V
    .locals 1

    iget-boolean v0, p0, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->inTextMode:Z

    if-nez v0, :cond_0

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->writeOperand(F)V

    const-string p1, "w"

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->writeOperator(Ljava/lang/String;)V

    return-void

    :cond_0
    new-instance p1, Ljava/io/IOException;

    const-string v0, "Error: setLineWidth is not allowed within a text block."

    invoke-direct {p1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public setNonStrokingColor(D)V
    .locals 0

    .line 1
    double-to-float p1, p1

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->writeOperand(F)V

    const-string p1, "g"

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->writeOperator(Ljava/lang/String;)V

    return-void
.end method

.method public setNonStrokingColor(DDDD)V
    .locals 0

    .line 2
    double-to-float p1, p1

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->writeOperand(F)V

    double-to-float p1, p3

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->writeOperand(F)V

    double-to-float p1, p5

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->writeOperand(F)V

    double-to-float p1, p7

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->writeOperand(F)V

    const-string p1, "k"

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->writeOperator(Ljava/lang/String;)V

    return-void
.end method

.method public setNonStrokingColor(I)V
    .locals 2

    .line 3
    int-to-float p1, p1

    const/high16 v0, 0x437f0000    # 255.0f

    div-float/2addr p1, v0

    float-to-double v0, p1

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->setNonStrokingColor(D)V

    return-void
.end method

.method public setNonStrokingColor(III)V
    .locals 1

    .line 4
    int-to-float p1, p1

    const/high16 v0, 0x437f0000    # 255.0f

    div-float/2addr p1, v0

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->writeOperand(F)V

    int-to-float p1, p2

    div-float/2addr p1, v0

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->writeOperand(F)V

    int-to-float p1, p3

    div-float/2addr p1, v0

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->writeOperand(F)V

    const-string p1, "rg"

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->writeOperator(Ljava/lang/String;)V

    return-void
.end method

.method public setNonStrokingColor(IIII)V
    .locals 10

    .line 5
    int-to-float p1, p1

    const/high16 v0, 0x437f0000    # 255.0f

    div-float/2addr p1, v0

    float-to-double v2, p1

    int-to-float p1, p2

    div-float/2addr p1, v0

    float-to-double v4, p1

    int-to-float p1, p3

    div-float/2addr p1, v0

    float-to-double v6, p1

    int-to-float p1, p4

    div-float/2addr p1, v0

    float-to-double v8, p1

    move-object v1, p0

    invoke-virtual/range {v1 .. v9}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->setNonStrokingColor(DDDD)V

    return-void
.end method

.method public setNonStrokingColor(Lorg/apache/pdfbox/pdmodel/graphics/color/PDColor;)V
    .locals 3

    .line 6
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->nonStrokingColorSpaceStack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/AbstractCollection;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->nonStrokingColorSpaceStack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/graphics/color/PDColor;->getColorSpace()Lorg/apache/pdfbox/pdmodel/graphics/color/PDColorSpace;

    move-result-object v1

    if-eq v0, v1, :cond_2

    :cond_0
    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/graphics/color/PDColor;->getColorSpace()Lorg/apache/pdfbox/pdmodel/graphics/color/PDColorSpace;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->getName(Lorg/apache/pdfbox/pdmodel/graphics/color/PDColorSpace;)Lorg/apache/pdfbox/cos/COSName;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->writeOperand(Lorg/apache/pdfbox/cos/COSName;)V

    const-string v0, "cs"

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->writeOperator(Ljava/lang/String;)V

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->nonStrokingColorSpaceStack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/AbstractCollection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->nonStrokingColorSpaceStack:Ljava/util/Stack;

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/graphics/color/PDColor;->getColorSpace()Lorg/apache/pdfbox/pdmodel/graphics/color/PDColorSpace;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/AbstractCollection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->nonStrokingColorSpaceStack:Ljava/util/Stack;

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/graphics/color/PDColor;->getColorSpace()Lorg/apache/pdfbox/pdmodel/graphics/color/PDColorSpace;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->nonStrokingColorSpaceStack:Ljava/util/Stack;

    invoke-virtual {v2}, Ljava/util/AbstractCollection;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/Vector;->setElementAt(Ljava/lang/Object;I)V

    :cond_2
    :goto_0
    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/graphics/color/PDColor;->getComponents()[F

    move-result-object p1

    array-length v0, p1

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v0, :cond_3

    aget v2, p1, v1

    invoke-direct {p0, v2}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->writeOperand(F)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_3
    const-string p1, "sc"

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->writeOperator(Ljava/lang/String;)V

    return-void
.end method

.method public setNonStrokingColor(Lorg/apache/pdfbox/util/awt/AWTColor;)V
    .locals 4

    .line 7
    const/4 v0, 0x3

    new-array v0, v0, [F

    invoke-virtual {p1}, Lorg/apache/pdfbox/util/awt/AWTColor;->getRed()I

    move-result v1

    int-to-float v1, v1

    const/high16 v2, 0x437f0000    # 255.0f

    div-float/2addr v1, v2

    const/4 v3, 0x0

    aput v1, v0, v3

    invoke-virtual {p1}, Lorg/apache/pdfbox/util/awt/AWTColor;->getGreen()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v1, v2

    const/4 v3, 0x1

    aput v1, v0, v3

    invoke-virtual {p1}, Lorg/apache/pdfbox/util/awt/AWTColor;->getBlue()I

    move-result p1

    int-to-float p1, p1

    div-float/2addr p1, v2

    const/4 v1, 0x2

    aput p1, v0, v1

    new-instance p1, Lorg/apache/pdfbox/pdmodel/graphics/color/PDColor;

    sget-object v1, Lorg/apache/pdfbox/pdmodel/graphics/color/PDDeviceRGB;->INSTANCE:Lorg/apache/pdfbox/pdmodel/graphics/color/PDDeviceRGB;

    invoke-direct {p1, v0, v1}, Lorg/apache/pdfbox/pdmodel/graphics/color/PDColor;-><init>([FLorg/apache/pdfbox/pdmodel/graphics/color/PDColorSpace;)V

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->setNonStrokingColor(Lorg/apache/pdfbox/pdmodel/graphics/color/PDColor;)V

    return-void
.end method

.method public setNonStrokingColor([F)V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 8
    iget-object p1, p0, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->nonStrokingColorSpaceStack:Ljava/util/Stack;

    invoke-virtual {p1}, Ljava/util/AbstractCollection;->isEmpty()Z

    move-result p1

    if-nez p1, :cond_0

    iget-object p1, p0, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->nonStrokingColorSpaceStack:Ljava/util/Stack;

    invoke-virtual {p1}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lorg/apache/pdfbox/pdmodel/graphics/color/PDColorSpace;

    const-string p1, "sc"

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->writeOperator(Ljava/lang/String;)V

    return-void

    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "The color space must be set before setting a color"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public setNonStrokingColorSpace(Lorg/apache/pdfbox/pdmodel/graphics/color/PDColorSpace;)V
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->nonStrokingColorSpaceStack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/AbstractCollection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->nonStrokingColorSpaceStack:Ljava/util/Stack;

    invoke-virtual {v0, p1}, Ljava/util/AbstractCollection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->nonStrokingColorSpaceStack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/AbstractCollection;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, p1, v1}, Ljava/util/Vector;->setElementAt(Ljava/lang/Object;I)V

    :goto_0
    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->getName(Lorg/apache/pdfbox/pdmodel/graphics/color/PDColorSpace;)Lorg/apache/pdfbox/cos/COSName;

    move-result-object p1

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->writeOperand(Lorg/apache/pdfbox/cos/COSName;)V

    const-string p1, "cs"

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->writeOperator(Ljava/lang/String;)V

    return-void
.end method

.method public setStrokingColor(D)V
    .locals 0

    .line 1
    double-to-float p1, p1

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->writeOperand(F)V

    const-string p1, "G"

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->writeOperator(Ljava/lang/String;)V

    return-void
.end method

.method public setStrokingColor(FFFF)V
    .locals 0

    .line 2
    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->writeOperand(F)V

    invoke-direct {p0, p2}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->writeOperand(F)V

    invoke-direct {p0, p3}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->writeOperand(F)V

    invoke-direct {p0, p4}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->writeOperand(F)V

    const-string p1, "K"

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->writeOperator(Ljava/lang/String;)V

    return-void
.end method

.method public setStrokingColor(I)V
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 3
    int-to-float p1, p1

    const/high16 v0, 0x437f0000    # 255.0f

    div-float/2addr p1, v0

    float-to-double v0, p1

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->setStrokingColor(D)V

    return-void
.end method

.method public setStrokingColor(III)V
    .locals 1

    .line 4
    int-to-float p1, p1

    const/high16 v0, 0x437f0000    # 255.0f

    div-float/2addr p1, v0

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->writeOperand(F)V

    int-to-float p1, p2

    div-float/2addr p1, v0

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->writeOperand(F)V

    int-to-float p1, p3

    div-float/2addr p1, v0

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->writeOperand(F)V

    const-string p1, "RG"

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->writeOperator(Ljava/lang/String;)V

    return-void
.end method

.method public setStrokingColor(IIII)V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 5
    int-to-float p1, p1

    const/high16 v0, 0x437f0000    # 255.0f

    div-float/2addr p1, v0

    int-to-float p2, p2

    div-float/2addr p2, v0

    int-to-float p3, p3

    div-float/2addr p3, v0

    int-to-float p4, p4

    div-float/2addr p4, v0

    invoke-virtual {p0, p1, p2, p3, p4}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->setStrokingColor(FFFF)V

    return-void
.end method

.method public setStrokingColor(Lorg/apache/pdfbox/pdmodel/graphics/color/PDColor;)V
    .locals 3

    .line 6
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->strokingColorSpaceStack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/AbstractCollection;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->strokingColorSpaceStack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/graphics/color/PDColor;->getColorSpace()Lorg/apache/pdfbox/pdmodel/graphics/color/PDColorSpace;

    move-result-object v1

    if-eq v0, v1, :cond_2

    :cond_0
    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/graphics/color/PDColor;->getColorSpace()Lorg/apache/pdfbox/pdmodel/graphics/color/PDColorSpace;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->getName(Lorg/apache/pdfbox/pdmodel/graphics/color/PDColorSpace;)Lorg/apache/pdfbox/cos/COSName;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->writeOperand(Lorg/apache/pdfbox/cos/COSName;)V

    const-string v0, "CS"

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->writeOperator(Ljava/lang/String;)V

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->strokingColorSpaceStack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/AbstractCollection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->strokingColorSpaceStack:Ljava/util/Stack;

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/graphics/color/PDColor;->getColorSpace()Lorg/apache/pdfbox/pdmodel/graphics/color/PDColorSpace;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/AbstractCollection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->strokingColorSpaceStack:Ljava/util/Stack;

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/graphics/color/PDColor;->getColorSpace()Lorg/apache/pdfbox/pdmodel/graphics/color/PDColorSpace;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->nonStrokingColorSpaceStack:Ljava/util/Stack;

    invoke-virtual {v2}, Ljava/util/AbstractCollection;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/Vector;->setElementAt(Ljava/lang/Object;I)V

    :cond_2
    :goto_0
    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/graphics/color/PDColor;->getComponents()[F

    move-result-object p1

    array-length v0, p1

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v0, :cond_3

    aget v2, p1, v1

    invoke-direct {p0, v2}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->writeOperand(F)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_3
    const-string p1, "SC"

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->writeOperator(Ljava/lang/String;)V

    return-void
.end method

.method public setStrokingColor(Lorg/apache/pdfbox/util/awt/AWTColor;)V
    .locals 4

    .line 7
    const/4 v0, 0x3

    new-array v0, v0, [F

    invoke-virtual {p1}, Lorg/apache/pdfbox/util/awt/AWTColor;->getRed()I

    move-result v1

    int-to-float v1, v1

    const/high16 v2, 0x437f0000    # 255.0f

    div-float/2addr v1, v2

    const/4 v3, 0x0

    aput v1, v0, v3

    invoke-virtual {p1}, Lorg/apache/pdfbox/util/awt/AWTColor;->getGreen()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v1, v2

    const/4 v3, 0x1

    aput v1, v0, v3

    invoke-virtual {p1}, Lorg/apache/pdfbox/util/awt/AWTColor;->getBlue()I

    move-result p1

    int-to-float p1, p1

    div-float/2addr p1, v2

    const/4 v1, 0x2

    aput p1, v0, v1

    new-instance p1, Lorg/apache/pdfbox/pdmodel/graphics/color/PDColor;

    sget-object v1, Lorg/apache/pdfbox/pdmodel/graphics/color/PDDeviceRGB;->INSTANCE:Lorg/apache/pdfbox/pdmodel/graphics/color/PDDeviceRGB;

    invoke-direct {p1, v0, v1}, Lorg/apache/pdfbox/pdmodel/graphics/color/PDColor;-><init>([FLorg/apache/pdfbox/pdmodel/graphics/color/PDColorSpace;)V

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->setStrokingColor(Lorg/apache/pdfbox/pdmodel/graphics/color/PDColor;)V

    return-void
.end method

.method public setStrokingColor([F)V
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 8
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->strokingColorSpaceStack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/AbstractCollection;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_0

    aget v1, p1, v0

    invoke-direct {p0, v1}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->writeOperand(F)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->strokingColorSpaceStack:Ljava/util/Stack;

    invoke-virtual {p1}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lorg/apache/pdfbox/pdmodel/graphics/color/PDColorSpace;

    const-string p1, "SC"

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->writeOperator(Ljava/lang/String;)V

    return-void

    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "The color space must be set before setting a color"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public setStrokingColorSpace(Lorg/apache/pdfbox/pdmodel/graphics/color/PDColorSpace;)V
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->strokingColorSpaceStack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/AbstractCollection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->strokingColorSpaceStack:Ljava/util/Stack;

    invoke-virtual {v0, p1}, Ljava/util/AbstractCollection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->strokingColorSpaceStack:Ljava/util/Stack;

    iget-object v1, p0, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->nonStrokingColorSpaceStack:Ljava/util/Stack;

    invoke-virtual {v1}, Ljava/util/AbstractCollection;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, p1, v1}, Ljava/util/Vector;->setElementAt(Ljava/lang/Object;I)V

    :goto_0
    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->getName(Lorg/apache/pdfbox/pdmodel/graphics/color/PDColorSpace;)Lorg/apache/pdfbox/cos/COSName;

    move-result-object p1

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->writeOperand(Lorg/apache/pdfbox/cos/COSName;)V

    const-string p1, "CS"

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->writeOperator(Ljava/lang/String;)V

    return-void
.end method

.method public setTextMatrix(DDDDDD)V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1
    new-instance v0, Lorg/apache/pdfbox/util/Matrix;

    double-to-float p2, p1

    double-to-float p3, p3

    double-to-float p4, p5

    double-to-float p5, p7

    double-to-float p6, p9

    double-to-float p7, p11

    move-object p1, v0

    invoke-direct/range {p1 .. p7}, Lorg/apache/pdfbox/util/Matrix;-><init>(FFFFFF)V

    invoke-virtual {p0, v0}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->setTextMatrix(Lorg/apache/pdfbox/util/Matrix;)V

    return-void
.end method

.method public setTextMatrix(Lorg/apache/pdfbox/util/Matrix;)V
    .locals 1

    .line 2
    iget-boolean v0, p0, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->inTextMode:Z

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lorg/apache/pdfbox/util/Matrix;->createAffineTransform()Lorg/apache/pdfbox/util/awt/AffineTransform;

    move-result-object p1

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->writeAffineTransform(Lorg/apache/pdfbox/util/awt/AffineTransform;)V

    const-string p1, "Tm"

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->writeOperator(Ljava/lang/String;)V

    return-void

    :cond_0
    new-instance p1, Ljava/io/IOException;

    const-string v0, "Error: must call beginText() before setTextMatrix"

    invoke-direct {p1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public setTextMatrix(Lorg/apache/pdfbox/util/awt/AffineTransform;)V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 3
    new-instance v0, Lorg/apache/pdfbox/util/Matrix;

    invoke-direct {v0, p1}, Lorg/apache/pdfbox/util/Matrix;-><init>(Lorg/apache/pdfbox/util/awt/AffineTransform;)V

    invoke-virtual {p0, v0}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->setTextMatrix(Lorg/apache/pdfbox/util/Matrix;)V

    return-void
.end method

.method public setTextRotation(DDD)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    double-to-float p3, p3

    double-to-float p4, p5

    invoke-static {p1, p2, p3, p4}, Lorg/apache/pdfbox/util/Matrix;->getRotateInstance(DFF)Lorg/apache/pdfbox/util/Matrix;

    move-result-object p1

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->setTextMatrix(Lorg/apache/pdfbox/util/Matrix;)V

    return-void
.end method

.method public setTextScaling(DDDD)V
    .locals 4
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    new-instance v0, Lorg/apache/pdfbox/util/Matrix;

    double-to-float p2, p1

    const/4 v1, 0x0

    const/4 v2, 0x0

    double-to-float v3, p3

    double-to-float p6, p5

    double-to-float p7, p7

    move-object p1, v0

    move p3, v1

    move p4, v2

    move p5, v3

    invoke-direct/range {p1 .. p7}, Lorg/apache/pdfbox/util/Matrix;-><init>(FFFFFF)V

    invoke-virtual {p0, v0}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->setTextMatrix(Lorg/apache/pdfbox/util/Matrix;)V

    return-void
.end method

.method public setTextTranslation(DD)V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    double-to-float p1, p1

    double-to-float p2, p3

    invoke-static {p1, p2}, Lorg/apache/pdfbox/util/Matrix;->getTranslateInstance(FF)Lorg/apache/pdfbox/util/Matrix;

    move-result-object p1

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->setTextMatrix(Lorg/apache/pdfbox/util/Matrix;)V

    return-void
.end method

.method public shadingFill(Lorg/apache/pdfbox/pdmodel/graphics/shading/PDShading;)V
    .locals 1

    iget-boolean v0, p0, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->inTextMode:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->resources:Lorg/apache/pdfbox/pdmodel/PDResources;

    invoke-virtual {v0, p1}, Lorg/apache/pdfbox/pdmodel/PDResources;->add(Lorg/apache/pdfbox/pdmodel/graphics/shading/PDShading;)Lorg/apache/pdfbox/cos/COSName;

    move-result-object p1

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->writeOperand(Lorg/apache/pdfbox/cos/COSName;)V

    const-string p1, "sh"

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->writeOperator(Ljava/lang/String;)V

    return-void

    :cond_0
    new-instance p1, Ljava/io/IOException;

    const-string v0, "Error: shadingFill is not allowed within a text block."

    invoke-direct {p1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public showText(Ljava/lang/String;)V
    .locals 3

    iget-boolean v0, p0, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->inTextMode:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->fontStack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/AbstractCollection;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->fontStack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/pdmodel/font/PDFont;

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/font/PDFont;->willBeSubset()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v1, v2, :cond_0

    invoke-virtual {p1, v1}, Ljava/lang/String;->codePointAt(I)I

    move-result v2

    invoke-virtual {v0, v2}, Lorg/apache/pdfbox/pdmodel/font/PDFont;->addToSubset(I)V

    invoke-static {v2}, Ljava/lang/Character;->charCount(I)I

    move-result v2

    add-int/2addr v1, v2

    goto :goto_0

    :cond_0
    invoke-virtual {v0, p1}, Lorg/apache/pdfbox/pdmodel/font/PDFont;->encode(Ljava/lang/String;)[B

    move-result-object p1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->output:Ljava/io/OutputStream;

    invoke-static {p1, v0}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->writeString([BLjava/io/OutputStream;)V

    const-string p1, " "

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->write(Ljava/lang/String;)V

    const-string p1, "Tj"

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->writeOperator(Ljava/lang/String;)V

    return-void

    :cond_1
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Must call setFont() before showText()"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_2
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Must call beginText() before showText()"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public stroke()V
    .locals 2

    iget-boolean v0, p0, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->inTextMode:Z

    if-nez v0, :cond_0

    const-string v0, "S"

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->writeOperator(Ljava/lang/String;)V

    return-void

    :cond_0
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Error: stroke is not allowed within a text block."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public transform(Lorg/apache/pdfbox/util/Matrix;)V
    .locals 0

    invoke-virtual {p1}, Lorg/apache/pdfbox/util/Matrix;->createAffineTransform()Lorg/apache/pdfbox/util/awt/AffineTransform;

    move-result-object p1

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->writeAffineTransform(Lorg/apache/pdfbox/util/awt/AffineTransform;)V

    const-string p1, "cm"

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->writeOperator(Ljava/lang/String;)V

    return-void
.end method

.class public final Lorg/apache/pdfbox/pdmodel/encryption/PublicKeySecurityHandler;
.super Lorg/apache/pdfbox/pdmodel/encryption/SecurityHandler;
.source "SourceFile"


# static fields
.field public static final FILTER:Ljava/lang/String; = "Adobe.PubSec"

.field private static final SUBFILTER:Ljava/lang/String; = "adbe.pkcs7.s4"


# instance fields
.field private policy:Lorg/apache/pdfbox/pdmodel/encryption/PublicKeyProtectionPolicy;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/encryption/SecurityHandler;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/encryption/PublicKeySecurityHandler;->policy:Lorg/apache/pdfbox/pdmodel/encryption/PublicKeyProtectionPolicy;

    return-void
.end method

.method public constructor <init>(Lorg/apache/pdfbox/pdmodel/encryption/PublicKeyProtectionPolicy;)V
    .locals 0

    .line 2
    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/encryption/SecurityHandler;-><init>()V

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/encryption/PublicKeySecurityHandler;->policy:Lorg/apache/pdfbox/pdmodel/encryption/PublicKeyProtectionPolicy;

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/encryption/ProtectionPolicy;->getEncryptionKeyLength()I

    move-result p1

    iput p1, p0, Lorg/apache/pdfbox/pdmodel/encryption/SecurityHandler;->keyLength:I

    return-void
.end method

.method private appendCertInfo(Ljava/lang/StringBuilder;Lorg/spongycastle/cms/KeyTransRecipientId;Ljava/security/cert/X509Certificate;Lorg/spongycastle/cert/X509CertificateHolder;)V
    .locals 3

    invoke-virtual {p2}, Lorg/spongycastle/cms/KeyTransRecipientId;->getSerialNumber()Ljava/math/BigInteger;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p3}, Ljava/security/cert/X509Certificate;->getSerialNumber()Ljava/math/BigInteger;

    move-result-object p3

    const/16 v1, 0x10

    if-eqz p3, :cond_0

    invoke-virtual {p3, v1}, Ljava/math/BigInteger;->toString(I)Ljava/lang/String;

    move-result-object p3

    goto :goto_0

    :cond_0
    const-string p3, "unknown"

    :goto_0
    const-string v2, "serial-#: rid "

    invoke-virtual {p1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/math/BigInteger;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " vs. cert "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p3, " issuer: rid \'"

    invoke-virtual {p1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Lorg/spongycastle/cms/KeyTransRecipientId;->getIssuer()Lorg/spongycastle/asn1/x500/X500Name;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p2, "\' vs. cert \'"

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-nez p4, :cond_1

    const-string p2, "null"

    goto :goto_1

    :cond_1
    invoke-virtual {p4}, Lorg/spongycastle/cert/X509CertificateHolder;->getIssuer()Lorg/spongycastle/asn1/x500/X500Name;

    move-result-object p2

    :goto_1
    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string p2, "\' "

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    return-void
.end method

.method private computeRecipientInfo(Ljava/security/cert/X509Certificate;[B)Lorg/spongycastle/asn1/cms/KeyTransRecipientInfo;
    .locals 5

    const-string v0, "Could not find a suitable javax.crypto provider"

    new-instance v1, Lorg/spongycastle/asn1/ASN1InputStream;

    invoke-virtual {p1}, Ljava/security/cert/X509Certificate;->getTBSCertificate()[B

    move-result-object v2

    invoke-direct {v1, v2}, Lorg/spongycastle/asn1/ASN1InputStream;-><init>([B)V

    invoke-virtual {v1}, Lorg/spongycastle/asn1/ASN1InputStream;->readObject()Lorg/spongycastle/asn1/ASN1Primitive;

    move-result-object v2

    invoke-static {v2}, Lorg/spongycastle/asn1/x509/TBSCertificateStructure;->getInstance(Ljava/lang/Object;)Lorg/spongycastle/asn1/x509/TBSCertificateStructure;

    move-result-object v2

    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    invoke-virtual {v2}, Lorg/spongycastle/asn1/x509/TBSCertificateStructure;->getSubjectPublicKeyInfo()Lorg/spongycastle/asn1/x509/SubjectPublicKeyInfo;

    move-result-object v1

    invoke-virtual {v1}, Lorg/spongycastle/asn1/x509/SubjectPublicKeyInfo;->getAlgorithm()Lorg/spongycastle/asn1/x509/AlgorithmIdentifier;

    move-result-object v1

    new-instance v3, Lorg/spongycastle/asn1/cms/IssuerAndSerialNumber;

    invoke-virtual {v2}, Lorg/spongycastle/asn1/x509/TBSCertificateStructure;->getIssuer()Lorg/spongycastle/asn1/x500/X500Name;

    move-result-object v4

    invoke-virtual {v2}, Lorg/spongycastle/asn1/x509/TBSCertificateStructure;->getSerialNumber()Lorg/spongycastle/asn1/ASN1Integer;

    move-result-object v2

    invoke-virtual {v2}, Lorg/spongycastle/asn1/ASN1Integer;->getValue()Ljava/math/BigInteger;

    move-result-object v2

    invoke-direct {v3, v4, v2}, Lorg/spongycastle/asn1/cms/IssuerAndSerialNumber;-><init>(Lorg/spongycastle/asn1/x500/X500Name;Ljava/math/BigInteger;)V

    :try_start_0
    invoke-virtual {v1}, Lorg/spongycastle/asn1/x509/AlgorithmIdentifier;->getAlgorithm()Lorg/spongycastle/asn1/ASN1ObjectIdentifier;

    move-result-object v2

    invoke-virtual {v2}, Lorg/spongycastle/asn1/ASN1ObjectIdentifier;->getId()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v0
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljavax/crypto/NoSuchPaddingException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v2, 0x1

    invoke-virtual {p1}, Ljava/security/cert/Certificate;->getPublicKey()Ljava/security/PublicKey;

    move-result-object p1

    invoke-virtual {v0, v2, p1}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;)V

    new-instance p1, Lorg/spongycastle/asn1/DEROctetString;

    invoke-virtual {v0, p2}, Ljavax/crypto/Cipher;->doFinal([B)[B

    move-result-object p2

    invoke-direct {p1, p2}, Lorg/spongycastle/asn1/DEROctetString;-><init>([B)V

    new-instance p2, Lorg/spongycastle/asn1/cms/RecipientIdentifier;

    invoke-direct {p2, v3}, Lorg/spongycastle/asn1/cms/RecipientIdentifier;-><init>(Lorg/spongycastle/asn1/cms/IssuerAndSerialNumber;)V

    new-instance v0, Lorg/spongycastle/asn1/cms/KeyTransRecipientInfo;

    invoke-direct {v0, p2, v1, p1}, Lorg/spongycastle/asn1/cms/KeyTransRecipientInfo;-><init>(Lorg/spongycastle/asn1/cms/RecipientIdentifier;Lorg/spongycastle/asn1/x509/AlgorithmIdentifier;Lorg/spongycastle/asn1/ASN1OctetString;)V

    return-object v0

    :catch_0
    move-exception p1

    new-instance p2, Ljava/lang/RuntimeException;

    invoke-direct {p2, v0, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw p2

    :catch_1
    move-exception p1

    new-instance p2, Ljava/lang/RuntimeException;

    invoke-direct {p2, v0, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw p2
.end method

.method private computeRecipientsField([B)[[B
    .locals 11

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/encryption/PublicKeySecurityHandler;->policy:Lorg/apache/pdfbox/pdmodel/encryption/PublicKeyProtectionPolicy;

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/encryption/PublicKeyProtectionPolicy;->getNumberOfRecipients()I

    move-result v0

    new-array v0, v0, [[B

    iget-object v1, p0, Lorg/apache/pdfbox/pdmodel/encryption/PublicKeySecurityHandler;->policy:Lorg/apache/pdfbox/pdmodel/encryption/PublicKeyProtectionPolicy;

    invoke-virtual {v1}, Lorg/apache/pdfbox/pdmodel/encryption/PublicKeyProtectionPolicy;->getRecipientsIterator()Ljava/util/Iterator;

    move-result-object v1

    const/4 v2, 0x0

    move v3, v2

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/pdfbox/pdmodel/encryption/PublicKeyRecipient;

    invoke-virtual {v4}, Lorg/apache/pdfbox/pdmodel/encryption/PublicKeyRecipient;->getX509()Ljava/security/cert/X509Certificate;

    move-result-object v5

    invoke-virtual {v4}, Lorg/apache/pdfbox/pdmodel/encryption/PublicKeyRecipient;->getPermission()Lorg/apache/pdfbox/pdmodel/encryption/AccessPermission;

    move-result-object v4

    invoke-virtual {v4}, Lorg/apache/pdfbox/pdmodel/encryption/AccessPermission;->getPermissionBytesForPublicKey()I

    move-result v4

    const/16 v6, 0x18

    new-array v7, v6, [B

    int-to-byte v8, v4

    ushr-int/lit8 v9, v4, 0x8

    int-to-byte v9, v9

    ushr-int/lit8 v10, v4, 0x10

    int-to-byte v10, v10

    ushr-int/2addr v4, v6

    int-to-byte v4, v4

    const/16 v6, 0x14

    invoke-static {p1, v2, v7, v2, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    aput-byte v4, v7, v6

    const/16 v4, 0x15

    aput-byte v10, v7, v4

    const/16 v4, 0x16

    aput-byte v9, v7, v4

    const/16 v4, 0x17

    aput-byte v8, v7, v4

    invoke-direct {p0, v7, v5}, Lorg/apache/pdfbox/pdmodel/encryption/PublicKeySecurityHandler;->createDERForRecipient([BLjava/security/cert/X509Certificate;)Lorg/spongycastle/asn1/ASN1Primitive;

    move-result-object v4

    new-instance v5, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v5}, Ljava/io/ByteArrayOutputStream;-><init>()V

    new-instance v6, Lorg/spongycastle/asn1/DEROutputStream;

    invoke-direct {v6, v5}, Lorg/spongycastle/asn1/DEROutputStream;-><init>(Ljava/io/OutputStream;)V

    invoke-virtual {v6, v4}, Lorg/spongycastle/asn1/DEROutputStream;->writeObject(Lorg/spongycastle/asn1/ASN1Encodable;)V

    invoke-virtual {v5}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v4

    aput-object v4, v0, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method private createDERForRecipient([BLjava/security/cert/X509Certificate;)Lorg/spongycastle/asn1/ASN1Primitive;
    .locals 6

    const-string v0, "Could not find a suitable javax.crypto provider"

    const-string v1, "1.2.840.113549.3.2"

    :try_start_0
    invoke-static {v1}, Ljava/security/AlgorithmParameterGenerator;->getInstance(Ljava/lang/String;)Ljava/security/AlgorithmParameterGenerator;

    move-result-object v2

    invoke-static {v1}, Ljavax/crypto/KeyGenerator;->getInstance(Ljava/lang/String;)Ljavax/crypto/KeyGenerator;

    move-result-object v3

    invoke-static {v1}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v0
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljavax/crypto/NoSuchPaddingException; {:try_start_0 .. :try_end_0} :catch_0

    invoke-virtual {v2}, Ljava/security/AlgorithmParameterGenerator;->generateParameters()Ljava/security/AlgorithmParameters;

    move-result-object v2

    new-instance v4, Lorg/spongycastle/asn1/ASN1InputStream;

    const-string v5, "ASN.1"

    invoke-virtual {v2, v5}, Ljava/security/AlgorithmParameters;->getEncoded(Ljava/lang/String;)[B

    move-result-object v5

    invoke-direct {v4, v5}, Lorg/spongycastle/asn1/ASN1InputStream;-><init>([B)V

    invoke-virtual {v4}, Lorg/spongycastle/asn1/ASN1InputStream;->readObject()Lorg/spongycastle/asn1/ASN1Primitive;

    move-result-object v5

    invoke-virtual {v4}, Ljava/io/InputStream;->close()V

    const/16 v4, 0x80

    invoke-virtual {v3, v4}, Ljavax/crypto/KeyGenerator;->init(I)V

    invoke-virtual {v3}, Ljavax/crypto/KeyGenerator;->generateKey()Ljavax/crypto/SecretKey;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v0, v4, v3, v2}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;Ljava/security/AlgorithmParameters;)V

    invoke-virtual {v0, p1}, Ljavax/crypto/Cipher;->doFinal([B)[B

    move-result-object p1

    invoke-interface {v3}, Ljava/security/Key;->getEncoded()[B

    move-result-object v0

    invoke-direct {p0, p2, v0}, Lorg/apache/pdfbox/pdmodel/encryption/PublicKeySecurityHandler;->computeRecipientInfo(Ljava/security/cert/X509Certificate;[B)Lorg/spongycastle/asn1/cms/KeyTransRecipientInfo;

    move-result-object p2

    new-instance v0, Lorg/spongycastle/asn1/DERSet;

    new-instance v2, Lorg/spongycastle/asn1/cms/RecipientInfo;

    invoke-direct {v2, p2}, Lorg/spongycastle/asn1/cms/RecipientInfo;-><init>(Lorg/spongycastle/asn1/cms/KeyTransRecipientInfo;)V

    invoke-direct {v0, v2}, Lorg/spongycastle/asn1/DERSet;-><init>(Lorg/spongycastle/asn1/ASN1Encodable;)V

    new-instance p2, Lorg/spongycastle/asn1/x509/AlgorithmIdentifier;

    new-instance v2, Lorg/spongycastle/asn1/ASN1ObjectIdentifier;

    invoke-direct {v2, v1}, Lorg/spongycastle/asn1/ASN1ObjectIdentifier;-><init>(Ljava/lang/String;)V

    invoke-direct {p2, v2, v5}, Lorg/spongycastle/asn1/x509/AlgorithmIdentifier;-><init>(Lorg/spongycastle/asn1/ASN1ObjectIdentifier;Lorg/spongycastle/asn1/ASN1Encodable;)V

    new-instance v1, Lorg/spongycastle/asn1/cms/EncryptedContentInfo;

    sget-object v2, Lorg/spongycastle/asn1/pkcs/PKCSObjectIdentifiers;->data:Lorg/spongycastle/asn1/ASN1ObjectIdentifier;

    new-instance v3, Lorg/spongycastle/asn1/DEROctetString;

    invoke-direct {v3, p1}, Lorg/spongycastle/asn1/DEROctetString;-><init>([B)V

    invoke-direct {v1, v2, p2, v3}, Lorg/spongycastle/asn1/cms/EncryptedContentInfo;-><init>(Lorg/spongycastle/asn1/ASN1ObjectIdentifier;Lorg/spongycastle/asn1/x509/AlgorithmIdentifier;Lorg/spongycastle/asn1/ASN1OctetString;)V

    new-instance p1, Lorg/spongycastle/asn1/cms/EnvelopedData;

    const/4 p2, 0x0

    invoke-direct {p1, p2, v0, v1, p2}, Lorg/spongycastle/asn1/cms/EnvelopedData;-><init>(Lorg/spongycastle/asn1/cms/OriginatorInfo;Lorg/spongycastle/asn1/ASN1Set;Lorg/spongycastle/asn1/cms/EncryptedContentInfo;Lorg/spongycastle/asn1/ASN1Set;)V

    new-instance p2, Lorg/spongycastle/asn1/cms/ContentInfo;

    sget-object v0, Lorg/spongycastle/asn1/pkcs/PKCSObjectIdentifiers;->envelopedData:Lorg/spongycastle/asn1/ASN1ObjectIdentifier;

    invoke-direct {p2, v0, p1}, Lorg/spongycastle/asn1/cms/ContentInfo;-><init>(Lorg/spongycastle/asn1/ASN1ObjectIdentifier;Lorg/spongycastle/asn1/ASN1Encodable;)V

    invoke-virtual {p2}, Lorg/spongycastle/asn1/cms/ContentInfo;->toASN1Primitive()Lorg/spongycastle/asn1/ASN1Primitive;

    move-result-object p1

    return-object p1

    :catch_0
    move-exception p1

    new-instance p2, Ljava/lang/RuntimeException;

    invoke-direct {p2, v0, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw p2

    :catch_1
    move-exception p1

    new-instance p2, Ljava/lang/RuntimeException;

    invoke-direct {p2, v0, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw p2
.end method


# virtual methods
.method public prepareDocumentForEncryption(Lorg/apache/pdfbox/pdmodel/PDDocument;)V
    .locals 8

    iget v0, p0, Lorg/apache/pdfbox/pdmodel/encryption/SecurityHandler;->keyLength:I

    const/16 v1, 0x100

    if-eq v0, v1, :cond_3

    :try_start_0
    new-instance v0, Lorg/spongycastle/jce/provider/BouncyCastleProvider;

    invoke-direct {v0}, Lorg/spongycastle/jce/provider/BouncyCastleProvider;-><init>()V

    invoke-static {v0}, Ljava/security/Security;->addProvider(Ljava/security/Provider;)I

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/PDDocument;->getEncryption()Lorg/apache/pdfbox/pdmodel/encryption/PDEncryption;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Lorg/apache/pdfbox/pdmodel/encryption/PDEncryption;

    invoke-direct {v0}, Lorg/apache/pdfbox/pdmodel/encryption/PDEncryption;-><init>()V

    :cond_0
    const-string v1, "Adobe.PubSec"

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/pdmodel/encryption/PDEncryption;->setFilter(Ljava/lang/String;)V

    iget v1, p0, Lorg/apache/pdfbox/pdmodel/encryption/SecurityHandler;->keyLength:I

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/pdmodel/encryption/PDEncryption;->setLength(I)V

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/pdmodel/encryption/PDEncryption;->setVersion(I)V

    const-string v1, "adbe.pkcs7.s4"

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/pdmodel/encryption/PDEncryption;->setSubFilter(Ljava/lang/String;)V

    const/16 v1, 0x14

    new-array v2, v1, [B
    :try_end_0
    .catch Ljava/security/GeneralSecurityException; {:try_start_0 .. :try_end_0} :catch_1

    :try_start_1
    const-string v3, "AES"

    invoke-static {v3}, Ljavax/crypto/KeyGenerator;->getInstance(Ljava/lang/String;)Ljavax/crypto/KeyGenerator;

    move-result-object v3
    :try_end_1
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/security/GeneralSecurityException; {:try_start_1 .. :try_end_1} :catch_1

    :try_start_2
    new-instance v4, Ljava/security/SecureRandom;

    invoke-direct {v4}, Ljava/security/SecureRandom;-><init>()V

    const/16 v5, 0xc0

    invoke-virtual {v3, v5, v4}, Ljavax/crypto/KeyGenerator;->init(ILjava/security/SecureRandom;)V

    invoke-virtual {v3}, Ljavax/crypto/KeyGenerator;->generateKey()Ljavax/crypto/SecretKey;

    move-result-object v3

    invoke-interface {v3}, Ljava/security/Key;->getEncoded()[B

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v3, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    invoke-direct {p0, v2}, Lorg/apache/pdfbox/pdmodel/encryption/PublicKeySecurityHandler;->computeRecipientsField([B)[[B

    move-result-object v3

    invoke-virtual {v0, v3}, Lorg/apache/pdfbox/pdmodel/encryption/PDEncryption;->setRecipients([[B)V

    move v5, v1

    move v3, v4

    :goto_0
    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/encryption/PDEncryption;->getRecipientsLength()I

    move-result v6

    if-ge v3, v6, :cond_1

    invoke-virtual {v0, v3}, Lorg/apache/pdfbox/pdmodel/encryption/PDEncryption;->getRecipientStringAt(I)Lorg/apache/pdfbox/cos/COSString;

    move-result-object v6

    invoke-virtual {v6}, Lorg/apache/pdfbox/cos/COSString;->getBytes()[B

    move-result-object v6

    array-length v6, v6

    add-int/2addr v5, v6

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    new-array v3, v5, [B

    invoke-static {v2, v4, v3, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    move v2, v4

    :goto_1
    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/encryption/PDEncryption;->getRecipientsLength()I

    move-result v5

    if-ge v2, v5, :cond_2

    invoke-virtual {v0, v2}, Lorg/apache/pdfbox/pdmodel/encryption/PDEncryption;->getRecipientStringAt(I)Lorg/apache/pdfbox/cos/COSString;

    move-result-object v5

    invoke-virtual {v5}, Lorg/apache/pdfbox/cos/COSString;->getBytes()[B

    move-result-object v6

    invoke-virtual {v5}, Lorg/apache/pdfbox/cos/COSString;->getBytes()[B

    move-result-object v7

    array-length v7, v7

    invoke-static {v6, v4, v3, v1, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    invoke-virtual {v5}, Lorg/apache/pdfbox/cos/COSString;->getBytes()[B

    move-result-object v5

    array-length v5, v5

    add-int/2addr v1, v5

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_2
    invoke-static {}, Lorg/apache/pdfbox/pdmodel/encryption/MessageDigests;->getSHA1()Ljava/security/MessageDigest;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/security/MessageDigest;->digest([B)[B

    move-result-object v1

    iget v2, p0, Lorg/apache/pdfbox/pdmodel/encryption/SecurityHandler;->keyLength:I

    div-int/lit8 v3, v2, 0x8

    new-array v3, v3, [B

    iput-object v3, p0, Lorg/apache/pdfbox/pdmodel/encryption/SecurityHandler;->encryptionKey:[B

    div-int/lit8 v2, v2, 0x8

    invoke-static {v1, v4, v3, v4, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    invoke-virtual {p1, v0}, Lorg/apache/pdfbox/pdmodel/PDDocument;->setEncryptionDictionary(Lorg/apache/pdfbox/pdmodel/encryption/PDEncryption;)V

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/PDDocument;->getDocument()Lorg/apache/pdfbox/cos/COSDocument;

    move-result-object p1

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/encryption/PDEncryption;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    invoke-virtual {p1, v0}, Lorg/apache/pdfbox/cos/COSDocument;->setEncryptionDictionary(Lorg/apache/pdfbox/cos/COSDictionary;)V

    return-void

    :catch_0
    move-exception p1

    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {v0, p1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v0
    :try_end_2
    .catch Ljava/security/GeneralSecurityException; {:try_start_2 .. :try_end_2} :catch_1

    :catch_1
    move-exception p1

    new-instance v0, Ljava/io/IOException;

    invoke-direct {v0, p1}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v0

    :cond_3
    new-instance p1, Ljava/io/IOException;

    const-string v0, "256 bit key length is not supported yet for public key security"

    invoke-direct {p1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public prepareForDecryption(Lorg/apache/pdfbox/pdmodel/encryption/PDEncryption;Lorg/apache/pdfbox/cos/COSArray;Lorg/apache/pdfbox/pdmodel/encryption/DecryptionMaterial;)V
    .locals 18

    move-object/from16 v1, p0

    move-object/from16 v0, p3

    instance-of v2, v0, Lorg/apache/pdfbox/pdmodel/encryption/PublicKeyDecryptionMaterial;

    if-eqz v2, :cond_9

    invoke-virtual/range {p1 .. p1}, Lorg/apache/pdfbox/pdmodel/encryption/PDEncryption;->isEncryptMetaData()Z

    move-result v2

    iput-boolean v2, v1, Lorg/apache/pdfbox/pdmodel/encryption/SecurityHandler;->decryptMetadata:Z

    invoke-virtual/range {p1 .. p1}, Lorg/apache/pdfbox/pdmodel/encryption/PDEncryption;->getLength()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual/range {p1 .. p1}, Lorg/apache/pdfbox/pdmodel/encryption/PDEncryption;->getLength()I

    move-result v2

    iput v2, v1, Lorg/apache/pdfbox/pdmodel/encryption/SecurityHandler;->keyLength:I

    :cond_0
    check-cast v0, Lorg/apache/pdfbox/pdmodel/encryption/PublicKeyDecryptionMaterial;

    :try_start_0
    invoke-virtual/range {p1 .. p1}, Lorg/apache/pdfbox/pdmodel/encryption/PDEncryption;->getRecipientsLength()I

    move-result v2

    new-array v3, v2, [[B

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    :goto_0
    invoke-virtual/range {p1 .. p1}, Lorg/apache/pdfbox/pdmodel/encryption/PDEncryption;->getRecipientsLength()I

    move-result v11

    if-ge v7, v11, :cond_5

    move-object/from16 v11, p1

    invoke-virtual {v11, v7}, Lorg/apache/pdfbox/pdmodel/encryption/PDEncryption;->getRecipientStringAt(I)Lorg/apache/pdfbox/cos/COSString;

    move-result-object v12

    invoke-virtual {v12}, Lorg/apache/pdfbox/cos/COSString;->getBytes()[B

    move-result-object v12

    new-instance v13, Lorg/spongycastle/cms/CMSEnvelopedData;

    invoke-direct {v13, v12}, Lorg/spongycastle/cms/CMSEnvelopedData;-><init>([B)V

    invoke-virtual {v13}, Lorg/spongycastle/cms/CMSEnvelopedData;->getRecipientInfos()Lorg/spongycastle/cms/RecipientInformationStore;

    move-result-object v13

    invoke-virtual {v13}, Lorg/spongycastle/cms/RecipientInformationStore;->getRecipients()Ljava/util/Collection;

    move-result-object v13

    invoke-interface {v13}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v13

    const/4 v14, 0x0

    :goto_1
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v15

    if-eqz v15, :cond_4

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lorg/spongycastle/cms/RecipientInformation;

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/encryption/PublicKeyDecryptionMaterial;->getCertificate()Ljava/security/cert/X509Certificate;

    move-result-object v5

    if-eqz v5, :cond_1

    new-instance v6, Lorg/spongycastle/cert/X509CertificateHolder;

    invoke-virtual {v5}, Ljava/security/cert/Certificate;->getEncoded()[B

    move-result-object v11

    invoke-direct {v6, v11}, Lorg/spongycastle/cert/X509CertificateHolder;-><init>([B)V

    goto :goto_2

    :cond_1
    const/4 v6, 0x0

    :goto_2
    invoke-virtual {v15}, Lorg/spongycastle/cms/RecipientInformation;->getRID()Lorg/spongycastle/cms/RecipientId;

    move-result-object v11

    invoke-interface {v11, v6}, Lorg/spongycastle/util/Selector;->match(Ljava/lang/Object;)Z

    move-result v16

    const/16 v17, 0x1

    if-eqz v16, :cond_2

    if-nez v8, :cond_2

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/encryption/PublicKeyDecryptionMaterial;->getPrivateKey()Ljava/security/Key;

    move-result-object v5

    check-cast v5, Ljava/security/PrivateKey;

    new-instance v6, Lorg/spongycastle/cms/jcajce/JceKeyTransEnvelopedRecipient;

    invoke-direct {v6, v5}, Lorg/spongycastle/cms/jcajce/JceKeyTransEnvelopedRecipient;-><init>(Ljava/security/PrivateKey;)V

    const-string v5, "BC"

    invoke-virtual {v6, v5}, Lorg/spongycastle/cms/jcajce/JceKeyTransRecipient;->setProvider(Ljava/lang/String;)Lorg/spongycastle/cms/jcajce/JceKeyTransRecipient;

    move-result-object v5

    invoke-virtual {v15, v5}, Lorg/spongycastle/cms/RecipientInformation;->getContent(Lorg/spongycastle/cms/Recipient;)[B

    move-result-object v9

    move/from16 v8, v17

    goto :goto_3

    :cond_2
    add-int/lit8 v14, v14, 0x1

    if-eqz v5, :cond_3

    const/16 v15, 0xa

    invoke-virtual {v4, v15}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v15, ": "

    invoke-virtual {v4, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    instance-of v15, v11, Lorg/spongycastle/cms/KeyTransRecipientId;

    if-eqz v15, :cond_3

    check-cast v11, Lorg/spongycastle/cms/KeyTransRecipientId;

    invoke-direct {v1, v4, v11, v5, v6}, Lorg/apache/pdfbox/pdmodel/encryption/PublicKeySecurityHandler;->appendCertInfo(Ljava/lang/StringBuilder;Lorg/spongycastle/cms/KeyTransRecipientId;Ljava/security/cert/X509Certificate;Lorg/spongycastle/cert/X509CertificateHolder;)V

    :cond_3
    move-object/from16 v11, p1

    goto :goto_1

    :cond_4
    :goto_3
    aput-object v12, v3, v7

    array-length v5, v12

    add-int/2addr v10, v5

    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_0

    :cond_5
    if-eqz v8, :cond_8

    if-eqz v9, :cond_8

    array-length v0, v9

    const/16 v4, 0x18

    if-ne v0, v4, :cond_7

    const/4 v0, 0x4

    new-array v4, v0, [B

    const/16 v5, 0x14

    const/4 v6, 0x0

    invoke-static {v9, v5, v4, v6, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    new-instance v0, Lorg/apache/pdfbox/pdmodel/encryption/AccessPermission;

    invoke-direct {v0, v4}, Lorg/apache/pdfbox/pdmodel/encryption/AccessPermission;-><init>([B)V

    iput-object v0, v1, Lorg/apache/pdfbox/pdmodel/encryption/SecurityHandler;->currentAccessPermission:Lorg/apache/pdfbox/pdmodel/encryption/AccessPermission;

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/encryption/AccessPermission;->setReadOnly()V

    add-int/2addr v10, v5

    new-array v0, v10, [B

    const/4 v4, 0x0

    invoke-static {v9, v4, v0, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    move v6, v4

    :goto_4
    if-ge v6, v2, :cond_6

    aget-object v7, v3, v6

    array-length v8, v7

    invoke-static {v7, v4, v0, v5, v8}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    array-length v4, v7

    add-int/2addr v5, v4

    add-int/lit8 v6, v6, 0x1

    const/4 v4, 0x0

    goto :goto_4

    :cond_6
    invoke-static {}, Lorg/apache/pdfbox/pdmodel/encryption/MessageDigests;->getSHA1()Ljava/security/MessageDigest;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/security/MessageDigest;->digest([B)[B

    move-result-object v0

    iget v2, v1, Lorg/apache/pdfbox/pdmodel/encryption/SecurityHandler;->keyLength:I

    div-int/lit8 v3, v2, 0x8

    new-array v3, v3, [B

    iput-object v3, v1, Lorg/apache/pdfbox/pdmodel/encryption/SecurityHandler;->encryptionKey:[B

    div-int/lit8 v2, v2, 0x8

    const/4 v4, 0x0

    invoke-static {v0, v4, v3, v4, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-void

    :cond_7
    new-instance v0, Ljava/io/IOException;

    const-string v2, "The enveloped data does not contain 24 bytes"

    invoke-direct {v0, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_8
    new-instance v0, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "The certificate matches none of "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v3, " recipient entries"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Lorg/spongycastle/cms/CMSException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/security/KeyStoreException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/security/cert/CertificateEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    :catch_0
    move-exception v0

    new-instance v2, Ljava/io/IOException;

    invoke-direct {v2, v0}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v2

    :catch_1
    move-exception v0

    new-instance v2, Ljava/io/IOException;

    invoke-direct {v2, v0}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v2

    :catch_2
    move-exception v0

    new-instance v2, Ljava/io/IOException;

    invoke-direct {v2, v0}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v2

    :cond_9
    new-instance v0, Ljava/io/IOException;

    const-string v2, "Provided decryption material is not compatible with the document"

    invoke-direct {v0, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

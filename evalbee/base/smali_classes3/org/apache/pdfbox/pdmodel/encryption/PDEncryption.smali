.class public Lorg/apache/pdfbox/pdmodel/encryption/PDEncryption;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final DEFAULT_LENGTH:I = 0x28

.field public static final DEFAULT_NAME:Ljava/lang/String; = "Standard"

.field public static final DEFAULT_VERSION:I = 0x0

.field public static final VERSION0_UNDOCUMENTED_UNSUPPORTED:I = 0x0

.field public static final VERSION1_40_BIT_ALGORITHM:I = 0x1

.field public static final VERSION2_VARIABLE_LENGTH_ALGORITHM:I = 0x2

.field public static final VERSION3_UNPUBLISHED_ALGORITHM:I = 0x3

.field public static final VERSION4_SECURITY_HANDLER:I = 0x4


# instance fields
.field private final dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

.field private securityHandler:Lorg/apache/pdfbox/pdmodel/encryption/SecurityHandler;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-direct {v0}, Lorg/apache/pdfbox/cos/COSDictionary;-><init>()V

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/encryption/PDEncryption;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    return-void
.end method

.method public constructor <init>(Lorg/apache/pdfbox/cos/COSDictionary;)V
    .locals 1

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/encryption/PDEncryption;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object p1, Lorg/apache/pdfbox/pdmodel/encryption/SecurityHandlerFactory;->INSTANCE:Lorg/apache/pdfbox/pdmodel/encryption/SecurityHandlerFactory;

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/encryption/PDEncryption;->getFilter()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lorg/apache/pdfbox/pdmodel/encryption/SecurityHandlerFactory;->newSecurityHandlerForFilter(Ljava/lang/String;)Lorg/apache/pdfbox/pdmodel/encryption/SecurityHandler;

    move-result-object p1

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/encryption/PDEncryption;->securityHandler:Lorg/apache/pdfbox/pdmodel/encryption/SecurityHandler;

    return-void
.end method


# virtual methods
.method public getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/encryption/PDEncryption;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    return-object v0
.end method

.method public getCryptFilterDictionary(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/pdmodel/encryption/PDCryptFilterDictionary;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/encryption/PDEncryption;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->CF:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSDictionary;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object p1

    check-cast p1, Lorg/apache/pdfbox/cos/COSDictionary;

    if-eqz p1, :cond_0

    new-instance v0, Lorg/apache/pdfbox/pdmodel/encryption/PDCryptFilterDictionary;

    invoke-direct {v0, p1}, Lorg/apache/pdfbox/pdmodel/encryption/PDCryptFilterDictionary;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    return-object v0

    :cond_0
    const/4 p1, 0x0

    return-object p1
.end method

.method public final getFilter()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/encryption/PDEncryption;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->FILTER:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getNameAsString(Lorg/apache/pdfbox/cos/COSName;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getLength()I
    .locals 3

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/encryption/PDEncryption;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->LENGTH:Lorg/apache/pdfbox/cos/COSName;

    const/16 v2, 0x28

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->getInt(Lorg/apache/pdfbox/cos/COSName;I)I

    move-result v0

    return v0
.end method

.method public getOwnerEncryptionKey()[B
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/encryption/PDEncryption;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->OE:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSString;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSString;->getBytes()[B

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public getOwnerKey()[B
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/encryption/PDEncryption;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->O:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSString;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSString;->getBytes()[B

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public getPermissions()I
    .locals 3

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/encryption/PDEncryption;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->P:Lorg/apache/pdfbox/cos/COSName;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->getInt(Lorg/apache/pdfbox/cos/COSName;I)I

    move-result v0

    return v0
.end method

.method public getPerms()[B
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/encryption/PDEncryption;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->PERMS:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSString;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSString;->getBytes()[B

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public getRecipientStringAt(I)Lorg/apache/pdfbox/cos/COSString;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/encryption/PDEncryption;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->RECIPIENTS:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getItem(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSArray;

    invoke-virtual {v0, p1}, Lorg/apache/pdfbox/cos/COSArray;->get(I)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object p1

    check-cast p1, Lorg/apache/pdfbox/cos/COSString;

    return-object p1
.end method

.method public getRecipientsLength()I
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/encryption/PDEncryption;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->RECIPIENTS:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getItem(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSArray;

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSArray;->size()I

    move-result v0

    return v0
.end method

.method public getRevision()I
    .locals 3

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/encryption/PDEncryption;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->R:Lorg/apache/pdfbox/cos/COSName;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->getInt(Lorg/apache/pdfbox/cos/COSName;I)I

    move-result v0

    return v0
.end method

.method public getSecurityHandler()Lorg/apache/pdfbox/pdmodel/encryption/SecurityHandler;
    .locals 3

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/encryption/PDEncryption;->securityHandler:Lorg/apache/pdfbox/pdmodel/encryption/SecurityHandler;

    if-eqz v0, :cond_0

    return-object v0

    :cond_0
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "No security handler for filter "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/encryption/PDEncryption;->getFilter()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getStdCryptFilterDictionary()Lorg/apache/pdfbox/pdmodel/encryption/PDCryptFilterDictionary;
    .locals 1

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->STD_CF:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p0, v0}, Lorg/apache/pdfbox/pdmodel/encryption/PDEncryption;->getCryptFilterDictionary(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/pdmodel/encryption/PDCryptFilterDictionary;

    move-result-object v0

    return-object v0
.end method

.method public getStreamFilterName()Lorg/apache/pdfbox/cos/COSName;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/encryption/PDEncryption;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->STM_F:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSName;

    if-nez v0, :cond_0

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->IDENTITY:Lorg/apache/pdfbox/cos/COSName;

    :cond_0
    return-object v0
.end method

.method public getStringFilterName()Lorg/apache/pdfbox/cos/COSName;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/encryption/PDEncryption;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->STR_F:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSName;

    if-nez v0, :cond_0

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->IDENTITY:Lorg/apache/pdfbox/cos/COSName;

    :cond_0
    return-object v0
.end method

.method public getSubFilter()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/encryption/PDEncryption;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->SUB_FILTER:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getNameAsString(Lorg/apache/pdfbox/cos/COSName;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getUserEncryptionKey()[B
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/encryption/PDEncryption;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->UE:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSString;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSString;->getBytes()[B

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public getUserKey()[B
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/encryption/PDEncryption;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->U:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSString;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSString;->getBytes()[B

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public getVersion()I
    .locals 3

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/encryption/PDEncryption;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->V:Lorg/apache/pdfbox/cos/COSName;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->getInt(Lorg/apache/pdfbox/cos/COSName;I)I

    move-result v0

    return v0
.end method

.method public hasSecurityHandler()Z
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/encryption/PDEncryption;->securityHandler:Lorg/apache/pdfbox/pdmodel/encryption/SecurityHandler;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isEncryptMetaData()Z
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/encryption/PDEncryption;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->ENCRYPT_META_DATA:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    instance-of v1, v0, Lorg/apache/pdfbox/cos/COSBoolean;

    if-eqz v1, :cond_0

    check-cast v0, Lorg/apache/pdfbox/cos/COSBoolean;

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSBoolean;->getValue()Z

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0
.end method

.method public setCryptFilterDictionary(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/pdmodel/encryption/PDCryptFilterDictionary;)V
    .locals 3

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/encryption/PDEncryption;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->CF:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSDictionary;

    if-nez v0, :cond_0

    new-instance v0, Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-direct {v0}, Lorg/apache/pdfbox/cos/COSDictionary;-><init>()V

    iget-object v2, p0, Lorg/apache/pdfbox/pdmodel/encryption/PDEncryption;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-virtual {v2, v1, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    :cond_0
    invoke-virtual {p2}, Lorg/apache/pdfbox/pdmodel/encryption/PDCryptFilterDictionary;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object p2

    invoke-virtual {v0, p1, p2}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    return-void
.end method

.method public setFilter(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/encryption/PDEncryption;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->FILTER:Lorg/apache/pdfbox/cos/COSName;

    invoke-static {p1}, Lorg/apache/pdfbox/cos/COSName;->getPDFName(Ljava/lang/String;)Lorg/apache/pdfbox/cos/COSName;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    return-void
.end method

.method public setLength(I)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/encryption/PDEncryption;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->LENGTH:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setInt(Lorg/apache/pdfbox/cos/COSName;I)V

    return-void
.end method

.method public setOwnerEncryptionKey([B)V
    .locals 3

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/encryption/PDEncryption;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->OE:Lorg/apache/pdfbox/cos/COSName;

    new-instance v2, Lorg/apache/pdfbox/cos/COSString;

    invoke-direct {v2, p1}, Lorg/apache/pdfbox/cos/COSString;-><init>([B)V

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    return-void
.end method

.method public setOwnerKey([B)V
    .locals 3

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/encryption/PDEncryption;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->O:Lorg/apache/pdfbox/cos/COSName;

    new-instance v2, Lorg/apache/pdfbox/cos/COSString;

    invoke-direct {v2, p1}, Lorg/apache/pdfbox/cos/COSString;-><init>([B)V

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    return-void
.end method

.method public setPermissions(I)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/encryption/PDEncryption;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->P:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setInt(Lorg/apache/pdfbox/cos/COSName;I)V

    return-void
.end method

.method public setPerms([B)V
    .locals 3

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/encryption/PDEncryption;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->PERMS:Lorg/apache/pdfbox/cos/COSName;

    new-instance v2, Lorg/apache/pdfbox/cos/COSString;

    invoke-direct {v2, p1}, Lorg/apache/pdfbox/cos/COSString;-><init>([B)V

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    return-void
.end method

.method public setRecipients([[B)V
    .locals 5

    new-instance v0, Lorg/apache/pdfbox/cos/COSArray;

    invoke-direct {v0}, Lorg/apache/pdfbox/cos/COSArray;-><init>()V

    array-length v1, p1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    aget-object v3, p1, v2

    new-instance v4, Lorg/apache/pdfbox/cos/COSString;

    invoke-direct {v4, v3}, Lorg/apache/pdfbox/cos/COSString;-><init>([B)V

    invoke-virtual {v0, v4}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lorg/apache/pdfbox/pdmodel/encryption/PDEncryption;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->RECIPIENTS:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p1, v1, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    return-void
.end method

.method public setRevision(I)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/encryption/PDEncryption;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->R:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setInt(Lorg/apache/pdfbox/cos/COSName;I)V

    return-void
.end method

.method public setSecurityHandler(Lorg/apache/pdfbox/pdmodel/encryption/SecurityHandler;)V
    .locals 0

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/encryption/PDEncryption;->securityHandler:Lorg/apache/pdfbox/pdmodel/encryption/SecurityHandler;

    return-void
.end method

.method public setStdCryptFilterDictionary(Lorg/apache/pdfbox/pdmodel/encryption/PDCryptFilterDictionary;)V
    .locals 1

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->STD_CF:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p0, v0, p1}, Lorg/apache/pdfbox/pdmodel/encryption/PDEncryption;->setCryptFilterDictionary(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/pdmodel/encryption/PDCryptFilterDictionary;)V

    return-void
.end method

.method public setStreamFilterName(Lorg/apache/pdfbox/cos/COSName;)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/encryption/PDEncryption;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->STM_F:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    return-void
.end method

.method public setStringFilterName(Lorg/apache/pdfbox/cos/COSName;)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/encryption/PDEncryption;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->STR_F:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    return-void
.end method

.method public setSubFilter(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/encryption/PDEncryption;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->SUB_FILTER:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setName(Lorg/apache/pdfbox/cos/COSName;Ljava/lang/String;)V

    return-void
.end method

.method public setUserEncryptionKey([B)V
    .locals 3

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/encryption/PDEncryption;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->UE:Lorg/apache/pdfbox/cos/COSName;

    new-instance v2, Lorg/apache/pdfbox/cos/COSString;

    invoke-direct {v2, p1}, Lorg/apache/pdfbox/cos/COSString;-><init>([B)V

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    return-void
.end method

.method public setUserKey([B)V
    .locals 3

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/encryption/PDEncryption;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->U:Lorg/apache/pdfbox/cos/COSName;

    new-instance v2, Lorg/apache/pdfbox/cos/COSString;

    invoke-direct {v2, p1}, Lorg/apache/pdfbox/cos/COSString;-><init>([B)V

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    return-void
.end method

.method public setVersion(I)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/encryption/PDEncryption;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->V:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setInt(Lorg/apache/pdfbox/cos/COSName;I)V

    return-void
.end method

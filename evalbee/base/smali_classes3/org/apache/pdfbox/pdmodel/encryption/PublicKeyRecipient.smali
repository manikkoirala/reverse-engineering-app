.class public Lorg/apache/pdfbox/pdmodel/encryption/PublicKeyRecipient;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private permission:Lorg/apache/pdfbox/pdmodel/encryption/AccessPermission;

.field private x509:Ljava/security/cert/X509Certificate;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getPermission()Lorg/apache/pdfbox/pdmodel/encryption/AccessPermission;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/encryption/PublicKeyRecipient;->permission:Lorg/apache/pdfbox/pdmodel/encryption/AccessPermission;

    return-object v0
.end method

.method public getX509()Ljava/security/cert/X509Certificate;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/encryption/PublicKeyRecipient;->x509:Ljava/security/cert/X509Certificate;

    return-object v0
.end method

.method public setPermission(Lorg/apache/pdfbox/pdmodel/encryption/AccessPermission;)V
    .locals 0

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/encryption/PublicKeyRecipient;->permission:Lorg/apache/pdfbox/pdmodel/encryption/AccessPermission;

    return-void
.end method

.method public setX509(Ljava/security/cert/X509Certificate;)V
    .locals 0

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/encryption/PublicKeyRecipient;->x509:Ljava/security/cert/X509Certificate;

    return-void
.end method

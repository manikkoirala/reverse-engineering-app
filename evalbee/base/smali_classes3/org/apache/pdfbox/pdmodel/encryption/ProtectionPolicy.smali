.class public abstract Lorg/apache/pdfbox/pdmodel/encryption/ProtectionPolicy;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final DEFAULT_KEY_LENGTH:I = 0x28


# instance fields
.field private encryptionKeyLength:I


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0x28

    iput v0, p0, Lorg/apache/pdfbox/pdmodel/encryption/ProtectionPolicy;->encryptionKeyLength:I

    return-void
.end method


# virtual methods
.method public getEncryptionKeyLength()I
    .locals 1

    iget v0, p0, Lorg/apache/pdfbox/pdmodel/encryption/ProtectionPolicy;->encryptionKeyLength:I

    return v0
.end method

.method public setEncryptionKeyLength(I)V
    .locals 3

    const/16 v0, 0x28

    if-eq p1, v0, :cond_1

    const/16 v0, 0x80

    if-eq p1, v0, :cond_1

    const/16 v0, 0x100

    if-ne p1, v0, :cond_0

    goto :goto_0

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid key length \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, "\' value must be 40, 128 or 256!"

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    :goto_0
    iput p1, p0, Lorg/apache/pdfbox/pdmodel/encryption/ProtectionPolicy;->encryptionKeyLength:I

    return-void
.end method

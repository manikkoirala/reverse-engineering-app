.class public Lorg/apache/pdfbox/pdmodel/encryption/PDCryptFilterDictionary;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field protected cryptFilterDictionary:Lorg/apache/pdfbox/cos/COSDictionary;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/encryption/PDCryptFilterDictionary;->cryptFilterDictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    new-instance v0, Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-direct {v0}, Lorg/apache/pdfbox/cos/COSDictionary;-><init>()V

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/encryption/PDCryptFilterDictionary;->cryptFilterDictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    return-void
.end method

.method public constructor <init>(Lorg/apache/pdfbox/cos/COSDictionary;)V
    .locals 0

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/encryption/PDCryptFilterDictionary;->cryptFilterDictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    return-void
.end method


# virtual methods
.method public getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/encryption/PDCryptFilterDictionary;->cryptFilterDictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    return-object v0
.end method

.method public getCryptFilterMethod()Lorg/apache/pdfbox/cos/COSName;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/encryption/PDCryptFilterDictionary;->cryptFilterDictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->CFM:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSName;

    return-object v0
.end method

.method public getLength()I
    .locals 3

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/encryption/PDCryptFilterDictionary;->cryptFilterDictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->LENGTH:Lorg/apache/pdfbox/cos/COSName;

    const/16 v2, 0x28

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->getInt(Lorg/apache/pdfbox/cos/COSName;I)I

    move-result v0

    return v0
.end method

.method public setCryptFilterMethod(Lorg/apache/pdfbox/cos/COSName;)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/encryption/PDCryptFilterDictionary;->cryptFilterDictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->CFM:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    return-void
.end method

.method public setLength(I)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/encryption/PDCryptFilterDictionary;->cryptFilterDictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->LENGTH:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setInt(Lorg/apache/pdfbox/cos/COSName;I)V

    return-void
.end method

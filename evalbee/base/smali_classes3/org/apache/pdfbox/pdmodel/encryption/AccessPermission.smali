.class public Lorg/apache/pdfbox/pdmodel/encryption/AccessPermission;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final ASSEMBLE_DOCUMENT_BIT:I = 0xb

.field private static final DEFAULT_PERMISSIONS:I = -0x4

.field private static final DEGRADED_PRINT_BIT:I = 0xc

.field private static final EXTRACT_BIT:I = 0x5

.field private static final EXTRACT_FOR_ACCESSIBILITY_BIT:I = 0xa

.field private static final FILL_IN_FORM_BIT:I = 0x9

.field private static final MODIFICATION_BIT:I = 0x4

.field private static final MODIFY_ANNOTATIONS_BIT:I = 0x6

.field private static final PRINT_BIT:I = 0x3


# instance fields
.field private bytes:I

.field private readOnly:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/pdfbox/pdmodel/encryption/AccessPermission;->readOnly:Z

    const/4 v0, -0x4

    iput v0, p0, Lorg/apache/pdfbox/pdmodel/encryption/AccessPermission;->bytes:I

    return-void
.end method

.method public constructor <init>(I)V
    .locals 1

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/pdfbox/pdmodel/encryption/AccessPermission;->readOnly:Z

    iput p1, p0, Lorg/apache/pdfbox/pdmodel/encryption/AccessPermission;->bytes:I

    return-void
.end method

.method public constructor <init>([B)V
    .locals 2

    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/pdfbox/pdmodel/encryption/AccessPermission;->readOnly:Z

    aget-byte v1, p1, v0

    and-int/lit16 v1, v1, 0xff

    or-int/2addr v0, v1

    shl-int/lit8 v0, v0, 0x8

    const/4 v1, 0x1

    aget-byte v1, p1, v1

    and-int/lit16 v1, v1, 0xff

    or-int/2addr v0, v1

    shl-int/lit8 v0, v0, 0x8

    const/4 v1, 0x2

    aget-byte v1, p1, v1

    and-int/lit16 v1, v1, 0xff

    or-int/2addr v0, v1

    shl-int/lit8 v0, v0, 0x8

    const/4 v1, 0x3

    aget-byte p1, p1, v1

    and-int/lit16 p1, p1, 0xff

    or-int/2addr p1, v0

    iput p1, p0, Lorg/apache/pdfbox/pdmodel/encryption/AccessPermission;->bytes:I

    return-void
.end method

.method public static getOwnerAccessPermission()Lorg/apache/pdfbox/pdmodel/encryption/AccessPermission;
    .locals 2

    new-instance v0, Lorg/apache/pdfbox/pdmodel/encryption/AccessPermission;

    invoke-direct {v0}, Lorg/apache/pdfbox/pdmodel/encryption/AccessPermission;-><init>()V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/pdmodel/encryption/AccessPermission;->setCanAssembleDocument(Z)V

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/pdmodel/encryption/AccessPermission;->setCanExtractContent(Z)V

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/pdmodel/encryption/AccessPermission;->setCanExtractForAccessibility(Z)V

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/pdmodel/encryption/AccessPermission;->setCanFillInForm(Z)V

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/pdmodel/encryption/AccessPermission;->setCanModify(Z)V

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/pdmodel/encryption/AccessPermission;->setCanModifyAnnotations(Z)V

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/pdmodel/encryption/AccessPermission;->setCanPrint(Z)V

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/pdmodel/encryption/AccessPermission;->setCanPrintDegraded(Z)V

    return-object v0
.end method

.method private isPermissionBitOn(I)Z
    .locals 2

    iget v0, p0, Lorg/apache/pdfbox/pdmodel/encryption/AccessPermission;->bytes:I

    const/4 v1, 0x1

    sub-int/2addr p1, v1

    shl-int p1, v1, p1

    and-int/2addr p1, v0

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method private setPermissionBit(IZ)Z
    .locals 2

    iget v0, p0, Lorg/apache/pdfbox/pdmodel/encryption/AccessPermission;->bytes:I

    const/4 v1, 0x1

    if-eqz p2, :cond_0

    add-int/lit8 p2, p1, -0x1

    shl-int p2, v1, p2

    or-int/2addr p2, v0

    goto :goto_0

    :cond_0
    add-int/lit8 p2, p1, -0x1

    shl-int p2, v1, p2

    xor-int/lit8 p2, p2, -0x1

    and-int/2addr p2, v0

    :goto_0
    iput p2, p0, Lorg/apache/pdfbox/pdmodel/encryption/AccessPermission;->bytes:I

    sub-int/2addr p1, v1

    shl-int p1, v1, p1

    and-int/2addr p1, p2

    if-eqz p1, :cond_1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    return v1
.end method


# virtual methods
.method public canAssembleDocument()Z
    .locals 1

    const/16 v0, 0xb

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/pdmodel/encryption/AccessPermission;->isPermissionBitOn(I)Z

    move-result v0

    return v0
.end method

.method public canExtractContent()Z
    .locals 1

    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/pdmodel/encryption/AccessPermission;->isPermissionBitOn(I)Z

    move-result v0

    return v0
.end method

.method public canExtractForAccessibility()Z
    .locals 1

    const/16 v0, 0xa

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/pdmodel/encryption/AccessPermission;->isPermissionBitOn(I)Z

    move-result v0

    return v0
.end method

.method public canFillInForm()Z
    .locals 1

    const/16 v0, 0x9

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/pdmodel/encryption/AccessPermission;->isPermissionBitOn(I)Z

    move-result v0

    return v0
.end method

.method public canModify()Z
    .locals 1

    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/pdmodel/encryption/AccessPermission;->isPermissionBitOn(I)Z

    move-result v0

    return v0
.end method

.method public canModifyAnnotations()Z
    .locals 1

    const/4 v0, 0x6

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/pdmodel/encryption/AccessPermission;->isPermissionBitOn(I)Z

    move-result v0

    return v0
.end method

.method public canPrint()Z
    .locals 1

    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/pdmodel/encryption/AccessPermission;->isPermissionBitOn(I)Z

    move-result v0

    return v0
.end method

.method public canPrintDegraded()Z
    .locals 1

    const/16 v0, 0xc

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/pdmodel/encryption/AccessPermission;->isPermissionBitOn(I)Z

    move-result v0

    return v0
.end method

.method public getPermissionBytes()I
    .locals 1

    iget v0, p0, Lorg/apache/pdfbox/pdmodel/encryption/AccessPermission;->bytes:I

    return v0
.end method

.method public getPermissionBytesForPublicKey()I
    .locals 3

    const/4 v0, 0x1

    invoke-direct {p0, v0, v0}, Lorg/apache/pdfbox/pdmodel/encryption/AccessPermission;->setPermissionBit(IZ)Z

    const/4 v0, 0x7

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/encryption/AccessPermission;->setPermissionBit(IZ)Z

    const/16 v0, 0x8

    invoke-direct {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/encryption/AccessPermission;->setPermissionBit(IZ)Z

    const/16 v0, 0xd

    :goto_0
    const/16 v2, 0x20

    if-gt v0, v2, :cond_0

    invoke-direct {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/encryption/AccessPermission;->setPermissionBit(IZ)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget v0, p0, Lorg/apache/pdfbox/pdmodel/encryption/AccessPermission;->bytes:I

    return v0
.end method

.method public hasAnyRevision3PermissionSet()Z
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/encryption/AccessPermission;->canFillInForm()Z

    move-result v0

    const/4 v1, 0x1

    if-eqz v0, :cond_0

    return v1

    :cond_0
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/encryption/AccessPermission;->canExtractForAccessibility()Z

    move-result v0

    if-eqz v0, :cond_1

    return v1

    :cond_1
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/encryption/AccessPermission;->canAssembleDocument()Z

    move-result v0

    if-eqz v0, :cond_2

    return v1

    :cond_2
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/encryption/AccessPermission;->canPrintDegraded()Z

    move-result v0

    return v0
.end method

.method public isOwnerPermission()Z
    .locals 1

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/encryption/AccessPermission;->canAssembleDocument()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/encryption/AccessPermission;->canExtractContent()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/encryption/AccessPermission;->canExtractForAccessibility()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/encryption/AccessPermission;->canFillInForm()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/encryption/AccessPermission;->canModify()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/encryption/AccessPermission;->canModifyAnnotations()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/encryption/AccessPermission;->canPrint()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/encryption/AccessPermission;->canPrintDegraded()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public isReadOnly()Z
    .locals 1

    iget-boolean v0, p0, Lorg/apache/pdfbox/pdmodel/encryption/AccessPermission;->readOnly:Z

    return v0
.end method

.method public setCanAssembleDocument(Z)V
    .locals 1

    iget-boolean v0, p0, Lorg/apache/pdfbox/pdmodel/encryption/AccessPermission;->readOnly:Z

    if-nez v0, :cond_0

    const/16 v0, 0xb

    invoke-direct {p0, v0, p1}, Lorg/apache/pdfbox/pdmodel/encryption/AccessPermission;->setPermissionBit(IZ)Z

    :cond_0
    return-void
.end method

.method public setCanExtractContent(Z)V
    .locals 1

    iget-boolean v0, p0, Lorg/apache/pdfbox/pdmodel/encryption/AccessPermission;->readOnly:Z

    if-nez v0, :cond_0

    const/4 v0, 0x5

    invoke-direct {p0, v0, p1}, Lorg/apache/pdfbox/pdmodel/encryption/AccessPermission;->setPermissionBit(IZ)Z

    :cond_0
    return-void
.end method

.method public setCanExtractForAccessibility(Z)V
    .locals 1

    iget-boolean v0, p0, Lorg/apache/pdfbox/pdmodel/encryption/AccessPermission;->readOnly:Z

    if-nez v0, :cond_0

    const/16 v0, 0xa

    invoke-direct {p0, v0, p1}, Lorg/apache/pdfbox/pdmodel/encryption/AccessPermission;->setPermissionBit(IZ)Z

    :cond_0
    return-void
.end method

.method public setCanFillInForm(Z)V
    .locals 1

    iget-boolean v0, p0, Lorg/apache/pdfbox/pdmodel/encryption/AccessPermission;->readOnly:Z

    if-nez v0, :cond_0

    const/16 v0, 0x9

    invoke-direct {p0, v0, p1}, Lorg/apache/pdfbox/pdmodel/encryption/AccessPermission;->setPermissionBit(IZ)Z

    :cond_0
    return-void
.end method

.method public setCanModify(Z)V
    .locals 1

    iget-boolean v0, p0, Lorg/apache/pdfbox/pdmodel/encryption/AccessPermission;->readOnly:Z

    if-nez v0, :cond_0

    const/4 v0, 0x4

    invoke-direct {p0, v0, p1}, Lorg/apache/pdfbox/pdmodel/encryption/AccessPermission;->setPermissionBit(IZ)Z

    :cond_0
    return-void
.end method

.method public setCanModifyAnnotations(Z)V
    .locals 1

    iget-boolean v0, p0, Lorg/apache/pdfbox/pdmodel/encryption/AccessPermission;->readOnly:Z

    if-nez v0, :cond_0

    const/4 v0, 0x6

    invoke-direct {p0, v0, p1}, Lorg/apache/pdfbox/pdmodel/encryption/AccessPermission;->setPermissionBit(IZ)Z

    :cond_0
    return-void
.end method

.method public setCanPrint(Z)V
    .locals 1

    iget-boolean v0, p0, Lorg/apache/pdfbox/pdmodel/encryption/AccessPermission;->readOnly:Z

    if-nez v0, :cond_0

    const/4 v0, 0x3

    invoke-direct {p0, v0, p1}, Lorg/apache/pdfbox/pdmodel/encryption/AccessPermission;->setPermissionBit(IZ)Z

    :cond_0
    return-void
.end method

.method public setCanPrintDegraded(Z)V
    .locals 1

    iget-boolean v0, p0, Lorg/apache/pdfbox/pdmodel/encryption/AccessPermission;->readOnly:Z

    if-nez v0, :cond_0

    const/16 v0, 0xc

    invoke-direct {p0, v0, p1}, Lorg/apache/pdfbox/pdmodel/encryption/AccessPermission;->setPermissionBit(IZ)Z

    :cond_0
    return-void
.end method

.method public setReadOnly()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/pdfbox/pdmodel/encryption/AccessPermission;->readOnly:Z

    return-void
.end method

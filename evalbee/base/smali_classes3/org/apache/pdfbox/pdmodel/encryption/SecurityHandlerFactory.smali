.class public final Lorg/apache/pdfbox/pdmodel/encryption/SecurityHandlerFactory;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final INSTANCE:Lorg/apache/pdfbox/pdmodel/encryption/SecurityHandlerFactory;


# instance fields
.field private final nameToHandler:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Class<",
            "+",
            "Lorg/apache/pdfbox/pdmodel/encryption/SecurityHandler;",
            ">;>;"
        }
    .end annotation
.end field

.field private final policyToHandler:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Class<",
            "+",
            "Lorg/apache/pdfbox/pdmodel/encryption/ProtectionPolicy;",
            ">;",
            "Ljava/lang/Class<",
            "+",
            "Lorg/apache/pdfbox/pdmodel/encryption/SecurityHandler;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lorg/apache/pdfbox/pdmodel/encryption/SecurityHandlerFactory;

    invoke-direct {v0}, Lorg/apache/pdfbox/pdmodel/encryption/SecurityHandlerFactory;-><init>()V

    sput-object v0, Lorg/apache/pdfbox/pdmodel/encryption/SecurityHandlerFactory;->INSTANCE:Lorg/apache/pdfbox/pdmodel/encryption/SecurityHandlerFactory;

    new-instance v0, Lorg/spongycastle/jce/provider/BouncyCastleProvider;

    invoke-direct {v0}, Lorg/spongycastle/jce/provider/BouncyCastleProvider;-><init>()V

    invoke-static {v0}, Ljava/security/Security;->addProvider(Ljava/security/Provider;)I

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/encryption/SecurityHandlerFactory;->nameToHandler:Ljava/util/Map;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/encryption/SecurityHandlerFactory;->policyToHandler:Ljava/util/Map;

    const-class v0, Lorg/apache/pdfbox/pdmodel/encryption/StandardSecurityHandler;

    const-class v1, Lorg/apache/pdfbox/pdmodel/encryption/StandardProtectionPolicy;

    const-string v2, "Standard"

    invoke-virtual {p0, v2, v0, v1}, Lorg/apache/pdfbox/pdmodel/encryption/SecurityHandlerFactory;->registerHandler(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Class;)V

    const-class v0, Lorg/apache/pdfbox/pdmodel/encryption/PublicKeySecurityHandler;

    const-class v1, Lorg/apache/pdfbox/pdmodel/encryption/PublicKeyProtectionPolicy;

    const-string v2, "Adobe.PubSec"

    invoke-virtual {p0, v2, v0, v1}, Lorg/apache/pdfbox/pdmodel/encryption/SecurityHandlerFactory;->registerHandler(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Class;)V

    return-void
.end method

.method private static newSecurityHandler(Ljava/lang/Class;[Ljava/lang/Class;[Ljava/lang/Object;)Lorg/apache/pdfbox/pdmodel/encryption/SecurityHandler;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class<",
            "+",
            "Lorg/apache/pdfbox/pdmodel/encryption/SecurityHandler;",
            ">;[",
            "Ljava/lang/Class<",
            "*>;[",
            "Ljava/lang/Object;",
            ")",
            "Lorg/apache/pdfbox/pdmodel/encryption/SecurityHandler;"
        }
    .end annotation

    :try_start_0
    invoke-virtual {p0, p1}, Ljava/lang/Class;->getDeclaredConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object p0

    invoke-virtual {p0, p2}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lorg/apache/pdfbox/pdmodel/encryption/SecurityHandler;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p0

    :catch_0
    move-exception p0

    new-instance p1, Ljava/lang/RuntimeException;

    invoke-direct {p1, p0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw p1

    :catch_1
    move-exception p0

    new-instance p1, Ljava/lang/RuntimeException;

    invoke-direct {p1, p0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw p1

    :catch_2
    move-exception p0

    new-instance p1, Ljava/lang/RuntimeException;

    invoke-direct {p1, p0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw p1

    :catch_3
    move-exception p0

    new-instance p1, Ljava/lang/RuntimeException;

    invoke-direct {p1, p0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw p1
.end method


# virtual methods
.method public newSecurityHandlerForFilter(Ljava/lang/String;)Lorg/apache/pdfbox/pdmodel/encryption/SecurityHandler;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/encryption/SecurityHandlerFactory;->nameToHandler:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Class;

    if-nez p1, :cond_0

    const/4 p1, 0x0

    return-object p1

    :cond_0
    const/4 v0, 0x0

    new-array v1, v0, [Ljava/lang/Class;

    new-array v0, v0, [Ljava/lang/Object;

    invoke-static {p1, v1, v0}, Lorg/apache/pdfbox/pdmodel/encryption/SecurityHandlerFactory;->newSecurityHandler(Ljava/lang/Class;[Ljava/lang/Class;[Ljava/lang/Object;)Lorg/apache/pdfbox/pdmodel/encryption/SecurityHandler;

    move-result-object p1

    return-object p1
.end method

.method public newSecurityHandlerForPolicy(Lorg/apache/pdfbox/pdmodel/encryption/ProtectionPolicy;)Lorg/apache/pdfbox/pdmodel/encryption/SecurityHandler;
    .locals 4

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/encryption/SecurityHandlerFactory;->policyToHandler:Ljava/util/Map;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    if-nez v0, :cond_0

    const/4 p1, 0x0

    return-object p1

    :cond_0
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Class;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    const/4 v3, 0x0

    aput-object v2, v1, v3

    filled-new-array {p1}, [Ljava/lang/Object;

    move-result-object p1

    invoke-static {v0, v1, p1}, Lorg/apache/pdfbox/pdmodel/encryption/SecurityHandlerFactory;->newSecurityHandler(Ljava/lang/Class;[Ljava/lang/Class;[Ljava/lang/Object;)Lorg/apache/pdfbox/pdmodel/encryption/SecurityHandler;

    move-result-object p1

    return-object p1
.end method

.method public registerHandler(Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Class;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Class<",
            "+",
            "Lorg/apache/pdfbox/pdmodel/encryption/SecurityHandler;",
            ">;",
            "Ljava/lang/Class<",
            "+",
            "Lorg/apache/pdfbox/pdmodel/encryption/ProtectionPolicy;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/encryption/SecurityHandlerFactory;->nameToHandler:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/encryption/SecurityHandlerFactory;->nameToHandler:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object p1, p0, Lorg/apache/pdfbox/pdmodel/encryption/SecurityHandlerFactory;->policyToHandler:Ljava/util/Map;

    invoke-interface {p1, p3, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void

    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "The security handler name is already registered"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.class public final Lorg/apache/pdfbox/pdmodel/encryption/StandardProtectionPolicy;
.super Lorg/apache/pdfbox/pdmodel/encryption/ProtectionPolicy;
.source "SourceFile"


# instance fields
.field private ownerPassword:Ljava/lang/String;

.field private permissions:Lorg/apache/pdfbox/pdmodel/encryption/AccessPermission;

.field private userPassword:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Lorg/apache/pdfbox/pdmodel/encryption/AccessPermission;)V
    .locals 0

    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/encryption/ProtectionPolicy;-><init>()V

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/encryption/StandardProtectionPolicy;->ownerPassword:Ljava/lang/String;

    iput-object p2, p0, Lorg/apache/pdfbox/pdmodel/encryption/StandardProtectionPolicy;->userPassword:Ljava/lang/String;

    iput-object p3, p0, Lorg/apache/pdfbox/pdmodel/encryption/StandardProtectionPolicy;->permissions:Lorg/apache/pdfbox/pdmodel/encryption/AccessPermission;

    return-void
.end method


# virtual methods
.method public getOwnerPassword()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/encryption/StandardProtectionPolicy;->ownerPassword:Ljava/lang/String;

    return-object v0
.end method

.method public getPermissions()Lorg/apache/pdfbox/pdmodel/encryption/AccessPermission;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/encryption/StandardProtectionPolicy;->permissions:Lorg/apache/pdfbox/pdmodel/encryption/AccessPermission;

    return-object v0
.end method

.method public getUserPassword()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/encryption/StandardProtectionPolicy;->userPassword:Ljava/lang/String;

    return-object v0
.end method

.method public setOwnerPassword(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/encryption/StandardProtectionPolicy;->ownerPassword:Ljava/lang/String;

    return-void
.end method

.method public setPermissions(Lorg/apache/pdfbox/pdmodel/encryption/AccessPermission;)V
    .locals 0

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/encryption/StandardProtectionPolicy;->permissions:Lorg/apache/pdfbox/pdmodel/encryption/AccessPermission;

    return-void
.end method

.method public setUserPassword(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/encryption/StandardProtectionPolicy;->userPassword:Ljava/lang/String;

    return-void
.end method

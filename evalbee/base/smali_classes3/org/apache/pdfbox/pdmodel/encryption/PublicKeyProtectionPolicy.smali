.class public final Lorg/apache/pdfbox/pdmodel/encryption/PublicKeyProtectionPolicy;
.super Lorg/apache/pdfbox/pdmodel/encryption/ProtectionPolicy;
.source "SourceFile"


# instance fields
.field private decryptionCertificate:Ljava/security/cert/X509Certificate;

.field public final recipients:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lorg/apache/pdfbox/pdmodel/encryption/PublicKeyRecipient;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/encryption/ProtectionPolicy;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/encryption/PublicKeyProtectionPolicy;->recipients:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public addRecipient(Lorg/apache/pdfbox/pdmodel/encryption/PublicKeyRecipient;)V
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/encryption/PublicKeyProtectionPolicy;->recipients:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public getDecryptionCertificate()Ljava/security/cert/X509Certificate;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/encryption/PublicKeyProtectionPolicy;->decryptionCertificate:Ljava/security/cert/X509Certificate;

    return-object v0
.end method

.method public getNumberOfRecipients()I
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/encryption/PublicKeyProtectionPolicy;->recipients:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getRecipientsIterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator<",
            "Lorg/apache/pdfbox/pdmodel/encryption/PublicKeyRecipient;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/encryption/PublicKeyProtectionPolicy;->recipients:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public removeRecipient(Lorg/apache/pdfbox/pdmodel/encryption/PublicKeyRecipient;)Z
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/encryption/PublicKeyProtectionPolicy;->recipients:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public setDecryptionCertificate(Ljava/security/cert/X509Certificate;)V
    .locals 0

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/encryption/PublicKeyProtectionPolicy;->decryptionCertificate:Ljava/security/cert/X509Certificate;

    return-void
.end method

.class public Lorg/apache/pdfbox/pdmodel/PDJavascriptNameTreeNode;
.super Lorg/apache/pdfbox/pdmodel/common/PDNameTreeNode;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    const-class v0, Lorg/apache/pdfbox/pdmodel/common/PDTextStream;

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/pdmodel/common/PDNameTreeNode;-><init>(Ljava/lang/Class;)V

    return-void
.end method

.method public constructor <init>(Lorg/apache/pdfbox/cos/COSDictionary;)V
    .locals 1

    .line 2
    const-class v0, Lorg/apache/pdfbox/pdmodel/common/PDTextStream;

    invoke-direct {p0, p1, v0}, Lorg/apache/pdfbox/pdmodel/common/PDNameTreeNode;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public convertCOSToPD(Lorg/apache/pdfbox/cos/COSBase;)Lorg/apache/pdfbox/pdmodel/common/COSObjectable;
    .locals 3

    instance-of v0, p1, Lorg/apache/pdfbox/cos/COSString;

    if-eqz v0, :cond_0

    new-instance v0, Lorg/apache/pdfbox/pdmodel/common/PDTextStream;

    check-cast p1, Lorg/apache/pdfbox/cos/COSString;

    invoke-direct {v0, p1}, Lorg/apache/pdfbox/pdmodel/common/PDTextStream;-><init>(Lorg/apache/pdfbox/cos/COSString;)V

    goto :goto_0

    :cond_0
    instance-of v0, p1, Lorg/apache/pdfbox/cos/COSStream;

    if-eqz v0, :cond_1

    new-instance v0, Lorg/apache/pdfbox/pdmodel/common/PDTextStream;

    check-cast p1, Lorg/apache/pdfbox/cos/COSStream;

    invoke-direct {v0, p1}, Lorg/apache/pdfbox/pdmodel/common/PDTextStream;-><init>(Lorg/apache/pdfbox/cos/COSStream;)V

    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Error creating Javascript object, expected either COSString or COSStream and not "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public createChildNode(Lorg/apache/pdfbox/cos/COSDictionary;)Lorg/apache/pdfbox/pdmodel/common/PDNameTreeNode;
    .locals 1

    new-instance v0, Lorg/apache/pdfbox/pdmodel/PDJavascriptNameTreeNode;

    invoke-direct {v0, p1}, Lorg/apache/pdfbox/pdmodel/PDJavascriptNameTreeNode;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    return-object v0
.end method

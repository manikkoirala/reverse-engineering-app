.class public Lorg/apache/pdfbox/pdmodel/PDDestinationNameTreeNode;
.super Lorg/apache/pdfbox/pdmodel/common/PDNameTreeNode;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    const-class v0, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/destination/PDPageDestination;

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/pdmodel/common/PDNameTreeNode;-><init>(Ljava/lang/Class;)V

    return-void
.end method

.method public constructor <init>(Lorg/apache/pdfbox/cos/COSDictionary;)V
    .locals 1

    .line 2
    const-class v0, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/destination/PDPageDestination;

    invoke-direct {p0, p1, v0}, Lorg/apache/pdfbox/pdmodel/common/PDNameTreeNode;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public convertCOSToPD(Lorg/apache/pdfbox/cos/COSBase;)Lorg/apache/pdfbox/pdmodel/common/COSObjectable;
    .locals 1

    instance-of v0, p1, Lorg/apache/pdfbox/cos/COSDictionary;

    if-eqz v0, :cond_0

    check-cast p1, Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->D:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p1, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object p1

    :cond_0
    invoke-static {p1}, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/destination/PDDestination;->create(Lorg/apache/pdfbox/cos/COSBase;)Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/destination/PDDestination;

    move-result-object p1

    return-object p1
.end method

.method public createChildNode(Lorg/apache/pdfbox/cos/COSDictionary;)Lorg/apache/pdfbox/pdmodel/common/PDNameTreeNode;
    .locals 1

    new-instance v0, Lorg/apache/pdfbox/pdmodel/PDDestinationNameTreeNode;

    invoke-direct {v0, p1}, Lorg/apache/pdfbox/pdmodel/PDDestinationNameTreeNode;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    return-object v0
.end method

.class public final Lorg/apache/pdfbox/pdmodel/PDResources;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/pdfbox/pdmodel/common/COSObjectable;


# instance fields
.field private final resources:Lorg/apache/pdfbox/cos/COSDictionary;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-direct {v0}, Lorg/apache/pdfbox/cos/COSDictionary;-><init>()V

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDResources;->resources:Lorg/apache/pdfbox/cos/COSDictionary;

    return-void
.end method

.method public constructor <init>(Lorg/apache/pdfbox/cos/COSDictionary;)V
    .locals 1

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-eqz p1, :cond_0

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/PDResources;->resources:Lorg/apache/pdfbox/cos/COSDictionary;

    return-void

    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "resourceDictionary is null"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private add(Lorg/apache/pdfbox/cos/COSName;Ljava/lang/String;Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)Lorg/apache/pdfbox/cos/COSName;
    .locals 2

    .line 1
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDResources;->resources:Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-virtual {v0, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSDictionary;

    if-eqz v0, :cond_0

    invoke-interface {p3}, Lorg/apache/pdfbox/pdmodel/common/COSObjectable;->getCOSObject()Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->containsValue(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p3}, Lorg/apache/pdfbox/pdmodel/common/COSObjectable;->getCOSObject()Lorg/apache/pdfbox/cos/COSBase;

    move-result-object p1

    invoke-virtual {v0, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->getKeyForValue(Ljava/lang/Object;)Lorg/apache/pdfbox/cos/COSName;

    move-result-object p1

    return-object p1

    :cond_0
    invoke-direct {p0, p1, p2}, Lorg/apache/pdfbox/pdmodel/PDResources;->createKey(Lorg/apache/pdfbox/cos/COSName;Ljava/lang/String;)Lorg/apache/pdfbox/cos/COSName;

    move-result-object p2

    invoke-direct {p0, p1, p2, p3}, Lorg/apache/pdfbox/pdmodel/PDResources;->put(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)V

    return-object p2
.end method

.method private createKey(Lorg/apache/pdfbox/cos/COSName;Ljava/lang/String;)Lorg/apache/pdfbox/cos/COSName;
    .locals 3

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDResources;->resources:Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-virtual {v0, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object p1

    check-cast p1, Lorg/apache/pdfbox/cos/COSDictionary;

    const/4 v0, 0x1

    if-nez p1, :cond_0

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lorg/apache/pdfbox/cos/COSName;->getPDFName(Ljava/lang/String;)Lorg/apache/pdfbox/cos/COSName;

    move-result-object p1

    return-object p1

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lorg/apache/pdfbox/cos/COSDictionary;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->size()I

    move-result v2

    add-int/2addr v2, v0

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {v1}, Lorg/apache/pdfbox/cos/COSName;->getPDFName(Ljava/lang/String;)Lorg/apache/pdfbox/cos/COSName;

    move-result-object p1

    return-object p1
.end method

.method private get(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDResources;->resources:Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-virtual {v0, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object p1

    check-cast p1, Lorg/apache/pdfbox/cos/COSDictionary;

    if-nez p1, :cond_0

    const/4 p1, 0x0

    return-object p1

    :cond_0
    invoke-virtual {p1, p2}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object p1

    return-object p1
.end method

.method private getNames(Lorg/apache/pdfbox/cos/COSName;)Ljava/lang/Iterable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/pdfbox/cos/COSName;",
            ")",
            "Ljava/lang/Iterable<",
            "Lorg/apache/pdfbox/cos/COSName;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDResources;->resources:Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-virtual {v0, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object p1

    check-cast p1, Lorg/apache/pdfbox/cos/COSDictionary;

    if-nez p1, :cond_0

    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object p1

    return-object p1

    :cond_0
    invoke-virtual {p1}, Lorg/apache/pdfbox/cos/COSDictionary;->keySet()Ljava/util/Set;

    move-result-object p1

    return-object p1
.end method

.method private put(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDResources;->resources:Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-virtual {v0, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSDictionary;

    if-nez v0, :cond_0

    new-instance v0, Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-direct {v0}, Lorg/apache/pdfbox/cos/COSDictionary;-><init>()V

    iget-object v1, p0, Lorg/apache/pdfbox/pdmodel/PDResources;->resources:Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-virtual {v1, p1, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    :cond_0
    invoke-virtual {v0, p2, p3}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)V

    return-void
.end method


# virtual methods
.method public add(Lorg/apache/pdfbox/pdmodel/documentinterchange/markedcontent/PDPropertyList;)Lorg/apache/pdfbox/cos/COSName;
    .locals 2

    .line 2
    instance-of v0, p1, Lorg/apache/pdfbox/pdmodel/graphics/optionalcontent/PDOptionalContentGroup;

    if-eqz v0, :cond_0

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->PROPERTIES:Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "oc"

    :goto_0
    invoke-direct {p0, v0, v1, p1}, Lorg/apache/pdfbox/pdmodel/PDResources;->add(Lorg/apache/pdfbox/cos/COSName;Ljava/lang/String;Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)Lorg/apache/pdfbox/cos/COSName;

    move-result-object p1

    return-object p1

    :cond_0
    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->PROPERTIES:Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Prop"

    goto :goto_0
.end method

.method public add(Lorg/apache/pdfbox/pdmodel/font/PDFont;)Lorg/apache/pdfbox/cos/COSName;
    .locals 2

    .line 3
    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->FONT:Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "F"

    invoke-direct {p0, v0, v1, p1}, Lorg/apache/pdfbox/pdmodel/PDResources;->add(Lorg/apache/pdfbox/cos/COSName;Ljava/lang/String;Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)Lorg/apache/pdfbox/cos/COSName;

    move-result-object p1

    return-object p1
.end method

.method public add(Lorg/apache/pdfbox/pdmodel/graphics/PDXObject;Ljava/lang/String;)Lorg/apache/pdfbox/cos/COSName;
    .locals 1

    .line 4
    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->XOBJECT:Lorg/apache/pdfbox/cos/COSName;

    invoke-direct {p0, v0, p2, p1}, Lorg/apache/pdfbox/pdmodel/PDResources;->add(Lorg/apache/pdfbox/cos/COSName;Ljava/lang/String;Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)Lorg/apache/pdfbox/cos/COSName;

    move-result-object p1

    return-object p1
.end method

.method public add(Lorg/apache/pdfbox/pdmodel/graphics/color/PDColorSpace;)Lorg/apache/pdfbox/cos/COSName;
    .locals 2

    .line 5
    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->COLORSPACE:Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "cs"

    invoke-direct {p0, v0, v1, p1}, Lorg/apache/pdfbox/pdmodel/PDResources;->add(Lorg/apache/pdfbox/cos/COSName;Ljava/lang/String;Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)Lorg/apache/pdfbox/cos/COSName;

    move-result-object p1

    return-object p1
.end method

.method public add(Lorg/apache/pdfbox/pdmodel/graphics/form/PDFormXObject;)Lorg/apache/pdfbox/cos/COSName;
    .locals 2

    .line 6
    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->XOBJECT:Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Form"

    invoke-direct {p0, v0, v1, p1}, Lorg/apache/pdfbox/pdmodel/PDResources;->add(Lorg/apache/pdfbox/cos/COSName;Ljava/lang/String;Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)Lorg/apache/pdfbox/cos/COSName;

    move-result-object p1

    return-object p1
.end method

.method public add(Lorg/apache/pdfbox/pdmodel/graphics/image/PDImageXObject;)Lorg/apache/pdfbox/cos/COSName;
    .locals 2

    .line 7
    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->XOBJECT:Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Im"

    invoke-direct {p0, v0, v1, p1}, Lorg/apache/pdfbox/pdmodel/PDResources;->add(Lorg/apache/pdfbox/cos/COSName;Ljava/lang/String;Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)Lorg/apache/pdfbox/cos/COSName;

    move-result-object p1

    return-object p1
.end method

.method public add(Lorg/apache/pdfbox/pdmodel/graphics/pattern/PDAbstractPattern;)Lorg/apache/pdfbox/cos/COSName;
    .locals 2

    .line 8
    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->PATTERN:Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "p"

    invoke-direct {p0, v0, v1, p1}, Lorg/apache/pdfbox/pdmodel/PDResources;->add(Lorg/apache/pdfbox/cos/COSName;Ljava/lang/String;Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)Lorg/apache/pdfbox/cos/COSName;

    move-result-object p1

    return-object p1
.end method

.method public add(Lorg/apache/pdfbox/pdmodel/graphics/shading/PDShading;)Lorg/apache/pdfbox/cos/COSName;
    .locals 2

    .line 9
    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->SHADING:Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "sh"

    invoke-direct {p0, v0, v1, p1}, Lorg/apache/pdfbox/pdmodel/PDResources;->add(Lorg/apache/pdfbox/cos/COSName;Ljava/lang/String;Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)Lorg/apache/pdfbox/cos/COSName;

    move-result-object p1

    return-object p1
.end method

.method public add(Lorg/apache/pdfbox/pdmodel/graphics/state/PDExtendedGraphicsState;)Lorg/apache/pdfbox/cos/COSName;
    .locals 2

    .line 10
    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->EXT_G_STATE:Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "gs"

    invoke-direct {p0, v0, v1, p1}, Lorg/apache/pdfbox/pdmodel/PDResources;->add(Lorg/apache/pdfbox/cos/COSName;Ljava/lang/String;Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)Lorg/apache/pdfbox/cos/COSName;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic getCOSObject()Lorg/apache/pdfbox/cos/COSBase;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/PDResources;->getCOSObject()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    return-object v0
.end method

.method public getCOSObject()Lorg/apache/pdfbox/cos/COSDictionary;
    .locals 1

    .line 2
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDResources;->resources:Lorg/apache/pdfbox/cos/COSDictionary;

    return-object v0
.end method

.method public getColorSpace(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/pdmodel/graphics/color/PDColorSpace;
    .locals 1

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->COLORSPACE:Lorg/apache/pdfbox/cos/COSName;

    invoke-direct {p0, v0, p1}, Lorg/apache/pdfbox/pdmodel/PDResources;->get(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {v0, p0}, Lorg/apache/pdfbox/pdmodel/graphics/color/PDColorSpace;->create(Lorg/apache/pdfbox/cos/COSBase;Lorg/apache/pdfbox/pdmodel/PDResources;)Lorg/apache/pdfbox/pdmodel/graphics/color/PDColorSpace;

    move-result-object p1

    return-object p1

    :cond_0
    invoke-static {p1, p0}, Lorg/apache/pdfbox/pdmodel/graphics/color/PDColorSpace;->create(Lorg/apache/pdfbox/cos/COSBase;Lorg/apache/pdfbox/pdmodel/PDResources;)Lorg/apache/pdfbox/pdmodel/graphics/color/PDColorSpace;

    move-result-object p1

    return-object p1
.end method

.method public getColorSpaceNames()Ljava/lang/Iterable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Iterable<",
            "Lorg/apache/pdfbox/cos/COSName;",
            ">;"
        }
    .end annotation

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->COLORSPACE:Lorg/apache/pdfbox/cos/COSName;

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/pdmodel/PDResources;->getNames(Lorg/apache/pdfbox/cos/COSName;)Ljava/lang/Iterable;

    move-result-object v0

    return-object v0
.end method

.method public getExtGState(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/pdmodel/graphics/state/PDExtendedGraphicsState;
    .locals 1

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->EXT_G_STATE:Lorg/apache/pdfbox/cos/COSName;

    invoke-direct {p0, v0, p1}, Lorg/apache/pdfbox/pdmodel/PDResources;->get(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object p1

    check-cast p1, Lorg/apache/pdfbox/cos/COSDictionary;

    if-nez p1, :cond_0

    const/4 p1, 0x0

    return-object p1

    :cond_0
    new-instance v0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDExtendedGraphicsState;

    invoke-direct {v0, p1}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDExtendedGraphicsState;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    return-object v0
.end method

.method public getExtGStateNames()Ljava/lang/Iterable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Iterable<",
            "Lorg/apache/pdfbox/cos/COSName;",
            ">;"
        }
    .end annotation

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->EXT_G_STATE:Lorg/apache/pdfbox/cos/COSName;

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/pdmodel/PDResources;->getNames(Lorg/apache/pdfbox/cos/COSName;)Ljava/lang/Iterable;

    move-result-object v0

    return-object v0
.end method

.method public getFont(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/pdmodel/font/PDFont;
    .locals 1

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->FONT:Lorg/apache/pdfbox/cos/COSName;

    invoke-direct {p0, v0, p1}, Lorg/apache/pdfbox/pdmodel/PDResources;->get(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object p1

    check-cast p1, Lorg/apache/pdfbox/cos/COSDictionary;

    if-nez p1, :cond_0

    const/4 p1, 0x0

    return-object p1

    :cond_0
    invoke-static {p1}, Lorg/apache/pdfbox/pdmodel/font/PDFontFactory;->createFont(Lorg/apache/pdfbox/cos/COSDictionary;)Lorg/apache/pdfbox/pdmodel/font/PDFont;

    move-result-object p1

    return-object p1
.end method

.method public getFontNames()Ljava/lang/Iterable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Iterable<",
            "Lorg/apache/pdfbox/cos/COSName;",
            ">;"
        }
    .end annotation

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->FONT:Lorg/apache/pdfbox/cos/COSName;

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/pdmodel/PDResources;->getNames(Lorg/apache/pdfbox/cos/COSName;)Ljava/lang/Iterable;

    move-result-object v0

    return-object v0
.end method

.method public getPattern(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/pdmodel/graphics/pattern/PDAbstractPattern;
    .locals 1

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->PATTERN:Lorg/apache/pdfbox/cos/COSName;

    invoke-direct {p0, v0, p1}, Lorg/apache/pdfbox/pdmodel/PDResources;->get(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object p1

    check-cast p1, Lorg/apache/pdfbox/cos/COSDictionary;

    if-nez p1, :cond_0

    const/4 p1, 0x0

    return-object p1

    :cond_0
    invoke-static {p1}, Lorg/apache/pdfbox/pdmodel/graphics/pattern/PDAbstractPattern;->create(Lorg/apache/pdfbox/cos/COSDictionary;)Lorg/apache/pdfbox/pdmodel/graphics/pattern/PDAbstractPattern;

    move-result-object p1

    return-object p1
.end method

.method public getPatternNames()Ljava/lang/Iterable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Iterable<",
            "Lorg/apache/pdfbox/cos/COSName;",
            ">;"
        }
    .end annotation

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->PATTERN:Lorg/apache/pdfbox/cos/COSName;

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/pdmodel/PDResources;->getNames(Lorg/apache/pdfbox/cos/COSName;)Ljava/lang/Iterable;

    move-result-object v0

    return-object v0
.end method

.method public getProperties(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/pdmodel/documentinterchange/markedcontent/PDPropertyList;
    .locals 1

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->PROPERTIES:Lorg/apache/pdfbox/cos/COSName;

    invoke-direct {p0, v0, p1}, Lorg/apache/pdfbox/pdmodel/PDResources;->get(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object p1

    check-cast p1, Lorg/apache/pdfbox/cos/COSDictionary;

    if-nez p1, :cond_0

    const/4 p1, 0x0

    return-object p1

    :cond_0
    invoke-static {p1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/markedcontent/PDPropertyList;->create(Lorg/apache/pdfbox/cos/COSDictionary;)Lorg/apache/pdfbox/pdmodel/documentinterchange/markedcontent/PDPropertyList;

    move-result-object p1

    return-object p1
.end method

.method public getPropertiesNames()Ljava/lang/Iterable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Iterable<",
            "Lorg/apache/pdfbox/cos/COSName;",
            ">;"
        }
    .end annotation

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->PROPERTIES:Lorg/apache/pdfbox/cos/COSName;

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/pdmodel/PDResources;->getNames(Lorg/apache/pdfbox/cos/COSName;)Ljava/lang/Iterable;

    move-result-object v0

    return-object v0
.end method

.method public getShading(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/pdmodel/graphics/shading/PDShading;
    .locals 1

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->SHADING:Lorg/apache/pdfbox/cos/COSName;

    invoke-direct {p0, v0, p1}, Lorg/apache/pdfbox/pdmodel/PDResources;->get(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object p1

    check-cast p1, Lorg/apache/pdfbox/cos/COSDictionary;

    if-nez p1, :cond_0

    const/4 p1, 0x0

    return-object p1

    :cond_0
    invoke-static {p1}, Lorg/apache/pdfbox/pdmodel/graphics/shading/PDShading;->create(Lorg/apache/pdfbox/cos/COSDictionary;)Lorg/apache/pdfbox/pdmodel/graphics/shading/PDShading;

    move-result-object p1

    return-object p1
.end method

.method public getShadingNames()Ljava/lang/Iterable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Iterable<",
            "Lorg/apache/pdfbox/cos/COSName;",
            ">;"
        }
    .end annotation

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->SHADING:Lorg/apache/pdfbox/cos/COSName;

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/pdmodel/PDResources;->getNames(Lorg/apache/pdfbox/cos/COSName;)Ljava/lang/Iterable;

    move-result-object v0

    return-object v0
.end method

.method public getXObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/pdmodel/graphics/PDXObject;
    .locals 4

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->XOBJECT:Lorg/apache/pdfbox/cos/COSName;

    invoke-direct {p0, v0, p1}, Lorg/apache/pdfbox/pdmodel/PDResources;->get(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 p1, 0x0

    return-object p1

    :cond_0
    instance-of v1, v0, Lorg/apache/pdfbox/cos/COSObject;

    if-eqz v1, :cond_1

    check-cast v0, Lorg/apache/pdfbox/cos/COSObject;

    invoke-virtual {p1}, Lorg/apache/pdfbox/cos/COSName;->getName()Ljava/lang/String;

    move-result-object p1

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "#"

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSObject;->getObjectNumber()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSObject;->getObject()Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    :goto_0
    invoke-static {v0, p1, p0}, Lorg/apache/pdfbox/pdmodel/graphics/PDXObject;->createXObject(Lorg/apache/pdfbox/cos/COSBase;Ljava/lang/String;Lorg/apache/pdfbox/pdmodel/PDResources;)Lorg/apache/pdfbox/pdmodel/graphics/PDXObject;

    move-result-object p1

    return-object p1

    :cond_1
    invoke-virtual {p1}, Lorg/apache/pdfbox/cos/COSName;->getName()Ljava/lang/String;

    move-result-object p1

    goto :goto_0
.end method

.method public getXObjectNames()Ljava/lang/Iterable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Iterable<",
            "Lorg/apache/pdfbox/cos/COSName;",
            ">;"
        }
    .end annotation

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->XOBJECT:Lorg/apache/pdfbox/cos/COSName;

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/pdmodel/PDResources;->getNames(Lorg/apache/pdfbox/cos/COSName;)Ljava/lang/Iterable;

    move-result-object v0

    return-object v0
.end method

.method public hasColorSpace(Lorg/apache/pdfbox/cos/COSName;)Z
    .locals 1

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->COLORSPACE:Lorg/apache/pdfbox/cos/COSName;

    invoke-direct {p0, v0, p1}, Lorg/apache/pdfbox/pdmodel/PDResources;->get(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public put(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/pdmodel/documentinterchange/markedcontent/PDPropertyList;)V
    .locals 1

    .line 2
    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->PROPERTIES:Lorg/apache/pdfbox/cos/COSName;

    invoke-direct {p0, v0, p1, p2}, Lorg/apache/pdfbox/pdmodel/PDResources;->put(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)V

    return-void
.end method

.method public put(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/pdmodel/font/PDFont;)V
    .locals 1

    .line 3
    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->FONT:Lorg/apache/pdfbox/cos/COSName;

    invoke-direct {p0, v0, p1, p2}, Lorg/apache/pdfbox/pdmodel/PDResources;->put(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)V

    return-void
.end method

.method public put(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/pdmodel/graphics/PDXObject;)V
    .locals 1

    .line 4
    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->XOBJECT:Lorg/apache/pdfbox/cos/COSName;

    invoke-direct {p0, v0, p1, p2}, Lorg/apache/pdfbox/pdmodel/PDResources;->put(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)V

    return-void
.end method

.method public put(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/pdmodel/graphics/color/PDColorSpace;)V
    .locals 1

    .line 5
    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->COLORSPACE:Lorg/apache/pdfbox/cos/COSName;

    invoke-direct {p0, v0, p1, p2}, Lorg/apache/pdfbox/pdmodel/PDResources;->put(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)V

    return-void
.end method

.method public put(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/pdmodel/graphics/pattern/PDAbstractPattern;)V
    .locals 1

    .line 6
    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->PATTERN:Lorg/apache/pdfbox/cos/COSName;

    invoke-direct {p0, v0, p1, p2}, Lorg/apache/pdfbox/pdmodel/PDResources;->put(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)V

    return-void
.end method

.method public put(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/pdmodel/graphics/shading/PDShading;)V
    .locals 1

    .line 7
    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->SHADING:Lorg/apache/pdfbox/cos/COSName;

    invoke-direct {p0, v0, p1, p2}, Lorg/apache/pdfbox/pdmodel/PDResources;->put(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)V

    return-void
.end method

.method public put(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/pdmodel/graphics/state/PDExtendedGraphicsState;)V
    .locals 1

    .line 8
    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->EXT_G_STATE:Lorg/apache/pdfbox/cos/COSName;

    invoke-direct {p0, v0, p1, p2}, Lorg/apache/pdfbox/pdmodel/PDResources;->put(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)V

    return-void
.end method

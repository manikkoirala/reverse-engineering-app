.class public Lorg/apache/pdfbox/pdmodel/fdf/FDFAnnotationSound;
.super Lorg/apache/pdfbox/pdmodel/fdf/FDFAnnotation;
.source "SourceFile"


# static fields
.field public static final SUBTYPE:Ljava/lang/String; = "Sound"


# direct methods
.method public constructor <init>()V
    .locals 3

    .line 1
    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/fdf/FDFAnnotation;-><init>()V

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/fdf/FDFAnnotation;->annot:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->SUBTYPE:Lorg/apache/pdfbox/cos/COSName;

    const-string v2, "Sound"

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->setName(Lorg/apache/pdfbox/cos/COSName;Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Lorg/apache/pdfbox/cos/COSDictionary;)V
    .locals 0

    .line 2
    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/fdf/FDFAnnotation;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    return-void
.end method

.method public constructor <init>(Lorg/w3c/dom/Element;)V
    .locals 2

    .line 3
    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/fdf/FDFAnnotation;-><init>(Lorg/w3c/dom/Element;)V

    iget-object p1, p0, Lorg/apache/pdfbox/pdmodel/fdf/FDFAnnotation;->annot:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->SUBTYPE:Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Sound"

    invoke-virtual {p1, v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->setName(Lorg/apache/pdfbox/cos/COSName;Ljava/lang/String;)V

    return-void
.end method

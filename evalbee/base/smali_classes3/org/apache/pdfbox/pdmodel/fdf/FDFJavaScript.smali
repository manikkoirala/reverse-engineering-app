.class public Lorg/apache/pdfbox/pdmodel/fdf/FDFJavaScript;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/pdfbox/pdmodel/common/COSObjectable;


# instance fields
.field private js:Lorg/apache/pdfbox/cos/COSDictionary;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-direct {v0}, Lorg/apache/pdfbox/cos/COSDictionary;-><init>()V

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/fdf/FDFJavaScript;->js:Lorg/apache/pdfbox/cos/COSDictionary;

    return-void
.end method

.method public constructor <init>(Lorg/apache/pdfbox/cos/COSDictionary;)V
    .locals 0

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/fdf/FDFJavaScript;->js:Lorg/apache/pdfbox/cos/COSDictionary;

    return-void
.end method


# virtual methods
.method public getAfter()Lorg/apache/pdfbox/pdmodel/common/PDTextStream;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/fdf/FDFJavaScript;->js:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->AFTER:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/pdfbox/pdmodel/common/PDTextStream;->createTextStream(Lorg/apache/pdfbox/cos/COSBase;)Lorg/apache/pdfbox/pdmodel/common/PDTextStream;

    move-result-object v0

    return-object v0
.end method

.method public getBefore()Lorg/apache/pdfbox/pdmodel/common/PDTextStream;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/fdf/FDFJavaScript;->js:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->BEFORE:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/pdfbox/pdmodel/common/PDTextStream;->createTextStream(Lorg/apache/pdfbox/cos/COSBase;)Lorg/apache/pdfbox/pdmodel/common/PDTextStream;

    move-result-object v0

    return-object v0
.end method

.method public getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/fdf/FDFJavaScript;->js:Lorg/apache/pdfbox/cos/COSDictionary;

    return-object v0
.end method

.method public getCOSObject()Lorg/apache/pdfbox/cos/COSBase;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/fdf/FDFJavaScript;->js:Lorg/apache/pdfbox/cos/COSDictionary;

    return-object v0
.end method

.method public getNamedJavaScripts()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lorg/apache/pdfbox/pdmodel/common/PDNamedTextStream;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/fdf/FDFJavaScript;->js:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->DOC:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSArray;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    if-nez v0, :cond_0

    new-instance v0, Lorg/apache/pdfbox/cos/COSArray;

    invoke-direct {v0}, Lorg/apache/pdfbox/cos/COSArray;-><init>()V

    iget-object v3, p0, Lorg/apache/pdfbox/pdmodel/fdf/FDFJavaScript;->js:Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-virtual {v3, v1, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    :cond_0
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSArray;->size()I

    move-result v3

    if-ge v1, v3, :cond_1

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSArray;->get(I)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v3

    check-cast v3, Lorg/apache/pdfbox/cos/COSName;

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSArray;->get(I)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v4

    new-instance v5, Lorg/apache/pdfbox/pdmodel/common/PDNamedTextStream;

    invoke-direct {v5, v3, v4}, Lorg/apache/pdfbox/pdmodel/common/PDNamedTextStream;-><init>(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    new-instance v1, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;

    invoke-direct {v1, v2, v0}, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;-><init>(Ljava/util/List;Lorg/apache/pdfbox/cos/COSArray;)V

    return-object v1
.end method

.method public setAfter(Lorg/apache/pdfbox/pdmodel/common/PDTextStream;)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/fdf/FDFJavaScript;->js:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->AFTER:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)V

    return-void
.end method

.method public setBefore(Lorg/apache/pdfbox/pdmodel/common/PDTextStream;)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/fdf/FDFJavaScript;->js:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->BEFORE:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)V

    return-void
.end method

.method public setNamedJavaScripts(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lorg/apache/pdfbox/pdmodel/common/PDTextStream;",
            ">;)V"
        }
    .end annotation

    invoke-static {p1}, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;->converterToCOSArray(Ljava/util/List;)Lorg/apache/pdfbox/cos/COSArray;

    move-result-object p1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/fdf/FDFJavaScript;->js:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->DOC:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    return-void
.end method

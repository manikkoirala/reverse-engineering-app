.class public Lorg/apache/pdfbox/pdmodel/fdf/FDFIconFit;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/pdfbox/pdmodel/common/COSObjectable;


# static fields
.field public static final SCALE_OPTION_ALWAYS:Ljava/lang/String; = "A"

.field public static final SCALE_OPTION_NEVER:Ljava/lang/String; = "N"

.field public static final SCALE_OPTION_ONLY_WHEN_ICON_IS_BIGGER:Ljava/lang/String; = "B"

.field public static final SCALE_OPTION_ONLY_WHEN_ICON_IS_SMALLER:Ljava/lang/String; = "S"

.field public static final SCALE_TYPE_ANAMORPHIC:Ljava/lang/String; = "A"

.field public static final SCALE_TYPE_PROPORTIONAL:Ljava/lang/String; = "P"


# instance fields
.field private fit:Lorg/apache/pdfbox/cos/COSDictionary;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-direct {v0}, Lorg/apache/pdfbox/cos/COSDictionary;-><init>()V

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/fdf/FDFIconFit;->fit:Lorg/apache/pdfbox/cos/COSDictionary;

    return-void
.end method

.method public constructor <init>(Lorg/apache/pdfbox/cos/COSDictionary;)V
    .locals 0

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/fdf/FDFIconFit;->fit:Lorg/apache/pdfbox/cos/COSDictionary;

    return-void
.end method


# virtual methods
.method public getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/fdf/FDFIconFit;->fit:Lorg/apache/pdfbox/cos/COSDictionary;

    return-object v0
.end method

.method public getCOSObject()Lorg/apache/pdfbox/cos/COSBase;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/fdf/FDFIconFit;->fit:Lorg/apache/pdfbox/cos/COSDictionary;

    return-object v0
.end method

.method public getFractionalSpaceToAllocate()Lorg/apache/pdfbox/pdmodel/common/PDRange;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/fdf/FDFIconFit;->fit:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->A:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSArray;

    if-nez v0, :cond_0

    new-instance v0, Lorg/apache/pdfbox/pdmodel/common/PDRange;

    invoke-direct {v0}, Lorg/apache/pdfbox/pdmodel/common/PDRange;-><init>()V

    const/high16 v1, 0x3f000000    # 0.5f

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/pdmodel/common/PDRange;->setMin(F)V

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/pdmodel/common/PDRange;->setMax(F)V

    invoke-virtual {p0, v0}, Lorg/apache/pdfbox/pdmodel/fdf/FDFIconFit;->setFractionalSpaceToAllocate(Lorg/apache/pdfbox/pdmodel/common/PDRange;)V

    goto :goto_0

    :cond_0
    new-instance v1, Lorg/apache/pdfbox/pdmodel/common/PDRange;

    invoke-direct {v1, v0}, Lorg/apache/pdfbox/pdmodel/common/PDRange;-><init>(Lorg/apache/pdfbox/cos/COSArray;)V

    move-object v0, v1

    :goto_0
    return-object v0
.end method

.method public getScaleOption()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/fdf/FDFIconFit;->fit:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->SW:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getNameAsString(Lorg/apache/pdfbox/cos/COSName;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, "A"

    :cond_0
    return-object v0
.end method

.method public getScaleType()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/fdf/FDFIconFit;->fit:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->S:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getNameAsString(Lorg/apache/pdfbox/cos/COSName;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, "P"

    :cond_0
    return-object v0
.end method

.method public setFractionalSpaceToAllocate(Lorg/apache/pdfbox/pdmodel/common/PDRange;)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/fdf/FDFIconFit;->fit:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->A:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)V

    return-void
.end method

.method public setScaleOption(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/fdf/FDFIconFit;->fit:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->SW:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setName(Lorg/apache/pdfbox/cos/COSName;Ljava/lang/String;)V

    return-void
.end method

.method public setScaleToFitAnnotation(Z)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/fdf/FDFIconFit;->fit:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->FB:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setBoolean(Lorg/apache/pdfbox/cos/COSName;Z)V

    return-void
.end method

.method public setScaleType(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/fdf/FDFIconFit;->fit:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->S:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setName(Lorg/apache/pdfbox/cos/COSName;Ljava/lang/String;)V

    return-void
.end method

.method public shouldScaleToFitAnnotation()Z
    .locals 3

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/fdf/FDFIconFit;->fit:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->FB:Lorg/apache/pdfbox/cos/COSName;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->getBoolean(Lorg/apache/pdfbox/cos/COSName;Z)Z

    move-result v0

    return v0
.end method

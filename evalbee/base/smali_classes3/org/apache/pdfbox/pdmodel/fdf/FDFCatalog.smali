.class public Lorg/apache/pdfbox/pdmodel/fdf/FDFCatalog;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/pdfbox/pdmodel/common/COSObjectable;


# instance fields
.field private catalog:Lorg/apache/pdfbox/cos/COSDictionary;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-direct {v0}, Lorg/apache/pdfbox/cos/COSDictionary;-><init>()V

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/fdf/FDFCatalog;->catalog:Lorg/apache/pdfbox/cos/COSDictionary;

    return-void
.end method

.method public constructor <init>(Lorg/apache/pdfbox/cos/COSDictionary;)V
    .locals 0

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/fdf/FDFCatalog;->catalog:Lorg/apache/pdfbox/cos/COSDictionary;

    return-void
.end method

.method public constructor <init>(Lorg/w3c/dom/Element;)V
    .locals 1

    .line 3
    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/fdf/FDFCatalog;-><init>()V

    new-instance v0, Lorg/apache/pdfbox/pdmodel/fdf/FDFDictionary;

    invoke-direct {v0, p1}, Lorg/apache/pdfbox/pdmodel/fdf/FDFDictionary;-><init>(Lorg/w3c/dom/Element;)V

    invoke-virtual {p0, v0}, Lorg/apache/pdfbox/pdmodel/fdf/FDFCatalog;->setFDF(Lorg/apache/pdfbox/pdmodel/fdf/FDFDictionary;)V

    return-void
.end method


# virtual methods
.method public getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/fdf/FDFCatalog;->catalog:Lorg/apache/pdfbox/cos/COSDictionary;

    return-object v0
.end method

.method public getCOSObject()Lorg/apache/pdfbox/cos/COSBase;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/fdf/FDFCatalog;->catalog:Lorg/apache/pdfbox/cos/COSDictionary;

    return-object v0
.end method

.method public getFDF()Lorg/apache/pdfbox/pdmodel/fdf/FDFDictionary;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/fdf/FDFCatalog;->catalog:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->FDF:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSDictionary;

    new-instance v1, Lorg/apache/pdfbox/pdmodel/fdf/FDFDictionary;

    if-eqz v0, :cond_0

    invoke-direct {v1, v0}, Lorg/apache/pdfbox/pdmodel/fdf/FDFDictionary;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    goto :goto_0

    :cond_0
    invoke-direct {v1}, Lorg/apache/pdfbox/pdmodel/fdf/FDFDictionary;-><init>()V

    invoke-virtual {p0, v1}, Lorg/apache/pdfbox/pdmodel/fdf/FDFCatalog;->setFDF(Lorg/apache/pdfbox/pdmodel/fdf/FDFDictionary;)V

    :goto_0
    return-object v1
.end method

.method public getSignature()Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/PDSignature;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/fdf/FDFCatalog;->catalog:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->SIG:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSDictionary;

    if-eqz v0, :cond_0

    new-instance v1, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/PDSignature;

    invoke-direct {v1, v0}, Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/PDSignature;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return-object v1
.end method

.method public getVersion()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/fdf/FDFCatalog;->catalog:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->VERSION:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getNameAsString(Lorg/apache/pdfbox/cos/COSName;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public setFDF(Lorg/apache/pdfbox/pdmodel/fdf/FDFDictionary;)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/fdf/FDFCatalog;->catalog:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->FDF:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)V

    return-void
.end method

.method public setSignature(Lorg/apache/pdfbox/pdmodel/interactive/digitalsignature/PDSignature;)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/fdf/FDFCatalog;->catalog:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->SIG:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)V

    return-void
.end method

.method public setVersion(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/fdf/FDFCatalog;->catalog:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->VERSION:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setName(Lorg/apache/pdfbox/cos/COSName;Ljava/lang/String;)V

    return-void
.end method

.method public writeXML(Ljava/io/Writer;)V
    .locals 1

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/fdf/FDFCatalog;->getFDF()Lorg/apache/pdfbox/pdmodel/fdf/FDFDictionary;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/pdfbox/pdmodel/fdf/FDFDictionary;->writeXML(Ljava/io/Writer;)V

    return-void
.end method

.class public Lorg/apache/pdfbox/pdmodel/fdf/FDFDocument;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/io/Closeable;


# instance fields
.field private document:Lorg/apache/pdfbox/cos/COSDocument;


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lorg/apache/pdfbox/cos/COSDocument;

    invoke-direct {v0}, Lorg/apache/pdfbox/cos/COSDocument;-><init>()V

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/fdf/FDFDocument;->document:Lorg/apache/pdfbox/cos/COSDocument;

    const v1, 0x3f99999a    # 1.2f

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDocument;->setVersion(F)V

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/fdf/FDFDocument;->document:Lorg/apache/pdfbox/cos/COSDocument;

    new-instance v1, Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-direct {v1}, Lorg/apache/pdfbox/cos/COSDictionary;-><init>()V

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDocument;->setTrailer(Lorg/apache/pdfbox/cos/COSDictionary;)V

    new-instance v0, Lorg/apache/pdfbox/pdmodel/fdf/FDFCatalog;

    invoke-direct {v0}, Lorg/apache/pdfbox/pdmodel/fdf/FDFCatalog;-><init>()V

    invoke-virtual {p0, v0}, Lorg/apache/pdfbox/pdmodel/fdf/FDFDocument;->setCatalog(Lorg/apache/pdfbox/pdmodel/fdf/FDFCatalog;)V

    return-void
.end method

.method public constructor <init>(Lorg/apache/pdfbox/cos/COSDocument;)V
    .locals 0

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/fdf/FDFDocument;->document:Lorg/apache/pdfbox/cos/COSDocument;

    return-void
.end method

.method public constructor <init>(Lorg/w3c/dom/Document;)V
    .locals 3

    .line 3
    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/fdf/FDFDocument;-><init>()V

    invoke-interface {p1}, Lorg/w3c/dom/Document;->getDocumentElement()Lorg/w3c/dom/Element;

    move-result-object p1

    invoke-interface {p1}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "xfdf"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lorg/apache/pdfbox/pdmodel/fdf/FDFCatalog;

    invoke-direct {v0, p1}, Lorg/apache/pdfbox/pdmodel/fdf/FDFCatalog;-><init>(Lorg/w3c/dom/Element;)V

    invoke-virtual {p0, v0}, Lorg/apache/pdfbox/pdmodel/fdf/FDFDocument;->setCatalog(Lorg/apache/pdfbox/pdmodel/fdf/FDFCatalog;)V

    return-void

    :cond_0
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Error while importing xfdf document, root should be \'xfdf\' and not \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {p1}, Lorg/w3c/dom/Node;->getNodeName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, "\'"

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static load(Ljava/io/File;)Lorg/apache/pdfbox/pdmodel/fdf/FDFDocument;
    .locals 1

    .line 1
    new-instance v0, Lorg/apache/pdfbox/pdfparser/FDFParser;

    invoke-direct {v0, p0}, Lorg/apache/pdfbox/pdfparser/FDFParser;-><init>(Ljava/io/File;)V

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdfparser/FDFParser;->parse()V

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdfparser/FDFParser;->getFDFDocument()Lorg/apache/pdfbox/pdmodel/fdf/FDFDocument;

    move-result-object p0

    return-object p0
.end method

.method public static load(Ljava/io/InputStream;)Lorg/apache/pdfbox/pdmodel/fdf/FDFDocument;
    .locals 1

    .line 2
    new-instance v0, Lorg/apache/pdfbox/pdfparser/FDFParser;

    invoke-direct {v0, p0}, Lorg/apache/pdfbox/pdfparser/FDFParser;-><init>(Ljava/io/InputStream;)V

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdfparser/FDFParser;->parse()V

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdfparser/FDFParser;->getFDFDocument()Lorg/apache/pdfbox/pdmodel/fdf/FDFDocument;

    move-result-object p0

    return-object p0
.end method

.method public static load(Ljava/lang/String;)Lorg/apache/pdfbox/pdmodel/fdf/FDFDocument;
    .locals 1

    .line 3
    new-instance v0, Lorg/apache/pdfbox/pdfparser/FDFParser;

    invoke-direct {v0, p0}, Lorg/apache/pdfbox/pdfparser/FDFParser;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdfparser/FDFParser;->parse()V

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdfparser/FDFParser;->getFDFDocument()Lorg/apache/pdfbox/pdmodel/fdf/FDFDocument;

    move-result-object p0

    return-object p0
.end method

.method public static loadXFDF(Ljava/io/File;)Lorg/apache/pdfbox/pdmodel/fdf/FDFDocument;
    .locals 2

    .line 1
    new-instance v0, Ljava/io/BufferedInputStream;

    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, p0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v0, v1}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    invoke-static {v0}, Lorg/apache/pdfbox/pdmodel/fdf/FDFDocument;->loadXFDF(Ljava/io/InputStream;)Lorg/apache/pdfbox/pdmodel/fdf/FDFDocument;

    move-result-object p0

    return-object p0
.end method

.method public static loadXFDF(Ljava/io/InputStream;)Lorg/apache/pdfbox/pdmodel/fdf/FDFDocument;
    .locals 1

    .line 2
    invoke-static {p0}, Lorg/apache/pdfbox/pdmodel/fdf/XMLUtil;->parse(Ljava/io/InputStream;)Lorg/w3c/dom/Document;

    move-result-object p0

    new-instance v0, Lorg/apache/pdfbox/pdmodel/fdf/FDFDocument;

    invoke-direct {v0, p0}, Lorg/apache/pdfbox/pdmodel/fdf/FDFDocument;-><init>(Lorg/w3c/dom/Document;)V

    return-object v0
.end method

.method public static loadXFDF(Ljava/lang/String;)Lorg/apache/pdfbox/pdmodel/fdf/FDFDocument;
    .locals 2

    .line 3
    new-instance v0, Ljava/io/BufferedInputStream;

    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, p0}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v1}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    invoke-static {v0}, Lorg/apache/pdfbox/pdmodel/fdf/FDFDocument;->loadXFDF(Ljava/io/InputStream;)Lorg/apache/pdfbox/pdmodel/fdf/FDFDocument;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public close()V
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/fdf/FDFDocument;->document:Lorg/apache/pdfbox/cos/COSDocument;

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSDocument;->close()V

    return-void
.end method

.method public getCatalog()Lorg/apache/pdfbox/pdmodel/fdf/FDFCatalog;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/fdf/FDFDocument;->document:Lorg/apache/pdfbox/cos/COSDocument;

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSDocument;->getTrailer()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->ROOT:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSDictionary;

    if-nez v0, :cond_0

    new-instance v0, Lorg/apache/pdfbox/pdmodel/fdf/FDFCatalog;

    invoke-direct {v0}, Lorg/apache/pdfbox/pdmodel/fdf/FDFCatalog;-><init>()V

    invoke-virtual {p0, v0}, Lorg/apache/pdfbox/pdmodel/fdf/FDFDocument;->setCatalog(Lorg/apache/pdfbox/pdmodel/fdf/FDFCatalog;)V

    goto :goto_0

    :cond_0
    new-instance v1, Lorg/apache/pdfbox/pdmodel/fdf/FDFCatalog;

    invoke-direct {v1, v0}, Lorg/apache/pdfbox/pdmodel/fdf/FDFCatalog;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    move-object v0, v1

    :goto_0
    return-object v0
.end method

.method public getDocument()Lorg/apache/pdfbox/cos/COSDocument;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/fdf/FDFDocument;->document:Lorg/apache/pdfbox/cos/COSDocument;

    return-object v0
.end method

.method public save(Ljava/io/File;)V
    .locals 1

    .line 1
    new-instance v0, Ljava/io/FileOutputStream;

    invoke-direct {v0, p1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-virtual {p0, v0}, Lorg/apache/pdfbox/pdmodel/fdf/FDFDocument;->save(Ljava/io/OutputStream;)V

    return-void
.end method

.method public save(Ljava/io/OutputStream;)V
    .locals 2

    .line 2
    const/4 v0, 0x0

    :try_start_0
    new-instance v1, Lorg/apache/pdfbox/pdfwriter/COSWriter;

    invoke-direct {v1, p1}, Lorg/apache/pdfbox/pdfwriter/COSWriter;-><init>(Ljava/io/OutputStream;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    invoke-virtual {v1, p0}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->write(Lorg/apache/pdfbox/pdmodel/fdf/FDFDocument;)V

    invoke-virtual {v1}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->close()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-virtual {v1}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->close()V

    return-void

    :catchall_0
    move-exception p1

    move-object v0, v1

    goto :goto_0

    :catchall_1
    move-exception p1

    :goto_0
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdfwriter/COSWriter;->close()V

    :cond_0
    throw p1
.end method

.method public save(Ljava/lang/String;)V
    .locals 1

    .line 3
    new-instance v0, Ljava/io/FileOutputStream;

    invoke-direct {v0, p1}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lorg/apache/pdfbox/pdmodel/fdf/FDFDocument;->save(Ljava/io/OutputStream;)V

    return-void
.end method

.method public saveXFDF(Ljava/io/File;)V
    .locals 2

    .line 1
    new-instance v0, Ljava/io/BufferedWriter;

    new-instance v1, Ljava/io/FileWriter;

    invoke-direct {v1, p1}, Ljava/io/FileWriter;-><init>(Ljava/io/File;)V

    invoke-direct {v0, v1}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V

    invoke-virtual {p0, v0}, Lorg/apache/pdfbox/pdmodel/fdf/FDFDocument;->saveXFDF(Ljava/io/Writer;)V

    return-void
.end method

.method public saveXFDF(Ljava/io/Writer;)V
    .locals 1

    .line 2
    :try_start_0
    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/pdmodel/fdf/FDFDocument;->writeXML(Ljava/io/Writer;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/io/Writer;->close()V

    :cond_0
    return-void

    :catchall_0
    move-exception v0

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/io/Writer;->close()V

    :cond_1
    throw v0
.end method

.method public saveXFDF(Ljava/lang/String;)V
    .locals 2

    .line 3
    new-instance v0, Ljava/io/BufferedWriter;

    new-instance v1, Ljava/io/FileWriter;

    invoke-direct {v1, p1}, Ljava/io/FileWriter;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v1}, Ljava/io/BufferedWriter;-><init>(Ljava/io/Writer;)V

    invoke-virtual {p0, v0}, Lorg/apache/pdfbox/pdmodel/fdf/FDFDocument;->saveXFDF(Ljava/io/Writer;)V

    return-void
.end method

.method public setCatalog(Lorg/apache/pdfbox/pdmodel/fdf/FDFCatalog;)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/fdf/FDFDocument;->document:Lorg/apache/pdfbox/cos/COSDocument;

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSDocument;->getTrailer()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->ROOT:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)V

    return-void
.end method

.method public writeXML(Ljava/io/Writer;)V
    .locals 1

    const-string v0, "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"

    invoke-virtual {p1, v0}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    const-string v0, "<xfdf xmlns=\"http://ns.adobe.com/xfdf/\" xml:space=\"preserve\">\n"

    invoke-virtual {p1, v0}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/fdf/FDFDocument;->getCatalog()Lorg/apache/pdfbox/pdmodel/fdf/FDFCatalog;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/pdfbox/pdmodel/fdf/FDFCatalog;->writeXML(Ljava/io/Writer;)V

    const-string v0, "</xfdf>\n"

    invoke-virtual {p1, v0}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    return-void
.end method

.class public Lorg/apache/pdfbox/pdmodel/graphics/predictor/Paeth;
.super Lorg/apache/pdfbox/pdmodel/graphics/predictor/PredictorAlgorithm;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/graphics/predictor/PredictorAlgorithm;-><init>()V

    return-void
.end method


# virtual methods
.method public decodeLine([B[BIIII)V
    .locals 6

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/predictor/PredictorAlgorithm;->getWidth()I

    move-result p3

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/predictor/PredictorAlgorithm;->getBpp()I

    move-result v0

    mul-int/2addr p3, v0

    const/4 v0, 0x0

    :goto_0
    if-ge v0, p3, :cond_0

    add-int v1, v0, p6

    add-int v2, v0, p4

    aget-byte v2, p1, v2

    invoke-virtual {p0, p2, p6, p5, v0}, Lorg/apache/pdfbox/pdmodel/graphics/predictor/PredictorAlgorithm;->leftPixel([BIII)I

    move-result v3

    invoke-virtual {p0, p2, p6, p5, v0}, Lorg/apache/pdfbox/pdmodel/graphics/predictor/PredictorAlgorithm;->abovePixel([BIII)I

    move-result v4

    invoke-virtual {p0, p2, p6, p5, v0}, Lorg/apache/pdfbox/pdmodel/graphics/predictor/PredictorAlgorithm;->aboveLeftPixel([BIII)I

    move-result v5

    invoke-virtual {p0, v3, v4, v5}, Lorg/apache/pdfbox/pdmodel/graphics/predictor/Paeth;->paethPredictor(III)I

    move-result v3

    add-int/2addr v2, v3

    int-to-byte v2, v2

    aput-byte v2, p2, v1

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public encodeLine([B[BIIII)V
    .locals 6

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/predictor/PredictorAlgorithm;->getWidth()I

    move-result p5

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/predictor/PredictorAlgorithm;->getBpp()I

    move-result v0

    mul-int/2addr p5, v0

    const/4 v0, 0x0

    :goto_0
    if-ge v0, p5, :cond_0

    add-int v1, v0, p6

    add-int v2, v0, p4

    aget-byte v2, p1, v2

    invoke-virtual {p0, p1, p4, p3, v0}, Lorg/apache/pdfbox/pdmodel/graphics/predictor/PredictorAlgorithm;->leftPixel([BIII)I

    move-result v3

    invoke-virtual {p0, p1, p4, p3, v0}, Lorg/apache/pdfbox/pdmodel/graphics/predictor/PredictorAlgorithm;->abovePixel([BIII)I

    move-result v4

    invoke-virtual {p0, p1, p4, p3, v0}, Lorg/apache/pdfbox/pdmodel/graphics/predictor/PredictorAlgorithm;->aboveLeftPixel([BIII)I

    move-result v5

    invoke-virtual {p0, v3, v4, v5}, Lorg/apache/pdfbox/pdmodel/graphics/predictor/Paeth;->paethPredictor(III)I

    move-result v3

    sub-int/2addr v2, v3

    int-to-byte v2, v2

    aput-byte v2, p2, v1

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public paethPredictor(III)I
    .locals 3

    add-int v0, p1, p2

    sub-int/2addr v0, p3

    sub-int v1, v0, p1

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    sub-int v2, v0, p2

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v2

    sub-int/2addr v0, p3

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    if-gt v1, v2, :cond_0

    if-gt v1, v0, :cond_0

    return p1

    :cond_0
    if-gt v2, v0, :cond_1

    return p2

    :cond_1
    return p3
.end method

.class public final enum Lorg/apache/pdfbox/pdmodel/graphics/optionalcontent/PDOptionalContentProperties$BaseState;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/pdfbox/pdmodel/graphics/optionalcontent/PDOptionalContentProperties;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "BaseState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lorg/apache/pdfbox/pdmodel/graphics/optionalcontent/PDOptionalContentProperties$BaseState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lorg/apache/pdfbox/pdmodel/graphics/optionalcontent/PDOptionalContentProperties$BaseState;

.field public static final enum OFF:Lorg/apache/pdfbox/pdmodel/graphics/optionalcontent/PDOptionalContentProperties$BaseState;

.field public static final enum ON:Lorg/apache/pdfbox/pdmodel/graphics/optionalcontent/PDOptionalContentProperties$BaseState;

.field public static final enum UNCHANGED:Lorg/apache/pdfbox/pdmodel/graphics/optionalcontent/PDOptionalContentProperties$BaseState;


# instance fields
.field private name:Lorg/apache/pdfbox/cos/COSName;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    new-instance v0, Lorg/apache/pdfbox/pdmodel/graphics/optionalcontent/PDOptionalContentProperties$BaseState;

    const/4 v1, 0x0

    sget-object v2, Lorg/apache/pdfbox/cos/COSName;->ON:Lorg/apache/pdfbox/cos/COSName;

    const-string v3, "ON"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/pdfbox/pdmodel/graphics/optionalcontent/PDOptionalContentProperties$BaseState;-><init>(Ljava/lang/String;ILorg/apache/pdfbox/cos/COSName;)V

    sput-object v0, Lorg/apache/pdfbox/pdmodel/graphics/optionalcontent/PDOptionalContentProperties$BaseState;->ON:Lorg/apache/pdfbox/pdmodel/graphics/optionalcontent/PDOptionalContentProperties$BaseState;

    new-instance v1, Lorg/apache/pdfbox/pdmodel/graphics/optionalcontent/PDOptionalContentProperties$BaseState;

    const/4 v2, 0x1

    sget-object v3, Lorg/apache/pdfbox/cos/COSName;->OFF:Lorg/apache/pdfbox/cos/COSName;

    const-string v4, "OFF"

    invoke-direct {v1, v4, v2, v3}, Lorg/apache/pdfbox/pdmodel/graphics/optionalcontent/PDOptionalContentProperties$BaseState;-><init>(Ljava/lang/String;ILorg/apache/pdfbox/cos/COSName;)V

    sput-object v1, Lorg/apache/pdfbox/pdmodel/graphics/optionalcontent/PDOptionalContentProperties$BaseState;->OFF:Lorg/apache/pdfbox/pdmodel/graphics/optionalcontent/PDOptionalContentProperties$BaseState;

    new-instance v2, Lorg/apache/pdfbox/pdmodel/graphics/optionalcontent/PDOptionalContentProperties$BaseState;

    const/4 v3, 0x2

    sget-object v4, Lorg/apache/pdfbox/cos/COSName;->UNCHANGED:Lorg/apache/pdfbox/cos/COSName;

    const-string v5, "UNCHANGED"

    invoke-direct {v2, v5, v3, v4}, Lorg/apache/pdfbox/pdmodel/graphics/optionalcontent/PDOptionalContentProperties$BaseState;-><init>(Ljava/lang/String;ILorg/apache/pdfbox/cos/COSName;)V

    sput-object v2, Lorg/apache/pdfbox/pdmodel/graphics/optionalcontent/PDOptionalContentProperties$BaseState;->UNCHANGED:Lorg/apache/pdfbox/pdmodel/graphics/optionalcontent/PDOptionalContentProperties$BaseState;

    filled-new-array {v0, v1, v2}, [Lorg/apache/pdfbox/pdmodel/graphics/optionalcontent/PDOptionalContentProperties$BaseState;

    move-result-object v0

    sput-object v0, Lorg/apache/pdfbox/pdmodel/graphics/optionalcontent/PDOptionalContentProperties$BaseState;->$VALUES:[Lorg/apache/pdfbox/pdmodel/graphics/optionalcontent/PDOptionalContentProperties$BaseState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILorg/apache/pdfbox/cos/COSName;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/pdfbox/cos/COSName;",
            ")V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lorg/apache/pdfbox/pdmodel/graphics/optionalcontent/PDOptionalContentProperties$BaseState;->name:Lorg/apache/pdfbox/cos/COSName;

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lorg/apache/pdfbox/pdmodel/graphics/optionalcontent/PDOptionalContentProperties$BaseState;
    .locals 1

    .line 1
    const-class v0, Lorg/apache/pdfbox/pdmodel/graphics/optionalcontent/PDOptionalContentProperties$BaseState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lorg/apache/pdfbox/pdmodel/graphics/optionalcontent/PDOptionalContentProperties$BaseState;

    return-object p0
.end method

.method public static valueOf(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/pdmodel/graphics/optionalcontent/PDOptionalContentProperties$BaseState;
    .locals 0

    .line 2
    if-nez p0, :cond_0

    sget-object p0, Lorg/apache/pdfbox/pdmodel/graphics/optionalcontent/PDOptionalContentProperties$BaseState;->ON:Lorg/apache/pdfbox/pdmodel/graphics/optionalcontent/PDOptionalContentProperties$BaseState;

    return-object p0

    :cond_0
    invoke-virtual {p0}, Lorg/apache/pdfbox/cos/COSName;->getName()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Lorg/apache/pdfbox/pdmodel/graphics/optionalcontent/PDOptionalContentProperties$BaseState;->valueOf(Ljava/lang/String;)Lorg/apache/pdfbox/pdmodel/graphics/optionalcontent/PDOptionalContentProperties$BaseState;

    move-result-object p0

    return-object p0
.end method

.method public static values()[Lorg/apache/pdfbox/pdmodel/graphics/optionalcontent/PDOptionalContentProperties$BaseState;
    .locals 1

    sget-object v0, Lorg/apache/pdfbox/pdmodel/graphics/optionalcontent/PDOptionalContentProperties$BaseState;->$VALUES:[Lorg/apache/pdfbox/pdmodel/graphics/optionalcontent/PDOptionalContentProperties$BaseState;

    invoke-virtual {v0}, [Lorg/apache/pdfbox/pdmodel/graphics/optionalcontent/PDOptionalContentProperties$BaseState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lorg/apache/pdfbox/pdmodel/graphics/optionalcontent/PDOptionalContentProperties$BaseState;

    return-object v0
.end method


# virtual methods
.method public getName()Lorg/apache/pdfbox/cos/COSName;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/optionalcontent/PDOptionalContentProperties$BaseState;->name:Lorg/apache/pdfbox/cos/COSName;

    return-object v0
.end method

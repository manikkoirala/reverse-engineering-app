.class public final Lorg/apache/pdfbox/pdmodel/graphics/color/PDDeviceGray;
.super Lorg/apache/pdfbox/pdmodel/graphics/color/PDDeviceColorSpace;
.source "SourceFile"


# static fields
.field public static final INSTANCE:Lorg/apache/pdfbox/pdmodel/graphics/color/PDDeviceGray;


# instance fields
.field private final initialColor:Lorg/apache/pdfbox/pdmodel/graphics/color/PDColor;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lorg/apache/pdfbox/pdmodel/graphics/color/PDDeviceGray;

    invoke-direct {v0}, Lorg/apache/pdfbox/pdmodel/graphics/color/PDDeviceGray;-><init>()V

    sput-object v0, Lorg/apache/pdfbox/pdmodel/graphics/color/PDDeviceGray;->INSTANCE:Lorg/apache/pdfbox/pdmodel/graphics/color/PDDeviceGray;

    return-void
.end method

.method private constructor <init>()V
    .locals 4

    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/graphics/color/PDDeviceColorSpace;-><init>()V

    new-instance v0, Lorg/apache/pdfbox/pdmodel/graphics/color/PDColor;

    const/4 v1, 0x1

    new-array v1, v1, [F

    const/4 v2, 0x0

    const/4 v3, 0x0

    aput v3, v1, v2

    invoke-direct {v0, v1, p0}, Lorg/apache/pdfbox/pdmodel/graphics/color/PDColor;-><init>([FLorg/apache/pdfbox/pdmodel/graphics/color/PDColorSpace;)V

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/color/PDDeviceGray;->initialColor:Lorg/apache/pdfbox/pdmodel/graphics/color/PDColor;

    return-void
.end method


# virtual methods
.method public getDefaultDecode(I)[F
    .locals 0

    const/4 p1, 0x2

    new-array p1, p1, [F

    fill-array-data p1, :array_0

    return-object p1

    nop

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public getInitialColor()Lorg/apache/pdfbox/pdmodel/graphics/color/PDColor;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/color/PDDeviceGray;->initialColor:Lorg/apache/pdfbox/pdmodel/graphics/color/PDColor;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->DEVICEGRAY:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSName;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getNumberOfComponents()I
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public toRGB([F)[F
    .locals 3

    const/4 v0, 0x3

    new-array v0, v0, [F

    const/4 v1, 0x0

    aget v2, p1, v1

    aput v2, v0, v1

    aget p1, p1, v1

    const/4 v1, 0x1

    aput p1, v0, v1

    const/4 v1, 0x2

    aput p1, v0, v1

    return-object v0
.end method

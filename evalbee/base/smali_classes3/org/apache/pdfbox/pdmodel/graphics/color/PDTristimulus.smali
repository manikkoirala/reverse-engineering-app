.class public final Lorg/apache/pdfbox/pdmodel/graphics/color/PDTristimulus;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/pdfbox/pdmodel/common/COSObjectable;


# instance fields
.field private values:Lorg/apache/pdfbox/cos/COSArray;


# direct methods
.method public constructor <init>()V
    .locals 3

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/color/PDTristimulus;->values:Lorg/apache/pdfbox/cos/COSArray;

    new-instance v0, Lorg/apache/pdfbox/cos/COSArray;

    invoke-direct {v0}, Lorg/apache/pdfbox/cos/COSArray;-><init>()V

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/color/PDTristimulus;->values:Lorg/apache/pdfbox/cos/COSArray;

    new-instance v1, Lorg/apache/pdfbox/cos/COSFloat;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lorg/apache/pdfbox/cos/COSFloat;-><init>(F)V

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/color/PDTristimulus;->values:Lorg/apache/pdfbox/cos/COSArray;

    new-instance v1, Lorg/apache/pdfbox/cos/COSFloat;

    invoke-direct {v1, v2}, Lorg/apache/pdfbox/cos/COSFloat;-><init>(F)V

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/color/PDTristimulus;->values:Lorg/apache/pdfbox/cos/COSArray;

    new-instance v1, Lorg/apache/pdfbox/cos/COSFloat;

    invoke-direct {v1, v2}, Lorg/apache/pdfbox/cos/COSFloat;-><init>(F)V

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    return-void
.end method

.method public constructor <init>(Lorg/apache/pdfbox/cos/COSArray;)V
    .locals 0

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/graphics/color/PDTristimulus;->values:Lorg/apache/pdfbox/cos/COSArray;

    return-void
.end method

.method public constructor <init>([F)V
    .locals 4

    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/color/PDTristimulus;->values:Lorg/apache/pdfbox/cos/COSArray;

    new-instance v0, Lorg/apache/pdfbox/cos/COSArray;

    invoke-direct {v0}, Lorg/apache/pdfbox/cos/COSArray;-><init>()V

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/color/PDTristimulus;->values:Lorg/apache/pdfbox/cos/COSArray;

    const/4 v0, 0x0

    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_0

    const/4 v1, 0x3

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lorg/apache/pdfbox/pdmodel/graphics/color/PDTristimulus;->values:Lorg/apache/pdfbox/cos/COSArray;

    new-instance v2, Lorg/apache/pdfbox/cos/COSFloat;

    aget v3, p1, v0

    invoke-direct {v2, v3}, Lorg/apache/pdfbox/cos/COSFloat;-><init>(F)V

    invoke-virtual {v1, v2}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method public getCOSObject()Lorg/apache/pdfbox/cos/COSBase;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/color/PDTristimulus;->values:Lorg/apache/pdfbox/cos/COSArray;

    return-object v0
.end method

.method public getX()F
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/color/PDTristimulus;->values:Lorg/apache/pdfbox/cos/COSArray;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSArray;->get(I)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSNumber;

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSNumber;->floatValue()F

    move-result v0

    return v0
.end method

.method public getY()F
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/color/PDTristimulus;->values:Lorg/apache/pdfbox/cos/COSArray;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSArray;->get(I)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSNumber;

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSNumber;->floatValue()F

    move-result v0

    return v0
.end method

.method public getZ()F
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/color/PDTristimulus;->values:Lorg/apache/pdfbox/cos/COSArray;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSArray;->get(I)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSNumber;

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSNumber;->floatValue()F

    move-result v0

    return v0
.end method

.method public setX(F)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/color/PDTristimulus;->values:Lorg/apache/pdfbox/cos/COSArray;

    new-instance v1, Lorg/apache/pdfbox/cos/COSFloat;

    invoke-direct {v1, p1}, Lorg/apache/pdfbox/cos/COSFloat;-><init>(F)V

    const/4 p1, 0x0

    invoke-virtual {v0, p1, v1}, Lorg/apache/pdfbox/cos/COSArray;->set(ILorg/apache/pdfbox/cos/COSBase;)V

    return-void
.end method

.method public setY(F)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/color/PDTristimulus;->values:Lorg/apache/pdfbox/cos/COSArray;

    new-instance v1, Lorg/apache/pdfbox/cos/COSFloat;

    invoke-direct {v1, p1}, Lorg/apache/pdfbox/cos/COSFloat;-><init>(F)V

    const/4 p1, 0x1

    invoke-virtual {v0, p1, v1}, Lorg/apache/pdfbox/cos/COSArray;->set(ILorg/apache/pdfbox/cos/COSBase;)V

    return-void
.end method

.method public setZ(F)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/color/PDTristimulus;->values:Lorg/apache/pdfbox/cos/COSArray;

    new-instance v1, Lorg/apache/pdfbox/cos/COSFloat;

    invoke-direct {v1, p1}, Lorg/apache/pdfbox/cos/COSFloat;-><init>(F)V

    const/4 p1, 0x2

    invoke-virtual {v0, p1, v1}, Lorg/apache/pdfbox/cos/COSArray;->set(ILorg/apache/pdfbox/cos/COSBase;)V

    return-void
.end method

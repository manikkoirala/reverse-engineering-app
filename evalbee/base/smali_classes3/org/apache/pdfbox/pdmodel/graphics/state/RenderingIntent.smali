.class public final enum Lorg/apache/pdfbox/pdmodel/graphics/state/RenderingIntent;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lorg/apache/pdfbox/pdmodel/graphics/state/RenderingIntent;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lorg/apache/pdfbox/pdmodel/graphics/state/RenderingIntent;

.field public static final enum ABSOLUTE_COLORIMETRIC:Lorg/apache/pdfbox/pdmodel/graphics/state/RenderingIntent;

.field public static final enum PERCEPTUAL:Lorg/apache/pdfbox/pdmodel/graphics/state/RenderingIntent;

.field public static final enum RELATIVE_COLORIMETRIC:Lorg/apache/pdfbox/pdmodel/graphics/state/RenderingIntent;

.field public static final enum SATURATION:Lorg/apache/pdfbox/pdmodel/graphics/state/RenderingIntent;


# instance fields
.field private final value:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    new-instance v0, Lorg/apache/pdfbox/pdmodel/graphics/state/RenderingIntent;

    const/4 v1, 0x0

    const-string v2, "AbsoluteColorimetric"

    const-string v3, "ABSOLUTE_COLORIMETRIC"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/pdfbox/pdmodel/graphics/state/RenderingIntent;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/pdmodel/graphics/state/RenderingIntent;->ABSOLUTE_COLORIMETRIC:Lorg/apache/pdfbox/pdmodel/graphics/state/RenderingIntent;

    new-instance v1, Lorg/apache/pdfbox/pdmodel/graphics/state/RenderingIntent;

    const/4 v2, 0x1

    const-string v3, "RelativeColorimetric"

    const-string v4, "RELATIVE_COLORIMETRIC"

    invoke-direct {v1, v4, v2, v3}, Lorg/apache/pdfbox/pdmodel/graphics/state/RenderingIntent;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lorg/apache/pdfbox/pdmodel/graphics/state/RenderingIntent;->RELATIVE_COLORIMETRIC:Lorg/apache/pdfbox/pdmodel/graphics/state/RenderingIntent;

    new-instance v2, Lorg/apache/pdfbox/pdmodel/graphics/state/RenderingIntent;

    const/4 v3, 0x2

    const-string v4, "Saturation"

    const-string v5, "SATURATION"

    invoke-direct {v2, v5, v3, v4}, Lorg/apache/pdfbox/pdmodel/graphics/state/RenderingIntent;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v2, Lorg/apache/pdfbox/pdmodel/graphics/state/RenderingIntent;->SATURATION:Lorg/apache/pdfbox/pdmodel/graphics/state/RenderingIntent;

    new-instance v3, Lorg/apache/pdfbox/pdmodel/graphics/state/RenderingIntent;

    const/4 v4, 0x3

    const-string v5, "Perceptual"

    const-string v6, "PERCEPTUAL"

    invoke-direct {v3, v6, v4, v5}, Lorg/apache/pdfbox/pdmodel/graphics/state/RenderingIntent;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v3, Lorg/apache/pdfbox/pdmodel/graphics/state/RenderingIntent;->PERCEPTUAL:Lorg/apache/pdfbox/pdmodel/graphics/state/RenderingIntent;

    filled-new-array {v0, v1, v2, v3}, [Lorg/apache/pdfbox/pdmodel/graphics/state/RenderingIntent;

    move-result-object v0

    sput-object v0, Lorg/apache/pdfbox/pdmodel/graphics/state/RenderingIntent;->$VALUES:[Lorg/apache/pdfbox/pdmodel/graphics/state/RenderingIntent;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lorg/apache/pdfbox/pdmodel/graphics/state/RenderingIntent;->value:Ljava/lang/String;

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lorg/apache/pdfbox/pdmodel/graphics/state/RenderingIntent;
    .locals 1

    const-string v0, "AbsoluteColorimetric"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object p0, Lorg/apache/pdfbox/pdmodel/graphics/state/RenderingIntent;->ABSOLUTE_COLORIMETRIC:Lorg/apache/pdfbox/pdmodel/graphics/state/RenderingIntent;

    return-object p0

    :cond_0
    const-string v0, "RelativeColorimetric"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object p0, Lorg/apache/pdfbox/pdmodel/graphics/state/RenderingIntent;->RELATIVE_COLORIMETRIC:Lorg/apache/pdfbox/pdmodel/graphics/state/RenderingIntent;

    return-object p0

    :cond_1
    const-string v0, "Saturation"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object p0, Lorg/apache/pdfbox/pdmodel/graphics/state/RenderingIntent;->SATURATION:Lorg/apache/pdfbox/pdmodel/graphics/state/RenderingIntent;

    return-object p0

    :cond_2
    const-string v0, "Perceptual"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object p0, Lorg/apache/pdfbox/pdmodel/graphics/state/RenderingIntent;->PERCEPTUAL:Lorg/apache/pdfbox/pdmodel/graphics/state/RenderingIntent;

    return-object p0

    :cond_3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static valueOf(Ljava/lang/String;)Lorg/apache/pdfbox/pdmodel/graphics/state/RenderingIntent;
    .locals 1

    const-class v0, Lorg/apache/pdfbox/pdmodel/graphics/state/RenderingIntent;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lorg/apache/pdfbox/pdmodel/graphics/state/RenderingIntent;

    return-object p0
.end method

.method public static values()[Lorg/apache/pdfbox/pdmodel/graphics/state/RenderingIntent;
    .locals 1

    sget-object v0, Lorg/apache/pdfbox/pdmodel/graphics/state/RenderingIntent;->$VALUES:[Lorg/apache/pdfbox/pdmodel/graphics/state/RenderingIntent;

    invoke-virtual {v0}, [Lorg/apache/pdfbox/pdmodel/graphics/state/RenderingIntent;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lorg/apache/pdfbox/pdmodel/graphics/state/RenderingIntent;

    return-object v0
.end method


# virtual methods
.method public stringValue()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/state/RenderingIntent;->value:Ljava/lang/String;

    return-object v0
.end method

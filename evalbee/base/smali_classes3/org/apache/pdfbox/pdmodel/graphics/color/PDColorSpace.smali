.class public abstract Lorg/apache/pdfbox/pdmodel/graphics/color/PDColorSpace;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/pdfbox/pdmodel/common/COSObjectable;


# instance fields
.field protected array:Lorg/apache/pdfbox/cos/COSArray;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static create(Lorg/apache/pdfbox/cos/COSBase;)Lorg/apache/pdfbox/pdmodel/graphics/color/PDColorSpace;
    .locals 1

    .line 1
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lorg/apache/pdfbox/pdmodel/graphics/color/PDColorSpace;->create(Lorg/apache/pdfbox/cos/COSBase;Lorg/apache/pdfbox/pdmodel/PDResources;)Lorg/apache/pdfbox/pdmodel/graphics/color/PDColorSpace;

    move-result-object p0

    return-object p0
.end method

.method public static create(Lorg/apache/pdfbox/cos/COSBase;Lorg/apache/pdfbox/pdmodel/PDResources;)Lorg/apache/pdfbox/pdmodel/graphics/color/PDColorSpace;
    .locals 2

    .line 2
    instance-of v0, p0, Lorg/apache/pdfbox/cos/COSObject;

    if-eqz v0, :cond_0

    check-cast p0, Lorg/apache/pdfbox/cos/COSObject;

    invoke-virtual {p0}, Lorg/apache/pdfbox/cos/COSObject;->getObject()Lorg/apache/pdfbox/cos/COSBase;

    move-result-object p0

    invoke-static {p0, p1}, Lorg/apache/pdfbox/pdmodel/graphics/color/PDColorSpace;->create(Lorg/apache/pdfbox/cos/COSBase;Lorg/apache/pdfbox/pdmodel/PDResources;)Lorg/apache/pdfbox/pdmodel/graphics/color/PDColorSpace;

    move-result-object p0

    return-object p0

    :cond_0
    instance-of v0, p0, Lorg/apache/pdfbox/cos/COSName;

    if-eqz v0, :cond_b

    check-cast p0, Lorg/apache/pdfbox/cos/COSName;

    if-eqz p1, :cond_4

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->DEVICECMYK:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p0, v0}, Lorg/apache/pdfbox/cos/COSName;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->DEFAULT_CMYK:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p1, v0}, Lorg/apache/pdfbox/pdmodel/PDResources;->hasColorSpace(Lorg/apache/pdfbox/cos/COSName;)Z

    move-result v1

    if-eqz v1, :cond_1

    goto :goto_0

    :cond_1
    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->DEVICERGB:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p0, v0}, Lorg/apache/pdfbox/cos/COSName;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->DEFAULT_RGB:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p1, v0}, Lorg/apache/pdfbox/pdmodel/PDResources;->hasColorSpace(Lorg/apache/pdfbox/cos/COSName;)Z

    move-result v1

    if-eqz v1, :cond_2

    goto :goto_0

    :cond_2
    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->DEVICEGRAY:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p0, v0}, Lorg/apache/pdfbox/cos/COSName;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->DEFAULT_GRAY:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p1, v0}, Lorg/apache/pdfbox/pdmodel/PDResources;->hasColorSpace(Lorg/apache/pdfbox/cos/COSName;)Z

    move-result v1

    if-eqz v1, :cond_3

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v0}, Lorg/apache/pdfbox/pdmodel/PDResources;->hasColorSpace(Lorg/apache/pdfbox/cos/COSName;)Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-virtual {p1, v0}, Lorg/apache/pdfbox/pdmodel/PDResources;->getColorSpace(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/pdmodel/graphics/color/PDColorSpace;

    move-result-object p0

    return-object p0

    :cond_4
    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->DEVICERGB:Lorg/apache/pdfbox/cos/COSName;

    if-eq p0, v0, :cond_a

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->RGB:Lorg/apache/pdfbox/cos/COSName;

    if-ne p0, v0, :cond_5

    goto :goto_2

    :cond_5
    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->DEVICEGRAY:Lorg/apache/pdfbox/cos/COSName;

    if-eq p0, v0, :cond_9

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->G:Lorg/apache/pdfbox/cos/COSName;

    if-ne p0, v0, :cond_6

    goto :goto_1

    :cond_6
    if-eqz p1, :cond_8

    invoke-virtual {p1, p0}, Lorg/apache/pdfbox/pdmodel/PDResources;->hasColorSpace(Lorg/apache/pdfbox/cos/COSName;)Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-virtual {p1, p0}, Lorg/apache/pdfbox/pdmodel/PDResources;->getColorSpace(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/pdmodel/graphics/color/PDColorSpace;

    move-result-object p0

    return-object p0

    :cond_7
    new-instance p1, Lorg/apache/pdfbox/pdmodel/MissingResourceException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Missing color space: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/apache/pdfbox/cos/COSName;->getName()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {p1, p0}, Lorg/apache/pdfbox/pdmodel/MissingResourceException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_8
    new-instance p1, Lorg/apache/pdfbox/pdmodel/MissingResourceException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unknown color space: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/apache/pdfbox/cos/COSName;->getName()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {p1, p0}, Lorg/apache/pdfbox/pdmodel/MissingResourceException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_9
    :goto_1
    sget-object p0, Lorg/apache/pdfbox/pdmodel/graphics/color/PDDeviceGray;->INSTANCE:Lorg/apache/pdfbox/pdmodel/graphics/color/PDDeviceGray;

    return-object p0

    :cond_a
    :goto_2
    sget-object p0, Lorg/apache/pdfbox/pdmodel/graphics/color/PDDeviceRGB;->INSTANCE:Lorg/apache/pdfbox/pdmodel/graphics/color/PDDeviceRGB;

    return-object p0

    :cond_b
    instance-of p1, p0, Lorg/apache/pdfbox/cos/COSArray;

    if-eqz p1, :cond_c

    check-cast p0, Lorg/apache/pdfbox/cos/COSArray;

    const/4 p1, 0x0

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/cos/COSArray;->get(I)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object p0

    check-cast p0, Lorg/apache/pdfbox/cos/COSName;

    new-instance p1, Ljava/io/IOException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Invalid color space kind: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {p1, p0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_c
    new-instance p1, Ljava/io/IOException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Expected a name or array but got: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {p1, p0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public getCOSObject()Lorg/apache/pdfbox/cos/COSBase;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/color/PDColorSpace;->array:Lorg/apache/pdfbox/cos/COSArray;

    return-object v0
.end method

.method public abstract getDefaultDecode(I)[F
.end method

.method public abstract getInitialColor()Lorg/apache/pdfbox/pdmodel/graphics/color/PDColor;
.end method

.method public abstract getName()Ljava/lang/String;
.end method

.method public abstract getNumberOfComponents()I
.end method

.method public abstract toRGB([F)[F
.end method

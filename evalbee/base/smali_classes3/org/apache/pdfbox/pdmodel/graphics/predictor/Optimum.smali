.class public Lorg/apache/pdfbox/pdmodel/graphics/predictor/Optimum;
.super Lorg/apache/pdfbox/pdmodel/graphics/predictor/PredictorAlgorithm;
.source "SourceFile"


# instance fields
.field filter:[Lorg/apache/pdfbox/pdmodel/graphics/predictor/PredictorAlgorithm;


# direct methods
.method public constructor <init>()V
    .locals 3

    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/graphics/predictor/PredictorAlgorithm;-><init>()V

    const/4 v0, 0x5

    new-array v0, v0, [Lorg/apache/pdfbox/pdmodel/graphics/predictor/PredictorAlgorithm;

    new-instance v1, Lorg/apache/pdfbox/pdmodel/graphics/predictor/None;

    invoke-direct {v1}, Lorg/apache/pdfbox/pdmodel/graphics/predictor/None;-><init>()V

    const/4 v2, 0x0

    aput-object v1, v0, v2

    new-instance v1, Lorg/apache/pdfbox/pdmodel/graphics/predictor/Sub;

    invoke-direct {v1}, Lorg/apache/pdfbox/pdmodel/graphics/predictor/Sub;-><init>()V

    const/4 v2, 0x1

    aput-object v1, v0, v2

    new-instance v1, Lorg/apache/pdfbox/pdmodel/graphics/predictor/Up;

    invoke-direct {v1}, Lorg/apache/pdfbox/pdmodel/graphics/predictor/Up;-><init>()V

    const/4 v2, 0x2

    aput-object v1, v0, v2

    new-instance v1, Lorg/apache/pdfbox/pdmodel/graphics/predictor/Average;

    invoke-direct {v1}, Lorg/apache/pdfbox/pdmodel/graphics/predictor/Average;-><init>()V

    const/4 v2, 0x3

    aput-object v1, v0, v2

    new-instance v1, Lorg/apache/pdfbox/pdmodel/graphics/predictor/Paeth;

    invoke-direct {v1}, Lorg/apache/pdfbox/pdmodel/graphics/predictor/Paeth;-><init>()V

    const/4 v2, 0x4

    aput-object v1, v0, v2

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/predictor/Optimum;->filter:[Lorg/apache/pdfbox/pdmodel/graphics/predictor/PredictorAlgorithm;

    return-void
.end method


# virtual methods
.method public checkBufsiz([B[B)V
    .locals 3

    array-length v0, p1

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/predictor/PredictorAlgorithm;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/predictor/PredictorAlgorithm;->getBpp()I

    move-result v2

    mul-int/2addr v1, v2

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/predictor/PredictorAlgorithm;->getHeight()I

    move-result v2

    mul-int/2addr v1, v2

    const-string v2, ","

    if-ne v0, v1, :cond_1

    array-length p1, p2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/predictor/PredictorAlgorithm;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/predictor/PredictorAlgorithm;->getHeight()I

    move-result v1

    mul-int/2addr v0, v1

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/predictor/PredictorAlgorithm;->getBpp()I

    move-result v1

    mul-int/2addr v0, v1

    if-ne p1, v0, :cond_0

    return-void

    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "raw.length != width * height * bpp, raw.length="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    array-length p2, p2

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p2, " w,h,bpp="

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/predictor/PredictorAlgorithm;->getWidth()I

    move-result p2

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/predictor/PredictorAlgorithm;->getHeight()I

    move-result p2

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/predictor/PredictorAlgorithm;->getBpp()I

    move-result p2

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    new-instance p2, Ljava/lang/IllegalArgumentException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "filtered.length != (width*bpp + 1) * height, "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    array-length p1, p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, " "

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/predictor/PredictorAlgorithm;->getWidth()I

    move-result p1

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/predictor/PredictorAlgorithm;->getBpp()I

    move-result v1

    mul-int/2addr p1, v1

    add-int/lit8 p1, p1, 0x1

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/predictor/PredictorAlgorithm;->getHeight()I

    move-result v1

    mul-int/2addr p1, v1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, "w,h,bpp="

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/predictor/PredictorAlgorithm;->getWidth()I

    move-result p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/predictor/PredictorAlgorithm;->getHeight()I

    move-result p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/predictor/PredictorAlgorithm;->getBpp()I

    move-result p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p2
.end method

.method public decode([B[B)V
    .locals 10

    invoke-virtual {p0, p1, p2}, Lorg/apache/pdfbox/pdmodel/graphics/predictor/Optimum;->checkBufsiz([B[B)V

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/predictor/PredictorAlgorithm;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/predictor/PredictorAlgorithm;->getBpp()I

    move-result v1

    mul-int/2addr v0, v1

    add-int/lit8 v1, v0, 0x1

    const/4 v2, 0x0

    move v9, v2

    :goto_0
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/predictor/PredictorAlgorithm;->getHeight()I

    move-result v2

    if-ge v9, v2, :cond_0

    iget-object v2, p0, Lorg/apache/pdfbox/pdmodel/graphics/predictor/Optimum;->filter:[Lorg/apache/pdfbox/pdmodel/graphics/predictor/PredictorAlgorithm;

    mul-int v3, v9, v1

    aget-byte v4, p1, v3

    aget-object v2, v2, v4

    add-int/lit8 v6, v3, 0x1

    mul-int v8, v9, v0

    move-object v3, p1

    move-object v4, p2

    move v5, v1

    move v7, v0

    invoke-virtual/range {v2 .. v8}, Lorg/apache/pdfbox/pdmodel/graphics/predictor/PredictorAlgorithm;->decodeLine([B[BIIII)V

    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public decodeLine([B[BIIII)V
    .locals 0

    new-instance p1, Ljava/lang/UnsupportedOperationException;

    const-string p2, "decodeLine"

    invoke-direct {p1, p2}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public encode([B[B)V
    .locals 0

    invoke-virtual {p0, p2, p1}, Lorg/apache/pdfbox/pdmodel/graphics/predictor/Optimum;->checkBufsiz([B[B)V

    new-instance p1, Ljava/lang/UnsupportedOperationException;

    const-string p2, "encode"

    invoke-direct {p1, p2}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public encodeLine([B[BIIII)V
    .locals 0

    new-instance p1, Ljava/lang/UnsupportedOperationException;

    const-string p2, "encodeLine"

    invoke-direct {p1, p2}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public setBpp(I)V
    .locals 3

    invoke-super {p0, p1}, Lorg/apache/pdfbox/pdmodel/graphics/predictor/PredictorAlgorithm;->setBpp(I)V

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lorg/apache/pdfbox/pdmodel/graphics/predictor/Optimum;->filter:[Lorg/apache/pdfbox/pdmodel/graphics/predictor/PredictorAlgorithm;

    array-length v2, v1

    if-ge v0, v2, :cond_0

    aget-object v1, v1, v0

    invoke-virtual {v1, p1}, Lorg/apache/pdfbox/pdmodel/graphics/predictor/PredictorAlgorithm;->setBpp(I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public setHeight(I)V
    .locals 3

    invoke-super {p0, p1}, Lorg/apache/pdfbox/pdmodel/graphics/predictor/PredictorAlgorithm;->setHeight(I)V

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lorg/apache/pdfbox/pdmodel/graphics/predictor/Optimum;->filter:[Lorg/apache/pdfbox/pdmodel/graphics/predictor/PredictorAlgorithm;

    array-length v2, v1

    if-ge v0, v2, :cond_0

    aget-object v1, v1, v0

    invoke-virtual {v1, p1}, Lorg/apache/pdfbox/pdmodel/graphics/predictor/PredictorAlgorithm;->setHeight(I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public setWidth(I)V
    .locals 3

    invoke-super {p0, p1}, Lorg/apache/pdfbox/pdmodel/graphics/predictor/PredictorAlgorithm;->setWidth(I)V

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lorg/apache/pdfbox/pdmodel/graphics/predictor/Optimum;->filter:[Lorg/apache/pdfbox/pdmodel/graphics/predictor/PredictorAlgorithm;

    array-length v2, v1

    if-ge v0, v2, :cond_0

    aget-object v1, v1, v0

    invoke-virtual {v1, p1}, Lorg/apache/pdfbox/pdmodel/graphics/predictor/PredictorAlgorithm;->setWidth(I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

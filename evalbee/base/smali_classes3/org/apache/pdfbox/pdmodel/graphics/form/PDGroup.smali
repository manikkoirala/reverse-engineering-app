.class public final Lorg/apache/pdfbox/pdmodel/graphics/form/PDGroup;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/pdfbox/pdmodel/common/COSObjectable;


# instance fields
.field private colorSpace:Lorg/apache/pdfbox/pdmodel/graphics/color/PDColorSpace;

.field private dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

.field private subType:Lorg/apache/pdfbox/cos/COSName;


# direct methods
.method public constructor <init>(Lorg/apache/pdfbox/cos/COSDictionary;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/graphics/form/PDGroup;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    return-void
.end method


# virtual methods
.method public getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/form/PDGroup;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    return-object v0
.end method

.method public getCOSObject()Lorg/apache/pdfbox/cos/COSBase;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/form/PDGroup;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    return-object v0
.end method

.method public getSubType()Lorg/apache/pdfbox/cos/COSName;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/form/PDGroup;->subType:Lorg/apache/pdfbox/cos/COSName;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/form/PDGroup;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->S:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSName;

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/form/PDGroup;->subType:Lorg/apache/pdfbox/cos/COSName;

    :cond_0
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/form/PDGroup;->subType:Lorg/apache/pdfbox/cos/COSName;

    return-object v0
.end method

.method public isIsolated()Z
    .locals 3

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/form/PDGroup;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->I:Lorg/apache/pdfbox/cos/COSName;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->getBoolean(Lorg/apache/pdfbox/cos/COSName;Z)Z

    move-result v0

    return v0
.end method

.method public isKnockout()Z
    .locals 3

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/form/PDGroup;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->K:Lorg/apache/pdfbox/cos/COSName;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->getBoolean(Lorg/apache/pdfbox/cos/COSName;Z)Z

    move-result v0

    return v0
.end method

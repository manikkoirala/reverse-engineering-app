.class abstract Lorg/apache/pdfbox/pdmodel/graphics/shading/PDTriangleBasedShadingType;
.super Lorg/apache/pdfbox/pdmodel/graphics/shading/PDShading;
.source "SourceFile"


# instance fields
.field private decode:Lorg/apache/pdfbox/cos/COSArray;


# direct methods
.method public constructor <init>(Lorg/apache/pdfbox/cos/COSDictionary;)V
    .locals 0

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/graphics/shading/PDShading;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    const/4 p1, 0x0

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/graphics/shading/PDTriangleBasedShadingType;->decode:Lorg/apache/pdfbox/cos/COSArray;

    return-void
.end method

.method private getDecodeValues()Lorg/apache/pdfbox/cos/COSArray;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/shading/PDTriangleBasedShadingType;->decode:Lorg/apache/pdfbox/cos/COSArray;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/shading/PDShading;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->DECODE:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSArray;

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/shading/PDTriangleBasedShadingType;->decode:Lorg/apache/pdfbox/cos/COSArray;

    :cond_0
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/shading/PDTriangleBasedShadingType;->decode:Lorg/apache/pdfbox/cos/COSArray;

    return-object v0
.end method


# virtual methods
.method public getBitsPerComponent()I
    .locals 3

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/shading/PDShading;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->BITS_PER_COMPONENT:Lorg/apache/pdfbox/cos/COSName;

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->getInt(Lorg/apache/pdfbox/cos/COSName;I)I

    move-result v0

    return v0
.end method

.method public getBitsPerCoordinate()I
    .locals 3

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/shading/PDShading;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->BITS_PER_COORDINATE:Lorg/apache/pdfbox/cos/COSName;

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->getInt(Lorg/apache/pdfbox/cos/COSName;I)I

    move-result v0

    return v0
.end method

.method public getDecodeForParameter(I)Lorg/apache/pdfbox/pdmodel/common/PDRange;
    .locals 3

    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/graphics/shading/PDTriangleBasedShadingType;->getDecodeValues()Lorg/apache/pdfbox/cos/COSArray;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSArray;->size()I

    move-result v1

    mul-int/lit8 v2, p1, 0x2

    add-int/lit8 v2, v2, 0x1

    if-lt v1, v2, :cond_0

    new-instance v1, Lorg/apache/pdfbox/pdmodel/common/PDRange;

    invoke-direct {v1, v0, p1}, Lorg/apache/pdfbox/pdmodel/common/PDRange;-><init>(Lorg/apache/pdfbox/cos/COSArray;I)V

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return-object v1
.end method

.method public setBitsPerComponent(I)V
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/shading/PDShading;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->BITS_PER_COMPONENT:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setInt(Lorg/apache/pdfbox/cos/COSName;I)V

    return-void
.end method

.method public setBitsPerCoordinate(I)V
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/shading/PDShading;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->BITS_PER_COORDINATE:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setInt(Lorg/apache/pdfbox/cos/COSName;I)V

    return-void
.end method

.method public setDecodeValues(Lorg/apache/pdfbox/cos/COSArray;)V
    .locals 2

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/graphics/shading/PDTriangleBasedShadingType;->decode:Lorg/apache/pdfbox/cos/COSArray;

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/shading/PDShading;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->DECODE:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    return-void
.end method

.class public final Lorg/apache/pdfbox/pdmodel/graphics/image/JPEGFactory;
.super Lorg/apache/pdfbox/pdmodel/graphics/image/ImageFactory;
.source "SourceFile"


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/graphics/image/ImageFactory;-><init>()V

    return-void
.end method

.method public static createFromStream(Lorg/apache/pdfbox/pdmodel/PDDocument;Ljava/io/InputStream;)Lorg/apache/pdfbox/pdmodel/graphics/image/PDImageXObject;
    .locals 9

    new-instance v2, Ljava/io/ByteArrayInputStream;

    invoke-static {p1}, Lorg/apache/pdfbox/io/IOUtils;->toByteArray(Ljava/io/InputStream;)[B

    move-result-object p1

    invoke-direct {v2, p1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-static {v2}, Lorg/apache/pdfbox/pdmodel/graphics/image/JPEGFactory;->readJPEG(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object p1

    invoke-virtual {v2}, Ljava/io/ByteArrayInputStream;->reset()V

    new-instance v8, Lorg/apache/pdfbox/pdmodel/graphics/image/PDImageXObject;

    sget-object v3, Lorg/apache/pdfbox/cos/COSName;->DCT_DECODE:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    const/16 v6, 0x8

    sget-object v7, Lorg/apache/pdfbox/pdmodel/graphics/color/PDDeviceRGB;->INSTANCE:Lorg/apache/pdfbox/pdmodel/graphics/color/PDDeviceRGB;

    move-object v0, v8

    move-object v1, p0

    invoke-direct/range {v0 .. v7}, Lorg/apache/pdfbox/pdmodel/graphics/image/PDImageXObject;-><init>(Lorg/apache/pdfbox/pdmodel/PDDocument;Ljava/io/InputStream;Lorg/apache/pdfbox/cos/COSBase;IIILorg/apache/pdfbox/pdmodel/graphics/color/PDColorSpace;)V

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->hasAlpha()Z

    move-result p0

    if-nez p0, :cond_0

    return-object v8

    :cond_0
    new-instance p0, Ljava/lang/UnsupportedOperationException;

    const-string p1, "alpha channel not implemented"

    invoke-direct {p0, p1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method private static readJPEG(Ljava/io/InputStream;)Landroid/graphics/Bitmap;
    .locals 0

    invoke-static {p0}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object p0

    return-object p0
.end method

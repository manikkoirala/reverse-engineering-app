.class public interface abstract Lorg/apache/pdfbox/pdmodel/graphics/image/PDImage;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/pdfbox/pdmodel/common/COSObjectable;


# virtual methods
.method public abstract getBitsPerComponent()I
.end method

.method public abstract getColorSpace()Lorg/apache/pdfbox/pdmodel/graphics/color/PDColorSpace;
.end method

.method public abstract getDecode()Lorg/apache/pdfbox/cos/COSArray;
.end method

.method public abstract getHeight()I
.end method

.method public abstract getImage()Landroid/graphics/Bitmap;
.end method

.method public abstract getInterpolate()Z
.end method

.method public abstract getStream()Lorg/apache/pdfbox/pdmodel/common/PDStream;
.end method

.method public abstract getSuffix()Ljava/lang/String;
.end method

.method public abstract getWidth()I
.end method

.method public abstract isStencil()Z
.end method

.method public abstract setBitsPerComponent(I)V
.end method

.method public abstract setColorSpace(Lorg/apache/pdfbox/pdmodel/graphics/color/PDColorSpace;)V
.end method

.method public abstract setDecode(Lorg/apache/pdfbox/cos/COSArray;)V
.end method

.method public abstract setHeight(I)V
.end method

.method public abstract setInterpolate(Z)V
.end method

.method public abstract setStencil(Z)V
.end method

.method public abstract setWidth(I)V
.end method

.class public abstract Lorg/apache/pdfbox/pdmodel/graphics/predictor/PredictorAlgorithm;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private bpp:I

.field private height:I

.field private width:I


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static dump([B)V
    .locals 4

    const/4 v0, 0x0

    :goto_0
    array-length v1, p0

    if-ge v0, v1, :cond_0

    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    aget-byte v3, p0, v0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->print(Ljava/lang/String;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    sget-object p0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {p0}, Ljava/io/PrintStream;->println()V

    return-void
.end method

.method public static getFilter(I)Lorg/apache/pdfbox/pdmodel/graphics/predictor/PredictorAlgorithm;
    .locals 0

    packed-switch p0, :pswitch_data_0

    new-instance p0, Lorg/apache/pdfbox/pdmodel/graphics/predictor/None;

    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/graphics/predictor/None;-><init>()V

    goto :goto_0

    :pswitch_0
    new-instance p0, Lorg/apache/pdfbox/pdmodel/graphics/predictor/Optimum;

    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/graphics/predictor/Optimum;-><init>()V

    goto :goto_0

    :pswitch_1
    new-instance p0, Lorg/apache/pdfbox/pdmodel/graphics/predictor/Paeth;

    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/graphics/predictor/Paeth;-><init>()V

    goto :goto_0

    :pswitch_2
    new-instance p0, Lorg/apache/pdfbox/pdmodel/graphics/predictor/Average;

    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/graphics/predictor/Average;-><init>()V

    goto :goto_0

    :pswitch_3
    new-instance p0, Lorg/apache/pdfbox/pdmodel/graphics/predictor/Up;

    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/graphics/predictor/Up;-><init>()V

    goto :goto_0

    :pswitch_4
    new-instance p0, Lorg/apache/pdfbox/pdmodel/graphics/predictor/Sub;

    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/graphics/predictor/Sub;-><init>()V

    goto :goto_0

    :pswitch_5
    new-instance p0, Lorg/apache/pdfbox/pdmodel/graphics/predictor/None;

    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/graphics/predictor/None;-><init>()V

    :goto_0
    return-object p0

    nop

    :pswitch_data_0
    .packed-switch 0xa
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static main([Ljava/lang/String;)V
    .locals 6

    new-instance p0, Ljava/util/Random;

    invoke-direct {p0}, Ljava/util/Random;-><init>()V

    const/16 v0, 0x4b

    new-array v1, v0, [B

    invoke-virtual {p0, v1}, Ljava/util/Random;->nextBytes([B)V

    sget-object p0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v2, "raw:   "

    invoke-virtual {p0, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    invoke-static {v1}, Lorg/apache/pdfbox/pdmodel/graphics/predictor/PredictorAlgorithm;->dump([B)V

    const/16 p0, 0xa

    :goto_0
    const/16 v2, 0xf

    if-ge p0, v2, :cond_0

    new-array v2, v0, [B

    new-array v3, v0, [B

    invoke-static {p0}, Lorg/apache/pdfbox/pdmodel/graphics/predictor/PredictorAlgorithm;->getFilter(I)Lorg/apache/pdfbox/pdmodel/graphics/predictor/PredictorAlgorithm;

    move-result-object v4

    const/4 v5, 0x5

    invoke-virtual {v4, v5}, Lorg/apache/pdfbox/pdmodel/graphics/predictor/PredictorAlgorithm;->setWidth(I)V

    invoke-virtual {v4, v5}, Lorg/apache/pdfbox/pdmodel/graphics/predictor/PredictorAlgorithm;->setHeight(I)V

    const/4 v5, 0x3

    invoke-virtual {v4, v5}, Lorg/apache/pdfbox/pdmodel/graphics/predictor/PredictorAlgorithm;->setBpp(I)V

    invoke-virtual {v4, v1, v3}, Lorg/apache/pdfbox/pdmodel/graphics/predictor/PredictorAlgorithm;->encode([B[B)V

    invoke-virtual {v4, v3, v2}, Lorg/apache/pdfbox/pdmodel/graphics/predictor/PredictorAlgorithm;->decode([B[B)V

    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    invoke-static {v2}, Lorg/apache/pdfbox/pdmodel/graphics/predictor/PredictorAlgorithm;->dump([B)V

    add-int/lit8 p0, p0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method public aboveLeftPixel([BIII)I
    .locals 1

    if-lt p2, p3, :cond_0

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/predictor/PredictorAlgorithm;->getBpp()I

    move-result v0

    if-lt p4, v0, :cond_0

    add-int/2addr p2, p4

    sub-int/2addr p2, p3

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/predictor/PredictorAlgorithm;->getBpp()I

    move-result p3

    sub-int/2addr p2, p3

    aget-byte p1, p1, p2

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public abovePixel([BIII)I
    .locals 0

    if-lt p2, p3, :cond_0

    add-int/2addr p2, p4

    sub-int/2addr p2, p3

    aget-byte p1, p1, p2

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public checkBufsiz([B[B)V
    .locals 1

    array-length v0, p1

    array-length p2, p2

    if-ne v0, p2, :cond_1

    array-length p1, p1

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/predictor/PredictorAlgorithm;->getWidth()I

    move-result p2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/predictor/PredictorAlgorithm;->getHeight()I

    move-result v0

    mul-int/2addr p2, v0

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/predictor/PredictorAlgorithm;->getBpp()I

    move-result v0

    mul-int/2addr p2, v0

    if-ne p1, p2, :cond_0

    return-void

    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "src.length != width * height * bpp"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "src.length != dest.length"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public decode([B[B)V
    .locals 9

    invoke-virtual {p0, p1, p2}, Lorg/apache/pdfbox/pdmodel/graphics/predictor/PredictorAlgorithm;->checkBufsiz([B[B)V

    iget v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/predictor/PredictorAlgorithm;->width:I

    iget v1, p0, Lorg/apache/pdfbox/pdmodel/graphics/predictor/PredictorAlgorithm;->bpp:I

    mul-int/2addr v0, v1

    const/4 v1, 0x0

    :goto_0
    iget v2, p0, Lorg/apache/pdfbox/pdmodel/graphics/predictor/PredictorAlgorithm;->height:I

    if-ge v1, v2, :cond_0

    mul-int v8, v1, v0

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move v5, v0

    move v6, v8

    move v7, v0

    invoke-virtual/range {v2 .. v8}, Lorg/apache/pdfbox/pdmodel/graphics/predictor/PredictorAlgorithm;->decodeLine([B[BIIII)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public abstract decodeLine([B[BIIII)V
.end method

.method public encode([B[B)V
    .locals 9

    invoke-virtual {p0, p2, p1}, Lorg/apache/pdfbox/pdmodel/graphics/predictor/PredictorAlgorithm;->checkBufsiz([B[B)V

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/predictor/PredictorAlgorithm;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/predictor/PredictorAlgorithm;->getBpp()I

    move-result v1

    mul-int/2addr v0, v1

    const/4 v1, 0x0

    :goto_0
    iget v2, p0, Lorg/apache/pdfbox/pdmodel/graphics/predictor/PredictorAlgorithm;->height:I

    if-ge v1, v2, :cond_0

    mul-int v8, v1, v0

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move v5, v0

    move v6, v8

    move v7, v0

    invoke-virtual/range {v2 .. v8}, Lorg/apache/pdfbox/pdmodel/graphics/predictor/PredictorAlgorithm;->encodeLine([B[BIIII)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public abstract encodeLine([B[BIIII)V
.end method

.method public getBpp()I
    .locals 1

    iget v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/predictor/PredictorAlgorithm;->bpp:I

    return v0
.end method

.method public getHeight()I
    .locals 1

    iget v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/predictor/PredictorAlgorithm;->height:I

    return v0
.end method

.method public getWidth()I
    .locals 1

    iget v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/predictor/PredictorAlgorithm;->width:I

    return v0
.end method

.method public leftPixel([BIII)I
    .locals 0

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/predictor/PredictorAlgorithm;->getBpp()I

    move-result p3

    if-lt p4, p3, :cond_0

    add-int/2addr p2, p4

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/predictor/PredictorAlgorithm;->getBpp()I

    move-result p3

    sub-int/2addr p2, p3

    aget-byte p1, p1, p2

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public setBpp(I)V
    .locals 0

    iput p1, p0, Lorg/apache/pdfbox/pdmodel/graphics/predictor/PredictorAlgorithm;->bpp:I

    return-void
.end method

.method public setHeight(I)V
    .locals 0

    iput p1, p0, Lorg/apache/pdfbox/pdmodel/graphics/predictor/PredictorAlgorithm;->height:I

    return-void
.end method

.method public setWidth(I)V
    .locals 0

    iput p1, p0, Lorg/apache/pdfbox/pdmodel/graphics/predictor/PredictorAlgorithm;->width:I

    return-void
.end method

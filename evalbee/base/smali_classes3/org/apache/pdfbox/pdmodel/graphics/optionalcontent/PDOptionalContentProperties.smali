.class public Lorg/apache/pdfbox/pdmodel/graphics/optionalcontent/PDOptionalContentProperties;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/pdfbox/pdmodel/common/COSObjectable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/pdfbox/pdmodel/graphics/optionalcontent/PDOptionalContentProperties$BaseState;
    }
.end annotation


# instance fields
.field private dict:Lorg/apache/pdfbox/cos/COSDictionary;


# direct methods
.method public constructor <init>()V
    .locals 3

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-direct {v0}, Lorg/apache/pdfbox/cos/COSDictionary;-><init>()V

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/optionalcontent/PDOptionalContentProperties;->dict:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->OCGS:Lorg/apache/pdfbox/cos/COSName;

    new-instance v2, Lorg/apache/pdfbox/cos/COSArray;

    invoke-direct {v2}, Lorg/apache/pdfbox/cos/COSArray;-><init>()V

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/optionalcontent/PDOptionalContentProperties;->dict:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->D:Lorg/apache/pdfbox/cos/COSName;

    new-instance v2, Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-direct {v2}, Lorg/apache/pdfbox/cos/COSDictionary;-><init>()V

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    return-void
.end method

.method public constructor <init>(Lorg/apache/pdfbox/cos/COSDictionary;)V
    .locals 0

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/graphics/optionalcontent/PDOptionalContentProperties;->dict:Lorg/apache/pdfbox/cos/COSDictionary;

    return-void
.end method

.method private getD()Lorg/apache/pdfbox/cos/COSDictionary;
    .locals 3

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/optionalcontent/PDOptionalContentProperties;->dict:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->D:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSDictionary;

    if-nez v0, :cond_0

    new-instance v0, Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-direct {v0}, Lorg/apache/pdfbox/cos/COSDictionary;-><init>()V

    iget-object v2, p0, Lorg/apache/pdfbox/pdmodel/graphics/optionalcontent/PDOptionalContentProperties;->dict:Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-virtual {v2, v1, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    :cond_0
    return-object v0
.end method

.method private getOCGs()Lorg/apache/pdfbox/cos/COSArray;
    .locals 3

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/optionalcontent/PDOptionalContentProperties;->dict:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->OCGS:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getItem(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSArray;

    if-nez v0, :cond_0

    new-instance v0, Lorg/apache/pdfbox/cos/COSArray;

    invoke-direct {v0}, Lorg/apache/pdfbox/cos/COSArray;-><init>()V

    iget-object v2, p0, Lorg/apache/pdfbox/pdmodel/graphics/optionalcontent/PDOptionalContentProperties;->dict:Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-virtual {v2, v1, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    :cond_0
    return-object v0
.end method

.method private toDictionary(Lorg/apache/pdfbox/cos/COSBase;)Lorg/apache/pdfbox/cos/COSDictionary;
    .locals 1

    instance-of v0, p1, Lorg/apache/pdfbox/cos/COSObject;

    if-eqz v0, :cond_0

    check-cast p1, Lorg/apache/pdfbox/cos/COSObject;

    invoke-virtual {p1}, Lorg/apache/pdfbox/cos/COSObject;->getObject()Lorg/apache/pdfbox/cos/COSBase;

    move-result-object p1

    check-cast p1, Lorg/apache/pdfbox/cos/COSDictionary;

    return-object p1

    :cond_0
    check-cast p1, Lorg/apache/pdfbox/cos/COSDictionary;

    return-object p1
.end method


# virtual methods
.method public addGroup(Lorg/apache/pdfbox/pdmodel/graphics/optionalcontent/PDOptionalContentGroup;)V
    .locals 3

    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/graphics/optionalcontent/PDOptionalContentProperties;->getOCGs()Lorg/apache/pdfbox/cos/COSArray;

    move-result-object v0

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/markedcontent/PDPropertyList;->getCOSObject()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/graphics/optionalcontent/PDOptionalContentProperties;->getD()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->ORDER:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSArray;

    if-nez v0, :cond_0

    new-instance v0, Lorg/apache/pdfbox/cos/COSArray;

    invoke-direct {v0}, Lorg/apache/pdfbox/cos/COSArray;-><init>()V

    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/graphics/optionalcontent/PDOptionalContentProperties;->getD()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v2

    invoke-virtual {v2, v1, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    :cond_0
    invoke-virtual {v0, p1}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)V

    return-void
.end method

.method public getBaseState()Lorg/apache/pdfbox/pdmodel/graphics/optionalcontent/PDOptionalContentProperties$BaseState;
    .locals 2

    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/graphics/optionalcontent/PDOptionalContentProperties;->getD()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->BASE_STATE:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getItem(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSName;

    invoke-static {v0}, Lorg/apache/pdfbox/pdmodel/graphics/optionalcontent/PDOptionalContentProperties$BaseState;->valueOf(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/pdmodel/graphics/optionalcontent/PDOptionalContentProperties$BaseState;

    move-result-object v0

    return-object v0
.end method

.method public getCOSObject()Lorg/apache/pdfbox/cos/COSBase;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/optionalcontent/PDOptionalContentProperties;->dict:Lorg/apache/pdfbox/cos/COSDictionary;

    return-object v0
.end method

.method public getGroup(Ljava/lang/String;)Lorg/apache/pdfbox/pdmodel/graphics/optionalcontent/PDOptionalContentGroup;
    .locals 3

    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/graphics/optionalcontent/PDOptionalContentProperties;->getOCGs()Lorg/apache/pdfbox/cos/COSArray;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSArray;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/pdfbox/cos/COSBase;

    invoke-direct {p0, v1}, Lorg/apache/pdfbox/pdmodel/graphics/optionalcontent/PDOptionalContentProperties;->toDictionary(Lorg/apache/pdfbox/cos/COSBase;)Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v1

    sget-object v2, Lorg/apache/pdfbox/cos/COSName;->NAME:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->getString(Lorg/apache/pdfbox/cos/COSName;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    new-instance p1, Lorg/apache/pdfbox/pdmodel/graphics/optionalcontent/PDOptionalContentGroup;

    invoke-direct {p1, v1}, Lorg/apache/pdfbox/pdmodel/graphics/optionalcontent/PDOptionalContentGroup;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    return-object p1

    :cond_1
    const/4 p1, 0x0

    return-object p1
.end method

.method public getGroupNames()[Ljava/lang/String;
    .locals 6

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/optionalcontent/PDOptionalContentProperties;->dict:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->OCGS:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSArray;

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSArray;->size()I

    move-result v1

    new-array v2, v1, [Ljava/lang/String;

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v1, :cond_0

    invoke-virtual {v0, v3}, Lorg/apache/pdfbox/cos/COSArray;->get(I)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v4

    invoke-direct {p0, v4}, Lorg/apache/pdfbox/pdmodel/graphics/optionalcontent/PDOptionalContentProperties;->toDictionary(Lorg/apache/pdfbox/cos/COSBase;)Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v4

    sget-object v5, Lorg/apache/pdfbox/cos/COSName;->NAME:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v4, v5}, Lorg/apache/pdfbox/cos/COSDictionary;->getString(Lorg/apache/pdfbox/cos/COSName;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_0
    return-object v2
.end method

.method public getOptionalContentGroups()Ljava/util/Collection;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection<",
            "Lorg/apache/pdfbox/pdmodel/graphics/optionalcontent/PDOptionalContentGroup;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/graphics/optionalcontent/PDOptionalContentProperties;->getOCGs()Lorg/apache/pdfbox/cos/COSArray;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/pdfbox/cos/COSArray;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/pdfbox/cos/COSBase;

    check-cast v2, Lorg/apache/pdfbox/cos/COSObject;

    new-instance v3, Lorg/apache/pdfbox/pdmodel/graphics/optionalcontent/PDOptionalContentGroup;

    invoke-virtual {v2}, Lorg/apache/pdfbox/cos/COSObject;->getObject()Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v2

    check-cast v2, Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-direct {v3, v2}, Lorg/apache/pdfbox/pdmodel/graphics/optionalcontent/PDOptionalContentGroup;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    invoke-interface {v0, v3}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method public hasGroup(Ljava/lang/String;)Z
    .locals 5

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/optionalcontent/PDOptionalContentProperties;->getGroupNames()[Ljava/lang/String;

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    move v3, v2

    :goto_0
    if-ge v3, v1, :cond_1

    aget-object v4, v0, v3

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    return v2
.end method

.method public isGroupEnabled(Ljava/lang/String;)Z
    .locals 5

    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/graphics/optionalcontent/PDOptionalContentProperties;->getD()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->ON:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v1

    check-cast v1, Lorg/apache/pdfbox/cos/COSArray;

    const/4 v2, 0x1

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lorg/apache/pdfbox/cos/COSArray;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/pdfbox/cos/COSBase;

    invoke-direct {p0, v3}, Lorg/apache/pdfbox/pdmodel/graphics/optionalcontent/PDOptionalContentProperties;->toDictionary(Lorg/apache/pdfbox/cos/COSBase;)Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v3

    sget-object v4, Lorg/apache/pdfbox/cos/COSName;->NAME:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v3, v4}, Lorg/apache/pdfbox/cos/COSDictionary;->getString(Lorg/apache/pdfbox/cos/COSName;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    return v2

    :cond_1
    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->OFF:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSArray;

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSArray;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/pdfbox/cos/COSBase;

    invoke-direct {p0, v1}, Lorg/apache/pdfbox/pdmodel/graphics/optionalcontent/PDOptionalContentProperties;->toDictionary(Lorg/apache/pdfbox/cos/COSBase;)Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v1

    sget-object v3, Lorg/apache/pdfbox/cos/COSName;->NAME:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v1, v3}, Lorg/apache/pdfbox/cos/COSDictionary;->getString(Lorg/apache/pdfbox/cos/COSName;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 p1, 0x0

    return p1

    :cond_3
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/optionalcontent/PDOptionalContentProperties;->getBaseState()Lorg/apache/pdfbox/pdmodel/graphics/optionalcontent/PDOptionalContentProperties$BaseState;

    move-result-object p1

    sget-object v0, Lorg/apache/pdfbox/pdmodel/graphics/optionalcontent/PDOptionalContentProperties$BaseState;->OFF:Lorg/apache/pdfbox/pdmodel/graphics/optionalcontent/PDOptionalContentProperties$BaseState;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result p1

    xor-int/2addr p1, v2

    return p1
.end method

.method public setBaseState(Lorg/apache/pdfbox/pdmodel/graphics/optionalcontent/PDOptionalContentProperties$BaseState;)V
    .locals 2

    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/graphics/optionalcontent/PDOptionalContentProperties;->getD()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->BASE_STATE:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/graphics/optionalcontent/PDOptionalContentProperties$BaseState;->getName()Lorg/apache/pdfbox/cos/COSName;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    return-void
.end method

.method public setGroupEnabled(Ljava/lang/String;Z)Z
    .locals 7

    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/graphics/optionalcontent/PDOptionalContentProperties;->getD()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->ON:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v2

    check-cast v2, Lorg/apache/pdfbox/cos/COSArray;

    if-nez v2, :cond_0

    new-instance v2, Lorg/apache/pdfbox/cos/COSArray;

    invoke-direct {v2}, Lorg/apache/pdfbox/cos/COSArray;-><init>()V

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    :cond_0
    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->OFF:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v3

    check-cast v3, Lorg/apache/pdfbox/cos/COSArray;

    if-nez v3, :cond_1

    new-instance v3, Lorg/apache/pdfbox/cos/COSArray;

    invoke-direct {v3}, Lorg/apache/pdfbox/cos/COSArray;-><init>()V

    invoke-virtual {v0, v1, v3}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    :cond_1
    const/4 v0, 0x1

    const/4 v1, 0x0

    if-eqz p2, :cond_3

    invoke-virtual {v3}, Lorg/apache/pdfbox/cos/COSArray;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/apache/pdfbox/cos/COSBase;

    invoke-direct {p0, v5}, Lorg/apache/pdfbox/pdmodel/graphics/optionalcontent/PDOptionalContentProperties;->toDictionary(Lorg/apache/pdfbox/cos/COSBase;)Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v5

    sget-object v6, Lorg/apache/pdfbox/cos/COSName;->NAME:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v5, v6}, Lorg/apache/pdfbox/cos/COSDictionary;->getString(Lorg/apache/pdfbox/cos/COSName;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-virtual {v3, v5}, Lorg/apache/pdfbox/cos/COSArray;->remove(Lorg/apache/pdfbox/cos/COSBase;)Z

    invoke-virtual {v2, v5}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    goto :goto_0

    :cond_3
    invoke-virtual {v2}, Lorg/apache/pdfbox/cos/COSArray;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_4
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/apache/pdfbox/cos/COSBase;

    invoke-direct {p0, v5}, Lorg/apache/pdfbox/pdmodel/graphics/optionalcontent/PDOptionalContentProperties;->toDictionary(Lorg/apache/pdfbox/cos/COSBase;)Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v5

    sget-object v6, Lorg/apache/pdfbox/cos/COSName;->NAME:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v5, v6}, Lorg/apache/pdfbox/cos/COSDictionary;->getString(Lorg/apache/pdfbox/cos/COSName;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    invoke-virtual {v2, v5}, Lorg/apache/pdfbox/cos/COSArray;->remove(Lorg/apache/pdfbox/cos/COSBase;)Z

    invoke-virtual {v3, v5}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    goto :goto_0

    :cond_5
    move v0, v1

    :goto_0
    if-nez v0, :cond_7

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/pdmodel/graphics/optionalcontent/PDOptionalContentProperties;->getGroup(Ljava/lang/String;)Lorg/apache/pdfbox/pdmodel/graphics/optionalcontent/PDOptionalContentGroup;

    move-result-object p1

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/markedcontent/PDPropertyList;->getCOSObject()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object p1

    if-eqz p2, :cond_6

    invoke-virtual {v2, p1}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    goto :goto_1

    :cond_6
    invoke-virtual {v3, p1}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    :cond_7
    :goto_1
    return v0
.end method

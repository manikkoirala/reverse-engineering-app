.class public final Lorg/apache/pdfbox/pdmodel/graphics/image/PDImageXObject;
.super Lorg/apache/pdfbox/pdmodel/graphics/PDXObject;
.source "SourceFile"

# interfaces
.implements Lorg/apache/pdfbox/pdmodel/graphics/image/PDImage;


# instance fields
.field private cachedImage:Landroid/graphics/Bitmap;

.field private colorSpace:Lorg/apache/pdfbox/pdmodel/graphics/color/PDColorSpace;

.field private resources:Lorg/apache/pdfbox/pdmodel/PDResources;


# direct methods
.method public constructor <init>(Lorg/apache/pdfbox/pdmodel/PDDocument;)V
    .locals 1

    .line 1
    new-instance v0, Lorg/apache/pdfbox/pdmodel/common/PDStream;

    invoke-direct {v0, p1}, Lorg/apache/pdfbox/pdmodel/common/PDStream;-><init>(Lorg/apache/pdfbox/pdmodel/PDDocument;)V

    const/4 p1, 0x0

    invoke-direct {p0, v0, p1}, Lorg/apache/pdfbox/pdmodel/graphics/image/PDImageXObject;-><init>(Lorg/apache/pdfbox/pdmodel/common/PDStream;Lorg/apache/pdfbox/pdmodel/PDResources;)V

    return-void
.end method

.method public constructor <init>(Lorg/apache/pdfbox/pdmodel/PDDocument;Ljava/io/InputStream;Lorg/apache/pdfbox/cos/COSBase;IIILorg/apache/pdfbox/pdmodel/graphics/color/PDColorSpace;)V
    .locals 2

    .line 2
    new-instance v0, Lorg/apache/pdfbox/pdmodel/common/PDStream;

    const/4 v1, 0x1

    invoke-direct {v0, p1, p2, v1}, Lorg/apache/pdfbox/pdmodel/common/PDStream;-><init>(Lorg/apache/pdfbox/pdmodel/PDDocument;Ljava/io/InputStream;Z)V

    sget-object p1, Lorg/apache/pdfbox/cos/COSName;->IMAGE:Lorg/apache/pdfbox/cos/COSName;

    invoke-direct {p0, v0, p1}, Lorg/apache/pdfbox/pdmodel/graphics/PDXObject;-><init>(Lorg/apache/pdfbox/pdmodel/common/PDStream;Lorg/apache/pdfbox/cos/COSName;)V

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/PDXObject;->getCOSStream()Lorg/apache/pdfbox/cos/COSStream;

    move-result-object p1

    sget-object p2, Lorg/apache/pdfbox/cos/COSName;->FILTER:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p1, p2, p3}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    const/4 p1, 0x0

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/graphics/image/PDImageXObject;->resources:Lorg/apache/pdfbox/pdmodel/PDResources;

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/graphics/image/PDImageXObject;->colorSpace:Lorg/apache/pdfbox/pdmodel/graphics/color/PDColorSpace;

    invoke-virtual {p0, p6}, Lorg/apache/pdfbox/pdmodel/graphics/image/PDImageXObject;->setBitsPerComponent(I)V

    invoke-virtual {p0, p4}, Lorg/apache/pdfbox/pdmodel/graphics/image/PDImageXObject;->setWidth(I)V

    invoke-virtual {p0, p5}, Lorg/apache/pdfbox/pdmodel/graphics/image/PDImageXObject;->setHeight(I)V

    invoke-virtual {p0, p7}, Lorg/apache/pdfbox/pdmodel/graphics/image/PDImageXObject;->setColorSpace(Lorg/apache/pdfbox/pdmodel/graphics/color/PDColorSpace;)V

    return-void
.end method

.method public constructor <init>(Lorg/apache/pdfbox/pdmodel/common/PDStream;Lorg/apache/pdfbox/pdmodel/PDResources;)V
    .locals 1

    .line 3
    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/common/PDStream;->getStream()Lorg/apache/pdfbox/cos/COSStream;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSStream;->getDecodeResult()Lorg/apache/pdfbox/filter/DecodeResult;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/pdfbox/pdmodel/graphics/image/PDImageXObject;-><init>(Lorg/apache/pdfbox/pdmodel/common/PDStream;Lorg/apache/pdfbox/pdmodel/PDResources;Lorg/apache/pdfbox/filter/DecodeResult;)V

    return-void
.end method

.method private constructor <init>(Lorg/apache/pdfbox/pdmodel/common/PDStream;Lorg/apache/pdfbox/pdmodel/PDResources;Lorg/apache/pdfbox/filter/DecodeResult;)V
    .locals 0

    .line 4
    invoke-static {p1, p3}, Lorg/apache/pdfbox/pdmodel/graphics/image/PDImageXObject;->repair(Lorg/apache/pdfbox/pdmodel/common/PDStream;Lorg/apache/pdfbox/filter/DecodeResult;)Lorg/apache/pdfbox/pdmodel/common/PDStream;

    move-result-object p1

    sget-object p3, Lorg/apache/pdfbox/cos/COSName;->IMAGE:Lorg/apache/pdfbox/cos/COSName;

    invoke-direct {p0, p1, p3}, Lorg/apache/pdfbox/pdmodel/graphics/PDXObject;-><init>(Lorg/apache/pdfbox/pdmodel/common/PDStream;Lorg/apache/pdfbox/cos/COSName;)V

    iput-object p2, p0, Lorg/apache/pdfbox/pdmodel/graphics/image/PDImageXObject;->resources:Lorg/apache/pdfbox/pdmodel/PDResources;

    return-void
.end method

.method private applyMask(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;Z)Landroid/graphics/Bitmap;
    .locals 2

    if-nez p2, :cond_0

    return-object p1

    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result p2

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result p1

    const/4 v0, 0x0

    :goto_0
    const/4 v1, 0x0

    if-ge v0, p1, :cond_2

    if-lez p2, :cond_1

    throw v1

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    return-object v1
.end method

.method public static createThumbnail(Lorg/apache/pdfbox/cos/COSStream;)Lorg/apache/pdfbox/pdmodel/graphics/image/PDImageXObject;
    .locals 2

    new-instance v0, Lorg/apache/pdfbox/pdmodel/common/PDStream;

    invoke-direct {v0, p0}, Lorg/apache/pdfbox/pdmodel/common/PDStream;-><init>(Lorg/apache/pdfbox/cos/COSStream;)V

    new-instance p0, Lorg/apache/pdfbox/pdmodel/graphics/image/PDImageXObject;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/graphics/image/PDImageXObject;-><init>(Lorg/apache/pdfbox/pdmodel/common/PDStream;Lorg/apache/pdfbox/pdmodel/PDResources;)V

    return-object p0
.end method

.method private static repair(Lorg/apache/pdfbox/pdmodel/common/PDStream;Lorg/apache/pdfbox/filter/DecodeResult;)Lorg/apache/pdfbox/pdmodel/common/PDStream;
    .locals 1

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/common/PDStream;->getStream()Lorg/apache/pdfbox/cos/COSStream;

    move-result-object v0

    invoke-virtual {p1}, Lorg/apache/pdfbox/filter/DecodeResult;->getParameters()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object p1

    invoke-virtual {v0, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->addAll(Lorg/apache/pdfbox/cos/COSDictionary;)V

    return-object p0
.end method


# virtual methods
.method public getBitsPerComponent()I
    .locals 4

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/image/PDImageXObject;->isStencil()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    return v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/PDXObject;->getCOSStream()Lorg/apache/pdfbox/cos/COSStream;

    move-result-object v1

    sget-object v2, Lorg/apache/pdfbox/cos/COSName;->BITS_PER_COMPONENT:Lorg/apache/pdfbox/cos/COSName;

    sget-object v3, Lorg/apache/pdfbox/cos/COSName;->BPC:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v1, v2, v3}, Lorg/apache/pdfbox/cos/COSDictionary;->getInt(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSName;)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "PdfBoxAndroid"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/PDXObject;->getCOSStream()Lorg/apache/pdfbox/cos/COSStream;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Lorg/apache/pdfbox/cos/COSDictionary;->getInt(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSName;)I

    move-result v0

    return v0
.end method

.method public getColorKeyMask()Lorg/apache/pdfbox/cos/COSArray;
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/PDXObject;->getCOSStream()Lorg/apache/pdfbox/cos/COSStream;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->MASK:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    instance-of v1, v0, Lorg/apache/pdfbox/cos/COSArray;

    if-eqz v1, :cond_0

    check-cast v0, Lorg/apache/pdfbox/cos/COSArray;

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public getColorSpace()Lorg/apache/pdfbox/pdmodel/graphics/color/PDColorSpace;
    .locals 3

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/image/PDImageXObject;->colorSpace:Lorg/apache/pdfbox/pdmodel/graphics/color/PDColorSpace;

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/PDXObject;->getCOSStream()Lorg/apache/pdfbox/cos/COSStream;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->COLORSPACE:Lorg/apache/pdfbox/cos/COSName;

    sget-object v2, Lorg/apache/pdfbox/cos/COSName;->CS:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lorg/apache/pdfbox/pdmodel/graphics/image/PDImageXObject;->resources:Lorg/apache/pdfbox/pdmodel/PDResources;

    invoke-static {v0, v1}, Lorg/apache/pdfbox/pdmodel/graphics/color/PDColorSpace;->create(Lorg/apache/pdfbox/cos/COSBase;Lorg/apache/pdfbox/pdmodel/PDResources;)Lorg/apache/pdfbox/pdmodel/graphics/color/PDColorSpace;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/image/PDImageXObject;->colorSpace:Lorg/apache/pdfbox/pdmodel/graphics/color/PDColorSpace;

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/image/PDImageXObject;->isStencil()Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object v0, Lorg/apache/pdfbox/pdmodel/graphics/color/PDDeviceGray;->INSTANCE:Lorg/apache/pdfbox/pdmodel/graphics/color/PDDeviceGray;

    return-object v0

    :cond_1
    new-instance v0, Ljava/io/IOException;

    const-string v1, "could not determine color space"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    :goto_0
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/image/PDImageXObject;->colorSpace:Lorg/apache/pdfbox/pdmodel/graphics/color/PDColorSpace;

    return-object v0
.end method

.method public getDecode()Lorg/apache/pdfbox/cos/COSArray;
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/PDXObject;->getCOSStream()Lorg/apache/pdfbox/cos/COSStream;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->DECODE:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    instance-of v1, v0, Lorg/apache/pdfbox/cos/COSArray;

    if-eqz v1, :cond_0

    check-cast v0, Lorg/apache/pdfbox/cos/COSArray;

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public getHeight()I
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/PDXObject;->getCOSStream()Lorg/apache/pdfbox/cos/COSStream;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->HEIGHT:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getInt(Lorg/apache/pdfbox/cos/COSName;)I

    move-result v0

    return v0
.end method

.method public getImage()Landroid/graphics/Bitmap;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/image/PDImageXObject;->cachedImage:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    return-object v0

    :cond_0
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/image/PDImageXObject;->getColorKeyMask()Lorg/apache/pdfbox/cos/COSArray;

    move-result-object v0

    invoke-static {p0, v0}, Lorg/apache/pdfbox/pdmodel/graphics/image/SampledImageReader;->getRGBImage(Lorg/apache/pdfbox/pdmodel/graphics/image/PDImage;Lorg/apache/pdfbox/cos/COSArray;)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/image/PDImageXObject;->getSoftMask()Lorg/apache/pdfbox/pdmodel/graphics/image/PDImageXObject;

    move-result-object v1

    if-eqz v1, :cond_1

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/image/PDImageXObject;->getMask()Lorg/apache/pdfbox/pdmodel/graphics/image/PDImageXObject;

    :goto_0
    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/image/PDImageXObject;->cachedImage:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getInterpolate()Z
    .locals 3

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/PDXObject;->getCOSStream()Lorg/apache/pdfbox/cos/COSStream;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->INTERPOLATE:Lorg/apache/pdfbox/cos/COSName;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->getBoolean(Lorg/apache/pdfbox/cos/COSName;Z)Z

    move-result v0

    return v0
.end method

.method public getMask()Lorg/apache/pdfbox/pdmodel/graphics/image/PDImageXObject;
    .locals 4

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/PDXObject;->getCOSStream()Lorg/apache/pdfbox/cos/COSStream;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->MASK:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    instance-of v0, v0, Lorg/apache/pdfbox/cos/COSArray;

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    return-object v2

    :cond_0
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/PDXObject;->getCOSStream()Lorg/apache/pdfbox/cos/COSStream;

    move-result-object v0

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSStream;

    if-eqz v0, :cond_1

    new-instance v1, Lorg/apache/pdfbox/pdmodel/graphics/image/PDImageXObject;

    new-instance v3, Lorg/apache/pdfbox/pdmodel/common/PDStream;

    invoke-direct {v3, v0}, Lorg/apache/pdfbox/pdmodel/common/PDStream;-><init>(Lorg/apache/pdfbox/cos/COSStream;)V

    invoke-direct {v1, v3, v2}, Lorg/apache/pdfbox/pdmodel/graphics/image/PDImageXObject;-><init>(Lorg/apache/pdfbox/pdmodel/common/PDStream;Lorg/apache/pdfbox/pdmodel/PDResources;)V

    return-object v1

    :cond_1
    return-object v2
.end method

.method public getMetadata()Lorg/apache/pdfbox/pdmodel/common/PDMetadata;
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/PDXObject;->getCOSStream()Lorg/apache/pdfbox/cos/COSStream;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->METADATA:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSStream;

    if-eqz v0, :cond_0

    new-instance v1, Lorg/apache/pdfbox/pdmodel/common/PDMetadata;

    invoke-direct {v1, v0}, Lorg/apache/pdfbox/pdmodel/common/PDMetadata;-><init>(Lorg/apache/pdfbox/cos/COSStream;)V

    return-object v1

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public getOpaqueImage()Landroid/graphics/Bitmap;
    .locals 1

    const/4 v0, 0x0

    invoke-static {p0, v0}, Lorg/apache/pdfbox/pdmodel/graphics/image/SampledImageReader;->getRGBImage(Lorg/apache/pdfbox/pdmodel/graphics/image/PDImage;Lorg/apache/pdfbox/cos/COSArray;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public getSoftMask()Lorg/apache/pdfbox/pdmodel/graphics/image/PDImageXObject;
    .locals 4

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/PDXObject;->getCOSStream()Lorg/apache/pdfbox/cos/COSStream;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->SMASK:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSStream;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    new-instance v2, Lorg/apache/pdfbox/pdmodel/graphics/image/PDImageXObject;

    new-instance v3, Lorg/apache/pdfbox/pdmodel/common/PDStream;

    invoke-direct {v3, v0}, Lorg/apache/pdfbox/pdmodel/common/PDStream;-><init>(Lorg/apache/pdfbox/cos/COSStream;)V

    invoke-direct {v2, v3, v1}, Lorg/apache/pdfbox/pdmodel/graphics/image/PDImageXObject;-><init>(Lorg/apache/pdfbox/pdmodel/common/PDStream;Lorg/apache/pdfbox/pdmodel/PDResources;)V

    return-object v2

    :cond_0
    return-object v1
.end method

.method public getStream()Lorg/apache/pdfbox/pdmodel/common/PDStream;
    .locals 1

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/PDXObject;->getPDStream()Lorg/apache/pdfbox/pdmodel/common/PDStream;

    move-result-object v0

    return-object v0
.end method

.method public getStructParent()I
    .locals 3

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/PDXObject;->getCOSStream()Lorg/apache/pdfbox/cos/COSStream;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->STRUCT_PARENT:Lorg/apache/pdfbox/cos/COSName;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->getInt(Lorg/apache/pdfbox/cos/COSName;I)I

    move-result v0

    return v0
.end method

.method public getSuffix()Ljava/lang/String;
    .locals 3

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/PDXObject;->getPDStream()Lorg/apache/pdfbox/pdmodel/common/PDStream;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/common/PDStream;->getFilters()Ljava/util/List;

    move-result-object v0

    const-string v1, "png"

    if-nez v0, :cond_0

    return-object v1

    :cond_0
    sget-object v2, Lorg/apache/pdfbox/cos/COSName;->DCT_DECODE:Lorg/apache/pdfbox/cos/COSName;

    invoke-interface {v0, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v0, "jpg"

    return-object v0

    :cond_1
    sget-object v2, Lorg/apache/pdfbox/cos/COSName;->JPX_DECODE:Lorg/apache/pdfbox/cos/COSName;

    invoke-interface {v0, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v0, "jpx"

    return-object v0

    :cond_2
    sget-object v2, Lorg/apache/pdfbox/cos/COSName;->CCITTFAX_DECODE:Lorg/apache/pdfbox/cos/COSName;

    invoke-interface {v0, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    const-string v0, "tiff"

    return-object v0

    :cond_3
    sget-object v2, Lorg/apache/pdfbox/cos/COSName;->FLATE_DECODE:Lorg/apache/pdfbox/cos/COSName;

    invoke-interface {v0, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    sget-object v2, Lorg/apache/pdfbox/cos/COSName;->LZW_DECODE:Lorg/apache/pdfbox/cos/COSName;

    invoke-interface {v0, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    sget-object v2, Lorg/apache/pdfbox/cos/COSName;->RUN_LENGTH_DECODE:Lorg/apache/pdfbox/cos/COSName;

    invoke-interface {v0, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    goto :goto_0

    :cond_4
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getSuffix() returns null, filters: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "PdfBoxAndroid"

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    return-object v0

    :cond_5
    :goto_0
    return-object v1
.end method

.method public getWidth()I
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/PDXObject;->getCOSStream()Lorg/apache/pdfbox/cos/COSStream;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->WIDTH:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getInt(Lorg/apache/pdfbox/cos/COSName;)I

    move-result v0

    return v0
.end method

.method public isStencil()Z
    .locals 3

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/PDXObject;->getCOSStream()Lorg/apache/pdfbox/cos/COSStream;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->IMAGE_MASK:Lorg/apache/pdfbox/cos/COSName;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->getBoolean(Lorg/apache/pdfbox/cos/COSName;Z)Z

    move-result v0

    return v0
.end method

.method public setBitsPerComponent(I)V
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/PDXObject;->getCOSStream()Lorg/apache/pdfbox/cos/COSStream;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->BITS_PER_COMPONENT:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setInt(Lorg/apache/pdfbox/cos/COSName;I)V

    return-void
.end method

.method public setColorSpace(Lorg/apache/pdfbox/pdmodel/graphics/color/PDColorSpace;)V
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/PDXObject;->getCOSStream()Lorg/apache/pdfbox/cos/COSStream;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->COLORSPACE:Lorg/apache/pdfbox/cos/COSName;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/graphics/color/PDColorSpace;->getCOSObject()Lorg/apache/pdfbox/cos/COSBase;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/PDXObject;->getCOSStream()Lorg/apache/pdfbox/cos/COSStream;

    move-result-object p1

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->DEVICERGB:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p1, v1, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    return-void
.end method

.method public setDecode(Lorg/apache/pdfbox/cos/COSArray;)V
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/PDXObject;->getCOSStream()Lorg/apache/pdfbox/cos/COSStream;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->DECODE:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    return-void
.end method

.method public setHeight(I)V
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/PDXObject;->getCOSStream()Lorg/apache/pdfbox/cos/COSStream;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->HEIGHT:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setInt(Lorg/apache/pdfbox/cos/COSName;I)V

    return-void
.end method

.method public setInterpolate(Z)V
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/PDXObject;->getCOSStream()Lorg/apache/pdfbox/cos/COSStream;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->INTERPOLATE:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setBoolean(Lorg/apache/pdfbox/cos/COSName;Z)V

    return-void
.end method

.method public setMetadata(Lorg/apache/pdfbox/pdmodel/common/PDMetadata;)V
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/PDXObject;->getCOSStream()Lorg/apache/pdfbox/cos/COSStream;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->METADATA:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)V

    return-void
.end method

.method public setStencil(Z)V
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/PDXObject;->getCOSStream()Lorg/apache/pdfbox/cos/COSStream;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->IMAGE_MASK:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setBoolean(Lorg/apache/pdfbox/cos/COSName;Z)V

    return-void
.end method

.method public setStructParent(I)V
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/PDXObject;->getCOSStream()Lorg/apache/pdfbox/cos/COSStream;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->STRUCT_PARENT:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setInt(Lorg/apache/pdfbox/cos/COSName;I)V

    return-void
.end method

.method public setWidth(I)V
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/PDXObject;->getCOSStream()Lorg/apache/pdfbox/cos/COSStream;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->WIDTH:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setInt(Lorg/apache/pdfbox/cos/COSName;I)V

    return-void
.end method

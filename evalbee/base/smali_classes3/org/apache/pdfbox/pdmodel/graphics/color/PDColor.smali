.class public final Lorg/apache/pdfbox/pdmodel/graphics/color/PDColor;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final colorSpace:Lorg/apache/pdfbox/pdmodel/graphics/color/PDColorSpace;

.field private final components:[F

.field private final patternName:Lorg/apache/pdfbox/cos/COSName;


# direct methods
.method public constructor <init>(Lorg/apache/pdfbox/cos/COSArray;Lorg/apache/pdfbox/pdmodel/graphics/color/PDColorSpace;)V
    .locals 3

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Lorg/apache/pdfbox/cos/COSArray;->size()I

    move-result v0

    const/4 v1, 0x0

    if-lez v0, :cond_1

    invoke-virtual {p1}, Lorg/apache/pdfbox/cos/COSArray;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p1, v0}, Lorg/apache/pdfbox/cos/COSArray;->get(I)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    instance-of v0, v0, Lorg/apache/pdfbox/cos/COSName;

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lorg/apache/pdfbox/cos/COSArray;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    new-array v0, v0, [F

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/color/PDColor;->components:[F

    :goto_0
    invoke-virtual {p1}, Lorg/apache/pdfbox/cos/COSArray;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ge v1, v0, :cond_0

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/color/PDColor;->components:[F

    invoke-virtual {p1, v1}, Lorg/apache/pdfbox/cos/COSArray;->get(I)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v2

    check-cast v2, Lorg/apache/pdfbox/cos/COSNumber;

    invoke-virtual {v2}, Lorg/apache/pdfbox/cos/COSNumber;->floatValue()F

    move-result v2

    aput v2, v0, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Lorg/apache/pdfbox/cos/COSArray;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p1, v0}, Lorg/apache/pdfbox/cos/COSArray;->get(I)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object p1

    check-cast p1, Lorg/apache/pdfbox/cos/COSName;

    goto :goto_2

    :cond_1
    invoke-virtual {p1}, Lorg/apache/pdfbox/cos/COSArray;->size()I

    move-result v0

    new-array v0, v0, [F

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/color/PDColor;->components:[F

    :goto_1
    invoke-virtual {p1}, Lorg/apache/pdfbox/cos/COSArray;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/color/PDColor;->components:[F

    invoke-virtual {p1, v1}, Lorg/apache/pdfbox/cos/COSArray;->get(I)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v2

    check-cast v2, Lorg/apache/pdfbox/cos/COSNumber;

    invoke-virtual {v2}, Lorg/apache/pdfbox/cos/COSNumber;->floatValue()F

    move-result v2

    aput v2, v0, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    const/4 p1, 0x0

    :goto_2
    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/graphics/color/PDColor;->patternName:Lorg/apache/pdfbox/cos/COSName;

    iput-object p2, p0, Lorg/apache/pdfbox/pdmodel/graphics/color/PDColor;->colorSpace:Lorg/apache/pdfbox/pdmodel/graphics/color/PDColorSpace;

    return-void
.end method

.method public constructor <init>(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/pdmodel/graphics/color/PDColorSpace;)V
    .locals 1

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    new-array v0, v0, [F

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/color/PDColor;->components:[F

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/graphics/color/PDColor;->patternName:Lorg/apache/pdfbox/cos/COSName;

    iput-object p2, p0, Lorg/apache/pdfbox/pdmodel/graphics/color/PDColor;->colorSpace:Lorg/apache/pdfbox/pdmodel/graphics/color/PDColorSpace;

    return-void
.end method

.method public constructor <init>([FLorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/pdmodel/graphics/color/PDColorSpace;)V
    .locals 0

    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, [F->clone()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, [F

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/graphics/color/PDColor;->components:[F

    iput-object p2, p0, Lorg/apache/pdfbox/pdmodel/graphics/color/PDColor;->patternName:Lorg/apache/pdfbox/cos/COSName;

    iput-object p3, p0, Lorg/apache/pdfbox/pdmodel/graphics/color/PDColor;->colorSpace:Lorg/apache/pdfbox/pdmodel/graphics/color/PDColorSpace;

    return-void
.end method

.method public constructor <init>([FLorg/apache/pdfbox/pdmodel/graphics/color/PDColorSpace;)V
    .locals 0

    .line 4
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, [F->clone()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, [F

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/graphics/color/PDColor;->components:[F

    const/4 p1, 0x0

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/graphics/color/PDColor;->patternName:Lorg/apache/pdfbox/cos/COSName;

    iput-object p2, p0, Lorg/apache/pdfbox/pdmodel/graphics/color/PDColor;->colorSpace:Lorg/apache/pdfbox/pdmodel/graphics/color/PDColorSpace;

    return-void
.end method


# virtual methods
.method public getColorSpace()Lorg/apache/pdfbox/pdmodel/graphics/color/PDColorSpace;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/color/PDColor;->colorSpace:Lorg/apache/pdfbox/pdmodel/graphics/color/PDColorSpace;

    return-object v0
.end method

.method public getComponents()[F
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/color/PDColor;->components:[F

    invoke-virtual {v0}, [F->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [F

    return-object v0
.end method

.method public getPatternName()Lorg/apache/pdfbox/cos/COSName;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/color/PDColor;->patternName:Lorg/apache/pdfbox/cos/COSName;

    return-object v0
.end method

.method public isPattern()Z
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/color/PDColor;->patternName:Lorg/apache/pdfbox/cos/COSName;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public toCOSArray()Lorg/apache/pdfbox/cos/COSArray;
    .locals 2

    new-instance v0, Lorg/apache/pdfbox/cos/COSArray;

    invoke-direct {v0}, Lorg/apache/pdfbox/cos/COSArray;-><init>()V

    iget-object v1, p0, Lorg/apache/pdfbox/pdmodel/graphics/color/PDColor;->components:[F

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSArray;->setFloatArray([F)V

    iget-object v1, p0, Lorg/apache/pdfbox/pdmodel/graphics/color/PDColor;->patternName:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "PDColor{components="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lorg/apache/pdfbox/pdmodel/graphics/color/PDColor;->components:[F

    invoke-static {v1}, Ljava/util/Arrays;->toString([F)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", patternName="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lorg/apache/pdfbox/pdmodel/graphics/color/PDColor;->patternName:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.class public Lorg/apache/pdfbox/pdmodel/graphics/PDXObject;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/pdfbox/pdmodel/common/COSObjectable;


# instance fields
.field private stream:Lorg/apache/pdfbox/pdmodel/common/PDStream;


# direct methods
.method public constructor <init>(Lorg/apache/pdfbox/pdmodel/PDDocument;Lorg/apache/pdfbox/cos/COSName;)V
    .locals 2

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lorg/apache/pdfbox/pdmodel/common/PDStream;

    invoke-direct {v0, p1}, Lorg/apache/pdfbox/pdmodel/common/PDStream;-><init>(Lorg/apache/pdfbox/pdmodel/PDDocument;)V

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/PDXObject;->stream:Lorg/apache/pdfbox/pdmodel/common/PDStream;

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/common/PDStream;->getStream()Lorg/apache/pdfbox/cos/COSStream;

    move-result-object p1

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->TYPE:Lorg/apache/pdfbox/cos/COSName;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->XOBJECT:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v1}, Lorg/apache/pdfbox/cos/COSName;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->setName(Lorg/apache/pdfbox/cos/COSName;Ljava/lang/String;)V

    iget-object p1, p0, Lorg/apache/pdfbox/pdmodel/graphics/PDXObject;->stream:Lorg/apache/pdfbox/pdmodel/common/PDStream;

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/common/PDStream;->getStream()Lorg/apache/pdfbox/cos/COSStream;

    move-result-object p1

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->SUBTYPE:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p2}, Lorg/apache/pdfbox/cos/COSName;->getName()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, v0, p2}, Lorg/apache/pdfbox/cos/COSDictionary;->setName(Lorg/apache/pdfbox/cos/COSName;Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Lorg/apache/pdfbox/pdmodel/common/PDStream;Lorg/apache/pdfbox/cos/COSName;)V
    .locals 3

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/graphics/PDXObject;->stream:Lorg/apache/pdfbox/pdmodel/common/PDStream;

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/common/PDStream;->getStream()Lorg/apache/pdfbox/cos/COSStream;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->TYPE:Lorg/apache/pdfbox/cos/COSName;

    sget-object v2, Lorg/apache/pdfbox/cos/COSName;->XOBJECT:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v2}, Lorg/apache/pdfbox/cos/COSName;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->setName(Lorg/apache/pdfbox/cos/COSName;Ljava/lang/String;)V

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/common/PDStream;->getStream()Lorg/apache/pdfbox/cos/COSStream;

    move-result-object p1

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->SUBTYPE:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p2}, Lorg/apache/pdfbox/cos/COSName;->getName()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, v0, p2}, Lorg/apache/pdfbox/cos/COSDictionary;->setName(Lorg/apache/pdfbox/cos/COSName;Ljava/lang/String;)V

    return-void
.end method

.method public static createXObject(Lorg/apache/pdfbox/cos/COSBase;Ljava/lang/String;Lorg/apache/pdfbox/pdmodel/PDResources;)Lorg/apache/pdfbox/pdmodel/graphics/PDXObject;
    .locals 2

    if-nez p0, :cond_0

    const/4 p0, 0x0

    return-object p0

    :cond_0
    instance-of v0, p0, Lorg/apache/pdfbox/cos/COSStream;

    if-eqz v0, :cond_4

    check-cast p0, Lorg/apache/pdfbox/cos/COSStream;

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->SUBTYPE:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p0, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->getNameAsString(Lorg/apache/pdfbox/cos/COSName;)Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->IMAGE:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v1}, Lorg/apache/pdfbox/cos/COSName;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance p1, Lorg/apache/pdfbox/pdmodel/graphics/image/PDImageXObject;

    new-instance v0, Lorg/apache/pdfbox/pdmodel/common/PDStream;

    invoke-direct {v0, p0}, Lorg/apache/pdfbox/pdmodel/common/PDStream;-><init>(Lorg/apache/pdfbox/cos/COSStream;)V

    invoke-direct {p1, v0, p2}, Lorg/apache/pdfbox/pdmodel/graphics/image/PDImageXObject;-><init>(Lorg/apache/pdfbox/pdmodel/common/PDStream;Lorg/apache/pdfbox/pdmodel/PDResources;)V

    return-object p1

    :cond_1
    sget-object p2, Lorg/apache/pdfbox/cos/COSName;->FORM:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p2}, Lorg/apache/pdfbox/cos/COSName;->getName()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p2

    if-eqz p2, :cond_2

    new-instance p2, Lorg/apache/pdfbox/pdmodel/graphics/form/PDFormXObject;

    new-instance v0, Lorg/apache/pdfbox/pdmodel/common/PDStream;

    invoke-direct {v0, p0}, Lorg/apache/pdfbox/pdmodel/common/PDStream;-><init>(Lorg/apache/pdfbox/cos/COSStream;)V

    invoke-direct {p2, v0, p1}, Lorg/apache/pdfbox/pdmodel/graphics/form/PDFormXObject;-><init>(Lorg/apache/pdfbox/pdmodel/common/PDStream;Ljava/lang/String;)V

    return-object p2

    :cond_2
    sget-object p1, Lorg/apache/pdfbox/cos/COSName;->PS:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p1}, Lorg/apache/pdfbox/cos/COSName;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    new-instance p1, Lorg/apache/pdfbox/pdmodel/graphics/PDPostScriptXObject;

    new-instance p2, Lorg/apache/pdfbox/pdmodel/common/PDStream;

    invoke-direct {p2, p0}, Lorg/apache/pdfbox/pdmodel/common/PDStream;-><init>(Lorg/apache/pdfbox/cos/COSStream;)V

    invoke-direct {p1, p2}, Lorg/apache/pdfbox/pdmodel/graphics/PDPostScriptXObject;-><init>(Lorg/apache/pdfbox/pdmodel/common/PDStream;)V

    return-object p1

    :cond_3
    new-instance p0, Ljava/io/IOException;

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string p2, "Invalid XObject Subtype: "

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p0

    :cond_4
    new-instance p1, Ljava/io/IOException;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Unexpected object type: "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {p1, p0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public final getCOSObject()Lorg/apache/pdfbox/cos/COSBase;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/PDXObject;->stream:Lorg/apache/pdfbox/pdmodel/common/PDStream;

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/common/PDStream;->getCOSObject()Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    return-object v0
.end method

.method public final getCOSStream()Lorg/apache/pdfbox/cos/COSStream;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/PDXObject;->stream:Lorg/apache/pdfbox/pdmodel/common/PDStream;

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/common/PDStream;->getStream()Lorg/apache/pdfbox/cos/COSStream;

    move-result-object v0

    return-object v0
.end method

.method public final getPDStream()Lorg/apache/pdfbox/pdmodel/common/PDStream;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/PDXObject;->stream:Lorg/apache/pdfbox/pdmodel/common/PDStream;

    return-object v0
.end method

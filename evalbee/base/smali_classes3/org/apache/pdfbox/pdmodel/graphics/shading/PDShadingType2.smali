.class public Lorg/apache/pdfbox/pdmodel/graphics/shading/PDShadingType2;
.super Lorg/apache/pdfbox/pdmodel/graphics/shading/PDShading;
.source "SourceFile"


# instance fields
.field private coords:Lorg/apache/pdfbox/cos/COSArray;

.field private domain:Lorg/apache/pdfbox/cos/COSArray;

.field private extend:Lorg/apache/pdfbox/cos/COSArray;


# direct methods
.method public constructor <init>(Lorg/apache/pdfbox/cos/COSDictionary;)V
    .locals 0

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/graphics/shading/PDShading;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    const/4 p1, 0x0

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/graphics/shading/PDShadingType2;->coords:Lorg/apache/pdfbox/cos/COSArray;

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/graphics/shading/PDShadingType2;->domain:Lorg/apache/pdfbox/cos/COSArray;

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/graphics/shading/PDShadingType2;->extend:Lorg/apache/pdfbox/cos/COSArray;

    return-void
.end method


# virtual methods
.method public getCoords()Lorg/apache/pdfbox/cos/COSArray;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/shading/PDShadingType2;->coords:Lorg/apache/pdfbox/cos/COSArray;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/shading/PDShading;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->COORDS:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSArray;

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/shading/PDShadingType2;->coords:Lorg/apache/pdfbox/cos/COSArray;

    :cond_0
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/shading/PDShadingType2;->coords:Lorg/apache/pdfbox/cos/COSArray;

    return-object v0
.end method

.method public getDomain()Lorg/apache/pdfbox/cos/COSArray;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/shading/PDShadingType2;->domain:Lorg/apache/pdfbox/cos/COSArray;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/shading/PDShading;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->DOMAIN:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSArray;

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/shading/PDShadingType2;->domain:Lorg/apache/pdfbox/cos/COSArray;

    :cond_0
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/shading/PDShadingType2;->domain:Lorg/apache/pdfbox/cos/COSArray;

    return-object v0
.end method

.method public getExtend()Lorg/apache/pdfbox/cos/COSArray;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/shading/PDShadingType2;->extend:Lorg/apache/pdfbox/cos/COSArray;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/shading/PDShading;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->EXTEND:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSArray;

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/shading/PDShadingType2;->extend:Lorg/apache/pdfbox/cos/COSArray;

    :cond_0
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/shading/PDShadingType2;->extend:Lorg/apache/pdfbox/cos/COSArray;

    return-object v0
.end method

.method public getShadingType()I
    .locals 1

    const/4 v0, 0x2

    return v0
.end method

.method public setCoords(Lorg/apache/pdfbox/cos/COSArray;)V
    .locals 2

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/graphics/shading/PDShadingType2;->coords:Lorg/apache/pdfbox/cos/COSArray;

    if-nez p1, :cond_0

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/shading/PDShading;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object p1

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->COORDS:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p1, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->removeItem(Lorg/apache/pdfbox/cos/COSName;)V

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/shading/PDShading;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->COORDS:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    :goto_0
    return-void
.end method

.method public setDomain(Lorg/apache/pdfbox/cos/COSArray;)V
    .locals 2

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/graphics/shading/PDShadingType2;->domain:Lorg/apache/pdfbox/cos/COSArray;

    if-nez p1, :cond_0

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/shading/PDShading;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object p1

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->DOMAIN:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p1, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->removeItem(Lorg/apache/pdfbox/cos/COSName;)V

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/shading/PDShading;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->DOMAIN:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    :goto_0
    return-void
.end method

.method public setExtend(Lorg/apache/pdfbox/cos/COSArray;)V
    .locals 2

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/graphics/shading/PDShadingType2;->extend:Lorg/apache/pdfbox/cos/COSArray;

    if-nez p1, :cond_0

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/shading/PDShading;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object p1

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->EXTEND:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p1, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->removeItem(Lorg/apache/pdfbox/cos/COSName;)V

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/shading/PDShading;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->EXTEND:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    :goto_0
    return-void
.end method

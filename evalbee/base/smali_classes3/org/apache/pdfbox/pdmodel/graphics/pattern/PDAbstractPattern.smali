.class public abstract Lorg/apache/pdfbox/pdmodel/graphics/pattern/PDAbstractPattern;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/pdfbox/pdmodel/common/COSObjectable;


# static fields
.field public static final TYPE_SHADING_PATTERN:I = 0x2

.field public static final TYPE_TILING_PATTERN:I = 0x1


# instance fields
.field private final patternDictionary:Lorg/apache/pdfbox/cos/COSDictionary;


# direct methods
.method public constructor <init>()V
    .locals 3

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-direct {v0}, Lorg/apache/pdfbox/cos/COSDictionary;-><init>()V

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/pattern/PDAbstractPattern;->patternDictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->TYPE:Lorg/apache/pdfbox/cos/COSName;

    sget-object v2, Lorg/apache/pdfbox/cos/COSName;->PATTERN:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v2}, Lorg/apache/pdfbox/cos/COSName;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->setName(Lorg/apache/pdfbox/cos/COSName;Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Lorg/apache/pdfbox/cos/COSDictionary;)V
    .locals 0

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/graphics/pattern/PDAbstractPattern;->patternDictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    return-void
.end method

.method public static create(Lorg/apache/pdfbox/cos/COSDictionary;)Lorg/apache/pdfbox/pdmodel/graphics/pattern/PDAbstractPattern;
    .locals 3

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->PATTERN_TYPE:Lorg/apache/pdfbox/cos/COSName;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getInt(Lorg/apache/pdfbox/cos/COSName;I)I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    new-instance v0, Lorg/apache/pdfbox/pdmodel/graphics/pattern/PDShadingPattern;

    invoke-direct {v0, p0}, Lorg/apache/pdfbox/pdmodel/graphics/pattern/PDShadingPattern;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    goto :goto_0

    :cond_0
    new-instance p0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Error: Unknown pattern type "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p0

    :cond_1
    new-instance v0, Lorg/apache/pdfbox/pdmodel/graphics/pattern/PDTilingPattern;

    invoke-direct {v0, p0}, Lorg/apache/pdfbox/pdmodel/graphics/pattern/PDTilingPattern;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    :goto_0
    return-object v0
.end method


# virtual methods
.method public getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/pattern/PDAbstractPattern;->patternDictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    return-object v0
.end method

.method public getCOSObject()Lorg/apache/pdfbox/cos/COSBase;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/pattern/PDAbstractPattern;->patternDictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    return-object v0
.end method

.method public getFilter()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/pattern/PDAbstractPattern;->patternDictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->FILTER:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getNameAsString(Lorg/apache/pdfbox/cos/COSName;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getLength()I
    .locals 3

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/pattern/PDAbstractPattern;->patternDictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->LENGTH:Lorg/apache/pdfbox/cos/COSName;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->getInt(Lorg/apache/pdfbox/cos/COSName;I)I

    move-result v0

    return v0
.end method

.method public getMatrix()Lorg/apache/pdfbox/util/Matrix;
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/pattern/PDAbstractPattern;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->MATRIX:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSArray;

    if-eqz v0, :cond_0

    new-instance v1, Lorg/apache/pdfbox/util/Matrix;

    invoke-direct {v1, v0}, Lorg/apache/pdfbox/util/Matrix;-><init>(Lorg/apache/pdfbox/cos/COSArray;)V

    return-object v1

    :cond_0
    new-instance v0, Lorg/apache/pdfbox/util/Matrix;

    invoke-direct {v0}, Lorg/apache/pdfbox/util/Matrix;-><init>()V

    return-object v0
.end method

.method public abstract getPatternType()I
.end method

.method public getType()Ljava/lang/String;
    .locals 1

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->PATTERN:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSName;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public setFilter(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/pattern/PDAbstractPattern;->patternDictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->FILTER:Lorg/apache/pdfbox/cos/COSName;

    invoke-static {p1}, Lorg/apache/pdfbox/cos/COSName;->getPDFName(Ljava/lang/String;)Lorg/apache/pdfbox/cos/COSName;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    return-void
.end method

.method public setLength(I)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/pattern/PDAbstractPattern;->patternDictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->LENGTH:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setInt(Lorg/apache/pdfbox/cos/COSName;I)V

    return-void
.end method

.method public setMatrix(Lorg/apache/pdfbox/util/awt/AffineTransform;)V
    .locals 6

    new-instance v0, Lorg/apache/pdfbox/cos/COSArray;

    invoke-direct {v0}, Lorg/apache/pdfbox/cos/COSArray;-><init>()V

    const/4 v1, 0x6

    new-array v2, v1, [D

    invoke-virtual {p1, v2}, Lorg/apache/pdfbox/util/awt/AffineTransform;->getMatrix([D)V

    const/4 p1, 0x0

    :goto_0
    if-ge p1, v1, :cond_0

    aget-wide v3, v2, p1

    new-instance v5, Lorg/apache/pdfbox/cos/COSFloat;

    double-to-float v3, v3

    invoke-direct {v5, v3}, Lorg/apache/pdfbox/cos/COSFloat;-><init>(F)V

    invoke-virtual {v0, v5}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/pattern/PDAbstractPattern;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object p1

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->MATRIX:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p1, v1, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    return-void
.end method

.method public setPaintType(I)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/pattern/PDAbstractPattern;->patternDictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->PAINT_TYPE:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setInt(Lorg/apache/pdfbox/cos/COSName;I)V

    return-void
.end method

.method public setPatternType(I)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/pattern/PDAbstractPattern;->patternDictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->PATTERN_TYPE:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setInt(Lorg/apache/pdfbox/cos/COSName;I)V

    return-void
.end method

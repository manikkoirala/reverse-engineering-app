.class public Lorg/apache/pdfbox/pdmodel/graphics/form/PDFormXObject;
.super Lorg/apache/pdfbox/pdmodel/graphics/PDXObject;
.source "SourceFile"

# interfaces
.implements Lorg/apache/pdfbox/contentstream/PDContentStream;


# instance fields
.field private group:Lorg/apache/pdfbox/pdmodel/graphics/form/PDGroup;

.field private name:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lorg/apache/pdfbox/pdmodel/PDDocument;)V
    .locals 1

    .line 1
    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->FORM:Lorg/apache/pdfbox/cos/COSName;

    invoke-direct {p0, p1, v0}, Lorg/apache/pdfbox/pdmodel/graphics/PDXObject;-><init>(Lorg/apache/pdfbox/pdmodel/PDDocument;Lorg/apache/pdfbox/cos/COSName;)V

    return-void
.end method

.method public constructor <init>(Lorg/apache/pdfbox/pdmodel/common/PDStream;)V
    .locals 1

    .line 2
    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->FORM:Lorg/apache/pdfbox/cos/COSName;

    invoke-direct {p0, p1, v0}, Lorg/apache/pdfbox/pdmodel/graphics/PDXObject;-><init>(Lorg/apache/pdfbox/pdmodel/common/PDStream;Lorg/apache/pdfbox/cos/COSName;)V

    return-void
.end method

.method public constructor <init>(Lorg/apache/pdfbox/pdmodel/common/PDStream;Ljava/lang/String;)V
    .locals 1

    .line 3
    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->FORM:Lorg/apache/pdfbox/cos/COSName;

    invoke-direct {p0, p1, v0}, Lorg/apache/pdfbox/pdmodel/graphics/PDXObject;-><init>(Lorg/apache/pdfbox/pdmodel/common/PDStream;Lorg/apache/pdfbox/cos/COSName;)V

    iput-object p2, p0, Lorg/apache/pdfbox/pdmodel/graphics/form/PDFormXObject;->name:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getBBox()Lorg/apache/pdfbox/pdmodel/common/PDRectangle;
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/PDXObject;->getCOSStream()Lorg/apache/pdfbox/cos/COSStream;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->BBOX:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSArray;

    if-eqz v0, :cond_0

    new-instance v1, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;

    invoke-direct {v1, v0}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;-><init>(Lorg/apache/pdfbox/cos/COSArray;)V

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return-object v1
.end method

.method public getContentStream()Lorg/apache/pdfbox/cos/COSStream;
    .locals 1

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/PDXObject;->getCOSStream()Lorg/apache/pdfbox/cos/COSStream;

    move-result-object v0

    return-object v0
.end method

.method public getFormType()I
    .locals 3

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/PDXObject;->getCOSStream()Lorg/apache/pdfbox/cos/COSStream;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->FORMTYPE:Lorg/apache/pdfbox/cos/COSName;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->getInt(Lorg/apache/pdfbox/cos/COSName;I)I

    move-result v0

    return v0
.end method

.method public getGroup()Lorg/apache/pdfbox/pdmodel/graphics/form/PDGroup;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/form/PDFormXObject;->group:Lorg/apache/pdfbox/pdmodel/graphics/form/PDGroup;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/PDXObject;->getCOSStream()Lorg/apache/pdfbox/cos/COSStream;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->GROUP:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSDictionary;

    if-eqz v0, :cond_0

    new-instance v1, Lorg/apache/pdfbox/pdmodel/graphics/form/PDGroup;

    invoke-direct {v1, v0}, Lorg/apache/pdfbox/pdmodel/graphics/form/PDGroup;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    iput-object v1, p0, Lorg/apache/pdfbox/pdmodel/graphics/form/PDFormXObject;->group:Lorg/apache/pdfbox/pdmodel/graphics/form/PDGroup;

    :cond_0
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/form/PDFormXObject;->group:Lorg/apache/pdfbox/pdmodel/graphics/form/PDGroup;

    return-object v0
.end method

.method public getMatrix()Lorg/apache/pdfbox/util/Matrix;
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/PDXObject;->getCOSStream()Lorg/apache/pdfbox/cos/COSStream;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->MATRIX:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSArray;

    if-eqz v0, :cond_0

    new-instance v1, Lorg/apache/pdfbox/util/Matrix;

    invoke-direct {v1, v0}, Lorg/apache/pdfbox/util/Matrix;-><init>(Lorg/apache/pdfbox/cos/COSArray;)V

    return-object v1

    :cond_0
    new-instance v0, Lorg/apache/pdfbox/util/Matrix;

    invoke-direct {v0}, Lorg/apache/pdfbox/util/Matrix;-><init>()V

    return-object v0
.end method

.method public getResources()Lorg/apache/pdfbox/pdmodel/PDResources;
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/PDXObject;->getCOSStream()Lorg/apache/pdfbox/cos/COSStream;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->RESOURCES:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSDictionary;

    if-eqz v0, :cond_0

    new-instance v1, Lorg/apache/pdfbox/pdmodel/PDResources;

    invoke-direct {v1, v0}, Lorg/apache/pdfbox/pdmodel/PDResources;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    return-object v1

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public getStructParents()I
    .locals 3

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/PDXObject;->getCOSStream()Lorg/apache/pdfbox/cos/COSStream;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->STRUCT_PARENTS:Lorg/apache/pdfbox/cos/COSName;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->getInt(Lorg/apache/pdfbox/cos/COSName;I)I

    move-result v0

    return v0
.end method

.method public setBBox(Lorg/apache/pdfbox/pdmodel/common/PDRectangle;)V
    .locals 2

    if-nez p1, :cond_0

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/PDXObject;->getCOSStream()Lorg/apache/pdfbox/cos/COSStream;

    move-result-object p1

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->BBOX:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p1, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->removeItem(Lorg/apache/pdfbox/cos/COSName;)V

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/PDXObject;->getCOSStream()Lorg/apache/pdfbox/cos/COSStream;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->BBOX:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->getCOSArray()Lorg/apache/pdfbox/cos/COSArray;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    :goto_0
    return-void
.end method

.method public setFormType(I)V
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/PDXObject;->getCOSStream()Lorg/apache/pdfbox/cos/COSStream;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->FORMTYPE:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setInt(Lorg/apache/pdfbox/cos/COSName;I)V

    return-void
.end method

.method public setMatrix(Lorg/apache/pdfbox/util/awt/AffineTransform;)V
    .locals 6

    new-instance v0, Lorg/apache/pdfbox/cos/COSArray;

    invoke-direct {v0}, Lorg/apache/pdfbox/cos/COSArray;-><init>()V

    const/4 v1, 0x6

    new-array v2, v1, [D

    invoke-virtual {p1, v2}, Lorg/apache/pdfbox/util/awt/AffineTransform;->getMatrix([D)V

    const/4 p1, 0x0

    :goto_0
    if-ge p1, v1, :cond_0

    aget-wide v3, v2, p1

    new-instance v5, Lorg/apache/pdfbox/cos/COSFloat;

    double-to-float v3, v3

    invoke-direct {v5, v3}, Lorg/apache/pdfbox/cos/COSFloat;-><init>(F)V

    invoke-virtual {v0, v5}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/PDXObject;->getCOSStream()Lorg/apache/pdfbox/cos/COSStream;

    move-result-object p1

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->MATRIX:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p1, v1, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    return-void
.end method

.method public setResources(Lorg/apache/pdfbox/pdmodel/PDResources;)V
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/PDXObject;->getCOSStream()Lorg/apache/pdfbox/cos/COSStream;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->RESOURCES:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)V

    return-void
.end method

.method public setStructParents(I)V
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/PDXObject;->getCOSStream()Lorg/apache/pdfbox/cos/COSStream;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->STRUCT_PARENTS:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setInt(Lorg/apache/pdfbox/cos/COSName;I)V

    return-void
.end method

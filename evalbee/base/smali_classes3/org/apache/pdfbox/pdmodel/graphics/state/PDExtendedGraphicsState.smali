.class public Lorg/apache/pdfbox/pdmodel/graphics/state/PDExtendedGraphicsState;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/pdfbox/pdmodel/common/COSObjectable;


# instance fields
.field private final dict:Lorg/apache/pdfbox/cos/COSDictionary;


# direct methods
.method public constructor <init>()V
    .locals 3

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-direct {v0}, Lorg/apache/pdfbox/cos/COSDictionary;-><init>()V

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDExtendedGraphicsState;->dict:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->TYPE:Lorg/apache/pdfbox/cos/COSName;

    sget-object v2, Lorg/apache/pdfbox/cos/COSName;->EXT_G_STATE:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    return-void
.end method

.method public constructor <init>(Lorg/apache/pdfbox/cos/COSDictionary;)V
    .locals 0

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDExtendedGraphicsState;->dict:Lorg/apache/pdfbox/cos/COSDictionary;

    return-void
.end method

.method private getFloatItem(Lorg/apache/pdfbox/cos/COSName;)Ljava/lang/Float;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDExtendedGraphicsState;->dict:Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-virtual {v0, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object p1

    check-cast p1, Lorg/apache/pdfbox/cos/COSNumber;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lorg/apache/pdfbox/cos/COSNumber;->floatValue()F

    move-result p1

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return-object p1
.end method

.method private setFloatItem(Lorg/apache/pdfbox/cos/COSName;Ljava/lang/Float;)V
    .locals 2

    if-nez p2, :cond_0

    iget-object p2, p0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDExtendedGraphicsState;->dict:Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-virtual {p2, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->removeItem(Lorg/apache/pdfbox/cos/COSName;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDExtendedGraphicsState;->dict:Lorg/apache/pdfbox/cos/COSDictionary;

    new-instance v1, Lorg/apache/pdfbox/cos/COSFloat;

    invoke-virtual {p2}, Ljava/lang/Float;->floatValue()F

    move-result p2

    invoke-direct {v1, p2}, Lorg/apache/pdfbox/cos/COSFloat;-><init>(F)V

    invoke-virtual {v0, p1, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    :goto_0
    return-void
.end method


# virtual methods
.method public copyIntoGraphicsState(Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;)V
    .locals 4

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDExtendedGraphicsState;->dict:Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSDictionary;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_11

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/pdfbox/cos/COSName;

    sget-object v2, Lorg/apache/pdfbox/cos/COSName;->LW:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v1, v2}, Lorg/apache/pdfbox/cos/COSName;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDExtendedGraphicsState;->getLineWidth()Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v1}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->setLineWidth(F)V

    goto :goto_0

    :cond_1
    sget-object v2, Lorg/apache/pdfbox/cos/COSName;->LC:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v1, v2}, Lorg/apache/pdfbox/cos/COSName;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDExtendedGraphicsState;->getLineCapStyle()Landroid/graphics/Paint$Cap;

    move-result-object v1

    invoke-virtual {p1, v1}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->setLineCap(Landroid/graphics/Paint$Cap;)V

    goto :goto_0

    :cond_2
    sget-object v2, Lorg/apache/pdfbox/cos/COSName;->LJ:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v1, v2}, Lorg/apache/pdfbox/cos/COSName;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDExtendedGraphicsState;->getLineJoinStyle()Landroid/graphics/Paint$Join;

    move-result-object v1

    invoke-virtual {p1, v1}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->setLineJoin(Landroid/graphics/Paint$Join;)V

    goto :goto_0

    :cond_3
    sget-object v2, Lorg/apache/pdfbox/cos/COSName;->ML:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v1, v2}, Lorg/apache/pdfbox/cos/COSName;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDExtendedGraphicsState;->getMiterLimit()Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v1}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->setMiterLimit(F)V

    goto :goto_0

    :cond_4
    sget-object v2, Lorg/apache/pdfbox/cos/COSName;->D:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v1, v2}, Lorg/apache/pdfbox/cos/COSName;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDExtendedGraphicsState;->getLineDashPattern()Lorg/apache/pdfbox/pdmodel/graphics/PDLineDashPattern;

    move-result-object v1

    invoke-virtual {p1, v1}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->setLineDashPattern(Lorg/apache/pdfbox/pdmodel/graphics/PDLineDashPattern;)V

    goto :goto_0

    :cond_5
    sget-object v2, Lorg/apache/pdfbox/cos/COSName;->RI:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v1, v2}, Lorg/apache/pdfbox/cos/COSName;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDExtendedGraphicsState;->getRenderingIntent()Lorg/apache/pdfbox/pdmodel/graphics/state/RenderingIntent;

    move-result-object v1

    invoke-virtual {p1, v1}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->setRenderingIntent(Lorg/apache/pdfbox/pdmodel/graphics/state/RenderingIntent;)V

    goto :goto_0

    :cond_6
    sget-object v2, Lorg/apache/pdfbox/cos/COSName;->OPM:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v1, v2}, Lorg/apache/pdfbox/cos/COSName;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDExtendedGraphicsState;->getOverprintMode()Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Float;->doubleValue()D

    move-result-wide v1

    invoke-virtual {p1, v1, v2}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->setOverprintMode(D)V

    goto/16 :goto_0

    :cond_7
    sget-object v2, Lorg/apache/pdfbox/cos/COSName;->FONT:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v1, v2}, Lorg/apache/pdfbox/cos/COSName;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDExtendedGraphicsState;->getFontSetting()Lorg/apache/pdfbox/pdmodel/graphics/PDFontSetting;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->getTextState()Lorg/apache/pdfbox/pdmodel/graphics/state/PDTextState;

    move-result-object v2

    invoke-virtual {v1}, Lorg/apache/pdfbox/pdmodel/graphics/PDFontSetting;->getFont()Lorg/apache/pdfbox/pdmodel/font/PDFont;

    move-result-object v3

    invoke-virtual {v2, v3}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDTextState;->setFont(Lorg/apache/pdfbox/pdmodel/font/PDFont;)V

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->getTextState()Lorg/apache/pdfbox/pdmodel/graphics/state/PDTextState;

    move-result-object v2

    invoke-virtual {v1}, Lorg/apache/pdfbox/pdmodel/graphics/PDFontSetting;->getFontSize()F

    move-result v1

    invoke-virtual {v2, v1}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDTextState;->setFontSize(F)V

    goto/16 :goto_0

    :cond_8
    sget-object v2, Lorg/apache/pdfbox/cos/COSName;->FL:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v1, v2}, Lorg/apache/pdfbox/cos/COSName;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDExtendedGraphicsState;->getFlatnessTolerance()Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    float-to-double v1, v1

    invoke-virtual {p1, v1, v2}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->setFlatness(D)V

    goto/16 :goto_0

    :cond_9
    sget-object v2, Lorg/apache/pdfbox/cos/COSName;->SM:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v1, v2}, Lorg/apache/pdfbox/cos/COSName;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDExtendedGraphicsState;->getSmoothnessTolerance()Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    float-to-double v1, v1

    invoke-virtual {p1, v1, v2}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->setSmoothness(D)V

    goto/16 :goto_0

    :cond_a
    sget-object v2, Lorg/apache/pdfbox/cos/COSName;->SA:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v1, v2}, Lorg/apache/pdfbox/cos/COSName;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_b

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDExtendedGraphicsState;->getAutomaticStrokeAdjustment()Z

    move-result v1

    invoke-virtual {p1, v1}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->setStrokeAdjustment(Z)V

    goto/16 :goto_0

    :cond_b
    sget-object v2, Lorg/apache/pdfbox/cos/COSName;->CA:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v1, v2}, Lorg/apache/pdfbox/cos/COSName;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_c

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDExtendedGraphicsState;->getStrokingAlphaConstant()Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    float-to-double v1, v1

    invoke-virtual {p1, v1, v2}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->setAlphaConstant(D)V

    goto/16 :goto_0

    :cond_c
    sget-object v2, Lorg/apache/pdfbox/cos/COSName;->CA_NS:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v1, v2}, Lorg/apache/pdfbox/cos/COSName;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_d

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDExtendedGraphicsState;->getNonStrokingAlphaConstant()Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    float-to-double v1, v1

    invoke-virtual {p1, v1, v2}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->setNonStrokeAlphaConstant(D)V

    goto/16 :goto_0

    :cond_d
    sget-object v2, Lorg/apache/pdfbox/cos/COSName;->AIS:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v1, v2}, Lorg/apache/pdfbox/cos/COSName;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_e

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDExtendedGraphicsState;->getAlphaSourceFlag()Z

    move-result v1

    invoke-virtual {p1, v1}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->setAlphaSource(Z)V

    goto/16 :goto_0

    :cond_e
    sget-object v2, Lorg/apache/pdfbox/cos/COSName;->TK:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v1, v2}, Lorg/apache/pdfbox/cos/COSName;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_f

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->getTextState()Lorg/apache/pdfbox/pdmodel/graphics/state/PDTextState;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDExtendedGraphicsState;->getTextKnockoutFlag()Z

    move-result v2

    invoke-virtual {v1, v2}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDTextState;->setKnockoutFlag(Z)V

    goto/16 :goto_0

    :cond_f
    sget-object v2, Lorg/apache/pdfbox/cos/COSName;->SMASK:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v1, v2}, Lorg/apache/pdfbox/cos/COSName;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_10

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDExtendedGraphicsState;->getSoftMask()Lorg/apache/pdfbox/pdmodel/graphics/state/PDSoftMask;

    move-result-object v1

    invoke-virtual {p1, v1}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->setSoftMask(Lorg/apache/pdfbox/pdmodel/graphics/state/PDSoftMask;)V

    goto/16 :goto_0

    :cond_10
    sget-object v2, Lorg/apache/pdfbox/cos/COSName;->BM:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v1, v2}, Lorg/apache/pdfbox/cos/COSName;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDExtendedGraphicsState;->getBlendMode()Lorg/apache/pdfbox/pdmodel/graphics/blend/BlendMode;

    move-result-object v1

    invoke-virtual {p1, v1}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->setBlendMode(Lorg/apache/pdfbox/pdmodel/graphics/blend/BlendMode;)V

    goto/16 :goto_0

    :cond_11
    return-void
.end method

.method public getAlphaSourceFlag()Z
    .locals 3

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDExtendedGraphicsState;->dict:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->AIS:Lorg/apache/pdfbox/cos/COSName;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->getBoolean(Lorg/apache/pdfbox/cos/COSName;Z)Z

    move-result v0

    return v0
.end method

.method public getAutomaticStrokeAdjustment()Z
    .locals 3

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDExtendedGraphicsState;->dict:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->SA:Lorg/apache/pdfbox/cos/COSName;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->getBoolean(Lorg/apache/pdfbox/cos/COSName;Z)Z

    move-result v0

    return v0
.end method

.method public getBlendMode()Lorg/apache/pdfbox/pdmodel/graphics/blend/BlendMode;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDExtendedGraphicsState;->dict:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->BM:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/pdfbox/pdmodel/graphics/blend/BlendMode;->getInstance(Lorg/apache/pdfbox/cos/COSBase;)Lorg/apache/pdfbox/pdmodel/graphics/blend/BlendMode;

    move-result-object v0

    return-object v0
.end method

.method public getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDExtendedGraphicsState;->dict:Lorg/apache/pdfbox/cos/COSDictionary;

    return-object v0
.end method

.method public getCOSObject()Lorg/apache/pdfbox/cos/COSBase;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDExtendedGraphicsState;->dict:Lorg/apache/pdfbox/cos/COSDictionary;

    return-object v0
.end method

.method public getFlatnessTolerance()Ljava/lang/Float;
    .locals 1

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->FL:Lorg/apache/pdfbox/cos/COSName;

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDExtendedGraphicsState;->getFloatItem(Lorg/apache/pdfbox/cos/COSName;)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public getFontSetting()Lorg/apache/pdfbox/pdmodel/graphics/PDFontSetting;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDExtendedGraphicsState;->dict:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->FONT:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    instance-of v1, v0, Lorg/apache/pdfbox/cos/COSArray;

    if-eqz v1, :cond_0

    check-cast v0, Lorg/apache/pdfbox/cos/COSArray;

    new-instance v1, Lorg/apache/pdfbox/pdmodel/graphics/PDFontSetting;

    invoke-direct {v1, v0}, Lorg/apache/pdfbox/pdmodel/graphics/PDFontSetting;-><init>(Lorg/apache/pdfbox/cos/COSArray;)V

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return-object v1
.end method

.method public getLineCapStyle()Landroid/graphics/Paint$Cap;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDExtendedGraphicsState;->dict:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->LC:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getInt(Lorg/apache/pdfbox/cos/COSName;)I

    move-result v0

    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    const/4 v0, 0x0

    return-object v0

    :cond_0
    sget-object v0, Landroid/graphics/Paint$Cap;->SQUARE:Landroid/graphics/Paint$Cap;

    return-object v0

    :cond_1
    sget-object v0, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;

    return-object v0

    :cond_2
    sget-object v0, Landroid/graphics/Paint$Cap;->BUTT:Landroid/graphics/Paint$Cap;

    return-object v0
.end method

.method public getLineDashPattern()Lorg/apache/pdfbox/pdmodel/graphics/PDLineDashPattern;
    .locals 3

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDExtendedGraphicsState;->dict:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->D:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSArray;

    if-eqz v0, :cond_0

    new-instance v1, Lorg/apache/pdfbox/cos/COSArray;

    invoke-direct {v1}, Lorg/apache/pdfbox/cos/COSArray;-><init>()V

    invoke-virtual {v0, v0}, Lorg/apache/pdfbox/cos/COSArray;->addAll(Lorg/apache/pdfbox/cos/COSArray;)V

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSArray;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v0, v2}, Lorg/apache/pdfbox/cos/COSArray;->remove(I)Lorg/apache/pdfbox/cos/COSBase;

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSArray;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v0, v2}, Lorg/apache/pdfbox/cos/COSArray;->getInt(I)I

    move-result v0

    new-instance v2, Lorg/apache/pdfbox/pdmodel/graphics/PDLineDashPattern;

    invoke-direct {v2, v1, v0}, Lorg/apache/pdfbox/pdmodel/graphics/PDLineDashPattern;-><init>(Lorg/apache/pdfbox/cos/COSArray;I)V

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    return-object v2
.end method

.method public getLineJoinStyle()Landroid/graphics/Paint$Join;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDExtendedGraphicsState;->dict:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->LJ:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getInt(Lorg/apache/pdfbox/cos/COSName;)I

    move-result v0

    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    const/4 v0, 0x0

    return-object v0

    :cond_0
    sget-object v0, Landroid/graphics/Paint$Join;->BEVEL:Landroid/graphics/Paint$Join;

    return-object v0

    :cond_1
    sget-object v0, Landroid/graphics/Paint$Join;->ROUND:Landroid/graphics/Paint$Join;

    return-object v0

    :cond_2
    sget-object v0, Landroid/graphics/Paint$Join;->MITER:Landroid/graphics/Paint$Join;

    return-object v0
.end method

.method public getLineWidth()Ljava/lang/Float;
    .locals 1

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->LW:Lorg/apache/pdfbox/cos/COSName;

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDExtendedGraphicsState;->getFloatItem(Lorg/apache/pdfbox/cos/COSName;)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public getMiterLimit()Ljava/lang/Float;
    .locals 1

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->ML:Lorg/apache/pdfbox/cos/COSName;

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDExtendedGraphicsState;->getFloatItem(Lorg/apache/pdfbox/cos/COSName;)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public getNonStrokingAlphaConstant()Ljava/lang/Float;
    .locals 1

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->CA_NS:Lorg/apache/pdfbox/cos/COSName;

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDExtendedGraphicsState;->getFloatItem(Lorg/apache/pdfbox/cos/COSName;)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public getNonStrokingOverprintControl()Z
    .locals 3

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDExtendedGraphicsState;->dict:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->OP_NS:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDExtendedGraphicsState;->getStrokingOverprintControl()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->getBoolean(Lorg/apache/pdfbox/cos/COSName;Z)Z

    move-result v0

    return v0
.end method

.method public getOverprintMode()Ljava/lang/Float;
    .locals 1

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->OPM:Lorg/apache/pdfbox/cos/COSName;

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDExtendedGraphicsState;->getFloatItem(Lorg/apache/pdfbox/cos/COSName;)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public getRenderingIntent()Lorg/apache/pdfbox/pdmodel/graphics/state/RenderingIntent;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDExtendedGraphicsState;->dict:Lorg/apache/pdfbox/cos/COSDictionary;

    const-string v1, "RI"

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getNameAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {v0}, Lorg/apache/pdfbox/pdmodel/graphics/state/RenderingIntent;->fromString(Ljava/lang/String;)Lorg/apache/pdfbox/pdmodel/graphics/state/RenderingIntent;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public getSmoothnessTolerance()Ljava/lang/Float;
    .locals 1

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->SM:Lorg/apache/pdfbox/cos/COSName;

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDExtendedGraphicsState;->getFloatItem(Lorg/apache/pdfbox/cos/COSName;)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public getSoftMask()Lorg/apache/pdfbox/pdmodel/graphics/state/PDSoftMask;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDExtendedGraphicsState;->dict:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->SMASK:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDSoftMask;->create(Lorg/apache/pdfbox/cos/COSBase;)Lorg/apache/pdfbox/pdmodel/graphics/state/PDSoftMask;

    move-result-object v0

    return-object v0
.end method

.method public getStrokingAlphaConstant()Ljava/lang/Float;
    .locals 1

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->CA:Lorg/apache/pdfbox/cos/COSName;

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDExtendedGraphicsState;->getFloatItem(Lorg/apache/pdfbox/cos/COSName;)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public getStrokingOverprintControl()Z
    .locals 3

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDExtendedGraphicsState;->dict:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->OP:Lorg/apache/pdfbox/cos/COSName;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->getBoolean(Lorg/apache/pdfbox/cos/COSName;Z)Z

    move-result v0

    return v0
.end method

.method public getTextKnockoutFlag()Z
    .locals 3

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDExtendedGraphicsState;->dict:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->TK:Lorg/apache/pdfbox/cos/COSName;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->getBoolean(Lorg/apache/pdfbox/cos/COSName;Z)Z

    move-result v0

    return v0
.end method

.method public setAlphaSourceFlag(Z)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDExtendedGraphicsState;->dict:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->AIS:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setBoolean(Lorg/apache/pdfbox/cos/COSName;Z)V

    return-void
.end method

.method public setAutomaticStrokeAdjustment(Z)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDExtendedGraphicsState;->dict:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->SA:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setBoolean(Lorg/apache/pdfbox/cos/COSName;Z)V

    return-void
.end method

.method public setFlatnessTolerance(Ljava/lang/Float;)V
    .locals 1

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->FL:Lorg/apache/pdfbox/cos/COSName;

    invoke-direct {p0, v0, p1}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDExtendedGraphicsState;->setFloatItem(Lorg/apache/pdfbox/cos/COSName;Ljava/lang/Float;)V

    return-void
.end method

.method public setFontSetting(Lorg/apache/pdfbox/pdmodel/graphics/PDFontSetting;)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDExtendedGraphicsState;->dict:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->FONT:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)V

    return-void
.end method

.method public setLineCapStyle(I)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDExtendedGraphicsState;->dict:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->LC:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setInt(Lorg/apache/pdfbox/cos/COSName;I)V

    return-void
.end method

.method public setLineDashPattern(Lorg/apache/pdfbox/pdmodel/graphics/PDLineDashPattern;)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDExtendedGraphicsState;->dict:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->D:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/graphics/PDLineDashPattern;->getCOSObject()Lorg/apache/pdfbox/cos/COSBase;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    return-void
.end method

.method public setLineJoinStyle(I)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDExtendedGraphicsState;->dict:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->LJ:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setInt(Lorg/apache/pdfbox/cos/COSName;I)V

    return-void
.end method

.method public setLineWidth(Ljava/lang/Float;)V
    .locals 1

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->LW:Lorg/apache/pdfbox/cos/COSName;

    invoke-direct {p0, v0, p1}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDExtendedGraphicsState;->setFloatItem(Lorg/apache/pdfbox/cos/COSName;Ljava/lang/Float;)V

    return-void
.end method

.method public setMiterLimit(Ljava/lang/Float;)V
    .locals 1

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->ML:Lorg/apache/pdfbox/cos/COSName;

    invoke-direct {p0, v0, p1}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDExtendedGraphicsState;->setFloatItem(Lorg/apache/pdfbox/cos/COSName;Ljava/lang/Float;)V

    return-void
.end method

.method public setNonStrokingAlphaConstant(Ljava/lang/Float;)V
    .locals 1

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->CA_NS:Lorg/apache/pdfbox/cos/COSName;

    invoke-direct {p0, v0, p1}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDExtendedGraphicsState;->setFloatItem(Lorg/apache/pdfbox/cos/COSName;Ljava/lang/Float;)V

    return-void
.end method

.method public setNonStrokingOverprintControl(Z)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDExtendedGraphicsState;->dict:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->OP_NS:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setBoolean(Lorg/apache/pdfbox/cos/COSName;Z)V

    return-void
.end method

.method public setOverprintMode(Ljava/lang/Float;)V
    .locals 1

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->OPM:Lorg/apache/pdfbox/cos/COSName;

    invoke-direct {p0, v0, p1}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDExtendedGraphicsState;->setFloatItem(Lorg/apache/pdfbox/cos/COSName;Ljava/lang/Float;)V

    return-void
.end method

.method public setRenderingIntent(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDExtendedGraphicsState;->dict:Lorg/apache/pdfbox/cos/COSDictionary;

    const-string v1, "RI"

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setName(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public setSmoothnessTolerance(Ljava/lang/Float;)V
    .locals 1

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->SM:Lorg/apache/pdfbox/cos/COSName;

    invoke-direct {p0, v0, p1}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDExtendedGraphicsState;->setFloatItem(Lorg/apache/pdfbox/cos/COSName;Ljava/lang/Float;)V

    return-void
.end method

.method public setStrokingAlphaConstant(Ljava/lang/Float;)V
    .locals 1

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->CA:Lorg/apache/pdfbox/cos/COSName;

    invoke-direct {p0, v0, p1}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDExtendedGraphicsState;->setFloatItem(Lorg/apache/pdfbox/cos/COSName;Ljava/lang/Float;)V

    return-void
.end method

.method public setStrokingOverprintControl(Z)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDExtendedGraphicsState;->dict:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->OP:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setBoolean(Lorg/apache/pdfbox/cos/COSName;Z)V

    return-void
.end method

.method public setTextKnockoutFlag(Z)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDExtendedGraphicsState;->dict:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->TK:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setBoolean(Lorg/apache/pdfbox/cos/COSName;Z)V

    return-void
.end method

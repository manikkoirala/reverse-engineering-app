.class public final Lorg/apache/pdfbox/pdmodel/graphics/state/PDSoftMask;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/pdfbox/pdmodel/common/COSObjectable;


# instance fields
.field private backdropColor:Lorg/apache/pdfbox/cos/COSArray;

.field private dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

.field private group:Lorg/apache/pdfbox/pdmodel/graphics/form/PDFormXObject;

.field private subType:Lorg/apache/pdfbox/cos/COSName;

.field private transferFunction:Lorg/apache/pdfbox/pdmodel/common/function/PDFunction;


# direct methods
.method public constructor <init>(Lorg/apache/pdfbox/cos/COSDictionary;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDSoftMask;->subType:Lorg/apache/pdfbox/cos/COSName;

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDSoftMask;->group:Lorg/apache/pdfbox/pdmodel/graphics/form/PDFormXObject;

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDSoftMask;->backdropColor:Lorg/apache/pdfbox/cos/COSArray;

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDSoftMask;->transferFunction:Lorg/apache/pdfbox/pdmodel/common/function/PDFunction;

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDSoftMask;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    return-void
.end method

.method public static create(Lorg/apache/pdfbox/cos/COSBase;)Lorg/apache/pdfbox/pdmodel/graphics/state/PDSoftMask;
    .locals 4

    instance-of v0, p0, Lorg/apache/pdfbox/cos/COSName;

    const-string v1, "Invalid SMask "

    const-string v2, "PdfBoxAndroid"

    const/4 v3, 0x0

    if-eqz v0, :cond_1

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->NONE:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, p0}, Lorg/apache/pdfbox/cos/COSName;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-object v3

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    :goto_0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-static {v2, p0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    return-object v3

    :cond_1
    instance-of v0, p0, Lorg/apache/pdfbox/cos/COSDictionary;

    if-eqz v0, :cond_2

    new-instance v0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDSoftMask;

    check-cast p0, Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-direct {v0, p0}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDSoftMask;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    return-object v0

    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    goto :goto_0
.end method


# virtual methods
.method public getBackdropColor()Lorg/apache/pdfbox/cos/COSArray;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDSoftMask;->backdropColor:Lorg/apache/pdfbox/cos/COSArray;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDSoftMask;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->BC:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSArray;

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDSoftMask;->backdropColor:Lorg/apache/pdfbox/cos/COSArray;

    :cond_0
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDSoftMask;->backdropColor:Lorg/apache/pdfbox/cos/COSArray;

    return-object v0
.end method

.method public getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDSoftMask;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    return-object v0
.end method

.method public getCOSObject()Lorg/apache/pdfbox/cos/COSBase;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDSoftMask;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    return-object v0
.end method

.method public getGroup()Lorg/apache/pdfbox/pdmodel/graphics/form/PDFormXObject;
    .locals 3

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDSoftMask;->group:Lorg/apache/pdfbox/pdmodel/graphics/form/PDFormXObject;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDSoftMask;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->G:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v1}, Lorg/apache/pdfbox/cos/COSName;->getName()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lorg/apache/pdfbox/pdmodel/graphics/PDXObject;->createXObject(Lorg/apache/pdfbox/cos/COSBase;Ljava/lang/String;Lorg/apache/pdfbox/pdmodel/PDResources;)Lorg/apache/pdfbox/pdmodel/graphics/PDXObject;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/pdmodel/graphics/form/PDFormXObject;

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDSoftMask;->group:Lorg/apache/pdfbox/pdmodel/graphics/form/PDFormXObject;

    :cond_0
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDSoftMask;->group:Lorg/apache/pdfbox/pdmodel/graphics/form/PDFormXObject;

    return-object v0
.end method

.method public getSubType()Lorg/apache/pdfbox/cos/COSName;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDSoftMask;->subType:Lorg/apache/pdfbox/cos/COSName;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDSoftMask;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->S:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSName;

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDSoftMask;->subType:Lorg/apache/pdfbox/cos/COSName;

    :cond_0
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDSoftMask;->subType:Lorg/apache/pdfbox/cos/COSName;

    return-object v0
.end method

.method public getTransferFunction()Lorg/apache/pdfbox/pdmodel/common/function/PDFunction;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDSoftMask;->transferFunction:Lorg/apache/pdfbox/pdmodel/common/function/PDFunction;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDSoftMask;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->TR:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {v0}, Lorg/apache/pdfbox/pdmodel/common/function/PDFunction;->create(Lorg/apache/pdfbox/cos/COSBase;)Lorg/apache/pdfbox/pdmodel/common/function/PDFunction;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDSoftMask;->transferFunction:Lorg/apache/pdfbox/pdmodel/common/function/PDFunction;

    :cond_0
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDSoftMask;->transferFunction:Lorg/apache/pdfbox/pdmodel/common/function/PDFunction;

    return-object v0
.end method

.class public abstract Lorg/apache/pdfbox/pdmodel/graphics/shading/PDShading;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/pdfbox/pdmodel/common/COSObjectable;


# static fields
.field public static final SHADING_TYPE1:I = 0x1

.field public static final SHADING_TYPE2:I = 0x2

.field public static final SHADING_TYPE3:I = 0x3

.field public static final SHADING_TYPE4:I = 0x4

.field public static final SHADING_TYPE5:I = 0x5

.field public static final SHADING_TYPE6:I = 0x6

.field public static final SHADING_TYPE7:I = 0x7


# instance fields
.field private bBox:Lorg/apache/pdfbox/pdmodel/common/PDRectangle;

.field private background:Lorg/apache/pdfbox/cos/COSArray;

.field private colorSpace:Lorg/apache/pdfbox/pdmodel/graphics/color/PDColorSpace;

.field private dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

.field private function:Lorg/apache/pdfbox/pdmodel/common/function/PDFunction;

.field private functionArray:[Lorg/apache/pdfbox/pdmodel/common/function/PDFunction;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/shading/PDShading;->background:Lorg/apache/pdfbox/cos/COSArray;

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/shading/PDShading;->bBox:Lorg/apache/pdfbox/pdmodel/common/PDRectangle;

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/shading/PDShading;->colorSpace:Lorg/apache/pdfbox/pdmodel/graphics/color/PDColorSpace;

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/shading/PDShading;->function:Lorg/apache/pdfbox/pdmodel/common/function/PDFunction;

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/shading/PDShading;->functionArray:[Lorg/apache/pdfbox/pdmodel/common/function/PDFunction;

    new-instance v0, Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-direct {v0}, Lorg/apache/pdfbox/cos/COSDictionary;-><init>()V

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/shading/PDShading;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    return-void
.end method

.method public constructor <init>(Lorg/apache/pdfbox/cos/COSDictionary;)V
    .locals 1

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/shading/PDShading;->background:Lorg/apache/pdfbox/cos/COSArray;

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/shading/PDShading;->bBox:Lorg/apache/pdfbox/pdmodel/common/PDRectangle;

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/shading/PDShading;->colorSpace:Lorg/apache/pdfbox/pdmodel/graphics/color/PDColorSpace;

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/shading/PDShading;->function:Lorg/apache/pdfbox/pdmodel/common/function/PDFunction;

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/shading/PDShading;->functionArray:[Lorg/apache/pdfbox/pdmodel/common/function/PDFunction;

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/graphics/shading/PDShading;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    return-void
.end method

.method public static create(Lorg/apache/pdfbox/cos/COSDictionary;)Lorg/apache/pdfbox/pdmodel/graphics/shading/PDShading;
    .locals 3

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->SHADING_TYPE:Lorg/apache/pdfbox/cos/COSName;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getInt(Lorg/apache/pdfbox/cos/COSName;I)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    new-instance p0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Error: Unknown shading type "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p0

    :pswitch_0
    new-instance v0, Lorg/apache/pdfbox/pdmodel/graphics/shading/PDShadingType7;

    invoke-direct {v0, p0}, Lorg/apache/pdfbox/pdmodel/graphics/shading/PDShadingType7;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    goto :goto_0

    :pswitch_1
    new-instance v0, Lorg/apache/pdfbox/pdmodel/graphics/shading/PDShadingType6;

    invoke-direct {v0, p0}, Lorg/apache/pdfbox/pdmodel/graphics/shading/PDShadingType6;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    goto :goto_0

    :pswitch_2
    new-instance v0, Lorg/apache/pdfbox/pdmodel/graphics/shading/PDShadingType5;

    invoke-direct {v0, p0}, Lorg/apache/pdfbox/pdmodel/graphics/shading/PDShadingType5;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    goto :goto_0

    :pswitch_3
    new-instance v0, Lorg/apache/pdfbox/pdmodel/graphics/shading/PDShadingType4;

    invoke-direct {v0, p0}, Lorg/apache/pdfbox/pdmodel/graphics/shading/PDShadingType4;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    goto :goto_0

    :pswitch_4
    new-instance v0, Lorg/apache/pdfbox/pdmodel/graphics/shading/PDShadingType3;

    invoke-direct {v0, p0}, Lorg/apache/pdfbox/pdmodel/graphics/shading/PDShadingType3;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    goto :goto_0

    :pswitch_5
    new-instance v0, Lorg/apache/pdfbox/pdmodel/graphics/shading/PDShadingType2;

    invoke-direct {v0, p0}, Lorg/apache/pdfbox/pdmodel/graphics/shading/PDShadingType2;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    goto :goto_0

    :pswitch_6
    new-instance v0, Lorg/apache/pdfbox/pdmodel/graphics/shading/PDShadingType1;

    invoke-direct {v0, p0}, Lorg/apache/pdfbox/pdmodel/graphics/shading/PDShadingType1;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    :goto_0
    return-object v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private getFunctionsArray()[Lorg/apache/pdfbox/pdmodel/common/function/PDFunction;
    .locals 5

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/shading/PDShading;->functionArray:[Lorg/apache/pdfbox/pdmodel/common/function/PDFunction;

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/shading/PDShading;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->FUNCTION:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    instance-of v1, v0, Lorg/apache/pdfbox/cos/COSDictionary;

    const/4 v2, 0x0

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    new-array v1, v1, [Lorg/apache/pdfbox/pdmodel/common/function/PDFunction;

    iput-object v1, p0, Lorg/apache/pdfbox/pdmodel/graphics/shading/PDShading;->functionArray:[Lorg/apache/pdfbox/pdmodel/common/function/PDFunction;

    invoke-static {v0}, Lorg/apache/pdfbox/pdmodel/common/function/PDFunction;->create(Lorg/apache/pdfbox/cos/COSBase;)Lorg/apache/pdfbox/pdmodel/common/function/PDFunction;

    move-result-object v0

    aput-object v0, v1, v2

    goto :goto_1

    :cond_0
    check-cast v0, Lorg/apache/pdfbox/cos/COSArray;

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSArray;->size()I

    move-result v1

    new-array v3, v1, [Lorg/apache/pdfbox/pdmodel/common/function/PDFunction;

    iput-object v3, p0, Lorg/apache/pdfbox/pdmodel/graphics/shading/PDShading;->functionArray:[Lorg/apache/pdfbox/pdmodel/common/function/PDFunction;

    :goto_0
    if-ge v2, v1, :cond_1

    iget-object v3, p0, Lorg/apache/pdfbox/pdmodel/graphics/shading/PDShading;->functionArray:[Lorg/apache/pdfbox/pdmodel/common/function/PDFunction;

    invoke-virtual {v0, v2}, Lorg/apache/pdfbox/cos/COSArray;->get(I)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v4

    invoke-static {v4}, Lorg/apache/pdfbox/pdmodel/common/function/PDFunction;->create(Lorg/apache/pdfbox/cos/COSBase;)Lorg/apache/pdfbox/pdmodel/common/function/PDFunction;

    move-result-object v4

    aput-object v4, v3, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    :goto_1
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/shading/PDShading;->functionArray:[Lorg/apache/pdfbox/pdmodel/common/function/PDFunction;

    return-object v0
.end method


# virtual methods
.method public evalFunction(F)[F
    .locals 2

    .line 1
    const/4 v0, 0x1

    new-array v0, v0, [F

    const/4 v1, 0x0

    aput p1, v0, v1

    invoke-virtual {p0, v0}, Lorg/apache/pdfbox/pdmodel/graphics/shading/PDShading;->evalFunction([F)[F

    move-result-object p1

    return-object p1
.end method

.method public evalFunction([F)[F
    .locals 6

    .line 2
    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/graphics/shading/PDShading;->getFunctionsArray()[Lorg/apache/pdfbox/pdmodel/common/function/PDFunction;

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-ne v1, v2, :cond_0

    aget-object v0, v0, v3

    invoke-virtual {v0, p1}, Lorg/apache/pdfbox/pdmodel/common/function/PDFunction;->eval([F)[F

    move-result-object p1

    goto :goto_1

    :cond_0
    new-array v2, v1, [F

    move v4, v3

    :goto_0
    if-ge v4, v1, :cond_1

    aget-object v5, v0, v4

    invoke-virtual {v5, p1}, Lorg/apache/pdfbox/pdmodel/common/function/PDFunction;->eval([F)[F

    move-result-object v5

    aget v5, v5, v3

    aput v5, v2, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_1
    move-object p1, v2

    :goto_1
    array-length v0, p1

    if-ge v3, v0, :cond_4

    aget v0, p1, v3

    const/4 v1, 0x0

    cmpg-float v2, v0, v1

    if-gez v2, :cond_2

    aput v1, p1, v3

    goto :goto_2

    :cond_2
    const/high16 v1, 0x3f800000    # 1.0f

    cmpl-float v0, v0, v1

    if-lez v0, :cond_3

    aput v1, p1, v3

    :cond_3
    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_4
    return-object p1
.end method

.method public getAntiAlias()Z
    .locals 3

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/shading/PDShading;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->ANTI_ALIAS:Lorg/apache/pdfbox/cos/COSName;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->getBoolean(Lorg/apache/pdfbox/cos/COSName;Z)Z

    move-result v0

    return v0
.end method

.method public getBBox()Lorg/apache/pdfbox/pdmodel/common/PDRectangle;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/shading/PDShading;->bBox:Lorg/apache/pdfbox/pdmodel/common/PDRectangle;

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/shading/PDShading;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->BBOX:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSArray;

    if-eqz v0, :cond_0

    new-instance v1, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;

    invoke-direct {v1, v0}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;-><init>(Lorg/apache/pdfbox/cos/COSArray;)V

    iput-object v1, p0, Lorg/apache/pdfbox/pdmodel/graphics/shading/PDShading;->bBox:Lorg/apache/pdfbox/pdmodel/common/PDRectangle;

    :cond_0
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/shading/PDShading;->bBox:Lorg/apache/pdfbox/pdmodel/common/PDRectangle;

    return-object v0
.end method

.method public getBackground()Lorg/apache/pdfbox/cos/COSArray;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/shading/PDShading;->background:Lorg/apache/pdfbox/cos/COSArray;

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/shading/PDShading;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->BACKGROUND:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSArray;

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/shading/PDShading;->background:Lorg/apache/pdfbox/cos/COSArray;

    :cond_0
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/shading/PDShading;->background:Lorg/apache/pdfbox/cos/COSArray;

    return-object v0
.end method

.method public getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/shading/PDShading;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    return-object v0
.end method

.method public getCOSObject()Lorg/apache/pdfbox/cos/COSBase;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/shading/PDShading;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    return-object v0
.end method

.method public getFunction()Lorg/apache/pdfbox/pdmodel/common/function/PDFunction;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/shading/PDShading;->function:Lorg/apache/pdfbox/pdmodel/common/function/PDFunction;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/shading/PDShading;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->FUNCTION:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {v0}, Lorg/apache/pdfbox/pdmodel/common/function/PDFunction;->create(Lorg/apache/pdfbox/cos/COSBase;)Lorg/apache/pdfbox/pdmodel/common/function/PDFunction;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/shading/PDShading;->function:Lorg/apache/pdfbox/pdmodel/common/function/PDFunction;

    :cond_0
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/shading/PDShading;->function:Lorg/apache/pdfbox/pdmodel/common/function/PDFunction;

    return-object v0
.end method

.method public abstract getShadingType()I
.end method

.method public getType()Ljava/lang/String;
    .locals 1

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->SHADING:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSName;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public setAntiAlias(Z)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/shading/PDShading;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->ANTI_ALIAS:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setBoolean(Lorg/apache/pdfbox/cos/COSName;Z)V

    return-void
.end method

.method public setBBox(Lorg/apache/pdfbox/pdmodel/common/PDRectangle;)V
    .locals 2

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/graphics/shading/PDShading;->bBox:Lorg/apache/pdfbox/pdmodel/common/PDRectangle;

    if-nez p1, :cond_0

    iget-object p1, p0, Lorg/apache/pdfbox/pdmodel/graphics/shading/PDShading;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->BBOX:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p1, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->removeItem(Lorg/apache/pdfbox/cos/COSName;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/shading/PDShading;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->BBOX:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->getCOSArray()Lorg/apache/pdfbox/cos/COSArray;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    :goto_0
    return-void
.end method

.method public setBackground(Lorg/apache/pdfbox/cos/COSArray;)V
    .locals 2

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/graphics/shading/PDShading;->background:Lorg/apache/pdfbox/cos/COSArray;

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/shading/PDShading;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->BACKGROUND:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    return-void
.end method

.method public setColorSpace(Lorg/apache/pdfbox/pdmodel/graphics/color/PDColorSpace;)V
    .locals 2

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/graphics/shading/PDShading;->colorSpace:Lorg/apache/pdfbox/pdmodel/graphics/color/PDColorSpace;

    if-eqz p1, :cond_0

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/shading/PDShading;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->COLORSPACE:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/graphics/color/PDColorSpace;->getCOSObject()Lorg/apache/pdfbox/cos/COSBase;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lorg/apache/pdfbox/pdmodel/graphics/shading/PDShading;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->COLORSPACE:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p1, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->removeItem(Lorg/apache/pdfbox/cos/COSName;)V

    :goto_0
    return-void
.end method

.method public setFunction(Lorg/apache/pdfbox/cos/COSArray;)V
    .locals 2

    .line 1
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/shading/PDShading;->functionArray:[Lorg/apache/pdfbox/pdmodel/common/function/PDFunction;

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/shading/PDShading;->function:Lorg/apache/pdfbox/pdmodel/common/function/PDFunction;

    if-nez p1, :cond_0

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/shading/PDShading;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object p1

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->FUNCTION:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p1, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->removeItem(Lorg/apache/pdfbox/cos/COSName;)V

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/shading/PDShading;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->FUNCTION:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    :goto_0
    return-void
.end method

.method public setFunction(Lorg/apache/pdfbox/pdmodel/common/function/PDFunction;)V
    .locals 2

    .line 2
    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/shading/PDShading;->functionArray:[Lorg/apache/pdfbox/pdmodel/common/function/PDFunction;

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/graphics/shading/PDShading;->function:Lorg/apache/pdfbox/pdmodel/common/function/PDFunction;

    if-nez p1, :cond_0

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/shading/PDShading;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object p1

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->FUNCTION:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p1, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->removeItem(Lorg/apache/pdfbox/cos/COSName;)V

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/shading/PDShading;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->FUNCTION:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)V

    :goto_0
    return-void
.end method

.method public setShadingType(I)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/shading/PDShading;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->SHADING_TYPE:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setInt(Lorg/apache/pdfbox/cos/COSName;I)V

    return-void
.end method

.class public Lorg/apache/pdfbox/pdmodel/graphics/predictor/Average;
.super Lorg/apache/pdfbox/pdmodel/graphics/predictor/PredictorAlgorithm;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/graphics/predictor/PredictorAlgorithm;-><init>()V

    return-void
.end method


# virtual methods
.method public decodeLine([B[BIIII)V
    .locals 5

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/predictor/PredictorAlgorithm;->getWidth()I

    move-result p3

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/predictor/PredictorAlgorithm;->getBpp()I

    move-result v0

    mul-int/2addr p3, v0

    const/4 v0, 0x0

    :goto_0
    if-ge v0, p3, :cond_0

    add-int v1, v0, p6

    add-int v2, v0, p4

    aget-byte v2, p1, v2

    invoke-virtual {p0, p2, p6, p5, v0}, Lorg/apache/pdfbox/pdmodel/graphics/predictor/PredictorAlgorithm;->leftPixel([BIII)I

    move-result v3

    invoke-virtual {p0, p2, p6, p5, v0}, Lorg/apache/pdfbox/pdmodel/graphics/predictor/PredictorAlgorithm;->abovePixel([BIII)I

    move-result v4

    add-int/2addr v3, v4

    ushr-int/lit8 v3, v3, 0x2

    add-int/2addr v2, v3

    int-to-byte v2, v2

    aput-byte v2, p2, v1

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public encodeLine([B[BIIII)V
    .locals 5

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/predictor/PredictorAlgorithm;->getWidth()I

    move-result p5

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/predictor/PredictorAlgorithm;->getBpp()I

    move-result v0

    mul-int/2addr p5, v0

    const/4 v0, 0x0

    :goto_0
    if-ge v0, p5, :cond_0

    add-int v1, v0, p6

    add-int v2, v0, p4

    aget-byte v2, p1, v2

    invoke-virtual {p0, p1, p4, p3, v0}, Lorg/apache/pdfbox/pdmodel/graphics/predictor/PredictorAlgorithm;->leftPixel([BIII)I

    move-result v3

    invoke-virtual {p0, p1, p4, p3, v0}, Lorg/apache/pdfbox/pdmodel/graphics/predictor/PredictorAlgorithm;->abovePixel([BIII)I

    move-result v4

    add-int/2addr v3, v4

    ushr-int/lit8 v3, v3, 0x2

    sub-int/2addr v2, v3

    int-to-byte v2, v2

    aput-byte v2, p2, v1

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

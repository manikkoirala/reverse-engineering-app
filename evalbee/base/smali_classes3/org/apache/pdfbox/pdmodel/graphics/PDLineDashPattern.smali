.class public final Lorg/apache/pdfbox/pdmodel/graphics/PDLineDashPattern;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/pdfbox/pdmodel/common/COSObjectable;


# instance fields
.field private final array:[F

.field private final phase:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    new-array v1, v0, [F

    iput-object v1, p0, Lorg/apache/pdfbox/pdmodel/graphics/PDLineDashPattern;->array:[F

    iput v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/PDLineDashPattern;->phase:I

    return-void
.end method

.method public constructor <init>(Lorg/apache/pdfbox/cos/COSArray;I)V
    .locals 0

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Lorg/apache/pdfbox/cos/COSArray;->toFloatArray()[F

    move-result-object p1

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/graphics/PDLineDashPattern;->array:[F

    iput p2, p0, Lorg/apache/pdfbox/pdmodel/graphics/PDLineDashPattern;->phase:I

    return-void
.end method


# virtual methods
.method public getCOSObject()Lorg/apache/pdfbox/cos/COSBase;
    .locals 4

    new-instance v0, Lorg/apache/pdfbox/cos/COSArray;

    invoke-direct {v0}, Lorg/apache/pdfbox/cos/COSArray;-><init>()V

    const/4 v1, 0x1

    new-array v1, v1, [[F

    const/4 v2, 0x0

    iget-object v3, p0, Lorg/apache/pdfbox/pdmodel/graphics/PDLineDashPattern;->array:[F

    aput-object v3, v1, v2

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-static {v1}, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;->converterToCOSArray(Ljava/util/List;)Lorg/apache/pdfbox/cos/COSArray;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    iget v1, p0, Lorg/apache/pdfbox/pdmodel/graphics/PDLineDashPattern;->phase:I

    int-to-long v1, v1

    invoke-static {v1, v2}, Lorg/apache/pdfbox/cos/COSInteger;->get(J)Lorg/apache/pdfbox/cos/COSInteger;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    return-object v0
.end method

.method public getDashArray()[F
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/PDLineDashPattern;->array:[F

    invoke-virtual {v0}, [F->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [F

    return-object v0
.end method

.method public getPhase()I
    .locals 1

    iget v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/PDLineDashPattern;->phase:I

    return v0
.end method

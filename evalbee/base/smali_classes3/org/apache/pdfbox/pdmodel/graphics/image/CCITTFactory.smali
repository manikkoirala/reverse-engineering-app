.class public final Lorg/apache/pdfbox/pdmodel/graphics/image/CCITTFactory;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static createFromFile(Lorg/apache/pdfbox/pdmodel/PDDocument;Ljava/io/File;)Lorg/apache/pdfbox/pdmodel/graphics/image/PDImageXObject;
    .locals 2

    .line 1
    new-instance v0, Lorg/apache/pdfbox/io/RandomAccessFile;

    const-string v1, "r"

    invoke-direct {v0, p1, v1}, Lorg/apache/pdfbox/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V

    const/4 p1, 0x0

    invoke-static {p0, v0, p1}, Lorg/apache/pdfbox/pdmodel/graphics/image/CCITTFactory;->createFromRandomAccessImpl(Lorg/apache/pdfbox/pdmodel/PDDocument;Lorg/apache/pdfbox/io/RandomAccess;I)Lorg/apache/pdfbox/pdmodel/graphics/image/PDImageXObject;

    move-result-object p0

    return-object p0
.end method

.method public static createFromFile(Lorg/apache/pdfbox/pdmodel/PDDocument;Ljava/io/File;I)Lorg/apache/pdfbox/pdmodel/graphics/image/PDImageXObject;
    .locals 2

    .line 2
    new-instance v0, Lorg/apache/pdfbox/io/RandomAccessFile;

    const-string v1, "r"

    invoke-direct {v0, p1, v1}, Lorg/apache/pdfbox/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-static {p0, v0, p2}, Lorg/apache/pdfbox/pdmodel/graphics/image/CCITTFactory;->createFromRandomAccessImpl(Lorg/apache/pdfbox/pdmodel/PDDocument;Lorg/apache/pdfbox/io/RandomAccess;I)Lorg/apache/pdfbox/pdmodel/graphics/image/PDImageXObject;

    move-result-object p0

    return-object p0
.end method

.method public static createFromRandomAccess(Lorg/apache/pdfbox/pdmodel/PDDocument;Lorg/apache/pdfbox/io/RandomAccess;)Lorg/apache/pdfbox/pdmodel/graphics/image/PDImageXObject;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 1
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Lorg/apache/pdfbox/pdmodel/graphics/image/CCITTFactory;->createFromRandomAccessImpl(Lorg/apache/pdfbox/pdmodel/PDDocument;Lorg/apache/pdfbox/io/RandomAccess;I)Lorg/apache/pdfbox/pdmodel/graphics/image/PDImageXObject;

    move-result-object p0

    return-object p0
.end method

.method public static createFromRandomAccess(Lorg/apache/pdfbox/pdmodel/PDDocument;Lorg/apache/pdfbox/io/RandomAccess;I)Lorg/apache/pdfbox/pdmodel/graphics/image/PDImageXObject;
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 2
    invoke-static {p0, p1, p2}, Lorg/apache/pdfbox/pdmodel/graphics/image/CCITTFactory;->createFromRandomAccessImpl(Lorg/apache/pdfbox/pdmodel/PDDocument;Lorg/apache/pdfbox/io/RandomAccess;I)Lorg/apache/pdfbox/pdmodel/graphics/image/PDImageXObject;

    move-result-object p0

    return-object p0
.end method

.method private static createFromRandomAccessImpl(Lorg/apache/pdfbox/pdmodel/PDDocument;Lorg/apache/pdfbox/io/RandomAccess;I)Lorg/apache/pdfbox/pdmodel/graphics/image/PDImageXObject;
    .locals 9

    new-instance v0, Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-direct {v0}, Lorg/apache/pdfbox/cos/COSDictionary;-><init>()V

    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    invoke-static {p1, v1, v0, p2}, Lorg/apache/pdfbox/pdmodel/graphics/image/CCITTFactory;->extractFromTiff(Lorg/apache/pdfbox/io/RandomAccess;Ljava/io/OutputStream;Lorg/apache/pdfbox/cos/COSDictionary;I)V

    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->size()I

    move-result p1

    if-nez p1, :cond_0

    const/4 p0, 0x0

    return-object p0

    :cond_0
    new-instance v3, Ljava/io/ByteArrayInputStream;

    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object p1

    invoke-direct {v3, p1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    new-instance p1, Lorg/apache/pdfbox/pdmodel/graphics/image/PDImageXObject;

    sget-object v4, Lorg/apache/pdfbox/cos/COSName;->CCITTFAX_DECODE:Lorg/apache/pdfbox/cos/COSName;

    sget-object p2, Lorg/apache/pdfbox/cos/COSName;->COLUMNS:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, p2}, Lorg/apache/pdfbox/cos/COSDictionary;->getInt(Lorg/apache/pdfbox/cos/COSName;)I

    move-result v5

    sget-object p2, Lorg/apache/pdfbox/cos/COSName;->ROWS:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, p2}, Lorg/apache/pdfbox/cos/COSDictionary;->getInt(Lorg/apache/pdfbox/cos/COSName;)I

    move-result v6

    const/4 v7, 0x1

    sget-object v8, Lorg/apache/pdfbox/pdmodel/graphics/color/PDDeviceGray;->INSTANCE:Lorg/apache/pdfbox/pdmodel/graphics/color/PDDeviceGray;

    move-object v1, p1

    move-object v2, p0

    invoke-direct/range {v1 .. v8}, Lorg/apache/pdfbox/pdmodel/graphics/image/PDImageXObject;-><init>(Lorg/apache/pdfbox/pdmodel/PDDocument;Ljava/io/InputStream;Lorg/apache/pdfbox/cos/COSBase;IIILorg/apache/pdfbox/pdmodel/graphics/color/PDColorSpace;)V

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/graphics/PDXObject;->getCOSStream()Lorg/apache/pdfbox/cos/COSStream;

    move-result-object p0

    sget-object p2, Lorg/apache/pdfbox/cos/COSName;->DECODE_PARMS:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p0, p2, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    return-object p1
.end method

.method private static extractFromTiff(Lorg/apache/pdfbox/io/RandomAccess;Ljava/io/OutputStream;Lorg/apache/pdfbox/cos/COSDictionary;I)V
    .locals 16

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    const-wide/16 v2, 0x0

    :try_start_0
    invoke-interface {v0, v2, v3}, Lorg/apache/pdfbox/io/RandomAccessRead;->seek(J)V

    invoke-interface/range {p0 .. p0}, Lorg/apache/pdfbox/io/SequentialRead;->read()I

    move-result v2

    int-to-char v2, v2

    invoke-interface/range {p0 .. p0}, Lorg/apache/pdfbox/io/SequentialRead;->read()I

    move-result v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    int-to-char v3, v3

    const-string v4, "Not a valid tiff file"

    if-ne v3, v2, :cond_1c

    const/16 v3, 0x4d

    if-eq v2, v3, :cond_1

    const/16 v5, 0x49

    if-ne v2, v5, :cond_0

    goto :goto_0

    :cond_0
    :try_start_1
    new-instance v0, Ljava/io/IOException;

    invoke-direct {v0, v4}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    :goto_0
    invoke-static {v2, v0}, Lorg/apache/pdfbox/pdmodel/graphics/image/CCITTFactory;->readshort(CLorg/apache/pdfbox/io/RandomAccess;)I

    move-result v5

    const/16 v6, 0x2a

    if-ne v5, v6, :cond_1b

    invoke-static {v2, v0}, Lorg/apache/pdfbox/pdmodel/graphics/image/CCITTFactory;->readlong(CLorg/apache/pdfbox/io/RandomAccess;)I

    move-result v5

    int-to-long v6, v5

    invoke-interface {v0, v6, v7}, Lorg/apache/pdfbox/io/RandomAccessRead;->seek(J)V

    const/4 v7, 0x0

    :goto_1
    const/16 v8, 0x32

    move/from16 v9, p3

    if-ge v7, v9, :cond_4

    invoke-static {v2, v0}, Lorg/apache/pdfbox/pdmodel/graphics/image/CCITTFactory;->readshort(CLorg/apache/pdfbox/io/RandomAccess;)I

    move-result v10

    if-gt v10, v8, :cond_3

    add-int/lit8 v5, v5, 0x2

    mul-int/lit8 v10, v10, 0xc

    add-int/2addr v5, v10

    int-to-long v10, v5

    invoke-interface {v0, v10, v11}, Lorg/apache/pdfbox/io/RandomAccessRead;->seek(J)V

    invoke-static {v2, v0}, Lorg/apache/pdfbox/pdmodel/graphics/image/CCITTFactory;->readlong(CLorg/apache/pdfbox/io/RandomAccess;)I

    move-result v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    if-nez v5, :cond_2

    invoke-virtual/range {p1 .. p1}, Ljava/io/OutputStream;->close()V

    return-void

    :cond_2
    int-to-long v10, v5

    :try_start_2
    invoke-interface {v0, v10, v11}, Lorg/apache/pdfbox/io/RandomAccessRead;->seek(J)V

    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    :cond_3
    new-instance v0, Ljava/io/IOException;

    invoke-direct {v0, v4}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    invoke-static {v2, v0}, Lorg/apache/pdfbox/pdmodel/graphics/image/CCITTFactory;->readshort(CLorg/apache/pdfbox/io/RandomAccess;)I

    move-result v5

    if-gt v5, v8, :cond_1a

    const/16 v4, -0x3e8

    move v9, v4

    const/4 v7, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    :goto_2
    if-ge v7, v5, :cond_16

    invoke-static {v2, v0}, Lorg/apache/pdfbox/pdmodel/graphics/image/CCITTFactory;->readshort(CLorg/apache/pdfbox/io/RandomAccess;)I

    move-result v12

    invoke-static {v2, v0}, Lorg/apache/pdfbox/pdmodel/graphics/image/CCITTFactory;->readshort(CLorg/apache/pdfbox/io/RandomAccess;)I

    move-result v13

    invoke-static {v2, v0}, Lorg/apache/pdfbox/pdmodel/graphics/image/CCITTFactory;->readlong(CLorg/apache/pdfbox/io/RandomAccess;)I

    move-result v14

    invoke-static {v2, v0}, Lorg/apache/pdfbox/pdmodel/graphics/image/CCITTFactory;->readlong(CLorg/apache/pdfbox/io/RandomAccess;)I

    move-result v15

    const/4 v8, 0x3

    const/4 v6, 0x1

    if-ne v2, v3, :cond_7

    if-eq v13, v6, :cond_6

    if-eq v13, v8, :cond_5

    goto :goto_3

    :cond_5
    shr-int/lit8 v15, v15, 0x10

    goto :goto_3

    :cond_6
    shr-int/lit8 v15, v15, 0x18

    :cond_7
    :goto_3
    const/16 v13, 0x100

    if-eq v12, v13, :cond_14

    const/16 v13, 0x101

    if-eq v12, v13, :cond_13

    const/16 v13, 0x103

    if-eq v12, v13, :cond_11

    const/16 v8, 0x106

    if-eq v12, v8, :cond_10

    const/16 v8, 0x111

    if-eq v12, v8, :cond_f

    const/16 v8, 0x117

    if-eq v12, v8, :cond_e

    const/16 v8, 0x124

    if-eq v12, v8, :cond_a

    const/16 v8, 0x144

    if-eq v12, v8, :cond_9

    const/16 v8, 0x145

    if-eq v12, v8, :cond_8

    goto :goto_7

    :cond_8
    if-ne v14, v6, :cond_15

    goto :goto_4

    :cond_9
    if-ne v14, v6, :cond_15

    goto :goto_5

    :cond_a
    and-int/lit8 v6, v15, 0x1

    if-eqz v6, :cond_b

    const/16 v9, 0x32

    :cond_b
    and-int/lit8 v6, v15, 0x4

    if-nez v6, :cond_d

    and-int/lit8 v6, v15, 0x2

    if-nez v6, :cond_c

    goto :goto_7

    :cond_c
    new-instance v0, Ljava/io/IOException;

    const-string v1, "CCITT Group 3 \'fill bits before EOL\' is not supported"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_d
    new-instance v0, Ljava/io/IOException;

    const-string v1, "CCITT Group 3 \'uncompressed mode\' is not supported"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_e
    if-ne v14, v6, :cond_15

    :goto_4
    move v11, v15

    goto :goto_7

    :cond_f
    if-ne v14, v6, :cond_15

    :goto_5
    move v10, v15

    goto :goto_7

    :cond_10
    if-ne v15, v6, :cond_15

    sget-object v8, Lorg/apache/pdfbox/cos/COSName;->BLACK_IS_1:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v1, v8, v6}, Lorg/apache/pdfbox/cos/COSDictionary;->setBoolean(Lorg/apache/pdfbox/cos/COSName;Z)V

    goto :goto_7

    :cond_11
    const/4 v6, 0x4

    if-ne v15, v6, :cond_12

    const/4 v9, -0x1

    :cond_12
    if-ne v15, v8, :cond_15

    const/4 v9, 0x0

    goto :goto_7

    :cond_13
    sget-object v6, Lorg/apache/pdfbox/cos/COSName;->ROWS:Lorg/apache/pdfbox/cos/COSName;

    :goto_6
    invoke-virtual {v1, v6, v15}, Lorg/apache/pdfbox/cos/COSDictionary;->setInt(Lorg/apache/pdfbox/cos/COSName;I)V

    goto :goto_7

    :cond_14
    sget-object v6, Lorg/apache/pdfbox/cos/COSName;->COLUMNS:Lorg/apache/pdfbox/cos/COSName;

    goto :goto_6

    :cond_15
    :goto_7
    add-int/lit8 v7, v7, 0x1

    const/16 v8, 0x32

    goto/16 :goto_2

    :cond_16
    if-eq v9, v4, :cond_19

    if-eqz v10, :cond_18

    sget-object v2, Lorg/apache/pdfbox/cos/COSName;->K:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v1, v2, v9}, Lorg/apache/pdfbox/cos/COSDictionary;->setInt(Lorg/apache/pdfbox/cos/COSName;I)V

    int-to-long v1, v10

    invoke-interface {v0, v1, v2}, Lorg/apache/pdfbox/io/RandomAccessRead;->seek(J)V

    const/16 v1, 0x2000

    new-array v2, v1, [B

    :goto_8
    invoke-static {v1, v11}, Ljava/lang/Math;->min(II)I

    move-result v3

    const/4 v4, 0x0

    invoke-interface {v0, v2, v4, v3}, Lorg/apache/pdfbox/io/SequentialRead;->read([BII)I

    move-result v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    if-lez v3, :cond_17

    sub-int/2addr v11, v3

    move-object/from16 v5, p1

    :try_start_3
    invoke-virtual {v5, v2, v4, v3}, Ljava/io/OutputStream;->write([BII)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_8

    :cond_17
    move-object/from16 v5, p1

    invoke-virtual/range {p1 .. p1}, Ljava/io/OutputStream;->close()V

    return-void

    :cond_18
    move-object/from16 v5, p1

    :try_start_4
    new-instance v0, Ljava/io/IOException;

    const-string v1, "First image in tiff is not a single tile/strip"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_19
    move-object/from16 v5, p1

    new-instance v0, Ljava/io/IOException;

    const-string v1, "First image in tiff is not CCITT T4 or T6 compressed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1a
    move-object/from16 v5, p1

    new-instance v0, Ljava/io/IOException;

    invoke-direct {v0, v4}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1b
    move-object/from16 v5, p1

    new-instance v0, Ljava/io/IOException;

    invoke-direct {v0, v4}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1c
    move-object/from16 v5, p1

    new-instance v0, Ljava/io/IOException;

    invoke-direct {v0, v4}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :catchall_0
    move-exception v0

    goto :goto_9

    :catchall_1
    move-exception v0

    move-object/from16 v5, p1

    :goto_9
    invoke-virtual/range {p1 .. p1}, Ljava/io/OutputStream;->close()V

    throw v0
.end method

.method private static readlong(CLorg/apache/pdfbox/io/RandomAccess;)I
    .locals 1

    const/16 v0, 0x49

    if-ne p0, v0, :cond_0

    invoke-interface {p1}, Lorg/apache/pdfbox/io/SequentialRead;->read()I

    move-result p0

    invoke-interface {p1}, Lorg/apache/pdfbox/io/SequentialRead;->read()I

    move-result v0

    shl-int/lit8 v0, v0, 0x8

    or-int/2addr p0, v0

    invoke-interface {p1}, Lorg/apache/pdfbox/io/SequentialRead;->read()I

    move-result v0

    shl-int/lit8 v0, v0, 0x10

    or-int/2addr p0, v0

    invoke-interface {p1}, Lorg/apache/pdfbox/io/SequentialRead;->read()I

    move-result p1

    shl-int/lit8 p1, p1, 0x18

    :goto_0
    or-int/2addr p0, p1

    return p0

    :cond_0
    invoke-interface {p1}, Lorg/apache/pdfbox/io/SequentialRead;->read()I

    move-result p0

    shl-int/lit8 p0, p0, 0x18

    invoke-interface {p1}, Lorg/apache/pdfbox/io/SequentialRead;->read()I

    move-result v0

    shl-int/lit8 v0, v0, 0x10

    or-int/2addr p0, v0

    invoke-interface {p1}, Lorg/apache/pdfbox/io/SequentialRead;->read()I

    move-result v0

    shl-int/lit8 v0, v0, 0x8

    or-int/2addr p0, v0

    invoke-interface {p1}, Lorg/apache/pdfbox/io/SequentialRead;->read()I

    move-result p1

    goto :goto_0
.end method

.method private static readshort(CLorg/apache/pdfbox/io/RandomAccess;)I
    .locals 1

    const/16 v0, 0x49

    if-ne p0, v0, :cond_0

    invoke-interface {p1}, Lorg/apache/pdfbox/io/SequentialRead;->read()I

    move-result p0

    invoke-interface {p1}, Lorg/apache/pdfbox/io/SequentialRead;->read()I

    move-result p1

    shl-int/lit8 p1, p1, 0x8

    :goto_0
    or-int/2addr p0, p1

    return p0

    :cond_0
    invoke-interface {p1}, Lorg/apache/pdfbox/io/SequentialRead;->read()I

    move-result p0

    shl-int/lit8 p0, p0, 0x8

    invoke-interface {p1}, Lorg/apache/pdfbox/io/SequentialRead;->read()I

    move-result p1

    goto :goto_0
.end method

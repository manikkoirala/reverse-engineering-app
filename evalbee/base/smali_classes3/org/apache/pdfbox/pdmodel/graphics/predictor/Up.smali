.class public Lorg/apache/pdfbox/pdmodel/graphics/predictor/Up;
.super Lorg/apache/pdfbox/pdmodel/graphics/predictor/PredictorAlgorithm;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/graphics/predictor/PredictorAlgorithm;-><init>()V

    return-void
.end method


# virtual methods
.method public decodeLine([B[BIIII)V
    .locals 4

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/predictor/PredictorAlgorithm;->getWidth()I

    move-result p3

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/predictor/PredictorAlgorithm;->getBpp()I

    move-result v0

    mul-int/2addr p3, v0

    sub-int v0, p6, p5

    const/4 v1, 0x0

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/predictor/PredictorAlgorithm;->getHeight()I

    move-result p5

    if-lez p5, :cond_1

    :goto_0
    if-ge v1, p3, :cond_1

    add-int p5, p6, v1

    add-int v0, p4, v1

    aget-byte v0, p1, v0

    aput-byte v0, p2, p5

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    :goto_1
    if-ge v1, p3, :cond_1

    add-int v0, p6, v1

    add-int v2, p4, v1

    aget-byte v2, p1, v2

    sub-int v3, v0, p5

    aget-byte v3, p2, v3

    add-int/2addr v2, v3

    int-to-byte v2, v2

    aput-byte v2, p2, v0

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_1
    return-void
.end method

.method public encodeLine([B[BIIII)V
    .locals 4

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/predictor/PredictorAlgorithm;->getWidth()I

    move-result p5

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/predictor/PredictorAlgorithm;->getBpp()I

    move-result v0

    mul-int/2addr p5, v0

    sub-int v0, p4, p3

    const/4 v1, 0x0

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/predictor/PredictorAlgorithm;->getHeight()I

    move-result p3

    if-lez p3, :cond_1

    :goto_0
    if-ge v1, p5, :cond_1

    add-int p3, p6, v1

    add-int v0, p4, v1

    aget-byte v0, p1, v0

    aput-byte v0, p2, p3

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    :goto_1
    if-ge v1, p5, :cond_1

    add-int v0, p6, v1

    add-int v2, p4, v1

    aget-byte v3, p1, v2

    sub-int/2addr v2, p3

    aget-byte v2, p1, v2

    sub-int/2addr v3, v2

    int-to-byte v2, v3

    aput-byte v2, p2, v0

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_1
    return-void
.end method

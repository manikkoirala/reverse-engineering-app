.class public Lorg/apache/pdfbox/pdmodel/graphics/pattern/PDShadingPattern;
.super Lorg/apache/pdfbox/pdmodel/graphics/pattern/PDAbstractPattern;
.source "SourceFile"


# instance fields
.field private extendedGraphicsState:Lorg/apache/pdfbox/pdmodel/graphics/state/PDExtendedGraphicsState;

.field private shading:Lorg/apache/pdfbox/pdmodel/graphics/shading/PDShading;


# direct methods
.method public constructor <init>()V
    .locals 3

    .line 1
    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/graphics/pattern/PDAbstractPattern;-><init>()V

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/pattern/PDAbstractPattern;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->PATTERN_TYPE:Lorg/apache/pdfbox/cos/COSName;

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->setInt(Lorg/apache/pdfbox/cos/COSName;I)V

    return-void
.end method

.method public constructor <init>(Lorg/apache/pdfbox/cos/COSDictionary;)V
    .locals 0

    .line 2
    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/graphics/pattern/PDAbstractPattern;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    return-void
.end method


# virtual methods
.method public getExtendedGraphicsState()Lorg/apache/pdfbox/pdmodel/graphics/state/PDExtendedGraphicsState;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/pattern/PDShadingPattern;->extendedGraphicsState:Lorg/apache/pdfbox/pdmodel/graphics/state/PDExtendedGraphicsState;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/pattern/PDAbstractPattern;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->EXT_G_STATE:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSDictionary;

    if-eqz v0, :cond_0

    new-instance v1, Lorg/apache/pdfbox/pdmodel/graphics/state/PDExtendedGraphicsState;

    invoke-direct {v1, v0}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDExtendedGraphicsState;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    iput-object v1, p0, Lorg/apache/pdfbox/pdmodel/graphics/pattern/PDShadingPattern;->extendedGraphicsState:Lorg/apache/pdfbox/pdmodel/graphics/state/PDExtendedGraphicsState;

    :cond_0
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/pattern/PDShadingPattern;->extendedGraphicsState:Lorg/apache/pdfbox/pdmodel/graphics/state/PDExtendedGraphicsState;

    return-object v0
.end method

.method public getPatternType()I
    .locals 1

    const/4 v0, 0x2

    return v0
.end method

.method public getShading()Lorg/apache/pdfbox/pdmodel/graphics/shading/PDShading;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/pattern/PDShadingPattern;->shading:Lorg/apache/pdfbox/pdmodel/graphics/shading/PDShading;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/pattern/PDAbstractPattern;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->SHADING:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSDictionary;

    if-eqz v0, :cond_0

    invoke-static {v0}, Lorg/apache/pdfbox/pdmodel/graphics/shading/PDShading;->create(Lorg/apache/pdfbox/cos/COSDictionary;)Lorg/apache/pdfbox/pdmodel/graphics/shading/PDShading;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/pattern/PDShadingPattern;->shading:Lorg/apache/pdfbox/pdmodel/graphics/shading/PDShading;

    :cond_0
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/pattern/PDShadingPattern;->shading:Lorg/apache/pdfbox/pdmodel/graphics/shading/PDShading;

    return-object v0
.end method

.method public setExternalGraphicsState(Lorg/apache/pdfbox/pdmodel/graphics/state/PDExtendedGraphicsState;)V
    .locals 2

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/graphics/pattern/PDShadingPattern;->extendedGraphicsState:Lorg/apache/pdfbox/pdmodel/graphics/state/PDExtendedGraphicsState;

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/pattern/PDAbstractPattern;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->EXT_G_STATE:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)V

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/pattern/PDAbstractPattern;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object p1

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->EXT_G_STATE:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p1, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->removeItem(Lorg/apache/pdfbox/cos/COSName;)V

    :goto_0
    return-void
.end method

.method public setShading(Lorg/apache/pdfbox/pdmodel/graphics/shading/PDShading;)V
    .locals 2

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/graphics/pattern/PDShadingPattern;->shading:Lorg/apache/pdfbox/pdmodel/graphics/shading/PDShading;

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/pattern/PDAbstractPattern;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->SHADING:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)V

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/pattern/PDAbstractPattern;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object p1

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->SHADING:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p1, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->removeItem(Lorg/apache/pdfbox/cos/COSName;)V

    :goto_0
    return-void
.end method

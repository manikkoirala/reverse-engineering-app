.class final Lorg/apache/pdfbox/pdmodel/graphics/image/SampledImageReader;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static getDecodeArray(Lorg/apache/pdfbox/pdmodel/graphics/image/PDImage;)[F
    .locals 10

    invoke-interface {p0}, Lorg/apache/pdfbox/pdmodel/graphics/image/PDImage;->getDecode()Lorg/apache/pdfbox/cos/COSArray;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSArray;->size()I

    move-result v1

    const/16 v2, 0x10

    if-eq v1, v2, :cond_1

    invoke-interface {p0}, Lorg/apache/pdfbox/pdmodel/graphics/image/PDImage;->isStencil()Z

    move-result p0

    const-string v1, "decode array "

    const-string v2, "PdfBoxAndroid"

    if-eqz p0, :cond_0

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSArray;->size()I

    move-result p0

    const/4 v3, 0x2

    if-lt p0, v3, :cond_0

    const/4 p0, 0x0

    invoke-virtual {v0, p0}, Lorg/apache/pdfbox/cos/COSArray;->get(I)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v4

    instance-of v4, v4, Lorg/apache/pdfbox/cos/COSNumber;

    if-eqz v4, :cond_0

    const/4 v4, 0x1

    invoke-virtual {v0, v4}, Lorg/apache/pdfbox/cos/COSArray;->get(I)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v5

    instance-of v5, v5, Lorg/apache/pdfbox/cos/COSNumber;

    if-eqz v5, :cond_0

    invoke-virtual {v0, p0}, Lorg/apache/pdfbox/cos/COSArray;->get(I)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v5

    check-cast v5, Lorg/apache/pdfbox/cos/COSNumber;

    invoke-virtual {v5}, Lorg/apache/pdfbox/cos/COSNumber;->floatValue()F

    move-result v5

    invoke-virtual {v0, v4}, Lorg/apache/pdfbox/cos/COSArray;->get(I)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v6

    check-cast v6, Lorg/apache/pdfbox/cos/COSNumber;

    invoke-virtual {v6}, Lorg/apache/pdfbox/cos/COSNumber;->floatValue()F

    move-result v6

    const/4 v7, 0x0

    cmpl-float v8, v5, v7

    if-ltz v8, :cond_0

    const/high16 v8, 0x3f800000    # 1.0f

    cmpg-float v9, v5, v8

    if-gtz v9, :cond_0

    cmpl-float v7, v6, v7

    if-ltz v7, :cond_0

    cmpg-float v7, v6, v8

    if-gtz v7, :cond_0

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v0, " not compatible with color space, using the first two entries"

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    new-array v0, v3, [F

    aput v5, v0, p0

    aput v6, v0, v4

    return-object v0

    :cond_0
    new-instance p0, Ljava/lang/StringBuilder;

    invoke-direct {p0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v0, " not compatible with color space, using default"

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-static {v2, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSArray;->toFloatArray()[F

    move-result-object p0

    goto :goto_1

    :cond_2
    :goto_0
    const/4 p0, 0x0

    :goto_1
    if-nez p0, :cond_3

    const/4 p0, 0x6

    new-array p0, p0, [F

    fill-array-data p0, :array_0

    :cond_3
    return-object p0

    nop

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
        0x0
        0x3f800000    # 1.0f
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public static getRGBImage(Lorg/apache/pdfbox/pdmodel/graphics/image/PDImage;Lorg/apache/pdfbox/cos/COSArray;)Landroid/graphics/Bitmap;
    .locals 5

    const-string p1, "PdfBoxAndroid"

    const-string v0, "getting image"

    invoke-static {p1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-interface {p0}, Lorg/apache/pdfbox/pdmodel/graphics/image/PDImage;->getStream()Lorg/apache/pdfbox/pdmodel/common/PDStream;

    move-result-object p1

    instance-of p1, p1, Lorg/apache/pdfbox/pdmodel/common/PDMemoryStream;

    const-string v0, "Image stream is empty"

    if-eqz p1, :cond_1

    invoke-interface {p0}, Lorg/apache/pdfbox/pdmodel/graphics/image/PDImage;->getStream()Lorg/apache/pdfbox/pdmodel/common/PDStream;

    move-result-object p1

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/common/PDStream;->getLength()I

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    new-instance p0, Ljava/io/IOException;

    invoke-direct {p0, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p0

    :cond_1
    invoke-interface {p0}, Lorg/apache/pdfbox/pdmodel/graphics/image/PDImage;->getStream()Lorg/apache/pdfbox/pdmodel/common/PDStream;

    move-result-object p1

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/common/PDStream;->getStream()Lorg/apache/pdfbox/cos/COSStream;

    move-result-object p1

    invoke-virtual {p1}, Lorg/apache/pdfbox/cos/COSStream;->getFilteredLength()J

    move-result-wide v1

    const-wide/16 v3, 0x0

    cmp-long p1, v1, v3

    if-eqz p1, :cond_2

    :goto_0
    invoke-interface {p0}, Lorg/apache/pdfbox/pdmodel/graphics/image/PDImage;->getStream()Lorg/apache/pdfbox/pdmodel/common/PDStream;

    move-result-object p0

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/common/PDStream;->createInputStream()Ljava/io/InputStream;

    move-result-object p0

    invoke-static {p0}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object p0

    return-object p0

    :cond_2
    new-instance p0, Ljava/io/IOException;

    invoke-direct {p0, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

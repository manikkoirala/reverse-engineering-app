.class public Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Cloneable;


# instance fields
.field private alphaConstant:D

.field private alphaSource:Z

.field private blendMode:Lorg/apache/pdfbox/pdmodel/graphics/blend/BlendMode;

.field private clippingPath:Landroid/graphics/Region;

.field private currentTransformationMatrix:Lorg/apache/pdfbox/util/Matrix;

.field private flatness:D

.field private isClippingPathDirty:Z

.field private lineCap:Landroid/graphics/Paint$Cap;

.field private lineDashPattern:Lorg/apache/pdfbox/pdmodel/graphics/PDLineDashPattern;

.field private lineJoin:Landroid/graphics/Paint$Join;

.field private lineWidth:F

.field private miterLimit:F

.field private nonStrokingAlphaConstant:D

.field private nonStrokingColor:Lorg/apache/pdfbox/pdmodel/graphics/color/PDColor;

.field private nonStrokingColorSpace:Lorg/apache/pdfbox/pdmodel/graphics/color/PDColorSpace;

.field private overprint:Z

.field private overprintMode:D

.field private renderingIntent:Lorg/apache/pdfbox/pdmodel/graphics/state/RenderingIntent;

.field private smoothness:D

.field private softMask:Lorg/apache/pdfbox/pdmodel/graphics/state/PDSoftMask;

.field private strokeAdjustment:Z

.field private strokingColor:Lorg/apache/pdfbox/pdmodel/graphics/color/PDColor;

.field private strokingColorSpace:Lorg/apache/pdfbox/pdmodel/graphics/color/PDColorSpace;

.field private textState:Lorg/apache/pdfbox/pdmodel/graphics/state/PDTextState;


# direct methods
.method public constructor <init>(Lorg/apache/pdfbox/pdmodel/common/PDRectangle;)V
    .locals 5

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lorg/apache/pdfbox/util/Matrix;

    invoke-direct {v0}, Lorg/apache/pdfbox/util/Matrix;-><init>()V

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->currentTransformationMatrix:Lorg/apache/pdfbox/util/Matrix;

    sget-object v0, Lorg/apache/pdfbox/pdmodel/graphics/color/PDDeviceGray;->INSTANCE:Lorg/apache/pdfbox/pdmodel/graphics/color/PDDeviceGray;

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/graphics/color/PDDeviceGray;->getInitialColor()Lorg/apache/pdfbox/pdmodel/graphics/color/PDColor;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->strokingColor:Lorg/apache/pdfbox/pdmodel/graphics/color/PDColor;

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/graphics/color/PDDeviceGray;->getInitialColor()Lorg/apache/pdfbox/pdmodel/graphics/color/PDColor;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->nonStrokingColor:Lorg/apache/pdfbox/pdmodel/graphics/color/PDColor;

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->strokingColorSpace:Lorg/apache/pdfbox/pdmodel/graphics/color/PDColorSpace;

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->nonStrokingColorSpace:Lorg/apache/pdfbox/pdmodel/graphics/color/PDColorSpace;

    new-instance v0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDTextState;

    invoke-direct {v0}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDTextState;-><init>()V

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->textState:Lorg/apache/pdfbox/pdmodel/graphics/state/PDTextState;

    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->lineWidth:F

    const/high16 v0, 0x41200000    # 10.0f

    iput v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->miterLimit:F

    new-instance v0, Lorg/apache/pdfbox/pdmodel/graphics/PDLineDashPattern;

    invoke-direct {v0}, Lorg/apache/pdfbox/pdmodel/graphics/PDLineDashPattern;-><init>()V

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->lineDashPattern:Lorg/apache/pdfbox/pdmodel/graphics/PDLineDashPattern;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->strokeAdjustment:Z

    sget-object v1, Lorg/apache/pdfbox/pdmodel/graphics/blend/BlendMode;->COMPATIBLE:Lorg/apache/pdfbox/pdmodel/graphics/blend/SeparableBlendMode;

    iput-object v1, p0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->blendMode:Lorg/apache/pdfbox/pdmodel/graphics/blend/BlendMode;

    const-wide/high16 v1, 0x3ff0000000000000L    # 1.0

    iput-wide v1, p0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->alphaConstant:D

    iput-wide v1, p0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->nonStrokingAlphaConstant:D

    iput-boolean v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->alphaSource:Z

    iput-boolean v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->overprint:Z

    const-wide/16 v3, 0x0

    iput-wide v3, p0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->overprintMode:D

    iput-wide v1, p0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->flatness:D

    iput-wide v3, p0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->smoothness:D

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->toGeneralPath()Landroid/graphics/Path;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Landroid/graphics/Path;->computeBounds(Landroid/graphics/RectF;Z)V

    new-instance v1, Landroid/graphics/Region;

    invoke-direct {v1}, Landroid/graphics/Region;-><init>()V

    iput-object v1, p0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->clippingPath:Landroid/graphics/Region;

    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    invoke-virtual {v0, v1}, Landroid/graphics/RectF;->round(Landroid/graphics/Rect;)V

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->clippingPath:Landroid/graphics/Region;

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->toGeneralPath()Landroid/graphics/Path;

    move-result-object p1

    new-instance v2, Landroid/graphics/Region;

    invoke-direct {v2, v1}, Landroid/graphics/Region;-><init>(Landroid/graphics/Rect;)V

    invoke-virtual {v0, p1, v2}, Landroid/graphics/Region;->setPath(Landroid/graphics/Path;Landroid/graphics/Region;)Z

    return-void
.end method


# virtual methods
.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->clone()Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;
    .locals 2

    .line 2
    :try_start_0
    invoke-super {p0}, Ljava/lang/Object;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;

    iget-object v1, p0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->textState:Lorg/apache/pdfbox/pdmodel/graphics/state/PDTextState;

    invoke-virtual {v1}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDTextState;->clone()Lorg/apache/pdfbox/pdmodel/graphics/state/PDTextState;

    move-result-object v1

    iput-object v1, v0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->textState:Lorg/apache/pdfbox/pdmodel/graphics/state/PDTextState;

    iget-object v1, p0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->currentTransformationMatrix:Lorg/apache/pdfbox/util/Matrix;

    invoke-virtual {v1}, Lorg/apache/pdfbox/util/Matrix;->clone()Lorg/apache/pdfbox/util/Matrix;

    move-result-object v1

    iput-object v1, v0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->currentTransformationMatrix:Lorg/apache/pdfbox/util/Matrix;

    iget-object v1, p0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->strokingColor:Lorg/apache/pdfbox/pdmodel/graphics/color/PDColor;

    iput-object v1, v0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->strokingColor:Lorg/apache/pdfbox/pdmodel/graphics/color/PDColor;

    iget-object v1, p0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->nonStrokingColor:Lorg/apache/pdfbox/pdmodel/graphics/color/PDColor;

    iput-object v1, v0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->nonStrokingColor:Lorg/apache/pdfbox/pdmodel/graphics/color/PDColor;

    iget-object v1, p0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->lineDashPattern:Lorg/apache/pdfbox/pdmodel/graphics/PDLineDashPattern;

    iput-object v1, v0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->lineDashPattern:Lorg/apache/pdfbox/pdmodel/graphics/PDLineDashPattern;

    iget-object v1, p0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->clippingPath:Landroid/graphics/Region;

    iput-object v1, v0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->clippingPath:Landroid/graphics/Region;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->isClippingPathDirty:Z
    :try_end_0
    .catch Ljava/lang/CloneNotSupportedException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public getAlphaConstant()D
    .locals 2

    iget-wide v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->alphaConstant:D

    return-wide v0
.end method

.method public getBlendMode()Lorg/apache/pdfbox/pdmodel/graphics/blend/BlendMode;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->blendMode:Lorg/apache/pdfbox/pdmodel/graphics/blend/BlendMode;

    return-object v0
.end method

.method public getCurrentClippingPath()Landroid/graphics/Region;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->clippingPath:Landroid/graphics/Region;

    return-object v0
.end method

.method public getCurrentTransformationMatrix()Lorg/apache/pdfbox/util/Matrix;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->currentTransformationMatrix:Lorg/apache/pdfbox/util/Matrix;

    return-object v0
.end method

.method public getFlatness()D
    .locals 2

    iget-wide v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->flatness:D

    return-wide v0
.end method

.method public getLineCap()Landroid/graphics/Paint$Cap;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->lineCap:Landroid/graphics/Paint$Cap;

    return-object v0
.end method

.method public getLineDashPattern()Lorg/apache/pdfbox/pdmodel/graphics/PDLineDashPattern;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->lineDashPattern:Lorg/apache/pdfbox/pdmodel/graphics/PDLineDashPattern;

    return-object v0
.end method

.method public getLineJoin()Landroid/graphics/Paint$Join;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->lineJoin:Landroid/graphics/Paint$Join;

    return-object v0
.end method

.method public getLineWidth()F
    .locals 1

    iget v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->lineWidth:F

    return v0
.end method

.method public getMiterLimit()F
    .locals 1

    iget v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->miterLimit:F

    return v0
.end method

.method public getNonStrokeAlphaConstant()D
    .locals 2

    iget-wide v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->nonStrokingAlphaConstant:D

    return-wide v0
.end method

.method public getNonStrokingColor()Lorg/apache/pdfbox/pdmodel/graphics/color/PDColor;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->nonStrokingColor:Lorg/apache/pdfbox/pdmodel/graphics/color/PDColor;

    return-object v0
.end method

.method public getNonStrokingColorSpace()Lorg/apache/pdfbox/pdmodel/graphics/color/PDColorSpace;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->nonStrokingColorSpace:Lorg/apache/pdfbox/pdmodel/graphics/color/PDColorSpace;

    return-object v0
.end method

.method public getOverprintMode()D
    .locals 2

    iget-wide v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->overprintMode:D

    return-wide v0
.end method

.method public getRenderingIntent()Lorg/apache/pdfbox/pdmodel/graphics/state/RenderingIntent;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->renderingIntent:Lorg/apache/pdfbox/pdmodel/graphics/state/RenderingIntent;

    return-object v0
.end method

.method public getSmoothness()D
    .locals 2

    iget-wide v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->smoothness:D

    return-wide v0
.end method

.method public getSoftMask()Lorg/apache/pdfbox/pdmodel/graphics/state/PDSoftMask;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->softMask:Lorg/apache/pdfbox/pdmodel/graphics/state/PDSoftMask;

    return-object v0
.end method

.method public getStrokingColor()Lorg/apache/pdfbox/pdmodel/graphics/color/PDColor;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->strokingColor:Lorg/apache/pdfbox/pdmodel/graphics/color/PDColor;

    return-object v0
.end method

.method public getStrokingColorSpace()Lorg/apache/pdfbox/pdmodel/graphics/color/PDColorSpace;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->strokingColorSpace:Lorg/apache/pdfbox/pdmodel/graphics/color/PDColorSpace;

    return-object v0
.end method

.method public getTextState()Lorg/apache/pdfbox/pdmodel/graphics/state/PDTextState;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->textState:Lorg/apache/pdfbox/pdmodel/graphics/state/PDTextState;

    return-object v0
.end method

.method public intersectClippingPath(Landroid/graphics/Path;)V
    .locals 3

    .line 1
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Path;->computeBounds(Landroid/graphics/RectF;Z)V

    new-instance v1, Landroid/graphics/Region;

    invoke-direct {v1}, Landroid/graphics/Region;-><init>()V

    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    invoke-virtual {v0, v2}, Landroid/graphics/RectF;->round(Landroid/graphics/Rect;)V

    new-instance v0, Landroid/graphics/Region;

    invoke-direct {v0, v2}, Landroid/graphics/Region;-><init>(Landroid/graphics/Rect;)V

    invoke-virtual {v1, p1, v0}, Landroid/graphics/Region;->setPath(Landroid/graphics/Path;Landroid/graphics/Region;)Z

    invoke-virtual {p0, v1}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->intersectClippingPath(Landroid/graphics/Region;)V

    return-void
.end method

.method public intersectClippingPath(Landroid/graphics/Region;)V
    .locals 2

    .line 2
    iget-boolean v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->isClippingPathDirty:Z

    if-nez v0, :cond_0

    new-instance v0, Landroid/graphics/Region;

    invoke-direct {v0, p1}, Landroid/graphics/Region;-><init>(Landroid/graphics/Region;)V

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->clippingPath:Landroid/graphics/Region;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->isClippingPathDirty:Z

    :cond_0
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->clippingPath:Landroid/graphics/Region;

    sget-object v1, Landroid/graphics/Region$Op;->INTERSECT:Landroid/graphics/Region$Op;

    invoke-virtual {v0, p1, v1}, Landroid/graphics/Region;->op(Landroid/graphics/Region;Landroid/graphics/Region$Op;)Z

    return-void
.end method

.method public isAlphaSource()Z
    .locals 1

    iget-boolean v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->alphaSource:Z

    return v0
.end method

.method public isOverprint()Z
    .locals 1

    iget-boolean v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->overprint:Z

    return v0
.end method

.method public isStrokeAdjustment()Z
    .locals 1

    iget-boolean v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->strokeAdjustment:Z

    return v0
.end method

.method public setAlphaConstant(D)V
    .locals 0

    iput-wide p1, p0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->alphaConstant:D

    return-void
.end method

.method public setAlphaSource(Z)V
    .locals 0

    iput-boolean p1, p0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->alphaSource:Z

    return-void
.end method

.method public setBlendMode(Lorg/apache/pdfbox/pdmodel/graphics/blend/BlendMode;)V
    .locals 0

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->blendMode:Lorg/apache/pdfbox/pdmodel/graphics/blend/BlendMode;

    return-void
.end method

.method public setCurrentTransformationMatrix(Lorg/apache/pdfbox/util/Matrix;)V
    .locals 0

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->currentTransformationMatrix:Lorg/apache/pdfbox/util/Matrix;

    return-void
.end method

.method public setFlatness(D)V
    .locals 0

    iput-wide p1, p0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->flatness:D

    return-void
.end method

.method public setLineCap(Landroid/graphics/Paint$Cap;)V
    .locals 0

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->lineCap:Landroid/graphics/Paint$Cap;

    return-void
.end method

.method public setLineDashPattern(Lorg/apache/pdfbox/pdmodel/graphics/PDLineDashPattern;)V
    .locals 0

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->lineDashPattern:Lorg/apache/pdfbox/pdmodel/graphics/PDLineDashPattern;

    return-void
.end method

.method public setLineJoin(Landroid/graphics/Paint$Join;)V
    .locals 0

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->lineJoin:Landroid/graphics/Paint$Join;

    return-void
.end method

.method public setLineWidth(F)V
    .locals 0

    iput p1, p0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->lineWidth:F

    return-void
.end method

.method public setMiterLimit(F)V
    .locals 0

    iput p1, p0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->miterLimit:F

    return-void
.end method

.method public setNonStrokeAlphaConstant(D)V
    .locals 0

    iput-wide p1, p0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->nonStrokingAlphaConstant:D

    return-void
.end method

.method public setNonStrokingColor(Lorg/apache/pdfbox/pdmodel/graphics/color/PDColor;)V
    .locals 0

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->nonStrokingColor:Lorg/apache/pdfbox/pdmodel/graphics/color/PDColor;

    return-void
.end method

.method public setNonStrokingColorSpace(Lorg/apache/pdfbox/pdmodel/graphics/color/PDColorSpace;)V
    .locals 0

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->nonStrokingColorSpace:Lorg/apache/pdfbox/pdmodel/graphics/color/PDColorSpace;

    return-void
.end method

.method public setOverprint(Z)V
    .locals 0

    iput-boolean p1, p0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->overprint:Z

    return-void
.end method

.method public setOverprintMode(D)V
    .locals 0

    iput-wide p1, p0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->overprintMode:D

    return-void
.end method

.method public setRenderingIntent(Lorg/apache/pdfbox/pdmodel/graphics/state/RenderingIntent;)V
    .locals 0

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->renderingIntent:Lorg/apache/pdfbox/pdmodel/graphics/state/RenderingIntent;

    return-void
.end method

.method public setSmoothness(D)V
    .locals 0

    iput-wide p1, p0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->smoothness:D

    return-void
.end method

.method public setSoftMask(Lorg/apache/pdfbox/pdmodel/graphics/state/PDSoftMask;)V
    .locals 0

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->softMask:Lorg/apache/pdfbox/pdmodel/graphics/state/PDSoftMask;

    return-void
.end method

.method public setStrokeAdjustment(Z)V
    .locals 0

    iput-boolean p1, p0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->strokeAdjustment:Z

    return-void
.end method

.method public setStrokingColor(Lorg/apache/pdfbox/pdmodel/graphics/color/PDColor;)V
    .locals 0

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->strokingColor:Lorg/apache/pdfbox/pdmodel/graphics/color/PDColor;

    return-void
.end method

.method public setStrokingColorSpace(Lorg/apache/pdfbox/pdmodel/graphics/color/PDColorSpace;)V
    .locals 0

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->strokingColorSpace:Lorg/apache/pdfbox/pdmodel/graphics/color/PDColorSpace;

    return-void
.end method

.method public setTextState(Lorg/apache/pdfbox/pdmodel/graphics/state/PDTextState;)V
    .locals 0

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->textState:Lorg/apache/pdfbox/pdmodel/graphics/state/PDTextState;

    return-void
.end method

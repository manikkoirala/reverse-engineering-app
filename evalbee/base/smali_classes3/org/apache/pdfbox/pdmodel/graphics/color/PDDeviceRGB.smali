.class public final Lorg/apache/pdfbox/pdmodel/graphics/color/PDDeviceRGB;
.super Lorg/apache/pdfbox/pdmodel/graphics/color/PDDeviceColorSpace;
.source "SourceFile"


# static fields
.field public static final INSTANCE:Lorg/apache/pdfbox/pdmodel/graphics/color/PDDeviceRGB;


# instance fields
.field private final initialColor:Lorg/apache/pdfbox/pdmodel/graphics/color/PDColor;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lorg/apache/pdfbox/pdmodel/graphics/color/PDDeviceRGB;

    invoke-direct {v0}, Lorg/apache/pdfbox/pdmodel/graphics/color/PDDeviceRGB;-><init>()V

    sput-object v0, Lorg/apache/pdfbox/pdmodel/graphics/color/PDDeviceRGB;->INSTANCE:Lorg/apache/pdfbox/pdmodel/graphics/color/PDDeviceRGB;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/graphics/color/PDDeviceColorSpace;-><init>()V

    new-instance v0, Lorg/apache/pdfbox/pdmodel/graphics/color/PDColor;

    const/4 v1, 0x3

    new-array v1, v1, [F

    fill-array-data v1, :array_0

    invoke-direct {v0, v1, p0}, Lorg/apache/pdfbox/pdmodel/graphics/color/PDColor;-><init>([FLorg/apache/pdfbox/pdmodel/graphics/color/PDColorSpace;)V

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/color/PDDeviceRGB;->initialColor:Lorg/apache/pdfbox/pdmodel/graphics/color/PDColor;

    return-void

    nop

    :array_0
    .array-data 4
        0x0
        0x0
        0x0
    .end array-data
.end method


# virtual methods
.method public getDefaultDecode(I)[F
    .locals 0

    const/4 p1, 0x6

    new-array p1, p1, [F

    fill-array-data p1, :array_0

    return-object p1

    nop

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
        0x0
        0x3f800000    # 1.0f
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public getInitialColor()Lorg/apache/pdfbox/pdmodel/graphics/color/PDColor;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/graphics/color/PDDeviceRGB;->initialColor:Lorg/apache/pdfbox/pdmodel/graphics/color/PDColor;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->DEVICERGB:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSName;->getName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getNumberOfComponents()I
    .locals 1

    const/4 v0, 0x3

    return v0
.end method

.method public toRGB([F)[F
    .locals 2

    array-length v0, p1

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    return-object p1

    :cond_0
    iget-object p1, p0, Lorg/apache/pdfbox/pdmodel/graphics/color/PDDeviceRGB;->initialColor:Lorg/apache/pdfbox/pdmodel/graphics/color/PDColor;

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/graphics/color/PDColor;->getComponents()[F

    move-result-object p1

    return-object p1
.end method

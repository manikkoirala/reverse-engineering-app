.class public Lorg/apache/pdfbox/pdmodel/graphics/predictor/None;
.super Lorg/apache/pdfbox/pdmodel/graphics/predictor/PredictorAlgorithm;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/graphics/predictor/PredictorAlgorithm;-><init>()V

    return-void
.end method


# virtual methods
.method public decode([B[B)V
    .locals 2

    array-length v0, p1

    const/4 v1, 0x0

    invoke-static {p1, v1, p2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-void
.end method

.method public decodeLine([B[BIIII)V
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/predictor/PredictorAlgorithm;->getWidth()I

    move-result p3

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/predictor/PredictorAlgorithm;->getBpp()I

    move-result p5

    mul-int/2addr p3, p5

    const/4 p5, 0x0

    :goto_0
    if-ge p5, p3, :cond_0

    add-int v0, p6, p5

    add-int v1, p4, p5

    aget-byte v1, p1, v1

    aput-byte v1, p2, v0

    add-int/lit8 p5, p5, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public encode([B[B)V
    .locals 2

    invoke-virtual {p0, p2, p1}, Lorg/apache/pdfbox/pdmodel/graphics/predictor/PredictorAlgorithm;->checkBufsiz([B[B)V

    array-length v0, p1

    const/4 v1, 0x0

    invoke-static {p1, v1, p2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-void
.end method

.method public encodeLine([B[BIIII)V
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/predictor/PredictorAlgorithm;->getWidth()I

    move-result p3

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/predictor/PredictorAlgorithm;->getBpp()I

    move-result p5

    mul-int/2addr p3, p5

    const/4 p5, 0x0

    :goto_0
    if-ge p5, p3, :cond_0

    add-int v0, p6, p5

    add-int v1, p4, p5

    aget-byte v1, p1, v1

    aput-byte v1, p2, v0

    add-int/lit8 p5, p5, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

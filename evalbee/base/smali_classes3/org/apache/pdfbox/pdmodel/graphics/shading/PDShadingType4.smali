.class public Lorg/apache/pdfbox/pdmodel/graphics/shading/PDShadingType4;
.super Lorg/apache/pdfbox/pdmodel/graphics/shading/PDTriangleBasedShadingType;
.source "SourceFile"


# direct methods
.method public constructor <init>(Lorg/apache/pdfbox/cos/COSDictionary;)V
    .locals 0

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/graphics/shading/PDTriangleBasedShadingType;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    return-void
.end method


# virtual methods
.method public bridge synthetic getBitsPerComponent()I
    .locals 1

    invoke-super {p0}, Lorg/apache/pdfbox/pdmodel/graphics/shading/PDTriangleBasedShadingType;->getBitsPerComponent()I

    move-result v0

    return v0
.end method

.method public bridge synthetic getBitsPerCoordinate()I
    .locals 1

    invoke-super {p0}, Lorg/apache/pdfbox/pdmodel/graphics/shading/PDTriangleBasedShadingType;->getBitsPerCoordinate()I

    move-result v0

    return v0
.end method

.method public getBitsPerFlag()I
    .locals 3

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/shading/PDShading;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->BITS_PER_FLAG:Lorg/apache/pdfbox/cos/COSName;

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->getInt(Lorg/apache/pdfbox/cos/COSName;I)I

    move-result v0

    return v0
.end method

.method public bridge synthetic getDecodeForParameter(I)Lorg/apache/pdfbox/pdmodel/common/PDRange;
    .locals 0

    invoke-super {p0, p1}, Lorg/apache/pdfbox/pdmodel/graphics/shading/PDTriangleBasedShadingType;->getDecodeForParameter(I)Lorg/apache/pdfbox/pdmodel/common/PDRange;

    move-result-object p1

    return-object p1
.end method

.method public getShadingType()I
    .locals 1

    const/4 v0, 0x4

    return v0
.end method

.method public bridge synthetic setBitsPerComponent(I)V
    .locals 0

    invoke-super {p0, p1}, Lorg/apache/pdfbox/pdmodel/graphics/shading/PDTriangleBasedShadingType;->setBitsPerComponent(I)V

    return-void
.end method

.method public bridge synthetic setBitsPerCoordinate(I)V
    .locals 0

    invoke-super {p0, p1}, Lorg/apache/pdfbox/pdmodel/graphics/shading/PDTriangleBasedShadingType;->setBitsPerCoordinate(I)V

    return-void
.end method

.method public setBitsPerFlag(I)V
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/graphics/shading/PDShading;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->BITS_PER_FLAG:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setInt(Lorg/apache/pdfbox/cos/COSName;I)V

    return-void
.end method

.method public bridge synthetic setDecodeValues(Lorg/apache/pdfbox/cos/COSArray;)V
    .locals 0

    invoke-super {p0, p1}, Lorg/apache/pdfbox/pdmodel/graphics/shading/PDTriangleBasedShadingType;->setDecodeValues(Lorg/apache/pdfbox/cos/COSArray;)V

    return-void
.end method

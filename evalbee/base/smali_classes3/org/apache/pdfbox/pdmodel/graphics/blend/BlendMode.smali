.class public abstract Lorg/apache/pdfbox/pdmodel/graphics/blend/BlendMode;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final BLEND_MODES:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lorg/apache/pdfbox/cos/COSName;",
            "Lorg/apache/pdfbox/pdmodel/graphics/blend/BlendMode;",
            ">;"
        }
    .end annotation
.end field

.field public static final COLOR_BURN:Lorg/apache/pdfbox/pdmodel/graphics/blend/SeparableBlendMode;

.field public static final COLOR_DODGE:Lorg/apache/pdfbox/pdmodel/graphics/blend/SeparableBlendMode;

.field public static final COMPATIBLE:Lorg/apache/pdfbox/pdmodel/graphics/blend/SeparableBlendMode;

.field public static final DARKEN:Lorg/apache/pdfbox/pdmodel/graphics/blend/SeparableBlendMode;

.field public static final DIFFERENCE:Lorg/apache/pdfbox/pdmodel/graphics/blend/SeparableBlendMode;

.field public static final EXCLUSION:Lorg/apache/pdfbox/pdmodel/graphics/blend/SeparableBlendMode;

.field public static final HARD_LIGHT:Lorg/apache/pdfbox/pdmodel/graphics/blend/SeparableBlendMode;

.field public static final LIGHTEN:Lorg/apache/pdfbox/pdmodel/graphics/blend/SeparableBlendMode;

.field public static final MULTIPLY:Lorg/apache/pdfbox/pdmodel/graphics/blend/SeparableBlendMode;

.field public static final NORMAL:Lorg/apache/pdfbox/pdmodel/graphics/blend/SeparableBlendMode;

.field public static final OVERLAY:Lorg/apache/pdfbox/pdmodel/graphics/blend/SeparableBlendMode;

.field public static final SCREEN:Lorg/apache/pdfbox/pdmodel/graphics/blend/SeparableBlendMode;

.field public static final SOFT_LIGHT:Lorg/apache/pdfbox/pdmodel/graphics/blend/SeparableBlendMode;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lorg/apache/pdfbox/pdmodel/graphics/blend/BlendMode$1;

    invoke-direct {v0}, Lorg/apache/pdfbox/pdmodel/graphics/blend/BlendMode$1;-><init>()V

    sput-object v0, Lorg/apache/pdfbox/pdmodel/graphics/blend/BlendMode;->NORMAL:Lorg/apache/pdfbox/pdmodel/graphics/blend/SeparableBlendMode;

    sput-object v0, Lorg/apache/pdfbox/pdmodel/graphics/blend/BlendMode;->COMPATIBLE:Lorg/apache/pdfbox/pdmodel/graphics/blend/SeparableBlendMode;

    new-instance v0, Lorg/apache/pdfbox/pdmodel/graphics/blend/BlendMode$2;

    invoke-direct {v0}, Lorg/apache/pdfbox/pdmodel/graphics/blend/BlendMode$2;-><init>()V

    sput-object v0, Lorg/apache/pdfbox/pdmodel/graphics/blend/BlendMode;->MULTIPLY:Lorg/apache/pdfbox/pdmodel/graphics/blend/SeparableBlendMode;

    new-instance v0, Lorg/apache/pdfbox/pdmodel/graphics/blend/BlendMode$3;

    invoke-direct {v0}, Lorg/apache/pdfbox/pdmodel/graphics/blend/BlendMode$3;-><init>()V

    sput-object v0, Lorg/apache/pdfbox/pdmodel/graphics/blend/BlendMode;->SCREEN:Lorg/apache/pdfbox/pdmodel/graphics/blend/SeparableBlendMode;

    new-instance v0, Lorg/apache/pdfbox/pdmodel/graphics/blend/BlendMode$4;

    invoke-direct {v0}, Lorg/apache/pdfbox/pdmodel/graphics/blend/BlendMode$4;-><init>()V

    sput-object v0, Lorg/apache/pdfbox/pdmodel/graphics/blend/BlendMode;->OVERLAY:Lorg/apache/pdfbox/pdmodel/graphics/blend/SeparableBlendMode;

    new-instance v0, Lorg/apache/pdfbox/pdmodel/graphics/blend/BlendMode$5;

    invoke-direct {v0}, Lorg/apache/pdfbox/pdmodel/graphics/blend/BlendMode$5;-><init>()V

    sput-object v0, Lorg/apache/pdfbox/pdmodel/graphics/blend/BlendMode;->DARKEN:Lorg/apache/pdfbox/pdmodel/graphics/blend/SeparableBlendMode;

    new-instance v0, Lorg/apache/pdfbox/pdmodel/graphics/blend/BlendMode$6;

    invoke-direct {v0}, Lorg/apache/pdfbox/pdmodel/graphics/blend/BlendMode$6;-><init>()V

    sput-object v0, Lorg/apache/pdfbox/pdmodel/graphics/blend/BlendMode;->LIGHTEN:Lorg/apache/pdfbox/pdmodel/graphics/blend/SeparableBlendMode;

    new-instance v0, Lorg/apache/pdfbox/pdmodel/graphics/blend/BlendMode$7;

    invoke-direct {v0}, Lorg/apache/pdfbox/pdmodel/graphics/blend/BlendMode$7;-><init>()V

    sput-object v0, Lorg/apache/pdfbox/pdmodel/graphics/blend/BlendMode;->COLOR_DODGE:Lorg/apache/pdfbox/pdmodel/graphics/blend/SeparableBlendMode;

    new-instance v0, Lorg/apache/pdfbox/pdmodel/graphics/blend/BlendMode$8;

    invoke-direct {v0}, Lorg/apache/pdfbox/pdmodel/graphics/blend/BlendMode$8;-><init>()V

    sput-object v0, Lorg/apache/pdfbox/pdmodel/graphics/blend/BlendMode;->COLOR_BURN:Lorg/apache/pdfbox/pdmodel/graphics/blend/SeparableBlendMode;

    new-instance v0, Lorg/apache/pdfbox/pdmodel/graphics/blend/BlendMode$9;

    invoke-direct {v0}, Lorg/apache/pdfbox/pdmodel/graphics/blend/BlendMode$9;-><init>()V

    sput-object v0, Lorg/apache/pdfbox/pdmodel/graphics/blend/BlendMode;->HARD_LIGHT:Lorg/apache/pdfbox/pdmodel/graphics/blend/SeparableBlendMode;

    new-instance v0, Lorg/apache/pdfbox/pdmodel/graphics/blend/BlendMode$10;

    invoke-direct {v0}, Lorg/apache/pdfbox/pdmodel/graphics/blend/BlendMode$10;-><init>()V

    sput-object v0, Lorg/apache/pdfbox/pdmodel/graphics/blend/BlendMode;->SOFT_LIGHT:Lorg/apache/pdfbox/pdmodel/graphics/blend/SeparableBlendMode;

    new-instance v0, Lorg/apache/pdfbox/pdmodel/graphics/blend/BlendMode$11;

    invoke-direct {v0}, Lorg/apache/pdfbox/pdmodel/graphics/blend/BlendMode$11;-><init>()V

    sput-object v0, Lorg/apache/pdfbox/pdmodel/graphics/blend/BlendMode;->DIFFERENCE:Lorg/apache/pdfbox/pdmodel/graphics/blend/SeparableBlendMode;

    new-instance v0, Lorg/apache/pdfbox/pdmodel/graphics/blend/BlendMode$12;

    invoke-direct {v0}, Lorg/apache/pdfbox/pdmodel/graphics/blend/BlendMode$12;-><init>()V

    sput-object v0, Lorg/apache/pdfbox/pdmodel/graphics/blend/BlendMode;->EXCLUSION:Lorg/apache/pdfbox/pdmodel/graphics/blend/SeparableBlendMode;

    invoke-static {}, Lorg/apache/pdfbox/pdmodel/graphics/blend/BlendMode;->createBlendModeMap()Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lorg/apache/pdfbox/pdmodel/graphics/blend/BlendMode;->BLEND_MODES:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static createBlendModeMap()Ljava/util/Map;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Lorg/apache/pdfbox/cos/COSName;",
            "Lorg/apache/pdfbox/pdmodel/graphics/blend/BlendMode;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->NORMAL:Lorg/apache/pdfbox/cos/COSName;

    sget-object v2, Lorg/apache/pdfbox/pdmodel/graphics/blend/BlendMode;->NORMAL:Lorg/apache/pdfbox/pdmodel/graphics/blend/SeparableBlendMode;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->COMPATIBLE:Lorg/apache/pdfbox/cos/COSName;

    sget-object v2, Lorg/apache/pdfbox/pdmodel/graphics/blend/BlendMode;->COMPATIBLE:Lorg/apache/pdfbox/pdmodel/graphics/blend/SeparableBlendMode;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->MULTIPLY:Lorg/apache/pdfbox/cos/COSName;

    sget-object v2, Lorg/apache/pdfbox/pdmodel/graphics/blend/BlendMode;->MULTIPLY:Lorg/apache/pdfbox/pdmodel/graphics/blend/SeparableBlendMode;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->SCREEN:Lorg/apache/pdfbox/cos/COSName;

    sget-object v2, Lorg/apache/pdfbox/pdmodel/graphics/blend/BlendMode;->SCREEN:Lorg/apache/pdfbox/pdmodel/graphics/blend/SeparableBlendMode;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->OVERLAY:Lorg/apache/pdfbox/cos/COSName;

    sget-object v2, Lorg/apache/pdfbox/pdmodel/graphics/blend/BlendMode;->OVERLAY:Lorg/apache/pdfbox/pdmodel/graphics/blend/SeparableBlendMode;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->DARKEN:Lorg/apache/pdfbox/cos/COSName;

    sget-object v2, Lorg/apache/pdfbox/pdmodel/graphics/blend/BlendMode;->DARKEN:Lorg/apache/pdfbox/pdmodel/graphics/blend/SeparableBlendMode;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->LIGHTEN:Lorg/apache/pdfbox/cos/COSName;

    sget-object v2, Lorg/apache/pdfbox/pdmodel/graphics/blend/BlendMode;->LIGHTEN:Lorg/apache/pdfbox/pdmodel/graphics/blend/SeparableBlendMode;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->COLOR_DODGE:Lorg/apache/pdfbox/cos/COSName;

    sget-object v2, Lorg/apache/pdfbox/pdmodel/graphics/blend/BlendMode;->COLOR_DODGE:Lorg/apache/pdfbox/pdmodel/graphics/blend/SeparableBlendMode;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->COLOR_BURN:Lorg/apache/pdfbox/cos/COSName;

    sget-object v2, Lorg/apache/pdfbox/pdmodel/graphics/blend/BlendMode;->COLOR_BURN:Lorg/apache/pdfbox/pdmodel/graphics/blend/SeparableBlendMode;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->HARD_LIGHT:Lorg/apache/pdfbox/cos/COSName;

    sget-object v2, Lorg/apache/pdfbox/pdmodel/graphics/blend/BlendMode;->HARD_LIGHT:Lorg/apache/pdfbox/pdmodel/graphics/blend/SeparableBlendMode;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->SOFT_LIGHT:Lorg/apache/pdfbox/cos/COSName;

    sget-object v2, Lorg/apache/pdfbox/pdmodel/graphics/blend/BlendMode;->SOFT_LIGHT:Lorg/apache/pdfbox/pdmodel/graphics/blend/SeparableBlendMode;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->DIFFERENCE:Lorg/apache/pdfbox/cos/COSName;

    sget-object v2, Lorg/apache/pdfbox/pdmodel/graphics/blend/BlendMode;->DIFFERENCE:Lorg/apache/pdfbox/pdmodel/graphics/blend/SeparableBlendMode;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->EXCLUSION:Lorg/apache/pdfbox/cos/COSName;

    sget-object v2, Lorg/apache/pdfbox/pdmodel/graphics/blend/BlendMode;->EXCLUSION:Lorg/apache/pdfbox/pdmodel/graphics/blend/SeparableBlendMode;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object v0
.end method

.method public static getInstance(Lorg/apache/pdfbox/cos/COSBase;)Lorg/apache/pdfbox/pdmodel/graphics/blend/BlendMode;
    .locals 3

    instance-of v0, p0, Lorg/apache/pdfbox/cos/COSName;

    if-eqz v0, :cond_0

    sget-object v0, Lorg/apache/pdfbox/pdmodel/graphics/blend/BlendMode;->BLEND_MODES:Ljava/util/Map;

    check-cast p0, Lorg/apache/pdfbox/cos/COSName;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p0

    check-cast p0, Lorg/apache/pdfbox/pdmodel/graphics/blend/BlendMode;

    goto :goto_2

    :cond_0
    instance-of v0, p0, Lorg/apache/pdfbox/cos/COSArray;

    const/4 v1, 0x0

    if-eqz v0, :cond_2

    check-cast p0, Lorg/apache/pdfbox/cos/COSArray;

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0}, Lorg/apache/pdfbox/cos/COSArray;->size()I

    move-result v2

    if-ge v0, v2, :cond_2

    sget-object v1, Lorg/apache/pdfbox/pdmodel/graphics/blend/BlendMode;->BLEND_MODES:Ljava/util/Map;

    invoke-virtual {p0, v0}, Lorg/apache/pdfbox/cos/COSArray;->get(I)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/pdfbox/pdmodel/graphics/blend/BlendMode;

    if-eqz v1, :cond_1

    goto :goto_1

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    :goto_1
    move-object p0, v1

    :goto_2
    if-eqz p0, :cond_3

    return-object p0

    :cond_3
    sget-object p0, Lorg/apache/pdfbox/pdmodel/graphics/blend/BlendMode;->COMPATIBLE:Lorg/apache/pdfbox/pdmodel/graphics/blend/SeparableBlendMode;

    return-object p0
.end method

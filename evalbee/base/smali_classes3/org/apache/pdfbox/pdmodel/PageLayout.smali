.class public final enum Lorg/apache/pdfbox/pdmodel/PageLayout;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lorg/apache/pdfbox/pdmodel/PageLayout;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lorg/apache/pdfbox/pdmodel/PageLayout;

.field public static final enum ONE_COLUMN:Lorg/apache/pdfbox/pdmodel/PageLayout;

.field public static final enum SINGLE_PAGE:Lorg/apache/pdfbox/pdmodel/PageLayout;

.field public static final enum TWO_COLUMN_LEFT:Lorg/apache/pdfbox/pdmodel/PageLayout;

.field public static final enum TWO_COLUMN_RIGHT:Lorg/apache/pdfbox/pdmodel/PageLayout;

.field public static final enum TWO_PAGE_LEFT:Lorg/apache/pdfbox/pdmodel/PageLayout;

.field public static final enum TWO_PAGE_RIGHT:Lorg/apache/pdfbox/pdmodel/PageLayout;


# instance fields
.field private final value:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    new-instance v0, Lorg/apache/pdfbox/pdmodel/PageLayout;

    const/4 v1, 0x0

    const-string v2, "SinglePage"

    const-string v3, "SINGLE_PAGE"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/pdfbox/pdmodel/PageLayout;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/pdmodel/PageLayout;->SINGLE_PAGE:Lorg/apache/pdfbox/pdmodel/PageLayout;

    new-instance v1, Lorg/apache/pdfbox/pdmodel/PageLayout;

    const/4 v2, 0x1

    const-string v3, "OneColumn"

    const-string v4, "ONE_COLUMN"

    invoke-direct {v1, v4, v2, v3}, Lorg/apache/pdfbox/pdmodel/PageLayout;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lorg/apache/pdfbox/pdmodel/PageLayout;->ONE_COLUMN:Lorg/apache/pdfbox/pdmodel/PageLayout;

    new-instance v2, Lorg/apache/pdfbox/pdmodel/PageLayout;

    const/4 v3, 0x2

    const-string v4, "TwoColumnLeft"

    const-string v5, "TWO_COLUMN_LEFT"

    invoke-direct {v2, v5, v3, v4}, Lorg/apache/pdfbox/pdmodel/PageLayout;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v2, Lorg/apache/pdfbox/pdmodel/PageLayout;->TWO_COLUMN_LEFT:Lorg/apache/pdfbox/pdmodel/PageLayout;

    new-instance v3, Lorg/apache/pdfbox/pdmodel/PageLayout;

    const/4 v4, 0x3

    const-string v5, "TwoColumnRight"

    const-string v6, "TWO_COLUMN_RIGHT"

    invoke-direct {v3, v6, v4, v5}, Lorg/apache/pdfbox/pdmodel/PageLayout;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v3, Lorg/apache/pdfbox/pdmodel/PageLayout;->TWO_COLUMN_RIGHT:Lorg/apache/pdfbox/pdmodel/PageLayout;

    new-instance v4, Lorg/apache/pdfbox/pdmodel/PageLayout;

    const/4 v5, 0x4

    const-string v6, "TwoPageLeft"

    const-string v7, "TWO_PAGE_LEFT"

    invoke-direct {v4, v7, v5, v6}, Lorg/apache/pdfbox/pdmodel/PageLayout;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v4, Lorg/apache/pdfbox/pdmodel/PageLayout;->TWO_PAGE_LEFT:Lorg/apache/pdfbox/pdmodel/PageLayout;

    new-instance v5, Lorg/apache/pdfbox/pdmodel/PageLayout;

    const/4 v6, 0x5

    const-string v7, "TwoPageRight"

    const-string v8, "TWO_PAGE_RIGHT"

    invoke-direct {v5, v8, v6, v7}, Lorg/apache/pdfbox/pdmodel/PageLayout;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lorg/apache/pdfbox/pdmodel/PageLayout;->TWO_PAGE_RIGHT:Lorg/apache/pdfbox/pdmodel/PageLayout;

    filled-new-array/range {v0 .. v5}, [Lorg/apache/pdfbox/pdmodel/PageLayout;

    move-result-object v0

    sput-object v0, Lorg/apache/pdfbox/pdmodel/PageLayout;->$VALUES:[Lorg/apache/pdfbox/pdmodel/PageLayout;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lorg/apache/pdfbox/pdmodel/PageLayout;->value:Ljava/lang/String;

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lorg/apache/pdfbox/pdmodel/PageLayout;
    .locals 1

    const-string v0, "SinglePage"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object p0, Lorg/apache/pdfbox/pdmodel/PageLayout;->SINGLE_PAGE:Lorg/apache/pdfbox/pdmodel/PageLayout;

    return-object p0

    :cond_0
    const-string v0, "OneColumn"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object p0, Lorg/apache/pdfbox/pdmodel/PageLayout;->ONE_COLUMN:Lorg/apache/pdfbox/pdmodel/PageLayout;

    return-object p0

    :cond_1
    const-string v0, "TwoColumnLeft"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object p0, Lorg/apache/pdfbox/pdmodel/PageLayout;->TWO_COLUMN_LEFT:Lorg/apache/pdfbox/pdmodel/PageLayout;

    return-object p0

    :cond_2
    const-string v0, "TwoPageLeft"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object p0, Lorg/apache/pdfbox/pdmodel/PageLayout;->TWO_PAGE_LEFT:Lorg/apache/pdfbox/pdmodel/PageLayout;

    return-object p0

    :cond_3
    const-string v0, "TwoPageRight"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    sget-object p0, Lorg/apache/pdfbox/pdmodel/PageLayout;->TWO_PAGE_RIGHT:Lorg/apache/pdfbox/pdmodel/PageLayout;

    return-object p0

    :cond_4
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static valueOf(Ljava/lang/String;)Lorg/apache/pdfbox/pdmodel/PageLayout;
    .locals 1

    const-class v0, Lorg/apache/pdfbox/pdmodel/PageLayout;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lorg/apache/pdfbox/pdmodel/PageLayout;

    return-object p0
.end method

.method public static values()[Lorg/apache/pdfbox/pdmodel/PageLayout;
    .locals 1

    sget-object v0, Lorg/apache/pdfbox/pdmodel/PageLayout;->$VALUES:[Lorg/apache/pdfbox/pdmodel/PageLayout;

    invoke-virtual {v0}, [Lorg/apache/pdfbox/pdmodel/PageLayout;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lorg/apache/pdfbox/pdmodel/PageLayout;

    return-object v0
.end method


# virtual methods
.method public stringValue()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PageLayout;->value:Ljava/lang/String;

    return-object v0
.end method

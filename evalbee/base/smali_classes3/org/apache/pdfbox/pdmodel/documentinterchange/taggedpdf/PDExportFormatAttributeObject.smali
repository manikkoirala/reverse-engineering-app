.class public Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDExportFormatAttributeObject;
.super Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDLayoutAttributeObject;
.source "SourceFile"


# static fields
.field public static final OWNER_CSS_1_00:Ljava/lang/String; = "CSS-1.00"

.field public static final OWNER_CSS_2_00:Ljava/lang/String; = "CSS-2.00"

.field public static final OWNER_HTML_3_20:Ljava/lang/String; = "HTML-3.2"

.field public static final OWNER_HTML_4_01:Ljava/lang/String; = "HTML-4.01"

.field public static final OWNER_OEB_1_00:Ljava/lang/String; = "OEB-1.00"

.field public static final OWNER_RTF_1_05:Ljava/lang/String; = "RTF-1.05"

.field public static final OWNER_XML_1_00:Ljava/lang/String; = "XML-1.00"


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDLayoutAttributeObject;-><init>()V

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDAttributeObject;->setOwner(Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Lorg/apache/pdfbox/cos/COSDictionary;)V
    .locals 0

    .line 2
    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDLayoutAttributeObject;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    return-void
.end method


# virtual methods
.method public getColSpan()I
    .locals 2

    const-string v0, "ColSpan"

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->getInteger(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public getHeaders()[Ljava/lang/String;
    .locals 1

    const-string v0, "Headers"

    invoke-virtual {p0, v0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->getArrayOfString(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getListNumbering()Ljava/lang/String;
    .locals 2

    const-string v0, "ListNumbering"

    const-string v1, "None"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->getName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getRowSpan()I
    .locals 2

    const-string v0, "RowSpan"

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->getInteger(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public getScope()Ljava/lang/String;
    .locals 1

    const-string v0, "Scope"

    invoke-virtual {p0, v0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->getName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSummary()Ljava/lang/String;
    .locals 1

    const-string v0, "Summary"

    invoke-virtual {p0, v0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public setColSpan(I)V
    .locals 1

    const-string v0, "ColSpan"

    invoke-virtual {p0, v0, p1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->setInteger(Ljava/lang/String;I)V

    return-void
.end method

.method public setHeaders([Ljava/lang/String;)V
    .locals 1

    const-string v0, "Headers"

    invoke-virtual {p0, v0, p1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->setArrayOfString(Ljava/lang/String;[Ljava/lang/String;)V

    return-void
.end method

.method public setListNumbering(Ljava/lang/String;)V
    .locals 1

    const-string v0, "ListNumbering"

    invoke-virtual {p0, v0, p1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->setName(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public setRowSpan(I)V
    .locals 1

    const-string v0, "RowSpan"

    invoke-virtual {p0, v0, p1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->setInteger(Ljava/lang/String;I)V

    return-void
.end method

.method public setScope(Ljava/lang/String;)V
    .locals 1

    const-string v0, "Scope"

    invoke-virtual {p0, v0, p1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->setName(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public setSummary(Ljava/lang/String;)V
    .locals 1

    const-string v0, "Summary"

    invoke-virtual {p0, v0, p1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->setString(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-super {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDLayoutAttributeObject;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "ListNumbering"

    invoke-virtual {p0, v1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->isSpecified(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, ", ListNumbering="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDExportFormatAttributeObject;->getListNumbering()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    const-string v1, "RowSpan"

    invoke-virtual {p0, v1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->isSpecified(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, ", RowSpan="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDExportFormatAttributeObject;->getRowSpan()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    const-string v1, "ColSpan"

    invoke-virtual {p0, v1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->isSpecified(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, ", ColSpan="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDExportFormatAttributeObject;->getColSpan()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    const-string v1, "Headers"

    invoke-virtual {p0, v1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->isSpecified(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    const-string v1, ", Headers="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDExportFormatAttributeObject;->getHeaders()[Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDAttributeObject;->arrayToString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_3
    const-string v1, "Scope"

    invoke-virtual {p0, v1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->isSpecified(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    const-string v1, ", Scope="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDExportFormatAttributeObject;->getScope()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_4
    const-string v1, "Summary"

    invoke-virtual {p0, v1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->isSpecified(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    const-string v1, ", Summary="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDExportFormatAttributeObject;->getSummary()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_5
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

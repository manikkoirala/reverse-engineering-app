.class public Lorg/apache/pdfbox/pdmodel/documentinterchange/markedcontent/PDMarkedContent;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final contents:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private final properties:Lorg/apache/pdfbox/cos/COSDictionary;

.field private final tag:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSDictionary;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-nez p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Lorg/apache/pdfbox/cos/COSName;->getName()Ljava/lang/String;

    move-result-object p1

    :goto_0
    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/documentinterchange/markedcontent/PDMarkedContent;->tag:Ljava/lang/String;

    iput-object p2, p0, Lorg/apache/pdfbox/pdmodel/documentinterchange/markedcontent/PDMarkedContent;->properties:Lorg/apache/pdfbox/cos/COSDictionary;

    new-instance p1, Ljava/util/ArrayList;

    invoke-direct {p1}, Ljava/util/ArrayList;-><init>()V

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/documentinterchange/markedcontent/PDMarkedContent;->contents:Ljava/util/List;

    return-void
.end method

.method public static create(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSDictionary;)Lorg/apache/pdfbox/pdmodel/documentinterchange/markedcontent/PDMarkedContent;
    .locals 1

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->ARTIFACT:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, p0}, Lorg/apache/pdfbox/cos/COSName;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance p0, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDArtifactMarkedContent;

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDArtifactMarkedContent;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    return-object p0

    :cond_0
    new-instance v0, Lorg/apache/pdfbox/pdmodel/documentinterchange/markedcontent/PDMarkedContent;

    invoke-direct {v0, p0, p1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/markedcontent/PDMarkedContent;-><init>(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSDictionary;)V

    return-object v0
.end method


# virtual methods
.method public addMarkedContent(Lorg/apache/pdfbox/pdmodel/documentinterchange/markedcontent/PDMarkedContent;)V
    .locals 1

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/markedcontent/PDMarkedContent;->getContents()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public addText(Lorg/apache/pdfbox/text/TextPosition;)V
    .locals 1

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/markedcontent/PDMarkedContent;->getContents()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public addXObject(Lorg/apache/pdfbox/pdmodel/graphics/PDXObject;)V
    .locals 1

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/markedcontent/PDMarkedContent;->getContents()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public getActualText()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/markedcontent/PDMarkedContent;->getProperties()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/markedcontent/PDMarkedContent;->getProperties()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->ACTUAL_TEXT:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getString(Lorg/apache/pdfbox/cos/COSName;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public getAlternateDescription()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/markedcontent/PDMarkedContent;->getProperties()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/markedcontent/PDMarkedContent;->getProperties()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->ALT:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getString(Lorg/apache/pdfbox/cos/COSName;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public getContents()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/documentinterchange/markedcontent/PDMarkedContent;->contents:Ljava/util/List;

    return-object v0
.end method

.method public getExpandedForm()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/markedcontent/PDMarkedContent;->getProperties()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/markedcontent/PDMarkedContent;->getProperties()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->E:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getString(Lorg/apache/pdfbox/cos/COSName;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public getLanguage()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/markedcontent/PDMarkedContent;->getProperties()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/markedcontent/PDMarkedContent;->getProperties()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->LANG:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getNameAsString(Lorg/apache/pdfbox/cos/COSName;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public getMCID()I
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/markedcontent/PDMarkedContent;->getProperties()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/markedcontent/PDMarkedContent;->getProperties()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->MCID:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getInt(Lorg/apache/pdfbox/cos/COSName;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    :goto_0
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public getProperties()Lorg/apache/pdfbox/cos/COSDictionary;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/documentinterchange/markedcontent/PDMarkedContent;->properties:Lorg/apache/pdfbox/cos/COSDictionary;

    return-object v0
.end method

.method public getTag()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/documentinterchange/markedcontent/PDMarkedContent;->tag:Ljava/lang/String;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "tag="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lorg/apache/pdfbox/pdmodel/documentinterchange/markedcontent/PDMarkedContent;->tag:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", properties="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lorg/apache/pdfbox/pdmodel/documentinterchange/markedcontent/PDMarkedContent;->properties:Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", contents="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lorg/apache/pdfbox/pdmodel/documentinterchange/markedcontent/PDMarkedContent;->contents:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

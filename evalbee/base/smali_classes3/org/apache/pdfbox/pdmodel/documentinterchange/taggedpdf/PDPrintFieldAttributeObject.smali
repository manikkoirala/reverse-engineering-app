.class public Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDPrintFieldAttributeObject;
.super Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;
.source "SourceFile"


# static fields
.field private static final CHECKED:Ljava/lang/String; = "checked"

.field public static final CHECKED_STATE_NEUTRAL:Ljava/lang/String; = "neutral"

.field public static final CHECKED_STATE_OFF:Ljava/lang/String; = "off"

.field public static final CHECKED_STATE_ON:Ljava/lang/String; = "on"

.field private static final DESC:Ljava/lang/String; = "Desc"

.field public static final OWNER_PRINT_FIELD:Ljava/lang/String; = "PrintField"

.field private static final ROLE:Ljava/lang/String; = "Role"

.field public static final ROLE_CB:Ljava/lang/String; = "cb"

.field public static final ROLE_PB:Ljava/lang/String; = "pb"

.field public static final ROLE_RB:Ljava/lang/String; = "rb"

.field public static final ROLE_TV:Ljava/lang/String; = "tv"


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;-><init>()V

    const-string v0, "PrintField"

    invoke-virtual {p0, v0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDAttributeObject;->setOwner(Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Lorg/apache/pdfbox/cos/COSDictionary;)V
    .locals 0

    .line 2
    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    return-void
.end method


# virtual methods
.method public getAlternateName()Ljava/lang/String;
    .locals 1

    const-string v0, "Desc"

    invoke-virtual {p0, v0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getCheckedState()Ljava/lang/String;
    .locals 2

    const-string v0, "checked"

    const-string v1, "off"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->getName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getRole()Ljava/lang/String;
    .locals 1

    const-string v0, "Role"

    invoke-virtual {p0, v0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->getName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public setAlternateName(Ljava/lang/String;)V
    .locals 1

    const-string v0, "Desc"

    invoke-virtual {p0, v0, p1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->setString(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public setCheckedState(Ljava/lang/String;)V
    .locals 1

    const-string v0, "checked"

    invoke-virtual {p0, v0, p1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->setName(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public setRole(Ljava/lang/String;)V
    .locals 1

    const-string v0, "Role"

    invoke-virtual {p0, v0, p1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->setName(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-super {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDAttributeObject;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "Role"

    invoke-virtual {p0, v1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->isSpecified(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, ", Role="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDPrintFieldAttributeObject;->getRole()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    const-string v1, "checked"

    invoke-virtual {p0, v1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->isSpecified(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, ", Checked="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDPrintFieldAttributeObject;->getCheckedState()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    const-string v1, "Desc"

    invoke-virtual {p0, v1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->isSpecified(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, ", Desc="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDPrintFieldAttributeObject;->getAlternateName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.class public Lorg/apache/pdfbox/pdmodel/documentinterchange/markedcontent/PDPropertyList;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/pdfbox/pdmodel/common/COSObjectable;


# instance fields
.field protected final dict:Lorg/apache/pdfbox/cos/COSDictionary;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-direct {v0}, Lorg/apache/pdfbox/cos/COSDictionary;-><init>()V

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/documentinterchange/markedcontent/PDPropertyList;->dict:Lorg/apache/pdfbox/cos/COSDictionary;

    return-void
.end method

.method public constructor <init>(Lorg/apache/pdfbox/cos/COSDictionary;)V
    .locals 0

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/documentinterchange/markedcontent/PDPropertyList;->dict:Lorg/apache/pdfbox/cos/COSDictionary;

    return-void
.end method

.method public static create(Lorg/apache/pdfbox/cos/COSDictionary;)Lorg/apache/pdfbox/pdmodel/documentinterchange/markedcontent/PDPropertyList;
    .locals 2

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->OCG:Lorg/apache/pdfbox/cos/COSName;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->TYPE:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getItem(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSName;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lorg/apache/pdfbox/pdmodel/graphics/optionalcontent/PDOptionalContentGroup;

    invoke-direct {v0, p0}, Lorg/apache/pdfbox/pdmodel/graphics/optionalcontent/PDOptionalContentGroup;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    return-object v0

    :cond_0
    new-instance v0, Lorg/apache/pdfbox/pdmodel/documentinterchange/markedcontent/PDPropertyList;

    invoke-direct {v0, p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/markedcontent/PDPropertyList;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    return-object v0
.end method


# virtual methods
.method public bridge synthetic getCOSObject()Lorg/apache/pdfbox/cos/COSBase;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/markedcontent/PDPropertyList;->getCOSObject()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    return-object v0
.end method

.method public getCOSObject()Lorg/apache/pdfbox/cos/COSDictionary;
    .locals 1

    .line 2
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/documentinterchange/markedcontent/PDPropertyList;->dict:Lorg/apache/pdfbox/cos/COSDictionary;

    return-object v0
.end method

.class public final Lorg/apache/pdfbox/pdmodel/documentinterchange/prepress/PDBoxStyle;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/pdfbox/pdmodel/common/COSObjectable;


# static fields
.field public static final GUIDELINE_STYLE_DASHED:Ljava/lang/String; = "D"

.field public static final GUIDELINE_STYLE_SOLID:Ljava/lang/String; = "S"


# instance fields
.field private dictionary:Lorg/apache/pdfbox/cos/COSDictionary;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-direct {v0}, Lorg/apache/pdfbox/cos/COSDictionary;-><init>()V

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/documentinterchange/prepress/PDBoxStyle;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    return-void
.end method

.method public constructor <init>(Lorg/apache/pdfbox/cos/COSDictionary;)V
    .locals 0

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/documentinterchange/prepress/PDBoxStyle;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    return-void
.end method


# virtual methods
.method public getCOSObject()Lorg/apache/pdfbox/cos/COSBase;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/documentinterchange/prepress/PDBoxStyle;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    return-object v0
.end method

.method public getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/documentinterchange/prepress/PDBoxStyle;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    return-object v0
.end method

.method public getGuidelineColor()Lorg/apache/pdfbox/pdmodel/graphics/color/PDColor;
    .locals 3

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/documentinterchange/prepress/PDBoxStyle;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->C:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSArray;

    if-nez v0, :cond_0

    new-instance v0, Lorg/apache/pdfbox/cos/COSArray;

    invoke-direct {v0}, Lorg/apache/pdfbox/cos/COSArray;-><init>()V

    sget-object v1, Lorg/apache/pdfbox/cos/COSInteger;->ZERO:Lorg/apache/pdfbox/cos/COSInteger;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    iget-object v1, p0, Lorg/apache/pdfbox/pdmodel/documentinterchange/prepress/PDBoxStyle;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    const-string v2, "C"

    invoke-virtual {v1, v2, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Ljava/lang/String;Lorg/apache/pdfbox/cos/COSBase;)V

    :cond_0
    new-instance v1, Lorg/apache/pdfbox/pdmodel/graphics/color/PDColor;

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSArray;->toFloatArray()[F

    move-result-object v0

    sget-object v2, Lorg/apache/pdfbox/pdmodel/graphics/color/PDDeviceRGB;->INSTANCE:Lorg/apache/pdfbox/pdmodel/graphics/color/PDDeviceRGB;

    invoke-direct {v1, v0, v2}, Lorg/apache/pdfbox/pdmodel/graphics/color/PDColor;-><init>([FLorg/apache/pdfbox/pdmodel/graphics/color/PDColorSpace;)V

    return-object v1
.end method

.method public getGuidelineStyle()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/documentinterchange/prepress/PDBoxStyle;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->S:Lorg/apache/pdfbox/cos/COSName;

    const-string v2, "S"

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->getNameAsString(Lorg/apache/pdfbox/cos/COSName;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getGuidelineWidth()F
    .locals 3

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/documentinterchange/prepress/PDBoxStyle;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->W:Lorg/apache/pdfbox/cos/COSName;

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->getFloat(Lorg/apache/pdfbox/cos/COSName;F)F

    move-result v0

    return v0
.end method

.method public getLineDashPattern()Lorg/apache/pdfbox/pdmodel/graphics/PDLineDashPattern;
    .locals 3

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/documentinterchange/prepress/PDBoxStyle;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->D:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSArray;

    if-nez v0, :cond_0

    new-instance v0, Lorg/apache/pdfbox/cos/COSArray;

    invoke-direct {v0}, Lorg/apache/pdfbox/cos/COSArray;-><init>()V

    sget-object v2, Lorg/apache/pdfbox/cos/COSInteger;->THREE:Lorg/apache/pdfbox/cos/COSInteger;

    invoke-virtual {v0, v2}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    iget-object v2, p0, Lorg/apache/pdfbox/pdmodel/documentinterchange/prepress/PDBoxStyle;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-virtual {v2, v1, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    :cond_0
    new-instance v1, Lorg/apache/pdfbox/cos/COSArray;

    invoke-direct {v1}, Lorg/apache/pdfbox/cos/COSArray;-><init>()V

    invoke-virtual {v1, v0}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    new-instance v0, Lorg/apache/pdfbox/pdmodel/graphics/PDLineDashPattern;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lorg/apache/pdfbox/pdmodel/graphics/PDLineDashPattern;-><init>(Lorg/apache/pdfbox/cos/COSArray;I)V

    return-object v0
.end method

.method public setGuideLineColor(Lorg/apache/pdfbox/pdmodel/graphics/color/PDColor;)V
    .locals 2

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/graphics/color/PDColor;->toCOSArray()Lorg/apache/pdfbox/cos/COSArray;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/documentinterchange/prepress/PDBoxStyle;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->C:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    return-void
.end method

.method public setGuidelineStyle(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/documentinterchange/prepress/PDBoxStyle;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->S:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setName(Lorg/apache/pdfbox/cos/COSName;Ljava/lang/String;)V

    return-void
.end method

.method public setGuidelineWidth(F)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/documentinterchange/prepress/PDBoxStyle;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->W:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setFloat(Lorg/apache/pdfbox/cos/COSName;F)V

    return-void
.end method

.method public setLineDashPattern(Lorg/apache/pdfbox/cos/COSArray;)V
    .locals 2

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/documentinterchange/prepress/PDBoxStyle;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->D:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    return-void
.end method

.class public Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDMarkInfo;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/pdfbox/pdmodel/common/COSObjectable;


# instance fields
.field private dictionary:Lorg/apache/pdfbox/cos/COSDictionary;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-direct {v0}, Lorg/apache/pdfbox/cos/COSDictionary;-><init>()V

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDMarkInfo;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    return-void
.end method

.method public constructor <init>(Lorg/apache/pdfbox/cos/COSDictionary;)V
    .locals 0

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDMarkInfo;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    return-void
.end method


# virtual methods
.method public getCOSObject()Lorg/apache/pdfbox/cos/COSBase;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDMarkInfo;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    return-object v0
.end method

.method public getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDMarkInfo;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    return-object v0
.end method

.method public isMarked()Z
    .locals 3

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDMarkInfo;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    const-string v1, "Marked"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public isSuspect()Z
    .locals 3

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDMarkInfo;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    const-string v1, "Suspects"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public setMarked(Z)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDMarkInfo;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    const-string v1, "Marked"

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setBoolean(Ljava/lang/String;Z)V

    return-void
.end method

.method public setSuspect(Z)V
    .locals 2

    iget-object p1, p0, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDMarkInfo;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    const-string v0, "Suspects"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->setBoolean(Ljava/lang/String;Z)V

    return-void
.end method

.method public setUserProperties(Z)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDMarkInfo;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    const-string v1, "UserProperties"

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setBoolean(Ljava/lang/String;Z)V

    return-void
.end method

.method public usesUserProperties()Z
    .locals 3

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDMarkInfo;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    const-string v1, "UserProperties"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

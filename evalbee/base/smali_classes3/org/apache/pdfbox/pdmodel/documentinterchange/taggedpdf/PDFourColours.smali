.class public Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDFourColours;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/pdfbox/pdmodel/common/COSObjectable;


# instance fields
.field private final array:Lorg/apache/pdfbox/cos/COSArray;


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lorg/apache/pdfbox/cos/COSArray;

    invoke-direct {v0}, Lorg/apache/pdfbox/cos/COSArray;-><init>()V

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDFourColours;->array:Lorg/apache/pdfbox/cos/COSArray;

    sget-object v1, Lorg/apache/pdfbox/cos/COSNull;->NULL:Lorg/apache/pdfbox/cos/COSNull;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    return-void
.end method

.method public constructor <init>(Lorg/apache/pdfbox/cos/COSArray;)V
    .locals 3

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDFourColours;->array:Lorg/apache/pdfbox/cos/COSArray;

    invoke-virtual {p1}, Lorg/apache/pdfbox/cos/COSArray;->size()I

    move-result v0

    const/4 v1, 0x4

    if-ge v0, v1, :cond_0

    invoke-virtual {p1}, Lorg/apache/pdfbox/cos/COSArray;->size()I

    move-result p1

    add-int/lit8 p1, p1, -0x1

    :goto_0
    if-ge p1, v1, :cond_0

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDFourColours;->array:Lorg/apache/pdfbox/cos/COSArray;

    sget-object v2, Lorg/apache/pdfbox/cos/COSNull;->NULL:Lorg/apache/pdfbox/cos/COSNull;

    invoke-virtual {v0, v2}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private getColourByIndex(I)Lorg/apache/pdfbox/pdmodel/graphics/color/PDGamma;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDFourColours;->array:Lorg/apache/pdfbox/cos/COSArray;

    invoke-virtual {v0, p1}, Lorg/apache/pdfbox/cos/COSArray;->getObject(I)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object p1

    instance-of v0, p1, Lorg/apache/pdfbox/cos/COSArray;

    if-eqz v0, :cond_0

    new-instance v0, Lorg/apache/pdfbox/pdmodel/graphics/color/PDGamma;

    check-cast p1, Lorg/apache/pdfbox/cos/COSArray;

    invoke-direct {v0, p1}, Lorg/apache/pdfbox/pdmodel/graphics/color/PDGamma;-><init>(Lorg/apache/pdfbox/cos/COSArray;)V

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method private setColourByIndex(ILorg/apache/pdfbox/pdmodel/graphics/color/PDGamma;)V
    .locals 1

    if-nez p2, :cond_0

    sget-object p2, Lorg/apache/pdfbox/cos/COSNull;->NULL:Lorg/apache/pdfbox/cos/COSNull;

    goto :goto_0

    :cond_0
    invoke-virtual {p2}, Lorg/apache/pdfbox/pdmodel/graphics/color/PDGamma;->getCOSArray()Lorg/apache/pdfbox/cos/COSArray;

    move-result-object p2

    :goto_0
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDFourColours;->array:Lorg/apache/pdfbox/cos/COSArray;

    invoke-virtual {v0, p1, p2}, Lorg/apache/pdfbox/cos/COSArray;->set(ILorg/apache/pdfbox/cos/COSBase;)V

    return-void
.end method


# virtual methods
.method public getAfterColour()Lorg/apache/pdfbox/pdmodel/graphics/color/PDGamma;
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDFourColours;->getColourByIndex(I)Lorg/apache/pdfbox/pdmodel/graphics/color/PDGamma;

    move-result-object v0

    return-object v0
.end method

.method public getBeforeColour()Lorg/apache/pdfbox/pdmodel/graphics/color/PDGamma;
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDFourColours;->getColourByIndex(I)Lorg/apache/pdfbox/pdmodel/graphics/color/PDGamma;

    move-result-object v0

    return-object v0
.end method

.method public getCOSObject()Lorg/apache/pdfbox/cos/COSBase;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDFourColours;->array:Lorg/apache/pdfbox/cos/COSArray;

    return-object v0
.end method

.method public getEndColour()Lorg/apache/pdfbox/pdmodel/graphics/color/PDGamma;
    .locals 1

    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDFourColours;->getColourByIndex(I)Lorg/apache/pdfbox/pdmodel/graphics/color/PDGamma;

    move-result-object v0

    return-object v0
.end method

.method public getStartColour()Lorg/apache/pdfbox/pdmodel/graphics/color/PDGamma;
    .locals 1

    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDFourColours;->getColourByIndex(I)Lorg/apache/pdfbox/pdmodel/graphics/color/PDGamma;

    move-result-object v0

    return-object v0
.end method

.method public setAfterColour(Lorg/apache/pdfbox/pdmodel/graphics/color/PDGamma;)V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0, p1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDFourColours;->setColourByIndex(ILorg/apache/pdfbox/pdmodel/graphics/color/PDGamma;)V

    return-void
.end method

.method public setBeforeColour(Lorg/apache/pdfbox/pdmodel/graphics/color/PDGamma;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0, p1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDFourColours;->setColourByIndex(ILorg/apache/pdfbox/pdmodel/graphics/color/PDGamma;)V

    return-void
.end method

.method public setEndColour(Lorg/apache/pdfbox/pdmodel/graphics/color/PDGamma;)V
    .locals 1

    const/4 v0, 0x3

    invoke-direct {p0, v0, p1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDFourColours;->setColourByIndex(ILorg/apache/pdfbox/pdmodel/graphics/color/PDGamma;)V

    return-void
.end method

.method public setStartColour(Lorg/apache/pdfbox/pdmodel/graphics/color/PDGamma;)V
    .locals 1

    const/4 v0, 0x2

    invoke-direct {p0, v0, p1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDFourColours;->setColourByIndex(ILorg/apache/pdfbox/pdmodel/graphics/color/PDGamma;)V

    return-void
.end method

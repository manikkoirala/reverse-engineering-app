.class public abstract Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureNode;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/pdfbox/pdmodel/common/COSObjectable;


# instance fields
.field private final dictionary:Lorg/apache/pdfbox/cos/COSDictionary;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 2

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-direct {v0}, Lorg/apache/pdfbox/cos/COSDictionary;-><init>()V

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureNode;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->TYPE:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setName(Lorg/apache/pdfbox/cos/COSName;Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Lorg/apache/pdfbox/cos/COSDictionary;)V
    .locals 0

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureNode;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    return-void
.end method

.method public static create(Lorg/apache/pdfbox/cos/COSDictionary;)Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureNode;
    .locals 2

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->TYPE:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p0, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->getNameAsString(Lorg/apache/pdfbox/cos/COSName;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "StructTreeRoot"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v0, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureTreeRoot;

    invoke-direct {v0, p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureTreeRoot;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    return-object v0

    :cond_0
    if-eqz v0, :cond_2

    const-string v1, "StructElem"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    goto :goto_0

    :cond_1
    new-instance p0, Ljava/lang/IllegalArgumentException;

    const-string v0, "Dictionary must not include a Type entry with a value that is neither StructTreeRoot nor StructElem."

    invoke-direct {p0, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p0

    :cond_2
    :goto_0
    new-instance v0, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureElement;

    invoke-direct {v0, p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureElement;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    return-object v0
.end method


# virtual methods
.method public appendKid(Lorg/apache/pdfbox/cos/COSBase;)V
    .locals 3

    .line 1
    if-nez p1, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureNode;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->K:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureNode;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    goto :goto_0

    :cond_1
    instance-of v2, v0, Lorg/apache/pdfbox/cos/COSArray;

    if-eqz v2, :cond_2

    check-cast v0, Lorg/apache/pdfbox/cos/COSArray;

    invoke-virtual {v0, p1}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    goto :goto_0

    :cond_2
    new-instance v2, Lorg/apache/pdfbox/cos/COSArray;

    invoke-direct {v2}, Lorg/apache/pdfbox/cos/COSArray;-><init>()V

    invoke-virtual {v2, v0}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    invoke-virtual {v2, p1}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureNode;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object p1

    invoke-virtual {p1, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    :goto_0
    return-void
.end method

.method public appendKid(Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureElement;)V
    .locals 0

    .line 2
    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureNode;->appendObjectableKid(Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)V

    invoke-virtual {p1, p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureElement;->setParent(Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureNode;)V

    return-void
.end method

.method public appendObjectableKid(Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)V
    .locals 0

    if-nez p1, :cond_0

    return-void

    :cond_0
    invoke-interface {p1}, Lorg/apache/pdfbox/pdmodel/common/COSObjectable;->getCOSObject()Lorg/apache/pdfbox/cos/COSBase;

    move-result-object p1

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureNode;->appendKid(Lorg/apache/pdfbox/cos/COSBase;)V

    return-void
.end method

.method public createObject(Lorg/apache/pdfbox/cos/COSBase;)Ljava/lang/Object;
    .locals 3

    instance-of v0, p1, Lorg/apache/pdfbox/cos/COSDictionary;

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    move-object v0, p1

    check-cast v0, Lorg/apache/pdfbox/cos/COSDictionary;

    goto :goto_0

    :cond_0
    instance-of v0, p1, Lorg/apache/pdfbox/cos/COSObject;

    if-eqz v0, :cond_1

    move-object v0, p1

    check-cast v0, Lorg/apache/pdfbox/cos/COSObject;

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSObject;->getObject()Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    instance-of v2, v0, Lorg/apache/pdfbox/cos/COSDictionary;

    if-eqz v2, :cond_1

    check-cast v0, Lorg/apache/pdfbox/cos/COSDictionary;

    goto :goto_0

    :cond_1
    move-object v0, v1

    :goto_0
    if-eqz v0, :cond_5

    sget-object p1, Lorg/apache/pdfbox/cos/COSName;->TYPE:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->getNameAsString(Lorg/apache/pdfbox/cos/COSName;)Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_4

    const-string v2, "StructElem"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    goto :goto_1

    :cond_2
    const-string v2, "OBJR"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    new-instance p1, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDObjectReference;

    invoke-direct {p1, v0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDObjectReference;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    return-object p1

    :cond_3
    const-string v2, "MCR"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_6

    new-instance p1, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDMarkedContentReference;

    invoke-direct {p1, v0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDMarkedContentReference;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    return-object p1

    :cond_4
    :goto_1
    new-instance p1, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureElement;

    invoke-direct {p1, v0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureElement;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    return-object p1

    :cond_5
    instance-of v0, p1, Lorg/apache/pdfbox/cos/COSInteger;

    if-eqz v0, :cond_6

    check-cast p1, Lorg/apache/pdfbox/cos/COSInteger;

    invoke-virtual {p1}, Lorg/apache/pdfbox/cos/COSInteger;->intValue()I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    return-object p1

    :cond_6
    return-object v1
.end method

.method public getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureNode;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    return-object v0
.end method

.method public getCOSObject()Lorg/apache/pdfbox/cos/COSBase;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureNode;->dictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    return-object v0
.end method

.method public getKids()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureNode;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v1

    sget-object v2, Lorg/apache/pdfbox/cos/COSName;->K:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v1

    instance-of v2, v1, Lorg/apache/pdfbox/cos/COSArray;

    if-eqz v2, :cond_1

    check-cast v1, Lorg/apache/pdfbox/cos/COSArray;

    invoke-virtual {v1}, Lorg/apache/pdfbox/cos/COSArray;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/pdfbox/cos/COSBase;

    invoke-virtual {p0, v2}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureNode;->createObject(Lorg/apache/pdfbox/cos/COSBase;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    invoke-virtual {p0, v1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureNode;->createObject(Lorg/apache/pdfbox/cos/COSBase;)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    return-object v0
.end method

.method public getType()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureNode;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->TYPE:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getNameAsString(Lorg/apache/pdfbox/cos/COSName;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public insertBefore(Lorg/apache/pdfbox/cos/COSBase;Ljava/lang/Object;)V
    .locals 4

    .line 1
    if-eqz p1, :cond_6

    if-nez p2, :cond_0

    goto :goto_1

    :cond_0
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureNode;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->K:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    if-nez v0, :cond_1

    return-void

    :cond_1
    instance-of v2, p2, Lorg/apache/pdfbox/pdmodel/common/COSObjectable;

    if-eqz v2, :cond_2

    check-cast p2, Lorg/apache/pdfbox/pdmodel/common/COSObjectable;

    invoke-interface {p2}, Lorg/apache/pdfbox/pdmodel/common/COSObjectable;->getCOSObject()Lorg/apache/pdfbox/cos/COSBase;

    move-result-object p2

    goto :goto_0

    :cond_2
    instance-of v2, p2, Lorg/apache/pdfbox/cos/COSInteger;

    if-eqz v2, :cond_3

    check-cast p2, Lorg/apache/pdfbox/cos/COSBase;

    goto :goto_0

    :cond_3
    const/4 p2, 0x0

    :goto_0
    instance-of v2, v0, Lorg/apache/pdfbox/cos/COSArray;

    if-eqz v2, :cond_4

    check-cast v0, Lorg/apache/pdfbox/cos/COSArray;

    invoke-virtual {v0, p2}, Lorg/apache/pdfbox/cos/COSArray;->indexOfObject(Lorg/apache/pdfbox/cos/COSBase;)I

    move-result p2

    invoke-virtual {p1}, Lorg/apache/pdfbox/cos/COSBase;->getCOSObject()Lorg/apache/pdfbox/cos/COSBase;

    move-result-object p1

    invoke-virtual {v0, p2, p1}, Lorg/apache/pdfbox/cos/COSArray;->add(ILorg/apache/pdfbox/cos/COSBase;)V

    goto :goto_1

    :cond_4
    invoke-virtual {v0, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    instance-of v3, v0, Lorg/apache/pdfbox/cos/COSObject;

    if-eqz v3, :cond_5

    check-cast v0, Lorg/apache/pdfbox/cos/COSObject;

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSObject;->getObject()Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    :cond_5
    if-eqz v2, :cond_6

    new-instance v0, Lorg/apache/pdfbox/cos/COSArray;

    invoke-direct {v0}, Lorg/apache/pdfbox/cos/COSArray;-><init>()V

    invoke-virtual {v0, p1}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    invoke-virtual {v0, p2}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureNode;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object p1

    invoke-virtual {p1, v1, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    :cond_6
    :goto_1
    return-void
.end method

.method public insertBefore(Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureElement;Ljava/lang/Object;)V
    .locals 0

    .line 2
    invoke-virtual {p0, p1, p2}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureNode;->insertObjectableBefore(Lorg/apache/pdfbox/pdmodel/common/COSObjectable;Ljava/lang/Object;)V

    return-void
.end method

.method public insertObjectableBefore(Lorg/apache/pdfbox/pdmodel/common/COSObjectable;Ljava/lang/Object;)V
    .locals 0

    if-nez p1, :cond_0

    return-void

    :cond_0
    invoke-interface {p1}, Lorg/apache/pdfbox/pdmodel/common/COSObjectable;->getCOSObject()Lorg/apache/pdfbox/cos/COSBase;

    move-result-object p1

    invoke-virtual {p0, p1, p2}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureNode;->insertBefore(Lorg/apache/pdfbox/cos/COSBase;Ljava/lang/Object;)V

    return-void
.end method

.method public removeKid(Lorg/apache/pdfbox/cos/COSBase;)Z
    .locals 6

    .line 1
    const/4 v0, 0x0

    if-nez p1, :cond_0

    return v0

    :cond_0
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureNode;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v1

    sget-object v2, Lorg/apache/pdfbox/cos/COSName;->K:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v1

    if-nez v1, :cond_1

    return v0

    :cond_1
    instance-of v3, v1, Lorg/apache/pdfbox/cos/COSArray;

    const/4 v4, 0x1

    if-eqz v3, :cond_3

    check-cast v1, Lorg/apache/pdfbox/cos/COSArray;

    invoke-virtual {v1, p1}, Lorg/apache/pdfbox/cos/COSArray;->removeObject(Lorg/apache/pdfbox/cos/COSBase;)Z

    move-result p1

    invoke-virtual {v1}, Lorg/apache/pdfbox/cos/COSArray;->size()I

    move-result v3

    if-ne v3, v4, :cond_2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureNode;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v3

    invoke-virtual {v1, v0}, Lorg/apache/pdfbox/cos/COSArray;->getObject(I)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    invoke-virtual {v3, v2, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    :cond_2
    return p1

    :cond_3
    invoke-virtual {v1, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    instance-of v5, v1, Lorg/apache/pdfbox/cos/COSObject;

    if-eqz v5, :cond_4

    check-cast v1, Lorg/apache/pdfbox/cos/COSObject;

    invoke-virtual {v1}, Lorg/apache/pdfbox/cos/COSObject;->getObject()Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    :cond_4
    if-eqz v3, :cond_5

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureNode;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object p1

    const/4 v0, 0x0

    invoke-virtual {p1, v2, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    return v4

    :cond_5
    return v0
.end method

.method public removeKid(Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureElement;)Z
    .locals 2

    .line 2
    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureNode;->removeObjectableKid(Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureElement;->setParent(Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureNode;)V

    :cond_0
    return v0
.end method

.method public removeObjectableKid(Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)Z
    .locals 0

    if-nez p1, :cond_0

    const/4 p1, 0x0

    return p1

    :cond_0
    invoke-interface {p1}, Lorg/apache/pdfbox/pdmodel/common/COSObjectable;->getCOSObject()Lorg/apache/pdfbox/cos/COSBase;

    move-result-object p1

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureNode;->removeKid(Lorg/apache/pdfbox/cos/COSBase;)Z

    move-result p1

    return p1
.end method

.method public setKids(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureNode;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->K:Lorg/apache/pdfbox/cos/COSName;

    invoke-static {p1}, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;->converterToCOSArray(Ljava/util/List;)Lorg/apache/pdfbox/cos/COSArray;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    return-void
.end method

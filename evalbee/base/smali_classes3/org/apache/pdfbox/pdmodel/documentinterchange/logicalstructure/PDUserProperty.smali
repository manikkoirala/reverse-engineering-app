.class public Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDUserProperty;
.super Lorg/apache/pdfbox/pdmodel/common/PDDictionaryWrapper;
.source "SourceFile"


# instance fields
.field private final userAttributeObject:Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDUserAttributeObject;


# direct methods
.method public constructor <init>(Lorg/apache/pdfbox/cos/COSDictionary;Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDUserAttributeObject;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/common/PDDictionaryWrapper;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    iput-object p2, p0, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDUserProperty;->userAttributeObject:Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDUserAttributeObject;

    return-void
.end method

.method public constructor <init>(Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDUserAttributeObject;)V
    .locals 0

    .line 2
    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/common/PDDictionaryWrapper;-><init>()V

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDUserProperty;->userAttributeObject:Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDUserAttributeObject;

    return-void
.end method

.method private static isEntryChanged(Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 1

    const/4 v0, 0x1

    if-nez p0, :cond_1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result p0

    xor-int/2addr p0, v0

    return p0
.end method

.method private potentiallyNotifyChanged(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    invoke-static {p1, p2}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDUserProperty;->isEntryChanged(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    iget-object p1, p0, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDUserProperty;->userAttributeObject:Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDUserAttributeObject;

    invoke-virtual {p1, p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDUserAttributeObject;->userPropertyChanged(Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDUserProperty;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    if-ne p0, p1, :cond_0

    return v0

    :cond_0
    invoke-super {p0, p1}, Lorg/apache/pdfbox/pdmodel/common/PDDictionaryWrapper;->equals(Ljava/lang/Object;)Z

    move-result v1

    const/4 v2, 0x0

    if-nez v1, :cond_1

    return v2

    :cond_1
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v1, v3, :cond_2

    return v2

    :cond_2
    check-cast p1, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDUserProperty;

    iget-object v1, p0, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDUserProperty;->userAttributeObject:Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDUserAttributeObject;

    iget-object p1, p1, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDUserProperty;->userAttributeObject:Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDUserAttributeObject;

    if-nez v1, :cond_3

    if-eqz p1, :cond_4

    return v2

    :cond_3
    invoke-virtual {v1, p1}, Lorg/apache/pdfbox/pdmodel/common/PDDictionaryWrapper;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_4

    return v2

    :cond_4
    return v0
.end method

.method public getFormattedValue()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/common/PDDictionaryWrapper;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->F:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getString(Lorg/apache/pdfbox/cos/COSName;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/common/PDDictionaryWrapper;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->N:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getNameAsString(Lorg/apache/pdfbox/cos/COSName;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getValue()Lorg/apache/pdfbox/cos/COSBase;
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/common/PDDictionaryWrapper;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->V:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    invoke-super {p0}, Lorg/apache/pdfbox/pdmodel/common/PDDictionaryWrapper;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDUserProperty;->userAttributeObject:Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDUserAttributeObject;

    if-nez v1, :cond_0

    const/4 v1, 0x0

    goto :goto_0

    :cond_0
    invoke-virtual {v1}, Lorg/apache/pdfbox/pdmodel/common/PDDictionaryWrapper;->hashCode()I

    move-result v1

    :goto_0
    add-int/2addr v0, v1

    return v0
.end method

.method public isHidden()Z
    .locals 3

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/common/PDDictionaryWrapper;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->H:Lorg/apache/pdfbox/cos/COSName;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->getBoolean(Lorg/apache/pdfbox/cos/COSName;Z)Z

    move-result v0

    return v0
.end method

.method public setFormattedValue(Ljava/lang/String;)V
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDUserProperty;->getFormattedValue()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDUserProperty;->potentiallyNotifyChanged(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/common/PDDictionaryWrapper;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->F:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setString(Lorg/apache/pdfbox/cos/COSName;Ljava/lang/String;)V

    return-void
.end method

.method public setHidden(Z)V
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDUserProperty;->isHidden()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDUserProperty;->potentiallyNotifyChanged(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/common/PDDictionaryWrapper;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->H:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setBoolean(Lorg/apache/pdfbox/cos/COSName;Z)V

    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDUserProperty;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDUserProperty;->potentiallyNotifyChanged(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/common/PDDictionaryWrapper;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->N:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setName(Lorg/apache/pdfbox/cos/COSName;Ljava/lang/String;)V

    return-void
.end method

.method public setValue(Lorg/apache/pdfbox/cos/COSBase;)V
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDUserProperty;->getValue()Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDUserProperty;->potentiallyNotifyChanged(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/common/PDDictionaryWrapper;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->V:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Name="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDUserProperty;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", Value="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDUserProperty;->getValue()Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", FormattedValue="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDUserProperty;->getFormattedValue()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", Hidden="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDUserProperty;->isHidden()Z

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.class public Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDLayoutAttributeObject;
.super Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;
.source "SourceFile"


# static fields
.field private static final BACKGROUND_COLOR:Ljava/lang/String; = "BackgroundColor"

.field private static final BASELINE_SHIFT:Ljava/lang/String; = "BaselineShift"

.field private static final BBOX:Ljava/lang/String; = "BBox"

.field private static final BLOCK_ALIGN:Ljava/lang/String; = "BlockAlign"

.field public static final BLOCK_ALIGN_AFTER:Ljava/lang/String; = "After"

.field public static final BLOCK_ALIGN_BEFORE:Ljava/lang/String; = "Before"

.field public static final BLOCK_ALIGN_JUSTIFY:Ljava/lang/String; = "Justify"

.field public static final BLOCK_ALIGN_MIDDLE:Ljava/lang/String; = "Middle"

.field private static final BORDER_COLOR:Ljava/lang/String; = "BorderColor"

.field private static final BORDER_STYLE:Ljava/lang/String; = "BorderStyle"

.field public static final BORDER_STYLE_DASHED:Ljava/lang/String; = "Dashed"

.field public static final BORDER_STYLE_DOTTED:Ljava/lang/String; = "Dotted"

.field public static final BORDER_STYLE_DOUBLE:Ljava/lang/String; = "Double"

.field public static final BORDER_STYLE_GROOVE:Ljava/lang/String; = "Groove"

.field public static final BORDER_STYLE_HIDDEN:Ljava/lang/String; = "Hidden"

.field public static final BORDER_STYLE_INSET:Ljava/lang/String; = "Inset"

.field public static final BORDER_STYLE_NONE:Ljava/lang/String; = "None"

.field public static final BORDER_STYLE_OUTSET:Ljava/lang/String; = "Outset"

.field public static final BORDER_STYLE_RIDGE:Ljava/lang/String; = "Ridge"

.field public static final BORDER_STYLE_SOLID:Ljava/lang/String; = "Solid"

.field private static final BORDER_THICKNESS:Ljava/lang/String; = "BorderThickness"

.field private static final COLOR:Ljava/lang/String; = "Color"

.field private static final COLUMN_COUNT:Ljava/lang/String; = "ColumnCount"

.field private static final COLUMN_GAP:Ljava/lang/String; = "ColumnGap"

.field private static final COLUMN_WIDTHS:Ljava/lang/String; = "ColumnWidths"

.field private static final END_INDENT:Ljava/lang/String; = "EndIndent"

.field private static final GLYPH_ORIENTATION_VERTICAL:Ljava/lang/String; = "GlyphOrientationVertical"

.field public static final GLYPH_ORIENTATION_VERTICAL_180_DEGREES:Ljava/lang/String; = "180"

.field public static final GLYPH_ORIENTATION_VERTICAL_270_DEGREES:Ljava/lang/String; = "270"

.field public static final GLYPH_ORIENTATION_VERTICAL_360_DEGREES:Ljava/lang/String; = "360"

.field public static final GLYPH_ORIENTATION_VERTICAL_90_DEGREES:Ljava/lang/String; = "90"

.field public static final GLYPH_ORIENTATION_VERTICAL_AUTO:Ljava/lang/String; = "Auto"

.field public static final GLYPH_ORIENTATION_VERTICAL_MINUS_180_DEGREES:Ljava/lang/String; = "-180"

.field public static final GLYPH_ORIENTATION_VERTICAL_MINUS_90_DEGREES:Ljava/lang/String; = "-90"

.field public static final GLYPH_ORIENTATION_VERTICAL_ZERO_DEGREES:Ljava/lang/String; = "0"

.field private static final HEIGHT:Ljava/lang/String; = "Height"

.field public static final HEIGHT_AUTO:Ljava/lang/String; = "Auto"

.field private static final INLINE_ALIGN:Ljava/lang/String; = "InlineAlign"

.field public static final INLINE_ALIGN_CENTER:Ljava/lang/String; = "Center"

.field public static final INLINE_ALIGN_END:Ljava/lang/String; = "End"

.field public static final INLINE_ALIGN_START:Ljava/lang/String; = "Start"

.field private static final LINE_HEIGHT:Ljava/lang/String; = "LineHeight"

.field public static final LINE_HEIGHT_AUTO:Ljava/lang/String; = "Auto"

.field public static final LINE_HEIGHT_NORMAL:Ljava/lang/String; = "Normal"

.field public static final OWNER_LAYOUT:Ljava/lang/String; = "Layout"

.field private static final PADDING:Ljava/lang/String; = "Padding"

.field private static final PLACEMENT:Ljava/lang/String; = "Placement"

.field public static final PLACEMENT_BEFORE:Ljava/lang/String; = "Before"

.field public static final PLACEMENT_BLOCK:Ljava/lang/String; = "Block"

.field public static final PLACEMENT_END:Ljava/lang/String; = "End"

.field public static final PLACEMENT_INLINE:Ljava/lang/String; = "Inline"

.field public static final PLACEMENT_START:Ljava/lang/String; = "Start"

.field private static final RUBY_ALIGN:Ljava/lang/String; = "RubyAlign"

.field public static final RUBY_ALIGN_CENTER:Ljava/lang/String; = "Center"

.field public static final RUBY_ALIGN_DISTRIBUTE:Ljava/lang/String; = "Distribute"

.field public static final RUBY_ALIGN_END:Ljava/lang/String; = "End"

.field public static final RUBY_ALIGN_JUSTIFY:Ljava/lang/String; = "Justify"

.field public static final RUBY_ALIGN_START:Ljava/lang/String; = "Start"

.field private static final RUBY_POSITION:Ljava/lang/String; = "RubyPosition"

.field public static final RUBY_POSITION_AFTER:Ljava/lang/String; = "After"

.field public static final RUBY_POSITION_BEFORE:Ljava/lang/String; = "Before"

.field public static final RUBY_POSITION_INLINE:Ljava/lang/String; = "Inline"

.field public static final RUBY_POSITION_WARICHU:Ljava/lang/String; = "Warichu"

.field private static final SPACE_AFTER:Ljava/lang/String; = "SpaceAfter"

.field private static final SPACE_BEFORE:Ljava/lang/String; = "SpaceBefore"

.field private static final START_INDENT:Ljava/lang/String; = "StartIndent"

.field private static final TEXT_ALIGN:Ljava/lang/String; = "TextAlign"

.field public static final TEXT_ALIGN_CENTER:Ljava/lang/String; = "Center"

.field public static final TEXT_ALIGN_END:Ljava/lang/String; = "End"

.field public static final TEXT_ALIGN_JUSTIFY:Ljava/lang/String; = "Justify"

.field public static final TEXT_ALIGN_START:Ljava/lang/String; = "Start"

.field private static final TEXT_DECORATION_COLOR:Ljava/lang/String; = "TextDecorationColor"

.field private static final TEXT_DECORATION_THICKNESS:Ljava/lang/String; = "TextDecorationThickness"

.field private static final TEXT_DECORATION_TYPE:Ljava/lang/String; = "TextDecorationType"

.field public static final TEXT_DECORATION_TYPE_LINE_THROUGH:Ljava/lang/String; = "LineThrough"

.field public static final TEXT_DECORATION_TYPE_NONE:Ljava/lang/String; = "None"

.field public static final TEXT_DECORATION_TYPE_OVERLINE:Ljava/lang/String; = "Overline"

.field public static final TEXT_DECORATION_TYPE_UNDERLINE:Ljava/lang/String; = "Underline"

.field private static final TEXT_INDENT:Ljava/lang/String; = "TextIndent"

.field private static final T_BORDER_STYLE:Ljava/lang/String; = "TBorderStyle"

.field private static final T_PADDING:Ljava/lang/String; = "TPadding"

.field private static final WIDTH:Ljava/lang/String; = "Width"

.field public static final WIDTH_AUTO:Ljava/lang/String; = "Auto"

.field private static final WRITING_MODE:Ljava/lang/String; = "WritingMode"

.field public static final WRITING_MODE_LRTB:Ljava/lang/String; = "LrTb"

.field public static final WRITING_MODE_RLTB:Ljava/lang/String; = "RlTb"

.field public static final WRITING_MODE_TBRL:Ljava/lang/String; = "TbRl"


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;-><init>()V

    const-string v0, "Layout"

    invoke-virtual {p0, v0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDAttributeObject;->setOwner(Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Lorg/apache/pdfbox/cos/COSDictionary;)V
    .locals 0

    .line 2
    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    return-void
.end method


# virtual methods
.method public getBBox()Lorg/apache/pdfbox/pdmodel/common/PDRectangle;
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/common/PDDictionaryWrapper;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    const-string v1, "BBox"

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Ljava/lang/String;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSArray;

    if-eqz v0, :cond_0

    new-instance v1, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;

    invoke-direct {v1, v0}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;-><init>(Lorg/apache/pdfbox/cos/COSArray;)V

    return-object v1

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public getBackgroundColor()Lorg/apache/pdfbox/pdmodel/graphics/color/PDGamma;
    .locals 1

    const-string v0, "BackgroundColor"

    invoke-virtual {p0, v0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->getColor(Ljava/lang/String;)Lorg/apache/pdfbox/pdmodel/graphics/color/PDGamma;

    move-result-object v0

    return-object v0
.end method

.method public getBaselineShift()F
    .locals 2

    const-string v0, "BaselineShift"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->getNumber(Ljava/lang/String;F)F

    move-result v0

    return v0
.end method

.method public getBlockAlign()Ljava/lang/String;
    .locals 2

    const-string v0, "BlockAlign"

    const-string v1, "Before"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->getName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getBorderColors()Ljava/lang/Object;
    .locals 1

    const-string v0, "BorderColor"

    invoke-virtual {p0, v0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->getColorOrFourColors(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getBorderStyle()Ljava/lang/Object;
    .locals 2

    const-string v0, "BorderStyle"

    const-string v1, "None"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->getNameOrArrayOfName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getBorderThickness()Ljava/lang/Object;
    .locals 2

    const-string v0, "BorderThickness"

    const/high16 v1, -0x40800000    # -1.0f

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->getNumberOrArrayOfNumber(Ljava/lang/String;F)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getColor()Lorg/apache/pdfbox/pdmodel/graphics/color/PDGamma;
    .locals 1

    const-string v0, "Color"

    invoke-virtual {p0, v0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->getColor(Ljava/lang/String;)Lorg/apache/pdfbox/pdmodel/graphics/color/PDGamma;

    move-result-object v0

    return-object v0
.end method

.method public getColumnCount()I
    .locals 2

    const-string v0, "ColumnCount"

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->getInteger(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public getColumnGap()Ljava/lang/Object;
    .locals 2

    const-string v0, "ColumnGap"

    const/high16 v1, -0x40800000    # -1.0f

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->getNumberOrArrayOfNumber(Ljava/lang/String;F)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getColumnWidths()Ljava/lang/Object;
    .locals 2

    const-string v0, "ColumnWidths"

    const/high16 v1, -0x40800000    # -1.0f

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->getNumberOrArrayOfNumber(Ljava/lang/String;F)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getEndIndent()F
    .locals 2

    const-string v0, "EndIndent"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->getNumber(Ljava/lang/String;F)F

    move-result v0

    return v0
.end method

.method public getGlyphOrientationVertical()Ljava/lang/String;
    .locals 2

    const-string v0, "GlyphOrientationVertical"

    const-string v1, "Auto"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->getName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getHeight()Ljava/lang/Object;
    .locals 2

    const-string v0, "Height"

    const-string v1, "Auto"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->getNumberOrName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getInlineAlign()Ljava/lang/String;
    .locals 2

    const-string v0, "InlineAlign"

    const-string v1, "Start"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->getName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getLineHeight()Ljava/lang/Object;
    .locals 2

    const-string v0, "LineHeight"

    const-string v1, "Normal"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->getNumberOrName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getPadding()Ljava/lang/Object;
    .locals 2

    const-string v0, "Padding"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->getNumberOrArrayOfNumber(Ljava/lang/String;F)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getPlacement()Ljava/lang/String;
    .locals 2

    const-string v0, "Placement"

    const-string v1, "Inline"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->getName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getRubyAlign()Ljava/lang/String;
    .locals 2

    const-string v0, "RubyAlign"

    const-string v1, "Distribute"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->getName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getRubyPosition()Ljava/lang/String;
    .locals 2

    const-string v0, "RubyPosition"

    const-string v1, "Before"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->getName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSpaceAfter()F
    .locals 2

    const-string v0, "SpaceAfter"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->getNumber(Ljava/lang/String;F)F

    move-result v0

    return v0
.end method

.method public getSpaceBefore()F
    .locals 2

    const-string v0, "SpaceBefore"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->getNumber(Ljava/lang/String;F)F

    move-result v0

    return v0
.end method

.method public getStartIndent()F
    .locals 2

    const-string v0, "StartIndent"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->getNumber(Ljava/lang/String;F)F

    move-result v0

    return v0
.end method

.method public getTBorderStyle()Ljava/lang/Object;
    .locals 2

    const-string v0, "TBorderStyle"

    const-string v1, "None"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->getNameOrArrayOfName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getTPadding()Ljava/lang/Object;
    .locals 2

    const-string v0, "TPadding"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->getNumberOrArrayOfNumber(Ljava/lang/String;F)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getTextAlign()Ljava/lang/String;
    .locals 2

    const-string v0, "TextAlign"

    const-string v1, "Start"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->getName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTextDecorationColor()Lorg/apache/pdfbox/pdmodel/graphics/color/PDGamma;
    .locals 1

    const-string v0, "TextDecorationColor"

    invoke-virtual {p0, v0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->getColor(Ljava/lang/String;)Lorg/apache/pdfbox/pdmodel/graphics/color/PDGamma;

    move-result-object v0

    return-object v0
.end method

.method public getTextDecorationThickness()F
    .locals 1

    const-string v0, "TextDecorationThickness"

    invoke-virtual {p0, v0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->getNumber(Ljava/lang/String;)F

    move-result v0

    return v0
.end method

.method public getTextDecorationType()Ljava/lang/String;
    .locals 2

    const-string v0, "TextDecorationType"

    const-string v1, "None"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->getName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTextIndent()F
    .locals 2

    const-string v0, "TextIndent"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->getNumber(Ljava/lang/String;F)F

    move-result v0

    return v0
.end method

.method public getWidth()Ljava/lang/Object;
    .locals 2

    const-string v0, "Width"

    const-string v1, "Auto"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->getNumberOrName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getWritingMode()Ljava/lang/String;
    .locals 2

    const-string v0, "WritingMode"

    const-string v1, "LrTb"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->getName(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public setAllBorderColors(Lorg/apache/pdfbox/pdmodel/graphics/color/PDGamma;)V
    .locals 1

    const-string v0, "BorderColor"

    invoke-virtual {p0, v0, p1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->setColor(Ljava/lang/String;Lorg/apache/pdfbox/pdmodel/graphics/color/PDGamma;)V

    return-void
.end method

.method public setAllBorderStyles(Ljava/lang/String;)V
    .locals 1

    const-string v0, "BorderStyle"

    invoke-virtual {p0, v0, p1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->setName(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public setAllBorderThicknesses(F)V
    .locals 1

    .line 1
    const-string v0, "BorderThickness"

    invoke-virtual {p0, v0, p1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->setNumber(Ljava/lang/String;F)V

    return-void
.end method

.method public setAllBorderThicknesses(I)V
    .locals 1

    .line 2
    const-string v0, "BorderThickness"

    invoke-virtual {p0, v0, p1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->setNumber(Ljava/lang/String;I)V

    return-void
.end method

.method public setAllColumnWidths(F)V
    .locals 1

    .line 1
    const-string v0, "ColumnWidths"

    invoke-virtual {p0, v0, p1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->setNumber(Ljava/lang/String;F)V

    return-void
.end method

.method public setAllColumnWidths(I)V
    .locals 1

    .line 2
    const-string v0, "ColumnWidths"

    invoke-virtual {p0, v0, p1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->setNumber(Ljava/lang/String;I)V

    return-void
.end method

.method public setAllPaddings(F)V
    .locals 1

    .line 1
    const-string v0, "Padding"

    invoke-virtual {p0, v0, p1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->setNumber(Ljava/lang/String;F)V

    return-void
.end method

.method public setAllPaddings(I)V
    .locals 1

    .line 2
    const-string v0, "Padding"

    invoke-virtual {p0, v0, p1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->setNumber(Ljava/lang/String;I)V

    return-void
.end method

.method public setAllTBorderStyles(Ljava/lang/String;)V
    .locals 1

    const-string v0, "TBorderStyle"

    invoke-virtual {p0, v0, p1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->setName(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public setAllTPaddings(F)V
    .locals 1

    .line 1
    const-string v0, "TPadding"

    invoke-virtual {p0, v0, p1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->setNumber(Ljava/lang/String;F)V

    return-void
.end method

.method public setAllTPaddings(I)V
    .locals 1

    .line 2
    const-string v0, "TPadding"

    invoke-virtual {p0, v0, p1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->setNumber(Ljava/lang/String;I)V

    return-void
.end method

.method public setBBox(Lorg/apache/pdfbox/pdmodel/common/PDRectangle;)V
    .locals 3

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/common/PDDictionaryWrapper;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    const-string v1, "BBox"

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Ljava/lang/String;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/common/PDDictionaryWrapper;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v2

    invoke-virtual {v2, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Ljava/lang/String;Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)V

    if-nez p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->getCOSObject()Lorg/apache/pdfbox/cos/COSBase;

    move-result-object p1

    :goto_0
    invoke-virtual {p0, v0, p1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDAttributeObject;->potentiallyNotifyChanged(Lorg/apache/pdfbox/cos/COSBase;Lorg/apache/pdfbox/cos/COSBase;)V

    return-void
.end method

.method public setBackgroundColor(Lorg/apache/pdfbox/pdmodel/graphics/color/PDGamma;)V
    .locals 1

    const-string v0, "BackgroundColor"

    invoke-virtual {p0, v0, p1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->setColor(Ljava/lang/String;Lorg/apache/pdfbox/pdmodel/graphics/color/PDGamma;)V

    return-void
.end method

.method public setBaselineShift(F)V
    .locals 1

    .line 1
    const-string v0, "BaselineShift"

    invoke-virtual {p0, v0, p1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->setNumber(Ljava/lang/String;F)V

    return-void
.end method

.method public setBaselineShift(I)V
    .locals 1

    .line 2
    const-string v0, "BaselineShift"

    invoke-virtual {p0, v0, p1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->setNumber(Ljava/lang/String;I)V

    return-void
.end method

.method public setBlockAlign(Ljava/lang/String;)V
    .locals 1

    const-string v0, "BlockAlign"

    invoke-virtual {p0, v0, p1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->setName(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public setBorderColors(Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDFourColours;)V
    .locals 1

    const-string v0, "BorderColor"

    invoke-virtual {p0, v0, p1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->setFourColors(Ljava/lang/String;Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDFourColours;)V

    return-void
.end method

.method public setBorderStyles([Ljava/lang/String;)V
    .locals 1

    const-string v0, "BorderStyle"

    invoke-virtual {p0, v0, p1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->setArrayOfName(Ljava/lang/String;[Ljava/lang/String;)V

    return-void
.end method

.method public setBorderThicknesses([F)V
    .locals 1

    const-string v0, "BorderThickness"

    invoke-virtual {p0, v0, p1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->setArrayOfNumber(Ljava/lang/String;[F)V

    return-void
.end method

.method public setColor(Lorg/apache/pdfbox/pdmodel/graphics/color/PDGamma;)V
    .locals 1

    const-string v0, "Color"

    invoke-virtual {p0, v0, p1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->setColor(Ljava/lang/String;Lorg/apache/pdfbox/pdmodel/graphics/color/PDGamma;)V

    return-void
.end method

.method public setColumnCount(I)V
    .locals 1

    const-string v0, "ColumnCount"

    invoke-virtual {p0, v0, p1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->setInteger(Ljava/lang/String;I)V

    return-void
.end method

.method public setColumnGap(F)V
    .locals 1

    .line 1
    const-string v0, "ColumnGap"

    invoke-virtual {p0, v0, p1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->setNumber(Ljava/lang/String;F)V

    return-void
.end method

.method public setColumnGap(I)V
    .locals 1

    .line 2
    const-string v0, "ColumnGap"

    invoke-virtual {p0, v0, p1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->setNumber(Ljava/lang/String;I)V

    return-void
.end method

.method public setColumnGaps([F)V
    .locals 1

    const-string v0, "ColumnGap"

    invoke-virtual {p0, v0, p1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->setArrayOfNumber(Ljava/lang/String;[F)V

    return-void
.end method

.method public setColumnWidths([F)V
    .locals 1

    const-string v0, "ColumnWidths"

    invoke-virtual {p0, v0, p1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->setArrayOfNumber(Ljava/lang/String;[F)V

    return-void
.end method

.method public setEndIndent(F)V
    .locals 1

    .line 1
    const-string v0, "EndIndent"

    invoke-virtual {p0, v0, p1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->setNumber(Ljava/lang/String;F)V

    return-void
.end method

.method public setEndIndent(I)V
    .locals 1

    .line 2
    const-string v0, "EndIndent"

    invoke-virtual {p0, v0, p1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->setNumber(Ljava/lang/String;I)V

    return-void
.end method

.method public setGlyphOrientationVertical(Ljava/lang/String;)V
    .locals 1

    const-string v0, "GlyphOrientationVertical"

    invoke-virtual {p0, v0, p1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->setName(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public setHeight(F)V
    .locals 1

    .line 1
    const-string v0, "Height"

    invoke-virtual {p0, v0, p1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->setNumber(Ljava/lang/String;F)V

    return-void
.end method

.method public setHeight(I)V
    .locals 1

    .line 2
    const-string v0, "Height"

    invoke-virtual {p0, v0, p1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->setNumber(Ljava/lang/String;I)V

    return-void
.end method

.method public setHeightAuto()V
    .locals 2

    const-string v0, "Height"

    const-string v1, "Auto"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->setName(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public setInlineAlign(Ljava/lang/String;)V
    .locals 1

    const-string v0, "InlineAlign"

    invoke-virtual {p0, v0, p1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->setName(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public setLineHeight(F)V
    .locals 1

    .line 1
    const-string v0, "LineHeight"

    invoke-virtual {p0, v0, p1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->setNumber(Ljava/lang/String;F)V

    return-void
.end method

.method public setLineHeight(I)V
    .locals 1

    .line 2
    const-string v0, "LineHeight"

    invoke-virtual {p0, v0, p1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->setNumber(Ljava/lang/String;I)V

    return-void
.end method

.method public setLineHeightAuto()V
    .locals 2

    const-string v0, "LineHeight"

    const-string v1, "Auto"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->setName(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public setLineHeightNormal()V
    .locals 2

    const-string v0, "LineHeight"

    const-string v1, "Normal"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->setName(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public setPaddings([F)V
    .locals 1

    const-string v0, "Padding"

    invoke-virtual {p0, v0, p1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->setArrayOfNumber(Ljava/lang/String;[F)V

    return-void
.end method

.method public setPlacement(Ljava/lang/String;)V
    .locals 1

    const-string v0, "Placement"

    invoke-virtual {p0, v0, p1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->setName(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public setRubyAlign(Ljava/lang/String;)V
    .locals 1

    const-string v0, "RubyAlign"

    invoke-virtual {p0, v0, p1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->setName(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public setRubyPosition(Ljava/lang/String;)V
    .locals 1

    const-string v0, "RubyPosition"

    invoke-virtual {p0, v0, p1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->setName(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public setSpaceAfter(F)V
    .locals 1

    .line 1
    const-string v0, "SpaceAfter"

    invoke-virtual {p0, v0, p1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->setNumber(Ljava/lang/String;F)V

    return-void
.end method

.method public setSpaceAfter(I)V
    .locals 1

    .line 2
    const-string v0, "SpaceAfter"

    invoke-virtual {p0, v0, p1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->setNumber(Ljava/lang/String;I)V

    return-void
.end method

.method public setSpaceBefore(F)V
    .locals 1

    .line 1
    const-string v0, "SpaceBefore"

    invoke-virtual {p0, v0, p1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->setNumber(Ljava/lang/String;F)V

    return-void
.end method

.method public setSpaceBefore(I)V
    .locals 1

    .line 2
    const-string v0, "SpaceBefore"

    invoke-virtual {p0, v0, p1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->setNumber(Ljava/lang/String;I)V

    return-void
.end method

.method public setStartIndent(F)V
    .locals 1

    .line 1
    const-string v0, "StartIndent"

    invoke-virtual {p0, v0, p1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->setNumber(Ljava/lang/String;F)V

    return-void
.end method

.method public setStartIndent(I)V
    .locals 1

    .line 2
    const-string v0, "StartIndent"

    invoke-virtual {p0, v0, p1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->setNumber(Ljava/lang/String;I)V

    return-void
.end method

.method public setTBorderStyles([Ljava/lang/String;)V
    .locals 1

    const-string v0, "TBorderStyle"

    invoke-virtual {p0, v0, p1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->setArrayOfName(Ljava/lang/String;[Ljava/lang/String;)V

    return-void
.end method

.method public setTPaddings([F)V
    .locals 1

    const-string v0, "TPadding"

    invoke-virtual {p0, v0, p1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->setArrayOfNumber(Ljava/lang/String;[F)V

    return-void
.end method

.method public setTextAlign(Ljava/lang/String;)V
    .locals 1

    const-string v0, "TextAlign"

    invoke-virtual {p0, v0, p1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->setName(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public setTextDecorationColor(Lorg/apache/pdfbox/pdmodel/graphics/color/PDGamma;)V
    .locals 1

    const-string v0, "TextDecorationColor"

    invoke-virtual {p0, v0, p1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->setColor(Ljava/lang/String;Lorg/apache/pdfbox/pdmodel/graphics/color/PDGamma;)V

    return-void
.end method

.method public setTextDecorationThickness(F)V
    .locals 1

    .line 1
    const-string v0, "TextDecorationThickness"

    invoke-virtual {p0, v0, p1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->setNumber(Ljava/lang/String;F)V

    return-void
.end method

.method public setTextDecorationThickness(I)V
    .locals 1

    .line 2
    const-string v0, "TextDecorationThickness"

    invoke-virtual {p0, v0, p1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->setNumber(Ljava/lang/String;I)V

    return-void
.end method

.method public setTextDecorationType(Ljava/lang/String;)V
    .locals 1

    const-string v0, "TextDecorationType"

    invoke-virtual {p0, v0, p1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->setName(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public setTextIndent(F)V
    .locals 1

    .line 1
    const-string v0, "TextIndent"

    invoke-virtual {p0, v0, p1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->setNumber(Ljava/lang/String;F)V

    return-void
.end method

.method public setTextIndent(I)V
    .locals 1

    .line 2
    const-string v0, "TextIndent"

    invoke-virtual {p0, v0, p1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->setNumber(Ljava/lang/String;I)V

    return-void
.end method

.method public setWidth(F)V
    .locals 1

    .line 1
    const-string v0, "Width"

    invoke-virtual {p0, v0, p1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->setNumber(Ljava/lang/String;F)V

    return-void
.end method

.method public setWidth(I)V
    .locals 1

    .line 2
    const-string v0, "Width"

    invoke-virtual {p0, v0, p1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->setNumber(Ljava/lang/String;I)V

    return-void
.end method

.method public setWidthAuto()V
    .locals 2

    const-string v0, "Width"

    const-string v1, "Auto"

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->setName(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public setWritingMode(Ljava/lang/String;)V
    .locals 1

    const-string v0, "WritingMode"

    invoke-virtual {p0, v0, p1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->setName(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-super {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDAttributeObject;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "Placement"

    invoke-virtual {p0, v1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->isSpecified(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, ", Placement="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDLayoutAttributeObject;->getPlacement()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    const-string v1, "WritingMode"

    invoke-virtual {p0, v1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->isSpecified(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, ", WritingMode="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDLayoutAttributeObject;->getWritingMode()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    const-string v1, "BackgroundColor"

    invoke-virtual {p0, v1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->isSpecified(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, ", BackgroundColor="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDLayoutAttributeObject;->getBackgroundColor()Lorg/apache/pdfbox/pdmodel/graphics/color/PDGamma;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_2
    const-string v1, "BorderColor"

    invoke-virtual {p0, v1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->isSpecified(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    const-string v1, ", BorderColor="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDLayoutAttributeObject;->getBorderColors()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_3
    const-string v1, "BorderStyle"

    invoke-virtual {p0, v1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->isSpecified(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDLayoutAttributeObject;->getBorderStyle()Ljava/lang/Object;

    move-result-object v1

    const-string v2, ", BorderStyle="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    instance-of v2, v1, [Ljava/lang/String;

    if-eqz v2, :cond_4

    check-cast v1, [Ljava/lang/String;

    invoke-static {v1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDAttributeObject;->arrayToString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_4
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_5
    :goto_0
    const-string v1, "BorderThickness"

    invoke-virtual {p0, v1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->isSpecified(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDLayoutAttributeObject;->getBorderThickness()Ljava/lang/Object;

    move-result-object v1

    const-string v2, ", BorderThickness="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    instance-of v2, v1, [F

    if-eqz v2, :cond_6

    check-cast v1, [F

    invoke-static {v1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDAttributeObject;->arrayToString([F)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    :cond_6
    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    :goto_1
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_7
    const-string v1, "Padding"

    invoke-virtual {p0, v1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->isSpecified(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_9

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDLayoutAttributeObject;->getPadding()Ljava/lang/Object;

    move-result-object v1

    const-string v2, ", Padding="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    instance-of v2, v1, [F

    if-eqz v2, :cond_8

    check-cast v1, [F

    invoke-static {v1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDAttributeObject;->arrayToString([F)Ljava/lang/String;

    move-result-object v1

    goto :goto_2

    :cond_8
    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    :goto_2
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_9
    const-string v1, "Color"

    invoke-virtual {p0, v1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->isSpecified(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_a

    const-string v1, ", Color="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDLayoutAttributeObject;->getColor()Lorg/apache/pdfbox/pdmodel/graphics/color/PDGamma;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_a
    const-string v1, "SpaceBefore"

    invoke-virtual {p0, v1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->isSpecified(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_b

    const-string v1, ", SpaceBefore="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDLayoutAttributeObject;->getSpaceBefore()F

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_b
    const-string v1, "SpaceAfter"

    invoke-virtual {p0, v1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->isSpecified(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_c

    const-string v1, ", SpaceAfter="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDLayoutAttributeObject;->getSpaceAfter()F

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_c
    const-string v1, "StartIndent"

    invoke-virtual {p0, v1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->isSpecified(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_d

    const-string v1, ", StartIndent="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDLayoutAttributeObject;->getStartIndent()F

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_d
    const-string v1, "EndIndent"

    invoke-virtual {p0, v1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->isSpecified(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_e

    const-string v1, ", EndIndent="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDLayoutAttributeObject;->getEndIndent()F

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_e
    const-string v1, "TextIndent"

    invoke-virtual {p0, v1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->isSpecified(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_f

    const-string v1, ", TextIndent="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDLayoutAttributeObject;->getTextIndent()F

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_f
    const-string v1, "TextAlign"

    invoke-virtual {p0, v1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->isSpecified(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_10

    const-string v1, ", TextAlign="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDLayoutAttributeObject;->getTextAlign()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_10
    const-string v1, "BBox"

    invoke-virtual {p0, v1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->isSpecified(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_11

    const-string v1, ", BBox="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDLayoutAttributeObject;->getBBox()Lorg/apache/pdfbox/pdmodel/common/PDRectangle;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_11
    const-string v1, "Width"

    invoke-virtual {p0, v1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->isSpecified(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_13

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDLayoutAttributeObject;->getWidth()Ljava/lang/Object;

    move-result-object v1

    const-string v2, ", Width="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    instance-of v2, v1, Ljava/lang/Float;

    if-eqz v2, :cond_12

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_3

    :cond_12
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_13
    :goto_3
    const-string v1, "Height"

    invoke-virtual {p0, v1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->isSpecified(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_15

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDLayoutAttributeObject;->getHeight()Ljava/lang/Object;

    move-result-object v1

    const-string v2, ", Height="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    instance-of v2, v1, Ljava/lang/Float;

    if-eqz v2, :cond_14

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_4

    :cond_14
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_15
    :goto_4
    const-string v1, "BlockAlign"

    invoke-virtual {p0, v1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->isSpecified(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_16

    const-string v1, ", BlockAlign="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDLayoutAttributeObject;->getBlockAlign()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_16
    const-string v1, "InlineAlign"

    invoke-virtual {p0, v1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->isSpecified(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_17

    const-string v1, ", InlineAlign="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDLayoutAttributeObject;->getInlineAlign()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_17
    const-string v1, "TBorderStyle"

    invoke-virtual {p0, v1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->isSpecified(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_19

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDLayoutAttributeObject;->getTBorderStyle()Ljava/lang/Object;

    move-result-object v1

    const-string v2, ", TBorderStyle="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    instance-of v2, v1, [Ljava/lang/String;

    if-eqz v2, :cond_18

    check-cast v1, [Ljava/lang/String;

    invoke-static {v1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDAttributeObject;->arrayToString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_5

    :cond_18
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_19
    :goto_5
    const-string v1, "TPadding"

    invoke-virtual {p0, v1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->isSpecified(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1b

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDLayoutAttributeObject;->getTPadding()Ljava/lang/Object;

    move-result-object v1

    const-string v2, ", TPadding="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    instance-of v2, v1, [F

    if-eqz v2, :cond_1a

    check-cast v1, [F

    invoke-static {v1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDAttributeObject;->arrayToString([F)Ljava/lang/String;

    move-result-object v1

    goto :goto_6

    :cond_1a
    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    :goto_6
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1b
    const-string v1, "BaselineShift"

    invoke-virtual {p0, v1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->isSpecified(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1c

    const-string v1, ", BaselineShift="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDLayoutAttributeObject;->getBaselineShift()F

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1c
    const-string v1, "LineHeight"

    invoke-virtual {p0, v1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->isSpecified(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1e

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDLayoutAttributeObject;->getLineHeight()Ljava/lang/Object;

    move-result-object v1

    const-string v2, ", LineHeight="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    instance-of v2, v1, Ljava/lang/Float;

    if-eqz v2, :cond_1d

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_7

    :cond_1d
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_1e
    :goto_7
    const-string v1, "TextDecorationColor"

    invoke-virtual {p0, v1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->isSpecified(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1f

    const-string v1, ", TextDecorationColor="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDLayoutAttributeObject;->getTextDecorationColor()Lorg/apache/pdfbox/pdmodel/graphics/color/PDGamma;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_1f
    const-string v1, "TextDecorationThickness"

    invoke-virtual {p0, v1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->isSpecified(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_20

    const-string v1, ", TextDecorationThickness="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDLayoutAttributeObject;->getTextDecorationThickness()F

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_20
    const-string v1, "TextDecorationType"

    invoke-virtual {p0, v1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->isSpecified(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_21

    const-string v1, ", TextDecorationType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDLayoutAttributeObject;->getTextDecorationType()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_21
    const-string v1, "RubyAlign"

    invoke-virtual {p0, v1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->isSpecified(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_22

    const-string v1, ", RubyAlign="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDLayoutAttributeObject;->getRubyAlign()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_22
    const-string v1, "RubyPosition"

    invoke-virtual {p0, v1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->isSpecified(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_23

    const-string v1, ", RubyPosition="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDLayoutAttributeObject;->getRubyPosition()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_23
    const-string v1, "GlyphOrientationVertical"

    invoke-virtual {p0, v1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->isSpecified(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_24

    const-string v1, ", GlyphOrientationVertical="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDLayoutAttributeObject;->getGlyphOrientationVertical()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_24
    const-string v1, "ColumnCount"

    invoke-virtual {p0, v1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->isSpecified(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_25

    const-string v1, ", ColumnCount="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDLayoutAttributeObject;->getColumnCount()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_25
    const-string v1, "ColumnGap"

    invoke-virtual {p0, v1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->isSpecified(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_27

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDLayoutAttributeObject;->getColumnGap()Ljava/lang/Object;

    move-result-object v1

    const-string v2, ", ColumnGap="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    instance-of v2, v1, [F

    if-eqz v2, :cond_26

    check-cast v1, [F

    invoke-static {v1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDAttributeObject;->arrayToString([F)Ljava/lang/String;

    move-result-object v1

    goto :goto_8

    :cond_26
    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    :goto_8
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_27
    const-string v1, "ColumnWidths"

    invoke-virtual {p0, v1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->isSpecified(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_29

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDLayoutAttributeObject;->getColumnWidths()Ljava/lang/Object;

    move-result-object v1

    const-string v2, ", ColumnWidths="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    instance-of v2, v1, [F

    if-eqz v2, :cond_28

    check-cast v1, [F

    invoke-static {v1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDAttributeObject;->arrayToString([F)Ljava/lang/String;

    move-result-object v1

    goto :goto_9

    :cond_28
    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    :goto_9
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_29
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

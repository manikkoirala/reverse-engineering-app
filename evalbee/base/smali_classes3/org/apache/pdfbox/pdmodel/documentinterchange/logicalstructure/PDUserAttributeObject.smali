.class public Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDUserAttributeObject;
.super Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDAttributeObject;
.source "SourceFile"


# static fields
.field public static final OWNER_USER_PROPERTIES:Ljava/lang/String; = "UserProperties"


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDAttributeObject;-><init>()V

    const-string v0, "UserProperties"

    invoke-virtual {p0, v0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDAttributeObject;->setOwner(Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Lorg/apache/pdfbox/cos/COSDictionary;)V
    .locals 0

    .line 2
    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDAttributeObject;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    return-void
.end method


# virtual methods
.method public addUserProperty(Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDUserProperty;)V
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/common/PDDictionaryWrapper;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->P:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSArray;

    invoke-virtual {v0, p1}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)V

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDAttributeObject;->notifyChanged()V

    return-void
.end method

.method public getOwnerUserProperties()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDUserProperty;",
            ">;"
        }
    .end annotation

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/common/PDDictionaryWrapper;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->P:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSArray;

    new-instance v1, Ljava/util/ArrayList;

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSArray;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v2, 0x0

    :goto_0
    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSArray;->size()I

    move-result v3

    if-ge v2, v3, :cond_0

    new-instance v3, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDUserProperty;

    invoke-virtual {v0, v2}, Lorg/apache/pdfbox/cos/COSArray;->getObject(I)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v4

    check-cast v4, Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-direct {v3, v4, p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDUserProperty;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDUserAttributeObject;)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    return-object v1
.end method

.method public removeUserProperty(Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDUserProperty;)V
    .locals 2

    if-nez p1, :cond_0

    return-void

    :cond_0
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/common/PDDictionaryWrapper;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->P:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSArray;

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/common/PDDictionaryWrapper;->getCOSObject()Lorg/apache/pdfbox/cos/COSBase;

    move-result-object p1

    invoke-virtual {v0, p1}, Lorg/apache/pdfbox/cos/COSArray;->remove(Lorg/apache/pdfbox/cos/COSBase;)Z

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDAttributeObject;->notifyChanged()V

    return-void
.end method

.method public setUserProperties(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDUserProperty;",
            ">;)V"
        }
    .end annotation

    new-instance v0, Lorg/apache/pdfbox/cos/COSArray;

    invoke-direct {v0}, Lorg/apache/pdfbox/cos/COSArray;-><init>()V

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDUserProperty;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)V

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/common/PDDictionaryWrapper;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object p1

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->P:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p1, v1, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-super {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDAttributeObject;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ", userProperties="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDUserAttributeObject;->getOwnerUserProperties()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public userPropertyChanged(Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDUserProperty;)V
    .locals 0

    return-void
.end method

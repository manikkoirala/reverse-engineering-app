.class public Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureElement;
.super Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureNode;
.source "SourceFile"


# static fields
.field public static final TYPE:Ljava/lang/String; = "StructElem"


# direct methods
.method public constructor <init>(Ljava/lang/String;Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureNode;)V
    .locals 1

    .line 1
    const-string v0, "StructElem"

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureNode;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureElement;->setStructureType(Ljava/lang/String;)V

    invoke-virtual {p0, p2}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureElement;->setParent(Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureNode;)V

    return-void
.end method

.method public constructor <init>(Lorg/apache/pdfbox/cos/COSDictionary;)V
    .locals 0

    .line 2
    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureNode;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    return-void
.end method

.method private getRoleMap()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureElement;->getStructureTreeRoot()Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureTreeRoot;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureTreeRoot;->getRoleMap()Ljava/util/Map;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method private getStructureTreeRoot()Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureTreeRoot;
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureElement;->getParent()Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureNode;

    move-result-object v0

    :goto_0
    instance-of v1, v0, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureElement;

    if-eqz v1, :cond_0

    check-cast v0, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureElement;

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureElement;->getParent()Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureNode;

    move-result-object v0

    goto :goto_0

    :cond_0
    instance-of v1, v0, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureTreeRoot;

    if-eqz v1, :cond_1

    check-cast v0, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureTreeRoot;

    return-object v0

    :cond_1
    const/4 v0, 0x0

    return-object v0
.end method


# virtual methods
.method public addAttribute(Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDAttributeObject;)V
    .locals 5

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->A:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p1, p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDAttributeObject;->setStructureElement(Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureElement;)V

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureNode;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v1

    invoke-virtual {v1, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v1

    instance-of v2, v1, Lorg/apache/pdfbox/cos/COSArray;

    if-eqz v2, :cond_0

    check-cast v1, Lorg/apache/pdfbox/cos/COSArray;

    goto :goto_0

    :cond_0
    new-instance v2, Lorg/apache/pdfbox/cos/COSArray;

    invoke-direct {v2}, Lorg/apache/pdfbox/cos/COSArray;-><init>()V

    if-eqz v1, :cond_1

    invoke-virtual {v2, v1}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    const-wide/16 v3, 0x0

    invoke-static {v3, v4}, Lorg/apache/pdfbox/cos/COSInteger;->get(J)Lorg/apache/pdfbox/cos/COSInteger;

    move-result-object v1

    invoke-virtual {v2, v1}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    :cond_1
    move-object v1, v2

    :goto_0
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureNode;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    invoke-virtual {v1, p1}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)V

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureElement;->getRevisionNumber()I

    move-result p1

    int-to-long v2, p1

    invoke-static {v2, v3}, Lorg/apache/pdfbox/cos/COSInteger;->get(J)Lorg/apache/pdfbox/cos/COSInteger;

    move-result-object p1

    invoke-virtual {v1, p1}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    return-void
.end method

.method public addClassName(Ljava/lang/String;)V
    .locals 5

    if-nez p1, :cond_0

    return-void

    :cond_0
    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->C:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureNode;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v1

    invoke-virtual {v1, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v1

    instance-of v2, v1, Lorg/apache/pdfbox/cos/COSArray;

    if-eqz v2, :cond_1

    check-cast v1, Lorg/apache/pdfbox/cos/COSArray;

    goto :goto_0

    :cond_1
    new-instance v2, Lorg/apache/pdfbox/cos/COSArray;

    invoke-direct {v2}, Lorg/apache/pdfbox/cos/COSArray;-><init>()V

    if-eqz v1, :cond_2

    invoke-virtual {v2, v1}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    const-wide/16 v3, 0x0

    invoke-static {v3, v4}, Lorg/apache/pdfbox/cos/COSInteger;->get(J)Lorg/apache/pdfbox/cos/COSInteger;

    move-result-object v1

    invoke-virtual {v2, v1}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    :cond_2
    move-object v1, v2

    :goto_0
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureNode;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    invoke-static {p1}, Lorg/apache/pdfbox/cos/COSName;->getPDFName(Ljava/lang/String;)Lorg/apache/pdfbox/cos/COSName;

    move-result-object p1

    invoke-virtual {v1, p1}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureElement;->getRevisionNumber()I

    move-result p1

    int-to-long v2, p1

    invoke-static {v2, v3}, Lorg/apache/pdfbox/cos/COSInteger;->get(J)Lorg/apache/pdfbox/cos/COSInteger;

    move-result-object p1

    invoke-virtual {v1, p1}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    return-void
.end method

.method public appendKid(Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDMarkedContentReference;)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureNode;->appendObjectableKid(Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)V

    return-void
.end method

.method public appendKid(Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDObjectReference;)V
    .locals 0

    .line 2
    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureNode;->appendObjectableKid(Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)V

    return-void
.end method

.method public appendKid(Lorg/apache/pdfbox/pdmodel/documentinterchange/markedcontent/PDMarkedContent;)V
    .locals 2

    .line 3
    if-nez p1, :cond_0

    return-void

    :cond_0
    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/markedcontent/PDMarkedContent;->getMCID()I

    move-result p1

    int-to-long v0, p1

    invoke-static {v0, v1}, Lorg/apache/pdfbox/cos/COSInteger;->get(J)Lorg/apache/pdfbox/cos/COSInteger;

    move-result-object p1

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureNode;->appendKid(Lorg/apache/pdfbox/cos/COSBase;)V

    return-void
.end method

.method public attributeChanged(Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDAttributeObject;)V
    .locals 5

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->A:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureNode;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v1

    invoke-virtual {v1, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v1

    instance-of v2, v1, Lorg/apache/pdfbox/cos/COSArray;

    if-eqz v2, :cond_1

    check-cast v1, Lorg/apache/pdfbox/cos/COSArray;

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1}, Lorg/apache/pdfbox/cos/COSArray;->size()I

    move-result v2

    if-ge v0, v2, :cond_2

    invoke-virtual {v1, v0}, Lorg/apache/pdfbox/cos/COSArray;->getObject(I)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v2

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/common/PDDictionaryWrapper;->getCOSObject()Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    add-int/lit8 v2, v0, 0x1

    invoke-virtual {v1, v2}, Lorg/apache/pdfbox/cos/COSArray;->get(I)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v3

    instance-of v3, v3, Lorg/apache/pdfbox/cos/COSInteger;

    if-eqz v3, :cond_0

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureElement;->getRevisionNumber()I

    move-result v3

    int-to-long v3, v3

    invoke-static {v3, v4}, Lorg/apache/pdfbox/cos/COSInteger;->get(J)Lorg/apache/pdfbox/cos/COSInteger;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lorg/apache/pdfbox/cos/COSArray;->set(ILorg/apache/pdfbox/cos/COSBase;)V

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    new-instance p1, Lorg/apache/pdfbox/cos/COSArray;

    invoke-direct {p1}, Lorg/apache/pdfbox/cos/COSArray;-><init>()V

    invoke-virtual {p1, v1}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureElement;->getRevisionNumber()I

    move-result v1

    int-to-long v1, v1

    invoke-static {v1, v2}, Lorg/apache/pdfbox/cos/COSInteger;->get(J)Lorg/apache/pdfbox/cos/COSInteger;

    move-result-object v1

    invoke-virtual {p1, v1}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureNode;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v1

    invoke-virtual {v1, v0, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    :cond_2
    return-void
.end method

.method public getActualText()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureNode;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->ACTUAL_TEXT:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getString(Lorg/apache/pdfbox/cos/COSName;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getAlternateDescription()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureNode;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->ALT:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getString(Lorg/apache/pdfbox/cos/COSName;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getAttributes()Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/Revisions;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/Revisions<",
            "Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDAttributeObject;",
            ">;"
        }
    .end annotation

    new-instance v0, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/Revisions;

    invoke-direct {v0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/Revisions;-><init>()V

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureNode;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v1

    sget-object v2, Lorg/apache/pdfbox/cos/COSName;->A:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v1

    instance-of v2, v1, Lorg/apache/pdfbox/cos/COSArray;

    const/4 v3, 0x0

    if-eqz v2, :cond_2

    move-object v2, v1

    check-cast v2, Lorg/apache/pdfbox/cos/COSArray;

    invoke-virtual {v2}, Lorg/apache/pdfbox/cos/COSArray;->iterator()Ljava/util/Iterator;

    move-result-object v2

    const/4 v4, 0x0

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/apache/pdfbox/cos/COSBase;

    instance-of v6, v5, Lorg/apache/pdfbox/cos/COSDictionary;

    if-eqz v6, :cond_1

    check-cast v5, Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-static {v5}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDAttributeObject;->create(Lorg/apache/pdfbox/cos/COSDictionary;)Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDAttributeObject;

    move-result-object v4

    invoke-virtual {v4, p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDAttributeObject;->setStructureElement(Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureElement;)V

    invoke-virtual {v0, v4, v3}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/Revisions;->addObject(Ljava/lang/Object;I)V

    goto :goto_0

    :cond_1
    instance-of v6, v5, Lorg/apache/pdfbox/cos/COSInteger;

    if-eqz v6, :cond_0

    check-cast v5, Lorg/apache/pdfbox/cos/COSInteger;

    invoke-virtual {v5}, Lorg/apache/pdfbox/cos/COSInteger;->intValue()I

    move-result v5

    invoke-virtual {v0, v4, v5}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/Revisions;->setRevisionNumber(Ljava/lang/Object;I)V

    goto :goto_0

    :cond_2
    instance-of v2, v1, Lorg/apache/pdfbox/cos/COSDictionary;

    if-eqz v2, :cond_3

    check-cast v1, Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-static {v1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDAttributeObject;->create(Lorg/apache/pdfbox/cos/COSDictionary;)Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDAttributeObject;

    move-result-object v1

    invoke-virtual {v1, p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDAttributeObject;->setStructureElement(Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureElement;)V

    invoke-virtual {v0, v1, v3}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/Revisions;->addObject(Ljava/lang/Object;I)V

    :cond_3
    return-object v0
.end method

.method public getClassNames()Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/Revisions;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/Revisions<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->C:Lorg/apache/pdfbox/cos/COSName;

    new-instance v1, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/Revisions;

    invoke-direct {v1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/Revisions;-><init>()V

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureNode;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v2

    invoke-virtual {v2, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    instance-of v2, v0, Lorg/apache/pdfbox/cos/COSName;

    const/4 v3, 0x0

    if-eqz v2, :cond_0

    move-object v2, v0

    check-cast v2, Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v2}, Lorg/apache/pdfbox/cos/COSName;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v3}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/Revisions;->addObject(Ljava/lang/Object;I)V

    :cond_0
    instance-of v2, v0, Lorg/apache/pdfbox/cos/COSArray;

    if-eqz v2, :cond_3

    check-cast v0, Lorg/apache/pdfbox/cos/COSArray;

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSArray;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v2, 0x0

    :cond_1
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/pdfbox/cos/COSBase;

    instance-of v5, v4, Lorg/apache/pdfbox/cos/COSName;

    if-eqz v5, :cond_2

    check-cast v4, Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v4}, Lorg/apache/pdfbox/cos/COSName;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v3}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/Revisions;->addObject(Ljava/lang/Object;I)V

    goto :goto_0

    :cond_2
    instance-of v5, v4, Lorg/apache/pdfbox/cos/COSInteger;

    if-eqz v5, :cond_1

    check-cast v4, Lorg/apache/pdfbox/cos/COSInteger;

    invoke-virtual {v4}, Lorg/apache/pdfbox/cos/COSInteger;->intValue()I

    move-result v4

    invoke-virtual {v1, v2, v4}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/Revisions;->setRevisionNumber(Ljava/lang/Object;I)V

    goto :goto_0

    :cond_3
    return-object v1
.end method

.method public getElementIdentifier()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureNode;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->ID:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getString(Lorg/apache/pdfbox/cos/COSName;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getExpandedForm()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureNode;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->E:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getString(Lorg/apache/pdfbox/cos/COSName;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getLanguage()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureNode;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->LANG:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getString(Lorg/apache/pdfbox/cos/COSName;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPage()Lorg/apache/pdfbox/pdmodel/PDPage;
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureNode;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->PG:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSDictionary;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    :cond_0
    new-instance v1, Lorg/apache/pdfbox/pdmodel/PDPage;

    invoke-direct {v1, v0}, Lorg/apache/pdfbox/pdmodel/PDPage;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    return-object v1
.end method

.method public getParent()Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureNode;
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureNode;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->P:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSDictionary;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    :cond_0
    invoke-static {v0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureNode;->create(Lorg/apache/pdfbox/cos/COSDictionary;)Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureNode;

    move-result-object v0

    return-object v0
.end method

.method public getRevisionNumber()I
    .locals 3

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureNode;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->R:Lorg/apache/pdfbox/cos/COSName;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->getInt(Lorg/apache/pdfbox/cos/COSName;I)I

    move-result v0

    return v0
.end method

.method public getStandardStructureType()Ljava/lang/String;
    .locals 3

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureElement;->getStructureType()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureElement;->getRoleMap()Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureElement;->getRoleMap()Ljava/util/Map;

    move-result-object v1

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    instance-of v2, v1, Ljava/lang/String;

    if-eqz v2, :cond_0

    move-object v0, v1

    check-cast v0, Ljava/lang/String;

    :cond_0
    return-object v0
.end method

.method public getStructureType()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureNode;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->S:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getNameAsString(Lorg/apache/pdfbox/cos/COSName;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureNode;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->T:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getString(Lorg/apache/pdfbox/cos/COSName;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public incrementRevisionNumber()V
    .locals 1

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureElement;->getRevisionNumber()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureElement;->setRevisionNumber(I)V

    return-void
.end method

.method public insertBefore(Lorg/apache/pdfbox/cos/COSInteger;Ljava/lang/Object;)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1, p2}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureNode;->insertBefore(Lorg/apache/pdfbox/cos/COSBase;Ljava/lang/Object;)V

    return-void
.end method

.method public insertBefore(Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDMarkedContentReference;Ljava/lang/Object;)V
    .locals 0

    .line 2
    invoke-virtual {p0, p1, p2}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureNode;->insertObjectableBefore(Lorg/apache/pdfbox/pdmodel/common/COSObjectable;Ljava/lang/Object;)V

    return-void
.end method

.method public insertBefore(Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDObjectReference;Ljava/lang/Object;)V
    .locals 0

    .line 3
    invoke-virtual {p0, p1, p2}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureNode;->insertObjectableBefore(Lorg/apache/pdfbox/pdmodel/common/COSObjectable;Ljava/lang/Object;)V

    return-void
.end method

.method public removeAttribute(Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDAttributeObject;)V
    .locals 5

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->A:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureNode;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v1

    invoke-virtual {v1, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v1

    instance-of v2, v1, Lorg/apache/pdfbox/cos/COSArray;

    const/4 v3, 0x0

    if-eqz v2, :cond_0

    check-cast v1, Lorg/apache/pdfbox/cos/COSArray;

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/common/PDDictionaryWrapper;->getCOSObject()Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v2

    invoke-virtual {v1, v2}, Lorg/apache/pdfbox/cos/COSArray;->remove(Lorg/apache/pdfbox/cos/COSBase;)Z

    invoke-virtual {v1}, Lorg/apache/pdfbox/cos/COSArray;->size()I

    move-result v2

    const/4 v4, 0x2

    if-ne v2, v4, :cond_2

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lorg/apache/pdfbox/cos/COSArray;->getInt(I)I

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureNode;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v2

    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Lorg/apache/pdfbox/cos/COSArray;->getObject(I)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    goto :goto_0

    :cond_0
    instance-of v2, v1, Lorg/apache/pdfbox/cos/COSObject;

    if-eqz v2, :cond_1

    check-cast v1, Lorg/apache/pdfbox/cos/COSObject;

    invoke-virtual {v1}, Lorg/apache/pdfbox/cos/COSObject;->getObject()Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v1

    :cond_1
    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/common/PDDictionaryWrapper;->getCOSObject()Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureNode;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v1

    invoke-virtual {v1, v0, v3}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    :cond_2
    :goto_0
    invoke-virtual {p1, v3}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDAttributeObject;->setStructureElement(Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureElement;)V

    return-void
.end method

.method public removeClassName(Ljava/lang/String;)V
    .locals 3

    if-nez p1, :cond_0

    return-void

    :cond_0
    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->C:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureNode;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v1

    invoke-virtual {v1, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v1

    invoke-static {p1}, Lorg/apache/pdfbox/cos/COSName;->getPDFName(Ljava/lang/String;)Lorg/apache/pdfbox/cos/COSName;

    move-result-object p1

    instance-of v2, v1, Lorg/apache/pdfbox/cos/COSArray;

    if-eqz v2, :cond_1

    check-cast v1, Lorg/apache/pdfbox/cos/COSArray;

    invoke-virtual {v1, p1}, Lorg/apache/pdfbox/cos/COSArray;->remove(Lorg/apache/pdfbox/cos/COSBase;)Z

    invoke-virtual {v1}, Lorg/apache/pdfbox/cos/COSArray;->size()I

    move-result p1

    const/4 v2, 0x2

    if-ne p1, v2, :cond_3

    const/4 p1, 0x1

    invoke-virtual {v1, p1}, Lorg/apache/pdfbox/cos/COSArray;->getInt(I)I

    move-result p1

    if-nez p1, :cond_3

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureNode;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object p1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lorg/apache/pdfbox/cos/COSArray;->getObject(I)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v1

    goto :goto_0

    :cond_1
    instance-of v2, v1, Lorg/apache/pdfbox/cos/COSObject;

    if-eqz v2, :cond_2

    check-cast v1, Lorg/apache/pdfbox/cos/COSObject;

    invoke-virtual {v1}, Lorg/apache/pdfbox/cos/COSObject;->getObject()Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v1

    :cond_2
    invoke-virtual {p1, v1}, Lorg/apache/pdfbox/cos/COSName;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_3

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureNode;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object p1

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p1, v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    :cond_3
    return-void
.end method

.method public removeKid(Lorg/apache/pdfbox/cos/COSInteger;)V
    .locals 0

    .line 1
    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureNode;->removeKid(Lorg/apache/pdfbox/cos/COSBase;)Z

    return-void
.end method

.method public removeKid(Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDMarkedContentReference;)V
    .locals 0

    .line 2
    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureNode;->removeObjectableKid(Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)Z

    return-void
.end method

.method public removeKid(Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDObjectReference;)V
    .locals 0

    .line 3
    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureNode;->removeObjectableKid(Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)Z

    return-void
.end method

.method public setActualText(Ljava/lang/String;)V
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureNode;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->ACTUAL_TEXT:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setString(Lorg/apache/pdfbox/cos/COSName;Ljava/lang/String;)V

    return-void
.end method

.method public setAlternateDescription(Ljava/lang/String;)V
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureNode;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->ALT:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setString(Lorg/apache/pdfbox/cos/COSName;Ljava/lang/String;)V

    return-void
.end method

.method public setAttributes(Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/Revisions;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/Revisions<",
            "Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDAttributeObject;",
            ">;)V"
        }
    .end annotation

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->A:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/Revisions;->size()I

    move-result v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-ne v1, v2, :cond_0

    invoke-virtual {p1, v3}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/Revisions;->getRevisionNumber(I)I

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p1, v3}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/Revisions;->getObject(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDAttributeObject;

    invoke-virtual {p1, p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDAttributeObject;->setStructureElement(Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureElement;)V

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureNode;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v1

    invoke-virtual {v1, v0, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)V

    return-void

    :cond_0
    new-instance v1, Lorg/apache/pdfbox/cos/COSArray;

    invoke-direct {v1}, Lorg/apache/pdfbox/cos/COSArray;-><init>()V

    :goto_0
    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/Revisions;->size()I

    move-result v2

    if-ge v3, v2, :cond_2

    invoke-virtual {p1, v3}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/Revisions;->getObject(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDAttributeObject;

    invoke-virtual {v2, p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDAttributeObject;->setStructureElement(Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureElement;)V

    invoke-virtual {p1, v3}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/Revisions;->getRevisionNumber(I)I

    move-result v4

    if-ltz v4, :cond_1

    invoke-virtual {v1, v2}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)V

    int-to-long v4, v4

    invoke-static {v4, v5}, Lorg/apache/pdfbox/cos/COSInteger;->get(J)Lorg/apache/pdfbox/cos/COSInteger;

    move-result-object v2

    invoke-virtual {v1, v2}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "The revision number shall be > -1"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_2
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureNode;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object p1

    invoke-virtual {p1, v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    return-void
.end method

.method public setClassNames(Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/Revisions;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/Revisions<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    if-nez p1, :cond_0

    return-void

    :cond_0
    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->C:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/Revisions;->size()I

    move-result v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-ne v1, v2, :cond_1

    invoke-virtual {p1, v3}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/Revisions;->getRevisionNumber(I)I

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {p1, v3}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/Revisions;->getObject(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureNode;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v1

    invoke-virtual {v1, v0, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setName(Lorg/apache/pdfbox/cos/COSName;Ljava/lang/String;)V

    return-void

    :cond_1
    new-instance v1, Lorg/apache/pdfbox/cos/COSArray;

    invoke-direct {v1}, Lorg/apache/pdfbox/cos/COSArray;-><init>()V

    :goto_0
    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/Revisions;->size()I

    move-result v2

    if-ge v3, v2, :cond_3

    invoke-virtual {p1, v3}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/Revisions;->getObject(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {p1, v3}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/Revisions;->getRevisionNumber(I)I

    move-result v4

    if-ltz v4, :cond_2

    invoke-static {v2}, Lorg/apache/pdfbox/cos/COSName;->getPDFName(Ljava/lang/String;)Lorg/apache/pdfbox/cos/COSName;

    move-result-object v2

    invoke-virtual {v1, v2}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    int-to-long v4, v4

    invoke-static {v4, v5}, Lorg/apache/pdfbox/cos/COSInteger;->get(J)Lorg/apache/pdfbox/cos/COSInteger;

    move-result-object v2

    invoke-virtual {v1, v2}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_2
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "The revision number shall be > -1"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_3
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureNode;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object p1

    invoke-virtual {p1, v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    return-void
.end method

.method public setElementIdentifier(Ljava/lang/String;)V
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureNode;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->ID:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setString(Lorg/apache/pdfbox/cos/COSName;Ljava/lang/String;)V

    return-void
.end method

.method public setExpandedForm(Ljava/lang/String;)V
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureNode;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->E:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setString(Lorg/apache/pdfbox/cos/COSName;Ljava/lang/String;)V

    return-void
.end method

.method public setLanguage(Ljava/lang/String;)V
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureNode;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->LANG:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setString(Lorg/apache/pdfbox/cos/COSName;Ljava/lang/String;)V

    return-void
.end method

.method public setPage(Lorg/apache/pdfbox/pdmodel/PDPage;)V
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureNode;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->PG:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)V

    return-void
.end method

.method public setParent(Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureNode;)V
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureNode;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->P:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)V

    return-void
.end method

.method public setRevisionNumber(I)V
    .locals 2

    if-ltz p1, :cond_0

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureNode;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->R:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setInt(Lorg/apache/pdfbox/cos/COSName;I)V

    return-void

    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "The revision number shall be > -1"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public setStructureType(Ljava/lang/String;)V
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureNode;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->S:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setName(Lorg/apache/pdfbox/cos/COSName;Ljava/lang/String;)V

    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureNode;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->T:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setString(Lorg/apache/pdfbox/cos/COSName;Ljava/lang/String;)V

    return-void
.end method

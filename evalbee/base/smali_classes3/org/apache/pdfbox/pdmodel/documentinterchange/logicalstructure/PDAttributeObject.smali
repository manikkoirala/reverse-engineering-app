.class public abstract Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDAttributeObject;
.super Lorg/apache/pdfbox/pdmodel/common/PDDictionaryWrapper;
.source "SourceFile"


# instance fields
.field private structureElement:Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureElement;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/common/PDDictionaryWrapper;-><init>()V

    return-void
.end method

.method public constructor <init>(Lorg/apache/pdfbox/cos/COSDictionary;)V
    .locals 0

    .line 1
    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/common/PDDictionaryWrapper;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    return-void
.end method

.method public static arrayToString([F)Ljava/lang/String;
    .locals 3

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v1, 0x0

    :goto_0
    array-length v2, p0

    if-ge v1, v2, :cond_1

    if-lez v1, :cond_0

    const-string v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    aget v2, p0, v1

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    const/16 p0, 0x5d

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static arrayToString([Ljava/lang/Object;)Ljava/lang/String;
    .locals 3

    .line 2
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v1, 0x0

    :goto_0
    array-length v2, p0

    if-ge v1, v2, :cond_1

    if-lez v1, :cond_0

    const-string v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    aget-object v2, p0, v1

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    const/16 p0, 0x5d

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method

.method public static create(Lorg/apache/pdfbox/cos/COSDictionary;)Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDAttributeObject;
    .locals 2

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->O:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p0, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->getNameAsString(Lorg/apache/pdfbox/cos/COSName;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "UserProperties"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v0, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDUserAttributeObject;

    invoke-direct {v0, p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDUserAttributeObject;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    return-object v0

    :cond_0
    const-string v1, "List"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance v0, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDListAttributeObject;

    invoke-direct {v0, p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDListAttributeObject;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    return-object v0

    :cond_1
    const-string v1, "PrintField"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    new-instance v0, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDPrintFieldAttributeObject;

    invoke-direct {v0, p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDPrintFieldAttributeObject;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    return-object v0

    :cond_2
    const-string v1, "Table"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    new-instance v0, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDTableAttributeObject;

    invoke-direct {v0, p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDTableAttributeObject;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    return-object v0

    :cond_3
    const-string v1, "Layout"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    new-instance v0, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDLayoutAttributeObject;

    invoke-direct {v0, p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDLayoutAttributeObject;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    return-object v0

    :cond_4
    const-string v1, "XML-1.00"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    const-string v1, "HTML-3.2"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    const-string v1, "HTML-4.01"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    const-string v1, "OEB-1.00"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    const-string v1, "RTF-1.05"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    const-string v1, "CSS-1.00"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    const-string v1, "CSS-2.00"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    goto :goto_0

    :cond_5
    new-instance v0, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDDefaultAttributeObject;

    invoke-direct {v0, p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDDefaultAttributeObject;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    return-object v0

    :cond_6
    :goto_0
    new-instance v0, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDExportFormatAttributeObject;

    invoke-direct {v0, p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDExportFormatAttributeObject;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    return-object v0
.end method

.method private getStructureElement()Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureElement;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDAttributeObject;->structureElement:Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureElement;

    return-object v0
.end method

.method private static isValueChanged(Lorg/apache/pdfbox/cos/COSBase;Lorg/apache/pdfbox/cos/COSBase;)Z
    .locals 1

    const/4 v0, 0x1

    if-nez p0, :cond_1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result p0

    xor-int/2addr p0, v0

    return p0
.end method


# virtual methods
.method public getOwner()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/common/PDDictionaryWrapper;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->O:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getNameAsString(Lorg/apache/pdfbox/cos/COSName;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public isEmpty()Z
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/common/PDDictionaryWrapper;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSDictionary;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDAttributeObject;->getOwner()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method public notifyChanged()V
    .locals 1

    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDAttributeObject;->getStructureElement()Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureElement;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDAttributeObject;->getStructureElement()Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureElement;

    move-result-object v0

    invoke-virtual {v0, p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureElement;->attributeChanged(Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDAttributeObject;)V

    :cond_0
    return-void
.end method

.method public potentiallyNotifyChanged(Lorg/apache/pdfbox/cos/COSBase;Lorg/apache/pdfbox/cos/COSBase;)V
    .locals 0

    invoke-static {p1, p2}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDAttributeObject;->isValueChanged(Lorg/apache/pdfbox/cos/COSBase;Lorg/apache/pdfbox/cos/COSBase;)Z

    move-result p1

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDAttributeObject;->notifyChanged()V

    :cond_0
    return-void
.end method

.method public setOwner(Ljava/lang/String;)V
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/common/PDDictionaryWrapper;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->O:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setName(Lorg/apache/pdfbox/cos/COSName;Ljava/lang/String;)V

    return-void
.end method

.method public setStructureElement(Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureElement;)V
    .locals 0

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDAttributeObject;->structureElement:Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureElement;

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "O="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDAttributeObject;->getOwner()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.class public Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDTableAttributeObject;
.super Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;
.source "SourceFile"


# static fields
.field protected static final COL_SPAN:Ljava/lang/String; = "ColSpan"

.field protected static final HEADERS:Ljava/lang/String; = "Headers"

.field public static final OWNER_TABLE:Ljava/lang/String; = "Table"

.field protected static final ROW_SPAN:Ljava/lang/String; = "RowSpan"

.field protected static final SCOPE:Ljava/lang/String; = "Scope"

.field public static final SCOPE_BOTH:Ljava/lang/String; = "Both"

.field public static final SCOPE_COLUMN:Ljava/lang/String; = "Column"

.field public static final SCOPE_ROW:Ljava/lang/String; = "Row"

.field protected static final SUMMARY:Ljava/lang/String; = "Summary"


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;-><init>()V

    const-string v0, "Table"

    invoke-virtual {p0, v0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDAttributeObject;->setOwner(Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Lorg/apache/pdfbox/cos/COSDictionary;)V
    .locals 0

    .line 2
    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    return-void
.end method


# virtual methods
.method public getColSpan()I
    .locals 2

    const-string v0, "ColSpan"

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->getInteger(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public getHeaders()[Ljava/lang/String;
    .locals 1

    const-string v0, "Headers"

    invoke-virtual {p0, v0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->getArrayOfString(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getRowSpan()I
    .locals 2

    const-string v0, "RowSpan"

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->getInteger(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public getScope()Ljava/lang/String;
    .locals 1

    const-string v0, "Scope"

    invoke-virtual {p0, v0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->getName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSummary()Ljava/lang/String;
    .locals 1

    const-string v0, "Summary"

    invoke-virtual {p0, v0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public setColSpan(I)V
    .locals 1

    const-string v0, "ColSpan"

    invoke-virtual {p0, v0, p1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->setInteger(Ljava/lang/String;I)V

    return-void
.end method

.method public setHeaders([Ljava/lang/String;)V
    .locals 1

    const-string v0, "Headers"

    invoke-virtual {p0, v0, p1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->setArrayOfString(Ljava/lang/String;[Ljava/lang/String;)V

    return-void
.end method

.method public setRowSpan(I)V
    .locals 1

    const-string v0, "RowSpan"

    invoke-virtual {p0, v0, p1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->setInteger(Ljava/lang/String;I)V

    return-void
.end method

.method public setScope(Ljava/lang/String;)V
    .locals 1

    const-string v0, "Scope"

    invoke-virtual {p0, v0, p1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->setName(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public setSummary(Ljava/lang/String;)V
    .locals 1

    const-string v0, "Summary"

    invoke-virtual {p0, v0, p1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->setString(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-super {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDAttributeObject;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "RowSpan"

    invoke-virtual {p0, v1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->isSpecified(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, ", RowSpan="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDTableAttributeObject;->getRowSpan()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    const-string v1, "ColSpan"

    invoke-virtual {p0, v1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->isSpecified(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, ", ColSpan="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDTableAttributeObject;->getColSpan()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    const-string v1, "Headers"

    invoke-virtual {p0, v1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->isSpecified(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, ", Headers="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDTableAttributeObject;->getHeaders()[Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDAttributeObject;->arrayToString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    const-string v1, "Scope"

    invoke-virtual {p0, v1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->isSpecified(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    const-string v1, ", Scope="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDTableAttributeObject;->getScope()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_3
    const-string v1, "Summary"

    invoke-virtual {p0, v1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDStandardAttributeObject;->isSpecified(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    const-string v1, ", Summary="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDTableAttributeObject;->getSummary()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_4
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

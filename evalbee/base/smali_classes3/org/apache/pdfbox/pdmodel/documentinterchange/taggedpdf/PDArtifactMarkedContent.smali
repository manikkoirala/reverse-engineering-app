.class public Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDArtifactMarkedContent;
.super Lorg/apache/pdfbox/pdmodel/documentinterchange/markedcontent/PDMarkedContent;
.source "SourceFile"


# direct methods
.method public constructor <init>(Lorg/apache/pdfbox/cos/COSDictionary;)V
    .locals 1

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->ARTIFACT:Lorg/apache/pdfbox/cos/COSName;

    invoke-direct {p0, v0, p1}, Lorg/apache/pdfbox/pdmodel/documentinterchange/markedcontent/PDMarkedContent;-><init>(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSDictionary;)V

    return-void
.end method

.method private isAttached(Ljava/lang/String;)Z
    .locals 4

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/markedcontent/PDMarkedContent;->getProperties()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->ATTACHED:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSArray;

    const/4 v1, 0x0

    if-eqz v0, :cond_1

    move v2, v1

    :goto_0
    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSArray;->size()I

    move-result v3

    if-ge v2, v3, :cond_1

    invoke-virtual {v0, v2}, Lorg/apache/pdfbox/cos/COSArray;->getName(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 p1, 0x1

    return p1

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    return v1
.end method


# virtual methods
.method public getBBox()Lorg/apache/pdfbox/pdmodel/common/PDRectangle;
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/markedcontent/PDMarkedContent;->getProperties()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->BBOX:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSArray;

    if-eqz v0, :cond_0

    new-instance v1, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;

    invoke-direct {v1, v0}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;-><init>(Lorg/apache/pdfbox/cos/COSArray;)V

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return-object v1
.end method

.method public getSubtype()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/markedcontent/PDMarkedContent;->getProperties()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->SUBTYPE:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getNameAsString(Lorg/apache/pdfbox/cos/COSName;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getType()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/markedcontent/PDMarkedContent;->getProperties()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->TYPE:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getNameAsString(Lorg/apache/pdfbox/cos/COSName;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public isBottomAttached()Z
    .locals 1

    const-string v0, "Bottom"

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDArtifactMarkedContent;->isAttached(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public isLeftAttached()Z
    .locals 1

    const-string v0, "Left"

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDArtifactMarkedContent;->isAttached(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public isRightAttached()Z
    .locals 1

    const-string v0, "Right"

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDArtifactMarkedContent;->isAttached(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public isTopAttached()Z
    .locals 1

    const-string v0, "Top"

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/taggedpdf/PDArtifactMarkedContent;->isAttached(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

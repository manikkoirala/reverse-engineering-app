.class public Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/pdfbox/pdmodel/common/COSObjectable;


# instance fields
.field private cachedAcroForm:Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;

.field private final document:Lorg/apache/pdfbox/pdmodel/PDDocument;

.field private final root:Lorg/apache/pdfbox/cos/COSDictionary;


# direct methods
.method public constructor <init>(Lorg/apache/pdfbox/pdmodel/PDDocument;)V
    .locals 3

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;->document:Lorg/apache/pdfbox/pdmodel/PDDocument;

    new-instance v0, Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-direct {v0}, Lorg/apache/pdfbox/cos/COSDictionary;-><init>()V

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;->root:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->TYPE:Lorg/apache/pdfbox/cos/COSName;

    sget-object v2, Lorg/apache/pdfbox/cos/COSName;->CATALOG:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/PDDocument;->getDocument()Lorg/apache/pdfbox/cos/COSDocument;

    move-result-object p1

    invoke-virtual {p1}, Lorg/apache/pdfbox/cos/COSDocument;->getTrailer()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object p1

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->ROOT:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p1, v1, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    return-void
.end method

.method public constructor <init>(Lorg/apache/pdfbox/pdmodel/PDDocument;Lorg/apache/pdfbox/cos/COSDictionary;)V
    .locals 0

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;->document:Lorg/apache/pdfbox/pdmodel/PDDocument;

    iput-object p2, p0, Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;->root:Lorg/apache/pdfbox/cos/COSDictionary;

    return-void
.end method


# virtual methods
.method public addOutputIntent(Lorg/apache/pdfbox/pdmodel/graphics/color/PDOutputIntent;)V
    .locals 3

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;->root:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->OUTPUT_INTENTS:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSArray;

    if-nez v0, :cond_0

    new-instance v0, Lorg/apache/pdfbox/cos/COSArray;

    invoke-direct {v0}, Lorg/apache/pdfbox/cos/COSArray;-><init>()V

    iget-object v2, p0, Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;->root:Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-virtual {v2, v1, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    :cond_0
    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/graphics/color/PDOutputIntent;->getCOSObject()Lorg/apache/pdfbox/cos/COSBase;

    move-result-object p1

    invoke-virtual {v0, p1}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    return-void
.end method

.method public getAcroForm()Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;
    .locals 3

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;->cachedAcroForm:Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;

    if-nez v0, :cond_1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;->root:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->ACRO_FORM:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSDictionary;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    new-instance v1, Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;

    iget-object v2, p0, Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;->document:Lorg/apache/pdfbox/pdmodel/PDDocument;

    invoke-direct {v1, v2, v0}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;-><init>(Lorg/apache/pdfbox/pdmodel/PDDocument;Lorg/apache/pdfbox/cos/COSDictionary;)V

    move-object v0, v1

    :goto_0
    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;->cachedAcroForm:Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;

    :cond_1
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;->cachedAcroForm:Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;

    return-object v0
.end method

.method public getActions()Lorg/apache/pdfbox/pdmodel/interactive/action/PDDocumentCatalogAdditionalActions;
    .locals 3

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;->root:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->AA:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSDictionary;

    if-nez v0, :cond_0

    new-instance v0, Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-direct {v0}, Lorg/apache/pdfbox/cos/COSDictionary;-><init>()V

    iget-object v2, p0, Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;->root:Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-virtual {v2, v1, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    :cond_0
    new-instance v1, Lorg/apache/pdfbox/pdmodel/interactive/action/PDDocumentCatalogAdditionalActions;

    invoke-direct {v1, v0}, Lorg/apache/pdfbox/pdmodel/interactive/action/PDDocumentCatalogAdditionalActions;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    return-object v1
.end method

.method public bridge synthetic getCOSObject()Lorg/apache/pdfbox/cos/COSBase;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;->getCOSObject()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    return-object v0
.end method

.method public getCOSObject()Lorg/apache/pdfbox/cos/COSDictionary;
    .locals 1

    .line 2
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;->root:Lorg/apache/pdfbox/cos/COSDictionary;

    return-object v0
.end method

.method public getDocumentOutline()Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDDocumentOutline;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;->root:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->OUTLINES:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSDictionary;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    new-instance v1, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDDocumentOutline;

    invoke-direct {v1, v0}, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDDocumentOutline;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    move-object v0, v1

    :goto_0
    return-object v0
.end method

.method public getLanguage()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;->root:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->LANG:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getString(Lorg/apache/pdfbox/cos/COSName;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getMarkInfo()Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDMarkInfo;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;->root:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->MARK_INFO:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSDictionary;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    new-instance v1, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDMarkInfo;

    invoke-direct {v1, v0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDMarkInfo;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    move-object v0, v1

    :goto_0
    return-object v0
.end method

.method public getMetadata()Lorg/apache/pdfbox/pdmodel/common/PDMetadata;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;->root:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->METADATA:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    instance-of v1, v0, Lorg/apache/pdfbox/cos/COSStream;

    if-eqz v1, :cond_0

    new-instance v1, Lorg/apache/pdfbox/pdmodel/common/PDMetadata;

    check-cast v0, Lorg/apache/pdfbox/cos/COSStream;

    invoke-direct {v1, v0}, Lorg/apache/pdfbox/pdmodel/common/PDMetadata;-><init>(Lorg/apache/pdfbox/cos/COSStream;)V

    return-object v1

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public getNames()Lorg/apache/pdfbox/pdmodel/PDDocumentNameDictionary;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;->root:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->NAMES:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSDictionary;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    new-instance v1, Lorg/apache/pdfbox/pdmodel/PDDocumentNameDictionary;

    invoke-direct {v1, p0, v0}, Lorg/apache/pdfbox/pdmodel/PDDocumentNameDictionary;-><init>(Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;Lorg/apache/pdfbox/cos/COSDictionary;)V

    move-object v0, v1

    :goto_0
    return-object v0
.end method

.method public getOCProperties()Lorg/apache/pdfbox/pdmodel/graphics/optionalcontent/PDOptionalContentProperties;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;->root:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->OCPROPERTIES:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSDictionary;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    new-instance v1, Lorg/apache/pdfbox/pdmodel/graphics/optionalcontent/PDOptionalContentProperties;

    invoke-direct {v1, v0}, Lorg/apache/pdfbox/pdmodel/graphics/optionalcontent/PDOptionalContentProperties;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    move-object v0, v1

    :goto_0
    return-object v0
.end method

.method public getOpenAction()Lorg/apache/pdfbox/pdmodel/common/PDDestinationOrAction;
    .locals 4

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;->root:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->OPEN_ACTION:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    :cond_0
    instance-of v1, v0, Lorg/apache/pdfbox/cos/COSDictionary;

    if-eqz v1, :cond_1

    check-cast v0, Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-static {v0}, Lorg/apache/pdfbox/pdmodel/interactive/action/PDActionFactory;->createAction(Lorg/apache/pdfbox/cos/COSDictionary;)Lorg/apache/pdfbox/pdmodel/interactive/action/PDAction;

    move-result-object v0

    return-object v0

    :cond_1
    instance-of v1, v0, Lorg/apache/pdfbox/cos/COSArray;

    if-eqz v1, :cond_2

    invoke-static {v0}, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/destination/PDDestination;->create(Lorg/apache/pdfbox/cos/COSBase;)Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/destination/PDDestination;

    move-result-object v0

    return-object v0

    :cond_2
    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown OpenAction "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public getOutputIntents()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lorg/apache/pdfbox/pdmodel/graphics/color/PDOutputIntent;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget-object v1, p0, Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;->root:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v2, Lorg/apache/pdfbox/cos/COSName;->OUTPUT_INTENTS:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v1

    check-cast v1, Lorg/apache/pdfbox/cos/COSArray;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lorg/apache/pdfbox/cos/COSArray;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/pdfbox/cos/COSBase;

    new-instance v3, Lorg/apache/pdfbox/pdmodel/graphics/color/PDOutputIntent;

    check-cast v2, Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-direct {v3, v2}, Lorg/apache/pdfbox/pdmodel/graphics/color/PDOutputIntent;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method public getPageLabels()Lorg/apache/pdfbox/pdmodel/common/PDPageLabels;
    .locals 3

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;->root:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->PAGE_LABELS:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSDictionary;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    new-instance v1, Lorg/apache/pdfbox/pdmodel/common/PDPageLabels;

    iget-object v2, p0, Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;->document:Lorg/apache/pdfbox/pdmodel/PDDocument;

    invoke-direct {v1, v2, v0}, Lorg/apache/pdfbox/pdmodel/common/PDPageLabels;-><init>(Lorg/apache/pdfbox/pdmodel/PDDocument;Lorg/apache/pdfbox/cos/COSDictionary;)V

    move-object v0, v1

    :goto_0
    return-object v0
.end method

.method public getPageLayout()Lorg/apache/pdfbox/pdmodel/PageLayout;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;->root:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->PAGE_LAYOUT:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getNameAsString(Lorg/apache/pdfbox/cos/COSName;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {v0}, Lorg/apache/pdfbox/pdmodel/PageLayout;->fromString(Ljava/lang/String;)Lorg/apache/pdfbox/pdmodel/PageLayout;

    move-result-object v0

    return-object v0

    :cond_0
    sget-object v0, Lorg/apache/pdfbox/pdmodel/PageLayout;->SINGLE_PAGE:Lorg/apache/pdfbox/pdmodel/PageLayout;

    return-object v0
.end method

.method public getPageMode()Lorg/apache/pdfbox/pdmodel/PageMode;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;->root:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->PAGE_MODE:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getNameAsString(Lorg/apache/pdfbox/cos/COSName;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {v0}, Lorg/apache/pdfbox/pdmodel/PageMode;->fromString(Ljava/lang/String;)Lorg/apache/pdfbox/pdmodel/PageMode;

    move-result-object v0

    return-object v0

    :cond_0
    sget-object v0, Lorg/apache/pdfbox/pdmodel/PageMode;->USE_NONE:Lorg/apache/pdfbox/pdmodel/PageMode;

    return-object v0
.end method

.method public getPages()Lorg/apache/pdfbox/pdmodel/PDPageTree;
    .locals 3

    new-instance v0, Lorg/apache/pdfbox/pdmodel/PDPageTree;

    iget-object v1, p0, Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;->root:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v2, Lorg/apache/pdfbox/cos/COSName;->PAGES:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v1

    check-cast v1, Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/pdmodel/PDPageTree;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    return-object v0
.end method

.method public getStructureTreeRoot()Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureTreeRoot;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;->root:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->STRUCT_TREE_ROOT:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSDictionary;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    new-instance v1, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureTreeRoot;

    invoke-direct {v1, v0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureTreeRoot;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    move-object v0, v1

    :goto_0
    return-object v0
.end method

.method public getThreads()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDThread;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;->root:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->THREADS:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSArray;

    if-nez v0, :cond_0

    new-instance v0, Lorg/apache/pdfbox/cos/COSArray;

    invoke-direct {v0}, Lorg/apache/pdfbox/cos/COSArray;-><init>()V

    iget-object v2, p0, Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;->root:Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-virtual {v2, v1, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    const/4 v2, 0x0

    :goto_0
    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSArray;->size()I

    move-result v3

    if-ge v2, v3, :cond_1

    new-instance v3, Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDThread;

    invoke-virtual {v0, v2}, Lorg/apache/pdfbox/cos/COSArray;->getObject(I)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v4

    check-cast v4, Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-direct {v3, v4}, Lorg/apache/pdfbox/pdmodel/interactive/pagenavigation/PDThread;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    new-instance v2, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;

    invoke-direct {v2, v1, v0}, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;-><init>(Ljava/util/List;Lorg/apache/pdfbox/cos/COSArray;)V

    return-object v2
.end method

.method public getURI()Lorg/apache/pdfbox/pdmodel/interactive/action/PDURIDictionary;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;->root:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->URI:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSDictionary;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    new-instance v1, Lorg/apache/pdfbox/pdmodel/interactive/action/PDURIDictionary;

    invoke-direct {v1, v0}, Lorg/apache/pdfbox/pdmodel/interactive/action/PDURIDictionary;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    move-object v0, v1

    :goto_0
    return-object v0
.end method

.method public getVersion()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;->root:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->VERSION:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getNameAsString(Lorg/apache/pdfbox/cos/COSName;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getViewerPreferences()Lorg/apache/pdfbox/pdmodel/interactive/viewerpreferences/PDViewerPreferences;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;->root:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->VIEWER_PREFERENCES:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSDictionary;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    goto :goto_0

    :cond_0
    new-instance v1, Lorg/apache/pdfbox/pdmodel/interactive/viewerpreferences/PDViewerPreferences;

    invoke-direct {v1, v0}, Lorg/apache/pdfbox/pdmodel/interactive/viewerpreferences/PDViewerPreferences;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    move-object v0, v1

    :goto_0
    return-object v0
.end method

.method public setAcroForm(Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;->root:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->ACRO_FORM:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)V

    const/4 p1, 0x0

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;->cachedAcroForm:Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;

    return-void
.end method

.method public setActions(Lorg/apache/pdfbox/pdmodel/interactive/action/PDDocumentCatalogAdditionalActions;)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;->root:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->AA:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)V

    return-void
.end method

.method public setDocumentOutline(Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDDocumentOutline;)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;->root:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->OUTLINES:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)V

    return-void
.end method

.method public setLanguage(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;->root:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->LANG:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setString(Lorg/apache/pdfbox/cos/COSName;Ljava/lang/String;)V

    return-void
.end method

.method public setMarkInfo(Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDMarkInfo;)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;->root:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->MARK_INFO:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)V

    return-void
.end method

.method public setMetadata(Lorg/apache/pdfbox/pdmodel/common/PDMetadata;)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;->root:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->METADATA:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)V

    return-void
.end method

.method public setNames(Lorg/apache/pdfbox/pdmodel/PDDocumentNameDictionary;)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;->root:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->NAMES:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)V

    return-void
.end method

.method public setOCProperties(Lorg/apache/pdfbox/pdmodel/graphics/optionalcontent/PDOptionalContentProperties;)V
    .locals 4

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;->root:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->OCPROPERTIES:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)V

    if-eqz p1, :cond_0

    iget-object p1, p0, Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;->document:Lorg/apache/pdfbox/pdmodel/PDDocument;

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/PDDocument;->getVersion()F

    move-result p1

    float-to-double v0, p1

    const-wide/high16 v2, 0x3ff8000000000000L    # 1.5

    cmpg-double p1, v0, v2

    if-gez p1, :cond_0

    iget-object p1, p0, Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;->document:Lorg/apache/pdfbox/pdmodel/PDDocument;

    const/high16 v0, 0x3fc00000    # 1.5f

    invoke-virtual {p1, v0}, Lorg/apache/pdfbox/pdmodel/PDDocument;->setVersion(F)V

    :cond_0
    return-void
.end method

.method public setOpenAction(Lorg/apache/pdfbox/pdmodel/common/PDDestinationOrAction;)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;->root:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->OPEN_ACTION:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)V

    return-void
.end method

.method public setOutputIntents(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lorg/apache/pdfbox/pdmodel/graphics/color/PDOutputIntent;",
            ">;)V"
        }
    .end annotation

    new-instance v0, Lorg/apache/pdfbox/cos/COSArray;

    invoke-direct {v0}, Lorg/apache/pdfbox/cos/COSArray;-><init>()V

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/pdfbox/pdmodel/graphics/color/PDOutputIntent;

    invoke-virtual {v1}, Lorg/apache/pdfbox/pdmodel/graphics/color/PDOutputIntent;->getCOSObject()Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;->root:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->OUTPUT_INTENTS:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p1, v1, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    return-void
.end method

.method public setPageLabels(Lorg/apache/pdfbox/pdmodel/common/PDPageLabels;)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;->root:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->PAGE_LABELS:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)V

    return-void
.end method

.method public setPageLayout(Lorg/apache/pdfbox/pdmodel/PageLayout;)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;->root:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->PAGE_LAYOUT:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/PageLayout;->stringValue()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setName(Lorg/apache/pdfbox/cos/COSName;Ljava/lang/String;)V

    return-void
.end method

.method public setPageMode(Lorg/apache/pdfbox/pdmodel/PageMode;)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;->root:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->PAGE_MODE:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/PageMode;->stringValue()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setName(Lorg/apache/pdfbox/cos/COSName;Ljava/lang/String;)V

    return-void
.end method

.method public setStructureTreeRoot(Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureTreeRoot;)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;->root:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->STRUCT_TREE_ROOT:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)V

    return-void
.end method

.method public setThreads(Ljava/util/List;)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;->root:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->THREADS:Lorg/apache/pdfbox/cos/COSName;

    invoke-static {p1}, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;->converterToCOSArray(Ljava/util/List;)Lorg/apache/pdfbox/cos/COSArray;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    return-void
.end method

.method public setURI(Lorg/apache/pdfbox/pdmodel/interactive/action/PDURIDictionary;)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;->root:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->URI:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)V

    return-void
.end method

.method public setVersion(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;->root:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->VERSION:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setName(Lorg/apache/pdfbox/cos/COSName;Ljava/lang/String;)V

    return-void
.end method

.method public setViewerPreferences(Lorg/apache/pdfbox/pdmodel/interactive/viewerpreferences/PDViewerPreferences;)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;->root:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->VIEWER_PREFERENCES:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)V

    return-void
.end method

.class public Lorg/apache/pdfbox/pdmodel/PDEmbeddedFilesNameTreeNode;
.super Lorg/apache/pdfbox/pdmodel/common/PDNameTreeNode;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    const-class v0, Lorg/apache/pdfbox/pdmodel/common/filespecification/PDComplexFileSpecification;

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/pdmodel/common/PDNameTreeNode;-><init>(Ljava/lang/Class;)V

    return-void
.end method

.method public constructor <init>(Lorg/apache/pdfbox/cos/COSDictionary;)V
    .locals 1

    .line 2
    const-class v0, Lorg/apache/pdfbox/pdmodel/common/filespecification/PDComplexFileSpecification;

    invoke-direct {p0, p1, v0}, Lorg/apache/pdfbox/pdmodel/common/PDNameTreeNode;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public convertCOSToPD(Lorg/apache/pdfbox/cos/COSBase;)Lorg/apache/pdfbox/pdmodel/common/COSObjectable;
    .locals 1

    new-instance v0, Lorg/apache/pdfbox/pdmodel/common/filespecification/PDComplexFileSpecification;

    check-cast p1, Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-direct {v0, p1}, Lorg/apache/pdfbox/pdmodel/common/filespecification/PDComplexFileSpecification;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    return-object v0
.end method

.method public createChildNode(Lorg/apache/pdfbox/cos/COSDictionary;)Lorg/apache/pdfbox/pdmodel/common/PDNameTreeNode;
    .locals 1

    new-instance v0, Lorg/apache/pdfbox/pdmodel/PDEmbeddedFilesNameTreeNode;

    invoke-direct {v0, p1}, Lorg/apache/pdfbox/pdmodel/PDEmbeddedFilesNameTreeNode;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    return-object v0
.end method

.class final Lorg/apache/pdfbox/pdmodel/common/function/type4/Parser$Tokenizer;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/pdfbox/pdmodel/common/function/type4/Parser;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Tokenizer"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z = false

.field private static final CR:C = '\r'

.field private static final EOT:C = '\u0004'

.field private static final FF:C = '\u000c'

.field private static final LF:C = '\n'

.field private static final NUL:C = '\u0000'

.field private static final SPACE:C = ' '

.field private static final TAB:C = '\t'


# instance fields
.field private final buffer:Ljava/lang/StringBuilder;

.field private final handler:Lorg/apache/pdfbox/pdmodel/common/function/type4/Parser$SyntaxHandler;

.field private index:I

.field private final input:Ljava/lang/CharSequence;

.field private state:Lorg/apache/pdfbox/pdmodel/common/function/type4/Parser$State;


# direct methods
.method public static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method private constructor <init>(Ljava/lang/CharSequence;Lorg/apache/pdfbox/pdmodel/common/function/type4/Parser$SyntaxHandler;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Parser$State;->WHITESPACE:Lorg/apache/pdfbox/pdmodel/common/function/type4/Parser$State;

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Parser$Tokenizer;->state:Lorg/apache/pdfbox/pdmodel/common/function/type4/Parser$State;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Parser$Tokenizer;->buffer:Ljava/lang/StringBuilder;

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Parser$Tokenizer;->input:Ljava/lang/CharSequence;

    iput-object p2, p0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Parser$Tokenizer;->handler:Lorg/apache/pdfbox/pdmodel/common/function/type4/Parser$SyntaxHandler;

    return-void
.end method

.method public synthetic constructor <init>(Ljava/lang/CharSequence;Lorg/apache/pdfbox/pdmodel/common/function/type4/Parser$SyntaxHandler;Lorg/apache/pdfbox/pdmodel/common/function/type4/Parser$1;)V
    .locals 0

    .line 2
    invoke-direct {p0, p1, p2}, Lorg/apache/pdfbox/pdmodel/common/function/type4/Parser$Tokenizer;-><init>(Ljava/lang/CharSequence;Lorg/apache/pdfbox/pdmodel/common/function/type4/Parser$SyntaxHandler;)V

    return-void
.end method

.method public static synthetic access$100(Lorg/apache/pdfbox/pdmodel/common/function/type4/Parser$Tokenizer;)V
    .locals 0

    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/common/function/type4/Parser$Tokenizer;->tokenize()V

    return-void
.end method

.method private currentChar()C
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Parser$Tokenizer;->input:Ljava/lang/CharSequence;

    iget v1, p0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Parser$Tokenizer;->index:I

    invoke-interface {v0, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    return v0
.end method

.method private hasMore()Z
    .locals 2

    iget v0, p0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Parser$Tokenizer;->index:I

    iget-object v1, p0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Parser$Tokenizer;->input:Ljava/lang/CharSequence;

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private nextChar()C
    .locals 1

    iget v0, p0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Parser$Tokenizer;->index:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Parser$Tokenizer;->index:I

    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/common/function/type4/Parser$Tokenizer;->hasMore()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x4

    return v0

    :cond_0
    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/common/function/type4/Parser$Tokenizer;->currentChar()C

    move-result v0

    return v0
.end method

.method private nextState()Lorg/apache/pdfbox/pdmodel/common/function/type4/Parser$State;
    .locals 2

    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/common/function/type4/Parser$Tokenizer;->currentChar()C

    move-result v0

    if-eqz v0, :cond_2

    const/16 v1, 0x20

    if-eq v0, v1, :cond_2

    const/16 v1, 0x25

    if-eq v0, v1, :cond_1

    const/16 v1, 0x9

    if-eq v0, v1, :cond_2

    const/16 v1, 0xa

    if-eq v0, v1, :cond_0

    const/16 v1, 0xc

    if-eq v0, v1, :cond_0

    const/16 v1, 0xd

    if-eq v0, v1, :cond_0

    sget-object v0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Parser$State;->TOKEN:Lorg/apache/pdfbox/pdmodel/common/function/type4/Parser$State;

    goto :goto_0

    :cond_0
    sget-object v0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Parser$State;->NEWLINE:Lorg/apache/pdfbox/pdmodel/common/function/type4/Parser$State;

    goto :goto_0

    :cond_1
    sget-object v0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Parser$State;->COMMENT:Lorg/apache/pdfbox/pdmodel/common/function/type4/Parser$State;

    goto :goto_0

    :cond_2
    sget-object v0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Parser$State;->WHITESPACE:Lorg/apache/pdfbox/pdmodel/common/function/type4/Parser$State;

    :goto_0
    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Parser$Tokenizer;->state:Lorg/apache/pdfbox/pdmodel/common/function/type4/Parser$State;

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Parser$Tokenizer;->state:Lorg/apache/pdfbox/pdmodel/common/function/type4/Parser$State;

    return-object v0
.end method

.method private peek()C
    .locals 2

    iget v0, p0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Parser$Tokenizer;->index:I

    iget-object v1, p0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Parser$Tokenizer;->input:Ljava/lang/CharSequence;

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_0

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Parser$Tokenizer;->input:Ljava/lang/CharSequence;

    iget v1, p0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Parser$Tokenizer;->index:I

    add-int/lit8 v1, v1, 0x1

    invoke-interface {v0, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    return v0

    :cond_0
    const/4 v0, 0x4

    return v0
.end method

.method private scanComment()V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Parser$Tokenizer;->buffer:Ljava/lang/StringBuilder;

    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/common/function/type4/Parser$Tokenizer;->currentChar()C

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :goto_0
    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/common/function/type4/Parser$Tokenizer;->hasMore()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/common/function/type4/Parser$Tokenizer;->nextChar()C

    move-result v0

    const/16 v1, 0xa

    if-eq v0, v1, :cond_0

    const/16 v1, 0xc

    if-eq v0, v1, :cond_0

    const/16 v1, 0xd

    if-eq v0, v1, :cond_0

    iget-object v1, p0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Parser$Tokenizer;->buffer:Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Parser$Tokenizer;->handler:Lorg/apache/pdfbox/pdmodel/common/function/type4/Parser$SyntaxHandler;

    iget-object v1, p0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Parser$Tokenizer;->buffer:Ljava/lang/StringBuilder;

    invoke-interface {v0, v1}, Lorg/apache/pdfbox/pdmodel/common/function/type4/Parser$SyntaxHandler;->comment(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private scanNewLine()V
    .locals 2

    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/common/function/type4/Parser$Tokenizer;->currentChar()C

    move-result v0

    iget-object v1, p0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Parser$Tokenizer;->buffer:Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/16 v1, 0xd

    if-ne v0, v1, :cond_0

    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/common/function/type4/Parser$Tokenizer;->peek()C

    move-result v0

    const/16 v1, 0xa

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Parser$Tokenizer;->buffer:Ljava/lang/StringBuilder;

    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/common/function/type4/Parser$Tokenizer;->nextChar()C

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :cond_0
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Parser$Tokenizer;->handler:Lorg/apache/pdfbox/pdmodel/common/function/type4/Parser$SyntaxHandler;

    iget-object v1, p0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Parser$Tokenizer;->buffer:Ljava/lang/StringBuilder;

    invoke-interface {v0, v1}, Lorg/apache/pdfbox/pdmodel/common/function/type4/Parser$SyntaxHandler;->newLine(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/common/function/type4/Parser$Tokenizer;->nextChar()C

    return-void
.end method

.method private scanToken()V
    .locals 4

    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/common/function/type4/Parser$Tokenizer;->currentChar()C

    move-result v0

    iget-object v1, p0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Parser$Tokenizer;->buffer:Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/16 v1, 0x7b

    if-eq v0, v1, :cond_1

    const/16 v2, 0x7d

    if-eq v0, v2, :cond_1

    :goto_0
    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/common/function/type4/Parser$Tokenizer;->hasMore()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/common/function/type4/Parser$Tokenizer;->nextChar()C

    move-result v0

    if-eqz v0, :cond_0

    const/4 v3, 0x4

    if-eq v0, v3, :cond_0

    const/16 v3, 0x20

    if-eq v0, v3, :cond_0

    if-eq v0, v1, :cond_0

    if-eq v0, v2, :cond_0

    const/16 v3, 0x9

    if-eq v0, v3, :cond_0

    const/16 v3, 0xa

    if-eq v0, v3, :cond_0

    const/16 v3, 0xc

    if-eq v0, v3, :cond_0

    const/16 v3, 0xd

    if-eq v0, v3, :cond_0

    iget-object v3, p0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Parser$Tokenizer;->buffer:Ljava/lang/StringBuilder;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Parser$Tokenizer;->handler:Lorg/apache/pdfbox/pdmodel/common/function/type4/Parser$SyntaxHandler;

    iget-object v1, p0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Parser$Tokenizer;->buffer:Ljava/lang/StringBuilder;

    invoke-interface {v0, v1}, Lorg/apache/pdfbox/pdmodel/common/function/type4/Parser$SyntaxHandler;->token(Ljava/lang/CharSequence;)V

    return-void

    :cond_1
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Parser$Tokenizer;->handler:Lorg/apache/pdfbox/pdmodel/common/function/type4/Parser$SyntaxHandler;

    iget-object v1, p0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Parser$Tokenizer;->buffer:Ljava/lang/StringBuilder;

    invoke-interface {v0, v1}, Lorg/apache/pdfbox/pdmodel/common/function/type4/Parser$SyntaxHandler;->token(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/common/function/type4/Parser$Tokenizer;->nextChar()C

    return-void
.end method

.method private scanWhitespace()V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Parser$Tokenizer;->buffer:Ljava/lang/StringBuilder;

    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/common/function/type4/Parser$Tokenizer;->currentChar()C

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :goto_0
    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/common/function/type4/Parser$Tokenizer;->hasMore()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/common/function/type4/Parser$Tokenizer;->nextChar()C

    move-result v0

    if-eqz v0, :cond_0

    const/16 v1, 0x9

    if-eq v0, v1, :cond_0

    const/16 v1, 0x20

    if-eq v0, v1, :cond_0

    goto :goto_1

    :cond_0
    iget-object v1, p0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Parser$Tokenizer;->buffer:Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_1
    :goto_1
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Parser$Tokenizer;->handler:Lorg/apache/pdfbox/pdmodel/common/function/type4/Parser$SyntaxHandler;

    iget-object v1, p0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Parser$Tokenizer;->buffer:Ljava/lang/StringBuilder;

    invoke-interface {v0, v1}, Lorg/apache/pdfbox/pdmodel/common/function/type4/Parser$SyntaxHandler;->whitespace(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private tokenize()V
    .locals 2

    :goto_0
    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/common/function/type4/Parser$Tokenizer;->hasMore()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Parser$Tokenizer;->buffer:Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/common/function/type4/Parser$Tokenizer;->nextState()Lorg/apache/pdfbox/pdmodel/common/function/type4/Parser$State;

    sget-object v0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Parser$1;->$SwitchMap$org$apache$pdfbox$pdmodel$common$function$type4$Parser$State:[I

    iget-object v1, p0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Parser$Tokenizer;->state:Lorg/apache/pdfbox/pdmodel/common/function/type4/Parser$State;

    invoke-virtual {v1}, Ljava/lang/Enum;->ordinal()I

    move-result v1

    aget v0, v0, v1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/common/function/type4/Parser$Tokenizer;->scanToken()V

    goto :goto_0

    :cond_0
    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/common/function/type4/Parser$Tokenizer;->scanComment()V

    goto :goto_0

    :cond_1
    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/common/function/type4/Parser$Tokenizer;->scanWhitespace()V

    goto :goto_0

    :cond_2
    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/common/function/type4/Parser$Tokenizer;->scanNewLine()V

    goto :goto_0

    :cond_3
    return-void
.end method

.class public Lorg/apache/pdfbox/pdmodel/common/filespecification/PDComplexFileSpecification;
.super Lorg/apache/pdfbox/pdmodel/common/filespecification/PDFileSpecification;
.source "SourceFile"


# instance fields
.field private efDictionary:Lorg/apache/pdfbox/cos/COSDictionary;

.field private fs:Lorg/apache/pdfbox/cos/COSDictionary;


# direct methods
.method public constructor <init>()V
    .locals 3

    .line 1
    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/common/filespecification/PDFileSpecification;-><init>()V

    new-instance v0, Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-direct {v0}, Lorg/apache/pdfbox/cos/COSDictionary;-><init>()V

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/filespecification/PDComplexFileSpecification;->fs:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->TYPE:Lorg/apache/pdfbox/cos/COSName;

    sget-object v2, Lorg/apache/pdfbox/cos/COSName;->FILESPEC:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    return-void
.end method

.method public constructor <init>(Lorg/apache/pdfbox/cos/COSDictionary;)V
    .locals 2

    .line 2
    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/common/filespecification/PDFileSpecification;-><init>()V

    if-nez p1, :cond_0

    new-instance p1, Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-direct {p1}, Lorg/apache/pdfbox/cos/COSDictionary;-><init>()V

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/common/filespecification/PDComplexFileSpecification;->fs:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->TYPE:Lorg/apache/pdfbox/cos/COSName;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->FILESPEC:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p1, v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    goto :goto_0

    :cond_0
    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/common/filespecification/PDComplexFileSpecification;->fs:Lorg/apache/pdfbox/cos/COSDictionary;

    :goto_0
    return-void
.end method

.method private getEFDictionary()Lorg/apache/pdfbox/cos/COSDictionary;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/filespecification/PDComplexFileSpecification;->efDictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/filespecification/PDComplexFileSpecification;->fs:Lorg/apache/pdfbox/cos/COSDictionary;

    if-eqz v0, :cond_0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->EF:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSDictionary;

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/filespecification/PDComplexFileSpecification;->efDictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    :cond_0
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/filespecification/PDComplexFileSpecification;->efDictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    return-object v0
.end method

.method private getObjectFromEFDictionary(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;
    .locals 1

    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/common/filespecification/PDComplexFileSpecification;->getEFDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object p1

    return-object p1

    :cond_0
    const/4 p1, 0x0

    return-object p1
.end method


# virtual methods
.method public getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/filespecification/PDComplexFileSpecification;->fs:Lorg/apache/pdfbox/cos/COSDictionary;

    return-object v0
.end method

.method public getCOSObject()Lorg/apache/pdfbox/cos/COSBase;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/filespecification/PDComplexFileSpecification;->fs:Lorg/apache/pdfbox/cos/COSDictionary;

    return-object v0
.end method

.method public getEmbeddedFile()Lorg/apache/pdfbox/pdmodel/common/filespecification/PDEmbeddedFile;
    .locals 2

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->F:Lorg/apache/pdfbox/cos/COSName;

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/pdmodel/common/filespecification/PDComplexFileSpecification;->getObjectFromEFDictionary(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSStream;

    if-eqz v0, :cond_0

    new-instance v1, Lorg/apache/pdfbox/pdmodel/common/filespecification/PDEmbeddedFile;

    invoke-direct {v1, v0}, Lorg/apache/pdfbox/pdmodel/common/filespecification/PDEmbeddedFile;-><init>(Lorg/apache/pdfbox/cos/COSStream;)V

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return-object v1
.end method

.method public getEmbeddedFileDos()Lorg/apache/pdfbox/pdmodel/common/filespecification/PDEmbeddedFile;
    .locals 2

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->DOS:Lorg/apache/pdfbox/cos/COSName;

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/pdmodel/common/filespecification/PDComplexFileSpecification;->getObjectFromEFDictionary(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSStream;

    if-eqz v0, :cond_0

    new-instance v1, Lorg/apache/pdfbox/pdmodel/common/filespecification/PDEmbeddedFile;

    invoke-direct {v1, v0}, Lorg/apache/pdfbox/pdmodel/common/filespecification/PDEmbeddedFile;-><init>(Lorg/apache/pdfbox/cos/COSStream;)V

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return-object v1
.end method

.method public getEmbeddedFileMac()Lorg/apache/pdfbox/pdmodel/common/filespecification/PDEmbeddedFile;
    .locals 2

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->MAC:Lorg/apache/pdfbox/cos/COSName;

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/pdmodel/common/filespecification/PDComplexFileSpecification;->getObjectFromEFDictionary(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSStream;

    if-eqz v0, :cond_0

    new-instance v1, Lorg/apache/pdfbox/pdmodel/common/filespecification/PDEmbeddedFile;

    invoke-direct {v1, v0}, Lorg/apache/pdfbox/pdmodel/common/filespecification/PDEmbeddedFile;-><init>(Lorg/apache/pdfbox/cos/COSStream;)V

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return-object v1
.end method

.method public getEmbeddedFileUnicode()Lorg/apache/pdfbox/pdmodel/common/filespecification/PDEmbeddedFile;
    .locals 2

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->UF:Lorg/apache/pdfbox/cos/COSName;

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/pdmodel/common/filespecification/PDComplexFileSpecification;->getObjectFromEFDictionary(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSStream;

    if-eqz v0, :cond_0

    new-instance v1, Lorg/apache/pdfbox/pdmodel/common/filespecification/PDEmbeddedFile;

    invoke-direct {v1, v0}, Lorg/apache/pdfbox/pdmodel/common/filespecification/PDEmbeddedFile;-><init>(Lorg/apache/pdfbox/cos/COSStream;)V

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return-object v1
.end method

.method public getEmbeddedFileUnix()Lorg/apache/pdfbox/pdmodel/common/filespecification/PDEmbeddedFile;
    .locals 2

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->UNIX:Lorg/apache/pdfbox/cos/COSName;

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/pdmodel/common/filespecification/PDComplexFileSpecification;->getObjectFromEFDictionary(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSStream;

    if-eqz v0, :cond_0

    new-instance v1, Lorg/apache/pdfbox/pdmodel/common/filespecification/PDEmbeddedFile;

    invoke-direct {v1, v0}, Lorg/apache/pdfbox/pdmodel/common/filespecification/PDEmbeddedFile;-><init>(Lorg/apache/pdfbox/cos/COSStream;)V

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return-object v1
.end method

.method public getFile()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/filespecification/PDComplexFileSpecification;->fs:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->F:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getString(Lorg/apache/pdfbox/cos/COSName;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getFileDescription()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/filespecification/PDComplexFileSpecification;->fs:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->DESC:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getString(Lorg/apache/pdfbox/cos/COSName;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getFileDos()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/filespecification/PDComplexFileSpecification;->fs:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->DOS:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getString(Lorg/apache/pdfbox/cos/COSName;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getFileMac()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/filespecification/PDComplexFileSpecification;->fs:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->MAC:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getString(Lorg/apache/pdfbox/cos/COSName;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getFileUnicode()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/filespecification/PDComplexFileSpecification;->fs:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->UF:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getString(Lorg/apache/pdfbox/cos/COSName;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getFileUnix()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/filespecification/PDComplexFileSpecification;->fs:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->UNIX:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getString(Lorg/apache/pdfbox/cos/COSName;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getFilename()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/common/filespecification/PDComplexFileSpecification;->getFileUnicode()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/common/filespecification/PDComplexFileSpecification;->getFileDos()Ljava/lang/String;

    move-result-object v0

    :cond_0
    if-nez v0, :cond_1

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/common/filespecification/PDComplexFileSpecification;->getFileMac()Ljava/lang/String;

    move-result-object v0

    :cond_1
    if-nez v0, :cond_2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/common/filespecification/PDComplexFileSpecification;->getFileUnix()Ljava/lang/String;

    move-result-object v0

    :cond_2
    if-nez v0, :cond_3

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/common/filespecification/PDComplexFileSpecification;->getFile()Ljava/lang/String;

    move-result-object v0

    :cond_3
    return-object v0
.end method

.method public isVolatile()Z
    .locals 3

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/filespecification/PDComplexFileSpecification;->fs:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->V:Lorg/apache/pdfbox/cos/COSName;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->getBoolean(Lorg/apache/pdfbox/cos/COSName;Z)Z

    move-result v0

    return v0
.end method

.method public setEmbeddedFile(Lorg/apache/pdfbox/pdmodel/common/filespecification/PDEmbeddedFile;)V
    .locals 3

    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/common/filespecification/PDComplexFileSpecification;->getEFDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    new-instance v0, Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-direct {v0}, Lorg/apache/pdfbox/cos/COSDictionary;-><init>()V

    iget-object v1, p0, Lorg/apache/pdfbox/pdmodel/common/filespecification/PDComplexFileSpecification;->fs:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v2, Lorg/apache/pdfbox/cos/COSName;->EF:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v1, v2, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    :cond_0
    if-eqz v0, :cond_1

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->F:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)V

    :cond_1
    return-void
.end method

.method public setEmbeddedFileDos(Lorg/apache/pdfbox/pdmodel/common/filespecification/PDEmbeddedFile;)V
    .locals 3

    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/common/filespecification/PDComplexFileSpecification;->getEFDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    new-instance v0, Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-direct {v0}, Lorg/apache/pdfbox/cos/COSDictionary;-><init>()V

    iget-object v1, p0, Lorg/apache/pdfbox/pdmodel/common/filespecification/PDComplexFileSpecification;->fs:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v2, Lorg/apache/pdfbox/cos/COSName;->EF:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v1, v2, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    :cond_0
    if-eqz v0, :cond_1

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->DOS:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)V

    :cond_1
    return-void
.end method

.method public setEmbeddedFileMac(Lorg/apache/pdfbox/pdmodel/common/filespecification/PDEmbeddedFile;)V
    .locals 3

    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/common/filespecification/PDComplexFileSpecification;->getEFDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    new-instance v0, Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-direct {v0}, Lorg/apache/pdfbox/cos/COSDictionary;-><init>()V

    iget-object v1, p0, Lorg/apache/pdfbox/pdmodel/common/filespecification/PDComplexFileSpecification;->fs:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v2, Lorg/apache/pdfbox/cos/COSName;->EF:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v1, v2, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    :cond_0
    if-eqz v0, :cond_1

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->MAC:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)V

    :cond_1
    return-void
.end method

.method public setEmbeddedFileUnicode(Lorg/apache/pdfbox/pdmodel/common/filespecification/PDEmbeddedFile;)V
    .locals 3

    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/common/filespecification/PDComplexFileSpecification;->getEFDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    new-instance v0, Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-direct {v0}, Lorg/apache/pdfbox/cos/COSDictionary;-><init>()V

    iget-object v1, p0, Lorg/apache/pdfbox/pdmodel/common/filespecification/PDComplexFileSpecification;->fs:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v2, Lorg/apache/pdfbox/cos/COSName;->EF:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v1, v2, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    :cond_0
    if-eqz v0, :cond_1

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->UF:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)V

    :cond_1
    return-void
.end method

.method public setEmbeddedFileUnix(Lorg/apache/pdfbox/pdmodel/common/filespecification/PDEmbeddedFile;)V
    .locals 3

    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/common/filespecification/PDComplexFileSpecification;->getEFDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    new-instance v0, Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-direct {v0}, Lorg/apache/pdfbox/cos/COSDictionary;-><init>()V

    iget-object v1, p0, Lorg/apache/pdfbox/pdmodel/common/filespecification/PDComplexFileSpecification;->fs:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v2, Lorg/apache/pdfbox/cos/COSName;->EF:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v1, v2, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    :cond_0
    if-eqz v0, :cond_1

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->UNIX:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)V

    :cond_1
    return-void
.end method

.method public setFile(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/filespecification/PDComplexFileSpecification;->fs:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->F:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setString(Lorg/apache/pdfbox/cos/COSName;Ljava/lang/String;)V

    return-void
.end method

.method public setFileDescription(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/filespecification/PDComplexFileSpecification;->fs:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->DESC:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setString(Lorg/apache/pdfbox/cos/COSName;Ljava/lang/String;)V

    return-void
.end method

.method public setFileDos(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/filespecification/PDComplexFileSpecification;->fs:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->DOS:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setString(Lorg/apache/pdfbox/cos/COSName;Ljava/lang/String;)V

    return-void
.end method

.method public setFileMac(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/filespecification/PDComplexFileSpecification;->fs:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->MAC:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setString(Lorg/apache/pdfbox/cos/COSName;Ljava/lang/String;)V

    return-void
.end method

.method public setFileUnicode(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/filespecification/PDComplexFileSpecification;->fs:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->UF:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setString(Lorg/apache/pdfbox/cos/COSName;Ljava/lang/String;)V

    return-void
.end method

.method public setFileUnix(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/filespecification/PDComplexFileSpecification;->fs:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->UNIX:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setString(Lorg/apache/pdfbox/cos/COSName;Ljava/lang/String;)V

    return-void
.end method

.method public setVolatile(Z)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/filespecification/PDComplexFileSpecification;->fs:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->V:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setBoolean(Lorg/apache/pdfbox/cos/COSName;Z)V

    return-void
.end method

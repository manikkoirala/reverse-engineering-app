.class public Lorg/apache/pdfbox/pdmodel/common/function/type4/InstructionSequence;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private instructions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/function/type4/InstructionSequence;->instructions:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public addBoolean(Z)V
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/function/type4/InstructionSequence;->instructions:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public addInteger(I)V
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/function/type4/InstructionSequence;->instructions:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public addName(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/function/type4/InstructionSequence;->instructions:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public addProc(Lorg/apache/pdfbox/pdmodel/common/function/type4/InstructionSequence;)V
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/function/type4/InstructionSequence;->instructions:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public addReal(F)V
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/function/type4/InstructionSequence;->instructions:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public execute(Lorg/apache/pdfbox/pdmodel/common/function/type4/ExecutionContext;)V
    .locals 4

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/common/function/type4/ExecutionContext;->getStack()Ljava/util/Stack;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/pdfbox/pdmodel/common/function/type4/InstructionSequence;->instructions:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    instance-of v3, v2, Ljava/lang/String;

    if-eqz v3, :cond_1

    check-cast v2, Ljava/lang/String;

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/common/function/type4/ExecutionContext;->getOperators()Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;

    move-result-object v3

    invoke-virtual {v3, v2}, Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;->getOperator(Ljava/lang/String;)Lorg/apache/pdfbox/pdmodel/common/function/type4/Operator;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-interface {v3, p1}, Lorg/apache/pdfbox/pdmodel/common/function/type4/Operator;->execute(Lorg/apache/pdfbox/pdmodel/common/function/type4/ExecutionContext;)V

    goto :goto_0

    :cond_0
    new-instance p1, Ljava/lang/UnsupportedOperationException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unknown operator or name: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    invoke-virtual {v0, v2}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_2
    :goto_1
    invoke-virtual {v0}, Ljava/util/AbstractCollection;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v1

    instance-of v1, v1, Lorg/apache/pdfbox/pdmodel/common/function/type4/InstructionSequence;

    if-eqz v1, :cond_3

    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/pdfbox/pdmodel/common/function/type4/InstructionSequence;

    invoke-virtual {v1, p1}, Lorg/apache/pdfbox/pdmodel/common/function/type4/InstructionSequence;->execute(Lorg/apache/pdfbox/pdmodel/common/function/type4/ExecutionContext;)V

    goto :goto_1

    :cond_3
    return-void
.end method

.class public abstract Lorg/apache/pdfbox/pdmodel/common/function/PDFunction;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/pdfbox/pdmodel/common/COSObjectable;


# instance fields
.field private domain:Lorg/apache/pdfbox/cos/COSArray;

.field private functionDictionary:Lorg/apache/pdfbox/cos/COSDictionary;

.field private functionStream:Lorg/apache/pdfbox/pdmodel/common/PDStream;

.field private numberOfInputValues:I

.field private numberOfOutputValues:I

.field private range:Lorg/apache/pdfbox/cos/COSArray;


# direct methods
.method public constructor <init>(Lorg/apache/pdfbox/cos/COSBase;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/function/PDFunction;->functionStream:Lorg/apache/pdfbox/pdmodel/common/PDStream;

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/function/PDFunction;->functionDictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/function/PDFunction;->domain:Lorg/apache/pdfbox/cos/COSArray;

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/function/PDFunction;->range:Lorg/apache/pdfbox/cos/COSArray;

    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/pdfbox/pdmodel/common/function/PDFunction;->numberOfInputValues:I

    iput v0, p0, Lorg/apache/pdfbox/pdmodel/common/function/PDFunction;->numberOfOutputValues:I

    instance-of v0, p1, Lorg/apache/pdfbox/cos/COSStream;

    if-eqz v0, :cond_0

    new-instance v0, Lorg/apache/pdfbox/pdmodel/common/PDStream;

    check-cast p1, Lorg/apache/pdfbox/cos/COSStream;

    invoke-direct {v0, p1}, Lorg/apache/pdfbox/pdmodel/common/PDStream;-><init>(Lorg/apache/pdfbox/cos/COSStream;)V

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/function/PDFunction;->functionStream:Lorg/apache/pdfbox/pdmodel/common/PDStream;

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/common/PDStream;->getStream()Lorg/apache/pdfbox/cos/COSStream;

    move-result-object p1

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->TYPE:Lorg/apache/pdfbox/cos/COSName;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->FUNCTION:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p1, v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    goto :goto_0

    :cond_0
    instance-of v0, p1, Lorg/apache/pdfbox/cos/COSDictionary;

    if-eqz v0, :cond_1

    check-cast p1, Lorg/apache/pdfbox/cos/COSDictionary;

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/common/function/PDFunction;->functionDictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    :cond_1
    :goto_0
    return-void
.end method

.method public static create(Lorg/apache/pdfbox/cos/COSBase;)Lorg/apache/pdfbox/pdmodel/common/function/PDFunction;
    .locals 3

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->IDENTITY:Lorg/apache/pdfbox/cos/COSName;

    const/4 v1, 0x0

    if-ne p0, v0, :cond_0

    new-instance p0, Lorg/apache/pdfbox/pdmodel/common/function/PDFunctionTypeIdentity;

    invoke-direct {p0, v1}, Lorg/apache/pdfbox/pdmodel/common/function/PDFunctionTypeIdentity;-><init>(Lorg/apache/pdfbox/cos/COSBase;)V

    return-object p0

    :cond_0
    instance-of v0, p0, Lorg/apache/pdfbox/cos/COSObject;

    if-eqz v0, :cond_1

    check-cast p0, Lorg/apache/pdfbox/cos/COSObject;

    invoke-virtual {p0}, Lorg/apache/pdfbox/cos/COSObject;->getObject()Lorg/apache/pdfbox/cos/COSBase;

    move-result-object p0

    :cond_1
    check-cast p0, Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->FUNCTION_TYPE:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p0, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->getInt(Lorg/apache/pdfbox/cos/COSName;)I

    move-result v0

    if-nez v0, :cond_2

    goto :goto_0

    :cond_2
    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    new-instance v1, Lorg/apache/pdfbox/pdmodel/common/function/PDFunctionType2;

    invoke-direct {v1, p0}, Lorg/apache/pdfbox/pdmodel/common/function/PDFunctionType2;-><init>(Lorg/apache/pdfbox/cos/COSBase;)V

    goto :goto_0

    :cond_3
    const/4 v1, 0x3

    if-ne v0, v1, :cond_4

    new-instance v1, Lorg/apache/pdfbox/pdmodel/common/function/PDFunctionType3;

    invoke-direct {v1, p0}, Lorg/apache/pdfbox/pdmodel/common/function/PDFunctionType3;-><init>(Lorg/apache/pdfbox/cos/COSBase;)V

    goto :goto_0

    :cond_4
    const/4 v1, 0x4

    if-ne v0, v1, :cond_5

    new-instance v1, Lorg/apache/pdfbox/pdmodel/common/function/PDFunctionType4;

    invoke-direct {v1, p0}, Lorg/apache/pdfbox/pdmodel/common/function/PDFunctionType4;-><init>(Lorg/apache/pdfbox/cos/COSBase;)V

    :goto_0
    return-object v1

    :cond_5
    new-instance p0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Error: Unknown function type "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method private getDomainValues()Lorg/apache/pdfbox/cos/COSArray;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/function/PDFunction;->domain:Lorg/apache/pdfbox/cos/COSArray;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/common/function/PDFunction;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->DOMAIN:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSArray;

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/function/PDFunction;->domain:Lorg/apache/pdfbox/cos/COSArray;

    :cond_0
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/function/PDFunction;->domain:Lorg/apache/pdfbox/cos/COSArray;

    return-object v0
.end method


# virtual methods
.method public clipToRange(FFF)F
    .locals 1

    .line 1
    cmpg-float v0, p1, p2

    if-gez v0, :cond_0

    return p2

    :cond_0
    cmpl-float p2, p1, p3

    if-lez p2, :cond_1

    return p3

    :cond_1
    return p1
.end method

.method public clipToRange([F)[F
    .locals 7

    .line 2
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/common/function/PDFunction;->getRangeValues()Lorg/apache/pdfbox/cos/COSArray;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSArray;->toFloatArray()[F

    move-result-object v0

    array-length v1, v0

    div-int/lit8 v1, v1, 0x2

    new-array v2, v1, [F

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v1, :cond_0

    shl-int/lit8 v4, v3, 0x1

    aget v5, p1, v3

    aget v6, v0, v4

    add-int/lit8 v4, v4, 0x1

    aget v4, v0, v4

    invoke-virtual {p0, v5, v6, v4}, Lorg/apache/pdfbox/pdmodel/common/function/PDFunction;->clipToRange(FFF)F

    move-result v4

    aput v4, v2, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_0
    move-object p1, v2

    :cond_1
    return-object p1
.end method

.method public eval(Lorg/apache/pdfbox/cos/COSArray;)Lorg/apache/pdfbox/cos/COSArray;
    .locals 1

    .line 1
    invoke-virtual {p1}, Lorg/apache/pdfbox/cos/COSArray;->toFloatArray()[F

    move-result-object p1

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/pdmodel/common/function/PDFunction;->eval([F)[F

    move-result-object p1

    new-instance v0, Lorg/apache/pdfbox/cos/COSArray;

    invoke-direct {v0}, Lorg/apache/pdfbox/cos/COSArray;-><init>()V

    invoke-virtual {v0, p1}, Lorg/apache/pdfbox/cos/COSArray;->setFloatArray([F)V

    return-object v0
.end method

.method public abstract eval([F)[F
.end method

.method public getCOSObject()Lorg/apache/pdfbox/cos/COSBase;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/function/PDFunction;->functionStream:Lorg/apache/pdfbox/pdmodel/common/PDStream;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/common/PDStream;->getCOSObject()Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    return-object v0

    :cond_0
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/function/PDFunction;->functionDictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    return-object v0
.end method

.method public getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/function/PDFunction;->functionStream:Lorg/apache/pdfbox/pdmodel/common/PDStream;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/common/PDStream;->getStream()Lorg/apache/pdfbox/cos/COSStream;

    move-result-object v0

    return-object v0

    :cond_0
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/function/PDFunction;->functionDictionary:Lorg/apache/pdfbox/cos/COSDictionary;

    return-object v0
.end method

.method public getDomainForInput(I)Lorg/apache/pdfbox/pdmodel/common/PDRange;
    .locals 2

    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/common/function/PDFunction;->getDomainValues()Lorg/apache/pdfbox/cos/COSArray;

    move-result-object v0

    new-instance v1, Lorg/apache/pdfbox/pdmodel/common/PDRange;

    invoke-direct {v1, v0, p1}, Lorg/apache/pdfbox/pdmodel/common/PDRange;-><init>(Lorg/apache/pdfbox/cos/COSArray;I)V

    return-object v1
.end method

.method public abstract getFunctionType()I
.end method

.method public getNumberOfInputParameters()I
    .locals 2

    iget v0, p0, Lorg/apache/pdfbox/pdmodel/common/function/PDFunction;->numberOfInputValues:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/common/function/PDFunction;->getDomainValues()Lorg/apache/pdfbox/cos/COSArray;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSArray;->size()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    iput v0, p0, Lorg/apache/pdfbox/pdmodel/common/function/PDFunction;->numberOfInputValues:I

    :cond_0
    iget v0, p0, Lorg/apache/pdfbox/pdmodel/common/function/PDFunction;->numberOfInputValues:I

    return v0
.end method

.method public getNumberOfOutputParameters()I
    .locals 2

    iget v0, p0, Lorg/apache/pdfbox/pdmodel/common/function/PDFunction;->numberOfOutputValues:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/common/function/PDFunction;->getRangeValues()Lorg/apache/pdfbox/cos/COSArray;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSArray;->size()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    iput v0, p0, Lorg/apache/pdfbox/pdmodel/common/function/PDFunction;->numberOfOutputValues:I

    :cond_0
    iget v0, p0, Lorg/apache/pdfbox/pdmodel/common/function/PDFunction;->numberOfOutputValues:I

    return v0
.end method

.method public getPDStream()Lorg/apache/pdfbox/pdmodel/common/PDStream;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/function/PDFunction;->functionStream:Lorg/apache/pdfbox/pdmodel/common/PDStream;

    return-object v0
.end method

.method public getRangeForOutput(I)Lorg/apache/pdfbox/pdmodel/common/PDRange;
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/common/function/PDFunction;->getRangeValues()Lorg/apache/pdfbox/cos/COSArray;

    move-result-object v0

    new-instance v1, Lorg/apache/pdfbox/pdmodel/common/PDRange;

    invoke-direct {v1, v0, p1}, Lorg/apache/pdfbox/pdmodel/common/PDRange;-><init>(Lorg/apache/pdfbox/cos/COSArray;I)V

    return-object v1
.end method

.method public getRangeValues()Lorg/apache/pdfbox/cos/COSArray;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/function/PDFunction;->range:Lorg/apache/pdfbox/cos/COSArray;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/common/function/PDFunction;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->RANGE:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSArray;

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/function/PDFunction;->range:Lorg/apache/pdfbox/cos/COSArray;

    :cond_0
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/function/PDFunction;->range:Lorg/apache/pdfbox/cos/COSArray;

    return-object v0
.end method

.method public interpolate(FFFFF)F
    .locals 0

    sub-float/2addr p1, p2

    sub-float/2addr p5, p4

    mul-float/2addr p1, p5

    sub-float/2addr p3, p2

    div-float/2addr p1, p3

    add-float/2addr p4, p1

    return p4
.end method

.method public setDomainValues(Lorg/apache/pdfbox/cos/COSArray;)V
    .locals 2

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/common/function/PDFunction;->domain:Lorg/apache/pdfbox/cos/COSArray;

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/common/function/PDFunction;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->DOMAIN:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    return-void
.end method

.method public setRangeValues(Lorg/apache/pdfbox/cos/COSArray;)V
    .locals 2

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/common/function/PDFunction;->range:Lorg/apache/pdfbox/cos/COSArray;

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/common/function/PDFunction;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->RANGE:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "FunctionType"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/common/function/PDFunction;->getFunctionType()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.class public Lorg/apache/pdfbox/pdmodel/common/PDPageLabelRange;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/pdfbox/pdmodel/common/COSObjectable;


# static fields
.field private static final KEY_PREFIX:Lorg/apache/pdfbox/cos/COSName;

.field private static final KEY_START:Lorg/apache/pdfbox/cos/COSName;

.field private static final KEY_STYLE:Lorg/apache/pdfbox/cos/COSName;

.field public static final STYLE_DECIMAL:Ljava/lang/String; = "D"

.field public static final STYLE_LETTERS_LOWER:Ljava/lang/String; = "a"

.field public static final STYLE_LETTERS_UPPER:Ljava/lang/String; = "A"

.field public static final STYLE_ROMAN_LOWER:Ljava/lang/String; = "r"

.field public static final STYLE_ROMAN_UPPER:Ljava/lang/String; = "R"


# instance fields
.field private root:Lorg/apache/pdfbox/cos/COSDictionary;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->ST:Lorg/apache/pdfbox/cos/COSName;

    sput-object v0, Lorg/apache/pdfbox/pdmodel/common/PDPageLabelRange;->KEY_START:Lorg/apache/pdfbox/cos/COSName;

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->P:Lorg/apache/pdfbox/cos/COSName;

    sput-object v0, Lorg/apache/pdfbox/pdmodel/common/PDPageLabelRange;->KEY_PREFIX:Lorg/apache/pdfbox/cos/COSName;

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->S:Lorg/apache/pdfbox/cos/COSName;

    sput-object v0, Lorg/apache/pdfbox/pdmodel/common/PDPageLabelRange;->KEY_STYLE:Lorg/apache/pdfbox/cos/COSName;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 1
    new-instance v0, Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-direct {v0}, Lorg/apache/pdfbox/cos/COSDictionary;-><init>()V

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/pdmodel/common/PDPageLabelRange;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    return-void
.end method

.method public constructor <init>(Lorg/apache/pdfbox/cos/COSDictionary;)V
    .locals 0

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/common/PDPageLabelRange;->root:Lorg/apache/pdfbox/cos/COSDictionary;

    return-void
.end method


# virtual methods
.method public getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/PDPageLabelRange;->root:Lorg/apache/pdfbox/cos/COSDictionary;

    return-object v0
.end method

.method public getCOSObject()Lorg/apache/pdfbox/cos/COSBase;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/PDPageLabelRange;->root:Lorg/apache/pdfbox/cos/COSDictionary;

    return-object v0
.end method

.method public getPrefix()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/PDPageLabelRange;->root:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/pdmodel/common/PDPageLabelRange;->KEY_PREFIX:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getString(Lorg/apache/pdfbox/cos/COSName;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getStart()I
    .locals 3

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/PDPageLabelRange;->root:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/pdmodel/common/PDPageLabelRange;->KEY_START:Lorg/apache/pdfbox/cos/COSName;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->getInt(Lorg/apache/pdfbox/cos/COSName;I)I

    move-result v0

    return v0
.end method

.method public getStyle()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/PDPageLabelRange;->root:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/pdmodel/common/PDPageLabelRange;->KEY_STYLE:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getNameAsString(Lorg/apache/pdfbox/cos/COSName;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public setPrefix(Ljava/lang/String;)V
    .locals 2

    if-eqz p1, :cond_0

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/PDPageLabelRange;->root:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/pdmodel/common/PDPageLabelRange;->KEY_PREFIX:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setString(Lorg/apache/pdfbox/cos/COSName;Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lorg/apache/pdfbox/pdmodel/common/PDPageLabelRange;->root:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v0, Lorg/apache/pdfbox/pdmodel/common/PDPageLabelRange;->KEY_PREFIX:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p1, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->removeItem(Lorg/apache/pdfbox/cos/COSName;)V

    :goto_0
    return-void
.end method

.method public setStart(I)V
    .locals 2

    if-lez p1, :cond_0

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/PDPageLabelRange;->root:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/pdmodel/common/PDPageLabelRange;->KEY_START:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setInt(Lorg/apache/pdfbox/cos/COSName;I)V

    return-void

    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "The page numbering start value must be a positive integer"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public setStyle(Ljava/lang/String;)V
    .locals 2

    if-eqz p1, :cond_0

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/PDPageLabelRange;->root:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/pdmodel/common/PDPageLabelRange;->KEY_STYLE:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setName(Lorg/apache/pdfbox/cos/COSName;Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lorg/apache/pdfbox/pdmodel/common/PDPageLabelRange;->root:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v0, Lorg/apache/pdfbox/pdmodel/common/PDPageLabelRange;->KEY_STYLE:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p1, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->removeItem(Lorg/apache/pdfbox/cos/COSName;)V

    :goto_0
    return-void
.end method

.class public Lorg/apache/pdfbox/pdmodel/common/XrefEntry;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private byteOffset:I

.field private generation:I

.field private inUse:Z

.field private objectNumber:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/pdfbox/pdmodel/common/XrefEntry;->objectNumber:I

    iput v0, p0, Lorg/apache/pdfbox/pdmodel/common/XrefEntry;->byteOffset:I

    iput v0, p0, Lorg/apache/pdfbox/pdmodel/common/XrefEntry;->generation:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/pdfbox/pdmodel/common/XrefEntry;->inUse:Z

    return-void
.end method

.method public constructor <init>(IIILjava/lang/String;)V
    .locals 1

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/pdfbox/pdmodel/common/XrefEntry;->inUse:Z

    iput p1, p0, Lorg/apache/pdfbox/pdmodel/common/XrefEntry;->objectNumber:I

    iput p2, p0, Lorg/apache/pdfbox/pdmodel/common/XrefEntry;->byteOffset:I

    iput p3, p0, Lorg/apache/pdfbox/pdmodel/common/XrefEntry;->generation:I

    const-string p1, "n"

    invoke-virtual {p1, p4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    iput-boolean p1, p0, Lorg/apache/pdfbox/pdmodel/common/XrefEntry;->inUse:Z

    return-void
.end method


# virtual methods
.method public getByteOffset()I
    .locals 1

    iget v0, p0, Lorg/apache/pdfbox/pdmodel/common/XrefEntry;->byteOffset:I

    return v0
.end method

.class public Lorg/apache/pdfbox/pdmodel/common/COSArrayList;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/util/List;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/List<",
        "TE;>;"
    }
.end annotation


# instance fields
.field private final actual:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "TE;>;"
        }
    .end annotation
.end field

.field private final array:Lorg/apache/pdfbox/cos/COSArray;

.field private dictKey:Lorg/apache/pdfbox/cos/COSName;

.field private parentDict:Lorg/apache/pdfbox/cos/COSDictionary;


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lorg/apache/pdfbox/cos/COSArray;

    invoke-direct {v0}, Lorg/apache/pdfbox/cos/COSArray;-><init>()V

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;->array:Lorg/apache/pdfbox/cos/COSArray;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;->actual:Ljava/util/List;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Object;Lorg/apache/pdfbox/cos/COSBase;Lorg/apache/pdfbox/cos/COSDictionary;Lorg/apache/pdfbox/cos/COSName;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;",
            "Lorg/apache/pdfbox/cos/COSBase;",
            "Lorg/apache/pdfbox/cos/COSDictionary;",
            "Lorg/apache/pdfbox/cos/COSName;",
            ")V"
        }
    .end annotation

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lorg/apache/pdfbox/cos/COSArray;

    invoke-direct {v0}, Lorg/apache/pdfbox/cos/COSArray;-><init>()V

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;->array:Lorg/apache/pdfbox/cos/COSArray;

    invoke-virtual {v0, p2}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    new-instance p2, Ljava/util/ArrayList;

    invoke-direct {p2}, Ljava/util/ArrayList;-><init>()V

    iput-object p2, p0, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;->actual:Ljava/util/List;

    invoke-interface {p2, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iput-object p3, p0, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;->parentDict:Lorg/apache/pdfbox/cos/COSDictionary;

    iput-object p4, p0, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;->dictKey:Lorg/apache/pdfbox/cos/COSName;

    return-void
.end method

.method public constructor <init>(Ljava/util/List;Lorg/apache/pdfbox/cos/COSArray;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "TE;>;",
            "Lorg/apache/pdfbox/cos/COSArray;",
            ")V"
        }
    .end annotation

    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;->actual:Ljava/util/List;

    iput-object p2, p0, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;->array:Lorg/apache/pdfbox/cos/COSArray;

    return-void
.end method

.method public static convertCOSNameCOSArrayToList(Lorg/apache/pdfbox/cos/COSArray;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/pdfbox/cos/COSArray;",
            ")",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    if-eqz p0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0}, Lorg/apache/pdfbox/cos/COSArray;->size()I

    move-result v2

    if-ge v1, v2, :cond_0

    invoke-virtual {p0, v1}, Lorg/apache/pdfbox/cos/COSArray;->getObject(I)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v2

    check-cast v2, Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v2}, Lorg/apache/pdfbox/cos/COSName;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    new-instance v1, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;

    invoke-direct {v1, v0, p0}, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;-><init>(Ljava/util/List;Lorg/apache/pdfbox/cos/COSArray;)V

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    return-object v1
.end method

.method public static convertCOSStringCOSArrayToList(Lorg/apache/pdfbox/cos/COSArray;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/pdfbox/cos/COSArray;",
            ")",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    if-eqz p0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0}, Lorg/apache/pdfbox/cos/COSArray;->size()I

    move-result v2

    if-ge v1, v2, :cond_0

    invoke-virtual {p0, v1}, Lorg/apache/pdfbox/cos/COSArray;->getObject(I)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v2

    check-cast v2, Lorg/apache/pdfbox/cos/COSString;

    invoke-virtual {v2}, Lorg/apache/pdfbox/cos/COSString;->getString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    new-instance v1, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;

    invoke-direct {v1, v0, p0}, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;-><init>(Ljava/util/List;Lorg/apache/pdfbox/cos/COSArray;)V

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    return-object v1
.end method

.method public static convertFloatCOSArrayToList(Lorg/apache/pdfbox/cos/COSArray;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/pdfbox/cos/COSArray;",
            ")",
            "Ljava/util/List<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation

    if-eqz p0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0}, Lorg/apache/pdfbox/cos/COSArray;->size()I

    move-result v2

    if-ge v1, v2, :cond_0

    invoke-virtual {p0, v1}, Lorg/apache/pdfbox/cos/COSArray;->get(I)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v2

    check-cast v2, Lorg/apache/pdfbox/cos/COSNumber;

    invoke-virtual {v2}, Lorg/apache/pdfbox/cos/COSNumber;->floatValue()F

    move-result v2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    new-instance v1, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;

    invoke-direct {v1, v0, p0}, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;-><init>(Ljava/util/List;Lorg/apache/pdfbox/cos/COSArray;)V

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    :goto_1
    return-object v1
.end method

.method public static convertIntegerCOSArrayToList(Lorg/apache/pdfbox/cos/COSArray;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/pdfbox/cos/COSArray;",
            ")",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    if-eqz p0, :cond_2

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0}, Lorg/apache/pdfbox/cos/COSArray;->size()I

    move-result v2

    if-ge v1, v2, :cond_1

    invoke-virtual {p0, v1}, Lorg/apache/pdfbox/cos/COSArray;->get(I)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v2

    instance-of v2, v2, Lorg/apache/pdfbox/cos/COSObject;

    if-eqz v2, :cond_0

    invoke-virtual {p0, v1}, Lorg/apache/pdfbox/cos/COSArray;->get(I)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v2

    check-cast v2, Lorg/apache/pdfbox/cos/COSObject;

    invoke-virtual {v2}, Lorg/apache/pdfbox/cos/COSObject;->getObject()Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v2

    goto :goto_1

    :cond_0
    invoke-virtual {p0, v1}, Lorg/apache/pdfbox/cos/COSArray;->get(I)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v2

    :goto_1
    check-cast v2, Lorg/apache/pdfbox/cos/COSNumber;

    invoke-virtual {v2}, Lorg/apache/pdfbox/cos/COSNumber;->intValue()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    new-instance v1, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;

    invoke-direct {v1, v0, p0}, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;-><init>(Ljava/util/List;Lorg/apache/pdfbox/cos/COSArray;)V

    goto :goto_2

    :cond_2
    const/4 v1, 0x0

    :goto_2
    return-object v1
.end method

.method public static convertStringListToCOSNameCOSArray(Ljava/util/List;)Lorg/apache/pdfbox/cos/COSArray;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lorg/apache/pdfbox/cos/COSArray;"
        }
    .end annotation

    new-instance v0, Lorg/apache/pdfbox/cos/COSArray;

    invoke-direct {v0}, Lorg/apache/pdfbox/cos/COSArray;-><init>()V

    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Lorg/apache/pdfbox/cos/COSName;->getPDFName(Ljava/lang/String;)Lorg/apache/pdfbox/cos/COSName;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method public static convertStringListToCOSStringCOSArray(Ljava/util/List;)Lorg/apache/pdfbox/cos/COSArray;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Lorg/apache/pdfbox/cos/COSArray;"
        }
    .end annotation

    new-instance v0, Lorg/apache/pdfbox/cos/COSArray;

    invoke-direct {v0}, Lorg/apache/pdfbox/cos/COSArray;-><init>()V

    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    new-instance v2, Lorg/apache/pdfbox/cos/COSString;

    invoke-direct {v2, v1}, Lorg/apache/pdfbox/cos/COSString;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method public static converterToCOSArray(Ljava/util/List;)Lorg/apache/pdfbox/cos/COSArray;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "*>;)",
            "Lorg/apache/pdfbox/cos/COSArray;"
        }
    .end annotation

    if-eqz p0, :cond_a

    instance-of v0, p0, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;

    if-eqz v0, :cond_0

    check-cast p0, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;

    iget-object p0, p0, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;->array:Lorg/apache/pdfbox/cos/COSArray;

    goto/16 :goto_5

    :cond_0
    new-instance v0, Lorg/apache/pdfbox/cos/COSArray;

    invoke-direct {v0}, Lorg/apache/pdfbox/cos/COSArray;-><init>()V

    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_9

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    instance-of v2, v1, Ljava/lang/String;

    if-eqz v2, :cond_1

    new-instance v2, Lorg/apache/pdfbox/cos/COSString;

    check-cast v1, Ljava/lang/String;

    invoke-direct {v2, v1}, Lorg/apache/pdfbox/cos/COSString;-><init>(Ljava/lang/String;)V

    :goto_1
    invoke-virtual {v0, v2}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    goto :goto_0

    :cond_1
    instance-of v2, v1, Ljava/lang/Integer;

    if-nez v2, :cond_8

    instance-of v2, v1, Ljava/lang/Long;

    if-eqz v2, :cond_2

    goto :goto_3

    :cond_2
    instance-of v2, v1, Ljava/lang/Float;

    if-nez v2, :cond_7

    instance-of v2, v1, Ljava/lang/Double;

    if-eqz v2, :cond_3

    goto :goto_2

    :cond_3
    instance-of v2, v1, Lorg/apache/pdfbox/pdmodel/common/COSObjectable;

    if-eqz v2, :cond_4

    check-cast v1, Lorg/apache/pdfbox/pdmodel/common/COSObjectable;

    invoke-interface {v1}, Lorg/apache/pdfbox/pdmodel/common/COSObjectable;->getCOSObject()Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v1

    goto :goto_4

    :cond_4
    instance-of v2, v1, Lorg/apache/pdfbox/pdmodel/common/DualCOSObjectable;

    if-eqz v2, :cond_5

    check-cast v1, Lorg/apache/pdfbox/pdmodel/common/DualCOSObjectable;

    invoke-interface {v1}, Lorg/apache/pdfbox/pdmodel/common/DualCOSObjectable;->getFirstCOSObject()Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v2

    invoke-virtual {v0, v2}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    invoke-interface {v1}, Lorg/apache/pdfbox/pdmodel/common/DualCOSObjectable;->getSecondCOSObject()Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v1

    goto :goto_4

    :cond_5
    if-nez v1, :cond_6

    sget-object v1, Lorg/apache/pdfbox/cos/COSNull;->NULL:Lorg/apache/pdfbox/cos/COSNull;

    goto :goto_4

    :cond_6
    new-instance p0, Ljava/lang/RuntimeException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Error: Don\'t know how to convert type to COSBase \'"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw p0

    :cond_7
    :goto_2
    new-instance v2, Lorg/apache/pdfbox/cos/COSFloat;

    check-cast v1, Ljava/lang/Number;

    invoke-virtual {v1}, Ljava/lang/Number;->floatValue()F

    move-result v1

    invoke-direct {v2, v1}, Lorg/apache/pdfbox/cos/COSFloat;-><init>(F)V

    goto :goto_1

    :cond_8
    :goto_3
    check-cast v1, Ljava/lang/Number;

    invoke-virtual {v1}, Ljava/lang/Number;->longValue()J

    move-result-wide v1

    invoke-static {v1, v2}, Lorg/apache/pdfbox/cos/COSInteger;->get(J)Lorg/apache/pdfbox/cos/COSInteger;

    move-result-object v1

    :goto_4
    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    goto/16 :goto_0

    :cond_9
    move-object p0, v0

    goto :goto_5

    :cond_a
    const/4 p0, 0x0

    :goto_5
    return-object p0
.end method

.method private toCOSObjectList(Ljava/util/Collection;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "*>;)",
            "Ljava/util/List<",
            "Lorg/apache/pdfbox/cos/COSBase;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    instance-of v2, v1, Ljava/lang/String;

    if-eqz v2, :cond_0

    new-instance v2, Lorg/apache/pdfbox/cos/COSString;

    check-cast v1, Ljava/lang/String;

    invoke-direct {v2, v1}, Lorg/apache/pdfbox/cos/COSString;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    instance-of v2, v1, Lorg/apache/pdfbox/pdmodel/common/DualCOSObjectable;

    if-eqz v2, :cond_1

    check-cast v1, Lorg/apache/pdfbox/pdmodel/common/DualCOSObjectable;

    iget-object v2, p0, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;->array:Lorg/apache/pdfbox/cos/COSArray;

    invoke-interface {v1}, Lorg/apache/pdfbox/pdmodel/common/DualCOSObjectable;->getFirstCOSObject()Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v3

    invoke-virtual {v2, v3}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    iget-object v2, p0, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;->array:Lorg/apache/pdfbox/cos/COSArray;

    invoke-interface {v1}, Lorg/apache/pdfbox/pdmodel/common/DualCOSObjectable;->getSecondCOSObject()Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v1

    invoke-virtual {v2, v1}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    goto :goto_0

    :cond_1
    check-cast v1, Lorg/apache/pdfbox/pdmodel/common/COSObjectable;

    invoke-interface {v1}, Lorg/apache/pdfbox/pdmodel/common/COSObjectable;->getCOSObject()Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    return-object v0
.end method


# virtual methods
.method public add(ILjava/lang/Object;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ITE;)V"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;->parentDict:Lorg/apache/pdfbox/cos/COSDictionary;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;->dictKey:Lorg/apache/pdfbox/cos/COSName;

    iget-object v2, p0, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;->array:Lorg/apache/pdfbox/cos/COSArray;

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;->parentDict:Lorg/apache/pdfbox/cos/COSDictionary;

    :cond_0
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;->actual:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    instance-of v0, p2, Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;->array:Lorg/apache/pdfbox/cos/COSArray;

    new-instance v1, Lorg/apache/pdfbox/cos/COSString;

    check-cast p2, Ljava/lang/String;

    invoke-direct {v1, p2}, Lorg/apache/pdfbox/cos/COSString;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1, v1}, Lorg/apache/pdfbox/cos/COSArray;->add(ILorg/apache/pdfbox/cos/COSBase;)V

    goto :goto_1

    :cond_1
    instance-of v0, p2, Lorg/apache/pdfbox/pdmodel/common/DualCOSObjectable;

    if-eqz v0, :cond_2

    check-cast p2, Lorg/apache/pdfbox/pdmodel/common/DualCOSObjectable;

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;->array:Lorg/apache/pdfbox/cos/COSArray;

    mul-int/lit8 p1, p1, 0x2

    invoke-interface {p2}, Lorg/apache/pdfbox/pdmodel/common/DualCOSObjectable;->getFirstCOSObject()Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lorg/apache/pdfbox/cos/COSArray;->add(ILorg/apache/pdfbox/cos/COSBase;)V

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;->array:Lorg/apache/pdfbox/cos/COSArray;

    add-int/lit8 p1, p1, 0x1

    invoke-interface {p2}, Lorg/apache/pdfbox/pdmodel/common/DualCOSObjectable;->getSecondCOSObject()Lorg/apache/pdfbox/cos/COSBase;

    move-result-object p2

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;->array:Lorg/apache/pdfbox/cos/COSArray;

    check-cast p2, Lorg/apache/pdfbox/pdmodel/common/COSObjectable;

    invoke-interface {p2}, Lorg/apache/pdfbox/pdmodel/common/COSObjectable;->getCOSObject()Lorg/apache/pdfbox/cos/COSBase;

    move-result-object p2

    :goto_0
    invoke-virtual {v0, p1, p2}, Lorg/apache/pdfbox/cos/COSArray;->add(ILorg/apache/pdfbox/cos/COSBase;)V

    :goto_1
    return-void
.end method

.method public add(Ljava/lang/Object;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;)Z"
        }
    .end annotation

    .line 2
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;->parentDict:Lorg/apache/pdfbox/cos/COSDictionary;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;->dictKey:Lorg/apache/pdfbox/cos/COSName;

    iget-object v2, p0, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;->array:Lorg/apache/pdfbox/cos/COSArray;

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;->parentDict:Lorg/apache/pdfbox/cos/COSDictionary;

    :cond_0
    instance-of v0, p1, Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;->array:Lorg/apache/pdfbox/cos/COSArray;

    new-instance v1, Lorg/apache/pdfbox/cos/COSString;

    move-object v2, p1

    check-cast v2, Ljava/lang/String;

    invoke-direct {v1, v2}, Lorg/apache/pdfbox/cos/COSString;-><init>(Ljava/lang/String;)V

    :goto_0
    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    goto :goto_1

    :cond_1
    instance-of v0, p1, Lorg/apache/pdfbox/pdmodel/common/DualCOSObjectable;

    if-eqz v0, :cond_2

    move-object v0, p1

    check-cast v0, Lorg/apache/pdfbox/pdmodel/common/DualCOSObjectable;

    iget-object v1, p0, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;->array:Lorg/apache/pdfbox/cos/COSArray;

    invoke-interface {v0}, Lorg/apache/pdfbox/pdmodel/common/DualCOSObjectable;->getFirstCOSObject()Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v2

    invoke-virtual {v1, v2}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    iget-object v1, p0, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;->array:Lorg/apache/pdfbox/cos/COSArray;

    invoke-interface {v0}, Lorg/apache/pdfbox/pdmodel/common/DualCOSObjectable;->getSecondCOSObject()Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    invoke-virtual {v1, v0}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;->array:Lorg/apache/pdfbox/cos/COSArray;

    if-eqz v0, :cond_3

    move-object v1, p1

    check-cast v1, Lorg/apache/pdfbox/pdmodel/common/COSObjectable;

    invoke-interface {v1}, Lorg/apache/pdfbox/pdmodel/common/COSObjectable;->getCOSObject()Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v1

    goto :goto_0

    :cond_3
    :goto_1
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;->actual:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public addAll(ILjava/util/Collection;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/Collection<",
            "+TE;>;)Z"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;->parentDict:Lorg/apache/pdfbox/cos/COSDictionary;

    if-eqz v0, :cond_0

    invoke-interface {p2}, Ljava/util/Collection;->size()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;->parentDict:Lorg/apache/pdfbox/cos/COSDictionary;

    iget-object v1, p0, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;->dictKey:Lorg/apache/pdfbox/cos/COSName;

    iget-object v2, p0, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;->array:Lorg/apache/pdfbox/cos/COSArray;

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;->parentDict:Lorg/apache/pdfbox/cos/COSDictionary;

    :cond_0
    invoke-interface {p2}, Ljava/util/Collection;->size()I

    move-result v0

    if-lez v0, :cond_1

    invoke-interface {p2}, Ljava/util/Collection;->toArray()[Ljava/lang/Object;

    move-result-object v0

    const/4 v1, 0x0

    aget-object v0, v0, v1

    instance-of v0, v0, Lorg/apache/pdfbox/pdmodel/common/DualCOSObjectable;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;->array:Lorg/apache/pdfbox/cos/COSArray;

    mul-int/lit8 v1, p1, 0x2

    invoke-direct {p0, p2}, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;->toCOSObjectList(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSArray;->addAll(ILjava/util/Collection;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;->array:Lorg/apache/pdfbox/cos/COSArray;

    invoke-direct {p0, p2}, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;->toCOSObjectList(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lorg/apache/pdfbox/cos/COSArray;->addAll(ILjava/util/Collection;)V

    :goto_0
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;->actual:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->addAll(ILjava/util/Collection;)Z

    move-result p1

    return p1
.end method

.method public addAll(Ljava/util/Collection;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "+TE;>;)Z"
        }
    .end annotation

    .line 2
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;->parentDict:Lorg/apache/pdfbox/cos/COSDictionary;

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;->parentDict:Lorg/apache/pdfbox/cos/COSDictionary;

    iget-object v1, p0, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;->dictKey:Lorg/apache/pdfbox/cos/COSName;

    iget-object v2, p0, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;->array:Lorg/apache/pdfbox/cos/COSArray;

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;->parentDict:Lorg/apache/pdfbox/cos/COSDictionary;

    :cond_0
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;->array:Lorg/apache/pdfbox/cos/COSArray;

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;->toCOSObjectList(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSArray;->addAll(Ljava/util/Collection;)V

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;->actual:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    move-result p1

    return p1
.end method

.method public clear()V
    .locals 3

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;->parentDict:Lorg/apache/pdfbox/cos/COSDictionary;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;->dictKey:Lorg/apache/pdfbox/cos/COSName;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    :cond_0
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;->actual:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;->array:Lorg/apache/pdfbox/cos/COSArray;

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSArray;->clear()V

    return-void
.end method

.method public contains(Ljava/lang/Object;)Z
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;->actual:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public containsAll(Ljava/util/Collection;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "*>;)Z"
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;->actual:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->containsAll(Ljava/util/Collection;)Z

    move-result p1

    return p1
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;->actual:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public get(I)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TE;"
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;->actual:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;->actual:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->hashCode()I

    move-result v0

    return v0
.end method

.method public indexOf(Ljava/lang/Object;)I
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;->actual:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result p1

    return p1
.end method

.method public isEmpty()Z
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;->actual:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator<",
            "TE;>;"
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;->actual:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    return-object v0
.end method

.method public lastIndexOf(Ljava/lang/Object;)I
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;->actual:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result p1

    return p1
.end method

.method public listIterator()Ljava/util/ListIterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ListIterator<",
            "TE;>;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;->actual:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->listIterator()Ljava/util/ListIterator;

    move-result-object v0

    return-object v0
.end method

.method public listIterator(I)Ljava/util/ListIterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/ListIterator<",
            "TE;>;"
        }
    .end annotation

    .line 2
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;->actual:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->listIterator(I)Ljava/util/ListIterator;

    move-result-object p1

    return-object p1
.end method

.method public remove(I)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TE;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;->array:Lorg/apache/pdfbox/cos/COSArray;

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSArray;->size()I

    move-result v0

    if-le v0, p1, :cond_0

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;->array:Lorg/apache/pdfbox/cos/COSArray;

    invoke-virtual {v0, p1}, Lorg/apache/pdfbox/cos/COSArray;->get(I)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    instance-of v0, v0, Lorg/apache/pdfbox/pdmodel/common/DualCOSObjectable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;->array:Lorg/apache/pdfbox/cos/COSArray;

    invoke-virtual {v0, p1}, Lorg/apache/pdfbox/cos/COSArray;->remove(I)Lorg/apache/pdfbox/cos/COSBase;

    :cond_0
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;->array:Lorg/apache/pdfbox/cos/COSArray;

    invoke-virtual {v0, p1}, Lorg/apache/pdfbox/cos/COSArray;->remove(I)Lorg/apache/pdfbox/cos/COSBase;

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;->actual:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public remove(Ljava/lang/Object;)Z
    .locals 1

    .line 2
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;->actual:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result p1

    if-ltz p1, :cond_0

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;->actual:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;->array:Lorg/apache/pdfbox/cos/COSArray;

    invoke-virtual {v0, p1}, Lorg/apache/pdfbox/cos/COSArray;->remove(I)Lorg/apache/pdfbox/cos/COSBase;

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public removeAll(Ljava/util/Collection;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "*>;)Z"
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;->array:Lorg/apache/pdfbox/cos/COSArray;

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;->toCOSObjectList(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSArray;->removeAll(Ljava/util/Collection;)V

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;->actual:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->removeAll(Ljava/util/Collection;)Z

    move-result p1

    return p1
.end method

.method public retainAll(Ljava/util/Collection;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "*>;)Z"
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;->array:Lorg/apache/pdfbox/cos/COSArray;

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;->toCOSObjectList(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSArray;->retainAll(Ljava/util/Collection;)V

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;->actual:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->retainAll(Ljava/util/Collection;)Z

    move-result p1

    return p1
.end method

.method public set(ILjava/lang/Object;)Ljava/lang/Object;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ITE;)TE;"
        }
    .end annotation

    instance-of v0, p2, Ljava/lang/String;

    if-eqz v0, :cond_1

    new-instance v0, Lorg/apache/pdfbox/cos/COSString;

    move-object v1, p2

    check-cast v1, Ljava/lang/String;

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSString;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;->parentDict:Lorg/apache/pdfbox/cos/COSDictionary;

    if-eqz v1, :cond_0

    if-nez p1, :cond_0

    iget-object v2, p0, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;->dictKey:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v1, v2, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    :cond_0
    iget-object v1, p0, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;->array:Lorg/apache/pdfbox/cos/COSArray;

    invoke-virtual {v1, p1, v0}, Lorg/apache/pdfbox/cos/COSArray;->set(ILorg/apache/pdfbox/cos/COSBase;)V

    goto :goto_0

    :cond_1
    instance-of v0, p2, Lorg/apache/pdfbox/pdmodel/common/DualCOSObjectable;

    if-eqz v0, :cond_2

    move-object v0, p2

    check-cast v0, Lorg/apache/pdfbox/pdmodel/common/DualCOSObjectable;

    iget-object v1, p0, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;->array:Lorg/apache/pdfbox/cos/COSArray;

    mul-int/lit8 v2, p1, 0x2

    invoke-interface {v0}, Lorg/apache/pdfbox/pdmodel/common/DualCOSObjectable;->getFirstCOSObject()Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lorg/apache/pdfbox/cos/COSArray;->set(ILorg/apache/pdfbox/cos/COSBase;)V

    iget-object v1, p0, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;->array:Lorg/apache/pdfbox/cos/COSArray;

    add-int/lit8 v2, v2, 0x1

    invoke-interface {v0}, Lorg/apache/pdfbox/pdmodel/common/DualCOSObjectable;->getSecondCOSObject()Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lorg/apache/pdfbox/cos/COSArray;->set(ILorg/apache/pdfbox/cos/COSBase;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;->parentDict:Lorg/apache/pdfbox/cos/COSDictionary;

    if-eqz v0, :cond_3

    if-nez p1, :cond_3

    iget-object v1, p0, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;->dictKey:Lorg/apache/pdfbox/cos/COSName;

    move-object v2, p2

    check-cast v2, Lorg/apache/pdfbox/pdmodel/common/COSObjectable;

    invoke-interface {v2}, Lorg/apache/pdfbox/pdmodel/common/COSObjectable;->getCOSObject()Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    :cond_3
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;->array:Lorg/apache/pdfbox/cos/COSArray;

    move-object v1, p2

    check-cast v1, Lorg/apache/pdfbox/pdmodel/common/COSObjectable;

    invoke-interface {v1}, Lorg/apache/pdfbox/pdmodel/common/COSObjectable;->getCOSObject()Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lorg/apache/pdfbox/cos/COSArray;->set(ILorg/apache/pdfbox/cos/COSBase;)V

    :goto_0
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;->actual:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public size()I
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;->actual:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public subList(II)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Ljava/util/List<",
            "TE;>;"
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;->actual:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

.method public toArray()[Ljava/lang/Object;
    .locals 1

    .line 1
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;->actual:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public toArray([Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<X:",
            "Ljava/lang/Object;",
            ">([TX;)[TX;"
        }
    .end annotation

    .line 2
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;->actual:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public toList()Lorg/apache/pdfbox/cos/COSArray;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;->array:Lorg/apache/pdfbox/cos/COSArray;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "COSArrayList{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;->array:Lorg/apache/pdfbox/cos/COSArray;

    invoke-virtual {v1}, Lorg/apache/pdfbox/cos/COSArray;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

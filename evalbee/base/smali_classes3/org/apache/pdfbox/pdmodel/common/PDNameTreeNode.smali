.class public Lorg/apache/pdfbox/pdmodel/common/PDNameTreeNode;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/pdfbox/pdmodel/common/COSObjectable;


# instance fields
.field private final node:Lorg/apache/pdfbox/cos/COSDictionary;

.field private parent:Lorg/apache/pdfbox/pdmodel/common/PDNameTreeNode;

.field private valueType:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class<",
            "+",
            "Lorg/apache/pdfbox/pdmodel/common/COSObjectable;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/Class;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class<",
            "+",
            "Lorg/apache/pdfbox/pdmodel/common/COSObjectable;",
            ">;)V"
        }
    .end annotation

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/PDNameTreeNode;->valueType:Ljava/lang/Class;

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/PDNameTreeNode;->parent:Lorg/apache/pdfbox/pdmodel/common/PDNameTreeNode;

    new-instance v0, Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-direct {v0}, Lorg/apache/pdfbox/cos/COSDictionary;-><init>()V

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/PDNameTreeNode;->node:Lorg/apache/pdfbox/cos/COSDictionary;

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/common/PDNameTreeNode;->valueType:Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>(Lorg/apache/pdfbox/cos/COSDictionary;Ljava/lang/Class;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/pdfbox/cos/COSDictionary;",
            "Ljava/lang/Class<",
            "+",
            "Lorg/apache/pdfbox/pdmodel/common/COSObjectable;",
            ">;)V"
        }
    .end annotation

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/PDNameTreeNode;->parent:Lorg/apache/pdfbox/pdmodel/common/PDNameTreeNode;

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/common/PDNameTreeNode;->node:Lorg/apache/pdfbox/cos/COSDictionary;

    iput-object p2, p0, Lorg/apache/pdfbox/pdmodel/common/PDNameTreeNode;->valueType:Ljava/lang/Class;

    return-void
.end method

.method private calculateLimits()V
    .locals 4

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/common/PDNameTreeNode;->isRootNode()Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/PDNameTreeNode;->node:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v2, Lorg/apache/pdfbox/cos/COSName;->LIMITS:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v2, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/common/PDNameTreeNode;->getKids()Ljava/util/List;

    move-result-object v0

    const/4 v2, 0x0

    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/pdfbox/pdmodel/common/PDNameTreeNode;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/pdmodel/common/PDNameTreeNode;

    invoke-virtual {v1}, Lorg/apache/pdfbox/pdmodel/common/PDNameTreeNode;->getLowerLimit()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lorg/apache/pdfbox/pdmodel/common/PDNameTreeNode;->setLowerLimit(Ljava/lang/String;)V

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/common/PDNameTreeNode;->getUpperLimit()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/pdmodel/common/PDNameTreeNode;->setUpperLimit(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    :try_start_0
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/common/PDNameTreeNode;->getNames()Ljava/util/Map;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v3

    if-lez v3, :cond_2

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->toArray()[Ljava/lang/Object;

    move-result-object v0

    aget-object v2, v0, v2

    check-cast v2, Ljava/lang/String;

    invoke-direct {p0, v2}, Lorg/apache/pdfbox/pdmodel/common/PDNameTreeNode;->setLowerLimit(Ljava/lang/String;)V

    array-length v2, v0

    add-int/lit8 v2, v2, -0x1

    aget-object v0, v0, v2

    check-cast v0, Ljava/lang/String;

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/pdmodel/common/PDNameTreeNode;->setUpperLimit(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/PDNameTreeNode;->node:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v2, Lorg/apache/pdfbox/cos/COSName;->LIMITS:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v2, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    iget-object v2, p0, Lorg/apache/pdfbox/pdmodel/common/PDNameTreeNode;->node:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v3, Lorg/apache/pdfbox/cos/COSName;->LIMITS:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v2, v3, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    const-string v1, "PdfBoxAndroid"

    const-string v2, "Error while calculating the Limits of a PageNameTreeNode:"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :goto_0
    return-void
.end method

.method private setLowerLimit(Ljava/lang/String;)V
    .locals 3

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/PDNameTreeNode;->node:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->LIMITS:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSArray;

    if-nez v0, :cond_0

    new-instance v0, Lorg/apache/pdfbox/cos/COSArray;

    invoke-direct {v0}, Lorg/apache/pdfbox/cos/COSArray;-><init>()V

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    invoke-virtual {v0, v2}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    iget-object v2, p0, Lorg/apache/pdfbox/pdmodel/common/PDNameTreeNode;->node:Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-virtual {v2, v1, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    :cond_0
    const/4 v1, 0x0

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSArray;->setString(ILjava/lang/String;)V

    return-void
.end method

.method private setUpperLimit(Ljava/lang/String;)V
    .locals 3

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/PDNameTreeNode;->node:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->LIMITS:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSArray;

    if-nez v0, :cond_0

    new-instance v0, Lorg/apache/pdfbox/cos/COSArray;

    invoke-direct {v0}, Lorg/apache/pdfbox/cos/COSArray;-><init>()V

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    invoke-virtual {v0, v2}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    iget-object v2, p0, Lorg/apache/pdfbox/pdmodel/common/PDNameTreeNode;->node:Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-virtual {v2, v1, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    :cond_0
    const/4 v1, 0x1

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSArray;->setString(ILjava/lang/String;)V

    return-void
.end method


# virtual methods
.method public convertCOSToPD(Lorg/apache/pdfbox/cos/COSBase;)Lorg/apache/pdfbox/pdmodel/common/COSObjectable;
    .locals 0

    return-object p1
.end method

.method public createChildNode(Lorg/apache/pdfbox/cos/COSDictionary;)Lorg/apache/pdfbox/pdmodel/common/PDNameTreeNode;
    .locals 2

    new-instance v0, Lorg/apache/pdfbox/pdmodel/common/PDNameTreeNode;

    iget-object v1, p0, Lorg/apache/pdfbox/pdmodel/common/PDNameTreeNode;->valueType:Ljava/lang/Class;

    invoke-direct {v0, p1, v1}, Lorg/apache/pdfbox/pdmodel/common/PDNameTreeNode;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;Ljava/lang/Class;)V

    return-object v0
.end method

.method public getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/PDNameTreeNode;->node:Lorg/apache/pdfbox/cos/COSDictionary;

    return-object v0
.end method

.method public getCOSObject()Lorg/apache/pdfbox/cos/COSBase;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/PDNameTreeNode;->node:Lorg/apache/pdfbox/cos/COSDictionary;

    return-object v0
.end method

.method public getKids()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lorg/apache/pdfbox/pdmodel/common/PDNameTreeNode;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/PDNameTreeNode;->node:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->KIDS:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSArray;

    if-eqz v0, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    const/4 v2, 0x0

    :goto_0
    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSArray;->size()I

    move-result v3

    if-ge v2, v3, :cond_0

    invoke-virtual {v0, v2}, Lorg/apache/pdfbox/cos/COSArray;->getObject(I)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v3

    check-cast v3, Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-virtual {p0, v3}, Lorg/apache/pdfbox/pdmodel/common/PDNameTreeNode;->createChildNode(Lorg/apache/pdfbox/cos/COSDictionary;)Lorg/apache/pdfbox/pdmodel/common/PDNameTreeNode;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    new-instance v2, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;

    invoke-direct {v2, v1, v0}, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;-><init>(Ljava/util/List;Lorg/apache/pdfbox/cos/COSArray;)V

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    :goto_1
    return-object v2
.end method

.method public getLowerLimit()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/PDNameTreeNode;->node:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->LIMITS:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSArray;

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public getNames()Ljava/util/Map;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lorg/apache/pdfbox/pdmodel/common/COSObjectable;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/PDNameTreeNode;->node:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->NAMES:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSArray;

    if-eqz v0, :cond_1

    new-instance v1, Ljava/util/LinkedHashMap;

    invoke-direct {v1}, Ljava/util/LinkedHashMap;-><init>()V

    const/4 v2, 0x0

    :goto_0
    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSArray;->size()I

    move-result v3

    if-ge v2, v3, :cond_0

    invoke-virtual {v0, v2}, Lorg/apache/pdfbox/cos/COSArray;->getObject(I)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v3

    check-cast v3, Lorg/apache/pdfbox/cos/COSString;

    add-int/lit8 v4, v2, 0x1

    invoke-virtual {v0, v4}, Lorg/apache/pdfbox/cos/COSArray;->getObject(I)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v4

    invoke-virtual {v3}, Lorg/apache/pdfbox/cos/COSString;->getString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v4}, Lorg/apache/pdfbox/pdmodel/common/PDNameTreeNode;->convertCOSToPD(Lorg/apache/pdfbox/cos/COSBase;)Lorg/apache/pdfbox/pdmodel/common/COSObjectable;

    move-result-object v4

    invoke-interface {v1, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v2, v2, 0x2

    goto :goto_0

    :cond_0
    invoke-static {v1}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    return-object v0

    :cond_1
    const/4 v0, 0x0

    return-object v0
.end method

.method public getParent()Lorg/apache/pdfbox/pdmodel/common/PDNameTreeNode;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/PDNameTreeNode;->parent:Lorg/apache/pdfbox/pdmodel/common/PDNameTreeNode;

    return-object v0
.end method

.method public getUpperLimit()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/PDNameTreeNode;->node:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->LIMITS:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSArray;

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public getValue(Ljava/lang/String;)Ljava/lang/Object;
    .locals 5

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/common/PDNameTreeNode;->getNames()Ljava/util/Map;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    goto :goto_1

    :cond_0
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/common/PDNameTreeNode;->getKids()Ljava/util/List;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_2

    const/4 v2, 0x0

    :goto_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    if-ge v2, v3, :cond_3

    if-nez v1, :cond_3

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/pdfbox/pdmodel/common/PDNameTreeNode;

    invoke-virtual {v3}, Lorg/apache/pdfbox/pdmodel/common/PDNameTreeNode;->getLowerLimit()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v4

    if-gtz v4, :cond_1

    invoke-virtual {v3}, Lorg/apache/pdfbox/pdmodel/common/PDNameTreeNode;->getUpperLimit()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v4

    if-ltz v4, :cond_1

    invoke-virtual {v3, p1}, Lorg/apache/pdfbox/pdmodel/common/PDNameTreeNode;->getValue(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    const-string p1, "PdfBoxAndroid"

    const-string v0, "NameTreeNode does not have \"names\" nor \"kids\" objects."

    invoke-static {p1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    move-object p1, v1

    :goto_1
    return-object p1
.end method

.method public isRootNode()Z
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/PDNameTreeNode;->parent:Lorg/apache/pdfbox/pdmodel/common/PDNameTreeNode;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public setKids(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "+",
            "Lorg/apache/pdfbox/pdmodel/common/PDNameTreeNode;",
            ">;)V"
        }
    .end annotation

    const/4 v0, 0x0

    if-eqz p1, :cond_1

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_1

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/pdfbox/pdmodel/common/PDNameTreeNode;

    invoke-virtual {v2, p0}, Lorg/apache/pdfbox/pdmodel/common/PDNameTreeNode;->setParent(Lorg/apache/pdfbox/pdmodel/common/PDNameTreeNode;)V

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lorg/apache/pdfbox/pdmodel/common/PDNameTreeNode;->node:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v2, Lorg/apache/pdfbox/cos/COSName;->KIDS:Lorg/apache/pdfbox/cos/COSName;

    invoke-static {p1}, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;->converterToCOSArray(Ljava/util/List;)Lorg/apache/pdfbox/cos/COSArray;

    move-result-object p1

    invoke-virtual {v1, v2, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/common/PDNameTreeNode;->isRootNode()Z

    move-result p1

    if-eqz p1, :cond_2

    iget-object p1, p0, Lorg/apache/pdfbox/pdmodel/common/PDNameTreeNode;->node:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->NAMES:Lorg/apache/pdfbox/cos/COSName;

    goto :goto_1

    :cond_1
    iget-object p1, p0, Lorg/apache/pdfbox/pdmodel/common/PDNameTreeNode;->node:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->KIDS:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p1, v1, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    iget-object p1, p0, Lorg/apache/pdfbox/pdmodel/common/PDNameTreeNode;->node:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->LIMITS:Lorg/apache/pdfbox/cos/COSName;

    :goto_1
    invoke-virtual {p1, v1, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    :cond_2
    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/common/PDNameTreeNode;->calculateLimits()V

    return-void
.end method

.method public setNames(Ljava/util/Map;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "+",
            "Lorg/apache/pdfbox/pdmodel/common/COSObjectable;",
            ">;)V"
        }
    .end annotation

    if-nez p1, :cond_0

    iget-object p1, p0, Lorg/apache/pdfbox/pdmodel/common/PDNameTreeNode;->node:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->NAMES:Lorg/apache/pdfbox/cos/COSName;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)V

    iget-object p1, p0, Lorg/apache/pdfbox/pdmodel/common/PDNameTreeNode;->node:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->LIMITS:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p1, v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)V

    goto :goto_1

    :cond_0
    new-instance v0, Lorg/apache/pdfbox/cos/COSArray;

    invoke-direct {v0}, Lorg/apache/pdfbox/cos/COSArray;-><init>()V

    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-static {v1}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    new-instance v3, Lorg/apache/pdfbox/cos/COSString;

    invoke-direct {v3, v2}, Lorg/apache/pdfbox/cos/COSString;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    invoke-interface {p1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/pdfbox/pdmodel/common/COSObjectable;

    invoke-virtual {v0, v2}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)V

    goto :goto_0

    :cond_1
    iget-object p1, p0, Lorg/apache/pdfbox/pdmodel/common/PDNameTreeNode;->node:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->NAMES:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p1, v1, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/common/PDNameTreeNode;->calculateLimits()V

    :goto_1
    return-void
.end method

.method public setParent(Lorg/apache/pdfbox/pdmodel/common/PDNameTreeNode;)V
    .locals 0

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/common/PDNameTreeNode;->parent:Lorg/apache/pdfbox/pdmodel/common/PDNameTreeNode;

    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/common/PDNameTreeNode;->calculateLimits()V

    return-void
.end method

.class public Lorg/apache/pdfbox/pdmodel/common/function/PDFunctionTypeIdentity;
.super Lorg/apache/pdfbox/pdmodel/common/function/PDFunction;
.source "SourceFile"


# direct methods
.method public constructor <init>(Lorg/apache/pdfbox/cos/COSBase;)V
    .locals 0

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/common/function/PDFunction;-><init>(Lorg/apache/pdfbox/cos/COSBase;)V

    return-void
.end method


# virtual methods
.method public eval([F)[F
    .locals 0

    return-object p1
.end method

.method public getFunctionType()I
    .locals 1

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    const-string v0, "FunctionTypeIdentity"

    return-object v0
.end method

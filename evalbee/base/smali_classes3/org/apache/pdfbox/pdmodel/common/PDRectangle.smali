.class public Lorg/apache/pdfbox/pdmodel/common/PDRectangle;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/pdfbox/pdmodel/common/COSObjectable;


# static fields
.field public static final A0:Lorg/apache/pdfbox/pdmodel/common/PDRectangle;

.field public static final A1:Lorg/apache/pdfbox/pdmodel/common/PDRectangle;

.field public static final A2:Lorg/apache/pdfbox/pdmodel/common/PDRectangle;

.field public static final A3:Lorg/apache/pdfbox/pdmodel/common/PDRectangle;

.field public static final A4:Lorg/apache/pdfbox/pdmodel/common/PDRectangle;

.field public static final A5:Lorg/apache/pdfbox/pdmodel/common/PDRectangle;

.field public static final A6:Lorg/apache/pdfbox/pdmodel/common/PDRectangle;

.field public static final LEGAL:Lorg/apache/pdfbox/pdmodel/common/PDRectangle;

.field public static final LETTER:Lorg/apache/pdfbox/pdmodel/common/PDRectangle;

.field private static final MM_PER_INCH:F = 2.8346457f

.field private static final POINTS_PER_INCH:F = 72.0f


# instance fields
.field private final rectArray:Lorg/apache/pdfbox/cos/COSArray;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    new-instance v0, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;

    const/high16 v1, 0x44460000    # 792.0f

    const/high16 v2, 0x44190000    # 612.0f

    invoke-direct {v0, v2, v1}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;-><init>(FF)V

    sput-object v0, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->LETTER:Lorg/apache/pdfbox/pdmodel/common/PDRectangle;

    new-instance v0, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;

    const/high16 v1, 0x447c0000    # 1008.0f

    invoke-direct {v0, v2, v1}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;-><init>(FF)V

    sput-object v0, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->LEGAL:Lorg/apache/pdfbox/pdmodel/common/PDRectangle;

    new-instance v0, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;

    const v1, 0x4552a64d

    const v2, 0x4514fefe

    invoke-direct {v0, v2, v1}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;-><init>(FF)V

    sput-object v0, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->A0:Lorg/apache/pdfbox/pdmodel/common/PDRectangle;

    new-instance v0, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;

    const v1, 0x44d278f2

    invoke-direct {v0, v1, v2}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;-><init>(FF)V

    sput-object v0, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->A1:Lorg/apache/pdfbox/pdmodel/common/PDRectangle;

    new-instance v0, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;

    const v2, 0x4494d1a4

    invoke-direct {v0, v2, v1}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;-><init>(FF)V

    sput-object v0, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->A2:Lorg/apache/pdfbox/pdmodel/common/PDRectangle;

    new-instance v0, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;

    const v1, 0x445278f2

    invoke-direct {v0, v1, v2}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;-><init>(FF)V

    sput-object v0, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->A3:Lorg/apache/pdfbox/pdmodel/common/PDRectangle;

    new-instance v0, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;

    const v2, 0x4414d1a4

    invoke-direct {v0, v2, v1}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;-><init>(FF)V

    sput-object v0, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->A4:Lorg/apache/pdfbox/pdmodel/common/PDRectangle;

    new-instance v0, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;

    const v1, 0x43d1c387

    invoke-direct {v0, v1, v2}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;-><init>(FF)V

    sput-object v0, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->A5:Lorg/apache/pdfbox/pdmodel/common/PDRectangle;

    new-instance v0, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;

    const v2, 0x4394d1a4

    invoke-direct {v0, v2, v1}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;-><init>(FF)V

    sput-object v0, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->A6:Lorg/apache/pdfbox/pdmodel/common/PDRectangle;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .line 1
    const/4 v0, 0x0

    invoke-direct {p0, v0, v0, v0, v0}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;-><init>(FFFF)V

    return-void
.end method

.method public constructor <init>(FF)V
    .locals 1

    .line 2
    const/4 v0, 0x0

    invoke-direct {p0, v0, v0, p1, p2}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;-><init>(FFFF)V

    return-void
.end method

.method public constructor <init>(FFFF)V
    .locals 2

    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lorg/apache/pdfbox/cos/COSArray;

    invoke-direct {v0}, Lorg/apache/pdfbox/cos/COSArray;-><init>()V

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->rectArray:Lorg/apache/pdfbox/cos/COSArray;

    new-instance v1, Lorg/apache/pdfbox/cos/COSFloat;

    invoke-direct {v1, p1}, Lorg/apache/pdfbox/cos/COSFloat;-><init>(F)V

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    new-instance v1, Lorg/apache/pdfbox/cos/COSFloat;

    invoke-direct {v1, p2}, Lorg/apache/pdfbox/cos/COSFloat;-><init>(F)V

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    new-instance v1, Lorg/apache/pdfbox/cos/COSFloat;

    add-float/2addr p1, p3

    invoke-direct {v1, p1}, Lorg/apache/pdfbox/cos/COSFloat;-><init>(F)V

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    new-instance p1, Lorg/apache/pdfbox/cos/COSFloat;

    add-float/2addr p2, p4

    invoke-direct {p1, p2}, Lorg/apache/pdfbox/cos/COSFloat;-><init>(F)V

    invoke-virtual {v0, p1}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    return-void
.end method

.method public constructor <init>(Lorg/apache/fontbox/util/BoundingBox;)V
    .locals 3

    .line 4
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lorg/apache/pdfbox/cos/COSArray;

    invoke-direct {v0}, Lorg/apache/pdfbox/cos/COSArray;-><init>()V

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->rectArray:Lorg/apache/pdfbox/cos/COSArray;

    new-instance v1, Lorg/apache/pdfbox/cos/COSFloat;

    invoke-virtual {p1}, Lorg/apache/fontbox/util/BoundingBox;->getLowerLeftX()F

    move-result v2

    invoke-direct {v1, v2}, Lorg/apache/pdfbox/cos/COSFloat;-><init>(F)V

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    new-instance v1, Lorg/apache/pdfbox/cos/COSFloat;

    invoke-virtual {p1}, Lorg/apache/fontbox/util/BoundingBox;->getLowerLeftY()F

    move-result v2

    invoke-direct {v1, v2}, Lorg/apache/pdfbox/cos/COSFloat;-><init>(F)V

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    new-instance v1, Lorg/apache/pdfbox/cos/COSFloat;

    invoke-virtual {p1}, Lorg/apache/fontbox/util/BoundingBox;->getUpperRightX()F

    move-result v2

    invoke-direct {v1, v2}, Lorg/apache/pdfbox/cos/COSFloat;-><init>(F)V

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    new-instance v1, Lorg/apache/pdfbox/cos/COSFloat;

    invoke-virtual {p1}, Lorg/apache/fontbox/util/BoundingBox;->getUpperRightY()F

    move-result p1

    invoke-direct {v1, p1}, Lorg/apache/pdfbox/cos/COSFloat;-><init>(F)V

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    return-void
.end method

.method public constructor <init>(Lorg/apache/pdfbox/cos/COSArray;)V
    .locals 8

    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Lorg/apache/pdfbox/cos/COSArray;->toFloatArray()[F

    move-result-object p1

    new-instance v0, Lorg/apache/pdfbox/cos/COSArray;

    invoke-direct {v0}, Lorg/apache/pdfbox/cos/COSArray;-><init>()V

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->rectArray:Lorg/apache/pdfbox/cos/COSArray;

    new-instance v1, Lorg/apache/pdfbox/cos/COSFloat;

    const/4 v2, 0x0

    aget v3, p1, v2

    const/4 v4, 0x2

    aget v5, p1, v4

    invoke-static {v3, v5}, Ljava/lang/Math;->min(FF)F

    move-result v3

    invoke-direct {v1, v3}, Lorg/apache/pdfbox/cos/COSFloat;-><init>(F)V

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    new-instance v1, Lorg/apache/pdfbox/cos/COSFloat;

    const/4 v3, 0x1

    aget v5, p1, v3

    const/4 v6, 0x3

    aget v7, p1, v6

    invoke-static {v5, v7}, Ljava/lang/Math;->min(FF)F

    move-result v5

    invoke-direct {v1, v5}, Lorg/apache/pdfbox/cos/COSFloat;-><init>(F)V

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    new-instance v1, Lorg/apache/pdfbox/cos/COSFloat;

    aget v2, p1, v2

    aget v4, p1, v4

    invoke-static {v2, v4}, Ljava/lang/Math;->max(FF)F

    move-result v2

    invoke-direct {v1, v2}, Lorg/apache/pdfbox/cos/COSFloat;-><init>(F)V

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    new-instance v1, Lorg/apache/pdfbox/cos/COSFloat;

    aget v2, p1, v3

    aget p1, p1, v6

    invoke-static {v2, p1}, Ljava/lang/Math;->max(FF)F

    move-result p1

    invoke-direct {v1, p1}, Lorg/apache/pdfbox/cos/COSFloat;-><init>(F)V

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    return-void
.end method


# virtual methods
.method public contains(FF)Z
    .locals 4

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->getLowerLeftX()F

    move-result v0

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->getUpperRightX()F

    move-result v1

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->getLowerLeftY()F

    move-result v2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->getUpperRightY()F

    move-result v3

    cmpl-float v0, p1, v0

    if-ltz v0, :cond_0

    cmpg-float p1, p1, v1

    if-gtz p1, :cond_0

    cmpl-float p1, p2, v2

    if-ltz p1, :cond_0

    cmpg-float p1, p2, v3

    if-gtz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public createRetranslatedRectangle()Lorg/apache/pdfbox/pdmodel/common/PDRectangle;
    .locals 2

    new-instance v0, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;

    invoke-direct {v0}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;-><init>()V

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->getWidth()F

    move-result v1

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->setUpperRightX(F)V

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->getHeight()F

    move-result v1

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->setUpperRightY(F)V

    return-object v0
.end method

.method public getCOSArray()Lorg/apache/pdfbox/cos/COSArray;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->rectArray:Lorg/apache/pdfbox/cos/COSArray;

    return-object v0
.end method

.method public getCOSObject()Lorg/apache/pdfbox/cos/COSBase;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->rectArray:Lorg/apache/pdfbox/cos/COSArray;

    return-object v0
.end method

.method public getHeight()F
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->getUpperRightY()F

    move-result v0

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->getLowerLeftY()F

    move-result v1

    sub-float/2addr v0, v1

    return v0
.end method

.method public getLowerLeftX()F
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->rectArray:Lorg/apache/pdfbox/cos/COSArray;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSArray;->get(I)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSNumber;

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSNumber;->floatValue()F

    move-result v0

    return v0
.end method

.method public getLowerLeftY()F
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->rectArray:Lorg/apache/pdfbox/cos/COSArray;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSArray;->get(I)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSNumber;

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSNumber;->floatValue()F

    move-result v0

    return v0
.end method

.method public getUpperRightX()F
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->rectArray:Lorg/apache/pdfbox/cos/COSArray;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSArray;->get(I)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSNumber;

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSNumber;->floatValue()F

    move-result v0

    return v0
.end method

.method public getUpperRightY()F
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->rectArray:Lorg/apache/pdfbox/cos/COSArray;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSArray;->get(I)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSNumber;

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSNumber;->floatValue()F

    move-result v0

    return v0
.end method

.method public getWidth()F
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->getUpperRightX()F

    move-result v0

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->getLowerLeftX()F

    move-result v1

    sub-float/2addr v0, v1

    return v0
.end method

.method public move(FF)V
    .locals 1

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->getUpperRightX()F

    move-result v0

    add-float/2addr v0, p1

    invoke-virtual {p0, v0}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->setUpperRightX(F)V

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->getLowerLeftX()F

    move-result v0

    add-float/2addr v0, p1

    invoke-virtual {p0, v0}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->setLowerLeftX(F)V

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->getUpperRightY()F

    move-result p1

    add-float/2addr p1, p2

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->setUpperRightY(F)V

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->getLowerLeftY()F

    move-result p1

    add-float/2addr p1, p2

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->setLowerLeftY(F)V

    return-void
.end method

.method public setLowerLeftX(F)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->rectArray:Lorg/apache/pdfbox/cos/COSArray;

    new-instance v1, Lorg/apache/pdfbox/cos/COSFloat;

    invoke-direct {v1, p1}, Lorg/apache/pdfbox/cos/COSFloat;-><init>(F)V

    const/4 p1, 0x0

    invoke-virtual {v0, p1, v1}, Lorg/apache/pdfbox/cos/COSArray;->set(ILorg/apache/pdfbox/cos/COSBase;)V

    return-void
.end method

.method public setLowerLeftY(F)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->rectArray:Lorg/apache/pdfbox/cos/COSArray;

    new-instance v1, Lorg/apache/pdfbox/cos/COSFloat;

    invoke-direct {v1, p1}, Lorg/apache/pdfbox/cos/COSFloat;-><init>(F)V

    const/4 p1, 0x1

    invoke-virtual {v0, p1, v1}, Lorg/apache/pdfbox/cos/COSArray;->set(ILorg/apache/pdfbox/cos/COSBase;)V

    return-void
.end method

.method public setUpperRightX(F)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->rectArray:Lorg/apache/pdfbox/cos/COSArray;

    new-instance v1, Lorg/apache/pdfbox/cos/COSFloat;

    invoke-direct {v1, p1}, Lorg/apache/pdfbox/cos/COSFloat;-><init>(F)V

    const/4 p1, 0x2

    invoke-virtual {v0, p1, v1}, Lorg/apache/pdfbox/cos/COSArray;->set(ILorg/apache/pdfbox/cos/COSBase;)V

    return-void
.end method

.method public setUpperRightY(F)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->rectArray:Lorg/apache/pdfbox/cos/COSArray;

    new-instance v1, Lorg/apache/pdfbox/cos/COSFloat;

    invoke-direct {v1, p1}, Lorg/apache/pdfbox/cos/COSFloat;-><init>(F)V

    const/4 p1, 0x3

    invoke-virtual {v0, p1, v1}, Lorg/apache/pdfbox/cos/COSArray;->set(ILorg/apache/pdfbox/cos/COSBase;)V

    return-void
.end method

.method public toGeneralPath()Landroid/graphics/Path;
    .locals 5

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->getLowerLeftX()F

    move-result v0

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->getLowerLeftY()F

    move-result v1

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->getUpperRightX()F

    move-result v2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->getUpperRightY()F

    move-result v3

    new-instance v4, Landroid/graphics/Path;

    invoke-direct {v4}, Landroid/graphics/Path;-><init>()V

    invoke-virtual {v4, v0, v1}, Landroid/graphics/Path;->moveTo(FF)V

    invoke-virtual {v4, v2, v1}, Landroid/graphics/Path;->lineTo(FF)V

    invoke-virtual {v4, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    invoke-virtual {v4, v0, v3}, Landroid/graphics/Path;->lineTo(FF)V

    invoke-virtual {v4}, Landroid/graphics/Path;->close()V

    return-object v4
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->getLowerLeftX()F

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->getLowerLeftY()F

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->getUpperRightX()F

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->getUpperRightY()F

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public transform(Lorg/apache/pdfbox/util/Matrix;)Landroid/graphics/Path;
    .locals 9

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->getLowerLeftX()F

    move-result v0

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->getLowerLeftY()F

    move-result v1

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->getUpperRightX()F

    move-result v2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->getUpperRightY()F

    move-result v3

    float-to-double v4, v0

    float-to-double v0, v1

    invoke-virtual {p1, v4, v5, v0, v1}, Lorg/apache/pdfbox/util/Matrix;->transformPoint(DD)Landroid/graphics/PointF;

    move-result-object v6

    float-to-double v7, v2

    invoke-virtual {p1, v7, v8, v0, v1}, Lorg/apache/pdfbox/util/Matrix;->transformPoint(DD)Landroid/graphics/PointF;

    move-result-object v0

    float-to-double v1, v3

    invoke-virtual {p1, v7, v8, v1, v2}, Lorg/apache/pdfbox/util/Matrix;->transformPoint(DD)Landroid/graphics/PointF;

    move-result-object v3

    invoke-virtual {p1, v4, v5, v1, v2}, Lorg/apache/pdfbox/util/Matrix;->transformPoint(DD)Landroid/graphics/PointF;

    move-result-object p1

    new-instance v1, Landroid/graphics/Path;

    invoke-direct {v1}, Landroid/graphics/Path;-><init>()V

    iget v2, v6, Landroid/graphics/PointF;->x:F

    iget v4, v6, Landroid/graphics/PointF;->y:F

    invoke-virtual {v1, v2, v4}, Landroid/graphics/Path;->moveTo(FF)V

    iget v2, v0, Landroid/graphics/PointF;->x:F

    iget v0, v0, Landroid/graphics/PointF;->y:F

    invoke-virtual {v1, v2, v0}, Landroid/graphics/Path;->lineTo(FF)V

    iget v0, v3, Landroid/graphics/PointF;->x:F

    iget v2, v3, Landroid/graphics/PointF;->y:F

    invoke-virtual {v1, v0, v2}, Landroid/graphics/Path;->lineTo(FF)V

    iget v0, p1, Landroid/graphics/PointF;->x:F

    iget p1, p1, Landroid/graphics/PointF;->y:F

    invoke-virtual {v1, v0, p1}, Landroid/graphics/Path;->lineTo(FF)V

    invoke-virtual {v1}, Landroid/graphics/Path;->close()V

    return-object v1
.end method

.class public Lorg/apache/pdfbox/pdmodel/common/PDTextStream;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/pdfbox/pdmodel/common/COSObjectable;


# instance fields
.field private stream:Lorg/apache/pdfbox/cos/COSStream;

.field private string:Lorg/apache/pdfbox/cos/COSString;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lorg/apache/pdfbox/cos/COSString;

    invoke-direct {v0, p1}, Lorg/apache/pdfbox/cos/COSString;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/PDTextStream;->string:Lorg/apache/pdfbox/cos/COSString;

    return-void
.end method

.method public constructor <init>(Lorg/apache/pdfbox/cos/COSStream;)V
    .locals 0

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/common/PDTextStream;->stream:Lorg/apache/pdfbox/cos/COSStream;

    return-void
.end method

.method public constructor <init>(Lorg/apache/pdfbox/cos/COSString;)V
    .locals 0

    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/common/PDTextStream;->string:Lorg/apache/pdfbox/cos/COSString;

    return-void
.end method

.method public static createTextStream(Lorg/apache/pdfbox/cos/COSBase;)Lorg/apache/pdfbox/pdmodel/common/PDTextStream;
    .locals 1

    instance-of v0, p0, Lorg/apache/pdfbox/cos/COSString;

    if-eqz v0, :cond_0

    new-instance v0, Lorg/apache/pdfbox/pdmodel/common/PDTextStream;

    check-cast p0, Lorg/apache/pdfbox/cos/COSString;

    invoke-direct {v0, p0}, Lorg/apache/pdfbox/pdmodel/common/PDTextStream;-><init>(Lorg/apache/pdfbox/cos/COSString;)V

    goto :goto_0

    :cond_0
    instance-of v0, p0, Lorg/apache/pdfbox/cos/COSStream;

    if-eqz v0, :cond_1

    new-instance v0, Lorg/apache/pdfbox/pdmodel/common/PDTextStream;

    check-cast p0, Lorg/apache/pdfbox/cos/COSStream;

    invoke-direct {v0, p0}, Lorg/apache/pdfbox/pdmodel/common/PDTextStream;-><init>(Lorg/apache/pdfbox/cos/COSStream;)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method


# virtual methods
.method public getAsStream()Ljava/io/InputStream;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/PDTextStream;->string:Lorg/apache/pdfbox/cos/COSString;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/io/ByteArrayInputStream;

    iget-object v1, p0, Lorg/apache/pdfbox/pdmodel/common/PDTextStream;->string:Lorg/apache/pdfbox/cos/COSString;

    invoke-virtual {v1}, Lorg/apache/pdfbox/cos/COSString;->getBytes()[B

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/PDTextStream;->stream:Lorg/apache/pdfbox/cos/COSStream;

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSStream;->getUnfilteredStream()Ljava/io/InputStream;

    move-result-object v0

    :goto_0
    return-object v0
.end method

.method public getAsString()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/PDTextStream;->string:Lorg/apache/pdfbox/cos/COSString;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSString;->getString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    iget-object v1, p0, Lorg/apache/pdfbox/pdmodel/common/PDTextStream;->stream:Lorg/apache/pdfbox/cos/COSStream;

    invoke-virtual {v1}, Lorg/apache/pdfbox/cos/COSStream;->getUnfilteredStream()Ljava/io/InputStream;

    move-result-object v1

    invoke-static {v1, v0}, Lorg/apache/pdfbox/io/IOUtils;->copy(Ljava/io/InputStream;Ljava/io/OutputStream;)J

    invoke-static {v1}, Lorg/apache/pdfbox/io/IOUtils;->closeQuietly(Ljava/io/Closeable;)V

    new-instance v1, Ljava/lang/String;

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    const-string v2, "ISO-8859-1"

    invoke-direct {v1, v0, v2}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    return-object v1
.end method

.method public getCOSObject()Lorg/apache/pdfbox/cos/COSBase;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/PDTextStream;->string:Lorg/apache/pdfbox/cos/COSString;

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/PDTextStream;->stream:Lorg/apache/pdfbox/cos/COSStream;

    :cond_0
    return-object v0
.end method

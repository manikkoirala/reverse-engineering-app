.class public Lorg/apache/pdfbox/pdmodel/common/COSStreamArray;
.super Lorg/apache/pdfbox/cos/COSStream;
.source "SourceFile"


# instance fields
.field private firstStream:Lorg/apache/pdfbox/cos/COSStream;

.field private streams:Lorg/apache/pdfbox/cos/COSArray;


# direct methods
.method public constructor <init>(Lorg/apache/pdfbox/cos/COSArray;)V
    .locals 1

    new-instance v0, Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-direct {v0}, Lorg/apache/pdfbox/cos/COSDictionary;-><init>()V

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/cos/COSStream;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/common/COSStreamArray;->streams:Lorg/apache/pdfbox/cos/COSArray;

    invoke-virtual {p1}, Lorg/apache/pdfbox/cos/COSArray;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lorg/apache/pdfbox/cos/COSArray;->getObject(I)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object p1

    check-cast p1, Lorg/apache/pdfbox/cos/COSStream;

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/common/COSStreamArray;->firstStream:Lorg/apache/pdfbox/cos/COSStream;

    :cond_0
    return-void
.end method


# virtual methods
.method public accept(Lorg/apache/pdfbox/cos/ICOSVisitor;)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/COSStreamArray;->streams:Lorg/apache/pdfbox/cos/COSArray;

    invoke-virtual {v0, p1}, Lorg/apache/pdfbox/cos/COSArray;->accept(Lorg/apache/pdfbox/cos/ICOSVisitor;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public appendStream(Lorg/apache/pdfbox/cos/COSStream;)V
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/COSStreamArray;->streams:Lorg/apache/pdfbox/cos/COSArray;

    invoke-virtual {v0, p1}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    return-void
.end method

.method public createFilteredStream()Ljava/io/OutputStream;
    .locals 1

    .line 1
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/COSStreamArray;->firstStream:Lorg/apache/pdfbox/cos/COSStream;

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSStream;->createFilteredStream()Ljava/io/OutputStream;

    move-result-object v0

    return-object v0
.end method

.method public createFilteredStream(Lorg/apache/pdfbox/cos/COSBase;)Ljava/io/OutputStream;
    .locals 1

    .line 2
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/COSStreamArray;->firstStream:Lorg/apache/pdfbox/cos/COSStream;

    invoke-virtual {v0, p1}, Lorg/apache/pdfbox/cos/COSStream;->createFilteredStream(Lorg/apache/pdfbox/cos/COSBase;)Ljava/io/OutputStream;

    move-result-object p1

    return-object p1
.end method

.method public createUnfilteredStream()Ljava/io/OutputStream;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/COSStreamArray;->firstStream:Lorg/apache/pdfbox/cos/COSStream;

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSStream;->createUnfilteredStream()Ljava/io/OutputStream;

    move-result-object v0

    return-object v0
.end method

.method public get(I)Lorg/apache/pdfbox/cos/COSBase;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/COSStreamArray;->streams:Lorg/apache/pdfbox/cos/COSArray;

    invoke-virtual {v0, p1}, Lorg/apache/pdfbox/cos/COSArray;->get(I)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object p1

    return-object p1
.end method

.method public getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/COSStreamArray;->firstStream:Lorg/apache/pdfbox/cos/COSStream;

    return-object v0
.end method

.method public getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/COSStreamArray;->firstStream:Lorg/apache/pdfbox/cos/COSStream;

    invoke-virtual {v0, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object p1

    return-object p1
.end method

.method public getFilteredStream()Ljava/io/InputStream;
    .locals 2

    new-instance v0, Ljava/io/IOException;

    const-string v1, "Error: Not allowed to get filtered stream from array of streams."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getFilters()Lorg/apache/pdfbox/cos/COSBase;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/COSStreamArray;->firstStream:Lorg/apache/pdfbox/cos/COSStream;

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSStream;->getFilters()Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    return-object v0
.end method

.method public getItem(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/COSStreamArray;->firstStream:Lorg/apache/pdfbox/cos/COSStream;

    invoke-virtual {v0, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->getItem(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object p1

    return-object p1
.end method

.method public getStreamCount()I
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/COSStreamArray;->streams:Lorg/apache/pdfbox/cos/COSArray;

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSArray;->size()I

    move-result v0

    return v0
.end method

.method public getUnfilteredStream()Ljava/io/InputStream;
    .locals 4

    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    const-string v1, "\n"

    const-string v2, "ISO-8859-1"

    invoke-virtual {v1, v2}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v1

    const/4 v2, 0x0

    :goto_0
    iget-object v3, p0, Lorg/apache/pdfbox/pdmodel/common/COSStreamArray;->streams:Lorg/apache/pdfbox/cos/COSArray;

    invoke-virtual {v3}, Lorg/apache/pdfbox/cos/COSArray;->size()I

    move-result v3

    if-ge v2, v3, :cond_0

    iget-object v3, p0, Lorg/apache/pdfbox/pdmodel/common/COSStreamArray;->streams:Lorg/apache/pdfbox/cos/COSArray;

    invoke-virtual {v3, v2}, Lorg/apache/pdfbox/cos/COSArray;->getObject(I)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v3

    check-cast v3, Lorg/apache/pdfbox/cos/COSStream;

    invoke-virtual {v3}, Lorg/apache/pdfbox/cos/COSStream;->getUnfilteredStream()Ljava/io/InputStream;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    new-instance v3, Ljava/io/ByteArrayInputStream;

    invoke-direct {v3, v1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-virtual {v0, v3}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    new-instance v1, Ljava/io/SequenceInputStream;

    invoke-virtual {v0}, Ljava/util/Vector;->elements()Ljava/util/Enumeration;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/SequenceInputStream;-><init>(Ljava/util/Enumeration;)V

    return-object v1
.end method

.method public insertCOSStream(Lorg/apache/pdfbox/pdmodel/common/PDStream;)V
    .locals 1

    new-instance v0, Lorg/apache/pdfbox/cos/COSArray;

    invoke-direct {v0}, Lorg/apache/pdfbox/cos/COSArray;-><init>()V

    invoke-virtual {v0, p1}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)V

    iget-object p1, p0, Lorg/apache/pdfbox/pdmodel/common/COSStreamArray;->streams:Lorg/apache/pdfbox/cos/COSArray;

    invoke-virtual {v0, p1}, Lorg/apache/pdfbox/cos/COSArray;->addAll(Lorg/apache/pdfbox/cos/COSArray;)V

    iget-object p1, p0, Lorg/apache/pdfbox/pdmodel/common/COSStreamArray;->streams:Lorg/apache/pdfbox/cos/COSArray;

    invoke-virtual {p1}, Lorg/apache/pdfbox/cos/COSArray;->clear()V

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/COSStreamArray;->streams:Lorg/apache/pdfbox/cos/COSArray;

    return-void
.end method

.method public setFilters(Lorg/apache/pdfbox/cos/COSBase;)V
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/COSStreamArray;->firstStream:Lorg/apache/pdfbox/cos/COSStream;

    invoke-virtual {v0, p1}, Lorg/apache/pdfbox/cos/COSStream;->setFilters(Lorg/apache/pdfbox/cos/COSBase;)V

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    const-string v0, "COSStream{}"

    return-object v0
.end method

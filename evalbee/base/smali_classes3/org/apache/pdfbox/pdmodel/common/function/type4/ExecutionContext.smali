.class public Lorg/apache/pdfbox/pdmodel/common/function/type4/ExecutionContext;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private operators:Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;

.field private stack:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/function/type4/ExecutionContext;->stack:Ljava/util/Stack;

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/common/function/type4/ExecutionContext;->operators:Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;

    return-void
.end method


# virtual methods
.method public getOperators()Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/function/type4/ExecutionContext;->operators:Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;

    return-object v0
.end method

.method public getStack()Ljava/util/Stack;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Stack<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/function/type4/ExecutionContext;->stack:Ljava/util/Stack;

    return-object v0
.end method

.method public popInt()I
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/function/type4/ExecutionContext;->stack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public popNumber()Ljava/lang/Number;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/function/type4/ExecutionContext;->stack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    return-object v0
.end method

.method public popReal()F
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/function/type4/ExecutionContext;->stack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->floatValue()F

    move-result v0

    return v0
.end method

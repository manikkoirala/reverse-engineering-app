.class public Lorg/apache/pdfbox/pdmodel/common/PDRange;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/pdfbox/pdmodel/common/COSObjectable;


# instance fields
.field private rangeArray:Lorg/apache/pdfbox/cos/COSArray;

.field private startingIndex:I


# direct methods
.method public constructor <init>()V
    .locals 3

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lorg/apache/pdfbox/cos/COSArray;

    invoke-direct {v0}, Lorg/apache/pdfbox/cos/COSArray;-><init>()V

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/PDRange;->rangeArray:Lorg/apache/pdfbox/cos/COSArray;

    new-instance v1, Lorg/apache/pdfbox/cos/COSFloat;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lorg/apache/pdfbox/cos/COSFloat;-><init>(F)V

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/PDRange;->rangeArray:Lorg/apache/pdfbox/cos/COSArray;

    new-instance v1, Lorg/apache/pdfbox/cos/COSFloat;

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-direct {v1, v2}, Lorg/apache/pdfbox/cos/COSFloat;-><init>(F)V

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/pdfbox/pdmodel/common/PDRange;->startingIndex:I

    return-void
.end method

.method public constructor <init>(Lorg/apache/pdfbox/cos/COSArray;)V
    .locals 0

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/common/PDRange;->rangeArray:Lorg/apache/pdfbox/cos/COSArray;

    return-void
.end method

.method public constructor <init>(Lorg/apache/pdfbox/cos/COSArray;I)V
    .locals 0

    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/common/PDRange;->rangeArray:Lorg/apache/pdfbox/cos/COSArray;

    iput p2, p0, Lorg/apache/pdfbox/pdmodel/common/PDRange;->startingIndex:I

    return-void
.end method


# virtual methods
.method public getCOSArray()Lorg/apache/pdfbox/cos/COSArray;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/PDRange;->rangeArray:Lorg/apache/pdfbox/cos/COSArray;

    return-object v0
.end method

.method public getCOSObject()Lorg/apache/pdfbox/cos/COSBase;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/PDRange;->rangeArray:Lorg/apache/pdfbox/cos/COSArray;

    return-object v0
.end method

.method public getMax()F
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/PDRange;->rangeArray:Lorg/apache/pdfbox/cos/COSArray;

    iget v1, p0, Lorg/apache/pdfbox/pdmodel/common/PDRange;->startingIndex:I

    mul-int/lit8 v1, v1, 0x2

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSArray;->getObject(I)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSNumber;

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSNumber;->floatValue()F

    move-result v0

    return v0
.end method

.method public getMin()F
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/PDRange;->rangeArray:Lorg/apache/pdfbox/cos/COSArray;

    iget v1, p0, Lorg/apache/pdfbox/pdmodel/common/PDRange;->startingIndex:I

    mul-int/lit8 v1, v1, 0x2

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSArray;->getObject(I)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSNumber;

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSNumber;->floatValue()F

    move-result v0

    return v0
.end method

.method public setMax(F)V
    .locals 3

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/PDRange;->rangeArray:Lorg/apache/pdfbox/cos/COSArray;

    iget v1, p0, Lorg/apache/pdfbox/pdmodel/common/PDRange;->startingIndex:I

    mul-int/lit8 v1, v1, 0x2

    add-int/lit8 v1, v1, 0x1

    new-instance v2, Lorg/apache/pdfbox/cos/COSFloat;

    invoke-direct {v2, p1}, Lorg/apache/pdfbox/cos/COSFloat;-><init>(F)V

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSArray;->set(ILorg/apache/pdfbox/cos/COSBase;)V

    return-void
.end method

.method public setMin(F)V
    .locals 3

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/PDRange;->rangeArray:Lorg/apache/pdfbox/cos/COSArray;

    iget v1, p0, Lorg/apache/pdfbox/pdmodel/common/PDRange;->startingIndex:I

    mul-int/lit8 v1, v1, 0x2

    new-instance v2, Lorg/apache/pdfbox/cos/COSFloat;

    invoke-direct {v2, p1}, Lorg/apache/pdfbox/cos/COSFloat;-><init>(F)V

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSArray;->set(ILorg/apache/pdfbox/cos/COSBase;)V

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "PDRange{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/common/PDRange;->getMin()F

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/common/PDRange;->getMax()F

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

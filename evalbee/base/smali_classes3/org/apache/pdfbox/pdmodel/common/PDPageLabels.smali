.class public Lorg/apache/pdfbox/pdmodel/common/PDPageLabels;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/pdfbox/pdmodel/common/COSObjectable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/pdfbox/pdmodel/common/PDPageLabels$LabelGenerator;,
        Lorg/apache/pdfbox/pdmodel/common/PDPageLabels$LabelHandler;
    }
.end annotation


# instance fields
.field private doc:Lorg/apache/pdfbox/pdmodel/PDDocument;

.field private labels:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Lorg/apache/pdfbox/pdmodel/common/PDPageLabelRange;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lorg/apache/pdfbox/pdmodel/PDDocument;)V
    .locals 2

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0}, Ljava/util/TreeMap;-><init>()V

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/PDPageLabels;->labels:Ljava/util/Map;

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/common/PDPageLabels;->doc:Lorg/apache/pdfbox/pdmodel/PDDocument;

    new-instance p1, Lorg/apache/pdfbox/pdmodel/common/PDPageLabelRange;

    invoke-direct {p1}, Lorg/apache/pdfbox/pdmodel/common/PDPageLabelRange;-><init>()V

    const-string v0, "D"

    invoke-virtual {p1, v0}, Lorg/apache/pdfbox/pdmodel/common/PDPageLabelRange;->setStyle(Ljava/lang/String;)V

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/PDPageLabels;->labels:Ljava/util/Map;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Lorg/apache/pdfbox/pdmodel/PDDocument;Lorg/apache/pdfbox/cos/COSDictionary;)V
    .locals 1

    .line 2
    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/common/PDPageLabels;-><init>(Lorg/apache/pdfbox/pdmodel/PDDocument;)V

    if-nez p2, :cond_0

    return-void

    :cond_0
    new-instance p1, Lorg/apache/pdfbox/pdmodel/common/PDNumberTreeNode;

    const-class v0, Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-direct {p1, p2, v0}, Lorg/apache/pdfbox/pdmodel/common/PDNumberTreeNode;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;Ljava/lang/Class;)V

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/common/PDPageLabels;->findLabels(Lorg/apache/pdfbox/pdmodel/common/PDNumberTreeNode;)V

    return-void
.end method

.method public static synthetic access$000(Lorg/apache/pdfbox/pdmodel/common/PDPageLabels;)Lorg/apache/pdfbox/pdmodel/PDDocument;
    .locals 0

    iget-object p0, p0, Lorg/apache/pdfbox/pdmodel/common/PDPageLabels;->doc:Lorg/apache/pdfbox/pdmodel/PDDocument;

    return-object p0
.end method

.method private computeLabels(Lorg/apache/pdfbox/pdmodel/common/PDPageLabels$LabelHandler;)V
    .locals 6

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/PDPageLabels;->labels:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_0

    return-void

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    const/4 v2, 0x0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    sub-int/2addr v4, v5

    new-instance v5, Lorg/apache/pdfbox/pdmodel/common/PDPageLabels$LabelGenerator;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/pdfbox/pdmodel/common/PDPageLabelRange;

    invoke-direct {v5, v1, v4}, Lorg/apache/pdfbox/pdmodel/common/PDPageLabels$LabelGenerator;-><init>(Lorg/apache/pdfbox/pdmodel/common/PDPageLabelRange;I)V

    :goto_1
    invoke-virtual {v5}, Lorg/apache/pdfbox/pdmodel/common/PDPageLabels$LabelGenerator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v5}, Lorg/apache/pdfbox/pdmodel/common/PDPageLabels$LabelGenerator;->next()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v2, v1}, Lorg/apache/pdfbox/pdmodel/common/PDPageLabels$LabelHandler;->newLabel(ILjava/lang/String;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_1
    move-object v1, v3

    goto :goto_0

    :cond_2
    new-instance v0, Lorg/apache/pdfbox/pdmodel/common/PDPageLabels$LabelGenerator;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/pdfbox/pdmodel/common/PDPageLabelRange;

    iget-object v4, p0, Lorg/apache/pdfbox/pdmodel/common/PDPageLabels;->doc:Lorg/apache/pdfbox/pdmodel/PDDocument;

    invoke-virtual {v4}, Lorg/apache/pdfbox/pdmodel/PDDocument;->getNumberOfPages()I

    move-result v4

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    sub-int/2addr v4, v1

    invoke-direct {v0, v3, v4}, Lorg/apache/pdfbox/pdmodel/common/PDPageLabels$LabelGenerator;-><init>(Lorg/apache/pdfbox/pdmodel/common/PDPageLabelRange;I)V

    :goto_2
    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/common/PDPageLabels$LabelGenerator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/common/PDPageLabels$LabelGenerator;->next()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1, v2, v1}, Lorg/apache/pdfbox/pdmodel/common/PDPageLabels$LabelHandler;->newLabel(ILjava/lang/String;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_3
    return-void
.end method

.method private findLabels(Lorg/apache/pdfbox/pdmodel/common/PDNumberTreeNode;)V
    .locals 4

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/common/PDNumberTreeNode;->getKids()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/common/PDNumberTreeNode;->getKids()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/pdmodel/common/PDNumberTreeNode;

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/pdmodel/common/PDPageLabels;->findLabels(Lorg/apache/pdfbox/pdmodel/common/PDNumberTreeNode;)V

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/common/PDNumberTreeNode;->getNumbers()Ljava/util/Map;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/common/PDNumberTreeNode;->getNumbers()Ljava/util/Map;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_1
    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ltz v1, :cond_1

    iget-object v1, p0, Lorg/apache/pdfbox/pdmodel/common/PDPageLabels;->labels:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    new-instance v3, Lorg/apache/pdfbox/pdmodel/common/PDPageLabelRange;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-direct {v3, v0}, Lorg/apache/pdfbox/pdmodel/common/PDPageLabelRange;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_2
    return-void
.end method


# virtual methods
.method public getCOSObject()Lorg/apache/pdfbox/cos/COSBase;
    .locals 6

    new-instance v0, Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-direct {v0}, Lorg/apache/pdfbox/cos/COSDictionary;-><init>()V

    new-instance v1, Lorg/apache/pdfbox/cos/COSArray;

    invoke-direct {v1}, Lorg/apache/pdfbox/cos/COSArray;-><init>()V

    iget-object v2, p0, Lorg/apache/pdfbox/pdmodel/common/PDPageLabels;->labels:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    int-to-long v4, v4

    invoke-static {v4, v5}, Lorg/apache/pdfbox/cos/COSInteger;->get(J)Lorg/apache/pdfbox/cos/COSInteger;

    move-result-object v4

    invoke-virtual {v1, v4}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/pdfbox/pdmodel/common/COSObjectable;

    invoke-virtual {v1, v3}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)V

    goto :goto_0

    :cond_0
    sget-object v2, Lorg/apache/pdfbox/cos/COSName;->NUMS:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v2, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    return-object v0
.end method

.method public getLabelsByPageIndices()[Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/PDPageLabels;->doc:Lorg/apache/pdfbox/pdmodel/PDDocument;

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/PDDocument;->getNumberOfPages()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    new-instance v1, Lorg/apache/pdfbox/pdmodel/common/PDPageLabels$2;

    invoke-direct {v1, p0, v0}, Lorg/apache/pdfbox/pdmodel/common/PDPageLabels$2;-><init>(Lorg/apache/pdfbox/pdmodel/common/PDPageLabels;[Ljava/lang/String;)V

    invoke-direct {p0, v1}, Lorg/apache/pdfbox/pdmodel/common/PDPageLabels;->computeLabels(Lorg/apache/pdfbox/pdmodel/common/PDPageLabels$LabelHandler;)V

    return-object v0
.end method

.method public getPageIndicesByLabels()Ljava/util/Map;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/HashMap;

    iget-object v1, p0, Lorg/apache/pdfbox/pdmodel/common/PDPageLabels;->doc:Lorg/apache/pdfbox/pdmodel/PDDocument;

    invoke-virtual {v1}, Lorg/apache/pdfbox/pdmodel/PDDocument;->getNumberOfPages()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    new-instance v1, Lorg/apache/pdfbox/pdmodel/common/PDPageLabels$1;

    invoke-direct {v1, p0, v0}, Lorg/apache/pdfbox/pdmodel/common/PDPageLabels$1;-><init>(Lorg/apache/pdfbox/pdmodel/common/PDPageLabels;Ljava/util/Map;)V

    invoke-direct {p0, v1}, Lorg/apache/pdfbox/pdmodel/common/PDPageLabels;->computeLabels(Lorg/apache/pdfbox/pdmodel/common/PDPageLabels$LabelHandler;)V

    return-object v0
.end method

.method public getPageLabelRange(I)Lorg/apache/pdfbox/pdmodel/common/PDPageLabelRange;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/PDPageLabels;->labels:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lorg/apache/pdfbox/pdmodel/common/PDPageLabelRange;

    return-object p1
.end method

.method public getPageRangeCount()I
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/PDPageLabels;->labels:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    return v0
.end method

.method public setLabelItem(ILorg/apache/pdfbox/pdmodel/common/PDPageLabelRange;)V
    .locals 1

    if-ltz p1, :cond_0

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/PDPageLabels;->labels:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void

    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "startPage parameter of setLabelItem may not be < 0"

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.class public Lorg/apache/pdfbox/pdmodel/common/PDStream;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/pdfbox/pdmodel/common/COSObjectable;


# instance fields
.field private stream:Lorg/apache/pdfbox/cos/COSStream;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(Lorg/apache/pdfbox/cos/COSStream;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/common/PDStream;->stream:Lorg/apache/pdfbox/cos/COSStream;

    return-void
.end method

.method public constructor <init>(Lorg/apache/pdfbox/pdmodel/PDDocument;)V
    .locals 0

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/PDDocument;->getDocument()Lorg/apache/pdfbox/cos/COSDocument;

    move-result-object p1

    invoke-virtual {p1}, Lorg/apache/pdfbox/cos/COSDocument;->createCOSStream()Lorg/apache/pdfbox/cos/COSStream;

    move-result-object p1

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/common/PDStream;->stream:Lorg/apache/pdfbox/cos/COSStream;

    return-void
.end method

.method public constructor <init>(Lorg/apache/pdfbox/pdmodel/PDDocument;Ljava/io/InputStream;)V
    .locals 1

    .line 3
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/pdfbox/pdmodel/common/PDStream;-><init>(Lorg/apache/pdfbox/pdmodel/PDDocument;Ljava/io/InputStream;Z)V

    return-void
.end method

.method public constructor <init>(Lorg/apache/pdfbox/pdmodel/PDDocument;Ljava/io/InputStream;Z)V
    .locals 2

    .line 4
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/PDDocument;->getDocument()Lorg/apache/pdfbox/cos/COSDocument;

    move-result-object p1

    invoke-virtual {p1}, Lorg/apache/pdfbox/cos/COSDocument;->createCOSStream()Lorg/apache/pdfbox/cos/COSStream;

    move-result-object p1

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/common/PDStream;->stream:Lorg/apache/pdfbox/cos/COSStream;

    if-eqz p3, :cond_0

    invoke-virtual {p1}, Lorg/apache/pdfbox/cos/COSStream;->createFilteredStream()Ljava/io/OutputStream;

    move-result-object p1

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Lorg/apache/pdfbox/cos/COSStream;->createUnfilteredStream()Ljava/io/OutputStream;

    move-result-object p1

    :goto_0
    move-object v0, p1

    const/16 p1, 0x400

    new-array p1, p1, [B

    :goto_1
    invoke-virtual {p2, p1}, Ljava/io/InputStream;->read([B)I

    move-result p3

    const/4 v1, -0x1

    if-eq p3, v1, :cond_1

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1, p3}, Ljava/io/OutputStream;->write([BII)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :cond_1
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V

    :cond_2
    invoke-virtual {p2}, Ljava/io/InputStream;->close()V

    return-void

    :catchall_0
    move-exception p1

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V

    :cond_3
    if-eqz p2, :cond_4

    invoke-virtual {p2}, Ljava/io/InputStream;->close()V

    :cond_4
    throw p1
.end method

.method public static createFromCOS(Lorg/apache/pdfbox/cos/COSBase;)Lorg/apache/pdfbox/pdmodel/common/PDStream;
    .locals 3

    instance-of v0, p0, Lorg/apache/pdfbox/cos/COSStream;

    if-eqz v0, :cond_0

    new-instance v0, Lorg/apache/pdfbox/pdmodel/common/PDStream;

    check-cast p0, Lorg/apache/pdfbox/cos/COSStream;

    invoke-direct {v0, p0}, Lorg/apache/pdfbox/pdmodel/common/PDStream;-><init>(Lorg/apache/pdfbox/cos/COSStream;)V

    goto :goto_0

    :cond_0
    instance-of v0, p0, Lorg/apache/pdfbox/cos/COSArray;

    if-eqz v0, :cond_1

    check-cast p0, Lorg/apache/pdfbox/cos/COSArray;

    invoke-virtual {p0}, Lorg/apache/pdfbox/cos/COSArray;->size()I

    move-result v0

    if-lez v0, :cond_2

    new-instance v0, Lorg/apache/pdfbox/pdmodel/common/PDStream;

    new-instance v1, Lorg/apache/pdfbox/pdmodel/common/COSStreamArray;

    invoke-direct {v1, p0}, Lorg/apache/pdfbox/pdmodel/common/COSStreamArray;-><init>(Lorg/apache/pdfbox/cos/COSArray;)V

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/pdmodel/common/PDStream;-><init>(Lorg/apache/pdfbox/cos/COSStream;)V

    goto :goto_0

    :cond_1
    if-nez p0, :cond_3

    :cond_2
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_3
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Contents are unknown type:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public addCompression()V
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/common/PDStream;->getFilters()Ljava/util/List;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->FLATE_DECODE:Lorg/apache/pdfbox/cos/COSName;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0, v0}, Lorg/apache/pdfbox/pdmodel/common/PDStream;->setFilters(Ljava/util/List;)V

    :cond_0
    return-void
.end method

.method public createInputStream()Ljava/io/InputStream;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/PDStream;->stream:Lorg/apache/pdfbox/cos/COSStream;

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSStream;->getUnfilteredStream()Ljava/io/InputStream;

    move-result-object v0

    return-object v0
.end method

.method public createOutputStream()Ljava/io/OutputStream;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/PDStream;->stream:Lorg/apache/pdfbox/cos/COSStream;

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSStream;->createUnfilteredStream()Ljava/io/OutputStream;

    move-result-object v0

    return-object v0
.end method

.method public getByteArray()[B
    .locals 5

    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    const/16 v1, 0x400

    new-array v1, v1, [B

    :try_start_0
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/common/PDStream;->createInputStream()Ljava/io/InputStream;

    move-result-object v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :goto_0
    :try_start_1
    invoke-virtual {v2, v1}, Ljava/io/InputStream;->read([B)I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    const/4 v4, 0x0

    invoke-virtual {v0, v1, v4, v3}, Ljava/io/ByteArrayOutputStream;->write([BII)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :cond_0
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    return-object v0

    :catchall_0
    move-exception v0

    goto :goto_1

    :catchall_1
    move-exception v0

    const/4 v2, 0x0

    :goto_1
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/io/InputStream;->close()V

    :cond_1
    throw v0
.end method

.method public getCOSObject()Lorg/apache/pdfbox/cos/COSBase;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/PDStream;->stream:Lorg/apache/pdfbox/cos/COSStream;

    return-object v0
.end method

.method public getDecodeParms()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/PDStream;->stream:Lorg/apache/pdfbox/cos/COSStream;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->DECODE_PARMS:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/PDStream;->stream:Lorg/apache/pdfbox/cos/COSStream;

    sget-object v2, Lorg/apache/pdfbox/cos/COSName;->DP:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    :cond_0
    instance-of v2, v0, Lorg/apache/pdfbox/cos/COSDictionary;

    if-eqz v2, :cond_1

    move-object v2, v0

    check-cast v2, Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-static {v2}, Lorg/apache/pdfbox/pdmodel/common/COSDictionaryMap;->convertBasicTypesToMap(Lorg/apache/pdfbox/cos/COSDictionary;)Lorg/apache/pdfbox/pdmodel/common/COSDictionaryMap;

    move-result-object v2

    new-instance v3, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;

    iget-object v4, p0, Lorg/apache/pdfbox/pdmodel/common/PDStream;->stream:Lorg/apache/pdfbox/cos/COSStream;

    invoke-direct {v3, v2, v0, v4, v1}, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;-><init>(Ljava/lang/Object;Lorg/apache/pdfbox/cos/COSBase;Lorg/apache/pdfbox/cos/COSDictionary;Lorg/apache/pdfbox/cos/COSName;)V

    goto :goto_1

    :cond_1
    instance-of v1, v0, Lorg/apache/pdfbox/cos/COSArray;

    if-eqz v1, :cond_3

    check-cast v0, Lorg/apache/pdfbox/cos/COSArray;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    const/4 v2, 0x0

    :goto_0
    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSArray;->size()I

    move-result v3

    if-ge v2, v3, :cond_2

    invoke-virtual {v0, v2}, Lorg/apache/pdfbox/cos/COSArray;->getObject(I)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v3

    check-cast v3, Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-static {v3}, Lorg/apache/pdfbox/pdmodel/common/COSDictionaryMap;->convertBasicTypesToMap(Lorg/apache/pdfbox/cos/COSDictionary;)Lorg/apache/pdfbox/pdmodel/common/COSDictionaryMap;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    new-instance v3, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;

    invoke-direct {v3, v1, v0}, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;-><init>(Ljava/util/List;Lorg/apache/pdfbox/cos/COSArray;)V

    goto :goto_1

    :cond_3
    const/4 v3, 0x0

    :goto_1
    return-object v3
.end method

.method public getDecodedStreamLength()I
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/PDStream;->stream:Lorg/apache/pdfbox/cos/COSStream;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->DL:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getInt(Lorg/apache/pdfbox/cos/COSName;)I

    move-result v0

    return v0
.end method

.method public getFile()Lorg/apache/pdfbox/pdmodel/common/filespecification/PDFileSpecification;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/PDStream;->stream:Lorg/apache/pdfbox/cos/COSStream;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->F:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    invoke-static {v0}, Lorg/apache/pdfbox/pdmodel/common/filespecification/PDFileSpecification;->createFS(Lorg/apache/pdfbox/cos/COSBase;)Lorg/apache/pdfbox/pdmodel/common/filespecification/PDFileSpecification;

    move-result-object v0

    return-object v0
.end method

.method public getFileDecodeParams()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/PDStream;->stream:Lorg/apache/pdfbox/cos/COSStream;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->F_DECODE_PARMS:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    instance-of v2, v0, Lorg/apache/pdfbox/cos/COSDictionary;

    if-eqz v2, :cond_0

    move-object v2, v0

    check-cast v2, Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-static {v2}, Lorg/apache/pdfbox/pdmodel/common/COSDictionaryMap;->convertBasicTypesToMap(Lorg/apache/pdfbox/cos/COSDictionary;)Lorg/apache/pdfbox/pdmodel/common/COSDictionaryMap;

    move-result-object v2

    new-instance v3, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;

    iget-object v4, p0, Lorg/apache/pdfbox/pdmodel/common/PDStream;->stream:Lorg/apache/pdfbox/cos/COSStream;

    invoke-direct {v3, v2, v0, v4, v1}, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;-><init>(Ljava/lang/Object;Lorg/apache/pdfbox/cos/COSBase;Lorg/apache/pdfbox/cos/COSDictionary;Lorg/apache/pdfbox/cos/COSName;)V

    goto :goto_1

    :cond_0
    instance-of v1, v0, Lorg/apache/pdfbox/cos/COSArray;

    if-eqz v1, :cond_2

    check-cast v0, Lorg/apache/pdfbox/cos/COSArray;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    const/4 v2, 0x0

    :goto_0
    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSArray;->size()I

    move-result v3

    if-ge v2, v3, :cond_1

    invoke-virtual {v0, v2}, Lorg/apache/pdfbox/cos/COSArray;->getObject(I)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v3

    check-cast v3, Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-static {v3}, Lorg/apache/pdfbox/pdmodel/common/COSDictionaryMap;->convertBasicTypesToMap(Lorg/apache/pdfbox/cos/COSDictionary;)Lorg/apache/pdfbox/pdmodel/common/COSDictionaryMap;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    new-instance v3, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;

    invoke-direct {v3, v1, v0}, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;-><init>(Ljava/util/List;Lorg/apache/pdfbox/cos/COSArray;)V

    goto :goto_1

    :cond_2
    const/4 v3, 0x0

    :goto_1
    return-object v3
.end method

.method public getFileFilters()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/PDStream;->stream:Lorg/apache/pdfbox/cos/COSStream;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->F_FILTER:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    instance-of v2, v0, Lorg/apache/pdfbox/cos/COSName;

    if-eqz v2, :cond_0

    check-cast v0, Lorg/apache/pdfbox/cos/COSName;

    new-instance v2, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSName;->getName()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lorg/apache/pdfbox/pdmodel/common/PDStream;->stream:Lorg/apache/pdfbox/cos/COSStream;

    invoke-direct {v2, v3, v0, v4, v1}, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;-><init>(Ljava/lang/Object;Lorg/apache/pdfbox/cos/COSBase;Lorg/apache/pdfbox/cos/COSDictionary;Lorg/apache/pdfbox/cos/COSName;)V

    goto :goto_0

    :cond_0
    instance-of v1, v0, Lorg/apache/pdfbox/cos/COSArray;

    if-eqz v1, :cond_1

    check-cast v0, Lorg/apache/pdfbox/cos/COSArray;

    invoke-static {v0}, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;->convertCOSNameCOSArrayToList(Lorg/apache/pdfbox/cos/COSArray;)Ljava/util/List;

    move-result-object v2

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    :goto_0
    return-object v2
.end method

.method public getFilters()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lorg/apache/pdfbox/cos/COSName;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/PDStream;->stream:Lorg/apache/pdfbox/cos/COSStream;

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSStream;->getFilters()Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    instance-of v1, v0, Lorg/apache/pdfbox/cos/COSName;

    if-eqz v1, :cond_0

    check-cast v0, Lorg/apache/pdfbox/cos/COSName;

    new-instance v1, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;

    iget-object v2, p0, Lorg/apache/pdfbox/pdmodel/common/PDStream;->stream:Lorg/apache/pdfbox/cos/COSStream;

    sget-object v3, Lorg/apache/pdfbox/cos/COSName;->FILTER:Lorg/apache/pdfbox/cos/COSName;

    invoke-direct {v1, v0, v0, v2, v3}, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;-><init>(Ljava/lang/Object;Lorg/apache/pdfbox/cos/COSBase;Lorg/apache/pdfbox/cos/COSDictionary;Lorg/apache/pdfbox/cos/COSName;)V

    goto :goto_0

    :cond_0
    instance-of v1, v0, Lorg/apache/pdfbox/cos/COSArray;

    if-eqz v1, :cond_1

    check-cast v0, Lorg/apache/pdfbox/cos/COSArray;

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSArray;->toList()Ljava/util/List;

    move-result-object v1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    return-object v1
.end method

.method public getInputStreamAsString()Ljava/lang/String;
    .locals 3

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/common/PDStream;->getByteArray()[B

    move-result-object v0

    new-instance v1, Ljava/lang/String;

    const-string v2, "ISO-8859-1"

    invoke-direct {v1, v0, v2}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    return-object v1
.end method

.method public getLength()I
    .locals 3

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/PDStream;->stream:Lorg/apache/pdfbox/cos/COSStream;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->LENGTH:Lorg/apache/pdfbox/cos/COSName;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->getInt(Lorg/apache/pdfbox/cos/COSName;I)I

    move-result v0

    return v0
.end method

.method public getMetadata()Lorg/apache/pdfbox/pdmodel/common/PDMetadata;
    .locals 4

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/PDStream;->stream:Lorg/apache/pdfbox/cos/COSStream;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->METADATA:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    if-eqz v0, :cond_2

    instance-of v1, v0, Lorg/apache/pdfbox/cos/COSStream;

    if-eqz v1, :cond_0

    new-instance v1, Lorg/apache/pdfbox/pdmodel/common/PDMetadata;

    check-cast v0, Lorg/apache/pdfbox/cos/COSStream;

    invoke-direct {v1, v0}, Lorg/apache/pdfbox/pdmodel/common/PDMetadata;-><init>(Lorg/apache/pdfbox/cos/COSStream;)V

    goto :goto_1

    :cond_0
    instance-of v1, v0, Lorg/apache/pdfbox/cos/COSNull;

    if-eqz v1, :cond_1

    goto :goto_0

    :cond_1
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Expected a COSStream but was a "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_2
    :goto_0
    const/4 v1, 0x0

    :goto_1
    return-object v1
.end method

.method public getPartiallyFilteredStream(Ljava/util/List;)Ljava/io/InputStream;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)",
            "Ljava/io/InputStream;"
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/PDStream;->stream:Lorg/apache/pdfbox/cos/COSStream;

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSStream;->getFilteredStream()Ljava/io/InputStream;

    move-result-object v0

    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/common/PDStream;->getFilters()Ljava/util/List;

    move-result-object v2

    const/4 v3, 0x0

    move v4, v3

    :goto_0
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v5

    if-ge v3, v5, :cond_1

    if-nez v4, :cond_1

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v5}, Lorg/apache/pdfbox/cos/COSName;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-interface {p1, v6}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    const/4 v4, 0x1

    goto :goto_1

    :cond_0
    sget-object v6, Lorg/apache/pdfbox/filter/FilterFactory;->INSTANCE:Lorg/apache/pdfbox/filter/FilterFactory;

    invoke-virtual {v6, v5}, Lorg/apache/pdfbox/filter/FilterFactory;->getFilter(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/filter/Filter;

    move-result-object v5

    iget-object v6, p0, Lorg/apache/pdfbox/pdmodel/common/PDStream;->stream:Lorg/apache/pdfbox/cos/COSStream;

    invoke-virtual {v5, v0, v1, v6, v3}, Lorg/apache/pdfbox/filter/Filter;->decode(Ljava/io/InputStream;Ljava/io/OutputStream;Lorg/apache/pdfbox/cos/COSDictionary;I)Lorg/apache/pdfbox/filter/DecodeResult;

    invoke-static {v0}, Lorg/apache/pdfbox/io/IOUtils;->closeQuietly(Ljava/io/Closeable;)V

    new-instance v0, Ljava/io/ByteArrayInputStream;

    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v5

    invoke-direct {v0, v5}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->reset()V

    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method public getStream()Lorg/apache/pdfbox/cos/COSStream;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/PDStream;->stream:Lorg/apache/pdfbox/cos/COSStream;

    return-object v0
.end method

.method public setDecodeParms(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "*>;)V"
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/PDStream;->stream:Lorg/apache/pdfbox/cos/COSStream;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->DECODE_PARMS:Lorg/apache/pdfbox/cos/COSName;

    invoke-static {p1}, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;->converterToCOSArray(Ljava/util/List;)Lorg/apache/pdfbox/cos/COSArray;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    return-void
.end method

.method public setDecodedStreamLength(I)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/PDStream;->stream:Lorg/apache/pdfbox/cos/COSStream;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->DL:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setInt(Lorg/apache/pdfbox/cos/COSName;I)V

    return-void
.end method

.method public setFile(Lorg/apache/pdfbox/pdmodel/common/filespecification/PDFileSpecification;)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/PDStream;->stream:Lorg/apache/pdfbox/cos/COSStream;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->F:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)V

    return-void
.end method

.method public setFileDecodeParams(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "*>;)V"
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/PDStream;->stream:Lorg/apache/pdfbox/cos/COSStream;

    const-string v1, "FDecodeParams"

    invoke-static {p1}, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;->converterToCOSArray(Ljava/util/List;)Lorg/apache/pdfbox/cos/COSArray;

    move-result-object p1

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Ljava/lang/String;Lorg/apache/pdfbox/cos/COSBase;)V

    return-void
.end method

.method public setFileFilters(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    invoke-static {p1}, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;->convertStringListToCOSNameCOSArray(Ljava/util/List;)Lorg/apache/pdfbox/cos/COSArray;

    move-result-object p1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/PDStream;->stream:Lorg/apache/pdfbox/cos/COSStream;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->F_FILTER:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    return-void
.end method

.method public setFilters(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lorg/apache/pdfbox/cos/COSName;",
            ">;)V"
        }
    .end annotation

    invoke-static {p1}, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;->converterToCOSArray(Ljava/util/List;)Lorg/apache/pdfbox/cos/COSArray;

    move-result-object p1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/PDStream;->stream:Lorg/apache/pdfbox/cos/COSStream;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->FILTER:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    return-void
.end method

.method public setMetadata(Lorg/apache/pdfbox/pdmodel/common/PDMetadata;)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/PDStream;->stream:Lorg/apache/pdfbox/cos/COSStream;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->METADATA:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)V

    return-void
.end method

.class final enum Lorg/apache/pdfbox/pdmodel/common/function/type4/Parser$State;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/pdfbox/pdmodel/common/function/type4/Parser;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "State"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lorg/apache/pdfbox/pdmodel/common/function/type4/Parser$State;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lorg/apache/pdfbox/pdmodel/common/function/type4/Parser$State;

.field public static final enum COMMENT:Lorg/apache/pdfbox/pdmodel/common/function/type4/Parser$State;

.field public static final enum NEWLINE:Lorg/apache/pdfbox/pdmodel/common/function/type4/Parser$State;

.field public static final enum TOKEN:Lorg/apache/pdfbox/pdmodel/common/function/type4/Parser$State;

.field public static final enum WHITESPACE:Lorg/apache/pdfbox/pdmodel/common/function/type4/Parser$State;


# direct methods
.method public static constructor <clinit>()V
    .locals 6

    new-instance v0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Parser$State;

    const-string v1, "NEWLINE"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lorg/apache/pdfbox/pdmodel/common/function/type4/Parser$State;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Parser$State;->NEWLINE:Lorg/apache/pdfbox/pdmodel/common/function/type4/Parser$State;

    new-instance v1, Lorg/apache/pdfbox/pdmodel/common/function/type4/Parser$State;

    const-string v2, "WHITESPACE"

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Lorg/apache/pdfbox/pdmodel/common/function/type4/Parser$State;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lorg/apache/pdfbox/pdmodel/common/function/type4/Parser$State;->WHITESPACE:Lorg/apache/pdfbox/pdmodel/common/function/type4/Parser$State;

    new-instance v2, Lorg/apache/pdfbox/pdmodel/common/function/type4/Parser$State;

    const-string v3, "COMMENT"

    const/4 v4, 0x2

    invoke-direct {v2, v3, v4}, Lorg/apache/pdfbox/pdmodel/common/function/type4/Parser$State;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lorg/apache/pdfbox/pdmodel/common/function/type4/Parser$State;->COMMENT:Lorg/apache/pdfbox/pdmodel/common/function/type4/Parser$State;

    new-instance v3, Lorg/apache/pdfbox/pdmodel/common/function/type4/Parser$State;

    const-string v4, "TOKEN"

    const/4 v5, 0x3

    invoke-direct {v3, v4, v5}, Lorg/apache/pdfbox/pdmodel/common/function/type4/Parser$State;-><init>(Ljava/lang/String;I)V

    sput-object v3, Lorg/apache/pdfbox/pdmodel/common/function/type4/Parser$State;->TOKEN:Lorg/apache/pdfbox/pdmodel/common/function/type4/Parser$State;

    filled-new-array {v0, v1, v2, v3}, [Lorg/apache/pdfbox/pdmodel/common/function/type4/Parser$State;

    move-result-object v0

    sput-object v0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Parser$State;->$VALUES:[Lorg/apache/pdfbox/pdmodel/common/function/type4/Parser$State;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lorg/apache/pdfbox/pdmodel/common/function/type4/Parser$State;
    .locals 1

    const-class v0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Parser$State;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Parser$State;

    return-object p0
.end method

.method public static values()[Lorg/apache/pdfbox/pdmodel/common/function/type4/Parser$State;
    .locals 1

    sget-object v0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Parser$State;->$VALUES:[Lorg/apache/pdfbox/pdmodel/common/function/type4/Parser$State;

    invoke-virtual {v0}, [Lorg/apache/pdfbox/pdmodel/common/function/type4/Parser$State;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lorg/apache/pdfbox/pdmodel/common/function/type4/Parser$State;

    return-object v0
.end method

.class public Lorg/apache/pdfbox/pdmodel/common/function/PDFunctionType2;
.super Lorg/apache/pdfbox/pdmodel/common/function/PDFunction;
.source "SourceFile"


# instance fields
.field private final c0:Lorg/apache/pdfbox/cos/COSArray;

.field private final c1:Lorg/apache/pdfbox/cos/COSArray;

.field private final exponent:F


# direct methods
.method public constructor <init>(Lorg/apache/pdfbox/cos/COSBase;)V
    .locals 2

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/common/function/PDFunction;-><init>(Lorg/apache/pdfbox/cos/COSBase;)V

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/common/function/PDFunction;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object p1

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->C0:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p1, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object p1

    if-nez p1, :cond_0

    new-instance p1, Lorg/apache/pdfbox/cos/COSArray;

    invoke-direct {p1}, Lorg/apache/pdfbox/cos/COSArray;-><init>()V

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/common/function/PDFunctionType2;->c0:Lorg/apache/pdfbox/cos/COSArray;

    new-instance v0, Lorg/apache/pdfbox/cos/COSFloat;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSFloat;-><init>(F)V

    invoke-virtual {p1, v0}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/common/function/PDFunction;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object p1

    invoke-virtual {p1, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object p1

    check-cast p1, Lorg/apache/pdfbox/cos/COSArray;

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/common/function/PDFunctionType2;->c0:Lorg/apache/pdfbox/cos/COSArray;

    :goto_0
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/common/function/PDFunction;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object p1

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->C1:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p1, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object p1

    if-nez p1, :cond_1

    new-instance p1, Lorg/apache/pdfbox/cos/COSArray;

    invoke-direct {p1}, Lorg/apache/pdfbox/cos/COSArray;-><init>()V

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/common/function/PDFunctionType2;->c1:Lorg/apache/pdfbox/cos/COSArray;

    new-instance v0, Lorg/apache/pdfbox/cos/COSFloat;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSFloat;-><init>(F)V

    invoke-virtual {p1, v0}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    goto :goto_1

    :cond_1
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/common/function/PDFunction;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object p1

    invoke-virtual {p1, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object p1

    check-cast p1, Lorg/apache/pdfbox/cos/COSArray;

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/common/function/PDFunctionType2;->c1:Lorg/apache/pdfbox/cos/COSArray;

    :goto_1
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/common/function/PDFunction;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object p1

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->N:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p1, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->getFloat(Lorg/apache/pdfbox/cos/COSName;)F

    move-result p1

    iput p1, p0, Lorg/apache/pdfbox/pdmodel/common/function/PDFunctionType2;->exponent:F

    return-void
.end method


# virtual methods
.method public eval([F)[F
    .locals 5

    const/4 v0, 0x0

    aget p1, p1, v0

    float-to-double v1, p1

    iget p1, p0, Lorg/apache/pdfbox/pdmodel/common/function/PDFunctionType2;->exponent:F

    float-to-double v3, p1

    invoke-static {v1, v2, v3, v4}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v1

    double-to-float p1, v1

    iget-object v1, p0, Lorg/apache/pdfbox/pdmodel/common/function/PDFunctionType2;->c0:Lorg/apache/pdfbox/cos/COSArray;

    invoke-virtual {v1}, Lorg/apache/pdfbox/cos/COSArray;->size()I

    move-result v1

    new-array v2, v1, [F

    :goto_0
    if-ge v0, v1, :cond_0

    iget-object v3, p0, Lorg/apache/pdfbox/pdmodel/common/function/PDFunctionType2;->c0:Lorg/apache/pdfbox/cos/COSArray;

    invoke-virtual {v3, v0}, Lorg/apache/pdfbox/cos/COSArray;->get(I)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v3

    check-cast v3, Lorg/apache/pdfbox/cos/COSNumber;

    invoke-virtual {v3}, Lorg/apache/pdfbox/cos/COSNumber;->floatValue()F

    move-result v3

    iget-object v4, p0, Lorg/apache/pdfbox/pdmodel/common/function/PDFunctionType2;->c1:Lorg/apache/pdfbox/cos/COSArray;

    invoke-virtual {v4, v0}, Lorg/apache/pdfbox/cos/COSArray;->get(I)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v4

    check-cast v4, Lorg/apache/pdfbox/cos/COSNumber;

    invoke-virtual {v4}, Lorg/apache/pdfbox/cos/COSNumber;->floatValue()F

    move-result v4

    sub-float/2addr v4, v3

    mul-float/2addr v4, p1

    add-float/2addr v3, v4

    aput v3, v2, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {p0, v2}, Lorg/apache/pdfbox/pdmodel/common/function/PDFunction;->clipToRange([F)[F

    move-result-object p1

    return-object p1
.end method

.method public getC0()Lorg/apache/pdfbox/cos/COSArray;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/function/PDFunctionType2;->c0:Lorg/apache/pdfbox/cos/COSArray;

    return-object v0
.end method

.method public getC1()Lorg/apache/pdfbox/cos/COSArray;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/function/PDFunctionType2;->c1:Lorg/apache/pdfbox/cos/COSArray;

    return-object v0
.end method

.method public getFunctionType()I
    .locals 1

    const/4 v0, 0x2

    return v0
.end method

.method public getN()F
    .locals 1

    iget v0, p0, Lorg/apache/pdfbox/pdmodel/common/function/PDFunctionType2;->exponent:F

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "FunctionType2{C0: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/common/function/PDFunctionType2;->getC0()Lorg/apache/pdfbox/cos/COSArray;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "C1: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/common/function/PDFunctionType2;->getC1()Lorg/apache/pdfbox/cos/COSArray;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "N: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/common/function/PDFunctionType2;->getN()F

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

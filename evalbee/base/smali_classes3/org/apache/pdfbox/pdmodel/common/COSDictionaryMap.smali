.class public Lorg/apache/pdfbox/pdmodel/common/COSDictionaryMap;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/util/Map;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/util/Map<",
        "TK;TV;>;"
    }
.end annotation


# instance fields
.field private final actuals:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "TK;TV;>;"
        }
    .end annotation
.end field

.field private final map:Lorg/apache/pdfbox/cos/COSDictionary;


# direct methods
.method public constructor <init>(Ljava/util/Map;Lorg/apache/pdfbox/cos/COSDictionary;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "TK;TV;>;",
            "Lorg/apache/pdfbox/cos/COSDictionary;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/common/COSDictionaryMap;->actuals:Ljava/util/Map;

    iput-object p2, p0, Lorg/apache/pdfbox/pdmodel/common/COSDictionaryMap;->map:Lorg/apache/pdfbox/cos/COSDictionary;

    return-void
.end method

.method public static convert(Ljava/util/Map;)Lorg/apache/pdfbox/cos/COSDictionary;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "*>;)",
            "Lorg/apache/pdfbox/cos/COSDictionary;"
        }
    .end annotation

    invoke-interface {p0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    new-instance v0, Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-direct {v0}, Lorg/apache/pdfbox/cos/COSDictionary;-><init>()V

    invoke-interface {p0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p0

    invoke-interface {p0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p0

    :goto_0
    invoke-interface {p0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/pdfbox/pdmodel/common/COSObjectable;

    invoke-static {v2}, Lorg/apache/pdfbox/cos/COSName;->getPDFName(Ljava/lang/String;)Lorg/apache/pdfbox/cos/COSName;

    move-result-object v2

    invoke-interface {v1}, Lorg/apache/pdfbox/pdmodel/common/COSObjectable;->getCOSObject()Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method public static convertBasicTypesToMap(Lorg/apache/pdfbox/cos/COSDictionary;)Lorg/apache/pdfbox/pdmodel/common/COSDictionaryMap;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/pdfbox/cos/COSDictionary;",
            ")",
            "Lorg/apache/pdfbox/pdmodel/common/COSDictionaryMap<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    if-eqz p0, :cond_7

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-virtual {p0}, Lorg/apache/pdfbox/cos/COSDictionary;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p0, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v3

    instance-of v4, v3, Lorg/apache/pdfbox/cos/COSString;

    if-eqz v4, :cond_0

    check-cast v3, Lorg/apache/pdfbox/cos/COSString;

    invoke-virtual {v3}, Lorg/apache/pdfbox/cos/COSString;->getString()Ljava/lang/String;

    move-result-object v3

    goto :goto_1

    :cond_0
    instance-of v4, v3, Lorg/apache/pdfbox/cos/COSInteger;

    if-eqz v4, :cond_1

    check-cast v3, Lorg/apache/pdfbox/cos/COSInteger;

    invoke-virtual {v3}, Lorg/apache/pdfbox/cos/COSInteger;->intValue()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    goto :goto_1

    :cond_1
    instance-of v4, v3, Lorg/apache/pdfbox/cos/COSName;

    if-eqz v4, :cond_2

    check-cast v3, Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v3}, Lorg/apache/pdfbox/cos/COSName;->getName()Ljava/lang/String;

    move-result-object v3

    goto :goto_1

    :cond_2
    instance-of v4, v3, Lorg/apache/pdfbox/cos/COSFloat;

    if-eqz v4, :cond_3

    check-cast v3, Lorg/apache/pdfbox/cos/COSFloat;

    invoke-virtual {v3}, Lorg/apache/pdfbox/cos/COSFloat;->floatValue()F

    move-result v3

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    goto :goto_1

    :cond_3
    instance-of v4, v3, Lorg/apache/pdfbox/cos/COSBoolean;

    if-eqz v4, :cond_5

    check-cast v3, Lorg/apache/pdfbox/cos/COSBoolean;

    invoke-virtual {v3}, Lorg/apache/pdfbox/cos/COSBoolean;->getValue()Z

    move-result v3

    if-eqz v3, :cond_4

    sget-object v3, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    goto :goto_1

    :cond_4
    sget-object v3, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    :goto_1
    invoke-virtual {v2}, Lorg/apache/pdfbox/cos/COSName;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_5
    new-instance p0, Ljava/io/IOException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Error:unknown type of object to convert:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p0

    :cond_6
    new-instance v1, Lorg/apache/pdfbox/pdmodel/common/COSDictionaryMap;

    invoke-direct {v1, v0, p0}, Lorg/apache/pdfbox/pdmodel/common/COSDictionaryMap;-><init>(Ljava/util/Map;Lorg/apache/pdfbox/cos/COSDictionary;)V

    goto :goto_2

    :cond_7
    const/4 v1, 0x0

    :goto_2
    return-object v1
.end method


# virtual methods
.method public clear()V
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/COSDictionaryMap;->map:Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSDictionary;->clear()V

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/COSDictionaryMap;->actuals:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    return-void
.end method

.method public containsKey(Ljava/lang/Object;)Z
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/COSDictionaryMap;->map:Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSDictionary;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public containsValue(Ljava/lang/Object;)Z
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/COSDictionaryMap;->actuals:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsValue(Ljava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public entrySet()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "Ljava/util/Map$Entry<",
            "TK;TV;>;>;"
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/COSDictionaryMap;->actuals:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 1

    instance-of v0, p1, Lorg/apache/pdfbox/pdmodel/common/COSDictionaryMap;

    if-eqz v0, :cond_0

    check-cast p1, Lorg/apache/pdfbox/pdmodel/common/COSDictionaryMap;

    iget-object p1, p1, Lorg/apache/pdfbox/pdmodel/common/COSDictionaryMap;->map:Lorg/apache/pdfbox/cos/COSDictionary;

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/COSDictionaryMap;->map:Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")TV;"
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/COSDictionaryMap;->actuals:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/COSDictionaryMap;->map:Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method

.method public isEmpty()Z
    .locals 1

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/common/COSDictionaryMap;->size()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public keySet()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "TK;>;"
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/COSDictionaryMap;->actuals:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;)TV;"
        }
    .end annotation

    move-object v0, p2

    check-cast v0, Lorg/apache/pdfbox/pdmodel/common/COSObjectable;

    iget-object v1, p0, Lorg/apache/pdfbox/pdmodel/common/COSDictionaryMap;->map:Lorg/apache/pdfbox/cos/COSDictionary;

    move-object v2, p1

    check-cast v2, Ljava/lang/String;

    invoke-static {v2}, Lorg/apache/pdfbox/cos/COSName;->getPDFName(Ljava/lang/String;)Lorg/apache/pdfbox/cos/COSName;

    move-result-object v2

    invoke-interface {v0}, Lorg/apache/pdfbox/pdmodel/common/COSObjectable;->getCOSObject()Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/COSDictionaryMap;->actuals:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public putAll(Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "+TK;+TV;>;)V"
        }
    .end annotation

    new-instance p1, Ljava/lang/RuntimeException;

    const-string v0, "Not yet implemented"

    invoke-direct {p1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public remove(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            ")TV;"
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/COSDictionaryMap;->map:Lorg/apache/pdfbox/cos/COSDictionary;

    move-object v1, p1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Lorg/apache/pdfbox/cos/COSName;->getPDFName(Ljava/lang/String;)Lorg/apache/pdfbox/cos/COSName;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->removeItem(Lorg/apache/pdfbox/cos/COSName;)V

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/COSDictionaryMap;->actuals:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public size()I
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/COSDictionaryMap;->map:Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSDictionary;->size()I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/COSDictionaryMap;->actuals:Ljava/util/Map;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public values()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection<",
            "TV;>;"
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/COSDictionaryMap;->actuals:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.class public Lorg/apache/pdfbox/pdmodel/common/function/PDFunctionType3;
.super Lorg/apache/pdfbox/pdmodel/common/function/PDFunction;
.source "SourceFile"


# instance fields
.field private bounds:Lorg/apache/pdfbox/cos/COSArray;

.field private encode:Lorg/apache/pdfbox/cos/COSArray;

.field private functions:Lorg/apache/pdfbox/cos/COSArray;


# direct methods
.method public constructor <init>(Lorg/apache/pdfbox/cos/COSBase;)V
    .locals 0

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/common/function/PDFunction;-><init>(Lorg/apache/pdfbox/cos/COSBase;)V

    const/4 p1, 0x0

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/common/function/PDFunctionType3;->functions:Lorg/apache/pdfbox/cos/COSArray;

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/common/function/PDFunctionType3;->encode:Lorg/apache/pdfbox/cos/COSArray;

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/common/function/PDFunctionType3;->bounds:Lorg/apache/pdfbox/cos/COSArray;

    return-void
.end method

.method private getEncodeForParameter(I)Lorg/apache/pdfbox/pdmodel/common/PDRange;
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/common/function/PDFunctionType3;->getEncode()Lorg/apache/pdfbox/cos/COSArray;

    move-result-object v0

    new-instance v1, Lorg/apache/pdfbox/pdmodel/common/PDRange;

    invoke-direct {v1, v0, p1}, Lorg/apache/pdfbox/pdmodel/common/PDRange;-><init>(Lorg/apache/pdfbox/cos/COSArray;I)V

    return-object v1
.end method


# virtual methods
.method public eval([F)[F
    .locals 10

    const/4 v0, 0x0

    aget p1, p1, v0

    invoke-virtual {p0, v0}, Lorg/apache/pdfbox/pdmodel/common/function/PDFunction;->getDomainForInput(I)Lorg/apache/pdfbox/pdmodel/common/PDRange;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/pdfbox/pdmodel/common/PDRange;->getMin()F

    move-result v2

    invoke-virtual {v1}, Lorg/apache/pdfbox/pdmodel/common/PDRange;->getMax()F

    move-result v3

    invoke-virtual {p0, p1, v2, v3}, Lorg/apache/pdfbox/pdmodel/common/function/PDFunction;->clipToRange(FFF)F

    move-result v5

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/common/function/PDFunctionType3;->getFunctions()Lorg/apache/pdfbox/cos/COSArray;

    move-result-object p1

    invoke-virtual {p1}, Lorg/apache/pdfbox/cos/COSArray;->size()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    invoke-virtual {p1, v0}, Lorg/apache/pdfbox/cos/COSArray;->get(I)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object p1

    invoke-static {p1}, Lorg/apache/pdfbox/pdmodel/common/function/PDFunction;->create(Lorg/apache/pdfbox/cos/COSBase;)Lorg/apache/pdfbox/pdmodel/common/function/PDFunction;

    move-result-object p1

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/pdmodel/common/function/PDFunctionType3;->getEncodeForParameter(I)Lorg/apache/pdfbox/pdmodel/common/PDRange;

    move-result-object v2

    invoke-virtual {v1}, Lorg/apache/pdfbox/pdmodel/common/PDRange;->getMin()F

    move-result v6

    invoke-virtual {v1}, Lorg/apache/pdfbox/pdmodel/common/PDRange;->getMax()F

    move-result v7

    invoke-virtual {v2}, Lorg/apache/pdfbox/pdmodel/common/PDRange;->getMin()F

    move-result v8

    invoke-virtual {v2}, Lorg/apache/pdfbox/pdmodel/common/PDRange;->getMax()F

    move-result v9

    :goto_0
    move-object v4, p0

    invoke-virtual/range {v4 .. v9}, Lorg/apache/pdfbox/pdmodel/common/function/PDFunction;->interpolate(FFFFF)F

    move-result v5

    goto :goto_2

    :cond_0
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/common/function/PDFunctionType3;->getBounds()Lorg/apache/pdfbox/cos/COSArray;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/pdfbox/cos/COSArray;->toFloatArray()[F

    move-result-object v2

    array-length v4, v2

    add-int/lit8 v6, v4, 0x2

    new-array v7, v6, [F

    invoke-virtual {v1}, Lorg/apache/pdfbox/pdmodel/common/PDRange;->getMin()F

    move-result v8

    aput v8, v7, v0

    add-int/lit8 v8, v6, -0x1

    invoke-virtual {v1}, Lorg/apache/pdfbox/pdmodel/common/PDRange;->getMax()F

    move-result v1

    aput v1, v7, v8

    invoke-static {v2, v0, v7, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    move v1, v0

    :goto_1
    if-ge v1, v8, :cond_3

    aget v2, v7, v1

    cmpl-float v2, v5, v2

    if-ltz v2, :cond_2

    add-int/lit8 v2, v1, 0x1

    aget v4, v7, v2

    cmpg-float v9, v5, v4

    if-ltz v9, :cond_1

    add-int/lit8 v9, v6, -0x2

    if-ne v1, v9, :cond_2

    cmpl-float v4, v5, v4

    if-nez v4, :cond_2

    :cond_1
    invoke-virtual {p1, v1}, Lorg/apache/pdfbox/cos/COSArray;->get(I)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object p1

    invoke-static {p1}, Lorg/apache/pdfbox/pdmodel/common/function/PDFunction;->create(Lorg/apache/pdfbox/cos/COSBase;)Lorg/apache/pdfbox/pdmodel/common/function/PDFunction;

    move-result-object p1

    invoke-direct {p0, v1}, Lorg/apache/pdfbox/pdmodel/common/function/PDFunctionType3;->getEncodeForParameter(I)Lorg/apache/pdfbox/pdmodel/common/PDRange;

    move-result-object v4

    aget v6, v7, v1

    aget v7, v7, v2

    invoke-virtual {v4}, Lorg/apache/pdfbox/pdmodel/common/PDRange;->getMin()F

    move-result v8

    invoke-virtual {v4}, Lorg/apache/pdfbox/pdmodel/common/PDRange;->getMax()F

    move-result v9

    goto :goto_0

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_3
    const/4 p1, 0x0

    :goto_2
    if-eqz p1, :cond_4

    new-array v1, v3, [F

    aput v5, v1, v0

    invoke-virtual {p1, v1}, Lorg/apache/pdfbox/pdmodel/common/function/PDFunction;->eval([F)[F

    move-result-object p1

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/pdmodel/common/function/PDFunction;->clipToRange([F)[F

    move-result-object p1

    return-object p1

    :cond_4
    new-instance p1, Ljava/io/IOException;

    const-string v0, "partition not found in type 3 function"

    invoke-direct {p1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public getBounds()Lorg/apache/pdfbox/cos/COSArray;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/function/PDFunctionType3;->bounds:Lorg/apache/pdfbox/cos/COSArray;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/common/function/PDFunction;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->BOUNDS:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSArray;

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/function/PDFunctionType3;->bounds:Lorg/apache/pdfbox/cos/COSArray;

    :cond_0
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/function/PDFunctionType3;->bounds:Lorg/apache/pdfbox/cos/COSArray;

    return-object v0
.end method

.method public getEncode()Lorg/apache/pdfbox/cos/COSArray;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/function/PDFunctionType3;->encode:Lorg/apache/pdfbox/cos/COSArray;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/common/function/PDFunction;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->ENCODE:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSArray;

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/function/PDFunctionType3;->encode:Lorg/apache/pdfbox/cos/COSArray;

    :cond_0
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/function/PDFunctionType3;->encode:Lorg/apache/pdfbox/cos/COSArray;

    return-object v0
.end method

.method public getFunctionType()I
    .locals 1

    const/4 v0, 0x3

    return v0
.end method

.method public getFunctions()Lorg/apache/pdfbox/cos/COSArray;
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/function/PDFunctionType3;->functions:Lorg/apache/pdfbox/cos/COSArray;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/common/function/PDFunction;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->FUNCTIONS:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSArray;

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/function/PDFunctionType3;->functions:Lorg/apache/pdfbox/cos/COSArray;

    :cond_0
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/function/PDFunctionType3;->functions:Lorg/apache/pdfbox/cos/COSArray;

    return-object v0
.end method

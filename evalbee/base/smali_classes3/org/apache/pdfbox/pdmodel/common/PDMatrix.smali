.class public Lorg/apache/pdfbox/pdmodel/common/PDMatrix;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/pdfbox/pdmodel/common/COSObjectable;


# instance fields
.field private matrix:Lorg/apache/pdfbox/cos/COSArray;

.field private numberOfRowElements:I


# direct methods
.method public constructor <init>()V
    .locals 4

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x3

    iput v0, p0, Lorg/apache/pdfbox/pdmodel/common/PDMatrix;->numberOfRowElements:I

    new-instance v0, Lorg/apache/pdfbox/cos/COSArray;

    invoke-direct {v0}, Lorg/apache/pdfbox/cos/COSArray;-><init>()V

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/PDMatrix;->matrix:Lorg/apache/pdfbox/cos/COSArray;

    new-instance v1, Lorg/apache/pdfbox/cos/COSFloat;

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-direct {v1, v2}, Lorg/apache/pdfbox/cos/COSFloat;-><init>(F)V

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/PDMatrix;->matrix:Lorg/apache/pdfbox/cos/COSArray;

    new-instance v1, Lorg/apache/pdfbox/cos/COSFloat;

    const/4 v3, 0x0

    invoke-direct {v1, v3}, Lorg/apache/pdfbox/cos/COSFloat;-><init>(F)V

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/PDMatrix;->matrix:Lorg/apache/pdfbox/cos/COSArray;

    new-instance v1, Lorg/apache/pdfbox/cos/COSFloat;

    invoke-direct {v1, v3}, Lorg/apache/pdfbox/cos/COSFloat;-><init>(F)V

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/PDMatrix;->matrix:Lorg/apache/pdfbox/cos/COSArray;

    new-instance v1, Lorg/apache/pdfbox/cos/COSFloat;

    invoke-direct {v1, v3}, Lorg/apache/pdfbox/cos/COSFloat;-><init>(F)V

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/PDMatrix;->matrix:Lorg/apache/pdfbox/cos/COSArray;

    new-instance v1, Lorg/apache/pdfbox/cos/COSFloat;

    invoke-direct {v1, v2}, Lorg/apache/pdfbox/cos/COSFloat;-><init>(F)V

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/PDMatrix;->matrix:Lorg/apache/pdfbox/cos/COSArray;

    new-instance v1, Lorg/apache/pdfbox/cos/COSFloat;

    invoke-direct {v1, v3}, Lorg/apache/pdfbox/cos/COSFloat;-><init>(F)V

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/PDMatrix;->matrix:Lorg/apache/pdfbox/cos/COSArray;

    new-instance v1, Lorg/apache/pdfbox/cos/COSFloat;

    invoke-direct {v1, v3}, Lorg/apache/pdfbox/cos/COSFloat;-><init>(F)V

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/PDMatrix;->matrix:Lorg/apache/pdfbox/cos/COSArray;

    new-instance v1, Lorg/apache/pdfbox/cos/COSFloat;

    invoke-direct {v1, v3}, Lorg/apache/pdfbox/cos/COSFloat;-><init>(F)V

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/PDMatrix;->matrix:Lorg/apache/pdfbox/cos/COSArray;

    new-instance v1, Lorg/apache/pdfbox/cos/COSFloat;

    invoke-direct {v1, v2}, Lorg/apache/pdfbox/cos/COSFloat;-><init>(F)V

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    return-void
.end method

.method public constructor <init>(Lorg/apache/pdfbox/cos/COSArray;)V
    .locals 2

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x3

    iput v0, p0, Lorg/apache/pdfbox/pdmodel/common/PDMatrix;->numberOfRowElements:I

    invoke-virtual {p1}, Lorg/apache/pdfbox/cos/COSArray;->size()I

    move-result v0

    const/4 v1, 0x6

    if-ne v0, v1, :cond_0

    const/4 v0, 0x2

    iput v0, p0, Lorg/apache/pdfbox/pdmodel/common/PDMatrix;->numberOfRowElements:I

    :cond_0
    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/common/PDMatrix;->matrix:Lorg/apache/pdfbox/cos/COSArray;

    return-void
.end method


# virtual methods
.method public getCOSArray()Lorg/apache/pdfbox/cos/COSArray;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/PDMatrix;->matrix:Lorg/apache/pdfbox/cos/COSArray;

    return-object v0
.end method

.method public getCOSObject()Lorg/apache/pdfbox/cos/COSBase;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/PDMatrix;->matrix:Lorg/apache/pdfbox/cos/COSArray;

    return-object v0
.end method

.method public getValue(II)F
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/PDMatrix;->matrix:Lorg/apache/pdfbox/cos/COSArray;

    iget v1, p0, Lorg/apache/pdfbox/pdmodel/common/PDMatrix;->numberOfRowElements:I

    mul-int/2addr p1, v1

    add-int/2addr p1, p2

    invoke-virtual {v0, p1}, Lorg/apache/pdfbox/cos/COSArray;->get(I)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object p1

    check-cast p1, Lorg/apache/pdfbox/cos/COSNumber;

    invoke-virtual {p1}, Lorg/apache/pdfbox/cos/COSNumber;->floatValue()F

    move-result p1

    return p1
.end method

.method public setValue(IIF)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/PDMatrix;->matrix:Lorg/apache/pdfbox/cos/COSArray;

    iget v1, p0, Lorg/apache/pdfbox/pdmodel/common/PDMatrix;->numberOfRowElements:I

    mul-int/2addr p1, v1

    add-int/2addr p1, p2

    new-instance p2, Lorg/apache/pdfbox/cos/COSFloat;

    invoke-direct {p2, p3}, Lorg/apache/pdfbox/cos/COSFloat;-><init>(F)V

    invoke-virtual {v0, p1, p2}, Lorg/apache/pdfbox/cos/COSArray;->set(ILorg/apache/pdfbox/cos/COSBase;)V

    return-void
.end method

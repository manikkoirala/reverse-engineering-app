.class public Lorg/apache/pdfbox/pdmodel/common/PDMemoryStream;
.super Lorg/apache/pdfbox/pdmodel/common/PDStream;
.source "SourceFile"


# instance fields
.field private final data:[B


# direct methods
.method public constructor <init>([B)V
    .locals 0

    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/common/PDStream;-><init>()V

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/common/PDMemoryStream;->data:[B

    return-void
.end method


# virtual methods
.method public addCompression()V
    .locals 0

    return-void
.end method

.method public createInputStream()Ljava/io/InputStream;
    .locals 2

    new-instance v0, Ljava/io/ByteArrayInputStream;

    iget-object v1, p0, Lorg/apache/pdfbox/pdmodel/common/PDMemoryStream;->data:[B

    invoke-direct {v0, v1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    return-object v0
.end method

.method public createOutputStream()Ljava/io/OutputStream;
    .locals 2

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "not supported for memory stream"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getByteArray()[B
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/PDMemoryStream;->data:[B

    return-object v0
.end method

.method public getCOSObject()Lorg/apache/pdfbox/cos/COSBase;
    .locals 2

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "not supported for memory stream"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getDecodeParams()Ljava/util/List;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getFile()Lorg/apache/pdfbox/pdmodel/common/filespecification/PDFileSpecification;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getFileDecodeParams()Ljava/util/List;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getFileFilters()Ljava/util/List;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getFilters()Ljava/util/List;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getLength()I
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/PDMemoryStream;->data:[B

    array-length v0, v0

    return v0
.end method

.method public getMetadata()Lorg/apache/pdfbox/pdmodel/common/PDMetadata;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getPartiallyFilteredStream(Ljava/util/List;)Ljava/io/InputStream;
    .locals 0

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/common/PDMemoryStream;->createInputStream()Ljava/io/InputStream;

    move-result-object p1

    return-object p1
.end method

.method public getStream()Lorg/apache/pdfbox/cos/COSStream;
    .locals 2

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "not supported for memory stream"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setDecodeParams(Ljava/util/List;)V
    .locals 0

    return-void
.end method

.method public setFile(Lorg/apache/pdfbox/pdmodel/common/filespecification/PDFileSpecification;)V
    .locals 0

    return-void
.end method

.method public setFileDecodeParams(Ljava/util/List;)V
    .locals 0

    return-void
.end method

.method public setFileFilters(Ljava/util/List;)V
    .locals 0

    return-void
.end method

.method public setFilters(Ljava/util/List;)V
    .locals 1

    new-instance p1, Ljava/lang/UnsupportedOperationException;

    const-string v0, "not supported for memory stream"

    invoke-direct {p1, v0}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public setMetadata(Lorg/apache/pdfbox/pdmodel/common/PDMetadata;)V
    .locals 0

    return-void
.end method

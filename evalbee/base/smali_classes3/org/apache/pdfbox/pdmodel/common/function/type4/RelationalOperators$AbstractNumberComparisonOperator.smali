.class abstract Lorg/apache/pdfbox/pdmodel/common/function/type4/RelationalOperators$AbstractNumberComparisonOperator;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/pdfbox/pdmodel/common/function/type4/Operator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/pdfbox/pdmodel/common/function/type4/RelationalOperators;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "AbstractNumberComparisonOperator"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lorg/apache/pdfbox/pdmodel/common/function/type4/RelationalOperators$1;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/common/function/type4/RelationalOperators$AbstractNumberComparisonOperator;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract compare(Ljava/lang/Number;Ljava/lang/Number;)Z
.end method

.method public execute(Lorg/apache/pdfbox/pdmodel/common/function/type4/ExecutionContext;)V
    .locals 2

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/common/function/type4/ExecutionContext;->getStack()Ljava/util/Stack;

    move-result-object p1

    invoke-virtual {p1}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p1}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Number;

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {p0, v1, v0}, Lorg/apache/pdfbox/pdmodel/common/function/type4/RelationalOperators$AbstractNumberComparisonOperator;->compare(Ljava/lang/Number;Ljava/lang/Number;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

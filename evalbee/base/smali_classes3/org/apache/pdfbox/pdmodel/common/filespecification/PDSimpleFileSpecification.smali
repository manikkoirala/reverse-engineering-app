.class public Lorg/apache/pdfbox/pdmodel/common/filespecification/PDSimpleFileSpecification;
.super Lorg/apache/pdfbox/pdmodel/common/filespecification/PDFileSpecification;
.source "SourceFile"


# instance fields
.field private file:Lorg/apache/pdfbox/cos/COSString;


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 1
    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/common/filespecification/PDFileSpecification;-><init>()V

    new-instance v0, Lorg/apache/pdfbox/cos/COSString;

    const-string v1, ""

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/cos/COSString;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/filespecification/PDSimpleFileSpecification;->file:Lorg/apache/pdfbox/cos/COSString;

    return-void
.end method

.method public constructor <init>(Lorg/apache/pdfbox/cos/COSString;)V
    .locals 0

    .line 2
    invoke-direct {p0}, Lorg/apache/pdfbox/pdmodel/common/filespecification/PDFileSpecification;-><init>()V

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/common/filespecification/PDSimpleFileSpecification;->file:Lorg/apache/pdfbox/cos/COSString;

    return-void
.end method


# virtual methods
.method public getCOSObject()Lorg/apache/pdfbox/cos/COSBase;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/filespecification/PDSimpleFileSpecification;->file:Lorg/apache/pdfbox/cos/COSString;

    return-object v0
.end method

.method public getFile()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/filespecification/PDSimpleFileSpecification;->file:Lorg/apache/pdfbox/cos/COSString;

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSString;->getString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public setFile(Ljava/lang/String;)V
    .locals 1

    new-instance v0, Lorg/apache/pdfbox/cos/COSString;

    invoke-direct {v0, p1}, Lorg/apache/pdfbox/cos/COSString;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/filespecification/PDSimpleFileSpecification;->file:Lorg/apache/pdfbox/cos/COSString;

    return-void
.end method

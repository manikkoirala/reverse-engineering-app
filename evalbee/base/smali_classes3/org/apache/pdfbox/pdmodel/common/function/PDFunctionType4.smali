.class public Lorg/apache/pdfbox/pdmodel/common/function/PDFunctionType4;
.super Lorg/apache/pdfbox/pdmodel/common/function/PDFunction;
.source "SourceFile"


# static fields
.field private static final OPERATORS:Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;


# instance fields
.field private final instructions:Lorg/apache/pdfbox/pdmodel/common/function/type4/InstructionSequence;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;

    invoke-direct {v0}, Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;-><init>()V

    sput-object v0, Lorg/apache/pdfbox/pdmodel/common/function/PDFunctionType4;->OPERATORS:Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;

    return-void
.end method

.method public constructor <init>(Lorg/apache/pdfbox/cos/COSBase;)V
    .locals 0

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/common/function/PDFunction;-><init>(Lorg/apache/pdfbox/cos/COSBase;)V

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/common/function/PDFunction;->getPDStream()Lorg/apache/pdfbox/pdmodel/common/PDStream;

    move-result-object p1

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/common/PDStream;->getInputStreamAsString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lorg/apache/pdfbox/pdmodel/common/function/type4/InstructionSequenceBuilder;->parse(Ljava/lang/CharSequence;)Lorg/apache/pdfbox/pdmodel/common/function/type4/InstructionSequence;

    move-result-object p1

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/common/function/PDFunctionType4;->instructions:Lorg/apache/pdfbox/pdmodel/common/function/type4/InstructionSequence;

    return-void
.end method


# virtual methods
.method public eval([F)[F
    .locals 5

    new-instance v0, Lorg/apache/pdfbox/pdmodel/common/function/type4/ExecutionContext;

    sget-object v1, Lorg/apache/pdfbox/pdmodel/common/function/PDFunctionType4;->OPERATORS:Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/pdmodel/common/function/type4/ExecutionContext;-><init>(Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;)V

    const/4 v1, 0x0

    :goto_0
    array-length v2, p1

    if-ge v1, v2, :cond_0

    invoke-virtual {p0, v1}, Lorg/apache/pdfbox/pdmodel/common/function/PDFunction;->getDomainForInput(I)Lorg/apache/pdfbox/pdmodel/common/PDRange;

    move-result-object v2

    aget v3, p1, v1

    invoke-virtual {v2}, Lorg/apache/pdfbox/pdmodel/common/PDRange;->getMin()F

    move-result v4

    invoke-virtual {v2}, Lorg/apache/pdfbox/pdmodel/common/PDRange;->getMax()F

    move-result v2

    invoke-virtual {p0, v3, v4, v2}, Lorg/apache/pdfbox/pdmodel/common/function/PDFunction;->clipToRange(FFF)F

    move-result v2

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/common/function/type4/ExecutionContext;->getStack()Ljava/util/Stack;

    move-result-object v3

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lorg/apache/pdfbox/pdmodel/common/function/PDFunctionType4;->instructions:Lorg/apache/pdfbox/pdmodel/common/function/type4/InstructionSequence;

    invoke-virtual {p1, v0}, Lorg/apache/pdfbox/pdmodel/common/function/type4/InstructionSequence;->execute(Lorg/apache/pdfbox/pdmodel/common/function/type4/ExecutionContext;)V

    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/common/function/PDFunction;->getNumberOfOutputParameters()I

    move-result p1

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/common/function/type4/ExecutionContext;->getStack()Ljava/util/Stack;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/AbstractCollection;->size()I

    move-result v1

    if-lt v1, p1, :cond_2

    new-array v1, p1, [F

    add-int/lit8 p1, p1, -0x1

    :goto_1
    if-ltz p1, :cond_1

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/pdmodel/common/function/PDFunction;->getRangeForOutput(I)Lorg/apache/pdfbox/pdmodel/common/PDRange;

    move-result-object v2

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/common/function/type4/ExecutionContext;->popReal()F

    move-result v3

    aput v3, v1, p1

    invoke-virtual {v2}, Lorg/apache/pdfbox/pdmodel/common/PDRange;->getMin()F

    move-result v4

    invoke-virtual {v2}, Lorg/apache/pdfbox/pdmodel/common/PDRange;->getMax()F

    move-result v2

    invoke-virtual {p0, v3, v4, v2}, Lorg/apache/pdfbox/pdmodel/common/function/PDFunction;->clipToRange(FFF)F

    move-result v2

    aput v2, v1, p1

    add-int/lit8 p1, p1, -0x1

    goto :goto_1

    :cond_1
    return-object v1

    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "The type 4 function returned "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, " values but the Range entry indicates that "

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p1, " values be returned."

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getFunctionType()I
    .locals 1

    const/4 v0, 0x4

    return v0
.end method

.class public Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final ABS:Lorg/apache/pdfbox/pdmodel/common/function/type4/Operator;

.field private static final ADD:Lorg/apache/pdfbox/pdmodel/common/function/type4/Operator;

.field private static final AND:Lorg/apache/pdfbox/pdmodel/common/function/type4/Operator;

.field private static final ATAN:Lorg/apache/pdfbox/pdmodel/common/function/type4/Operator;

.field private static final BITSHIFT:Lorg/apache/pdfbox/pdmodel/common/function/type4/Operator;

.field private static final CEILING:Lorg/apache/pdfbox/pdmodel/common/function/type4/Operator;

.field private static final COPY:Lorg/apache/pdfbox/pdmodel/common/function/type4/Operator;

.field private static final COS:Lorg/apache/pdfbox/pdmodel/common/function/type4/Operator;

.field private static final CVI:Lorg/apache/pdfbox/pdmodel/common/function/type4/Operator;

.field private static final CVR:Lorg/apache/pdfbox/pdmodel/common/function/type4/Operator;

.field private static final DIV:Lorg/apache/pdfbox/pdmodel/common/function/type4/Operator;

.field private static final DUP:Lorg/apache/pdfbox/pdmodel/common/function/type4/Operator;

.field private static final EQ:Lorg/apache/pdfbox/pdmodel/common/function/type4/Operator;

.field private static final EXCH:Lorg/apache/pdfbox/pdmodel/common/function/type4/Operator;

.field private static final EXP:Lorg/apache/pdfbox/pdmodel/common/function/type4/Operator;

.field private static final FALSE:Lorg/apache/pdfbox/pdmodel/common/function/type4/Operator;

.field private static final FLOOR:Lorg/apache/pdfbox/pdmodel/common/function/type4/Operator;

.field private static final GE:Lorg/apache/pdfbox/pdmodel/common/function/type4/Operator;

.field private static final GT:Lorg/apache/pdfbox/pdmodel/common/function/type4/Operator;

.field private static final IDIV:Lorg/apache/pdfbox/pdmodel/common/function/type4/Operator;

.field private static final IF:Lorg/apache/pdfbox/pdmodel/common/function/type4/Operator;

.field private static final IFELSE:Lorg/apache/pdfbox/pdmodel/common/function/type4/Operator;

.field private static final INDEX:Lorg/apache/pdfbox/pdmodel/common/function/type4/Operator;

.field private static final LE:Lorg/apache/pdfbox/pdmodel/common/function/type4/Operator;

.field private static final LN:Lorg/apache/pdfbox/pdmodel/common/function/type4/Operator;

.field private static final LOG:Lorg/apache/pdfbox/pdmodel/common/function/type4/Operator;

.field private static final LT:Lorg/apache/pdfbox/pdmodel/common/function/type4/Operator;

.field private static final MOD:Lorg/apache/pdfbox/pdmodel/common/function/type4/Operator;

.field private static final MUL:Lorg/apache/pdfbox/pdmodel/common/function/type4/Operator;

.field private static final NE:Lorg/apache/pdfbox/pdmodel/common/function/type4/Operator;

.field private static final NEG:Lorg/apache/pdfbox/pdmodel/common/function/type4/Operator;

.field private static final NOT:Lorg/apache/pdfbox/pdmodel/common/function/type4/Operator;

.field private static final OR:Lorg/apache/pdfbox/pdmodel/common/function/type4/Operator;

.field private static final POP:Lorg/apache/pdfbox/pdmodel/common/function/type4/Operator;

.field private static final ROLL:Lorg/apache/pdfbox/pdmodel/common/function/type4/Operator;

.field private static final ROUND:Lorg/apache/pdfbox/pdmodel/common/function/type4/Operator;

.field private static final SIN:Lorg/apache/pdfbox/pdmodel/common/function/type4/Operator;

.field private static final SQRT:Lorg/apache/pdfbox/pdmodel/common/function/type4/Operator;

.field private static final SUB:Lorg/apache/pdfbox/pdmodel/common/function/type4/Operator;

.field private static final TRUE:Lorg/apache/pdfbox/pdmodel/common/function/type4/Operator;

.field private static final TRUNCATE:Lorg/apache/pdfbox/pdmodel/common/function/type4/Operator;

.field private static final XOR:Lorg/apache/pdfbox/pdmodel/common/function/type4/Operator;


# instance fields
.field private operators:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lorg/apache/pdfbox/pdmodel/common/function/type4/Operator;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lorg/apache/pdfbox/pdmodel/common/function/type4/ArithmeticOperators$Abs;

    invoke-direct {v0}, Lorg/apache/pdfbox/pdmodel/common/function/type4/ArithmeticOperators$Abs;-><init>()V

    sput-object v0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;->ABS:Lorg/apache/pdfbox/pdmodel/common/function/type4/Operator;

    new-instance v0, Lorg/apache/pdfbox/pdmodel/common/function/type4/ArithmeticOperators$Add;

    invoke-direct {v0}, Lorg/apache/pdfbox/pdmodel/common/function/type4/ArithmeticOperators$Add;-><init>()V

    sput-object v0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;->ADD:Lorg/apache/pdfbox/pdmodel/common/function/type4/Operator;

    new-instance v0, Lorg/apache/pdfbox/pdmodel/common/function/type4/ArithmeticOperators$Atan;

    invoke-direct {v0}, Lorg/apache/pdfbox/pdmodel/common/function/type4/ArithmeticOperators$Atan;-><init>()V

    sput-object v0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;->ATAN:Lorg/apache/pdfbox/pdmodel/common/function/type4/Operator;

    new-instance v0, Lorg/apache/pdfbox/pdmodel/common/function/type4/ArithmeticOperators$Ceiling;

    invoke-direct {v0}, Lorg/apache/pdfbox/pdmodel/common/function/type4/ArithmeticOperators$Ceiling;-><init>()V

    sput-object v0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;->CEILING:Lorg/apache/pdfbox/pdmodel/common/function/type4/Operator;

    new-instance v0, Lorg/apache/pdfbox/pdmodel/common/function/type4/ArithmeticOperators$Cos;

    invoke-direct {v0}, Lorg/apache/pdfbox/pdmodel/common/function/type4/ArithmeticOperators$Cos;-><init>()V

    sput-object v0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;->COS:Lorg/apache/pdfbox/pdmodel/common/function/type4/Operator;

    new-instance v0, Lorg/apache/pdfbox/pdmodel/common/function/type4/ArithmeticOperators$Cvi;

    invoke-direct {v0}, Lorg/apache/pdfbox/pdmodel/common/function/type4/ArithmeticOperators$Cvi;-><init>()V

    sput-object v0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;->CVI:Lorg/apache/pdfbox/pdmodel/common/function/type4/Operator;

    new-instance v0, Lorg/apache/pdfbox/pdmodel/common/function/type4/ArithmeticOperators$Cvr;

    invoke-direct {v0}, Lorg/apache/pdfbox/pdmodel/common/function/type4/ArithmeticOperators$Cvr;-><init>()V

    sput-object v0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;->CVR:Lorg/apache/pdfbox/pdmodel/common/function/type4/Operator;

    new-instance v0, Lorg/apache/pdfbox/pdmodel/common/function/type4/ArithmeticOperators$Div;

    invoke-direct {v0}, Lorg/apache/pdfbox/pdmodel/common/function/type4/ArithmeticOperators$Div;-><init>()V

    sput-object v0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;->DIV:Lorg/apache/pdfbox/pdmodel/common/function/type4/Operator;

    new-instance v0, Lorg/apache/pdfbox/pdmodel/common/function/type4/ArithmeticOperators$Exp;

    invoke-direct {v0}, Lorg/apache/pdfbox/pdmodel/common/function/type4/ArithmeticOperators$Exp;-><init>()V

    sput-object v0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;->EXP:Lorg/apache/pdfbox/pdmodel/common/function/type4/Operator;

    new-instance v0, Lorg/apache/pdfbox/pdmodel/common/function/type4/ArithmeticOperators$Floor;

    invoke-direct {v0}, Lorg/apache/pdfbox/pdmodel/common/function/type4/ArithmeticOperators$Floor;-><init>()V

    sput-object v0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;->FLOOR:Lorg/apache/pdfbox/pdmodel/common/function/type4/Operator;

    new-instance v0, Lorg/apache/pdfbox/pdmodel/common/function/type4/ArithmeticOperators$IDiv;

    invoke-direct {v0}, Lorg/apache/pdfbox/pdmodel/common/function/type4/ArithmeticOperators$IDiv;-><init>()V

    sput-object v0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;->IDIV:Lorg/apache/pdfbox/pdmodel/common/function/type4/Operator;

    new-instance v0, Lorg/apache/pdfbox/pdmodel/common/function/type4/ArithmeticOperators$Ln;

    invoke-direct {v0}, Lorg/apache/pdfbox/pdmodel/common/function/type4/ArithmeticOperators$Ln;-><init>()V

    sput-object v0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;->LN:Lorg/apache/pdfbox/pdmodel/common/function/type4/Operator;

    new-instance v0, Lorg/apache/pdfbox/pdmodel/common/function/type4/ArithmeticOperators$Log;

    invoke-direct {v0}, Lorg/apache/pdfbox/pdmodel/common/function/type4/ArithmeticOperators$Log;-><init>()V

    sput-object v0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;->LOG:Lorg/apache/pdfbox/pdmodel/common/function/type4/Operator;

    new-instance v0, Lorg/apache/pdfbox/pdmodel/common/function/type4/ArithmeticOperators$Mod;

    invoke-direct {v0}, Lorg/apache/pdfbox/pdmodel/common/function/type4/ArithmeticOperators$Mod;-><init>()V

    sput-object v0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;->MOD:Lorg/apache/pdfbox/pdmodel/common/function/type4/Operator;

    new-instance v0, Lorg/apache/pdfbox/pdmodel/common/function/type4/ArithmeticOperators$Mul;

    invoke-direct {v0}, Lorg/apache/pdfbox/pdmodel/common/function/type4/ArithmeticOperators$Mul;-><init>()V

    sput-object v0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;->MUL:Lorg/apache/pdfbox/pdmodel/common/function/type4/Operator;

    new-instance v0, Lorg/apache/pdfbox/pdmodel/common/function/type4/ArithmeticOperators$Neg;

    invoke-direct {v0}, Lorg/apache/pdfbox/pdmodel/common/function/type4/ArithmeticOperators$Neg;-><init>()V

    sput-object v0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;->NEG:Lorg/apache/pdfbox/pdmodel/common/function/type4/Operator;

    new-instance v0, Lorg/apache/pdfbox/pdmodel/common/function/type4/ArithmeticOperators$Round;

    invoke-direct {v0}, Lorg/apache/pdfbox/pdmodel/common/function/type4/ArithmeticOperators$Round;-><init>()V

    sput-object v0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;->ROUND:Lorg/apache/pdfbox/pdmodel/common/function/type4/Operator;

    new-instance v0, Lorg/apache/pdfbox/pdmodel/common/function/type4/ArithmeticOperators$Sin;

    invoke-direct {v0}, Lorg/apache/pdfbox/pdmodel/common/function/type4/ArithmeticOperators$Sin;-><init>()V

    sput-object v0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;->SIN:Lorg/apache/pdfbox/pdmodel/common/function/type4/Operator;

    new-instance v0, Lorg/apache/pdfbox/pdmodel/common/function/type4/ArithmeticOperators$Sqrt;

    invoke-direct {v0}, Lorg/apache/pdfbox/pdmodel/common/function/type4/ArithmeticOperators$Sqrt;-><init>()V

    sput-object v0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;->SQRT:Lorg/apache/pdfbox/pdmodel/common/function/type4/Operator;

    new-instance v0, Lorg/apache/pdfbox/pdmodel/common/function/type4/ArithmeticOperators$Sub;

    invoke-direct {v0}, Lorg/apache/pdfbox/pdmodel/common/function/type4/ArithmeticOperators$Sub;-><init>()V

    sput-object v0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;->SUB:Lorg/apache/pdfbox/pdmodel/common/function/type4/Operator;

    new-instance v0, Lorg/apache/pdfbox/pdmodel/common/function/type4/ArithmeticOperators$Truncate;

    invoke-direct {v0}, Lorg/apache/pdfbox/pdmodel/common/function/type4/ArithmeticOperators$Truncate;-><init>()V

    sput-object v0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;->TRUNCATE:Lorg/apache/pdfbox/pdmodel/common/function/type4/Operator;

    new-instance v0, Lorg/apache/pdfbox/pdmodel/common/function/type4/BitwiseOperators$And;

    invoke-direct {v0}, Lorg/apache/pdfbox/pdmodel/common/function/type4/BitwiseOperators$And;-><init>()V

    sput-object v0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;->AND:Lorg/apache/pdfbox/pdmodel/common/function/type4/Operator;

    new-instance v0, Lorg/apache/pdfbox/pdmodel/common/function/type4/BitwiseOperators$Bitshift;

    invoke-direct {v0}, Lorg/apache/pdfbox/pdmodel/common/function/type4/BitwiseOperators$Bitshift;-><init>()V

    sput-object v0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;->BITSHIFT:Lorg/apache/pdfbox/pdmodel/common/function/type4/Operator;

    new-instance v0, Lorg/apache/pdfbox/pdmodel/common/function/type4/RelationalOperators$Eq;

    invoke-direct {v0}, Lorg/apache/pdfbox/pdmodel/common/function/type4/RelationalOperators$Eq;-><init>()V

    sput-object v0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;->EQ:Lorg/apache/pdfbox/pdmodel/common/function/type4/Operator;

    new-instance v0, Lorg/apache/pdfbox/pdmodel/common/function/type4/BitwiseOperators$False;

    invoke-direct {v0}, Lorg/apache/pdfbox/pdmodel/common/function/type4/BitwiseOperators$False;-><init>()V

    sput-object v0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;->FALSE:Lorg/apache/pdfbox/pdmodel/common/function/type4/Operator;

    new-instance v0, Lorg/apache/pdfbox/pdmodel/common/function/type4/RelationalOperators$Ge;

    invoke-direct {v0}, Lorg/apache/pdfbox/pdmodel/common/function/type4/RelationalOperators$Ge;-><init>()V

    sput-object v0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;->GE:Lorg/apache/pdfbox/pdmodel/common/function/type4/Operator;

    new-instance v0, Lorg/apache/pdfbox/pdmodel/common/function/type4/RelationalOperators$Gt;

    invoke-direct {v0}, Lorg/apache/pdfbox/pdmodel/common/function/type4/RelationalOperators$Gt;-><init>()V

    sput-object v0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;->GT:Lorg/apache/pdfbox/pdmodel/common/function/type4/Operator;

    new-instance v0, Lorg/apache/pdfbox/pdmodel/common/function/type4/RelationalOperators$Le;

    invoke-direct {v0}, Lorg/apache/pdfbox/pdmodel/common/function/type4/RelationalOperators$Le;-><init>()V

    sput-object v0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;->LE:Lorg/apache/pdfbox/pdmodel/common/function/type4/Operator;

    new-instance v0, Lorg/apache/pdfbox/pdmodel/common/function/type4/RelationalOperators$Lt;

    invoke-direct {v0}, Lorg/apache/pdfbox/pdmodel/common/function/type4/RelationalOperators$Lt;-><init>()V

    sput-object v0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;->LT:Lorg/apache/pdfbox/pdmodel/common/function/type4/Operator;

    new-instance v0, Lorg/apache/pdfbox/pdmodel/common/function/type4/RelationalOperators$Ne;

    invoke-direct {v0}, Lorg/apache/pdfbox/pdmodel/common/function/type4/RelationalOperators$Ne;-><init>()V

    sput-object v0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;->NE:Lorg/apache/pdfbox/pdmodel/common/function/type4/Operator;

    new-instance v0, Lorg/apache/pdfbox/pdmodel/common/function/type4/BitwiseOperators$Not;

    invoke-direct {v0}, Lorg/apache/pdfbox/pdmodel/common/function/type4/BitwiseOperators$Not;-><init>()V

    sput-object v0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;->NOT:Lorg/apache/pdfbox/pdmodel/common/function/type4/Operator;

    new-instance v0, Lorg/apache/pdfbox/pdmodel/common/function/type4/BitwiseOperators$Or;

    invoke-direct {v0}, Lorg/apache/pdfbox/pdmodel/common/function/type4/BitwiseOperators$Or;-><init>()V

    sput-object v0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;->OR:Lorg/apache/pdfbox/pdmodel/common/function/type4/Operator;

    new-instance v0, Lorg/apache/pdfbox/pdmodel/common/function/type4/BitwiseOperators$True;

    invoke-direct {v0}, Lorg/apache/pdfbox/pdmodel/common/function/type4/BitwiseOperators$True;-><init>()V

    sput-object v0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;->TRUE:Lorg/apache/pdfbox/pdmodel/common/function/type4/Operator;

    new-instance v0, Lorg/apache/pdfbox/pdmodel/common/function/type4/BitwiseOperators$Xor;

    invoke-direct {v0}, Lorg/apache/pdfbox/pdmodel/common/function/type4/BitwiseOperators$Xor;-><init>()V

    sput-object v0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;->XOR:Lorg/apache/pdfbox/pdmodel/common/function/type4/Operator;

    new-instance v0, Lorg/apache/pdfbox/pdmodel/common/function/type4/ConditionalOperators$If;

    invoke-direct {v0}, Lorg/apache/pdfbox/pdmodel/common/function/type4/ConditionalOperators$If;-><init>()V

    sput-object v0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;->IF:Lorg/apache/pdfbox/pdmodel/common/function/type4/Operator;

    new-instance v0, Lorg/apache/pdfbox/pdmodel/common/function/type4/ConditionalOperators$IfElse;

    invoke-direct {v0}, Lorg/apache/pdfbox/pdmodel/common/function/type4/ConditionalOperators$IfElse;-><init>()V

    sput-object v0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;->IFELSE:Lorg/apache/pdfbox/pdmodel/common/function/type4/Operator;

    new-instance v0, Lorg/apache/pdfbox/pdmodel/common/function/type4/StackOperators$Copy;

    invoke-direct {v0}, Lorg/apache/pdfbox/pdmodel/common/function/type4/StackOperators$Copy;-><init>()V

    sput-object v0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;->COPY:Lorg/apache/pdfbox/pdmodel/common/function/type4/Operator;

    new-instance v0, Lorg/apache/pdfbox/pdmodel/common/function/type4/StackOperators$Dup;

    invoke-direct {v0}, Lorg/apache/pdfbox/pdmodel/common/function/type4/StackOperators$Dup;-><init>()V

    sput-object v0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;->DUP:Lorg/apache/pdfbox/pdmodel/common/function/type4/Operator;

    new-instance v0, Lorg/apache/pdfbox/pdmodel/common/function/type4/StackOperators$Exch;

    invoke-direct {v0}, Lorg/apache/pdfbox/pdmodel/common/function/type4/StackOperators$Exch;-><init>()V

    sput-object v0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;->EXCH:Lorg/apache/pdfbox/pdmodel/common/function/type4/Operator;

    new-instance v0, Lorg/apache/pdfbox/pdmodel/common/function/type4/StackOperators$Index;

    invoke-direct {v0}, Lorg/apache/pdfbox/pdmodel/common/function/type4/StackOperators$Index;-><init>()V

    sput-object v0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;->INDEX:Lorg/apache/pdfbox/pdmodel/common/function/type4/Operator;

    new-instance v0, Lorg/apache/pdfbox/pdmodel/common/function/type4/StackOperators$Pop;

    invoke-direct {v0}, Lorg/apache/pdfbox/pdmodel/common/function/type4/StackOperators$Pop;-><init>()V

    sput-object v0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;->POP:Lorg/apache/pdfbox/pdmodel/common/function/type4/Operator;

    new-instance v0, Lorg/apache/pdfbox/pdmodel/common/function/type4/StackOperators$Roll;

    invoke-direct {v0}, Lorg/apache/pdfbox/pdmodel/common/function/type4/StackOperators$Roll;-><init>()V

    sput-object v0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;->ROLL:Lorg/apache/pdfbox/pdmodel/common/function/type4/Operator;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;->operators:Ljava/util/Map;

    const-string v1, "add"

    sget-object v2, Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;->ADD:Lorg/apache/pdfbox/pdmodel/common/function/type4/Operator;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;->operators:Ljava/util/Map;

    const-string v1, "abs"

    sget-object v2, Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;->ABS:Lorg/apache/pdfbox/pdmodel/common/function/type4/Operator;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;->operators:Ljava/util/Map;

    const-string v1, "atan"

    sget-object v2, Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;->ATAN:Lorg/apache/pdfbox/pdmodel/common/function/type4/Operator;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;->operators:Ljava/util/Map;

    const-string v1, "ceiling"

    sget-object v2, Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;->CEILING:Lorg/apache/pdfbox/pdmodel/common/function/type4/Operator;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;->operators:Ljava/util/Map;

    const-string v1, "cos"

    sget-object v2, Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;->COS:Lorg/apache/pdfbox/pdmodel/common/function/type4/Operator;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;->operators:Ljava/util/Map;

    const-string v1, "cvi"

    sget-object v2, Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;->CVI:Lorg/apache/pdfbox/pdmodel/common/function/type4/Operator;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;->operators:Ljava/util/Map;

    const-string v1, "cvr"

    sget-object v2, Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;->CVR:Lorg/apache/pdfbox/pdmodel/common/function/type4/Operator;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;->operators:Ljava/util/Map;

    const-string v1, "div"

    sget-object v2, Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;->DIV:Lorg/apache/pdfbox/pdmodel/common/function/type4/Operator;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;->operators:Ljava/util/Map;

    const-string v1, "exp"

    sget-object v2, Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;->EXP:Lorg/apache/pdfbox/pdmodel/common/function/type4/Operator;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;->operators:Ljava/util/Map;

    const-string v1, "floor"

    sget-object v2, Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;->FLOOR:Lorg/apache/pdfbox/pdmodel/common/function/type4/Operator;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;->operators:Ljava/util/Map;

    const-string v1, "idiv"

    sget-object v2, Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;->IDIV:Lorg/apache/pdfbox/pdmodel/common/function/type4/Operator;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;->operators:Ljava/util/Map;

    const-string v1, "ln"

    sget-object v2, Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;->LN:Lorg/apache/pdfbox/pdmodel/common/function/type4/Operator;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;->operators:Ljava/util/Map;

    const-string v1, "log"

    sget-object v2, Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;->LOG:Lorg/apache/pdfbox/pdmodel/common/function/type4/Operator;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;->operators:Ljava/util/Map;

    const-string v1, "mod"

    sget-object v2, Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;->MOD:Lorg/apache/pdfbox/pdmodel/common/function/type4/Operator;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;->operators:Ljava/util/Map;

    const-string v1, "mul"

    sget-object v2, Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;->MUL:Lorg/apache/pdfbox/pdmodel/common/function/type4/Operator;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;->operators:Ljava/util/Map;

    const-string v1, "neg"

    sget-object v2, Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;->NEG:Lorg/apache/pdfbox/pdmodel/common/function/type4/Operator;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;->operators:Ljava/util/Map;

    const-string v1, "round"

    sget-object v2, Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;->ROUND:Lorg/apache/pdfbox/pdmodel/common/function/type4/Operator;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;->operators:Ljava/util/Map;

    const-string v1, "sin"

    sget-object v2, Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;->SIN:Lorg/apache/pdfbox/pdmodel/common/function/type4/Operator;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;->operators:Ljava/util/Map;

    const-string v1, "sqrt"

    sget-object v2, Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;->SQRT:Lorg/apache/pdfbox/pdmodel/common/function/type4/Operator;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;->operators:Ljava/util/Map;

    const-string v1, "sub"

    sget-object v2, Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;->SUB:Lorg/apache/pdfbox/pdmodel/common/function/type4/Operator;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;->operators:Ljava/util/Map;

    const-string v1, "truncate"

    sget-object v2, Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;->TRUNCATE:Lorg/apache/pdfbox/pdmodel/common/function/type4/Operator;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;->operators:Ljava/util/Map;

    const-string v1, "and"

    sget-object v2, Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;->AND:Lorg/apache/pdfbox/pdmodel/common/function/type4/Operator;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;->operators:Ljava/util/Map;

    const-string v1, "bitshift"

    sget-object v2, Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;->BITSHIFT:Lorg/apache/pdfbox/pdmodel/common/function/type4/Operator;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;->operators:Ljava/util/Map;

    const-string v1, "eq"

    sget-object v2, Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;->EQ:Lorg/apache/pdfbox/pdmodel/common/function/type4/Operator;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;->operators:Ljava/util/Map;

    const-string v1, "false"

    sget-object v2, Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;->FALSE:Lorg/apache/pdfbox/pdmodel/common/function/type4/Operator;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;->operators:Ljava/util/Map;

    const-string v1, "ge"

    sget-object v2, Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;->GE:Lorg/apache/pdfbox/pdmodel/common/function/type4/Operator;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;->operators:Ljava/util/Map;

    const-string v1, "gt"

    sget-object v2, Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;->GT:Lorg/apache/pdfbox/pdmodel/common/function/type4/Operator;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;->operators:Ljava/util/Map;

    const-string v1, "le"

    sget-object v2, Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;->LE:Lorg/apache/pdfbox/pdmodel/common/function/type4/Operator;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;->operators:Ljava/util/Map;

    const-string v1, "lt"

    sget-object v2, Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;->LT:Lorg/apache/pdfbox/pdmodel/common/function/type4/Operator;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;->operators:Ljava/util/Map;

    const-string v1, "ne"

    sget-object v2, Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;->NE:Lorg/apache/pdfbox/pdmodel/common/function/type4/Operator;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;->operators:Ljava/util/Map;

    const-string v1, "not"

    sget-object v2, Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;->NOT:Lorg/apache/pdfbox/pdmodel/common/function/type4/Operator;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;->operators:Ljava/util/Map;

    const-string v1, "or"

    sget-object v2, Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;->OR:Lorg/apache/pdfbox/pdmodel/common/function/type4/Operator;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;->operators:Ljava/util/Map;

    const-string v1, "true"

    sget-object v2, Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;->TRUE:Lorg/apache/pdfbox/pdmodel/common/function/type4/Operator;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;->operators:Ljava/util/Map;

    const-string v1, "xor"

    sget-object v2, Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;->XOR:Lorg/apache/pdfbox/pdmodel/common/function/type4/Operator;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;->operators:Ljava/util/Map;

    const-string v1, "if"

    sget-object v2, Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;->IF:Lorg/apache/pdfbox/pdmodel/common/function/type4/Operator;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;->operators:Ljava/util/Map;

    const-string v1, "ifelse"

    sget-object v2, Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;->IFELSE:Lorg/apache/pdfbox/pdmodel/common/function/type4/Operator;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;->operators:Ljava/util/Map;

    const-string v1, "copy"

    sget-object v2, Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;->COPY:Lorg/apache/pdfbox/pdmodel/common/function/type4/Operator;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;->operators:Ljava/util/Map;

    const-string v1, "dup"

    sget-object v2, Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;->DUP:Lorg/apache/pdfbox/pdmodel/common/function/type4/Operator;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;->operators:Ljava/util/Map;

    const-string v1, "exch"

    sget-object v2, Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;->EXCH:Lorg/apache/pdfbox/pdmodel/common/function/type4/Operator;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;->operators:Ljava/util/Map;

    const-string v1, "index"

    sget-object v2, Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;->INDEX:Lorg/apache/pdfbox/pdmodel/common/function/type4/Operator;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;->operators:Ljava/util/Map;

    const-string v1, "pop"

    sget-object v2, Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;->POP:Lorg/apache/pdfbox/pdmodel/common/function/type4/Operator;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;->operators:Ljava/util/Map;

    const-string v1, "roll"

    sget-object v2, Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;->ROLL:Lorg/apache/pdfbox/pdmodel/common/function/type4/Operator;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public getOperator(Ljava/lang/String;)Lorg/apache/pdfbox/pdmodel/common/function/type4/Operator;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/function/type4/Operators;->operators:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lorg/apache/pdfbox/pdmodel/common/function/type4/Operator;

    return-object p1
.end method

.class public Lorg/apache/pdfbox/pdmodel/common/PDNamedTextStream;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/pdfbox/pdmodel/common/DualCOSObjectable;


# instance fields
.field private stream:Lorg/apache/pdfbox/pdmodel/common/PDTextStream;

.field private streamName:Lorg/apache/pdfbox/cos/COSName;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/common/PDNamedTextStream;->streamName:Lorg/apache/pdfbox/cos/COSName;

    invoke-static {p2}, Lorg/apache/pdfbox/pdmodel/common/PDTextStream;->createTextStream(Lorg/apache/pdfbox/cos/COSBase;)Lorg/apache/pdfbox/pdmodel/common/PDTextStream;

    move-result-object p1

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/common/PDNamedTextStream;->stream:Lorg/apache/pdfbox/pdmodel/common/PDTextStream;

    return-void
.end method


# virtual methods
.method public getFirstCOSObject()Lorg/apache/pdfbox/cos/COSBase;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/PDNamedTextStream;->streamName:Lorg/apache/pdfbox/cos/COSName;

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/PDNamedTextStream;->streamName:Lorg/apache/pdfbox/cos/COSName;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSName;->getName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public getSecondCOSObject()Lorg/apache/pdfbox/cos/COSBase;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/PDNamedTextStream;->stream:Lorg/apache/pdfbox/pdmodel/common/PDTextStream;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/common/PDTextStream;->getCOSObject()Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0
.end method

.method public getStream()Lorg/apache/pdfbox/pdmodel/common/PDTextStream;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/common/PDNamedTextStream;->stream:Lorg/apache/pdfbox/pdmodel/common/PDTextStream;

    return-object v0
.end method

.method public setName(Ljava/lang/String;)V
    .locals 0

    invoke-static {p1}, Lorg/apache/pdfbox/cos/COSName;->getPDFName(Ljava/lang/String;)Lorg/apache/pdfbox/cos/COSName;

    move-result-object p1

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/common/PDNamedTextStream;->streamName:Lorg/apache/pdfbox/cos/COSName;

    return-void
.end method

.method public setStream(Lorg/apache/pdfbox/pdmodel/common/PDTextStream;)V
    .locals 0

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/common/PDNamedTextStream;->stream:Lorg/apache/pdfbox/pdmodel/common/PDTextStream;

    return-void
.end method

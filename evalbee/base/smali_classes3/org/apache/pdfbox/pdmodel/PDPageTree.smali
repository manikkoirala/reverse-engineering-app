.class public Lorg/apache/pdfbox/pdmodel/PDPageTree;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/pdfbox/pdmodel/common/COSObjectable;
.implements Ljava/lang/Iterable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/pdfbox/pdmodel/PDPageTree$PageIterator;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lorg/apache/pdfbox/pdmodel/common/COSObjectable;",
        "Ljava/lang/Iterable<",
        "Lorg/apache/pdfbox/pdmodel/PDPage;",
        ">;"
    }
.end annotation


# instance fields
.field private final root:Lorg/apache/pdfbox/cos/COSDictionary;


# direct methods
.method public constructor <init>()V
    .locals 3

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-direct {v0}, Lorg/apache/pdfbox/cos/COSDictionary;-><init>()V

    iput-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDPageTree;->root:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->TYPE:Lorg/apache/pdfbox/cos/COSName;

    sget-object v2, Lorg/apache/pdfbox/cos/COSName;->PAGES:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->KIDS:Lorg/apache/pdfbox/cos/COSName;

    new-instance v2, Lorg/apache/pdfbox/cos/COSArray;

    invoke-direct {v2}, Lorg/apache/pdfbox/cos/COSArray;-><init>()V

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->COUNT:Lorg/apache/pdfbox/cos/COSName;

    sget-object v2, Lorg/apache/pdfbox/cos/COSInteger;->ZERO:Lorg/apache/pdfbox/cos/COSInteger;

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    return-void
.end method

.method public constructor <init>(Lorg/apache/pdfbox/cos/COSDictionary;)V
    .locals 1

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-eqz p1, :cond_0

    iput-object p1, p0, Lorg/apache/pdfbox/pdmodel/PDPageTree;->root:Lorg/apache/pdfbox/cos/COSDictionary;

    return-void

    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "root cannot be null"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public static synthetic access$100(Lorg/apache/pdfbox/pdmodel/PDPageTree;Lorg/apache/pdfbox/cos/COSDictionary;)Z
    .locals 0

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/PDPageTree;->isPageTreeNode(Lorg/apache/pdfbox/cos/COSDictionary;)Z

    move-result p0

    return p0
.end method

.method public static synthetic access$200(Lorg/apache/pdfbox/pdmodel/PDPageTree;Lorg/apache/pdfbox/cos/COSDictionary;)Ljava/util/List;
    .locals 0

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/PDPageTree;->getKids(Lorg/apache/pdfbox/cos/COSDictionary;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method private get(ILorg/apache/pdfbox/cos/COSDictionary;I)Lorg/apache/pdfbox/cos/COSDictionary;
    .locals 3

    .line 1
    const-string v0, "Index out of bounds: "

    if-ltz p1, :cond_7

    invoke-direct {p0, p2}, Lorg/apache/pdfbox/pdmodel/PDPageTree;->isPageTreeNode(Lorg/apache/pdfbox/cos/COSDictionary;)Z

    move-result v1

    if-eqz v1, :cond_5

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->COUNT:Lorg/apache/pdfbox/cos/COSName;

    const/4 v2, 0x0

    invoke-virtual {p2, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->getInt(Lorg/apache/pdfbox/cos/COSName;I)I

    move-result v1

    add-int/2addr v1, p3

    if-gt p1, v1, :cond_4

    invoke-direct {p0, p2}, Lorg/apache/pdfbox/pdmodel/PDPageTree;->getKids(Lorg/apache/pdfbox/cos/COSDictionary;)Ljava/util/List;

    move-result-object p2

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :cond_0
    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/pdmodel/PDPageTree;->isPageTreeNode(Lorg/apache/pdfbox/cos/COSDictionary;)Z

    move-result v1

    if-eqz v1, :cond_2

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->COUNT:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->getInt(Lorg/apache/pdfbox/cos/COSName;I)I

    move-result v1

    add-int/2addr v1, p3

    if-gt p1, v1, :cond_1

    invoke-direct {p0, p1, v0, p3}, Lorg/apache/pdfbox/pdmodel/PDPageTree;->get(ILorg/apache/pdfbox/cos/COSDictionary;I)Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object p1

    return-object p1

    :cond_1
    move p3, v1

    goto :goto_0

    :cond_2
    add-int/lit8 p3, p3, 0x1

    if-ne p1, p3, :cond_0

    invoke-direct {p0, p1, v0, p3}, Lorg/apache/pdfbox/pdmodel/PDPageTree;->get(ILorg/apache/pdfbox/cos/COSDictionary;I)Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object p1

    return-object p1

    :cond_3
    new-instance p1, Ljava/lang/IllegalStateException;

    invoke-direct {p1}, Ljava/lang/IllegalStateException;-><init>()V

    throw p1

    :cond_4
    new-instance p2, Ljava/lang/IndexOutOfBoundsException;

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw p2

    :cond_5
    if-ne p3, p1, :cond_6

    return-object p2

    :cond_6
    new-instance p1, Ljava/lang/IllegalStateException;

    invoke-direct {p1}, Ljava/lang/IllegalStateException;-><init>()V

    throw p1

    :cond_7
    new-instance p2, Ljava/lang/IndexOutOfBoundsException;

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw p2
.end method

.method public static getInheritableAttribute(Lorg/apache/pdfbox/cos/COSDictionary;Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;
    .locals 2

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    if-eqz v0, :cond_0

    return-object v0

    :cond_0
    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->PARENT:Lorg/apache/pdfbox/cos/COSName;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->P:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object p0

    check-cast p0, Lorg/apache/pdfbox/cos/COSDictionary;

    if-eqz p0, :cond_1

    invoke-static {p0, p1}, Lorg/apache/pdfbox/pdmodel/PDPageTree;->getInheritableAttribute(Lorg/apache/pdfbox/cos/COSDictionary;Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object p0

    return-object p0

    :cond_1
    const/4 p0, 0x0

    return-object p0
.end method

.method private getKids(Lorg/apache/pdfbox/cos/COSDictionary;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/pdfbox/cos/COSDictionary;",
            ")",
            "Ljava/util/List<",
            "Lorg/apache/pdfbox/cos/COSDictionary;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->KIDS:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p1, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object p1

    check-cast p1, Lorg/apache/pdfbox/cos/COSArray;

    if-nez p1, :cond_0

    return-object v0

    :cond_0
    invoke-virtual {p1}, Lorg/apache/pdfbox/cos/COSArray;->size()I

    move-result v1

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_1

    invoke-virtual {p1, v2}, Lorg/apache/pdfbox/cos/COSArray;->getObject(I)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v3

    check-cast v3, Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    return-object v0
.end method

.method private isPageTreeNode(Lorg/apache/pdfbox/cos/COSDictionary;)Z
    .locals 2

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->TYPE:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p1, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->getCOSName(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSName;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->PAGES:Lorg/apache/pdfbox/cos/COSName;

    if-eq v0, v1, :cond_1

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->KIDS:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p1, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->containsKey(Lorg/apache/pdfbox/cos/COSName;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method

.method private remove(Lorg/apache/pdfbox/cos/COSDictionary;)V
    .locals 2

    .line 2
    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->PARENT:Lorg/apache/pdfbox/cos/COSName;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->P:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p1, v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->KIDS:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSArray;

    invoke-virtual {v0, p1}, Lorg/apache/pdfbox/cos/COSArray;->removeObject(Lorg/apache/pdfbox/cos/COSBase;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->PARENT:Lorg/apache/pdfbox/cos/COSName;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->P:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p1, v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object p1

    check-cast p1, Lorg/apache/pdfbox/cos/COSDictionary;

    if-eqz p1, :cond_1

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->COUNT:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p1, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->getInt(Lorg/apache/pdfbox/cos/COSName;)I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p1, v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->setInt(Lorg/apache/pdfbox/cos/COSName;I)V

    :cond_1
    if-nez p1, :cond_0

    :cond_2
    return-void
.end method


# virtual methods
.method public add(Lorg/apache/pdfbox/pdmodel/PDPage;)V
    .locals 2

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/PDPage;->getCOSObject()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object p1

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->PARENT:Lorg/apache/pdfbox/cos/COSName;

    iget-object v1, p0, Lorg/apache/pdfbox/pdmodel/PDPageTree;->root:Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-virtual {p1, v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDPageTree;->root:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->KIDS:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSArray;

    invoke-virtual {v0, p1}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    :cond_0
    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->PARENT:Lorg/apache/pdfbox/cos/COSName;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->P:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p1, v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object p1

    check-cast p1, Lorg/apache/pdfbox/cos/COSDictionary;

    if-eqz p1, :cond_1

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->COUNT:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p1, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->getInt(Lorg/apache/pdfbox/cos/COSName;)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {p1, v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->setInt(Lorg/apache/pdfbox/cos/COSName;I)V

    :cond_1
    if-nez p1, :cond_0

    return-void
.end method

.method public get(I)Lorg/apache/pdfbox/pdmodel/PDPage;
    .locals 3

    .line 2
    add-int/lit8 p1, p1, 0x1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDPageTree;->root:Lorg/apache/pdfbox/cos/COSDictionary;

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lorg/apache/pdfbox/pdmodel/PDPageTree;->get(ILorg/apache/pdfbox/cos/COSDictionary;I)Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object p1

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->TYPE:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p1, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->getCOSName(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSName;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->PAGE:Lorg/apache/pdfbox/cos/COSName;

    if-ne v0, v1, :cond_0

    new-instance v0, Lorg/apache/pdfbox/pdmodel/PDPage;

    invoke-direct {v0, p1}, Lorg/apache/pdfbox/pdmodel/PDPage;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expected Page but got "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public bridge synthetic getCOSObject()Lorg/apache/pdfbox/cos/COSBase;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lorg/apache/pdfbox/pdmodel/PDPageTree;->getCOSObject()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    return-object v0
.end method

.method public getCOSObject()Lorg/apache/pdfbox/cos/COSDictionary;
    .locals 1

    .line 2
    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDPageTree;->root:Lorg/apache/pdfbox/cos/COSDictionary;

    return-object v0
.end method

.method public getCount()I
    .locals 3

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDPageTree;->root:Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->COUNT:Lorg/apache/pdfbox/cos/COSName;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->getInt(Lorg/apache/pdfbox/cos/COSName;I)I

    move-result v0

    return v0
.end method

.method public indexOf(Lorg/apache/pdfbox/pdmodel/PDPage;)I
    .locals 3

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/PDPage;->getCOSObject()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object p1

    const/4 v0, 0x0

    :cond_0
    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/PDPageTree;->isPageTreeNode(Lorg/apache/pdfbox/cos/COSDictionary;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/PDPageTree;->getKids(Lorg/apache/pdfbox/cos/COSDictionary;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/pdfbox/cos/COSDictionary;

    if-ne v2, p1, :cond_1

    goto :goto_1

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    add-int/lit8 v0, v0, 0x1

    :cond_3
    :goto_1
    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->PARENT:Lorg/apache/pdfbox/cos/COSName;

    sget-object v2, Lorg/apache/pdfbox/cos/COSName;->P:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p1, v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object p1

    check-cast p1, Lorg/apache/pdfbox/cos/COSDictionary;

    if-nez p1, :cond_0

    add-int/lit8 v0, v0, -0x1

    return v0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator<",
            "Lorg/apache/pdfbox/pdmodel/PDPage;",
            ">;"
        }
    .end annotation

    new-instance v0, Lorg/apache/pdfbox/pdmodel/PDPageTree$PageIterator;

    iget-object v1, p0, Lorg/apache/pdfbox/pdmodel/PDPageTree;->root:Lorg/apache/pdfbox/cos/COSDictionary;

    const/4 v2, 0x0

    invoke-direct {v0, p0, v1, v2}, Lorg/apache/pdfbox/pdmodel/PDPageTree$PageIterator;-><init>(Lorg/apache/pdfbox/pdmodel/PDPageTree;Lorg/apache/pdfbox/cos/COSDictionary;Lorg/apache/pdfbox/pdmodel/PDPageTree$1;)V

    return-object v0
.end method

.method public remove(I)V
    .locals 2

    .line 1
    add-int/lit8 p1, p1, 0x1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PDPageTree;->root:Lorg/apache/pdfbox/cos/COSDictionary;

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lorg/apache/pdfbox/pdmodel/PDPageTree;->get(ILorg/apache/pdfbox/cos/COSDictionary;I)Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object p1

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/PDPageTree;->remove(Lorg/apache/pdfbox/cos/COSDictionary;)V

    return-void
.end method

.method public remove(Lorg/apache/pdfbox/pdmodel/PDPage;)V
    .locals 0

    .line 3
    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/PDPage;->getCOSObject()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object p1

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/pdmodel/PDPageTree;->remove(Lorg/apache/pdfbox/cos/COSDictionary;)V

    return-void
.end method

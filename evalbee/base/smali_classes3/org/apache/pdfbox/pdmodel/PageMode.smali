.class public final enum Lorg/apache/pdfbox/pdmodel/PageMode;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lorg/apache/pdfbox/pdmodel/PageMode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lorg/apache/pdfbox/pdmodel/PageMode;

.field public static final enum FULL_SCREEN:Lorg/apache/pdfbox/pdmodel/PageMode;

.field public static final enum USE_ATTACHMENTS:Lorg/apache/pdfbox/pdmodel/PageMode;

.field public static final enum USE_NONE:Lorg/apache/pdfbox/pdmodel/PageMode;

.field public static final enum USE_OPTIONAL_CONTENT:Lorg/apache/pdfbox/pdmodel/PageMode;

.field public static final enum USE_OUTLINES:Lorg/apache/pdfbox/pdmodel/PageMode;

.field public static final enum USE_THUMBS:Lorg/apache/pdfbox/pdmodel/PageMode;


# instance fields
.field private final value:Ljava/lang/String;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    new-instance v0, Lorg/apache/pdfbox/pdmodel/PageMode;

    const/4 v1, 0x0

    const-string v2, "UseNone"

    const-string v3, "USE_NONE"

    invoke-direct {v0, v3, v1, v2}, Lorg/apache/pdfbox/pdmodel/PageMode;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lorg/apache/pdfbox/pdmodel/PageMode;->USE_NONE:Lorg/apache/pdfbox/pdmodel/PageMode;

    new-instance v1, Lorg/apache/pdfbox/pdmodel/PageMode;

    const/4 v2, 0x1

    const-string v3, "UseOutlines"

    const-string v4, "USE_OUTLINES"

    invoke-direct {v1, v4, v2, v3}, Lorg/apache/pdfbox/pdmodel/PageMode;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v1, Lorg/apache/pdfbox/pdmodel/PageMode;->USE_OUTLINES:Lorg/apache/pdfbox/pdmodel/PageMode;

    new-instance v2, Lorg/apache/pdfbox/pdmodel/PageMode;

    const/4 v3, 0x2

    const-string v4, "UseThumbs"

    const-string v5, "USE_THUMBS"

    invoke-direct {v2, v5, v3, v4}, Lorg/apache/pdfbox/pdmodel/PageMode;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v2, Lorg/apache/pdfbox/pdmodel/PageMode;->USE_THUMBS:Lorg/apache/pdfbox/pdmodel/PageMode;

    new-instance v3, Lorg/apache/pdfbox/pdmodel/PageMode;

    const/4 v4, 0x3

    const-string v5, "FullScreen"

    const-string v6, "FULL_SCREEN"

    invoke-direct {v3, v6, v4, v5}, Lorg/apache/pdfbox/pdmodel/PageMode;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v3, Lorg/apache/pdfbox/pdmodel/PageMode;->FULL_SCREEN:Lorg/apache/pdfbox/pdmodel/PageMode;

    new-instance v4, Lorg/apache/pdfbox/pdmodel/PageMode;

    const/4 v5, 0x4

    const-string v6, "UseOC"

    const-string v7, "USE_OPTIONAL_CONTENT"

    invoke-direct {v4, v7, v5, v6}, Lorg/apache/pdfbox/pdmodel/PageMode;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v4, Lorg/apache/pdfbox/pdmodel/PageMode;->USE_OPTIONAL_CONTENT:Lorg/apache/pdfbox/pdmodel/PageMode;

    new-instance v5, Lorg/apache/pdfbox/pdmodel/PageMode;

    const/4 v6, 0x5

    const-string v7, "UseAttachments"

    const-string v8, "USE_ATTACHMENTS"

    invoke-direct {v5, v8, v6, v7}, Lorg/apache/pdfbox/pdmodel/PageMode;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v5, Lorg/apache/pdfbox/pdmodel/PageMode;->USE_ATTACHMENTS:Lorg/apache/pdfbox/pdmodel/PageMode;

    filled-new-array/range {v0 .. v5}, [Lorg/apache/pdfbox/pdmodel/PageMode;

    move-result-object v0

    sput-object v0, Lorg/apache/pdfbox/pdmodel/PageMode;->$VALUES:[Lorg/apache/pdfbox/pdmodel/PageMode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput-object p3, p0, Lorg/apache/pdfbox/pdmodel/PageMode;->value:Ljava/lang/String;

    return-void
.end method

.method public static fromString(Ljava/lang/String;)Lorg/apache/pdfbox/pdmodel/PageMode;
    .locals 1

    const-string v0, "UseNone"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object p0, Lorg/apache/pdfbox/pdmodel/PageMode;->USE_NONE:Lorg/apache/pdfbox/pdmodel/PageMode;

    return-object p0

    :cond_0
    const-string v0, "UseOutlines"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object p0, Lorg/apache/pdfbox/pdmodel/PageMode;->USE_OUTLINES:Lorg/apache/pdfbox/pdmodel/PageMode;

    return-object p0

    :cond_1
    const-string v0, "UseThumbs"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object p0, Lorg/apache/pdfbox/pdmodel/PageMode;->USE_THUMBS:Lorg/apache/pdfbox/pdmodel/PageMode;

    return-object p0

    :cond_2
    const-string v0, "FullScreen"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object p0, Lorg/apache/pdfbox/pdmodel/PageMode;->FULL_SCREEN:Lorg/apache/pdfbox/pdmodel/PageMode;

    return-object p0

    :cond_3
    const-string v0, "UseOC"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    sget-object p0, Lorg/apache/pdfbox/pdmodel/PageMode;->USE_OPTIONAL_CONTENT:Lorg/apache/pdfbox/pdmodel/PageMode;

    return-object p0

    :cond_4
    const-string v0, "UseAttachments"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    sget-object p0, Lorg/apache/pdfbox/pdmodel/PageMode;->USE_ATTACHMENTS:Lorg/apache/pdfbox/pdmodel/PageMode;

    return-object p0

    :cond_5
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0, p0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static valueOf(Ljava/lang/String;)Lorg/apache/pdfbox/pdmodel/PageMode;
    .locals 1

    const-class v0, Lorg/apache/pdfbox/pdmodel/PageMode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lorg/apache/pdfbox/pdmodel/PageMode;

    return-object p0
.end method

.method public static values()[Lorg/apache/pdfbox/pdmodel/PageMode;
    .locals 1

    sget-object v0, Lorg/apache/pdfbox/pdmodel/PageMode;->$VALUES:[Lorg/apache/pdfbox/pdmodel/PageMode;

    invoke-virtual {v0}, [Lorg/apache/pdfbox/pdmodel/PageMode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lorg/apache/pdfbox/pdmodel/PageMode;

    return-object v0
.end method


# virtual methods
.method public stringValue()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/pdmodel/PageMode;->value:Ljava/lang/String;

    return-object v0
.end method

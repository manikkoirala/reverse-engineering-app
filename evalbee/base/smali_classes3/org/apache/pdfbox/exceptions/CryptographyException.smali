.class public Lorg/apache/pdfbox/exceptions/CryptographyException;
.super Ljava/lang/Exception;
.source "SourceFile"


# instance fields
.field private embedded:Ljava/lang/Exception;


# direct methods
.method public constructor <init>(Ljava/lang/Exception;)V
    .locals 1

    .line 1
    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/exceptions/CryptographyException;->setEmbedded(Ljava/lang/Exception;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .line 2
    invoke-direct {p0, p1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method private setEmbedded(Ljava/lang/Exception;)V
    .locals 0

    iput-object p1, p0, Lorg/apache/pdfbox/exceptions/CryptographyException;->embedded:Ljava/lang/Exception;

    return-void
.end method


# virtual methods
.method public getEmbedded()Ljava/lang/Exception;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/exceptions/CryptographyException;->embedded:Ljava/lang/Exception;

    return-object v0
.end method

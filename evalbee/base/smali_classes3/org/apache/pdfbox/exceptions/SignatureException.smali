.class public Lorg/apache/pdfbox/exceptions/SignatureException;
.super Ljava/lang/Exception;
.source "SourceFile"


# static fields
.field public static final CERT_PATH_CHECK_INVALID:I = 0x3

.field public static final INVALID_PAGE_FOR_SIGNATURE:I = 0x5

.field public static final NO_SUCH_ALGORITHM:I = 0x4

.field public static final UNSUPPORTED_OPERATION:I = 0x2

.field public static final VISUAL_SIGNATURE_INVALID:I = 0x6

.field public static final WRONG_PASSWORD:I = 0x1


# instance fields
.field private no:I


# direct methods
.method public constructor <init>(ILjava/lang/String;)V
    .locals 0

    .line 1
    invoke-direct {p0, p2}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    iput p1, p0, Lorg/apache/pdfbox/exceptions/SignatureException;->no:I

    return-void
.end method

.method public constructor <init>(ILjava/lang/Throwable;)V
    .locals 0

    .line 2
    invoke-direct {p0, p2}, Ljava/lang/Exception;-><init>(Ljava/lang/Throwable;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .line 3
    invoke-direct {p0, p1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/Throwable;)V
    .locals 0

    .line 4
    invoke-direct {p0, p1}, Ljava/lang/Exception;-><init>(Ljava/lang/Throwable;)V

    return-void
.end method


# virtual methods
.method public getErrNo()I
    .locals 1

    iget v0, p0, Lorg/apache/pdfbox/exceptions/SignatureException;->no:I

    return v0
.end method

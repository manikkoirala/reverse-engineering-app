.class public Lorg/apache/pdfbox/contentstream/PDFStreamEngine;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private currentPage:Lorg/apache/pdfbox/pdmodel/PDPage;

.field private graphicsStack:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack<",
            "Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;",
            ">;"
        }
    .end annotation
.end field

.field private initialMatrix:Lorg/apache/pdfbox/util/Matrix;

.field private isProcessingPage:Z

.field private final operators:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;",
            ">;"
        }
    .end annotation
.end field

.field private resources:Lorg/apache/pdfbox/pdmodel/PDResources;

.field private textLineMatrix:Lorg/apache/pdfbox/util/Matrix;

.field private textMatrix:Lorg/apache/pdfbox/util/Matrix;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->operators:Ljava/util/Map;

    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->graphicsStack:Ljava/util/Stack;

    return-void
.end method

.method private clipToRect(Lorg/apache/pdfbox/pdmodel/common/PDRectangle;)V
    .locals 1

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->getGraphicsState()Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->getCurrentTransformationMatrix()Lorg/apache/pdfbox/util/Matrix;

    move-result-object v0

    invoke-virtual {p1, v0}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->transform(Lorg/apache/pdfbox/util/Matrix;)Landroid/graphics/Path;

    move-result-object p1

    invoke-virtual {p0}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->getGraphicsState()Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->intersectClippingPath(Landroid/graphics/Path;)V

    :cond_0
    return-void
.end method

.method private initPage(Lorg/apache/pdfbox/pdmodel/PDPage;)V
    .locals 3

    if-eqz p1, :cond_0

    iput-object p1, p0, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->currentPage:Lorg/apache/pdfbox/pdmodel/PDPage;

    iget-object v0, p0, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->graphicsStack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/AbstractCollection;->clear()V

    iget-object v0, p0, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->graphicsStack:Ljava/util/Stack;

    new-instance v1, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/PDPage;->getCropBox()Lorg/apache/pdfbox/pdmodel/common/PDRectangle;

    move-result-object v2

    invoke-direct {v1, v2}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;-><init>(Lorg/apache/pdfbox/pdmodel/common/PDRectangle;)V

    invoke-virtual {v0, v1}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->textMatrix:Lorg/apache/pdfbox/util/Matrix;

    iput-object v0, p0, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->textLineMatrix:Lorg/apache/pdfbox/util/Matrix;

    iput-object v0, p0, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->resources:Lorg/apache/pdfbox/pdmodel/PDResources;

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/PDPage;->getMatrix()Lorg/apache/pdfbox/util/Matrix;

    move-result-object p1

    iput-object p1, p0, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->initialMatrix:Lorg/apache/pdfbox/util/Matrix;

    return-void

    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Page cannot be null"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private popResources(Lorg/apache/pdfbox/pdmodel/PDResources;)V
    .locals 0

    iput-object p1, p0, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->resources:Lorg/apache/pdfbox/pdmodel/PDResources;

    return-void
.end method

.method private processStream(Lorg/apache/pdfbox/contentstream/PDContentStream;)V
    .locals 5

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->pushResources(Lorg/apache/pdfbox/contentstream/PDContentStream;)Lorg/apache/pdfbox/pdmodel/PDResources;

    move-result-object v0

    invoke-virtual {p0}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->saveGraphicsStack()Ljava/util/Stack;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->initialMatrix:Lorg/apache/pdfbox/util/Matrix;

    invoke-virtual {p0}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->getGraphicsState()Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;

    move-result-object v3

    invoke-virtual {v3}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->getCurrentTransformationMatrix()Lorg/apache/pdfbox/util/Matrix;

    move-result-object v3

    invoke-interface {p1}, Lorg/apache/pdfbox/contentstream/PDContentStream;->getMatrix()Lorg/apache/pdfbox/util/Matrix;

    move-result-object v4

    invoke-virtual {v3, v4}, Lorg/apache/pdfbox/util/Matrix;->concatenate(Lorg/apache/pdfbox/util/Matrix;)V

    invoke-virtual {p0}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->getGraphicsState()Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;

    move-result-object v3

    invoke-virtual {v3}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->getCurrentTransformationMatrix()Lorg/apache/pdfbox/util/Matrix;

    move-result-object v3

    invoke-virtual {v3}, Lorg/apache/pdfbox/util/Matrix;->clone()Lorg/apache/pdfbox/util/Matrix;

    move-result-object v3

    iput-object v3, p0, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->initialMatrix:Lorg/apache/pdfbox/util/Matrix;

    invoke-interface {p1}, Lorg/apache/pdfbox/contentstream/PDContentStream;->getBBox()Lorg/apache/pdfbox/pdmodel/common/PDRectangle;

    move-result-object v3

    invoke-direct {p0, v3}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->clipToRect(Lorg/apache/pdfbox/pdmodel/common/PDRectangle;)V

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->processStreamOperators(Lorg/apache/pdfbox/contentstream/PDContentStream;)V

    iput-object v2, p0, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->initialMatrix:Lorg/apache/pdfbox/util/Matrix;

    invoke-virtual {p0, v1}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->restoreGraphicsStack(Ljava/util/Stack;)V

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->popResources(Lorg/apache/pdfbox/pdmodel/PDResources;)V

    return-void
.end method

.method private processStreamOperators(Lorg/apache/pdfbox/contentstream/PDContentStream;)V
    .locals 4

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    new-instance v1, Lorg/apache/pdfbox/pdfparser/PDFStreamParser;

    invoke-interface {p1}, Lorg/apache/pdfbox/contentstream/PDContentStream;->getContentStream()Lorg/apache/pdfbox/cos/COSStream;

    move-result-object p1

    invoke-direct {v1, p1}, Lorg/apache/pdfbox/pdfparser/PDFStreamParser;-><init>(Lorg/apache/pdfbox/cos/COSStream;)V

    :try_start_0
    invoke-virtual {v1}, Lorg/apache/pdfbox/pdfparser/PDFStreamParser;->getTokenIterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    instance-of v3, v2, Lorg/apache/pdfbox/cos/COSObject;

    if-eqz v3, :cond_0

    check-cast v2, Lorg/apache/pdfbox/cos/COSObject;

    invoke-virtual {v2}, Lorg/apache/pdfbox/cos/COSObject;->getObject()Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v2

    :goto_1
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    instance-of v3, v2, Lorg/apache/pdfbox/contentstream/operator/Operator;

    if-eqz v3, :cond_1

    check-cast v2, Lorg/apache/pdfbox/contentstream/operator/Operator;

    invoke-virtual {p0, v2, v0}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->processOperator(Lorg/apache/pdfbox/contentstream/operator/Operator;Ljava/util/List;)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    goto :goto_0

    :cond_1
    check-cast v2, Lorg/apache/pdfbox/cos/COSBase;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :cond_2
    invoke-virtual {v1}, Lorg/apache/pdfbox/pdfparser/BaseParser;->close()V

    return-void

    :catchall_0
    move-exception p1

    invoke-virtual {v1}, Lorg/apache/pdfbox/pdfparser/BaseParser;->close()V

    throw p1
.end method

.method private pushResources(Lorg/apache/pdfbox/contentstream/PDContentStream;)Lorg/apache/pdfbox/pdmodel/PDResources;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->resources:Lorg/apache/pdfbox/pdmodel/PDResources;

    invoke-interface {p1}, Lorg/apache/pdfbox/contentstream/PDContentStream;->getResources()Lorg/apache/pdfbox/pdmodel/PDResources;

    move-result-object p1

    if-eqz p1, :cond_0

    :goto_0
    iput-object p1, p0, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->resources:Lorg/apache/pdfbox/pdmodel/PDResources;

    goto :goto_1

    :cond_0
    iget-object p1, p0, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->resources:Lorg/apache/pdfbox/pdmodel/PDResources;

    if-eqz p1, :cond_1

    goto :goto_1

    :cond_1
    iget-object p1, p0, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->currentPage:Lorg/apache/pdfbox/pdmodel/PDPage;

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/PDPage;->getResources()Lorg/apache/pdfbox/pdmodel/PDResources;

    move-result-object p1

    goto :goto_0

    :goto_1
    iget-object p1, p0, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->resources:Lorg/apache/pdfbox/pdmodel/PDResources;

    if-nez p1, :cond_2

    new-instance p1, Lorg/apache/pdfbox/pdmodel/PDResources;

    invoke-direct {p1}, Lorg/apache/pdfbox/pdmodel/PDResources;-><init>()V

    iput-object p1, p0, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->resources:Lorg/apache/pdfbox/pdmodel/PDResources;

    :cond_2
    return-object v0
.end method


# virtual methods
.method public final addOperator(Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;)V
    .locals 2

    invoke-virtual {p1, p0}, Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;->setContext(Lorg/apache/pdfbox/contentstream/PDFStreamEngine;)V

    iget-object v0, p0, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->operators:Ljava/util/Map;

    invoke-virtual {p1}, Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public applyTextAdjustment(FF)V
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->textMatrix:Lorg/apache/pdfbox/util/Matrix;

    invoke-static {p1, p2}, Lorg/apache/pdfbox/util/Matrix;->getTranslateInstance(FF)Lorg/apache/pdfbox/util/Matrix;

    move-result-object p1

    invoke-virtual {v0, p1}, Lorg/apache/pdfbox/util/Matrix;->concatenate(Lorg/apache/pdfbox/util/Matrix;)V

    return-void
.end method

.method public beginText()V
    .locals 0

    return-void
.end method

.method public endText()V
    .locals 0

    return-void
.end method

.method public getAppearance(Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotation;)Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAppearanceStream;
    .locals 0

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotation;->getNormalAppearanceStream()Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAppearanceStream;

    move-result-object p1

    return-object p1
.end method

.method public getCurrentPage()Lorg/apache/pdfbox/pdmodel/PDPage;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->currentPage:Lorg/apache/pdfbox/pdmodel/PDPage;

    return-object v0
.end method

.method public getGraphicsStackSize()I
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->graphicsStack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/AbstractCollection;->size()I

    move-result v0

    return v0
.end method

.method public getGraphicsState()Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->graphicsStack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;

    return-object v0
.end method

.method public getInitialMatrix()Lorg/apache/pdfbox/util/Matrix;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->initialMatrix:Lorg/apache/pdfbox/util/Matrix;

    return-object v0
.end method

.method public getResources()Lorg/apache/pdfbox/pdmodel/PDResources;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->resources:Lorg/apache/pdfbox/pdmodel/PDResources;

    return-object v0
.end method

.method public getTextLineMatrix()Lorg/apache/pdfbox/util/Matrix;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->textLineMatrix:Lorg/apache/pdfbox/util/Matrix;

    return-object v0
.end method

.method public getTextMatrix()Lorg/apache/pdfbox/util/Matrix;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->textMatrix:Lorg/apache/pdfbox/util/Matrix;

    return-object v0
.end method

.method public operatorException(Lorg/apache/pdfbox/contentstream/operator/Operator;Ljava/util/List;Ljava/io/IOException;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/pdfbox/contentstream/operator/Operator;",
            "Ljava/util/List<",
            "Lorg/apache/pdfbox/cos/COSBase;",
            ">;",
            "Ljava/io/IOException;",
            ")V"
        }
    .end annotation

    instance-of p2, p3, Lorg/apache/pdfbox/contentstream/operator/MissingOperandException;

    const-string v0, "PdfBoxAndroid"

    if-nez p2, :cond_3

    instance-of p2, p3, Lorg/apache/pdfbox/pdmodel/MissingResourceException;

    if-nez p2, :cond_3

    instance-of p2, p3, Lorg/apache/pdfbox/filter/MissingImageReaderException;

    if-eqz p2, :cond_0

    goto :goto_1

    :cond_0
    instance-of p2, p3, Lorg/apache/pdfbox/contentstream/operator/state/EmptyGraphicsStackException;

    if-eqz p2, :cond_1

    :goto_0
    invoke-virtual {p3}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    :cond_1
    invoke-virtual {p1}, Lorg/apache/pdfbox/contentstream/operator/Operator;->getName()Ljava/lang/String;

    move-result-object p1

    const-string p2, "Do"

    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_2

    goto :goto_0

    :cond_2
    throw p3

    :cond_3
    :goto_1
    invoke-virtual {p3}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-static {v0, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_2
    return-void
.end method

.method public processAnnotation(Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotation;Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAppearanceStream;)V
    .locals 7

    invoke-direct {p0, p2}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->pushResources(Lorg/apache/pdfbox/contentstream/PDContentStream;)Lorg/apache/pdfbox/pdmodel/PDResources;

    move-result-object v0

    invoke-virtual {p0}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->saveGraphicsState()V

    invoke-virtual {p2}, Lorg/apache/pdfbox/pdmodel/graphics/form/PDFormXObject;->getBBox()Lorg/apache/pdfbox/pdmodel/common/PDRectangle;

    move-result-object v1

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotation;->getRectangle()Lorg/apache/pdfbox/pdmodel/common/PDRectangle;

    move-result-object p1

    invoke-virtual {p2}, Lorg/apache/pdfbox/pdmodel/graphics/form/PDFormXObject;->getMatrix()Lorg/apache/pdfbox/util/Matrix;

    move-result-object v2

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->getWidth()F

    move-result v3

    const/4 v4, 0x0

    cmpl-float v3, v3, v4

    if-lez v3, :cond_0

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->getHeight()F

    move-result v3

    cmpl-float v3, v3, v4

    if-lez v3, :cond_0

    new-instance v3, Landroid/graphics/RectF;

    invoke-direct {v3}, Landroid/graphics/RectF;-><init>()V

    invoke-virtual {v1, v2}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->transform(Lorg/apache/pdfbox/util/Matrix;)Landroid/graphics/Path;

    move-result-object v4

    const/4 v5, 0x1

    invoke-virtual {v4, v3, v5}, Landroid/graphics/Path;->computeBounds(Landroid/graphics/RectF;Z)V

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->getLowerLeftX()F

    move-result v4

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->getLowerLeftY()F

    move-result v5

    invoke-static {v4, v5}, Lorg/apache/pdfbox/util/Matrix;->getTranslateInstance(FF)Lorg/apache/pdfbox/util/Matrix;

    move-result-object v4

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->getWidth()F

    move-result v5

    invoke-virtual {v3}, Landroid/graphics/RectF;->width()F

    move-result v6

    div-float/2addr v5, v6

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->getHeight()F

    move-result p1

    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v6

    div-float/2addr p1, v6

    invoke-static {v5, p1}, Lorg/apache/pdfbox/util/Matrix;->getScaleInstance(FF)Lorg/apache/pdfbox/util/Matrix;

    move-result-object p1

    invoke-virtual {v4, p1}, Lorg/apache/pdfbox/util/Matrix;->concatenate(Lorg/apache/pdfbox/util/Matrix;)V

    iget p1, v3, Landroid/graphics/RectF;->left:F

    neg-float p1, p1

    iget v3, v3, Landroid/graphics/RectF;->top:F

    neg-float v3, v3

    invoke-static {p1, v3}, Lorg/apache/pdfbox/util/Matrix;->getTranslateInstance(FF)Lorg/apache/pdfbox/util/Matrix;

    move-result-object p1

    invoke-virtual {v4, p1}, Lorg/apache/pdfbox/util/Matrix;->concatenate(Lorg/apache/pdfbox/util/Matrix;)V

    invoke-static {v2, v4}, Lorg/apache/pdfbox/util/Matrix;->concatenate(Lorg/apache/pdfbox/util/Matrix;Lorg/apache/pdfbox/util/Matrix;)Lorg/apache/pdfbox/util/Matrix;

    move-result-object p1

    invoke-virtual {p0}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->getGraphicsState()Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;

    move-result-object v2

    invoke-virtual {v2, p1}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->setCurrentTransformationMatrix(Lorg/apache/pdfbox/util/Matrix;)V

    invoke-direct {p0, v1}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->clipToRect(Lorg/apache/pdfbox/pdmodel/common/PDRectangle;)V

    invoke-direct {p0, p2}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->processStreamOperators(Lorg/apache/pdfbox/contentstream/PDContentStream;)V

    :cond_0
    invoke-virtual {p0}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->restoreGraphicsState()V

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->popResources(Lorg/apache/pdfbox/pdmodel/PDResources;)V

    return-void
.end method

.method public processChildStream(Lorg/apache/pdfbox/contentstream/PDContentStream;Lorg/apache/pdfbox/pdmodel/PDPage;)V
    .locals 1

    iget-boolean v0, p0, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->isProcessingPage:Z

    if-nez v0, :cond_0

    invoke-direct {p0, p2}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->initPage(Lorg/apache/pdfbox/pdmodel/PDPage;)V

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->processStream(Lorg/apache/pdfbox/contentstream/PDContentStream;)V

    const/4 p1, 0x0

    iput-object p1, p0, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->currentPage:Lorg/apache/pdfbox/pdmodel/PDPage;

    return-void

    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "Current page has already been set via  #processPage(PDPage) call #processChildStream(PDContentStream) instead"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public processOperator(Ljava/lang/String;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Lorg/apache/pdfbox/cos/COSBase;",
            ">;)V"
        }
    .end annotation

    .line 1
    invoke-static {p1}, Lorg/apache/pdfbox/contentstream/operator/Operator;->getOperator(Ljava/lang/String;)Lorg/apache/pdfbox/contentstream/operator/Operator;

    move-result-object p1

    invoke-virtual {p0, p1, p2}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->processOperator(Lorg/apache/pdfbox/contentstream/operator/Operator;Ljava/util/List;)V

    return-void
.end method

.method public processOperator(Lorg/apache/pdfbox/contentstream/operator/Operator;Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/pdfbox/contentstream/operator/Operator;",
            "Ljava/util/List<",
            "Lorg/apache/pdfbox/cos/COSBase;",
            ">;)V"
        }
    .end annotation

    .line 2
    invoke-virtual {p1}, Lorg/apache/pdfbox/contentstream/operator/Operator;->getName()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->operators:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p0}, Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;->setContext(Lorg/apache/pdfbox/contentstream/PDFStreamEngine;)V

    :try_start_0
    invoke-virtual {v0, p1, p2}, Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;->process(Lorg/apache/pdfbox/contentstream/operator/Operator;Ljava/util/List;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {p0, p1, p2, v0}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->operatorException(Lorg/apache/pdfbox/contentstream/operator/Operator;Ljava/util/List;Ljava/io/IOException;)V

    goto :goto_0

    :cond_0
    invoke-virtual {p0, p1, p2}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->unsupportedOperator(Lorg/apache/pdfbox/contentstream/operator/Operator;Ljava/util/List;)V

    :goto_0
    return-void
.end method

.method public processPage(Lorg/apache/pdfbox/pdmodel/PDPage;)V
    .locals 1

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->initPage(Lorg/apache/pdfbox/pdmodel/PDPage;)V

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/PDPage;->getStream()Lorg/apache/pdfbox/pdmodel/common/PDStream;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->isProcessingPage:Z

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->processStream(Lorg/apache/pdfbox/contentstream/PDContentStream;)V

    const/4 p1, 0x0

    iput-boolean p1, p0, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->isProcessingPage:Z

    :cond_0
    return-void
.end method

.method public processSoftMask(Lorg/apache/pdfbox/pdmodel/graphics/form/PDFormXObject;)V
    .locals 3

    invoke-virtual {p0}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->saveGraphicsStack()Ljava/util/Stack;

    move-result-object v0

    invoke-virtual {p0}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->getGraphicsState()Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->setSoftMask(Lorg/apache/pdfbox/pdmodel/graphics/state/PDSoftMask;)V

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->processTransparencyGroup(Lorg/apache/pdfbox/pdmodel/graphics/form/PDFormXObject;)V

    invoke-virtual {p0, v0}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->restoreGraphicsStack(Ljava/util/Stack;)V

    return-void
.end method

.method public final processTilingPattern(Lorg/apache/pdfbox/pdmodel/graphics/pattern/PDTilingPattern;Lorg/apache/pdfbox/pdmodel/graphics/color/PDColor;Lorg/apache/pdfbox/pdmodel/graphics/color/PDColorSpace;)V
    .locals 1

    .line 1
    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/graphics/pattern/PDAbstractPattern;->getMatrix()Lorg/apache/pdfbox/util/Matrix;

    move-result-object v0

    invoke-virtual {p0, p1, p2, p3, v0}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->processTilingPattern(Lorg/apache/pdfbox/pdmodel/graphics/pattern/PDTilingPattern;Lorg/apache/pdfbox/pdmodel/graphics/color/PDColor;Lorg/apache/pdfbox/pdmodel/graphics/color/PDColorSpace;Lorg/apache/pdfbox/util/Matrix;)V

    return-void
.end method

.method public final processTilingPattern(Lorg/apache/pdfbox/pdmodel/graphics/pattern/PDTilingPattern;Lorg/apache/pdfbox/pdmodel/graphics/color/PDColor;Lorg/apache/pdfbox/pdmodel/graphics/color/PDColorSpace;Lorg/apache/pdfbox/util/Matrix;)V
    .locals 8

    .line 2
    invoke-direct {p0, p1}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->pushResources(Lorg/apache/pdfbox/contentstream/PDContentStream;)Lorg/apache/pdfbox/pdmodel/PDResources;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->initialMatrix:Lorg/apache/pdfbox/util/Matrix;

    invoke-static {v1, p4}, Lorg/apache/pdfbox/util/Matrix;->concatenate(Lorg/apache/pdfbox/util/Matrix;Lorg/apache/pdfbox/util/Matrix;)Lorg/apache/pdfbox/util/Matrix;

    move-result-object v2

    iput-object v2, p0, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->initialMatrix:Lorg/apache/pdfbox/util/Matrix;

    invoke-virtual {p0}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->saveGraphicsStack()Ljava/util/Stack;

    move-result-object v2

    new-instance v3, Landroid/graphics/RectF;

    invoke-direct {v3}, Landroid/graphics/RectF;-><init>()V

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/graphics/pattern/PDTilingPattern;->getBBox()Lorg/apache/pdfbox/pdmodel/common/PDRectangle;

    move-result-object v4

    invoke-virtual {v4, p4}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->transform(Lorg/apache/pdfbox/util/Matrix;)Landroid/graphics/Path;

    move-result-object v4

    const/4 v5, 0x1

    invoke-virtual {v4, v3, v5}, Landroid/graphics/Path;->computeBounds(Landroid/graphics/RectF;Z)V

    new-instance v4, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;

    iget v5, v3, Landroid/graphics/RectF;->left:F

    iget v6, v3, Landroid/graphics/RectF;->top:F

    invoke-virtual {v3}, Landroid/graphics/RectF;->width()F

    move-result v7

    invoke-virtual {v3}, Landroid/graphics/RectF;->height()F

    move-result v3

    invoke-direct {v4, v5, v6, v7, v3}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;-><init>(FFFF)V

    iget-object v3, p0, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->graphicsStack:Ljava/util/Stack;

    new-instance v5, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;

    invoke-direct {v5, v4}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;-><init>(Lorg/apache/pdfbox/pdmodel/common/PDRectangle;)V

    invoke-virtual {v3, v5}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    if-eqz p3, :cond_0

    new-instance v3, Lorg/apache/pdfbox/pdmodel/graphics/color/PDColor;

    invoke-virtual {p2}, Lorg/apache/pdfbox/pdmodel/graphics/color/PDColor;->getComponents()[F

    move-result-object p2

    invoke-direct {v3, p2, p3}, Lorg/apache/pdfbox/pdmodel/graphics/color/PDColor;-><init>([FLorg/apache/pdfbox/pdmodel/graphics/color/PDColorSpace;)V

    invoke-virtual {p0}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->getGraphicsState()Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;

    move-result-object p2

    invoke-virtual {p2, p3}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->setNonStrokingColorSpace(Lorg/apache/pdfbox/pdmodel/graphics/color/PDColorSpace;)V

    invoke-virtual {p0}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->getGraphicsState()Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;

    move-result-object p2

    invoke-virtual {p2, v3}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->setNonStrokingColor(Lorg/apache/pdfbox/pdmodel/graphics/color/PDColor;)V

    invoke-virtual {p0}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->getGraphicsState()Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;

    move-result-object p2

    invoke-virtual {p2, p3}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->setStrokingColorSpace(Lorg/apache/pdfbox/pdmodel/graphics/color/PDColorSpace;)V

    invoke-virtual {p0}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->getGraphicsState()Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;

    move-result-object p2

    invoke-virtual {p2, v3}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->setStrokingColor(Lorg/apache/pdfbox/pdmodel/graphics/color/PDColor;)V

    :cond_0
    invoke-virtual {p0}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->getGraphicsState()Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;

    move-result-object p2

    invoke-virtual {p2}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->getCurrentTransformationMatrix()Lorg/apache/pdfbox/util/Matrix;

    move-result-object p2

    invoke-virtual {p2, p4}, Lorg/apache/pdfbox/util/Matrix;->concatenate(Lorg/apache/pdfbox/util/Matrix;)V

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/graphics/pattern/PDTilingPattern;->getBBox()Lorg/apache/pdfbox/pdmodel/common/PDRectangle;

    move-result-object p2

    invoke-direct {p0, p2}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->clipToRect(Lorg/apache/pdfbox/pdmodel/common/PDRectangle;)V

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->processStreamOperators(Lorg/apache/pdfbox/contentstream/PDContentStream;)V

    iput-object v1, p0, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->initialMatrix:Lorg/apache/pdfbox/util/Matrix;

    invoke-virtual {p0, v2}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->restoreGraphicsStack(Ljava/util/Stack;)V

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->popResources(Lorg/apache/pdfbox/pdmodel/PDResources;)V

    return-void
.end method

.method public processTransparencyGroup(Lorg/apache/pdfbox/pdmodel/graphics/form/PDFormXObject;)V
    .locals 4

    iget-object v0, p0, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->currentPage:Lorg/apache/pdfbox/pdmodel/PDPage;

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->pushResources(Lorg/apache/pdfbox/contentstream/PDContentStream;)Lorg/apache/pdfbox/pdmodel/PDResources;

    move-result-object v0

    invoke-virtual {p0}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->saveGraphicsStack()Ljava/util/Stack;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->getGraphicsState()Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->getCurrentTransformationMatrix()Lorg/apache/pdfbox/util/Matrix;

    move-result-object v2

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/graphics/form/PDFormXObject;->getMatrix()Lorg/apache/pdfbox/util/Matrix;

    move-result-object v3

    invoke-virtual {v2, v3}, Lorg/apache/pdfbox/util/Matrix;->concatenate(Lorg/apache/pdfbox/util/Matrix;)V

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->processStreamOperators(Lorg/apache/pdfbox/contentstream/PDContentStream;)V

    invoke-virtual {p0, v1}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->restoreGraphicsStack(Ljava/util/Stack;)V

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->popResources(Lorg/apache/pdfbox/pdmodel/PDResources;)V

    return-void

    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "No current page, call #processChildStream(PDContentStream, PDPage) instead"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public processType3Stream(Lorg/apache/pdfbox/pdmodel/font/PDType3CharProc;Lorg/apache/pdfbox/util/Matrix;)V
    .locals 3

    iget-object v0, p0, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->currentPage:Lorg/apache/pdfbox/pdmodel/PDPage;

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->pushResources(Lorg/apache/pdfbox/contentstream/PDContentStream;)Lorg/apache/pdfbox/pdmodel/PDResources;

    move-result-object v0

    invoke-virtual {p0}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->saveGraphicsState()V

    invoke-virtual {p0}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->getGraphicsState()Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;

    move-result-object v1

    invoke-virtual {v1, p2}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->setCurrentTransformationMatrix(Lorg/apache/pdfbox/util/Matrix;)V

    invoke-virtual {p0}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->getGraphicsState()Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;

    move-result-object p2

    invoke-virtual {p2}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->getCurrentTransformationMatrix()Lorg/apache/pdfbox/util/Matrix;

    move-result-object p2

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/font/PDType3CharProc;->getMatrix()Lorg/apache/pdfbox/util/Matrix;

    move-result-object v1

    invoke-virtual {p2, v1}, Lorg/apache/pdfbox/util/Matrix;->concatenate(Lorg/apache/pdfbox/util/Matrix;)V

    iget-object p2, p0, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->textMatrix:Lorg/apache/pdfbox/util/Matrix;

    new-instance v1, Lorg/apache/pdfbox/util/Matrix;

    invoke-direct {v1}, Lorg/apache/pdfbox/util/Matrix;-><init>()V

    iput-object v1, p0, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->textMatrix:Lorg/apache/pdfbox/util/Matrix;

    iget-object v1, p0, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->textLineMatrix:Lorg/apache/pdfbox/util/Matrix;

    new-instance v2, Lorg/apache/pdfbox/util/Matrix;

    invoke-direct {v2}, Lorg/apache/pdfbox/util/Matrix;-><init>()V

    iput-object v2, p0, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->textLineMatrix:Lorg/apache/pdfbox/util/Matrix;

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->processStreamOperators(Lorg/apache/pdfbox/contentstream/PDContentStream;)V

    iput-object p2, p0, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->textMatrix:Lorg/apache/pdfbox/util/Matrix;

    iput-object v1, p0, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->textLineMatrix:Lorg/apache/pdfbox/util/Matrix;

    invoke-virtual {p0}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->restoreGraphicsState()V

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->popResources(Lorg/apache/pdfbox/pdmodel/PDResources;)V

    return-void

    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "No current page, call #processChildStream(PDContentStream, PDPage) instead"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public registerOperatorProcessor(Ljava/lang/String;Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;)V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    invoke-virtual {p2, p0}, Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;->setContext(Lorg/apache/pdfbox/contentstream/PDFStreamEngine;)V

    iget-object v0, p0, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->operators:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public final restoreGraphicsStack(Ljava/util/Stack;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Stack<",
            "Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->graphicsStack:Ljava/util/Stack;

    return-void
.end method

.method public restoreGraphicsState()V
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->graphicsStack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    return-void
.end method

.method public final saveGraphicsStack()Ljava/util/Stack;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Stack<",
            "Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->graphicsStack:Ljava/util/Stack;

    new-instance v1, Ljava/util/Stack;

    invoke-direct {v1}, Ljava/util/Stack;-><init>()V

    iput-object v1, p0, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->graphicsStack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;

    invoke-virtual {v2}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->clone()Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/AbstractCollection;->add(Ljava/lang/Object;)Z

    return-object v0
.end method

.method public saveGraphicsState()V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->graphicsStack:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;

    invoke-virtual {v1}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->clone()Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public setLineDashPattern(Lorg/apache/pdfbox/cos/COSArray;I)V
    .locals 2

    if-gez p2, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Dash phase has negative value "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p2, ", set to 0"

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    const-string v0, "PdfBoxAndroid"

    invoke-static {v0, p2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 p2, 0x0

    :cond_0
    new-instance v0, Lorg/apache/pdfbox/pdmodel/graphics/PDLineDashPattern;

    invoke-direct {v0, p1, p2}, Lorg/apache/pdfbox/pdmodel/graphics/PDLineDashPattern;-><init>(Lorg/apache/pdfbox/cos/COSArray;I)V

    invoke-virtual {p0}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->getGraphicsState()Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;

    move-result-object p1

    invoke-virtual {p1, v0}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->setLineDashPattern(Lorg/apache/pdfbox/pdmodel/graphics/PDLineDashPattern;)V

    return-void
.end method

.method public setTextLineMatrix(Lorg/apache/pdfbox/util/Matrix;)V
    .locals 0

    iput-object p1, p0, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->textLineMatrix:Lorg/apache/pdfbox/util/Matrix;

    return-void
.end method

.method public setTextMatrix(Lorg/apache/pdfbox/util/Matrix;)V
    .locals 0

    iput-object p1, p0, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->textMatrix:Lorg/apache/pdfbox/util/Matrix;

    return-void
.end method

.method public showAnnotation(Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotation;)V
    .locals 1

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->getAppearance(Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotation;)Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAppearanceStream;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1, v0}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->processAnnotation(Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotation;Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAppearanceStream;)V

    :cond_0
    return-void
.end method

.method public showFontGlyph(Lorg/apache/pdfbox/util/Matrix;Lorg/apache/pdfbox/pdmodel/font/PDFont;ILjava/lang/String;Lorg/apache/pdfbox/util/Vector;)V
    .locals 0

    return-void
.end method

.method public showForm(Lorg/apache/pdfbox/pdmodel/graphics/form/PDFormXObject;)V
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->currentPage:Lorg/apache/pdfbox/pdmodel/PDPage;

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->processStream(Lorg/apache/pdfbox/contentstream/PDContentStream;)V

    return-void

    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "No current page, call #processChildStream(PDContentStream, PDPage) instead"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public showGlyph(Lorg/apache/pdfbox/util/Matrix;Lorg/apache/pdfbox/pdmodel/font/PDFont;ILjava/lang/String;Lorg/apache/pdfbox/util/Vector;)V
    .locals 7

    instance-of v0, p2, Lorg/apache/pdfbox/pdmodel/font/PDType3Font;

    if-eqz v0, :cond_0

    move-object v3, p2

    check-cast v3, Lorg/apache/pdfbox/pdmodel/font/PDType3Font;

    move-object v1, p0

    move-object v2, p1

    move v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-virtual/range {v1 .. v6}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->showType3Glyph(Lorg/apache/pdfbox/util/Matrix;Lorg/apache/pdfbox/pdmodel/font/PDType3Font;ILjava/lang/String;Lorg/apache/pdfbox/util/Vector;)V

    goto :goto_0

    :cond_0
    invoke-virtual/range {p0 .. p5}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->showFontGlyph(Lorg/apache/pdfbox/util/Matrix;Lorg/apache/pdfbox/pdmodel/font/PDFont;ILjava/lang/String;Lorg/apache/pdfbox/util/Vector;)V

    :goto_0
    return-void
.end method

.method public showText([B)V
    .locals 21

    move-object/from16 v6, p0

    invoke-virtual/range {p0 .. p0}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->getGraphicsState()Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;

    move-result-object v7

    invoke-virtual {v7}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->getTextState()Lorg/apache/pdfbox/pdmodel/graphics/state/PDTextState;

    move-result-object v8

    invoke-virtual {v8}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDTextState;->getFont()Lorg/apache/pdfbox/pdmodel/font/PDFont;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, "PdfBoxAndroid"

    const-string v1, "No current font, will use default"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lorg/apache/pdfbox/pdmodel/font/PDFontFactory;->createDefaultFont()Lorg/apache/pdfbox/pdmodel/font/PDFont;

    move-result-object v0

    :cond_0
    move-object v9, v0

    invoke-virtual {v8}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDTextState;->getFontSize()F

    move-result v17

    invoke-virtual {v8}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDTextState;->getHorizontalScaling()F

    move-result v0

    const/high16 v1, 0x42c80000    # 100.0f

    div-float v18, v0, v1

    invoke-virtual {v8}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDTextState;->getCharacterSpacing()F

    move-result v19

    new-instance v5, Lorg/apache/pdfbox/util/Matrix;

    mul-float v11, v17, v18

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v15, 0x0

    invoke-virtual {v8}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDTextState;->getRise()F

    move-result v16

    move-object v10, v5

    move/from16 v14, v17

    invoke-direct/range {v10 .. v16}, Lorg/apache/pdfbox/util/Matrix;-><init>(FFFFFF)V

    new-instance v10, Ljava/io/ByteArrayInputStream;

    move-object/from16 v0, p1

    invoke-direct {v10, v0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    :goto_0
    invoke-virtual {v10}, Ljava/io/InputStream;->available()I

    move-result v0

    if-lez v0, :cond_4

    invoke-virtual {v10}, Ljava/io/InputStream;->available()I

    move-result v0

    invoke-virtual {v9, v10}, Lorg/apache/pdfbox/pdmodel/font/PDFont;->readCode(Ljava/io/InputStream;)I

    move-result v3

    invoke-virtual {v10}, Ljava/io/InputStream;->available()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {v9, v3}, Lorg/apache/pdfbox/pdmodel/font/PDFont;->toUnicode(I)Ljava/lang/String;

    move-result-object v4

    const/4 v1, 0x1

    const/4 v11, 0x0

    if-ne v0, v1, :cond_1

    const/16 v0, 0x20

    if-ne v3, v0, :cond_1

    invoke-virtual {v8}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDTextState;->getWordSpacing()F

    move-result v0

    add-float/2addr v0, v11

    move v12, v0

    goto :goto_1

    :cond_1
    move v12, v11

    :goto_1
    invoke-virtual {v7}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->getCurrentTransformationMatrix()Lorg/apache/pdfbox/util/Matrix;

    move-result-object v0

    iget-object v1, v6, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->textMatrix:Lorg/apache/pdfbox/util/Matrix;

    invoke-virtual {v5, v1}, Lorg/apache/pdfbox/util/Matrix;->multiply(Lorg/apache/pdfbox/util/Matrix;)Lorg/apache/pdfbox/util/Matrix;

    move-result-object v1

    invoke-virtual {v1, v0}, Lorg/apache/pdfbox/util/Matrix;->multiply(Lorg/apache/pdfbox/util/Matrix;)Lorg/apache/pdfbox/util/Matrix;

    move-result-object v1

    invoke-virtual {v9}, Lorg/apache/pdfbox/pdmodel/font/PDFont;->isVertical()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {v9, v3}, Lorg/apache/pdfbox/pdmodel/font/PDFont;->getPositionVector(I)Lorg/apache/pdfbox/util/Vector;

    move-result-object v0

    invoke-virtual {v1, v0}, Lorg/apache/pdfbox/util/Matrix;->translate(Lorg/apache/pdfbox/util/Vector;)V

    :cond_2
    invoke-virtual {v9, v3}, Lorg/apache/pdfbox/pdmodel/font/PDFont;->getDisplacement(I)Lorg/apache/pdfbox/util/Vector;

    move-result-object v13

    invoke-virtual/range {p0 .. p0}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->saveGraphicsState()V

    move-object/from16 v0, p0

    move-object v2, v9

    move-object v14, v5

    move-object v5, v13

    invoke-virtual/range {v0 .. v5}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->showGlyph(Lorg/apache/pdfbox/util/Matrix;Lorg/apache/pdfbox/pdmodel/font/PDFont;ILjava/lang/String;Lorg/apache/pdfbox/util/Vector;)V

    invoke-virtual/range {p0 .. p0}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->restoreGraphicsState()V

    invoke-virtual {v9}, Lorg/apache/pdfbox/pdmodel/font/PDFont;->isVertical()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {v13}, Lorg/apache/pdfbox/util/Vector;->getY()F

    move-result v0

    mul-float v0, v0, v17

    add-float v0, v0, v19

    add-float/2addr v0, v12

    goto :goto_2

    :cond_3
    invoke-virtual {v13}, Lorg/apache/pdfbox/util/Vector;->getX()F

    move-result v0

    mul-float v0, v0, v17

    add-float v0, v0, v19

    add-float/2addr v0, v12

    mul-float v0, v0, v18

    move/from16 v20, v11

    move v11, v0

    move/from16 v0, v20

    :goto_2
    iget-object v1, v6, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->textMatrix:Lorg/apache/pdfbox/util/Matrix;

    invoke-static {v11, v0}, Lorg/apache/pdfbox/util/Matrix;->getTranslateInstance(FF)Lorg/apache/pdfbox/util/Matrix;

    move-result-object v0

    invoke-virtual {v1, v0}, Lorg/apache/pdfbox/util/Matrix;->concatenate(Lorg/apache/pdfbox/util/Matrix;)V

    move-object v5, v14

    goto :goto_0

    :cond_4
    return-void
.end method

.method public showTextString([B)V
    .locals 0

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->showText([B)V

    return-void
.end method

.method public showTextStrings(Lorg/apache/pdfbox/cos/COSArray;)V
    .locals 7

    invoke-virtual {p0}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->getGraphicsState()Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->getTextState()Lorg/apache/pdfbox/pdmodel/graphics/state/PDTextState;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDTextState;->getFontSize()F

    move-result v1

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDTextState;->getHorizontalScaling()F

    move-result v2

    const/high16 v3, 0x42c80000    # 100.0f

    div-float/2addr v2, v3

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDTextState;->getFont()Lorg/apache/pdfbox/pdmodel/font/PDFont;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/font/PDFont;->isVertical()Z

    move-result v0

    invoke-virtual {p1}, Lorg/apache/pdfbox/cos/COSArray;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/pdfbox/cos/COSBase;

    instance-of v4, v3, Lorg/apache/pdfbox/cos/COSNumber;

    if-eqz v4, :cond_1

    check-cast v3, Lorg/apache/pdfbox/cos/COSNumber;

    invoke-virtual {v3}, Lorg/apache/pdfbox/cos/COSNumber;->floatValue()F

    move-result v3

    const/4 v4, 0x0

    const/high16 v5, 0x447a0000    # 1000.0f

    neg-float v3, v3

    div-float/2addr v3, v5

    mul-float/2addr v3, v1

    if-eqz v0, :cond_0

    goto :goto_1

    :cond_0
    mul-float/2addr v3, v2

    move v6, v4

    move v4, v3

    move v3, v6

    :goto_1
    invoke-virtual {p0, v4, v3}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->applyTextAdjustment(FF)V

    goto :goto_0

    :cond_1
    instance-of v4, v3, Lorg/apache/pdfbox/cos/COSString;

    if-eqz v4, :cond_2

    check-cast v3, Lorg/apache/pdfbox/cos/COSString;

    invoke-virtual {v3}, Lorg/apache/pdfbox/cos/COSString;->getBytes()[B

    move-result-object v3

    invoke-virtual {p0, v3}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->showText([B)V

    goto :goto_0

    :cond_2
    new-instance p1, Ljava/io/IOException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unknown type in array for TJ operation:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_3
    return-void
.end method

.method public showTransparencyGroup(Lorg/apache/pdfbox/pdmodel/graphics/form/PDFormXObject;)V
    .locals 0

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->processTransparencyGroup(Lorg/apache/pdfbox/pdmodel/graphics/form/PDFormXObject;)V

    return-void
.end method

.method public showType3Glyph(Lorg/apache/pdfbox/util/Matrix;Lorg/apache/pdfbox/pdmodel/font/PDType3Font;ILjava/lang/String;Lorg/apache/pdfbox/util/Vector;)V
    .locals 0

    invoke-virtual {p2, p3}, Lorg/apache/pdfbox/pdmodel/font/PDType3Font;->getCharProc(I)Lorg/apache/pdfbox/pdmodel/font/PDType3CharProc;

    move-result-object p2

    if-eqz p2, :cond_0

    invoke-virtual {p0, p2, p1}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->processType3Stream(Lorg/apache/pdfbox/pdmodel/font/PDType3CharProc;Lorg/apache/pdfbox/util/Matrix;)V

    :cond_0
    return-void
.end method

.method public transformWidth(F)F
    .locals 4

    invoke-virtual {p0}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->getGraphicsState()Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->getCurrentTransformationMatrix()Lorg/apache/pdfbox/util/Matrix;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/pdfbox/util/Matrix;->getScaleX()F

    move-result v1

    invoke-virtual {v0}, Lorg/apache/pdfbox/util/Matrix;->getShearX()F

    move-result v2

    add-float/2addr v1, v2

    invoke-virtual {v0}, Lorg/apache/pdfbox/util/Matrix;->getScaleY()F

    move-result v2

    invoke-virtual {v0}, Lorg/apache/pdfbox/util/Matrix;->getShearY()F

    move-result v0

    add-float/2addr v2, v0

    mul-float/2addr v1, v1

    mul-float/2addr v2, v2

    add-float/2addr v1, v2

    float-to-double v0, v1

    const-wide/high16 v2, 0x3fe0000000000000L    # 0.5

    mul-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    double-to-float v0, v0

    mul-float/2addr p1, v0

    return p1
.end method

.method public transformedPoint(FF)Landroid/graphics/PointF;
    .locals 8

    const/4 v0, 0x2

    new-array v0, v0, [F

    const/4 v7, 0x0

    aput p1, v0, v7

    const/4 p1, 0x1

    aput p2, v0, p1

    invoke-virtual {p0}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->getGraphicsState()Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;

    move-result-object p2

    invoke-virtual {p2}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->getCurrentTransformationMatrix()Lorg/apache/pdfbox/util/Matrix;

    move-result-object p2

    invoke-virtual {p2}, Lorg/apache/pdfbox/util/Matrix;->createAffineTransform()Lorg/apache/pdfbox/util/awt/AffineTransform;

    move-result-object v1

    const/4 v3, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x1

    move-object v2, v0

    move-object v4, v0

    invoke-virtual/range {v1 .. v6}, Lorg/apache/pdfbox/util/awt/AffineTransform;->transform([FI[FII)V

    new-instance p2, Landroid/graphics/PointF;

    aget v1, v0, v7

    aget p1, v0, p1

    invoke-direct {p2, v1, p1}, Landroid/graphics/PointF;-><init>(FF)V

    return-object p2
.end method

.method public unsupportedOperator(Lorg/apache/pdfbox/contentstream/operator/Operator;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/pdfbox/contentstream/operator/Operator;",
            "Ljava/util/List<",
            "Lorg/apache/pdfbox/cos/COSBase;",
            ">;)V"
        }
    .end annotation

    return-void
.end method

.class public Lorg/apache/pdfbox/contentstream/operator/state/Restore;
.super Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;-><init>()V

    return-void
.end method


# virtual methods
.method public getName()Ljava/lang/String;
    .locals 1

    const-string v0, "Q"

    return-object v0
.end method

.method public process(Lorg/apache/pdfbox/contentstream/operator/Operator;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/pdfbox/contentstream/operator/Operator;",
            "Ljava/util/List<",
            "Lorg/apache/pdfbox/cos/COSBase;",
            ">;)V"
        }
    .end annotation

    iget-object p1, p0, Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;->context:Lorg/apache/pdfbox/contentstream/PDFStreamEngine;

    invoke-virtual {p1}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->getGraphicsStackSize()I

    move-result p1

    const/4 p2, 0x1

    if-le p1, p2, :cond_0

    iget-object p1, p0, Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;->context:Lorg/apache/pdfbox/contentstream/PDFStreamEngine;

    invoke-virtual {p1}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->restoreGraphicsState()V

    return-void

    :cond_0
    new-instance p1, Lorg/apache/pdfbox/contentstream/operator/state/EmptyGraphicsStackException;

    invoke-direct {p1}, Lorg/apache/pdfbox/contentstream/operator/state/EmptyGraphicsStackException;-><init>()V

    throw p1
.end method

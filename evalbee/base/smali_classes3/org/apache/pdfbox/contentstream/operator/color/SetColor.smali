.class public abstract Lorg/apache/pdfbox/contentstream/operator/color/SetColor;
.super Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract getColor()Lorg/apache/pdfbox/pdmodel/graphics/color/PDColor;
.end method

.method public abstract getColorSpace()Lorg/apache/pdfbox/pdmodel/graphics/color/PDColorSpace;
.end method

.method public process(Lorg/apache/pdfbox/contentstream/operator/Operator;Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/pdfbox/contentstream/operator/Operator;",
            "Ljava/util/List<",
            "Lorg/apache/pdfbox/cos/COSBase;",
            ">;)V"
        }
    .end annotation

    invoke-virtual {p0}, Lorg/apache/pdfbox/contentstream/operator/color/SetColor;->getColorSpace()Lorg/apache/pdfbox/pdmodel/graphics/color/PDColorSpace;

    move-result-object v0

    instance-of v1, v0, Lorg/apache/pdfbox/pdmodel/graphics/color/PDDeviceColorSpace;

    if-eqz v1, :cond_1

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v1

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/graphics/color/PDColorSpace;->getNumberOfComponents()I

    move-result v2

    if-lt v1, v2, :cond_0

    goto :goto_0

    :cond_0
    new-instance v0, Lorg/apache/pdfbox/contentstream/operator/MissingOperandException;

    invoke-direct {v0, p1, p2}, Lorg/apache/pdfbox/contentstream/operator/MissingOperandException;-><init>(Lorg/apache/pdfbox/contentstream/operator/Operator;Ljava/util/List;)V

    throw v0

    :cond_1
    :goto_0
    new-instance p1, Lorg/apache/pdfbox/cos/COSArray;

    invoke-direct {p1}, Lorg/apache/pdfbox/cos/COSArray;-><init>()V

    invoke-virtual {p1, p2}, Lorg/apache/pdfbox/cos/COSArray;->addAll(Ljava/util/Collection;)V

    new-instance p2, Lorg/apache/pdfbox/pdmodel/graphics/color/PDColor;

    invoke-direct {p2, p1, v0}, Lorg/apache/pdfbox/pdmodel/graphics/color/PDColor;-><init>(Lorg/apache/pdfbox/cos/COSArray;Lorg/apache/pdfbox/pdmodel/graphics/color/PDColorSpace;)V

    invoke-virtual {p0, p2}, Lorg/apache/pdfbox/contentstream/operator/color/SetColor;->setColor(Lorg/apache/pdfbox/pdmodel/graphics/color/PDColor;)V

    return-void
.end method

.method public abstract setColor(Lorg/apache/pdfbox/pdmodel/graphics/color/PDColor;)V
.end method

.class public Lorg/apache/pdfbox/contentstream/operator/DrawObject;
.super Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;-><init>()V

    return-void
.end method


# virtual methods
.method public getName()Ljava/lang/String;
    .locals 1

    const-string v0, "Do"

    return-object v0
.end method

.method public process(Lorg/apache/pdfbox/contentstream/operator/Operator;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/pdfbox/contentstream/operator/Operator;",
            "Ljava/util/List<",
            "Lorg/apache/pdfbox/cos/COSBase;",
            ">;)V"
        }
    .end annotation

    const/4 p1, 0x0

    invoke-interface {p2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lorg/apache/pdfbox/cos/COSName;

    iget-object p2, p0, Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;->context:Lorg/apache/pdfbox/contentstream/PDFStreamEngine;

    invoke-virtual {p2}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->getResources()Lorg/apache/pdfbox/pdmodel/PDResources;

    move-result-object p2

    invoke-virtual {p2, p1}, Lorg/apache/pdfbox/pdmodel/PDResources;->getXObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/pdmodel/graphics/PDXObject;

    move-result-object p1

    iget-object p2, p0, Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;->context:Lorg/apache/pdfbox/contentstream/PDFStreamEngine;

    instance-of v0, p2, Lorg/apache/pdfbox/text/PDFMarkedContentExtractor;

    if-eqz v0, :cond_0

    check-cast p2, Lorg/apache/pdfbox/text/PDFMarkedContentExtractor;

    invoke-virtual {p2, p1}, Lorg/apache/pdfbox/text/PDFMarkedContentExtractor;->xobject(Lorg/apache/pdfbox/pdmodel/graphics/PDXObject;)V

    :cond_0
    instance-of p2, p1, Lorg/apache/pdfbox/pdmodel/graphics/form/PDFormXObject;

    if-eqz p2, :cond_1

    check-cast p1, Lorg/apache/pdfbox/pdmodel/graphics/form/PDFormXObject;

    iget-object p2, p0, Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;->context:Lorg/apache/pdfbox/contentstream/PDFStreamEngine;

    invoke-virtual {p2, p1}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->showForm(Lorg/apache/pdfbox/pdmodel/graphics/form/PDFormXObject;)V

    :cond_1
    return-void
.end method

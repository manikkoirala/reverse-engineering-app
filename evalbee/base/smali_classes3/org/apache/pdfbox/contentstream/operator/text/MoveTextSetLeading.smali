.class public Lorg/apache/pdfbox/contentstream/operator/text/MoveTextSetLeading;
.super Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;-><init>()V

    return-void
.end method


# virtual methods
.method public getName()Ljava/lang/String;
    .locals 1

    const-string v0, "TD"

    return-object v0
.end method

.method public process(Lorg/apache/pdfbox/contentstream/operator/Operator;Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/pdfbox/contentstream/operator/Operator;",
            "Ljava/util/List<",
            "Lorg/apache/pdfbox/cos/COSBase;",
            ">;)V"
        }
    .end annotation

    const/4 p1, 0x1

    invoke-interface {p2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lorg/apache/pdfbox/cos/COSNumber;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    new-instance v1, Lorg/apache/pdfbox/cos/COSFloat;

    const/high16 v2, -0x40800000    # -1.0f

    invoke-virtual {p1}, Lorg/apache/pdfbox/cos/COSNumber;->floatValue()F

    move-result p1

    mul-float/2addr p1, v2

    invoke-direct {v1, p1}, Lorg/apache/pdfbox/cos/COSFloat;-><init>(F)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object p1, p0, Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;->context:Lorg/apache/pdfbox/contentstream/PDFStreamEngine;

    const-string v1, "TL"

    invoke-virtual {p1, v1, v0}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->processOperator(Ljava/lang/String;Ljava/util/List;)V

    iget-object p1, p0, Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;->context:Lorg/apache/pdfbox/contentstream/PDFStreamEngine;

    const-string v0, "Td"

    invoke-virtual {p1, v0, p2}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->processOperator(Ljava/lang/String;Ljava/util/List;)V

    return-void
.end method

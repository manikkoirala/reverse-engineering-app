.class public Lorg/apache/pdfbox/contentstream/operator/text/ShowTextLine;
.super Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;-><init>()V

    return-void
.end method


# virtual methods
.method public getName()Ljava/lang/String;
    .locals 1

    const-string v0, "\'"

    return-object v0
.end method

.method public process(Lorg/apache/pdfbox/contentstream/operator/Operator;Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/pdfbox/contentstream/operator/Operator;",
            "Ljava/util/List<",
            "Lorg/apache/pdfbox/cos/COSBase;",
            ">;)V"
        }
    .end annotation

    iget-object p1, p0, Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;->context:Lorg/apache/pdfbox/contentstream/PDFStreamEngine;

    const-string v0, "T*"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->processOperator(Ljava/lang/String;Ljava/util/List;)V

    iget-object p1, p0, Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;->context:Lorg/apache/pdfbox/contentstream/PDFStreamEngine;

    const-string v0, "Tj"

    invoke-virtual {p1, v0, p2}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->processOperator(Ljava/lang/String;Ljava/util/List;)V

    return-void
.end method

.class public Lorg/apache/pdfbox/contentstream/operator/text/SetFontAndSize;
.super Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;-><init>()V

    return-void
.end method


# virtual methods
.method public getName()Ljava/lang/String;
    .locals 1

    const-string v0, "Tf"

    return-object v0
.end method

.method public process(Lorg/apache/pdfbox/contentstream/operator/Operator;Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/pdfbox/contentstream/operator/Operator;",
            "Ljava/util/List<",
            "Lorg/apache/pdfbox/cos/COSBase;",
            ">;)V"
        }
    .end annotation

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x2

    if-lt v0, v1, :cond_0

    const/4 p1, 0x0

    invoke-interface {p2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lorg/apache/pdfbox/cos/COSName;

    const/4 v0, 0x1

    invoke-interface {p2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lorg/apache/pdfbox/cos/COSNumber;

    invoke-virtual {p2}, Lorg/apache/pdfbox/cos/COSNumber;->floatValue()F

    move-result p2

    iget-object v0, p0, Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;->context:Lorg/apache/pdfbox/contentstream/PDFStreamEngine;

    invoke-virtual {v0}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->getGraphicsState()Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->getTextState()Lorg/apache/pdfbox/pdmodel/graphics/state/PDTextState;

    move-result-object v0

    invoke-virtual {v0, p2}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDTextState;->setFontSize(F)V

    iget-object p2, p0, Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;->context:Lorg/apache/pdfbox/contentstream/PDFStreamEngine;

    invoke-virtual {p2}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->getResources()Lorg/apache/pdfbox/pdmodel/PDResources;

    move-result-object p2

    invoke-virtual {p2, p1}, Lorg/apache/pdfbox/pdmodel/PDResources;->getFont(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/pdmodel/font/PDFont;

    move-result-object p1

    iget-object p2, p0, Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;->context:Lorg/apache/pdfbox/contentstream/PDFStreamEngine;

    invoke-virtual {p2}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->getGraphicsState()Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;

    move-result-object p2

    invoke-virtual {p2}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->getTextState()Lorg/apache/pdfbox/pdmodel/graphics/state/PDTextState;

    move-result-object p2

    invoke-virtual {p2, p1}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDTextState;->setFont(Lorg/apache/pdfbox/pdmodel/font/PDFont;)V

    return-void

    :cond_0
    new-instance v0, Lorg/apache/pdfbox/contentstream/operator/MissingOperandException;

    invoke-direct {v0, p1, p2}, Lorg/apache/pdfbox/contentstream/operator/MissingOperandException;-><init>(Lorg/apache/pdfbox/contentstream/operator/Operator;Ljava/util/List;)V

    throw v0
.end method

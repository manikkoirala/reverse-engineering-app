.class public abstract Lorg/apache/pdfbox/contentstream/operator/graphics/GraphicsOperatorProcessor;
.super Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;
.source "SourceFile"


# instance fields
.field protected context:Lorg/apache/pdfbox/contentstream/PDFGraphicsStreamEngine;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;-><init>()V

    return-void
.end method


# virtual methods
.method public setContext(Lorg/apache/pdfbox/contentstream/PDFStreamEngine;)V
    .locals 0

    invoke-super {p0, p1}, Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;->setContext(Lorg/apache/pdfbox/contentstream/PDFStreamEngine;)V

    check-cast p1, Lorg/apache/pdfbox/contentstream/PDFGraphicsStreamEngine;

    iput-object p1, p0, Lorg/apache/pdfbox/contentstream/operator/graphics/GraphicsOperatorProcessor;->context:Lorg/apache/pdfbox/contentstream/PDFGraphicsStreamEngine;

    return-void
.end method

.class public Lorg/apache/pdfbox/contentstream/operator/color/SetNonStrokingDeviceRGBColor;
.super Lorg/apache/pdfbox/contentstream/operator/color/SetNonStrokingColor;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lorg/apache/pdfbox/contentstream/operator/color/SetNonStrokingColor;-><init>()V

    return-void
.end method


# virtual methods
.method public getName()Ljava/lang/String;
    .locals 1

    const-string v0, "rg"

    return-object v0
.end method

.method public process(Lorg/apache/pdfbox/contentstream/operator/Operator;Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/pdfbox/contentstream/operator/Operator;",
            "Ljava/util/List<",
            "Lorg/apache/pdfbox/cos/COSBase;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;->context:Lorg/apache/pdfbox/contentstream/PDFStreamEngine;

    invoke-virtual {v0}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->getResources()Lorg/apache/pdfbox/pdmodel/PDResources;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->DEVICERGB:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/pdmodel/PDResources;->getColorSpace(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/pdmodel/graphics/color/PDColorSpace;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;->context:Lorg/apache/pdfbox/contentstream/PDFStreamEngine;

    invoke-virtual {v1}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->getGraphicsState()Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;

    move-result-object v1

    invoke-virtual {v1, v0}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->setNonStrokingColorSpace(Lorg/apache/pdfbox/pdmodel/graphics/color/PDColorSpace;)V

    invoke-super {p0, p1, p2}, Lorg/apache/pdfbox/contentstream/operator/color/SetColor;->process(Lorg/apache/pdfbox/contentstream/operator/Operator;Ljava/util/List;)V

    return-void
.end method

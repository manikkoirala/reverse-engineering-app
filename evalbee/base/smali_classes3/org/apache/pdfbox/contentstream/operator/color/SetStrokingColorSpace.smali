.class public Lorg/apache/pdfbox/contentstream/operator/color/SetStrokingColorSpace;
.super Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;-><init>()V

    return-void
.end method


# virtual methods
.method public getName()Ljava/lang/String;
    .locals 1

    const-string v0, "CS"

    return-object v0
.end method

.method public process(Lorg/apache/pdfbox/contentstream/operator/Operator;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/pdfbox/contentstream/operator/Operator;",
            "Ljava/util/List<",
            "Lorg/apache/pdfbox/cos/COSBase;",
            ">;)V"
        }
    .end annotation

    const/4 p1, 0x0

    invoke-interface {p2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lorg/apache/pdfbox/cos/COSName;

    iget-object p2, p0, Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;->context:Lorg/apache/pdfbox/contentstream/PDFStreamEngine;

    invoke-virtual {p2}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->getResources()Lorg/apache/pdfbox/pdmodel/PDResources;

    move-result-object p2

    invoke-virtual {p2, p1}, Lorg/apache/pdfbox/pdmodel/PDResources;->getColorSpace(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/pdmodel/graphics/color/PDColorSpace;

    move-result-object p1

    iget-object p2, p0, Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;->context:Lorg/apache/pdfbox/contentstream/PDFStreamEngine;

    invoke-virtual {p2}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->getGraphicsState()Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;

    move-result-object p2

    invoke-virtual {p2, p1}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->setStrokingColorSpace(Lorg/apache/pdfbox/pdmodel/graphics/color/PDColorSpace;)V

    iget-object p2, p0, Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;->context:Lorg/apache/pdfbox/contentstream/PDFStreamEngine;

    invoke-virtual {p2}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->getGraphicsState()Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;

    move-result-object p2

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/graphics/color/PDColorSpace;->getInitialColor()Lorg/apache/pdfbox/pdmodel/graphics/color/PDColor;

    move-result-object p1

    invoke-virtual {p2, p1}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->setStrokingColor(Lorg/apache/pdfbox/pdmodel/graphics/color/PDColor;)V

    return-void
.end method

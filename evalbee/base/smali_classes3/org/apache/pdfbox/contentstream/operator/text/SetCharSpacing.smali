.class public Lorg/apache/pdfbox/contentstream/operator/text/SetCharSpacing;
.super Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;-><init>()V

    return-void
.end method


# virtual methods
.method public getName()Ljava/lang/String;
    .locals 1

    const-string v0, "Tc"

    return-object v0
.end method

.method public process(Lorg/apache/pdfbox/contentstream/operator/Operator;Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/pdfbox/contentstream/operator/Operator;",
            "Ljava/util/List<",
            "Lorg/apache/pdfbox/cos/COSBase;",
            ">;)V"
        }
    .end annotation

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result p1

    add-int/lit8 p1, p1, -0x1

    invoke-interface {p2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    instance-of p2, p1, Lorg/apache/pdfbox/cos/COSNumber;

    if-eqz p2, :cond_0

    check-cast p1, Lorg/apache/pdfbox/cos/COSNumber;

    iget-object p2, p0, Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;->context:Lorg/apache/pdfbox/contentstream/PDFStreamEngine;

    invoke-virtual {p2}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->getGraphicsState()Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;

    move-result-object p2

    invoke-virtual {p2}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->getTextState()Lorg/apache/pdfbox/pdmodel/graphics/state/PDTextState;

    move-result-object p2

    invoke-virtual {p1}, Lorg/apache/pdfbox/cos/COSNumber;->floatValue()F

    move-result p1

    invoke-virtual {p2, p1}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDTextState;->setCharacterSpacing(F)V

    :cond_0
    return-void

    :cond_1
    new-instance v0, Lorg/apache/pdfbox/contentstream/operator/MissingOperandException;

    invoke-direct {v0, p1, p2}, Lorg/apache/pdfbox/contentstream/operator/MissingOperandException;-><init>(Lorg/apache/pdfbox/contentstream/operator/Operator;Ljava/util/List;)V

    throw v0
.end method

.class public Lorg/apache/pdfbox/contentstream/operator/markedcontent/BeginMarkedContentSequence;
.super Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;-><init>()V

    return-void
.end method


# virtual methods
.method public getName()Ljava/lang/String;
    .locals 1

    const-string v0, "BMC"

    return-object v0
.end method

.method public process(Lorg/apache/pdfbox/contentstream/operator/Operator;Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/pdfbox/contentstream/operator/Operator;",
            "Ljava/util/List<",
            "Lorg/apache/pdfbox/cos/COSBase;",
            ">;)V"
        }
    .end annotation

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    const/4 p2, 0x0

    move-object v0, p2

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/pdfbox/cos/COSBase;

    instance-of v2, v1, Lorg/apache/pdfbox/cos/COSName;

    if-eqz v2, :cond_0

    move-object v0, v1

    check-cast v0, Lorg/apache/pdfbox/cos/COSName;

    goto :goto_0

    :cond_1
    iget-object p1, p0, Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;->context:Lorg/apache/pdfbox/contentstream/PDFStreamEngine;

    instance-of v1, p1, Lorg/apache/pdfbox/text/PDFMarkedContentExtractor;

    if-eqz v1, :cond_2

    check-cast p1, Lorg/apache/pdfbox/text/PDFMarkedContentExtractor;

    invoke-virtual {p1, v0, p2}, Lorg/apache/pdfbox/text/PDFMarkedContentExtractor;->beginMarkedContentSequence(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSDictionary;)V

    :cond_2
    return-void
.end method

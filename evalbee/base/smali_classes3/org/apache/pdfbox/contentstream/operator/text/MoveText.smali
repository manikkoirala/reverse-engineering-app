.class public Lorg/apache/pdfbox/contentstream/operator/text/MoveText;
.super Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;-><init>()V

    return-void
.end method


# virtual methods
.method public getName()Ljava/lang/String;
    .locals 1

    const-string v0, "Td"

    return-object v0
.end method

.method public process(Lorg/apache/pdfbox/contentstream/operator/Operator;Ljava/util/List;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/pdfbox/contentstream/operator/Operator;",
            "Ljava/util/List<",
            "Lorg/apache/pdfbox/cos/COSBase;",
            ">;)V"
        }
    .end annotation

    const/4 p1, 0x0

    invoke-interface {p2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lorg/apache/pdfbox/cos/COSNumber;

    const/4 v0, 0x1

    invoke-interface {p2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lorg/apache/pdfbox/cos/COSNumber;

    new-instance v7, Lorg/apache/pdfbox/util/Matrix;

    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-virtual {p1}, Lorg/apache/pdfbox/cos/COSNumber;->floatValue()F

    move-result v5

    invoke-virtual {p2}, Lorg/apache/pdfbox/cos/COSNumber;->floatValue()F

    move-result v6

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lorg/apache/pdfbox/util/Matrix;-><init>(FFFFFF)V

    iget-object p1, p0, Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;->context:Lorg/apache/pdfbox/contentstream/PDFStreamEngine;

    invoke-virtual {p1}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->getTextLineMatrix()Lorg/apache/pdfbox/util/Matrix;

    move-result-object p1

    invoke-virtual {p1, v7}, Lorg/apache/pdfbox/util/Matrix;->concatenate(Lorg/apache/pdfbox/util/Matrix;)V

    iget-object p1, p0, Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;->context:Lorg/apache/pdfbox/contentstream/PDFStreamEngine;

    invoke-virtual {p1}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->getTextLineMatrix()Lorg/apache/pdfbox/util/Matrix;

    move-result-object p2

    invoke-virtual {p2}, Lorg/apache/pdfbox/util/Matrix;->clone()Lorg/apache/pdfbox/util/Matrix;

    move-result-object p2

    invoke-virtual {p1, p2}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->setTextMatrix(Lorg/apache/pdfbox/util/Matrix;)V

    return-void
.end method

.class public final Lorg/apache/pdfbox/contentstream/operator/graphics/DrawObject;
.super Lorg/apache/pdfbox/contentstream/operator/graphics/GraphicsOperatorProcessor;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lorg/apache/pdfbox/contentstream/operator/graphics/GraphicsOperatorProcessor;-><init>()V

    return-void
.end method


# virtual methods
.method public getName()Ljava/lang/String;
    .locals 1

    const-string v0, "Do"

    return-object v0
.end method

.method public process(Lorg/apache/pdfbox/contentstream/operator/Operator;Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/pdfbox/contentstream/operator/Operator;",
            "Ljava/util/List<",
            "Lorg/apache/pdfbox/cos/COSBase;",
            ">;)V"
        }
    .end annotation

    const/4 p1, 0x0

    invoke-interface {p2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lorg/apache/pdfbox/cos/COSName;

    iget-object p2, p0, Lorg/apache/pdfbox/contentstream/operator/graphics/GraphicsOperatorProcessor;->context:Lorg/apache/pdfbox/contentstream/PDFGraphicsStreamEngine;

    invoke-virtual {p2}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->getResources()Lorg/apache/pdfbox/pdmodel/PDResources;

    move-result-object p2

    invoke-virtual {p2, p1}, Lorg/apache/pdfbox/pdmodel/PDResources;->getXObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/pdmodel/graphics/PDXObject;

    move-result-object p2

    if-eqz p2, :cond_3

    instance-of p1, p2, Lorg/apache/pdfbox/pdmodel/graphics/image/PDImageXObject;

    if-eqz p1, :cond_0

    check-cast p2, Lorg/apache/pdfbox/pdmodel/graphics/image/PDImageXObject;

    iget-object p1, p0, Lorg/apache/pdfbox/contentstream/operator/graphics/GraphicsOperatorProcessor;->context:Lorg/apache/pdfbox/contentstream/PDFGraphicsStreamEngine;

    invoke-virtual {p1, p2}, Lorg/apache/pdfbox/contentstream/PDFGraphicsStreamEngine;->drawImage(Lorg/apache/pdfbox/pdmodel/graphics/image/PDImage;)V

    goto :goto_0

    :cond_0
    instance-of p1, p2, Lorg/apache/pdfbox/pdmodel/graphics/form/PDFormXObject;

    if-eqz p1, :cond_2

    check-cast p2, Lorg/apache/pdfbox/pdmodel/graphics/form/PDFormXObject;

    invoke-virtual {p2}, Lorg/apache/pdfbox/pdmodel/graphics/form/PDFormXObject;->getGroup()Lorg/apache/pdfbox/pdmodel/graphics/form/PDGroup;

    move-result-object p1

    if-eqz p1, :cond_1

    sget-object p1, Lorg/apache/pdfbox/cos/COSName;->TRANSPARENCY:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p2}, Lorg/apache/pdfbox/pdmodel/graphics/form/PDFormXObject;->getGroup()Lorg/apache/pdfbox/pdmodel/graphics/form/PDGroup;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/graphics/form/PDGroup;->getSubType()Lorg/apache/pdfbox/cos/COSName;

    move-result-object v0

    invoke-virtual {p1, v0}, Lorg/apache/pdfbox/cos/COSName;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_1

    invoke-virtual {p0}, Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;->getContext()Lorg/apache/pdfbox/contentstream/PDFStreamEngine;

    move-result-object p1

    invoke-virtual {p1, p2}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->showTransparencyGroup(Lorg/apache/pdfbox/pdmodel/graphics/form/PDFormXObject;)V

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;->getContext()Lorg/apache/pdfbox/contentstream/PDFStreamEngine;

    move-result-object p1

    invoke-virtual {p1, p2}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->showForm(Lorg/apache/pdfbox/pdmodel/graphics/form/PDFormXObject;)V

    :cond_2
    :goto_0
    return-void

    :cond_3
    new-instance p2, Lorg/apache/pdfbox/pdmodel/MissingResourceException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Missing XObject: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lorg/apache/pdfbox/cos/COSName;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Lorg/apache/pdfbox/pdmodel/MissingResourceException;-><init>(Ljava/lang/String;)V

    throw p2
.end method

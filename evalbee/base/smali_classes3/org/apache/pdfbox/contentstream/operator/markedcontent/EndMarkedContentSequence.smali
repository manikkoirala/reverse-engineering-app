.class public Lorg/apache/pdfbox/contentstream/operator/markedcontent/EndMarkedContentSequence;
.super Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;-><init>()V

    return-void
.end method


# virtual methods
.method public getName()Ljava/lang/String;
    .locals 1

    const-string v0, "EMC"

    return-object v0
.end method

.method public process(Lorg/apache/pdfbox/contentstream/operator/Operator;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/pdfbox/contentstream/operator/Operator;",
            "Ljava/util/List<",
            "Lorg/apache/pdfbox/cos/COSBase;",
            ">;)V"
        }
    .end annotation

    iget-object p1, p0, Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;->context:Lorg/apache/pdfbox/contentstream/PDFStreamEngine;

    instance-of p2, p1, Lorg/apache/pdfbox/text/PDFMarkedContentExtractor;

    if-eqz p2, :cond_0

    check-cast p1, Lorg/apache/pdfbox/text/PDFMarkedContentExtractor;

    invoke-virtual {p1}, Lorg/apache/pdfbox/text/PDFMarkedContentExtractor;->endMarkedContentSequence()V

    :cond_0
    return-void
.end method

.class public Lorg/apache/pdfbox/contentstream/operator/color/SetNonStrokingColor;
.super Lorg/apache/pdfbox/contentstream/operator/color/SetColor;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lorg/apache/pdfbox/contentstream/operator/color/SetColor;-><init>()V

    return-void
.end method


# virtual methods
.method public getColor()Lorg/apache/pdfbox/pdmodel/graphics/color/PDColor;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;->context:Lorg/apache/pdfbox/contentstream/PDFStreamEngine;

    invoke-virtual {v0}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->getGraphicsState()Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->getNonStrokingColor()Lorg/apache/pdfbox/pdmodel/graphics/color/PDColor;

    move-result-object v0

    return-object v0
.end method

.method public getColorSpace()Lorg/apache/pdfbox/pdmodel/graphics/color/PDColorSpace;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;->context:Lorg/apache/pdfbox/contentstream/PDFStreamEngine;

    invoke-virtual {v0}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->getGraphicsState()Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->getNonStrokingColorSpace()Lorg/apache/pdfbox/pdmodel/graphics/color/PDColorSpace;

    move-result-object v0

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    const-string v0, "sc"

    return-object v0
.end method

.method public setColor(Lorg/apache/pdfbox/pdmodel/graphics/color/PDColor;)V
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;->context:Lorg/apache/pdfbox/contentstream/PDFStreamEngine;

    invoke-virtual {v0}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->getGraphicsState()Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->setNonStrokingColor(Lorg/apache/pdfbox/pdmodel/graphics/color/PDColor;)V

    return-void
.end method

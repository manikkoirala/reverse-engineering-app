.class public final Lorg/apache/pdfbox/contentstream/operator/graphics/CurveToReplicateFinalPoint;
.super Lorg/apache/pdfbox/contentstream/operator/graphics/GraphicsOperatorProcessor;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lorg/apache/pdfbox/contentstream/operator/graphics/GraphicsOperatorProcessor;-><init>()V

    return-void
.end method


# virtual methods
.method public getName()Ljava/lang/String;
    .locals 1

    const-string v0, "y"

    return-object v0
.end method

.method public process(Lorg/apache/pdfbox/contentstream/operator/Operator;Ljava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/pdfbox/contentstream/operator/Operator;",
            "Ljava/util/List<",
            "Lorg/apache/pdfbox/cos/COSBase;",
            ">;)V"
        }
    .end annotation

    const/4 p1, 0x0

    invoke-interface {p2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lorg/apache/pdfbox/cos/COSNumber;

    const/4 v0, 0x1

    invoke-interface {p2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSNumber;

    const/4 v1, 0x2

    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/pdfbox/cos/COSNumber;

    const/4 v2, 0x3

    invoke-interface {p2, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lorg/apache/pdfbox/cos/COSNumber;

    iget-object v2, p0, Lorg/apache/pdfbox/contentstream/operator/graphics/GraphicsOperatorProcessor;->context:Lorg/apache/pdfbox/contentstream/PDFGraphicsStreamEngine;

    invoke-virtual {p1}, Lorg/apache/pdfbox/cos/COSNumber;->floatValue()F

    move-result p1

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSNumber;->floatValue()F

    move-result v0

    invoke-virtual {v2, p1, v0}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->transformedPoint(FF)Landroid/graphics/PointF;

    move-result-object p1

    iget-object v0, p0, Lorg/apache/pdfbox/contentstream/operator/graphics/GraphicsOperatorProcessor;->context:Lorg/apache/pdfbox/contentstream/PDFGraphicsStreamEngine;

    invoke-virtual {v1}, Lorg/apache/pdfbox/cos/COSNumber;->floatValue()F

    move-result v1

    invoke-virtual {p2}, Lorg/apache/pdfbox/cos/COSNumber;->floatValue()F

    move-result p2

    invoke-virtual {v0, v1, p2}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->transformedPoint(FF)Landroid/graphics/PointF;

    move-result-object p2

    iget-object v0, p0, Lorg/apache/pdfbox/contentstream/operator/graphics/GraphicsOperatorProcessor;->context:Lorg/apache/pdfbox/contentstream/PDFGraphicsStreamEngine;

    iget v1, p1, Landroid/graphics/PointF;->x:F

    iget v2, p1, Landroid/graphics/PointF;->y:F

    iget v5, p2, Landroid/graphics/PointF;->x:F

    iget v6, p2, Landroid/graphics/PointF;->y:F

    move v3, v5

    move v4, v6

    invoke-virtual/range {v0 .. v6}, Lorg/apache/pdfbox/contentstream/PDFGraphicsStreamEngine;->curveTo(FFFFFF)V

    return-void
.end method

.class public abstract Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field protected context:Lorg/apache/pdfbox/contentstream/PDFStreamEngine;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final getContext()Lorg/apache/pdfbox/contentstream/PDFStreamEngine;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;->context:Lorg/apache/pdfbox/contentstream/PDFStreamEngine;

    return-object v0
.end method

.method public abstract getName()Ljava/lang/String;
.end method

.method public abstract process(Lorg/apache/pdfbox/contentstream/operator/Operator;Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/pdfbox/contentstream/operator/Operator;",
            "Ljava/util/List<",
            "Lorg/apache/pdfbox/cos/COSBase;",
            ">;)V"
        }
    .end annotation
.end method

.method public setContext(Lorg/apache/pdfbox/contentstream/PDFStreamEngine;)V
    .locals 0

    iput-object p1, p0, Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;->context:Lorg/apache/pdfbox/contentstream/PDFStreamEngine;

    return-void
.end method

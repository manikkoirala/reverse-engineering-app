.class public abstract Lorg/apache/pdfbox/contentstream/PDFGraphicsStreamEngine;
.super Lorg/apache/pdfbox/contentstream/PDFStreamEngine;
.source "SourceFile"


# instance fields
.field private final page:Lorg/apache/pdfbox/pdmodel/PDPage;


# direct methods
.method public constructor <init>(Lorg/apache/pdfbox/pdmodel/PDPage;)V
    .locals 0

    invoke-direct {p0}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;-><init>()V

    iput-object p1, p0, Lorg/apache/pdfbox/contentstream/PDFGraphicsStreamEngine;->page:Lorg/apache/pdfbox/pdmodel/PDPage;

    new-instance p1, Lorg/apache/pdfbox/contentstream/operator/graphics/CloseFillNonZeroAndStrokePath;

    invoke-direct {p1}, Lorg/apache/pdfbox/contentstream/operator/graphics/CloseFillNonZeroAndStrokePath;-><init>()V

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->addOperator(Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;)V

    new-instance p1, Lorg/apache/pdfbox/contentstream/operator/graphics/CloseFillEvenOddAndStrokePath;

    invoke-direct {p1}, Lorg/apache/pdfbox/contentstream/operator/graphics/CloseFillEvenOddAndStrokePath;-><init>()V

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->addOperator(Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;)V

    new-instance p1, Lorg/apache/pdfbox/contentstream/operator/text/BeginText;

    invoke-direct {p1}, Lorg/apache/pdfbox/contentstream/operator/text/BeginText;-><init>()V

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->addOperator(Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;)V

    new-instance p1, Lorg/apache/pdfbox/contentstream/operator/graphics/CurveTo;

    invoke-direct {p1}, Lorg/apache/pdfbox/contentstream/operator/graphics/CurveTo;-><init>()V

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->addOperator(Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;)V

    new-instance p1, Lorg/apache/pdfbox/contentstream/operator/state/Concatenate;

    invoke-direct {p1}, Lorg/apache/pdfbox/contentstream/operator/state/Concatenate;-><init>()V

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->addOperator(Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;)V

    new-instance p1, Lorg/apache/pdfbox/contentstream/operator/color/SetStrokingColorSpace;

    invoke-direct {p1}, Lorg/apache/pdfbox/contentstream/operator/color/SetStrokingColorSpace;-><init>()V

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->addOperator(Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;)V

    new-instance p1, Lorg/apache/pdfbox/contentstream/operator/color/SetNonStrokingColorSpace;

    invoke-direct {p1}, Lorg/apache/pdfbox/contentstream/operator/color/SetNonStrokingColorSpace;-><init>()V

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->addOperator(Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;)V

    new-instance p1, Lorg/apache/pdfbox/contentstream/operator/state/SetLineDashPattern;

    invoke-direct {p1}, Lorg/apache/pdfbox/contentstream/operator/state/SetLineDashPattern;-><init>()V

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->addOperator(Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;)V

    new-instance p1, Lorg/apache/pdfbox/contentstream/operator/graphics/DrawObject;

    invoke-direct {p1}, Lorg/apache/pdfbox/contentstream/operator/graphics/DrawObject;-><init>()V

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->addOperator(Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;)V

    new-instance p1, Lorg/apache/pdfbox/contentstream/operator/text/EndText;

    invoke-direct {p1}, Lorg/apache/pdfbox/contentstream/operator/text/EndText;-><init>()V

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->addOperator(Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;)V

    new-instance p1, Lorg/apache/pdfbox/contentstream/operator/color/SetStrokingDeviceGrayColor;

    invoke-direct {p1}, Lorg/apache/pdfbox/contentstream/operator/color/SetStrokingDeviceGrayColor;-><init>()V

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->addOperator(Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;)V

    new-instance p1, Lorg/apache/pdfbox/contentstream/operator/color/SetNonStrokingDeviceGrayColor;

    invoke-direct {p1}, Lorg/apache/pdfbox/contentstream/operator/color/SetNonStrokingDeviceGrayColor;-><init>()V

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->addOperator(Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;)V

    new-instance p1, Lorg/apache/pdfbox/contentstream/operator/state/SetGraphicsStateParameters;

    invoke-direct {p1}, Lorg/apache/pdfbox/contentstream/operator/state/SetGraphicsStateParameters;-><init>()V

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->addOperator(Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;)V

    new-instance p1, Lorg/apache/pdfbox/contentstream/operator/graphics/ClosePath;

    invoke-direct {p1}, Lorg/apache/pdfbox/contentstream/operator/graphics/ClosePath;-><init>()V

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->addOperator(Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;)V

    new-instance p1, Lorg/apache/pdfbox/contentstream/operator/state/SetFlatness;

    invoke-direct {p1}, Lorg/apache/pdfbox/contentstream/operator/state/SetFlatness;-><init>()V

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->addOperator(Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;)V

    new-instance p1, Lorg/apache/pdfbox/contentstream/operator/state/SetLineJoinStyle;

    invoke-direct {p1}, Lorg/apache/pdfbox/contentstream/operator/state/SetLineJoinStyle;-><init>()V

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->addOperator(Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;)V

    new-instance p1, Lorg/apache/pdfbox/contentstream/operator/state/SetLineCapStyle;

    invoke-direct {p1}, Lorg/apache/pdfbox/contentstream/operator/state/SetLineCapStyle;-><init>()V

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->addOperator(Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;)V

    new-instance p1, Lorg/apache/pdfbox/contentstream/operator/graphics/LineTo;

    invoke-direct {p1}, Lorg/apache/pdfbox/contentstream/operator/graphics/LineTo;-><init>()V

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->addOperator(Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;)V

    new-instance p1, Lorg/apache/pdfbox/contentstream/operator/graphics/MoveTo;

    invoke-direct {p1}, Lorg/apache/pdfbox/contentstream/operator/graphics/MoveTo;-><init>()V

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->addOperator(Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;)V

    new-instance p1, Lorg/apache/pdfbox/contentstream/operator/state/SetLineMiterLimit;

    invoke-direct {p1}, Lorg/apache/pdfbox/contentstream/operator/state/SetLineMiterLimit;-><init>()V

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->addOperator(Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;)V

    new-instance p1, Lorg/apache/pdfbox/contentstream/operator/graphics/EndPath;

    invoke-direct {p1}, Lorg/apache/pdfbox/contentstream/operator/graphics/EndPath;-><init>()V

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->addOperator(Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;)V

    new-instance p1, Lorg/apache/pdfbox/contentstream/operator/state/Save;

    invoke-direct {p1}, Lorg/apache/pdfbox/contentstream/operator/state/Save;-><init>()V

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->addOperator(Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;)V

    new-instance p1, Lorg/apache/pdfbox/contentstream/operator/state/Restore;

    invoke-direct {p1}, Lorg/apache/pdfbox/contentstream/operator/state/Restore;-><init>()V

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->addOperator(Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;)V

    new-instance p1, Lorg/apache/pdfbox/contentstream/operator/graphics/AppendRectangleToPath;

    invoke-direct {p1}, Lorg/apache/pdfbox/contentstream/operator/graphics/AppendRectangleToPath;-><init>()V

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->addOperator(Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;)V

    new-instance p1, Lorg/apache/pdfbox/contentstream/operator/color/SetStrokingDeviceRGBColor;

    invoke-direct {p1}, Lorg/apache/pdfbox/contentstream/operator/color/SetStrokingDeviceRGBColor;-><init>()V

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->addOperator(Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;)V

    new-instance p1, Lorg/apache/pdfbox/contentstream/operator/color/SetNonStrokingDeviceRGBColor;

    invoke-direct {p1}, Lorg/apache/pdfbox/contentstream/operator/color/SetNonStrokingDeviceRGBColor;-><init>()V

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->addOperator(Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;)V

    new-instance p1, Lorg/apache/pdfbox/contentstream/operator/state/SetRenderingIntent;

    invoke-direct {p1}, Lorg/apache/pdfbox/contentstream/operator/state/SetRenderingIntent;-><init>()V

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->addOperator(Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;)V

    new-instance p1, Lorg/apache/pdfbox/contentstream/operator/graphics/CloseAndStrokePath;

    invoke-direct {p1}, Lorg/apache/pdfbox/contentstream/operator/graphics/CloseAndStrokePath;-><init>()V

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->addOperator(Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;)V

    new-instance p1, Lorg/apache/pdfbox/contentstream/operator/graphics/StrokePath;

    invoke-direct {p1}, Lorg/apache/pdfbox/contentstream/operator/graphics/StrokePath;-><init>()V

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->addOperator(Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;)V

    new-instance p1, Lorg/apache/pdfbox/contentstream/operator/color/SetStrokingColor;

    invoke-direct {p1}, Lorg/apache/pdfbox/contentstream/operator/color/SetStrokingColor;-><init>()V

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->addOperator(Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;)V

    new-instance p1, Lorg/apache/pdfbox/contentstream/operator/color/SetNonStrokingColor;

    invoke-direct {p1}, Lorg/apache/pdfbox/contentstream/operator/color/SetNonStrokingColor;-><init>()V

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->addOperator(Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;)V

    new-instance p1, Lorg/apache/pdfbox/contentstream/operator/graphics/ShadingFill;

    invoke-direct {p1}, Lorg/apache/pdfbox/contentstream/operator/graphics/ShadingFill;-><init>()V

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->addOperator(Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;)V

    new-instance p1, Lorg/apache/pdfbox/contentstream/operator/text/NextLine;

    invoke-direct {p1}, Lorg/apache/pdfbox/contentstream/operator/text/NextLine;-><init>()V

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->addOperator(Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;)V

    new-instance p1, Lorg/apache/pdfbox/contentstream/operator/text/SetCharSpacing;

    invoke-direct {p1}, Lorg/apache/pdfbox/contentstream/operator/text/SetCharSpacing;-><init>()V

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->addOperator(Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;)V

    new-instance p1, Lorg/apache/pdfbox/contentstream/operator/text/MoveText;

    invoke-direct {p1}, Lorg/apache/pdfbox/contentstream/operator/text/MoveText;-><init>()V

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->addOperator(Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;)V

    new-instance p1, Lorg/apache/pdfbox/contentstream/operator/text/MoveTextSetLeading;

    invoke-direct {p1}, Lorg/apache/pdfbox/contentstream/operator/text/MoveTextSetLeading;-><init>()V

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->addOperator(Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;)V

    new-instance p1, Lorg/apache/pdfbox/contentstream/operator/text/SetFontAndSize;

    invoke-direct {p1}, Lorg/apache/pdfbox/contentstream/operator/text/SetFontAndSize;-><init>()V

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->addOperator(Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;)V

    new-instance p1, Lorg/apache/pdfbox/contentstream/operator/text/ShowText;

    invoke-direct {p1}, Lorg/apache/pdfbox/contentstream/operator/text/ShowText;-><init>()V

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->addOperator(Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;)V

    new-instance p1, Lorg/apache/pdfbox/contentstream/operator/text/ShowTextAdjusted;

    invoke-direct {p1}, Lorg/apache/pdfbox/contentstream/operator/text/ShowTextAdjusted;-><init>()V

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->addOperator(Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;)V

    new-instance p1, Lorg/apache/pdfbox/contentstream/operator/text/SetTextLeading;

    invoke-direct {p1}, Lorg/apache/pdfbox/contentstream/operator/text/SetTextLeading;-><init>()V

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->addOperator(Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;)V

    new-instance p1, Lorg/apache/pdfbox/contentstream/operator/state/SetMatrix;

    invoke-direct {p1}, Lorg/apache/pdfbox/contentstream/operator/state/SetMatrix;-><init>()V

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->addOperator(Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;)V

    new-instance p1, Lorg/apache/pdfbox/contentstream/operator/text/SetTextRenderingMode;

    invoke-direct {p1}, Lorg/apache/pdfbox/contentstream/operator/text/SetTextRenderingMode;-><init>()V

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->addOperator(Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;)V

    new-instance p1, Lorg/apache/pdfbox/contentstream/operator/text/SetTextRise;

    invoke-direct {p1}, Lorg/apache/pdfbox/contentstream/operator/text/SetTextRise;-><init>()V

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->addOperator(Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;)V

    new-instance p1, Lorg/apache/pdfbox/contentstream/operator/text/SetWordSpacing;

    invoke-direct {p1}, Lorg/apache/pdfbox/contentstream/operator/text/SetWordSpacing;-><init>()V

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->addOperator(Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;)V

    new-instance p1, Lorg/apache/pdfbox/contentstream/operator/text/SetTextHorizontalScaling;

    invoke-direct {p1}, Lorg/apache/pdfbox/contentstream/operator/text/SetTextHorizontalScaling;-><init>()V

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->addOperator(Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;)V

    new-instance p1, Lorg/apache/pdfbox/contentstream/operator/graphics/CurveToReplicateInitialPoint;

    invoke-direct {p1}, Lorg/apache/pdfbox/contentstream/operator/graphics/CurveToReplicateInitialPoint;-><init>()V

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->addOperator(Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;)V

    new-instance p1, Lorg/apache/pdfbox/contentstream/operator/state/SetLineWidth;

    invoke-direct {p1}, Lorg/apache/pdfbox/contentstream/operator/state/SetLineWidth;-><init>()V

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->addOperator(Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;)V

    new-instance p1, Lorg/apache/pdfbox/contentstream/operator/graphics/CurveToReplicateFinalPoint;

    invoke-direct {p1}, Lorg/apache/pdfbox/contentstream/operator/graphics/CurveToReplicateFinalPoint;-><init>()V

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->addOperator(Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;)V

    new-instance p1, Lorg/apache/pdfbox/contentstream/operator/text/ShowTextLine;

    invoke-direct {p1}, Lorg/apache/pdfbox/contentstream/operator/text/ShowTextLine;-><init>()V

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->addOperator(Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;)V

    new-instance p1, Lorg/apache/pdfbox/contentstream/operator/text/ShowTextLineAndSpace;

    invoke-direct {p1}, Lorg/apache/pdfbox/contentstream/operator/text/ShowTextLineAndSpace;-><init>()V

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->addOperator(Lorg/apache/pdfbox/contentstream/operator/OperatorProcessor;)V

    return-void
.end method


# virtual methods
.method public abstract appendRectangle(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;)V
.end method

.method public abstract clip(I)V
.end method

.method public abstract closePath()V
.end method

.method public abstract curveTo(FFFFFF)V
.end method

.method public abstract drawImage(Lorg/apache/pdfbox/pdmodel/graphics/image/PDImage;)V
.end method

.method public abstract endPath()V
.end method

.method public abstract fillAndStrokePath(I)V
.end method

.method public abstract fillPath(I)V
.end method

.method public abstract getCurrentPoint()Landroid/graphics/PointF;
.end method

.method public final getPage()Lorg/apache/pdfbox/pdmodel/PDPage;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/contentstream/PDFGraphicsStreamEngine;->page:Lorg/apache/pdfbox/pdmodel/PDPage;

    return-object v0
.end method

.method public abstract lineTo(FF)V
.end method

.method public abstract moveTo(FF)V
.end method

.method public abstract shadingFill(Lorg/apache/pdfbox/cos/COSName;)V
.end method

.method public abstract strokePath()V
.end method

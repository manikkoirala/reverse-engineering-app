.class final Lorg/apache/pdfbox/rendering/CIDType0Glyph2D;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/pdfbox/rendering/Glyph2D;


# instance fields
.field private final cache:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Landroid/graphics/Path;",
            ">;"
        }
    .end annotation
.end field

.field private final font:Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType0;

.field private final fontName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType0;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/pdfbox/rendering/CIDType0Glyph2D;->cache:Ljava/util/Map;

    iput-object p1, p0, Lorg/apache/pdfbox/rendering/CIDType0Glyph2D;->font:Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType0;

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/font/PDCIDFont;->getBaseFont()Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lorg/apache/pdfbox/rendering/CIDType0Glyph2D;->fontName:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public dispose()V
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/rendering/CIDType0Glyph2D;->cache:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    return-void
.end method

.method public getPathForCharacterCode(I)Landroid/graphics/Path;
    .locals 6

    const-string v0, "PdfBoxAndroid"

    iget-object v1, p0, Lorg/apache/pdfbox/rendering/CIDType0Glyph2D;->font:Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType0;

    invoke-virtual {v1}, Lorg/apache/pdfbox/pdmodel/font/PDCIDFont;->getParent()Lorg/apache/pdfbox/pdmodel/font/PDType0Font;

    move-result-object v1

    invoke-virtual {v1, p1}, Lorg/apache/pdfbox/pdmodel/font/PDType0Font;->codeToCID(I)I

    move-result v1

    iget-object v2, p0, Lorg/apache/pdfbox/rendering/CIDType0Glyph2D;->cache:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v0, p0, Lorg/apache/pdfbox/rendering/CIDType0Glyph2D;->cache:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/graphics/Path;

    return-object p1

    :cond_0
    :try_start_0
    iget-object v2, p0, Lorg/apache/pdfbox/rendering/CIDType0Glyph2D;->font:Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType0;

    invoke-virtual {v2, v1}, Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType0;->getType2CharString(I)Lorg/apache/fontbox/cff/Type2CharString;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/fontbox/cff/Type2CharString;->getGID()I

    move-result v3

    if-nez v3, :cond_1

    const-string v3, "%04x"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v5, 0x0

    aput-object v1, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "No glyph for "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v4, " (CID "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, ") in font "

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lorg/apache/pdfbox/rendering/CIDType0Glyph2D;->fontName:Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    invoke-virtual {v2}, Lorg/apache/fontbox/cff/Type1CharString;->getPath()Landroid/graphics/Path;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/pdfbox/rendering/CIDType0Glyph2D;->cache:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {v2, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    :catch_0
    move-exception p1

    const-string v1, "Glyph rendering failed"

    invoke-static {v0, v1, p1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    new-instance p1, Landroid/graphics/Path;

    invoke-direct {p1}, Landroid/graphics/Path;-><init>()V

    return-object p1
.end method

.class public final Lorg/apache/pdfbox/rendering/PageDrawer;
.super Lorg/apache/pdfbox/contentstream/PDFGraphicsStreamEngine;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/pdfbox/rendering/PageDrawer$TransparencyGroup;
    }
.end annotation


# instance fields
.field canvas:Landroid/graphics/Canvas;

.field private clipWindingRule:I

.field private final fontGlyph2D:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lorg/apache/pdfbox/pdmodel/font/PDFont;",
            "Lorg/apache/pdfbox/rendering/Glyph2D;",
            ">;"
        }
    .end annotation
.end field

.field private lastClip:Landroid/graphics/Region;

.field private linePath:Landroid/graphics/Path;

.field pageSize:Lorg/apache/pdfbox/pdmodel/common/PDRectangle;

.field paint:Landroid/graphics/Paint;

.field private textClippingArea:Landroid/graphics/Region;

.field private xform:Lorg/apache/pdfbox/util/awt/AffineTransform;


# direct methods
.method public constructor <init>(Lorg/apache/pdfbox/pdmodel/PDPage;)V
    .locals 0

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/contentstream/PDFGraphicsStreamEngine;-><init>(Lorg/apache/pdfbox/pdmodel/PDPage;)V

    const/4 p1, -0x1

    iput p1, p0, Lorg/apache/pdfbox/rendering/PageDrawer;->clipWindingRule:I

    new-instance p1, Landroid/graphics/Path;

    invoke-direct {p1}, Landroid/graphics/Path;-><init>()V

    iput-object p1, p0, Lorg/apache/pdfbox/rendering/PageDrawer;->linePath:Landroid/graphics/Path;

    new-instance p1, Ljava/util/HashMap;

    invoke-direct {p1}, Ljava/util/HashMap;-><init>()V

    iput-object p1, p0, Lorg/apache/pdfbox/rendering/PageDrawer;->fontGlyph2D:Ljava/util/Map;

    return-void
.end method

.method public static synthetic access$100(Lorg/apache/pdfbox/rendering/PageDrawer;Lorg/apache/pdfbox/pdmodel/graphics/form/PDFormXObject;)V
    .locals 0

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->processTransparencyGroup(Lorg/apache/pdfbox/pdmodel/graphics/form/PDFormXObject;)V

    return-void
.end method

.method private createGlyph2D(Lorg/apache/pdfbox/pdmodel/font/PDFont;)Lorg/apache/pdfbox/rendering/Glyph2D;
    .locals 3

    iget-object v0, p0, Lorg/apache/pdfbox/rendering/PageDrawer;->fontGlyph2D:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/pdfbox/rendering/PageDrawer;->fontGlyph2D:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lorg/apache/pdfbox/rendering/Glyph2D;

    return-object p1

    :cond_0
    instance-of v0, p1, Lorg/apache/pdfbox/pdmodel/font/PDTrueTypeFont;

    if-eqz v0, :cond_1

    move-object v0, p1

    check-cast v0, Lorg/apache/pdfbox/pdmodel/font/PDTrueTypeFont;

    new-instance v1, Lorg/apache/pdfbox/rendering/TTFGlyph2D;

    invoke-direct {v1, v0}, Lorg/apache/pdfbox/rendering/TTFGlyph2D;-><init>(Lorg/apache/pdfbox/pdmodel/font/PDTrueTypeFont;)V

    goto :goto_0

    :cond_1
    instance-of v0, p1, Lorg/apache/pdfbox/pdmodel/font/PDType1Font;

    if-eqz v0, :cond_2

    move-object v0, p1

    check-cast v0, Lorg/apache/pdfbox/pdmodel/font/PDType1Font;

    new-instance v1, Lorg/apache/pdfbox/rendering/Type1Glyph2D;

    invoke-direct {v1, v0}, Lorg/apache/pdfbox/rendering/Type1Glyph2D;-><init>(Lorg/apache/pdfbox/pdmodel/font/PDType1Equivalent;)V

    goto :goto_0

    :cond_2
    instance-of v0, p1, Lorg/apache/pdfbox/pdmodel/font/PDType1CFont;

    if-eqz v0, :cond_3

    move-object v0, p1

    check-cast v0, Lorg/apache/pdfbox/pdmodel/font/PDType1CFont;

    new-instance v1, Lorg/apache/pdfbox/rendering/Type1Glyph2D;

    invoke-direct {v1, v0}, Lorg/apache/pdfbox/rendering/Type1Glyph2D;-><init>(Lorg/apache/pdfbox/pdmodel/font/PDType1Equivalent;)V

    goto :goto_0

    :cond_3
    instance-of v0, p1, Lorg/apache/pdfbox/pdmodel/font/PDType0Font;

    if-eqz v0, :cond_8

    move-object v0, p1

    check-cast v0, Lorg/apache/pdfbox/pdmodel/font/PDType0Font;

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/font/PDType0Font;->getDescendantFont()Lorg/apache/pdfbox/pdmodel/font/PDCIDFont;

    move-result-object v1

    instance-of v1, v1, Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType2;

    if-eqz v1, :cond_4

    new-instance v1, Lorg/apache/pdfbox/rendering/TTFGlyph2D;

    invoke-direct {v1, v0}, Lorg/apache/pdfbox/rendering/TTFGlyph2D;-><init>(Lorg/apache/pdfbox/pdmodel/font/PDType0Font;)V

    goto :goto_0

    :cond_4
    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/font/PDType0Font;->getDescendantFont()Lorg/apache/pdfbox/pdmodel/font/PDCIDFont;

    move-result-object v1

    instance-of v1, v1, Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType0;

    if-eqz v1, :cond_5

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/font/PDType0Font;->getDescendantFont()Lorg/apache/pdfbox/pdmodel/font/PDCIDFont;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType0;

    new-instance v1, Lorg/apache/pdfbox/rendering/CIDType0Glyph2D;

    invoke-direct {v1, v0}, Lorg/apache/pdfbox/rendering/CIDType0Glyph2D;-><init>(Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType0;)V

    goto :goto_0

    :cond_5
    const/4 v0, 0x0

    move-object v1, v0

    :goto_0
    if-eqz v1, :cond_6

    iget-object v0, p0, Lorg/apache/pdfbox/rendering/PageDrawer;->fontGlyph2D:Ljava/util/Map;

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_6
    if-eqz v1, :cond_7

    return-object v1

    :cond_7
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "No font for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/font/PDFont;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_8
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Bad font type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private drawGlyph2D(Lorg/apache/pdfbox/rendering/Glyph2D;Lorg/apache/pdfbox/pdmodel/font/PDFont;ILorg/apache/pdfbox/util/Vector;Lorg/apache/pdfbox/util/awt/AffineTransform;)V
    .locals 6

    invoke-virtual {p0}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->getGraphicsState()Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->getTextState()Lorg/apache/pdfbox/pdmodel/graphics/state/PDTextState;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDTextState;->getRenderingMode()Lorg/apache/pdfbox/pdmodel/graphics/state/RenderingMode;

    move-result-object v0

    invoke-interface {p1, p3}, Lorg/apache/pdfbox/rendering/Glyph2D;->getPathForCharacterCode(I)Landroid/graphics/Path;

    move-result-object p1

    if-eqz p1, :cond_3

    invoke-virtual {p2}, Lorg/apache/pdfbox/pdmodel/font/PDFont;->isEmbedded()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-interface {p2, p3}, Lorg/apache/pdfbox/pdmodel/font/PDFontLike;->getWidthFromFont(I)F

    move-result p2

    const/4 p3, 0x0

    cmpl-float p3, p2, p3

    if-lez p3, :cond_0

    invoke-virtual {p4}, Lorg/apache/pdfbox/util/Vector;->getX()F

    move-result p3

    const/high16 v1, 0x447a0000    # 1000.0f

    mul-float/2addr p3, v1

    sub-float p3, p2, p3

    invoke-static {p3}, Ljava/lang/Math;->abs(F)F

    move-result p3

    float-to-double v2, p3

    const-wide v4, 0x3f1a36e2eb1c432dL    # 1.0E-4

    cmpl-double p3, v2, v4

    if-lez p3, :cond_0

    invoke-virtual {p4}, Lorg/apache/pdfbox/util/Vector;->getX()F

    move-result p3

    mul-float/2addr p3, v1

    div-float/2addr p3, p2

    float-to-double p2, p3

    const-wide/high16 v1, 0x3ff0000000000000L    # 1.0

    invoke-virtual {p5, p2, p3, v1, v2}, Lorg/apache/pdfbox/util/awt/AffineTransform;->scale(DD)V

    :cond_0
    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/graphics/state/RenderingMode;->isFill()Z

    move-result p2

    const/high16 p3, -0x1000000

    const/4 p4, 0x0

    if-eqz p2, :cond_1

    iget-object p2, p0, Lorg/apache/pdfbox/rendering/PageDrawer;->paint:Landroid/graphics/Paint;

    invoke-virtual {p2, p3}, Landroid/graphics/Paint;->setColor(I)V

    invoke-direct {p0}, Lorg/apache/pdfbox/rendering/PageDrawer;->setClip()V

    iget-object p2, p0, Lorg/apache/pdfbox/rendering/PageDrawer;->paint:Landroid/graphics/Paint;

    sget-object p5, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {p2, p5}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object p2, p0, Lorg/apache/pdfbox/rendering/PageDrawer;->canvas:Landroid/graphics/Canvas;

    invoke-virtual {p2, p1}, Landroid/graphics/Canvas;->clipPath(Landroid/graphics/Path;)Z

    iget-object p2, p0, Lorg/apache/pdfbox/rendering/PageDrawer;->canvas:Landroid/graphics/Canvas;

    new-instance p5, Landroid/graphics/Rect;

    iget-object v1, p0, Lorg/apache/pdfbox/rendering/PageDrawer;->canvas:Landroid/graphics/Canvas;

    invoke-virtual {v1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v1

    iget-object v2, p0, Lorg/apache/pdfbox/rendering/PageDrawer;->canvas:Landroid/graphics/Canvas;

    invoke-virtual {v2}, Landroid/graphics/Canvas;->getHeight()I

    move-result v2

    invoke-direct {p5, p4, p4, v1, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    iget-object v1, p0, Lorg/apache/pdfbox/rendering/PageDrawer;->paint:Landroid/graphics/Paint;

    invoke-virtual {p2, p5, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    :cond_1
    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/graphics/state/RenderingMode;->isStroke()Z

    move-result p2

    if-eqz p2, :cond_2

    iget-object p2, p0, Lorg/apache/pdfbox/rendering/PageDrawer;->paint:Landroid/graphics/Paint;

    invoke-virtual {p2, p3}, Landroid/graphics/Paint;->setColor(I)V

    invoke-direct {p0}, Lorg/apache/pdfbox/rendering/PageDrawer;->setClip()V

    iget-object p2, p0, Lorg/apache/pdfbox/rendering/PageDrawer;->paint:Landroid/graphics/Paint;

    sget-object p3, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {p2, p3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object p2, p0, Lorg/apache/pdfbox/rendering/PageDrawer;->canvas:Landroid/graphics/Canvas;

    invoke-virtual {p2, p1}, Landroid/graphics/Canvas;->clipPath(Landroid/graphics/Path;)Z

    iget-object p1, p0, Lorg/apache/pdfbox/rendering/PageDrawer;->canvas:Landroid/graphics/Canvas;

    new-instance p2, Landroid/graphics/Rect;

    iget-object p3, p0, Lorg/apache/pdfbox/rendering/PageDrawer;->canvas:Landroid/graphics/Canvas;

    invoke-virtual {p3}, Landroid/graphics/Canvas;->getWidth()I

    move-result p3

    iget-object p5, p0, Lorg/apache/pdfbox/rendering/PageDrawer;->canvas:Landroid/graphics/Canvas;

    invoke-virtual {p5}, Landroid/graphics/Canvas;->getHeight()I

    move-result p5

    invoke-direct {p2, p4, p4, p3, p5}, Landroid/graphics/Rect;-><init>(IIII)V

    iget-object p3, p0, Lorg/apache/pdfbox/rendering/PageDrawer;->paint:Landroid/graphics/Paint;

    invoke-virtual {p1, p2, p3}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    :cond_2
    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/graphics/state/RenderingMode;->isClip()Z

    :cond_3
    return-void
.end method

.method private getColor(Lorg/apache/pdfbox/pdmodel/graphics/color/PDColor;)I
    .locals 4

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/graphics/color/PDColor;->getColorSpace()Lorg/apache/pdfbox/pdmodel/graphics/color/PDColorSpace;

    move-result-object v0

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/graphics/color/PDColor;->getComponents()[F

    move-result-object p1

    invoke-virtual {v0, p1}, Lorg/apache/pdfbox/pdmodel/graphics/color/PDColorSpace;->toRGB([F)[F

    move-result-object p1

    const/4 v0, 0x0

    aget v0, p1, v0

    const/high16 v1, 0x437f0000    # 255.0f

    mul-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    const/4 v2, 0x1

    aget v2, p1, v2

    mul-float/2addr v2, v1

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    const/4 v3, 0x2

    aget p1, p1, v3

    mul-float/2addr p1, v1

    invoke-static {p1}, Ljava/lang/Math;->round(F)I

    move-result p1

    invoke-static {v0, v2, p1}, Landroid/graphics/Color;->rgb(III)I

    move-result p1

    return p1
.end method

.method private getStrokingColor()I
    .locals 1

    invoke-virtual {p0}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->getGraphicsState()Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->getStrokingColor()Lorg/apache/pdfbox/pdmodel/graphics/color/PDColor;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/rendering/PageDrawer;->getColor(Lorg/apache/pdfbox/pdmodel/graphics/color/PDColor;)I

    move-result v0

    return v0
.end method

.method private setClip()V
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->getGraphicsState()Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->getCurrentClippingPath()Landroid/graphics/Region;

    move-result-object v0

    iget-object v1, p0, Lorg/apache/pdfbox/rendering/PageDrawer;->lastClip:Landroid/graphics/Region;

    if-eq v0, v1, :cond_0

    iget-object v1, p0, Lorg/apache/pdfbox/rendering/PageDrawer;->canvas:Landroid/graphics/Canvas;

    invoke-virtual {v1, v0}, Landroid/graphics/Canvas;->clipRegion(Landroid/graphics/Region;)Z

    iput-object v0, p0, Lorg/apache/pdfbox/rendering/PageDrawer;->lastClip:Landroid/graphics/Region;

    :cond_0
    return-void
.end method

.method private setRenderingHints()V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/rendering/PageDrawer;->paint:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    return-void
.end method

.method private setStroke()V
    .locals 6

    invoke-virtual {p0}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->getGraphicsState()Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->getLineWidth()F

    move-result v1

    invoke-virtual {p0, v1}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->transformWidth(F)F

    move-result v1

    float-to-double v2, v1

    const-wide/high16 v4, 0x3fd0000000000000L    # 0.25

    cmpg-double v2, v2, v4

    if-gez v2, :cond_0

    const/high16 v1, 0x3e800000    # 0.25f

    :cond_0
    iget-object v2, p0, Lorg/apache/pdfbox/rendering/PageDrawer;->paint:Landroid/graphics/Paint;

    invoke-virtual {v2, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    iget-object v1, p0, Lorg/apache/pdfbox/rendering/PageDrawer;->paint:Landroid/graphics/Paint;

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->getLineCap()Landroid/graphics/Paint$Cap;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    iget-object v1, p0, Lorg/apache/pdfbox/rendering/PageDrawer;->paint:Landroid/graphics/Paint;

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->getLineJoin()Landroid/graphics/Paint$Join;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setStrokeJoin(Landroid/graphics/Paint$Join;)V

    return-void
.end method


# virtual methods
.method public appendRectangle(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/rendering/PageDrawer;->linePath:Landroid/graphics/Path;

    iget v1, p1, Landroid/graphics/PointF;->x:F

    iget p1, p1, Landroid/graphics/PointF;->y:F

    invoke-virtual {v0, v1, p1}, Landroid/graphics/Path;->moveTo(FF)V

    iget-object p1, p0, Lorg/apache/pdfbox/rendering/PageDrawer;->linePath:Landroid/graphics/Path;

    iget v0, p2, Landroid/graphics/PointF;->x:F

    iget p2, p2, Landroid/graphics/PointF;->y:F

    invoke-virtual {p1, v0, p2}, Landroid/graphics/Path;->lineTo(FF)V

    iget-object p1, p0, Lorg/apache/pdfbox/rendering/PageDrawer;->linePath:Landroid/graphics/Path;

    iget p2, p3, Landroid/graphics/PointF;->x:F

    iget p3, p3, Landroid/graphics/PointF;->y:F

    invoke-virtual {p1, p2, p3}, Landroid/graphics/Path;->lineTo(FF)V

    iget-object p1, p0, Lorg/apache/pdfbox/rendering/PageDrawer;->linePath:Landroid/graphics/Path;

    iget p2, p4, Landroid/graphics/PointF;->x:F

    iget p3, p4, Landroid/graphics/PointF;->y:F

    invoke-virtual {p1, p2, p3}, Landroid/graphics/Path;->lineTo(FF)V

    iget-object p1, p0, Lorg/apache/pdfbox/rendering/PageDrawer;->linePath:Landroid/graphics/Path;

    invoke-virtual {p1}, Landroid/graphics/Path;->close()V

    return-void
.end method

.method public beginText()V
    .locals 0

    invoke-direct {p0}, Lorg/apache/pdfbox/rendering/PageDrawer;->setClip()V

    return-void
.end method

.method public clip(I)V
    .locals 0

    return-void
.end method

.method public closePath()V
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/rendering/PageDrawer;->linePath:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->close()V

    return-void
.end method

.method public curveTo(FFFFFF)V
    .locals 7

    iget-object v0, p0, Lorg/apache/pdfbox/rendering/PageDrawer;->linePath:Landroid/graphics/Path;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    return-void
.end method

.method public drawBufferedImage(Landroid/graphics/Bitmap;Lorg/apache/pdfbox/util/awt/AffineTransform;)V
    .locals 9

    invoke-direct {p0}, Lorg/apache/pdfbox/rendering/PageDrawer;->setClip()V

    invoke-virtual {p0}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->getGraphicsState()Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->getSoftMask()Lorg/apache/pdfbox/pdmodel/graphics/state/PDSoftMask;

    move-result-object v0

    const-wide/16 v1, 0x0

    const/high16 v3, 0x3f800000    # 1.0f

    if-eqz v0, :cond_0

    new-instance p1, Lorg/apache/pdfbox/util/awt/AffineTransform;

    invoke-direct {p1, p2}, Lorg/apache/pdfbox/util/awt/AffineTransform;-><init>(Lorg/apache/pdfbox/util/awt/AffineTransform;)V

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/high16 v6, -0x4010000000000000L    # -1.0

    invoke-virtual {p1, v4, v5, v6, v7}, Lorg/apache/pdfbox/util/awt/AffineTransform;->scale(DD)V

    invoke-virtual {p1, v1, v2, v6, v7}, Lorg/apache/pdfbox/util/awt/AffineTransform;->translate(DD)V

    new-instance p1, Landroid/graphics/RectF;

    const/4 p2, 0x0

    invoke-direct {p1, p2, p2, v3, v3}, Landroid/graphics/RectF;-><init>(FFFF)V

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result p1

    new-instance v4, Lorg/apache/pdfbox/util/awt/AffineTransform;

    invoke-direct {v4, p2}, Lorg/apache/pdfbox/util/awt/AffineTransform;-><init>(Lorg/apache/pdfbox/util/awt/AffineTransform;)V

    int-to-float p2, v0

    div-float/2addr v3, p2

    float-to-double v5, v3

    const/high16 p2, -0x40800000    # -1.0f

    int-to-float v0, p1

    div-float/2addr p2, v0

    float-to-double v7, p2

    invoke-virtual {v4, v5, v6, v7, v8}, Lorg/apache/pdfbox/util/awt/AffineTransform;->scale(DD)V

    neg-int p1, p1

    int-to-double p1, p1

    invoke-virtual {v4, v1, v2, p1, p2}, Lorg/apache/pdfbox/util/awt/AffineTransform;->translate(DD)V

    :goto_0
    return-void
.end method

.method public drawImage(Lorg/apache/pdfbox/pdmodel/graphics/image/PDImage;)V
    .locals 5

    invoke-virtual {p0}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->getGraphicsState()Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->getCurrentTransformationMatrix()Lorg/apache/pdfbox/util/Matrix;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/pdfbox/util/Matrix;->createAffineTransform()Lorg/apache/pdfbox/util/awt/AffineTransform;

    move-result-object v0

    invoke-interface {p1}, Lorg/apache/pdfbox/pdmodel/graphics/image/PDImage;->getInterpolate()Z

    move-result v1

    if-nez v1, :cond_2

    invoke-interface {p1}, Lorg/apache/pdfbox/pdmodel/graphics/image/PDImage;->getWidth()I

    move-result v1

    int-to-long v1, v1

    invoke-virtual {v0}, Lorg/apache/pdfbox/util/awt/AffineTransform;->getScaleX()D

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Math;->round(D)J

    move-result-wide v3

    cmp-long v1, v1, v3

    if-ltz v1, :cond_1

    invoke-interface {p1}, Lorg/apache/pdfbox/pdmodel/graphics/image/PDImage;->getHeight()I

    move-result v1

    int-to-long v1, v1

    invoke-virtual {v0}, Lorg/apache/pdfbox/util/awt/AffineTransform;->getScaleY()D

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Math;->round(D)J

    move-result-wide v3

    cmp-long v1, v1, v3

    if-gez v1, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v1, 0x1

    :goto_1
    if-nez v1, :cond_2

    invoke-interface {p1}, Lorg/apache/pdfbox/pdmodel/graphics/image/PDImage;->isStencil()Z

    :cond_2
    invoke-interface {p1}, Lorg/apache/pdfbox/pdmodel/graphics/image/PDImage;->isStencil()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {p0}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->getGraphicsState()Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->getNonStrokingColor()Lorg/apache/pdfbox/pdmodel/graphics/color/PDColor;

    goto :goto_2

    :cond_3
    invoke-interface {p1}, Lorg/apache/pdfbox/pdmodel/graphics/image/PDImage;->getImage()Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {p0, v1, v0}, Lorg/apache/pdfbox/rendering/PageDrawer;->drawBufferedImage(Landroid/graphics/Bitmap;Lorg/apache/pdfbox/util/awt/AffineTransform;)V

    :goto_2
    invoke-interface {p1}, Lorg/apache/pdfbox/pdmodel/graphics/image/PDImage;->getInterpolate()Z

    move-result p1

    if-nez p1, :cond_4

    invoke-direct {p0}, Lorg/apache/pdfbox/rendering/PageDrawer;->setRenderingHints()V

    :cond_4
    return-void
.end method

.method public drawPage(Landroid/graphics/Paint;Landroid/graphics/Canvas;Lorg/apache/pdfbox/pdmodel/common/PDRectangle;)V
    .locals 1

    iput-object p1, p0, Lorg/apache/pdfbox/rendering/PageDrawer;->paint:Landroid/graphics/Paint;

    iput-object p2, p0, Lorg/apache/pdfbox/rendering/PageDrawer;->canvas:Landroid/graphics/Canvas;

    new-instance p1, Lorg/apache/pdfbox/util/awt/AffineTransform;

    invoke-virtual {p2}, Landroid/graphics/Canvas;->getMatrix()Landroid/graphics/Matrix;

    move-result-object p2

    invoke-direct {p1, p2}, Lorg/apache/pdfbox/util/awt/AffineTransform;-><init>(Landroid/graphics/Matrix;)V

    iput-object p1, p0, Lorg/apache/pdfbox/rendering/PageDrawer;->xform:Lorg/apache/pdfbox/util/awt/AffineTransform;

    iput-object p3, p0, Lorg/apache/pdfbox/rendering/PageDrawer;->pageSize:Lorg/apache/pdfbox/pdmodel/common/PDRectangle;

    invoke-direct {p0}, Lorg/apache/pdfbox/rendering/PageDrawer;->setRenderingHints()V

    iget-object p1, p0, Lorg/apache/pdfbox/rendering/PageDrawer;->canvas:Landroid/graphics/Canvas;

    invoke-virtual {p3}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->getHeight()F

    move-result p2

    const/4 v0, 0x0

    invoke-virtual {p1, v0, p2}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object p1, p0, Lorg/apache/pdfbox/rendering/PageDrawer;->canvas:Landroid/graphics/Canvas;

    const/high16 p2, -0x40800000    # -1.0f

    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {p1, v0, p2}, Landroid/graphics/Canvas;->scale(FF)V

    iget-object p1, p0, Lorg/apache/pdfbox/rendering/PageDrawer;->paint:Landroid/graphics/Paint;

    sget-object p2, Landroid/graphics/Paint$Cap;->BUTT:Landroid/graphics/Paint$Cap;

    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    iget-object p1, p0, Lorg/apache/pdfbox/rendering/PageDrawer;->paint:Landroid/graphics/Paint;

    sget-object p2, Landroid/graphics/Paint$Join;->MITER:Landroid/graphics/Paint$Join;

    invoke-virtual {p1, p2}, Landroid/graphics/Paint;->setStrokeJoin(Landroid/graphics/Paint$Join;)V

    iget-object p1, p0, Lorg/apache/pdfbox/rendering/PageDrawer;->paint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    iget-object p1, p0, Lorg/apache/pdfbox/rendering/PageDrawer;->canvas:Landroid/graphics/Canvas;

    invoke-virtual {p3}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->getLowerLeftX()F

    move-result p2

    neg-float p2, p2

    invoke-virtual {p3}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->getLowerLeftY()F

    move-result p3

    neg-float p3, p3

    invoke-virtual {p1, p2, p3}, Landroid/graphics/Canvas;->translate(FF)V

    invoke-virtual {p0}, Lorg/apache/pdfbox/contentstream/PDFGraphicsStreamEngine;->getPage()Lorg/apache/pdfbox/pdmodel/PDPage;

    move-result-object p1

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->processPage(Lorg/apache/pdfbox/pdmodel/PDPage;)V

    invoke-virtual {p0}, Lorg/apache/pdfbox/contentstream/PDFGraphicsStreamEngine;->getPage()Lorg/apache/pdfbox/pdmodel/PDPage;

    move-result-object p1

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/PDPage;->getAnnotations()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotation;

    invoke-virtual {p0, p2}, Lorg/apache/pdfbox/rendering/PageDrawer;->showAnnotation(Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotation;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public endPath()V
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/rendering/PageDrawer;->linePath:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    return-void
.end method

.method public fillAndStrokePath(I)V
    .locals 2

    new-instance v0, Landroid/graphics/Path;

    iget-object v1, p0, Lorg/apache/pdfbox/rendering/PageDrawer;->linePath:Landroid/graphics/Path;

    invoke-direct {v0, v1}, Landroid/graphics/Path;-><init>(Landroid/graphics/Path;)V

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/rendering/PageDrawer;->fillPath(I)V

    iput-object v0, p0, Lorg/apache/pdfbox/rendering/PageDrawer;->linePath:Landroid/graphics/Path;

    invoke-virtual {p0}, Lorg/apache/pdfbox/rendering/PageDrawer;->strokePath()V

    return-void
.end method

.method public fillPath(I)V
    .locals 0

    invoke-direct {p0}, Lorg/apache/pdfbox/rendering/PageDrawer;->setClip()V

    invoke-direct {p0}, Lorg/apache/pdfbox/rendering/PageDrawer;->setRenderingHints()V

    return-void
.end method

.method public getCurrentPoint()Landroid/graphics/PointF;
    .locals 2

    const-string v0, "PdfBoxAndroid"

    const-string v1, "PageDrawer.getCurrentPoint does not return the right value"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    return-object v0
.end method

.method public lineTo(FF)V
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/rendering/PageDrawer;->linePath:Landroid/graphics/Path;

    invoke-virtual {v0, p1, p2}, Landroid/graphics/Path;->lineTo(FF)V

    return-void
.end method

.method public moveTo(FF)V
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/rendering/PageDrawer;->linePath:Landroid/graphics/Path;

    invoke-virtual {v0, p1, p2}, Landroid/graphics/Path;->moveTo(FF)V

    return-void
.end method

.method public shadingFill(Lorg/apache/pdfbox/cos/COSName;)V
    .locals 1

    invoke-virtual {p0}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->getResources()Lorg/apache/pdfbox/pdmodel/PDResources;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/pdfbox/pdmodel/PDResources;->getShading(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/pdmodel/graphics/shading/PDShading;

    invoke-virtual {p0}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->getGraphicsState()Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;

    move-result-object p1

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->getCurrentTransformationMatrix()Lorg/apache/pdfbox/util/Matrix;

    return-void
.end method

.method public showAnnotation(Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotation;)V
    .locals 1

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotation;->isNoView()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotation;->isHidden()Z

    move-result v0

    if-eqz v0, :cond_1

    return-void

    :cond_1
    invoke-super {p0, p1}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->showAnnotation(Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotation;)V

    return-void
.end method

.method public showText([B)V
    .locals 3

    invoke-virtual {p0}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->getGraphicsState()Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->getTextState()Lorg/apache/pdfbox/pdmodel/graphics/state/PDTextState;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDTextState;->getRenderingMode()Lorg/apache/pdfbox/pdmodel/graphics/state/RenderingMode;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/pdfbox/pdmodel/graphics/state/RenderingMode;->isClip()Z

    move-result v2

    if-eqz v2, :cond_0

    new-instance v2, Landroid/graphics/Region;

    invoke-direct {v2}, Landroid/graphics/Region;-><init>()V

    iput-object v2, p0, Lorg/apache/pdfbox/rendering/PageDrawer;->textClippingArea:Landroid/graphics/Region;

    :cond_0
    invoke-super {p0, p1}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->showText([B)V

    invoke-virtual {v1}, Lorg/apache/pdfbox/pdmodel/graphics/state/RenderingMode;->isClip()Z

    move-result p1

    if-eqz p1, :cond_1

    iget-object p1, p0, Lorg/apache/pdfbox/rendering/PageDrawer;->textClippingArea:Landroid/graphics/Region;

    invoke-virtual {v0, p1}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->intersectClippingPath(Landroid/graphics/Region;)V

    const/4 p1, 0x0

    iput-object p1, p0, Lorg/apache/pdfbox/rendering/PageDrawer;->textClippingArea:Landroid/graphics/Region;

    :cond_1
    return-void
.end method

.method public showTransparencyGroup(Lorg/apache/pdfbox/pdmodel/graphics/form/PDFormXObject;)V
    .locals 3

    new-instance v0, Lorg/apache/pdfbox/rendering/PageDrawer$TransparencyGroup;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-direct {v0, p0, p1, v1, v2}, Lorg/apache/pdfbox/rendering/PageDrawer$TransparencyGroup;-><init>(Lorg/apache/pdfbox/rendering/PageDrawer;Lorg/apache/pdfbox/pdmodel/graphics/form/PDFormXObject;ZLorg/apache/pdfbox/rendering/PageDrawer$1;)V

    invoke-direct {p0}, Lorg/apache/pdfbox/rendering/PageDrawer;->setClip()V

    invoke-virtual {p0}, Lorg/apache/pdfbox/contentstream/PDFStreamEngine;->getGraphicsState()Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;

    move-result-object p1

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/graphics/state/PDGraphicsState;->getSoftMask()Lorg/apache/pdfbox/pdmodel/graphics/state/PDSoftMask;

    return-void
.end method

.method public strokePath()V
    .locals 3

    invoke-direct {p0}, Lorg/apache/pdfbox/rendering/PageDrawer;->setStroke()V

    invoke-direct {p0}, Lorg/apache/pdfbox/rendering/PageDrawer;->setClip()V

    iget-object v0, p0, Lorg/apache/pdfbox/rendering/PageDrawer;->paint:Landroid/graphics/Paint;

    const/16 v1, 0xff

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, v2, v2}, Landroid/graphics/Paint;->setARGB(IIII)V

    iget-object v0, p0, Lorg/apache/pdfbox/rendering/PageDrawer;->paint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v0, p0, Lorg/apache/pdfbox/rendering/PageDrawer;->paint:Landroid/graphics/Paint;

    invoke-direct {p0}, Lorg/apache/pdfbox/rendering/PageDrawer;->getStrokingColor()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v0, p0, Lorg/apache/pdfbox/rendering/PageDrawer;->canvas:Landroid/graphics/Canvas;

    iget-object v1, p0, Lorg/apache/pdfbox/rendering/PageDrawer;->linePath:Landroid/graphics/Path;

    iget-object v2, p0, Lorg/apache/pdfbox/rendering/PageDrawer;->paint:Landroid/graphics/Paint;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    iget-object v0, p0, Lorg/apache/pdfbox/rendering/PageDrawer;->linePath:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    return-void
.end method

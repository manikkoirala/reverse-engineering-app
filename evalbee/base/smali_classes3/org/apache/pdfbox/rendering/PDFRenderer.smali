.class public Lorg/apache/pdfbox/rendering/PDFRenderer;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field protected final document:Lorg/apache/pdfbox/pdmodel/PDDocument;


# direct methods
.method public constructor <init>(Lorg/apache/pdfbox/pdmodel/PDDocument;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lorg/apache/pdfbox/rendering/PDFRenderer;->document:Lorg/apache/pdfbox/pdmodel/PDDocument;

    return-void
.end method


# virtual methods
.method public renderImage(I)Landroid/graphics/Bitmap;
    .locals 2

    .line 1
    const/high16 v0, 0x3f800000    # 1.0f

    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-virtual {p0, p1, v0, v1}, Lorg/apache/pdfbox/rendering/PDFRenderer;->renderImage(IFLandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object p1

    return-object p1
.end method

.method public renderImage(IFLandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;
    .locals 10

    .line 2
    iget-object v0, p0, Lorg/apache/pdfbox/rendering/PDFRenderer;->document:Lorg/apache/pdfbox/pdmodel/PDDocument;

    invoke-virtual {v0, p1}, Lorg/apache/pdfbox/pdmodel/PDDocument;->getPage(I)Lorg/apache/pdfbox/pdmodel/PDPage;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/pdfbox/pdmodel/PDPage;->getCropBox()Lorg/apache/pdfbox/pdmodel/common/PDRectangle;

    move-result-object p1

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->getWidth()F

    move-result v0

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->getHeight()F

    move-result p1

    mul-float/2addr v0, p2

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    mul-float/2addr p1, p2

    invoke-static {p1}, Ljava/lang/Math;->round(F)I

    move-result p1

    invoke-virtual {v2}, Lorg/apache/pdfbox/pdmodel/PDPage;->getRotation()I

    move-result v1

    const/16 v3, 0x5a

    if-eq v1, v3, :cond_1

    const/16 v3, 0x10e

    if-ne v1, v3, :cond_0

    goto :goto_0

    :cond_0
    invoke-static {v0, p1, p3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object p1

    goto :goto_1

    :cond_1
    :goto_0
    invoke-static {p1, v0, p3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object p1

    :goto_1
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    new-instance v9, Landroid/graphics/Canvas;

    invoke-direct {v9, p1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    if-eq p3, v1, :cond_2

    const/4 p3, -0x1

    invoke-virtual {v0, p3}, Landroid/graphics/Paint;->setColor(I)V

    sget-object p3, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, p3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result p3

    int-to-float v6, p3

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result p3

    int-to-float v7, p3

    move-object v3, v9

    move-object v8, v0

    invoke-virtual/range {v3 .. v8}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    invoke-virtual {v0}, Landroid/graphics/Paint;->reset()V

    :cond_2
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    move-object v1, p0

    move-object v3, v0

    move-object v4, v9

    move v7, p2

    move v8, p2

    invoke-virtual/range {v1 .. v8}, Lorg/apache/pdfbox/rendering/PDFRenderer;->renderPage(Lorg/apache/pdfbox/pdmodel/PDPage;Landroid/graphics/Paint;Landroid/graphics/Canvas;IIFF)V

    return-object p1
.end method

.method public renderPage(Lorg/apache/pdfbox/pdmodel/PDPage;Landroid/graphics/Paint;Landroid/graphics/Canvas;IIFF)V
    .locals 1

    invoke-virtual {p3, p6, p7}, Landroid/graphics/Canvas;->scale(FF)V

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/PDPage;->getCropBox()Lorg/apache/pdfbox/pdmodel/common/PDRectangle;

    move-result-object p4

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/PDPage;->getRotation()I

    move-result p5

    if-eqz p5, :cond_3

    const/16 p6, 0x5a

    const/4 p7, 0x0

    if-eq p5, p6, :cond_2

    const/16 p6, 0xb4

    if-eq p5, p6, :cond_1

    const/16 p6, 0x10e

    if-eq p5, p6, :cond_0

    move p6, p7

    goto :goto_0

    :cond_0
    invoke-virtual {p4}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->getWidth()F

    move-result p6

    goto :goto_0

    :cond_1
    invoke-virtual {p4}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->getWidth()F

    move-result p7

    invoke-virtual {p4}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->getHeight()F

    move-result p6

    goto :goto_0

    :cond_2
    invoke-virtual {p4}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->getHeight()F

    move-result p6

    move v0, p7

    move p7, p6

    move p6, v0

    :goto_0
    invoke-virtual {p3, p7, p6}, Landroid/graphics/Canvas;->translate(FF)V

    int-to-double p5, p5

    invoke-static {p5, p6}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide p5

    double-to-float p5, p5

    invoke-virtual {p3, p5}, Landroid/graphics/Canvas;->rotate(F)V

    :cond_3
    new-instance p5, Lorg/apache/pdfbox/rendering/PageDrawer;

    invoke-direct {p5, p1}, Lorg/apache/pdfbox/rendering/PageDrawer;-><init>(Lorg/apache/pdfbox/pdmodel/PDPage;)V

    invoke-virtual {p5, p2, p3, p4}, Lorg/apache/pdfbox/rendering/PageDrawer;->drawPage(Landroid/graphics/Paint;Landroid/graphics/Canvas;Lorg/apache/pdfbox/pdmodel/common/PDRectangle;)V

    return-void
.end method

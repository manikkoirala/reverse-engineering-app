.class final Lorg/apache/pdfbox/rendering/TTFGlyph2D;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/pdfbox/rendering/Glyph2D;


# instance fields
.field private final font:Lorg/apache/pdfbox/pdmodel/font/PDFont;

.field private final glyphs:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Landroid/graphics/Path;",
            ">;"
        }
    .end annotation
.end field

.field private hasScaling:Z

.field private final isCIDFont:Z

.field private scale:F

.field private final ttf:Lorg/apache/fontbox/ttf/TrueTypeFont;


# direct methods
.method public constructor <init>(Lorg/apache/fontbox/ttf/TrueTypeFont;Lorg/apache/pdfbox/pdmodel/font/PDFont;Z)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lorg/apache/pdfbox/rendering/TTFGlyph2D;->scale:F

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/pdfbox/rendering/TTFGlyph2D;->glyphs:Ljava/util/Map;

    iput-object p2, p0, Lorg/apache/pdfbox/rendering/TTFGlyph2D;->font:Lorg/apache/pdfbox/pdmodel/font/PDFont;

    iput-object p1, p0, Lorg/apache/pdfbox/rendering/TTFGlyph2D;->ttf:Lorg/apache/fontbox/ttf/TrueTypeFont;

    iput-boolean p3, p0, Lorg/apache/pdfbox/rendering/TTFGlyph2D;->isCIDFont:Z

    invoke-virtual {p1}, Lorg/apache/fontbox/ttf/TrueTypeFont;->getHeader()Lorg/apache/fontbox/ttf/HeaderTable;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lorg/apache/fontbox/ttf/HeaderTable;->getUnitsPerEm()I

    move-result p2

    const/16 p3, 0x3e8

    if-eq p2, p3, :cond_0

    invoke-virtual {p1}, Lorg/apache/fontbox/ttf/HeaderTable;->getUnitsPerEm()I

    move-result p1

    int-to-float p1, p1

    const/high16 p2, 0x447a0000    # 1000.0f

    div-float/2addr p2, p1

    iput p2, p0, Lorg/apache/pdfbox/rendering/TTFGlyph2D;->scale:F

    const/4 p1, 0x1

    iput-boolean p1, p0, Lorg/apache/pdfbox/rendering/TTFGlyph2D;->hasScaling:Z

    :cond_0
    return-void
.end method

.method public constructor <init>(Lorg/apache/pdfbox/pdmodel/font/PDTrueTypeFont;)V
    .locals 2

    .line 2
    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/font/PDTrueTypeFont;->getTrueTypeFont()Lorg/apache/fontbox/ttf/TrueTypeFont;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, p1, v1}, Lorg/apache/pdfbox/rendering/TTFGlyph2D;-><init>(Lorg/apache/fontbox/ttf/TrueTypeFont;Lorg/apache/pdfbox/pdmodel/font/PDFont;Z)V

    return-void
.end method

.method public constructor <init>(Lorg/apache/pdfbox/pdmodel/font/PDType0Font;)V
    .locals 2

    .line 3
    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/font/PDType0Font;->getDescendantFont()Lorg/apache/pdfbox/pdmodel/font/PDCIDFont;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType2;

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/font/PDCIDFontType2;->getTrueTypeFont()Lorg/apache/fontbox/ttf/TrueTypeFont;

    move-result-object v0

    const/4 v1, 0x1

    invoke-direct {p0, v0, p1, v1}, Lorg/apache/pdfbox/rendering/TTFGlyph2D;-><init>(Lorg/apache/fontbox/ttf/TrueTypeFont;Lorg/apache/pdfbox/pdmodel/font/PDFont;Z)V

    return-void
.end method

.method private getGIDForCharacterCode(I)I
    .locals 1

    iget-boolean v0, p0, Lorg/apache/pdfbox/rendering/TTFGlyph2D;->isCIDFont:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/apache/pdfbox/rendering/TTFGlyph2D;->font:Lorg/apache/pdfbox/pdmodel/font/PDFont;

    check-cast v0, Lorg/apache/pdfbox/pdmodel/font/PDType0Font;

    invoke-virtual {v0, p1}, Lorg/apache/pdfbox/pdmodel/font/PDType0Font;->codeToGID(I)I

    move-result p1

    return p1

    :cond_0
    iget-object v0, p0, Lorg/apache/pdfbox/rendering/TTFGlyph2D;->font:Lorg/apache/pdfbox/pdmodel/font/PDFont;

    check-cast v0, Lorg/apache/pdfbox/pdmodel/font/PDTrueTypeFont;

    invoke-virtual {v0, p1}, Lorg/apache/pdfbox/pdmodel/font/PDTrueTypeFont;->codeToGID(I)I

    move-result p1

    return p1
.end method


# virtual methods
.method public dispose()V
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/rendering/TTFGlyph2D;->glyphs:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    return-void
.end method

.method public getPathForCharacterCode(I)Landroid/graphics/Path;
    .locals 1

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/rendering/TTFGlyph2D;->getGIDForCharacterCode(I)I

    move-result v0

    invoke-virtual {p0, v0, p1}, Lorg/apache/pdfbox/rendering/TTFGlyph2D;->getPathForGID(II)Landroid/graphics/Path;

    move-result-object p1

    return-object p1
.end method

.method public getPathForGID(II)Landroid/graphics/Path;
    .locals 6

    iget-object v0, p0, Lorg/apache/pdfbox/rendering/TTFGlyph2D;->glyphs:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    iget-object p2, p0, Lorg/apache/pdfbox/rendering/TTFGlyph2D;->glyphs:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {p2, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Landroid/graphics/Path;

    goto/16 :goto_2

    :cond_0
    if-eqz p1, :cond_1

    iget-object v0, p0, Lorg/apache/pdfbox/rendering/TTFGlyph2D;->ttf:Lorg/apache/fontbox/ttf/TrueTypeFont;

    invoke-virtual {v0}, Lorg/apache/fontbox/ttf/TrueTypeFont;->getMaximumProfile()Lorg/apache/fontbox/ttf/MaximumProfileTable;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/fontbox/ttf/MaximumProfileTable;->getNumGlyphs()I

    move-result v0

    if-lt p1, v0, :cond_3

    :cond_1
    iget-boolean v0, p0, Lorg/apache/pdfbox/rendering/TTFGlyph2D;->isCIDFont:Z

    const-string v2, "No glyph for "

    const-string v3, "PdfBoxAndroid"

    if-eqz v0, :cond_2

    iget-object v0, p0, Lorg/apache/pdfbox/rendering/TTFGlyph2D;->font:Lorg/apache/pdfbox/pdmodel/font/PDFont;

    check-cast v0, Lorg/apache/pdfbox/pdmodel/font/PDType0Font;

    invoke-virtual {v0, p2}, Lorg/apache/pdfbox/pdmodel/font/PDType0Font;->codeToCID(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    filled-new-array {v0}, [Ljava/lang/Object;

    move-result-object v0

    const-string v4, "%04x"

    invoke-static {v4, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p2, " (CID "

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p2, ") in font "

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p2, p0, Lorg/apache/pdfbox/rendering/TTFGlyph2D;->font:Lorg/apache/pdfbox/pdmodel/font/PDFont;

    invoke-virtual {p2}, Lorg/apache/pdfbox/pdmodel/font/PDFont;->getName()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    goto :goto_0

    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string p2, " in font "

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object p2, p0, Lorg/apache/pdfbox/rendering/TTFGlyph2D;->font:Lorg/apache/pdfbox/pdmodel/font/PDFont;

    invoke-virtual {p2}, Lorg/apache/pdfbox/pdmodel/font/PDFont;->getName()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    :goto_0
    invoke-static {v3, p2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    iget-object p2, p0, Lorg/apache/pdfbox/rendering/TTFGlyph2D;->ttf:Lorg/apache/fontbox/ttf/TrueTypeFont;

    invoke-virtual {p2}, Lorg/apache/fontbox/ttf/TrueTypeFont;->getGlyph()Lorg/apache/fontbox/ttf/GlyphTable;

    move-result-object p2

    invoke-virtual {p2, p1}, Lorg/apache/fontbox/ttf/GlyphTable;->getGlyph(I)Lorg/apache/fontbox/ttf/GlyphData;

    move-result-object p2

    if-nez p1, :cond_4

    iget-object v0, p0, Lorg/apache/pdfbox/rendering/TTFGlyph2D;->font:Lorg/apache/pdfbox/pdmodel/font/PDFont;

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/font/PDFont;->isEmbedded()Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lorg/apache/pdfbox/rendering/TTFGlyph2D;->font:Lorg/apache/pdfbox/pdmodel/font/PDFont;

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/font/PDFont;->isStandard14()Z

    move-result v0

    if-nez v0, :cond_4

    move-object p2, v1

    :cond_4
    if-nez p2, :cond_6

    new-instance p2, Landroid/graphics/Path;

    invoke-direct {p2}, Landroid/graphics/Path;-><init>()V

    :cond_5
    :goto_1
    iget-object v0, p0, Lorg/apache/pdfbox/rendering/TTFGlyph2D;->glyphs:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object p1, p2

    goto :goto_2

    :cond_6
    invoke-virtual {p2}, Lorg/apache/fontbox/ttf/GlyphData;->getPath()Landroid/graphics/Path;

    move-result-object p2

    iget-boolean v0, p0, Lorg/apache/pdfbox/rendering/TTFGlyph2D;->hasScaling:Z

    if-eqz v0, :cond_5

    iget v0, p0, Lorg/apache/pdfbox/rendering/TTFGlyph2D;->scale:F

    float-to-double v2, v0

    float-to-double v4, v0

    invoke-static {v2, v3, v4, v5}, Lorg/apache/pdfbox/util/awt/AffineTransform;->getScaleInstance(DD)Lorg/apache/pdfbox/util/awt/AffineTransform;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/pdfbox/util/awt/AffineTransform;->toMatrix()Landroid/graphics/Matrix;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/graphics/Path;->transform(Landroid/graphics/Matrix;)V

    goto :goto_1

    :goto_2
    if-eqz p1, :cond_7

    new-instance v1, Landroid/graphics/Path;

    invoke-direct {v1, p1}, Landroid/graphics/Path;-><init>(Landroid/graphics/Path;)V

    :cond_7
    return-object v1
.end method

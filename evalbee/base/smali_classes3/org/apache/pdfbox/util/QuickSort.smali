.class public Lorg/apache/pdfbox/util/QuickSort;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final objComp:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator<",
            "+",
            "Ljava/lang/Comparable;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lorg/apache/pdfbox/util/QuickSort$1;

    invoke-direct {v0}, Lorg/apache/pdfbox/util/QuickSort$1;-><init>()V

    sput-object v0, Lorg/apache/pdfbox/util/QuickSort;->objComp:Ljava/util/Comparator;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static quicksort(Ljava/util/List;Ljava/util/Comparator;II)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/List<",
            "TT;>;",
            "Ljava/util/Comparator<",
            "TT;>;II)V"
        }
    .end annotation

    if-ge p2, p3, :cond_0

    invoke-static {p0, p1, p2, p3}, Lorg/apache/pdfbox/util/QuickSort;->split(Ljava/util/List;Ljava/util/Comparator;II)I

    move-result v0

    add-int/lit8 v1, v0, -0x1

    invoke-static {p0, p1, p2, v1}, Lorg/apache/pdfbox/util/QuickSort;->quicksort(Ljava/util/List;Ljava/util/Comparator;II)V

    add-int/lit8 v0, v0, 0x1

    invoke-static {p0, p1, v0, p3}, Lorg/apache/pdfbox/util/QuickSort;->quicksort(Ljava/util/List;Ljava/util/Comparator;II)V

    :cond_0
    return-void
.end method

.method public static sort(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Ljava/lang/Comparable;",
            ">(",
            "Ljava/util/List<",
            "TT;>;)V"
        }
    .end annotation

    .line 1
    sget-object v0, Lorg/apache/pdfbox/util/QuickSort;->objComp:Ljava/util/Comparator;

    invoke-static {p0, v0}, Lorg/apache/pdfbox/util/QuickSort;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    return-void
.end method

.method public static sort(Ljava/util/List;Ljava/util/Comparator;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/List<",
            "TT;>;",
            "Ljava/util/Comparator<",
            "TT;>;)V"
        }
    .end annotation

    .line 2
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x2

    if-ge v0, v1, :cond_0

    return-void

    :cond_0
    add-int/lit8 v0, v0, -0x1

    const/4 v1, 0x0

    invoke-static {p0, p1, v1, v0}, Lorg/apache/pdfbox/util/QuickSort;->quicksort(Ljava/util/List;Ljava/util/Comparator;II)V

    return-void
.end method

.method private static split(Ljava/util/List;Ljava/util/Comparator;II)I
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/List<",
            "TT;>;",
            "Ljava/util/Comparator<",
            "TT;>;II)I"
        }
    .end annotation

    add-int/lit8 v0, p3, -0x1

    invoke-interface {p0, p3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    move v2, p2

    :cond_0
    :goto_0
    invoke-interface {p0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-interface {p1, v3, v1}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v3

    if-gtz v3, :cond_1

    if-ge v2, p3, :cond_1

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    :goto_1
    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-interface {p1, v1, v3}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v3

    if-gtz v3, :cond_2

    if-le v0, p2, :cond_2

    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    :cond_2
    if-ge v2, v0, :cond_3

    invoke-static {p0, v2, v0}, Lorg/apache/pdfbox/util/QuickSort;->swap(Ljava/util/List;II)V

    :cond_3
    if-lt v2, v0, :cond_0

    invoke-interface {p0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p2

    invoke-interface {p1, v1, p2}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result p1

    if-gez p1, :cond_4

    invoke-static {p0, v2, p3}, Lorg/apache/pdfbox/util/QuickSort;->swap(Ljava/util/List;II)V

    :cond_4
    return v2
.end method

.method private static swap(Ljava/util/List;II)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/List<",
            "TT;>;II)V"
        }
    .end annotation

    invoke-interface {p0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-interface {p0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {p0, p1, v1}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    invoke-interface {p0, p2, v0}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.class public Lorg/apache/pdfbox/util/PageExtractor;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field protected endPage:I

.field protected sourceDocument:Lorg/apache/pdfbox/pdmodel/PDDocument;

.field protected startPage:I


# direct methods
.method public constructor <init>(Lorg/apache/pdfbox/pdmodel/PDDocument;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lorg/apache/pdfbox/util/PageExtractor;->startPage:I

    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/pdfbox/util/PageExtractor;->endPage:I

    iput-object p1, p0, Lorg/apache/pdfbox/util/PageExtractor;->sourceDocument:Lorg/apache/pdfbox/pdmodel/PDDocument;

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/PDDocument;->getNumberOfPages()I

    move-result p1

    iput p1, p0, Lorg/apache/pdfbox/util/PageExtractor;->endPage:I

    return-void
.end method

.method public constructor <init>(Lorg/apache/pdfbox/pdmodel/PDDocument;II)V
    .locals 0

    .line 2
    invoke-direct {p0, p1}, Lorg/apache/pdfbox/util/PageExtractor;-><init>(Lorg/apache/pdfbox/pdmodel/PDDocument;)V

    iput p2, p0, Lorg/apache/pdfbox/util/PageExtractor;->startPage:I

    iput p3, p0, Lorg/apache/pdfbox/util/PageExtractor;->endPage:I

    return-void
.end method


# virtual methods
.method public extract()Lorg/apache/pdfbox/pdmodel/PDDocument;
    .locals 5

    new-instance v0, Lorg/apache/pdfbox/pdmodel/PDDocument;

    invoke-direct {v0}, Lorg/apache/pdfbox/pdmodel/PDDocument;-><init>()V

    iget-object v1, p0, Lorg/apache/pdfbox/util/PageExtractor;->sourceDocument:Lorg/apache/pdfbox/pdmodel/PDDocument;

    invoke-virtual {v1}, Lorg/apache/pdfbox/pdmodel/PDDocument;->getDocumentInformation()Lorg/apache/pdfbox/pdmodel/PDDocumentInformation;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/pdmodel/PDDocument;->setDocumentInformation(Lorg/apache/pdfbox/pdmodel/PDDocumentInformation;)V

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/PDDocument;->getDocumentCatalog()Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/pdfbox/util/PageExtractor;->sourceDocument:Lorg/apache/pdfbox/pdmodel/PDDocument;

    invoke-virtual {v2}, Lorg/apache/pdfbox/pdmodel/PDDocument;->getDocumentCatalog()Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;->getViewerPreferences()Lorg/apache/pdfbox/pdmodel/interactive/viewerpreferences/PDViewerPreferences;

    move-result-object v2

    invoke-virtual {v1, v2}, Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;->setViewerPreferences(Lorg/apache/pdfbox/pdmodel/interactive/viewerpreferences/PDViewerPreferences;)V

    iget v1, p0, Lorg/apache/pdfbox/util/PageExtractor;->startPage:I

    :goto_0
    iget v2, p0, Lorg/apache/pdfbox/util/PageExtractor;->endPage:I

    if-gt v1, v2, :cond_0

    iget-object v2, p0, Lorg/apache/pdfbox/util/PageExtractor;->sourceDocument:Lorg/apache/pdfbox/pdmodel/PDDocument;

    add-int/lit8 v3, v1, -0x1

    invoke-virtual {v2, v3}, Lorg/apache/pdfbox/pdmodel/PDDocument;->getPage(I)Lorg/apache/pdfbox/pdmodel/PDPage;

    move-result-object v2

    invoke-virtual {v0, v2}, Lorg/apache/pdfbox/pdmodel/PDDocument;->importPage(Lorg/apache/pdfbox/pdmodel/PDPage;)Lorg/apache/pdfbox/pdmodel/PDPage;

    move-result-object v3

    invoke-virtual {v2}, Lorg/apache/pdfbox/pdmodel/PDPage;->getCropBox()Lorg/apache/pdfbox/pdmodel/common/PDRectangle;

    move-result-object v4

    invoke-virtual {v3, v4}, Lorg/apache/pdfbox/pdmodel/PDPage;->setCropBox(Lorg/apache/pdfbox/pdmodel/common/PDRectangle;)V

    invoke-virtual {v2}, Lorg/apache/pdfbox/pdmodel/PDPage;->getMediaBox()Lorg/apache/pdfbox/pdmodel/common/PDRectangle;

    move-result-object v4

    invoke-virtual {v3, v4}, Lorg/apache/pdfbox/pdmodel/PDPage;->setMediaBox(Lorg/apache/pdfbox/pdmodel/common/PDRectangle;)V

    invoke-virtual {v2}, Lorg/apache/pdfbox/pdmodel/PDPage;->getResources()Lorg/apache/pdfbox/pdmodel/PDResources;

    move-result-object v4

    invoke-virtual {v3, v4}, Lorg/apache/pdfbox/pdmodel/PDPage;->setResources(Lorg/apache/pdfbox/pdmodel/PDResources;)V

    invoke-virtual {v2}, Lorg/apache/pdfbox/pdmodel/PDPage;->getRotation()I

    move-result v2

    invoke-virtual {v3, v2}, Lorg/apache/pdfbox/pdmodel/PDPage;->setRotation(I)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method public getEndPage()I
    .locals 1

    iget v0, p0, Lorg/apache/pdfbox/util/PageExtractor;->endPage:I

    return v0
.end method

.method public getStartPage()I
    .locals 1

    iget v0, p0, Lorg/apache/pdfbox/util/PageExtractor;->startPage:I

    return v0
.end method

.method public setEndPage(I)V
    .locals 0

    iput p1, p0, Lorg/apache/pdfbox/util/PageExtractor;->endPage:I

    return-void
.end method

.method public setStartPage(I)V
    .locals 0

    iput p1, p0, Lorg/apache/pdfbox/util/PageExtractor;->startPage:I

    return-void
.end method

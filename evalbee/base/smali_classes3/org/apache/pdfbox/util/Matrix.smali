.class public final Lorg/apache/pdfbox/util/Matrix;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Cloneable;


# static fields
.field static final DEFAULT_SINGLE:[F


# instance fields
.field private single:[F


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    const/16 v0, 0x9

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    sput-object v0, Lorg/apache/pdfbox/util/Matrix;->DEFAULT_SINGLE:[F

    return-void

    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x0
        0x0
        0x0
        0x3f800000    # 1.0f
        0x0
        0x0
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public constructor <init>()V
    .locals 4

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lorg/apache/pdfbox/util/Matrix;->DEFAULT_SINGLE:[F

    array-length v1, v0

    new-array v1, v1, [F

    iput-object v1, p0, Lorg/apache/pdfbox/util/Matrix;->single:[F

    const/4 v2, 0x0

    array-length v3, v0

    invoke-static {v0, v2, v1, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-void
.end method

.method public constructor <init>(FFFFFF)V
    .locals 2

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lorg/apache/pdfbox/util/Matrix;->DEFAULT_SINGLE:[F

    array-length v0, v0

    new-array v0, v0, [F

    iput-object v0, p0, Lorg/apache/pdfbox/util/Matrix;->single:[F

    const/4 v1, 0x0

    aput p1, v0, v1

    const/4 p1, 0x1

    aput p2, v0, p1

    const/4 p1, 0x3

    aput p3, v0, p1

    const/4 p1, 0x4

    aput p4, v0, p1

    const/4 p1, 0x6

    aput p5, v0, p1

    const/4 p1, 0x7

    aput p6, v0, p1

    const/16 p1, 0x8

    const/high16 p2, 0x3f800000    # 1.0f

    aput p2, v0, p1

    return-void
.end method

.method public constructor <init>(Lorg/apache/pdfbox/cos/COSArray;)V
    .locals 3

    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lorg/apache/pdfbox/util/Matrix;->DEFAULT_SINGLE:[F

    array-length v0, v0

    new-array v0, v0, [F

    iput-object v0, p0, Lorg/apache/pdfbox/util/Matrix;->single:[F

    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Lorg/apache/pdfbox/cos/COSArray;->get(I)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v2

    check-cast v2, Lorg/apache/pdfbox/cos/COSNumber;

    invoke-virtual {v2}, Lorg/apache/pdfbox/cos/COSNumber;->floatValue()F

    move-result v2

    aput v2, v0, v1

    iget-object v0, p0, Lorg/apache/pdfbox/util/Matrix;->single:[F

    const/4 v1, 0x1

    invoke-virtual {p1, v1}, Lorg/apache/pdfbox/cos/COSArray;->get(I)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v2

    check-cast v2, Lorg/apache/pdfbox/cos/COSNumber;

    invoke-virtual {v2}, Lorg/apache/pdfbox/cos/COSNumber;->floatValue()F

    move-result v2

    aput v2, v0, v1

    iget-object v0, p0, Lorg/apache/pdfbox/util/Matrix;->single:[F

    const/4 v1, 0x2

    invoke-virtual {p1, v1}, Lorg/apache/pdfbox/cos/COSArray;->get(I)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v1

    check-cast v1, Lorg/apache/pdfbox/cos/COSNumber;

    invoke-virtual {v1}, Lorg/apache/pdfbox/cos/COSNumber;->floatValue()F

    move-result v1

    const/4 v2, 0x3

    aput v1, v0, v2

    iget-object v0, p0, Lorg/apache/pdfbox/util/Matrix;->single:[F

    invoke-virtual {p1, v2}, Lorg/apache/pdfbox/cos/COSArray;->get(I)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v1

    check-cast v1, Lorg/apache/pdfbox/cos/COSNumber;

    invoke-virtual {v1}, Lorg/apache/pdfbox/cos/COSNumber;->floatValue()F

    move-result v1

    const/4 v2, 0x4

    aput v1, v0, v2

    iget-object v0, p0, Lorg/apache/pdfbox/util/Matrix;->single:[F

    invoke-virtual {p1, v2}, Lorg/apache/pdfbox/cos/COSArray;->get(I)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v1

    check-cast v1, Lorg/apache/pdfbox/cos/COSNumber;

    invoke-virtual {v1}, Lorg/apache/pdfbox/cos/COSNumber;->floatValue()F

    move-result v1

    const/4 v2, 0x6

    aput v1, v0, v2

    iget-object v0, p0, Lorg/apache/pdfbox/util/Matrix;->single:[F

    const/4 v1, 0x5

    invoke-virtual {p1, v1}, Lorg/apache/pdfbox/cos/COSArray;->get(I)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object p1

    check-cast p1, Lorg/apache/pdfbox/cos/COSNumber;

    invoke-virtual {p1}, Lorg/apache/pdfbox/cos/COSNumber;->floatValue()F

    move-result p1

    const/4 v1, 0x7

    aput p1, v0, v1

    iget-object p1, p0, Lorg/apache/pdfbox/util/Matrix;->single:[F

    const/16 v0, 0x8

    const/high16 v1, 0x3f800000    # 1.0f

    aput v1, p1, v0

    return-void
.end method

.method public constructor <init>(Lorg/apache/pdfbox/util/awt/AffineTransform;)V
    .locals 4

    .line 4
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Lorg/apache/pdfbox/util/Matrix;->DEFAULT_SINGLE:[F

    array-length v1, v0

    new-array v1, v1, [F

    iput-object v1, p0, Lorg/apache/pdfbox/util/Matrix;->single:[F

    array-length v2, v0

    const/4 v3, 0x0

    invoke-static {v0, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v0, p0, Lorg/apache/pdfbox/util/Matrix;->single:[F

    invoke-virtual {p1}, Lorg/apache/pdfbox/util/awt/AffineTransform;->getScaleX()D

    move-result-wide v1

    double-to-float v1, v1

    aput v1, v0, v3

    iget-object v0, p0, Lorg/apache/pdfbox/util/Matrix;->single:[F

    invoke-virtual {p1}, Lorg/apache/pdfbox/util/awt/AffineTransform;->getShearY()D

    move-result-wide v1

    double-to-float v1, v1

    const/4 v2, 0x1

    aput v1, v0, v2

    iget-object v0, p0, Lorg/apache/pdfbox/util/Matrix;->single:[F

    invoke-virtual {p1}, Lorg/apache/pdfbox/util/awt/AffineTransform;->getShearX()D

    move-result-wide v1

    double-to-float v1, v1

    const/4 v2, 0x3

    aput v1, v0, v2

    iget-object v0, p0, Lorg/apache/pdfbox/util/Matrix;->single:[F

    invoke-virtual {p1}, Lorg/apache/pdfbox/util/awt/AffineTransform;->getScaleY()D

    move-result-wide v1

    double-to-float v1, v1

    const/4 v2, 0x4

    aput v1, v0, v2

    iget-object v0, p0, Lorg/apache/pdfbox/util/Matrix;->single:[F

    invoke-virtual {p1}, Lorg/apache/pdfbox/util/awt/AffineTransform;->getTranslateX()D

    move-result-wide v1

    double-to-float v1, v1

    const/4 v2, 0x6

    aput v1, v0, v2

    iget-object v0, p0, Lorg/apache/pdfbox/util/Matrix;->single:[F

    invoke-virtual {p1}, Lorg/apache/pdfbox/util/awt/AffineTransform;->getTranslateY()D

    move-result-wide v1

    double-to-float p1, v1

    const/4 v1, 0x7

    aput p1, v0, v1

    return-void
.end method

.method public static concatenate(Lorg/apache/pdfbox/util/Matrix;Lorg/apache/pdfbox/util/Matrix;)Lorg/apache/pdfbox/util/Matrix;
    .locals 0

    .line 1
    invoke-virtual {p0}, Lorg/apache/pdfbox/util/Matrix;->clone()Lorg/apache/pdfbox/util/Matrix;

    move-result-object p0

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/util/Matrix;->concatenate(Lorg/apache/pdfbox/util/Matrix;)V

    return-object p0
.end method

.method public static getRotateInstance(DFF)Lorg/apache/pdfbox/util/Matrix;
    .locals 3

    invoke-static {p0, p1}, Ljava/lang/Math;->cos(D)D

    move-result-wide v0

    double-to-float v0, v0

    invoke-static {p0, p1}, Ljava/lang/Math;->sin(D)D

    move-result-wide p0

    double-to-float p0, p0

    new-instance p1, Lorg/apache/pdfbox/util/Matrix;

    invoke-direct {p1}, Lorg/apache/pdfbox/util/Matrix;-><init>()V

    iget-object v1, p1, Lorg/apache/pdfbox/util/Matrix;->single:[F

    const/4 v2, 0x0

    aput v0, v1, v2

    const/4 v2, 0x1

    aput p0, v1, v2

    const/4 v2, 0x3

    neg-float p0, p0

    aput p0, v1, v2

    const/4 p0, 0x4

    aput v0, v1, p0

    const/4 p0, 0x6

    aput p2, v1, p0

    const/4 p0, 0x7

    aput p3, v1, p0

    return-object p1
.end method

.method public static getScaleInstance(FF)Lorg/apache/pdfbox/util/Matrix;
    .locals 3

    new-instance v0, Lorg/apache/pdfbox/util/Matrix;

    invoke-direct {v0}, Lorg/apache/pdfbox/util/Matrix;-><init>()V

    iget-object v1, v0, Lorg/apache/pdfbox/util/Matrix;->single:[F

    const/4 v2, 0x0

    aput p0, v1, v2

    const/4 p0, 0x4

    aput p1, v1, p0

    return-object v0
.end method

.method public static getTranslateInstance(FF)Lorg/apache/pdfbox/util/Matrix;
    .locals 3

    new-instance v0, Lorg/apache/pdfbox/util/Matrix;

    invoke-direct {v0}, Lorg/apache/pdfbox/util/Matrix;-><init>()V

    iget-object v1, v0, Lorg/apache/pdfbox/util/Matrix;->single:[F

    const/4 v2, 0x6

    aput p0, v1, v2

    const/4 p0, 0x7

    aput p1, v1, p0

    return-object v0
.end method

.method public static getTranslatingInstance(FF)Lorg/apache/pdfbox/util/Matrix;
    .locals 0

    invoke-static {p0, p1}, Lorg/apache/pdfbox/util/Matrix;->getTranslateInstance(FF)Lorg/apache/pdfbox/util/Matrix;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public bridge synthetic clone()Ljava/lang/Object;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lorg/apache/pdfbox/util/Matrix;->clone()Lorg/apache/pdfbox/util/Matrix;

    move-result-object v0

    return-object v0
.end method

.method public clone()Lorg/apache/pdfbox/util/Matrix;
    .locals 5

    .line 2
    new-instance v0, Lorg/apache/pdfbox/util/Matrix;

    invoke-direct {v0}, Lorg/apache/pdfbox/util/Matrix;-><init>()V

    iget-object v1, p0, Lorg/apache/pdfbox/util/Matrix;->single:[F

    iget-object v2, v0, Lorg/apache/pdfbox/util/Matrix;->single:[F

    const/16 v3, 0x9

    const/4 v4, 0x0

    invoke-static {v1, v4, v2, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v0
.end method

.method public concatenate(Lorg/apache/pdfbox/util/Matrix;)V
    .locals 0

    .line 2
    invoke-virtual {p1, p0, p0}, Lorg/apache/pdfbox/util/Matrix;->multiply(Lorg/apache/pdfbox/util/Matrix;Lorg/apache/pdfbox/util/Matrix;)Lorg/apache/pdfbox/util/Matrix;

    return-void
.end method

.method public createAffineTransform()Lorg/apache/pdfbox/util/awt/AffineTransform;
    .locals 14

    new-instance v13, Lorg/apache/pdfbox/util/awt/AffineTransform;

    iget-object v0, p0, Lorg/apache/pdfbox/util/Matrix;->single:[F

    const/4 v1, 0x0

    aget v1, v0, v1

    float-to-double v1, v1

    const/4 v3, 0x1

    aget v3, v0, v3

    float-to-double v3, v3

    const/4 v5, 0x3

    aget v5, v0, v5

    float-to-double v5, v5

    const/4 v7, 0x4

    aget v7, v0, v7

    float-to-double v7, v7

    const/4 v9, 0x6

    aget v9, v0, v9

    float-to-double v9, v9

    const/4 v11, 0x7

    aget v0, v0, v11

    float-to-double v11, v0

    move-object v0, v13

    invoke-direct/range {v0 .. v12}, Lorg/apache/pdfbox/util/awt/AffineTransform;-><init>(DDDDDD)V

    return-object v13
.end method

.method public extractScaling()Lorg/apache/pdfbox/util/Matrix;
    .locals 5
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    new-instance v0, Lorg/apache/pdfbox/util/Matrix;

    invoke-direct {v0}, Lorg/apache/pdfbox/util/Matrix;-><init>()V

    iget-object v1, v0, Lorg/apache/pdfbox/util/Matrix;->single:[F

    iget-object v2, p0, Lorg/apache/pdfbox/util/Matrix;->single:[F

    const/4 v3, 0x0

    aget v4, v2, v3

    aput v4, v1, v3

    const/4 v3, 0x4

    aget v2, v2, v3

    aput v2, v1, v3

    return-object v0
.end method

.method public extractTranslating()Lorg/apache/pdfbox/util/Matrix;
    .locals 5
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    new-instance v0, Lorg/apache/pdfbox/util/Matrix;

    invoke-direct {v0}, Lorg/apache/pdfbox/util/Matrix;-><init>()V

    iget-object v1, v0, Lorg/apache/pdfbox/util/Matrix;->single:[F

    iget-object v2, p0, Lorg/apache/pdfbox/util/Matrix;->single:[F

    const/4 v3, 0x6

    aget v4, v2, v3

    aput v4, v1, v3

    const/4 v3, 0x7

    aget v2, v2, v3

    aput v2, v1, v3

    return-object v0
.end method

.method public getScaleX()F
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/util/Matrix;->single:[F

    const/4 v1, 0x0

    aget v0, v0, v1

    return v0
.end method

.method public getScaleY()F
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/util/Matrix;->single:[F

    const/4 v1, 0x4

    aget v0, v0, v1

    return v0
.end method

.method public getScalingFactorX()F
    .locals 7

    iget-object v0, p0, Lorg/apache/pdfbox/util/Matrix;->single:[F

    const/4 v1, 0x0

    aget v1, v0, v1

    const/4 v2, 0x1

    aget v3, v0, v2

    const/4 v4, 0x0

    cmpl-float v3, v3, v4

    if-nez v3, :cond_0

    const/4 v3, 0x3

    aget v0, v0, v3

    cmpl-float v0, v0, v4

    if-eqz v0, :cond_1

    :cond_0
    float-to-double v0, v1

    const-wide/high16 v3, 0x4000000000000000L    # 2.0

    invoke-static {v0, v1, v3, v4}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    iget-object v5, p0, Lorg/apache/pdfbox/util/Matrix;->single:[F

    aget v2, v5, v2

    float-to-double v5, v2

    invoke-static {v5, v6, v3, v4}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v2

    add-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    double-to-float v1, v0

    :cond_1
    return v1
.end method

.method public getScalingFactorY()F
    .locals 6

    iget-object v0, p0, Lorg/apache/pdfbox/util/Matrix;->single:[F

    const/4 v1, 0x4

    aget v2, v0, v1

    const/4 v3, 0x1

    aget v3, v0, v3

    const/4 v4, 0x0

    cmpl-float v3, v3, v4

    const/4 v5, 0x3

    if-nez v3, :cond_0

    aget v3, v0, v5

    cmpl-float v3, v3, v4

    if-eqz v3, :cond_1

    :cond_0
    aget v0, v0, v5

    float-to-double v2, v0

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v2

    iget-object v0, p0, Lorg/apache/pdfbox/util/Matrix;->single:[F

    aget v0, v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1, v4, v5}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    add-double/2addr v2, v0

    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    double-to-float v2, v0

    :cond_1
    return v2
.end method

.method public getShearX()F
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/util/Matrix;->single:[F

    const/4 v1, 0x3

    aget v0, v0, v1

    return v0
.end method

.method public getShearY()F
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/util/Matrix;->single:[F

    const/4 v1, 0x1

    aget v0, v0, v1

    return v0
.end method

.method public getTranslateX()F
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/util/Matrix;->single:[F

    const/4 v1, 0x6

    aget v0, v0, v1

    return v0
.end method

.method public getTranslateY()F
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/util/Matrix;->single:[F

    const/4 v1, 0x7

    aget v0, v0, v1

    return v0
.end method

.method public getValue(II)F
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/util/Matrix;->single:[F

    mul-int/lit8 p1, p1, 0x3

    add-int/2addr p1, p2

    aget p1, v0, p1

    return p1
.end method

.method public getValues()[[F
    .locals 8

    const/4 v0, 0x3

    filled-new-array {v0, v0}, [I

    move-result-object v1

    sget-object v2, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    invoke-static {v2, v1}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [[F

    const/4 v2, 0x0

    aget-object v3, v1, v2

    iget-object v4, p0, Lorg/apache/pdfbox/util/Matrix;->single:[F

    aget v5, v4, v2

    aput v5, v3, v2

    const/4 v5, 0x1

    aget v6, v4, v5

    aput v6, v3, v5

    const/4 v6, 0x2

    aget v7, v4, v6

    aput v7, v3, v6

    aget-object v3, v1, v5

    aget v0, v4, v0

    aput v0, v3, v2

    const/4 v0, 0x4

    aget v0, v4, v0

    aput v0, v3, v5

    const/4 v0, 0x5

    aget v0, v4, v0

    aput v0, v3, v6

    aget-object v0, v1, v6

    const/4 v3, 0x6

    aget v3, v4, v3

    aput v3, v0, v2

    const/4 v2, 0x7

    aget v2, v4, v2

    aput v2, v0, v5

    const/16 v2, 0x8

    aget v2, v4, v2

    aput v2, v0, v6

    return-object v1
.end method

.method public getValuesAsDouble()[[D
    .locals 9
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    const/4 v0, 0x3

    filled-new-array {v0, v0}, [I

    move-result-object v1

    sget-object v2, Ljava/lang/Double;->TYPE:Ljava/lang/Class;

    invoke-static {v2, v1}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [[D

    const/4 v2, 0x0

    aget-object v3, v1, v2

    iget-object v4, p0, Lorg/apache/pdfbox/util/Matrix;->single:[F

    aget v5, v4, v2

    float-to-double v5, v5

    aput-wide v5, v3, v2

    const/4 v5, 0x1

    aget v6, v4, v5

    float-to-double v6, v6

    aput-wide v6, v3, v5

    const/4 v6, 0x2

    aget v7, v4, v6

    float-to-double v7, v7

    aput-wide v7, v3, v6

    aget-object v3, v1, v5

    aget v0, v4, v0

    float-to-double v7, v0

    aput-wide v7, v3, v2

    const/4 v0, 0x4

    aget v0, v4, v0

    float-to-double v7, v0

    aput-wide v7, v3, v5

    const/4 v0, 0x5

    aget v0, v4, v0

    float-to-double v7, v0

    aput-wide v7, v3, v6

    aget-object v0, v1, v6

    const/4 v3, 0x6

    aget v3, v4, v3

    float-to-double v7, v3

    aput-wide v7, v0, v2

    const/4 v2, 0x7

    aget v2, v4, v2

    float-to-double v2, v2

    aput-wide v2, v0, v5

    const/16 v2, 0x8

    aget v2, v4, v2

    float-to-double v2, v2

    aput-wide v2, v0, v6

    return-object v1
.end method

.method public getXPosition()F
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    iget-object v0, p0, Lorg/apache/pdfbox/util/Matrix;->single:[F

    const/4 v1, 0x6

    aget v0, v0, v1

    return v0
.end method

.method public getYPosition()F
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    iget-object v0, p0, Lorg/apache/pdfbox/util/Matrix;->single:[F

    const/4 v1, 0x7

    aget v0, v0, v1

    return v0
.end method

.method public multiply(Lorg/apache/pdfbox/util/Matrix;)Lorg/apache/pdfbox/util/Matrix;
    .locals 1

    .line 1
    new-instance v0, Lorg/apache/pdfbox/util/Matrix;

    invoke-direct {v0}, Lorg/apache/pdfbox/util/Matrix;-><init>()V

    invoke-virtual {p0, p1, v0}, Lorg/apache/pdfbox/util/Matrix;->multiply(Lorg/apache/pdfbox/util/Matrix;Lorg/apache/pdfbox/util/Matrix;)Lorg/apache/pdfbox/util/Matrix;

    move-result-object p1

    return-object p1
.end method

.method public multiply(Lorg/apache/pdfbox/util/Matrix;Lorg/apache/pdfbox/util/Matrix;)Lorg/apache/pdfbox/util/Matrix;
    .locals 23

    .line 2
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    if-nez p2, :cond_0

    new-instance v2, Lorg/apache/pdfbox/util/Matrix;

    invoke-direct {v2}, Lorg/apache/pdfbox/util/Matrix;-><init>()V

    goto :goto_0

    :cond_0
    move-object/from16 v2, p2

    :goto_0
    if-eqz v1, :cond_3

    iget-object v3, v1, Lorg/apache/pdfbox/util/Matrix;->single:[F

    if-eqz v3, :cond_3

    iget-object v4, v0, Lorg/apache/pdfbox/util/Matrix;->single:[F

    const/4 v5, 0x0

    if-ne v0, v2, :cond_1

    array-length v6, v4

    new-array v6, v6, [F

    array-length v7, v4

    invoke-static {v4, v5, v6, v5, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    move-object v4, v6

    :cond_1
    if-ne v1, v2, :cond_2

    iget-object v1, v1, Lorg/apache/pdfbox/util/Matrix;->single:[F

    array-length v3, v1

    new-array v3, v3, [F

    array-length v6, v1

    invoke-static {v1, v5, v3, v5, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    iget-object v1, v2, Lorg/apache/pdfbox/util/Matrix;->single:[F

    aget v6, v4, v5

    aget v7, v3, v5

    mul-float/2addr v6, v7

    const/4 v7, 0x1

    aget v8, v4, v7

    const/4 v9, 0x3

    aget v10, v3, v9

    mul-float v11, v8, v10

    add-float/2addr v6, v11

    const/4 v11, 0x2

    aget v12, v4, v11

    const/4 v13, 0x6

    aget v14, v3, v13

    mul-float v15, v12, v14

    add-float/2addr v6, v15

    aput v6, v1, v5

    aget v6, v4, v5

    aget v15, v3, v7

    mul-float/2addr v15, v6

    const/16 v16, 0x4

    aget v17, v3, v16

    mul-float v8, v8, v17

    add-float/2addr v15, v8

    const/4 v8, 0x7

    aget v18, v3, v8

    mul-float v19, v12, v18

    add-float v15, v15, v19

    aput v15, v1, v7

    aget v15, v3, v11

    mul-float/2addr v6, v15

    aget v15, v4, v7

    const/16 v19, 0x5

    aget v20, v3, v19

    mul-float v15, v15, v20

    add-float/2addr v6, v15

    const/16 v15, 0x8

    aget v21, v3, v15

    mul-float v12, v12, v21

    add-float/2addr v6, v12

    aput v6, v1, v11

    aget v6, v4, v9

    aget v5, v3, v5

    mul-float/2addr v6, v5

    aget v12, v4, v16

    mul-float/2addr v10, v12

    add-float/2addr v6, v10

    aget v10, v4, v19

    mul-float v22, v10, v14

    add-float v6, v6, v22

    aput v6, v1, v9

    aget v6, v4, v9

    aget v7, v3, v7

    mul-float v22, v6, v7

    mul-float v12, v12, v17

    add-float v22, v22, v12

    mul-float v12, v10, v18

    add-float v22, v22, v12

    aput v22, v1, v16

    aget v11, v3, v11

    mul-float/2addr v6, v11

    aget v12, v4, v16

    mul-float v12, v12, v20

    add-float/2addr v6, v12

    mul-float v10, v10, v21

    add-float/2addr v6, v10

    aput v6, v1, v19

    aget v6, v4, v13

    mul-float/2addr v6, v5

    aget v5, v4, v8

    aget v9, v3, v9

    mul-float/2addr v9, v5

    add-float/2addr v6, v9

    aget v9, v4, v15

    mul-float/2addr v14, v9

    add-float/2addr v6, v14

    aput v6, v1, v13

    aget v6, v4, v13

    mul-float/2addr v7, v6

    aget v10, v3, v16

    mul-float/2addr v5, v10

    add-float/2addr v7, v5

    mul-float v18, v18, v9

    add-float v7, v7, v18

    aput v7, v1, v8

    mul-float/2addr v6, v11

    aget v4, v4, v8

    aget v3, v3, v19

    mul-float/2addr v4, v3

    add-float/2addr v6, v4

    mul-float v9, v9, v21

    add-float/2addr v6, v9

    aput v6, v1, v15

    :cond_3
    return-object v2
.end method

.method public reset()V
    .locals 4
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    sget-object v0, Lorg/apache/pdfbox/util/Matrix;->DEFAULT_SINGLE:[F

    iget-object v1, p0, Lorg/apache/pdfbox/util/Matrix;->single:[F

    array-length v2, v0

    const/4 v3, 0x0

    invoke-static {v0, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-void
.end method

.method public rotate(D)V
    .locals 1

    const/4 v0, 0x0

    invoke-static {p1, p2, v0, v0}, Lorg/apache/pdfbox/util/Matrix;->getRotateInstance(DFF)Lorg/apache/pdfbox/util/Matrix;

    move-result-object p1

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/util/Matrix;->concatenate(Lorg/apache/pdfbox/util/Matrix;)V

    return-void
.end method

.method public scale(FF)V
    .locals 0

    invoke-static {p1, p2}, Lorg/apache/pdfbox/util/Matrix;->getScaleInstance(FF)Lorg/apache/pdfbox/util/Matrix;

    move-result-object p1

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/util/Matrix;->concatenate(Lorg/apache/pdfbox/util/Matrix;)V

    return-void
.end method

.method public setFromAffineTransform(Lorg/apache/pdfbox/util/awt/AffineTransform;)V
    .locals 3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    iget-object v0, p0, Lorg/apache/pdfbox/util/Matrix;->single:[F

    invoke-virtual {p1}, Lorg/apache/pdfbox/util/awt/AffineTransform;->getScaleX()D

    move-result-wide v1

    double-to-float v1, v1

    const/4 v2, 0x0

    aput v1, v0, v2

    iget-object v0, p0, Lorg/apache/pdfbox/util/Matrix;->single:[F

    invoke-virtual {p1}, Lorg/apache/pdfbox/util/awt/AffineTransform;->getShearY()D

    move-result-wide v1

    double-to-float v1, v1

    const/4 v2, 0x1

    aput v1, v0, v2

    iget-object v0, p0, Lorg/apache/pdfbox/util/Matrix;->single:[F

    invoke-virtual {p1}, Lorg/apache/pdfbox/util/awt/AffineTransform;->getShearX()D

    move-result-wide v1

    double-to-float v1, v1

    const/4 v2, 0x3

    aput v1, v0, v2

    iget-object v0, p0, Lorg/apache/pdfbox/util/Matrix;->single:[F

    invoke-virtual {p1}, Lorg/apache/pdfbox/util/awt/AffineTransform;->getScaleY()D

    move-result-wide v1

    double-to-float v1, v1

    const/4 v2, 0x4

    aput v1, v0, v2

    iget-object v0, p0, Lorg/apache/pdfbox/util/Matrix;->single:[F

    invoke-virtual {p1}, Lorg/apache/pdfbox/util/awt/AffineTransform;->getTranslateX()D

    move-result-wide v1

    double-to-float v1, v1

    const/4 v2, 0x6

    aput v1, v0, v2

    iget-object v0, p0, Lorg/apache/pdfbox/util/Matrix;->single:[F

    invoke-virtual {p1}, Lorg/apache/pdfbox/util/awt/AffineTransform;->getTranslateY()D

    move-result-wide v1

    double-to-float p1, v1

    const/4 v1, 0x7

    aput p1, v0, v1

    return-void
.end method

.method public setValue(IIF)V
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/util/Matrix;->single:[F

    mul-int/lit8 p1, p1, 0x3

    add-int/2addr p1, p2

    aput p3, v0, p1

    return-void
.end method

.method public toCOSArray()Lorg/apache/pdfbox/cos/COSArray;
    .locals 3

    new-instance v0, Lorg/apache/pdfbox/cos/COSArray;

    invoke-direct {v0}, Lorg/apache/pdfbox/cos/COSArray;-><init>()V

    new-instance v1, Lorg/apache/pdfbox/cos/COSFloat;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lorg/apache/pdfbox/cos/COSFloat;-><init>(F)V

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    new-instance v1, Lorg/apache/pdfbox/cos/COSFloat;

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-direct {v1, v2}, Lorg/apache/pdfbox/cos/COSFloat;-><init>(F)V

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    new-instance v1, Lorg/apache/pdfbox/cos/COSFloat;

    const/high16 v2, 0x40400000    # 3.0f

    invoke-direct {v1, v2}, Lorg/apache/pdfbox/cos/COSFloat;-><init>(F)V

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    new-instance v1, Lorg/apache/pdfbox/cos/COSFloat;

    const/high16 v2, 0x40800000    # 4.0f

    invoke-direct {v1, v2}, Lorg/apache/pdfbox/cos/COSFloat;-><init>(F)V

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    new-instance v1, Lorg/apache/pdfbox/cos/COSFloat;

    const/high16 v2, 0x40c00000    # 6.0f

    invoke-direct {v1, v2}, Lorg/apache/pdfbox/cos/COSFloat;-><init>(F)V

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    new-instance v1, Lorg/apache/pdfbox/cos/COSFloat;

    const/high16 v2, 0x40e00000    # 7.0f

    invoke-direct {v1, v2}, Lorg/apache/pdfbox/cos/COSFloat;-><init>(F)V

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    new-instance v0, Ljava/lang/StringBuffer;

    const-string v1, ""

    invoke-direct {v0, v1}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    const-string v1, "["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lorg/apache/pdfbox/util/Matrix;->single:[F

    const/4 v3, 0x0

    aget v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lorg/apache/pdfbox/util/Matrix;->single:[F

    const/4 v4, 0x1

    aget v3, v3, v4

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lorg/apache/pdfbox/util/Matrix;->single:[F

    const/4 v4, 0x3

    aget v3, v3, v4

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lorg/apache/pdfbox/util/Matrix;->single:[F

    const/4 v4, 0x4

    aget v3, v3, v4

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lorg/apache/pdfbox/util/Matrix;->single:[F

    const/4 v4, 0x6

    aget v3, v3, v4

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lorg/apache/pdfbox/util/Matrix;->single:[F

    const/4 v3, 0x7

    aget v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public transform(Lorg/apache/pdfbox/util/Vector;)Lorg/apache/pdfbox/util/Vector;
    .locals 8

    .line 1
    iget-object v0, p0, Lorg/apache/pdfbox/util/Matrix;->single:[F

    const/4 v1, 0x0

    aget v1, v0, v1

    const/4 v2, 0x1

    aget v2, v0, v2

    const/4 v3, 0x3

    aget v3, v0, v3

    const/4 v4, 0x4

    aget v4, v0, v4

    const/4 v5, 0x6

    aget v5, v0, v5

    const/4 v6, 0x7

    aget v0, v0, v6

    invoke-virtual {p1}, Lorg/apache/pdfbox/util/Vector;->getX()F

    move-result v6

    invoke-virtual {p1}, Lorg/apache/pdfbox/util/Vector;->getY()F

    move-result p1

    new-instance v7, Lorg/apache/pdfbox/util/Vector;

    mul-float/2addr v1, v6

    mul-float/2addr v3, p1

    add-float/2addr v1, v3

    add-float/2addr v1, v5

    mul-float/2addr v6, v2

    mul-float/2addr p1, v4

    add-float/2addr v6, p1

    add-float/2addr v6, v0

    invoke-direct {v7, v1, v6}, Lorg/apache/pdfbox/util/Vector;-><init>(FF)V

    return-object v7
.end method

.method public transform(Landroid/graphics/PointF;)V
    .locals 9

    .line 2
    iget v0, p1, Landroid/graphics/PointF;->x:F

    iget v1, p1, Landroid/graphics/PointF;->y:F

    iget-object v2, p0, Lorg/apache/pdfbox/util/Matrix;->single:[F

    const/4 v3, 0x0

    aget v3, v2, v3

    const/4 v4, 0x1

    aget v4, v2, v4

    const/4 v5, 0x3

    aget v5, v2, v5

    const/4 v6, 0x4

    aget v6, v2, v6

    const/4 v7, 0x6

    aget v7, v2, v7

    const/4 v8, 0x7

    aget v2, v2, v8

    mul-float/2addr v3, v0

    mul-float/2addr v5, v1

    add-float/2addr v3, v5

    add-float/2addr v3, v7

    mul-float/2addr v0, v4

    mul-float/2addr v1, v6

    add-float/2addr v0, v1

    add-float/2addr v0, v2

    invoke-virtual {p1, v3, v0}, Landroid/graphics/PointF;->set(FF)V

    return-void
.end method

.method public transformPoint(DD)Landroid/graphics/PointF;
    .locals 11

    iget-object v0, p0, Lorg/apache/pdfbox/util/Matrix;->single:[F

    const/4 v1, 0x0

    aget v1, v0, v1

    const/4 v2, 0x1

    aget v2, v0, v2

    const/4 v3, 0x3

    aget v3, v0, v3

    const/4 v4, 0x4

    aget v4, v0, v4

    const/4 v5, 0x6

    aget v5, v0, v5

    const/4 v6, 0x7

    aget v0, v0, v6

    new-instance v6, Landroid/graphics/PointF;

    float-to-double v7, v1

    mul-double/2addr v7, p1

    float-to-double v9, v3

    mul-double/2addr v9, p3

    add-double/2addr v7, v9

    float-to-double v9, v5

    add-double/2addr v7, v9

    double-to-float v1, v7

    float-to-double v2, v2

    mul-double/2addr p1, v2

    float-to-double v2, v4

    mul-double/2addr p3, v2

    add-double/2addr p1, p3

    float-to-double p3, v0

    add-double/2addr p1, p3

    double-to-float p1, p1

    invoke-direct {v6, v1, p1}, Landroid/graphics/PointF;-><init>(FF)V

    return-object v6
.end method

.method public translate(FF)V
    .locals 0

    .line 1
    invoke-static {p1, p2}, Lorg/apache/pdfbox/util/Matrix;->getTranslateInstance(FF)Lorg/apache/pdfbox/util/Matrix;

    move-result-object p1

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/util/Matrix;->concatenate(Lorg/apache/pdfbox/util/Matrix;)V

    return-void
.end method

.method public translate(Lorg/apache/pdfbox/util/Vector;)V
    .locals 1

    .line 2
    invoke-virtual {p1}, Lorg/apache/pdfbox/util/Vector;->getX()F

    move-result v0

    invoke-virtual {p1}, Lorg/apache/pdfbox/util/Vector;->getY()F

    move-result p1

    invoke-static {v0, p1}, Lorg/apache/pdfbox/util/Matrix;->getTranslateInstance(FF)Lorg/apache/pdfbox/util/Matrix;

    move-result-object p1

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/util/Matrix;->concatenate(Lorg/apache/pdfbox/util/Matrix;)V

    return-void
.end method

.class public Lorg/apache/pdfbox/util/PDFHighlighter;
.super Lorg/apache/pdfbox/text/PDFTextStripper;
.source "SourceFile"


# static fields
.field private static final ENCODING:Ljava/lang/String; = "UTF-16"


# instance fields
.field private highlighterOutput:Ljava/io/Writer;

.field private searchedWords:[Ljava/lang/String;

.field private textOS:Ljava/io/ByteArrayOutputStream;

.field private textWriter:Ljava/io/Writer;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lorg/apache/pdfbox/text/PDFTextStripper;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/pdfbox/util/PDFHighlighter;->highlighterOutput:Ljava/io/Writer;

    iput-object v0, p0, Lorg/apache/pdfbox/util/PDFHighlighter;->textOS:Ljava/io/ByteArrayOutputStream;

    iput-object v0, p0, Lorg/apache/pdfbox/util/PDFHighlighter;->textWriter:Ljava/io/Writer;

    const-string v0, ""

    invoke-super {p0, v0}, Lorg/apache/pdfbox/text/PDFTextStripper;->setLineSeparator(Ljava/lang/String;)V

    invoke-super {p0, v0}, Lorg/apache/pdfbox/text/PDFTextStripper;->setWordSeparator(Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-super {p0, v0}, Lorg/apache/pdfbox/text/PDFTextStripper;->setShouldSeparateByBeads(Z)V

    invoke-super {p0, v0}, Lorg/apache/pdfbox/text/PDFTextStripper;->setSuppressDuplicateOverlappingText(Z)V

    return-void
.end method

.method public static main([Ljava/lang/String;)V
    .locals 6

    new-instance v0, Lorg/apache/pdfbox/util/PDFHighlighter;

    invoke-direct {v0}, Lorg/apache/pdfbox/util/PDFHighlighter;-><init>()V

    const/4 v1, 0x0

    :try_start_0
    array-length v2, p0

    const/4 v3, 0x2

    if-ge v2, v3, :cond_0

    invoke-static {}, Lorg/apache/pdfbox/util/PDFHighlighter;->usage()V

    :cond_0
    array-length v2, p0

    const/4 v3, 0x1

    sub-int/2addr v2, v3

    new-array v4, v2, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static {p0, v3, v4, v5, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    new-instance v2, Ljava/io/File;

    aget-object p0, p0, v5

    invoke-direct {v2, p0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v2}, Lorg/apache/pdfbox/pdmodel/PDDocument;->load(Ljava/io/File;)Lorg/apache/pdfbox/pdmodel/PDDocument;

    move-result-object v1

    new-instance p0, Ljava/io/OutputStreamWriter;

    sget-object v2, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-direct {p0, v2}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;)V

    invoke-virtual {v0, v1, v4, p0}, Lorg/apache/pdfbox/util/PDFHighlighter;->generateXMLHighlight(Lorg/apache/pdfbox/pdmodel/PDDocument;[Ljava/lang/String;Ljava/io/Writer;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lorg/apache/pdfbox/pdmodel/PDDocument;->close()V

    :cond_1
    return-void

    :catchall_0
    move-exception p0

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lorg/apache/pdfbox/pdmodel/PDDocument;->close()V

    :cond_2
    throw p0
.end method

.method private static usage()V
    .locals 3

    sget-object v0, Ljava/lang/System;->err:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "usage: java "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-class v2, Lorg/apache/pdfbox/util/PDFHighlighter;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " <pdf file> word1 word2 word3 ..."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/System;->exit(I)V

    return-void
.end method


# virtual methods
.method public endPage(Lorg/apache/pdfbox/pdmodel/PDPage;)V
    .locals 9

    iget-object p1, p0, Lorg/apache/pdfbox/util/PDFHighlighter;->textWriter:Ljava/io/Writer;

    invoke-virtual {p1}, Ljava/io/Writer;->flush()V

    new-instance p1, Ljava/lang/String;

    iget-object v0, p0, Lorg/apache/pdfbox/util/PDFHighlighter;->textOS:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    const-string v1, "UTF-16"

    invoke-direct {p1, v0, v1}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    iget-object v0, p0, Lorg/apache/pdfbox/util/PDFHighlighter;->textOS:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->reset()V

    const/16 v0, 0x61

    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const-string v0, "a[0-9]{1,3}"

    const-string v1, "."

    invoke-virtual {p1, v0, v1}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    :cond_0
    iget-object v0, p0, Lorg/apache/pdfbox/util/PDFHighlighter;->searchedWords:[Ljava/lang/String;

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_2

    aget-object v3, v0, v2

    const/4 v4, 0x2

    invoke-static {v3, v4}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;I)Ljava/util/regex/Pattern;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v3

    :goto_1
    invoke-virtual {v3}, Ljava/util/regex/Matcher;->find()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {v3}, Ljava/util/regex/Matcher;->start()I

    move-result v4

    invoke-virtual {v3}, Ljava/util/regex/Matcher;->end()I

    move-result v5

    iget-object v6, p0, Lorg/apache/pdfbox/util/PDFHighlighter;->highlighterOutput:Ljava/io/Writer;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "    <loc pg="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/apache/pdfbox/text/PDFTextStripper;->getCurrentPageNo()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v8, " pos="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v8, " len="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sub-int/2addr v5, v4

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v4, ">\n"

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v6, v4}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    goto :goto_1

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    return-void
.end method

.method public generateXMLHighlight(Lorg/apache/pdfbox/pdmodel/PDDocument;Ljava/lang/String;Ljava/io/Writer;)V
    .locals 0

    .line 1
    filled-new-array {p2}, [Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p0, p1, p2, p3}, Lorg/apache/pdfbox/util/PDFHighlighter;->generateXMLHighlight(Lorg/apache/pdfbox/pdmodel/PDDocument;[Ljava/lang/String;Ljava/io/Writer;)V

    return-void
.end method

.method public generateXMLHighlight(Lorg/apache/pdfbox/pdmodel/PDDocument;[Ljava/lang/String;Ljava/io/Writer;)V
    .locals 1

    .line 2
    iput-object p3, p0, Lorg/apache/pdfbox/util/PDFHighlighter;->highlighterOutput:Ljava/io/Writer;

    iput-object p2, p0, Lorg/apache/pdfbox/util/PDFHighlighter;->searchedWords:[Ljava/lang/String;

    const-string p2, "<XML>\n<Body units=characters  version=2>\n<Highlight>\n"

    invoke-virtual {p3, p2}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    new-instance p2, Ljava/io/ByteArrayOutputStream;

    invoke-direct {p2}, Ljava/io/ByteArrayOutputStream;-><init>()V

    iput-object p2, p0, Lorg/apache/pdfbox/util/PDFHighlighter;->textOS:Ljava/io/ByteArrayOutputStream;

    new-instance p2, Ljava/io/OutputStreamWriter;

    iget-object p3, p0, Lorg/apache/pdfbox/util/PDFHighlighter;->textOS:Ljava/io/ByteArrayOutputStream;

    const-string v0, "UTF-16"

    invoke-direct {p2, p3, v0}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;Ljava/lang/String;)V

    iput-object p2, p0, Lorg/apache/pdfbox/util/PDFHighlighter;->textWriter:Ljava/io/Writer;

    invoke-virtual {p0, p1, p2}, Lorg/apache/pdfbox/text/PDFTextStripper;->writeText(Lorg/apache/pdfbox/pdmodel/PDDocument;Ljava/io/Writer;)V

    iget-object p1, p0, Lorg/apache/pdfbox/util/PDFHighlighter;->highlighterOutput:Ljava/io/Writer;

    const-string p2, "</Highlight>\n</Body>\n</XML>"

    invoke-virtual {p1, p2}, Ljava/io/Writer;->write(Ljava/lang/String;)V

    iget-object p1, p0, Lorg/apache/pdfbox/util/PDFHighlighter;->highlighterOutput:Ljava/io/Writer;

    invoke-virtual {p1}, Ljava/io/Writer;->flush()V

    return-void
.end method

.class public Lorg/apache/pdfbox/util/PDFBoxResourceLoader;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static ASSET_MANAGER:Landroid/content/res/AssetManager; = null

.field private static CONTEXT:Landroid/content/Context; = null

.field private static hasWarned:Z = false


# direct methods
.method public static constructor <clinit>()V
    .locals 0

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getStream(Ljava/lang/String;)Ljava/io/InputStream;
    .locals 1

    sget-object v0, Lorg/apache/pdfbox/util/PDFBoxResourceLoader;->ASSET_MANAGER:Landroid/content/res/AssetManager;

    invoke-virtual {v0, p0}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object p0

    return-object p0
.end method

.method public static init(Landroid/content/Context;)V
    .locals 1

    sget-object v0, Lorg/apache/pdfbox/util/PDFBoxResourceLoader;->CONTEXT:Landroid/content/Context;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object p0

    sput-object p0, Lorg/apache/pdfbox/util/PDFBoxResourceLoader;->CONTEXT:Landroid/content/Context;

    invoke-virtual {p0}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object p0

    sput-object p0, Lorg/apache/pdfbox/util/PDFBoxResourceLoader;->ASSET_MANAGER:Landroid/content/res/AssetManager;

    :cond_0
    return-void
.end method

.method public static isReady()Z
    .locals 3

    sget-object v0, Lorg/apache/pdfbox/util/PDFBoxResourceLoader;->ASSET_MANAGER:Landroid/content/res/AssetManager;

    const/4 v1, 0x1

    if-nez v0, :cond_0

    sget-boolean v0, Lorg/apache/pdfbox/util/PDFBoxResourceLoader;->hasWarned:Z

    if-nez v0, :cond_0

    const-string v0, "PdfBoxAndroid"

    const-string v2, "Call PDFBoxResourceLoader.init() first to decrease resource load time"

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    sput-boolean v1, Lorg/apache/pdfbox/util/PDFBoxResourceLoader;->hasWarned:Z

    :cond_0
    sget-object v0, Lorg/apache/pdfbox/util/PDFBoxResourceLoader;->ASSET_MANAGER:Landroid/content/res/AssetManager;

    if-eqz v0, :cond_1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

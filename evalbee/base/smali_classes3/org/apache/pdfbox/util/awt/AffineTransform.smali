.class public Lorg/apache/pdfbox/util/awt/AffineTransform;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private m00:D

.field private m01:D

.field private m02:D

.field private m10:D

.field private m11:D

.field private m12:D


# direct methods
.method public constructor <init>()V
    .locals 2

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    iput-wide v0, p0, Lorg/apache/pdfbox/util/awt/AffineTransform;->m11:D

    iput-wide v0, p0, Lorg/apache/pdfbox/util/awt/AffineTransform;->m00:D

    return-void
.end method

.method public constructor <init>(DDDDDD)V
    .locals 0

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lorg/apache/pdfbox/util/awt/AffineTransform;->m00:D

    iput-wide p3, p0, Lorg/apache/pdfbox/util/awt/AffineTransform;->m10:D

    iput-wide p5, p0, Lorg/apache/pdfbox/util/awt/AffineTransform;->m01:D

    iput-wide p7, p0, Lorg/apache/pdfbox/util/awt/AffineTransform;->m11:D

    iput-wide p9, p0, Lorg/apache/pdfbox/util/awt/AffineTransform;->m02:D

    iput-wide p11, p0, Lorg/apache/pdfbox/util/awt/AffineTransform;->m12:D

    return-void
.end method

.method public constructor <init>(Landroid/graphics/Matrix;)V
    .locals 3

    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0x9

    new-array v0, v0, [F

    invoke-virtual {p1, v0}, Landroid/graphics/Matrix;->getValues([F)V

    const/4 p1, 0x0

    aget p1, v0, p1

    float-to-double v1, p1

    iput-wide v1, p0, Lorg/apache/pdfbox/util/awt/AffineTransform;->m00:D

    const/4 p1, 0x1

    aget p1, v0, p1

    float-to-double v1, p1

    iput-wide v1, p0, Lorg/apache/pdfbox/util/awt/AffineTransform;->m01:D

    const/4 p1, 0x2

    aget p1, v0, p1

    float-to-double v1, p1

    iput-wide v1, p0, Lorg/apache/pdfbox/util/awt/AffineTransform;->m02:D

    const/4 p1, 0x3

    aget p1, v0, p1

    float-to-double v1, p1

    iput-wide v1, p0, Lorg/apache/pdfbox/util/awt/AffineTransform;->m10:D

    const/4 p1, 0x4

    aget p1, v0, p1

    float-to-double v1, p1

    iput-wide v1, p0, Lorg/apache/pdfbox/util/awt/AffineTransform;->m11:D

    const/4 p1, 0x5

    aget p1, v0, p1

    float-to-double v0, p1

    iput-wide v0, p0, Lorg/apache/pdfbox/util/awt/AffineTransform;->m12:D

    return-void
.end method

.method public constructor <init>(Lorg/apache/pdfbox/util/awt/AffineTransform;)V
    .locals 0

    .line 4
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/util/awt/AffineTransform;->setTransform(Lorg/apache/pdfbox/util/awt/AffineTransform;)V

    return-void
.end method

.method public constructor <init>([D)V
    .locals 2

    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    aget-wide v0, p1, v0

    iput-wide v0, p0, Lorg/apache/pdfbox/util/awt/AffineTransform;->m00:D

    const/4 v0, 0x1

    aget-wide v0, p1, v0

    iput-wide v0, p0, Lorg/apache/pdfbox/util/awt/AffineTransform;->m10:D

    const/4 v0, 0x2

    aget-wide v0, p1, v0

    iput-wide v0, p0, Lorg/apache/pdfbox/util/awt/AffineTransform;->m01:D

    const/4 v0, 0x3

    aget-wide v0, p1, v0

    iput-wide v0, p0, Lorg/apache/pdfbox/util/awt/AffineTransform;->m11:D

    array-length v0, p1

    const/4 v1, 0x6

    if-lt v0, v1, :cond_0

    const/4 v0, 0x4

    aget-wide v0, p1, v0

    iput-wide v0, p0, Lorg/apache/pdfbox/util/awt/AffineTransform;->m02:D

    const/4 v0, 0x5

    aget-wide v0, p1, v0

    iput-wide v0, p0, Lorg/apache/pdfbox/util/awt/AffineTransform;->m12:D

    :cond_0
    return-void
.end method

.method public static getScaleInstance(DD)Lorg/apache/pdfbox/util/awt/AffineTransform;
    .locals 1

    new-instance v0, Lorg/apache/pdfbox/util/awt/AffineTransform;

    invoke-direct {v0}, Lorg/apache/pdfbox/util/awt/AffineTransform;-><init>()V

    invoke-virtual {v0, p0, p1, p2, p3}, Lorg/apache/pdfbox/util/awt/AffineTransform;->setToScale(DD)V

    return-object v0
.end method

.method public static getTranslateInstance(DD)Lorg/apache/pdfbox/util/awt/AffineTransform;
    .locals 1

    new-instance v0, Lorg/apache/pdfbox/util/awt/AffineTransform;

    invoke-direct {v0}, Lorg/apache/pdfbox/util/awt/AffineTransform;-><init>()V

    iput-wide p0, v0, Lorg/apache/pdfbox/util/awt/AffineTransform;->m02:D

    iput-wide p2, v0, Lorg/apache/pdfbox/util/awt/AffineTransform;->m12:D

    return-object v0
.end method


# virtual methods
.method public getMatrix([D)V
    .locals 3

    const/4 v0, 0x0

    iget-wide v1, p0, Lorg/apache/pdfbox/util/awt/AffineTransform;->m00:D

    aput-wide v1, p1, v0

    const/4 v0, 0x1

    iget-wide v1, p0, Lorg/apache/pdfbox/util/awt/AffineTransform;->m10:D

    aput-wide v1, p1, v0

    const/4 v0, 0x2

    iget-wide v1, p0, Lorg/apache/pdfbox/util/awt/AffineTransform;->m01:D

    aput-wide v1, p1, v0

    const/4 v0, 0x3

    iget-wide v1, p0, Lorg/apache/pdfbox/util/awt/AffineTransform;->m11:D

    aput-wide v1, p1, v0

    const/4 v0, 0x4

    iget-wide v1, p0, Lorg/apache/pdfbox/util/awt/AffineTransform;->m02:D

    aput-wide v1, p1, v0

    const/4 v0, 0x5

    iget-wide v1, p0, Lorg/apache/pdfbox/util/awt/AffineTransform;->m12:D

    aput-wide v1, p1, v0

    return-void
.end method

.method public getScaleX()D
    .locals 2

    iget-wide v0, p0, Lorg/apache/pdfbox/util/awt/AffineTransform;->m00:D

    return-wide v0
.end method

.method public getScaleY()D
    .locals 2

    iget-wide v0, p0, Lorg/apache/pdfbox/util/awt/AffineTransform;->m11:D

    return-wide v0
.end method

.method public getShearX()D
    .locals 2

    iget-wide v0, p0, Lorg/apache/pdfbox/util/awt/AffineTransform;->m01:D

    return-wide v0
.end method

.method public getShearY()D
    .locals 2

    iget-wide v0, p0, Lorg/apache/pdfbox/util/awt/AffineTransform;->m10:D

    return-wide v0
.end method

.method public getTranslateX()D
    .locals 2

    iget-wide v0, p0, Lorg/apache/pdfbox/util/awt/AffineTransform;->m02:D

    return-wide v0
.end method

.method public getTranslateY()D
    .locals 2

    iget-wide v0, p0, Lorg/apache/pdfbox/util/awt/AffineTransform;->m12:D

    return-wide v0
.end method

.method public isIdentity()Z
    .locals 6

    iget-wide v0, p0, Lorg/apache/pdfbox/util/awt/AffineTransform;->m00:D

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    cmpl-double v0, v0, v2

    if-nez v0, :cond_0

    iget-wide v0, p0, Lorg/apache/pdfbox/util/awt/AffineTransform;->m01:D

    const-wide/16 v4, 0x0

    cmpl-double v0, v0, v4

    if-nez v0, :cond_0

    iget-wide v0, p0, Lorg/apache/pdfbox/util/awt/AffineTransform;->m02:D

    cmpl-double v0, v0, v4

    if-nez v0, :cond_0

    iget-wide v0, p0, Lorg/apache/pdfbox/util/awt/AffineTransform;->m10:D

    cmpl-double v0, v0, v4

    if-nez v0, :cond_0

    iget-wide v0, p0, Lorg/apache/pdfbox/util/awt/AffineTransform;->m11:D

    cmpl-double v0, v0, v2

    if-nez v0, :cond_0

    iget-wide v0, p0, Lorg/apache/pdfbox/util/awt/AffineTransform;->m12:D

    cmpl-double v0, v0, v4

    if-nez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public rotate(D)V
    .locals 15

    move-object v0, p0

    invoke-static/range {p1 .. p2}, Ljava/lang/Math;->cos(D)D

    move-result-wide v1

    invoke-static/range {p1 .. p2}, Ljava/lang/Math;->sin(D)D

    move-result-wide v3

    iget-wide v5, v0, Lorg/apache/pdfbox/util/awt/AffineTransform;->m00:D

    mul-double v7, v5, v1

    iget-wide v9, v0, Lorg/apache/pdfbox/util/awt/AffineTransform;->m01:D

    mul-double v11, v9, v3

    add-double/2addr v7, v11

    neg-double v11, v3

    mul-double/2addr v5, v11

    mul-double/2addr v9, v1

    add-double/2addr v5, v9

    iget-wide v9, v0, Lorg/apache/pdfbox/util/awt/AffineTransform;->m10:D

    mul-double v13, v9, v1

    move-wide/from16 p1, v5

    iget-wide v5, v0, Lorg/apache/pdfbox/util/awt/AffineTransform;->m11:D

    mul-double/2addr v3, v5

    add-double/2addr v13, v3

    mul-double/2addr v9, v11

    mul-double/2addr v5, v1

    add-double/2addr v9, v5

    iput-wide v7, v0, Lorg/apache/pdfbox/util/awt/AffineTransform;->m00:D

    move-wide/from16 v5, p1

    iput-wide v5, v0, Lorg/apache/pdfbox/util/awt/AffineTransform;->m01:D

    iput-wide v13, v0, Lorg/apache/pdfbox/util/awt/AffineTransform;->m10:D

    iput-wide v9, v0, Lorg/apache/pdfbox/util/awt/AffineTransform;->m11:D

    return-void
.end method

.method public scale(DD)V
    .locals 2

    iget-wide v0, p0, Lorg/apache/pdfbox/util/awt/AffineTransform;->m00:D

    mul-double/2addr v0, p1

    iput-wide v0, p0, Lorg/apache/pdfbox/util/awt/AffineTransform;->m00:D

    iget-wide v0, p0, Lorg/apache/pdfbox/util/awt/AffineTransform;->m01:D

    mul-double/2addr v0, p3

    iput-wide v0, p0, Lorg/apache/pdfbox/util/awt/AffineTransform;->m01:D

    iget-wide v0, p0, Lorg/apache/pdfbox/util/awt/AffineTransform;->m10:D

    mul-double/2addr v0, p1

    iput-wide v0, p0, Lorg/apache/pdfbox/util/awt/AffineTransform;->m10:D

    iget-wide p1, p0, Lorg/apache/pdfbox/util/awt/AffineTransform;->m11:D

    mul-double/2addr p1, p3

    iput-wide p1, p0, Lorg/apache/pdfbox/util/awt/AffineTransform;->m11:D

    return-void
.end method

.method public setToScale(DD)V
    .locals 0

    iput-wide p1, p0, Lorg/apache/pdfbox/util/awt/AffineTransform;->m00:D

    const-wide/16 p1, 0x0

    iput-wide p1, p0, Lorg/apache/pdfbox/util/awt/AffineTransform;->m12:D

    iput-wide p1, p0, Lorg/apache/pdfbox/util/awt/AffineTransform;->m10:D

    iput-wide p1, p0, Lorg/apache/pdfbox/util/awt/AffineTransform;->m02:D

    iput-wide p1, p0, Lorg/apache/pdfbox/util/awt/AffineTransform;->m01:D

    iput-wide p3, p0, Lorg/apache/pdfbox/util/awt/AffineTransform;->m11:D

    return-void
.end method

.method public setTransform(Lorg/apache/pdfbox/util/awt/AffineTransform;)V
    .locals 2

    iget-wide v0, p1, Lorg/apache/pdfbox/util/awt/AffineTransform;->m00:D

    iput-wide v0, p0, Lorg/apache/pdfbox/util/awt/AffineTransform;->m00:D

    iget-wide v0, p1, Lorg/apache/pdfbox/util/awt/AffineTransform;->m01:D

    iput-wide v0, p0, Lorg/apache/pdfbox/util/awt/AffineTransform;->m01:D

    iget-wide v0, p1, Lorg/apache/pdfbox/util/awt/AffineTransform;->m02:D

    iput-wide v0, p0, Lorg/apache/pdfbox/util/awt/AffineTransform;->m02:D

    iget-wide v0, p1, Lorg/apache/pdfbox/util/awt/AffineTransform;->m10:D

    iput-wide v0, p0, Lorg/apache/pdfbox/util/awt/AffineTransform;->m10:D

    iget-wide v0, p1, Lorg/apache/pdfbox/util/awt/AffineTransform;->m11:D

    iput-wide v0, p0, Lorg/apache/pdfbox/util/awt/AffineTransform;->m11:D

    iget-wide v0, p1, Lorg/apache/pdfbox/util/awt/AffineTransform;->m12:D

    iput-wide v0, p0, Lorg/apache/pdfbox/util/awt/AffineTransform;->m12:D

    return-void
.end method

.method public toMatrix()Landroid/graphics/Matrix;
    .locals 4

    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    const/16 v1, 0x9

    new-array v1, v1, [F

    iget-wide v2, p0, Lorg/apache/pdfbox/util/awt/AffineTransform;->m00:D

    double-to-float v2, v2

    const/4 v3, 0x0

    aput v2, v1, v3

    iget-wide v2, p0, Lorg/apache/pdfbox/util/awt/AffineTransform;->m01:D

    double-to-float v2, v2

    const/4 v3, 0x1

    aput v2, v1, v3

    iget-wide v2, p0, Lorg/apache/pdfbox/util/awt/AffineTransform;->m02:D

    double-to-float v2, v2

    const/4 v3, 0x2

    aput v2, v1, v3

    iget-wide v2, p0, Lorg/apache/pdfbox/util/awt/AffineTransform;->m10:D

    double-to-float v2, v2

    const/4 v3, 0x3

    aput v2, v1, v3

    iget-wide v2, p0, Lorg/apache/pdfbox/util/awt/AffineTransform;->m11:D

    double-to-float v2, v2

    const/4 v3, 0x4

    aput v2, v1, v3

    iget-wide v2, p0, Lorg/apache/pdfbox/util/awt/AffineTransform;->m12:D

    double-to-float v2, v2

    const/4 v3, 0x5

    aput v2, v1, v3

    const/4 v2, 0x6

    const/4 v3, 0x0

    aput v3, v1, v2

    const/4 v2, 0x7

    aput v3, v1, v2

    const/16 v2, 0x8

    const/high16 v3, 0x3f800000    # 1.0f

    aput v3, v1, v2

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->setValues([F)V

    return-object v0
.end method

.method public transform(Landroid/graphics/PointF;Landroid/graphics/PointF;)Landroid/graphics/PointF;
    .locals 8

    .line 1
    if-nez p2, :cond_0

    new-instance p2, Landroid/graphics/PointF;

    invoke-direct {p2}, Landroid/graphics/PointF;-><init>()V

    :cond_0
    iget v0, p1, Landroid/graphics/PointF;->x:F

    float-to-double v0, v0

    iget p1, p1, Landroid/graphics/PointF;->y:F

    float-to-double v2, p1

    iget-wide v4, p0, Lorg/apache/pdfbox/util/awt/AffineTransform;->m00:D

    mul-double/2addr v4, v0

    iget-wide v6, p0, Lorg/apache/pdfbox/util/awt/AffineTransform;->m01:D

    mul-double/2addr v6, v2

    add-double/2addr v4, v6

    iget-wide v6, p0, Lorg/apache/pdfbox/util/awt/AffineTransform;->m02:D

    add-double/2addr v4, v6

    iget-wide v6, p0, Lorg/apache/pdfbox/util/awt/AffineTransform;->m10:D

    mul-double/2addr v6, v0

    iget-wide v0, p0, Lorg/apache/pdfbox/util/awt/AffineTransform;->m11:D

    mul-double/2addr v0, v2

    add-double/2addr v6, v0

    iget-wide v0, p0, Lorg/apache/pdfbox/util/awt/AffineTransform;->m12:D

    add-double/2addr v6, v0

    double-to-float p1, v4

    double-to-float v0, v6

    invoke-virtual {p2, p1, v0}, Landroid/graphics/PointF;->set(FF)V

    return-object p2
.end method

.method public transform([DI[DII)V
    .locals 9

    .line 2
    if-ne p1, p3, :cond_0

    if-le p4, p2, :cond_0

    const/4 v0, 0x1

    if-le p5, v0, :cond_0

    mul-int/lit8 v0, p5, 0x2

    add-int v1, p2, v0

    if-le v1, p4, :cond_0

    new-array v1, v0, [D

    const/4 v2, 0x0

    invoke-static {p1, p2, v1, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    move-object p1, v1

    :cond_0
    :goto_0
    add-int/lit8 p5, p5, -0x1

    if-ltz p5, :cond_1

    add-int/lit8 v0, p2, 0x1

    aget-wide v1, p1, p2

    add-int/lit8 p2, v0, 0x1

    aget-wide v3, p1, v0

    add-int/lit8 v0, p4, 0x1

    iget-wide v5, p0, Lorg/apache/pdfbox/util/awt/AffineTransform;->m00:D

    mul-double/2addr v5, v1

    iget-wide v7, p0, Lorg/apache/pdfbox/util/awt/AffineTransform;->m01:D

    mul-double/2addr v7, v3

    add-double/2addr v5, v7

    iget-wide v7, p0, Lorg/apache/pdfbox/util/awt/AffineTransform;->m02:D

    add-double/2addr v5, v7

    aput-wide v5, p3, p4

    add-int/lit8 p4, v0, 0x1

    iget-wide v5, p0, Lorg/apache/pdfbox/util/awt/AffineTransform;->m10:D

    mul-double/2addr v5, v1

    iget-wide v1, p0, Lorg/apache/pdfbox/util/awt/AffineTransform;->m11:D

    mul-double/2addr v1, v3

    add-double/2addr v5, v1

    iget-wide v1, p0, Lorg/apache/pdfbox/util/awt/AffineTransform;->m12:D

    add-double/2addr v5, v1

    aput-wide v5, p3, v0

    goto :goto_0

    :cond_1
    return-void
.end method

.method public transform([FI[FII)V
    .locals 15

    .line 3
    move-object v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p2

    move-object/from16 v3, p3

    move/from16 v4, p4

    move/from16 v5, p5

    if-ne v1, v3, :cond_0

    if-le v4, v2, :cond_0

    const/4 v6, 0x1

    if-le v5, v6, :cond_0

    mul-int/lit8 v6, v5, 0x2

    add-int v7, v2, v6

    if-le v7, v4, :cond_0

    new-array v7, v6, [F

    const/4 v8, 0x0

    invoke-static {v1, v2, v7, v8, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    move-object v1, v7

    :cond_0
    :goto_0
    add-int/lit8 v5, v5, -0x1

    if-ltz v5, :cond_1

    add-int/lit8 v6, v2, 0x1

    aget v2, v1, v2

    add-int/lit8 v7, v6, 0x1

    aget v6, v1, v6

    add-int/lit8 v8, v4, 0x1

    iget-wide v9, v0, Lorg/apache/pdfbox/util/awt/AffineTransform;->m00:D

    float-to-double v11, v2

    mul-double/2addr v9, v11

    iget-wide v13, v0, Lorg/apache/pdfbox/util/awt/AffineTransform;->m01:D

    move-object/from16 p1, v1

    float-to-double v1, v6

    mul-double/2addr v13, v1

    add-double/2addr v9, v13

    iget-wide v13, v0, Lorg/apache/pdfbox/util/awt/AffineTransform;->m02:D

    add-double/2addr v9, v13

    double-to-float v6, v9

    aput v6, v3, v4

    add-int/lit8 v4, v8, 0x1

    iget-wide v9, v0, Lorg/apache/pdfbox/util/awt/AffineTransform;->m10:D

    mul-double/2addr v9, v11

    iget-wide v11, v0, Lorg/apache/pdfbox/util/awt/AffineTransform;->m11:D

    mul-double/2addr v11, v1

    add-double/2addr v9, v11

    iget-wide v1, v0, Lorg/apache/pdfbox/util/awt/AffineTransform;->m12:D

    add-double/2addr v9, v1

    double-to-float v1, v9

    aput v1, v3, v8

    move-object/from16 v1, p1

    move v2, v7

    goto :goto_0

    :cond_1
    return-void
.end method

.method public transform([F[F)[F
    .locals 11

    .line 4
    if-nez p2, :cond_0

    const/4 p2, 0x2

    new-array p2, p2, [F

    :cond_0
    const/4 v0, 0x0

    aget v1, p1, v0

    const/4 v2, 0x1

    aget p1, p1, v2

    iget-wide v3, p0, Lorg/apache/pdfbox/util/awt/AffineTransform;->m00:D

    float-to-double v5, v1

    mul-double/2addr v3, v5

    iget-wide v7, p0, Lorg/apache/pdfbox/util/awt/AffineTransform;->m01:D

    float-to-double v9, p1

    mul-double/2addr v7, v9

    add-double/2addr v3, v7

    iget-wide v7, p0, Lorg/apache/pdfbox/util/awt/AffineTransform;->m02:D

    add-double/2addr v3, v7

    double-to-float p1, v3

    iget-wide v3, p0, Lorg/apache/pdfbox/util/awt/AffineTransform;->m10:D

    mul-double/2addr v3, v5

    iget-wide v5, p0, Lorg/apache/pdfbox/util/awt/AffineTransform;->m11:D

    mul-double/2addr v5, v9

    add-double/2addr v3, v5

    iget-wide v5, p0, Lorg/apache/pdfbox/util/awt/AffineTransform;->m12:D

    add-double/2addr v3, v5

    double-to-float v1, v3

    aput p1, p2, v0

    aput v1, p2, v2

    return-object p2
.end method

.method public translate(DD)V
    .locals 6

    iget-wide v0, p0, Lorg/apache/pdfbox/util/awt/AffineTransform;->m02:D

    iget-wide v2, p0, Lorg/apache/pdfbox/util/awt/AffineTransform;->m00:D

    mul-double/2addr v2, p1

    iget-wide v4, p0, Lorg/apache/pdfbox/util/awt/AffineTransform;->m01:D

    mul-double/2addr v4, p3

    add-double/2addr v2, v4

    add-double/2addr v0, v2

    iput-wide v0, p0, Lorg/apache/pdfbox/util/awt/AffineTransform;->m02:D

    iget-wide v0, p0, Lorg/apache/pdfbox/util/awt/AffineTransform;->m12:D

    iget-wide v2, p0, Lorg/apache/pdfbox/util/awt/AffineTransform;->m10:D

    mul-double/2addr p1, v2

    iget-wide v2, p0, Lorg/apache/pdfbox/util/awt/AffineTransform;->m11:D

    mul-double/2addr p3, v2

    add-double/2addr p1, p3

    add-double/2addr v0, p1

    iput-wide v0, p0, Lorg/apache/pdfbox/util/awt/AffineTransform;->m12:D

    return-void
.end method

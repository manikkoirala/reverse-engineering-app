.class public Lorg/apache/pdfbox/util/awt/AWTColor;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final BLACK:Lorg/apache/pdfbox/util/awt/AWTColor;

.field public static final BLUE:Lorg/apache/pdfbox/util/awt/AWTColor;

.field public static final CYAN:Lorg/apache/pdfbox/util/awt/AWTColor;

.field public static final DARK_GRAY:Lorg/apache/pdfbox/util/awt/AWTColor;

.field public static final GRAY:Lorg/apache/pdfbox/util/awt/AWTColor;

.field public static final GREEN:Lorg/apache/pdfbox/util/awt/AWTColor;

.field public static final LIGHT_GRAY:Lorg/apache/pdfbox/util/awt/AWTColor;

.field public static final MAGENTA:Lorg/apache/pdfbox/util/awt/AWTColor;

.field public static final ORANGE:Lorg/apache/pdfbox/util/awt/AWTColor;

.field public static final PINK:Lorg/apache/pdfbox/util/awt/AWTColor;

.field public static final RED:Lorg/apache/pdfbox/util/awt/AWTColor;

.field public static final WHITE:Lorg/apache/pdfbox/util/awt/AWTColor;

.field public static final YELLOW:Lorg/apache/pdfbox/util/awt/AWTColor;

.field public static final black:Lorg/apache/pdfbox/util/awt/AWTColor;

.field public static final blue:Lorg/apache/pdfbox/util/awt/AWTColor;

.field public static final cyan:Lorg/apache/pdfbox/util/awt/AWTColor;

.field public static final darkGray:Lorg/apache/pdfbox/util/awt/AWTColor;

.field public static final gray:Lorg/apache/pdfbox/util/awt/AWTColor;

.field public static final green:Lorg/apache/pdfbox/util/awt/AWTColor;

.field public static final lightGray:Lorg/apache/pdfbox/util/awt/AWTColor;

.field public static final magenta:Lorg/apache/pdfbox/util/awt/AWTColor;

.field public static final orange:Lorg/apache/pdfbox/util/awt/AWTColor;

.field public static final pink:Lorg/apache/pdfbox/util/awt/AWTColor;

.field public static final red:Lorg/apache/pdfbox/util/awt/AWTColor;

.field public static final white:Lorg/apache/pdfbox/util/awt/AWTColor;

.field public static final yellow:Lorg/apache/pdfbox/util/awt/AWTColor;


# instance fields
.field public color:I


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    new-instance v0, Lorg/apache/pdfbox/util/awt/AWTColor;

    const/16 v1, 0xff

    invoke-direct {v0, v1, v1, v1}, Lorg/apache/pdfbox/util/awt/AWTColor;-><init>(III)V

    sput-object v0, Lorg/apache/pdfbox/util/awt/AWTColor;->white:Lorg/apache/pdfbox/util/awt/AWTColor;

    sput-object v0, Lorg/apache/pdfbox/util/awt/AWTColor;->WHITE:Lorg/apache/pdfbox/util/awt/AWTColor;

    new-instance v0, Lorg/apache/pdfbox/util/awt/AWTColor;

    const/16 v2, 0xc0

    invoke-direct {v0, v2, v2, v2}, Lorg/apache/pdfbox/util/awt/AWTColor;-><init>(III)V

    sput-object v0, Lorg/apache/pdfbox/util/awt/AWTColor;->lightGray:Lorg/apache/pdfbox/util/awt/AWTColor;

    sput-object v0, Lorg/apache/pdfbox/util/awt/AWTColor;->LIGHT_GRAY:Lorg/apache/pdfbox/util/awt/AWTColor;

    new-instance v0, Lorg/apache/pdfbox/util/awt/AWTColor;

    const/16 v2, 0x80

    invoke-direct {v0, v2, v2, v2}, Lorg/apache/pdfbox/util/awt/AWTColor;-><init>(III)V

    sput-object v0, Lorg/apache/pdfbox/util/awt/AWTColor;->gray:Lorg/apache/pdfbox/util/awt/AWTColor;

    sput-object v0, Lorg/apache/pdfbox/util/awt/AWTColor;->GRAY:Lorg/apache/pdfbox/util/awt/AWTColor;

    new-instance v0, Lorg/apache/pdfbox/util/awt/AWTColor;

    const/16 v2, 0x40

    invoke-direct {v0, v2, v2, v2}, Lorg/apache/pdfbox/util/awt/AWTColor;-><init>(III)V

    sput-object v0, Lorg/apache/pdfbox/util/awt/AWTColor;->darkGray:Lorg/apache/pdfbox/util/awt/AWTColor;

    sput-object v0, Lorg/apache/pdfbox/util/awt/AWTColor;->DARK_GRAY:Lorg/apache/pdfbox/util/awt/AWTColor;

    new-instance v0, Lorg/apache/pdfbox/util/awt/AWTColor;

    const/4 v2, 0x0

    invoke-direct {v0, v2, v2, v2}, Lorg/apache/pdfbox/util/awt/AWTColor;-><init>(III)V

    sput-object v0, Lorg/apache/pdfbox/util/awt/AWTColor;->black:Lorg/apache/pdfbox/util/awt/AWTColor;

    sput-object v0, Lorg/apache/pdfbox/util/awt/AWTColor;->BLACK:Lorg/apache/pdfbox/util/awt/AWTColor;

    new-instance v0, Lorg/apache/pdfbox/util/awt/AWTColor;

    invoke-direct {v0, v1, v2, v2}, Lorg/apache/pdfbox/util/awt/AWTColor;-><init>(III)V

    sput-object v0, Lorg/apache/pdfbox/util/awt/AWTColor;->red:Lorg/apache/pdfbox/util/awt/AWTColor;

    sput-object v0, Lorg/apache/pdfbox/util/awt/AWTColor;->RED:Lorg/apache/pdfbox/util/awt/AWTColor;

    new-instance v0, Lorg/apache/pdfbox/util/awt/AWTColor;

    const/16 v3, 0xaf

    invoke-direct {v0, v1, v3, v3}, Lorg/apache/pdfbox/util/awt/AWTColor;-><init>(III)V

    sput-object v0, Lorg/apache/pdfbox/util/awt/AWTColor;->pink:Lorg/apache/pdfbox/util/awt/AWTColor;

    sput-object v0, Lorg/apache/pdfbox/util/awt/AWTColor;->PINK:Lorg/apache/pdfbox/util/awt/AWTColor;

    new-instance v0, Lorg/apache/pdfbox/util/awt/AWTColor;

    const/16 v3, 0xc8

    invoke-direct {v0, v1, v3, v2}, Lorg/apache/pdfbox/util/awt/AWTColor;-><init>(III)V

    sput-object v0, Lorg/apache/pdfbox/util/awt/AWTColor;->orange:Lorg/apache/pdfbox/util/awt/AWTColor;

    sput-object v0, Lorg/apache/pdfbox/util/awt/AWTColor;->ORANGE:Lorg/apache/pdfbox/util/awt/AWTColor;

    new-instance v0, Lorg/apache/pdfbox/util/awt/AWTColor;

    invoke-direct {v0, v1, v1, v2}, Lorg/apache/pdfbox/util/awt/AWTColor;-><init>(III)V

    sput-object v0, Lorg/apache/pdfbox/util/awt/AWTColor;->yellow:Lorg/apache/pdfbox/util/awt/AWTColor;

    sput-object v0, Lorg/apache/pdfbox/util/awt/AWTColor;->YELLOW:Lorg/apache/pdfbox/util/awt/AWTColor;

    new-instance v0, Lorg/apache/pdfbox/util/awt/AWTColor;

    invoke-direct {v0, v2, v1, v2}, Lorg/apache/pdfbox/util/awt/AWTColor;-><init>(III)V

    sput-object v0, Lorg/apache/pdfbox/util/awt/AWTColor;->green:Lorg/apache/pdfbox/util/awt/AWTColor;

    sput-object v0, Lorg/apache/pdfbox/util/awt/AWTColor;->GREEN:Lorg/apache/pdfbox/util/awt/AWTColor;

    new-instance v0, Lorg/apache/pdfbox/util/awt/AWTColor;

    invoke-direct {v0, v1, v2, v1}, Lorg/apache/pdfbox/util/awt/AWTColor;-><init>(III)V

    sput-object v0, Lorg/apache/pdfbox/util/awt/AWTColor;->magenta:Lorg/apache/pdfbox/util/awt/AWTColor;

    sput-object v0, Lorg/apache/pdfbox/util/awt/AWTColor;->MAGENTA:Lorg/apache/pdfbox/util/awt/AWTColor;

    new-instance v0, Lorg/apache/pdfbox/util/awt/AWTColor;

    invoke-direct {v0, v2, v1, v1}, Lorg/apache/pdfbox/util/awt/AWTColor;-><init>(III)V

    sput-object v0, Lorg/apache/pdfbox/util/awt/AWTColor;->cyan:Lorg/apache/pdfbox/util/awt/AWTColor;

    sput-object v0, Lorg/apache/pdfbox/util/awt/AWTColor;->CYAN:Lorg/apache/pdfbox/util/awt/AWTColor;

    new-instance v0, Lorg/apache/pdfbox/util/awt/AWTColor;

    invoke-direct {v0, v2, v2, v1}, Lorg/apache/pdfbox/util/awt/AWTColor;-><init>(III)V

    sput-object v0, Lorg/apache/pdfbox/util/awt/AWTColor;->blue:Lorg/apache/pdfbox/util/awt/AWTColor;

    sput-object v0, Lorg/apache/pdfbox/util/awt/AWTColor;->BLUE:Lorg/apache/pdfbox/util/awt/AWTColor;

    return-void
.end method

.method public constructor <init>(I)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lorg/apache/pdfbox/util/awt/AWTColor;->color:I

    return-void
.end method

.method public constructor <init>(III)V
    .locals 1

    .line 2
    const/16 v0, 0xff

    invoke-direct {p0, p1, p2, p3, v0}, Lorg/apache/pdfbox/util/awt/AWTColor;-><init>(IIII)V

    return-void
.end method

.method public constructor <init>(IIII)V
    .locals 0

    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p4, p1, p2, p3}, Landroid/graphics/Color;->argb(IIII)I

    move-result p1

    iput p1, p0, Lorg/apache/pdfbox/util/awt/AWTColor;->color:I

    return-void
.end method


# virtual methods
.method public getAlpha()I
    .locals 1

    iget v0, p0, Lorg/apache/pdfbox/util/awt/AWTColor;->color:I

    invoke-static {v0}, Landroid/graphics/Color;->alpha(I)I

    move-result v0

    return v0
.end method

.method public getBlue()I
    .locals 1

    iget v0, p0, Lorg/apache/pdfbox/util/awt/AWTColor;->color:I

    invoke-static {v0}, Landroid/graphics/Color;->blue(I)I

    move-result v0

    return v0
.end method

.method public getGreen()I
    .locals 1

    iget v0, p0, Lorg/apache/pdfbox/util/awt/AWTColor;->color:I

    invoke-static {v0}, Landroid/graphics/Color;->green(I)I

    move-result v0

    return v0
.end method

.method public getRed()I
    .locals 1

    iget v0, p0, Lorg/apache/pdfbox/util/awt/AWTColor;->color:I

    invoke-static {v0}, Landroid/graphics/Color;->red(I)I

    move-result v0

    return v0
.end method

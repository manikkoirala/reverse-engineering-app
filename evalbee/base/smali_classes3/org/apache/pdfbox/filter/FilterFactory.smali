.class public final Lorg/apache/pdfbox/filter/FilterFactory;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final INSTANCE:Lorg/apache/pdfbox/filter/FilterFactory;


# instance fields
.field private final filters:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lorg/apache/pdfbox/cos/COSName;",
            "Lorg/apache/pdfbox/filter/Filter;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    new-instance v0, Lorg/apache/pdfbox/filter/FilterFactory;

    invoke-direct {v0}, Lorg/apache/pdfbox/filter/FilterFactory;-><init>()V

    sput-object v0, Lorg/apache/pdfbox/filter/FilterFactory;->INSTANCE:Lorg/apache/pdfbox/filter/FilterFactory;

    return-void
.end method

.method private constructor <init>()V
    .locals 10

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/pdfbox/filter/FilterFactory;->filters:Ljava/util/Map;

    new-instance v1, Lorg/apache/pdfbox/filter/FlateFilter;

    invoke-direct {v1}, Lorg/apache/pdfbox/filter/FlateFilter;-><init>()V

    new-instance v2, Lorg/apache/pdfbox/filter/DCTFilter;

    invoke-direct {v2}, Lorg/apache/pdfbox/filter/DCTFilter;-><init>()V

    new-instance v3, Lorg/apache/pdfbox/filter/CCITTFaxFilter;

    invoke-direct {v3}, Lorg/apache/pdfbox/filter/CCITTFaxFilter;-><init>()V

    new-instance v4, Lorg/apache/pdfbox/filter/LZWFilter;

    invoke-direct {v4}, Lorg/apache/pdfbox/filter/LZWFilter;-><init>()V

    new-instance v5, Lorg/apache/pdfbox/filter/ASCIIHexFilter;

    invoke-direct {v5}, Lorg/apache/pdfbox/filter/ASCIIHexFilter;-><init>()V

    new-instance v6, Lorg/apache/pdfbox/filter/ASCII85Filter;

    invoke-direct {v6}, Lorg/apache/pdfbox/filter/ASCII85Filter;-><init>()V

    new-instance v7, Lorg/apache/pdfbox/filter/RunLengthDecodeFilter;

    invoke-direct {v7}, Lorg/apache/pdfbox/filter/RunLengthDecodeFilter;-><init>()V

    new-instance v8, Lorg/apache/pdfbox/filter/CryptFilter;

    invoke-direct {v8}, Lorg/apache/pdfbox/filter/CryptFilter;-><init>()V

    sget-object v9, Lorg/apache/pdfbox/cos/COSName;->FLATE_DECODE:Lorg/apache/pdfbox/cos/COSName;

    invoke-interface {v0, v9, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v9, Lorg/apache/pdfbox/cos/COSName;->FLATE_DECODE_ABBREVIATION:Lorg/apache/pdfbox/cos/COSName;

    invoke-interface {v0, v9, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->DCT_DECODE:Lorg/apache/pdfbox/cos/COSName;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->DCT_DECODE_ABBREVIATION:Lorg/apache/pdfbox/cos/COSName;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->CCITTFAX_DECODE:Lorg/apache/pdfbox/cos/COSName;

    invoke-interface {v0, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->CCITTFAX_DECODE_ABBREVIATION:Lorg/apache/pdfbox/cos/COSName;

    invoke-interface {v0, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->LZW_DECODE:Lorg/apache/pdfbox/cos/COSName;

    invoke-interface {v0, v1, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->LZW_DECODE_ABBREVIATION:Lorg/apache/pdfbox/cos/COSName;

    invoke-interface {v0, v1, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->ASCII_HEX_DECODE:Lorg/apache/pdfbox/cos/COSName;

    invoke-interface {v0, v1, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->ASCII_HEX_DECODE_ABBREVIATION:Lorg/apache/pdfbox/cos/COSName;

    invoke-interface {v0, v1, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->ASCII85_DECODE:Lorg/apache/pdfbox/cos/COSName;

    invoke-interface {v0, v1, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->ASCII85_DECODE_ABBREVIATION:Lorg/apache/pdfbox/cos/COSName;

    invoke-interface {v0, v1, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->RUN_LENGTH_DECODE:Lorg/apache/pdfbox/cos/COSName;

    invoke-interface {v0, v1, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->RUN_LENGTH_DECODE_ABBREVIATION:Lorg/apache/pdfbox/cos/COSName;

    invoke-interface {v0, v1, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->CRYPT:Lorg/apache/pdfbox/cos/COSName;

    invoke-interface {v0, v1, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public getAllFilters()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection<",
            "Lorg/apache/pdfbox/filter/Filter;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/pdfbox/filter/FilterFactory;->filters:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method public getFilter(Ljava/lang/String;)Lorg/apache/pdfbox/filter/Filter;
    .locals 0

    .line 1
    invoke-static {p1}, Lorg/apache/pdfbox/cos/COSName;->getPDFName(Ljava/lang/String;)Lorg/apache/pdfbox/cos/COSName;

    move-result-object p1

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/filter/FilterFactory;->getFilter(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/filter/Filter;

    move-result-object p1

    return-object p1
.end method

.method public getFilter(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/filter/Filter;
    .locals 3

    .line 2
    iget-object v0, p0, Lorg/apache/pdfbox/filter/FilterFactory;->filters:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/filter/Filter;

    if-eqz v0, :cond_0

    return-object v0

    :cond_0
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid filter: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

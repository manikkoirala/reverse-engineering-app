.class public abstract Lorg/apache/pdfbox/filter/Filter;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getDecodeParams(Lorg/apache/pdfbox/cos/COSDictionary;I)Lorg/apache/pdfbox/cos/COSDictionary;
    .locals 2

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->DECODE_PARMS:Lorg/apache/pdfbox/cos/COSName;

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->DP:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object p0

    instance-of v0, p0, Lorg/apache/pdfbox/cos/COSDictionary;

    if-eqz v0, :cond_0

    check-cast p0, Lorg/apache/pdfbox/cos/COSDictionary;

    return-object p0

    :cond_0
    instance-of v0, p0, Lorg/apache/pdfbox/cos/COSArray;

    if-eqz v0, :cond_1

    check-cast p0, Lorg/apache/pdfbox/cos/COSArray;

    invoke-virtual {p0}, Lorg/apache/pdfbox/cos/COSArray;->size()I

    move-result v0

    if-ge p1, v0, :cond_2

    invoke-virtual {p0, p1}, Lorg/apache/pdfbox/cos/COSArray;->getObject(I)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object p0

    check-cast p0, Lorg/apache/pdfbox/cos/COSDictionary;

    return-object p0

    :cond_1
    if-eqz p0, :cond_2

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Expected DecodeParams to be an Array or Dictionary but found "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    const-string p1, "PdfBoxAndroid"

    invoke-static {p1, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    new-instance p0, Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-direct {p0}, Lorg/apache/pdfbox/cos/COSDictionary;-><init>()V

    return-object p0
.end method


# virtual methods
.method public abstract decode(Ljava/io/InputStream;Ljava/io/OutputStream;Lorg/apache/pdfbox/cos/COSDictionary;I)Lorg/apache/pdfbox/filter/DecodeResult;
.end method

.method public abstract encode(Ljava/io/InputStream;Ljava/io/OutputStream;Lorg/apache/pdfbox/cos/COSDictionary;)V
.end method

.method public final encode(Ljava/io/InputStream;Ljava/io/OutputStream;Lorg/apache/pdfbox/cos/COSDictionary;I)V
    .locals 0

    .line 1
    invoke-virtual {p3}, Lorg/apache/pdfbox/cos/COSDictionary;->asUnmodifiableDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object p3

    invoke-virtual {p0, p1, p2, p3}, Lorg/apache/pdfbox/filter/Filter;->encode(Ljava/io/InputStream;Ljava/io/OutputStream;Lorg/apache/pdfbox/cos/COSDictionary;)V

    return-void
.end method

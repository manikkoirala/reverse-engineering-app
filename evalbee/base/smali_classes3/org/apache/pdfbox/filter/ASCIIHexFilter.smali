.class final Lorg/apache/pdfbox/filter/ASCIIHexFilter;
.super Lorg/apache/pdfbox/filter/Filter;
.source "SourceFile"


# static fields
.field private static final REVERSE_HEX:[I


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    const/16 v0, 0x67

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lorg/apache/pdfbox/filter/ASCIIHexFilter;->REVERSE_HEX:[I

    return-void

    :array_0
    .array-data 4
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        0x0
        0x1
        0x2
        0x3
        0x4
        0x5
        0x6
        0x7
        0x8
        0x9
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        0xa
        0xb
        0xc
        0xd
        0xe
        0xf
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        -0x1
        0xa
        0xb
        0xc
        0xd
        0xe
        0xf
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lorg/apache/pdfbox/filter/Filter;-><init>()V

    return-void
.end method

.method private static isEOD(I)Z
    .locals 1

    const/16 v0, 0x3e

    if-ne p0, v0, :cond_0

    const/4 p0, 0x1

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    :goto_0
    return p0
.end method

.method private static isWhitespace(I)Z
    .locals 1

    if-eqz p0, :cond_1

    const/16 v0, 0x9

    if-eq p0, v0, :cond_1

    const/16 v0, 0xa

    if-eq p0, v0, :cond_1

    const/16 v0, 0xc

    if-eq p0, v0, :cond_1

    const/16 v0, 0xd

    if-eq p0, v0, :cond_1

    const/16 v0, 0x20

    if-ne p0, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p0, 0x1

    :goto_1
    return p0
.end method


# virtual methods
.method public decode(Ljava/io/InputStream;Ljava/io/OutputStream;Lorg/apache/pdfbox/cos/COSDictionary;I)Lorg/apache/pdfbox/filter/DecodeResult;
    .locals 7

    :goto_0
    invoke-virtual {p1}, Ljava/io/InputStream;->read()I

    move-result p4

    const/4 v0, -0x1

    if-eq p4, v0, :cond_7

    :goto_1
    invoke-static {p4}, Lorg/apache/pdfbox/filter/ASCIIHexFilter;->isWhitespace(I)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Ljava/io/InputStream;->read()I

    move-result p4

    goto :goto_1

    :cond_0
    if-eq p4, v0, :cond_7

    invoke-static {p4}, Lorg/apache/pdfbox/filter/ASCIIHexFilter;->isEOD(I)Z

    move-result v1

    if-eqz v1, :cond_1

    goto :goto_3

    :cond_1
    sget-object v1, Lorg/apache/pdfbox/filter/ASCIIHexFilter;->REVERSE_HEX:[I

    aget v2, v1, p4

    const-string v3, " char: "

    const-string v4, "Invalid hex, int: "

    const-string v5, "PdfBoxAndroid"

    if-ne v2, v0, :cond_2

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    int-to-char v6, p4

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v5, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    aget p4, v1, p4

    mul-int/lit8 p4, p4, 0x10

    invoke-virtual {p1}, Ljava/io/InputStream;->read()I

    move-result v2

    if-eq v2, v0, :cond_6

    invoke-static {v2}, Lorg/apache/pdfbox/filter/ASCIIHexFilter;->isEOD(I)Z

    move-result v6

    if-eqz v6, :cond_3

    goto :goto_2

    :cond_3
    if-ltz v2, :cond_5

    aget v6, v1, v2

    if-ne v6, v0, :cond_4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    int-to-char v3, v2

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v5, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    aget v0, v1, v2

    add-int/2addr p4, v0

    :cond_5
    invoke-virtual {p2, p4}, Ljava/io/OutputStream;->write(I)V

    goto :goto_0

    :cond_6
    :goto_2
    invoke-virtual {p2, p4}, Ljava/io/OutputStream;->write(I)V

    :cond_7
    :goto_3
    invoke-virtual {p2}, Ljava/io/OutputStream;->flush()V

    new-instance p1, Lorg/apache/pdfbox/filter/DecodeResult;

    invoke-direct {p1, p3}, Lorg/apache/pdfbox/filter/DecodeResult;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    return-object p1
.end method

.method public encode(Ljava/io/InputStream;Ljava/io/OutputStream;Lorg/apache/pdfbox/cos/COSDictionary;)V
    .locals 1

    :goto_0
    invoke-virtual {p1}, Ljava/io/InputStream;->read()I

    move-result p3

    const/4 v0, -0x1

    if-eq p3, v0, :cond_0

    int-to-byte p3, p3

    invoke-static {p3}, Lorg/apache/pdfbox/util/Hex;->getBytes(B)[B

    move-result-object p3

    invoke-virtual {p2, p3}, Ljava/io/OutputStream;->write([B)V

    goto :goto_0

    :cond_0
    invoke-virtual {p2}, Ljava/io/OutputStream;->flush()V

    return-void
.end method

.class Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream$NonLeafLookupTreeNode;
.super Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream$LookupTreeNode;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "NonLeafLookupTreeNode"
.end annotation


# instance fields
.field private one:Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream$LookupTreeNode;

.field private zero:Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream$LookupTreeNode;


# direct methods
.method private constructor <init>()V
    .locals 1

    .line 1
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream$LookupTreeNode;-><init>(Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream$1;)V

    return-void
.end method

.method public synthetic constructor <init>(Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream$1;)V
    .locals 0

    .line 2
    invoke-direct {p0}, Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream$NonLeafLookupTreeNode;-><init>()V

    return-void
.end method


# virtual methods
.method public get(I)Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream$LookupTreeNode;
    .locals 0

    if-nez p1, :cond_0

    iget-object p1, p0, Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream$NonLeafLookupTreeNode;->zero:Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream$LookupTreeNode;

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream$NonLeafLookupTreeNode;->one:Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream$LookupTreeNode;

    :goto_0
    return-object p1
.end method

.method public getNextCodeWord(Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream;)Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream$CodeWord;
    .locals 1

    invoke-static {p1}, Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream;->access$300(Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream;)I

    move-result v0

    if-gez v0, :cond_0

    const/4 p1, 0x0

    return-object p1

    :cond_0
    invoke-virtual {p0, v0}, Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream$NonLeafLookupTreeNode;->get(I)Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream$LookupTreeNode;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0, p1}, Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream$LookupTreeNode;->getNextCodeWord(Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream;)Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream$CodeWord;

    move-result-object p1

    return-object p1

    :cond_1
    new-instance p1, Ljava/io/IOException;

    const-string v0, "Invalid code word encountered"

    invoke-direct {p1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public set(ILorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream$LookupTreeNode;)V
    .locals 0

    if-nez p1, :cond_0

    iput-object p2, p0, Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream$NonLeafLookupTreeNode;->zero:Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream$LookupTreeNode;

    goto :goto_0

    :cond_0
    iput-object p2, p0, Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream$NonLeafLookupTreeNode;->one:Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream$LookupTreeNode;

    :goto_0
    return-void
.end method

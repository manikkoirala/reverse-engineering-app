.class public final Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream;
.super Ljava/io/InputStream;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream$EndOfLineTreeNode;,
        Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream$MakeUpTreeNode;,
        Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream$RunLengthTreeNode;,
        Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream$NonLeafLookupTreeNode;,
        Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream$CodeWord;,
        Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream$LookupTreeNode;
    }
.end annotation


# static fields
.field private static final BIT_POS_MASKS:[I

.field private static final BLACK_LOOKUP_TREE_ROOT:Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream$NonLeafLookupTreeNode;

.field private static final CODE_WORD:I = 0x0

.field private static final EOL_STARTER:S = 0xb00s

.field private static final SIGNAL_EOD:I = -0x1

.field private static final SIGNAL_EOL:I = -0x2

.field private static final WHITE_LOOKUP_TREE_ROOT:Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream$NonLeafLookupTreeNode;


# instance fields
.field private accumulatedRunLength:I

.field private bitPos:I

.field private bits:I

.field private columns:I

.field private decodedLine:Lorg/apache/pdfbox/filter/ccitt/PackedBitArray;

.field private decodedReadPos:I

.field private decodedWritePos:I

.field private encodedByteAlign:Z

.field private rows:I

.field private source:Ljava/io/InputStream;

.field private y:I


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream$NonLeafLookupTreeNode;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream$NonLeafLookupTreeNode;-><init>(Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream$1;)V

    sput-object v0, Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream;->WHITE_LOOKUP_TREE_ROOT:Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream$NonLeafLookupTreeNode;

    new-instance v0, Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream$NonLeafLookupTreeNode;

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream$NonLeafLookupTreeNode;-><init>(Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream$1;)V

    sput-object v0, Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream;->BLACK_LOOKUP_TREE_ROOT:Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream$NonLeafLookupTreeNode;

    invoke-static {}, Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream;->buildLookupTree()V

    const/16 v0, 0x8

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream;->BIT_POS_MASKS:[I

    return-void

    :array_0
    .array-data 4
        0x80
        0x40
        0x20
        0x10
        0x8
        0x4
        0x2
        0x1
    .end array-data
.end method

.method public constructor <init>(Ljava/io/InputStream;IIZ)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    const/16 v0, 0x8

    iput v0, p0, Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream;->bitPos:I

    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream;->y:I

    iput-object p1, p0, Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream;->source:Ljava/io/InputStream;

    iput p2, p0, Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream;->columns:I

    iput p3, p0, Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream;->rows:I

    new-instance p1, Lorg/apache/pdfbox/filter/ccitt/PackedBitArray;

    invoke-direct {p1, p2}, Lorg/apache/pdfbox/filter/ccitt/PackedBitArray;-><init>(I)V

    iput-object p1, p0, Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream;->decodedLine:Lorg/apache/pdfbox/filter/ccitt/PackedBitArray;

    invoke-virtual {p1}, Lorg/apache/pdfbox/filter/ccitt/PackedBitArray;->getByteCount()I

    move-result p1

    iput p1, p0, Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream;->decodedReadPos:I

    iput-boolean p4, p0, Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream;->encodedByteAlign:Z

    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;IZ)V
    .locals 1

    .line 2
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0, p3}, Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream;-><init>(Ljava/io/InputStream;IIZ)V

    return-void
.end method

.method public static synthetic access$300(Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream;)I
    .locals 0

    invoke-direct {p0}, Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream;->readBit()I

    move-result p0

    return p0
.end method

.method public static synthetic access$400(Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream;II)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream;->writeRun(II)V

    return-void
.end method

.method public static synthetic access$500(Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream;I)V
    .locals 0

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream;->writeNonTerminating(I)V

    return-void
.end method

.method private static addLookupTreeNode(SLorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream$NonLeafLookupTreeNode;Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream$LookupTreeNode;)V
    .locals 4

    shr-int/lit8 v0, p0, 0x8

    and-int/lit16 p0, p0, 0xff

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-lez v0, :cond_2

    shr-int v1, p0, v0

    and-int/lit8 v1, v1, 0x1

    invoke-virtual {p1, v1}, Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream$NonLeafLookupTreeNode;->get(I)Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream$LookupTreeNode;

    move-result-object v2

    if-nez v2, :cond_0

    new-instance v2, Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream$NonLeafLookupTreeNode;

    const/4 v3, 0x0

    invoke-direct {v2, v3}, Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream$NonLeafLookupTreeNode;-><init>(Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream$1;)V

    invoke-virtual {p1, v1, v2}, Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream$NonLeafLookupTreeNode;->set(ILorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream$LookupTreeNode;)V

    :cond_0
    instance-of p1, v2, Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream$NonLeafLookupTreeNode;

    if-eqz p1, :cond_1

    move-object p1, v2

    check-cast p1, Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream$NonLeafLookupTreeNode;

    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_1
    new-instance p0, Ljava/lang/IllegalStateException;

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string p2, "NonLeafLookupTreeNode expected, was "

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p2

    invoke-virtual {p2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p0

    :cond_2
    and-int/lit8 p0, p0, 0x1

    invoke-virtual {p1, p0}, Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream$NonLeafLookupTreeNode;->get(I)Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream$LookupTreeNode;

    move-result-object v0

    if-nez v0, :cond_3

    invoke-virtual {p1, p0, p2}, Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream$NonLeafLookupTreeNode;->set(ILorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream$LookupTreeNode;)V

    return-void

    :cond_3
    new-instance p0, Ljava/lang/IllegalStateException;

    const-string p1, "Two codes conflicting in lookup tree"

    invoke-direct {p0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p0
.end method

.method private static buildLookupTree()V
    .locals 4

    sget-object v0, Lorg/apache/pdfbox/filter/ccitt/CCITTFaxConstants;->WHITE_TERMINATING:[S

    sget-object v1, Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream;->WHITE_LOOKUP_TREE_ROOT:Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream$NonLeafLookupTreeNode;

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream;->buildUpTerminating([SLorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream$NonLeafLookupTreeNode;Z)V

    sget-object v0, Lorg/apache/pdfbox/filter/ccitt/CCITTFaxConstants;->BLACK_TERMINATING:[S

    sget-object v2, Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream;->BLACK_LOOKUP_TREE_ROOT:Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream$NonLeafLookupTreeNode;

    const/4 v3, 0x0

    invoke-static {v0, v2, v3}, Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream;->buildUpTerminating([SLorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream$NonLeafLookupTreeNode;Z)V

    sget-object v0, Lorg/apache/pdfbox/filter/ccitt/CCITTFaxConstants;->WHITE_MAKE_UP:[S

    invoke-static {v0, v1}, Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream;->buildUpMakeUp([SLorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream$NonLeafLookupTreeNode;)V

    sget-object v0, Lorg/apache/pdfbox/filter/ccitt/CCITTFaxConstants;->BLACK_MAKE_UP:[S

    invoke-static {v0, v2}, Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream;->buildUpMakeUp([SLorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream$NonLeafLookupTreeNode;)V

    sget-object v0, Lorg/apache/pdfbox/filter/ccitt/CCITTFaxConstants;->LONG_MAKE_UP:[S

    invoke-static {v0, v1}, Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream;->buildUpMakeUpLong([SLorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream$NonLeafLookupTreeNode;)V

    invoke-static {v0, v2}, Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream;->buildUpMakeUpLong([SLorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream$NonLeafLookupTreeNode;)V

    new-instance v0, Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream$EndOfLineTreeNode;

    const/4 v3, 0x0

    invoke-direct {v0, v3}, Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream$EndOfLineTreeNode;-><init>(Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream$1;)V

    const/16 v3, 0xb00

    invoke-static {v3, v1, v0}, Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream;->addLookupTreeNode(SLorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream$NonLeafLookupTreeNode;Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream$LookupTreeNode;)V

    invoke-static {v3, v2, v0}, Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream;->addLookupTreeNode(SLorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream$NonLeafLookupTreeNode;Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream$LookupTreeNode;)V

    return-void
.end method

.method private static buildUpMakeUp([SLorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream$NonLeafLookupTreeNode;)V
    .locals 5

    array-length v0, p0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    new-instance v2, Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream$MakeUpTreeNode;

    add-int/lit8 v3, v1, 0x1

    mul-int/lit8 v4, v3, 0x40

    invoke-direct {v2, v4}, Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream$MakeUpTreeNode;-><init>(I)V

    aget-short v1, p0, v1

    invoke-static {v1, p1, v2}, Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream;->addLookupTreeNode(SLorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream$NonLeafLookupTreeNode;Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream$LookupTreeNode;)V

    move v1, v3

    goto :goto_0

    :cond_0
    return-void
.end method

.method private static buildUpMakeUpLong([SLorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream$NonLeafLookupTreeNode;)V
    .locals 4

    array-length v0, p0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    new-instance v2, Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream$MakeUpTreeNode;

    add-int/lit8 v3, v1, 0x1c

    mul-int/lit8 v3, v3, 0x40

    invoke-direct {v2, v3}, Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream$MakeUpTreeNode;-><init>(I)V

    aget-short v3, p0, v1

    invoke-static {v3, p1, v2}, Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream;->addLookupTreeNode(SLorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream$NonLeafLookupTreeNode;Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream$LookupTreeNode;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private static buildUpTerminating([SLorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream$NonLeafLookupTreeNode;Z)V
    .locals 4

    array-length v0, p0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    new-instance v2, Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream$RunLengthTreeNode;

    xor-int/lit8 v3, p2, 0x1

    invoke-direct {v2, v3, v1}, Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream$RunLengthTreeNode;-><init>(II)V

    aget-short v3, p0, v1

    invoke-static {v3, p1, v2}, Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream;->addLookupTreeNode(SLorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream$NonLeafLookupTreeNode;Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream$LookupTreeNode;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private decodeLine()Z
    .locals 9

    iget-boolean v0, p0, Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream;->encodedByteAlign:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream;->bitPos:I

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream;->readByte()V

    :cond_0
    iget v0, p0, Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream;->bits:I

    const/4 v1, 0x0

    if-gez v0, :cond_1

    return v1

    :cond_1
    iget v0, p0, Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream;->y:I

    const/4 v2, 0x1

    add-int/2addr v0, v2

    iput v0, p0, Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream;->y:I

    iget v3, p0, Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream;->rows:I

    if-lez v3, :cond_2

    if-lt v0, v3, :cond_2

    return v1

    :cond_2
    iget-object v0, p0, Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream;->decodedLine:Lorg/apache/pdfbox/filter/ccitt/PackedBitArray;

    invoke-virtual {v0}, Lorg/apache/pdfbox/filter/ccitt/PackedBitArray;->clear()V

    iput v1, p0, Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream;->decodedWritePos:I

    const/4 v0, 0x6

    move v3, v1

    move v4, v2

    :cond_3
    :goto_0
    iget v5, p0, Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream;->columns:I

    if-lt v3, v5, :cond_5

    iget v5, p0, Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream;->accumulatedRunLength:I

    if-lez v5, :cond_4

    goto :goto_1

    :cond_4
    iput v1, p0, Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream;->decodedReadPos:I

    return v2

    :cond_5
    :goto_1
    if-eqz v4, :cond_6

    sget-object v5, Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream;->WHITE_LOOKUP_TREE_ROOT:Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream$NonLeafLookupTreeNode;

    goto :goto_2

    :cond_6
    sget-object v5, Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream;->BLACK_LOOKUP_TREE_ROOT:Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream$NonLeafLookupTreeNode;

    :goto_2
    invoke-virtual {v5, p0}, Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream$LookupTreeNode;->getNextCodeWord(Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream;)Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream$CodeWord;

    move-result-object v5

    if-nez v5, :cond_8

    if-lez v3, :cond_7

    iput v1, p0, Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream;->decodedReadPos:I

    return v2

    :cond_7
    return v1

    :cond_8
    invoke-interface {v5}, Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream$CodeWord;->getType()I

    move-result v6

    const/4 v7, -0x2

    const/4 v8, -0x1

    if-ne v6, v7, :cond_9

    add-int/2addr v0, v8

    if-nez v0, :cond_3

    return v1

    :cond_9
    invoke-interface {v5, p0}, Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream$CodeWord;->execute(Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream;)I

    move-result v0

    add-int/2addr v3, v0

    iget v0, p0, Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream;->accumulatedRunLength:I

    if-nez v0, :cond_a

    xor-int/lit8 v0, v4, 0x1

    move v4, v0

    :cond_a
    move v0, v8

    goto :goto_0
.end method

.method private readBit()I
    .locals 4

    iget v0, p0, Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream;->bitPos:I

    const/16 v1, 0x8

    if-lt v0, v1, :cond_0

    invoke-direct {p0}, Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream;->readByte()V

    iget v0, p0, Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream;->bits:I

    if-gez v0, :cond_0

    const/4 v0, -0x1

    return v0

    :cond_0
    iget v0, p0, Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream;->bits:I

    sget-object v1, Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream;->BIT_POS_MASKS:[I

    iget v2, p0, Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream;->bitPos:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream;->bitPos:I

    aget v1, v1, v2

    and-int/2addr v0, v1

    if-nez v0, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0
.end method

.method private readByte()V
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream;->source:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->read()I

    move-result v0

    iput v0, p0, Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream;->bits:I

    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream;->bitPos:I

    return-void
.end method

.method private writeNonTerminating(I)V
    .locals 1

    iget v0, p0, Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream;->accumulatedRunLength:I

    add-int/2addr v0, p1

    iput v0, p0, Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream;->accumulatedRunLength:I

    return-void
.end method

.method private writeRun(II)V
    .locals 1

    iget v0, p0, Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream;->accumulatedRunLength:I

    add-int/2addr v0, p2

    iput v0, p0, Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream;->accumulatedRunLength:I

    if-eqz p1, :cond_0

    iget-object p1, p0, Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream;->decodedLine:Lorg/apache/pdfbox/filter/ccitt/PackedBitArray;

    iget p2, p0, Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream;->decodedWritePos:I

    invoke-virtual {p1, p2, v0}, Lorg/apache/pdfbox/filter/ccitt/PackedBitArray;->setBits(II)V

    :cond_0
    iget p1, p0, Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream;->decodedWritePos:I

    iget p2, p0, Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream;->accumulatedRunLength:I

    add-int/2addr p1, p2

    iput p1, p0, Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream;->decodedWritePos:I

    const/4 p1, 0x0

    iput p1, p0, Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream;->accumulatedRunLength:I

    return-void
.end method


# virtual methods
.method public markSupported()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public read()I
    .locals 3

    iget v0, p0, Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream;->decodedReadPos:I

    iget-object v1, p0, Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream;->decodedLine:Lorg/apache/pdfbox/filter/ccitt/PackedBitArray;

    invoke-virtual {v1}, Lorg/apache/pdfbox/filter/ccitt/PackedBitArray;->getByteCount()I

    move-result v1

    if-lt v0, v1, :cond_0

    invoke-direct {p0}, Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream;->decodeLine()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, -0x1

    return v0

    :cond_0
    iget-object v0, p0, Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream;->decodedLine:Lorg/apache/pdfbox/filter/ccitt/PackedBitArray;

    invoke-virtual {v0}, Lorg/apache/pdfbox/filter/ccitt/PackedBitArray;->getData()[B

    move-result-object v0

    iget v1, p0, Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream;->decodedReadPos:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream;->decodedReadPos:I

    aget-byte v0, v0, v1

    and-int/lit16 v0, v0, 0xff

    return v0
.end method

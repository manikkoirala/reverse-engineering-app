.class Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream$EndOfLineTreeNode;
.super Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream$LookupTreeNode;
.source "SourceFile"

# interfaces
.implements Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream$CodeWord;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "EndOfLineTreeNode"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 1

    .line 1
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream$LookupTreeNode;-><init>(Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream$1;)V

    return-void
.end method

.method public synthetic constructor <init>(Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream$1;)V
    .locals 0

    .line 2
    invoke-direct {p0}, Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream$EndOfLineTreeNode;-><init>()V

    return-void
.end method


# virtual methods
.method public execute(Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream;)I
    .locals 0

    const/4 p1, 0x0

    return p1
.end method

.method public getNextCodeWord(Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream;)Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream$CodeWord;
    .locals 1

    :cond_0
    invoke-static {p1}, Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream;->access$300(Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream;)I

    move-result v0

    if-eqz v0, :cond_0

    if-gez v0, :cond_1

    const/4 p1, 0x0

    return-object p1

    :cond_1
    return-object p0
.end method

.method public getType()I
    .locals 1

    const/4 v0, -0x2

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    const-string v0, "EOL"

    return-object v0
.end method

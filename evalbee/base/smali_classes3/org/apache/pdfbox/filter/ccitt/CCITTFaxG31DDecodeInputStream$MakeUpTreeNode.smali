.class Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream$MakeUpTreeNode;
.super Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream$LookupTreeNode;
.source "SourceFile"

# interfaces
.implements Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream$CodeWord;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "MakeUpTreeNode"
.end annotation


# instance fields
.field private final length:I


# direct methods
.method public constructor <init>(I)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream$LookupTreeNode;-><init>(Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream$1;)V

    iput p1, p0, Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream$MakeUpTreeNode;->length:I

    return-void
.end method


# virtual methods
.method public execute(Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream;)I
    .locals 1

    iget v0, p0, Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream$MakeUpTreeNode;->length:I

    invoke-static {p1, v0}, Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream;->access$500(Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream;I)V

    iget p1, p0, Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream$MakeUpTreeNode;->length:I

    return p1
.end method

.method public getNextCodeWord(Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream;)Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream$CodeWord;
    .locals 0

    return-object p0
.end method

.method public getType()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Make up code for length "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream$MakeUpTreeNode;->length:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.class public Lorg/apache/pdfbox/filter/LZWFilter;
.super Lorg/apache/pdfbox/filter/Filter;
.source "SourceFile"


# static fields
.field public static final CLEAR_TABLE:J = 0x100L

.field public static final EOD:J = 0x101L


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lorg/apache/pdfbox/filter/Filter;-><init>()V

    return-void
.end method

.method private static calculateChunk(II)I
    .locals 1

    rsub-int v0, p1, 0x800

    if-lt p0, v0, :cond_0

    const/16 p0, 0xc

    return p0

    :cond_0
    rsub-int v0, p1, 0x400

    if-lt p0, v0, :cond_1

    const/16 p0, 0xb

    return p0

    :cond_1
    rsub-int p1, p1, 0x200

    if-lt p0, p1, :cond_2

    const/16 p0, 0xa

    return p0

    :cond_2
    const/16 p0, 0x9

    return p0
.end method

.method private static createCodeTable()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "[B>;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0x1000

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    const/16 v3, 0x100

    if-ge v2, v3, :cond_0

    const/4 v3, 0x1

    new-array v3, v3, [B

    and-int/lit16 v4, v2, 0xff

    int-to-byte v4, v4

    aput-byte v4, v3, v1

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object v0
.end method

.method private static doLZWDecode(Ljava/io/InputStream;Ljava/io/OutputStream;I)V
    .locals 11

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    new-instance v1, Lorg/apache/javax/imageio/stream/MemoryCacheImageInputStream;

    invoke-direct {v1, p0}, Lorg/apache/javax/imageio/stream/MemoryCacheImageInputStream;-><init>(Ljava/io/InputStream;)V

    const/16 p0, 0x9

    const-wide/16 v2, -0x1

    :goto_0
    move v4, p0

    move-wide v5, v2

    :goto_1
    :try_start_0
    invoke-virtual {v1, v4}, Lorg/apache/javax/imageio/stream/MemoryCacheImageInputStream;->readBits(I)J

    move-result-wide v7

    const-wide/16 v9, 0x101

    cmp-long v4, v7, v9

    if-eqz v4, :cond_3

    const-wide/16 v9, 0x100

    cmp-long v4, v7, v9

    if-nez v4, :cond_0

    invoke-static {}, Lorg/apache/pdfbox/filter/LZWFilter;->createCodeTable()Ljava/util/List;

    move-result-object v0

    goto :goto_0

    :cond_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    int-to-long v9, v4

    cmp-long v4, v7, v9

    const/4 v9, 0x0

    if-gez v4, :cond_1

    long-to-int v4, v7

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [B

    aget-byte v9, v4, v9

    invoke-virtual {p1, v4}, Ljava/io/OutputStream;->write([B)V

    cmp-long v4, v5, v2

    if-eqz v4, :cond_2

    long-to-int v4, v5

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [B

    array-length v5, v4

    add-int/lit8 v5, v5, 0x1

    invoke-static {v4, v5}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v5

    array-length v4, v4

    aput-byte v9, v5, v4

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_1
    long-to-int v4, v5

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [B

    array-length v5, v4

    add-int/lit8 v5, v5, 0x1

    invoke-static {v4, v5}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v5

    array-length v6, v4

    aget-byte v4, v4, v9

    aput-byte v4, v5, v6

    invoke-virtual {p1, v5}, Ljava/io/OutputStream;->write([B)V

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    :goto_2
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    invoke-static {v4, p2}, Lorg/apache/pdfbox/filter/LZWFilter;->calculateChunk(II)I

    move-result v4
    :try_end_0
    .catch Ljava/io/EOFException; {:try_start_0 .. :try_end_0} :catch_0

    move-wide v5, v7

    goto :goto_1

    :catch_0
    const-string p0, "PdfBoxAndroid"

    const-string p2, "Premature EOF in LZW stream, EOD code missing"

    invoke-static {p0, p2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    invoke-virtual {p1}, Ljava/io/OutputStream;->flush()V

    return-void
.end method

.method private static findPatternCode(Ljava/util/List;[B)I
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "[B>;[B)I"
        }
    .end annotation

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    sub-int/2addr v0, v1

    const/4 v2, -0x1

    const/4 v3, 0x0

    move v4, v2

    :goto_0
    if-ltz v0, :cond_4

    int-to-long v5, v0

    const-wide/16 v7, 0x101

    cmp-long v5, v5, v7

    if-gtz v5, :cond_1

    if-eq v4, v2, :cond_0

    return v4

    :cond_0
    array-length v5, p1

    if-le v5, v1, :cond_1

    return v2

    :cond_1
    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [B

    if-ne v4, v2, :cond_2

    array-length v6, v5

    if-le v6, v3, :cond_3

    :cond_2
    invoke-static {v5, p1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v6

    if-eqz v6, :cond_3

    array-length v3, v5

    move v4, v0

    :cond_3
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_4
    return v4
.end method


# virtual methods
.method public decode(Ljava/io/InputStream;Ljava/io/OutputStream;Lorg/apache/pdfbox/cos/COSDictionary;I)Lorg/apache/pdfbox/filter/DecodeResult;
    .locals 9

    invoke-static {p3, p4}, Lorg/apache/pdfbox/filter/Filter;->getDecodeParams(Lorg/apache/pdfbox/cos/COSDictionary;I)Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object p4

    const/4 v0, 0x1

    if-eqz p4, :cond_1

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->PREDICTOR:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p4, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getInt(Lorg/apache/pdfbox/cos/COSName;)I

    move-result v1

    sget-object v2, Lorg/apache/pdfbox/cos/COSName;->EARLY_CHANGE:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p4, v2, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->getInt(Lorg/apache/pdfbox/cos/COSName;I)I

    move-result v2

    if-eqz v2, :cond_0

    if-eq v2, v0, :cond_0

    goto :goto_0

    :cond_0
    move v8, v2

    move v2, v1

    move v1, v8

    goto :goto_1

    :cond_1
    const/4 v1, -0x1

    :goto_0
    move v2, v1

    move v1, v0

    :goto_1
    if-le v2, v0, :cond_2

    sget-object v3, Lorg/apache/pdfbox/cos/COSName;->COLORS:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p4, v3, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->getInt(Lorg/apache/pdfbox/cos/COSName;I)I

    move-result v3

    const/16 v4, 0x20

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v3

    sget-object v4, Lorg/apache/pdfbox/cos/COSName;->BITS_PER_COMPONENT:Lorg/apache/pdfbox/cos/COSName;

    const/16 v5, 0x8

    invoke-virtual {p4, v4, v5}, Lorg/apache/pdfbox/cos/COSDictionary;->getInt(Lorg/apache/pdfbox/cos/COSName;I)I

    move-result v4

    sget-object v5, Lorg/apache/pdfbox/cos/COSName;->COLUMNS:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p4, v5, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->getInt(Lorg/apache/pdfbox/cos/COSName;I)I

    move-result v5

    new-instance p4, Ljava/io/ByteArrayOutputStream;

    invoke-direct {p4}, Ljava/io/ByteArrayOutputStream;-><init>()V

    invoke-static {p1, p4, v1}, Lorg/apache/pdfbox/filter/LZWFilter;->doLZWDecode(Ljava/io/InputStream;Ljava/io/OutputStream;I)V

    new-instance p1, Ljava/io/ByteArrayInputStream;

    invoke-virtual {p4}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    move-object v6, p1

    move-object v7, p2

    invoke-static/range {v2 .. v7}, Lorg/apache/pdfbox/filter/Predictor;->decodePredictor(IIIILjava/io/InputStream;Ljava/io/OutputStream;)V

    invoke-virtual {p2}, Ljava/io/OutputStream;->flush()V

    invoke-virtual {p4}, Ljava/io/ByteArrayOutputStream;->reset()V

    invoke-virtual {p1}, Ljava/io/ByteArrayInputStream;->reset()V

    goto :goto_2

    :cond_2
    invoke-static {p1, p2, v1}, Lorg/apache/pdfbox/filter/LZWFilter;->doLZWDecode(Ljava/io/InputStream;Ljava/io/OutputStream;I)V

    :goto_2
    new-instance p1, Lorg/apache/pdfbox/filter/DecodeResult;

    invoke-direct {p1, p3}, Lorg/apache/pdfbox/filter/DecodeResult;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    return-object p1
.end method

.method public encode(Ljava/io/InputStream;Ljava/io/OutputStream;Lorg/apache/pdfbox/cos/COSDictionary;)V
    .locals 11

    invoke-static {}, Lorg/apache/pdfbox/filter/LZWFilter;->createCodeTable()Ljava/util/List;

    move-result-object p3

    new-instance v0, Lorg/apache/javax/imageio/stream/MemoryCacheImageOutputStream;

    invoke-direct {v0, p2}, Lorg/apache/javax/imageio/stream/MemoryCacheImageOutputStream;-><init>(Ljava/io/OutputStream;)V

    const-wide/16 v1, 0x100

    const/16 p2, 0x9

    invoke-virtual {v0, v1, v2, p2}, Lorg/apache/javax/imageio/stream/MemoryCacheImageOutputStream;->writeBits(JI)V

    const/4 p2, 0x0

    const/4 v3, -0x1

    move v4, v3

    :goto_0
    invoke-virtual {p1}, Ljava/io/InputStream;->read()I

    move-result v5

    const/4 v6, 0x1

    if-eq v5, v3, :cond_3

    int-to-byte v5, v5

    const/4 v7, 0x0

    if-nez p2, :cond_0

    new-array p2, v6, [B

    aput-byte v5, p2, v7

    :goto_1
    and-int/lit16 v4, v5, 0xff

    goto :goto_0

    :cond_0
    array-length v8, p2

    add-int/2addr v8, v6

    invoke-static {p2, v8}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object p2

    array-length v8, p2

    sub-int/2addr v8, v6

    aput-byte v5, p2, v8

    invoke-static {p3, p2}, Lorg/apache/pdfbox/filter/LZWFilter;->findPatternCode(Ljava/util/List;[B)I

    move-result v8

    if-ne v8, v3, :cond_2

    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v8

    sub-int/2addr v8, v6

    invoke-static {v8, v6}, Lorg/apache/pdfbox/filter/LZWFilter;->calculateChunk(II)I

    move-result v8

    int-to-long v9, v4

    invoke-virtual {v0, v9, v10, v8}, Lorg/apache/javax/imageio/stream/MemoryCacheImageOutputStream;->writeBits(JI)V

    invoke-interface {p3, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result p2

    const/16 v4, 0x1000

    if-ne p2, v4, :cond_1

    invoke-virtual {v0, v1, v2, v8}, Lorg/apache/javax/imageio/stream/MemoryCacheImageOutputStream;->writeBits(JI)V

    invoke-static {}, Lorg/apache/pdfbox/filter/LZWFilter;->createCodeTable()Ljava/util/List;

    move-result-object p3

    :cond_1
    new-array p2, v6, [B

    aput-byte v5, p2, v7

    goto :goto_1

    :cond_2
    move v4, v8

    goto :goto_0

    :cond_3
    if-eq v4, v3, :cond_4

    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result p1

    sub-int/2addr p1, v6

    invoke-static {p1, v6}, Lorg/apache/pdfbox/filter/LZWFilter;->calculateChunk(II)I

    move-result p1

    int-to-long v1, v4

    invoke-virtual {v0, v1, v2, p1}, Lorg/apache/javax/imageio/stream/MemoryCacheImageOutputStream;->writeBits(JI)V

    :cond_4
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result p1

    invoke-static {p1, v6}, Lorg/apache/pdfbox/filter/LZWFilter;->calculateChunk(II)I

    move-result p1

    const-wide/16 p2, 0x101

    invoke-virtual {v0, p2, p3, p1}, Lorg/apache/javax/imageio/stream/MemoryCacheImageOutputStream;->writeBits(JI)V

    const-wide/16 p1, 0x0

    const/4 p3, 0x7

    invoke-virtual {v0, p1, p2, p3}, Lorg/apache/javax/imageio/stream/MemoryCacheImageOutputStream;->writeBits(JI)V

    invoke-virtual {v0}, Lorg/apache/javax/imageio/stream/MemoryCacheImageOutputStream;->flush()V

    return-void
.end method

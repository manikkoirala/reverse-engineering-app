.class final Lorg/apache/pdfbox/filter/ASCII85OutputStream;
.super Ljava/io/FilterOutputStream;
.source "SourceFile"


# static fields
.field private static final NEWLINE:C = '\n'

.field private static final OFFSET:C = '!'

.field private static final Z:C = 'z'


# instance fields
.field private count:I

.field private flushed:Z

.field private indata:[B

.field private lineBreak:I

.field private maxline:I

.field private outdata:[B

.field private terminator:C


# direct methods
.method public constructor <init>(Ljava/io/OutputStream;)V
    .locals 0

    invoke-direct {p0, p1}, Ljava/io/FilterOutputStream;-><init>(Ljava/io/OutputStream;)V

    const/16 p1, 0x48

    iput p1, p0, Lorg/apache/pdfbox/filter/ASCII85OutputStream;->lineBreak:I

    iput p1, p0, Lorg/apache/pdfbox/filter/ASCII85OutputStream;->maxline:I

    const/4 p1, 0x0

    iput p1, p0, Lorg/apache/pdfbox/filter/ASCII85OutputStream;->count:I

    const/4 p1, 0x4

    new-array p1, p1, [B

    iput-object p1, p0, Lorg/apache/pdfbox/filter/ASCII85OutputStream;->indata:[B

    const/4 p1, 0x5

    new-array p1, p1, [B

    iput-object p1, p0, Lorg/apache/pdfbox/filter/ASCII85OutputStream;->outdata:[B

    const/4 p1, 0x1

    iput-boolean p1, p0, Lorg/apache/pdfbox/filter/ASCII85OutputStream;->flushed:Z

    const/16 p1, 0x7e

    iput-char p1, p0, Lorg/apache/pdfbox/filter/ASCII85OutputStream;->terminator:C

    return-void
.end method

.method private transformASCII85()V
    .locals 14

    iget-object v0, p0, Lorg/apache/pdfbox/filter/ASCII85OutputStream;->indata:[B

    const/4 v1, 0x0

    aget-byte v2, v0, v1

    shl-int/lit8 v2, v2, 0x8

    const/4 v3, 0x1

    aget-byte v4, v0, v3

    and-int/lit16 v4, v4, 0xff

    or-int/2addr v2, v4

    shl-int/lit8 v2, v2, 0x10

    const/4 v4, 0x2

    aget-byte v5, v0, v4

    and-int/lit16 v5, v5, 0xff

    shl-int/lit8 v5, v5, 0x8

    or-int/2addr v2, v5

    const/4 v5, 0x3

    aget-byte v0, v0, v5

    and-int/lit16 v0, v0, 0xff

    or-int/2addr v0, v2

    int-to-long v6, v0

    const-wide v8, 0xffffffffL

    and-long/2addr v6, v8

    const-wide/16 v8, 0x0

    cmp-long v0, v6, v8

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/pdfbox/filter/ASCII85OutputStream;->outdata:[B

    const/16 v2, 0x7a

    aput-byte v2, v0, v1

    aput-byte v1, v0, v3

    return-void

    :cond_0
    const-wide/32 v8, 0x31c84b1

    div-long v8, v6, v8

    iget-object v0, p0, Lorg/apache/pdfbox/filter/ASCII85OutputStream;->outdata:[B

    const-wide/16 v10, 0x21

    add-long v12, v8, v10

    long-to-int v2, v12

    int-to-byte v2, v2

    aput-byte v2, v0, v1

    const-wide/16 v1, 0x55

    mul-long/2addr v8, v1

    mul-long/2addr v8, v1

    mul-long/2addr v8, v1

    mul-long/2addr v8, v1

    sub-long/2addr v6, v8

    const-wide/32 v8, 0x95eed

    div-long v8, v6, v8

    add-long v12, v8, v10

    long-to-int v12, v12

    int-to-byte v12, v12

    aput-byte v12, v0, v3

    mul-long/2addr v8, v1

    mul-long/2addr v8, v1

    mul-long/2addr v8, v1

    sub-long/2addr v6, v8

    const-wide/16 v8, 0x1c39

    div-long v8, v6, v8

    add-long v12, v8, v10

    long-to-int v3, v12

    int-to-byte v3, v3

    aput-byte v3, v0, v4

    mul-long/2addr v8, v1

    mul-long/2addr v8, v1

    sub-long/2addr v6, v8

    div-long v3, v6, v1

    add-long/2addr v3, v10

    long-to-int v3, v3

    int-to-byte v3, v3

    aput-byte v3, v0, v5

    rem-long/2addr v6, v1

    add-long/2addr v6, v10

    long-to-int v1, v6

    int-to-byte v1, v1

    const/4 v2, 0x4

    aput-byte v1, v0, v2

    return-void
.end method


# virtual methods
.method public close()V
    .locals 2

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0}, Lorg/apache/pdfbox/filter/ASCII85OutputStream;->flush()V

    invoke-super {p0}, Ljava/io/FilterOutputStream;->close()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iput-object v0, p0, Lorg/apache/pdfbox/filter/ASCII85OutputStream;->outdata:[B

    iput-object v0, p0, Lorg/apache/pdfbox/filter/ASCII85OutputStream;->indata:[B

    return-void

    :catchall_0
    move-exception v1

    iput-object v0, p0, Lorg/apache/pdfbox/filter/ASCII85OutputStream;->outdata:[B

    iput-object v0, p0, Lorg/apache/pdfbox/filter/ASCII85OutputStream;->indata:[B

    throw v1
.end method

.method public flush()V
    .locals 6

    iget-boolean v0, p0, Lorg/apache/pdfbox/filter/ASCII85OutputStream;->flushed:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget v0, p0, Lorg/apache/pdfbox/filter/ASCII85OutputStream;->count:I

    const/16 v1, 0xa

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-lez v0, :cond_4

    :goto_0
    const/4 v4, 0x4

    if-ge v0, v4, :cond_1

    iget-object v4, p0, Lorg/apache/pdfbox/filter/ASCII85OutputStream;->indata:[B

    aput-byte v2, v4, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    invoke-direct {p0}, Lorg/apache/pdfbox/filter/ASCII85OutputStream;->transformASCII85()V

    iget-object v0, p0, Lorg/apache/pdfbox/filter/ASCII85OutputStream;->outdata:[B

    aget-byte v0, v0, v2

    const/16 v4, 0x7a

    if-ne v0, v4, :cond_2

    move v0, v2

    :goto_1
    const/4 v4, 0x5

    if-ge v0, v4, :cond_2

    iget-object v4, p0, Lorg/apache/pdfbox/filter/ASCII85OutputStream;->outdata:[B

    const/16 v5, 0x21

    aput-byte v5, v4, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    move v0, v2

    :goto_2
    iget v4, p0, Lorg/apache/pdfbox/filter/ASCII85OutputStream;->count:I

    add-int/2addr v4, v3

    if-ge v0, v4, :cond_4

    iget-object v4, p0, Ljava/io/FilterOutputStream;->out:Ljava/io/OutputStream;

    iget-object v5, p0, Lorg/apache/pdfbox/filter/ASCII85OutputStream;->outdata:[B

    aget-byte v5, v5, v0

    invoke-virtual {v4, v5}, Ljava/io/OutputStream;->write(I)V

    iget v4, p0, Lorg/apache/pdfbox/filter/ASCII85OutputStream;->lineBreak:I

    sub-int/2addr v4, v3

    iput v4, p0, Lorg/apache/pdfbox/filter/ASCII85OutputStream;->lineBreak:I

    if-nez v4, :cond_3

    iget-object v4, p0, Ljava/io/FilterOutputStream;->out:Ljava/io/OutputStream;

    invoke-virtual {v4, v1}, Ljava/io/OutputStream;->write(I)V

    iget v4, p0, Lorg/apache/pdfbox/filter/ASCII85OutputStream;->maxline:I

    iput v4, p0, Lorg/apache/pdfbox/filter/ASCII85OutputStream;->lineBreak:I

    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    iget v0, p0, Lorg/apache/pdfbox/filter/ASCII85OutputStream;->lineBreak:I

    sub-int/2addr v0, v3

    iput v0, p0, Lorg/apache/pdfbox/filter/ASCII85OutputStream;->lineBreak:I

    if-nez v0, :cond_5

    iget-object v0, p0, Ljava/io/FilterOutputStream;->out:Ljava/io/OutputStream;

    invoke-virtual {v0, v1}, Ljava/io/OutputStream;->write(I)V

    :cond_5
    iget-object v0, p0, Ljava/io/FilterOutputStream;->out:Ljava/io/OutputStream;

    iget-char v4, p0, Lorg/apache/pdfbox/filter/ASCII85OutputStream;->terminator:C

    invoke-virtual {v0, v4}, Ljava/io/OutputStream;->write(I)V

    iget-object v0, p0, Ljava/io/FilterOutputStream;->out:Ljava/io/OutputStream;

    invoke-virtual {v0, v1}, Ljava/io/OutputStream;->write(I)V

    iput v2, p0, Lorg/apache/pdfbox/filter/ASCII85OutputStream;->count:I

    iget v0, p0, Lorg/apache/pdfbox/filter/ASCII85OutputStream;->maxline:I

    iput v0, p0, Lorg/apache/pdfbox/filter/ASCII85OutputStream;->lineBreak:I

    iput-boolean v3, p0, Lorg/apache/pdfbox/filter/ASCII85OutputStream;->flushed:Z

    invoke-super {p0}, Ljava/io/FilterOutputStream;->flush()V

    return-void
.end method

.method public getLineLength()I
    .locals 1

    iget v0, p0, Lorg/apache/pdfbox/filter/ASCII85OutputStream;->maxline:I

    return v0
.end method

.method public getTerminator()C
    .locals 1

    iget-char v0, p0, Lorg/apache/pdfbox/filter/ASCII85OutputStream;->terminator:C

    return v0
.end method

.method public setLineLength(I)V
    .locals 1

    iget v0, p0, Lorg/apache/pdfbox/filter/ASCII85OutputStream;->lineBreak:I

    if-le v0, p1, :cond_0

    iput p1, p0, Lorg/apache/pdfbox/filter/ASCII85OutputStream;->lineBreak:I

    :cond_0
    iput p1, p0, Lorg/apache/pdfbox/filter/ASCII85OutputStream;->maxline:I

    return-void
.end method

.method public setTerminator(C)V
    .locals 1

    const/16 v0, 0x76

    if-lt p1, v0, :cond_0

    const/16 v0, 0x7e

    if-gt p1, v0, :cond_0

    const/16 v0, 0x7a

    if-eq p1, v0, :cond_0

    iput-char p1, p0, Lorg/apache/pdfbox/filter/ASCII85OutputStream;->terminator:C

    return-void

    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string v0, "Terminator must be 118-126 excluding z"

    invoke-direct {p1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public write(I)V
    .locals 4

    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/pdfbox/filter/ASCII85OutputStream;->flushed:Z

    iget-object v1, p0, Lorg/apache/pdfbox/filter/ASCII85OutputStream;->indata:[B

    iget v2, p0, Lorg/apache/pdfbox/filter/ASCII85OutputStream;->count:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lorg/apache/pdfbox/filter/ASCII85OutputStream;->count:I

    int-to-byte p1, p1

    aput-byte p1, v1, v2

    const/4 p1, 0x4

    if-ge v3, p1, :cond_0

    return-void

    :cond_0
    invoke-direct {p0}, Lorg/apache/pdfbox/filter/ASCII85OutputStream;->transformASCII85()V

    move p1, v0

    :goto_0
    const/4 v1, 0x5

    if-ge p1, v1, :cond_3

    iget-object v1, p0, Lorg/apache/pdfbox/filter/ASCII85OutputStream;->outdata:[B

    aget-byte v1, v1, p1

    if-nez v1, :cond_1

    goto :goto_1

    :cond_1
    iget-object v2, p0, Ljava/io/FilterOutputStream;->out:Ljava/io/OutputStream;

    invoke-virtual {v2, v1}, Ljava/io/OutputStream;->write(I)V

    iget v1, p0, Lorg/apache/pdfbox/filter/ASCII85OutputStream;->lineBreak:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lorg/apache/pdfbox/filter/ASCII85OutputStream;->lineBreak:I

    if-nez v1, :cond_2

    iget-object v1, p0, Ljava/io/FilterOutputStream;->out:Ljava/io/OutputStream;

    const/16 v2, 0xa

    invoke-virtual {v1, v2}, Ljava/io/OutputStream;->write(I)V

    iget v1, p0, Lorg/apache/pdfbox/filter/ASCII85OutputStream;->maxline:I

    iput v1, p0, Lorg/apache/pdfbox/filter/ASCII85OutputStream;->lineBreak:I

    :cond_2
    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    :cond_3
    :goto_1
    iput v0, p0, Lorg/apache/pdfbox/filter/ASCII85OutputStream;->count:I

    return-void
.end method

.class final Lorg/apache/pdfbox/filter/RunLengthDecodeFilter;
.super Lorg/apache/pdfbox/filter/Filter;
.source "SourceFile"


# static fields
.field private static final RUN_LENGTH_EOD:I = 0x80


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lorg/apache/pdfbox/filter/Filter;-><init>()V

    return-void
.end method


# virtual methods
.method public decode(Ljava/io/InputStream;Ljava/io/OutputStream;Lorg/apache/pdfbox/cos/COSDictionary;I)Lorg/apache/pdfbox/filter/DecodeResult;
    .locals 5

    const/16 p4, 0x80

    new-array v0, p4, [B

    :cond_0
    invoke-virtual {p1}, Ljava/io/InputStream;->read()I

    move-result v1

    const/4 v2, -0x1

    if-eq v1, v2, :cond_2

    if-eq v1, p4, :cond_2

    const/16 v2, 0x7f

    const/4 v3, 0x0

    if-gt v1, v2, :cond_1

    add-int/lit8 v1, v1, 0x1

    :goto_0
    if-lez v1, :cond_0

    invoke-virtual {p1, v0, v3, v1}, Ljava/io/InputStream;->read([BII)I

    move-result v2

    invoke-virtual {p2, v0, v3, v2}, Ljava/io/OutputStream;->write([BII)V

    sub-int/2addr v1, v2

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Ljava/io/InputStream;->read()I

    move-result v2

    :goto_1
    rsub-int v4, v1, 0x101

    if-ge v3, v4, :cond_0

    invoke-virtual {p2, v2}, Ljava/io/OutputStream;->write(I)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_2
    new-instance p1, Lorg/apache/pdfbox/filter/DecodeResult;

    invoke-direct {p1, p3}, Lorg/apache/pdfbox/filter/DecodeResult;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    return-object p1
.end method

.method public encode(Ljava/io/InputStream;Ljava/io/OutputStream;Lorg/apache/pdfbox/cos/COSDictionary;)V
    .locals 0

    const-string p1, "PdfBoxAndroid"

    const-string p2, "RunLengthDecodeFilter.encode is not implemented yet, skipping this stream."

    invoke-static {p1, p2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.class final Lorg/apache/pdfbox/filter/FlateFilter;
.super Lorg/apache/pdfbox/filter/Filter;
.source "SourceFile"


# static fields
.field private static final BUFFER_SIZE:I = 0x3fdc


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lorg/apache/pdfbox/filter/Filter;-><init>()V

    return-void
.end method

.method private static decompress(Ljava/io/InputStream;Ljava/io/OutputStream;)V
    .locals 5

    const/16 v0, 0x800

    new-array v1, v0, [B

    invoke-virtual {p0, v1}, Ljava/io/InputStream;->read([B)I

    move-result v2

    if-lez v2, :cond_2

    new-instance v3, Ljava/util/zip/Inflater;

    invoke-direct {v3}, Ljava/util/zip/Inflater;-><init>()V

    const/4 v4, 0x0

    invoke-virtual {v3, v1, v4, v2}, Ljava/util/zip/Inflater;->setInput([BII)V

    new-array v0, v0, [B

    :goto_0
    invoke-virtual {v3, v0}, Ljava/util/zip/Inflater;->inflate([B)I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p1, v0, v4, v2}, Ljava/io/OutputStream;->write([BII)V

    goto :goto_0

    :cond_0
    invoke-virtual {v3}, Ljava/util/zip/Inflater;->finished()Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {v3}, Ljava/util/zip/Inflater;->needsDictionary()Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {p0}, Ljava/io/InputStream;->available()I

    move-result v2

    if-nez v2, :cond_1

    goto :goto_1

    :cond_1
    invoke-virtual {p0, v1}, Ljava/io/InputStream;->read([B)I

    move-result v2

    invoke-virtual {v3, v1, v4, v2}, Ljava/util/zip/Inflater;->setInput([BII)V

    goto :goto_0

    :cond_2
    :goto_1
    invoke-virtual {p1}, Ljava/io/OutputStream;->flush()V

    return-void
.end method


# virtual methods
.method public decode(Ljava/io/InputStream;Ljava/io/OutputStream;Lorg/apache/pdfbox/cos/COSDictionary;I)Lorg/apache/pdfbox/filter/DecodeResult;
    .locals 7

    invoke-static {p3, p4}, Lorg/apache/pdfbox/filter/Filter;->getDecodeParams(Lorg/apache/pdfbox/cos/COSDictionary;I)Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object p4

    if-eqz p4, :cond_0

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->PREDICTOR:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p4, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->getInt(Lorg/apache/pdfbox/cos/COSName;)I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, -0x1

    :goto_0
    move v1, v0

    const/4 v0, 0x1

    if-le v1, v0, :cond_1

    :try_start_0
    sget-object v2, Lorg/apache/pdfbox/cos/COSName;->COLORS:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p4, v2, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->getInt(Lorg/apache/pdfbox/cos/COSName;I)I

    move-result v2

    const/16 v3, 0x20

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    sget-object v3, Lorg/apache/pdfbox/cos/COSName;->BITS_PER_COMPONENT:Lorg/apache/pdfbox/cos/COSName;

    const/16 v4, 0x8

    invoke-virtual {p4, v3, v4}, Lorg/apache/pdfbox/cos/COSDictionary;->getInt(Lorg/apache/pdfbox/cos/COSName;I)I

    move-result v3

    sget-object v4, Lorg/apache/pdfbox/cos/COSName;->COLUMNS:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p4, v4, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->getInt(Lorg/apache/pdfbox/cos/COSName;I)I

    move-result v4

    new-instance p4, Ljava/io/ByteArrayOutputStream;

    invoke-direct {p4}, Ljava/io/ByteArrayOutputStream;-><init>()V

    invoke-static {p1, p4}, Lorg/apache/pdfbox/filter/FlateFilter;->decompress(Ljava/io/InputStream;Ljava/io/OutputStream;)V

    new-instance p1, Ljava/io/ByteArrayInputStream;

    invoke-virtual {p4}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    move-object v5, p1

    move-object v6, p2

    invoke-static/range {v1 .. v6}, Lorg/apache/pdfbox/filter/Predictor;->decodePredictor(IIIILjava/io/InputStream;Ljava/io/OutputStream;)V

    invoke-virtual {p2}, Ljava/io/OutputStream;->flush()V

    invoke-virtual {p4}, Ljava/io/ByteArrayOutputStream;->reset()V

    invoke-virtual {p1}, Ljava/io/ByteArrayInputStream;->reset()V

    goto :goto_1

    :cond_1
    invoke-static {p1, p2}, Lorg/apache/pdfbox/filter/FlateFilter;->decompress(Ljava/io/InputStream;Ljava/io/OutputStream;)V
    :try_end_0
    .catch Ljava/util/zip/DataFormatException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    new-instance p1, Lorg/apache/pdfbox/filter/DecodeResult;

    invoke-direct {p1, p3}, Lorg/apache/pdfbox/filter/DecodeResult;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    return-object p1

    :catch_0
    move-exception p1

    const-string p2, "PdfBoxAndroid"

    const-string p3, "FlateFilter: stop reading corrupt stream due to a DataFormatException"

    invoke-static {p2, p3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    new-instance p2, Ljava/io/IOException;

    invoke-direct {p2, p1}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw p2
.end method

.method public encode(Ljava/io/InputStream;Ljava/io/OutputStream;Lorg/apache/pdfbox/cos/COSDictionary;)V
    .locals 6

    new-instance p3, Ljava/util/zip/DeflaterOutputStream;

    invoke-direct {p3, p2}, Ljava/util/zip/DeflaterOutputStream;-><init>(Ljava/io/OutputStream;)V

    invoke-virtual {p1}, Ljava/io/InputStream;->available()I

    move-result v0

    if-lez v0, :cond_0

    const/16 v1, 0x3fdc

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v2

    new-array v2, v2, [B

    :goto_0
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v3

    const/4 v4, 0x0

    invoke-virtual {p1, v2, v4, v3}, Ljava/io/InputStream;->read([BII)I

    move-result v3

    const/4 v5, -0x1

    if-eq v3, v5, :cond_0

    invoke-virtual {p3, v2, v4, v3}, Ljava/util/zip/DeflaterOutputStream;->write([BII)V

    goto :goto_0

    :cond_0
    invoke-virtual {p3}, Ljava/util/zip/DeflaterOutputStream;->close()V

    invoke-virtual {p2}, Ljava/io/OutputStream;->flush()V

    return-void
.end method

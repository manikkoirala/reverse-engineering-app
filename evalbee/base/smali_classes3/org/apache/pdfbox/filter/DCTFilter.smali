.class final Lorg/apache/pdfbox/filter/DCTFilter;
.super Lorg/apache/pdfbox/filter/Filter;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lorg/apache/pdfbox/filter/Filter;-><init>()V

    return-void
.end method

.method private static clamp(F)I
    .locals 2

    const/4 v0, 0x0

    cmpg-float v1, p0, v0

    if-gez v1, :cond_0

    :goto_0
    move p0, v0

    goto :goto_1

    :cond_0
    const/high16 v0, 0x437f0000    # 255.0f

    cmpl-float v1, p0, v0

    if-lez v1, :cond_1

    goto :goto_0

    :cond_1
    :goto_1
    float-to-int p0, p0

    return p0
.end method


# virtual methods
.method public decode(Ljava/io/InputStream;Ljava/io/OutputStream;Lorg/apache/pdfbox/cos/COSDictionary;I)Lorg/apache/pdfbox/filter/DecodeResult;
    .locals 0

    invoke-static {p1, p2}, Lorg/apache/pdfbox/io/IOUtils;->copy(Ljava/io/InputStream;Ljava/io/OutputStream;)J

    new-instance p1, Lorg/apache/pdfbox/filter/DecodeResult;

    invoke-direct {p1, p3}, Lorg/apache/pdfbox/filter/DecodeResult;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    return-object p1
.end method

.method public encode(Ljava/io/InputStream;Ljava/io/OutputStream;Lorg/apache/pdfbox/cos/COSDictionary;)V
    .locals 0

    const-string p1, "PdfBoxAndroid"

    const-string p2, "DCTFilter#encode is not implemented yet, skipping this stream."

    invoke-static {p1, p2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.class public final Lorg/apache/pdfbox/filter/DecodeResult;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final DEFAULT:Lorg/apache/pdfbox/filter/DecodeResult;


# instance fields
.field private final parameters:Lorg/apache/pdfbox/cos/COSDictionary;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    new-instance v0, Lorg/apache/pdfbox/filter/DecodeResult;

    new-instance v1, Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-direct {v1}, Lorg/apache/pdfbox/cos/COSDictionary;-><init>()V

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/filter/DecodeResult;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    sput-object v0, Lorg/apache/pdfbox/filter/DecodeResult;->DEFAULT:Lorg/apache/pdfbox/filter/DecodeResult;

    return-void
.end method

.method public constructor <init>(Lorg/apache/pdfbox/cos/COSDictionary;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lorg/apache/pdfbox/filter/DecodeResult;->parameters:Lorg/apache/pdfbox/cos/COSDictionary;

    return-void
.end method


# virtual methods
.method public getParameters()Lorg/apache/pdfbox/cos/COSDictionary;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/filter/DecodeResult;->parameters:Lorg/apache/pdfbox/cos/COSDictionary;

    return-object v0
.end method

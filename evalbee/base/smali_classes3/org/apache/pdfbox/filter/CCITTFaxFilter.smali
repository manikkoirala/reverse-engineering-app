.class final Lorg/apache/pdfbox/filter/CCITTFaxFilter;
.super Lorg/apache/pdfbox/filter/Filter;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lorg/apache/pdfbox/filter/Filter;-><init>()V

    return-void
.end method

.method private static invertBitmap([B)V
    .locals 3

    array-length v0, p0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    aget-byte v2, p0, v1

    not-int v2, v2

    and-int/lit16 v2, v2, 0xff

    int-to-byte v2, v2

    aput-byte v2, p0, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method public decode(Ljava/io/InputStream;Ljava/io/OutputStream;Lorg/apache/pdfbox/cos/COSDictionary;I)Lorg/apache/pdfbox/filter/DecodeResult;
    .locals 14

    move-object/from16 v0, p3

    new-instance v1, Lorg/apache/pdfbox/filter/DecodeResult;

    new-instance v2, Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-direct {v2}, Lorg/apache/pdfbox/cos/COSDictionary;-><init>()V

    invoke-direct {v1, v2}, Lorg/apache/pdfbox/filter/DecodeResult;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    invoke-virtual {v1}, Lorg/apache/pdfbox/filter/DecodeResult;->getParameters()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v2

    invoke-virtual {v2, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->addAll(Lorg/apache/pdfbox/cos/COSDictionary;)V

    invoke-static/range {p3 .. p4}, Lorg/apache/pdfbox/filter/Filter;->getDecodeParams(Lorg/apache/pdfbox/cos/COSDictionary;I)Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v2

    sget-object v3, Lorg/apache/pdfbox/cos/COSName;->COLUMNS:Lorg/apache/pdfbox/cos/COSName;

    const/16 v4, 0x6c0

    invoke-virtual {v2, v3, v4}, Lorg/apache/pdfbox/cos/COSDictionary;->getInt(Lorg/apache/pdfbox/cos/COSName;I)I

    move-result v3

    sget-object v4, Lorg/apache/pdfbox/cos/COSName;->ROWS:Lorg/apache/pdfbox/cos/COSName;

    const/4 v5, 0x0

    invoke-virtual {v2, v4, v5}, Lorg/apache/pdfbox/cos/COSDictionary;->getInt(Lorg/apache/pdfbox/cos/COSName;I)I

    move-result v4

    sget-object v6, Lorg/apache/pdfbox/cos/COSName;->HEIGHT:Lorg/apache/pdfbox/cos/COSName;

    sget-object v7, Lorg/apache/pdfbox/cos/COSName;->H:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v6, v7, v5}, Lorg/apache/pdfbox/cos/COSDictionary;->getInt(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSName;I)I

    move-result v6

    if-lez v4, :cond_0

    if-lez v6, :cond_0

    invoke-static {v4, v6}, Ljava/lang/Math;->min(II)I

    move-result v4

    goto :goto_0

    :cond_0
    invoke-static {v4, v6}, Ljava/lang/Math;->max(II)I

    move-result v4

    :goto_0
    move v10, v4

    sget-object v4, Lorg/apache/pdfbox/cos/COSName;->K:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v2, v4, v5}, Lorg/apache/pdfbox/cos/COSDictionary;->getInt(Lorg/apache/pdfbox/cos/COSName;I)I

    move-result v4

    sget-object v6, Lorg/apache/pdfbox/cos/COSName;->ENCODED_BYTE_ALIGN:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v2, v6, v5}, Lorg/apache/pdfbox/cos/COSDictionary;->getBoolean(Lorg/apache/pdfbox/cos/COSName;Z)Z

    move-result v13

    add-int/lit8 v6, v3, 0x7

    div-int/lit8 v6, v6, 0x8

    mul-int/2addr v6, v10

    new-instance v7, Lorg/apache/pdfbox/filter/ccitt/TIFFFaxDecoder;

    const/4 v8, 0x1

    invoke-direct {v7, v8, v3, v10}, Lorg/apache/pdfbox/filter/ccitt/TIFFFaxDecoder;-><init>(III)V

    const-wide/16 v11, 0x0

    invoke-static {p1}, Lorg/apache/pdfbox/io/IOUtils;->toByteArray(Ljava/io/InputStream;)[B

    move-result-object v8

    if-nez v4, :cond_1

    new-instance v4, Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream;

    new-instance v6, Ljava/io/ByteArrayInputStream;

    invoke-direct {v6, v8}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-direct {v4, v6, v3, v13}, Lorg/apache/pdfbox/filter/ccitt/CCITTFaxG31DDecodeInputStream;-><init>(Ljava/io/InputStream;IZ)V

    new-instance v3, Lorg/apache/pdfbox/filter/ccitt/FillOrderChangeInputStream;

    invoke-direct {v3, v4}, Lorg/apache/pdfbox/filter/ccitt/FillOrderChangeInputStream;-><init>(Ljava/io/InputStream;)V

    invoke-static {v3}, Lorg/apache/pdfbox/io/IOUtils;->toByteArray(Ljava/io/InputStream;)[B

    move-result-object v4

    invoke-virtual {v3}, Ljava/io/InputStream;->close()V

    goto :goto_1

    :cond_1
    if-lez v4, :cond_2

    new-array v4, v6, [B

    const/4 v9, 0x0

    const-wide/16 v11, 0x0

    move-object v6, v7

    move-object v7, v4

    invoke-virtual/range {v6 .. v12}, Lorg/apache/pdfbox/filter/ccitt/TIFFFaxDecoder;->decode2D([B[BIIJ)V

    goto :goto_1

    :cond_2
    if-gez v4, :cond_3

    new-array v4, v6, [B

    const/4 v9, 0x0

    move-object v6, v7

    move-object v7, v4

    invoke-virtual/range {v6 .. v13}, Lorg/apache/pdfbox/filter/ccitt/TIFFFaxDecoder;->decodeT6([B[BIIJZ)V

    goto :goto_1

    :cond_3
    const/4 v4, 0x0

    :goto_1
    sget-object v3, Lorg/apache/pdfbox/cos/COSName;->BLACK_IS_1:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v2, v3, v5}, Lorg/apache/pdfbox/cos/COSDictionary;->getBoolean(Lorg/apache/pdfbox/cos/COSName;Z)Z

    move-result v2

    if-nez v2, :cond_4

    invoke-static {v4}, Lorg/apache/pdfbox/filter/CCITTFaxFilter;->invertBitmap([B)V

    :cond_4
    sget-object v2, Lorg/apache/pdfbox/cos/COSName;->COLORSPACE:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->containsKey(Lorg/apache/pdfbox/cos/COSName;)Z

    move-result v3

    if-nez v3, :cond_5

    invoke-virtual {v1}, Lorg/apache/pdfbox/filter/DecodeResult;->getParameters()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v1

    sget-object v3, Lorg/apache/pdfbox/cos/COSName;->DEVICEGRAY:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v3}, Lorg/apache/pdfbox/cos/COSName;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lorg/apache/pdfbox/cos/COSDictionary;->setName(Lorg/apache/pdfbox/cos/COSName;Ljava/lang/String;)V

    :cond_5
    move-object/from16 v1, p2

    invoke-virtual {v1, v4}, Ljava/io/OutputStream;->write([B)V

    new-instance v1, Lorg/apache/pdfbox/filter/DecodeResult;

    invoke-direct {v1, v0}, Lorg/apache/pdfbox/filter/DecodeResult;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    return-object v1
.end method

.method public encode(Ljava/io/InputStream;Ljava/io/OutputStream;Lorg/apache/pdfbox/cos/COSDictionary;)V
    .locals 0

    const-string p1, "PdfBoxAndroid"

    const-string p2, "CCITTFaxDecode.encode is not implemented yet, skipping this stream."

    invoke-static {p1, p2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

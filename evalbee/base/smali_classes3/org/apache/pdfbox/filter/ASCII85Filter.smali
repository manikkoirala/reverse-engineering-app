.class final Lorg/apache/pdfbox/filter/ASCII85Filter;
.super Lorg/apache/pdfbox/filter/Filter;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lorg/apache/pdfbox/filter/Filter;-><init>()V

    return-void
.end method


# virtual methods
.method public decode(Ljava/io/InputStream;Ljava/io/OutputStream;Lorg/apache/pdfbox/cos/COSDictionary;I)Lorg/apache/pdfbox/filter/DecodeResult;
    .locals 4

    const/4 p4, 0x0

    :try_start_0
    new-instance v0, Lorg/apache/pdfbox/filter/ASCII85InputStream;

    invoke-direct {v0, p1}, Lorg/apache/pdfbox/filter/ASCII85InputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    const/16 p1, 0x400

    :try_start_1
    new-array p4, p1, [B

    :goto_0
    const/4 v1, 0x0

    invoke-virtual {v0, p4, v1, p1}, Lorg/apache/pdfbox/filter/ASCII85InputStream;->read([BII)I

    move-result v2

    const/4 v3, -0x1

    if-eq v2, v3, :cond_0

    invoke-virtual {p2, p4, v1, v2}, Ljava/io/OutputStream;->write([BII)V

    goto :goto_0

    :cond_0
    invoke-virtual {p2}, Ljava/io/OutputStream;->flush()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-static {v0}, Lorg/apache/pdfbox/io/IOUtils;->closeQuietly(Ljava/io/Closeable;)V

    new-instance p1, Lorg/apache/pdfbox/filter/DecodeResult;

    invoke-direct {p1, p3}, Lorg/apache/pdfbox/filter/DecodeResult;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    return-object p1

    :catchall_0
    move-exception p1

    move-object p4, v0

    goto :goto_1

    :catchall_1
    move-exception p1

    :goto_1
    invoke-static {p4}, Lorg/apache/pdfbox/io/IOUtils;->closeQuietly(Ljava/io/Closeable;)V

    throw p1
.end method

.method public encode(Ljava/io/InputStream;Ljava/io/OutputStream;Lorg/apache/pdfbox/cos/COSDictionary;)V
    .locals 5

    new-instance p3, Lorg/apache/pdfbox/filter/ASCII85OutputStream;

    invoke-direct {p3, p2}, Lorg/apache/pdfbox/filter/ASCII85OutputStream;-><init>(Ljava/io/OutputStream;)V

    const/16 v0, 0x400

    new-array v1, v0, [B

    :goto_0
    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2, v0}, Ljava/io/InputStream;->read([BII)I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    invoke-virtual {p3, v1, v2, v3}, Ljava/io/OutputStream;->write([BII)V

    goto :goto_0

    :cond_0
    invoke-virtual {p3}, Lorg/apache/pdfbox/filter/ASCII85OutputStream;->close()V

    invoke-virtual {p2}, Ljava/io/OutputStream;->flush()V

    return-void
.end method

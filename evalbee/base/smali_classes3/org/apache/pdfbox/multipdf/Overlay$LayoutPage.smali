.class final Lorg/apache/pdfbox/multipdf/Overlay$LayoutPage;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/pdfbox/multipdf/Overlay;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "LayoutPage"
.end annotation


# instance fields
.field private final overlayContentStream:Lorg/apache/pdfbox/cos/COSStream;

.field private final overlayMediaBox:Lorg/apache/pdfbox/pdmodel/common/PDRectangle;

.field private final overlayResources:Lorg/apache/pdfbox/cos/COSDictionary;


# direct methods
.method private constructor <init>(Lorg/apache/pdfbox/pdmodel/common/PDRectangle;Lorg/apache/pdfbox/cos/COSStream;Lorg/apache/pdfbox/cos/COSDictionary;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lorg/apache/pdfbox/multipdf/Overlay$LayoutPage;->overlayMediaBox:Lorg/apache/pdfbox/pdmodel/common/PDRectangle;

    iput-object p2, p0, Lorg/apache/pdfbox/multipdf/Overlay$LayoutPage;->overlayContentStream:Lorg/apache/pdfbox/cos/COSStream;

    iput-object p3, p0, Lorg/apache/pdfbox/multipdf/Overlay$LayoutPage;->overlayResources:Lorg/apache/pdfbox/cos/COSDictionary;

    return-void
.end method

.method public synthetic constructor <init>(Lorg/apache/pdfbox/pdmodel/common/PDRectangle;Lorg/apache/pdfbox/cos/COSStream;Lorg/apache/pdfbox/cos/COSDictionary;Lorg/apache/pdfbox/multipdf/Overlay$1;)V
    .locals 0

    .line 2
    invoke-direct {p0, p1, p2, p3}, Lorg/apache/pdfbox/multipdf/Overlay$LayoutPage;-><init>(Lorg/apache/pdfbox/pdmodel/common/PDRectangle;Lorg/apache/pdfbox/cos/COSStream;Lorg/apache/pdfbox/cos/COSDictionary;)V

    return-void
.end method

.method public static synthetic access$100(Lorg/apache/pdfbox/multipdf/Overlay$LayoutPage;)Lorg/apache/pdfbox/cos/COSStream;
    .locals 0

    iget-object p0, p0, Lorg/apache/pdfbox/multipdf/Overlay$LayoutPage;->overlayContentStream:Lorg/apache/pdfbox/cos/COSStream;

    return-object p0
.end method

.method public static synthetic access$200(Lorg/apache/pdfbox/multipdf/Overlay$LayoutPage;)Lorg/apache/pdfbox/cos/COSDictionary;
    .locals 0

    iget-object p0, p0, Lorg/apache/pdfbox/multipdf/Overlay$LayoutPage;->overlayResources:Lorg/apache/pdfbox/cos/COSDictionary;

    return-object p0
.end method

.method public static synthetic access$300(Lorg/apache/pdfbox/multipdf/Overlay$LayoutPage;)Lorg/apache/pdfbox/pdmodel/common/PDRectangle;
    .locals 0

    iget-object p0, p0, Lorg/apache/pdfbox/multipdf/Overlay$LayoutPage;->overlayMediaBox:Lorg/apache/pdfbox/pdmodel/common/PDRectangle;

    return-object p0
.end method

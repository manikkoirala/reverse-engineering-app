.class public Lorg/apache/pdfbox/multipdf/LayerUtility;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final DEBUG:Z = true

.field private static final PAGE_TO_FORM_FILTER:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private cloner:Lorg/apache/pdfbox/multipdf/PDFCloneUtility;

.field private targetDoc:Lorg/apache/pdfbox/pdmodel/PDDocument;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    new-instance v0, Ljava/util/HashSet;

    const-string v1, "LastModified"

    const-string v2, "Metadata"

    const-string v3, "Group"

    filled-new-array {v3, v1, v2}, [Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    sput-object v0, Lorg/apache/pdfbox/multipdf/LayerUtility;->PAGE_TO_FORM_FILTER:Ljava/util/Set;

    return-void
.end method

.method public constructor <init>(Lorg/apache/pdfbox/pdmodel/PDDocument;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lorg/apache/pdfbox/multipdf/LayerUtility;->targetDoc:Lorg/apache/pdfbox/pdmodel/PDDocument;

    new-instance v0, Lorg/apache/pdfbox/multipdf/PDFCloneUtility;

    invoke-direct {v0, p1}, Lorg/apache/pdfbox/multipdf/PDFCloneUtility;-><init>(Lorg/apache/pdfbox/pdmodel/PDDocument;)V

    iput-object v0, p0, Lorg/apache/pdfbox/multipdf/LayerUtility;->cloner:Lorg/apache/pdfbox/multipdf/PDFCloneUtility;

    return-void
.end method

.method private transferDict(Lorg/apache/pdfbox/cos/COSDictionary;Lorg/apache/pdfbox/cos/COSDictionary;Ljava/util/Set;Z)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/pdfbox/cos/COSDictionary;",
            "Lorg/apache/pdfbox/cos/COSDictionary;",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;Z)V"
        }
    .end annotation

    invoke-virtual {p1}, Lorg/apache/pdfbox/cos/COSDictionary;->entrySet()Ljava/util/Set;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/pdfbox/cos/COSName;

    if-eqz p4, :cond_0

    invoke-virtual {v1}, Lorg/apache/pdfbox/cos/COSName;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p3, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    goto :goto_0

    :cond_0
    if-nez p4, :cond_1

    invoke-virtual {v1}, Lorg/apache/pdfbox/cos/COSName;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p3, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lorg/apache/pdfbox/multipdf/LayerUtility;->cloner:Lorg/apache/pdfbox/multipdf/PDFCloneUtility;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v2, v0}, Lorg/apache/pdfbox/multipdf/PDFCloneUtility;->cloneForNewDocument(Ljava/lang/Object;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    invoke-virtual {p2, v1, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    goto :goto_0

    :cond_2
    return-void
.end method


# virtual methods
.method public appendFormAsLayer(Lorg/apache/pdfbox/pdmodel/PDPage;Lorg/apache/pdfbox/pdmodel/graphics/form/PDFormXObject;Lorg/apache/pdfbox/util/awt/AffineTransform;Ljava/lang/String;)Lorg/apache/pdfbox/pdmodel/graphics/optionalcontent/PDOptionalContentGroup;
    .locals 4

    iget-object v0, p0, Lorg/apache/pdfbox/multipdf/LayerUtility;->targetDoc:Lorg/apache/pdfbox/pdmodel/PDDocument;

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/PDDocument;->getDocumentCatalog()Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;->getOCProperties()Lorg/apache/pdfbox/pdmodel/graphics/optionalcontent/PDOptionalContentProperties;

    move-result-object v1

    if-nez v1, :cond_0

    new-instance v1, Lorg/apache/pdfbox/pdmodel/graphics/optionalcontent/PDOptionalContentProperties;

    invoke-direct {v1}, Lorg/apache/pdfbox/pdmodel/graphics/optionalcontent/PDOptionalContentProperties;-><init>()V

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;->setOCProperties(Lorg/apache/pdfbox/pdmodel/graphics/optionalcontent/PDOptionalContentProperties;)V

    :cond_0
    invoke-virtual {v1, p4}, Lorg/apache/pdfbox/pdmodel/graphics/optionalcontent/PDOptionalContentProperties;->hasGroup(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Lorg/apache/pdfbox/pdmodel/graphics/optionalcontent/PDOptionalContentGroup;

    invoke-direct {v0, p4}, Lorg/apache/pdfbox/pdmodel/graphics/optionalcontent/PDOptionalContentGroup;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Lorg/apache/pdfbox/pdmodel/graphics/optionalcontent/PDOptionalContentProperties;->addGroup(Lorg/apache/pdfbox/pdmodel/graphics/optionalcontent/PDOptionalContentGroup;)V

    new-instance p4, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;

    iget-object v1, p0, Lorg/apache/pdfbox/multipdf/LayerUtility;->targetDoc:Lorg/apache/pdfbox/pdmodel/PDDocument;

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-direct {p4, v1, p1, v2, v3}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;-><init>(Lorg/apache/pdfbox/pdmodel/PDDocument;Lorg/apache/pdfbox/pdmodel/PDPage;ZZ)V

    sget-object p1, Lorg/apache/pdfbox/cos/COSName;->OC:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p4, p1, v0}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->beginMarkedContent(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/pdmodel/documentinterchange/markedcontent/PDPropertyList;)V

    invoke-virtual {p4}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->saveGraphicsState()V

    new-instance p1, Lorg/apache/pdfbox/util/Matrix;

    invoke-direct {p1, p3}, Lorg/apache/pdfbox/util/Matrix;-><init>(Lorg/apache/pdfbox/util/awt/AffineTransform;)V

    invoke-virtual {p4, p1}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->transform(Lorg/apache/pdfbox/util/Matrix;)V

    invoke-virtual {p4, p2}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->drawForm(Lorg/apache/pdfbox/pdmodel/graphics/form/PDFormXObject;)V

    invoke-virtual {p4}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->restoreGraphicsState()V

    invoke-virtual {p4}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->endMarkedContent()V

    invoke-virtual {p4}, Lorg/apache/pdfbox/pdmodel/PDPageContentStream;->close()V

    return-object v0

    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string p3, "Optional group (layer) already exists: "

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public getDocument()Lorg/apache/pdfbox/pdmodel/PDDocument;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/multipdf/LayerUtility;->targetDoc:Lorg/apache/pdfbox/pdmodel/PDDocument;

    return-object v0
.end method

.method public importPageAsForm(Lorg/apache/pdfbox/pdmodel/PDDocument;I)Lorg/apache/pdfbox/pdmodel/graphics/form/PDFormXObject;
    .locals 0

    .line 1
    invoke-virtual {p1, p2}, Lorg/apache/pdfbox/pdmodel/PDDocument;->getPage(I)Lorg/apache/pdfbox/pdmodel/PDPage;

    move-result-object p2

    invoke-virtual {p0, p1, p2}, Lorg/apache/pdfbox/multipdf/LayerUtility;->importPageAsForm(Lorg/apache/pdfbox/pdmodel/PDDocument;Lorg/apache/pdfbox/pdmodel/PDPage;)Lorg/apache/pdfbox/pdmodel/graphics/form/PDFormXObject;

    move-result-object p1

    return-object p1
.end method

.method public importPageAsForm(Lorg/apache/pdfbox/pdmodel/PDDocument;Lorg/apache/pdfbox/pdmodel/PDPage;)Lorg/apache/pdfbox/pdmodel/graphics/form/PDFormXObject;
    .locals 9

    .line 2
    invoke-virtual {p2}, Lorg/apache/pdfbox/pdmodel/PDPage;->getStream()Lorg/apache/pdfbox/pdmodel/common/PDStream;

    move-result-object p1

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/common/PDStream;->getCOSObject()Lorg/apache/pdfbox/cos/COSBase;

    move-result-object p1

    check-cast p1, Lorg/apache/pdfbox/cos/COSStream;

    new-instance v0, Lorg/apache/pdfbox/pdmodel/common/PDStream;

    iget-object v1, p0, Lorg/apache/pdfbox/multipdf/LayerUtility;->targetDoc:Lorg/apache/pdfbox/pdmodel/PDDocument;

    invoke-virtual {p1}, Lorg/apache/pdfbox/cos/COSStream;->getUnfilteredStream()Ljava/io/InputStream;

    move-result-object p1

    const/4 v2, 0x0

    invoke-direct {v0, v1, p1, v2}, Lorg/apache/pdfbox/pdmodel/common/PDStream;-><init>(Lorg/apache/pdfbox/pdmodel/PDDocument;Ljava/io/InputStream;Z)V

    new-instance p1, Lorg/apache/pdfbox/pdmodel/graphics/form/PDFormXObject;

    invoke-direct {p1, v0}, Lorg/apache/pdfbox/pdmodel/graphics/form/PDFormXObject;-><init>(Lorg/apache/pdfbox/pdmodel/common/PDStream;)V

    invoke-virtual {p2}, Lorg/apache/pdfbox/pdmodel/PDPage;->getResources()Lorg/apache/pdfbox/pdmodel/PDResources;

    move-result-object v0

    new-instance v1, Lorg/apache/pdfbox/pdmodel/PDResources;

    invoke-direct {v1}, Lorg/apache/pdfbox/pdmodel/PDResources;-><init>()V

    iget-object v2, p0, Lorg/apache/pdfbox/multipdf/LayerUtility;->cloner:Lorg/apache/pdfbox/multipdf/PDFCloneUtility;

    invoke-virtual {v2, v0, v1}, Lorg/apache/pdfbox/multipdf/PDFCloneUtility;->cloneMerge(Lorg/apache/pdfbox/pdmodel/common/COSObjectable;Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)V

    invoke-virtual {p1, v1}, Lorg/apache/pdfbox/pdmodel/graphics/form/PDFormXObject;->setResources(Lorg/apache/pdfbox/pdmodel/PDResources;)V

    invoke-virtual {p2}, Lorg/apache/pdfbox/pdmodel/PDPage;->getCOSObject()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/graphics/PDXObject;->getCOSStream()Lorg/apache/pdfbox/cos/COSStream;

    move-result-object v1

    sget-object v2, Lorg/apache/pdfbox/multipdf/LayerUtility;->PAGE_TO_FORM_FILTER:Ljava/util/Set;

    const/4 v3, 0x1

    invoke-direct {p0, v0, v1, v2, v3}, Lorg/apache/pdfbox/multipdf/LayerUtility;->transferDict(Lorg/apache/pdfbox/cos/COSDictionary;Lorg/apache/pdfbox/cos/COSDictionary;Ljava/util/Set;Z)V

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/graphics/form/PDFormXObject;->getMatrix()Lorg/apache/pdfbox/util/Matrix;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/pdfbox/util/Matrix;->createAffineTransform()Lorg/apache/pdfbox/util/awt/AffineTransform;

    move-result-object v0

    invoke-virtual {p2}, Lorg/apache/pdfbox/pdmodel/PDPage;->getMediaBox()Lorg/apache/pdfbox/pdmodel/common/PDRectangle;

    move-result-object v1

    invoke-virtual {p2}, Lorg/apache/pdfbox/pdmodel/PDPage;->getCropBox()Lorg/apache/pdfbox/pdmodel/common/PDRectangle;

    move-result-object v2

    if-eqz v2, :cond_0

    goto :goto_0

    :cond_0
    move-object v2, v1

    :goto_0
    invoke-virtual {p2}, Lorg/apache/pdfbox/pdmodel/PDPage;->getRotation()I

    move-result p2

    invoke-virtual {v1}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->getLowerLeftX()F

    move-result v3

    invoke-virtual {v2}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->getLowerLeftX()F

    move-result v4

    sub-float/2addr v3, v4

    float-to-double v3, v3

    invoke-virtual {v1}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->getLowerLeftY()F

    move-result v1

    invoke-virtual {v2}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->getLowerLeftY()F

    move-result v5

    sub-float/2addr v1, v5

    float-to-double v5, v1

    invoke-virtual {v0, v3, v4, v5, v6}, Lorg/apache/pdfbox/util/awt/AffineTransform;->translate(DD)V

    const/16 v1, 0x5a

    const-wide/16 v3, 0x0

    if-eq p2, v1, :cond_3

    const/16 v1, 0xb4

    if-eq p2, v1, :cond_2

    const/16 v1, 0x10e

    if-eq p2, v1, :cond_1

    goto :goto_2

    :cond_1
    invoke-virtual {v2}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->getWidth()F

    move-result p2

    invoke-virtual {v2}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->getHeight()F

    move-result v1

    div-float/2addr p2, v1

    float-to-double v5, p2

    invoke-virtual {v2}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->getHeight()F

    move-result p2

    invoke-virtual {v2}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->getWidth()F

    move-result v1

    div-float/2addr p2, v1

    float-to-double v7, p2

    invoke-virtual {v0, v5, v6, v7, v8}, Lorg/apache/pdfbox/util/awt/AffineTransform;->scale(DD)V

    invoke-virtual {v2}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->getHeight()F

    move-result p2

    float-to-double v5, p2

    invoke-virtual {v0, v5, v6, v3, v4}, Lorg/apache/pdfbox/util/awt/AffineTransform;->translate(DD)V

    const-wide v3, -0x3fed268380000000L    # -4.71238899230957

    goto :goto_1

    :cond_2
    invoke-virtual {v2}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->getWidth()F

    move-result p2

    float-to-double v3, p2

    invoke-virtual {v2}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->getHeight()F

    move-result p2

    float-to-double v5, p2

    invoke-virtual {v0, v3, v4, v5, v6}, Lorg/apache/pdfbox/util/awt/AffineTransform;->translate(DD)V

    const-wide v3, -0x3ff6de04a0000000L    # -3.1415927410125732

    goto :goto_1

    :cond_3
    invoke-virtual {v2}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->getWidth()F

    move-result p2

    invoke-virtual {v2}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->getHeight()F

    move-result v1

    div-float/2addr p2, v1

    float-to-double v5, p2

    invoke-virtual {v2}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->getHeight()F

    move-result p2

    invoke-virtual {v2}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->getWidth()F

    move-result v1

    div-float/2addr p2, v1

    float-to-double v7, p2

    invoke-virtual {v0, v5, v6, v7, v8}, Lorg/apache/pdfbox/util/awt/AffineTransform;->scale(DD)V

    invoke-virtual {v2}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->getWidth()F

    move-result p2

    float-to-double v5, p2

    invoke-virtual {v0, v3, v4, v5, v6}, Lorg/apache/pdfbox/util/awt/AffineTransform;->translate(DD)V

    const-wide v3, -0x4006de04a0000000L    # -1.5707963705062866

    :goto_1
    invoke-virtual {v0, v3, v4}, Lorg/apache/pdfbox/util/awt/AffineTransform;->rotate(D)V

    :goto_2
    invoke-virtual {v2}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->getLowerLeftX()F

    move-result p2

    neg-float p2, p2

    float-to-double v3, p2

    invoke-virtual {v2}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->getLowerLeftY()F

    move-result p2

    neg-float p2, p2

    float-to-double v5, p2

    invoke-virtual {v0, v3, v4, v5, v6}, Lorg/apache/pdfbox/util/awt/AffineTransform;->translate(DD)V

    invoke-virtual {v0}, Lorg/apache/pdfbox/util/awt/AffineTransform;->isIdentity()Z

    move-result p2

    if-nez p2, :cond_4

    invoke-virtual {p1, v0}, Lorg/apache/pdfbox/pdmodel/graphics/form/PDFormXObject;->setMatrix(Lorg/apache/pdfbox/util/awt/AffineTransform;)V

    :cond_4
    new-instance p2, Lorg/apache/fontbox/util/BoundingBox;

    invoke-direct {p2}, Lorg/apache/fontbox/util/BoundingBox;-><init>()V

    invoke-virtual {v2}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->getLowerLeftX()F

    move-result v0

    invoke-virtual {p2, v0}, Lorg/apache/fontbox/util/BoundingBox;->setLowerLeftX(F)V

    invoke-virtual {v2}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->getLowerLeftY()F

    move-result v0

    invoke-virtual {p2, v0}, Lorg/apache/fontbox/util/BoundingBox;->setLowerLeftY(F)V

    invoke-virtual {v2}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->getUpperRightX()F

    move-result v0

    invoke-virtual {p2, v0}, Lorg/apache/fontbox/util/BoundingBox;->setUpperRightX(F)V

    invoke-virtual {v2}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->getUpperRightY()F

    move-result v0

    invoke-virtual {p2, v0}, Lorg/apache/fontbox/util/BoundingBox;->setUpperRightY(F)V

    new-instance v0, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;

    invoke-direct {v0, p2}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;-><init>(Lorg/apache/fontbox/util/BoundingBox;)V

    invoke-virtual {p1, v0}, Lorg/apache/pdfbox/pdmodel/graphics/form/PDFormXObject;->setBBox(Lorg/apache/pdfbox/pdmodel/common/PDRectangle;)V

    return-object p1
.end method

.method public wrapInSaveRestore(Lorg/apache/pdfbox/pdmodel/PDPage;)V
    .locals 5

    new-instance v0, Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-direct {v0}, Lorg/apache/pdfbox/cos/COSDictionary;-><init>()V

    invoke-virtual {p0}, Lorg/apache/pdfbox/multipdf/LayerUtility;->getDocument()Lorg/apache/pdfbox/pdmodel/PDDocument;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/pdfbox/pdmodel/PDDocument;->getDocument()Lorg/apache/pdfbox/cos/COSDocument;

    move-result-object v1

    invoke-virtual {v1, v0}, Lorg/apache/pdfbox/cos/COSDocument;->createCOSStream(Lorg/apache/pdfbox/cos/COSDictionary;)Lorg/apache/pdfbox/cos/COSStream;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/pdfbox/cos/COSStream;->createUnfilteredStream()Ljava/io/OutputStream;

    move-result-object v2

    const-string v3, "q\n"

    const-string v4, "ISO-8859-1"

    invoke-virtual {v3, v4}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/io/OutputStream;->write([B)V

    invoke-virtual {v2}, Ljava/io/OutputStream;->flush()V

    invoke-virtual {p0}, Lorg/apache/pdfbox/multipdf/LayerUtility;->getDocument()Lorg/apache/pdfbox/pdmodel/PDDocument;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/pdfbox/pdmodel/PDDocument;->getDocument()Lorg/apache/pdfbox/cos/COSDocument;

    move-result-object v2

    invoke-virtual {v2, v0}, Lorg/apache/pdfbox/cos/COSDocument;->createCOSStream(Lorg/apache/pdfbox/cos/COSDictionary;)Lorg/apache/pdfbox/cos/COSStream;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSStream;->createUnfilteredStream()Ljava/io/OutputStream;

    move-result-object v2

    const-string v3, "Q\n"

    invoke-virtual {v3, v4}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/io/OutputStream;->write([B)V

    invoke-virtual {v2}, Ljava/io/OutputStream;->flush()V

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/PDPage;->getCOSObject()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object p1

    sget-object v2, Lorg/apache/pdfbox/cos/COSName;->CONTENTS:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v3

    instance-of v4, v3, Lorg/apache/pdfbox/cos/COSStream;

    if-eqz v4, :cond_0

    check-cast v3, Lorg/apache/pdfbox/cos/COSStream;

    new-instance v4, Lorg/apache/pdfbox/cos/COSArray;

    invoke-direct {v4}, Lorg/apache/pdfbox/cos/COSArray;-><init>()V

    invoke-virtual {v4, v1}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    invoke-virtual {v4, v3}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    invoke-virtual {v4, v0}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    invoke-virtual {p1, v2, v4}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    goto :goto_0

    :cond_0
    instance-of p1, v3, Lorg/apache/pdfbox/cos/COSArray;

    if-eqz p1, :cond_1

    check-cast v3, Lorg/apache/pdfbox/cos/COSArray;

    const/4 p1, 0x0

    invoke-virtual {v3, p1, v1}, Lorg/apache/pdfbox/cos/COSArray;->add(ILorg/apache/pdfbox/cos/COSBase;)V

    invoke-virtual {v3, v0}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    :goto_0
    return-void

    :cond_1
    new-instance p1, Ljava/io/IOException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Contents are unknown type: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

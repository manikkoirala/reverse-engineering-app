.class public Lorg/apache/pdfbox/multipdf/Splitter;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private currentDestinationDocument:Lorg/apache/pdfbox/pdmodel/PDDocument;

.field private currentPageNumber:I

.field private destinationDocuments:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lorg/apache/pdfbox/pdmodel/PDDocument;",
            ">;"
        }
    .end annotation
.end field

.field private endPage:I

.field private sourceDocument:Lorg/apache/pdfbox/pdmodel/PDDocument;

.field private splitLength:I

.field private startPage:I


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lorg/apache/pdfbox/multipdf/Splitter;->splitLength:I

    const/high16 v0, -0x80000000

    iput v0, p0, Lorg/apache/pdfbox/multipdf/Splitter;->startPage:I

    const v0, 0x7fffffff

    iput v0, p0, Lorg/apache/pdfbox/multipdf/Splitter;->endPage:I

    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/pdfbox/multipdf/Splitter;->currentPageNumber:I

    return-void
.end method

.method private createNewDocumentIfNecessary()V
    .locals 2

    iget v0, p0, Lorg/apache/pdfbox/multipdf/Splitter;->currentPageNumber:I

    invoke-virtual {p0, v0}, Lorg/apache/pdfbox/multipdf/Splitter;->splitAtPage(I)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/pdfbox/multipdf/Splitter;->currentDestinationDocument:Lorg/apache/pdfbox/pdmodel/PDDocument;

    if-nez v0, :cond_1

    :cond_0
    invoke-virtual {p0}, Lorg/apache/pdfbox/multipdf/Splitter;->createNewDocument()Lorg/apache/pdfbox/pdmodel/PDDocument;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/pdfbox/multipdf/Splitter;->currentDestinationDocument:Lorg/apache/pdfbox/pdmodel/PDDocument;

    iget-object v1, p0, Lorg/apache/pdfbox/multipdf/Splitter;->destinationDocuments:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    return-void
.end method

.method private processAnnotations(Lorg/apache/pdfbox/pdmodel/PDPage;)V
    .locals 4

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/PDPage;->getAnnotations()Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotation;

    instance-of v1, v0, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotationLink;

    const/4 v2, 0x0

    if-eqz v1, :cond_2

    check-cast v0, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotationLink;

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotationLink;->getDestination()Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/destination/PDDestination;

    move-result-object v1

    if-nez v1, :cond_1

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotationLink;->getAction()Lorg/apache/pdfbox/pdmodel/interactive/action/PDAction;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotationLink;->getAction()Lorg/apache/pdfbox/pdmodel/interactive/action/PDAction;

    move-result-object v0

    instance-of v3, v0, Lorg/apache/pdfbox/pdmodel/interactive/action/PDActionGoTo;

    if-eqz v3, :cond_1

    check-cast v0, Lorg/apache/pdfbox/pdmodel/interactive/action/PDActionGoTo;

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/interactive/action/PDActionGoTo;->getDestination()Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/destination/PDDestination;

    move-result-object v1

    :cond_1
    instance-of v0, v1, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/destination/PDPageDestination;

    if-eqz v0, :cond_0

    check-cast v1, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/destination/PDPageDestination;

    invoke-virtual {v1, v2}, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/destination/PDPageDestination;->setPage(Lorg/apache/pdfbox/pdmodel/PDPage;)V

    goto :goto_0

    :cond_2
    invoke-virtual {v0, v2}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotation;->setPage(Lorg/apache/pdfbox/pdmodel/PDPage;)V

    goto :goto_0

    :cond_3
    return-void
.end method

.method private processPages()V
    .locals 5

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lorg/apache/pdfbox/multipdf/Splitter;->sourceDocument:Lorg/apache/pdfbox/pdmodel/PDDocument;

    invoke-virtual {v1}, Lorg/apache/pdfbox/pdmodel/PDDocument;->getNumberOfPages()I

    move-result v1

    if-ge v0, v1, :cond_2

    iget-object v1, p0, Lorg/apache/pdfbox/multipdf/Splitter;->sourceDocument:Lorg/apache/pdfbox/pdmodel/PDDocument;

    invoke-virtual {v1, v0}, Lorg/apache/pdfbox/pdmodel/PDDocument;->getPage(I)Lorg/apache/pdfbox/pdmodel/PDPage;

    move-result-object v1

    iget v2, p0, Lorg/apache/pdfbox/multipdf/Splitter;->currentPageNumber:I

    add-int/lit8 v3, v2, 0x1

    iget v4, p0, Lorg/apache/pdfbox/multipdf/Splitter;->startPage:I

    if-lt v3, v4, :cond_0

    add-int/lit8 v3, v2, 0x1

    iget v4, p0, Lorg/apache/pdfbox/multipdf/Splitter;->endPage:I

    if-gt v3, v4, :cond_0

    invoke-virtual {p0, v1}, Lorg/apache/pdfbox/multipdf/Splitter;->processPage(Lorg/apache/pdfbox/pdmodel/PDPage;)V

    iget v1, p0, Lorg/apache/pdfbox/multipdf/Splitter;->currentPageNumber:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lorg/apache/pdfbox/multipdf/Splitter;->currentPageNumber:I

    goto :goto_1

    :cond_0
    iget v1, p0, Lorg/apache/pdfbox/multipdf/Splitter;->endPage:I

    if-le v2, v1, :cond_1

    goto :goto_2

    :cond_1
    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lorg/apache/pdfbox/multipdf/Splitter;->currentPageNumber:I

    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    :goto_2
    return-void
.end method


# virtual methods
.method public createNewDocument()Lorg/apache/pdfbox/pdmodel/PDDocument;
    .locals 3

    new-instance v0, Lorg/apache/pdfbox/pdmodel/PDDocument;

    invoke-direct {v0}, Lorg/apache/pdfbox/pdmodel/PDDocument;-><init>()V

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/PDDocument;->getDocument()Lorg/apache/pdfbox/cos/COSDocument;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/pdfbox/multipdf/Splitter;->getSourceDocument()Lorg/apache/pdfbox/pdmodel/PDDocument;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/pdfbox/pdmodel/PDDocument;->getVersion()F

    move-result v2

    invoke-virtual {v1, v2}, Lorg/apache/pdfbox/cos/COSDocument;->setVersion(F)V

    invoke-virtual {p0}, Lorg/apache/pdfbox/multipdf/Splitter;->getSourceDocument()Lorg/apache/pdfbox/pdmodel/PDDocument;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/pdfbox/pdmodel/PDDocument;->getDocumentInformation()Lorg/apache/pdfbox/pdmodel/PDDocumentInformation;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/pdmodel/PDDocument;->setDocumentInformation(Lorg/apache/pdfbox/pdmodel/PDDocumentInformation;)V

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/PDDocument;->getDocumentCatalog()Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;

    move-result-object v1

    invoke-virtual {p0}, Lorg/apache/pdfbox/multipdf/Splitter;->getSourceDocument()Lorg/apache/pdfbox/pdmodel/PDDocument;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/pdfbox/pdmodel/PDDocument;->getDocumentCatalog()Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;->getViewerPreferences()Lorg/apache/pdfbox/pdmodel/interactive/viewerpreferences/PDViewerPreferences;

    move-result-object v2

    invoke-virtual {v1, v2}, Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;->setViewerPreferences(Lorg/apache/pdfbox/pdmodel/interactive/viewerpreferences/PDViewerPreferences;)V

    return-object v0
.end method

.method public final getDestinationDocument()Lorg/apache/pdfbox/pdmodel/PDDocument;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/multipdf/Splitter;->currentDestinationDocument:Lorg/apache/pdfbox/pdmodel/PDDocument;

    return-object v0
.end method

.method public final getSourceDocument()Lorg/apache/pdfbox/pdmodel/PDDocument;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/multipdf/Splitter;->sourceDocument:Lorg/apache/pdfbox/pdmodel/PDDocument;

    return-object v0
.end method

.method public processPage(Lorg/apache/pdfbox/pdmodel/PDPage;)V
    .locals 2

    invoke-direct {p0}, Lorg/apache/pdfbox/multipdf/Splitter;->createNewDocumentIfNecessary()V

    invoke-virtual {p0}, Lorg/apache/pdfbox/multipdf/Splitter;->getDestinationDocument()Lorg/apache/pdfbox/pdmodel/PDDocument;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/pdfbox/pdmodel/PDDocument;->importPage(Lorg/apache/pdfbox/pdmodel/PDPage;)Lorg/apache/pdfbox/pdmodel/PDPage;

    move-result-object v0

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/PDPage;->getCropBox()Lorg/apache/pdfbox/pdmodel/common/PDRectangle;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/pdmodel/PDPage;->setCropBox(Lorg/apache/pdfbox/pdmodel/common/PDRectangle;)V

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/PDPage;->getMediaBox()Lorg/apache/pdfbox/pdmodel/common/PDRectangle;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/pdmodel/PDPage;->setMediaBox(Lorg/apache/pdfbox/pdmodel/common/PDRectangle;)V

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/PDPage;->getResources()Lorg/apache/pdfbox/pdmodel/PDResources;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/pdmodel/PDPage;->setResources(Lorg/apache/pdfbox/pdmodel/PDResources;)V

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/PDPage;->getRotation()I

    move-result p1

    invoke-virtual {v0, p1}, Lorg/apache/pdfbox/pdmodel/PDPage;->setRotation(I)V

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/multipdf/Splitter;->processAnnotations(Lorg/apache/pdfbox/pdmodel/PDPage;)V

    return-void
.end method

.method public setEndPage(I)V
    .locals 1

    if-lez p1, :cond_0

    iput p1, p0, Lorg/apache/pdfbox/multipdf/Splitter;->endPage:I

    return-void

    :cond_0
    new-instance p1, Ljava/lang/RuntimeException;

    const-string v0, "Error split must be at least one page."

    invoke-direct {p1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public setSplitAtPage(I)V
    .locals 1

    if-lez p1, :cond_0

    iput p1, p0, Lorg/apache/pdfbox/multipdf/Splitter;->splitLength:I

    return-void

    :cond_0
    new-instance p1, Ljava/lang/RuntimeException;

    const-string v0, "Error split must be at least one page."

    invoke-direct {p1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public setStartPage(I)V
    .locals 1

    if-lez p1, :cond_0

    iput p1, p0, Lorg/apache/pdfbox/multipdf/Splitter;->startPage:I

    return-void

    :cond_0
    new-instance p1, Ljava/lang/RuntimeException;

    const-string v0, "Error split must be at least one page."

    invoke-direct {p1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public split(Lorg/apache/pdfbox/pdmodel/PDDocument;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/pdfbox/pdmodel/PDDocument;",
            ")",
            "Ljava/util/List<",
            "Lorg/apache/pdfbox/pdmodel/PDDocument;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/apache/pdfbox/multipdf/Splitter;->destinationDocuments:Ljava/util/List;

    iput-object p1, p0, Lorg/apache/pdfbox/multipdf/Splitter;->sourceDocument:Lorg/apache/pdfbox/pdmodel/PDDocument;

    invoke-direct {p0}, Lorg/apache/pdfbox/multipdf/Splitter;->processPages()V

    iget-object p1, p0, Lorg/apache/pdfbox/multipdf/Splitter;->destinationDocuments:Ljava/util/List;

    return-object p1
.end method

.method public splitAtPage(I)Z
    .locals 1

    iget v0, p0, Lorg/apache/pdfbox/multipdf/Splitter;->splitLength:I

    rem-int/2addr p1, v0

    if-nez p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.class Lorg/apache/pdfbox/multipdf/PDFCloneUtility;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final clonedVersion:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Object;",
            "Lorg/apache/pdfbox/cos/COSBase;",
            ">;"
        }
    .end annotation
.end field

.field private final destination:Lorg/apache/pdfbox/pdmodel/PDDocument;


# direct methods
.method public constructor <init>(Lorg/apache/pdfbox/pdmodel/PDDocument;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/pdfbox/multipdf/PDFCloneUtility;->clonedVersion:Ljava/util/Map;

    iput-object p1, p0, Lorg/apache/pdfbox/multipdf/PDFCloneUtility;->destination:Lorg/apache/pdfbox/pdmodel/PDDocument;

    return-void
.end method


# virtual methods
.method public cloneForNewDocument(Ljava/lang/Object;)Lorg/apache/pdfbox/cos/COSBase;
    .locals 5

    if-nez p1, :cond_0

    const/4 p1, 0x0

    return-object p1

    :cond_0
    iget-object v0, p0, Lorg/apache/pdfbox/multipdf/PDFCloneUtility;->clonedVersion:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSBase;

    if-eqz v0, :cond_1

    goto/16 :goto_7

    :cond_1
    instance-of v0, p1, Ljava/util/List;

    if-eqz v0, :cond_2

    new-instance v0, Lorg/apache/pdfbox/cos/COSArray;

    invoke-direct {v0}, Lorg/apache/pdfbox/cos/COSArray;-><init>()V

    move-object v1, p1

    check-cast v1, Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_e

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p0, v2}, Lorg/apache/pdfbox/multipdf/PDFCloneUtility;->cloneForNewDocument(Ljava/lang/Object;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v2

    invoke-virtual {v0, v2}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    goto :goto_0

    :cond_2
    instance-of v0, p1, Lorg/apache/pdfbox/pdmodel/common/COSObjectable;

    if-eqz v0, :cond_3

    instance-of v0, p1, Lorg/apache/pdfbox/cos/COSBase;

    if-nez v0, :cond_3

    move-object v0, p1

    check-cast v0, Lorg/apache/pdfbox/pdmodel/common/COSObjectable;

    invoke-interface {v0}, Lorg/apache/pdfbox/pdmodel/common/COSObjectable;->getCOSObject()Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    goto :goto_1

    :cond_3
    instance-of v0, p1, Lorg/apache/pdfbox/cos/COSObject;

    if-eqz v0, :cond_5

    move-object v0, p1

    check-cast v0, Lorg/apache/pdfbox/cos/COSObject;

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSObject;->getObject()Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    :goto_1
    invoke-virtual {p0, v0}, Lorg/apache/pdfbox/multipdf/PDFCloneUtility;->cloneForNewDocument(Ljava/lang/Object;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    :cond_4
    :goto_2
    iget-object v1, p0, Lorg/apache/pdfbox/multipdf/PDFCloneUtility;->clonedVersion:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_7

    :cond_5
    instance-of v0, p1, Lorg/apache/pdfbox/cos/COSArray;

    const/4 v1, 0x0

    if-eqz v0, :cond_6

    new-instance v0, Lorg/apache/pdfbox/cos/COSArray;

    invoke-direct {v0}, Lorg/apache/pdfbox/cos/COSArray;-><init>()V

    move-object v2, p1

    check-cast v2, Lorg/apache/pdfbox/cos/COSArray;

    :goto_3
    invoke-virtual {v2}, Lorg/apache/pdfbox/cos/COSArray;->size()I

    move-result v3

    if-ge v1, v3, :cond_4

    invoke-virtual {v2, v1}, Lorg/apache/pdfbox/cos/COSArray;->get(I)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v3

    invoke-virtual {p0, v3}, Lorg/apache/pdfbox/multipdf/PDFCloneUtility;->cloneForNewDocument(Ljava/lang/Object;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v3

    invoke-virtual {v0, v3}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    :cond_6
    instance-of v0, p1, Lorg/apache/pdfbox/pdmodel/common/COSStreamArray;

    if-eqz v0, :cond_9

    move-object v0, p1

    check-cast v0, Lorg/apache/pdfbox/pdmodel/common/COSStreamArray;

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSDictionary;->size()I

    move-result v2

    if-gtz v2, :cond_8

    new-instance v2, Lorg/apache/pdfbox/cos/COSArray;

    invoke-direct {v2}, Lorg/apache/pdfbox/cos/COSArray;-><init>()V

    :goto_4
    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/common/COSStreamArray;->getStreamCount()I

    move-result v3

    if-ge v1, v3, :cond_7

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/pdmodel/common/COSStreamArray;->get(I)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v3

    invoke-virtual {p0, v3}, Lorg/apache/pdfbox/multipdf/PDFCloneUtility;->cloneForNewDocument(Ljava/lang/Object;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v3

    invoke-virtual {v2, v3}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    :cond_7
    new-instance v0, Lorg/apache/pdfbox/pdmodel/common/COSStreamArray;

    invoke-direct {v0, v2}, Lorg/apache/pdfbox/pdmodel/common/COSStreamArray;-><init>(Lorg/apache/pdfbox/cos/COSArray;)V

    goto :goto_2

    :cond_8
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "Cannot close stream array with items next to the streams."

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_9
    instance-of v0, p1, Lorg/apache/pdfbox/cos/COSStream;

    if-eqz v0, :cond_b

    move-object v0, p1

    check-cast v0, Lorg/apache/pdfbox/cos/COSStream;

    new-instance v1, Lorg/apache/pdfbox/pdmodel/common/PDStream;

    iget-object v2, p0, Lorg/apache/pdfbox/multipdf/PDFCloneUtility;->destination:Lorg/apache/pdfbox/pdmodel/PDDocument;

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSStream;->getFilteredStream()Ljava/io/InputStream;

    move-result-object v3

    const/4 v4, 0x1

    invoke-direct {v1, v2, v3, v4}, Lorg/apache/pdfbox/pdmodel/common/PDStream;-><init>(Lorg/apache/pdfbox/pdmodel/PDDocument;Ljava/io/InputStream;Z)V

    iget-object v2, p0, Lorg/apache/pdfbox/multipdf/PDFCloneUtility;->clonedVersion:Ljava/util/Map;

    invoke-virtual {v1}, Lorg/apache/pdfbox/pdmodel/common/PDStream;->getStream()Lorg/apache/pdfbox/cos/COSStream;

    move-result-object v3

    invoke-interface {v2, p1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSDictionary;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_5
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_a

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    invoke-virtual {v1}, Lorg/apache/pdfbox/pdmodel/common/PDStream;->getStream()Lorg/apache/pdfbox/cos/COSStream;

    move-result-object v3

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/pdfbox/cos/COSName;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p0, v2}, Lorg/apache/pdfbox/multipdf/PDFCloneUtility;->cloneForNewDocument(Ljava/lang/Object;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v2

    invoke-virtual {v3, v4, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    goto :goto_5

    :cond_a
    invoke-virtual {v1}, Lorg/apache/pdfbox/pdmodel/common/PDStream;->getStream()Lorg/apache/pdfbox/cos/COSStream;

    move-result-object v0

    goto :goto_7

    :cond_b
    instance-of v0, p1, Lorg/apache/pdfbox/cos/COSDictionary;

    if-eqz v0, :cond_d

    move-object v0, p1

    check-cast v0, Lorg/apache/pdfbox/cos/COSDictionary;

    new-instance v1, Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-direct {v1}, Lorg/apache/pdfbox/cos/COSDictionary;-><init>()V

    iget-object v2, p0, Lorg/apache/pdfbox/multipdf/PDFCloneUtility;->clonedVersion:Ljava/util/Map;

    invoke-interface {v2, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSDictionary;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_6
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_c

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/pdfbox/cos/COSName;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p0, v2}, Lorg/apache/pdfbox/multipdf/PDFCloneUtility;->cloneForNewDocument(Ljava/lang/Object;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v2

    invoke-virtual {v1, v3, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    goto :goto_6

    :cond_c
    move-object v0, v1

    goto :goto_7

    :cond_d
    move-object v0, p1

    check-cast v0, Lorg/apache/pdfbox/cos/COSBase;

    :cond_e
    :goto_7
    iget-object v1, p0, Lorg/apache/pdfbox/multipdf/PDFCloneUtility;->clonedVersion:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object v0
.end method

.method public cloneMerge(Lorg/apache/pdfbox/pdmodel/common/COSObjectable;Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)V
    .locals 6

    if-nez p1, :cond_0

    return-void

    :cond_0
    iget-object v0, p0, Lorg/apache/pdfbox/multipdf/PDFCloneUtility;->clonedVersion:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSBase;

    if-eqz v0, :cond_1

    return-void

    :cond_1
    instance-of v1, p1, Ljava/util/List;

    if-eqz v1, :cond_3

    new-instance v1, Lorg/apache/pdfbox/cos/COSArray;

    invoke-direct {v1}, Lorg/apache/pdfbox/cos/COSArray;-><init>()V

    move-object v2, p1

    check-cast v2, Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {p0, v3}, Lorg/apache/pdfbox/multipdf/PDFCloneUtility;->cloneForNewDocument(Ljava/lang/Object;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v3

    invoke-virtual {v1, v3}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    goto :goto_0

    :cond_2
    check-cast p2, Ljava/util/List;

    invoke-interface {p2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_5

    :cond_3
    instance-of v1, p1, Lorg/apache/pdfbox/cos/COSBase;

    if-nez v1, :cond_5

    invoke-interface {p1}, Lorg/apache/pdfbox/pdmodel/common/COSObjectable;->getCOSObject()Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v1

    invoke-interface {p2}, Lorg/apache/pdfbox/pdmodel/common/COSObjectable;->getCOSObject()Lorg/apache/pdfbox/cos/COSBase;

    move-result-object p2

    :goto_1
    invoke-virtual {p0, v1, p2}, Lorg/apache/pdfbox/multipdf/PDFCloneUtility;->cloneMerge(Lorg/apache/pdfbox/pdmodel/common/COSObjectable;Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)V

    :cond_4
    iget-object p2, p0, Lorg/apache/pdfbox/multipdf/PDFCloneUtility;->clonedVersion:Ljava/util/Map;

    invoke-interface {p2, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_5

    :cond_5
    instance-of v1, p1, Lorg/apache/pdfbox/cos/COSObject;

    if-eqz v1, :cond_7

    instance-of v1, p2, Lorg/apache/pdfbox/cos/COSObject;

    if-eqz v1, :cond_6

    move-object v1, p1

    check-cast v1, Lorg/apache/pdfbox/cos/COSObject;

    invoke-virtual {v1}, Lorg/apache/pdfbox/cos/COSObject;->getObject()Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v1

    check-cast p2, Lorg/apache/pdfbox/cos/COSObject;

    invoke-virtual {p2}, Lorg/apache/pdfbox/cos/COSObject;->getObject()Lorg/apache/pdfbox/cos/COSBase;

    move-result-object p2

    goto :goto_1

    :cond_6
    instance-of v1, p2, Lorg/apache/pdfbox/cos/COSDictionary;

    if-eqz v1, :cond_4

    move-object v1, p1

    check-cast v1, Lorg/apache/pdfbox/cos/COSObject;

    invoke-virtual {v1}, Lorg/apache/pdfbox/cos/COSObject;->getObject()Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v1

    goto :goto_1

    :cond_7
    instance-of v1, p1, Lorg/apache/pdfbox/cos/COSArray;

    if-eqz v1, :cond_8

    move-object v1, p1

    check-cast v1, Lorg/apache/pdfbox/cos/COSArray;

    const/4 v2, 0x0

    :goto_2
    invoke-virtual {v1}, Lorg/apache/pdfbox/cos/COSArray;->size()I

    move-result v3

    if-ge v2, v3, :cond_4

    move-object v3, p2

    check-cast v3, Lorg/apache/pdfbox/cos/COSArray;

    invoke-virtual {v1, v2}, Lorg/apache/pdfbox/cos/COSArray;->get(I)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v4

    invoke-virtual {p0, v4}, Lorg/apache/pdfbox/multipdf/PDFCloneUtility;->cloneForNewDocument(Ljava/lang/Object;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v4

    invoke-virtual {v3, v4}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_8
    instance-of v1, p1, Lorg/apache/pdfbox/cos/COSStream;

    if-eqz v1, :cond_a

    move-object p2, p1

    check-cast p2, Lorg/apache/pdfbox/cos/COSStream;

    new-instance v0, Lorg/apache/pdfbox/pdmodel/common/PDStream;

    iget-object v1, p0, Lorg/apache/pdfbox/multipdf/PDFCloneUtility;->destination:Lorg/apache/pdfbox/pdmodel/PDDocument;

    invoke-virtual {p2}, Lorg/apache/pdfbox/cos/COSStream;->getFilteredStream()Ljava/io/InputStream;

    move-result-object v2

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, Lorg/apache/pdfbox/pdmodel/common/PDStream;-><init>(Lorg/apache/pdfbox/pdmodel/PDDocument;Ljava/io/InputStream;Z)V

    iget-object v1, p0, Lorg/apache/pdfbox/multipdf/PDFCloneUtility;->clonedVersion:Ljava/util/Map;

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/common/PDStream;->getStream()Lorg/apache/pdfbox/cos/COSStream;

    move-result-object v2

    invoke-interface {v1, p1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p2}, Lorg/apache/pdfbox/cos/COSDictionary;->entrySet()Ljava/util/Set;

    move-result-object p2

    invoke-interface {p2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :goto_3
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_9

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/common/PDStream;->getStream()Lorg/apache/pdfbox/cos/COSStream;

    move-result-object v2

    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/pdfbox/cos/COSName;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p0, v1}, Lorg/apache/pdfbox/multipdf/PDFCloneUtility;->cloneForNewDocument(Ljava/lang/Object;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v1

    invoke-virtual {v2, v3, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    goto :goto_3

    :cond_9
    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/common/PDStream;->getStream()Lorg/apache/pdfbox/cos/COSStream;

    move-result-object v0

    goto :goto_5

    :cond_a
    instance-of v1, p1, Lorg/apache/pdfbox/cos/COSDictionary;

    if-eqz v1, :cond_c

    move-object v1, p1

    check-cast v1, Lorg/apache/pdfbox/cos/COSDictionary;

    iget-object v2, p0, Lorg/apache/pdfbox/multipdf/PDFCloneUtility;->clonedVersion:Ljava/util/Map;

    invoke-interface {v2, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v1}, Lorg/apache/pdfbox/cos/COSDictionary;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_d

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/pdfbox/cos/COSName;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/pdfbox/cos/COSBase;

    move-object v4, p2

    check-cast v4, Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-virtual {v4, v3}, Lorg/apache/pdfbox/cos/COSDictionary;->getItem(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v5

    if-eqz v5, :cond_b

    invoke-virtual {v4, v3}, Lorg/apache/pdfbox/cos/COSDictionary;->getItem(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v3

    invoke-virtual {p0, v2, v3}, Lorg/apache/pdfbox/multipdf/PDFCloneUtility;->cloneMerge(Lorg/apache/pdfbox/pdmodel/common/COSObjectable;Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)V

    goto :goto_4

    :cond_b
    invoke-virtual {p0, v2}, Lorg/apache/pdfbox/multipdf/PDFCloneUtility;->cloneForNewDocument(Ljava/lang/Object;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v2

    invoke-virtual {v4, v3, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    goto :goto_4

    :cond_c
    move-object v0, p1

    check-cast v0, Lorg/apache/pdfbox/cos/COSBase;

    :cond_d
    :goto_5
    iget-object p2, p0, Lorg/apache/pdfbox/multipdf/PDFCloneUtility;->clonedVersion:Ljava/util/Map;

    invoke-interface {p2, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public getDestination()Lorg/apache/pdfbox/pdmodel/PDDocument;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/multipdf/PDFCloneUtility;->destination:Lorg/apache/pdfbox/pdmodel/PDDocument;

    return-object v0
.end method

.class public Lorg/apache/pdfbox/multipdf/Overlay;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/pdfbox/multipdf/Overlay$LayoutPage;,
        Lorg/apache/pdfbox/multipdf/Overlay$Position;
    }
.end annotation


# instance fields
.field private allPagesOverlay:Lorg/apache/pdfbox/pdmodel/PDDocument;

.field private allPagesOverlayFilename:Ljava/lang/String;

.field private defaultOverlay:Lorg/apache/pdfbox/pdmodel/PDDocument;

.field private defaultOverlayFilename:Ljava/lang/String;

.field private defaultOverlayPage:Lorg/apache/pdfbox/multipdf/Overlay$LayoutPage;

.field private evenPageOverlay:Lorg/apache/pdfbox/pdmodel/PDDocument;

.field private evenPageOverlayFilename:Ljava/lang/String;

.field private evenPageOverlayPage:Lorg/apache/pdfbox/multipdf/Overlay$LayoutPage;

.field private firstPageOverlay:Lorg/apache/pdfbox/pdmodel/PDDocument;

.field private firstPageOverlayFilename:Ljava/lang/String;

.field private firstPageOverlayPage:Lorg/apache/pdfbox/multipdf/Overlay$LayoutPage;

.field private inputFileName:Ljava/lang/String;

.field private inputPDFDocument:Lorg/apache/pdfbox/pdmodel/PDDocument;

.field private lastPageOverlay:Lorg/apache/pdfbox/pdmodel/PDDocument;

.field private lastPageOverlayFilename:Ljava/lang/String;

.field private lastPageOverlayPage:Lorg/apache/pdfbox/multipdf/Overlay$LayoutPage;

.field private numberOfOverlayPages:I

.field private oddPageOverlay:Lorg/apache/pdfbox/pdmodel/PDDocument;

.field private oddPageOverlayFilename:Ljava/lang/String;

.field private oddPageOverlayPage:Lorg/apache/pdfbox/multipdf/Overlay$LayoutPage;

.field private outputFilename:Ljava/lang/String;

.field private position:Lorg/apache/pdfbox/multipdf/Overlay$Position;

.field private final specificPageOverlay:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Lorg/apache/pdfbox/pdmodel/PDDocument;",
            ">;"
        }
    .end annotation
.end field

.field private specificPageOverlayPage:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Lorg/apache/pdfbox/multipdf/Overlay$LayoutPage;",
            ">;"
        }
    .end annotation
.end field

.field private useAllOverlayPages:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/pdfbox/multipdf/Overlay;->specificPageOverlay:Ljava/util/Map;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/pdfbox/multipdf/Overlay;->specificPageOverlayPage:Ljava/util/Map;

    sget-object v0, Lorg/apache/pdfbox/multipdf/Overlay$Position;->BACKGROUND:Lorg/apache/pdfbox/multipdf/Overlay$Position;

    iput-object v0, p0, Lorg/apache/pdfbox/multipdf/Overlay;->position:Lorg/apache/pdfbox/multipdf/Overlay$Position;

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/pdfbox/multipdf/Overlay;->inputFileName:Ljava/lang/String;

    iput-object v0, p0, Lorg/apache/pdfbox/multipdf/Overlay;->inputPDFDocument:Lorg/apache/pdfbox/pdmodel/PDDocument;

    iput-object v0, p0, Lorg/apache/pdfbox/multipdf/Overlay;->outputFilename:Ljava/lang/String;

    iput-object v0, p0, Lorg/apache/pdfbox/multipdf/Overlay;->defaultOverlayFilename:Ljava/lang/String;

    iput-object v0, p0, Lorg/apache/pdfbox/multipdf/Overlay;->defaultOverlay:Lorg/apache/pdfbox/pdmodel/PDDocument;

    iput-object v0, p0, Lorg/apache/pdfbox/multipdf/Overlay;->firstPageOverlayFilename:Ljava/lang/String;

    iput-object v0, p0, Lorg/apache/pdfbox/multipdf/Overlay;->firstPageOverlay:Lorg/apache/pdfbox/pdmodel/PDDocument;

    iput-object v0, p0, Lorg/apache/pdfbox/multipdf/Overlay;->lastPageOverlayFilename:Ljava/lang/String;

    iput-object v0, p0, Lorg/apache/pdfbox/multipdf/Overlay;->lastPageOverlay:Lorg/apache/pdfbox/pdmodel/PDDocument;

    iput-object v0, p0, Lorg/apache/pdfbox/multipdf/Overlay;->allPagesOverlayFilename:Ljava/lang/String;

    iput-object v0, p0, Lorg/apache/pdfbox/multipdf/Overlay;->allPagesOverlay:Lorg/apache/pdfbox/pdmodel/PDDocument;

    iput-object v0, p0, Lorg/apache/pdfbox/multipdf/Overlay;->oddPageOverlayFilename:Ljava/lang/String;

    iput-object v0, p0, Lorg/apache/pdfbox/multipdf/Overlay;->oddPageOverlay:Lorg/apache/pdfbox/pdmodel/PDDocument;

    iput-object v0, p0, Lorg/apache/pdfbox/multipdf/Overlay;->evenPageOverlayFilename:Ljava/lang/String;

    iput-object v0, p0, Lorg/apache/pdfbox/multipdf/Overlay;->evenPageOverlay:Lorg/apache/pdfbox/pdmodel/PDDocument;

    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/pdfbox/multipdf/Overlay;->numberOfOverlayPages:I

    iput-boolean v0, p0, Lorg/apache/pdfbox/multipdf/Overlay;->useAllOverlayPages:Z

    return-void
.end method

.method private addOriginalContent(Lorg/apache/pdfbox/cos/COSBase;Lorg/apache/pdfbox/cos/COSArray;)V
    .locals 2

    instance-of v0, p1, Lorg/apache/pdfbox/cos/COSStream;

    if-eqz v0, :cond_0

    invoke-virtual {p2, p1}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    goto :goto_0

    :cond_0
    instance-of v0, p1, Lorg/apache/pdfbox/cos/COSArray;

    if-eqz v0, :cond_1

    check-cast p1, Lorg/apache/pdfbox/cos/COSArray;

    invoke-virtual {p2, p1}, Lorg/apache/pdfbox/cos/COSArray;->addAll(Lorg/apache/pdfbox/cos/COSArray;)V

    :goto_0
    return-void

    :cond_1
    new-instance p2, Ljava/io/IOException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unknown content type:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p2
.end method

.method private createContentStream(Lorg/apache/pdfbox/cos/COSBase;)Lorg/apache/pdfbox/cos/COSStream;
    .locals 6

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/multipdf/Overlay;->createContentStreamList(Lorg/apache/pdfbox/cos/COSBase;)Ljava/util/List;

    move-result-object p1

    new-instance v0, Lorg/apache/pdfbox/cos/COSStream;

    invoke-direct {v0}, Lorg/apache/pdfbox/cos/COSStream;-><init>()V

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSStream;->createUnfilteredStream()Ljava/io/OutputStream;

    move-result-object v1

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/pdfbox/cos/COSStream;

    invoke-virtual {v2}, Lorg/apache/pdfbox/cos/COSStream;->getUnfilteredStream()Ljava/io/InputStream;

    move-result-object v2

    const/16 v3, 0x800

    new-array v3, v3, [B

    :goto_1
    invoke-virtual {v2, v3}, Ljava/io/InputStream;->read([B)I

    move-result v4

    if-lez v4, :cond_0

    const/4 v5, 0x0

    invoke-virtual {v1, v3, v5, v4}, Ljava/io/OutputStream;->write([BII)V

    goto :goto_1

    :cond_0
    invoke-virtual {v1}, Ljava/io/OutputStream;->flush()V

    goto :goto_0

    :cond_1
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V

    sget-object p1, Lorg/apache/pdfbox/cos/COSName;->FLATE_DECODE:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, p1}, Lorg/apache/pdfbox/cos/COSStream;->setFilters(Lorg/apache/pdfbox/cos/COSBase;)V

    return-object v0
.end method

.method private createContentStreamList(Lorg/apache/pdfbox/cos/COSBase;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/pdfbox/cos/COSBase;",
            ")",
            "Ljava/util/List<",
            "Lorg/apache/pdfbox/cos/COSStream;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    instance-of v1, p1, Lorg/apache/pdfbox/cos/COSStream;

    if-eqz v1, :cond_0

    check-cast p1, Lorg/apache/pdfbox/cos/COSStream;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_0
    instance-of v1, p1, Lorg/apache/pdfbox/cos/COSArray;

    if-eqz v1, :cond_1

    check-cast p1, Lorg/apache/pdfbox/cos/COSArray;

    invoke-virtual {p1}, Lorg/apache/pdfbox/cos/COSArray;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/pdfbox/cos/COSBase;

    invoke-direct {p0, v1}, Lorg/apache/pdfbox/multipdf/Overlay;->createContentStreamList(Lorg/apache/pdfbox/cos/COSBase;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    :cond_1
    instance-of v1, p1, Lorg/apache/pdfbox/cos/COSObject;

    if-eqz v1, :cond_3

    check-cast p1, Lorg/apache/pdfbox/cos/COSObject;

    invoke-virtual {p1}, Lorg/apache/pdfbox/cos/COSObject;->getObject()Lorg/apache/pdfbox/cos/COSBase;

    move-result-object p1

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/multipdf/Overlay;->createContentStreamList(Lorg/apache/pdfbox/cos/COSBase;)Ljava/util/List;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_2
    :goto_1
    return-object v0

    :cond_3
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Contents are unknown type:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private createOverlayStream(Lorg/apache/pdfbox/pdmodel/PDPage;Lorg/apache/pdfbox/multipdf/Overlay$LayoutPage;Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSStream;
    .locals 2

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/PDPage;->getMediaBox()Lorg/apache/pdfbox/pdmodel/common/PDRectangle;

    move-result-object p1

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->getWidth()F

    move-result v0

    invoke-static {p2}, Lorg/apache/pdfbox/multipdf/Overlay$LayoutPage;->access$300(Lorg/apache/pdfbox/multipdf/Overlay$LayoutPage;)Lorg/apache/pdfbox/pdmodel/common/PDRectangle;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->getWidth()F

    move-result v1

    sub-float/2addr v0, v1

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->getHeight()F

    move-result p1

    invoke-static {p2}, Lorg/apache/pdfbox/multipdf/Overlay$LayoutPage;->access$300(Lorg/apache/pdfbox/multipdf/Overlay$LayoutPage;)Lorg/apache/pdfbox/pdmodel/common/PDRectangle;

    move-result-object p2

    invoke-virtual {p2}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->getHeight()F

    move-result p2

    sub-float/2addr p1, p2

    div-float/2addr p1, v1

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "q\nq 1 0 0 1 "

    invoke-virtual {p2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/multipdf/Overlay;->float2String(F)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/multipdf/Overlay;->float2String(F)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, " cm /"

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Lorg/apache/pdfbox/cos/COSName;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p1, " Do Q\nQ\n"

    invoke-virtual {p2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/multipdf/Overlay;->createStream(Ljava/lang/String;)Lorg/apache/pdfbox/cos/COSStream;

    move-result-object p1

    return-object p1
.end method

.method private createOverlayXObject(Lorg/apache/pdfbox/pdmodel/PDPage;Lorg/apache/pdfbox/multipdf/Overlay$LayoutPage;Lorg/apache/pdfbox/cos/COSStream;)Lorg/apache/pdfbox/cos/COSName;
    .locals 2

    new-instance v0, Lorg/apache/pdfbox/pdmodel/graphics/form/PDFormXObject;

    new-instance v1, Lorg/apache/pdfbox/pdmodel/common/PDStream;

    invoke-direct {v1, p3}, Lorg/apache/pdfbox/pdmodel/common/PDStream;-><init>(Lorg/apache/pdfbox/cos/COSStream;)V

    invoke-direct {v0, v1}, Lorg/apache/pdfbox/pdmodel/graphics/form/PDFormXObject;-><init>(Lorg/apache/pdfbox/pdmodel/common/PDStream;)V

    new-instance p3, Lorg/apache/pdfbox/pdmodel/PDResources;

    invoke-static {p2}, Lorg/apache/pdfbox/multipdf/Overlay$LayoutPage;->access$200(Lorg/apache/pdfbox/multipdf/Overlay$LayoutPage;)Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v1

    invoke-direct {p3, v1}, Lorg/apache/pdfbox/pdmodel/PDResources;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    invoke-virtual {v0, p3}, Lorg/apache/pdfbox/pdmodel/graphics/form/PDFormXObject;->setResources(Lorg/apache/pdfbox/pdmodel/PDResources;)V

    const/4 p3, 0x1

    invoke-virtual {v0, p3}, Lorg/apache/pdfbox/pdmodel/graphics/form/PDFormXObject;->setFormType(I)V

    invoke-static {p2}, Lorg/apache/pdfbox/multipdf/Overlay$LayoutPage;->access$300(Lorg/apache/pdfbox/multipdf/Overlay$LayoutPage;)Lorg/apache/pdfbox/pdmodel/common/PDRectangle;

    move-result-object p2

    invoke-virtual {p2}, Lorg/apache/pdfbox/pdmodel/common/PDRectangle;->createRetranslatedRectangle()Lorg/apache/pdfbox/pdmodel/common/PDRectangle;

    move-result-object p2

    invoke-virtual {v0, p2}, Lorg/apache/pdfbox/pdmodel/graphics/form/PDFormXObject;->setBBox(Lorg/apache/pdfbox/pdmodel/common/PDRectangle;)V

    new-instance p2, Lorg/apache/pdfbox/util/awt/AffineTransform;

    invoke-direct {p2}, Lorg/apache/pdfbox/util/awt/AffineTransform;-><init>()V

    invoke-virtual {v0, p2}, Lorg/apache/pdfbox/pdmodel/graphics/form/PDFormXObject;->setMatrix(Lorg/apache/pdfbox/util/awt/AffineTransform;)V

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/PDPage;->getResources()Lorg/apache/pdfbox/pdmodel/PDResources;

    move-result-object p1

    const-string p2, "OL"

    invoke-virtual {p1, v0, p2}, Lorg/apache/pdfbox/pdmodel/PDResources;->add(Lorg/apache/pdfbox/pdmodel/graphics/PDXObject;Ljava/lang/String;)Lorg/apache/pdfbox/cos/COSName;

    move-result-object p1

    return-object p1
.end method

.method private createStream(Ljava/lang/String;)Lorg/apache/pdfbox/cos/COSStream;
    .locals 3

    new-instance v0, Lorg/apache/pdfbox/cos/COSStream;

    invoke-direct {v0}, Lorg/apache/pdfbox/cos/COSStream;-><init>()V

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSStream;->createUnfilteredStream()Ljava/io/OutputStream;

    move-result-object v1

    const-string v2, "ISO-8859-1"

    invoke-virtual {p1, v2}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object p1

    invoke-virtual {v1, p1}, Ljava/io/OutputStream;->write([B)V

    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V

    sget-object p1, Lorg/apache/pdfbox/cos/COSName;->FLATE_DECODE:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, p1}, Lorg/apache/pdfbox/cos/COSStream;->setFilters(Lorg/apache/pdfbox/cos/COSBase;)V

    return-object v0
.end method

.method private float2String(F)Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/math/BigDecimal;

    invoke-static {p1}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/math/BigDecimal;->toPlainString()Ljava/lang/String;

    move-result-object p1

    const/16 v0, 0x2e

    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    const/4 v1, -0x1

    if-le v0, v1, :cond_0

    const-string v0, ".0"

    invoke-virtual {p1, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    const-string v1, "0"

    invoke-virtual {p1, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p1, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    const/4 v2, 0x0

    invoke-virtual {p1, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_0
    return-object p1
.end method

.method private getLayoutPage(Lorg/apache/pdfbox/pdmodel/PDDocument;)Lorg/apache/pdfbox/multipdf/Overlay$LayoutPage;
    .locals 4

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lorg/apache/pdfbox/pdmodel/PDDocument;->getPage(I)Lorg/apache/pdfbox/pdmodel/PDPage;

    move-result-object p1

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/PDPage;->getCOSObject()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v1, Lorg/apache/pdfbox/cos/COSName;->CONTENTS:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/PDPage;->getResources()Lorg/apache/pdfbox/pdmodel/PDResources;

    move-result-object v1

    if-nez v1, :cond_0

    new-instance v1, Lorg/apache/pdfbox/pdmodel/PDResources;

    invoke-direct {v1}, Lorg/apache/pdfbox/pdmodel/PDResources;-><init>()V

    :cond_0
    new-instance v2, Lorg/apache/pdfbox/multipdf/Overlay$LayoutPage;

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/PDPage;->getMediaBox()Lorg/apache/pdfbox/pdmodel/common/PDRectangle;

    move-result-object p1

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/multipdf/Overlay;->createContentStream(Lorg/apache/pdfbox/cos/COSBase;)Lorg/apache/pdfbox/cos/COSStream;

    move-result-object v0

    invoke-virtual {v1}, Lorg/apache/pdfbox/pdmodel/PDResources;->getCOSObject()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v1

    const/4 v3, 0x0

    invoke-direct {v2, p1, v0, v1, v3}, Lorg/apache/pdfbox/multipdf/Overlay$LayoutPage;-><init>(Lorg/apache/pdfbox/pdmodel/common/PDRectangle;Lorg/apache/pdfbox/cos/COSStream;Lorg/apache/pdfbox/cos/COSDictionary;Lorg/apache/pdfbox/multipdf/Overlay$1;)V

    return-object v2
.end method

.method private getLayoutPages(Lorg/apache/pdfbox/pdmodel/PDDocument;)Ljava/util/Map;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/pdfbox/pdmodel/PDDocument;",
            ")",
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Lorg/apache/pdfbox/multipdf/Overlay$LayoutPage;",
            ">;"
        }
    .end annotation

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/PDDocument;->getNumberOfPages()I

    move-result v0

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1, v0}, Ljava/util/HashMap;-><init>(I)V

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v0, :cond_1

    invoke-virtual {p1, v2}, Lorg/apache/pdfbox/pdmodel/PDDocument;->getPage(I)Lorg/apache/pdfbox/pdmodel/PDPage;

    move-result-object v3

    invoke-virtual {v3}, Lorg/apache/pdfbox/pdmodel/PDPage;->getCOSObject()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v4

    sget-object v5, Lorg/apache/pdfbox/cos/COSName;->CONTENTS:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v4, v5}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v4

    invoke-virtual {v3}, Lorg/apache/pdfbox/pdmodel/PDPage;->getResources()Lorg/apache/pdfbox/pdmodel/PDResources;

    move-result-object v5

    if-nez v5, :cond_0

    new-instance v5, Lorg/apache/pdfbox/pdmodel/PDResources;

    invoke-direct {v5}, Lorg/apache/pdfbox/pdmodel/PDResources;-><init>()V

    :cond_0
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    new-instance v7, Lorg/apache/pdfbox/multipdf/Overlay$LayoutPage;

    invoke-virtual {v3}, Lorg/apache/pdfbox/pdmodel/PDPage;->getMediaBox()Lorg/apache/pdfbox/pdmodel/common/PDRectangle;

    move-result-object v3

    invoke-direct {p0, v4}, Lorg/apache/pdfbox/multipdf/Overlay;->createContentStream(Lorg/apache/pdfbox/cos/COSBase;)Lorg/apache/pdfbox/cos/COSStream;

    move-result-object v4

    invoke-virtual {v5}, Lorg/apache/pdfbox/pdmodel/PDResources;->getCOSObject()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v5

    const/4 v8, 0x0

    invoke-direct {v7, v3, v4, v5, v8}, Lorg/apache/pdfbox/multipdf/Overlay$LayoutPage;-><init>(Lorg/apache/pdfbox/pdmodel/common/PDRectangle;Lorg/apache/pdfbox/cos/COSStream;Lorg/apache/pdfbox/cos/COSDictionary;Lorg/apache/pdfbox/multipdf/Overlay$1;)V

    invoke-interface {v1, v6, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    return-object v1
.end method

.method private loadPDF(Ljava/lang/String;)Lorg/apache/pdfbox/pdmodel/PDDocument;
    .locals 1

    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lorg/apache/pdfbox/pdmodel/PDDocument;->load(Ljava/io/File;)Lorg/apache/pdfbox/pdmodel/PDDocument;

    move-result-object p1

    return-object p1
.end method

.method private loadPDFs()V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/multipdf/Overlay;->inputFileName:Ljava/lang/String;

    if-eqz v0, :cond_0

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/multipdf/Overlay;->loadPDF(Ljava/lang/String;)Lorg/apache/pdfbox/pdmodel/PDDocument;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/pdfbox/multipdf/Overlay;->inputPDFDocument:Lorg/apache/pdfbox/pdmodel/PDDocument;

    :cond_0
    iget-object v0, p0, Lorg/apache/pdfbox/multipdf/Overlay;->defaultOverlayFilename:Ljava/lang/String;

    if-eqz v0, :cond_1

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/multipdf/Overlay;->loadPDF(Ljava/lang/String;)Lorg/apache/pdfbox/pdmodel/PDDocument;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/pdfbox/multipdf/Overlay;->defaultOverlay:Lorg/apache/pdfbox/pdmodel/PDDocument;

    :cond_1
    iget-object v0, p0, Lorg/apache/pdfbox/multipdf/Overlay;->defaultOverlay:Lorg/apache/pdfbox/pdmodel/PDDocument;

    if-eqz v0, :cond_2

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/multipdf/Overlay;->getLayoutPage(Lorg/apache/pdfbox/pdmodel/PDDocument;)Lorg/apache/pdfbox/multipdf/Overlay$LayoutPage;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/pdfbox/multipdf/Overlay;->defaultOverlayPage:Lorg/apache/pdfbox/multipdf/Overlay$LayoutPage;

    :cond_2
    iget-object v0, p0, Lorg/apache/pdfbox/multipdf/Overlay;->firstPageOverlayFilename:Ljava/lang/String;

    if-eqz v0, :cond_3

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/multipdf/Overlay;->loadPDF(Ljava/lang/String;)Lorg/apache/pdfbox/pdmodel/PDDocument;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/pdfbox/multipdf/Overlay;->firstPageOverlay:Lorg/apache/pdfbox/pdmodel/PDDocument;

    :cond_3
    iget-object v0, p0, Lorg/apache/pdfbox/multipdf/Overlay;->firstPageOverlay:Lorg/apache/pdfbox/pdmodel/PDDocument;

    if-eqz v0, :cond_4

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/multipdf/Overlay;->getLayoutPage(Lorg/apache/pdfbox/pdmodel/PDDocument;)Lorg/apache/pdfbox/multipdf/Overlay$LayoutPage;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/pdfbox/multipdf/Overlay;->firstPageOverlayPage:Lorg/apache/pdfbox/multipdf/Overlay$LayoutPage;

    :cond_4
    iget-object v0, p0, Lorg/apache/pdfbox/multipdf/Overlay;->lastPageOverlayFilename:Ljava/lang/String;

    if-eqz v0, :cond_5

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/multipdf/Overlay;->loadPDF(Ljava/lang/String;)Lorg/apache/pdfbox/pdmodel/PDDocument;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/pdfbox/multipdf/Overlay;->lastPageOverlay:Lorg/apache/pdfbox/pdmodel/PDDocument;

    :cond_5
    iget-object v0, p0, Lorg/apache/pdfbox/multipdf/Overlay;->lastPageOverlay:Lorg/apache/pdfbox/pdmodel/PDDocument;

    if-eqz v0, :cond_6

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/multipdf/Overlay;->getLayoutPage(Lorg/apache/pdfbox/pdmodel/PDDocument;)Lorg/apache/pdfbox/multipdf/Overlay$LayoutPage;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/pdfbox/multipdf/Overlay;->lastPageOverlayPage:Lorg/apache/pdfbox/multipdf/Overlay$LayoutPage;

    :cond_6
    iget-object v0, p0, Lorg/apache/pdfbox/multipdf/Overlay;->oddPageOverlayFilename:Ljava/lang/String;

    if-eqz v0, :cond_7

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/multipdf/Overlay;->loadPDF(Ljava/lang/String;)Lorg/apache/pdfbox/pdmodel/PDDocument;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/pdfbox/multipdf/Overlay;->oddPageOverlay:Lorg/apache/pdfbox/pdmodel/PDDocument;

    :cond_7
    iget-object v0, p0, Lorg/apache/pdfbox/multipdf/Overlay;->oddPageOverlay:Lorg/apache/pdfbox/pdmodel/PDDocument;

    if-eqz v0, :cond_8

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/multipdf/Overlay;->getLayoutPage(Lorg/apache/pdfbox/pdmodel/PDDocument;)Lorg/apache/pdfbox/multipdf/Overlay$LayoutPage;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/pdfbox/multipdf/Overlay;->oddPageOverlayPage:Lorg/apache/pdfbox/multipdf/Overlay$LayoutPage;

    :cond_8
    iget-object v0, p0, Lorg/apache/pdfbox/multipdf/Overlay;->evenPageOverlayFilename:Ljava/lang/String;

    if-eqz v0, :cond_9

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/multipdf/Overlay;->loadPDF(Ljava/lang/String;)Lorg/apache/pdfbox/pdmodel/PDDocument;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/pdfbox/multipdf/Overlay;->evenPageOverlay:Lorg/apache/pdfbox/pdmodel/PDDocument;

    :cond_9
    iget-object v0, p0, Lorg/apache/pdfbox/multipdf/Overlay;->evenPageOverlay:Lorg/apache/pdfbox/pdmodel/PDDocument;

    if-eqz v0, :cond_a

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/multipdf/Overlay;->getLayoutPage(Lorg/apache/pdfbox/pdmodel/PDDocument;)Lorg/apache/pdfbox/multipdf/Overlay$LayoutPage;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/pdfbox/multipdf/Overlay;->evenPageOverlayPage:Lorg/apache/pdfbox/multipdf/Overlay$LayoutPage;

    :cond_a
    iget-object v0, p0, Lorg/apache/pdfbox/multipdf/Overlay;->allPagesOverlayFilename:Ljava/lang/String;

    if-eqz v0, :cond_b

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/multipdf/Overlay;->loadPDF(Ljava/lang/String;)Lorg/apache/pdfbox/pdmodel/PDDocument;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/pdfbox/multipdf/Overlay;->allPagesOverlay:Lorg/apache/pdfbox/pdmodel/PDDocument;

    :cond_b
    iget-object v0, p0, Lorg/apache/pdfbox/multipdf/Overlay;->allPagesOverlay:Lorg/apache/pdfbox/pdmodel/PDDocument;

    if-eqz v0, :cond_c

    invoke-direct {p0, v0}, Lorg/apache/pdfbox/multipdf/Overlay;->getLayoutPages(Lorg/apache/pdfbox/pdmodel/PDDocument;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/pdfbox/multipdf/Overlay;->specificPageOverlayPage:Ljava/util/Map;

    const/4 v1, 0x1

    iput-boolean v1, p0, Lorg/apache/pdfbox/multipdf/Overlay;->useAllOverlayPages:Z

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    iput v0, p0, Lorg/apache/pdfbox/multipdf/Overlay;->numberOfOverlayPages:I

    :cond_c
    return-void
.end method

.method private overlayPage(Lorg/apache/pdfbox/cos/COSArray;Lorg/apache/pdfbox/pdmodel/PDPage;II)V
    .locals 2

    iget-boolean v0, p0, Lorg/apache/pdfbox/multipdf/Overlay;->useAllOverlayPages:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/pdfbox/multipdf/Overlay;->specificPageOverlayPage:Ljava/util/Map;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    iget-object p4, p0, Lorg/apache/pdfbox/multipdf/Overlay;->specificPageOverlayPage:Ljava/util/Map;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p3

    invoke-interface {p4, p3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p3

    check-cast p3, Lorg/apache/pdfbox/multipdf/Overlay$LayoutPage;

    goto :goto_3

    :cond_0
    const/4 v0, 0x1

    if-ne p3, v0, :cond_1

    iget-object v1, p0, Lorg/apache/pdfbox/multipdf/Overlay;->firstPageOverlayPage:Lorg/apache/pdfbox/multipdf/Overlay$LayoutPage;

    if-eqz v1, :cond_1

    :goto_1
    move-object p3, v1

    goto :goto_3

    :cond_1
    if-ne p3, p4, :cond_2

    iget-object p4, p0, Lorg/apache/pdfbox/multipdf/Overlay;->lastPageOverlayPage:Lorg/apache/pdfbox/multipdf/Overlay$LayoutPage;

    if-eqz p4, :cond_2

    :goto_2
    move-object p3, p4

    goto :goto_3

    :cond_2
    rem-int/lit8 p4, p3, 0x2

    if-ne p4, v0, :cond_3

    iget-object v1, p0, Lorg/apache/pdfbox/multipdf/Overlay;->oddPageOverlayPage:Lorg/apache/pdfbox/multipdf/Overlay$LayoutPage;

    if-eqz v1, :cond_3

    goto :goto_1

    :cond_3
    if-nez p4, :cond_4

    iget-object p4, p0, Lorg/apache/pdfbox/multipdf/Overlay;->evenPageOverlayPage:Lorg/apache/pdfbox/multipdf/Overlay$LayoutPage;

    if-eqz p4, :cond_4

    goto :goto_2

    :cond_4
    iget-object p4, p0, Lorg/apache/pdfbox/multipdf/Overlay;->defaultOverlayPage:Lorg/apache/pdfbox/multipdf/Overlay$LayoutPage;

    if-eqz p4, :cond_5

    goto :goto_2

    :cond_5
    iget-boolean p4, p0, Lorg/apache/pdfbox/multipdf/Overlay;->useAllOverlayPages:Z

    if-eqz p4, :cond_6

    sub-int/2addr p3, v0

    iget p4, p0, Lorg/apache/pdfbox/multipdf/Overlay;->numberOfOverlayPages:I

    rem-int/2addr p3, p4

    goto :goto_0

    :cond_6
    const/4 p3, 0x0

    :goto_3
    if-eqz p3, :cond_8

    invoke-virtual {p2}, Lorg/apache/pdfbox/pdmodel/PDPage;->getResources()Lorg/apache/pdfbox/pdmodel/PDResources;

    move-result-object p4

    if-nez p4, :cond_7

    new-instance p4, Lorg/apache/pdfbox/pdmodel/PDResources;

    invoke-direct {p4}, Lorg/apache/pdfbox/pdmodel/PDResources;-><init>()V

    invoke-virtual {p2, p4}, Lorg/apache/pdfbox/pdmodel/PDPage;->setResources(Lorg/apache/pdfbox/pdmodel/PDResources;)V

    :cond_7
    invoke-static {p3}, Lorg/apache/pdfbox/multipdf/Overlay$LayoutPage;->access$100(Lorg/apache/pdfbox/multipdf/Overlay$LayoutPage;)Lorg/apache/pdfbox/cos/COSStream;

    move-result-object p4

    invoke-direct {p0, p2, p3, p4}, Lorg/apache/pdfbox/multipdf/Overlay;->createOverlayXObject(Lorg/apache/pdfbox/pdmodel/PDPage;Lorg/apache/pdfbox/multipdf/Overlay$LayoutPage;Lorg/apache/pdfbox/cos/COSStream;)Lorg/apache/pdfbox/cos/COSName;

    move-result-object p4

    invoke-direct {p0, p2, p3, p4}, Lorg/apache/pdfbox/multipdf/Overlay;->createOverlayStream(Lorg/apache/pdfbox/pdmodel/PDPage;Lorg/apache/pdfbox/multipdf/Overlay$LayoutPage;Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSStream;

    move-result-object p2

    invoke-virtual {p1, p2}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    :cond_8
    return-void
.end method

.method private processPages(Lorg/apache/pdfbox/pdmodel/PDDocument;)V
    .locals 9

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/PDDocument;->getPages()Lorg/apache/pdfbox/pdmodel/PDPageTree;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/PDPageTree;->iterator()Ljava/util/Iterator;

    move-result-object v0

    const/4 v1, 0x0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/pdfbox/pdmodel/PDPage;

    invoke-virtual {v2}, Lorg/apache/pdfbox/pdmodel/PDPage;->getCOSObject()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v3

    sget-object v4, Lorg/apache/pdfbox/cos/COSName;->CONTENTS:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v3, v4}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v5

    new-instance v6, Lorg/apache/pdfbox/cos/COSArray;

    invoke-direct {v6}, Lorg/apache/pdfbox/cos/COSArray;-><init>()V

    sget-object v7, Lorg/apache/pdfbox/multipdf/Overlay$1;->$SwitchMap$org$apache$pdfbox$multipdf$Overlay$Position:[I

    iget-object v8, p0, Lorg/apache/pdfbox/multipdf/Overlay;->position:Lorg/apache/pdfbox/multipdf/Overlay$Position;

    invoke-virtual {v8}, Ljava/lang/Enum;->ordinal()I

    move-result v8

    aget v7, v7, v8

    const/4 v8, 0x1

    if-eq v7, v8, :cond_1

    const/4 v8, 0x2

    if-ne v7, v8, :cond_0

    add-int/lit8 v7, v1, 0x1

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/PDDocument;->getNumberOfPages()I

    move-result v8

    invoke-direct {p0, v6, v2, v7, v8}, Lorg/apache/pdfbox/multipdf/Overlay;->overlayPage(Lorg/apache/pdfbox/cos/COSArray;Lorg/apache/pdfbox/pdmodel/PDPage;II)V

    invoke-direct {p0, v5, v6}, Lorg/apache/pdfbox/multipdf/Overlay;->addOriginalContent(Lorg/apache/pdfbox/cos/COSBase;Lorg/apache/pdfbox/cos/COSArray;)V

    goto :goto_1

    :cond_0
    new-instance p1, Ljava/io/IOException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unknown type of position:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lorg/apache/pdfbox/multipdf/Overlay;->position:Lorg/apache/pdfbox/multipdf/Overlay$Position;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    const-string v7, "q\n"

    invoke-direct {p0, v7}, Lorg/apache/pdfbox/multipdf/Overlay;->createStream(Ljava/lang/String;)Lorg/apache/pdfbox/cos/COSStream;

    move-result-object v7

    invoke-virtual {v6, v7}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    invoke-direct {p0, v5, v6}, Lorg/apache/pdfbox/multipdf/Overlay;->addOriginalContent(Lorg/apache/pdfbox/cos/COSBase;Lorg/apache/pdfbox/cos/COSArray;)V

    const-string v5, "Q\n"

    invoke-direct {p0, v5}, Lorg/apache/pdfbox/multipdf/Overlay;->createStream(Ljava/lang/String;)Lorg/apache/pdfbox/cos/COSStream;

    move-result-object v5

    invoke-virtual {v6, v5}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    add-int/lit8 v5, v1, 0x1

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/PDDocument;->getNumberOfPages()I

    move-result v7

    invoke-direct {p0, v6, v2, v5, v7}, Lorg/apache/pdfbox/multipdf/Overlay;->overlayPage(Lorg/apache/pdfbox/cos/COSArray;Lorg/apache/pdfbox/pdmodel/PDPage;II)V

    :goto_1
    invoke-virtual {v3, v4, v6}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    return-void
.end method


# virtual methods
.method public getDefaultOverlayFile()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/multipdf/Overlay;->defaultOverlayFilename:Ljava/lang/String;

    return-object v0
.end method

.method public getInputFile()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/multipdf/Overlay;->inputFileName:Ljava/lang/String;

    return-object v0
.end method

.method public getOutputFile()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/multipdf/Overlay;->outputFilename:Ljava/lang/String;

    return-object v0
.end method

.method public overlay(Ljava/util/Map;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    :try_start_0
    invoke-direct {p0}, Lorg/apache/pdfbox/multipdf/Overlay;->loadPDFs()V

    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-direct {p0, v1}, Lorg/apache/pdfbox/multipdf/Overlay;->loadPDF(Ljava/lang/String;)Lorg/apache/pdfbox/pdmodel/PDDocument;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/pdfbox/multipdf/Overlay;->specificPageOverlay:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v2, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v2, p0, Lorg/apache/pdfbox/multipdf/Overlay;->specificPageOverlayPage:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-direct {p0, v1}, Lorg/apache/pdfbox/multipdf/Overlay;->getLayoutPage(Lorg/apache/pdfbox/pdmodel/PDDocument;)Lorg/apache/pdfbox/multipdf/Overlay$LayoutPage;

    move-result-object v1

    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    iget-object p1, p0, Lorg/apache/pdfbox/multipdf/Overlay;->inputPDFDocument:Lorg/apache/pdfbox/pdmodel/PDDocument;

    invoke-direct {p0, p1}, Lorg/apache/pdfbox/multipdf/Overlay;->processPages(Lorg/apache/pdfbox/pdmodel/PDDocument;)V

    iget-object p1, p0, Lorg/apache/pdfbox/multipdf/Overlay;->inputPDFDocument:Lorg/apache/pdfbox/pdmodel/PDDocument;

    iget-object v0, p0, Lorg/apache/pdfbox/multipdf/Overlay;->outputFilename:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lorg/apache/pdfbox/pdmodel/PDDocument;->save(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object p1, p0, Lorg/apache/pdfbox/multipdf/Overlay;->inputPDFDocument:Lorg/apache/pdfbox/pdmodel/PDDocument;

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/PDDocument;->close()V

    :cond_1
    iget-object p1, p0, Lorg/apache/pdfbox/multipdf/Overlay;->defaultOverlay:Lorg/apache/pdfbox/pdmodel/PDDocument;

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/PDDocument;->close()V

    :cond_2
    iget-object p1, p0, Lorg/apache/pdfbox/multipdf/Overlay;->firstPageOverlay:Lorg/apache/pdfbox/pdmodel/PDDocument;

    if-eqz p1, :cond_3

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/PDDocument;->close()V

    :cond_3
    iget-object p1, p0, Lorg/apache/pdfbox/multipdf/Overlay;->lastPageOverlay:Lorg/apache/pdfbox/pdmodel/PDDocument;

    if-eqz p1, :cond_4

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/PDDocument;->close()V

    :cond_4
    iget-object p1, p0, Lorg/apache/pdfbox/multipdf/Overlay;->allPagesOverlay:Lorg/apache/pdfbox/pdmodel/PDDocument;

    if-eqz p1, :cond_5

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/PDDocument;->close()V

    :cond_5
    iget-object p1, p0, Lorg/apache/pdfbox/multipdf/Overlay;->oddPageOverlay:Lorg/apache/pdfbox/pdmodel/PDDocument;

    if-eqz p1, :cond_6

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/PDDocument;->close()V

    :cond_6
    iget-object p1, p0, Lorg/apache/pdfbox/multipdf/Overlay;->evenPageOverlay:Lorg/apache/pdfbox/pdmodel/PDDocument;

    if-eqz p1, :cond_7

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/PDDocument;->close()V

    :cond_7
    iget-object p1, p0, Lorg/apache/pdfbox/multipdf/Overlay;->specificPageOverlay:Ljava/util/Map;

    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/pdmodel/PDDocument;

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/PDDocument;->close()V

    goto :goto_1

    :cond_8
    iget-object p1, p0, Lorg/apache/pdfbox/multipdf/Overlay;->specificPageOverlay:Ljava/util/Map;

    invoke-interface {p1}, Ljava/util/Map;->clear()V

    iget-object p1, p0, Lorg/apache/pdfbox/multipdf/Overlay;->specificPageOverlayPage:Ljava/util/Map;

    invoke-interface {p1}, Ljava/util/Map;->clear()V

    return-void

    :catchall_0
    move-exception p1

    iget-object v0, p0, Lorg/apache/pdfbox/multipdf/Overlay;->inputPDFDocument:Lorg/apache/pdfbox/pdmodel/PDDocument;

    if-eqz v0, :cond_9

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/PDDocument;->close()V

    :cond_9
    iget-object v0, p0, Lorg/apache/pdfbox/multipdf/Overlay;->defaultOverlay:Lorg/apache/pdfbox/pdmodel/PDDocument;

    if-eqz v0, :cond_a

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/PDDocument;->close()V

    :cond_a
    iget-object v0, p0, Lorg/apache/pdfbox/multipdf/Overlay;->firstPageOverlay:Lorg/apache/pdfbox/pdmodel/PDDocument;

    if-eqz v0, :cond_b

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/PDDocument;->close()V

    :cond_b
    iget-object v0, p0, Lorg/apache/pdfbox/multipdf/Overlay;->lastPageOverlay:Lorg/apache/pdfbox/pdmodel/PDDocument;

    if-eqz v0, :cond_c

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/PDDocument;->close()V

    :cond_c
    iget-object v0, p0, Lorg/apache/pdfbox/multipdf/Overlay;->allPagesOverlay:Lorg/apache/pdfbox/pdmodel/PDDocument;

    if-eqz v0, :cond_d

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/PDDocument;->close()V

    :cond_d
    iget-object v0, p0, Lorg/apache/pdfbox/multipdf/Overlay;->oddPageOverlay:Lorg/apache/pdfbox/pdmodel/PDDocument;

    if-eqz v0, :cond_e

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/PDDocument;->close()V

    :cond_e
    iget-object v0, p0, Lorg/apache/pdfbox/multipdf/Overlay;->evenPageOverlay:Lorg/apache/pdfbox/pdmodel/PDDocument;

    if-eqz v0, :cond_f

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/PDDocument;->close()V

    :cond_f
    iget-object v0, p0, Lorg/apache/pdfbox/multipdf/Overlay;->specificPageOverlay:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_10

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/pdfbox/pdmodel/PDDocument;

    invoke-virtual {v1}, Lorg/apache/pdfbox/pdmodel/PDDocument;->close()V

    goto :goto_2

    :cond_10
    iget-object v0, p0, Lorg/apache/pdfbox/multipdf/Overlay;->specificPageOverlay:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    iget-object v0, p0, Lorg/apache/pdfbox/multipdf/Overlay;->specificPageOverlayPage:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    throw p1
.end method

.method public setAllPagesOverlayFile(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lorg/apache/pdfbox/multipdf/Overlay;->allPagesOverlayFilename:Ljava/lang/String;

    return-void
.end method

.method public setAllPagesOverlayPDF(Lorg/apache/pdfbox/pdmodel/PDDocument;)V
    .locals 0

    iput-object p1, p0, Lorg/apache/pdfbox/multipdf/Overlay;->allPagesOverlay:Lorg/apache/pdfbox/pdmodel/PDDocument;

    return-void
.end method

.method public setDefaultOverlayFile(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lorg/apache/pdfbox/multipdf/Overlay;->defaultOverlayFilename:Ljava/lang/String;

    return-void
.end method

.method public setDefaultOverlayPDF(Lorg/apache/pdfbox/pdmodel/PDDocument;)V
    .locals 0

    iput-object p1, p0, Lorg/apache/pdfbox/multipdf/Overlay;->defaultOverlay:Lorg/apache/pdfbox/pdmodel/PDDocument;

    return-void
.end method

.method public setEvenPageOverlayFile(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lorg/apache/pdfbox/multipdf/Overlay;->evenPageOverlayFilename:Ljava/lang/String;

    return-void
.end method

.method public setEvenPageOverlayPDF(Lorg/apache/pdfbox/pdmodel/PDDocument;)V
    .locals 0

    iput-object p1, p0, Lorg/apache/pdfbox/multipdf/Overlay;->evenPageOverlay:Lorg/apache/pdfbox/pdmodel/PDDocument;

    return-void
.end method

.method public setFirstPageOverlayFile(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lorg/apache/pdfbox/multipdf/Overlay;->firstPageOverlayFilename:Ljava/lang/String;

    return-void
.end method

.method public setFirstPageOverlayPDF(Lorg/apache/pdfbox/pdmodel/PDDocument;)V
    .locals 0

    iput-object p1, p0, Lorg/apache/pdfbox/multipdf/Overlay;->firstPageOverlay:Lorg/apache/pdfbox/pdmodel/PDDocument;

    return-void
.end method

.method public setInputFile(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lorg/apache/pdfbox/multipdf/Overlay;->inputFileName:Ljava/lang/String;

    return-void
.end method

.method public setInputPDF(Lorg/apache/pdfbox/pdmodel/PDDocument;)V
    .locals 0

    iput-object p1, p0, Lorg/apache/pdfbox/multipdf/Overlay;->inputPDFDocument:Lorg/apache/pdfbox/pdmodel/PDDocument;

    return-void
.end method

.method public setLastPageOverlayFile(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lorg/apache/pdfbox/multipdf/Overlay;->lastPageOverlayFilename:Ljava/lang/String;

    return-void
.end method

.method public setLastPageOverlayPDF(Lorg/apache/pdfbox/pdmodel/PDDocument;)V
    .locals 0

    iput-object p1, p0, Lorg/apache/pdfbox/multipdf/Overlay;->lastPageOverlay:Lorg/apache/pdfbox/pdmodel/PDDocument;

    return-void
.end method

.method public setOddPageOverlayFile(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lorg/apache/pdfbox/multipdf/Overlay;->oddPageOverlayFilename:Ljava/lang/String;

    return-void
.end method

.method public setOddPageOverlayPDF(Lorg/apache/pdfbox/pdmodel/PDDocument;)V
    .locals 0

    iput-object p1, p0, Lorg/apache/pdfbox/multipdf/Overlay;->oddPageOverlay:Lorg/apache/pdfbox/pdmodel/PDDocument;

    return-void
.end method

.method public setOutputFile(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lorg/apache/pdfbox/multipdf/Overlay;->outputFilename:Ljava/lang/String;

    return-void
.end method

.method public setOverlayPosition(Lorg/apache/pdfbox/multipdf/Overlay$Position;)V
    .locals 0

    iput-object p1, p0, Lorg/apache/pdfbox/multipdf/Overlay;->position:Lorg/apache/pdfbox/multipdf/Overlay$Position;

    return-void
.end method

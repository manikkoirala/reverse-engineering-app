.class public Lorg/apache/pdfbox/multipdf/PDFMergerUtility;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final STRUCTURETYPE_DOCUMENT:Ljava/lang/String; = "Document"


# instance fields
.field private destinationFileName:Ljava/lang/String;

.field private destinationStream:Ljava/io/OutputStream;

.field private ignoreAcroFormErrors:Z

.field private nextFieldNum:I

.field private final sources:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/io/InputStream;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/pdfbox/multipdf/PDFMergerUtility;->ignoreAcroFormErrors:Z

    const/4 v0, 0x1

    iput v0, p0, Lorg/apache/pdfbox/multipdf/PDFMergerUtility;->nextFieldNum:I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/apache/pdfbox/multipdf/PDFMergerUtility;->sources:Ljava/util/List;

    return-void
.end method

.method private isDynamicXfa(Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;)Z
    .locals 0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;->xfaIsDynamic()Z

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method private mergeAcroForm(Lorg/apache/pdfbox/multipdf/PDFCloneUtility;Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;)V
    .locals 5

    invoke-virtual {p2}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;->getFields()Ljava/util/List;

    move-result-object v0

    invoke-virtual {p3}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;->getFields()Ljava/util/List;

    move-result-object p3

    if-eqz p3, :cond_2

    if-nez v0, :cond_0

    new-instance v0, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;

    invoke-direct {v0}, Lorg/apache/pdfbox/pdmodel/common/COSArrayList;-><init>()V

    invoke-virtual {p2, v0}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;->setFields(Ljava/util/List;)V

    :cond_0
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p3

    :goto_0
    invoke-interface {p3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {p3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;

    invoke-virtual {v1}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v1

    invoke-virtual {p1, v1}, Lorg/apache/pdfbox/multipdf/PDFCloneUtility;->cloneForNewDocument(Ljava/lang/Object;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v1

    check-cast v1, Lorg/apache/pdfbox/cos/COSDictionary;

    const/4 v2, 0x0

    invoke-static {p2, v1, v2}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->createField(Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;Lorg/apache/pdfbox/cos/COSDictionary;Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;)Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->getFullyQualifiedName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v2}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;->getField(Ljava/lang/String;)Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;

    move-result-object v2

    if-eqz v2, :cond_1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "dummyFieldName"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v3, p0, Lorg/apache/pdfbox/multipdf/PDFMergerUtility;->nextFieldNum:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, Lorg/apache/pdfbox/multipdf/PDFMergerUtility;->nextFieldNum:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDFieldTreeNode;->setPartialName(Ljava/lang/String;)V

    :cond_1
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    return-void
.end method

.method private updatePageReferences(Lorg/apache/pdfbox/cos/COSArray;Ljava/util/Map;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/pdfbox/cos/COSArray;",
            "Ljava/util/Map<",
            "Lorg/apache/pdfbox/cos/COSDictionary;",
            "Lorg/apache/pdfbox/cos/COSDictionary;",
            ">;)V"
        }
    .end annotation

    .line 1
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1}, Lorg/apache/pdfbox/cos/COSArray;->size()I

    move-result v1

    if-ge v0, v1, :cond_2

    invoke-virtual {p1, v0}, Lorg/apache/pdfbox/cos/COSArray;->getObject(I)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v1

    instance-of v2, v1, Lorg/apache/pdfbox/cos/COSArray;

    if-eqz v2, :cond_0

    check-cast v1, Lorg/apache/pdfbox/cos/COSArray;

    invoke-direct {p0, v1, p2}, Lorg/apache/pdfbox/multipdf/PDFMergerUtility;->updatePageReferences(Lorg/apache/pdfbox/cos/COSArray;Ljava/util/Map;)V

    goto :goto_1

    :cond_0
    instance-of v2, v1, Lorg/apache/pdfbox/cos/COSDictionary;

    if-eqz v2, :cond_1

    check-cast v1, Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-direct {p0, v1, p2}, Lorg/apache/pdfbox/multipdf/PDFMergerUtility;->updatePageReferences(Lorg/apache/pdfbox/cos/COSDictionary;Ljava/util/Map;)V

    :cond_1
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    return-void
.end method

.method private updatePageReferences(Lorg/apache/pdfbox/cos/COSDictionary;Ljava/util/Map;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/pdfbox/cos/COSDictionary;",
            "Ljava/util/Map<",
            "Lorg/apache/pdfbox/cos/COSDictionary;",
            "Lorg/apache/pdfbox/cos/COSDictionary;",
            ">;)V"
        }
    .end annotation

    .line 2
    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->PG:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p1, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v1

    instance-of v2, v1, Lorg/apache/pdfbox/cos/COSDictionary;

    if-eqz v2, :cond_0

    invoke-interface {p2, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {p2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/pdfbox/cos/COSBase;

    invoke-virtual {p1, v0, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    :cond_0
    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->OBJ:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p1, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v2

    instance-of v3, v2, Lorg/apache/pdfbox/cos/COSDictionary;

    if-eqz v3, :cond_1

    invoke-interface {p2, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p2, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/pdfbox/cos/COSBase;

    invoke-virtual {p1, v0, v1}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    :cond_1
    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->K:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {p1, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object p1

    instance-of v0, p1, Lorg/apache/pdfbox/cos/COSArray;

    if-eqz v0, :cond_2

    check-cast p1, Lorg/apache/pdfbox/cos/COSArray;

    invoke-direct {p0, p1, p2}, Lorg/apache/pdfbox/multipdf/PDFMergerUtility;->updatePageReferences(Lorg/apache/pdfbox/cos/COSArray;Ljava/util/Map;)V

    goto :goto_0

    :cond_2
    instance-of v0, p1, Lorg/apache/pdfbox/cos/COSDictionary;

    if-eqz v0, :cond_3

    check-cast p1, Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-direct {p0, p1, p2}, Lorg/apache/pdfbox/multipdf/PDFMergerUtility;->updatePageReferences(Lorg/apache/pdfbox/cos/COSDictionary;Ljava/util/Map;)V

    :cond_3
    :goto_0
    return-void
.end method

.method private updateParentEntry(Lorg/apache/pdfbox/cos/COSArray;Lorg/apache/pdfbox/cos/COSDictionary;)V
    .locals 4

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1}, Lorg/apache/pdfbox/cos/COSArray;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    invoke-virtual {p1, v0}, Lorg/apache/pdfbox/cos/COSArray;->getObject(I)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v1

    instance-of v2, v1, Lorg/apache/pdfbox/cos/COSDictionary;

    if-eqz v2, :cond_0

    check-cast v1, Lorg/apache/pdfbox/cos/COSDictionary;

    sget-object v2, Lorg/apache/pdfbox/cos/COSName;->P:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v1, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {v1, v2, p2}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method private updateStructParentEntries(Lorg/apache/pdfbox/pdmodel/PDPage;I)V
    .locals 4

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/PDPage;->getStructParents()I

    move-result v0

    add-int/2addr v0, p2

    invoke-virtual {p1, v0}, Lorg/apache/pdfbox/pdmodel/PDPage;->setStructParents(I)V

    invoke-virtual {p1}, Lorg/apache/pdfbox/pdmodel/PDPage;->getAnnotations()Ljava/util/List;

    move-result-object v0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotation;

    invoke-virtual {v2}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotation;->getStructParent()I

    move-result v3

    add-int/2addr v3, p2

    invoke-virtual {v2, v3}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotation;->setStructParent(I)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    invoke-virtual {p1, v1}, Lorg/apache/pdfbox/pdmodel/PDPage;->setAnnotations(Ljava/util/List;)V

    return-void
.end method


# virtual methods
.method public addSource(Ljava/io/File;)V
    .locals 2

    .line 1
    iget-object v0, p0, Lorg/apache/pdfbox/multipdf/PDFMergerUtility;->sources:Ljava/util/List;

    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public addSource(Ljava/io/InputStream;)V
    .locals 1

    .line 2
    iget-object v0, p0, Lorg/apache/pdfbox/multipdf/PDFMergerUtility;->sources:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public addSource(Ljava/lang/String;)V
    .locals 3

    .line 3
    iget-object v0, p0, Lorg/apache/pdfbox/multipdf/PDFMergerUtility;->sources:Ljava/util/List;

    new-instance v1, Ljava/io/FileInputStream;

    new-instance v2, Ljava/io/File;

    invoke-direct {v2, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v2}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public addSources(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/io/InputStream;",
            ">;)V"
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/pdfbox/multipdf/PDFMergerUtility;->sources:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    return-void
.end method

.method public appendDocument(Lorg/apache/pdfbox/pdmodel/PDDocument;Lorg/apache/pdfbox/pdmodel/PDDocument;)V
    .locals 20

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    invoke-virtual/range {p1 .. p1}, Lorg/apache/pdfbox/pdmodel/PDDocument;->isEncrypted()Z

    move-result v0

    if-nez v0, :cond_1e

    invoke-virtual/range {p2 .. p2}, Lorg/apache/pdfbox/pdmodel/PDDocument;->isEncrypted()Z

    move-result v0

    if-nez v0, :cond_1d

    invoke-virtual/range {p1 .. p1}, Lorg/apache/pdfbox/pdmodel/PDDocument;->getDocumentCatalog()Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;

    move-result-object v3

    invoke-virtual/range {p2 .. p2}, Lorg/apache/pdfbox/pdmodel/PDDocument;->getDocumentCatalog()Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;

    move-result-object v4

    invoke-virtual {v4}, Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;->getAcroForm()Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;

    move-result-object v0

    invoke-direct {v1, v0}, Lorg/apache/pdfbox/multipdf/PDFMergerUtility;->isDynamicXfa(Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;)Z

    move-result v0

    if-nez v0, :cond_1c

    invoke-virtual/range {p1 .. p1}, Lorg/apache/pdfbox/pdmodel/PDDocument;->getDocumentInformation()Lorg/apache/pdfbox/pdmodel/PDDocumentInformation;

    move-result-object v0

    invoke-virtual/range {p2 .. p2}, Lorg/apache/pdfbox/pdmodel/PDDocument;->getDocumentInformation()Lorg/apache/pdfbox/pdmodel/PDDocumentInformation;

    move-result-object v5

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/PDDocumentInformation;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    invoke-virtual {v5}, Lorg/apache/pdfbox/pdmodel/PDDocumentInformation;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v5

    invoke-virtual {v0, v5}, Lorg/apache/pdfbox/cos/COSDictionary;->mergeInto(Lorg/apache/pdfbox/cos/COSDictionary;)V

    invoke-virtual/range {p1 .. p1}, Lorg/apache/pdfbox/pdmodel/PDDocument;->getVersion()F

    move-result v0

    invoke-virtual/range {p2 .. p2}, Lorg/apache/pdfbox/pdmodel/PDDocument;->getVersion()F

    move-result v5

    cmpg-float v0, v0, v5

    if-gez v0, :cond_0

    invoke-virtual {v2, v5}, Lorg/apache/pdfbox/pdmodel/PDDocument;->setVersion(F)V

    :cond_0
    invoke-virtual {v3}, Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;->getOpenAction()Lorg/apache/pdfbox/pdmodel/common/PDDestinationOrAction;

    move-result-object v0

    if-nez v0, :cond_1

    invoke-virtual {v4}, Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;->getOpenAction()Lorg/apache/pdfbox/pdmodel/common/PDDestinationOrAction;

    move-result-object v0

    invoke-virtual {v3, v0}, Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;->setOpenAction(Lorg/apache/pdfbox/pdmodel/common/PDDestinationOrAction;)V

    :cond_1
    new-instance v5, Lorg/apache/pdfbox/multipdf/PDFCloneUtility;

    invoke-direct {v5, v2}, Lorg/apache/pdfbox/multipdf/PDFCloneUtility;-><init>(Lorg/apache/pdfbox/pdmodel/PDDocument;)V

    :try_start_0
    invoke-virtual {v3}, Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;->getAcroForm()Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;

    move-result-object v0

    invoke-virtual {v4}, Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;->getAcroForm()Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;

    move-result-object v6

    if-nez v0, :cond_2

    if-eqz v6, :cond_2

    invoke-virtual {v3}, Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;->getCOSObject()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v7, Lorg/apache/pdfbox/cos/COSName;->ACRO_FORM:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v6}, Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v6

    invoke-virtual {v5, v6}, Lorg/apache/pdfbox/multipdf/PDFCloneUtility;->cloneForNewDocument(Ljava/lang/Object;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v6

    invoke-virtual {v0, v7, v6}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    goto :goto_0

    :cond_2
    if-eqz v6, :cond_3

    invoke-direct {v1, v5, v0, v6}, Lorg/apache/pdfbox/multipdf/PDFMergerUtility;->mergeAcroForm(Lorg/apache/pdfbox/multipdf/PDFCloneUtility;Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;Lorg/apache/pdfbox/pdmodel/interactive/form/PDAcroForm;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    iget-boolean v6, v1, Lorg/apache/pdfbox/multipdf/PDFMergerUtility;->ignoreAcroFormErrors:Z

    if-eqz v6, :cond_1b

    :cond_3
    :goto_0
    invoke-virtual {v3}, Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;->getCOSObject()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v6, Lorg/apache/pdfbox/cos/COSName;->THREADS:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v6}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSArray;

    invoke-virtual {v3}, Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;->getCOSObject()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v7

    invoke-virtual {v7, v6}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v7

    invoke-virtual {v5, v7}, Lorg/apache/pdfbox/multipdf/PDFCloneUtility;->cloneForNewDocument(Ljava/lang/Object;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v7

    check-cast v7, Lorg/apache/pdfbox/cos/COSArray;

    if-nez v0, :cond_4

    invoke-virtual {v3}, Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;->getCOSObject()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    invoke-virtual {v0, v6, v7}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    goto :goto_1

    :cond_4
    invoke-virtual {v0, v7}, Lorg/apache/pdfbox/cos/COSArray;->addAll(Lorg/apache/pdfbox/cos/COSArray;)V

    :goto_1
    invoke-virtual {v3}, Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;->getNames()Lorg/apache/pdfbox/pdmodel/PDDocumentNameDictionary;

    move-result-object v0

    invoke-virtual {v4}, Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;->getNames()Lorg/apache/pdfbox/pdmodel/PDDocumentNameDictionary;

    move-result-object v6

    if-eqz v6, :cond_6

    if-nez v0, :cond_5

    invoke-virtual {v3}, Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;->getCOSObject()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v7, Lorg/apache/pdfbox/cos/COSName;->NAMES:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v5, v6}, Lorg/apache/pdfbox/multipdf/PDFCloneUtility;->cloneForNewDocument(Ljava/lang/Object;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v6

    invoke-virtual {v0, v7, v6}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    goto :goto_2

    :cond_5
    invoke-virtual {v5, v6, v0}, Lorg/apache/pdfbox/multipdf/PDFCloneUtility;->cloneMerge(Lorg/apache/pdfbox/pdmodel/common/COSObjectable;Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)V

    :cond_6
    :goto_2
    invoke-virtual {v3}, Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;->getDocumentOutline()Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDDocumentOutline;

    move-result-object v0

    invoke-virtual {v4}, Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;->getDocumentOutline()Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDDocumentOutline;

    move-result-object v6

    if-eqz v6, :cond_8

    if-nez v0, :cond_7

    new-instance v0, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDDocumentOutline;

    invoke-virtual {v5, v6}, Lorg/apache/pdfbox/multipdf/PDFCloneUtility;->cloneForNewDocument(Ljava/lang/Object;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v6

    check-cast v6, Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-direct {v0, v6}, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDDocumentOutline;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    invoke-virtual {v3, v0}, Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;->setDocumentOutline(Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDDocumentOutline;)V

    goto :goto_3

    :cond_7
    invoke-virtual {v6}, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineNode;->getFirstChild()Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineItem;

    move-result-object v6

    if-eqz v6, :cond_8

    new-instance v7, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineItem;

    invoke-virtual {v5, v6}, Lorg/apache/pdfbox/multipdf/PDFCloneUtility;->cloneForNewDocument(Ljava/lang/Object;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v6

    check-cast v6, Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-direct {v7, v6}, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineItem;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    invoke-virtual {v0, v7}, Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineNode;->addLast(Lorg/apache/pdfbox/pdmodel/interactive/documentnavigation/outline/PDOutlineItem;)V

    :cond_8
    :goto_3
    invoke-virtual {v3}, Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;->getPageMode()Lorg/apache/pdfbox/pdmodel/PageMode;

    move-result-object v0

    invoke-virtual {v4}, Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;->getPageMode()Lorg/apache/pdfbox/pdmodel/PageMode;

    move-result-object v6

    if-nez v0, :cond_9

    invoke-virtual {v3, v6}, Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;->setPageMode(Lorg/apache/pdfbox/pdmodel/PageMode;)V

    :cond_9
    invoke-virtual {v3}, Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;->getCOSObject()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v6, Lorg/apache/pdfbox/cos/COSName;->PAGE_LABELS:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v6}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-virtual {v4}, Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;->getCOSObject()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v7

    invoke-virtual {v7, v6}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v7

    check-cast v7, Lorg/apache/pdfbox/cos/COSDictionary;

    const/4 v8, 0x0

    if-eqz v7, :cond_b

    invoke-virtual/range {p1 .. p1}, Lorg/apache/pdfbox/pdmodel/PDDocument;->getNumberOfPages()I

    move-result v9

    if-nez v0, :cond_a

    new-instance v0, Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-direct {v0}, Lorg/apache/pdfbox/cos/COSDictionary;-><init>()V

    new-instance v10, Lorg/apache/pdfbox/cos/COSArray;

    invoke-direct {v10}, Lorg/apache/pdfbox/cos/COSArray;-><init>()V

    sget-object v11, Lorg/apache/pdfbox/cos/COSName;->NUMS:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v11, v10}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    invoke-virtual {v3}, Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;->getCOSObject()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v11

    invoke-virtual {v11, v6, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    goto :goto_4

    :cond_a
    sget-object v6, Lorg/apache/pdfbox/cos/COSName;->NUMS:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v6}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lorg/apache/pdfbox/cos/COSArray;

    :goto_4
    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->NUMS:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v7, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSArray;

    if-eqz v0, :cond_b

    move v6, v8

    :goto_5
    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSArray;->size()I

    move-result v7

    if-ge v6, v7, :cond_b

    invoke-virtual {v0, v6}, Lorg/apache/pdfbox/cos/COSArray;->getObject(I)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v7

    check-cast v7, Lorg/apache/pdfbox/cos/COSNumber;

    invoke-virtual {v7}, Lorg/apache/pdfbox/cos/COSNumber;->intValue()I

    move-result v7

    int-to-long v11, v7

    int-to-long v13, v9

    add-long/2addr v11, v13

    invoke-static {v11, v12}, Lorg/apache/pdfbox/cos/COSInteger;->get(J)Lorg/apache/pdfbox/cos/COSInteger;

    move-result-object v7

    invoke-virtual {v10, v7}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    add-int/lit8 v7, v6, 0x1

    invoke-virtual {v0, v7}, Lorg/apache/pdfbox/cos/COSArray;->getObject(I)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v7

    invoke-virtual {v5, v7}, Lorg/apache/pdfbox/multipdf/PDFCloneUtility;->cloneForNewDocument(Ljava/lang/Object;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v7

    invoke-virtual {v10, v7}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    add-int/lit8 v6, v6, 0x2

    goto :goto_5

    :cond_b
    invoke-virtual {v3}, Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;->getCOSObject()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v0

    sget-object v6, Lorg/apache/pdfbox/cos/COSName;->METADATA:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v6}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    check-cast v0, Lorg/apache/pdfbox/cos/COSStream;

    invoke-virtual {v4}, Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;->getCOSObject()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v7

    invoke-virtual {v7, v6}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v7

    check-cast v7, Lorg/apache/pdfbox/cos/COSStream;

    if-nez v0, :cond_c

    if-eqz v7, :cond_c

    new-instance v0, Lorg/apache/pdfbox/pdmodel/common/PDStream;

    invoke-virtual {v7}, Lorg/apache/pdfbox/cos/COSStream;->getUnfilteredStream()Ljava/io/InputStream;

    move-result-object v9

    invoke-direct {v0, v2, v9, v8}, Lorg/apache/pdfbox/pdmodel/common/PDStream;-><init>(Lorg/apache/pdfbox/pdmodel/PDDocument;Ljava/io/InputStream;Z)V

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/common/PDStream;->getStream()Lorg/apache/pdfbox/cos/COSStream;

    move-result-object v9

    invoke-virtual {v9, v7}, Lorg/apache/pdfbox/cos/COSDictionary;->mergeInto(Lorg/apache/pdfbox/cos/COSDictionary;)V

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/common/PDStream;->addCompression()V

    invoke-virtual {v3}, Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;->getCOSObject()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v7

    invoke-virtual {v7, v6, v0}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)V

    :cond_c
    invoke-virtual {v3}, Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;->getMarkInfo()Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDMarkInfo;

    move-result-object v0

    invoke-virtual {v3}, Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;->getStructureTreeRoot()Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureTreeRoot;

    move-result-object v6

    invoke-virtual {v4}, Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;->getMarkInfo()Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDMarkInfo;

    invoke-virtual {v4}, Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;->getStructureTreeRoot()Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureTreeRoot;

    move-result-object v7

    const/4 v9, 0x0

    if-eqz v6, :cond_13

    invoke-virtual {v6}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureTreeRoot;->getParentTree()Lorg/apache/pdfbox/pdmodel/common/PDNumberTreeNode;

    move-result-object v11

    invoke-virtual {v6}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureTreeRoot;->getParentTreeNextKey()I

    move-result v12

    if-eqz v11, :cond_10

    invoke-virtual {v11}, Lorg/apache/pdfbox/pdmodel/common/PDNumberTreeNode;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v11

    sget-object v13, Lorg/apache/pdfbox/cos/COSName;->NUMS:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v11, v13}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v14

    check-cast v14, Lorg/apache/pdfbox/cos/COSArray;

    if-eqz v14, :cond_f

    if-gez v12, :cond_d

    invoke-virtual {v14}, Lorg/apache/pdfbox/cos/COSArray;->size()I

    move-result v12

    div-int/lit8 v12, v12, 0x2

    :cond_d
    if-lez v12, :cond_f

    if-eqz v7, :cond_f

    invoke-virtual {v7}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureTreeRoot;->getParentTree()Lorg/apache/pdfbox/pdmodel/common/PDNumberTreeNode;

    move-result-object v15

    if-eqz v15, :cond_f

    invoke-virtual {v15}, Lorg/apache/pdfbox/pdmodel/common/PDNumberTreeNode;->getCOSDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v15

    invoke-virtual {v15, v13}, Lorg/apache/pdfbox/cos/COSDictionary;->getDictionaryObject(Lorg/apache/pdfbox/cos/COSName;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v13

    check-cast v13, Lorg/apache/pdfbox/cos/COSArray;

    if-eqz v13, :cond_e

    const/4 v15, 0x1

    goto :goto_6

    :cond_e
    move v15, v8

    goto :goto_6

    :cond_f
    move v15, v8

    move-object v13, v9

    goto :goto_6

    :cond_10
    move v15, v8

    move-object v11, v9

    move-object v13, v11

    move-object v14, v13

    :goto_6
    if-eqz v0, :cond_11

    invoke-virtual {v0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDMarkInfo;->isMarked()Z

    move-result v16

    if-eqz v16, :cond_11

    if-nez v15, :cond_11

    invoke-virtual {v0, v8}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDMarkInfo;->setMarked(Z)V

    :cond_11
    if-nez v15, :cond_12

    invoke-virtual {v3, v9}, Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;->setStructureTreeRoot(Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureTreeRoot;)V

    :cond_12
    move-object v9, v13

    goto :goto_7

    :cond_13
    const/4 v12, -0x1

    move v15, v8

    move-object v11, v9

    move-object v14, v11

    :goto_7
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-virtual {v4}, Lorg/apache/pdfbox/pdmodel/PDDocumentCatalog;->getPages()Lorg/apache/pdfbox/pdmodel/PDPageTree;

    move-result-object v3

    invoke-virtual {v3}, Lorg/apache/pdfbox/pdmodel/PDPageTree;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_8
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_16

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/pdfbox/pdmodel/PDPage;

    new-instance v13, Lorg/apache/pdfbox/pdmodel/PDPage;

    invoke-virtual {v4}, Lorg/apache/pdfbox/pdmodel/PDPage;->getCOSObject()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v8

    invoke-virtual {v5, v8}, Lorg/apache/pdfbox/multipdf/PDFCloneUtility;->cloneForNewDocument(Ljava/lang/Object;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v8

    check-cast v8, Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-direct {v13, v8}, Lorg/apache/pdfbox/pdmodel/PDPage;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    invoke-virtual {v4}, Lorg/apache/pdfbox/pdmodel/PDPage;->getCropBox()Lorg/apache/pdfbox/pdmodel/common/PDRectangle;

    move-result-object v8

    invoke-virtual {v13, v8}, Lorg/apache/pdfbox/pdmodel/PDPage;->setCropBox(Lorg/apache/pdfbox/pdmodel/common/PDRectangle;)V

    invoke-virtual {v4}, Lorg/apache/pdfbox/pdmodel/PDPage;->getMediaBox()Lorg/apache/pdfbox/pdmodel/common/PDRectangle;

    move-result-object v8

    invoke-virtual {v13, v8}, Lorg/apache/pdfbox/pdmodel/PDPage;->setMediaBox(Lorg/apache/pdfbox/pdmodel/common/PDRectangle;)V

    invoke-virtual {v4}, Lorg/apache/pdfbox/pdmodel/PDPage;->getRotation()I

    move-result v8

    invoke-virtual {v13, v8}, Lorg/apache/pdfbox/pdmodel/PDPage;->setRotation(I)V

    new-instance v8, Lorg/apache/pdfbox/pdmodel/PDResources;

    invoke-virtual {v4}, Lorg/apache/pdfbox/pdmodel/PDPage;->getResources()Lorg/apache/pdfbox/pdmodel/PDResources;

    move-result-object v10

    invoke-virtual {v5, v10}, Lorg/apache/pdfbox/multipdf/PDFCloneUtility;->cloneForNewDocument(Ljava/lang/Object;)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v10

    check-cast v10, Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-direct {v8, v10}, Lorg/apache/pdfbox/pdmodel/PDResources;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;)V

    invoke-virtual {v13, v8}, Lorg/apache/pdfbox/pdmodel/PDPage;->setResources(Lorg/apache/pdfbox/pdmodel/PDResources;)V

    if-eqz v15, :cond_14

    invoke-direct {v1, v13, v12}, Lorg/apache/pdfbox/multipdf/PDFMergerUtility;->updateStructParentEntries(Lorg/apache/pdfbox/pdmodel/PDPage;I)V

    invoke-virtual {v4}, Lorg/apache/pdfbox/pdmodel/PDPage;->getCOSObject()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v8

    invoke-virtual {v13}, Lorg/apache/pdfbox/pdmodel/PDPage;->getCOSObject()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v10

    invoke-interface {v0, v8, v10}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v4}, Lorg/apache/pdfbox/pdmodel/PDPage;->getAnnotations()Ljava/util/List;

    move-result-object v4

    invoke-virtual {v13}, Lorg/apache/pdfbox/pdmodel/PDPage;->getAnnotations()Ljava/util/List;

    move-result-object v8

    move-object/from16 v17, v3

    const/4 v10, 0x0

    :goto_9
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v3

    if-ge v10, v3, :cond_15

    invoke-interface {v4, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotation;

    invoke-virtual {v3}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotation;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v3

    invoke-interface {v8, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotation;

    move-object/from16 v19, v4

    invoke-virtual/range {v18 .. v18}, Lorg/apache/pdfbox/pdmodel/interactive/annotation/PDAnnotation;->getDictionary()Lorg/apache/pdfbox/cos/COSDictionary;

    move-result-object v4

    invoke-interface {v0, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v10, v10, 0x1

    move-object/from16 v4, v19

    goto :goto_9

    :cond_14
    move-object/from16 v17, v3

    :cond_15
    invoke-virtual {v2, v13}, Lorg/apache/pdfbox/pdmodel/PDDocument;->addPage(Lorg/apache/pdfbox/pdmodel/PDPage;)V

    move-object/from16 v3, v17

    const/4 v8, 0x0

    goto/16 :goto_8

    :cond_16
    if-eqz v15, :cond_1a

    invoke-direct {v1, v9, v0}, Lorg/apache/pdfbox/multipdf/PDFMergerUtility;->updatePageReferences(Lorg/apache/pdfbox/cos/COSArray;Ljava/util/Map;)V

    const/4 v8, 0x0

    :goto_a
    invoke-virtual {v9}, Lorg/apache/pdfbox/cos/COSArray;->size()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    if-ge v8, v0, :cond_17

    add-int v0, v12, v8

    int-to-long v2, v0

    invoke-static {v2, v3}, Lorg/apache/pdfbox/cos/COSInteger;->get(J)Lorg/apache/pdfbox/cos/COSInteger;

    move-result-object v0

    invoke-virtual {v14, v0}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    mul-int/lit8 v0, v8, 0x2

    const/4 v2, 0x1

    add-int/2addr v0, v2

    invoke-virtual {v9, v0}, Lorg/apache/pdfbox/cos/COSArray;->getObject(I)Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    invoke-virtual {v14, v0}, Lorg/apache/pdfbox/cos/COSArray;->add(Lorg/apache/pdfbox/cos/COSBase;)V

    add-int/lit8 v8, v8, 0x1

    goto :goto_a

    :cond_17
    invoke-virtual {v9}, Lorg/apache/pdfbox/cos/COSArray;->size()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    add-int/2addr v12, v0

    sget-object v0, Lorg/apache/pdfbox/cos/COSName;->NUMS:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v11, v0, v14}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    new-instance v0, Lorg/apache/pdfbox/pdmodel/common/PDNumberTreeNode;

    const-class v2, Lorg/apache/pdfbox/cos/COSBase;

    invoke-direct {v0, v11, v2}, Lorg/apache/pdfbox/pdmodel/common/PDNumberTreeNode;-><init>(Lorg/apache/pdfbox/cos/COSDictionary;Ljava/lang/Class;)V

    invoke-virtual {v6, v0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureTreeRoot;->setParentTree(Lorg/apache/pdfbox/pdmodel/common/PDNumberTreeNode;)V

    invoke-virtual {v6, v12}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureTreeRoot;->setParentTreeNextKey(I)V

    new-instance v0, Lorg/apache/pdfbox/cos/COSDictionary;

    invoke-direct {v0}, Lorg/apache/pdfbox/cos/COSDictionary;-><init>()V

    new-instance v2, Lorg/apache/pdfbox/cos/COSArray;

    invoke-direct {v2}, Lorg/apache/pdfbox/cos/COSArray;-><init>()V

    invoke-virtual {v6}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureTreeRoot;->getKArray()Lorg/apache/pdfbox/cos/COSArray;

    move-result-object v3

    invoke-virtual {v7}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureTreeRoot;->getKArray()Lorg/apache/pdfbox/cos/COSArray;

    move-result-object v4

    if-eqz v3, :cond_19

    if-eqz v4, :cond_19

    invoke-direct {v1, v3, v0}, Lorg/apache/pdfbox/multipdf/PDFMergerUtility;->updateParentEntry(Lorg/apache/pdfbox/cos/COSArray;Lorg/apache/pdfbox/cos/COSDictionary;)V

    invoke-virtual {v2, v3}, Lorg/apache/pdfbox/cos/COSArray;->addAll(Lorg/apache/pdfbox/cos/COSArray;)V

    if-eqz v15, :cond_18

    invoke-direct {v1, v4, v0}, Lorg/apache/pdfbox/multipdf/PDFMergerUtility;->updateParentEntry(Lorg/apache/pdfbox/cos/COSArray;Lorg/apache/pdfbox/cos/COSDictionary;)V

    :cond_18
    invoke-virtual {v2, v4}, Lorg/apache/pdfbox/cos/COSArray;->addAll(Lorg/apache/pdfbox/cos/COSArray;)V

    :cond_19
    sget-object v3, Lorg/apache/pdfbox/cos/COSName;->K:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v3, v2}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    sget-object v2, Lorg/apache/pdfbox/cos/COSName;->P:Lorg/apache/pdfbox/cos/COSName;

    invoke-virtual {v0, v2, v6}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/pdmodel/common/COSObjectable;)V

    sget-object v2, Lorg/apache/pdfbox/cos/COSName;->S:Lorg/apache/pdfbox/cos/COSName;

    new-instance v3, Lorg/apache/pdfbox/cos/COSString;

    const-string v4, "Document"

    invoke-direct {v3, v4}, Lorg/apache/pdfbox/cos/COSString;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2, v3}, Lorg/apache/pdfbox/cos/COSDictionary;->setItem(Lorg/apache/pdfbox/cos/COSName;Lorg/apache/pdfbox/cos/COSBase;)V

    invoke-virtual {v6, v0}, Lorg/apache/pdfbox/pdmodel/documentinterchange/logicalstructure/PDStructureTreeRoot;->setK(Lorg/apache/pdfbox/cos/COSBase;)V

    :cond_1a
    return-void

    :cond_1b
    new-instance v2, Ljava/io/IOException;

    invoke-direct {v2, v0}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v2

    :cond_1c
    new-instance v0, Ljava/io/IOException;

    const-string v2, "Error: can\'t merge source document containing dynamic XFA form content."

    invoke-direct {v0, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1d
    new-instance v0, Ljava/io/IOException;

    const-string v2, "Error: source PDF is encrypted, can\'t append encrypted PDF documents."

    invoke-direct {v0, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1e
    new-instance v0, Ljava/io/IOException;

    const-string v2, "Error: destination PDF is encrypted, can\'t append encrypted PDF documents."

    invoke-direct {v0, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public getDestinationFileName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/multipdf/PDFMergerUtility;->destinationFileName:Ljava/lang/String;

    return-object v0
.end method

.method public getDestinationStream()Ljava/io/OutputStream;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/multipdf/PDFMergerUtility;->destinationStream:Ljava/io/OutputStream;

    return-object v0
.end method

.method public isIgnoreAcroFormErrors()Z
    .locals 1

    iget-boolean v0, p0, Lorg/apache/pdfbox/multipdf/PDFMergerUtility;->ignoreAcroFormErrors:Z

    return v0
.end method

.method public mergeDocuments()V
    .locals 4

    iget-object v0, p0, Lorg/apache/pdfbox/multipdf/PDFMergerUtility;->sources:Ljava/util/List;

    if-eqz v0, :cond_4

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_4

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    const/4 v1, 0x0

    :try_start_0
    iget-object v2, p0, Lorg/apache/pdfbox/multipdf/PDFMergerUtility;->sources:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    new-instance v3, Lorg/apache/pdfbox/pdmodel/PDDocument;

    invoke-direct {v3}, Lorg/apache/pdfbox/pdmodel/PDDocument;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :goto_0
    :try_start_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/io/InputStream;

    invoke-static {v1}, Lorg/apache/pdfbox/pdmodel/PDDocument;->load(Ljava/io/InputStream;)Lorg/apache/pdfbox/pdmodel/PDDocument;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0, v3, v1}, Lorg/apache/pdfbox/multipdf/PDFMergerUtility;->appendDocument(Lorg/apache/pdfbox/pdmodel/PDDocument;Lorg/apache/pdfbox/pdmodel/PDDocument;)V

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lorg/apache/pdfbox/multipdf/PDFMergerUtility;->destinationStream:Ljava/io/OutputStream;

    if-nez v1, :cond_1

    iget-object v1, p0, Lorg/apache/pdfbox/multipdf/PDFMergerUtility;->destinationFileName:Ljava/lang/String;

    invoke-virtual {v3, v1}, Lorg/apache/pdfbox/pdmodel/PDDocument;->save(Ljava/lang/String;)V

    goto :goto_1

    :cond_1
    invoke-virtual {v3, v1}, Lorg/apache/pdfbox/pdmodel/PDDocument;->save(Ljava/io/OutputStream;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_1
    invoke-virtual {v3}, Lorg/apache/pdfbox/pdmodel/PDDocument;->close()V

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/pdfbox/pdmodel/PDDocument;

    invoke-virtual {v1}, Lorg/apache/pdfbox/pdmodel/PDDocument;->close()V

    goto :goto_2

    :catchall_0
    move-exception v1

    goto :goto_3

    :catchall_1
    move-exception v2

    move-object v3, v1

    move-object v1, v2

    :goto_3
    if-eqz v3, :cond_2

    invoke-virtual {v3}, Lorg/apache/pdfbox/pdmodel/PDDocument;->close()V

    :cond_2
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_4
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/pdfbox/pdmodel/PDDocument;

    invoke-virtual {v2}, Lorg/apache/pdfbox/pdmodel/PDDocument;->close()V

    goto :goto_4

    :cond_3
    throw v1

    :cond_4
    return-void
.end method

.method public setDestinationFileName(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lorg/apache/pdfbox/multipdf/PDFMergerUtility;->destinationFileName:Ljava/lang/String;

    return-void
.end method

.method public setDestinationStream(Ljava/io/OutputStream;)V
    .locals 0

    iput-object p1, p0, Lorg/apache/pdfbox/multipdf/PDFMergerUtility;->destinationStream:Ljava/io/OutputStream;

    return-void
.end method

.method public setIgnoreAcroFormErrors(Z)V
    .locals 0

    iput-boolean p1, p0, Lorg/apache/pdfbox/multipdf/PDFMergerUtility;->ignoreAcroFormErrors:Z

    return-void
.end method

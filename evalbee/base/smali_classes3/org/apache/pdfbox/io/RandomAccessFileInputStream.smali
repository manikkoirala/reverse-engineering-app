.class public Lorg/apache/pdfbox/io/RandomAccessFileInputStream;
.super Ljava/io/InputStream;
.source "SourceFile"


# instance fields
.field private currentPosition:J

.field private final endPosition:J

.field private final file:Lorg/apache/pdfbox/io/RandomAccess;


# direct methods
.method public constructor <init>(Lorg/apache/pdfbox/io/RandomAccess;JJ)V
    .locals 0

    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    iput-object p1, p0, Lorg/apache/pdfbox/io/RandomAccessFileInputStream;->file:Lorg/apache/pdfbox/io/RandomAccess;

    iput-wide p2, p0, Lorg/apache/pdfbox/io/RandomAccessFileInputStream;->currentPosition:J

    add-long/2addr p2, p4

    iput-wide p2, p0, Lorg/apache/pdfbox/io/RandomAccessFileInputStream;->endPosition:J

    return-void
.end method


# virtual methods
.method public available()I
    .locals 4

    iget-wide v0, p0, Lorg/apache/pdfbox/io/RandomAccessFileInputStream;->endPosition:J

    iget-wide v2, p0, Lorg/apache/pdfbox/io/RandomAccessFileInputStream;->currentPosition:J

    sub-long/2addr v0, v2

    long-to-int v0, v0

    return v0
.end method

.method public close()V
    .locals 0

    return-void
.end method

.method public read()I
    .locals 5

    .line 1
    iget-object v0, p0, Lorg/apache/pdfbox/io/RandomAccessFileInputStream;->file:Lorg/apache/pdfbox/io/RandomAccess;

    monitor-enter v0

    :try_start_0
    iget-wide v1, p0, Lorg/apache/pdfbox/io/RandomAccessFileInputStream;->currentPosition:J

    iget-wide v3, p0, Lorg/apache/pdfbox/io/RandomAccessFileInputStream;->endPosition:J

    cmp-long v3, v1, v3

    if-gez v3, :cond_0

    iget-object v3, p0, Lorg/apache/pdfbox/io/RandomAccessFileInputStream;->file:Lorg/apache/pdfbox/io/RandomAccess;

    invoke-interface {v3, v1, v2}, Lorg/apache/pdfbox/io/RandomAccessRead;->seek(J)V

    iget-wide v1, p0, Lorg/apache/pdfbox/io/RandomAccessFileInputStream;->currentPosition:J

    const-wide/16 v3, 0x1

    add-long/2addr v1, v3

    iput-wide v1, p0, Lorg/apache/pdfbox/io/RandomAccessFileInputStream;->currentPosition:J

    iget-object v1, p0, Lorg/apache/pdfbox/io/RandomAccessFileInputStream;->file:Lorg/apache/pdfbox/io/RandomAccess;

    invoke-interface {v1}, Lorg/apache/pdfbox/io/SequentialRead;->read()I

    move-result v1

    goto :goto_0

    :cond_0
    const/4 v1, -0x1

    :goto_0
    monitor-exit v0

    return v1

    :catchall_0
    move-exception v1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public read([BII)I
    .locals 4

    .line 2
    invoke-virtual {p0}, Lorg/apache/pdfbox/io/RandomAccessFileInputStream;->available()I

    move-result v0

    if-le p3, v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/pdfbox/io/RandomAccessFileInputStream;->available()I

    move-result p3

    :cond_0
    invoke-virtual {p0}, Lorg/apache/pdfbox/io/RandomAccessFileInputStream;->available()I

    move-result v0

    if-lez v0, :cond_1

    iget-object v0, p0, Lorg/apache/pdfbox/io/RandomAccessFileInputStream;->file:Lorg/apache/pdfbox/io/RandomAccess;

    monitor-enter v0

    :try_start_0
    iget-object v1, p0, Lorg/apache/pdfbox/io/RandomAccessFileInputStream;->file:Lorg/apache/pdfbox/io/RandomAccess;

    iget-wide v2, p0, Lorg/apache/pdfbox/io/RandomAccessFileInputStream;->currentPosition:J

    invoke-interface {v1, v2, v3}, Lorg/apache/pdfbox/io/RandomAccessRead;->seek(J)V

    iget-object v1, p0, Lorg/apache/pdfbox/io/RandomAccessFileInputStream;->file:Lorg/apache/pdfbox/io/RandomAccess;

    invoke-interface {v1, p1, p2, p3}, Lorg/apache/pdfbox/io/SequentialRead;->read([BII)I

    move-result p1

    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception p1

    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1

    :cond_1
    const/4 p1, -0x1

    :goto_0
    if-lez p1, :cond_2

    iget-wide p2, p0, Lorg/apache/pdfbox/io/RandomAccessFileInputStream;->currentPosition:J

    int-to-long v0, p1

    add-long/2addr p2, v0

    iput-wide p2, p0, Lorg/apache/pdfbox/io/RandomAccessFileInputStream;->currentPosition:J

    :cond_2
    return p1
.end method

.method public skip(J)J
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/io/RandomAccessFileInputStream;->available()I

    move-result v0

    int-to-long v0, v0

    invoke-static {p1, p2, v0, v1}, Ljava/lang/Math;->min(JJ)J

    move-result-wide p1

    iget-wide v0, p0, Lorg/apache/pdfbox/io/RandomAccessFileInputStream;->currentPosition:J

    add-long/2addr v0, p1

    iput-wide v0, p0, Lorg/apache/pdfbox/io/RandomAccessFileInputStream;->currentPosition:J

    return-wide p1
.end method

.class public Lorg/apache/pdfbox/io/RandomAccessBufferedFileInputStream;
.super Ljava/io/InputStream;
.source "SourceFile"

# interfaces
.implements Lorg/apache/pdfbox/io/RandomAccessRead;


# instance fields
.field private curPage:[B

.field private curPageOffset:J

.field private final fileLength:J

.field private fileOffset:J

.field private isClosed:Z

.field private lastRemovedCachePage:[B

.field private maxCachedPages:I

.field private offsetWithinPage:I

.field private final pageCache:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Long;",
            "[B>;"
        }
    .end annotation
.end field

.field private pageOffsetMask:J

.field private pageSize:I

.field private pageSizeShift:I

.field private final raFile:Ljava/io/RandomAccessFile;


# direct methods
.method public constructor <init>(Ljava/io/File;)V
    .locals 6

    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    const/16 v0, 0xc

    iput v0, p0, Lorg/apache/pdfbox/io/RandomAccessBufferedFileInputStream;->pageSizeShift:I

    const/4 v1, 0x1

    shl-int/2addr v1, v0

    iput v1, p0, Lorg/apache/pdfbox/io/RandomAccessBufferedFileInputStream;->pageSize:I

    const-wide/16 v1, -0x1

    shl-long v3, v1, v0

    iput-wide v3, p0, Lorg/apache/pdfbox/io/RandomAccessBufferedFileInputStream;->pageOffsetMask:J

    const/16 v0, 0x3e8

    iput v0, p0, Lorg/apache/pdfbox/io/RandomAccessBufferedFileInputStream;->maxCachedPages:I

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/pdfbox/io/RandomAccessBufferedFileInputStream;->lastRemovedCachePage:[B

    new-instance v0, Lorg/apache/pdfbox/io/RandomAccessBufferedFileInputStream$1;

    iget v3, p0, Lorg/apache/pdfbox/io/RandomAccessBufferedFileInputStream;->maxCachedPages:I

    const/high16 v4, 0x3f400000    # 0.75f

    const/4 v5, 0x1

    invoke-direct {v0, p0, v3, v4, v5}, Lorg/apache/pdfbox/io/RandomAccessBufferedFileInputStream$1;-><init>(Lorg/apache/pdfbox/io/RandomAccessBufferedFileInputStream;IFZ)V

    iput-object v0, p0, Lorg/apache/pdfbox/io/RandomAccessBufferedFileInputStream;->pageCache:Ljava/util/Map;

    iput-wide v1, p0, Lorg/apache/pdfbox/io/RandomAccessBufferedFileInputStream;->curPageOffset:J

    iget v0, p0, Lorg/apache/pdfbox/io/RandomAccessBufferedFileInputStream;->pageSize:I

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/apache/pdfbox/io/RandomAccessBufferedFileInputStream;->curPage:[B

    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/pdfbox/io/RandomAccessBufferedFileInputStream;->offsetWithinPage:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lorg/apache/pdfbox/io/RandomAccessBufferedFileInputStream;->fileOffset:J

    new-instance v2, Ljava/io/RandomAccessFile;

    const-string v3, "r"

    invoke-direct {v2, p1, v3}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v2, p0, Lorg/apache/pdfbox/io/RandomAccessBufferedFileInputStream;->raFile:Ljava/io/RandomAccessFile;

    invoke-virtual {p1}, Ljava/io/File;->length()J

    move-result-wide v2

    iput-wide v2, p0, Lorg/apache/pdfbox/io/RandomAccessBufferedFileInputStream;->fileLength:J

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/io/RandomAccessBufferedFileInputStream;->seek(J)V

    return-void
.end method

.method public static synthetic access$000(Lorg/apache/pdfbox/io/RandomAccessBufferedFileInputStream;)I
    .locals 0

    iget p0, p0, Lorg/apache/pdfbox/io/RandomAccessBufferedFileInputStream;->maxCachedPages:I

    return p0
.end method

.method public static synthetic access$102(Lorg/apache/pdfbox/io/RandomAccessBufferedFileInputStream;[B)[B
    .locals 0

    iput-object p1, p0, Lorg/apache/pdfbox/io/RandomAccessBufferedFileInputStream;->lastRemovedCachePage:[B

    return-object p1
.end method

.method private readPage()[B
    .locals 4

    iget-object v0, p0, Lorg/apache/pdfbox/io/RandomAccessBufferedFileInputStream;->lastRemovedCachePage:[B

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    iput-object v1, p0, Lorg/apache/pdfbox/io/RandomAccessBufferedFileInputStream;->lastRemovedCachePage:[B

    goto :goto_0

    :cond_0
    iget v0, p0, Lorg/apache/pdfbox/io/RandomAccessBufferedFileInputStream;->pageSize:I

    new-array v0, v0, [B

    :goto_0
    const/4 v1, 0x0

    :goto_1
    iget v2, p0, Lorg/apache/pdfbox/io/RandomAccessBufferedFileInputStream;->pageSize:I

    if-ge v1, v2, :cond_2

    iget-object v3, p0, Lorg/apache/pdfbox/io/RandomAccessBufferedFileInputStream;->raFile:Ljava/io/RandomAccessFile;

    sub-int/2addr v2, v1

    invoke-virtual {v3, v0, v1, v2}, Ljava/io/RandomAccessFile;->read([BII)I

    move-result v2

    if-gez v2, :cond_1

    goto :goto_2

    :cond_1
    add-int/2addr v1, v2

    goto :goto_1

    :cond_2
    :goto_2
    return-object v0
.end method


# virtual methods
.method public available()I
    .locals 4

    iget-wide v0, p0, Lorg/apache/pdfbox/io/RandomAccessBufferedFileInputStream;->fileLength:J

    iget-wide v2, p0, Lorg/apache/pdfbox/io/RandomAccessBufferedFileInputStream;->fileOffset:J

    sub-long/2addr v0, v2

    const-wide/32 v2, 0x7fffffff

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method

.method public close()V
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/io/RandomAccessBufferedFileInputStream;->raFile:Ljava/io/RandomAccessFile;

    invoke-virtual {v0}, Ljava/io/RandomAccessFile;->close()V

    iget-object v0, p0, Lorg/apache/pdfbox/io/RandomAccessBufferedFileInputStream;->pageCache:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/pdfbox/io/RandomAccessBufferedFileInputStream;->isClosed:Z

    return-void
.end method

.method public getFilePointer()J
    .locals 2

    iget-wide v0, p0, Lorg/apache/pdfbox/io/RandomAccessBufferedFileInputStream;->fileOffset:J

    return-wide v0
.end method

.method public getPosition()J
    .locals 2

    iget-wide v0, p0, Lorg/apache/pdfbox/io/RandomAccessBufferedFileInputStream;->fileOffset:J

    return-wide v0
.end method

.method public isClosed()Z
    .locals 1

    iget-boolean v0, p0, Lorg/apache/pdfbox/io/RandomAccessBufferedFileInputStream;->isClosed:Z

    return v0
.end method

.method public length()J
    .locals 2

    iget-wide v0, p0, Lorg/apache/pdfbox/io/RandomAccessBufferedFileInputStream;->fileLength:J

    return-wide v0
.end method

.method public read()I
    .locals 4

    .line 1
    iget-wide v0, p0, Lorg/apache/pdfbox/io/RandomAccessBufferedFileInputStream;->fileOffset:J

    iget-wide v2, p0, Lorg/apache/pdfbox/io/RandomAccessBufferedFileInputStream;->fileLength:J

    cmp-long v2, v0, v2

    if-ltz v2, :cond_0

    const/4 v0, -0x1

    return v0

    :cond_0
    iget v2, p0, Lorg/apache/pdfbox/io/RandomAccessBufferedFileInputStream;->offsetWithinPage:I

    iget v3, p0, Lorg/apache/pdfbox/io/RandomAccessBufferedFileInputStream;->pageSize:I

    if-ne v2, v3, :cond_1

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/io/RandomAccessBufferedFileInputStream;->seek(J)V

    :cond_1
    iget-wide v0, p0, Lorg/apache/pdfbox/io/RandomAccessBufferedFileInputStream;->fileOffset:J

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    iput-wide v0, p0, Lorg/apache/pdfbox/io/RandomAccessBufferedFileInputStream;->fileOffset:J

    iget-object v0, p0, Lorg/apache/pdfbox/io/RandomAccessBufferedFileInputStream;->curPage:[B

    iget v1, p0, Lorg/apache/pdfbox/io/RandomAccessBufferedFileInputStream;->offsetWithinPage:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lorg/apache/pdfbox/io/RandomAccessBufferedFileInputStream;->offsetWithinPage:I

    aget-byte v0, v0, v1

    and-int/lit16 v0, v0, 0xff

    return v0
.end method

.method public read([BII)I
    .locals 8

    .line 2
    iget-wide v0, p0, Lorg/apache/pdfbox/io/RandomAccessBufferedFileInputStream;->fileOffset:J

    iget-wide v2, p0, Lorg/apache/pdfbox/io/RandomAccessBufferedFileInputStream;->fileLength:J

    cmp-long v2, v0, v2

    if-ltz v2, :cond_0

    const/4 p1, -0x1

    return p1

    :cond_0
    iget v2, p0, Lorg/apache/pdfbox/io/RandomAccessBufferedFileInputStream;->offsetWithinPage:I

    iget v3, p0, Lorg/apache/pdfbox/io/RandomAccessBufferedFileInputStream;->pageSize:I

    if-ne v2, v3, :cond_1

    invoke-virtual {p0, v0, v1}, Lorg/apache/pdfbox/io/RandomAccessBufferedFileInputStream;->seek(J)V

    :cond_1
    iget v0, p0, Lorg/apache/pdfbox/io/RandomAccessBufferedFileInputStream;->pageSize:I

    iget v1, p0, Lorg/apache/pdfbox/io/RandomAccessBufferedFileInputStream;->offsetWithinPage:I

    sub-int/2addr v0, v1

    invoke-static {v0, p3}, Ljava/lang/Math;->min(II)I

    move-result p3

    iget-wide v0, p0, Lorg/apache/pdfbox/io/RandomAccessBufferedFileInputStream;->fileLength:J

    iget-wide v2, p0, Lorg/apache/pdfbox/io/RandomAccessBufferedFileInputStream;->fileOffset:J

    sub-long v4, v0, v2

    iget v6, p0, Lorg/apache/pdfbox/io/RandomAccessBufferedFileInputStream;->pageSize:I

    int-to-long v6, v6

    cmp-long v4, v4, v6

    if-gez v4, :cond_2

    sub-long/2addr v0, v2

    long-to-int v0, v0

    invoke-static {p3, v0}, Ljava/lang/Math;->min(II)I

    move-result p3

    :cond_2
    iget-object v0, p0, Lorg/apache/pdfbox/io/RandomAccessBufferedFileInputStream;->curPage:[B

    iget v1, p0, Lorg/apache/pdfbox/io/RandomAccessBufferedFileInputStream;->offsetWithinPage:I

    invoke-static {v0, v1, p1, p2, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget p1, p0, Lorg/apache/pdfbox/io/RandomAccessBufferedFileInputStream;->offsetWithinPage:I

    add-int/2addr p1, p3

    iput p1, p0, Lorg/apache/pdfbox/io/RandomAccessBufferedFileInputStream;->offsetWithinPage:I

    iget-wide p1, p0, Lorg/apache/pdfbox/io/RandomAccessBufferedFileInputStream;->fileOffset:J

    int-to-long v0, p3

    add-long/2addr p1, v0

    iput-wide p1, p0, Lorg/apache/pdfbox/io/RandomAccessBufferedFileInputStream;->fileOffset:J

    return p3
.end method

.method public seek(J)V
    .locals 5

    iget-wide v0, p0, Lorg/apache/pdfbox/io/RandomAccessBufferedFileInputStream;->pageOffsetMask:J

    and-long/2addr v0, p1

    iget-wide v2, p0, Lorg/apache/pdfbox/io/RandomAccessBufferedFileInputStream;->curPageOffset:J

    cmp-long v2, v0, v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lorg/apache/pdfbox/io/RandomAccessBufferedFileInputStream;->pageCache:Ljava/util/Map;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [B

    if-nez v2, :cond_0

    iget-object v2, p0, Lorg/apache/pdfbox/io/RandomAccessBufferedFileInputStream;->raFile:Ljava/io/RandomAccessFile;

    invoke-virtual {v2, v0, v1}, Ljava/io/RandomAccessFile;->seek(J)V

    invoke-direct {p0}, Lorg/apache/pdfbox/io/RandomAccessBufferedFileInputStream;->readPage()[B

    move-result-object v2

    iget-object v3, p0, Lorg/apache/pdfbox/io/RandomAccessBufferedFileInputStream;->pageCache:Ljava/util/Map;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v3, v4, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    iput-wide v0, p0, Lorg/apache/pdfbox/io/RandomAccessBufferedFileInputStream;->curPageOffset:J

    iput-object v2, p0, Lorg/apache/pdfbox/io/RandomAccessBufferedFileInputStream;->curPage:[B

    :cond_1
    iget-wide v0, p0, Lorg/apache/pdfbox/io/RandomAccessBufferedFileInputStream;->curPageOffset:J

    sub-long v0, p1, v0

    long-to-int v0, v0

    iput v0, p0, Lorg/apache/pdfbox/io/RandomAccessBufferedFileInputStream;->offsetWithinPage:I

    iput-wide p1, p0, Lorg/apache/pdfbox/io/RandomAccessBufferedFileInputStream;->fileOffset:J

    return-void
.end method

.method public skip(J)J
    .locals 8

    iget-wide v0, p0, Lorg/apache/pdfbox/io/RandomAccessBufferedFileInputStream;->fileLength:J

    iget-wide v2, p0, Lorg/apache/pdfbox/io/RandomAccessBufferedFileInputStream;->fileOffset:J

    sub-long v4, v0, v2

    cmp-long v4, v4, p1

    if-gez v4, :cond_0

    sub-long p1, v0, v2

    :cond_0
    iget v0, p0, Lorg/apache/pdfbox/io/RandomAccessBufferedFileInputStream;->pageSize:I

    int-to-long v4, v0

    cmp-long v1, p1, v4

    if-gez v1, :cond_1

    iget v1, p0, Lorg/apache/pdfbox/io/RandomAccessBufferedFileInputStream;->offsetWithinPage:I

    int-to-long v4, v1

    add-long/2addr v4, p1

    int-to-long v6, v0

    cmp-long v0, v4, v6

    if-gtz v0, :cond_1

    int-to-long v0, v1

    add-long/2addr v0, p1

    long-to-int v0, v0

    iput v0, p0, Lorg/apache/pdfbox/io/RandomAccessBufferedFileInputStream;->offsetWithinPage:I

    add-long/2addr v2, p1

    iput-wide v2, p0, Lorg/apache/pdfbox/io/RandomAccessBufferedFileInputStream;->fileOffset:J

    goto :goto_0

    :cond_1
    add-long/2addr v2, p1

    invoke-virtual {p0, v2, v3}, Lorg/apache/pdfbox/io/RandomAccessBufferedFileInputStream;->seek(J)V

    :goto_0
    return-wide p1
.end method

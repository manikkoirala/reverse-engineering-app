.class public Lorg/apache/pdfbox/io/RandomAccessFileOutputStream;
.super Ljava/io/OutputStream;
.source "SourceFile"


# instance fields
.field private expectedLength:Lorg/apache/pdfbox/cos/COSBase;

.field private final file:Lorg/apache/pdfbox/io/RandomAccess;

.field private lengthWritten:J

.field private final position:J


# direct methods
.method public constructor <init>(Lorg/apache/pdfbox/io/RandomAccess;)V
    .locals 2

    invoke-direct {p0}, Ljava/io/OutputStream;-><init>()V

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lorg/apache/pdfbox/io/RandomAccessFileOutputStream;->lengthWritten:J

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/pdfbox/io/RandomAccessFileOutputStream;->expectedLength:Lorg/apache/pdfbox/cos/COSBase;

    iput-object p1, p0, Lorg/apache/pdfbox/io/RandomAccessFileOutputStream;->file:Lorg/apache/pdfbox/io/RandomAccess;

    invoke-interface {p1}, Lorg/apache/pdfbox/io/RandomAccessRead;->length()J

    move-result-wide v0

    iput-wide v0, p0, Lorg/apache/pdfbox/io/RandomAccessFileOutputStream;->position:J

    return-void
.end method


# virtual methods
.method public getExpectedLength()Lorg/apache/pdfbox/cos/COSBase;
    .locals 1

    iget-object v0, p0, Lorg/apache/pdfbox/io/RandomAccessFileOutputStream;->expectedLength:Lorg/apache/pdfbox/cos/COSBase;

    return-object v0
.end method

.method public getLength()J
    .locals 4

    iget-object v0, p0, Lorg/apache/pdfbox/io/RandomAccessFileOutputStream;->expectedLength:Lorg/apache/pdfbox/cos/COSBase;

    instance-of v1, v0, Lorg/apache/pdfbox/cos/COSNumber;

    const-wide/16 v2, -0x1

    if-eqz v1, :cond_0

    :goto_0
    check-cast v0, Lorg/apache/pdfbox/cos/COSNumber;

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSNumber;->intValue()I

    move-result v0

    int-to-long v0, v0

    goto :goto_1

    :cond_0
    instance-of v1, v0, Lorg/apache/pdfbox/cos/COSObject;

    if-eqz v1, :cond_1

    check-cast v0, Lorg/apache/pdfbox/cos/COSObject;

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSObject;->getObject()Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    instance-of v0, v0, Lorg/apache/pdfbox/cos/COSNumber;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lorg/apache/pdfbox/io/RandomAccessFileOutputStream;->expectedLength:Lorg/apache/pdfbox/cos/COSBase;

    check-cast v0, Lorg/apache/pdfbox/cos/COSObject;

    invoke-virtual {v0}, Lorg/apache/pdfbox/cos/COSObject;->getObject()Lorg/apache/pdfbox/cos/COSBase;

    move-result-object v0

    goto :goto_0

    :cond_1
    move-wide v0, v2

    :goto_1
    cmp-long v2, v0, v2

    if-nez v2, :cond_2

    iget-wide v0, p0, Lorg/apache/pdfbox/io/RandomAccessFileOutputStream;->lengthWritten:J

    :cond_2
    return-wide v0
.end method

.method public getLengthWritten()J
    .locals 2

    iget-wide v0, p0, Lorg/apache/pdfbox/io/RandomAccessFileOutputStream;->lengthWritten:J

    return-wide v0
.end method

.method public getPosition()J
    .locals 2

    iget-wide v0, p0, Lorg/apache/pdfbox/io/RandomAccessFileOutputStream;->position:J

    return-wide v0
.end method

.method public setExpectedLength(Lorg/apache/pdfbox/cos/COSBase;)V
    .locals 0

    iput-object p1, p0, Lorg/apache/pdfbox/io/RandomAccessFileOutputStream;->expectedLength:Lorg/apache/pdfbox/cos/COSBase;

    return-void
.end method

.method public write(I)V
    .locals 5

    .line 1
    iget-object v0, p0, Lorg/apache/pdfbox/io/RandomAccessFileOutputStream;->file:Lorg/apache/pdfbox/io/RandomAccess;

    iget-wide v1, p0, Lorg/apache/pdfbox/io/RandomAccessFileOutputStream;->position:J

    iget-wide v3, p0, Lorg/apache/pdfbox/io/RandomAccessFileOutputStream;->lengthWritten:J

    add-long/2addr v1, v3

    invoke-interface {v0, v1, v2}, Lorg/apache/pdfbox/io/RandomAccessRead;->seek(J)V

    iget-wide v0, p0, Lorg/apache/pdfbox/io/RandomAccessFileOutputStream;->lengthWritten:J

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    iput-wide v0, p0, Lorg/apache/pdfbox/io/RandomAccessFileOutputStream;->lengthWritten:J

    iget-object v0, p0, Lorg/apache/pdfbox/io/RandomAccessFileOutputStream;->file:Lorg/apache/pdfbox/io/RandomAccess;

    invoke-interface {v0, p1}, Lorg/apache/pdfbox/io/RandomAccess;->write(I)V

    return-void
.end method

.method public write([BII)V
    .locals 5

    .line 2
    iget-object v0, p0, Lorg/apache/pdfbox/io/RandomAccessFileOutputStream;->file:Lorg/apache/pdfbox/io/RandomAccess;

    iget-wide v1, p0, Lorg/apache/pdfbox/io/RandomAccessFileOutputStream;->position:J

    iget-wide v3, p0, Lorg/apache/pdfbox/io/RandomAccessFileOutputStream;->lengthWritten:J

    add-long/2addr v1, v3

    invoke-interface {v0, v1, v2}, Lorg/apache/pdfbox/io/RandomAccessRead;->seek(J)V

    iget-wide v0, p0, Lorg/apache/pdfbox/io/RandomAccessFileOutputStream;->lengthWritten:J

    int-to-long v2, p3

    add-long/2addr v0, v2

    iput-wide v0, p0, Lorg/apache/pdfbox/io/RandomAccessFileOutputStream;->lengthWritten:J

    iget-object v0, p0, Lorg/apache/pdfbox/io/RandomAccessFileOutputStream;->file:Lorg/apache/pdfbox/io/RandomAccess;

    invoke-interface {v0, p1, p2, p3}, Lorg/apache/pdfbox/io/RandomAccess;->write([BII)V

    return-void
.end method

.class public Lorg/apache/pdfbox/io/PushBackInputStream;
.super Ljava/io/PushbackInputStream;
.source "SourceFile"


# instance fields
.field private offset:J

.field private final raInput:Lorg/apache/pdfbox/io/RandomAccessRead;


# direct methods
.method public constructor <init>(Ljava/io/InputStream;I)V
    .locals 2

    invoke-direct {p0, p1, p2}, Ljava/io/PushbackInputStream;-><init>(Ljava/io/InputStream;I)V

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lorg/apache/pdfbox/io/PushBackInputStream;->offset:J

    if-eqz p1, :cond_1

    instance-of p2, p1, Lorg/apache/pdfbox/io/RandomAccessRead;

    if-eqz p2, :cond_0

    check-cast p1, Lorg/apache/pdfbox/io/RandomAccessRead;

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    iput-object p1, p0, Lorg/apache/pdfbox/io/PushBackInputStream;->raInput:Lorg/apache/pdfbox/io/RandomAccessRead;

    return-void

    :cond_1
    new-instance p1, Ljava/io/IOException;

    const-string p2, "Error: input was null"

    invoke-direct {p1, p2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public fillBuffer()V
    .locals 6

    iget-object v0, p0, Ljava/io/PushbackInputStream;->buf:[B

    array-length v0, v0

    new-array v1, v0, [B

    const/4 v2, 0x0

    move v3, v2

    move v4, v3

    :cond_0
    :goto_0
    const/4 v5, -0x1

    if-eq v3, v5, :cond_1

    if-ge v4, v0, :cond_1

    sub-int v3, v0, v4

    invoke-virtual {p0, v1, v4, v3}, Lorg/apache/pdfbox/io/PushBackInputStream;->read([BII)I

    move-result v3

    if-eq v3, v5, :cond_0

    add-int/2addr v4, v3

    goto :goto_0

    :cond_1
    invoke-virtual {p0, v1, v2, v4}, Lorg/apache/pdfbox/io/PushBackInputStream;->unread([BII)V

    return-void
.end method

.method public getOffset()J
    .locals 2

    iget-wide v0, p0, Lorg/apache/pdfbox/io/PushBackInputStream;->offset:J

    return-wide v0
.end method

.method public isEOF()Z
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/io/PushBackInputStream;->peek()I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public peek()I
    .locals 2

    invoke-virtual {p0}, Lorg/apache/pdfbox/io/PushBackInputStream;->read()I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    invoke-virtual {p0, v0}, Lorg/apache/pdfbox/io/PushBackInputStream;->unread(I)V

    :cond_0
    return v0
.end method

.method public read()I
    .locals 5

    .line 1
    invoke-super {p0}, Ljava/io/PushbackInputStream;->read()I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget-wide v1, p0, Lorg/apache/pdfbox/io/PushBackInputStream;->offset:J

    const-wide/16 v3, 0x1

    add-long/2addr v1, v3

    iput-wide v1, p0, Lorg/apache/pdfbox/io/PushBackInputStream;->offset:J

    :cond_0
    return v0
.end method

.method public read([B)I
    .locals 2

    .line 2
    array-length v0, p1

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v1, v0}, Lorg/apache/pdfbox/io/PushBackInputStream;->read([BII)I

    move-result p1

    return p1
.end method

.method public read([BII)I
    .locals 2

    .line 3
    invoke-super {p0, p1, p2, p3}, Ljava/io/PushbackInputStream;->read([BII)I

    move-result p1

    const/4 p2, -0x1

    if-eq p1, p2, :cond_0

    iget-wide p2, p0, Lorg/apache/pdfbox/io/PushBackInputStream;->offset:J

    int-to-long v0, p1

    add-long/2addr p2, v0

    iput-wide p2, p0, Lorg/apache/pdfbox/io/PushBackInputStream;->offset:J

    :cond_0
    return p1
.end method

.method public readFully(I)[B
    .locals 3

    new-array v0, p1, [B

    const/4 v1, 0x0

    :goto_0
    if-ge v1, p1, :cond_1

    sub-int v2, p1, v1

    invoke-virtual {p0, v0, v1, v2}, Lorg/apache/pdfbox/io/PushBackInputStream;->read([BII)I

    move-result v2

    if-ltz v2, :cond_0

    add-int/2addr v1, v2

    goto :goto_0

    :cond_0
    new-instance p1, Ljava/io/EOFException;

    const-string v0, "Premature end of file"

    invoke-direct {p1, v0}, Ljava/io/EOFException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    return-object v0
.end method

.method public seek(J)V
    .locals 2

    iget-object v0, p0, Lorg/apache/pdfbox/io/PushBackInputStream;->raInput:Lorg/apache/pdfbox/io/RandomAccessRead;

    if-eqz v0, :cond_1

    iget-object v0, p0, Ljava/io/PushbackInputStream;->buf:[B

    array-length v0, v0

    iget v1, p0, Ljava/io/PushbackInputStream;->pos:I

    sub-int/2addr v0, v1

    if-lez v0, :cond_0

    int-to-long v0, v0

    invoke-virtual {p0, v0, v1}, Ljava/io/InputStream;->skip(J)J

    :cond_0
    iget-object v0, p0, Lorg/apache/pdfbox/io/PushBackInputStream;->raInput:Lorg/apache/pdfbox/io/RandomAccessRead;

    invoke-interface {v0, p1, p2}, Lorg/apache/pdfbox/io/RandomAccessRead;->seek(J)V

    iput-wide p1, p0, Lorg/apache/pdfbox/io/PushBackInputStream;->offset:J

    return-void

    :cond_1
    new-instance p1, Ljava/io/IOException;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Provided stream of type "

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Ljava/io/PushbackInputStream;->in:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, " is not seekable."

    invoke-virtual {p2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public unread(I)V
    .locals 4

    .line 1
    iget-wide v0, p0, Lorg/apache/pdfbox/io/PushBackInputStream;->offset:J

    const-wide/16 v2, 0x1

    sub-long/2addr v0, v2

    iput-wide v0, p0, Lorg/apache/pdfbox/io/PushBackInputStream;->offset:J

    invoke-super {p0, p1}, Ljava/io/PushbackInputStream;->unread(I)V

    return-void
.end method

.method public unread([B)V
    .locals 2

    .line 2
    array-length v0, p1

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v1, v0}, Lorg/apache/pdfbox/io/PushBackInputStream;->unread([BII)V

    return-void
.end method

.method public unread([BII)V
    .locals 4

    .line 3
    if-lez p3, :cond_0

    iget-wide v0, p0, Lorg/apache/pdfbox/io/PushBackInputStream;->offset:J

    int-to-long v2, p3

    sub-long/2addr v0, v2

    iput-wide v0, p0, Lorg/apache/pdfbox/io/PushBackInputStream;->offset:J

    invoke-super {p0, p1, p2, p3}, Ljava/io/PushbackInputStream;->unread([BII)V

    :cond_0
    return-void
.end method

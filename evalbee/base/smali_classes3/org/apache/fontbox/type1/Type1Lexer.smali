.class Lorg/apache/fontbox/type1/Type1Lexer;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private aheadToken:Lorg/apache/fontbox/type1/Token;

.field private final buffer:Ljava/nio/ByteBuffer;

.field private openParens:I


# direct methods
.method public constructor <init>([B)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/fontbox/type1/Type1Lexer;->openParens:I

    invoke-static {p1}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object p1

    iput-object p1, p0, Lorg/apache/fontbox/type1/Type1Lexer;->buffer:Ljava/nio/ByteBuffer;

    const/4 p1, 0x0

    invoke-direct {p0, p1}, Lorg/apache/fontbox/type1/Type1Lexer;->readToken(Lorg/apache/fontbox/type1/Token;)Lorg/apache/fontbox/type1/Token;

    move-result-object p1

    iput-object p1, p0, Lorg/apache/fontbox/type1/Type1Lexer;->aheadToken:Lorg/apache/fontbox/type1/Token;

    return-void
.end method

.method private getChar()C
    .locals 1

    iget-object v0, p0, Lorg/apache/fontbox/type1/Type1Lexer;->buffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->get()B

    move-result v0

    int-to-char v0, v0

    return v0
.end method

.method private readCharString(I)Lorg/apache/fontbox/type1/Token;
    .locals 2

    iget-object v0, p0, Lorg/apache/fontbox/type1/Type1Lexer;->buffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->get()B

    new-array p1, p1, [B

    iget-object v0, p0, Lorg/apache/fontbox/type1/Type1Lexer;->buffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, p1}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;

    new-instance v0, Lorg/apache/fontbox/type1/Token;

    sget-object v1, Lorg/apache/fontbox/type1/Token;->CHARSTRING:Lorg/apache/fontbox/type1/Token$Kind;

    invoke-direct {v0, p1, v1}, Lorg/apache/fontbox/type1/Token;-><init>([BLorg/apache/fontbox/type1/Token$Kind;)V

    return-object v0
.end method

.method private readComment()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    :goto_0
    iget-object v1, p0, Lorg/apache/fontbox/type1/Type1Lexer;->buffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/Buffer;->hasRemaining()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-direct {p0}, Lorg/apache/fontbox/type1/Type1Lexer;->getChar()C

    move-result v1

    const/16 v2, 0xd

    if-eq v1, v2, :cond_1

    const/16 v2, 0xa

    if-ne v1, v2, :cond_0

    goto :goto_1

    :cond_0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_1
    :goto_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private readRegular()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    :goto_0
    iget-object v1, p0, Lorg/apache/fontbox/type1/Type1Lexer;->buffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/Buffer;->hasRemaining()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lorg/apache/fontbox/type1/Type1Lexer;->buffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->mark()Ljava/nio/Buffer;

    invoke-direct {p0}, Lorg/apache/fontbox/type1/Type1Lexer;->getChar()C

    move-result v1

    invoke-static {v1}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v2

    if-nez v2, :cond_1

    const/16 v2, 0x28

    if-eq v1, v2, :cond_1

    const/16 v2, 0x29

    if-eq v1, v2, :cond_1

    const/16 v2, 0x3c

    if-eq v1, v2, :cond_1

    const/16 v2, 0x3e

    if-eq v1, v2, :cond_1

    const/16 v2, 0x5b

    if-eq v1, v2, :cond_1

    const/16 v2, 0x5d

    if-eq v1, v2, :cond_1

    const/16 v2, 0x7b

    if-eq v1, v2, :cond_1

    const/16 v2, 0x7d

    if-eq v1, v2, :cond_1

    const/16 v2, 0x2f

    if-eq v1, v2, :cond_1

    const/16 v2, 0x25

    if-ne v1, v2, :cond_0

    goto :goto_1

    :cond_0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_1
    :goto_1
    iget-object v1, p0, Lorg/apache/fontbox/type1/Type1Lexer;->buffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->reset()Ljava/nio/Buffer;

    :cond_2
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_3

    const/4 v0, 0x0

    :cond_3
    return-object v0
.end method

.method private readString()Lorg/apache/fontbox/type1/Token;
    .locals 8

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    :cond_0
    :goto_0
    iget-object v1, p0, Lorg/apache/fontbox/type1/Type1Lexer;->buffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/Buffer;->hasRemaining()Z

    move-result v1

    if-eqz v1, :cond_e

    invoke-direct {p0}, Lorg/apache/fontbox/type1/Type1Lexer;->getChar()C

    move-result v1

    const/4 v2, 0x1

    const/16 v3, 0x28

    if-ne v1, v3, :cond_1

    iget v1, p0, Lorg/apache/fontbox/type1/Type1Lexer;->openParens:I

    add-int/2addr v1, v2

    iput v1, p0, Lorg/apache/fontbox/type1/Type1Lexer;->openParens:I

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_1
    const/16 v4, 0x29

    if-ne v1, v4, :cond_3

    iget v1, p0, Lorg/apache/fontbox/type1/Type1Lexer;->openParens:I

    if-nez v1, :cond_2

    new-instance v1, Lorg/apache/fontbox/type1/Token;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v2, Lorg/apache/fontbox/type1/Token;->STRING:Lorg/apache/fontbox/type1/Token$Kind;

    invoke-direct {v1, v0, v2}, Lorg/apache/fontbox/type1/Token;-><init>(Ljava/lang/String;Lorg/apache/fontbox/type1/Token$Kind;)V

    return-object v1

    :cond_2
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    iget v1, p0, Lorg/apache/fontbox/type1/Type1Lexer;->openParens:I

    sub-int/2addr v1, v2

    iput v1, p0, Lorg/apache/fontbox/type1/Type1Lexer;->openParens:I

    goto :goto_0

    :cond_3
    const-string v5, "\n"

    const/16 v6, 0x5c

    if-ne v1, v6, :cond_c

    invoke-direct {p0}, Lorg/apache/fontbox/type1/Type1Lexer;->getChar()C

    move-result v1

    const/16 v7, 0x8

    if-eq v1, v3, :cond_a

    if-eq v1, v4, :cond_9

    if-eq v1, v6, :cond_8

    const/16 v3, 0x62

    if-eq v1, v3, :cond_7

    const/16 v3, 0x66

    if-eq v1, v3, :cond_6

    const/16 v3, 0x6e

    if-eq v1, v3, :cond_5

    const/16 v3, 0x72

    if-eq v1, v3, :cond_5

    const/16 v3, 0x74

    if-eq v1, v3, :cond_4

    goto :goto_2

    :cond_4
    const/16 v3, 0x9

    goto :goto_1

    :cond_5
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    :cond_6
    const/16 v3, 0xc

    goto :goto_1

    :cond_7
    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_2

    :cond_8
    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_2

    :cond_9
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_2

    :cond_a
    :goto_1
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :goto_2
    invoke-static {v1}, Ljava/lang/Character;->isDigit(C)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v3, 0x3

    new-array v3, v3, [C

    const/4 v4, 0x0

    aput-char v1, v3, v4

    invoke-direct {p0}, Lorg/apache/fontbox/type1/Type1Lexer;->getChar()C

    move-result v1

    aput-char v1, v3, v2

    const/4 v1, 0x2

    invoke-direct {p0}, Lorg/apache/fontbox/type1/Type1Lexer;->getChar()C

    move-result v2

    aput-char v2, v3, v1

    invoke-static {v3}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    int-to-char v1, v1

    :cond_b
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    :cond_c
    const/16 v2, 0xd

    if-eq v1, v2, :cond_d

    const/16 v2, 0xa

    if-ne v1, v2, :cond_b

    :cond_d
    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    :cond_e
    const/4 v0, 0x0

    return-object v0
.end method

.method private readToken(Lorg/apache/fontbox/type1/Token;)Lorg/apache/fontbox/type1/Token;
    .locals 3

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lorg/apache/fontbox/type1/Type1Lexer;->buffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/Buffer;->hasRemaining()Z

    move-result v1

    if-eqz v1, :cond_10

    invoke-direct {p0}, Lorg/apache/fontbox/type1/Type1Lexer;->getChar()C

    move-result v1

    const/16 v2, 0x25

    if-ne v1, v2, :cond_1

    invoke-direct {p0}, Lorg/apache/fontbox/type1/Type1Lexer;->readComment()Ljava/lang/String;

    goto :goto_0

    :cond_1
    const/16 v0, 0x28

    if-ne v1, v0, :cond_2

    invoke-direct {p0}, Lorg/apache/fontbox/type1/Type1Lexer;->readString()Lorg/apache/fontbox/type1/Token;

    move-result-object p1

    return-object p1

    :cond_2
    const/16 v0, 0x29

    if-eq v1, v0, :cond_f

    const/16 v0, 0x5b

    if-ne v1, v0, :cond_3

    new-instance p1, Lorg/apache/fontbox/type1/Token;

    sget-object v0, Lorg/apache/fontbox/type1/Token;->START_ARRAY:Lorg/apache/fontbox/type1/Token$Kind;

    invoke-direct {p1, v1, v0}, Lorg/apache/fontbox/type1/Token;-><init>(CLorg/apache/fontbox/type1/Token$Kind;)V

    return-object p1

    :cond_3
    const/16 v0, 0x7b

    if-ne v1, v0, :cond_4

    new-instance p1, Lorg/apache/fontbox/type1/Token;

    sget-object v0, Lorg/apache/fontbox/type1/Token;->START_PROC:Lorg/apache/fontbox/type1/Token$Kind;

    invoke-direct {p1, v1, v0}, Lorg/apache/fontbox/type1/Token;-><init>(CLorg/apache/fontbox/type1/Token$Kind;)V

    return-object p1

    :cond_4
    const/16 v0, 0x5d

    if-ne v1, v0, :cond_5

    new-instance p1, Lorg/apache/fontbox/type1/Token;

    sget-object v0, Lorg/apache/fontbox/type1/Token;->END_ARRAY:Lorg/apache/fontbox/type1/Token$Kind;

    invoke-direct {p1, v1, v0}, Lorg/apache/fontbox/type1/Token;-><init>(CLorg/apache/fontbox/type1/Token$Kind;)V

    return-object p1

    :cond_5
    const/16 v0, 0x7d

    if-ne v1, v0, :cond_6

    new-instance p1, Lorg/apache/fontbox/type1/Token;

    sget-object v0, Lorg/apache/fontbox/type1/Token;->END_PROC:Lorg/apache/fontbox/type1/Token$Kind;

    invoke-direct {p1, v1, v0}, Lorg/apache/fontbox/type1/Token;-><init>(CLorg/apache/fontbox/type1/Token$Kind;)V

    return-object p1

    :cond_6
    const/16 v0, 0x2f

    if-ne v1, v0, :cond_7

    new-instance p1, Lorg/apache/fontbox/type1/Token;

    invoke-direct {p0}, Lorg/apache/fontbox/type1/Type1Lexer;->readRegular()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lorg/apache/fontbox/type1/Token;->LITERAL:Lorg/apache/fontbox/type1/Token$Kind;

    invoke-direct {p1, v0, v1}, Lorg/apache/fontbox/type1/Token;-><init>(Ljava/lang/String;Lorg/apache/fontbox/type1/Token$Kind;)V

    return-object p1

    :cond_7
    invoke-static {v1}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v0

    const/4 v2, 0x1

    if-eqz v0, :cond_8

    :goto_1
    move v0, v2

    goto :goto_0

    :cond_8
    if-nez v1, :cond_9

    const-string v0, "PdfBoxAndroid"

    const-string v1, "NULL byte in font, skipped"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_9
    iget-object v0, p0, Lorg/apache/fontbox/type1/Type1Lexer;->buffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/Buffer;->position()I

    move-result v1

    sub-int/2addr v1, v2

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    invoke-direct {p0}, Lorg/apache/fontbox/type1/Type1Lexer;->tryReadNumber()Lorg/apache/fontbox/type1/Token;

    move-result-object v0

    if-eqz v0, :cond_a

    return-object v0

    :cond_a
    invoke-direct {p0}, Lorg/apache/fontbox/type1/Type1Lexer;->readRegular()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_e

    const-string v1, "RD"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_c

    const-string v1, "-|"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    goto :goto_2

    :cond_b
    new-instance p1, Lorg/apache/fontbox/type1/Token;

    sget-object v1, Lorg/apache/fontbox/type1/Token;->NAME:Lorg/apache/fontbox/type1/Token$Kind;

    invoke-direct {p1, v0, v1}, Lorg/apache/fontbox/type1/Token;-><init>(Ljava/lang/String;Lorg/apache/fontbox/type1/Token$Kind;)V

    return-object p1

    :cond_c
    :goto_2
    invoke-virtual {p1}, Lorg/apache/fontbox/type1/Token;->getKind()Lorg/apache/fontbox/type1/Token$Kind;

    move-result-object v0

    sget-object v1, Lorg/apache/fontbox/type1/Token;->INTEGER:Lorg/apache/fontbox/type1/Token$Kind;

    if-ne v0, v1, :cond_d

    invoke-virtual {p1}, Lorg/apache/fontbox/type1/Token;->intValue()I

    move-result p1

    invoke-direct {p0, p1}, Lorg/apache/fontbox/type1/Type1Lexer;->readCharString(I)Lorg/apache/fontbox/type1/Token;

    move-result-object p1

    return-object p1

    :cond_d
    new-instance p1, Ljava/io/IOException;

    const-string v0, "expected INTEGER before -| or RD"

    invoke-direct {p1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_e
    new-instance p1, Lorg/apache/fontbox/type1/DamagedFontException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Could not read token at position "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lorg/apache/fontbox/type1/Type1Lexer;->buffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/Buffer;->position()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Lorg/apache/fontbox/type1/DamagedFontException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_f
    new-instance p1, Ljava/io/IOException;

    const-string v0, "unexpected closing parenthesis"

    invoke-direct {p1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_10
    if-nez v0, :cond_0

    const/4 p1, 0x0

    return-object p1
.end method

.method private tryReadNumber()Lorg/apache/fontbox/type1/Token;
    .locals 8

    iget-object v0, p0, Lorg/apache/fontbox/type1/Type1Lexer;->buffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->mark()Ljava/nio/Buffer;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-direct {p0}, Lorg/apache/fontbox/type1/Type1Lexer;->getChar()C

    move-result v1

    const/16 v2, 0x2b

    const/16 v3, 0x2d

    const/4 v4, 0x0

    if-eq v1, v2, :cond_0

    if-ne v1, v3, :cond_1

    :cond_0
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-direct {p0}, Lorg/apache/fontbox/type1/Type1Lexer;->getChar()C

    move-result v1

    :cond_1
    :goto_0
    invoke-static {v1}, Ljava/lang/Character;->isDigit(C)Z

    move-result v2

    const/4 v5, 0x1

    if-eqz v2, :cond_2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-direct {p0}, Lorg/apache/fontbox/type1/Type1Lexer;->getChar()C

    move-result v1

    move v4, v5

    goto :goto_0

    :cond_2
    const/16 v2, 0x2e

    const/4 v6, 0x0

    if-ne v1, v2, :cond_3

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-direct {p0}, Lorg/apache/fontbox/type1/Type1Lexer;->getChar()C

    move-result v1

    move v2, v1

    move-object v1, v6

    goto :goto_1

    :cond_3
    const/16 v2, 0x23

    if-ne v1, v2, :cond_9

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-direct {p0}, Lorg/apache/fontbox/type1/Type1Lexer;->getChar()C

    move-result v2

    move-object v7, v1

    move-object v1, v0

    move-object v0, v7

    :goto_1
    invoke-static {v2}, Ljava/lang/Character;->isDigit(C)Z

    move-result v4

    if-eqz v4, :cond_6

    :goto_2
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-direct {p0}, Lorg/apache/fontbox/type1/Type1Lexer;->getChar()C

    move-result v2

    invoke-static {v2}, Ljava/lang/Character;->isDigit(C)Z

    move-result v4

    if-eqz v4, :cond_4

    goto :goto_2

    :cond_4
    const/16 v4, 0x45

    if-ne v2, v4, :cond_7

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-direct {p0}, Lorg/apache/fontbox/type1/Type1Lexer;->getChar()C

    move-result v2

    if-ne v2, v3, :cond_5

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-direct {p0}, Lorg/apache/fontbox/type1/Type1Lexer;->getChar()C

    move-result v2

    :cond_5
    invoke-static {v2}, Ljava/lang/Character;->isDigit(C)Z

    move-result v3

    if-eqz v3, :cond_6

    :goto_3
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-direct {p0}, Lorg/apache/fontbox/type1/Type1Lexer;->getChar()C

    move-result v2

    invoke-static {v2}, Ljava/lang/Character;->isDigit(C)Z

    move-result v3

    if-eqz v3, :cond_7

    goto :goto_3

    :cond_6
    :goto_4
    iget-object v0, p0, Lorg/apache/fontbox/type1/Type1Lexer;->buffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->reset()Ljava/nio/Buffer;

    return-object v6

    :cond_7
    iget-object v2, p0, Lorg/apache/fontbox/type1/Type1Lexer;->buffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v2}, Ljava/nio/Buffer;->position()I

    move-result v3

    sub-int/2addr v3, v5

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    if-eqz v1, :cond_8

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    new-instance v1, Lorg/apache/fontbox/type1/Token;

    invoke-virtual {v0}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v2, Lorg/apache/fontbox/type1/Token;->INTEGER:Lorg/apache/fontbox/type1/Token$Kind;

    invoke-direct {v1, v0, v2}, Lorg/apache/fontbox/type1/Token;-><init>(Ljava/lang/String;Lorg/apache/fontbox/type1/Token$Kind;)V

    return-object v1

    :cond_8
    new-instance v1, Lorg/apache/fontbox/type1/Token;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v2, Lorg/apache/fontbox/type1/Token;->REAL:Lorg/apache/fontbox/type1/Token$Kind;

    invoke-direct {v1, v0, v2}, Lorg/apache/fontbox/type1/Token;-><init>(Ljava/lang/String;Lorg/apache/fontbox/type1/Token$Kind;)V

    return-object v1

    :cond_9
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-eqz v1, :cond_6

    if-nez v4, :cond_a

    goto :goto_4

    :cond_a
    iget-object v1, p0, Lorg/apache/fontbox/type1/Type1Lexer;->buffer:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/Buffer;->position()I

    move-result v2

    sub-int/2addr v2, v5

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    new-instance v1, Lorg/apache/fontbox/type1/Token;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v2, Lorg/apache/fontbox/type1/Token;->INTEGER:Lorg/apache/fontbox/type1/Token$Kind;

    invoke-direct {v1, v0, v2}, Lorg/apache/fontbox/type1/Token;-><init>(Ljava/lang/String;Lorg/apache/fontbox/type1/Token$Kind;)V

    return-object v1
.end method


# virtual methods
.method public nextToken()Lorg/apache/fontbox/type1/Token;
    .locals 2

    iget-object v0, p0, Lorg/apache/fontbox/type1/Type1Lexer;->aheadToken:Lorg/apache/fontbox/type1/Token;

    invoke-direct {p0, v0}, Lorg/apache/fontbox/type1/Type1Lexer;->readToken(Lorg/apache/fontbox/type1/Token;)Lorg/apache/fontbox/type1/Token;

    move-result-object v1

    iput-object v1, p0, Lorg/apache/fontbox/type1/Type1Lexer;->aheadToken:Lorg/apache/fontbox/type1/Token;

    return-object v0
.end method

.method public peekToken()Lorg/apache/fontbox/type1/Token;
    .locals 1

    iget-object v0, p0, Lorg/apache/fontbox/type1/Type1Lexer;->aheadToken:Lorg/apache/fontbox/type1/Token;

    return-object v0
.end method

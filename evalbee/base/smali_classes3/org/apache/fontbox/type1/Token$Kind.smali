.class final enum Lorg/apache/fontbox/type1/Token$Kind;
.super Ljava/lang/Enum;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/fontbox/type1/Token;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Kind"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lorg/apache/fontbox/type1/Token$Kind;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lorg/apache/fontbox/type1/Token$Kind;

.field public static final enum CHARSTRING:Lorg/apache/fontbox/type1/Token$Kind;

.field public static final enum END_ARRAY:Lorg/apache/fontbox/type1/Token$Kind;

.field public static final enum END_PROC:Lorg/apache/fontbox/type1/Token$Kind;

.field public static final enum INTEGER:Lorg/apache/fontbox/type1/Token$Kind;

.field public static final enum LITERAL:Lorg/apache/fontbox/type1/Token$Kind;

.field public static final enum NAME:Lorg/apache/fontbox/type1/Token$Kind;

.field public static final enum NONE:Lorg/apache/fontbox/type1/Token$Kind;

.field public static final enum REAL:Lorg/apache/fontbox/type1/Token$Kind;

.field public static final enum START_ARRAY:Lorg/apache/fontbox/type1/Token$Kind;

.field public static final enum START_PROC:Lorg/apache/fontbox/type1/Token$Kind;

.field public static final enum STRING:Lorg/apache/fontbox/type1/Token$Kind;


# direct methods
.method public static constructor <clinit>()V
    .locals 13

    new-instance v0, Lorg/apache/fontbox/type1/Token$Kind;

    const-string v1, "NONE"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lorg/apache/fontbox/type1/Token$Kind;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lorg/apache/fontbox/type1/Token$Kind;->NONE:Lorg/apache/fontbox/type1/Token$Kind;

    new-instance v1, Lorg/apache/fontbox/type1/Token$Kind;

    const-string v2, "STRING"

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Lorg/apache/fontbox/type1/Token$Kind;-><init>(Ljava/lang/String;I)V

    sput-object v1, Lorg/apache/fontbox/type1/Token$Kind;->STRING:Lorg/apache/fontbox/type1/Token$Kind;

    new-instance v2, Lorg/apache/fontbox/type1/Token$Kind;

    const-string v3, "NAME"

    const/4 v4, 0x2

    invoke-direct {v2, v3, v4}, Lorg/apache/fontbox/type1/Token$Kind;-><init>(Ljava/lang/String;I)V

    sput-object v2, Lorg/apache/fontbox/type1/Token$Kind;->NAME:Lorg/apache/fontbox/type1/Token$Kind;

    new-instance v3, Lorg/apache/fontbox/type1/Token$Kind;

    const-string v4, "LITERAL"

    const/4 v5, 0x3

    invoke-direct {v3, v4, v5}, Lorg/apache/fontbox/type1/Token$Kind;-><init>(Ljava/lang/String;I)V

    sput-object v3, Lorg/apache/fontbox/type1/Token$Kind;->LITERAL:Lorg/apache/fontbox/type1/Token$Kind;

    new-instance v4, Lorg/apache/fontbox/type1/Token$Kind;

    const-string v5, "REAL"

    const/4 v6, 0x4

    invoke-direct {v4, v5, v6}, Lorg/apache/fontbox/type1/Token$Kind;-><init>(Ljava/lang/String;I)V

    sput-object v4, Lorg/apache/fontbox/type1/Token$Kind;->REAL:Lorg/apache/fontbox/type1/Token$Kind;

    new-instance v5, Lorg/apache/fontbox/type1/Token$Kind;

    const-string v6, "INTEGER"

    const/4 v7, 0x5

    invoke-direct {v5, v6, v7}, Lorg/apache/fontbox/type1/Token$Kind;-><init>(Ljava/lang/String;I)V

    sput-object v5, Lorg/apache/fontbox/type1/Token$Kind;->INTEGER:Lorg/apache/fontbox/type1/Token$Kind;

    new-instance v6, Lorg/apache/fontbox/type1/Token$Kind;

    const-string v7, "START_ARRAY"

    const/4 v8, 0x6

    invoke-direct {v6, v7, v8}, Lorg/apache/fontbox/type1/Token$Kind;-><init>(Ljava/lang/String;I)V

    sput-object v6, Lorg/apache/fontbox/type1/Token$Kind;->START_ARRAY:Lorg/apache/fontbox/type1/Token$Kind;

    new-instance v7, Lorg/apache/fontbox/type1/Token$Kind;

    const-string v8, "END_ARRAY"

    const/4 v9, 0x7

    invoke-direct {v7, v8, v9}, Lorg/apache/fontbox/type1/Token$Kind;-><init>(Ljava/lang/String;I)V

    sput-object v7, Lorg/apache/fontbox/type1/Token$Kind;->END_ARRAY:Lorg/apache/fontbox/type1/Token$Kind;

    new-instance v8, Lorg/apache/fontbox/type1/Token$Kind;

    const-string v9, "START_PROC"

    const/16 v10, 0x8

    invoke-direct {v8, v9, v10}, Lorg/apache/fontbox/type1/Token$Kind;-><init>(Ljava/lang/String;I)V

    sput-object v8, Lorg/apache/fontbox/type1/Token$Kind;->START_PROC:Lorg/apache/fontbox/type1/Token$Kind;

    new-instance v9, Lorg/apache/fontbox/type1/Token$Kind;

    const-string v10, "END_PROC"

    const/16 v11, 0x9

    invoke-direct {v9, v10, v11}, Lorg/apache/fontbox/type1/Token$Kind;-><init>(Ljava/lang/String;I)V

    sput-object v9, Lorg/apache/fontbox/type1/Token$Kind;->END_PROC:Lorg/apache/fontbox/type1/Token$Kind;

    new-instance v10, Lorg/apache/fontbox/type1/Token$Kind;

    const-string v11, "CHARSTRING"

    const/16 v12, 0xa

    invoke-direct {v10, v11, v12}, Lorg/apache/fontbox/type1/Token$Kind;-><init>(Ljava/lang/String;I)V

    sput-object v10, Lorg/apache/fontbox/type1/Token$Kind;->CHARSTRING:Lorg/apache/fontbox/type1/Token$Kind;

    filled-new-array/range {v0 .. v10}, [Lorg/apache/fontbox/type1/Token$Kind;

    move-result-object v0

    sput-object v0, Lorg/apache/fontbox/type1/Token$Kind;->$VALUES:[Lorg/apache/fontbox/type1/Token$Kind;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lorg/apache/fontbox/type1/Token$Kind;
    .locals 1

    const-class v0, Lorg/apache/fontbox/type1/Token$Kind;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object p0

    check-cast p0, Lorg/apache/fontbox/type1/Token$Kind;

    return-object p0
.end method

.method public static values()[Lorg/apache/fontbox/type1/Token$Kind;
    .locals 1

    sget-object v0, Lorg/apache/fontbox/type1/Token$Kind;->$VALUES:[Lorg/apache/fontbox/type1/Token$Kind;

    invoke-virtual {v0}, [Lorg/apache/fontbox/type1/Token$Kind;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lorg/apache/fontbox/type1/Token$Kind;

    return-object v0
.end method

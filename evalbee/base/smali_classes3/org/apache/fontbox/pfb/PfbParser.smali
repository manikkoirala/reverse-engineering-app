.class public Lorg/apache/fontbox/pfb/PfbParser;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final ASCII_MARKER:I = 0x1

.field private static final BINARY_MARKER:I = 0x2

.field private static final BUFFER_SIZE:I = 0xffff

.field private static final PFB_HEADER_LENGTH:I = 0x12

.field private static final PFB_RECORDS:[I

.field private static final START_MARKER:I = 0x80


# instance fields
.field private lengths:[I

.field private pfbdata:[B


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    const/4 v0, 0x1

    const/4 v1, 0x2

    filled-new-array {v0, v1, v0}, [I

    move-result-object v0

    sput-object v0, Lorg/apache/fontbox/pfb/PfbParser;->PFB_RECORDS:[I

    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-direct {p0, p1}, Lorg/apache/fontbox/pfb/PfbParser;->readPfbInput(Ljava/io/InputStream;)[B

    move-result-object p1

    invoke-direct {p0, p1}, Lorg/apache/fontbox/pfb/PfbParser;->parsePfb([B)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 2

    .line 2
    new-instance v0, Ljava/io/BufferedInputStream;

    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, p1}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    const p1, 0xffff

    invoke-direct {v0, v1, p1}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;I)V

    invoke-direct {p0, v0}, Lorg/apache/fontbox/pfb/PfbParser;-><init>(Ljava/io/InputStream;)V

    return-void
.end method

.method public constructor <init>([B)V
    .locals 0

    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-direct {p0, p1}, Lorg/apache/fontbox/pfb/PfbParser;->parsePfb([B)V

    return-void
.end method

.method private parsePfb([B)V
    .locals 5

    new-instance v0, Ljava/io/ByteArrayInputStream;

    invoke-direct {v0, p1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    array-length p1, p1

    add-int/lit8 p1, p1, -0x12

    new-array p1, p1, [B

    iput-object p1, p0, Lorg/apache/fontbox/pfb/PfbParser;->pfbdata:[B

    sget-object p1, Lorg/apache/fontbox/pfb/PfbParser;->PFB_RECORDS:[I

    array-length p1, p1

    new-array p1, p1, [I

    iput-object p1, p0, Lorg/apache/fontbox/pfb/PfbParser;->lengths:[I

    const/4 p1, 0x0

    move v1, p1

    :goto_0
    sget-object v2, Lorg/apache/fontbox/pfb/PfbParser;->PFB_RECORDS:[I

    array-length v3, v2

    if-ge p1, v3, :cond_3

    invoke-virtual {v0}, Ljava/io/ByteArrayInputStream;->read()I

    move-result v3

    const/16 v4, 0x80

    if-ne v3, v4, :cond_2

    invoke-virtual {v0}, Ljava/io/ByteArrayInputStream;->read()I

    move-result v3

    aget v2, v2, p1

    if-ne v3, v2, :cond_1

    invoke-virtual {v0}, Ljava/io/ByteArrayInputStream;->read()I

    move-result v2

    invoke-virtual {v0}, Ljava/io/ByteArrayInputStream;->read()I

    move-result v3

    shl-int/lit8 v3, v3, 0x8

    add-int/2addr v2, v3

    invoke-virtual {v0}, Ljava/io/ByteArrayInputStream;->read()I

    move-result v3

    shl-int/lit8 v3, v3, 0x10

    add-int/2addr v2, v3

    invoke-virtual {v0}, Ljava/io/ByteArrayInputStream;->read()I

    move-result v3

    shl-int/lit8 v3, v3, 0x18

    add-int/2addr v2, v3

    iget-object v3, p0, Lorg/apache/fontbox/pfb/PfbParser;->lengths:[I

    aput v2, v3, p1

    iget-object v3, p0, Lorg/apache/fontbox/pfb/PfbParser;->pfbdata:[B

    invoke-virtual {v0, v3, v1, v2}, Ljava/io/ByteArrayInputStream;->read([BII)I

    move-result v2

    if-ltz v2, :cond_0

    add-int/2addr v1, v2

    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    :cond_0
    new-instance p1, Ljava/io/EOFException;

    invoke-direct {p1}, Ljava/io/EOFException;-><init>()V

    throw p1

    :cond_1
    new-instance p1, Ljava/io/IOException;

    const-string v0, "Incorrect record type"

    invoke-direct {p1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_2
    new-instance p1, Ljava/io/IOException;

    const-string v0, "Start marker missing"

    invoke-direct {p1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_3
    return-void
.end method

.method private readPfbInput(Ljava/io/InputStream;)[B
    .locals 4

    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    const v1, 0xffff

    new-array v1, v1, [B

    :goto_0
    invoke-virtual {p1, v1}, Ljava/io/InputStream;->read([B)I

    move-result v2

    const/4 v3, -0x1

    if-eq v2, v3, :cond_0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3, v2}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public getInputStream()Ljava/io/InputStream;
    .locals 2

    new-instance v0, Ljava/io/ByteArrayInputStream;

    iget-object v1, p0, Lorg/apache/fontbox/pfb/PfbParser;->pfbdata:[B

    invoke-direct {v0, v1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    return-object v0
.end method

.method public getLengths()[I
    .locals 1

    iget-object v0, p0, Lorg/apache/fontbox/pfb/PfbParser;->lengths:[I

    return-object v0
.end method

.method public getPfbdata()[B
    .locals 1

    iget-object v0, p0, Lorg/apache/fontbox/pfb/PfbParser;->pfbdata:[B

    return-object v0
.end method

.method public getSegment1()[B
    .locals 3

    iget-object v0, p0, Lorg/apache/fontbox/pfb/PfbParser;->pfbdata:[B

    iget-object v1, p0, Lorg/apache/fontbox/pfb/PfbParser;->lengths:[I

    const/4 v2, 0x0

    aget v1, v1, v2

    invoke-static {v0, v2, v1}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object v0

    return-object v0
.end method

.method public getSegment2()[B
    .locals 4

    iget-object v0, p0, Lorg/apache/fontbox/pfb/PfbParser;->pfbdata:[B

    iget-object v1, p0, Lorg/apache/fontbox/pfb/PfbParser;->lengths:[I

    const/4 v2, 0x0

    aget v2, v1, v2

    const/4 v3, 0x1

    aget v1, v1, v3

    add-int/2addr v1, v2

    invoke-static {v0, v2, v1}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object v0

    return-object v0
.end method

.method public size()I
    .locals 1

    iget-object v0, p0, Lorg/apache/fontbox/pfb/PfbParser;->pfbdata:[B

    array-length v0, v0

    return v0
.end method

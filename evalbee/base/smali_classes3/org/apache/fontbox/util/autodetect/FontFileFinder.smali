.class public Lorg/apache/fontbox/util/autodetect/FontFileFinder;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private fontDirFinder:Lorg/apache/fontbox/util/autodetect/FontDirFinder;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/fontbox/util/autodetect/FontFileFinder;->fontDirFinder:Lorg/apache/fontbox/util/autodetect/FontDirFinder;

    return-void
.end method

.method private checkFontfile(Ljava/io/File;)Z
    .locals 1

    invoke-virtual {p1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object p1

    const-string v0, ".ttf"

    invoke-virtual {p1, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, ".otf"

    invoke-virtual {p1, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, ".pfb"

    invoke-virtual {p1, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, ".ttc"

    invoke-virtual {p1, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p1, 0x1

    :goto_1
    return p1
.end method

.method private determineDirFinder()Lorg/apache/fontbox/util/autodetect/FontDirFinder;
    .locals 2

    const-string v0, "java.vendor"

    invoke-static {v0}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "The Android Project"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lorg/apache/fontbox/util/autodetect/AndroidFontDirFinder;

    invoke-direct {v0}, Lorg/apache/fontbox/util/autodetect/AndroidFontDirFinder;-><init>()V

    return-object v0

    :cond_0
    new-instance v0, Lorg/apache/fontbox/util/autodetect/UnixFontDirFinder;

    invoke-direct {v0}, Lorg/apache/fontbox/util/autodetect/UnixFontDirFinder;-><init>()V

    return-object v0
.end method

.method private walk(Ljava/io/File;Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/File;",
            "Ljava/util/List<",
            "Ljava/net/URI;",
            ">;)V"
        }
    .end annotation

    invoke-virtual {p1}, Ljava/io/File;->isDirectory()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object p1

    if-eqz p1, :cond_3

    array-length v0, p1

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_3

    aget-object v2, p1, v1

    invoke-virtual {v2}, Ljava/io/File;->isDirectory()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v3

    const-string v4, "."

    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    goto :goto_1

    :cond_0
    invoke-direct {p0, v2, p2}, Lorg/apache/fontbox/util/autodetect/FontFileFinder;->walk(Ljava/io/File;Ljava/util/List;)V

    goto :goto_1

    :cond_1
    invoke-direct {p0, v2}, Lorg/apache/fontbox/util/autodetect/FontFileFinder;->checkFontfile(Ljava/io/File;)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {v2}, Ljava/io/File;->toURI()Ljava/net/URI;

    move-result-object v2

    invoke-interface {p2, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_3
    return-void
.end method


# virtual methods
.method public find()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/net/URI;",
            ">;"
        }
    .end annotation

    .line 1
    iget-object v0, p0, Lorg/apache/fontbox/util/autodetect/FontFileFinder;->fontDirFinder:Lorg/apache/fontbox/util/autodetect/FontDirFinder;

    if-nez v0, :cond_0

    invoke-direct {p0}, Lorg/apache/fontbox/util/autodetect/FontFileFinder;->determineDirFinder()Lorg/apache/fontbox/util/autodetect/FontDirFinder;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/fontbox/util/autodetect/FontFileFinder;->fontDirFinder:Lorg/apache/fontbox/util/autodetect/FontDirFinder;

    :cond_0
    iget-object v0, p0, Lorg/apache/fontbox/util/autodetect/FontFileFinder;->fontDirFinder:Lorg/apache/fontbox/util/autodetect/FontDirFinder;

    invoke-interface {v0}, Lorg/apache/fontbox/util/autodetect/FontDirFinder;->find()Ljava/util/List;

    move-result-object v0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/io/File;

    invoke-direct {p0, v2, v1}, Lorg/apache/fontbox/util/autodetect/FontFileFinder;->walk(Ljava/io/File;Ljava/util/List;)V

    goto :goto_0

    :cond_1
    return-object v1
.end method

.method public find(Ljava/lang/String;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List<",
            "Ljava/net/URI;",
            ">;"
        }
    .end annotation

    .line 2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->isDirectory()Z

    move-result p1

    if-eqz p1, :cond_0

    invoke-direct {p0, v1, v0}, Lorg/apache/fontbox/util/autodetect/FontFileFinder;->walk(Ljava/io/File;Ljava/util/List;)V

    :cond_0
    return-object v0
.end method

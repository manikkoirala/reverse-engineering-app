.class public Lorg/apache/fontbox/cff/Type1CharStringFormatter;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private output:Ljava/io/ByteArrayOutputStream;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/fontbox/cff/Type1CharStringFormatter;->output:Ljava/io/ByteArrayOutputStream;

    return-void
.end method

.method private writeCommand(Lorg/apache/fontbox/cff/CharStringCommand;)V
    .locals 3

    invoke-virtual {p1}, Lorg/apache/fontbox/cff/CharStringCommand;->getKey()Lorg/apache/fontbox/cff/CharStringCommand$Key;

    move-result-object p1

    invoke-virtual {p1}, Lorg/apache/fontbox/cff/CharStringCommand$Key;->getValue()[I

    move-result-object p1

    const/4 v0, 0x0

    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lorg/apache/fontbox/cff/Type1CharStringFormatter;->output:Ljava/io/ByteArrayOutputStream;

    aget v2, p1, v0

    invoke-virtual {v1, v2}, Ljava/io/ByteArrayOutputStream;->write(I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private writeNumber(Ljava/lang/Integer;)V
    .locals 5

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    const/16 v0, -0x6b

    if-lt p1, v0, :cond_0

    const/16 v0, 0x6b

    if-gt p1, v0, :cond_0

    iget-object v0, p0, Lorg/apache/fontbox/cff/Type1CharStringFormatter;->output:Ljava/io/ByteArrayOutputStream;

    add-int/lit16 p1, p1, 0x8b

    :goto_0
    invoke-virtual {v0, p1}, Ljava/io/ByteArrayOutputStream;->write(I)V

    goto :goto_2

    :cond_0
    const/16 v0, 0x6c

    if-lt p1, v0, :cond_1

    const/16 v1, 0x46b

    if-gt p1, v1, :cond_1

    sub-int/2addr p1, v0

    rem-int/lit16 v0, p1, 0x100

    sub-int/2addr p1, v0

    div-int/lit16 p1, p1, 0x100

    add-int/lit16 p1, p1, 0xf7

    :goto_1
    iget-object v1, p0, Lorg/apache/fontbox/cff/Type1CharStringFormatter;->output:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v1, p1}, Ljava/io/ByteArrayOutputStream;->write(I)V

    iget-object p1, p0, Lorg/apache/fontbox/cff/Type1CharStringFormatter;->output:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {p1, v0}, Ljava/io/ByteArrayOutputStream;->write(I)V

    goto :goto_2

    :cond_1
    const/16 v1, -0x46b

    if-lt p1, v1, :cond_2

    const/16 v1, -0x6c

    if-gt p1, v1, :cond_2

    add-int/2addr p1, v0

    rem-int/lit16 v0, p1, 0x100

    neg-int v0, v0

    add-int/2addr p1, v0

    div-int/lit16 p1, p1, 0x100

    add-int/lit16 p1, p1, -0xfb

    neg-int p1, p1

    goto :goto_1

    :cond_2
    ushr-int/lit8 v0, p1, 0x18

    const/16 v1, 0xff

    and-int/2addr v0, v1

    ushr-int/lit8 v2, p1, 0x10

    and-int/2addr v2, v1

    ushr-int/lit8 v3, p1, 0x8

    and-int/2addr v3, v1

    ushr-int/lit8 p1, p1, 0x0

    and-int/2addr p1, v1

    iget-object v4, p0, Lorg/apache/fontbox/cff/Type1CharStringFormatter;->output:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v4, v1}, Ljava/io/ByteArrayOutputStream;->write(I)V

    iget-object v1, p0, Lorg/apache/fontbox/cff/Type1CharStringFormatter;->output:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v1, v0}, Ljava/io/ByteArrayOutputStream;->write(I)V

    iget-object v0, p0, Lorg/apache/fontbox/cff/Type1CharStringFormatter;->output:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v0, v2}, Ljava/io/ByteArrayOutputStream;->write(I)V

    iget-object v0, p0, Lorg/apache/fontbox/cff/Type1CharStringFormatter;->output:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v0, v3}, Ljava/io/ByteArrayOutputStream;->write(I)V

    iget-object v0, p0, Lorg/apache/fontbox/cff/Type1CharStringFormatter;->output:Ljava/io/ByteArrayOutputStream;

    goto :goto_0

    :goto_2
    return-void
.end method


# virtual methods
.method public format(Ljava/util/List;)[B
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/Object;",
            ">;)[B"
        }
    .end annotation

    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    iput-object v0, p0, Lorg/apache/fontbox/cff/Type1CharStringFormatter;->output:Ljava/io/ByteArrayOutputStream;

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Lorg/apache/fontbox/cff/CharStringCommand;

    if-eqz v1, :cond_0

    check-cast v0, Lorg/apache/fontbox/cff/CharStringCommand;

    invoke-direct {p0, v0}, Lorg/apache/fontbox/cff/Type1CharStringFormatter;->writeCommand(Lorg/apache/fontbox/cff/CharStringCommand;)V

    goto :goto_0

    :cond_0
    instance-of v1, v0, Ljava/lang/Integer;

    if-eqz v1, :cond_1

    check-cast v0, Ljava/lang/Integer;

    invoke-direct {p0, v0}, Lorg/apache/fontbox/cff/Type1CharStringFormatter;->writeNumber(Ljava/lang/Integer;)V

    goto :goto_0

    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    invoke-direct {p1}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw p1

    :cond_2
    iget-object p1, p0, Lorg/apache/fontbox/cff/Type1CharStringFormatter;->output:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {p1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object p1

    return-object p1
.end method

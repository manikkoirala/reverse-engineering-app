.class Lorg/apache/fontbox/cff/CFFParser$EmbeddedEncoding$Supplement;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/fontbox/cff/CFFParser$EmbeddedEncoding;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Supplement"
.end annotation


# instance fields
.field private code:I

.field private name:Ljava/lang/String;

.field private sid:I


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static synthetic access$2300(Lorg/apache/fontbox/cff/CFFParser$EmbeddedEncoding$Supplement;)I
    .locals 0

    iget p0, p0, Lorg/apache/fontbox/cff/CFFParser$EmbeddedEncoding$Supplement;->code:I

    return p0
.end method

.method public static synthetic access$2302(Lorg/apache/fontbox/cff/CFFParser$EmbeddedEncoding$Supplement;I)I
    .locals 0

    iput p1, p0, Lorg/apache/fontbox/cff/CFFParser$EmbeddedEncoding$Supplement;->code:I

    return p1
.end method

.method public static synthetic access$2400(Lorg/apache/fontbox/cff/CFFParser$EmbeddedEncoding$Supplement;)I
    .locals 0

    iget p0, p0, Lorg/apache/fontbox/cff/CFFParser$EmbeddedEncoding$Supplement;->sid:I

    return p0
.end method

.method public static synthetic access$2402(Lorg/apache/fontbox/cff/CFFParser$EmbeddedEncoding$Supplement;I)I
    .locals 0

    iput p1, p0, Lorg/apache/fontbox/cff/CFFParser$EmbeddedEncoding$Supplement;->sid:I

    return p1
.end method

.method public static synthetic access$2502(Lorg/apache/fontbox/cff/CFFParser$EmbeddedEncoding$Supplement;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, Lorg/apache/fontbox/cff/CFFParser$EmbeddedEncoding$Supplement;->name:Ljava/lang/String;

    return-object p1
.end method


# virtual methods
.method public getCode()I
    .locals 1

    iget v0, p0, Lorg/apache/fontbox/cff/CFFParser$EmbeddedEncoding$Supplement;->code:I

    return v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/apache/fontbox/cff/CFFParser$EmbeddedEncoding$Supplement;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getSID()I
    .locals 1

    iget v0, p0, Lorg/apache/fontbox/cff/CFFParser$EmbeddedEncoding$Supplement;->sid:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "[code="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lorg/apache/fontbox/cff/CFFParser$EmbeddedEncoding$Supplement;->code:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", sid="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lorg/apache/fontbox/cff/CFFParser$EmbeddedEncoding$Supplement;->sid:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.class public final Lorg/apache/fontbox/cff/CFFExpertCharset;
.super Lorg/apache/fontbox/cff/CFFCharset;
.source "SourceFile"


# static fields
.field private static final INSTANCE:Lorg/apache/fontbox/cff/CFFExpertCharset;


# direct methods
.method public static constructor <clinit>()V
    .locals 9

    new-instance v0, Lorg/apache/fontbox/cff/CFFExpertCharset;

    invoke-direct {v0}, Lorg/apache/fontbox/cff/CFFExpertCharset;-><init>()V

    sput-object v0, Lorg/apache/fontbox/cff/CFFExpertCharset;->INSTANCE:Lorg/apache/fontbox/cff/CFFExpertCharset;

    const-string v1, ".notdef"

    const/4 v2, 0x0

    invoke-virtual {v0, v2, v2, v1}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const-string v1, "space"

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v2, v1}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0xe5

    const-string v2, "exclamsmall"

    const/4 v3, 0x2

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0xe6

    const-string v2, "Hungarumlautsmall"

    const/4 v3, 0x3

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0xe7

    const-string v2, "dollaroldstyle"

    const/4 v3, 0x4

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0xe8

    const-string v2, "dollarsuperior"

    const/4 v3, 0x5

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0xe9

    const-string v2, "ampersandsmall"

    const/4 v3, 0x6

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0xea

    const-string v2, "Acutesmall"

    const/4 v3, 0x7

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0xeb

    const-string v2, "parenleftsuperior"

    const/16 v3, 0x8

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0xec

    const-string v2, "parenrightsuperior"

    const/16 v3, 0x9

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0xed

    const-string v2, "twodotenleader"

    const/16 v3, 0xa

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0xee

    const-string v2, "onedotenleader"

    const/16 v3, 0xb

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const-string v1, "comma"

    const/16 v2, 0xc

    const/16 v3, 0xd

    invoke-virtual {v0, v2, v3, v1}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const-string v1, "hyphen"

    const/16 v2, 0xe

    invoke-virtual {v0, v3, v2, v1}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const-string v1, "period"

    const/16 v3, 0xf

    invoke-virtual {v0, v2, v3, v1}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const-string v1, "fraction"

    const/16 v2, 0x63

    invoke-virtual {v0, v3, v2, v1}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0xef

    const-string v3, "zerooldstyle"

    const/16 v4, 0x10

    invoke-virtual {v0, v4, v1, v3}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0xf0

    const-string v3, "oneoldstyle"

    const/16 v4, 0x11

    invoke-virtual {v0, v4, v1, v3}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0xf1

    const-string v3, "twooldstyle"

    const/16 v4, 0x12

    invoke-virtual {v0, v4, v1, v3}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0xf2

    const-string v3, "threeoldstyle"

    const/16 v4, 0x13

    invoke-virtual {v0, v4, v1, v3}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0xf3

    const-string v3, "fouroldstyle"

    const/16 v4, 0x14

    invoke-virtual {v0, v4, v1, v3}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0xf4

    const-string v3, "fiveoldstyle"

    const/16 v4, 0x15

    invoke-virtual {v0, v4, v1, v3}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0xf5

    const-string v3, "sixoldstyle"

    const/16 v4, 0x16

    invoke-virtual {v0, v4, v1, v3}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0xf6

    const-string v3, "sevenoldstyle"

    const/16 v4, 0x17

    invoke-virtual {v0, v4, v1, v3}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0xf7

    const-string v3, "eightoldstyle"

    const/16 v4, 0x18

    invoke-virtual {v0, v4, v1, v3}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0xf8

    const-string v3, "nineoldstyle"

    const/16 v4, 0x19

    invoke-virtual {v0, v4, v1, v3}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const-string v1, "colon"

    const/16 v3, 0x1a

    const/16 v4, 0x1b

    invoke-virtual {v0, v3, v4, v1}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const-string v1, "semicolon"

    const/16 v3, 0x1c

    invoke-virtual {v0, v4, v3, v1}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0xf9

    const-string v4, "commasuperior"

    invoke-virtual {v0, v3, v1, v4}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0xfa

    const-string v3, "threequartersemdash"

    const/16 v4, 0x1d

    invoke-virtual {v0, v4, v1, v3}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0xfb

    const-string v3, "periodsuperior"

    const/16 v4, 0x1e

    invoke-virtual {v0, v4, v1, v3}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0xfc

    const-string v3, "questionsmall"

    const/16 v4, 0x1f

    invoke-virtual {v0, v4, v1, v3}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0xfd

    const-string v3, "asuperior"

    const/16 v4, 0x20

    invoke-virtual {v0, v4, v1, v3}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0xfe

    const-string v3, "bsuperior"

    const/16 v4, 0x21

    invoke-virtual {v0, v4, v1, v3}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0xff

    const-string v3, "centsuperior"

    const/16 v4, 0x22

    invoke-virtual {v0, v4, v1, v3}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x100

    const-string v3, "dsuperior"

    const/16 v4, 0x23

    invoke-virtual {v0, v4, v1, v3}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x101

    const-string v3, "esuperior"

    const/16 v4, 0x24

    invoke-virtual {v0, v4, v1, v3}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x102

    const-string v3, "isuperior"

    const/16 v4, 0x25

    invoke-virtual {v0, v4, v1, v3}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x103

    const-string v3, "lsuperior"

    const/16 v4, 0x26

    invoke-virtual {v0, v4, v1, v3}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x104

    const-string v3, "msuperior"

    const/16 v4, 0x27

    invoke-virtual {v0, v4, v1, v3}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x105

    const-string v3, "nsuperior"

    const/16 v4, 0x28

    invoke-virtual {v0, v4, v1, v3}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x106

    const-string v3, "osuperior"

    const/16 v4, 0x29

    invoke-virtual {v0, v4, v1, v3}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x107

    const-string v3, "rsuperior"

    const/16 v4, 0x2a

    invoke-virtual {v0, v4, v1, v3}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x108

    const-string v3, "ssuperior"

    const/16 v4, 0x2b

    invoke-virtual {v0, v4, v1, v3}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x109

    const-string v3, "tsuperior"

    const/16 v4, 0x2c

    invoke-virtual {v0, v4, v1, v3}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x10a

    const-string v3, "ff"

    const/16 v4, 0x2d

    invoke-virtual {v0, v4, v1, v3}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const-string v1, "fi"

    const/16 v3, 0x2e

    const/16 v4, 0x6d

    invoke-virtual {v0, v3, v4, v1}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const-string v1, "fl"

    const/16 v3, 0x2f

    const/16 v5, 0x6e

    invoke-virtual {v0, v3, v5, v1}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x10b

    const-string v3, "ffi"

    const/16 v6, 0x30

    invoke-virtual {v0, v6, v1, v3}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x10c

    const-string v3, "ffl"

    const/16 v6, 0x31

    invoke-virtual {v0, v6, v1, v3}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x10d

    const-string v3, "parenleftinferior"

    const/16 v6, 0x32

    invoke-virtual {v0, v6, v1, v3}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x10e

    const-string v3, "parenrightinferior"

    const/16 v6, 0x33

    invoke-virtual {v0, v6, v1, v3}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x10f

    const-string v3, "Circumflexsmall"

    const/16 v6, 0x34

    invoke-virtual {v0, v6, v1, v3}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x110

    const-string v3, "hyphensuperior"

    const/16 v6, 0x35

    invoke-virtual {v0, v6, v1, v3}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x111

    const-string v3, "Gravesmall"

    const/16 v6, 0x36

    invoke-virtual {v0, v6, v1, v3}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x112

    const-string v3, "Asmall"

    const/16 v6, 0x37

    invoke-virtual {v0, v6, v1, v3}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x113

    const-string v3, "Bsmall"

    const/16 v6, 0x38

    invoke-virtual {v0, v6, v1, v3}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x114

    const-string v3, "Csmall"

    const/16 v6, 0x39

    invoke-virtual {v0, v6, v1, v3}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x115

    const-string v3, "Dsmall"

    const/16 v6, 0x3a

    invoke-virtual {v0, v6, v1, v3}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x116

    const-string v3, "Esmall"

    const/16 v6, 0x3b

    invoke-virtual {v0, v6, v1, v3}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x117

    const-string v3, "Fsmall"

    const/16 v6, 0x3c

    invoke-virtual {v0, v6, v1, v3}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x118

    const-string v3, "Gsmall"

    const/16 v6, 0x3d

    invoke-virtual {v0, v6, v1, v3}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x119

    const-string v3, "Hsmall"

    const/16 v6, 0x3e

    invoke-virtual {v0, v6, v1, v3}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x11a

    const-string v3, "Ismall"

    const/16 v6, 0x3f

    invoke-virtual {v0, v6, v1, v3}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x11b

    const-string v3, "Jsmall"

    const/16 v6, 0x40

    invoke-virtual {v0, v6, v1, v3}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x11c

    const-string v3, "Ksmall"

    const/16 v6, 0x41

    invoke-virtual {v0, v6, v1, v3}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x11d

    const-string v3, "Lsmall"

    const/16 v6, 0x42

    invoke-virtual {v0, v6, v1, v3}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x11e

    const-string v3, "Msmall"

    const/16 v6, 0x43

    invoke-virtual {v0, v6, v1, v3}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x11f

    const-string v3, "Nsmall"

    const/16 v6, 0x44

    invoke-virtual {v0, v6, v1, v3}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x120

    const-string v3, "Osmall"

    const/16 v6, 0x45

    invoke-virtual {v0, v6, v1, v3}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x121

    const-string v3, "Psmall"

    const/16 v6, 0x46

    invoke-virtual {v0, v6, v1, v3}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x122

    const-string v3, "Qsmall"

    const/16 v6, 0x47

    invoke-virtual {v0, v6, v1, v3}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x123

    const-string v3, "Rsmall"

    const/16 v6, 0x48

    invoke-virtual {v0, v6, v1, v3}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x124

    const-string v3, "Ssmall"

    const/16 v6, 0x49

    invoke-virtual {v0, v6, v1, v3}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x125

    const-string v3, "Tsmall"

    const/16 v6, 0x4a

    invoke-virtual {v0, v6, v1, v3}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x126

    const-string v3, "Usmall"

    const/16 v6, 0x4b

    invoke-virtual {v0, v6, v1, v3}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x127

    const-string v3, "Vsmall"

    const/16 v6, 0x4c

    invoke-virtual {v0, v6, v1, v3}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x128

    const-string v3, "Wsmall"

    const/16 v6, 0x4d

    invoke-virtual {v0, v6, v1, v3}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x129

    const-string v3, "Xsmall"

    const/16 v6, 0x4e

    invoke-virtual {v0, v6, v1, v3}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x12a

    const-string v3, "Ysmall"

    const/16 v6, 0x4f

    invoke-virtual {v0, v6, v1, v3}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x12b

    const-string v3, "Zsmall"

    const/16 v6, 0x50

    invoke-virtual {v0, v6, v1, v3}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x12c

    const-string v3, "colonmonetary"

    const/16 v6, 0x51

    invoke-virtual {v0, v6, v1, v3}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x12d

    const-string v3, "onefitted"

    const/16 v6, 0x52

    invoke-virtual {v0, v6, v1, v3}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x12e

    const-string v3, "rupiah"

    const/16 v6, 0x53

    invoke-virtual {v0, v6, v1, v3}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x12f

    const-string v3, "Tildesmall"

    const/16 v6, 0x54

    invoke-virtual {v0, v6, v1, v3}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x130

    const-string v3, "exclamdownsmall"

    const/16 v6, 0x55

    invoke-virtual {v0, v6, v1, v3}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x131

    const-string v3, "centoldstyle"

    const/16 v6, 0x56

    invoke-virtual {v0, v6, v1, v3}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x132

    const-string v3, "Lslashsmall"

    const/16 v6, 0x57

    invoke-virtual {v0, v6, v1, v3}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x133

    const-string v3, "Scaronsmall"

    const/16 v6, 0x58

    invoke-virtual {v0, v6, v1, v3}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x134

    const-string v3, "Zcaronsmall"

    const/16 v6, 0x59

    invoke-virtual {v0, v6, v1, v3}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x135

    const-string v3, "Dieresissmall"

    const/16 v6, 0x5a

    invoke-virtual {v0, v6, v1, v3}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x136

    const-string v3, "Brevesmall"

    const/16 v6, 0x5b

    invoke-virtual {v0, v6, v1, v3}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x137

    const-string v3, "Caronsmall"

    const/16 v6, 0x5c

    invoke-virtual {v0, v6, v1, v3}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x138

    const-string v3, "Dotaccentsmall"

    const/16 v6, 0x5d

    invoke-virtual {v0, v6, v1, v3}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x139

    const-string v3, "Macronsmall"

    const/16 v6, 0x5e

    invoke-virtual {v0, v6, v1, v3}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x13a

    const-string v3, "figuredash"

    const/16 v6, 0x5f

    invoke-virtual {v0, v6, v1, v3}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x13b

    const-string v3, "hypheninferior"

    const/16 v6, 0x60

    invoke-virtual {v0, v6, v1, v3}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x13c

    const-string v3, "Ogoneksmall"

    const/16 v6, 0x61

    invoke-virtual {v0, v6, v1, v3}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x13d

    const-string v3, "Ringsmall"

    const/16 v6, 0x62

    invoke-virtual {v0, v6, v1, v3}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x13e

    const-string v3, "Cedillasmall"

    invoke-virtual {v0, v2, v1, v3}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const-string v1, "onequarter"

    const/16 v2, 0x64

    const/16 v3, 0x9e

    invoke-virtual {v0, v2, v3, v1}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const-string v1, "onehalf"

    const/16 v2, 0x65

    const/16 v6, 0x9b

    invoke-virtual {v0, v2, v6, v1}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const-string v1, "threequarters"

    const/16 v2, 0x66

    const/16 v7, 0xa3

    invoke-virtual {v0, v2, v7, v1}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x13f

    const-string v2, "questiondownsmall"

    const/16 v8, 0x67

    invoke-virtual {v0, v8, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x140

    const-string v2, "oneeighth"

    const/16 v8, 0x68

    invoke-virtual {v0, v8, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x141

    const-string v2, "threeeighths"

    const/16 v8, 0x69

    invoke-virtual {v0, v8, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x142

    const-string v2, "fiveeighths"

    const/16 v8, 0x6a

    invoke-virtual {v0, v8, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x143

    const-string v2, "seveneighths"

    const/16 v8, 0x6b

    invoke-virtual {v0, v8, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x144

    const-string v2, "onethird"

    const/16 v8, 0x6c

    invoke-virtual {v0, v8, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x145

    const-string v2, "twothirds"

    invoke-virtual {v0, v4, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x146

    const-string v2, "zerosuperior"

    invoke-virtual {v0, v5, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const-string v1, "onesuperior"

    const/16 v2, 0x6f

    const/16 v4, 0x96

    invoke-virtual {v0, v2, v4, v1}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const-string v1, "twosuperior"

    const/16 v2, 0x70

    const/16 v5, 0xa4

    invoke-virtual {v0, v2, v5, v1}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0xa9

    const-string v2, "threesuperior"

    const/16 v8, 0x71

    invoke-virtual {v0, v8, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x147

    const-string v2, "foursuperior"

    const/16 v8, 0x72

    invoke-virtual {v0, v8, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x148

    const-string v2, "fivesuperior"

    const/16 v8, 0x73

    invoke-virtual {v0, v8, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x149

    const-string v2, "sixsuperior"

    const/16 v8, 0x74

    invoke-virtual {v0, v8, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x14a

    const-string v2, "sevensuperior"

    const/16 v8, 0x75

    invoke-virtual {v0, v8, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x14b

    const-string v2, "eightsuperior"

    const/16 v8, 0x76

    invoke-virtual {v0, v8, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x14c

    const-string v2, "ninesuperior"

    const/16 v8, 0x77

    invoke-virtual {v0, v8, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x14d

    const-string v2, "zeroinferior"

    const/16 v8, 0x78

    invoke-virtual {v0, v8, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x14e

    const-string v2, "oneinferior"

    const/16 v8, 0x79

    invoke-virtual {v0, v8, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x14f

    const-string v2, "twoinferior"

    const/16 v8, 0x7a

    invoke-virtual {v0, v8, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x150

    const-string v2, "threeinferior"

    const/16 v8, 0x7b

    invoke-virtual {v0, v8, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x151

    const-string v2, "fourinferior"

    const/16 v8, 0x7c

    invoke-virtual {v0, v8, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x152

    const-string v2, "fiveinferior"

    const/16 v8, 0x7d

    invoke-virtual {v0, v8, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x153

    const-string v2, "sixinferior"

    const/16 v8, 0x7e

    invoke-virtual {v0, v8, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x154

    const-string v2, "seveninferior"

    const/16 v8, 0x7f

    invoke-virtual {v0, v8, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x155

    const-string v2, "eightinferior"

    const/16 v8, 0x80

    invoke-virtual {v0, v8, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x156

    const-string v2, "nineinferior"

    const/16 v8, 0x81

    invoke-virtual {v0, v8, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x157

    const-string v2, "centinferior"

    const/16 v8, 0x82

    invoke-virtual {v0, v8, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x158

    const-string v2, "dollarinferior"

    const/16 v8, 0x83

    invoke-virtual {v0, v8, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x159

    const-string v2, "periodinferior"

    const/16 v8, 0x84

    invoke-virtual {v0, v8, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x15a

    const-string v2, "commainferior"

    const/16 v8, 0x85

    invoke-virtual {v0, v8, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x15b

    const-string v2, "Agravesmall"

    const/16 v8, 0x86

    invoke-virtual {v0, v8, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x15c

    const-string v2, "Aacutesmall"

    const/16 v8, 0x87

    invoke-virtual {v0, v8, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x15d

    const-string v2, "Acircumflexsmall"

    const/16 v8, 0x88

    invoke-virtual {v0, v8, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x15e

    const-string v2, "Atildesmall"

    const/16 v8, 0x89

    invoke-virtual {v0, v8, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x15f

    const-string v2, "Adieresissmall"

    const/16 v8, 0x8a

    invoke-virtual {v0, v8, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x160

    const-string v2, "Aringsmall"

    const/16 v8, 0x8b

    invoke-virtual {v0, v8, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x161

    const-string v2, "AEsmall"

    const/16 v8, 0x8c

    invoke-virtual {v0, v8, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x162

    const-string v2, "Ccedillasmall"

    const/16 v8, 0x8d

    invoke-virtual {v0, v8, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x163

    const-string v2, "Egravesmall"

    const/16 v8, 0x8e

    invoke-virtual {v0, v8, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x164

    const-string v2, "Eacutesmall"

    const/16 v8, 0x8f

    invoke-virtual {v0, v8, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x165

    const-string v2, "Ecircumflexsmall"

    const/16 v8, 0x90

    invoke-virtual {v0, v8, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x166

    const-string v2, "Edieresissmall"

    const/16 v8, 0x91

    invoke-virtual {v0, v8, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x167

    const-string v2, "Igravesmall"

    const/16 v8, 0x92

    invoke-virtual {v0, v8, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x168

    const-string v2, "Iacutesmall"

    const/16 v8, 0x93

    invoke-virtual {v0, v8, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x169

    const-string v2, "Icircumflexsmall"

    const/16 v8, 0x94

    invoke-virtual {v0, v8, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x16a

    const-string v2, "Idieresissmall"

    const/16 v8, 0x95

    invoke-virtual {v0, v8, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x16b

    const-string v2, "Ethsmall"

    invoke-virtual {v0, v4, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x16c

    const-string v2, "Ntildesmall"

    const/16 v4, 0x97

    invoke-virtual {v0, v4, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x16d

    const-string v2, "Ogravesmall"

    const/16 v4, 0x98

    invoke-virtual {v0, v4, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x16e

    const-string v2, "Oacutesmall"

    const/16 v4, 0x99

    invoke-virtual {v0, v4, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x16f

    const-string v2, "Ocircumflexsmall"

    const/16 v4, 0x9a

    invoke-virtual {v0, v4, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x170

    const-string v2, "Otildesmall"

    invoke-virtual {v0, v6, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x171

    const-string v2, "Odieresissmall"

    const/16 v4, 0x9c

    invoke-virtual {v0, v4, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x172

    const-string v2, "OEsmall"

    const/16 v4, 0x9d

    invoke-virtual {v0, v4, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x173

    const-string v2, "Oslashsmall"

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x174

    const-string v2, "Ugravesmall"

    const/16 v3, 0x9f

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x175

    const-string v2, "Uacutesmall"

    const/16 v3, 0xa0

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x176

    const-string v2, "Ucircumflexsmall"

    const/16 v3, 0xa1

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x177

    const-string v2, "Udieresissmall"

    const/16 v3, 0xa2

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x178

    const-string v2, "Yacutesmall"

    invoke-virtual {v0, v7, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x179

    const-string v2, "Thornsmall"

    invoke-virtual {v0, v5, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x17a

    const-string v2, "Ydieresissmall"

    const/16 v3, 0xa5

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lorg/apache/fontbox/cff/CFFCharset;-><init>(Z)V

    return-void
.end method

.method public static getInstance()Lorg/apache/fontbox/cff/CFFExpertCharset;
    .locals 1

    sget-object v0, Lorg/apache/fontbox/cff/CFFExpertCharset;->INSTANCE:Lorg/apache/fontbox/cff/CFFExpertCharset;

    return-object v0
.end method

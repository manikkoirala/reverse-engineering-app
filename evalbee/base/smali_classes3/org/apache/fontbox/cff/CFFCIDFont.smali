.class public Lorg/apache/fontbox/cff/CFFCIDFont;
.super Lorg/apache/fontbox/cff/CFFFont;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/fontbox/cff/CFFCIDFont$PrivateType1CharStringReader;
    }
.end annotation


# instance fields
.field private final charStringCache:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Lorg/apache/fontbox/cff/CIDKeyedType2CharString;",
            ">;"
        }
    .end annotation
.end field

.field private fdSelect:Lorg/apache/fontbox/cff/FDSelect;

.field private fontDictionaries:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation
.end field

.field private ordering:Ljava/lang/String;

.field private privateDictionaries:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation
.end field

.field private final reader:Lorg/apache/fontbox/cff/CFFCIDFont$PrivateType1CharStringReader;

.field private registry:Ljava/lang/String;

.field private supplement:I


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Lorg/apache/fontbox/cff/CFFFont;-><init>()V

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lorg/apache/fontbox/cff/CFFCIDFont;->fontDictionaries:Ljava/util/List;

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lorg/apache/fontbox/cff/CFFCIDFont;->privateDictionaries:Ljava/util/List;

    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/fontbox/cff/CFFCIDFont;->charStringCache:Ljava/util/Map;

    new-instance v0, Lorg/apache/fontbox/cff/CFFCIDFont$PrivateType1CharStringReader;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lorg/apache/fontbox/cff/CFFCIDFont$PrivateType1CharStringReader;-><init>(Lorg/apache/fontbox/cff/CFFCIDFont;Lorg/apache/fontbox/cff/CFFCIDFont$1;)V

    iput-object v0, p0, Lorg/apache/fontbox/cff/CFFCIDFont;->reader:Lorg/apache/fontbox/cff/CFFCIDFont$PrivateType1CharStringReader;

    return-void
.end method

.method private getDefaultWidthX(I)I
    .locals 3

    iget-object v0, p0, Lorg/apache/fontbox/cff/CFFCIDFont;->fdSelect:Lorg/apache/fontbox/cff/FDSelect;

    invoke-virtual {v0, p1}, Lorg/apache/fontbox/cff/FDSelect;->getFDIndex(I)I

    move-result p1

    const/4 v0, -0x1

    const/16 v1, 0x3e8

    if-ne p1, v0, :cond_0

    return v1

    :cond_0
    iget-object v0, p0, Lorg/apache/fontbox/cff/CFFCIDFont;->privateDictionaries:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Map;

    const-string v0, "defaultWidthX"

    invoke-interface {p1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Number;

    invoke-virtual {p1}, Ljava/lang/Number;->intValue()I

    move-result v1

    :cond_1
    return v1
.end method

.method private getLocalSubrIndex(I)Lorg/apache/fontbox/cff/IndexData;
    .locals 1

    iget-object v0, p0, Lorg/apache/fontbox/cff/CFFCIDFont;->fdSelect:Lorg/apache/fontbox/cff/FDSelect;

    invoke-virtual {v0, p1}, Lorg/apache/fontbox/cff/FDSelect;->getFDIndex(I)I

    move-result p1

    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    new-instance p1, Lorg/apache/fontbox/cff/IndexData;

    const/4 v0, 0x0

    invoke-direct {p1, v0}, Lorg/apache/fontbox/cff/IndexData;-><init>(I)V

    return-object p1

    :cond_0
    iget-object v0, p0, Lorg/apache/fontbox/cff/CFFCIDFont;->privateDictionaries:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Map;

    const-string v0, "Subrs"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lorg/apache/fontbox/cff/IndexData;

    return-object p1
.end method

.method private getNominalWidthX(I)I
    .locals 3

    iget-object v0, p0, Lorg/apache/fontbox/cff/CFFCIDFont;->fdSelect:Lorg/apache/fontbox/cff/FDSelect;

    invoke-virtual {v0, p1}, Lorg/apache/fontbox/cff/FDSelect;->getFDIndex(I)I

    move-result p1

    const/4 v0, -0x1

    const/4 v1, 0x0

    if-ne p1, v0, :cond_0

    return v1

    :cond_0
    iget-object v0, p0, Lorg/apache/fontbox/cff/CFFCIDFont;->privateDictionaries:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Map;

    const-string v0, "nominalWidthX"

    invoke-interface {p1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Number;

    invoke-virtual {p1}, Ljava/lang/Number;->intValue()I

    move-result v1

    :cond_1
    return v1
.end method


# virtual methods
.method public getFdSelect()Lorg/apache/fontbox/cff/FDSelect;
    .locals 1

    iget-object v0, p0, Lorg/apache/fontbox/cff/CFFCIDFont;->fdSelect:Lorg/apache/fontbox/cff/FDSelect;

    return-object v0
.end method

.method public getFontDicts()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/fontbox/cff/CFFCIDFont;->fontDictionaries:Ljava/util/List;

    return-object v0
.end method

.method public getFontMatrix()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/Number;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/fontbox/cff/CFFFont;->topDict:Ljava/util/Map;

    const-string v1, "FontMatrix"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method public getOrdering()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/apache/fontbox/cff/CFFCIDFont;->ordering:Ljava/lang/String;

    return-object v0
.end method

.method public getPrivDicts()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/fontbox/cff/CFFCIDFont;->privateDictionaries:Ljava/util/List;

    return-object v0
.end method

.method public getRegistry()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/apache/fontbox/cff/CFFCIDFont;->registry:Ljava/lang/String;

    return-object v0
.end method

.method public getSupplement()I
    .locals 1

    iget v0, p0, Lorg/apache/fontbox/cff/CFFCIDFont;->supplement:I

    return v0
.end method

.method public getType2CharString(I)Lorg/apache/fontbox/cff/CIDKeyedType2CharString;
    .locals 9

    iget-object v0, p0, Lorg/apache/fontbox/cff/CFFCIDFont;->charStringCache:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/fontbox/cff/CIDKeyedType2CharString;

    if-nez v0, :cond_1

    iget-object v0, p0, Lorg/apache/fontbox/cff/CFFFont;->charset:Lorg/apache/fontbox/cff/CFFCharset;

    invoke-virtual {v0, p1}, Lorg/apache/fontbox/cff/CFFCharset;->getGIDForCID(I)I

    move-result v5

    iget-object v0, p0, Lorg/apache/fontbox/cff/CFFFont;->charStrings:Ljava/util/List;

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    if-nez v0, :cond_0

    iget-object v0, p0, Lorg/apache/fontbox/cff/CFFFont;->charStrings:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    :cond_0
    new-instance v1, Lorg/apache/fontbox/cff/Type2CharStringParser;

    iget-object v2, p0, Lorg/apache/fontbox/cff/CFFFont;->fontName:Ljava/lang/String;

    invoke-direct {v1, v2, p1}, Lorg/apache/fontbox/cff/Type2CharStringParser;-><init>(Ljava/lang/String;I)V

    iget-object v2, p0, Lorg/apache/fontbox/cff/CFFFont;->globalSubrIndex:Lorg/apache/fontbox/cff/IndexData;

    invoke-direct {p0, v5}, Lorg/apache/fontbox/cff/CFFCIDFont;->getLocalSubrIndex(I)Lorg/apache/fontbox/cff/IndexData;

    move-result-object v3

    invoke-virtual {v1, v0, v2, v3}, Lorg/apache/fontbox/cff/Type2CharStringParser;->parse([BLorg/apache/fontbox/cff/IndexData;Lorg/apache/fontbox/cff/IndexData;)Ljava/util/List;

    move-result-object v6

    new-instance v0, Lorg/apache/fontbox/cff/CIDKeyedType2CharString;

    iget-object v2, p0, Lorg/apache/fontbox/cff/CFFCIDFont;->reader:Lorg/apache/fontbox/cff/CFFCIDFont$PrivateType1CharStringReader;

    iget-object v3, p0, Lorg/apache/fontbox/cff/CFFFont;->fontName:Ljava/lang/String;

    invoke-direct {p0, p1}, Lorg/apache/fontbox/cff/CFFCIDFont;->getDefaultWidthX(I)I

    move-result v7

    invoke-direct {p0, p1}, Lorg/apache/fontbox/cff/CFFCIDFont;->getNominalWidthX(I)I

    move-result v8

    move-object v1, v0

    move v4, p1

    invoke-direct/range {v1 .. v8}, Lorg/apache/fontbox/cff/CIDKeyedType2CharString;-><init>(Lorg/apache/fontbox/type1/Type1CharStringReader;Ljava/lang/String;IILjava/util/List;II)V

    iget-object v1, p0, Lorg/apache/fontbox/cff/CFFCIDFont;->charStringCache:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    return-object v0
.end method

.method public setFdSelect(Lorg/apache/fontbox/cff/FDSelect;)V
    .locals 0

    iput-object p1, p0, Lorg/apache/fontbox/cff/CFFCIDFont;->fdSelect:Lorg/apache/fontbox/cff/FDSelect;

    return-void
.end method

.method public setFontDict(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;>;)V"
        }
    .end annotation

    iput-object p1, p0, Lorg/apache/fontbox/cff/CFFCIDFont;->fontDictionaries:Ljava/util/List;

    return-void
.end method

.method public setOrdering(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lorg/apache/fontbox/cff/CFFCIDFont;->ordering:Ljava/lang/String;

    return-void
.end method

.method public setPrivDict(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;>;)V"
        }
    .end annotation

    iput-object p1, p0, Lorg/apache/fontbox/cff/CFFCIDFont;->privateDictionaries:Ljava/util/List;

    return-void
.end method

.method public setRegistry(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lorg/apache/fontbox/cff/CFFCIDFont;->registry:Ljava/lang/String;

    return-void
.end method

.method public setSupplement(I)V
    .locals 0

    iput p1, p0, Lorg/apache/fontbox/cff/CFFCIDFont;->supplement:I

    return-void
.end method

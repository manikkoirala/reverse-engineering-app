.class Lorg/apache/fontbox/cff/CFFParser$Format2Charset$Range2;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/fontbox/cff/CFFParser$Format2Charset;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Range2"
.end annotation


# instance fields
.field private first:I

.field private nLeft:I


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public synthetic constructor <init>(Lorg/apache/fontbox/cff/CFFParser$1;)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lorg/apache/fontbox/cff/CFFParser$Format2Charset$Range2;-><init>()V

    return-void
.end method

.method public static synthetic access$4700(Lorg/apache/fontbox/cff/CFFParser$Format2Charset$Range2;)I
    .locals 0

    iget p0, p0, Lorg/apache/fontbox/cff/CFFParser$Format2Charset$Range2;->first:I

    return p0
.end method

.method public static synthetic access$4702(Lorg/apache/fontbox/cff/CFFParser$Format2Charset$Range2;I)I
    .locals 0

    iput p1, p0, Lorg/apache/fontbox/cff/CFFParser$Format2Charset$Range2;->first:I

    return p1
.end method

.method public static synthetic access$4800(Lorg/apache/fontbox/cff/CFFParser$Format2Charset$Range2;)I
    .locals 0

    iget p0, p0, Lorg/apache/fontbox/cff/CFFParser$Format2Charset$Range2;->nLeft:I

    return p0
.end method

.method public static synthetic access$4802(Lorg/apache/fontbox/cff/CFFParser$Format2Charset$Range2;I)I
    .locals 0

    iput p1, p0, Lorg/apache/fontbox/cff/CFFParser$Format2Charset$Range2;->nLeft:I

    return p1
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "[first="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lorg/apache/fontbox/cff/CFFParser$Format2Charset$Range2;->first:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, ", nLeft="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, p0, Lorg/apache/fontbox/cff/CFFParser$Format2Charset$Range2;->nLeft:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.class public final Lorg/apache/fontbox/cff/CFFISOAdobeCharset;
.super Lorg/apache/fontbox/cff/CFFCharset;
.source "SourceFile"


# static fields
.field private static final INSTANCE:Lorg/apache/fontbox/cff/CFFISOAdobeCharset;


# direct methods
.method public static constructor <clinit>()V
    .locals 4

    new-instance v0, Lorg/apache/fontbox/cff/CFFISOAdobeCharset;

    invoke-direct {v0}, Lorg/apache/fontbox/cff/CFFISOAdobeCharset;-><init>()V

    sput-object v0, Lorg/apache/fontbox/cff/CFFISOAdobeCharset;->INSTANCE:Lorg/apache/fontbox/cff/CFFISOAdobeCharset;

    const-string v1, ".notdef"

    const/4 v2, 0x0

    invoke-virtual {v0, v2, v2, v1}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const-string v1, "space"

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v2, v1}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const-string v1, "exclam"

    const/4 v2, 0x2

    invoke-virtual {v0, v2, v2, v1}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const-string v1, "quotedbl"

    const/4 v2, 0x3

    invoke-virtual {v0, v2, v2, v1}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const-string v1, "numbersign"

    const/4 v2, 0x4

    invoke-virtual {v0, v2, v2, v1}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const-string v1, "dollar"

    const/4 v2, 0x5

    invoke-virtual {v0, v2, v2, v1}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const-string v1, "percent"

    const/4 v2, 0x6

    invoke-virtual {v0, v2, v2, v1}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const-string v1, "ampersand"

    const/4 v2, 0x7

    invoke-virtual {v0, v2, v2, v1}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const-string v1, "quoteright"

    const/16 v2, 0x8

    invoke-virtual {v0, v2, v2, v1}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const-string v1, "parenleft"

    const/16 v2, 0x9

    invoke-virtual {v0, v2, v2, v1}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const-string v1, "parenright"

    const/16 v2, 0xa

    invoke-virtual {v0, v2, v2, v1}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const-string v1, "asterisk"

    const/16 v2, 0xb

    invoke-virtual {v0, v2, v2, v1}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const-string v1, "plus"

    const/16 v2, 0xc

    invoke-virtual {v0, v2, v2, v1}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const-string v1, "comma"

    const/16 v2, 0xd

    invoke-virtual {v0, v2, v2, v1}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const-string v1, "hyphen"

    const/16 v2, 0xe

    invoke-virtual {v0, v2, v2, v1}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const-string v1, "period"

    const/16 v2, 0xf

    invoke-virtual {v0, v2, v2, v1}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const-string v1, "slash"

    const/16 v2, 0x10

    invoke-virtual {v0, v2, v2, v1}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const-string v1, "zero"

    const/16 v2, 0x11

    invoke-virtual {v0, v2, v2, v1}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const-string v1, "one"

    const/16 v2, 0x12

    invoke-virtual {v0, v2, v2, v1}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const-string v1, "two"

    const/16 v2, 0x13

    invoke-virtual {v0, v2, v2, v1}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const-string v1, "three"

    const/16 v2, 0x14

    invoke-virtual {v0, v2, v2, v1}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const-string v1, "four"

    const/16 v2, 0x15

    invoke-virtual {v0, v2, v2, v1}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x16

    const-string v2, "five"

    const/16 v3, 0x16

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x17

    const-string v2, "six"

    const/16 v3, 0x17

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x18

    const-string v2, "seven"

    const/16 v3, 0x18

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x19

    const-string v2, "eight"

    const/16 v3, 0x19

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x1a

    const-string v2, "nine"

    const/16 v3, 0x1a

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x1b

    const-string v2, "colon"

    const/16 v3, 0x1b

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x1c

    const-string v2, "semicolon"

    const/16 v3, 0x1c

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x1d

    const-string v2, "less"

    const/16 v3, 0x1d

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x1e

    const-string v2, "equal"

    const/16 v3, 0x1e

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x1f

    const-string v2, "greater"

    const/16 v3, 0x1f

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x20

    const-string v2, "question"

    const/16 v3, 0x20

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x21

    const-string v2, "at"

    const/16 v3, 0x21

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x22

    const-string v2, "A"

    const/16 v3, 0x22

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x23

    const-string v2, "B"

    const/16 v3, 0x23

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x24

    const-string v2, "C"

    const/16 v3, 0x24

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x25

    const-string v2, "D"

    const/16 v3, 0x25

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x26

    const-string v2, "E"

    const/16 v3, 0x26

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x27

    const-string v2, "F"

    const/16 v3, 0x27

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x28

    const-string v2, "G"

    const/16 v3, 0x28

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x29

    const-string v2, "H"

    const/16 v3, 0x29

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x2a

    const-string v2, "I"

    const/16 v3, 0x2a

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x2b

    const-string v2, "J"

    const/16 v3, 0x2b

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x2c

    const-string v2, "K"

    const/16 v3, 0x2c

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x2d

    const-string v2, "L"

    const/16 v3, 0x2d

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x2e

    const-string v2, "M"

    const/16 v3, 0x2e

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x2f

    const-string v2, "N"

    const/16 v3, 0x2f

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x30

    const-string v2, "O"

    const/16 v3, 0x30

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x31

    const-string v2, "P"

    const/16 v3, 0x31

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x32

    const-string v2, "Q"

    const/16 v3, 0x32

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x33

    const-string v2, "R"

    const/16 v3, 0x33

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x34

    const-string v2, "S"

    const/16 v3, 0x34

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x35

    const-string v2, "T"

    const/16 v3, 0x35

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x36

    const-string v2, "U"

    const/16 v3, 0x36

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x37

    const-string v2, "V"

    const/16 v3, 0x37

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x38

    const-string v2, "W"

    const/16 v3, 0x38

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x39

    const-string v2, "X"

    const/16 v3, 0x39

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x3a

    const-string v2, "Y"

    const/16 v3, 0x3a

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x3b

    const-string v2, "Z"

    const/16 v3, 0x3b

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x3c

    const-string v2, "bracketleft"

    const/16 v3, 0x3c

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x3d

    const-string v2, "backslash"

    const/16 v3, 0x3d

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x3e

    const-string v2, "bracketright"

    const/16 v3, 0x3e

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x3f

    const-string v2, "asciicircum"

    const/16 v3, 0x3f

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x40

    const-string v2, "underscore"

    const/16 v3, 0x40

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x41

    const-string v2, "quoteleft"

    const/16 v3, 0x41

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x42

    const-string v2, "a"

    const/16 v3, 0x42

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x43

    const-string v2, "b"

    const/16 v3, 0x43

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x44

    const-string v2, "c"

    const/16 v3, 0x44

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x45

    const-string v2, "d"

    const/16 v3, 0x45

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x46

    const-string v2, "e"

    const/16 v3, 0x46

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x47

    const-string v2, "f"

    const/16 v3, 0x47

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x48

    const-string v2, "g"

    const/16 v3, 0x48

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x49

    const-string v2, "h"

    const/16 v3, 0x49

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x4a

    const-string v2, "i"

    const/16 v3, 0x4a

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x4b

    const-string v2, "j"

    const/16 v3, 0x4b

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x4c

    const-string v2, "k"

    const/16 v3, 0x4c

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x4d

    const-string v2, "l"

    const/16 v3, 0x4d

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x4e

    const-string v2, "m"

    const/16 v3, 0x4e

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x4f

    const-string v2, "n"

    const/16 v3, 0x4f

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x50

    const-string v2, "o"

    const/16 v3, 0x50

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x51

    const-string v2, "p"

    const/16 v3, 0x51

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x52

    const-string v2, "q"

    const/16 v3, 0x52

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x53

    const-string v2, "r"

    const/16 v3, 0x53

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x54

    const-string v2, "s"

    const/16 v3, 0x54

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x55

    const-string v2, "t"

    const/16 v3, 0x55

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x56

    const-string v2, "u"

    const/16 v3, 0x56

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x57

    const-string v2, "v"

    const/16 v3, 0x57

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x58

    const-string v2, "w"

    const/16 v3, 0x58

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x59

    const-string v2, "x"

    const/16 v3, 0x59

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x5a

    const-string v2, "y"

    const/16 v3, 0x5a

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x5b

    const-string v2, "z"

    const/16 v3, 0x5b

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x5c

    const-string v2, "braceleft"

    const/16 v3, 0x5c

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x5d

    const-string v2, "bar"

    const/16 v3, 0x5d

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x5e

    const-string v2, "braceright"

    const/16 v3, 0x5e

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x5f

    const-string v2, "asciitilde"

    const/16 v3, 0x5f

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x60

    const-string v2, "exclamdown"

    const/16 v3, 0x60

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x61

    const-string v2, "cent"

    const/16 v3, 0x61

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x62

    const-string v2, "sterling"

    const/16 v3, 0x62

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x63

    const-string v2, "fraction"

    const/16 v3, 0x63

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x64

    const-string v2, "yen"

    const/16 v3, 0x64

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x65

    const-string v2, "florin"

    const/16 v3, 0x65

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x66

    const-string v2, "section"

    const/16 v3, 0x66

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x67

    const-string v2, "currency"

    const/16 v3, 0x67

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x68

    const-string v2, "quotesingle"

    const/16 v3, 0x68

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x69

    const-string v2, "quotedblleft"

    const/16 v3, 0x69

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x6a

    const-string v2, "guillemotleft"

    const/16 v3, 0x6a

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x6b

    const-string v2, "guilsinglleft"

    const/16 v3, 0x6b

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x6c

    const-string v2, "guilsinglright"

    const/16 v3, 0x6c

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x6d

    const-string v2, "fi"

    const/16 v3, 0x6d

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x6e

    const-string v2, "fl"

    const/16 v3, 0x6e

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x6f

    const-string v2, "endash"

    const/16 v3, 0x6f

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x70

    const-string v2, "dagger"

    const/16 v3, 0x70

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x71

    const-string v2, "daggerdbl"

    const/16 v3, 0x71

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x72

    const-string v2, "periodcentered"

    const/16 v3, 0x72

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x73

    const-string v2, "paragraph"

    const/16 v3, 0x73

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x74

    const-string v2, "bullet"

    const/16 v3, 0x74

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x75

    const-string v2, "quotesinglbase"

    const/16 v3, 0x75

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x76

    const-string v2, "quotedblbase"

    const/16 v3, 0x76

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x77

    const-string v2, "quotedblright"

    const/16 v3, 0x77

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x78

    const-string v2, "guillemotright"

    const/16 v3, 0x78

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x79

    const-string v2, "ellipsis"

    const/16 v3, 0x79

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x7a

    const-string v2, "perthousand"

    const/16 v3, 0x7a

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x7b

    const-string v2, "questiondown"

    const/16 v3, 0x7b

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x7c

    const-string v2, "grave"

    const/16 v3, 0x7c

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x7d

    const-string v2, "acute"

    const/16 v3, 0x7d

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x7e

    const-string v2, "circumflex"

    const/16 v3, 0x7e

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x7f

    const-string v2, "tilde"

    const/16 v3, 0x7f

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x80

    const-string v2, "macron"

    const/16 v3, 0x80

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x81

    const-string v2, "breve"

    const/16 v3, 0x81

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x82

    const-string v2, "dotaccent"

    const/16 v3, 0x82

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x83

    const-string v2, "dieresis"

    const/16 v3, 0x83

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x84

    const-string v2, "ring"

    const/16 v3, 0x84

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x85

    const-string v2, "cedilla"

    const/16 v3, 0x85

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x86

    const-string v2, "hungarumlaut"

    const/16 v3, 0x86

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x87

    const-string v2, "ogonek"

    const/16 v3, 0x87

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x88

    const-string v2, "caron"

    const/16 v3, 0x88

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x89

    const-string v2, "emdash"

    const/16 v3, 0x89

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x8a

    const-string v2, "AE"

    const/16 v3, 0x8a

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x8b

    const-string v2, "ordfeminine"

    const/16 v3, 0x8b

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x8c

    const-string v2, "Lslash"

    const/16 v3, 0x8c

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x8d

    const-string v2, "Oslash"

    const/16 v3, 0x8d

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x8e

    const-string v2, "OE"

    const/16 v3, 0x8e

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x8f

    const-string v2, "ordmasculine"

    const/16 v3, 0x8f

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x90

    const-string v2, "ae"

    const/16 v3, 0x90

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x91

    const-string v2, "dotlessi"

    const/16 v3, 0x91

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x92

    const-string v2, "lslash"

    const/16 v3, 0x92

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x93

    const-string v2, "oslash"

    const/16 v3, 0x93

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x94

    const-string v2, "oe"

    const/16 v3, 0x94

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x95

    const-string v2, "germandbls"

    const/16 v3, 0x95

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x96

    const-string v2, "onesuperior"

    const/16 v3, 0x96

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x97

    const-string v2, "logicalnot"

    const/16 v3, 0x97

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x98

    const-string v2, "mu"

    const/16 v3, 0x98

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x99

    const-string v2, "trademark"

    const/16 v3, 0x99

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x9a

    const-string v2, "Eth"

    const/16 v3, 0x9a

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x9b

    const-string v2, "onehalf"

    const/16 v3, 0x9b

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x9c

    const-string v2, "plusminus"

    const/16 v3, 0x9c

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x9d

    const-string v2, "Thorn"

    const/16 v3, 0x9d

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x9e

    const-string v2, "onequarter"

    const/16 v3, 0x9e

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x9f

    const-string v2, "divide"

    const/16 v3, 0x9f

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0xa0

    const-string v2, "brokenbar"

    const/16 v3, 0xa0

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0xa1

    const-string v2, "degree"

    const/16 v3, 0xa1

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0xa2

    const-string v2, "thorn"

    const/16 v3, 0xa2

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0xa3

    const-string v2, "threequarters"

    const/16 v3, 0xa3

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0xa4

    const-string v2, "twosuperior"

    const/16 v3, 0xa4

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0xa5

    const-string v2, "registered"

    const/16 v3, 0xa5

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0xa6

    const-string v2, "minus"

    const/16 v3, 0xa6

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0xa7

    const-string v2, "eth"

    const/16 v3, 0xa7

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0xa8

    const-string v2, "multiply"

    const/16 v3, 0xa8

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0xa9

    const-string v2, "threesuperior"

    const/16 v3, 0xa9

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0xaa

    const-string v2, "copyright"

    const/16 v3, 0xaa

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0xab

    const-string v2, "Aacute"

    const/16 v3, 0xab

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0xac

    const-string v2, "Acircumflex"

    const/16 v3, 0xac

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0xad

    const-string v2, "Adieresis"

    const/16 v3, 0xad

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0xae

    const-string v2, "Agrave"

    const/16 v3, 0xae

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0xaf

    const-string v2, "Aring"

    const/16 v3, 0xaf

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0xb0

    const-string v2, "Atilde"

    const/16 v3, 0xb0

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0xb1

    const-string v2, "Ccedilla"

    const/16 v3, 0xb1

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0xb2

    const-string v2, "Eacute"

    const/16 v3, 0xb2

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0xb3

    const-string v2, "Ecircumflex"

    const/16 v3, 0xb3

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0xb4

    const-string v2, "Edieresis"

    const/16 v3, 0xb4

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0xb5

    const-string v2, "Egrave"

    const/16 v3, 0xb5

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0xb6

    const-string v2, "Iacute"

    const/16 v3, 0xb6

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0xb7

    const-string v2, "Icircumflex"

    const/16 v3, 0xb7

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0xb8

    const-string v2, "Idieresis"

    const/16 v3, 0xb8

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0xb9

    const-string v2, "Igrave"

    const/16 v3, 0xb9

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0xba

    const-string v2, "Ntilde"

    const/16 v3, 0xba

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0xbb

    const-string v2, "Oacute"

    const/16 v3, 0xbb

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0xbc

    const-string v2, "Ocircumflex"

    const/16 v3, 0xbc

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0xbd

    const-string v2, "Odieresis"

    const/16 v3, 0xbd

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0xbe

    const-string v2, "Ograve"

    const/16 v3, 0xbe

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0xbf

    const-string v2, "Otilde"

    const/16 v3, 0xbf

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0xc0

    const-string v2, "Scaron"

    const/16 v3, 0xc0

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0xc1

    const-string v2, "Uacute"

    const/16 v3, 0xc1

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0xc2

    const-string v2, "Ucircumflex"

    const/16 v3, 0xc2

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0xc3

    const-string v2, "Udieresis"

    const/16 v3, 0xc3

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0xc4

    const-string v2, "Ugrave"

    const/16 v3, 0xc4

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0xc5

    const-string v2, "Yacute"

    const/16 v3, 0xc5

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0xc6

    const-string v2, "Ydieresis"

    const/16 v3, 0xc6

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0xc7

    const-string v2, "Zcaron"

    const/16 v3, 0xc7

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0xc8

    const-string v2, "aacute"

    const/16 v3, 0xc8

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0xc9

    const-string v2, "acircumflex"

    const/16 v3, 0xc9

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0xca

    const-string v2, "adieresis"

    const/16 v3, 0xca

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0xcb

    const-string v2, "agrave"

    const/16 v3, 0xcb

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0xcc

    const-string v2, "aring"

    const/16 v3, 0xcc

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0xcd

    const-string v2, "atilde"

    const/16 v3, 0xcd

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0xce

    const-string v2, "ccedilla"

    const/16 v3, 0xce

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0xcf

    const-string v2, "eacute"

    const/16 v3, 0xcf

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0xd0

    const-string v2, "ecircumflex"

    const/16 v3, 0xd0

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0xd1

    const-string v2, "edieresis"

    const/16 v3, 0xd1

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0xd2

    const-string v2, "egrave"

    const/16 v3, 0xd2

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0xd3

    const-string v2, "iacute"

    const/16 v3, 0xd3

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0xd4

    const-string v2, "icircumflex"

    const/16 v3, 0xd4

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0xd5

    const-string v2, "idieresis"

    const/16 v3, 0xd5

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0xd6

    const-string v2, "igrave"

    const/16 v3, 0xd6

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0xd7

    const-string v2, "ntilde"

    const/16 v3, 0xd7

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0xd8

    const-string v2, "oacute"

    const/16 v3, 0xd8

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0xd9

    const-string v2, "ocircumflex"

    const/16 v3, 0xd9

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0xda

    const-string v2, "odieresis"

    const/16 v3, 0xda

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0xdb

    const-string v2, "ograve"

    const/16 v3, 0xdb

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0xdc

    const-string v2, "otilde"

    const/16 v3, 0xdc

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0xdd

    const-string v2, "scaron"

    const/16 v3, 0xdd

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0xde

    const-string v2, "uacute"

    const/16 v3, 0xde

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0xdf

    const-string v2, "ucircumflex"

    const/16 v3, 0xdf

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0xe0

    const-string v2, "udieresis"

    const/16 v3, 0xe0

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0xe1

    const-string v2, "ugrave"

    const/16 v3, 0xe1

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0xe2

    const-string v2, "yacute"

    const/16 v3, 0xe2

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0xe3

    const-string v2, "ydieresis"

    const/16 v3, 0xe3

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0xe4

    const-string v2, "zcaron"

    const/16 v3, 0xe4

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lorg/apache/fontbox/cff/CFFCharset;-><init>(Z)V

    return-void
.end method

.method public static getInstance()Lorg/apache/fontbox/cff/CFFISOAdobeCharset;
    .locals 1

    sget-object v0, Lorg/apache/fontbox/cff/CFFISOAdobeCharset;->INSTANCE:Lorg/apache/fontbox/cff/CFFISOAdobeCharset;

    return-object v0
.end method

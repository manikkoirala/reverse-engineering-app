.class public final Lorg/apache/fontbox/cff/CFFStandardEncoding;
.super Lorg/apache/fontbox/cff/CFFEncoding;
.source "SourceFile"


# static fields
.field private static final INSTANCE:Lorg/apache/fontbox/cff/CFFStandardEncoding;


# direct methods
.method public static constructor <clinit>()V
    .locals 16

    new-instance v0, Lorg/apache/fontbox/cff/CFFStandardEncoding;

    invoke-direct {v0}, Lorg/apache/fontbox/cff/CFFStandardEncoding;-><init>()V

    sput-object v0, Lorg/apache/fontbox/cff/CFFStandardEncoding;->INSTANCE:Lorg/apache/fontbox/cff/CFFStandardEncoding;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/4 v3, 0x2

    invoke-virtual {v0, v3, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/4 v4, 0x3

    invoke-virtual {v0, v4, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/4 v5, 0x4

    invoke-virtual {v0, v5, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/4 v6, 0x5

    invoke-virtual {v0, v6, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/4 v7, 0x6

    invoke-virtual {v0, v7, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/4 v8, 0x7

    invoke-virtual {v0, v8, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v9, 0x8

    invoke-virtual {v0, v9, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v10, 0x9

    invoke-virtual {v0, v10, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v11, 0xa

    invoke-virtual {v0, v11, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v12, 0xb

    invoke-virtual {v0, v12, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v13, 0xc

    invoke-virtual {v0, v13, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v14, 0xd

    invoke-virtual {v0, v14, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v15, 0xe

    invoke-virtual {v0, v15, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v15, 0xf

    invoke-virtual {v0, v15, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v15, 0x10

    invoke-virtual {v0, v15, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v15, 0x11

    invoke-virtual {v0, v15, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v15, 0x12

    invoke-virtual {v0, v15, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v15, 0x13

    invoke-virtual {v0, v15, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v15, 0x14

    invoke-virtual {v0, v15, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v15, 0x15

    invoke-virtual {v0, v15, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v15, 0x16

    invoke-virtual {v0, v15, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v15, 0x17

    invoke-virtual {v0, v15, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v15, 0x18

    invoke-virtual {v0, v15, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v15, 0x19

    invoke-virtual {v0, v15, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v15, 0x1a

    invoke-virtual {v0, v15, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v15, 0x1b

    invoke-virtual {v0, v15, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v15, 0x1c

    invoke-virtual {v0, v15, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v15, 0x1d

    invoke-virtual {v0, v15, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v15, 0x1e

    invoke-virtual {v0, v15, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v15, 0x1f

    invoke-virtual {v0, v15, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v15, 0x20

    invoke-virtual {v0, v15, v2}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x21

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x22

    invoke-virtual {v0, v2, v4}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x23

    invoke-virtual {v0, v2, v5}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x24

    invoke-virtual {v0, v2, v6}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x25

    invoke-virtual {v0, v2, v7}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x26

    invoke-virtual {v0, v2, v8}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x27

    invoke-virtual {v0, v2, v9}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x28

    invoke-virtual {v0, v2, v10}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x29

    invoke-virtual {v0, v2, v11}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x2a

    invoke-virtual {v0, v2, v12}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x2b

    invoke-virtual {v0, v2, v13}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x2c

    invoke-virtual {v0, v2, v14}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x2d

    const/16 v3, 0xe

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x2e

    const/16 v3, 0xf

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x2f

    const/16 v3, 0x10

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x30

    const/16 v3, 0x11

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x31

    const/16 v3, 0x12

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x32

    const/16 v3, 0x13

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x33

    const/16 v3, 0x14

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x34

    const/16 v3, 0x15

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x35

    const/16 v3, 0x16

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x36

    const/16 v3, 0x17

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x37

    const/16 v3, 0x18

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x38

    const/16 v3, 0x19

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x39

    const/16 v3, 0x1a

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x3a

    const/16 v3, 0x1b

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x3b

    const/16 v3, 0x1c

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x3c

    const/16 v3, 0x1d

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x3d

    const/16 v3, 0x1e

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x3e

    const/16 v3, 0x1f

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x3f

    const/16 v3, 0x20

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x40

    const/16 v3, 0x21

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x41

    const/16 v3, 0x22

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x42

    const/16 v3, 0x23

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x43

    const/16 v3, 0x24

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x44

    const/16 v3, 0x25

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x45

    const/16 v3, 0x26

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x46

    const/16 v3, 0x27

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x47

    const/16 v3, 0x28

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x48

    const/16 v3, 0x29

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x49

    const/16 v3, 0x2a

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x4a

    const/16 v3, 0x2b

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x4b

    const/16 v3, 0x2c

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x4c

    const/16 v3, 0x2d

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x4d

    const/16 v3, 0x2e

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x4e

    const/16 v3, 0x2f

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x4f

    const/16 v3, 0x30

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x50

    const/16 v3, 0x31

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x51

    const/16 v3, 0x32

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x52

    const/16 v3, 0x33

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x53

    const/16 v3, 0x34

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x54

    const/16 v3, 0x35

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x55

    const/16 v3, 0x36

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x56

    const/16 v3, 0x37

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x57

    const/16 v3, 0x38

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x58

    const/16 v3, 0x39

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x59

    const/16 v3, 0x3a

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x5a

    const/16 v3, 0x3b

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x5b

    const/16 v3, 0x3c

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x5c

    const/16 v3, 0x3d

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x5d

    const/16 v3, 0x3e

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x5e

    const/16 v3, 0x3f

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x5f

    const/16 v3, 0x40

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x60

    const/16 v3, 0x41

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x61

    const/16 v3, 0x42

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x62

    const/16 v3, 0x43

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x63

    const/16 v3, 0x44

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x64

    const/16 v3, 0x45

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x65

    const/16 v3, 0x46

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x66

    const/16 v3, 0x47

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x67

    const/16 v3, 0x48

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x68

    const/16 v3, 0x49

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x69

    const/16 v3, 0x4a

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x6a

    const/16 v3, 0x4b

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x6b

    const/16 v3, 0x4c

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x6c

    const/16 v3, 0x4d

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x6d

    const/16 v3, 0x4e

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x6e

    const/16 v3, 0x4f

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x6f

    const/16 v3, 0x50

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x70

    const/16 v3, 0x51

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x71

    const/16 v3, 0x52

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x72

    const/16 v3, 0x53

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x73

    const/16 v3, 0x54

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x74

    const/16 v3, 0x55

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x75

    const/16 v3, 0x56

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x76

    const/16 v3, 0x57

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x77

    const/16 v3, 0x58

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x78

    const/16 v3, 0x59

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x79

    const/16 v3, 0x5a

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x7a

    const/16 v3, 0x5b

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x7b

    const/16 v3, 0x5c

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x7c

    const/16 v3, 0x5d

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x7d

    const/16 v3, 0x5e

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x7e

    const/16 v3, 0x5f

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x7f

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x80

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x81

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x82

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x83

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x84

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x85

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x86

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x87

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x88

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x89

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x8a

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x8b

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x8c

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x8d

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x8e

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x8f

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x90

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x91

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x92

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x93

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x94

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x95

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x96

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x97

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x98

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x99

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x9a

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x9b

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x9c

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x9d

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x9e

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x9f

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xa0

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xa1

    const/16 v3, 0x60

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xa2

    const/16 v3, 0x61

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xa3

    const/16 v3, 0x62

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xa4

    const/16 v3, 0x63

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xa5

    const/16 v3, 0x64

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xa6

    const/16 v3, 0x65

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xa7

    const/16 v3, 0x66

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xa8

    const/16 v3, 0x67

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xa9

    const/16 v3, 0x68

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xaa

    const/16 v3, 0x69

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xab

    const/16 v3, 0x6a

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xac

    const/16 v3, 0x6b

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xad

    const/16 v3, 0x6c

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xae

    const/16 v3, 0x6d

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xaf

    const/16 v3, 0x6e

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xb0

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xb1

    const/16 v3, 0x6f

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xb2

    const/16 v3, 0x70

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xb3

    const/16 v3, 0x71

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xb4

    const/16 v3, 0x72

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xb5

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xb6

    const/16 v3, 0x73

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xb7

    const/16 v3, 0x74

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xb8

    const/16 v3, 0x75

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xb9

    const/16 v3, 0x76

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xba

    const/16 v3, 0x77

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xbb

    const/16 v3, 0x78

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xbc

    const/16 v3, 0x79

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xbd

    const/16 v3, 0x7a

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xbe

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xbf

    const/16 v3, 0x7b

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xc0

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xc1

    const/16 v3, 0x7c

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xc2

    const/16 v3, 0x7d

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xc3

    const/16 v3, 0x7e

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xc4

    const/16 v3, 0x7f

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xc5

    const/16 v3, 0x80

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xc6

    const/16 v3, 0x81

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xc7

    const/16 v3, 0x82

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xc8

    const/16 v3, 0x83

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xc9

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xca

    const/16 v3, 0x84

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xcb

    const/16 v3, 0x85

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xcc

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xcd

    const/16 v3, 0x86

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xce

    const/16 v3, 0x87

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xcf

    const/16 v3, 0x88

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xd0

    const/16 v3, 0x89

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xd1

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xd2

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xd3

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xd4

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xd5

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xd6

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xd7

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xd8

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xd9

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xda

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xdb

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xdc

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xdd

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xde

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xdf

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xe0

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xe1

    const/16 v3, 0x8a

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xe2

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xe3

    const/16 v3, 0x8b

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xe4

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xe5

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xe6

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xe7

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xe8

    const/16 v3, 0x8c

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xe9

    const/16 v3, 0x8d

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xea

    const/16 v3, 0x8e

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xeb

    const/16 v3, 0x8f

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xec

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xed

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xee

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xef

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xf0

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xf1

    const/16 v3, 0x90

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xf2

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xf3

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xf4

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xf5

    const/16 v3, 0x91

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xf6

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xf7

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xf8

    const/16 v3, 0x92

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xf9

    const/16 v3, 0x93

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xfa

    const/16 v3, 0x94

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xfb

    const/16 v3, 0x95

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xfc

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xfd

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xfe

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xff

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lorg/apache/fontbox/cff/CFFEncoding;-><init>()V

    return-void
.end method

.method public static getInstance()Lorg/apache/fontbox/cff/CFFStandardEncoding;
    .locals 1

    sget-object v0, Lorg/apache/fontbox/cff/CFFStandardEncoding;->INSTANCE:Lorg/apache/fontbox/cff/CFFStandardEncoding;

    return-object v0
.end method

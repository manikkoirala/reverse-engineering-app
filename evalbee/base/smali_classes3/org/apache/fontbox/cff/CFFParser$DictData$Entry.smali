.class Lorg/apache/fontbox/cff/CFFParser$DictData$Entry;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/fontbox/cff/CFFParser$DictData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Entry"
.end annotation


# instance fields
.field private operands:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Number;",
            ">;"
        }
    .end annotation
.end field

.field private operator:Lorg/apache/fontbox/cff/CFFOperator;


# direct methods
.method private constructor <init>()V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/apache/fontbox/cff/CFFParser$DictData$Entry;->operands:Ljava/util/List;

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/fontbox/cff/CFFParser$DictData$Entry;->operator:Lorg/apache/fontbox/cff/CFFOperator;

    return-void
.end method

.method public synthetic constructor <init>(Lorg/apache/fontbox/cff/CFFParser$1;)V
    .locals 0

    .line 2
    invoke-direct {p0}, Lorg/apache/fontbox/cff/CFFParser$DictData$Entry;-><init>()V

    return-void
.end method

.method public static synthetic access$800(Lorg/apache/fontbox/cff/CFFParser$DictData$Entry;)Lorg/apache/fontbox/cff/CFFOperator;
    .locals 0

    iget-object p0, p0, Lorg/apache/fontbox/cff/CFFParser$DictData$Entry;->operator:Lorg/apache/fontbox/cff/CFFOperator;

    return-object p0
.end method

.method public static synthetic access$802(Lorg/apache/fontbox/cff/CFFParser$DictData$Entry;Lorg/apache/fontbox/cff/CFFOperator;)Lorg/apache/fontbox/cff/CFFOperator;
    .locals 0

    iput-object p1, p0, Lorg/apache/fontbox/cff/CFFParser$DictData$Entry;->operator:Lorg/apache/fontbox/cff/CFFOperator;

    return-object p1
.end method

.method public static synthetic access$900(Lorg/apache/fontbox/cff/CFFParser$DictData$Entry;)Ljava/util/List;
    .locals 0

    iget-object p0, p0, Lorg/apache/fontbox/cff/CFFParser$DictData$Entry;->operands:Ljava/util/List;

    return-object p0
.end method


# virtual methods
.method public getArray()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/Number;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/fontbox/cff/CFFParser$DictData$Entry;->operands:Ljava/util/List;

    return-object v0
.end method

.method public getBoolean(I)Ljava/lang/Boolean;
    .locals 1

    iget-object v0, p0, Lorg/apache/fontbox/cff/CFFParser$DictData$Entry;->operands:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Number;

    instance-of v0, p1, Ljava/lang/Integer;

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Ljava/lang/Number;->intValue()I

    move-result p1

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    sget-object p1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    return-object p1

    :cond_0
    sget-object p1, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    return-object p1

    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    invoke-direct {p1}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw p1
.end method

.method public getDelta()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/Number;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/fontbox/cff/CFFParser$DictData$Entry;->operands:Ljava/util/List;

    return-object v0
.end method

.method public getNumber(I)Ljava/lang/Number;
    .locals 1

    iget-object v0, p0, Lorg/apache/fontbox/cff/CFFParser$DictData$Entry;->operands:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Number;

    return-object p1
.end method

.method public getSID(I)Ljava/lang/Integer;
    .locals 1

    iget-object v0, p0, Lorg/apache/fontbox/cff/CFFParser$DictData$Entry;->operands:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Number;

    instance-of v0, p1, Ljava/lang/Integer;

    if-eqz v0, :cond_0

    check-cast p1, Ljava/lang/Integer;

    return-object p1

    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    invoke-direct {p1}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw p1
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "[operands="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lorg/apache/fontbox/cff/CFFParser$DictData$Entry;->operands:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ", operator="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lorg/apache/fontbox/cff/CFFParser$DictData$Entry;->operator:Lorg/apache/fontbox/cff/CFFOperator;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

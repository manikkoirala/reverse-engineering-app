.class public Lorg/apache/fontbox/cff/CFFDataInput;
.super Lorg/apache/fontbox/cff/DataInput;
.source "SourceFile"


# direct methods
.method public constructor <init>([B)V
    .locals 0

    invoke-direct {p0, p1}, Lorg/apache/fontbox/cff/DataInput;-><init>([B)V

    return-void
.end method


# virtual methods
.method public readCard16()I
    .locals 1

    invoke-virtual {p0}, Lorg/apache/fontbox/cff/DataInput;->readUnsignedShort()I

    move-result v0

    return v0
.end method

.method public readCard8()I
    .locals 1

    invoke-virtual {p0}, Lorg/apache/fontbox/cff/DataInput;->readUnsignedByte()I

    move-result v0

    return v0
.end method

.method public readOffSize()I
    .locals 1

    invoke-virtual {p0}, Lorg/apache/fontbox/cff/DataInput;->readUnsignedByte()I

    move-result v0

    return v0
.end method

.method public readOffset(I)I
    .locals 3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v0, p1, :cond_0

    shl-int/lit8 v1, v1, 0x8

    invoke-virtual {p0}, Lorg/apache/fontbox/cff/DataInput;->readUnsignedByte()I

    move-result v2

    or-int/2addr v1, v2

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return v1
.end method

.method public readSID()I
    .locals 1

    invoke-virtual {p0}, Lorg/apache/fontbox/cff/DataInput;->readUnsignedShort()I

    move-result v0

    return v0
.end method

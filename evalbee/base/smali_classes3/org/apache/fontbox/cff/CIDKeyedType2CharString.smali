.class public Lorg/apache/fontbox/cff/CIDKeyedType2CharString;
.super Lorg/apache/fontbox/cff/Type2CharString;
.source "SourceFile"


# instance fields
.field private final cid:I


# direct methods
.method public constructor <init>(Lorg/apache/fontbox/type1/Type1CharStringReader;Ljava/lang/String;IILjava/util/List;II)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/fontbox/type1/Type1CharStringReader;",
            "Ljava/lang/String;",
            "II",
            "Ljava/util/List<",
            "Ljava/lang/Object;",
            ">;II)V"
        }
    .end annotation

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    filled-new-array {v0}, [Ljava/lang/Object;

    move-result-object v0

    const-string v1, "%04x"

    invoke-static {v1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move v6, p4

    move-object v7, p5

    move/from16 v8, p6

    move/from16 v9, p7

    invoke-direct/range {v2 .. v9}, Lorg/apache/fontbox/cff/Type2CharString;-><init>(Lorg/apache/fontbox/type1/Type1CharStringReader;Ljava/lang/String;Ljava/lang/String;ILjava/util/List;II)V

    move-object v0, p0

    move v1, p3

    iput v1, v0, Lorg/apache/fontbox/cff/CIDKeyedType2CharString;->cid:I

    return-void
.end method


# virtual methods
.method public getCID()I
    .locals 1

    iget v0, p0, Lorg/apache/fontbox/cff/CIDKeyedType2CharString;->cid:I

    return v0
.end method

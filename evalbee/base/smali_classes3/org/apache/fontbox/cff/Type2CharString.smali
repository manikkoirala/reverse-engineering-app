.class public Lorg/apache/fontbox/cff/Type2CharString;
.super Lorg/apache/fontbox/cff/Type1CharString;
.source "SourceFile"


# instance fields
.field private defWidthX:I

.field private final gid:I

.field private nominalWidthX:I

.field private pathCount:I

.field private final type2sequence:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lorg/apache/fontbox/type1/Type1CharStringReader;Ljava/lang/String;Ljava/lang/String;ILjava/util/List;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/fontbox/type1/Type1CharStringReader;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "I",
            "Ljava/util/List<",
            "Ljava/lang/Object;",
            ">;II)V"
        }
    .end annotation

    invoke-direct {p0, p1, p2, p3}, Lorg/apache/fontbox/cff/Type1CharString;-><init>(Lorg/apache/fontbox/type1/Type1CharStringReader;Ljava/lang/String;Ljava/lang/String;)V

    const/4 p1, 0x0

    iput p1, p0, Lorg/apache/fontbox/cff/Type2CharString;->pathCount:I

    iput p4, p0, Lorg/apache/fontbox/cff/Type2CharString;->gid:I

    iput-object p5, p0, Lorg/apache/fontbox/cff/Type2CharString;->type2sequence:Ljava/util/List;

    iput p6, p0, Lorg/apache/fontbox/cff/Type2CharString;->defWidthX:I

    iput p7, p0, Lorg/apache/fontbox/cff/Type2CharString;->nominalWidthX:I

    invoke-direct {p0, p5}, Lorg/apache/fontbox/cff/Type2CharString;->convertType1ToType2(Ljava/util/List;)V

    return-void
.end method

.method public static synthetic access$000(Lorg/apache/fontbox/cff/Type2CharString;Ljava/util/List;Lorg/apache/fontbox/cff/CharStringCommand;)Ljava/util/List;
    .locals 0

    invoke-direct {p0, p1, p2}, Lorg/apache/fontbox/cff/Type2CharString;->handleCommand(Ljava/util/List;Lorg/apache/fontbox/cff/CharStringCommand;)Ljava/util/List;

    move-result-object p0

    return-object p0
.end method

.method private addCommand(Ljava/util/List;Lorg/apache/fontbox/cff/CharStringCommand;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;",
            "Lorg/apache/fontbox/cff/CharStringCommand;",
            ")V"
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/fontbox/cff/Type1CharString;->type1Sequence:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    iget-object p1, p0, Lorg/apache/fontbox/cff/Type1CharString;->type1Sequence:Ljava/util/List;

    invoke-interface {p1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method private addCommandList(Ljava/util/List;Lorg/apache/fontbox/cff/CharStringCommand;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;>;",
            "Lorg/apache/fontbox/cff/CharStringCommand;",
            ")V"
        }
    .end annotation

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-direct {p0, v0, p2}, Lorg/apache/fontbox/cff/Type2CharString;->addCommand(Ljava/util/List;Lorg/apache/fontbox/cff/CharStringCommand;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private clearStack(Ljava/util/List;Z)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;Z)",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/fontbox/cff/Type1CharString;->type1Sequence:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    const/16 v0, 0xd

    const/4 v1, 0x2

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz p2, :cond_0

    new-array p2, v1, [Ljava/lang/Integer;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, p2, v3

    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget v3, p0, Lorg/apache/fontbox/cff/Type2CharString;->nominalWidthX:I

    add-int/2addr v1, v3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, p2, v2

    invoke-static {p2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p2

    new-instance v1, Lorg/apache/fontbox/cff/CharStringCommand;

    invoke-direct {v1, v0}, Lorg/apache/fontbox/cff/CharStringCommand;-><init>(I)V

    invoke-direct {p0, p2, v1}, Lorg/apache/fontbox/cff/Type2CharString;->addCommand(Ljava/util/List;Lorg/apache/fontbox/cff/CharStringCommand;)V

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p2

    invoke-interface {p1, v2, p2}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object p1

    goto :goto_0

    :cond_0
    new-array p2, v1, [Ljava/lang/Integer;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, p2, v3

    iget v1, p0, Lorg/apache/fontbox/cff/Type2CharString;->defWidthX:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, p2, v2

    invoke-static {p2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p2

    new-instance v1, Lorg/apache/fontbox/cff/CharStringCommand;

    invoke-direct {v1, v0}, Lorg/apache/fontbox/cff/CharStringCommand;-><init>(I)V

    invoke-direct {p0, p2, v1}, Lorg/apache/fontbox/cff/Type2CharString;->addCommand(Ljava/util/List;Lorg/apache/fontbox/cff/CharStringCommand;)V

    :cond_1
    :goto_0
    return-object p1
.end method

.method private closePath()V
    .locals 3

    iget v0, p0, Lorg/apache/fontbox/cff/Type2CharString;->pathCount:I

    if-lez v0, :cond_0

    iget-object v0, p0, Lorg/apache/fontbox/cff/Type1CharString;->type1Sequence:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/fontbox/cff/CharStringCommand;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    new-instance v1, Lorg/apache/fontbox/cff/CharStringCommand;

    const/16 v2, 0x9

    invoke-direct {v1, v2}, Lorg/apache/fontbox/cff/CharStringCommand;-><init>(I)V

    if-eqz v0, :cond_1

    invoke-virtual {v1, v0}, Lorg/apache/fontbox/cff/CharStringCommand;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0, v1}, Lorg/apache/fontbox/cff/Type2CharString;->addCommand(Ljava/util/List;Lorg/apache/fontbox/cff/CharStringCommand;)V

    :cond_1
    return-void
.end method

.method private convertType1ToType2(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/apache/fontbox/cff/Type1CharString;->type1Sequence:Ljava/util/List;

    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/fontbox/cff/Type2CharString;->pathCount:I

    new-instance v0, Lorg/apache/fontbox/cff/Type2CharString$1;

    invoke-direct {v0, p0}, Lorg/apache/fontbox/cff/Type2CharString$1;-><init>(Lorg/apache/fontbox/cff/Type2CharString;)V

    invoke-virtual {v0, p1}, Lorg/apache/fontbox/cff/CharStringHandler;->handleSequence(Ljava/util/List;)Ljava/util/List;

    return-void
.end method

.method private drawAlternatingCurve(Ljava/util/List;Z)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;Z)V"
        }
    .end annotation

    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_5

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x5

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-ne v0, v1, :cond_0

    move v0, v2

    goto :goto_1

    :cond_0
    move v0, v3

    :goto_1
    const/16 v4, 0x8

    const/4 v5, 0x6

    const/4 v6, 0x3

    const/4 v7, 0x2

    const/4 v8, 0x4

    new-array v5, v5, [Ljava/lang/Integer;

    if-eqz p2, :cond_2

    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Integer;

    aput-object v9, v5, v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v5, v2

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    aput-object v2, v5, v7

    invoke-interface {p1, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    aput-object v2, v5, v6

    if-eqz v0, :cond_1

    invoke-interface {p1, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v3

    :cond_1
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v5, v8

    invoke-interface {p1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    aput-object v2, v5, v1

    invoke-static {v5}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    new-instance v3, Lorg/apache/fontbox/cff/CharStringCommand;

    invoke-direct {v3, v4}, Lorg/apache/fontbox/cff/CharStringCommand;-><init>(I)V

    goto :goto_2

    :cond_2
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v5, v3

    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Integer;

    aput-object v9, v5, v2

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    aput-object v2, v5, v7

    invoke-interface {p1, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    aput-object v2, v5, v6

    invoke-interface {p1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    aput-object v2, v5, v8

    if-eqz v0, :cond_3

    invoke-interface {p1, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v3

    :cond_3
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v5, v1

    invoke-static {v5}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    new-instance v3, Lorg/apache/fontbox/cff/CharStringCommand;

    invoke-direct {v3, v4}, Lorg/apache/fontbox/cff/CharStringCommand;-><init>(I)V

    :goto_2
    invoke-direct {p0, v2, v3}, Lorg/apache/fontbox/cff/Type2CharString;->addCommand(Ljava/util/List;Lorg/apache/fontbox/cff/CharStringCommand;)V

    if-eqz v0, :cond_4

    goto :goto_3

    :cond_4
    move v1, v8

    :goto_3
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    invoke-interface {p1, v1, v0}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object p1

    xor-int/lit8 p2, p2, 0x1

    goto/16 :goto_0

    :cond_5
    return-void
.end method

.method private drawAlternatingLine(Ljava/util/List;Z)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;Z)V"
        }
    .end annotation

    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_1

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-interface {p1, v0, v1}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    new-instance v2, Lorg/apache/fontbox/cff/CharStringCommand;

    if-eqz p2, :cond_0

    const/4 v3, 0x6

    goto :goto_1

    :cond_0
    const/4 v3, 0x7

    :goto_1
    invoke-direct {v2, v3}, Lorg/apache/fontbox/cff/CharStringCommand;-><init>(I)V

    invoke-direct {p0, v0, v2}, Lorg/apache/fontbox/cff/Type2CharString;->addCommand(Ljava/util/List;Lorg/apache/fontbox/cff/CharStringCommand;)V

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    invoke-interface {p1, v1, v0}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object p1

    xor-int/lit8 p2, p2, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method private drawCurve(Ljava/util/List;Z)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;Z)V"
        }
    .end annotation

    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_b

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x4

    rem-int/2addr v0, v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-ne v0, v2, :cond_0

    move v0, v2

    goto :goto_1

    :cond_0
    move v0, v3

    :goto_1
    const/16 v4, 0x8

    const/4 v5, 0x6

    const/4 v6, 0x5

    const/4 v7, 0x3

    const/4 v8, 0x2

    new-array v5, v5, [Ljava/lang/Integer;

    if-eqz p2, :cond_5

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Integer;

    aput-object v9, v5, v3

    if-eqz v0, :cond_1

    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Integer;

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v9

    goto :goto_2

    :cond_1
    move v9, v3

    :goto_2
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v5, v2

    if-eqz v0, :cond_2

    move v2, v8

    :cond_2
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    aput-object v2, v5, v8

    if-eqz v0, :cond_3

    move v8, v7

    :cond_3
    invoke-interface {p1, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    aput-object v2, v5, v7

    if-eqz v0, :cond_4

    move v7, v1

    :cond_4
    invoke-interface {p1, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    aput-object v2, v5, v1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v5, v6

    invoke-static {v5}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    new-instance v3, Lorg/apache/fontbox/cff/CharStringCommand;

    invoke-direct {v3, v4}, Lorg/apache/fontbox/cff/CharStringCommand;-><init>(I)V

    goto :goto_4

    :cond_5
    if-eqz v0, :cond_6

    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Integer;

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v9

    goto :goto_3

    :cond_6
    move v9, v3

    :goto_3
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v5, v3

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Integer;

    aput-object v9, v5, v2

    if-eqz v0, :cond_7

    move v2, v8

    :cond_7
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    aput-object v2, v5, v8

    if-eqz v0, :cond_8

    move v8, v7

    :cond_8
    invoke-interface {p1, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    aput-object v2, v5, v7

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v5, v1

    if-eqz v0, :cond_9

    move v7, v1

    :cond_9
    invoke-interface {p1, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    aput-object v2, v5, v6

    invoke-static {v5}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    new-instance v3, Lorg/apache/fontbox/cff/CharStringCommand;

    invoke-direct {v3, v4}, Lorg/apache/fontbox/cff/CharStringCommand;-><init>(I)V

    :goto_4
    invoke-direct {p0, v2, v3}, Lorg/apache/fontbox/cff/Type2CharString;->addCommand(Ljava/util/List;Lorg/apache/fontbox/cff/CharStringCommand;)V

    if-eqz v0, :cond_a

    move v1, v6

    :cond_a
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    invoke-interface {p1, v1, v0}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object p1

    goto/16 :goto_0

    :cond_b
    return-void
.end method

.method private expandStemHints(Ljava/util/List;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;Z)V"
        }
    .end annotation

    return-void
.end method

.method private handleCommand(Ljava/util/List;Lorg/apache/fontbox/cff/CharStringCommand;)Ljava/util/List;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;",
            "Lorg/apache/fontbox/cff/CharStringCommand;",
            ")",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    iget v0, p0, Lorg/apache/fontbox/cff/Type1CharString;->commandCount:I

    const/4 v1, 0x1

    add-int/2addr v0, v1

    iput v0, p0, Lorg/apache/fontbox/cff/Type1CharString;->commandCount:I

    sget-object v0, Lorg/apache/fontbox/cff/CharStringCommand;->TYPE2_VOCABULARY:Ljava/util/Map;

    invoke-virtual {p2}, Lorg/apache/fontbox/cff/CharStringCommand;->getKey()Lorg/apache/fontbox/cff/CharStringCommand$Key;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v2, "hstem"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    const/4 v3, 0x2

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    if-eqz v2, :cond_1

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p2

    rem-int/2addr p2, v3

    if-eqz p2, :cond_0

    :goto_0
    move v4, v1

    :cond_0
    invoke-direct {p0, p1, v4}, Lorg/apache/fontbox/cff/Type2CharString;->clearStack(Ljava/util/List;Z)Ljava/util/List;

    move-result-object p1

    invoke-direct {p0, p1, v1}, Lorg/apache/fontbox/cff/Type2CharString;->expandStemHints(Ljava/util/List;Z)V

    goto/16 :goto_c

    :cond_1
    const-string v2, "vstem"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p2

    rem-int/2addr p2, v3

    if-eqz p2, :cond_2

    goto :goto_1

    :cond_2
    move v1, v4

    :goto_1
    invoke-direct {p0, p1, v1}, Lorg/apache/fontbox/cff/Type2CharString;->clearStack(Ljava/util/List;Z)Ljava/util/List;

    move-result-object p1

    :goto_2
    invoke-direct {p0, p1, v4}, Lorg/apache/fontbox/cff/Type2CharString;->expandStemHints(Ljava/util/List;Z)V

    goto/16 :goto_c

    :cond_3
    const-string v2, "vmoveto"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, v1, :cond_4

    goto :goto_3

    :cond_4
    move v1, v4

    :goto_3
    invoke-direct {p0, p1, v1}, Lorg/apache/fontbox/cff/Type2CharString;->clearStack(Ljava/util/List;Z)Ljava/util/List;

    move-result-object p1

    invoke-direct {p0}, Lorg/apache/fontbox/cff/Type2CharString;->markPath()V

    :cond_5
    :goto_4
    invoke-direct {p0, p1, p2}, Lorg/apache/fontbox/cff/Type2CharString;->addCommand(Ljava/util/List;Lorg/apache/fontbox/cff/CharStringCommand;)V

    goto/16 :goto_c

    :cond_6
    const-string v2, "rlineto"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-static {p1, v3}, Lorg/apache/fontbox/cff/Type2CharString;->split(Ljava/util/List;I)Ljava/util/List;

    move-result-object p1

    :goto_5
    invoke-direct {p0, p1, p2}, Lorg/apache/fontbox/cff/Type2CharString;->addCommandList(Ljava/util/List;Lorg/apache/fontbox/cff/CharStringCommand;)V

    goto/16 :goto_c

    :cond_7
    const-string v2, "hlineto"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-direct {p0, p1, v1}, Lorg/apache/fontbox/cff/Type2CharString;->drawAlternatingLine(Ljava/util/List;Z)V

    goto/16 :goto_c

    :cond_8
    const-string v2, "vlineto"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    invoke-direct {p0, p1, v4}, Lorg/apache/fontbox/cff/Type2CharString;->drawAlternatingLine(Ljava/util/List;Z)V

    goto/16 :goto_c

    :cond_9
    const-string v2, "rrcurveto"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    const/4 v6, 0x6

    if-eqz v2, :cond_a

    invoke-static {p1, v6}, Lorg/apache/fontbox/cff/Type2CharString;->split(Ljava/util/List;I)Ljava/util/List;

    move-result-object p1

    goto :goto_5

    :cond_a
    const-string v2, "endchar"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    const/16 v7, 0xc

    const/4 v8, 0x4

    const/4 v9, 0x5

    if-eqz v2, :cond_d

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-eq v0, v9, :cond_c

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v1, :cond_b

    goto :goto_6

    :cond_b
    move v1, v4

    :cond_c
    :goto_6
    invoke-direct {p0, p1, v1}, Lorg/apache/fontbox/cff/Type2CharString;->clearStack(Ljava/util/List;Z)Ljava/util/List;

    move-result-object p1

    invoke-direct {p0}, Lorg/apache/fontbox/cff/Type2CharString;->closePath()V

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v8, :cond_5

    invoke-interface {p1, v4, v5}, Ljava/util/List;->add(ILjava/lang/Object;)V

    new-instance p2, Lorg/apache/fontbox/cff/CharStringCommand;

    invoke-direct {p2, v7, v6}, Lorg/apache/fontbox/cff/CharStringCommand;-><init>(II)V

    goto :goto_4

    :cond_d
    const-string v2, "rmoveto"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_e

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, v3, :cond_4

    goto/16 :goto_3

    :cond_e
    const-string v2, "hmoveto"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_f

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, v1, :cond_4

    goto/16 :goto_3

    :cond_f
    const-string v2, "vhcurveto"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_10

    invoke-direct {p0, p1, v4}, Lorg/apache/fontbox/cff/Type2CharString;->drawAlternatingCurve(Ljava/util/List;Z)V

    goto/16 :goto_c

    :cond_10
    const-string v2, "hvcurveto"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_11

    invoke-direct {p0, p1, v1}, Lorg/apache/fontbox/cff/Type2CharString;->drawAlternatingCurve(Ljava/util/List;Z)V

    goto/16 :goto_c

    :cond_11
    const-string v2, "hflex"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    const/4 v10, 0x3

    const/16 v11, 0x8

    if-eqz v2, :cond_12

    new-array p2, v6, [Ljava/lang/Integer;

    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    aput-object v0, p2, v4

    aput-object v5, p2, v1

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    aput-object v0, p2, v3

    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    aput-object v0, p2, v10

    invoke-interface {p1, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    aput-object v0, p2, v8

    aput-object v5, p2, v9

    invoke-static {p2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p2

    new-array v0, v6, [Ljava/lang/Integer;

    invoke-interface {p1, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    aput-object v2, v0, v4

    aput-object v5, v0, v1

    invoke-interface {p1, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    aput-object v2, v0, v3

    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    neg-int v2, v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v10

    invoke-interface {p1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Integer;

    aput-object p1, v0, v8

    aput-object v5, v0, v9

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    new-array v0, v3, [Ljava/util/List;

    aput-object p2, v0, v4

    aput-object p1, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    new-instance p2, Lorg/apache/fontbox/cff/CharStringCommand;

    invoke-direct {p2, v11}, Lorg/apache/fontbox/cff/CharStringCommand;-><init>(I)V

    goto/16 :goto_5

    :cond_12
    const-string v2, "flex"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_13

    invoke-interface {p1, v4, v6}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object p2

    invoke-interface {p1, v6, v7}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object p1

    new-array v0, v3, [Ljava/util/List;

    aput-object p2, v0, v4

    aput-object p1, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    new-instance p2, Lorg/apache/fontbox/cff/CharStringCommand;

    invoke-direct {p2, v11}, Lorg/apache/fontbox/cff/CharStringCommand;-><init>(I)V

    goto/16 :goto_5

    :cond_13
    const-string v2, "hflex1"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    const/4 v7, 0x7

    if-eqz v2, :cond_14

    new-array p2, v6, [Ljava/lang/Integer;

    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    aput-object v0, p2, v4

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    aput-object v0, p2, v1

    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    aput-object v0, p2, v3

    invoke-interface {p1, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    aput-object v0, p2, v10

    invoke-interface {p1, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    aput-object v0, p2, v8

    aput-object v5, p2, v9

    invoke-static {p2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p2

    new-array v0, v6, [Ljava/lang/Integer;

    invoke-interface {p1, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    aput-object v2, v0, v4

    aput-object v5, v0, v1

    invoke-interface {p1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    aput-object v2, v0, v3

    invoke-interface {p1, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    aput-object v2, v0, v10

    invoke-interface {p1, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Integer;

    aput-object p1, v0, v8

    aput-object v5, v0, v9

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    new-array v0, v3, [Ljava/util/List;

    aput-object p2, v0, v4

    aput-object p1, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    new-instance p2, Lorg/apache/fontbox/cff/CharStringCommand;

    invoke-direct {p2, v11}, Lorg/apache/fontbox/cff/CharStringCommand;-><init>(I)V

    goto/16 :goto_5

    :cond_14
    const-string v2, "flex1"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_18

    move p2, v4

    move v0, p2

    move v2, v0

    :goto_7
    if-ge p2, v9, :cond_15

    mul-int/lit8 v5, p2, 0x2

    invoke-interface {p1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/Integer;

    invoke-virtual {v12}, Ljava/lang/Integer;->intValue()I

    move-result v12

    add-int/2addr v0, v12

    add-int/2addr v5, v1

    invoke-interface {p1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    add-int/2addr v2, v5

    add-int/lit8 p2, p2, 0x1

    goto :goto_7

    :cond_15
    invoke-interface {p1, v4, v6}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object p2

    new-array v5, v6, [Ljava/lang/Integer;

    invoke-interface {p1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    aput-object v6, v5, v4

    invoke-interface {p1, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    aput-object v6, v5, v1

    invoke-interface {p1, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    aput-object v6, v5, v3

    const/16 v6, 0x9

    invoke-interface {p1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    aput-object v6, v5, v10

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v6

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v7

    const/16 v10, 0xa

    if-le v6, v7, :cond_16

    invoke-interface {p1, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    goto :goto_8

    :cond_16
    neg-int v6, v0

    :goto_8
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v6

    if-le v0, v6, :cond_17

    neg-int p1, v2

    goto :goto_9

    :cond_17
    invoke-interface {p1, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    :goto_9
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    aput-object p1, v5, v9

    invoke-static {v5}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    new-array v0, v3, [Ljava/util/List;

    aput-object p2, v0, v4

    aput-object p1, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    new-instance p2, Lorg/apache/fontbox/cff/CharStringCommand;

    invoke-direct {p2, v11}, Lorg/apache/fontbox/cff/CharStringCommand;-><init>(I)V

    goto/16 :goto_5

    :cond_18
    const-string v2, "hstemhm"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_19

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p2

    rem-int/2addr p2, v3

    if-eqz p2, :cond_0

    goto/16 :goto_0

    :cond_19
    const-string v2, "hintmask"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1f

    const-string v2, "cntrmask"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1a

    goto/16 :goto_a

    :cond_1a
    const-string v2, "vstemhm"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1b

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p2

    rem-int/2addr p2, v3

    if-eqz p2, :cond_2

    goto/16 :goto_1

    :cond_1b
    const-string v2, "rcurveline"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1c

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p2

    sub-int/2addr p2, v3

    invoke-interface {p1, v4, p2}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object p2

    invoke-static {p2, v6}, Lorg/apache/fontbox/cff/Type2CharString;->split(Ljava/util/List;I)Ljava/util/List;

    move-result-object p2

    new-instance v0, Lorg/apache/fontbox/cff/CharStringCommand;

    invoke-direct {v0, v11}, Lorg/apache/fontbox/cff/CharStringCommand;-><init>(I)V

    invoke-direct {p0, p2, v0}, Lorg/apache/fontbox/cff/Type2CharString;->addCommandList(Ljava/util/List;Lorg/apache/fontbox/cff/CharStringCommand;)V

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p2

    sub-int/2addr p2, v3

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    invoke-interface {p1, p2, v0}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object p1

    new-instance p2, Lorg/apache/fontbox/cff/CharStringCommand;

    invoke-direct {p2, v9}, Lorg/apache/fontbox/cff/CharStringCommand;-><init>(I)V

    goto/16 :goto_4

    :cond_1c
    const-string v2, "rlinecurve"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1d

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p2

    sub-int/2addr p2, v6

    invoke-interface {p1, v4, p2}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object p2

    invoke-static {p2, v3}, Lorg/apache/fontbox/cff/Type2CharString;->split(Ljava/util/List;I)Ljava/util/List;

    move-result-object p2

    new-instance v0, Lorg/apache/fontbox/cff/CharStringCommand;

    invoke-direct {v0, v9}, Lorg/apache/fontbox/cff/CharStringCommand;-><init>(I)V

    invoke-direct {p0, p2, v0}, Lorg/apache/fontbox/cff/Type2CharString;->addCommandList(Ljava/util/List;Lorg/apache/fontbox/cff/CharStringCommand;)V

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p2

    sub-int/2addr p2, v6

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    invoke-interface {p1, p2, v0}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object p1

    new-instance p2, Lorg/apache/fontbox/cff/CharStringCommand;

    invoke-direct {p2, v11}, Lorg/apache/fontbox/cff/CharStringCommand;-><init>(I)V

    goto/16 :goto_4

    :cond_1d
    const-string v2, "vvcurveto"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1e

    invoke-direct {p0, p1, v4}, Lorg/apache/fontbox/cff/Type2CharString;->drawCurve(Ljava/util/List;Z)V

    goto :goto_c

    :cond_1e
    const-string v2, "hhcurveto"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-direct {p0, p1, v1}, Lorg/apache/fontbox/cff/Type2CharString;->drawCurve(Ljava/util/List;Z)V

    goto :goto_c

    :cond_1f
    :goto_a
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p2

    rem-int/2addr p2, v3

    if-eqz p2, :cond_20

    goto :goto_b

    :cond_20
    move v1, v4

    :goto_b
    invoke-direct {p0, p1, v1}, Lorg/apache/fontbox/cff/Type2CharString;->clearStack(Ljava/util/List;Z)Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p2

    if-lez p2, :cond_21

    goto/16 :goto_2

    :cond_21
    :goto_c
    const/4 p1, 0x0

    return-object p1
.end method

.method private markPath()V
    .locals 1

    iget v0, p0, Lorg/apache/fontbox/cff/Type2CharString;->pathCount:I

    if-lez v0, :cond_0

    invoke-direct {p0}, Lorg/apache/fontbox/cff/Type2CharString;->closePath()V

    :cond_0
    iget v0, p0, Lorg/apache/fontbox/cff/Type2CharString;->pathCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/fontbox/cff/Type2CharString;->pathCount:I

    return-void
.end method

.method private static split(Ljava/util/List;I)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/List<",
            "TE;>;I)",
            "Ljava/util/List<",
            "Ljava/util/List<",
            "TE;>;>;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    const/4 v1, 0x0

    :goto_0
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v2

    div-int/2addr v2, p1

    if-ge v1, v2, :cond_0

    mul-int v2, v1, p1

    add-int/lit8 v1, v1, 0x1

    mul-int v3, v1, p1

    invoke-interface {p0, v2, v3}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v0
.end method


# virtual methods
.method public getGID()I
    .locals 1

    iget v0, p0, Lorg/apache/fontbox/cff/Type2CharString;->gid:I

    return v0
.end method

.method public getType2Sequence()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/fontbox/cff/Type2CharString;->type2sequence:Ljava/util/List;

    return-object v0
.end method

.class public Lorg/apache/fontbox/cff/CharStringConverter;
.super Lorg/apache/fontbox/cff/CharStringHandler;
.source "SourceFile"


# instance fields
.field private defaultWidthX:I

.field private nominalWidthX:I

.field private pathCount:I

.field private sequence:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(II)V
    .locals 1

    .line 1
    invoke-direct {p0}, Lorg/apache/fontbox/cff/CharStringHandler;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/fontbox/cff/CharStringConverter;->sequence:Ljava/util/List;

    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/fontbox/cff/CharStringConverter;->pathCount:I

    iput p1, p0, Lorg/apache/fontbox/cff/CharStringConverter;->defaultWidthX:I

    iput p2, p0, Lorg/apache/fontbox/cff/CharStringConverter;->nominalWidthX:I

    return-void
.end method

.method public constructor <init>(IILorg/apache/fontbox/cff/IndexData;Lorg/apache/fontbox/cff/IndexData;)V
    .locals 0

    .line 2
    invoke-direct {p0}, Lorg/apache/fontbox/cff/CharStringHandler;-><init>()V

    const/4 p3, 0x0

    iput-object p3, p0, Lorg/apache/fontbox/cff/CharStringConverter;->sequence:Ljava/util/List;

    const/4 p3, 0x0

    iput p3, p0, Lorg/apache/fontbox/cff/CharStringConverter;->pathCount:I

    iput p1, p0, Lorg/apache/fontbox/cff/CharStringConverter;->defaultWidthX:I

    iput p2, p0, Lorg/apache/fontbox/cff/CharStringConverter;->nominalWidthX:I

    return-void
.end method

.method private addCommand(Ljava/util/List;Lorg/apache/fontbox/cff/CharStringCommand;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;",
            "Lorg/apache/fontbox/cff/CharStringCommand;",
            ")V"
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/fontbox/cff/CharStringConverter;->sequence:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    iget-object p1, p0, Lorg/apache/fontbox/cff/CharStringConverter;->sequence:Ljava/util/List;

    invoke-interface {p1, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method private addCommandList(Ljava/util/List;Lorg/apache/fontbox/cff/CharStringCommand;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;>;",
            "Lorg/apache/fontbox/cff/CharStringCommand;",
            ")V"
        }
    .end annotation

    const/4 v0, 0x0

    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    invoke-direct {p0, v1, p2}, Lorg/apache/fontbox/cff/CharStringConverter;->addCommand(Ljava/util/List;Lorg/apache/fontbox/cff/CharStringCommand;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private clearStack(Ljava/util/List;Z)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;Z)",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/fontbox/cff/CharStringConverter;->sequence:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_1

    const/16 v0, 0xd

    const/4 v1, 0x2

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-eqz p2, :cond_0

    new-array p2, v1, [Ljava/lang/Integer;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, p2, v3

    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget v3, p0, Lorg/apache/fontbox/cff/CharStringConverter;->nominalWidthX:I

    add-int/2addr v1, v3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, p2, v2

    invoke-static {p2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p2

    new-instance v1, Lorg/apache/fontbox/cff/CharStringCommand;

    invoke-direct {v1, v0}, Lorg/apache/fontbox/cff/CharStringCommand;-><init>(I)V

    invoke-direct {p0, p2, v1}, Lorg/apache/fontbox/cff/CharStringConverter;->addCommand(Ljava/util/List;Lorg/apache/fontbox/cff/CharStringCommand;)V

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p2

    invoke-interface {p1, v2, p2}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object p1

    goto :goto_0

    :cond_0
    new-array p2, v1, [Ljava/lang/Integer;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, p2, v3

    iget v1, p0, Lorg/apache/fontbox/cff/CharStringConverter;->defaultWidthX:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, p2, v2

    invoke-static {p2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p2

    new-instance v1, Lorg/apache/fontbox/cff/CharStringCommand;

    invoke-direct {v1, v0}, Lorg/apache/fontbox/cff/CharStringCommand;-><init>(I)V

    invoke-direct {p0, p2, v1}, Lorg/apache/fontbox/cff/CharStringConverter;->addCommand(Ljava/util/List;Lorg/apache/fontbox/cff/CharStringCommand;)V

    :cond_1
    :goto_0
    return-object p1
.end method

.method private closePath()V
    .locals 3

    iget v0, p0, Lorg/apache/fontbox/cff/CharStringConverter;->pathCount:I

    if-lez v0, :cond_0

    iget-object v0, p0, Lorg/apache/fontbox/cff/CharStringConverter;->sequence:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/fontbox/cff/CharStringCommand;

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    new-instance v1, Lorg/apache/fontbox/cff/CharStringCommand;

    const/16 v2, 0x9

    invoke-direct {v1, v2}, Lorg/apache/fontbox/cff/CharStringCommand;-><init>(I)V

    if-eqz v0, :cond_1

    invoke-virtual {v1, v0}, Lorg/apache/fontbox/cff/CharStringCommand;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0, v1}, Lorg/apache/fontbox/cff/CharStringConverter;->addCommand(Ljava/util/List;Lorg/apache/fontbox/cff/CharStringCommand;)V

    :cond_1
    return-void
.end method

.method private drawAlternatingCurve(Ljava/util/List;Z)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;Z)V"
        }
    .end annotation

    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_5

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x5

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-ne v0, v1, :cond_0

    move v0, v2

    goto :goto_1

    :cond_0
    move v0, v3

    :goto_1
    const/16 v4, 0x8

    const/4 v5, 0x6

    const/4 v6, 0x3

    const/4 v7, 0x2

    const/4 v8, 0x4

    new-array v5, v5, [Ljava/lang/Integer;

    if-eqz p2, :cond_2

    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Integer;

    aput-object v9, v5, v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v5, v2

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    aput-object v2, v5, v7

    invoke-interface {p1, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    aput-object v2, v5, v6

    if-eqz v0, :cond_1

    invoke-interface {p1, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    goto :goto_2

    :cond_1
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    :goto_2
    aput-object v2, v5, v8

    invoke-interface {p1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    aput-object v2, v5, v1

    invoke-static {v5}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    new-instance v3, Lorg/apache/fontbox/cff/CharStringCommand;

    invoke-direct {v3, v4}, Lorg/apache/fontbox/cff/CharStringCommand;-><init>(I)V

    goto :goto_4

    :cond_2
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v5, v3

    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Integer;

    aput-object v9, v5, v2

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    aput-object v2, v5, v7

    invoke-interface {p1, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    aput-object v2, v5, v6

    invoke-interface {p1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    aput-object v2, v5, v8

    if-eqz v0, :cond_3

    invoke-interface {p1, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    goto :goto_3

    :cond_3
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    :goto_3
    aput-object v2, v5, v1

    invoke-static {v5}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    new-instance v3, Lorg/apache/fontbox/cff/CharStringCommand;

    invoke-direct {v3, v4}, Lorg/apache/fontbox/cff/CharStringCommand;-><init>(I)V

    :goto_4
    invoke-direct {p0, v2, v3}, Lorg/apache/fontbox/cff/CharStringConverter;->addCommand(Ljava/util/List;Lorg/apache/fontbox/cff/CharStringCommand;)V

    if-eqz v0, :cond_4

    goto :goto_5

    :cond_4
    move v1, v8

    :goto_5
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    invoke-interface {p1, v1, v0}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object p1

    xor-int/lit8 p2, p2, 0x1

    goto/16 :goto_0

    :cond_5
    return-void
.end method

.method private drawAlternatingLine(Ljava/util/List;Z)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;Z)V"
        }
    .end annotation

    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_1

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-interface {p1, v0, v1}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    new-instance v2, Lorg/apache/fontbox/cff/CharStringCommand;

    if-eqz p2, :cond_0

    const/4 v3, 0x6

    goto :goto_1

    :cond_0
    const/4 v3, 0x7

    :goto_1
    invoke-direct {v2, v3}, Lorg/apache/fontbox/cff/CharStringCommand;-><init>(I)V

    invoke-direct {p0, v0, v2}, Lorg/apache/fontbox/cff/CharStringConverter;->addCommand(Ljava/util/List;Lorg/apache/fontbox/cff/CharStringCommand;)V

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    invoke-interface {p1, v1, v0}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object p1

    xor-int/lit8 p2, p2, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method private drawCurve(Ljava/util/List;Z)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;Z)V"
        }
    .end annotation

    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_b

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x4

    rem-int/2addr v0, v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-ne v0, v2, :cond_0

    move v0, v2

    goto :goto_1

    :cond_0
    move v0, v3

    :goto_1
    const/16 v4, 0x8

    const/4 v5, 0x6

    const/4 v6, 0x5

    const/4 v7, 0x3

    const/4 v8, 0x2

    new-array v5, v5, [Ljava/lang/Integer;

    if-eqz p2, :cond_5

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Integer;

    aput-object v9, v5, v3

    if-eqz v0, :cond_1

    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Integer;

    goto :goto_2

    :cond_1
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    :goto_2
    aput-object v9, v5, v2

    if-eqz v0, :cond_2

    move v2, v8

    :cond_2
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    aput-object v2, v5, v8

    if-eqz v0, :cond_3

    move v8, v7

    :cond_3
    invoke-interface {p1, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    aput-object v2, v5, v7

    if-eqz v0, :cond_4

    move v7, v1

    :cond_4
    invoke-interface {p1, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    aput-object v2, v5, v1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v5, v6

    invoke-static {v5}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    new-instance v3, Lorg/apache/fontbox/cff/CharStringCommand;

    invoke-direct {v3, v4}, Lorg/apache/fontbox/cff/CharStringCommand;-><init>(I)V

    goto :goto_4

    :cond_5
    if-eqz v0, :cond_6

    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Integer;

    goto :goto_3

    :cond_6
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    :goto_3
    aput-object v9, v5, v3

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Integer;

    aput-object v9, v5, v2

    if-eqz v0, :cond_7

    move v2, v8

    :cond_7
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    aput-object v2, v5, v8

    if-eqz v0, :cond_8

    move v8, v7

    :cond_8
    invoke-interface {p1, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    aput-object v2, v5, v7

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v5, v1

    if-eqz v0, :cond_9

    move v7, v1

    :cond_9
    invoke-interface {p1, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    aput-object v2, v5, v6

    invoke-static {v5}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    new-instance v3, Lorg/apache/fontbox/cff/CharStringCommand;

    invoke-direct {v3, v4}, Lorg/apache/fontbox/cff/CharStringCommand;-><init>(I)V

    :goto_4
    invoke-direct {p0, v2, v3}, Lorg/apache/fontbox/cff/CharStringConverter;->addCommand(Ljava/util/List;Lorg/apache/fontbox/cff/CharStringCommand;)V

    if-eqz v0, :cond_a

    move v1, v6

    :cond_a
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    invoke-interface {p1, v1, v0}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object p1

    goto/16 :goto_0

    :cond_b
    return-void
.end method

.method private expandStemHints(Ljava/util/List;Z)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;Z)V"
        }
    .end annotation

    return-void
.end method

.method private handleType1Command(Ljava/util/List;Lorg/apache/fontbox/cff/CharStringCommand;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;",
            "Lorg/apache/fontbox/cff/CharStringCommand;",
            ")",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    sget-object v0, Lorg/apache/fontbox/cff/CharStringCommand;->TYPE1_VOCABULARY:Ljava/util/Map;

    invoke-virtual {p2}, Lorg/apache/fontbox/cff/CharStringCommand;->getKey()Lorg/apache/fontbox/cff/CharStringCommand$Key;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v1, "hstem"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    const/4 v2, 0x2

    const/4 v3, 0x0

    const/4 v4, 0x1

    if-eqz v1, :cond_1

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p2

    rem-int/2addr p2, v2

    if-eqz p2, :cond_0

    move v3, v4

    :cond_0
    invoke-direct {p0, p1, v3}, Lorg/apache/fontbox/cff/CharStringConverter;->clearStack(Ljava/util/List;Z)Ljava/util/List;

    move-result-object p1

    invoke-direct {p0, p1, v4}, Lorg/apache/fontbox/cff/CharStringConverter;->expandStemHints(Ljava/util/List;Z)V

    goto/16 :goto_4

    :cond_1
    const-string v1, "vstem"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p2

    rem-int/2addr p2, v2

    if-eqz p2, :cond_2

    goto :goto_0

    :cond_2
    move v4, v3

    :goto_0
    invoke-direct {p0, p1, v4}, Lorg/apache/fontbox/cff/CharStringConverter;->clearStack(Ljava/util/List;Z)Ljava/util/List;

    move-result-object p1

    invoke-direct {p0, p1, v3}, Lorg/apache/fontbox/cff/CharStringConverter;->expandStemHints(Ljava/util/List;Z)V

    goto/16 :goto_4

    :cond_3
    const-string v1, "vmoveto"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, v4, :cond_4

    :goto_1
    move v3, v4

    :cond_4
    invoke-direct {p0, p1, v3}, Lorg/apache/fontbox/cff/CharStringConverter;->clearStack(Ljava/util/List;Z)Ljava/util/List;

    move-result-object p1

    invoke-direct {p0}, Lorg/apache/fontbox/cff/CharStringConverter;->markPath()V

    :cond_5
    :goto_2
    invoke-direct {p0, p1, p2}, Lorg/apache/fontbox/cff/CharStringConverter;->addCommand(Ljava/util/List;Lorg/apache/fontbox/cff/CharStringCommand;)V

    goto/16 :goto_4

    :cond_6
    const-string v1, "rlineto"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-static {p1, v2}, Lorg/apache/fontbox/cff/CharStringConverter;->split(Ljava/util/List;I)Ljava/util/List;

    move-result-object p1

    :goto_3
    invoke-direct {p0, p1, p2}, Lorg/apache/fontbox/cff/CharStringConverter;->addCommandList(Ljava/util/List;Lorg/apache/fontbox/cff/CharStringCommand;)V

    goto/16 :goto_4

    :cond_7
    const-string v1, "hlineto"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-direct {p0, p1, v4}, Lorg/apache/fontbox/cff/CharStringConverter;->drawAlternatingLine(Ljava/util/List;Z)V

    goto/16 :goto_4

    :cond_8
    const-string v1, "vlineto"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    invoke-direct {p0, p1, v3}, Lorg/apache/fontbox/cff/CharStringConverter;->drawAlternatingLine(Ljava/util/List;Z)V

    goto :goto_4

    :cond_9
    const-string v1, "rrcurveto"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    const/4 v0, 0x6

    invoke-static {p1, v0}, Lorg/apache/fontbox/cff/CharStringConverter;->split(Ljava/util/List;I)Ljava/util/List;

    move-result-object p1

    goto :goto_3

    :cond_a
    const-string v1, "endchar"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_b

    move v3, v4

    :cond_b
    invoke-direct {p0, p1, v3}, Lorg/apache/fontbox/cff/CharStringConverter;->clearStack(Ljava/util/List;Z)Ljava/util/List;

    move-result-object p1

    invoke-direct {p0}, Lorg/apache/fontbox/cff/CharStringConverter;->closePath()V

    goto :goto_2

    :cond_c
    const-string v1, "rmoveto"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_d

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, v2, :cond_4

    goto :goto_1

    :cond_d
    const-string v1, "hmoveto"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_e

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, v4, :cond_4

    goto :goto_1

    :cond_e
    const-string v1, "vhcurveto"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_f

    invoke-direct {p0, p1, v3}, Lorg/apache/fontbox/cff/CharStringConverter;->drawAlternatingCurve(Ljava/util/List;Z)V

    goto :goto_4

    :cond_f
    const-string v1, "hvcurveto"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_10

    invoke-direct {p0, p1, v4}, Lorg/apache/fontbox/cff/CharStringConverter;->drawAlternatingCurve(Ljava/util/List;Z)V

    goto :goto_4

    :cond_10
    const-string v1, "return"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    return-object p1

    :goto_4
    const/4 p1, 0x0

    return-object p1
.end method

.method private handleType2Command(Ljava/util/List;Lorg/apache/fontbox/cff/CharStringCommand;)Ljava/util/List;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;",
            "Lorg/apache/fontbox/cff/CharStringCommand;",
            ")",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    sget-object v0, Lorg/apache/fontbox/cff/CharStringCommand;->TYPE2_VOCABULARY:Ljava/util/Map;

    invoke-virtual {p2}, Lorg/apache/fontbox/cff/CharStringCommand;->getKey()Lorg/apache/fontbox/cff/CharStringCommand$Key;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v1, "hflex"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    const/4 v2, 0x4

    const/4 v3, 0x3

    const/16 v4, 0x8

    const/4 v5, 0x5

    const/4 v6, 0x6

    const/4 v7, 0x1

    const/4 v8, 0x2

    const/4 v9, 0x0

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    if-eqz v1, :cond_0

    new-array p2, v6, [Ljava/lang/Integer;

    invoke-interface {p1, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    aput-object v0, p2, v9

    aput-object v10, p2, v7

    invoke-interface {p1, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    aput-object v0, p2, v8

    invoke-interface {p1, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    aput-object v0, p2, v3

    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    aput-object v0, p2, v2

    aput-object v10, p2, v5

    invoke-static {p2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p2

    new-array v0, v6, [Ljava/lang/Integer;

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    aput-object v1, v0, v9

    aput-object v10, v0, v7

    invoke-interface {p1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    aput-object v1, v0, v8

    invoke-interface {p1, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    neg-int v1, v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v3

    invoke-interface {p1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Integer;

    aput-object p1, v0, v2

    aput-object v10, v0, v5

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    new-array v0, v8, [Ljava/util/List;

    aput-object p2, v0, v9

    aput-object p1, v0, v7

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    new-instance p2, Lorg/apache/fontbox/cff/CharStringCommand;

    invoke-direct {p2, v4}, Lorg/apache/fontbox/cff/CharStringCommand;-><init>(I)V

    :goto_0
    invoke-direct {p0, p1, p2}, Lorg/apache/fontbox/cff/CharStringConverter;->addCommandList(Ljava/util/List;Lorg/apache/fontbox/cff/CharStringCommand;)V

    goto/16 :goto_9

    :cond_0
    const-string v1, "flex"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {p1, v9, v6}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object p2

    const/16 v0, 0xc

    invoke-interface {p1, v6, v0}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object p1

    new-array v0, v8, [Ljava/util/List;

    aput-object p2, v0, v9

    aput-object p1, v0, v7

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    new-instance p2, Lorg/apache/fontbox/cff/CharStringCommand;

    invoke-direct {p2, v4}, Lorg/apache/fontbox/cff/CharStringCommand;-><init>(I)V

    goto :goto_0

    :cond_1
    const-string v1, "hflex1"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    const/4 v11, 0x7

    if-eqz v1, :cond_2

    new-array p2, v6, [Ljava/lang/Integer;

    invoke-interface {p1, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    aput-object v0, p2, v9

    invoke-interface {p1, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    aput-object v0, p2, v7

    invoke-interface {p1, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    aput-object v0, p2, v8

    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    aput-object v0, p2, v3

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    aput-object v0, p2, v2

    aput-object v10, p2, v5

    invoke-static {p2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p2

    new-array v0, v6, [Ljava/lang/Integer;

    invoke-interface {p1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    aput-object v1, v0, v9

    aput-object v10, v0, v7

    invoke-interface {p1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    aput-object v1, v0, v8

    invoke-interface {p1, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    aput-object v1, v0, v3

    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Integer;

    aput-object p1, v0, v2

    aput-object v10, v0, v5

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    new-array v0, v8, [Ljava/util/List;

    aput-object p2, v0, v9

    aput-object p1, v0, v7

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    new-instance p2, Lorg/apache/fontbox/cff/CharStringCommand;

    invoke-direct {p2, v4}, Lorg/apache/fontbox/cff/CharStringCommand;-><init>(I)V

    goto/16 :goto_0

    :cond_2
    const-string v1, "flex1"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    move p2, v9

    move v0, p2

    move v1, v0

    :goto_1
    if-ge p2, v5, :cond_3

    mul-int/lit8 v10, p2, 0x2

    invoke-interface {p1, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/Integer;

    invoke-virtual {v12}, Ljava/lang/Integer;->intValue()I

    move-result v12

    add-int/2addr v0, v12

    add-int/2addr v10, v7

    invoke-interface {p1, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/Integer;

    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v10

    add-int/2addr v1, v10

    add-int/lit8 p2, p2, 0x1

    goto :goto_1

    :cond_3
    invoke-interface {p1, v9, v6}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object p2

    new-array v10, v6, [Ljava/lang/Integer;

    invoke-interface {p1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    aput-object v6, v10, v9

    invoke-interface {p1, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    aput-object v6, v10, v7

    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    aput-object v6, v10, v8

    const/16 v6, 0x9

    invoke-interface {p1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    aput-object v6, v10, v3

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v3

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v6

    const/16 v11, 0xa

    if-le v3, v6, :cond_4

    invoke-interface {p1, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    goto :goto_2

    :cond_4
    neg-int v3, v0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    :goto_2
    aput-object v3, v10, v2

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v2

    if-le v0, v2, :cond_5

    neg-int p1, v1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    goto :goto_3

    :cond_5
    invoke-interface {p1, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Integer;

    :goto_3
    aput-object p1, v10, v5

    invoke-static {v10}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    new-array v0, v8, [Ljava/util/List;

    aput-object p2, v0, v9

    aput-object p1, v0, v7

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object p1

    new-instance p2, Lorg/apache/fontbox/cff/CharStringCommand;

    invoke-direct {p2, v4}, Lorg/apache/fontbox/cff/CharStringCommand;-><init>(I)V

    goto/16 :goto_0

    :cond_6
    const-string v1, "hstemhm"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p2

    rem-int/2addr p2, v8

    if-eqz p2, :cond_7

    move v9, v7

    :cond_7
    invoke-direct {p0, p1, v9}, Lorg/apache/fontbox/cff/CharStringConverter;->clearStack(Ljava/util/List;Z)Ljava/util/List;

    move-result-object p1

    invoke-direct {p0, p1, v7}, Lorg/apache/fontbox/cff/CharStringConverter;->expandStemHints(Ljava/util/List;Z)V

    goto/16 :goto_9

    :cond_8
    const-string v1, "hintmask"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_10

    const-string v1, "cntrmask"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    goto/16 :goto_6

    :cond_9
    const-string v1, "vstemhm"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p2

    rem-int/2addr p2, v8

    if-eqz p2, :cond_a

    goto :goto_4

    :cond_a
    move v7, v9

    :goto_4
    invoke-direct {p0, p1, v7}, Lorg/apache/fontbox/cff/CharStringConverter;->clearStack(Ljava/util/List;Z)Ljava/util/List;

    move-result-object p1

    goto/16 :goto_8

    :cond_b
    const-string v1, "rcurveline"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_d

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p2

    sub-int/2addr p2, v8

    invoke-interface {p1, v9, p2}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object p2

    invoke-static {p2, v6}, Lorg/apache/fontbox/cff/CharStringConverter;->split(Ljava/util/List;I)Ljava/util/List;

    move-result-object p2

    new-instance v0, Lorg/apache/fontbox/cff/CharStringCommand;

    invoke-direct {v0, v4}, Lorg/apache/fontbox/cff/CharStringCommand;-><init>(I)V

    invoke-direct {p0, p2, v0}, Lorg/apache/fontbox/cff/CharStringConverter;->addCommandList(Ljava/util/List;Lorg/apache/fontbox/cff/CharStringCommand;)V

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p2

    sub-int/2addr p2, v8

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    invoke-interface {p1, p2, v0}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object p1

    new-instance p2, Lorg/apache/fontbox/cff/CharStringCommand;

    invoke-direct {p2, v5}, Lorg/apache/fontbox/cff/CharStringCommand;-><init>(I)V

    :cond_c
    :goto_5
    invoke-direct {p0, p1, p2}, Lorg/apache/fontbox/cff/CharStringConverter;->addCommand(Ljava/util/List;Lorg/apache/fontbox/cff/CharStringCommand;)V

    goto :goto_9

    :cond_d
    const-string v1, "rlinecurve"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_e

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p2

    sub-int/2addr p2, v6

    invoke-interface {p1, v9, p2}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object p2

    invoke-static {p2, v8}, Lorg/apache/fontbox/cff/CharStringConverter;->split(Ljava/util/List;I)Ljava/util/List;

    move-result-object p2

    new-instance v0, Lorg/apache/fontbox/cff/CharStringCommand;

    invoke-direct {v0, v5}, Lorg/apache/fontbox/cff/CharStringCommand;-><init>(I)V

    invoke-direct {p0, p2, v0}, Lorg/apache/fontbox/cff/CharStringConverter;->addCommandList(Ljava/util/List;Lorg/apache/fontbox/cff/CharStringCommand;)V

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p2

    sub-int/2addr p2, v6

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    invoke-interface {p1, p2, v0}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object p1

    new-instance p2, Lorg/apache/fontbox/cff/CharStringCommand;

    invoke-direct {p2, v4}, Lorg/apache/fontbox/cff/CharStringCommand;-><init>(I)V

    goto :goto_5

    :cond_e
    const-string v1, "vvcurveto"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_f

    invoke-direct {p0, p1, v9}, Lorg/apache/fontbox/cff/CharStringConverter;->drawCurve(Ljava/util/List;Z)V

    goto :goto_9

    :cond_f
    const-string v1, "hhcurveto"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    invoke-direct {p0, p1, v7}, Lorg/apache/fontbox/cff/CharStringConverter;->drawCurve(Ljava/util/List;Z)V

    goto :goto_9

    :cond_10
    :goto_6
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p2

    rem-int/2addr p2, v8

    if-eqz p2, :cond_11

    goto :goto_7

    :cond_11
    move v7, v9

    :goto_7
    invoke-direct {p0, p1, v7}, Lorg/apache/fontbox/cff/CharStringConverter;->clearStack(Ljava/util/List;Z)Ljava/util/List;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result p2

    if-lez p2, :cond_12

    :goto_8
    invoke-direct {p0, p1, v9}, Lorg/apache/fontbox/cff/CharStringConverter;->expandStemHints(Ljava/util/List;Z)V

    :cond_12
    :goto_9
    const/4 p1, 0x0

    return-object p1
.end method

.method private markPath()V
    .locals 1

    iget v0, p0, Lorg/apache/fontbox/cff/CharStringConverter;->pathCount:I

    if-lez v0, :cond_0

    invoke-direct {p0}, Lorg/apache/fontbox/cff/CharStringConverter;->closePath()V

    :cond_0
    iget v0, p0, Lorg/apache/fontbox/cff/CharStringConverter;->pathCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lorg/apache/fontbox/cff/CharStringConverter;->pathCount:I

    return-void
.end method

.method private static split(Ljava/util/List;I)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/List<",
            "TE;>;I)",
            "Ljava/util/List<",
            "Ljava/util/List<",
            "TE;>;>;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    const/4 v1, 0x0

    :goto_0
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v2

    div-int/2addr v2, p1

    if-ge v1, v2, :cond_0

    mul-int v2, v1, p1

    add-int/lit8 v1, v1, 0x1

    mul-int v3, v1, p1

    invoke-interface {p0, v2, v3}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v0
.end method


# virtual methods
.method public convert(Ljava/util/List;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/Object;",
            ">;)",
            "Ljava/util/List<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/apache/fontbox/cff/CharStringConverter;->sequence:Ljava/util/List;

    const/4 v0, 0x0

    iput v0, p0, Lorg/apache/fontbox/cff/CharStringConverter;->pathCount:I

    invoke-virtual {p0, p1}, Lorg/apache/fontbox/cff/CharStringHandler;->handleSequence(Ljava/util/List;)Ljava/util/List;

    iget-object p1, p0, Lorg/apache/fontbox/cff/CharStringConverter;->sequence:Ljava/util/List;

    return-object p1
.end method

.method public handleCommand(Ljava/util/List;Lorg/apache/fontbox/cff/CharStringCommand;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;",
            "Lorg/apache/fontbox/cff/CharStringCommand;",
            ")",
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    sget-object v0, Lorg/apache/fontbox/cff/CharStringCommand;->TYPE1_VOCABULARY:Ljava/util/Map;

    invoke-virtual {p2}, Lorg/apache/fontbox/cff/CharStringCommand;->getKey()Lorg/apache/fontbox/cff/CharStringCommand$Key;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p1, p2}, Lorg/apache/fontbox/cff/CharStringConverter;->handleType1Command(Ljava/util/List;Lorg/apache/fontbox/cff/CharStringCommand;)Ljava/util/List;

    move-result-object p1

    return-object p1

    :cond_0
    invoke-direct {p0, p1, p2}, Lorg/apache/fontbox/cff/CharStringConverter;->handleType2Command(Ljava/util/List;Lorg/apache/fontbox/cff/CharStringCommand;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

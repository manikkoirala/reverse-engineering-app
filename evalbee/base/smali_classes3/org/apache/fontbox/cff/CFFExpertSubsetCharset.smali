.class public final Lorg/apache/fontbox/cff/CFFExpertSubsetCharset;
.super Lorg/apache/fontbox/cff/CFFCharset;
.source "SourceFile"


# static fields
.field private static final INSTANCE:Lorg/apache/fontbox/cff/CFFExpertSubsetCharset;


# direct methods
.method public static constructor <clinit>()V
    .locals 7

    new-instance v0, Lorg/apache/fontbox/cff/CFFExpertSubsetCharset;

    invoke-direct {v0}, Lorg/apache/fontbox/cff/CFFExpertSubsetCharset;-><init>()V

    sput-object v0, Lorg/apache/fontbox/cff/CFFExpertSubsetCharset;->INSTANCE:Lorg/apache/fontbox/cff/CFFExpertSubsetCharset;

    const-string v1, ".notdef"

    const/4 v2, 0x0

    invoke-virtual {v0, v2, v2, v1}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const-string v1, "space"

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v2, v1}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0xe7

    const-string v2, "dollaroldstyle"

    const/4 v3, 0x2

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0xe8

    const-string v2, "dollarsuperior"

    const/4 v3, 0x3

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0xeb

    const-string v2, "parenleftsuperior"

    const/4 v3, 0x4

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0xec

    const-string v2, "parenrightsuperior"

    const/4 v3, 0x5

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0xed

    const-string v2, "twodotenleader"

    const/4 v3, 0x6

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0xee

    const-string v2, "onedotenleader"

    const/4 v3, 0x7

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const-string v1, "comma"

    const/16 v2, 0x8

    const/16 v3, 0xd

    invoke-virtual {v0, v2, v3, v1}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const-string v1, "hyphen"

    const/16 v2, 0x9

    const/16 v4, 0xe

    invoke-virtual {v0, v2, v4, v1}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const-string v1, "period"

    const/16 v2, 0xa

    const/16 v5, 0xf

    invoke-virtual {v0, v2, v5, v1}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x63

    const-string v2, "fraction"

    const/16 v6, 0xb

    invoke-virtual {v0, v6, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0xef

    const-string v2, "zerooldstyle"

    const/16 v6, 0xc

    invoke-virtual {v0, v6, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0xf0

    const-string v2, "oneoldstyle"

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0xf1

    const-string v2, "twooldstyle"

    invoke-virtual {v0, v4, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0xf2

    const-string v2, "threeoldstyle"

    invoke-virtual {v0, v5, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0xf3

    const-string v2, "fouroldstyle"

    const/16 v3, 0x10

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0xf4

    const-string v2, "fiveoldstyle"

    const/16 v3, 0x11

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0xf5

    const-string v2, "sixoldstyle"

    const/16 v3, 0x12

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0xf6

    const-string v2, "sevenoldstyle"

    const/16 v3, 0x13

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0xf7

    const-string v2, "eightoldstyle"

    const/16 v3, 0x14

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0xf8

    const-string v2, "nineoldstyle"

    const/16 v3, 0x15

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const-string v1, "colon"

    const/16 v2, 0x16

    const/16 v3, 0x1b

    invoke-virtual {v0, v2, v3, v1}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const-string v1, "semicolon"

    const/16 v2, 0x17

    const/16 v4, 0x1c

    invoke-virtual {v0, v2, v4, v1}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0xf9

    const-string v2, "commasuperior"

    const/16 v5, 0x18

    invoke-virtual {v0, v5, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0xfa

    const-string v2, "threequartersemdash"

    const/16 v5, 0x19

    invoke-virtual {v0, v5, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0xfb

    const-string v2, "periodsuperior"

    const/16 v5, 0x1a

    invoke-virtual {v0, v5, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0xfd

    const-string v2, "asuperior"

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0xfe

    const-string v2, "bsuperior"

    invoke-virtual {v0, v4, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0xff

    const-string v2, "centsuperior"

    const/16 v3, 0x1d

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x100

    const-string v2, "dsuperior"

    const/16 v3, 0x1e

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x101

    const-string v2, "esuperior"

    const/16 v3, 0x1f

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x102

    const-string v2, "isuperior"

    const/16 v3, 0x20

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x103

    const-string v2, "lsuperior"

    const/16 v3, 0x21

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x104

    const-string v2, "msuperior"

    const/16 v3, 0x22

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x105

    const-string v2, "nsuperior"

    const/16 v3, 0x23

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x106

    const-string v2, "osuperior"

    const/16 v3, 0x24

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x107

    const-string v2, "rsuperior"

    const/16 v3, 0x25

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x108

    const-string v2, "ssuperior"

    const/16 v3, 0x26

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x109

    const-string v2, "tsuperior"

    const/16 v3, 0x27

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x10a

    const-string v2, "ff"

    const/16 v3, 0x28

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x6d

    const-string v2, "fi"

    const/16 v3, 0x29

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x6e

    const-string v2, "fl"

    const/16 v3, 0x2a

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x10b

    const-string v2, "ffi"

    const/16 v3, 0x2b

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x10c

    const-string v2, "ffl"

    const/16 v3, 0x2c

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x10d

    const-string v2, "parenleftinferior"

    const/16 v3, 0x2d

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x10e

    const-string v2, "parenrightinferior"

    const/16 v3, 0x2e

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x110

    const-string v2, "hyphensuperior"

    const/16 v3, 0x2f

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x12c

    const-string v2, "colonmonetary"

    const/16 v3, 0x30

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x12d

    const-string v2, "onefitted"

    const/16 v3, 0x31

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x12e

    const-string v2, "rupiah"

    const/16 v3, 0x32

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x131

    const-string v2, "centoldstyle"

    const/16 v3, 0x33

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x13a

    const-string v2, "figuredash"

    const/16 v3, 0x34

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x13b

    const-string v2, "hypheninferior"

    const/16 v3, 0x35

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x9e

    const-string v2, "onequarter"

    const/16 v3, 0x36

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x9b

    const-string v2, "onehalf"

    const/16 v3, 0x37

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0xa3

    const-string v2, "threequarters"

    const/16 v3, 0x38

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x140

    const-string v2, "oneeighth"

    const/16 v3, 0x39

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x141

    const-string v2, "threeeighths"

    const/16 v3, 0x3a

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x142

    const-string v2, "fiveeighths"

    const/16 v3, 0x3b

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x143

    const-string v2, "seveneighths"

    const/16 v3, 0x3c

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x144

    const-string v2, "onethird"

    const/16 v3, 0x3d

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x145

    const-string v2, "twothirds"

    const/16 v3, 0x3e

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x146

    const-string v2, "zerosuperior"

    const/16 v3, 0x3f

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x96

    const-string v2, "onesuperior"

    const/16 v3, 0x40

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0xa4

    const-string v2, "twosuperior"

    const/16 v3, 0x41

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0xa9

    const-string v2, "threesuperior"

    const/16 v3, 0x42

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x147

    const-string v2, "foursuperior"

    const/16 v3, 0x43

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x148

    const-string v2, "fivesuperior"

    const/16 v3, 0x44

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x149

    const-string v2, "sixsuperior"

    const/16 v3, 0x45

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x14a

    const-string v2, "sevensuperior"

    const/16 v3, 0x46

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x14b

    const-string v2, "eightsuperior"

    const/16 v3, 0x47

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x14c

    const-string v2, "ninesuperior"

    const/16 v3, 0x48

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x14d

    const-string v2, "zeroinferior"

    const/16 v3, 0x49

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x14e

    const-string v2, "oneinferior"

    const/16 v3, 0x4a

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x14f

    const-string v2, "twoinferior"

    const/16 v3, 0x4b

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x150

    const-string v2, "threeinferior"

    const/16 v3, 0x4c

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x151

    const-string v2, "fourinferior"

    const/16 v3, 0x4d

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x152

    const-string v2, "fiveinferior"

    const/16 v3, 0x4e

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x153

    const-string v2, "sixinferior"

    const/16 v3, 0x4f

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x154

    const-string v2, "seveninferior"

    const/16 v3, 0x50

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x155

    const-string v2, "eightinferior"

    const/16 v3, 0x51

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x156

    const-string v2, "nineinferior"

    const/16 v3, 0x52

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x157

    const-string v2, "centinferior"

    const/16 v3, 0x53

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x158

    const-string v2, "dollarinferior"

    const/16 v3, 0x54

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x159

    const-string v2, "periodinferior"

    const/16 v3, 0x55

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    const/16 v1, 0x15a

    const-string v2, "commainferior"

    const/16 v3, 0x56

    invoke-virtual {v0, v3, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lorg/apache/fontbox/cff/CFFCharset;-><init>(Z)V

    return-void
.end method

.method public static getInstance()Lorg/apache/fontbox/cff/CFFExpertSubsetCharset;
    .locals 1

    sget-object v0, Lorg/apache/fontbox/cff/CFFExpertSubsetCharset;->INSTANCE:Lorg/apache/fontbox/cff/CFFExpertSubsetCharset;

    return-object v0
.end method

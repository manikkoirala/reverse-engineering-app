.class public Lorg/apache/fontbox/cff/CFFParser;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/fontbox/cff/CFFParser$Format2Charset;,
        Lorg/apache/fontbox/cff/CFFParser$Format1Charset;,
        Lorg/apache/fontbox/cff/CFFParser$Format0Charset;,
        Lorg/apache/fontbox/cff/CFFParser$EmptyCharset;,
        Lorg/apache/fontbox/cff/CFFParser$EmbeddedCharset;,
        Lorg/apache/fontbox/cff/CFFParser$Format1Encoding;,
        Lorg/apache/fontbox/cff/CFFParser$Format0Encoding;,
        Lorg/apache/fontbox/cff/CFFParser$EmbeddedEncoding;,
        Lorg/apache/fontbox/cff/CFFParser$DictData;,
        Lorg/apache/fontbox/cff/CFFParser$Header;,
        Lorg/apache/fontbox/cff/CFFParser$Format0FDSelect;,
        Lorg/apache/fontbox/cff/CFFParser$Range3;,
        Lorg/apache/fontbox/cff/CFFParser$Format3FDSelect;
    }
.end annotation


# static fields
.field private static final TAG_OTTO:Ljava/lang/String; = "OTTO"

.field private static final TAG_TTCF:Ljava/lang/String; = "ttcf"

.field private static final TAG_TTFONLY:Ljava/lang/String; = "\u0000\u0001\u0000\u0000"


# instance fields
.field private debugFontName:Ljava/lang/String;

.field private header:Lorg/apache/fontbox/cff/CFFParser$Header;

.field private input:Lorg/apache/fontbox/cff/CFFDataInput;

.field private nameIndex:Lorg/apache/fontbox/cff/IndexData;

.field private stringIndex:Lorg/apache/fontbox/cff/IndexData;

.field private topDictIndex:Lorg/apache/fontbox/cff/IndexData;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/fontbox/cff/CFFParser;->input:Lorg/apache/fontbox/cff/CFFDataInput;

    iput-object v0, p0, Lorg/apache/fontbox/cff/CFFParser;->header:Lorg/apache/fontbox/cff/CFFParser$Header;

    iput-object v0, p0, Lorg/apache/fontbox/cff/CFFParser;->nameIndex:Lorg/apache/fontbox/cff/IndexData;

    iput-object v0, p0, Lorg/apache/fontbox/cff/CFFParser;->topDictIndex:Lorg/apache/fontbox/cff/IndexData;

    iput-object v0, p0, Lorg/apache/fontbox/cff/CFFParser;->stringIndex:Lorg/apache/fontbox/cff/IndexData;

    return-void
.end method

.method private static getArray(Lorg/apache/fontbox/cff/CFFParser$DictData;Ljava/lang/String;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/fontbox/cff/CFFParser$DictData;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/Number;",
            ">;)",
            "Ljava/util/List<",
            "Ljava/lang/Number;",
            ">;"
        }
    .end annotation

    invoke-virtual {p0, p1}, Lorg/apache/fontbox/cff/CFFParser$DictData;->getEntry(Ljava/lang/String;)Lorg/apache/fontbox/cff/CFFParser$DictData$Entry;

    move-result-object p0

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lorg/apache/fontbox/cff/CFFParser$DictData$Entry;->getArray()Ljava/util/List;

    move-result-object p2

    :cond_0
    return-object p2
.end method

.method private static getBoolean(Lorg/apache/fontbox/cff/CFFParser$DictData;Ljava/lang/String;Z)Ljava/lang/Boolean;
    .locals 0

    invoke-virtual {p0, p1}, Lorg/apache/fontbox/cff/CFFParser$DictData;->getEntry(Ljava/lang/String;)Lorg/apache/fontbox/cff/CFFParser$DictData$Entry;

    move-result-object p0

    if-eqz p0, :cond_0

    const/4 p1, 0x0

    invoke-virtual {p0, p1}, Lorg/apache/fontbox/cff/CFFParser$DictData$Entry;->getBoolean(I)Ljava/lang/Boolean;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result p2

    :cond_0
    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object p0

    return-object p0
.end method

.method private static getDelta(Lorg/apache/fontbox/cff/CFFParser$DictData;Ljava/lang/String;Ljava/util/List;)Ljava/util/List;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/fontbox/cff/CFFParser$DictData;",
            "Ljava/lang/String;",
            "Ljava/util/List<",
            "Ljava/lang/Number;",
            ">;)",
            "Ljava/util/List<",
            "Ljava/lang/Number;",
            ">;"
        }
    .end annotation

    invoke-virtual {p0, p1}, Lorg/apache/fontbox/cff/CFFParser$DictData;->getEntry(Ljava/lang/String;)Lorg/apache/fontbox/cff/CFFParser$DictData$Entry;

    move-result-object p0

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lorg/apache/fontbox/cff/CFFParser$DictData$Entry;->getArray()Ljava/util/List;

    move-result-object p2

    :cond_0
    return-object p2
.end method

.method private static getNumber(Lorg/apache/fontbox/cff/CFFParser$DictData;Ljava/lang/String;Ljava/lang/Number;)Ljava/lang/Number;
    .locals 0

    invoke-virtual {p0, p1}, Lorg/apache/fontbox/cff/CFFParser$DictData;->getEntry(Ljava/lang/String;)Lorg/apache/fontbox/cff/CFFParser$DictData$Entry;

    move-result-object p0

    if-eqz p0, :cond_0

    const/4 p1, 0x0

    invoke-virtual {p0, p1}, Lorg/apache/fontbox/cff/CFFParser$DictData$Entry;->getNumber(I)Ljava/lang/Number;

    move-result-object p2

    :cond_0
    return-object p2
.end method

.method private getString(Lorg/apache/fontbox/cff/CFFParser$DictData;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    invoke-virtual {p1, p2}, Lorg/apache/fontbox/cff/CFFParser$DictData;->getEntry(Ljava/lang/String;)Lorg/apache/fontbox/cff/CFFParser$DictData$Entry;

    move-result-object p1

    if-eqz p1, :cond_0

    const/4 p2, 0x0

    invoke-virtual {p1, p2}, Lorg/apache/fontbox/cff/CFFParser$DictData$Entry;->getNumber(I)Ljava/lang/Number;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Number;->intValue()I

    move-result p1

    invoke-direct {p0, p1}, Lorg/apache/fontbox/cff/CFFParser;->readString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return-object p1
.end method

.method private parseCIDFontDicts(Lorg/apache/fontbox/cff/CFFParser$DictData;Lorg/apache/fontbox/cff/CFFCIDFont;Lorg/apache/fontbox/cff/IndexData;)V
    .locals 10

    const-string v0, "FDArray"

    invoke-virtual {p1, v0}, Lorg/apache/fontbox/cff/CFFParser$DictData;->getEntry(Ljava/lang/String;)Lorg/apache/fontbox/cff/CFFParser$DictData$Entry;

    move-result-object v0

    if-eqz v0, :cond_3

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lorg/apache/fontbox/cff/CFFParser$DictData$Entry;->getNumber(I)Ljava/lang/Number;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v0

    iget-object v2, p0, Lorg/apache/fontbox/cff/CFFParser;->input:Lorg/apache/fontbox/cff/CFFDataInput;

    invoke-virtual {v2, v0}, Lorg/apache/fontbox/cff/DataInput;->setPosition(I)V

    iget-object v0, p0, Lorg/apache/fontbox/cff/CFFParser;->input:Lorg/apache/fontbox/cff/CFFDataInput;

    invoke-static {v0}, Lorg/apache/fontbox/cff/CFFParser;->readIndexData(Lorg/apache/fontbox/cff/CFFDataInput;)Lorg/apache/fontbox/cff/IndexData;

    move-result-object v0

    new-instance v2, Ljava/util/LinkedList;

    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    new-instance v3, Ljava/util/LinkedList;

    invoke-direct {v3}, Ljava/util/LinkedList;-><init>()V

    move v4, v1

    :goto_0
    invoke-virtual {v0}, Lorg/apache/fontbox/cff/IndexData;->getCount()I

    move-result v5

    if-ge v4, v5, :cond_2

    invoke-virtual {v0, v4}, Lorg/apache/fontbox/cff/IndexData;->getBytes(I)[B

    move-result-object v5

    new-instance v6, Lorg/apache/fontbox/cff/CFFDataInput;

    invoke-direct {v6, v5}, Lorg/apache/fontbox/cff/CFFDataInput;-><init>([B)V

    invoke-static {v6}, Lorg/apache/fontbox/cff/CFFParser;->readDictData(Lorg/apache/fontbox/cff/CFFDataInput;)Lorg/apache/fontbox/cff/CFFParser$DictData;

    move-result-object v5

    new-instance v6, Ljava/util/LinkedHashMap;

    invoke-direct {v6}, Ljava/util/LinkedHashMap;-><init>()V

    const-string v7, "FontName"

    invoke-direct {p0, v5, v7}, Lorg/apache/fontbox/cff/CFFParser;->getString(Lorg/apache/fontbox/cff/CFFParser$DictData;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-interface {v6, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    const-string v8, "FontType"

    invoke-static {v5, v8, v7}, Lorg/apache/fontbox/cff/CFFParser;->getNumber(Lorg/apache/fontbox/cff/CFFParser$DictData;Ljava/lang/String;Ljava/lang/Number;)Ljava/lang/Number;

    move-result-object v7

    invoke-interface {v6, v8, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v7, "FontBBox"

    const/4 v8, 0x0

    invoke-static {v5, v7, v8}, Lorg/apache/fontbox/cff/CFFParser;->getDelta(Lorg/apache/fontbox/cff/CFFParser$DictData;Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object v9

    invoke-interface {v6, v7, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v7, "FontMatrix"

    invoke-static {v5, v7, v8}, Lorg/apache/fontbox/cff/CFFParser;->getDelta(Lorg/apache/fontbox/cff/CFFParser$DictData;Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object v8

    invoke-interface {v6, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {v3, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string v6, "Private"

    invoke-virtual {v5, v6}, Lorg/apache/fontbox/cff/CFFParser$DictData;->getEntry(Ljava/lang/String;)Lorg/apache/fontbox/cff/CFFParser$DictData$Entry;

    move-result-object v5

    if-eqz v5, :cond_1

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Lorg/apache/fontbox/cff/CFFParser$DictData$Entry;->getNumber(I)Ljava/lang/Number;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Number;->intValue()I

    move-result v6

    iget-object v7, p0, Lorg/apache/fontbox/cff/CFFParser;->input:Lorg/apache/fontbox/cff/CFFDataInput;

    invoke-virtual {v7, v6}, Lorg/apache/fontbox/cff/DataInput;->setPosition(I)V

    invoke-virtual {v5, v1}, Lorg/apache/fontbox/cff/CFFParser$DictData$Entry;->getNumber(I)Ljava/lang/Number;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Number;->intValue()I

    move-result v5

    new-instance v7, Lorg/apache/fontbox/cff/CFFDataInput;

    iget-object v8, p0, Lorg/apache/fontbox/cff/CFFParser;->input:Lorg/apache/fontbox/cff/CFFDataInput;

    invoke-virtual {v8, v5}, Lorg/apache/fontbox/cff/DataInput;->readBytes(I)[B

    move-result-object v5

    invoke-direct {v7, v5}, Lorg/apache/fontbox/cff/CFFDataInput;-><init>([B)V

    invoke-static {v7}, Lorg/apache/fontbox/cff/CFFParser;->readDictData(Lorg/apache/fontbox/cff/CFFDataInput;)Lorg/apache/fontbox/cff/CFFParser$DictData;

    move-result-object v5

    invoke-direct {p0, v5}, Lorg/apache/fontbox/cff/CFFParser;->readPrivateDict(Lorg/apache/fontbox/cff/CFFParser$DictData;)Ljava/util/Map;

    move-result-object v7

    invoke-interface {v2, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    const-string v9, "Subrs"

    invoke-static {v5, v9, v8}, Lorg/apache/fontbox/cff/CFFParser;->getNumber(Lorg/apache/fontbox/cff/CFFParser$DictData;Ljava/lang/String;Ljava/lang/Number;)Ljava/lang/Number;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    if-nez v5, :cond_0

    new-instance v5, Lorg/apache/fontbox/cff/IndexData;

    invoke-direct {v5, v1}, Lorg/apache/fontbox/cff/IndexData;-><init>(I)V

    goto :goto_1

    :cond_0
    iget-object v8, p0, Lorg/apache/fontbox/cff/CFFParser;->input:Lorg/apache/fontbox/cff/CFFDataInput;

    add-int/2addr v6, v5

    invoke-virtual {v8, v6}, Lorg/apache/fontbox/cff/DataInput;->setPosition(I)V

    iget-object v5, p0, Lorg/apache/fontbox/cff/CFFParser;->input:Lorg/apache/fontbox/cff/CFFDataInput;

    invoke-static {v5}, Lorg/apache/fontbox/cff/CFFParser;->readIndexData(Lorg/apache/fontbox/cff/CFFDataInput;)Lorg/apache/fontbox/cff/IndexData;

    move-result-object v5

    :goto_1
    invoke-interface {v7, v9, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_0

    :cond_1
    new-instance p1, Ljava/io/IOException;

    const-string p2, "Font DICT invalid without \"Private\" entry"

    invoke-direct {p1, p2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_2
    const-string v0, "FDSelect"

    invoke-virtual {p1, v0}, Lorg/apache/fontbox/cff/CFFParser$DictData;->getEntry(Ljava/lang/String;)Lorg/apache/fontbox/cff/CFFParser$DictData$Entry;

    move-result-object p1

    invoke-virtual {p1, v1}, Lorg/apache/fontbox/cff/CFFParser$DictData$Entry;->getNumber(I)Ljava/lang/Number;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Number;->intValue()I

    move-result p1

    iget-object v0, p0, Lorg/apache/fontbox/cff/CFFParser;->input:Lorg/apache/fontbox/cff/CFFDataInput;

    invoke-virtual {v0, p1}, Lorg/apache/fontbox/cff/DataInput;->setPosition(I)V

    iget-object p1, p0, Lorg/apache/fontbox/cff/CFFParser;->input:Lorg/apache/fontbox/cff/CFFDataInput;

    invoke-virtual {p3}, Lorg/apache/fontbox/cff/IndexData;->getCount()I

    move-result p3

    invoke-static {p1, p3, p2}, Lorg/apache/fontbox/cff/CFFParser;->readFDSelect(Lorg/apache/fontbox/cff/CFFDataInput;ILorg/apache/fontbox/cff/CFFCIDFont;)Lorg/apache/fontbox/cff/FDSelect;

    move-result-object p1

    invoke-virtual {p2, v3}, Lorg/apache/fontbox/cff/CFFCIDFont;->setFontDict(Ljava/util/List;)V

    invoke-virtual {p2, v2}, Lorg/apache/fontbox/cff/CFFCIDFont;->setPrivDict(Ljava/util/List;)V

    invoke-virtual {p2, p1}, Lorg/apache/fontbox/cff/CFFCIDFont;->setFdSelect(Lorg/apache/fontbox/cff/FDSelect;)V

    return-void

    :cond_3
    new-instance p1, Ljava/io/IOException;

    const-string p2, "FDArray is missing for a CIDKeyed Font."

    invoke-direct {p1, p2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private parseFont(I)Lorg/apache/fontbox/cff/CFFFont;
    .locals 17

    move-object/from16 v0, p0

    move/from16 v1, p1

    new-instance v2, Lorg/apache/fontbox/cff/DataInput;

    iget-object v3, v0, Lorg/apache/fontbox/cff/CFFParser;->nameIndex:Lorg/apache/fontbox/cff/IndexData;

    invoke-virtual {v3, v1}, Lorg/apache/fontbox/cff/IndexData;->getBytes(I)[B

    move-result-object v3

    invoke-direct {v2, v3}, Lorg/apache/fontbox/cff/DataInput;-><init>([B)V

    invoke-virtual {v2}, Lorg/apache/fontbox/cff/DataInput;->getString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lorg/apache/fontbox/cff/CFFDataInput;

    iget-object v4, v0, Lorg/apache/fontbox/cff/CFFParser;->topDictIndex:Lorg/apache/fontbox/cff/IndexData;

    invoke-virtual {v4, v1}, Lorg/apache/fontbox/cff/IndexData;->getBytes(I)[B

    move-result-object v1

    invoke-direct {v3, v1}, Lorg/apache/fontbox/cff/CFFDataInput;-><init>([B)V

    invoke-static {v3}, Lorg/apache/fontbox/cff/CFFParser;->readDictData(Lorg/apache/fontbox/cff/CFFDataInput;)Lorg/apache/fontbox/cff/CFFParser$DictData;

    move-result-object v1

    const-string v3, "SyntheticBase"

    invoke-virtual {v1, v3}, Lorg/apache/fontbox/cff/CFFParser$DictData;->getEntry(Ljava/lang/String;)Lorg/apache/fontbox/cff/CFFParser$DictData$Entry;

    move-result-object v3

    if-nez v3, :cond_b

    const-string v3, "ROS"

    invoke-virtual {v1, v3}, Lorg/apache/fontbox/cff/CFFParser$DictData;->getEntry(Ljava/lang/String;)Lorg/apache/fontbox/cff/CFFParser$DictData$Entry;

    move-result-object v4

    const/4 v5, 0x1

    const/4 v6, 0x0

    if-eqz v4, :cond_0

    move v4, v5

    goto :goto_0

    :cond_0
    move v4, v6

    :goto_0
    const/4 v7, 0x2

    if-eqz v4, :cond_1

    new-instance v8, Lorg/apache/fontbox/cff/CFFCIDFont;

    invoke-direct {v8}, Lorg/apache/fontbox/cff/CFFCIDFont;-><init>()V

    invoke-virtual {v1, v3}, Lorg/apache/fontbox/cff/CFFParser$DictData;->getEntry(Ljava/lang/String;)Lorg/apache/fontbox/cff/CFFParser$DictData$Entry;

    move-result-object v3

    invoke-virtual {v3, v6}, Lorg/apache/fontbox/cff/CFFParser$DictData$Entry;->getNumber(I)Ljava/lang/Number;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Number;->intValue()I

    move-result v9

    invoke-direct {v0, v9}, Lorg/apache/fontbox/cff/CFFParser;->readString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lorg/apache/fontbox/cff/CFFCIDFont;->setRegistry(Ljava/lang/String;)V

    invoke-virtual {v3, v5}, Lorg/apache/fontbox/cff/CFFParser$DictData$Entry;->getNumber(I)Ljava/lang/Number;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Number;->intValue()I

    move-result v9

    invoke-direct {v0, v9}, Lorg/apache/fontbox/cff/CFFParser;->readString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Lorg/apache/fontbox/cff/CFFCIDFont;->setOrdering(Ljava/lang/String;)V

    invoke-virtual {v3, v7}, Lorg/apache/fontbox/cff/CFFParser$DictData$Entry;->getNumber(I)Ljava/lang/Number;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Number;->intValue()I

    move-result v3

    invoke-virtual {v8, v3}, Lorg/apache/fontbox/cff/CFFCIDFont;->setSupplement(I)V

    goto :goto_1

    :cond_1
    new-instance v8, Lorg/apache/fontbox/cff/CFFType1Font;

    invoke-direct {v8}, Lorg/apache/fontbox/cff/CFFType1Font;-><init>()V

    :goto_1
    iput-object v2, v0, Lorg/apache/fontbox/cff/CFFParser;->debugFontName:Ljava/lang/String;

    invoke-virtual {v8, v2}, Lorg/apache/fontbox/cff/CFFFont;->setName(Ljava/lang/String;)V

    const-string v2, "version"

    invoke-direct {v0, v1, v2}, Lorg/apache/fontbox/cff/CFFParser;->getString(Lorg/apache/fontbox/cff/CFFParser$DictData;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v8, v2, v3}, Lorg/apache/fontbox/cff/CFFFont;->addValueToTopDict(Ljava/lang/String;Ljava/lang/Object;)V

    const-string v2, "Notice"

    invoke-direct {v0, v1, v2}, Lorg/apache/fontbox/cff/CFFParser;->getString(Lorg/apache/fontbox/cff/CFFParser$DictData;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v8, v2, v3}, Lorg/apache/fontbox/cff/CFFFont;->addValueToTopDict(Ljava/lang/String;Ljava/lang/Object;)V

    const-string v2, "Copyright"

    invoke-direct {v0, v1, v2}, Lorg/apache/fontbox/cff/CFFParser;->getString(Lorg/apache/fontbox/cff/CFFParser$DictData;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v8, v2, v3}, Lorg/apache/fontbox/cff/CFFFont;->addValueToTopDict(Ljava/lang/String;Ljava/lang/Object;)V

    const-string v2, "FullName"

    invoke-direct {v0, v1, v2}, Lorg/apache/fontbox/cff/CFFParser;->getString(Lorg/apache/fontbox/cff/CFFParser$DictData;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v8, v2, v3}, Lorg/apache/fontbox/cff/CFFFont;->addValueToTopDict(Ljava/lang/String;Ljava/lang/Object;)V

    const-string v2, "FamilyName"

    invoke-direct {v0, v1, v2}, Lorg/apache/fontbox/cff/CFFParser;->getString(Lorg/apache/fontbox/cff/CFFParser$DictData;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v8, v2, v3}, Lorg/apache/fontbox/cff/CFFFont;->addValueToTopDict(Ljava/lang/String;Ljava/lang/Object;)V

    const-string v2, "Weight"

    invoke-direct {v0, v1, v2}, Lorg/apache/fontbox/cff/CFFParser;->getString(Lorg/apache/fontbox/cff/CFFParser$DictData;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v8, v2, v3}, Lorg/apache/fontbox/cff/CFFFont;->addValueToTopDict(Ljava/lang/String;Ljava/lang/Object;)V

    const-string v2, "isFixedPitch"

    invoke-static {v1, v2, v6}, Lorg/apache/fontbox/cff/CFFParser;->getBoolean(Lorg/apache/fontbox/cff/CFFParser$DictData;Ljava/lang/String;Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v8, v2, v3}, Lorg/apache/fontbox/cff/CFFFont;->addValueToTopDict(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const-string v3, "ItalicAngle"

    invoke-static {v1, v3, v2}, Lorg/apache/fontbox/cff/CFFParser;->getNumber(Lorg/apache/fontbox/cff/CFFParser$DictData;Ljava/lang/String;Ljava/lang/Number;)Ljava/lang/Number;

    move-result-object v2

    invoke-virtual {v8, v3, v2}, Lorg/apache/fontbox/cff/CFFFont;->addValueToTopDict(Ljava/lang/String;Ljava/lang/Object;)V

    const/16 v2, -0x64

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const-string v3, "UnderlinePosition"

    invoke-static {v1, v3, v2}, Lorg/apache/fontbox/cff/CFFParser;->getNumber(Lorg/apache/fontbox/cff/CFFParser$DictData;Ljava/lang/String;Ljava/lang/Number;)Ljava/lang/Number;

    move-result-object v2

    invoke-virtual {v8, v3, v2}, Lorg/apache/fontbox/cff/CFFFont;->addValueToTopDict(Ljava/lang/String;Ljava/lang/Object;)V

    const/16 v2, 0x32

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const-string v3, "UnderlineThickness"

    invoke-static {v1, v3, v2}, Lorg/apache/fontbox/cff/CFFParser;->getNumber(Lorg/apache/fontbox/cff/CFFParser$DictData;Ljava/lang/String;Ljava/lang/Number;)Ljava/lang/Number;

    move-result-object v2

    invoke-virtual {v8, v3, v2}, Lorg/apache/fontbox/cff/CFFFont;->addValueToTopDict(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const-string v3, "PaintType"

    invoke-static {v1, v3, v2}, Lorg/apache/fontbox/cff/CFFParser;->getNumber(Lorg/apache/fontbox/cff/CFFParser$DictData;Ljava/lang/String;Ljava/lang/Number;)Ljava/lang/Number;

    move-result-object v2

    invoke-virtual {v8, v3, v2}, Lorg/apache/fontbox/cff/CFFFont;->addValueToTopDict(Ljava/lang/String;Ljava/lang/Object;)V

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const-string v3, "CharstringType"

    invoke-static {v1, v3, v2}, Lorg/apache/fontbox/cff/CFFParser;->getNumber(Lorg/apache/fontbox/cff/CFFParser$DictData;Ljava/lang/String;Ljava/lang/Number;)Ljava/lang/Number;

    move-result-object v2

    invoke-virtual {v8, v3, v2}, Lorg/apache/fontbox/cff/CFFFont;->addValueToTopDict(Ljava/lang/String;Ljava/lang/Object;)V

    const/4 v2, 0x6

    new-array v3, v2, [Ljava/lang/Number;

    const-wide v9, 0x3f50624dd2f1a9fcL    # 0.001

    invoke-static {v9, v10}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v11

    aput-object v11, v3, v6

    const-wide/16 v11, 0x0

    invoke-static {v11, v12}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v13

    aput-object v13, v3, v5

    invoke-static {v11, v12}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v13

    aput-object v13, v3, v7

    invoke-static {v9, v10}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v13

    const/4 v14, 0x3

    aput-object v13, v3, v14

    invoke-static {v11, v12}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v13

    const/4 v15, 0x4

    aput-object v13, v3, v15

    const/4 v13, 0x5

    invoke-static {v11, v12}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v16

    aput-object v16, v3, v13

    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    const-string v13, "FontMatrix"

    invoke-static {v1, v13, v3}, Lorg/apache/fontbox/cff/CFFParser;->getArray(Lorg/apache/fontbox/cff/CFFParser$DictData;Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    invoke-virtual {v8, v13, v3}, Lorg/apache/fontbox/cff/CFFFont;->addValueToTopDict(Ljava/lang/String;Ljava/lang/Object;)V

    const-string v3, "UniqueID"

    const/4 v11, 0x0

    invoke-static {v1, v3, v11}, Lorg/apache/fontbox/cff/CFFParser;->getNumber(Lorg/apache/fontbox/cff/CFFParser$DictData;Ljava/lang/String;Ljava/lang/Number;)Ljava/lang/Number;

    move-result-object v3

    const-string v11, "UniqueID"

    invoke-virtual {v8, v11, v3}, Lorg/apache/fontbox/cff/CFFFont;->addValueToTopDict(Ljava/lang/String;Ljava/lang/Object;)V

    new-array v3, v15, [Ljava/lang/Number;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v3, v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v3, v5

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v3, v7

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v3, v14

    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    const-string v11, "FontBBox"

    invoke-static {v1, v11, v3}, Lorg/apache/fontbox/cff/CFFParser;->getArray(Lorg/apache/fontbox/cff/CFFParser$DictData;Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    const-string v11, "FontBBox"

    invoke-virtual {v8, v11, v3}, Lorg/apache/fontbox/cff/CFFFont;->addValueToTopDict(Ljava/lang/String;Ljava/lang/Object;)V

    const-string v3, "StrokeWidth"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-static {v1, v3, v11}, Lorg/apache/fontbox/cff/CFFParser;->getNumber(Lorg/apache/fontbox/cff/CFFParser$DictData;Ljava/lang/String;Ljava/lang/Number;)Ljava/lang/Number;

    move-result-object v3

    const-string v11, "StrokeWidth"

    invoke-virtual {v8, v11, v3}, Lorg/apache/fontbox/cff/CFFFont;->addValueToTopDict(Ljava/lang/String;Ljava/lang/Object;)V

    const-string v3, "XUID"

    const/4 v11, 0x0

    invoke-static {v1, v3, v11}, Lorg/apache/fontbox/cff/CFFParser;->getArray(Lorg/apache/fontbox/cff/CFFParser$DictData;Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    const-string v11, "XUID"

    invoke-virtual {v8, v11, v3}, Lorg/apache/fontbox/cff/CFFFont;->addValueToTopDict(Ljava/lang/String;Ljava/lang/Object;)V

    const-string v3, "CharStrings"

    invoke-virtual {v1, v3}, Lorg/apache/fontbox/cff/CFFParser$DictData;->getEntry(Ljava/lang/String;)Lorg/apache/fontbox/cff/CFFParser$DictData$Entry;

    move-result-object v3

    invoke-virtual {v3, v6}, Lorg/apache/fontbox/cff/CFFParser$DictData$Entry;->getNumber(I)Ljava/lang/Number;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Number;->intValue()I

    move-result v3

    iget-object v11, v0, Lorg/apache/fontbox/cff/CFFParser;->input:Lorg/apache/fontbox/cff/CFFDataInput;

    invoke-virtual {v11, v3}, Lorg/apache/fontbox/cff/DataInput;->setPosition(I)V

    iget-object v3, v0, Lorg/apache/fontbox/cff/CFFParser;->input:Lorg/apache/fontbox/cff/CFFDataInput;

    invoke-static {v3}, Lorg/apache/fontbox/cff/CFFParser;->readIndexData(Lorg/apache/fontbox/cff/CFFDataInput;)Lorg/apache/fontbox/cff/IndexData;

    move-result-object v3

    const-string v11, "charset"

    invoke-virtual {v1, v11}, Lorg/apache/fontbox/cff/CFFParser$DictData;->getEntry(Ljava/lang/String;)Lorg/apache/fontbox/cff/CFFParser$DictData$Entry;

    move-result-object v11

    if-eqz v11, :cond_5

    invoke-virtual {v11, v6}, Lorg/apache/fontbox/cff/CFFParser$DictData$Entry;->getNumber(I)Ljava/lang/Number;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/Number;->intValue()I

    move-result v11

    if-nez v4, :cond_2

    if-nez v11, :cond_2

    goto :goto_2

    :cond_2
    if-nez v4, :cond_3

    if-ne v11, v5, :cond_3

    invoke-static {}, Lorg/apache/fontbox/cff/CFFExpertCharset;->getInstance()Lorg/apache/fontbox/cff/CFFExpertCharset;

    move-result-object v11

    goto :goto_3

    :cond_3
    if-nez v4, :cond_4

    if-ne v11, v7, :cond_4

    invoke-static {}, Lorg/apache/fontbox/cff/CFFExpertSubsetCharset;->getInstance()Lorg/apache/fontbox/cff/CFFExpertSubsetCharset;

    move-result-object v11

    goto :goto_3

    :cond_4
    iget-object v12, v0, Lorg/apache/fontbox/cff/CFFParser;->input:Lorg/apache/fontbox/cff/CFFDataInput;

    invoke-virtual {v12, v11}, Lorg/apache/fontbox/cff/DataInput;->setPosition(I)V

    iget-object v11, v0, Lorg/apache/fontbox/cff/CFFParser;->input:Lorg/apache/fontbox/cff/CFFDataInput;

    invoke-virtual {v3}, Lorg/apache/fontbox/cff/IndexData;->getCount()I

    move-result v12

    invoke-direct {v0, v11, v12, v4}, Lorg/apache/fontbox/cff/CFFParser;->readCharset(Lorg/apache/fontbox/cff/CFFDataInput;IZ)Lorg/apache/fontbox/cff/CFFCharset;

    move-result-object v11

    goto :goto_3

    :cond_5
    if-eqz v4, :cond_6

    new-instance v11, Lorg/apache/fontbox/cff/CFFParser$EmptyCharset;

    invoke-virtual {v3}, Lorg/apache/fontbox/cff/IndexData;->getCount()I

    move-result v12

    invoke-direct {v11, v12}, Lorg/apache/fontbox/cff/CFFParser$EmptyCharset;-><init>(I)V

    goto :goto_3

    :cond_6
    :goto_2
    invoke-static {}, Lorg/apache/fontbox/cff/CFFISOAdobeCharset;->getInstance()Lorg/apache/fontbox/cff/CFFISOAdobeCharset;

    move-result-object v11

    :goto_3
    invoke-virtual {v8, v11}, Lorg/apache/fontbox/cff/CFFFont;->setCharset(Lorg/apache/fontbox/cff/CFFCharset;)V

    invoke-virtual {v8}, Lorg/apache/fontbox/cff/CFFFont;->getCharStringBytes()Ljava/util/List;

    move-result-object v12

    invoke-virtual {v3, v6}, Lorg/apache/fontbox/cff/IndexData;->getBytes(I)[B

    move-result-object v15

    invoke-interface {v12, v15}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v12, v5

    :goto_4
    invoke-virtual {v3}, Lorg/apache/fontbox/cff/IndexData;->getCount()I

    move-result v15

    if-ge v12, v15, :cond_7

    invoke-virtual {v3, v12}, Lorg/apache/fontbox/cff/IndexData;->getBytes(I)[B

    move-result-object v15

    invoke-virtual {v8}, Lorg/apache/fontbox/cff/CFFFont;->getCharStringBytes()Ljava/util/List;

    move-result-object v14

    invoke-interface {v14, v15}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v12, v12, 0x1

    const/4 v14, 0x3

    goto :goto_4

    :cond_7
    if-eqz v4, :cond_9

    move-object v4, v8

    check-cast v4, Lorg/apache/fontbox/cff/CFFCIDFont;

    invoke-direct {v0, v1, v4, v3}, Lorg/apache/fontbox/cff/CFFParser;->parseCIDFontDicts(Lorg/apache/fontbox/cff/CFFParser$DictData;Lorg/apache/fontbox/cff/CFFCIDFont;Lorg/apache/fontbox/cff/IndexData;)V

    invoke-virtual {v1, v13}, Lorg/apache/fontbox/cff/CFFParser$DictData;->getEntry(Ljava/lang/String;)Lorg/apache/fontbox/cff/CFFParser$DictData$Entry;

    move-result-object v3

    if-nez v3, :cond_a

    invoke-virtual {v4}, Lorg/apache/fontbox/cff/CFFCIDFont;->getFontDicts()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_8

    invoke-interface {v3, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Map;

    invoke-interface {v4, v13}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_8

    invoke-interface {v3, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map;

    invoke-interface {v1, v13}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    goto :goto_5

    :cond_8
    new-array v2, v2, [Ljava/lang/Number;

    invoke-static {v9, v10}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    aput-object v3, v2, v6

    const-wide/16 v3, 0x0

    invoke-static {v3, v4}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v6

    aput-object v6, v2, v5

    invoke-static {v3, v4}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    aput-object v5, v2, v7

    invoke-static {v9, v10}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    const/4 v6, 0x3

    aput-object v5, v2, v6

    invoke-static {v3, v4}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    const/4 v6, 0x4

    aput-object v5, v2, v6

    const/4 v5, 0x5

    invoke-static {v3, v4}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-static {v1, v13, v2}, Lorg/apache/fontbox/cff/CFFParser;->getArray(Lorg/apache/fontbox/cff/CFFParser$DictData;Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    :goto_5
    invoke-virtual {v8, v13, v1}, Lorg/apache/fontbox/cff/CFFFont;->addValueToTopDict(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_6

    :cond_9
    move-object v2, v8

    check-cast v2, Lorg/apache/fontbox/cff/CFFType1Font;

    invoke-direct {v0, v1, v2, v11}, Lorg/apache/fontbox/cff/CFFParser;->parseType1Dicts(Lorg/apache/fontbox/cff/CFFParser$DictData;Lorg/apache/fontbox/cff/CFFType1Font;Lorg/apache/fontbox/cff/CFFCharset;)V

    :cond_a
    :goto_6
    return-object v8

    :cond_b
    new-instance v1, Ljava/io/IOException;

    const-string v2, "Synthetic Fonts are not supported"

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private parseType1Dicts(Lorg/apache/fontbox/cff/CFFParser$DictData;Lorg/apache/fontbox/cff/CFFType1Font;Lorg/apache/fontbox/cff/CFFCharset;)V
    .locals 4

    const-string v0, "Encoding"

    invoke-virtual {p1, v0}, Lorg/apache/fontbox/cff/CFFParser$DictData;->getEntry(Ljava/lang/String;)Lorg/apache/fontbox/cff/CFFParser$DictData$Entry;

    move-result-object v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    invoke-virtual {v0, v1}, Lorg/apache/fontbox/cff/CFFParser$DictData$Entry;->getNumber(I)Ljava/lang/Number;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v0

    goto :goto_0

    :cond_0
    move v0, v1

    :goto_0
    const/4 v2, 0x1

    if-nez v0, :cond_1

    invoke-static {}, Lorg/apache/fontbox/cff/CFFStandardEncoding;->getInstance()Lorg/apache/fontbox/cff/CFFStandardEncoding;

    move-result-object p3

    goto :goto_1

    :cond_1
    if-ne v0, v2, :cond_2

    invoke-static {}, Lorg/apache/fontbox/cff/CFFExpertEncoding;->getInstance()Lorg/apache/fontbox/cff/CFFExpertEncoding;

    move-result-object p3

    goto :goto_1

    :cond_2
    iget-object v3, p0, Lorg/apache/fontbox/cff/CFFParser;->input:Lorg/apache/fontbox/cff/CFFDataInput;

    invoke-virtual {v3, v0}, Lorg/apache/fontbox/cff/DataInput;->setPosition(I)V

    iget-object v0, p0, Lorg/apache/fontbox/cff/CFFParser;->input:Lorg/apache/fontbox/cff/CFFDataInput;

    invoke-direct {p0, v0, p3}, Lorg/apache/fontbox/cff/CFFParser;->readEncoding(Lorg/apache/fontbox/cff/CFFDataInput;Lorg/apache/fontbox/cff/CFFCharset;)Lorg/apache/fontbox/cff/CFFEncoding;

    move-result-object p3

    :goto_1
    invoke-virtual {p2, p3}, Lorg/apache/fontbox/cff/CFFType1Font;->setEncoding(Lorg/apache/fontbox/cff/CFFEncoding;)V

    const-string p3, "Private"

    invoke-virtual {p1, p3}, Lorg/apache/fontbox/cff/CFFParser$DictData;->getEntry(Ljava/lang/String;)Lorg/apache/fontbox/cff/CFFParser$DictData$Entry;

    move-result-object p1

    invoke-virtual {p1, v2}, Lorg/apache/fontbox/cff/CFFParser$DictData$Entry;->getNumber(I)Ljava/lang/Number;

    move-result-object p3

    invoke-virtual {p3}, Ljava/lang/Number;->intValue()I

    move-result p3

    iget-object v0, p0, Lorg/apache/fontbox/cff/CFFParser;->input:Lorg/apache/fontbox/cff/CFFDataInput;

    invoke-virtual {v0, p3}, Lorg/apache/fontbox/cff/DataInput;->setPosition(I)V

    invoke-virtual {p1, v1}, Lorg/apache/fontbox/cff/CFFParser$DictData$Entry;->getNumber(I)Ljava/lang/Number;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Number;->intValue()I

    move-result p1

    new-instance v0, Lorg/apache/fontbox/cff/CFFDataInput;

    iget-object v2, p0, Lorg/apache/fontbox/cff/CFFParser;->input:Lorg/apache/fontbox/cff/CFFDataInput;

    invoke-virtual {v2, p1}, Lorg/apache/fontbox/cff/DataInput;->readBytes(I)[B

    move-result-object p1

    invoke-direct {v0, p1}, Lorg/apache/fontbox/cff/CFFDataInput;-><init>([B)V

    invoke-static {v0}, Lorg/apache/fontbox/cff/CFFParser;->readDictData(Lorg/apache/fontbox/cff/CFFDataInput;)Lorg/apache/fontbox/cff/CFFParser$DictData;

    move-result-object p1

    invoke-direct {p0, p1}, Lorg/apache/fontbox/cff/CFFParser;->readPrivateDict(Lorg/apache/fontbox/cff/CFFParser$DictData;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p2, v3, v2}, Lorg/apache/fontbox/cff/CFFType1Font;->addToPrivateDict(Ljava/lang/String;Ljava/lang/Object;)V

    goto :goto_2

    :cond_3
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v2, "Subrs"

    invoke-static {p1, v2, v0}, Lorg/apache/fontbox/cff/CFFParser;->getNumber(Lorg/apache/fontbox/cff/CFFParser$DictData;Ljava/lang/String;Ljava/lang/Number;)Ljava/lang/Number;

    move-result-object p1

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    if-nez p1, :cond_4

    new-instance p1, Lorg/apache/fontbox/cff/IndexData;

    invoke-direct {p1, v1}, Lorg/apache/fontbox/cff/IndexData;-><init>(I)V

    goto :goto_3

    :cond_4
    iget-object v0, p0, Lorg/apache/fontbox/cff/CFFParser;->input:Lorg/apache/fontbox/cff/CFFDataInput;

    add-int/2addr p3, p1

    invoke-virtual {v0, p3}, Lorg/apache/fontbox/cff/DataInput;->setPosition(I)V

    iget-object p1, p0, Lorg/apache/fontbox/cff/CFFParser;->input:Lorg/apache/fontbox/cff/CFFDataInput;

    invoke-static {p1}, Lorg/apache/fontbox/cff/CFFParser;->readIndexData(Lorg/apache/fontbox/cff/CFFDataInput;)Lorg/apache/fontbox/cff/IndexData;

    move-result-object p1

    :goto_3
    invoke-virtual {p2, v2, p1}, Lorg/apache/fontbox/cff/CFFType1Font;->addToPrivateDict(Ljava/lang/String;Ljava/lang/Object;)V

    return-void
.end method

.method private readCharset(Lorg/apache/fontbox/cff/CFFDataInput;IZ)Lorg/apache/fontbox/cff/CFFCharset;
    .locals 2

    invoke-virtual {p1}, Lorg/apache/fontbox/cff/CFFDataInput;->readCard8()I

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0, p1, v0, p2, p3}, Lorg/apache/fontbox/cff/CFFParser;->readFormat0Charset(Lorg/apache/fontbox/cff/CFFDataInput;IIZ)Lorg/apache/fontbox/cff/CFFParser$Format0Charset;

    move-result-object p1

    return-object p1

    :cond_0
    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    invoke-direct {p0, p1, v0, p2, p3}, Lorg/apache/fontbox/cff/CFFParser;->readFormat1Charset(Lorg/apache/fontbox/cff/CFFDataInput;IIZ)Lorg/apache/fontbox/cff/CFFParser$Format1Charset;

    move-result-object p1

    return-object p1

    :cond_1
    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    invoke-direct {p0, p1, v0, p2, p3}, Lorg/apache/fontbox/cff/CFFParser;->readFormat2Charset(Lorg/apache/fontbox/cff/CFFDataInput;IIZ)Lorg/apache/fontbox/cff/CFFParser$Format2Charset;

    move-result-object p1

    return-object p1

    :cond_2
    new-instance p1, Ljava/lang/IllegalArgumentException;

    invoke-direct {p1}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw p1
.end method

.method private static readDictData(Lorg/apache/fontbox/cff/CFFDataInput;)Lorg/apache/fontbox/cff/CFFParser$DictData;
    .locals 3

    new-instance v0, Lorg/apache/fontbox/cff/CFFParser$DictData;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lorg/apache/fontbox/cff/CFFParser$DictData;-><init>(Lorg/apache/fontbox/cff/CFFParser$1;)V

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v0, v1}, Lorg/apache/fontbox/cff/CFFParser$DictData;->access$602(Lorg/apache/fontbox/cff/CFFParser$DictData;Ljava/util/List;)Ljava/util/List;

    :goto_0
    invoke-virtual {p0}, Lorg/apache/fontbox/cff/DataInput;->hasRemaining()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, Lorg/apache/fontbox/cff/CFFParser;->readEntry(Lorg/apache/fontbox/cff/CFFDataInput;)Lorg/apache/fontbox/cff/CFFParser$DictData$Entry;

    move-result-object v1

    invoke-static {v0}, Lorg/apache/fontbox/cff/CFFParser$DictData;->access$600(Lorg/apache/fontbox/cff/CFFParser$DictData;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method private readEncoding(Lorg/apache/fontbox/cff/CFFDataInput;Lorg/apache/fontbox/cff/CFFCharset;)Lorg/apache/fontbox/cff/CFFEncoding;
    .locals 3

    invoke-virtual {p1}, Lorg/apache/fontbox/cff/CFFDataInput;->readCard8()I

    move-result v0

    and-int/lit8 v1, v0, 0x7f

    if-nez v1, :cond_0

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/fontbox/cff/CFFParser;->readFormat0Encoding(Lorg/apache/fontbox/cff/CFFDataInput;Lorg/apache/fontbox/cff/CFFCharset;I)Lorg/apache/fontbox/cff/CFFParser$Format0Encoding;

    move-result-object p1

    return-object p1

    :cond_0
    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/fontbox/cff/CFFParser;->readFormat1Encoding(Lorg/apache/fontbox/cff/CFFDataInput;Lorg/apache/fontbox/cff/CFFCharset;I)Lorg/apache/fontbox/cff/CFFParser$Format1Encoding;

    move-result-object p1

    return-object p1

    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    invoke-direct {p1}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw p1
.end method

.method private static readEntry(Lorg/apache/fontbox/cff/CFFDataInput;)Lorg/apache/fontbox/cff/CFFParser$DictData$Entry;
    .locals 3

    new-instance v0, Lorg/apache/fontbox/cff/CFFParser$DictData$Entry;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lorg/apache/fontbox/cff/CFFParser$DictData$Entry;-><init>(Lorg/apache/fontbox/cff/CFFParser$1;)V

    :goto_0
    invoke-virtual {p0}, Lorg/apache/fontbox/cff/DataInput;->readUnsignedByte()I

    move-result v1

    if-ltz v1, :cond_0

    const/16 v2, 0x15

    if-gt v1, v2, :cond_0

    invoke-static {p0, v1}, Lorg/apache/fontbox/cff/CFFParser;->readOperator(Lorg/apache/fontbox/cff/CFFDataInput;I)Lorg/apache/fontbox/cff/CFFOperator;

    move-result-object p0

    invoke-static {v0, p0}, Lorg/apache/fontbox/cff/CFFParser$DictData$Entry;->access$802(Lorg/apache/fontbox/cff/CFFParser$DictData$Entry;Lorg/apache/fontbox/cff/CFFOperator;)Lorg/apache/fontbox/cff/CFFOperator;

    return-object v0

    :cond_0
    const/16 v2, 0x1c

    if-eq v1, v2, :cond_4

    const/16 v2, 0x1d

    if-ne v1, v2, :cond_1

    goto :goto_1

    :cond_1
    const/16 v2, 0x1e

    if-ne v1, v2, :cond_2

    invoke-static {v0}, Lorg/apache/fontbox/cff/CFFParser$DictData$Entry;->access$900(Lorg/apache/fontbox/cff/CFFParser$DictData$Entry;)Ljava/util/List;

    move-result-object v2

    invoke-static {p0, v1}, Lorg/apache/fontbox/cff/CFFParser;->readRealNumber(Lorg/apache/fontbox/cff/CFFDataInput;I)Ljava/lang/Double;

    move-result-object v1

    goto :goto_2

    :cond_2
    const/16 v2, 0x20

    if-lt v1, v2, :cond_3

    const/16 v2, 0xfe

    if-gt v1, v2, :cond_3

    goto :goto_1

    :cond_3
    new-instance p0, Ljava/lang/IllegalArgumentException;

    invoke-direct {p0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw p0

    :cond_4
    :goto_1
    invoke-static {v0}, Lorg/apache/fontbox/cff/CFFParser$DictData$Entry;->access$900(Lorg/apache/fontbox/cff/CFFParser$DictData$Entry;)Ljava/util/List;

    move-result-object v2

    invoke-static {p0, v1}, Lorg/apache/fontbox/cff/CFFParser;->readIntegerNumber(Lorg/apache/fontbox/cff/CFFDataInput;I)Ljava/lang/Integer;

    move-result-object v1

    :goto_2
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private static readFDSelect(Lorg/apache/fontbox/cff/CFFDataInput;ILorg/apache/fontbox/cff/CFFCIDFont;)Lorg/apache/fontbox/cff/FDSelect;
    .locals 2

    invoke-virtual {p0}, Lorg/apache/fontbox/cff/CFFDataInput;->readCard8()I

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0, v0, p1, p2}, Lorg/apache/fontbox/cff/CFFParser;->readFormat0FDSelect(Lorg/apache/fontbox/cff/CFFDataInput;IILorg/apache/fontbox/cff/CFFCIDFont;)Lorg/apache/fontbox/cff/CFFParser$Format0FDSelect;

    move-result-object p0

    return-object p0

    :cond_0
    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    invoke-static {p0, v0, p1, p2}, Lorg/apache/fontbox/cff/CFFParser;->readFormat3FDSelect(Lorg/apache/fontbox/cff/CFFDataInput;IILorg/apache/fontbox/cff/CFFCIDFont;)Lorg/apache/fontbox/cff/CFFParser$Format3FDSelect;

    move-result-object p0

    return-object p0

    :cond_1
    new-instance p0, Ljava/lang/IllegalArgumentException;

    invoke-direct {p0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw p0
.end method

.method private readFormat0Charset(Lorg/apache/fontbox/cff/CFFDataInput;IIZ)Lorg/apache/fontbox/cff/CFFParser$Format0Charset;
    .locals 2

    new-instance v0, Lorg/apache/fontbox/cff/CFFParser$Format0Charset;

    invoke-direct {v0, p4}, Lorg/apache/fontbox/cff/CFFParser$Format0Charset;-><init>(Z)V

    invoke-static {v0, p2}, Lorg/apache/fontbox/cff/CFFParser$Format0Charset;->access$3702(Lorg/apache/fontbox/cff/CFFParser$Format0Charset;I)I

    new-array p2, p3, [I

    invoke-static {v0, p2}, Lorg/apache/fontbox/cff/CFFParser$Format0Charset;->access$3802(Lorg/apache/fontbox/cff/CFFParser$Format0Charset;[I)[I

    invoke-static {v0}, Lorg/apache/fontbox/cff/CFFParser$Format0Charset;->access$3800(Lorg/apache/fontbox/cff/CFFParser$Format0Charset;)[I

    move-result-object p2

    const/4 p3, 0x0

    aput p3, p2, p3

    if-eqz p4, :cond_0

    invoke-virtual {v0, p3, p3}, Lorg/apache/fontbox/cff/CFFCharset;->addCID(II)V

    goto :goto_0

    :cond_0
    const-string p2, ".notdef"

    invoke-virtual {v0, p3, p3, p2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    :goto_0
    const/4 p2, 0x1

    :goto_1
    invoke-static {v0}, Lorg/apache/fontbox/cff/CFFParser$Format0Charset;->access$3800(Lorg/apache/fontbox/cff/CFFParser$Format0Charset;)[I

    move-result-object p3

    array-length p3, p3

    if-ge p2, p3, :cond_2

    invoke-virtual {p1}, Lorg/apache/fontbox/cff/CFFDataInput;->readSID()I

    move-result p3

    invoke-static {v0}, Lorg/apache/fontbox/cff/CFFParser$Format0Charset;->access$3800(Lorg/apache/fontbox/cff/CFFParser$Format0Charset;)[I

    move-result-object v1

    aput p3, v1, p2

    if-eqz p4, :cond_1

    invoke-virtual {v0, p2, p3}, Lorg/apache/fontbox/cff/CFFCharset;->addCID(II)V

    goto :goto_2

    :cond_1
    invoke-direct {p0, p3}, Lorg/apache/fontbox/cff/CFFParser;->readString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p2, p3, v1}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    :goto_2
    add-int/lit8 p2, p2, 0x1

    goto :goto_1

    :cond_2
    return-object v0
.end method

.method private readFormat0Encoding(Lorg/apache/fontbox/cff/CFFDataInput;Lorg/apache/fontbox/cff/CFFCharset;I)Lorg/apache/fontbox/cff/CFFParser$Format0Encoding;
    .locals 5

    new-instance v0, Lorg/apache/fontbox/cff/CFFParser$Format0Encoding;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lorg/apache/fontbox/cff/CFFParser$Format0Encoding;-><init>(Lorg/apache/fontbox/cff/CFFParser$1;)V

    invoke-static {v0, p3}, Lorg/apache/fontbox/cff/CFFParser$Format0Encoding;->access$1102(Lorg/apache/fontbox/cff/CFFParser$Format0Encoding;I)I

    invoke-virtual {p1}, Lorg/apache/fontbox/cff/CFFDataInput;->readCard8()I

    move-result v1

    invoke-static {v0, v1}, Lorg/apache/fontbox/cff/CFFParser$Format0Encoding;->access$1202(Lorg/apache/fontbox/cff/CFFParser$Format0Encoding;I)I

    invoke-static {v0}, Lorg/apache/fontbox/cff/CFFParser$Format0Encoding;->access$1200(Lorg/apache/fontbox/cff/CFFParser$Format0Encoding;)I

    move-result v1

    new-array v1, v1, [I

    invoke-static {v0, v1}, Lorg/apache/fontbox/cff/CFFParser$Format0Encoding;->access$1302(Lorg/apache/fontbox/cff/CFFParser$Format0Encoding;[I)[I

    const/4 v1, 0x0

    const-string v2, ".notdef"

    invoke-virtual {v0, v1, v1, v2}, Lorg/apache/fontbox/cff/CFFEncoding;->add(IILjava/lang/String;)V

    const/4 v1, 0x1

    :goto_0
    invoke-static {v0}, Lorg/apache/fontbox/cff/CFFParser$Format0Encoding;->access$1200(Lorg/apache/fontbox/cff/CFFParser$Format0Encoding;)I

    move-result v2

    if-gt v1, v2, :cond_0

    invoke-virtual {p1}, Lorg/apache/fontbox/cff/CFFDataInput;->readCard8()I

    move-result v2

    invoke-static {v0}, Lorg/apache/fontbox/cff/CFFParser$Format0Encoding;->access$1300(Lorg/apache/fontbox/cff/CFFParser$Format0Encoding;)[I

    move-result-object v3

    add-int/lit8 v4, v1, -0x1

    aput v2, v3, v4

    invoke-virtual {p2, v1}, Lorg/apache/fontbox/cff/CFFCharset;->getSIDForGID(I)I

    move-result v3

    invoke-direct {p0, v3}, Lorg/apache/fontbox/cff/CFFParser;->readString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v2, v3, v4}, Lorg/apache/fontbox/cff/CFFEncoding;->add(IILjava/lang/String;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    and-int/lit16 p2, p3, 0x80

    if-eqz p2, :cond_1

    invoke-direct {p0, p1, v0}, Lorg/apache/fontbox/cff/CFFParser;->readSupplement(Lorg/apache/fontbox/cff/CFFDataInput;Lorg/apache/fontbox/cff/CFFParser$EmbeddedEncoding;)V

    :cond_1
    return-object v0
.end method

.method private static readFormat0FDSelect(Lorg/apache/fontbox/cff/CFFDataInput;IILorg/apache/fontbox/cff/CFFCIDFont;)Lorg/apache/fontbox/cff/CFFParser$Format0FDSelect;
    .locals 2

    new-instance v0, Lorg/apache/fontbox/cff/CFFParser$Format0FDSelect;

    const/4 v1, 0x0

    invoke-direct {v0, p3, v1}, Lorg/apache/fontbox/cff/CFFParser$Format0FDSelect;-><init>(Lorg/apache/fontbox/cff/CFFCIDFont;Lorg/apache/fontbox/cff/CFFParser$1;)V

    invoke-static {v0, p1}, Lorg/apache/fontbox/cff/CFFParser$Format0FDSelect;->access$2702(Lorg/apache/fontbox/cff/CFFParser$Format0FDSelect;I)I

    new-array p1, p2, [I

    invoke-static {v0, p1}, Lorg/apache/fontbox/cff/CFFParser$Format0FDSelect;->access$2802(Lorg/apache/fontbox/cff/CFFParser$Format0FDSelect;[I)[I

    const/4 p1, 0x0

    :goto_0
    invoke-static {v0}, Lorg/apache/fontbox/cff/CFFParser$Format0FDSelect;->access$2800(Lorg/apache/fontbox/cff/CFFParser$Format0FDSelect;)[I

    move-result-object p2

    array-length p2, p2

    if-ge p1, p2, :cond_0

    invoke-static {v0}, Lorg/apache/fontbox/cff/CFFParser$Format0FDSelect;->access$2800(Lorg/apache/fontbox/cff/CFFParser$Format0FDSelect;)[I

    move-result-object p2

    invoke-virtual {p0}, Lorg/apache/fontbox/cff/CFFDataInput;->readCard8()I

    move-result p3

    aput p3, p2, p1

    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method private readFormat1Charset(Lorg/apache/fontbox/cff/CFFDataInput;IIZ)Lorg/apache/fontbox/cff/CFFParser$Format1Charset;
    .locals 9

    new-instance v0, Lorg/apache/fontbox/cff/CFFParser$Format1Charset;

    invoke-direct {v0, p4}, Lorg/apache/fontbox/cff/CFFParser$Format1Charset;-><init>(Z)V

    invoke-static {v0, p2}, Lorg/apache/fontbox/cff/CFFParser$Format1Charset;->access$3902(Lorg/apache/fontbox/cff/CFFParser$Format1Charset;I)I

    new-instance p2, Ljava/util/ArrayList;

    invoke-direct {p2}, Ljava/util/ArrayList;-><init>()V

    const/4 v1, 0x0

    if-eqz p4, :cond_0

    invoke-virtual {v0, v1, v1}, Lorg/apache/fontbox/cff/CFFCharset;->addCID(II)V

    goto :goto_0

    :cond_0
    const-string v2, ".notdef"

    invoke-virtual {v0, v1, v1, v2}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    :goto_0
    const/4 v2, 0x1

    move v3, v2

    :goto_1
    if-ge v3, p3, :cond_3

    new-instance v4, Lorg/apache/fontbox/cff/CFFParser$Format1Charset$Range1;

    const/4 v5, 0x0

    invoke-direct {v4, v5}, Lorg/apache/fontbox/cff/CFFParser$Format1Charset$Range1;-><init>(Lorg/apache/fontbox/cff/CFFParser$1;)V

    invoke-virtual {p1}, Lorg/apache/fontbox/cff/CFFDataInput;->readSID()I

    move-result v5

    invoke-static {v4, v5}, Lorg/apache/fontbox/cff/CFFParser$Format1Charset$Range1;->access$4102(Lorg/apache/fontbox/cff/CFFParser$Format1Charset$Range1;I)I

    invoke-virtual {p1}, Lorg/apache/fontbox/cff/CFFDataInput;->readCard8()I

    move-result v5

    invoke-static {v4, v5}, Lorg/apache/fontbox/cff/CFFParser$Format1Charset$Range1;->access$4202(Lorg/apache/fontbox/cff/CFFParser$Format1Charset$Range1;I)I

    invoke-interface {p2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v5, v1

    :goto_2
    invoke-static {v4}, Lorg/apache/fontbox/cff/CFFParser$Format1Charset$Range1;->access$4200(Lorg/apache/fontbox/cff/CFFParser$Format1Charset$Range1;)I

    move-result v6

    add-int/2addr v6, v2

    if-ge v5, v6, :cond_2

    invoke-static {v4}, Lorg/apache/fontbox/cff/CFFParser$Format1Charset$Range1;->access$4100(Lorg/apache/fontbox/cff/CFFParser$Format1Charset$Range1;)I

    move-result v6

    add-int/2addr v6, v5

    add-int v7, v3, v5

    if-eqz p4, :cond_1

    invoke-virtual {v0, v7, v6}, Lorg/apache/fontbox/cff/CFFCharset;->addCID(II)V

    goto :goto_3

    :cond_1
    invoke-direct {p0, v6}, Lorg/apache/fontbox/cff/CFFParser;->readString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v7, v6, v8}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    :goto_3
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    :cond_2
    invoke-static {v4}, Lorg/apache/fontbox/cff/CFFParser$Format1Charset$Range1;->access$4200(Lorg/apache/fontbox/cff/CFFParser$Format1Charset$Range1;)I

    move-result v4

    add-int/2addr v3, v4

    add-int/2addr v3, v2

    goto :goto_1

    :cond_3
    new-array p1, v1, [Lorg/apache/fontbox/cff/CFFParser$Format1Charset$Range1;

    invoke-interface {p2, p1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object p1

    check-cast p1, [Lorg/apache/fontbox/cff/CFFParser$Format1Charset$Range1;

    invoke-static {v0, p1}, Lorg/apache/fontbox/cff/CFFParser$Format1Charset;->access$4302(Lorg/apache/fontbox/cff/CFFParser$Format1Charset;[Lorg/apache/fontbox/cff/CFFParser$Format1Charset$Range1;)[Lorg/apache/fontbox/cff/CFFParser$Format1Charset$Range1;

    return-object v0
.end method

.method private readFormat1Encoding(Lorg/apache/fontbox/cff/CFFDataInput;Lorg/apache/fontbox/cff/CFFCharset;I)Lorg/apache/fontbox/cff/CFFParser$Format1Encoding;
    .locals 11

    new-instance v0, Lorg/apache/fontbox/cff/CFFParser$Format1Encoding;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lorg/apache/fontbox/cff/CFFParser$Format1Encoding;-><init>(Lorg/apache/fontbox/cff/CFFParser$1;)V

    invoke-static {v0, p3}, Lorg/apache/fontbox/cff/CFFParser$Format1Encoding;->access$1502(Lorg/apache/fontbox/cff/CFFParser$Format1Encoding;I)I

    invoke-virtual {p1}, Lorg/apache/fontbox/cff/CFFDataInput;->readCard8()I

    move-result v2

    invoke-static {v0, v2}, Lorg/apache/fontbox/cff/CFFParser$Format1Encoding;->access$1602(Lorg/apache/fontbox/cff/CFFParser$Format1Encoding;I)I

    invoke-static {v0}, Lorg/apache/fontbox/cff/CFFParser$Format1Encoding;->access$1600(Lorg/apache/fontbox/cff/CFFParser$Format1Encoding;)I

    move-result v2

    new-array v2, v2, [Lorg/apache/fontbox/cff/CFFParser$Format1Encoding$Range1;

    invoke-static {v0, v2}, Lorg/apache/fontbox/cff/CFFParser$Format1Encoding;->access$1702(Lorg/apache/fontbox/cff/CFFParser$Format1Encoding;[Lorg/apache/fontbox/cff/CFFParser$Format1Encoding$Range1;)[Lorg/apache/fontbox/cff/CFFParser$Format1Encoding$Range1;

    const-string v2, ".notdef"

    const/4 v3, 0x0

    invoke-virtual {v0, v3, v3, v2}, Lorg/apache/fontbox/cff/CFFEncoding;->add(IILjava/lang/String;)V

    const/4 v2, 0x1

    move v5, v2

    move v4, v3

    :goto_0
    invoke-static {v0}, Lorg/apache/fontbox/cff/CFFParser$Format1Encoding;->access$1700(Lorg/apache/fontbox/cff/CFFParser$Format1Encoding;)[Lorg/apache/fontbox/cff/CFFParser$Format1Encoding$Range1;

    move-result-object v6

    array-length v6, v6

    if-ge v4, v6, :cond_1

    new-instance v6, Lorg/apache/fontbox/cff/CFFParser$Format1Encoding$Range1;

    invoke-direct {v6, v1}, Lorg/apache/fontbox/cff/CFFParser$Format1Encoding$Range1;-><init>(Lorg/apache/fontbox/cff/CFFParser$1;)V

    invoke-virtual {p1}, Lorg/apache/fontbox/cff/CFFDataInput;->readCard8()I

    move-result v7

    invoke-static {v6, v7}, Lorg/apache/fontbox/cff/CFFParser$Format1Encoding$Range1;->access$1902(Lorg/apache/fontbox/cff/CFFParser$Format1Encoding$Range1;I)I

    invoke-virtual {p1}, Lorg/apache/fontbox/cff/CFFDataInput;->readCard8()I

    move-result v7

    invoke-static {v6, v7}, Lorg/apache/fontbox/cff/CFFParser$Format1Encoding$Range1;->access$2002(Lorg/apache/fontbox/cff/CFFParser$Format1Encoding$Range1;I)I

    invoke-static {v0}, Lorg/apache/fontbox/cff/CFFParser$Format1Encoding;->access$1700(Lorg/apache/fontbox/cff/CFFParser$Format1Encoding;)[Lorg/apache/fontbox/cff/CFFParser$Format1Encoding$Range1;

    move-result-object v7

    aput-object v6, v7, v4

    move v7, v3

    :goto_1
    invoke-static {v6}, Lorg/apache/fontbox/cff/CFFParser$Format1Encoding$Range1;->access$2000(Lorg/apache/fontbox/cff/CFFParser$Format1Encoding$Range1;)I

    move-result v8

    add-int/2addr v8, v2

    if-ge v7, v8, :cond_0

    invoke-virtual {p2, v5}, Lorg/apache/fontbox/cff/CFFCharset;->getSIDForGID(I)I

    move-result v8

    invoke-static {v6}, Lorg/apache/fontbox/cff/CFFParser$Format1Encoding$Range1;->access$1900(Lorg/apache/fontbox/cff/CFFParser$Format1Encoding$Range1;)I

    move-result v9

    add-int/2addr v9, v7

    invoke-direct {p0, v8}, Lorg/apache/fontbox/cff/CFFParser;->readString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0, v9, v8, v10}, Lorg/apache/fontbox/cff/CFFEncoding;->add(IILjava/lang/String;)V

    add-int/lit8 v5, v5, 0x1

    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    :cond_0
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_1
    and-int/lit16 p2, p3, 0x80

    if-eqz p2, :cond_2

    invoke-direct {p0, p1, v0}, Lorg/apache/fontbox/cff/CFFParser;->readSupplement(Lorg/apache/fontbox/cff/CFFDataInput;Lorg/apache/fontbox/cff/CFFParser$EmbeddedEncoding;)V

    :cond_2
    return-object v0
.end method

.method private readFormat2Charset(Lorg/apache/fontbox/cff/CFFDataInput;IIZ)Lorg/apache/fontbox/cff/CFFParser$Format2Charset;
    .locals 8

    new-instance v0, Lorg/apache/fontbox/cff/CFFParser$Format2Charset;

    invoke-direct {v0, p4}, Lorg/apache/fontbox/cff/CFFParser$Format2Charset;-><init>(Z)V

    invoke-static {v0, p2}, Lorg/apache/fontbox/cff/CFFParser$Format2Charset;->access$4402(Lorg/apache/fontbox/cff/CFFParser$Format2Charset;I)I

    const/4 p2, 0x0

    new-array v1, p2, [Lorg/apache/fontbox/cff/CFFParser$Format2Charset$Range2;

    invoke-static {v0, v1}, Lorg/apache/fontbox/cff/CFFParser$Format2Charset;->access$4502(Lorg/apache/fontbox/cff/CFFParser$Format2Charset;[Lorg/apache/fontbox/cff/CFFParser$Format2Charset$Range2;)[Lorg/apache/fontbox/cff/CFFParser$Format2Charset$Range2;

    if-eqz p4, :cond_0

    invoke-virtual {v0, p2, p2}, Lorg/apache/fontbox/cff/CFFCharset;->addCID(II)V

    goto :goto_0

    :cond_0
    const-string v1, ".notdef"

    invoke-virtual {v0, p2, p2, v1}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    :goto_0
    const/4 v1, 0x1

    move v2, v1

    :goto_1
    if-ge v2, p3, :cond_3

    invoke-static {v0}, Lorg/apache/fontbox/cff/CFFParser$Format2Charset;->access$4500(Lorg/apache/fontbox/cff/CFFParser$Format2Charset;)[Lorg/apache/fontbox/cff/CFFParser$Format2Charset$Range2;

    move-result-object v3

    array-length v3, v3

    add-int/2addr v3, v1

    new-array v3, v3, [Lorg/apache/fontbox/cff/CFFParser$Format2Charset$Range2;

    invoke-static {v0}, Lorg/apache/fontbox/cff/CFFParser$Format2Charset;->access$4500(Lorg/apache/fontbox/cff/CFFParser$Format2Charset;)[Lorg/apache/fontbox/cff/CFFParser$Format2Charset$Range2;

    move-result-object v4

    invoke-static {v0}, Lorg/apache/fontbox/cff/CFFParser$Format2Charset;->access$4500(Lorg/apache/fontbox/cff/CFFParser$Format2Charset;)[Lorg/apache/fontbox/cff/CFFParser$Format2Charset$Range2;

    move-result-object v5

    array-length v5, v5

    invoke-static {v4, p2, v3, p2, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    invoke-static {v0, v3}, Lorg/apache/fontbox/cff/CFFParser$Format2Charset;->access$4502(Lorg/apache/fontbox/cff/CFFParser$Format2Charset;[Lorg/apache/fontbox/cff/CFFParser$Format2Charset$Range2;)[Lorg/apache/fontbox/cff/CFFParser$Format2Charset$Range2;

    new-instance v3, Lorg/apache/fontbox/cff/CFFParser$Format2Charset$Range2;

    const/4 v4, 0x0

    invoke-direct {v3, v4}, Lorg/apache/fontbox/cff/CFFParser$Format2Charset$Range2;-><init>(Lorg/apache/fontbox/cff/CFFParser$1;)V

    invoke-virtual {p1}, Lorg/apache/fontbox/cff/CFFDataInput;->readSID()I

    move-result v4

    invoke-static {v3, v4}, Lorg/apache/fontbox/cff/CFFParser$Format2Charset$Range2;->access$4702(Lorg/apache/fontbox/cff/CFFParser$Format2Charset$Range2;I)I

    invoke-virtual {p1}, Lorg/apache/fontbox/cff/CFFDataInput;->readCard16()I

    move-result v4

    invoke-static {v3, v4}, Lorg/apache/fontbox/cff/CFFParser$Format2Charset$Range2;->access$4802(Lorg/apache/fontbox/cff/CFFParser$Format2Charset$Range2;I)I

    invoke-static {v0}, Lorg/apache/fontbox/cff/CFFParser$Format2Charset;->access$4500(Lorg/apache/fontbox/cff/CFFParser$Format2Charset;)[Lorg/apache/fontbox/cff/CFFParser$Format2Charset$Range2;

    move-result-object v4

    invoke-static {v0}, Lorg/apache/fontbox/cff/CFFParser$Format2Charset;->access$4500(Lorg/apache/fontbox/cff/CFFParser$Format2Charset;)[Lorg/apache/fontbox/cff/CFFParser$Format2Charset$Range2;

    move-result-object v5

    array-length v5, v5

    sub-int/2addr v5, v1

    aput-object v3, v4, v5

    move v4, p2

    :goto_2
    invoke-static {v3}, Lorg/apache/fontbox/cff/CFFParser$Format2Charset$Range2;->access$4800(Lorg/apache/fontbox/cff/CFFParser$Format2Charset$Range2;)I

    move-result v5

    add-int/2addr v5, v1

    if-ge v4, v5, :cond_2

    invoke-static {v3}, Lorg/apache/fontbox/cff/CFFParser$Format2Charset$Range2;->access$4700(Lorg/apache/fontbox/cff/CFFParser$Format2Charset$Range2;)I

    move-result v5

    add-int/2addr v5, v4

    add-int v6, v2, v4

    if-eqz p4, :cond_1

    invoke-virtual {v0, v6, v5}, Lorg/apache/fontbox/cff/CFFCharset;->addCID(II)V

    goto :goto_3

    :cond_1
    invoke-direct {p0, v5}, Lorg/apache/fontbox/cff/CFFParser;->readString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v6, v5, v7}, Lorg/apache/fontbox/cff/CFFCharset;->addSID(IILjava/lang/String;)V

    :goto_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    :cond_2
    invoke-static {v3}, Lorg/apache/fontbox/cff/CFFParser$Format2Charset$Range2;->access$4800(Lorg/apache/fontbox/cff/CFFParser$Format2Charset$Range2;)I

    move-result v3

    add-int/2addr v2, v3

    add-int/2addr v2, v1

    goto :goto_1

    :cond_3
    return-object v0
.end method

.method private static readFormat3FDSelect(Lorg/apache/fontbox/cff/CFFDataInput;IILorg/apache/fontbox/cff/CFFCIDFont;)Lorg/apache/fontbox/cff/CFFParser$Format3FDSelect;
    .locals 2

    new-instance p2, Lorg/apache/fontbox/cff/CFFParser$Format3FDSelect;

    const/4 v0, 0x0

    invoke-direct {p2, p3, v0}, Lorg/apache/fontbox/cff/CFFParser$Format3FDSelect;-><init>(Lorg/apache/fontbox/cff/CFFCIDFont;Lorg/apache/fontbox/cff/CFFParser$1;)V

    invoke-static {p2, p1}, Lorg/apache/fontbox/cff/CFFParser$Format3FDSelect;->access$3002(Lorg/apache/fontbox/cff/CFFParser$Format3FDSelect;I)I

    invoke-virtual {p0}, Lorg/apache/fontbox/cff/CFFDataInput;->readCard16()I

    move-result p1

    invoke-static {p2, p1}, Lorg/apache/fontbox/cff/CFFParser$Format3FDSelect;->access$3102(Lorg/apache/fontbox/cff/CFFParser$Format3FDSelect;I)I

    invoke-static {p2}, Lorg/apache/fontbox/cff/CFFParser$Format3FDSelect;->access$3100(Lorg/apache/fontbox/cff/CFFParser$Format3FDSelect;)I

    move-result p1

    new-array p1, p1, [Lorg/apache/fontbox/cff/CFFParser$Range3;

    invoke-static {p2, p1}, Lorg/apache/fontbox/cff/CFFParser$Format3FDSelect;->access$3202(Lorg/apache/fontbox/cff/CFFParser$Format3FDSelect;[Lorg/apache/fontbox/cff/CFFParser$Range3;)[Lorg/apache/fontbox/cff/CFFParser$Range3;

    const/4 p1, 0x0

    :goto_0
    invoke-static {p2}, Lorg/apache/fontbox/cff/CFFParser$Format3FDSelect;->access$3100(Lorg/apache/fontbox/cff/CFFParser$Format3FDSelect;)I

    move-result p3

    if-ge p1, p3, :cond_0

    new-instance p3, Lorg/apache/fontbox/cff/CFFParser$Range3;

    invoke-direct {p3, v0}, Lorg/apache/fontbox/cff/CFFParser$Range3;-><init>(Lorg/apache/fontbox/cff/CFFParser$1;)V

    invoke-virtual {p0}, Lorg/apache/fontbox/cff/CFFDataInput;->readCard16()I

    move-result v1

    invoke-static {p3, v1}, Lorg/apache/fontbox/cff/CFFParser$Range3;->access$3402(Lorg/apache/fontbox/cff/CFFParser$Range3;I)I

    invoke-virtual {p0}, Lorg/apache/fontbox/cff/CFFDataInput;->readCard8()I

    move-result v1

    invoke-static {p3, v1}, Lorg/apache/fontbox/cff/CFFParser$Range3;->access$3502(Lorg/apache/fontbox/cff/CFFParser$Range3;I)I

    invoke-static {p2}, Lorg/apache/fontbox/cff/CFFParser$Format3FDSelect;->access$3200(Lorg/apache/fontbox/cff/CFFParser$Format3FDSelect;)[Lorg/apache/fontbox/cff/CFFParser$Range3;

    move-result-object v1

    aput-object p3, v1, p1

    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lorg/apache/fontbox/cff/CFFDataInput;->readCard16()I

    move-result p0

    invoke-static {p2, p0}, Lorg/apache/fontbox/cff/CFFParser$Format3FDSelect;->access$3602(Lorg/apache/fontbox/cff/CFFParser$Format3FDSelect;I)I

    return-object p2
.end method

.method private static readHeader(Lorg/apache/fontbox/cff/CFFDataInput;)Lorg/apache/fontbox/cff/CFFParser$Header;
    .locals 2

    new-instance v0, Lorg/apache/fontbox/cff/CFFParser$Header;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lorg/apache/fontbox/cff/CFFParser$Header;-><init>(Lorg/apache/fontbox/cff/CFFParser$1;)V

    invoke-virtual {p0}, Lorg/apache/fontbox/cff/CFFDataInput;->readCard8()I

    move-result v1

    invoke-static {v0, v1}, Lorg/apache/fontbox/cff/CFFParser$Header;->access$102(Lorg/apache/fontbox/cff/CFFParser$Header;I)I

    invoke-virtual {p0}, Lorg/apache/fontbox/cff/CFFDataInput;->readCard8()I

    move-result v1

    invoke-static {v0, v1}, Lorg/apache/fontbox/cff/CFFParser$Header;->access$202(Lorg/apache/fontbox/cff/CFFParser$Header;I)I

    invoke-virtual {p0}, Lorg/apache/fontbox/cff/CFFDataInput;->readCard8()I

    move-result v1

    invoke-static {v0, v1}, Lorg/apache/fontbox/cff/CFFParser$Header;->access$302(Lorg/apache/fontbox/cff/CFFParser$Header;I)I

    invoke-virtual {p0}, Lorg/apache/fontbox/cff/CFFDataInput;->readOffSize()I

    move-result p0

    invoke-static {v0, p0}, Lorg/apache/fontbox/cff/CFFParser$Header;->access$402(Lorg/apache/fontbox/cff/CFFParser$Header;I)I

    return-object v0
.end method

.method private static readIndexData(Lorg/apache/fontbox/cff/CFFDataInput;)Lorg/apache/fontbox/cff/IndexData;
    .locals 7

    invoke-virtual {p0}, Lorg/apache/fontbox/cff/CFFDataInput;->readCard16()I

    move-result v0

    new-instance v1, Lorg/apache/fontbox/cff/IndexData;

    invoke-direct {v1, v0}, Lorg/apache/fontbox/cff/IndexData;-><init>(I)V

    if-nez v0, :cond_0

    return-object v1

    :cond_0
    invoke-virtual {p0}, Lorg/apache/fontbox/cff/CFFDataInput;->readOffSize()I

    move-result v2

    const/4 v3, 0x0

    move v4, v3

    :goto_0
    if-gt v4, v0, :cond_2

    invoke-virtual {p0, v2}, Lorg/apache/fontbox/cff/CFFDataInput;->readOffset(I)I

    move-result v5

    invoke-virtual {p0}, Lorg/apache/fontbox/cff/DataInput;->length()I

    move-result v6

    if-gt v5, v6, :cond_1

    invoke-virtual {v1, v4, v5}, Lorg/apache/fontbox/cff/IndexData;->setOffset(II)V

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_1
    new-instance p0, Ljava/io/IOException;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "illegal offset value "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, " in CFF font"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p0

    :cond_2
    invoke-virtual {v1, v0}, Lorg/apache/fontbox/cff/IndexData;->getOffset(I)I

    move-result v0

    invoke-virtual {v1, v3}, Lorg/apache/fontbox/cff/IndexData;->getOffset(I)I

    move-result v2

    sub-int/2addr v0, v2

    invoke-virtual {v1, v0}, Lorg/apache/fontbox/cff/IndexData;->initData(I)V

    :goto_1
    if-ge v3, v0, :cond_3

    invoke-virtual {p0}, Lorg/apache/fontbox/cff/CFFDataInput;->readCard8()I

    move-result v2

    invoke-virtual {v1, v3, v2}, Lorg/apache/fontbox/cff/IndexData;->setData(II)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_3
    return-object v1
.end method

.method private static readIntegerNumber(Lorg/apache/fontbox/cff/CFFDataInput;I)Ljava/lang/Integer;
    .locals 2

    const/16 v0, 0x1c

    if-ne p1, v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/fontbox/cff/DataInput;->readUnsignedByte()I

    move-result p1

    invoke-virtual {p0}, Lorg/apache/fontbox/cff/DataInput;->readUnsignedByte()I

    move-result p0

    shl-int/lit8 p1, p1, 0x8

    or-int/2addr p0, p1

    int-to-short p0, p0

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    return-object p0

    :cond_0
    const/16 v0, 0x1d

    if-ne p1, v0, :cond_1

    invoke-virtual {p0}, Lorg/apache/fontbox/cff/DataInput;->readUnsignedByte()I

    move-result p1

    invoke-virtual {p0}, Lorg/apache/fontbox/cff/DataInput;->readUnsignedByte()I

    move-result v0

    invoke-virtual {p0}, Lorg/apache/fontbox/cff/DataInput;->readUnsignedByte()I

    move-result v1

    invoke-virtual {p0}, Lorg/apache/fontbox/cff/DataInput;->readUnsignedByte()I

    move-result p0

    shl-int/lit8 p1, p1, 0x18

    shl-int/lit8 v0, v0, 0x10

    or-int/2addr p1, v0

    shl-int/lit8 v0, v1, 0x8

    or-int/2addr p1, v0

    or-int/2addr p0, p1

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    return-object p0

    :cond_1
    const/16 v0, 0x20

    if-lt p1, v0, :cond_2

    const/16 v0, 0xf6

    if-gt p1, v0, :cond_2

    add-int/lit16 p1, p1, -0x8b

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    return-object p0

    :cond_2
    const/16 v0, 0xf7

    if-lt p1, v0, :cond_3

    const/16 v1, 0xfa

    if-gt p1, v1, :cond_3

    invoke-virtual {p0}, Lorg/apache/fontbox/cff/DataInput;->readUnsignedByte()I

    move-result p0

    sub-int/2addr p1, v0

    mul-int/lit16 p1, p1, 0x100

    add-int/2addr p1, p0

    add-int/lit8 p1, p1, 0x6c

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    return-object p0

    :cond_3
    const/16 v0, 0xfb

    if-lt p1, v0, :cond_4

    const/16 v1, 0xfe

    if-gt p1, v1, :cond_4

    invoke-virtual {p0}, Lorg/apache/fontbox/cff/DataInput;->readUnsignedByte()I

    move-result p0

    sub-int/2addr p1, v0

    neg-int p1, p1

    mul-int/lit16 p1, p1, 0x100

    sub-int/2addr p1, p0

    add-int/lit8 p1, p1, -0x6c

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p0

    return-object p0

    :cond_4
    new-instance p0, Ljava/lang/IllegalArgumentException;

    invoke-direct {p0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw p0
.end method

.method private static readLong(Lorg/apache/fontbox/cff/CFFDataInput;)J
    .locals 2

    invoke-virtual {p0}, Lorg/apache/fontbox/cff/CFFDataInput;->readCard16()I

    move-result v0

    shl-int/lit8 v0, v0, 0x10

    invoke-virtual {p0}, Lorg/apache/fontbox/cff/CFFDataInput;->readCard16()I

    move-result p0

    or-int/2addr p0, v0

    int-to-long v0, p0

    return-wide v0
.end method

.method private static readOperator(Lorg/apache/fontbox/cff/CFFDataInput;I)Lorg/apache/fontbox/cff/CFFOperator;
    .locals 0

    invoke-static {p0, p1}, Lorg/apache/fontbox/cff/CFFParser;->readOperatorKey(Lorg/apache/fontbox/cff/CFFDataInput;I)Lorg/apache/fontbox/cff/CFFOperator$Key;

    move-result-object p0

    invoke-static {p0}, Lorg/apache/fontbox/cff/CFFOperator;->getOperator(Lorg/apache/fontbox/cff/CFFOperator$Key;)Lorg/apache/fontbox/cff/CFFOperator;

    move-result-object p0

    return-object p0
.end method

.method private static readOperatorKey(Lorg/apache/fontbox/cff/CFFDataInput;I)Lorg/apache/fontbox/cff/CFFOperator$Key;
    .locals 1

    const/16 v0, 0xc

    if-ne p1, v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/fontbox/cff/DataInput;->readUnsignedByte()I

    move-result p0

    new-instance v0, Lorg/apache/fontbox/cff/CFFOperator$Key;

    invoke-direct {v0, p1, p0}, Lorg/apache/fontbox/cff/CFFOperator$Key;-><init>(II)V

    return-object v0

    :cond_0
    new-instance p0, Lorg/apache/fontbox/cff/CFFOperator$Key;

    invoke-direct {p0, p1}, Lorg/apache/fontbox/cff/CFFOperator$Key;-><init>(I)V

    return-object p0
.end method

.method private readPrivateDict(Lorg/apache/fontbox/cff/CFFParser$DictData;)Ljava/util/Map;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/fontbox/cff/CFFParser$DictData;",
            ")",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    const-string v1, "BlueValues"

    const/4 v2, 0x0

    invoke-static {p1, v1, v2}, Lorg/apache/fontbox/cff/CFFParser;->getDelta(Lorg/apache/fontbox/cff/CFFParser$DictData;Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v0, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "OtherBlues"

    invoke-static {p1, v1, v2}, Lorg/apache/fontbox/cff/CFFParser;->getDelta(Lorg/apache/fontbox/cff/CFFParser$DictData;Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v0, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "FamilyBlues"

    invoke-static {p1, v1, v2}, Lorg/apache/fontbox/cff/CFFParser;->getDelta(Lorg/apache/fontbox/cff/CFFParser$DictData;Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v0, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "FamilyOtherBlues"

    invoke-static {p1, v1, v2}, Lorg/apache/fontbox/cff/CFFParser;->getDelta(Lorg/apache/fontbox/cff/CFFParser$DictData;Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v0, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-wide v3, 0x3fa449ba5e353f7dL    # 0.039625

    invoke-static {v3, v4}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    const-string v3, "BlueScale"

    invoke-static {p1, v3, v1}, Lorg/apache/fontbox/cff/CFFParser;->getNumber(Lorg/apache/fontbox/cff/CFFParser$DictData;Ljava/lang/String;Ljava/lang/Number;)Ljava/lang/Number;

    move-result-object v1

    invoke-interface {v0, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v1, 0x7

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v3, "BlueShift"

    invoke-static {p1, v3, v1}, Lorg/apache/fontbox/cff/CFFParser;->getNumber(Lorg/apache/fontbox/cff/CFFParser$DictData;Ljava/lang/String;Ljava/lang/Number;)Ljava/lang/Number;

    move-result-object v1

    invoke-interface {v0, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v3, "BlueFuzz"

    invoke-static {p1, v3, v1}, Lorg/apache/fontbox/cff/CFFParser;->getNumber(Lorg/apache/fontbox/cff/CFFParser$DictData;Ljava/lang/String;Ljava/lang/Number;)Ljava/lang/Number;

    move-result-object v1

    invoke-interface {v0, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "StdHW"

    invoke-static {p1, v1, v2}, Lorg/apache/fontbox/cff/CFFParser;->getNumber(Lorg/apache/fontbox/cff/CFFParser$DictData;Ljava/lang/String;Ljava/lang/Number;)Ljava/lang/Number;

    move-result-object v3

    invoke-interface {v0, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "StdVW"

    invoke-static {p1, v1, v2}, Lorg/apache/fontbox/cff/CFFParser;->getNumber(Lorg/apache/fontbox/cff/CFFParser$DictData;Ljava/lang/String;Ljava/lang/Number;)Ljava/lang/Number;

    move-result-object v3

    invoke-interface {v0, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "StemSnapH"

    invoke-static {p1, v1, v2}, Lorg/apache/fontbox/cff/CFFParser;->getDelta(Lorg/apache/fontbox/cff/CFFParser$DictData;Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v0, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "StemSnapV"

    invoke-static {p1, v1, v2}, Lorg/apache/fontbox/cff/CFFParser;->getDelta(Lorg/apache/fontbox/cff/CFFParser$DictData;Ljava/lang/String;Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "ForceBold"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {p1, v1, v2}, Lorg/apache/fontbox/cff/CFFParser;->getBoolean(Lorg/apache/fontbox/cff/CFFParser$DictData;Ljava/lang/String;Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "LanguageGroup"

    invoke-static {p1, v1, v3}, Lorg/apache/fontbox/cff/CFFParser;->getNumber(Lorg/apache/fontbox/cff/CFFParser$DictData;Ljava/lang/String;Ljava/lang/Number;)Ljava/lang/Number;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-wide v1, 0x3faeb851eb851eb8L    # 0.06

    invoke-static {v1, v2}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    const-string v2, "ExpansionFactor"

    invoke-static {p1, v2, v1}, Lorg/apache/fontbox/cff/CFFParser;->getNumber(Lorg/apache/fontbox/cff/CFFParser$DictData;Ljava/lang/String;Ljava/lang/Number;)Ljava/lang/Number;

    move-result-object v1

    invoke-interface {v0, v2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "initialRandomSeed"

    invoke-static {p1, v1, v3}, Lorg/apache/fontbox/cff/CFFParser;->getNumber(Lorg/apache/fontbox/cff/CFFParser$DictData;Ljava/lang/String;Ljava/lang/Number;)Ljava/lang/Number;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "defaultWidthX"

    invoke-static {p1, v1, v3}, Lorg/apache/fontbox/cff/CFFParser;->getNumber(Lorg/apache/fontbox/cff/CFFParser$DictData;Ljava/lang/String;Ljava/lang/Number;)Ljava/lang/Number;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v1, "nominalWidthX"

    invoke-static {p1, v1, v3}, Lorg/apache/fontbox/cff/CFFParser;->getNumber(Lorg/apache/fontbox/cff/CFFParser$DictData;Ljava/lang/String;Ljava/lang/Number;)Ljava/lang/Number;

    move-result-object p1

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object v0
.end method

.method private static readRealNumber(Lorg/apache/fontbox/cff/CFFDataInput;I)Ljava/lang/Double;
    .locals 7

    new-instance p1, Ljava/lang/StringBuffer;

    invoke-direct {p1}, Ljava/lang/StringBuffer;-><init>()V

    const/4 v0, 0x0

    move v1, v0

    move v2, v1

    :cond_0
    if-nez v1, :cond_1

    invoke-virtual {p0}, Lorg/apache/fontbox/cff/DataInput;->readUnsignedByte()I

    move-result v3

    div-int/lit8 v4, v3, 0x10

    rem-int/lit8 v3, v3, 0x10

    filled-new-array {v4, v3}, [I

    move-result-object v3

    move v4, v0

    :goto_0
    const/4 v5, 0x2

    if-ge v4, v5, :cond_0

    aget v5, v3, v4

    const/4 v6, 0x1

    packed-switch v5, :pswitch_data_0

    new-instance p0, Ljava/lang/IllegalArgumentException;

    invoke-direct {p0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw p0

    :pswitch_0
    move v1, v6

    goto :goto_3

    :pswitch_1
    const-string v5, "-"

    goto :goto_2

    :pswitch_2
    const-string v2, "E-"

    goto :goto_1

    :pswitch_3
    const-string v2, "E"

    :goto_1
    invoke-virtual {p1, v2}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move v2, v6

    goto :goto_3

    :pswitch_4
    const-string v5, "."

    :goto_2
    invoke-virtual {p1, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_3

    :pswitch_5
    invoke-virtual {p1, v5}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move v2, v0

    :goto_3
    :pswitch_6
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_1
    if-eqz v2, :cond_2

    const-string p0, "0"

    invoke-virtual {p1, p0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    :cond_2
    invoke-virtual {p1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-static {p0}, Ljava/lang/Double;->valueOf(Ljava/lang/String;)Ljava/lang/Double;

    move-result-object p0

    return-object p0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_6
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private readString(I)Ljava/lang/String;
    .locals 2

    if-ltz p1, :cond_0

    const/16 v0, 0x186

    if-gt p1, v0, :cond_0

    invoke-static {p1}, Lorg/apache/fontbox/cff/CFFStandardString;->getName(I)Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_0
    add-int/lit16 v0, p1, -0x187

    iget-object v1, p0, Lorg/apache/fontbox/cff/CFFParser;->stringIndex:Lorg/apache/fontbox/cff/IndexData;

    invoke-virtual {v1}, Lorg/apache/fontbox/cff/IndexData;->getCount()I

    move-result v1

    if-ge v0, v1, :cond_1

    new-instance p1, Lorg/apache/fontbox/cff/DataInput;

    iget-object v1, p0, Lorg/apache/fontbox/cff/CFFParser;->stringIndex:Lorg/apache/fontbox/cff/IndexData;

    invoke-virtual {v1, v0}, Lorg/apache/fontbox/cff/IndexData;->getBytes(I)[B

    move-result-object v0

    invoke-direct {p1, v0}, Lorg/apache/fontbox/cff/DataInput;-><init>([B)V

    invoke-virtual {p1}, Lorg/apache/fontbox/cff/DataInput;->getString()Ljava/lang/String;

    move-result-object p1

    return-object p1

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SID"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    return-object p1
.end method

.method private readSupplement(Lorg/apache/fontbox/cff/CFFDataInput;Lorg/apache/fontbox/cff/CFFParser$EmbeddedEncoding;)V
    .locals 4

    invoke-virtual {p1}, Lorg/apache/fontbox/cff/CFFDataInput;->readCard8()I

    move-result v0

    invoke-static {p2, v0}, Lorg/apache/fontbox/cff/CFFParser$EmbeddedEncoding;->access$2102(Lorg/apache/fontbox/cff/CFFParser$EmbeddedEncoding;I)I

    invoke-static {p2}, Lorg/apache/fontbox/cff/CFFParser$EmbeddedEncoding;->access$2100(Lorg/apache/fontbox/cff/CFFParser$EmbeddedEncoding;)I

    move-result v0

    new-array v0, v0, [Lorg/apache/fontbox/cff/CFFParser$EmbeddedEncoding$Supplement;

    invoke-static {p2, v0}, Lorg/apache/fontbox/cff/CFFParser$EmbeddedEncoding;->access$2202(Lorg/apache/fontbox/cff/CFFParser$EmbeddedEncoding;[Lorg/apache/fontbox/cff/CFFParser$EmbeddedEncoding$Supplement;)[Lorg/apache/fontbox/cff/CFFParser$EmbeddedEncoding$Supplement;

    const/4 v0, 0x0

    :goto_0
    invoke-static {p2}, Lorg/apache/fontbox/cff/CFFParser$EmbeddedEncoding;->access$2200(Lorg/apache/fontbox/cff/CFFParser$EmbeddedEncoding;)[Lorg/apache/fontbox/cff/CFFParser$EmbeddedEncoding$Supplement;

    move-result-object v1

    array-length v1, v1

    if-ge v0, v1, :cond_0

    new-instance v1, Lorg/apache/fontbox/cff/CFFParser$EmbeddedEncoding$Supplement;

    invoke-direct {v1}, Lorg/apache/fontbox/cff/CFFParser$EmbeddedEncoding$Supplement;-><init>()V

    invoke-virtual {p1}, Lorg/apache/fontbox/cff/CFFDataInput;->readCard8()I

    move-result v2

    invoke-static {v1, v2}, Lorg/apache/fontbox/cff/CFFParser$EmbeddedEncoding$Supplement;->access$2302(Lorg/apache/fontbox/cff/CFFParser$EmbeddedEncoding$Supplement;I)I

    invoke-virtual {p1}, Lorg/apache/fontbox/cff/CFFDataInput;->readSID()I

    move-result v2

    invoke-static {v1, v2}, Lorg/apache/fontbox/cff/CFFParser$EmbeddedEncoding$Supplement;->access$2402(Lorg/apache/fontbox/cff/CFFParser$EmbeddedEncoding$Supplement;I)I

    invoke-static {v1}, Lorg/apache/fontbox/cff/CFFParser$EmbeddedEncoding$Supplement;->access$2400(Lorg/apache/fontbox/cff/CFFParser$EmbeddedEncoding$Supplement;)I

    move-result v2

    invoke-direct {p0, v2}, Lorg/apache/fontbox/cff/CFFParser;->readString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lorg/apache/fontbox/cff/CFFParser$EmbeddedEncoding$Supplement;->access$2502(Lorg/apache/fontbox/cff/CFFParser$EmbeddedEncoding$Supplement;Ljava/lang/String;)Ljava/lang/String;

    invoke-static {p2}, Lorg/apache/fontbox/cff/CFFParser$EmbeddedEncoding;->access$2200(Lorg/apache/fontbox/cff/CFFParser$EmbeddedEncoding;)[Lorg/apache/fontbox/cff/CFFParser$EmbeddedEncoding$Supplement;

    move-result-object v2

    aput-object v1, v2, v0

    invoke-static {v1}, Lorg/apache/fontbox/cff/CFFParser$EmbeddedEncoding$Supplement;->access$2300(Lorg/apache/fontbox/cff/CFFParser$EmbeddedEncoding$Supplement;)I

    move-result v2

    invoke-static {v1}, Lorg/apache/fontbox/cff/CFFParser$EmbeddedEncoding$Supplement;->access$2400(Lorg/apache/fontbox/cff/CFFParser$EmbeddedEncoding$Supplement;)I

    move-result v3

    invoke-static {v1}, Lorg/apache/fontbox/cff/CFFParser$EmbeddedEncoding$Supplement;->access$2400(Lorg/apache/fontbox/cff/CFFParser$EmbeddedEncoding$Supplement;)I

    move-result v1

    invoke-direct {p0, v1}, Lorg/apache/fontbox/cff/CFFParser;->readString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v2, v3, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(IILjava/lang/String;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private static readTagName(Lorg/apache/fontbox/cff/CFFDataInput;)Ljava/lang/String;
    .locals 2

    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lorg/apache/fontbox/cff/DataInput;->readBytes(I)[B

    move-result-object p0

    new-instance v0, Ljava/lang/String;

    const-string v1, "ISO-8859-1"

    invoke-direct {v0, p0, v1}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public parse([B)Ljava/util/List;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([B)",
            "Ljava/util/List<",
            "Lorg/apache/fontbox/cff/CFFFont;",
            ">;"
        }
    .end annotation

    new-instance v0, Lorg/apache/fontbox/cff/CFFDataInput;

    invoke-direct {v0, p1}, Lorg/apache/fontbox/cff/CFFDataInput;-><init>([B)V

    iput-object v0, p0, Lorg/apache/fontbox/cff/CFFParser;->input:Lorg/apache/fontbox/cff/CFFDataInput;

    invoke-static {v0}, Lorg/apache/fontbox/cff/CFFParser;->readTagName(Lorg/apache/fontbox/cff/CFFDataInput;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "OTTO"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    const/4 v2, 0x0

    if-eqz v1, :cond_3

    iget-object v0, p0, Lorg/apache/fontbox/cff/CFFParser;->input:Lorg/apache/fontbox/cff/CFFDataInput;

    invoke-virtual {v0}, Lorg/apache/fontbox/cff/DataInput;->readShort()S

    move-result v0

    iget-object v1, p0, Lorg/apache/fontbox/cff/CFFParser;->input:Lorg/apache/fontbox/cff/CFFDataInput;

    invoke-virtual {v1}, Lorg/apache/fontbox/cff/DataInput;->readShort()S

    iget-object v1, p0, Lorg/apache/fontbox/cff/CFFParser;->input:Lorg/apache/fontbox/cff/CFFDataInput;

    invoke-virtual {v1}, Lorg/apache/fontbox/cff/DataInput;->readShort()S

    iget-object v1, p0, Lorg/apache/fontbox/cff/CFFParser;->input:Lorg/apache/fontbox/cff/CFFDataInput;

    invoke-virtual {v1}, Lorg/apache/fontbox/cff/DataInput;->readShort()S

    move v1, v2

    :goto_0
    if-ge v1, v0, :cond_1

    iget-object v3, p0, Lorg/apache/fontbox/cff/CFFParser;->input:Lorg/apache/fontbox/cff/CFFDataInput;

    invoke-static {v3}, Lorg/apache/fontbox/cff/CFFParser;->readTagName(Lorg/apache/fontbox/cff/CFFDataInput;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lorg/apache/fontbox/cff/CFFParser;->input:Lorg/apache/fontbox/cff/CFFDataInput;

    invoke-static {v4}, Lorg/apache/fontbox/cff/CFFParser;->readLong(Lorg/apache/fontbox/cff/CFFDataInput;)J

    iget-object v4, p0, Lorg/apache/fontbox/cff/CFFParser;->input:Lorg/apache/fontbox/cff/CFFDataInput;

    invoke-static {v4}, Lorg/apache/fontbox/cff/CFFParser;->readLong(Lorg/apache/fontbox/cff/CFFDataInput;)J

    move-result-wide v4

    iget-object v6, p0, Lorg/apache/fontbox/cff/CFFParser;->input:Lorg/apache/fontbox/cff/CFFDataInput;

    invoke-static {v6}, Lorg/apache/fontbox/cff/CFFParser;->readLong(Lorg/apache/fontbox/cff/CFFDataInput;)J

    move-result-wide v6

    const-string v8, "CFF "

    invoke-virtual {v3, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    long-to-int v0, v6

    new-array v1, v0, [B

    long-to-int v3, v4

    invoke-static {p1, v3, v1, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    new-instance p1, Lorg/apache/fontbox/cff/CFFDataInput;

    invoke-direct {p1, v1}, Lorg/apache/fontbox/cff/CFFDataInput;-><init>([B)V

    iput-object p1, p0, Lorg/apache/fontbox/cff/CFFParser;->input:Lorg/apache/fontbox/cff/CFFDataInput;

    const/4 p1, 0x1

    goto :goto_1

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    move p1, v2

    :goto_1
    if-eqz p1, :cond_2

    goto :goto_2

    :cond_2
    new-instance p1, Ljava/io/IOException;

    const-string v0, "CFF tag not found in this OpenType font."

    invoke-direct {p1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_3
    const-string p1, "ttcf"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_6

    const-string p1, "\u0000\u0001\u0000\u0000"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-nez p1, :cond_5

    iget-object p1, p0, Lorg/apache/fontbox/cff/CFFParser;->input:Lorg/apache/fontbox/cff/CFFDataInput;

    invoke-virtual {p1, v2}, Lorg/apache/fontbox/cff/DataInput;->setPosition(I)V

    :goto_2
    iget-object p1, p0, Lorg/apache/fontbox/cff/CFFParser;->input:Lorg/apache/fontbox/cff/CFFDataInput;

    invoke-static {p1}, Lorg/apache/fontbox/cff/CFFParser;->readHeader(Lorg/apache/fontbox/cff/CFFDataInput;)Lorg/apache/fontbox/cff/CFFParser$Header;

    move-result-object p1

    iput-object p1, p0, Lorg/apache/fontbox/cff/CFFParser;->header:Lorg/apache/fontbox/cff/CFFParser$Header;

    iget-object p1, p0, Lorg/apache/fontbox/cff/CFFParser;->input:Lorg/apache/fontbox/cff/CFFDataInput;

    invoke-static {p1}, Lorg/apache/fontbox/cff/CFFParser;->readIndexData(Lorg/apache/fontbox/cff/CFFDataInput;)Lorg/apache/fontbox/cff/IndexData;

    move-result-object p1

    iput-object p1, p0, Lorg/apache/fontbox/cff/CFFParser;->nameIndex:Lorg/apache/fontbox/cff/IndexData;

    iget-object p1, p0, Lorg/apache/fontbox/cff/CFFParser;->input:Lorg/apache/fontbox/cff/CFFDataInput;

    invoke-static {p1}, Lorg/apache/fontbox/cff/CFFParser;->readIndexData(Lorg/apache/fontbox/cff/CFFDataInput;)Lorg/apache/fontbox/cff/IndexData;

    move-result-object p1

    iput-object p1, p0, Lorg/apache/fontbox/cff/CFFParser;->topDictIndex:Lorg/apache/fontbox/cff/IndexData;

    iget-object p1, p0, Lorg/apache/fontbox/cff/CFFParser;->input:Lorg/apache/fontbox/cff/CFFDataInput;

    invoke-static {p1}, Lorg/apache/fontbox/cff/CFFParser;->readIndexData(Lorg/apache/fontbox/cff/CFFDataInput;)Lorg/apache/fontbox/cff/IndexData;

    move-result-object p1

    iput-object p1, p0, Lorg/apache/fontbox/cff/CFFParser;->stringIndex:Lorg/apache/fontbox/cff/IndexData;

    iget-object p1, p0, Lorg/apache/fontbox/cff/CFFParser;->input:Lorg/apache/fontbox/cff/CFFDataInput;

    invoke-static {p1}, Lorg/apache/fontbox/cff/CFFParser;->readIndexData(Lorg/apache/fontbox/cff/CFFDataInput;)Lorg/apache/fontbox/cff/IndexData;

    move-result-object p1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    :goto_3
    iget-object v1, p0, Lorg/apache/fontbox/cff/CFFParser;->nameIndex:Lorg/apache/fontbox/cff/IndexData;

    invoke-virtual {v1}, Lorg/apache/fontbox/cff/IndexData;->getCount()I

    move-result v1

    if-ge v2, v1, :cond_4

    invoke-direct {p0, v2}, Lorg/apache/fontbox/cff/CFFParser;->parseFont(I)Lorg/apache/fontbox/cff/CFFFont;

    move-result-object v1

    invoke-virtual {v1, p1}, Lorg/apache/fontbox/cff/CFFFont;->setGlobalSubrIndex(Lorg/apache/fontbox/cff/IndexData;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_4
    return-object v0

    :cond_5
    new-instance p1, Ljava/io/IOException;

    const-string v0, "OpenType fonts containing a true type font are not supported."

    invoke-direct {p1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_6
    new-instance p1, Ljava/io/IOException;

    const-string v0, "True Type Collection fonts are not supported."

    invoke-direct {p1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lorg/apache/fontbox/cff/CFFParser;->debugFontName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

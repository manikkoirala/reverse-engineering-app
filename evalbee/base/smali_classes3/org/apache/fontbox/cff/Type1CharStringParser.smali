.class public Lorg/apache/fontbox/cff/Type1CharStringParser;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field static final CALLOTHERSUBR:I = 0x10

.field static final CALLSUBR:I = 0xa

.field static final POP:I = 0x11

.field static final RETURN:I = 0xb

.field static final TWO_BYTE:I = 0xc


# instance fields
.field private final fontName:Ljava/lang/String;

.field private final glyphName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lorg/apache/fontbox/cff/Type1CharStringParser;->fontName:Ljava/lang/String;

    iput-object p2, p0, Lorg/apache/fontbox/cff/Type1CharStringParser;->glyphName:Ljava/lang/String;

    return-void
.end method

.method private parse([BLjava/util/List;Ljava/util/List;)Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([B",
            "Ljava/util/List<",
            "[B>;",
            "Ljava/util/List<",
            "Ljava/lang/Object;",
            ">;)",
            "Ljava/util/List<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .line 2
    new-instance v0, Lorg/apache/fontbox/cff/DataInput;

    invoke-direct {v0, p1}, Lorg/apache/fontbox/cff/DataInput;-><init>([B)V

    :cond_0
    :goto_0
    invoke-virtual {v0}, Lorg/apache/fontbox/cff/DataInput;->hasRemaining()Z

    move-result p1

    if-eqz p1, :cond_a

    invoke-virtual {v0}, Lorg/apache/fontbox/cff/DataInput;->readUnsignedByte()I

    move-result p1

    const/16 v1, 0xa

    const/4 v2, 0x0

    const/4 v3, 0x1

    if-ne p1, v1, :cond_1

    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result p1

    sub-int/2addr p1, v3

    invoke-interface {p3, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v4

    if-ge v1, v4, :cond_0

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    invoke-interface {p2, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, [B

    invoke-direct {p0, p1, p2, p3}, Lorg/apache/fontbox/cff/Type1CharStringParser;->parse([BLjava/util/List;Ljava/util/List;)Ljava/util/List;

    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result p1

    sub-int/2addr p1, v3

    invoke-interface {p3, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    instance-of v1, p1, Lorg/apache/fontbox/cff/CharStringCommand;

    if-eqz v1, :cond_0

    check-cast p1, Lorg/apache/fontbox/cff/CharStringCommand;

    invoke-virtual {p1}, Lorg/apache/fontbox/cff/CharStringCommand;->getKey()Lorg/apache/fontbox/cff/CharStringCommand$Key;

    move-result-object p1

    invoke-virtual {p1}, Lorg/apache/fontbox/cff/CharStringCommand$Key;->getValue()[I

    move-result-object p1

    aget p1, p1, v2

    const/16 v1, 0xb

    if-ne p1, v1, :cond_0

    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result p1

    sub-int/2addr p1, v3

    invoke-interface {p3, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    goto :goto_0

    :cond_1
    const/16 v1, 0xc

    if-ne p1, v1, :cond_7

    invoke-virtual {v0, v2}, Lorg/apache/fontbox/cff/DataInput;->peekUnsignedByte(I)I

    move-result v4

    const/16 v5, 0x10

    if-ne v4, v5, :cond_7

    invoke-virtual {v0}, Lorg/apache/fontbox/cff/DataInput;->readByte()B

    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result p1

    sub-int/2addr p1, v3

    invoke-interface {p3, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Integer;

    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v4

    sub-int/2addr v4, v3

    invoke-interface {p3, v4}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    new-instance v6, Ljava/util/Stack;

    invoke-direct {v6}, Ljava/util/Stack;-><init>()V

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v7

    if-nez v7, :cond_2

    invoke-direct {p0, p3}, Lorg/apache/fontbox/cff/Type1CharStringParser;->removeInteger(Ljava/util/List;)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v6, p1}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0, p3}, Lorg/apache/fontbox/cff/Type1CharStringParser;->removeInteger(Ljava/util/List;)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v6, p1}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result p1

    sub-int/2addr p1, v3

    invoke-interface {p3, p1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {p3, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance p1, Lorg/apache/fontbox/cff/CharStringCommand;

    invoke-direct {p1, v1, v5}, Lorg/apache/fontbox/cff/CharStringCommand;-><init>(II)V

    :goto_1
    invoke-interface {p3, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_2
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v7

    if-ne v7, v3, :cond_3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {p3, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance p1, Lorg/apache/fontbox/cff/CharStringCommand;

    invoke-direct {p1, v1, v5}, Lorg/apache/fontbox/cff/CharStringCommand;-><init>(II)V

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    const/4 v5, 0x3

    if-ne p1, v5, :cond_4

    invoke-direct {p0, p3}, Lorg/apache/fontbox/cff/Type1CharStringParser;->removeInteger(Ljava/util/List;)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {v6, p1}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_3

    :cond_4
    move p1, v2

    :goto_2
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v5

    if-ge p1, v5, :cond_5

    invoke-direct {p0, p3}, Lorg/apache/fontbox/cff/Type1CharStringParser;->removeInteger(Ljava/util/List;)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v6, v5}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 p1, p1, 0x1

    goto :goto_2

    :cond_5
    :goto_3
    invoke-virtual {v0, v2}, Lorg/apache/fontbox/cff/DataInput;->peekUnsignedByte(I)I

    move-result p1

    if-ne p1, v1, :cond_6

    invoke-virtual {v0, v3}, Lorg/apache/fontbox/cff/DataInput;->peekUnsignedByte(I)I

    move-result p1

    const/16 v4, 0x11

    if-ne p1, v4, :cond_6

    invoke-virtual {v0}, Lorg/apache/fontbox/cff/DataInput;->readByte()B

    invoke-virtual {v0}, Lorg/apache/fontbox/cff/DataInput;->readByte()B

    invoke-virtual {v6}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Integer;

    goto :goto_1

    :cond_6
    invoke-virtual {v6}, Ljava/util/AbstractCollection;->size()I

    move-result p1

    if-lez p1, :cond_0

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Value left on the PostScript stack in glyph "

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lorg/apache/fontbox/cff/Type1CharStringParser;->glyphName:Ljava/lang/String;

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, " of font "

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lorg/apache/fontbox/cff/Type1CharStringParser;->fontName:Ljava/lang/String;

    invoke-virtual {p1, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string v1, "PdfBoxAndroid"

    invoke-static {v1, p1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_7
    if-ltz p1, :cond_8

    const/16 v1, 0x1f

    if-gt p1, v1, :cond_8

    invoke-direct {p0, v0, p1}, Lorg/apache/fontbox/cff/Type1CharStringParser;->readCommand(Lorg/apache/fontbox/cff/DataInput;I)Lorg/apache/fontbox/cff/CharStringCommand;

    move-result-object p1

    :goto_4
    invoke-interface {p3, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :cond_8
    const/16 v1, 0x20

    if-lt p1, v1, :cond_9

    const/16 v1, 0xff

    if-gt p1, v1, :cond_9

    invoke-direct {p0, v0, p1}, Lorg/apache/fontbox/cff/Type1CharStringParser;->readNumber(Lorg/apache/fontbox/cff/DataInput;I)Ljava/lang/Integer;

    move-result-object p1

    goto :goto_4

    :cond_9
    new-instance p1, Ljava/lang/IllegalArgumentException;

    invoke-direct {p1}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw p1

    :cond_a
    return-object p3
.end method

.method private readCommand(Lorg/apache/fontbox/cff/DataInput;I)Lorg/apache/fontbox/cff/CharStringCommand;
    .locals 1

    const/16 v0, 0xc

    if-ne p2, v0, :cond_0

    invoke-virtual {p1}, Lorg/apache/fontbox/cff/DataInput;->readUnsignedByte()I

    move-result p1

    new-instance v0, Lorg/apache/fontbox/cff/CharStringCommand;

    invoke-direct {v0, p2, p1}, Lorg/apache/fontbox/cff/CharStringCommand;-><init>(II)V

    return-object v0

    :cond_0
    new-instance p1, Lorg/apache/fontbox/cff/CharStringCommand;

    invoke-direct {p1, p2}, Lorg/apache/fontbox/cff/CharStringCommand;-><init>(I)V

    return-object p1
.end method

.method private readNumber(Lorg/apache/fontbox/cff/DataInput;I)Ljava/lang/Integer;
    .locals 2

    const/16 v0, 0x20

    if-lt p2, v0, :cond_0

    const/16 v0, 0xf6

    if-gt p2, v0, :cond_0

    add-int/lit16 p2, p2, -0x8b

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    return-object p1

    :cond_0
    const/16 v0, 0xf7

    if-lt p2, v0, :cond_1

    const/16 v1, 0xfa

    if-gt p2, v1, :cond_1

    invoke-virtual {p1}, Lorg/apache/fontbox/cff/DataInput;->readUnsignedByte()I

    move-result p1

    sub-int/2addr p2, v0

    mul-int/lit16 p2, p2, 0x100

    add-int/2addr p2, p1

    add-int/lit8 p2, p2, 0x6c

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    return-object p1

    :cond_1
    const/16 v0, 0xfb

    if-lt p2, v0, :cond_2

    const/16 v1, 0xfe

    if-gt p2, v1, :cond_2

    invoke-virtual {p1}, Lorg/apache/fontbox/cff/DataInput;->readUnsignedByte()I

    move-result p1

    sub-int/2addr p2, v0

    neg-int p2, p2

    mul-int/lit16 p2, p2, 0x100

    sub-int/2addr p2, p1

    add-int/lit8 p2, p2, -0x6c

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    return-object p1

    :cond_2
    const/16 v0, 0xff

    if-ne p2, v0, :cond_3

    invoke-virtual {p1}, Lorg/apache/fontbox/cff/DataInput;->readUnsignedByte()I

    move-result p2

    invoke-virtual {p1}, Lorg/apache/fontbox/cff/DataInput;->readUnsignedByte()I

    move-result v0

    invoke-virtual {p1}, Lorg/apache/fontbox/cff/DataInput;->readUnsignedByte()I

    move-result v1

    invoke-virtual {p1}, Lorg/apache/fontbox/cff/DataInput;->readUnsignedByte()I

    move-result p1

    shl-int/lit8 p2, p2, 0x18

    shl-int/lit8 v0, v0, 0x10

    or-int/2addr p2, v0

    shl-int/lit8 v0, v1, 0x8

    or-int/2addr p2, v0

    or-int/2addr p1, p2

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    return-object p1

    :cond_3
    new-instance p1, Ljava/lang/IllegalArgumentException;

    invoke-direct {p1}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw p1
.end method

.method private removeInteger(Ljava/util/List;)Ljava/lang/Integer;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/Object;",
            ">;)",
            "Ljava/lang/Integer;"
        }
    .end annotation

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-interface {p1, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Ljava/lang/Integer;

    if-eqz v1, :cond_0

    check-cast v0, Ljava/lang/Integer;

    return-object v0

    :cond_0
    check-cast v0, Lorg/apache/fontbox/cff/CharStringCommand;

    invoke-virtual {v0}, Lorg/apache/fontbox/cff/CharStringCommand;->getKey()Lorg/apache/fontbox/cff/CharStringCommand$Key;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/fontbox/cff/CharStringCommand$Key;->getValue()[I

    move-result-object v1

    const/4 v2, 0x0

    aget v1, v1, v2

    const/16 v3, 0xc

    if-ne v1, v3, :cond_1

    invoke-virtual {v0}, Lorg/apache/fontbox/cff/CharStringCommand;->getKey()Lorg/apache/fontbox/cff/CharStringCommand$Key;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/fontbox/cff/CharStringCommand$Key;->getValue()[I

    move-result-object v1

    aget v1, v1, v2

    if-ne v1, v3, :cond_1

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-interface {p1, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {p1, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    div-int/2addr p1, v0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    return-object p1

    :cond_1
    new-instance p1, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unexpected char string command: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lorg/apache/fontbox/cff/CharStringCommand;->getKey()Lorg/apache/fontbox/cff/CharStringCommand$Key;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public parse([BLjava/util/List;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([B",
            "Ljava/util/List<",
            "[B>;)",
            "Ljava/util/List<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .line 1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-direct {p0, p1, p2, v0}, Lorg/apache/fontbox/cff/Type1CharStringParser;->parse([BLjava/util/List;Ljava/util/List;)Ljava/util/List;

    move-result-object p1

    return-object p1
.end method

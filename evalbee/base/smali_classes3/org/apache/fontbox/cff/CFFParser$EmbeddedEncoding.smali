.class abstract Lorg/apache/fontbox/cff/CFFParser$EmbeddedEncoding;
.super Lorg/apache/fontbox/cff/CFFEncoding;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/fontbox/cff/CFFParser;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "EmbeddedEncoding"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/fontbox/cff/CFFParser$EmbeddedEncoding$Supplement;
    }
.end annotation


# instance fields
.field private nSups:I

.field private supplement:[Lorg/apache/fontbox/cff/CFFParser$EmbeddedEncoding$Supplement;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lorg/apache/fontbox/cff/CFFEncoding;-><init>()V

    return-void
.end method

.method public static synthetic access$2100(Lorg/apache/fontbox/cff/CFFParser$EmbeddedEncoding;)I
    .locals 0

    iget p0, p0, Lorg/apache/fontbox/cff/CFFParser$EmbeddedEncoding;->nSups:I

    return p0
.end method

.method public static synthetic access$2102(Lorg/apache/fontbox/cff/CFFParser$EmbeddedEncoding;I)I
    .locals 0

    iput p1, p0, Lorg/apache/fontbox/cff/CFFParser$EmbeddedEncoding;->nSups:I

    return p1
.end method

.method public static synthetic access$2200(Lorg/apache/fontbox/cff/CFFParser$EmbeddedEncoding;)[Lorg/apache/fontbox/cff/CFFParser$EmbeddedEncoding$Supplement;
    .locals 0

    iget-object p0, p0, Lorg/apache/fontbox/cff/CFFParser$EmbeddedEncoding;->supplement:[Lorg/apache/fontbox/cff/CFFParser$EmbeddedEncoding$Supplement;

    return-object p0
.end method

.method public static synthetic access$2202(Lorg/apache/fontbox/cff/CFFParser$EmbeddedEncoding;[Lorg/apache/fontbox/cff/CFFParser$EmbeddedEncoding$Supplement;)[Lorg/apache/fontbox/cff/CFFParser$EmbeddedEncoding$Supplement;
    .locals 0

    iput-object p1, p0, Lorg/apache/fontbox/cff/CFFParser$EmbeddedEncoding;->supplement:[Lorg/apache/fontbox/cff/CFFParser$EmbeddedEncoding$Supplement;

    return-object p1
.end method

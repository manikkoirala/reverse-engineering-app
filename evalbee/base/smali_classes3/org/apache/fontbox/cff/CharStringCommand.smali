.class public Lorg/apache/fontbox/cff/CharStringCommand;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/fontbox/cff/CharStringCommand$Key;
    }
.end annotation


# static fields
.field public static final TYPE1_VOCABULARY:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lorg/apache/fontbox/cff/CharStringCommand$Key;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field public static final TYPE2_VOCABULARY:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Lorg/apache/fontbox/cff/CharStringCommand$Key;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private commandKey:Lorg/apache/fontbox/cff/CharStringCommand$Key;


# direct methods
.method public static constructor <clinit>()V
    .locals 16

    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    new-instance v1, Lorg/apache/fontbox/cff/CharStringCommand$Key;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Lorg/apache/fontbox/cff/CharStringCommand$Key;-><init>(I)V

    const-string v3, "hstem"

    invoke-interface {v0, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Lorg/apache/fontbox/cff/CharStringCommand$Key;

    const/4 v4, 0x3

    invoke-direct {v1, v4}, Lorg/apache/fontbox/cff/CharStringCommand$Key;-><init>(I)V

    const-string v5, "vstem"

    invoke-interface {v0, v1, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Lorg/apache/fontbox/cff/CharStringCommand$Key;

    const/4 v6, 0x4

    invoke-direct {v1, v6}, Lorg/apache/fontbox/cff/CharStringCommand$Key;-><init>(I)V

    const-string v7, "vmoveto"

    invoke-interface {v0, v1, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Lorg/apache/fontbox/cff/CharStringCommand$Key;

    const/4 v8, 0x5

    invoke-direct {v1, v8}, Lorg/apache/fontbox/cff/CharStringCommand$Key;-><init>(I)V

    const-string v9, "rlineto"

    invoke-interface {v0, v1, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Lorg/apache/fontbox/cff/CharStringCommand$Key;

    const/4 v10, 0x6

    invoke-direct {v1, v10}, Lorg/apache/fontbox/cff/CharStringCommand$Key;-><init>(I)V

    const-string v11, "hlineto"

    invoke-interface {v0, v1, v11}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Lorg/apache/fontbox/cff/CharStringCommand$Key;

    const/4 v12, 0x7

    invoke-direct {v1, v12}, Lorg/apache/fontbox/cff/CharStringCommand$Key;-><init>(I)V

    const-string v13, "vlineto"

    invoke-interface {v0, v1, v13}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Lorg/apache/fontbox/cff/CharStringCommand$Key;

    const/16 v14, 0x8

    invoke-direct {v1, v14}, Lorg/apache/fontbox/cff/CharStringCommand$Key;-><init>(I)V

    const-string v15, "rrcurveto"

    invoke-interface {v0, v1, v15}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Lorg/apache/fontbox/cff/CharStringCommand$Key;

    const/16 v14, 0x9

    invoke-direct {v1, v14}, Lorg/apache/fontbox/cff/CharStringCommand$Key;-><init>(I)V

    const-string v14, "closepath"

    invoke-interface {v0, v1, v14}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Lorg/apache/fontbox/cff/CharStringCommand$Key;

    const/16 v14, 0xa

    invoke-direct {v1, v14}, Lorg/apache/fontbox/cff/CharStringCommand$Key;-><init>(I)V

    const-string v14, "callsubr"

    invoke-interface {v0, v1, v14}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Lorg/apache/fontbox/cff/CharStringCommand$Key;

    const/16 v14, 0xb

    invoke-direct {v1, v14}, Lorg/apache/fontbox/cff/CharStringCommand$Key;-><init>(I)V

    const-string v14, "return"

    invoke-interface {v0, v1, v14}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Lorg/apache/fontbox/cff/CharStringCommand$Key;

    const/16 v14, 0xc

    invoke-direct {v1, v14}, Lorg/apache/fontbox/cff/CharStringCommand$Key;-><init>(I)V

    const-string v8, "escape"

    invoke-interface {v0, v1, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Lorg/apache/fontbox/cff/CharStringCommand$Key;

    const/4 v8, 0x0

    invoke-direct {v1, v14, v8}, Lorg/apache/fontbox/cff/CharStringCommand$Key;-><init>(II)V

    const-string v8, "dotsection"

    invoke-interface {v0, v1, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Lorg/apache/fontbox/cff/CharStringCommand$Key;

    invoke-direct {v1, v14, v2}, Lorg/apache/fontbox/cff/CharStringCommand$Key;-><init>(II)V

    const-string v8, "vstem3"

    invoke-interface {v0, v1, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Lorg/apache/fontbox/cff/CharStringCommand$Key;

    const/4 v8, 0x2

    invoke-direct {v1, v14, v8}, Lorg/apache/fontbox/cff/CharStringCommand$Key;-><init>(II)V

    const-string v8, "hstem3"

    invoke-interface {v0, v1, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Lorg/apache/fontbox/cff/CharStringCommand$Key;

    invoke-direct {v1, v14, v10}, Lorg/apache/fontbox/cff/CharStringCommand$Key;-><init>(II)V

    const-string v8, "seac"

    invoke-interface {v0, v1, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Lorg/apache/fontbox/cff/CharStringCommand$Key;

    invoke-direct {v1, v14, v12}, Lorg/apache/fontbox/cff/CharStringCommand$Key;-><init>(II)V

    const-string v8, "sbw"

    invoke-interface {v0, v1, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Lorg/apache/fontbox/cff/CharStringCommand$Key;

    invoke-direct {v1, v14, v14}, Lorg/apache/fontbox/cff/CharStringCommand$Key;-><init>(II)V

    const-string v8, "div"

    invoke-interface {v0, v1, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Lorg/apache/fontbox/cff/CharStringCommand$Key;

    const/16 v8, 0x10

    invoke-direct {v1, v14, v8}, Lorg/apache/fontbox/cff/CharStringCommand$Key;-><init>(II)V

    const-string v8, "callothersubr"

    invoke-interface {v0, v1, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Lorg/apache/fontbox/cff/CharStringCommand$Key;

    const/16 v8, 0x11

    invoke-direct {v1, v14, v8}, Lorg/apache/fontbox/cff/CharStringCommand$Key;-><init>(II)V

    const-string v8, "pop"

    invoke-interface {v0, v1, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Lorg/apache/fontbox/cff/CharStringCommand$Key;

    const/16 v8, 0x21

    invoke-direct {v1, v14, v8}, Lorg/apache/fontbox/cff/CharStringCommand$Key;-><init>(II)V

    const-string v8, "setcurrentpoint"

    invoke-interface {v0, v1, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Lorg/apache/fontbox/cff/CharStringCommand$Key;

    const/16 v8, 0xd

    invoke-direct {v1, v8}, Lorg/apache/fontbox/cff/CharStringCommand$Key;-><init>(I)V

    const-string v8, "hsbw"

    invoke-interface {v0, v1, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Lorg/apache/fontbox/cff/CharStringCommand$Key;

    const/16 v8, 0xe

    invoke-direct {v1, v8}, Lorg/apache/fontbox/cff/CharStringCommand$Key;-><init>(I)V

    const-string v8, "endchar"

    invoke-interface {v0, v1, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Lorg/apache/fontbox/cff/CharStringCommand$Key;

    const/16 v8, 0x15

    invoke-direct {v1, v8}, Lorg/apache/fontbox/cff/CharStringCommand$Key;-><init>(I)V

    const-string v8, "rmoveto"

    invoke-interface {v0, v1, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Lorg/apache/fontbox/cff/CharStringCommand$Key;

    const/16 v8, 0x16

    invoke-direct {v1, v8}, Lorg/apache/fontbox/cff/CharStringCommand$Key;-><init>(I)V

    const-string v8, "hmoveto"

    invoke-interface {v0, v1, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Lorg/apache/fontbox/cff/CharStringCommand$Key;

    const/16 v8, 0x1e

    invoke-direct {v1, v8}, Lorg/apache/fontbox/cff/CharStringCommand$Key;-><init>(I)V

    const-string v8, "vhcurveto"

    invoke-interface {v0, v1, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Lorg/apache/fontbox/cff/CharStringCommand$Key;

    const/16 v8, 0x1f

    invoke-direct {v1, v8}, Lorg/apache/fontbox/cff/CharStringCommand$Key;-><init>(I)V

    const-string v8, "hvcurveto"

    invoke-interface {v0, v1, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lorg/apache/fontbox/cff/CharStringCommand;->TYPE1_VOCABULARY:Ljava/util/Map;

    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    new-instance v1, Lorg/apache/fontbox/cff/CharStringCommand$Key;

    invoke-direct {v1, v2}, Lorg/apache/fontbox/cff/CharStringCommand$Key;-><init>(I)V

    invoke-interface {v0, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Lorg/apache/fontbox/cff/CharStringCommand$Key;

    invoke-direct {v1, v4}, Lorg/apache/fontbox/cff/CharStringCommand$Key;-><init>(I)V

    invoke-interface {v0, v1, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Lorg/apache/fontbox/cff/CharStringCommand$Key;

    invoke-direct {v1, v6}, Lorg/apache/fontbox/cff/CharStringCommand$Key;-><init>(I)V

    invoke-interface {v0, v1, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Lorg/apache/fontbox/cff/CharStringCommand$Key;

    const/4 v2, 0x5

    invoke-direct {v1, v2}, Lorg/apache/fontbox/cff/CharStringCommand$Key;-><init>(I)V

    invoke-interface {v0, v1, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Lorg/apache/fontbox/cff/CharStringCommand$Key;

    invoke-direct {v1, v10}, Lorg/apache/fontbox/cff/CharStringCommand$Key;-><init>(I)V

    invoke-interface {v0, v1, v11}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Lorg/apache/fontbox/cff/CharStringCommand$Key;

    invoke-direct {v1, v12}, Lorg/apache/fontbox/cff/CharStringCommand$Key;-><init>(I)V

    invoke-interface {v0, v1, v13}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Lorg/apache/fontbox/cff/CharStringCommand$Key;

    const/16 v2, 0x8

    invoke-direct {v1, v2}, Lorg/apache/fontbox/cff/CharStringCommand$Key;-><init>(I)V

    invoke-interface {v0, v1, v15}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Lorg/apache/fontbox/cff/CharStringCommand$Key;

    const/16 v2, 0xa

    invoke-direct {v1, v2}, Lorg/apache/fontbox/cff/CharStringCommand$Key;-><init>(I)V

    const-string v2, "callsubr"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Lorg/apache/fontbox/cff/CharStringCommand$Key;

    const/16 v2, 0xb

    invoke-direct {v1, v2}, Lorg/apache/fontbox/cff/CharStringCommand$Key;-><init>(I)V

    const-string v2, "return"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Lorg/apache/fontbox/cff/CharStringCommand$Key;

    invoke-direct {v1, v14}, Lorg/apache/fontbox/cff/CharStringCommand$Key;-><init>(I)V

    const-string v2, "escape"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Lorg/apache/fontbox/cff/CharStringCommand$Key;

    invoke-direct {v1, v14, v4}, Lorg/apache/fontbox/cff/CharStringCommand$Key;-><init>(II)V

    const-string v2, "and"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Lorg/apache/fontbox/cff/CharStringCommand$Key;

    invoke-direct {v1, v14, v6}, Lorg/apache/fontbox/cff/CharStringCommand$Key;-><init>(II)V

    const-string v2, "or"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Lorg/apache/fontbox/cff/CharStringCommand$Key;

    const/4 v2, 0x5

    invoke-direct {v1, v14, v2}, Lorg/apache/fontbox/cff/CharStringCommand$Key;-><init>(II)V

    const-string v2, "not"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Lorg/apache/fontbox/cff/CharStringCommand$Key;

    const/16 v2, 0x9

    invoke-direct {v1, v14, v2}, Lorg/apache/fontbox/cff/CharStringCommand$Key;-><init>(II)V

    const-string v2, "abs"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Lorg/apache/fontbox/cff/CharStringCommand$Key;

    const/16 v2, 0xa

    invoke-direct {v1, v14, v2}, Lorg/apache/fontbox/cff/CharStringCommand$Key;-><init>(II)V

    const-string v2, "add"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Lorg/apache/fontbox/cff/CharStringCommand$Key;

    const/16 v2, 0xb

    invoke-direct {v1, v14, v2}, Lorg/apache/fontbox/cff/CharStringCommand$Key;-><init>(II)V

    const-string v2, "sub"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Lorg/apache/fontbox/cff/CharStringCommand$Key;

    invoke-direct {v1, v14, v14}, Lorg/apache/fontbox/cff/CharStringCommand$Key;-><init>(II)V

    const-string v2, "div"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Lorg/apache/fontbox/cff/CharStringCommand$Key;

    const/16 v2, 0xe

    invoke-direct {v1, v14, v2}, Lorg/apache/fontbox/cff/CharStringCommand$Key;-><init>(II)V

    const-string v2, "neg"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Lorg/apache/fontbox/cff/CharStringCommand$Key;

    const/16 v2, 0xf

    invoke-direct {v1, v14, v2}, Lorg/apache/fontbox/cff/CharStringCommand$Key;-><init>(II)V

    const-string v2, "eq"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Lorg/apache/fontbox/cff/CharStringCommand$Key;

    const/16 v2, 0x12

    invoke-direct {v1, v14, v2}, Lorg/apache/fontbox/cff/CharStringCommand$Key;-><init>(II)V

    const-string v2, "drop"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Lorg/apache/fontbox/cff/CharStringCommand$Key;

    const/16 v2, 0x14

    invoke-direct {v1, v14, v2}, Lorg/apache/fontbox/cff/CharStringCommand$Key;-><init>(II)V

    const-string v2, "put"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Lorg/apache/fontbox/cff/CharStringCommand$Key;

    const/16 v2, 0x15

    invoke-direct {v1, v14, v2}, Lorg/apache/fontbox/cff/CharStringCommand$Key;-><init>(II)V

    const-string v2, "get"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Lorg/apache/fontbox/cff/CharStringCommand$Key;

    const/16 v2, 0x16

    invoke-direct {v1, v14, v2}, Lorg/apache/fontbox/cff/CharStringCommand$Key;-><init>(II)V

    const-string v2, "ifelse"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Lorg/apache/fontbox/cff/CharStringCommand$Key;

    const/16 v2, 0x17

    invoke-direct {v1, v14, v2}, Lorg/apache/fontbox/cff/CharStringCommand$Key;-><init>(II)V

    const-string v2, "random"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Lorg/apache/fontbox/cff/CharStringCommand$Key;

    const/16 v2, 0x18

    invoke-direct {v1, v14, v2}, Lorg/apache/fontbox/cff/CharStringCommand$Key;-><init>(II)V

    const-string v2, "mul"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Lorg/apache/fontbox/cff/CharStringCommand$Key;

    const/16 v2, 0x1a

    invoke-direct {v1, v14, v2}, Lorg/apache/fontbox/cff/CharStringCommand$Key;-><init>(II)V

    const-string v2, "sqrt"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Lorg/apache/fontbox/cff/CharStringCommand$Key;

    const/16 v2, 0x1b

    invoke-direct {v1, v14, v2}, Lorg/apache/fontbox/cff/CharStringCommand$Key;-><init>(II)V

    const-string v2, "dup"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Lorg/apache/fontbox/cff/CharStringCommand$Key;

    const/16 v2, 0x1c

    invoke-direct {v1, v14, v2}, Lorg/apache/fontbox/cff/CharStringCommand$Key;-><init>(II)V

    const-string v2, "exch"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Lorg/apache/fontbox/cff/CharStringCommand$Key;

    const/16 v2, 0x1d

    invoke-direct {v1, v14, v2}, Lorg/apache/fontbox/cff/CharStringCommand$Key;-><init>(II)V

    const-string v2, "index"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Lorg/apache/fontbox/cff/CharStringCommand$Key;

    const/16 v2, 0x1e

    invoke-direct {v1, v14, v2}, Lorg/apache/fontbox/cff/CharStringCommand$Key;-><init>(II)V

    const-string v2, "roll"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Lorg/apache/fontbox/cff/CharStringCommand$Key;

    const/16 v2, 0x22

    invoke-direct {v1, v14, v2}, Lorg/apache/fontbox/cff/CharStringCommand$Key;-><init>(II)V

    const-string v2, "hflex"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Lorg/apache/fontbox/cff/CharStringCommand$Key;

    const/16 v2, 0x23

    invoke-direct {v1, v14, v2}, Lorg/apache/fontbox/cff/CharStringCommand$Key;-><init>(II)V

    const-string v2, "flex"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Lorg/apache/fontbox/cff/CharStringCommand$Key;

    const/16 v2, 0x24

    invoke-direct {v1, v14, v2}, Lorg/apache/fontbox/cff/CharStringCommand$Key;-><init>(II)V

    const-string v2, "hflex1"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Lorg/apache/fontbox/cff/CharStringCommand$Key;

    const/16 v2, 0x25

    invoke-direct {v1, v14, v2}, Lorg/apache/fontbox/cff/CharStringCommand$Key;-><init>(II)V

    const-string v2, "flex1"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Lorg/apache/fontbox/cff/CharStringCommand$Key;

    const/16 v2, 0xe

    invoke-direct {v1, v2}, Lorg/apache/fontbox/cff/CharStringCommand$Key;-><init>(I)V

    const-string v2, "endchar"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Lorg/apache/fontbox/cff/CharStringCommand$Key;

    const/16 v2, 0x12

    invoke-direct {v1, v2}, Lorg/apache/fontbox/cff/CharStringCommand$Key;-><init>(I)V

    const-string v2, "hstemhm"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Lorg/apache/fontbox/cff/CharStringCommand$Key;

    const/16 v2, 0x13

    invoke-direct {v1, v2}, Lorg/apache/fontbox/cff/CharStringCommand$Key;-><init>(I)V

    const-string v2, "hintmask"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Lorg/apache/fontbox/cff/CharStringCommand$Key;

    const/16 v2, 0x14

    invoke-direct {v1, v2}, Lorg/apache/fontbox/cff/CharStringCommand$Key;-><init>(I)V

    const-string v2, "cntrmask"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Lorg/apache/fontbox/cff/CharStringCommand$Key;

    const/16 v2, 0x15

    invoke-direct {v1, v2}, Lorg/apache/fontbox/cff/CharStringCommand$Key;-><init>(I)V

    const-string v2, "rmoveto"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Lorg/apache/fontbox/cff/CharStringCommand$Key;

    const/16 v2, 0x16

    invoke-direct {v1, v2}, Lorg/apache/fontbox/cff/CharStringCommand$Key;-><init>(I)V

    const-string v2, "hmoveto"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Lorg/apache/fontbox/cff/CharStringCommand$Key;

    const/16 v2, 0x17

    invoke-direct {v1, v2}, Lorg/apache/fontbox/cff/CharStringCommand$Key;-><init>(I)V

    const-string v2, "vstemhm"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Lorg/apache/fontbox/cff/CharStringCommand$Key;

    const/16 v2, 0x18

    invoke-direct {v1, v2}, Lorg/apache/fontbox/cff/CharStringCommand$Key;-><init>(I)V

    const-string v2, "rcurveline"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Lorg/apache/fontbox/cff/CharStringCommand$Key;

    const/16 v2, 0x19

    invoke-direct {v1, v2}, Lorg/apache/fontbox/cff/CharStringCommand$Key;-><init>(I)V

    const-string v2, "rlinecurve"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Lorg/apache/fontbox/cff/CharStringCommand$Key;

    const/16 v2, 0x1a

    invoke-direct {v1, v2}, Lorg/apache/fontbox/cff/CharStringCommand$Key;-><init>(I)V

    const-string v2, "vvcurveto"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Lorg/apache/fontbox/cff/CharStringCommand$Key;

    const/16 v2, 0x1b

    invoke-direct {v1, v2}, Lorg/apache/fontbox/cff/CharStringCommand$Key;-><init>(I)V

    const-string v2, "hhcurveto"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Lorg/apache/fontbox/cff/CharStringCommand$Key;

    const/16 v2, 0x1c

    invoke-direct {v1, v2}, Lorg/apache/fontbox/cff/CharStringCommand$Key;-><init>(I)V

    const-string v2, "shortint"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Lorg/apache/fontbox/cff/CharStringCommand$Key;

    const/16 v2, 0x1d

    invoke-direct {v1, v2}, Lorg/apache/fontbox/cff/CharStringCommand$Key;-><init>(I)V

    const-string v2, "callgsubr"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Lorg/apache/fontbox/cff/CharStringCommand$Key;

    const/16 v2, 0x1e

    invoke-direct {v1, v2}, Lorg/apache/fontbox/cff/CharStringCommand$Key;-><init>(I)V

    const-string v2, "vhcurveto"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Lorg/apache/fontbox/cff/CharStringCommand$Key;

    const/16 v2, 0x1f

    invoke-direct {v1, v2}, Lorg/apache/fontbox/cff/CharStringCommand$Key;-><init>(I)V

    const-string v2, "hvcurveto"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lorg/apache/fontbox/cff/CharStringCommand;->TYPE2_VOCABULARY:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>(I)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/fontbox/cff/CharStringCommand;->commandKey:Lorg/apache/fontbox/cff/CharStringCommand$Key;

    new-instance v0, Lorg/apache/fontbox/cff/CharStringCommand$Key;

    invoke-direct {v0, p1}, Lorg/apache/fontbox/cff/CharStringCommand$Key;-><init>(I)V

    invoke-direct {p0, v0}, Lorg/apache/fontbox/cff/CharStringCommand;->setKey(Lorg/apache/fontbox/cff/CharStringCommand$Key;)V

    return-void
.end method

.method public constructor <init>(II)V
    .locals 1

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/fontbox/cff/CharStringCommand;->commandKey:Lorg/apache/fontbox/cff/CharStringCommand$Key;

    new-instance v0, Lorg/apache/fontbox/cff/CharStringCommand$Key;

    invoke-direct {v0, p1, p2}, Lorg/apache/fontbox/cff/CharStringCommand$Key;-><init>(II)V

    invoke-direct {p0, v0}, Lorg/apache/fontbox/cff/CharStringCommand;->setKey(Lorg/apache/fontbox/cff/CharStringCommand$Key;)V

    return-void
.end method

.method public constructor <init>([I)V
    .locals 1

    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/fontbox/cff/CharStringCommand;->commandKey:Lorg/apache/fontbox/cff/CharStringCommand$Key;

    new-instance v0, Lorg/apache/fontbox/cff/CharStringCommand$Key;

    invoke-direct {v0, p1}, Lorg/apache/fontbox/cff/CharStringCommand$Key;-><init>([I)V

    invoke-direct {p0, v0}, Lorg/apache/fontbox/cff/CharStringCommand;->setKey(Lorg/apache/fontbox/cff/CharStringCommand$Key;)V

    return-void
.end method

.method private setKey(Lorg/apache/fontbox/cff/CharStringCommand$Key;)V
    .locals 0

    iput-object p1, p0, Lorg/apache/fontbox/cff/CharStringCommand;->commandKey:Lorg/apache/fontbox/cff/CharStringCommand$Key;

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 1

    instance-of v0, p1, Lorg/apache/fontbox/cff/CharStringCommand;

    if-eqz v0, :cond_0

    check-cast p1, Lorg/apache/fontbox/cff/CharStringCommand;

    invoke-virtual {p0}, Lorg/apache/fontbox/cff/CharStringCommand;->getKey()Lorg/apache/fontbox/cff/CharStringCommand$Key;

    move-result-object v0

    invoke-virtual {p1}, Lorg/apache/fontbox/cff/CharStringCommand;->getKey()Lorg/apache/fontbox/cff/CharStringCommand$Key;

    move-result-object p1

    invoke-virtual {v0, p1}, Lorg/apache/fontbox/cff/CharStringCommand$Key;->equals(Ljava/lang/Object;)Z

    move-result p1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method public getKey()Lorg/apache/fontbox/cff/CharStringCommand$Key;
    .locals 1

    iget-object v0, p0, Lorg/apache/fontbox/cff/CharStringCommand;->commandKey:Lorg/apache/fontbox/cff/CharStringCommand$Key;

    return-object v0
.end method

.method public hashCode()I
    .locals 1

    invoke-virtual {p0}, Lorg/apache/fontbox/cff/CharStringCommand;->getKey()Lorg/apache/fontbox/cff/CharStringCommand$Key;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/fontbox/cff/CharStringCommand$Key;->hashCode()I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    sget-object v0, Lorg/apache/fontbox/cff/CharStringCommand;->TYPE2_VOCABULARY:Ljava/util/Map;

    invoke-virtual {p0}, Lorg/apache/fontbox/cff/CharStringCommand;->getKey()Lorg/apache/fontbox/cff/CharStringCommand$Key;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-nez v0, :cond_0

    sget-object v0, Lorg/apache/fontbox/cff/CharStringCommand;->TYPE1_VOCABULARY:Ljava/util/Map;

    invoke-virtual {p0}, Lorg/apache/fontbox/cff/CharStringCommand;->getKey()Lorg/apache/fontbox/cff/CharStringCommand$Key;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    :cond_0
    const/16 v1, 0x7c

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lorg/apache/fontbox/cff/CharStringCommand;->getKey()Lorg/apache/fontbox/cff/CharStringCommand$Key;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/fontbox/cff/CharStringCommand$Key;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.class public final Lorg/apache/fontbox/cff/CFFExpertEncoding;
.super Lorg/apache/fontbox/cff/CFFEncoding;
.source "SourceFile"


# static fields
.field private static final INSTANCE:Lorg/apache/fontbox/cff/CFFExpertEncoding;


# direct methods
.method public static constructor <clinit>()V
    .locals 16

    new-instance v0, Lorg/apache/fontbox/cff/CFFExpertEncoding;

    invoke-direct {v0}, Lorg/apache/fontbox/cff/CFFExpertEncoding;-><init>()V

    sput-object v0, Lorg/apache/fontbox/cff/CFFExpertEncoding;->INSTANCE:Lorg/apache/fontbox/cff/CFFExpertEncoding;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/4 v3, 0x2

    invoke-virtual {v0, v3, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/4 v3, 0x3

    invoke-virtual {v0, v3, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/4 v3, 0x4

    invoke-virtual {v0, v3, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/4 v3, 0x5

    invoke-virtual {v0, v3, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/4 v3, 0x6

    invoke-virtual {v0, v3, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/4 v3, 0x7

    invoke-virtual {v0, v3, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v3, 0x8

    invoke-virtual {v0, v3, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v3, 0x9

    invoke-virtual {v0, v3, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v3, 0xa

    invoke-virtual {v0, v3, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v3, 0xb

    invoke-virtual {v0, v3, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v3, 0xc

    invoke-virtual {v0, v3, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v3, 0xd

    invoke-virtual {v0, v3, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v4, 0xe

    invoke-virtual {v0, v4, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v5, 0xf

    invoke-virtual {v0, v5, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v6, 0x10

    invoke-virtual {v0, v6, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v6, 0x11

    invoke-virtual {v0, v6, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v6, 0x12

    invoke-virtual {v0, v6, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v6, 0x13

    invoke-virtual {v0, v6, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v6, 0x14

    invoke-virtual {v0, v6, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v6, 0x15

    invoke-virtual {v0, v6, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v6, 0x16

    invoke-virtual {v0, v6, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v6, 0x17

    invoke-virtual {v0, v6, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v6, 0x18

    invoke-virtual {v0, v6, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v6, 0x19

    invoke-virtual {v0, v6, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v6, 0x1a

    invoke-virtual {v0, v6, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v6, 0x1b

    invoke-virtual {v0, v6, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v7, 0x1c

    invoke-virtual {v0, v7, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v8, 0x1d

    invoke-virtual {v0, v8, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v8, 0x1e

    invoke-virtual {v0, v8, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v8, 0x1f

    invoke-virtual {v0, v8, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v8, 0x20

    invoke-virtual {v0, v8, v2}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x21

    const/16 v8, 0xe5

    invoke-virtual {v0, v2, v8}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x22

    const/16 v9, 0xe6

    invoke-virtual {v0, v2, v9}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x23

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x24

    const/16 v10, 0xe7

    invoke-virtual {v0, v2, v10}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x25

    const/16 v11, 0xe8

    invoke-virtual {v0, v2, v11}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x26

    const/16 v12, 0xe9

    invoke-virtual {v0, v2, v12}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x27

    const/16 v13, 0xea

    invoke-virtual {v0, v2, v13}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x28

    const/16 v14, 0xeb

    invoke-virtual {v0, v2, v14}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x29

    const/16 v15, 0xec

    invoke-virtual {v0, v2, v15}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x2a

    const/16 v15, 0xed

    invoke-virtual {v0, v2, v15}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x2b

    const/16 v15, 0xee

    invoke-virtual {v0, v2, v15}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x2c

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x2d

    invoke-virtual {v0, v2, v4}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x2e

    invoke-virtual {v0, v2, v5}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x2f

    const/16 v3, 0x63

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x30

    const/16 v4, 0xef

    invoke-virtual {v0, v2, v4}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x31

    const/16 v5, 0xf0

    invoke-virtual {v0, v2, v5}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x32

    const/16 v5, 0xf1

    invoke-virtual {v0, v2, v5}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x33

    const/16 v5, 0xf2

    invoke-virtual {v0, v2, v5}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x34

    const/16 v5, 0xf3

    invoke-virtual {v0, v2, v5}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x35

    const/16 v5, 0xf4

    invoke-virtual {v0, v2, v5}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x36

    const/16 v5, 0xf5

    invoke-virtual {v0, v2, v5}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x37

    const/16 v5, 0xf6

    invoke-virtual {v0, v2, v5}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x38

    const/16 v5, 0xf7

    invoke-virtual {v0, v2, v5}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x39

    const/16 v5, 0xf8

    invoke-virtual {v0, v2, v5}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x3a

    invoke-virtual {v0, v2, v6}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x3b

    invoke-virtual {v0, v2, v7}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x3c

    const/16 v5, 0xf9

    invoke-virtual {v0, v2, v5}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x3d

    const/16 v5, 0xfa

    invoke-virtual {v0, v2, v5}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x3e

    const/16 v5, 0xfb

    invoke-virtual {v0, v2, v5}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x3f

    const/16 v5, 0xfc

    invoke-virtual {v0, v2, v5}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x40

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x41

    const/16 v5, 0xfd

    invoke-virtual {v0, v2, v5}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x42

    const/16 v5, 0xfe

    invoke-virtual {v0, v2, v5}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x43

    const/16 v5, 0xff

    invoke-virtual {v0, v2, v5}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x44

    const/16 v5, 0x100

    invoke-virtual {v0, v2, v5}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x45

    const/16 v5, 0x101

    invoke-virtual {v0, v2, v5}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x46

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x47

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x48

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x49

    const/16 v5, 0x102

    invoke-virtual {v0, v2, v5}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x4a

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x4b

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x4c

    const/16 v5, 0x103

    invoke-virtual {v0, v2, v5}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x4d

    const/16 v5, 0x104

    invoke-virtual {v0, v2, v5}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x4e

    const/16 v5, 0x105

    invoke-virtual {v0, v2, v5}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x4f

    const/16 v5, 0x106

    invoke-virtual {v0, v2, v5}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x50

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x51

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x52

    const/16 v5, 0x107

    invoke-virtual {v0, v2, v5}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x53

    const/16 v5, 0x108

    invoke-virtual {v0, v2, v5}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x54

    const/16 v5, 0x109

    invoke-virtual {v0, v2, v5}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x55

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x56

    const/16 v5, 0x10a

    invoke-virtual {v0, v2, v5}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x57

    const/16 v5, 0x6d

    invoke-virtual {v0, v2, v5}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x58

    const/16 v5, 0x6e

    invoke-virtual {v0, v2, v5}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x59

    const/16 v5, 0x10b

    invoke-virtual {v0, v2, v5}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x5a

    const/16 v5, 0x10c

    invoke-virtual {v0, v2, v5}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x5b

    const/16 v5, 0x10d

    invoke-virtual {v0, v2, v5}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x5c

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x5d

    const/16 v5, 0x10e

    invoke-virtual {v0, v2, v5}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x5e

    const/16 v5, 0x10f

    invoke-virtual {v0, v2, v5}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x5f

    const/16 v5, 0x110

    invoke-virtual {v0, v2, v5}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x60

    const/16 v5, 0x111

    invoke-virtual {v0, v2, v5}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x61

    const/16 v5, 0x112

    invoke-virtual {v0, v2, v5}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x62

    const/16 v5, 0x113

    invoke-virtual {v0, v2, v5}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x114

    invoke-virtual {v0, v3, v2}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x64

    const/16 v3, 0x115

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x65

    const/16 v3, 0x116

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x66

    const/16 v3, 0x117

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x67

    const/16 v3, 0x118

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x68

    const/16 v3, 0x119

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x69

    const/16 v3, 0x11a

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x6a

    const/16 v3, 0x11b

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x6b

    const/16 v3, 0x11c

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x6c

    const/16 v3, 0x11d

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x6d

    const/16 v3, 0x11e

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x6e

    const/16 v3, 0x11f

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x6f

    const/16 v3, 0x120

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x70

    const/16 v3, 0x121

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x71

    const/16 v3, 0x122

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x72

    const/16 v3, 0x123

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x73

    const/16 v3, 0x124

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x74

    const/16 v3, 0x125

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x75

    const/16 v3, 0x126

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x76

    const/16 v3, 0x127

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x77

    const/16 v3, 0x128

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x78

    const/16 v3, 0x129

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x79

    const/16 v3, 0x12a

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x7a

    const/16 v3, 0x12b

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x7b

    const/16 v3, 0x12c

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x7c

    const/16 v3, 0x12d

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x7d

    const/16 v3, 0x12e

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x7e

    const/16 v3, 0x12f

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x7f

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x80

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x81

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x82

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x83

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x84

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x85

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x86

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x87

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x88

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x89

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x8a

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x8b

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x8c

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x8d

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x8e

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x8f

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x90

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x91

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x92

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x93

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x94

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x95

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x96

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x97

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x98

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x99

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x9a

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x9b

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x9c

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x9d

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x9e

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0x9f

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xa0

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xa1

    const/16 v3, 0x130

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xa2

    const/16 v3, 0x131

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xa3

    const/16 v3, 0x132

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xa4

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xa5

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xa6

    const/16 v3, 0x133

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xa7

    const/16 v3, 0x134

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xa8

    const/16 v3, 0x135

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xa9

    const/16 v3, 0x136

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xaa

    const/16 v3, 0x137

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xab

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xac

    const/16 v3, 0x138

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xad

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xae

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xaf

    const/16 v3, 0x139

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xb0

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xb1

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xb2

    const/16 v3, 0x13a

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xb3

    const/16 v3, 0x13b

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xb4

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xb5

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xb6

    const/16 v3, 0x13c

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xb7

    const/16 v3, 0x13d

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xb8

    const/16 v3, 0x13e

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xb9

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xba

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xbb

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xbc

    const/16 v3, 0x9e

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xbd

    const/16 v3, 0x9b

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xbe

    const/16 v3, 0xa3

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xbf

    const/16 v3, 0x13f

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xc0

    const/16 v3, 0x140

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xc1

    const/16 v3, 0x141

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xc2

    const/16 v3, 0x142

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xc3

    const/16 v3, 0x143

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xc4

    const/16 v3, 0x144

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xc5

    const/16 v3, 0x145

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xc6

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v2, 0xc7

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v1, 0xc8

    const/16 v2, 0x146

    invoke-virtual {v0, v1, v2}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v1, 0xc9

    const/16 v2, 0x96

    invoke-virtual {v0, v1, v2}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v1, 0xca

    const/16 v2, 0xa4

    invoke-virtual {v0, v1, v2}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v1, 0xcb

    const/16 v2, 0xa9

    invoke-virtual {v0, v1, v2}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v1, 0xcc

    const/16 v2, 0x147

    invoke-virtual {v0, v1, v2}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v1, 0xcd

    const/16 v2, 0x148

    invoke-virtual {v0, v1, v2}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v1, 0xce

    const/16 v2, 0x149

    invoke-virtual {v0, v1, v2}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v1, 0xcf

    const/16 v2, 0x14a

    invoke-virtual {v0, v1, v2}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v1, 0xd0

    const/16 v2, 0x14b

    invoke-virtual {v0, v1, v2}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v1, 0xd1

    const/16 v2, 0x14c

    invoke-virtual {v0, v1, v2}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v1, 0xd2

    const/16 v2, 0x14d

    invoke-virtual {v0, v1, v2}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v1, 0xd3

    const/16 v2, 0x14e

    invoke-virtual {v0, v1, v2}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v1, 0xd4

    const/16 v2, 0x14f

    invoke-virtual {v0, v1, v2}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v1, 0xd5

    const/16 v2, 0x150

    invoke-virtual {v0, v1, v2}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v1, 0xd6

    const/16 v2, 0x151

    invoke-virtual {v0, v1, v2}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v1, 0xd7

    const/16 v2, 0x152

    invoke-virtual {v0, v1, v2}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v1, 0xd8

    const/16 v2, 0x153

    invoke-virtual {v0, v1, v2}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v1, 0xd9

    const/16 v2, 0x154

    invoke-virtual {v0, v1, v2}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v1, 0xda

    const/16 v2, 0x155

    invoke-virtual {v0, v1, v2}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v1, 0xdb

    const/16 v2, 0x156

    invoke-virtual {v0, v1, v2}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v1, 0xdc

    const/16 v2, 0x157

    invoke-virtual {v0, v1, v2}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v1, 0xdd

    const/16 v2, 0x158

    invoke-virtual {v0, v1, v2}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v1, 0xde

    const/16 v2, 0x159

    invoke-virtual {v0, v1, v2}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v1, 0xdf

    const/16 v2, 0x15a

    invoke-virtual {v0, v1, v2}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v1, 0xe0

    const/16 v2, 0x15b

    invoke-virtual {v0, v1, v2}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v1, 0xe1

    const/16 v2, 0x15c

    invoke-virtual {v0, v1, v2}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v1, 0xe2

    const/16 v2, 0x15d

    invoke-virtual {v0, v1, v2}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v1, 0xe3

    const/16 v2, 0x15e

    invoke-virtual {v0, v1, v2}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v1, 0xe4

    const/16 v2, 0x15f

    invoke-virtual {v0, v1, v2}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v1, 0x160

    invoke-virtual {v0, v8, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v1, 0x161

    invoke-virtual {v0, v9, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v1, 0x162

    invoke-virtual {v0, v10, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v1, 0x163

    invoke-virtual {v0, v11, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v1, 0x164

    invoke-virtual {v0, v12, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v1, 0x165

    invoke-virtual {v0, v13, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v1, 0x166

    invoke-virtual {v0, v14, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v1, 0x167

    const/16 v2, 0xec

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v1, 0x168

    const/16 v2, 0xed

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v1, 0x169

    invoke-virtual {v0, v15, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v1, 0x16a

    invoke-virtual {v0, v4, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v1, 0x16b

    const/16 v2, 0xf0

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v1, 0x16c

    const/16 v2, 0xf1

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v1, 0x16d

    const/16 v2, 0xf2

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v1, 0xf3

    const/16 v2, 0x16e

    invoke-virtual {v0, v1, v2}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v1, 0xf4

    const/16 v2, 0x16f

    invoke-virtual {v0, v1, v2}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v1, 0xf5

    const/16 v2, 0x170

    invoke-virtual {v0, v1, v2}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v1, 0xf6

    const/16 v2, 0x171

    invoke-virtual {v0, v1, v2}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v1, 0xf7

    const/16 v2, 0x172

    invoke-virtual {v0, v1, v2}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v1, 0xf8

    const/16 v2, 0x173

    invoke-virtual {v0, v1, v2}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v1, 0xf9

    const/16 v2, 0x174

    invoke-virtual {v0, v1, v2}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v1, 0xfa

    const/16 v2, 0x175

    invoke-virtual {v0, v1, v2}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v1, 0xfb

    const/16 v2, 0x176

    invoke-virtual {v0, v1, v2}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v1, 0xfc

    const/16 v2, 0x177

    invoke-virtual {v0, v1, v2}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v1, 0xfd

    const/16 v2, 0x178

    invoke-virtual {v0, v1, v2}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v1, 0xfe

    const/16 v2, 0x179

    invoke-virtual {v0, v1, v2}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    const/16 v1, 0xff

    const/16 v2, 0x17a

    invoke-virtual {v0, v1, v2}, Lorg/apache/fontbox/cff/CFFEncoding;->add(II)V

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lorg/apache/fontbox/cff/CFFEncoding;-><init>()V

    return-void
.end method

.method public static getInstance()Lorg/apache/fontbox/cff/CFFExpertEncoding;
    .locals 1

    sget-object v0, Lorg/apache/fontbox/cff/CFFExpertEncoding;->INSTANCE:Lorg/apache/fontbox/cff/CFFExpertEncoding;

    return-object v0
.end method

.class public Lorg/apache/fontbox/ttf/GlyfCompositeDescript;
.super Lorg/apache/fontbox/ttf/GlyfDescript;
.source "SourceFile"


# instance fields
.field private beingResolved:Z

.field private final components:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lorg/apache/fontbox/ttf/GlyfCompositeComp;",
            ">;"
        }
    .end annotation
.end field

.field private glyphTable:Lorg/apache/fontbox/ttf/GlyphTable;

.field private resolved:Z


# direct methods
.method public constructor <init>(Lorg/apache/fontbox/ttf/TTFDataStream;Lorg/apache/fontbox/ttf/GlyphTable;)V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0, v0, p1}, Lorg/apache/fontbox/ttf/GlyfDescript;-><init>(SLorg/apache/fontbox/ttf/TTFDataStream;)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/apache/fontbox/ttf/GlyfCompositeDescript;->components:Ljava/util/List;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/apache/fontbox/ttf/GlyfCompositeDescript;->beingResolved:Z

    iput-boolean v0, p0, Lorg/apache/fontbox/ttf/GlyfCompositeDescript;->resolved:Z

    iput-object p2, p0, Lorg/apache/fontbox/ttf/GlyfCompositeDescript;->glyphTable:Lorg/apache/fontbox/ttf/GlyphTable;

    :cond_0
    new-instance p2, Lorg/apache/fontbox/ttf/GlyfCompositeComp;

    invoke-direct {p2, p1}, Lorg/apache/fontbox/ttf/GlyfCompositeComp;-><init>(Lorg/apache/fontbox/ttf/TTFDataStream;)V

    iget-object v0, p0, Lorg/apache/fontbox/ttf/GlyfCompositeDescript;->components:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {p2}, Lorg/apache/fontbox/ttf/GlyfCompositeComp;->getFlags()S

    move-result v0

    and-int/lit8 v0, v0, 0x20

    if-nez v0, :cond_0

    invoke-virtual {p2}, Lorg/apache/fontbox/ttf/GlyfCompositeComp;->getFlags()S

    move-result p2

    and-int/lit16 p2, p2, 0x100

    if-eqz p2, :cond_1

    invoke-virtual {p1}, Lorg/apache/fontbox/ttf/TTFDataStream;->readUnsignedShort()I

    move-result p2

    invoke-virtual {p0, p1, p2}, Lorg/apache/fontbox/ttf/GlyfDescript;->readInstructions(Lorg/apache/fontbox/ttf/TTFDataStream;I)V

    :cond_1
    return-void
.end method

.method private getCompositeComp(I)Lorg/apache/fontbox/ttf/GlyfCompositeComp;
    .locals 4

    iget-object v0, p0, Lorg/apache/fontbox/ttf/GlyfCompositeDescript;->components:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/fontbox/ttf/GlyfCompositeComp;

    invoke-virtual {v1}, Lorg/apache/fontbox/ttf/GlyfCompositeComp;->getGlyphIndex()I

    move-result v2

    invoke-direct {p0, v2}, Lorg/apache/fontbox/ttf/GlyfCompositeDescript;->getGlypDescription(I)Lorg/apache/fontbox/ttf/GlyphDescription;

    move-result-object v2

    invoke-virtual {v1}, Lorg/apache/fontbox/ttf/GlyfCompositeComp;->getFirstIndex()I

    move-result v3

    if-gt v3, p1, :cond_0

    invoke-virtual {v1}, Lorg/apache/fontbox/ttf/GlyfCompositeComp;->getFirstIndex()I

    move-result v3

    invoke-interface {v2}, Lorg/apache/fontbox/ttf/GlyphDescription;->getPointCount()I

    move-result v2

    add-int/2addr v3, v2

    if-ge p1, v3, :cond_0

    return-object v1

    :cond_1
    const/4 p1, 0x0

    return-object p1
.end method

.method private getCompositeCompEndPt(I)Lorg/apache/fontbox/ttf/GlyfCompositeComp;
    .locals 4

    iget-object v0, p0, Lorg/apache/fontbox/ttf/GlyfCompositeDescript;->components:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lorg/apache/fontbox/ttf/GlyfCompositeComp;

    invoke-virtual {v1}, Lorg/apache/fontbox/ttf/GlyfCompositeComp;->getGlyphIndex()I

    move-result v2

    invoke-direct {p0, v2}, Lorg/apache/fontbox/ttf/GlyfCompositeDescript;->getGlypDescription(I)Lorg/apache/fontbox/ttf/GlyphDescription;

    move-result-object v2

    invoke-virtual {v1}, Lorg/apache/fontbox/ttf/GlyfCompositeComp;->getFirstContour()I

    move-result v3

    if-gt v3, p1, :cond_0

    invoke-virtual {v1}, Lorg/apache/fontbox/ttf/GlyfCompositeComp;->getFirstContour()I

    move-result v3

    invoke-interface {v2}, Lorg/apache/fontbox/ttf/GlyphDescription;->getContourCount()I

    move-result v2

    add-int/2addr v3, v2

    if-ge p1, v3, :cond_0

    return-object v1

    :cond_1
    const/4 p1, 0x0

    return-object p1
.end method

.method private getGlypDescription(I)Lorg/apache/fontbox/ttf/GlyphDescription;
    .locals 2

    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p0, Lorg/apache/fontbox/ttf/GlyfCompositeDescript;->glyphTable:Lorg/apache/fontbox/ttf/GlyphTable;

    invoke-virtual {v1, p1}, Lorg/apache/fontbox/ttf/GlyphTable;->getGlyph(I)Lorg/apache/fontbox/ttf/GlyphData;

    move-result-object p1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lorg/apache/fontbox/ttf/GlyphData;->getDescription()Lorg/apache/fontbox/ttf/GlyphDescription;

    move-result-object p1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-object p1

    :cond_0
    return-object v0

    :catch_0
    move-exception p1

    const-string v1, "PdfBoxAndroid"

    invoke-virtual {p1}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object p1

    invoke-static {v1, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-object v0
.end method


# virtual methods
.method public getComponentCount()I
    .locals 1

    iget-object v0, p0, Lorg/apache/fontbox/ttf/GlyfCompositeDescript;->components:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getContourCount()I
    .locals 2

    iget-boolean v0, p0, Lorg/apache/fontbox/ttf/GlyfCompositeDescript;->resolved:Z

    if-nez v0, :cond_0

    const-string v0, "PdfBoxAndroid"

    const-string v1, "getContourCount called on unresolved GlyfCompositeDescript"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lorg/apache/fontbox/ttf/GlyfCompositeDescript;->components:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/fontbox/ttf/GlyfCompositeComp;

    invoke-virtual {v0}, Lorg/apache/fontbox/ttf/GlyfCompositeComp;->getFirstContour()I

    move-result v1

    invoke-virtual {v0}, Lorg/apache/fontbox/ttf/GlyfCompositeComp;->getGlyphIndex()I

    move-result v0

    invoke-direct {p0, v0}, Lorg/apache/fontbox/ttf/GlyfCompositeDescript;->getGlypDescription(I)Lorg/apache/fontbox/ttf/GlyphDescription;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/fontbox/ttf/GlyphDescription;->getContourCount()I

    move-result v0

    add-int/2addr v1, v0

    return v1
.end method

.method public getEndPtOfContours(I)I
    .locals 3

    invoke-direct {p0, p1}, Lorg/apache/fontbox/ttf/GlyfCompositeDescript;->getCompositeCompEndPt(I)Lorg/apache/fontbox/ttf/GlyfCompositeComp;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lorg/apache/fontbox/ttf/GlyfCompositeComp;->getGlyphIndex()I

    move-result v1

    invoke-direct {p0, v1}, Lorg/apache/fontbox/ttf/GlyfCompositeDescript;->getGlypDescription(I)Lorg/apache/fontbox/ttf/GlyphDescription;

    move-result-object v1

    invoke-virtual {v0}, Lorg/apache/fontbox/ttf/GlyfCompositeComp;->getFirstContour()I

    move-result v2

    sub-int/2addr p1, v2

    invoke-interface {v1, p1}, Lorg/apache/fontbox/ttf/GlyphDescription;->getEndPtOfContours(I)I

    move-result p1

    invoke-virtual {v0}, Lorg/apache/fontbox/ttf/GlyfCompositeComp;->getFirstIndex()I

    move-result v0

    add-int/2addr p1, v0

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method public getFlags(I)B
    .locals 2

    invoke-direct {p0, p1}, Lorg/apache/fontbox/ttf/GlyfCompositeDescript;->getCompositeComp(I)Lorg/apache/fontbox/ttf/GlyfCompositeComp;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lorg/apache/fontbox/ttf/GlyfCompositeComp;->getGlyphIndex()I

    move-result v1

    invoke-direct {p0, v1}, Lorg/apache/fontbox/ttf/GlyfCompositeDescript;->getGlypDescription(I)Lorg/apache/fontbox/ttf/GlyphDescription;

    move-result-object v1

    invoke-virtual {v0}, Lorg/apache/fontbox/ttf/GlyfCompositeComp;->getFirstIndex()I

    move-result v0

    sub-int/2addr p1, v0

    invoke-interface {v1, p1}, Lorg/apache/fontbox/ttf/GlyphDescription;->getFlags(I)B

    move-result p1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method public getPointCount()I
    .locals 4

    iget-boolean v0, p0, Lorg/apache/fontbox/ttf/GlyfCompositeDescript;->resolved:Z

    const-string v1, "PdfBoxAndroid"

    if-nez v0, :cond_0

    const-string v0, "getPointCount called on unresolved GlyfCompositeDescript"

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lorg/apache/fontbox/ttf/GlyfCompositeDescript;->components:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/fontbox/ttf/GlyfCompositeComp;

    invoke-virtual {v0}, Lorg/apache/fontbox/ttf/GlyfCompositeComp;->getGlyphIndex()I

    move-result v2

    invoke-direct {p0, v2}, Lorg/apache/fontbox/ttf/GlyfCompositeDescript;->getGlypDescription(I)Lorg/apache/fontbox/ttf/GlyphDescription;

    move-result-object v2

    if-nez v2, :cond_1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getGlypDescription("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Lorg/apache/fontbox/ttf/GlyfCompositeComp;->getGlyphIndex()I

    move-result v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v0, ") is null, returning 0"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    return v0

    :cond_1
    invoke-virtual {v0}, Lorg/apache/fontbox/ttf/GlyfCompositeComp;->getFirstIndex()I

    move-result v0

    invoke-interface {v2}, Lorg/apache/fontbox/ttf/GlyphDescription;->getPointCount()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public getXCoordinate(I)S
    .locals 3

    invoke-direct {p0, p1}, Lorg/apache/fontbox/ttf/GlyfCompositeDescript;->getCompositeComp(I)Lorg/apache/fontbox/ttf/GlyfCompositeComp;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lorg/apache/fontbox/ttf/GlyfCompositeComp;->getGlyphIndex()I

    move-result v1

    invoke-direct {p0, v1}, Lorg/apache/fontbox/ttf/GlyfCompositeDescript;->getGlypDescription(I)Lorg/apache/fontbox/ttf/GlyphDescription;

    move-result-object v1

    invoke-virtual {v0}, Lorg/apache/fontbox/ttf/GlyfCompositeComp;->getFirstIndex()I

    move-result v2

    sub-int/2addr p1, v2

    invoke-interface {v1, p1}, Lorg/apache/fontbox/ttf/GlyphDescription;->getXCoordinate(I)S

    move-result v2

    invoke-interface {v1, p1}, Lorg/apache/fontbox/ttf/GlyphDescription;->getYCoordinate(I)S

    move-result p1

    invoke-virtual {v0, v2, p1}, Lorg/apache/fontbox/ttf/GlyfCompositeComp;->scaleX(II)I

    move-result p1

    int-to-short p1, p1

    invoke-virtual {v0}, Lorg/apache/fontbox/ttf/GlyfCompositeComp;->getXTranslate()I

    move-result v0

    add-int/2addr p1, v0

    int-to-short p1, p1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method public getYCoordinate(I)S
    .locals 3

    invoke-direct {p0, p1}, Lorg/apache/fontbox/ttf/GlyfCompositeDescript;->getCompositeComp(I)Lorg/apache/fontbox/ttf/GlyfCompositeComp;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lorg/apache/fontbox/ttf/GlyfCompositeComp;->getGlyphIndex()I

    move-result v1

    invoke-direct {p0, v1}, Lorg/apache/fontbox/ttf/GlyfCompositeDescript;->getGlypDescription(I)Lorg/apache/fontbox/ttf/GlyphDescription;

    move-result-object v1

    invoke-virtual {v0}, Lorg/apache/fontbox/ttf/GlyfCompositeComp;->getFirstIndex()I

    move-result v2

    sub-int/2addr p1, v2

    invoke-interface {v1, p1}, Lorg/apache/fontbox/ttf/GlyphDescription;->getXCoordinate(I)S

    move-result v2

    invoke-interface {v1, p1}, Lorg/apache/fontbox/ttf/GlyphDescription;->getYCoordinate(I)S

    move-result p1

    invoke-virtual {v0, v2, p1}, Lorg/apache/fontbox/ttf/GlyfCompositeComp;->scaleY(II)I

    move-result p1

    int-to-short p1, p1

    invoke-virtual {v0}, Lorg/apache/fontbox/ttf/GlyfCompositeComp;->getYTranslate()I

    move-result v0

    add-int/2addr p1, v0

    int-to-short p1, p1

    return p1

    :cond_0
    const/4 p1, 0x0

    return p1
.end method

.method public isComposite()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public resolve()V
    .locals 7

    iget-boolean v0, p0, Lorg/apache/fontbox/ttf/GlyfCompositeDescript;->resolved:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    iget-boolean v0, p0, Lorg/apache/fontbox/ttf/GlyfCompositeDescript;->beingResolved:Z

    if-eqz v0, :cond_1

    const-string v0, "PdfBoxAndroid"

    const-string v1, "Circular reference in GlyfCompositeDesc"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/fontbox/ttf/GlyfCompositeDescript;->beingResolved:Z

    iget-object v1, p0, Lorg/apache/fontbox/ttf/GlyfCompositeDescript;->components:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    const/4 v2, 0x0

    move v3, v2

    move v4, v3

    :cond_2
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lorg/apache/fontbox/ttf/GlyfCompositeComp;

    invoke-virtual {v5, v3}, Lorg/apache/fontbox/ttf/GlyfCompositeComp;->setFirstIndex(I)V

    invoke-virtual {v5, v4}, Lorg/apache/fontbox/ttf/GlyfCompositeComp;->setFirstContour(I)V

    invoke-virtual {v5}, Lorg/apache/fontbox/ttf/GlyfCompositeComp;->getGlyphIndex()I

    move-result v5

    invoke-direct {p0, v5}, Lorg/apache/fontbox/ttf/GlyfCompositeDescript;->getGlypDescription(I)Lorg/apache/fontbox/ttf/GlyphDescription;

    move-result-object v5

    if-eqz v5, :cond_2

    invoke-interface {v5}, Lorg/apache/fontbox/ttf/GlyphDescription;->resolve()V

    invoke-interface {v5}, Lorg/apache/fontbox/ttf/GlyphDescription;->getPointCount()I

    move-result v6

    add-int/2addr v3, v6

    invoke-interface {v5}, Lorg/apache/fontbox/ttf/GlyphDescription;->getContourCount()I

    move-result v5

    add-int/2addr v4, v5

    goto :goto_0

    :cond_3
    iput-boolean v0, p0, Lorg/apache/fontbox/ttf/GlyfCompositeDescript;->resolved:Z

    iput-boolean v2, p0, Lorg/apache/fontbox/ttf/GlyfCompositeDescript;->beingResolved:Z

    return-void
.end method

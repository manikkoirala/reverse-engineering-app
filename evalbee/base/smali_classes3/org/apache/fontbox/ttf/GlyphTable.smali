.class public Lorg/apache/fontbox/ttf/GlyphTable;
.super Lorg/apache/fontbox/ttf/TTFTable;
.source "SourceFile"


# static fields
.field public static final TAG:Ljava/lang/String; = "glyf"


# instance fields
.field protected cache:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Lorg/apache/fontbox/ttf/GlyphData;",
            ">;"
        }
    .end annotation
.end field

.field private data:Lorg/apache/fontbox/ttf/TTFDataStream;

.field private glyphs:[Lorg/apache/fontbox/ttf/GlyphData;

.field private loca:Lorg/apache/fontbox/ttf/IndexToLocationTable;

.field private numGlyphs:I


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lorg/apache/fontbox/ttf/TTFTable;-><init>()V

    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/fontbox/ttf/GlyphTable;->cache:Ljava/util/Map;

    return-void
.end method

.method private readAll()V
    .locals 12

    iget-object v0, p0, Lorg/apache/fontbox/ttf/GlyphTable;->loca:Lorg/apache/fontbox/ttf/IndexToLocationTable;

    invoke-virtual {v0}, Lorg/apache/fontbox/ttf/IndexToLocationTable;->getOffsets()[J

    move-result-object v0

    iget v1, p0, Lorg/apache/fontbox/ttf/GlyphTable;->numGlyphs:I

    aget-wide v1, v0, v1

    invoke-virtual {p0}, Lorg/apache/fontbox/ttf/TTFTable;->getOffset()J

    move-result-wide v3

    iget v5, p0, Lorg/apache/fontbox/ttf/GlyphTable;->numGlyphs:I

    new-array v5, v5, [Lorg/apache/fontbox/ttf/GlyphData;

    iput-object v5, p0, Lorg/apache/fontbox/ttf/GlyphTable;->glyphs:[Lorg/apache/fontbox/ttf/GlyphData;

    const/4 v5, 0x0

    move v6, v5

    :goto_0
    iget v7, p0, Lorg/apache/fontbox/ttf/GlyphTable;->numGlyphs:I

    if-ge v6, v7, :cond_2

    const-wide/16 v7, 0x0

    cmp-long v7, v1, v7

    if-eqz v7, :cond_0

    aget-wide v7, v0, v6

    cmp-long v7, v1, v7

    if-nez v7, :cond_0

    goto :goto_2

    :cond_0
    add-int/lit8 v7, v6, 0x1

    aget-wide v8, v0, v7

    aget-wide v10, v0, v6

    cmp-long v8, v8, v10

    if-gtz v8, :cond_1

    goto :goto_1

    :cond_1
    iget-object v8, p0, Lorg/apache/fontbox/ttf/GlyphTable;->glyphs:[Lorg/apache/fontbox/ttf/GlyphData;

    new-instance v9, Lorg/apache/fontbox/ttf/GlyphData;

    invoke-direct {v9}, Lorg/apache/fontbox/ttf/GlyphData;-><init>()V

    aput-object v9, v8, v6

    iget-object v8, p0, Lorg/apache/fontbox/ttf/GlyphTable;->data:Lorg/apache/fontbox/ttf/TTFDataStream;

    aget-wide v9, v0, v6

    add-long/2addr v9, v3

    invoke-virtual {v8, v9, v10}, Lorg/apache/fontbox/ttf/TTFDataStream;->seek(J)V

    iget-object v8, p0, Lorg/apache/fontbox/ttf/GlyphTable;->glyphs:[Lorg/apache/fontbox/ttf/GlyphData;

    aget-object v6, v8, v6

    iget-object v8, p0, Lorg/apache/fontbox/ttf/GlyphTable;->data:Lorg/apache/fontbox/ttf/TTFDataStream;

    invoke-virtual {v6, p0, v8}, Lorg/apache/fontbox/ttf/GlyphData;->initData(Lorg/apache/fontbox/ttf/GlyphTable;Lorg/apache/fontbox/ttf/TTFDataStream;)V

    :goto_1
    move v6, v7

    goto :goto_0

    :cond_2
    :goto_2
    iget v0, p0, Lorg/apache/fontbox/ttf/GlyphTable;->numGlyphs:I

    if-ge v5, v0, :cond_4

    iget-object v0, p0, Lorg/apache/fontbox/ttf/GlyphTable;->glyphs:[Lorg/apache/fontbox/ttf/GlyphData;

    aget-object v0, v0, v5

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lorg/apache/fontbox/ttf/GlyphData;->getDescription()Lorg/apache/fontbox/ttf/GlyphDescription;

    move-result-object v1

    invoke-interface {v1}, Lorg/apache/fontbox/ttf/GlyphDescription;->isComposite()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {v0}, Lorg/apache/fontbox/ttf/GlyphData;->getDescription()Lorg/apache/fontbox/ttf/GlyphDescription;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/fontbox/ttf/GlyphDescription;->resolve()V

    :cond_3
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    :cond_4
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/fontbox/ttf/TTFTable;->initialized:Z

    return-void
.end method


# virtual methods
.method public getGlyph(I)Lorg/apache/fontbox/ttf/GlyphData;
    .locals 8

    const/4 v0, 0x0

    if-ltz p1, :cond_4

    iget v1, p0, Lorg/apache/fontbox/ttf/GlyphTable;->numGlyphs:I

    if-lt p1, v1, :cond_0

    goto :goto_1

    :cond_0
    iget-object v1, p0, Lorg/apache/fontbox/ttf/GlyphTable;->cache:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v0, p0, Lorg/apache/fontbox/ttf/GlyphTable;->cache:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lorg/apache/fontbox/ttf/GlyphData;

    return-object p1

    :cond_1
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lorg/apache/fontbox/ttf/GlyphTable;->data:Lorg/apache/fontbox/ttf/TTFDataStream;

    invoke-virtual {v1}, Lorg/apache/fontbox/ttf/TTFDataStream;->getCurrentPosition()J

    move-result-wide v1

    iget-object v3, p0, Lorg/apache/fontbox/ttf/GlyphTable;->loca:Lorg/apache/fontbox/ttf/IndexToLocationTable;

    invoke-virtual {v3}, Lorg/apache/fontbox/ttf/IndexToLocationTable;->getOffsets()[J

    move-result-object v3

    aget-wide v4, v3, p1

    add-int/lit8 v6, p1, 0x1

    aget-wide v6, v3, v6

    cmp-long v4, v4, v6

    if-nez v4, :cond_2

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lorg/apache/fontbox/ttf/GlyphTable;->data:Lorg/apache/fontbox/ttf/TTFDataStream;

    invoke-virtual {p0}, Lorg/apache/fontbox/ttf/TTFTable;->getOffset()J

    move-result-wide v4

    aget-wide v6, v3, p1

    add-long/2addr v4, v6

    invoke-virtual {v0, v4, v5}, Lorg/apache/fontbox/ttf/TTFDataStream;->seek(J)V

    new-instance v0, Lorg/apache/fontbox/ttf/GlyphData;

    invoke-direct {v0}, Lorg/apache/fontbox/ttf/GlyphData;-><init>()V

    iget-object p1, p0, Lorg/apache/fontbox/ttf/GlyphTable;->data:Lorg/apache/fontbox/ttf/TTFDataStream;

    invoke-virtual {v0, p0, p1}, Lorg/apache/fontbox/ttf/GlyphData;->initData(Lorg/apache/fontbox/ttf/GlyphTable;Lorg/apache/fontbox/ttf/TTFDataStream;)V

    invoke-virtual {v0}, Lorg/apache/fontbox/ttf/GlyphData;->getDescription()Lorg/apache/fontbox/ttf/GlyphDescription;

    move-result-object p1

    invoke-interface {p1}, Lorg/apache/fontbox/ttf/GlyphDescription;->isComposite()Z

    move-result p1

    if-eqz p1, :cond_3

    invoke-virtual {v0}, Lorg/apache/fontbox/ttf/GlyphData;->getDescription()Lorg/apache/fontbox/ttf/GlyphDescription;

    move-result-object p1

    invoke-interface {p1}, Lorg/apache/fontbox/ttf/GlyphDescription;->resolve()V

    :cond_3
    :goto_0
    iget-object p1, p0, Lorg/apache/fontbox/ttf/GlyphTable;->data:Lorg/apache/fontbox/ttf/TTFDataStream;

    invoke-virtual {p1, v1, v2}, Lorg/apache/fontbox/ttf/TTFDataStream;->seek(J)V

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception p1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw p1

    :cond_4
    :goto_1
    return-object v0
.end method

.method public declared-synchronized getGlyphs()[Lorg/apache/fontbox/ttf/GlyphData;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/fontbox/ttf/GlyphTable;->glyphs:[Lorg/apache/fontbox/ttf/GlyphData;

    if-nez v0, :cond_0

    invoke-direct {p0}, Lorg/apache/fontbox/ttf/GlyphTable;->readAll()V

    :cond_0
    iget-object v0, p0, Lorg/apache/fontbox/ttf/GlyphTable;->glyphs:[Lorg/apache/fontbox/ttf/GlyphData;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public read(Lorg/apache/fontbox/ttf/TrueTypeFont;Lorg/apache/fontbox/ttf/TTFDataStream;)V
    .locals 1

    invoke-virtual {p1}, Lorg/apache/fontbox/ttf/TrueTypeFont;->getIndexToLocation()Lorg/apache/fontbox/ttf/IndexToLocationTable;

    move-result-object v0

    iput-object v0, p0, Lorg/apache/fontbox/ttf/GlyphTable;->loca:Lorg/apache/fontbox/ttf/IndexToLocationTable;

    invoke-virtual {p1}, Lorg/apache/fontbox/ttf/TrueTypeFont;->getNumberOfGlyphs()I

    move-result p1

    iput p1, p0, Lorg/apache/fontbox/ttf/GlyphTable;->numGlyphs:I

    iput-object p2, p0, Lorg/apache/fontbox/ttf/GlyphTable;->data:Lorg/apache/fontbox/ttf/TTFDataStream;

    const/4 p1, 0x1

    iput-boolean p1, p0, Lorg/apache/fontbox/ttf/TTFTable;->initialized:Z

    return-void
.end method

.method public setGlyphs([Lorg/apache/fontbox/ttf/GlyphData;)V
    .locals 0

    iput-object p1, p0, Lorg/apache/fontbox/ttf/GlyphTable;->glyphs:[Lorg/apache/fontbox/ttf/GlyphData;

    return-void
.end method

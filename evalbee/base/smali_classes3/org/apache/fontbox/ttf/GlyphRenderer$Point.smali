.class Lorg/apache/fontbox/ttf/GlyphRenderer$Point;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lorg/apache/fontbox/ttf/GlyphRenderer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Point"
.end annotation


# instance fields
.field private endOfContour:Z

.field private onCurve:Z

.field private x:I

.field private y:I


# direct methods
.method public constructor <init>(II)V
    .locals 2

    .line 1
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2, v0, v1}, Lorg/apache/fontbox/ttf/GlyphRenderer$Point;-><init>(IIZZ)V

    return-void
.end method

.method public constructor <init>(IIZZ)V
    .locals 0

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lorg/apache/fontbox/ttf/GlyphRenderer$Point;->x:I

    iput p2, p0, Lorg/apache/fontbox/ttf/GlyphRenderer$Point;->y:I

    iput-boolean p3, p0, Lorg/apache/fontbox/ttf/GlyphRenderer$Point;->onCurve:Z

    iput-boolean p4, p0, Lorg/apache/fontbox/ttf/GlyphRenderer$Point;->endOfContour:Z

    return-void
.end method

.method public static synthetic access$000(Lorg/apache/fontbox/ttf/GlyphRenderer$Point;)Z
    .locals 0

    iget-boolean p0, p0, Lorg/apache/fontbox/ttf/GlyphRenderer$Point;->endOfContour:Z

    return p0
.end method

.method public static synthetic access$100(Lorg/apache/fontbox/ttf/GlyphRenderer$Point;)Z
    .locals 0

    iget-boolean p0, p0, Lorg/apache/fontbox/ttf/GlyphRenderer$Point;->onCurve:Z

    return p0
.end method

.method public static synthetic access$200(Lorg/apache/fontbox/ttf/GlyphRenderer$Point;)I
    .locals 0

    iget p0, p0, Lorg/apache/fontbox/ttf/GlyphRenderer$Point;->x:I

    return p0
.end method

.method public static synthetic access$300(Lorg/apache/fontbox/ttf/GlyphRenderer$Point;)I
    .locals 0

    iget p0, p0, Lorg/apache/fontbox/ttf/GlyphRenderer$Point;->y:I

    return p0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 4

    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    iget v1, p0, Lorg/apache/fontbox/ttf/GlyphRenderer$Point;->x:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x0

    aput-object v1, v0, v2

    iget v1, p0, Lorg/apache/fontbox/ttf/GlyphRenderer$Point;->y:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x1

    aput-object v1, v0, v2

    iget-boolean v1, p0, Lorg/apache/fontbox/ttf/GlyphRenderer$Point;->onCurve:Z

    const-string v2, ""

    if-eqz v1, :cond_0

    const-string v1, "onCurve"

    goto :goto_0

    :cond_0
    move-object v1, v2

    :goto_0
    const/4 v3, 0x2

    aput-object v1, v0, v3

    iget-boolean v1, p0, Lorg/apache/fontbox/ttf/GlyphRenderer$Point;->endOfContour:Z

    if-eqz v1, :cond_1

    const-string v2, "endOfContour"

    :cond_1
    const/4 v1, 0x3

    aput-object v2, v0, v1

    const-string v1, "Point(%d,%d,%s,%s)"

    invoke-static {v1, v0}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

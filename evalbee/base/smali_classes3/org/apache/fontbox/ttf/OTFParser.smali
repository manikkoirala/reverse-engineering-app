.class public Lorg/apache/fontbox/ttf/OTFParser;
.super Lorg/apache/fontbox/ttf/TTFParser;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lorg/apache/fontbox/ttf/TTFParser;-><init>()V

    return-void
.end method

.method public constructor <init>(Z)V
    .locals 1

    .line 1
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lorg/apache/fontbox/ttf/OTFParser;-><init>(ZZ)V

    return-void
.end method

.method public constructor <init>(ZZ)V
    .locals 0

    .line 2
    invoke-direct {p0, p1, p2}, Lorg/apache/fontbox/ttf/TTFParser;-><init>(ZZ)V

    return-void
.end method


# virtual methods
.method public newFont(Lorg/apache/fontbox/ttf/TTFDataStream;)Lorg/apache/fontbox/ttf/OpenTypeFont;
    .locals 1

    .line 1
    new-instance v0, Lorg/apache/fontbox/ttf/OpenTypeFont;

    invoke-direct {v0, p1}, Lorg/apache/fontbox/ttf/OpenTypeFont;-><init>(Lorg/apache/fontbox/ttf/TTFDataStream;)V

    return-object v0
.end method

.method public bridge synthetic newFont(Lorg/apache/fontbox/ttf/TTFDataStream;)Lorg/apache/fontbox/ttf/TrueTypeFont;
    .locals 0

    .line 2
    invoke-virtual {p0, p1}, Lorg/apache/fontbox/ttf/OTFParser;->newFont(Lorg/apache/fontbox/ttf/TTFDataStream;)Lorg/apache/fontbox/ttf/OpenTypeFont;

    move-result-object p1

    return-object p1
.end method

.method public parse(Ljava/io/File;)Lorg/apache/fontbox/ttf/OpenTypeFont;
    .locals 0

    .line 1
    invoke-super {p0, p1}, Lorg/apache/fontbox/ttf/TTFParser;->parse(Ljava/io/File;)Lorg/apache/fontbox/ttf/TrueTypeFont;

    move-result-object p1

    check-cast p1, Lorg/apache/fontbox/ttf/OpenTypeFont;

    return-object p1
.end method

.method public parse(Ljava/io/InputStream;)Lorg/apache/fontbox/ttf/OpenTypeFont;
    .locals 0

    .line 2
    invoke-super {p0, p1}, Lorg/apache/fontbox/ttf/TTFParser;->parse(Ljava/io/InputStream;)Lorg/apache/fontbox/ttf/TrueTypeFont;

    move-result-object p1

    check-cast p1, Lorg/apache/fontbox/ttf/OpenTypeFont;

    return-object p1
.end method

.method public parse(Ljava/lang/String;)Lorg/apache/fontbox/ttf/OpenTypeFont;
    .locals 0

    .line 3
    invoke-super {p0, p1}, Lorg/apache/fontbox/ttf/TTFParser;->parse(Ljava/lang/String;)Lorg/apache/fontbox/ttf/TrueTypeFont;

    move-result-object p1

    check-cast p1, Lorg/apache/fontbox/ttf/OpenTypeFont;

    return-object p1
.end method

.method public parse(Lorg/apache/fontbox/ttf/TTFDataStream;)Lorg/apache/fontbox/ttf/OpenTypeFont;
    .locals 0

    .line 4
    invoke-super {p0, p1}, Lorg/apache/fontbox/ttf/TTFParser;->parse(Lorg/apache/fontbox/ttf/TTFDataStream;)Lorg/apache/fontbox/ttf/TrueTypeFont;

    move-result-object p1

    check-cast p1, Lorg/apache/fontbox/ttf/OpenTypeFont;

    return-object p1
.end method

.method public bridge synthetic parse(Ljava/io/File;)Lorg/apache/fontbox/ttf/TrueTypeFont;
    .locals 0

    .line 5
    invoke-virtual {p0, p1}, Lorg/apache/fontbox/ttf/OTFParser;->parse(Ljava/io/File;)Lorg/apache/fontbox/ttf/OpenTypeFont;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic parse(Ljava/io/InputStream;)Lorg/apache/fontbox/ttf/TrueTypeFont;
    .locals 0

    .line 6
    invoke-virtual {p0, p1}, Lorg/apache/fontbox/ttf/OTFParser;->parse(Ljava/io/InputStream;)Lorg/apache/fontbox/ttf/OpenTypeFont;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic parse(Ljava/lang/String;)Lorg/apache/fontbox/ttf/TrueTypeFont;
    .locals 0

    .line 7
    invoke-virtual {p0, p1}, Lorg/apache/fontbox/ttf/OTFParser;->parse(Ljava/lang/String;)Lorg/apache/fontbox/ttf/OpenTypeFont;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic parse(Lorg/apache/fontbox/ttf/TTFDataStream;)Lorg/apache/fontbox/ttf/TrueTypeFont;
    .locals 0

    .line 8
    invoke-virtual {p0, p1}, Lorg/apache/fontbox/ttf/OTFParser;->parse(Lorg/apache/fontbox/ttf/TTFDataStream;)Lorg/apache/fontbox/ttf/OpenTypeFont;

    move-result-object p1

    return-object p1
.end method

.method public readTable(Ljava/lang/String;)Lorg/apache/fontbox/ttf/TTFTable;
    .locals 1

    const-string v0, "BASE"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "GDEF"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "GPOS"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "GSUB"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "JSTF"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const-string v0, "CFF "

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance p1, Lorg/apache/fontbox/ttf/CFFTable;

    invoke-direct {p1}, Lorg/apache/fontbox/ttf/CFFTable;-><init>()V

    return-object p1

    :cond_1
    invoke-super {p0, p1}, Lorg/apache/fontbox/ttf/TTFParser;->readTable(Ljava/lang/String;)Lorg/apache/fontbox/ttf/TTFTable;

    move-result-object p1

    return-object p1

    :cond_2
    :goto_0
    new-instance p1, Lorg/apache/fontbox/ttf/OTLTable;

    invoke-direct {p1}, Lorg/apache/fontbox/ttf/OTLTable;-><init>()V

    return-object p1
.end method

.class public final Lorg/apache/fontbox/ttf/TTFSubsetter;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final PAD_BUF:[B


# instance fields
.field private final glyphIds:Ljava/util/SortedSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/SortedSet<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private hasAddedCompoundReferences:Z

.field private final keepTables:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private prefix:Ljava/lang/String;

.field private final ttf:Lorg/apache/fontbox/ttf/TrueTypeFont;

.field private final uniToGID:Ljava/util/SortedMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/SortedMap<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final unicodeCmap:Lorg/apache/fontbox/ttf/CmapSubtable;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x3

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, Lorg/apache/fontbox/ttf/TTFSubsetter;->PAD_BUF:[B

    return-void

    nop

    :array_0
    .array-data 1
        0x0t
        0x0t
        0x0t
    .end array-data
.end method

.method public constructor <init>(Lorg/apache/fontbox/ttf/TrueTypeFont;)V
    .locals 1

    .line 1
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lorg/apache/fontbox/ttf/TTFSubsetter;-><init>(Lorg/apache/fontbox/ttf/TrueTypeFont;Ljava/util/List;)V

    return-void
.end method

.method public constructor <init>(Lorg/apache/fontbox/ttf/TrueTypeFont;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/fontbox/ttf/TrueTypeFont;",
            "Ljava/util/List<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .line 2
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lorg/apache/fontbox/ttf/TTFSubsetter;->ttf:Lorg/apache/fontbox/ttf/TrueTypeFont;

    iput-object p2, p0, Lorg/apache/fontbox/ttf/TTFSubsetter;->keepTables:Ljava/util/List;

    new-instance p2, Ljava/util/TreeMap;

    invoke-direct {p2}, Ljava/util/TreeMap;-><init>()V

    iput-object p2, p0, Lorg/apache/fontbox/ttf/TTFSubsetter;->uniToGID:Ljava/util/SortedMap;

    new-instance p2, Ljava/util/TreeSet;

    invoke-direct {p2}, Ljava/util/TreeSet;-><init>()V

    iput-object p2, p0, Lorg/apache/fontbox/ttf/TTFSubsetter;->glyphIds:Ljava/util/SortedSet;

    invoke-virtual {p1}, Lorg/apache/fontbox/ttf/TrueTypeFont;->getUnicodeCmap()Lorg/apache/fontbox/ttf/CmapSubtable;

    move-result-object p1

    iput-object p1, p0, Lorg/apache/fontbox/ttf/TTFSubsetter;->unicodeCmap:Lorg/apache/fontbox/ttf/CmapSubtable;

    const/4 p1, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {p2, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method private addCompoundReferences()V
    .locals 13

    iget-boolean v0, p0, Lorg/apache/fontbox/ttf/TTFSubsetter;->hasAddedCompoundReferences:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/fontbox/ttf/TTFSubsetter;->hasAddedCompoundReferences:Z

    :cond_1
    iget-object v1, p0, Lorg/apache/fontbox/ttf/TTFSubsetter;->ttf:Lorg/apache/fontbox/ttf/TrueTypeFont;

    invoke-virtual {v1}, Lorg/apache/fontbox/ttf/TrueTypeFont;->getGlyph()Lorg/apache/fontbox/ttf/GlyphTable;

    move-result-object v1

    iget-object v2, p0, Lorg/apache/fontbox/ttf/TTFSubsetter;->ttf:Lorg/apache/fontbox/ttf/TrueTypeFont;

    invoke-virtual {v2}, Lorg/apache/fontbox/ttf/TrueTypeFont;->getIndexToLocation()Lorg/apache/fontbox/ttf/IndexToLocationTable;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/fontbox/ttf/IndexToLocationTable;->getOffsets()[J

    move-result-object v2

    iget-object v3, p0, Lorg/apache/fontbox/ttf/TTFSubsetter;->ttf:Lorg/apache/fontbox/ttf/TrueTypeFont;

    invoke-virtual {v3}, Lorg/apache/fontbox/ttf/TrueTypeFont;->getOriginalData()Ljava/io/InputStream;

    move-result-object v3

    :try_start_0
    invoke-virtual {v1}, Lorg/apache/fontbox/ttf/TTFTable;->getOffset()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/io/InputStream;->skip(J)J

    iget-object v1, p0, Lorg/apache/fontbox/ttf/TTFSubsetter;->glyphIds:Ljava/util/SortedSet;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    const/4 v4, 0x0

    const-wide/16 v5, 0x0

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    const/4 v8, 0x0

    if-eqz v7, :cond_a

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v9

    aget-wide v9, v2, v9

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v11

    add-int/2addr v11, v0

    aget-wide v11, v2, v11

    sub-long/2addr v11, v9

    sub-long/2addr v9, v5

    invoke-virtual {v3, v9, v10}, Ljava/io/InputStream;->skip(J)J

    long-to-int v5, v11

    new-array v6, v5, [B

    invoke-virtual {v3, v6}, Ljava/io/InputStream;->read([B)I

    const/4 v9, 0x2

    if-lt v5, v9, :cond_9

    aget-byte v5, v6, v8

    const/4 v8, -0x1

    if-ne v5, v8, :cond_9

    aget-byte v5, v6, v0

    if-ne v5, v8, :cond_9

    const/16 v5, 0xa

    :cond_2
    aget-byte v8, v6, v5

    and-int/lit16 v8, v8, 0xff

    shl-int/lit8 v8, v8, 0x8

    add-int/lit8 v9, v5, 0x1

    aget-byte v9, v6, v9

    and-int/lit16 v9, v9, 0xff

    or-int/2addr v8, v9

    add-int/lit8 v5, v5, 0x2

    aget-byte v9, v6, v5

    and-int/lit16 v9, v9, 0xff

    shl-int/lit8 v9, v9, 0x8

    add-int/lit8 v10, v5, 0x1

    aget-byte v10, v6, v10

    and-int/lit16 v10, v10, 0xff

    or-int/2addr v9, v10

    iget-object v10, p0, Lorg/apache/fontbox/ttf/TTFSubsetter;->glyphIds:Ljava/util/SortedSet;

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-interface {v10, v11}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_4

    if-nez v4, :cond_3

    new-instance v4, Ljava/util/TreeSet;

    invoke-direct {v4}, Ljava/util/TreeSet;-><init>()V

    :cond_3
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-interface {v4, v9}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_4
    add-int/lit8 v5, v5, 0x2

    and-int/lit8 v9, v8, 0x1

    if-eqz v9, :cond_5

    add-int/lit8 v5, v5, 0x4

    goto :goto_1

    :cond_5
    add-int/lit8 v5, v5, 0x2

    :goto_1
    and-int/lit16 v9, v8, 0x80

    if-eqz v9, :cond_6

    add-int/lit8 v5, v5, 0x8

    goto :goto_2

    :cond_6
    and-int/lit8 v9, v8, 0x40

    if-eqz v9, :cond_7

    add-int/lit8 v5, v5, 0x4

    goto :goto_2

    :cond_7
    and-int/lit8 v9, v8, 0x8

    if-eqz v9, :cond_8

    add-int/lit8 v5, v5, 0x2

    :cond_8
    :goto_2
    and-int/lit8 v8, v8, 0x20

    if-nez v8, :cond_2

    :cond_9
    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v5

    add-int/2addr v5, v0

    aget-wide v5, v2, v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/16 :goto_0

    :cond_a
    invoke-virtual {v3}, Ljava/io/InputStream;->close()V

    if-eqz v4, :cond_b

    iget-object v1, p0, Lorg/apache/fontbox/ttf/TTFSubsetter;->glyphIds:Ljava/util/SortedSet;

    invoke-interface {v1, v4}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    :cond_b
    if-eqz v4, :cond_c

    move v8, v0

    :cond_c
    if-nez v8, :cond_1

    return-void

    :catchall_0
    move-exception v0

    invoke-virtual {v3}, Ljava/io/InputStream;->close()V

    throw v0
.end method

.method private buildCmapTable()[B
    .locals 17

    move-object/from16 v0, p0

    iget-object v1, v0, Lorg/apache/fontbox/ttf/TTFSubsetter;->ttf:Lorg/apache/fontbox/ttf/TrueTypeFont;

    invoke-virtual {v1}, Lorg/apache/fontbox/ttf/TrueTypeFont;->getCmap()Lorg/apache/fontbox/ttf/CmapTable;

    move-result-object v1

    if-eqz v1, :cond_b

    iget-object v1, v0, Lorg/apache/fontbox/ttf/TTFSubsetter;->keepTables:Ljava/util/List;

    if-eqz v1, :cond_0

    const-string v2, "cmap"

    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    goto/16 :goto_7

    :cond_0
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    new-instance v2, Ljava/io/DataOutputStream;

    invoke-direct {v2, v1}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lorg/apache/fontbox/ttf/TTFSubsetter;->writeUint16(Ljava/io/DataOutputStream;I)V

    const/4 v4, 0x1

    invoke-static {v2, v4}, Lorg/apache/fontbox/ttf/TTFSubsetter;->writeUint16(Ljava/io/DataOutputStream;I)V

    const/4 v5, 0x3

    invoke-static {v2, v5}, Lorg/apache/fontbox/ttf/TTFSubsetter;->writeUint16(Ljava/io/DataOutputStream;I)V

    invoke-static {v2, v4}, Lorg/apache/fontbox/ttf/TTFSubsetter;->writeUint16(Ljava/io/DataOutputStream;I)V

    const-wide/16 v5, 0xc

    invoke-static {v2, v5, v6}, Lorg/apache/fontbox/ttf/TTFSubsetter;->writeUint32(Ljava/io/DataOutputStream;J)V

    iget-object v5, v0, Lorg/apache/fontbox/ttf/TTFSubsetter;->uniToGID:Ljava/util/SortedMap;

    invoke-interface {v5}, Ljava/util/SortedMap;->entrySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/Map$Entry;

    invoke-interface {v6}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-direct {v0, v7}, Lorg/apache/fontbox/ttf/TTFSubsetter;->getNewGlyphId(Ljava/lang/Integer;)I

    move-result v7

    iget-object v8, v0, Lorg/apache/fontbox/ttf/TTFSubsetter;->uniToGID:Ljava/util/SortedMap;

    invoke-interface {v8}, Ljava/util/Map;->size()I

    move-result v8

    new-array v8, v8, [I

    iget-object v9, v0, Lorg/apache/fontbox/ttf/TTFSubsetter;->uniToGID:Ljava/util/SortedMap;

    invoke-interface {v9}, Ljava/util/Map;->size()I

    move-result v9

    new-array v9, v9, [I

    iget-object v10, v0, Lorg/apache/fontbox/ttf/TTFSubsetter;->uniToGID:Ljava/util/SortedMap;

    invoke-interface {v10}, Ljava/util/Map;->size()I

    move-result v10

    new-array v10, v10, [I

    move v12, v3

    move v11, v7

    move-object v7, v6

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    const v14, 0xffff

    if-eqz v13, :cond_6

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/util/Map$Entry;

    invoke-interface {v13}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/lang/Integer;

    invoke-direct {v0, v15}, Lorg/apache/fontbox/ttf/TTFSubsetter;->getNewGlyphId(Ljava/lang/Integer;)I

    move-result v15

    invoke-interface {v13}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Ljava/lang/Integer;

    invoke-virtual/range {v16 .. v16}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-gt v3, v14, :cond_5

    invoke-interface {v13}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-interface {v7}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/lang/Integer;

    invoke-virtual {v14}, Ljava/lang/Integer;->intValue()I

    move-result v14

    add-int/2addr v14, v4

    if-ne v3, v14, :cond_1

    sub-int v3, v15, v11

    invoke-interface {v13}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v14

    check-cast v14, Ljava/lang/Integer;

    invoke-virtual {v14}, Ljava/lang/Integer;->intValue()I

    move-result v14

    invoke-interface {v6}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Ljava/lang/Integer;

    invoke-virtual/range {v16 .. v16}, Ljava/lang/Integer;->intValue()I

    move-result v16

    sub-int v14, v14, v16

    if-eq v3, v14, :cond_4

    :cond_1
    invoke-interface {v6}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    if-eqz v11, :cond_2

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    aput v3, v8, v12

    invoke-interface {v7}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    aput v3, v9, v12

    invoke-interface {v6}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    sub-int/2addr v11, v3

    aput v11, v10, v12

    :goto_1
    add-int/lit8 v12, v12, 0x1

    goto :goto_2

    :cond_2
    invoke-interface {v7}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v14

    invoke-virtual {v3, v14}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    invoke-interface {v6}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    add-int/2addr v3, v4

    aput v3, v8, v12

    invoke-interface {v7}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    aput v3, v9, v12

    invoke-interface {v6}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    sub-int/2addr v11, v3

    aput v11, v10, v12

    goto :goto_1

    :cond_3
    :goto_2
    move-object v6, v13

    move v11, v15

    :cond_4
    move-object v7, v13

    const/4 v3, 0x0

    goto/16 :goto_0

    :cond_5
    new-instance v1, Ljava/lang/UnsupportedOperationException;

    const-string v2, "non-BMP Unicode character"

    invoke-direct {v1, v2}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_6
    invoke-interface {v6}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    aput v3, v8, v12

    invoke-interface {v7}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    aput v3, v9, v12

    invoke-interface {v6}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    sub-int/2addr v11, v3

    aput v11, v10, v12

    add-int/2addr v12, v4

    aput v14, v8, v12

    aput v14, v9, v12

    aput v4, v10, v12

    add-int/2addr v12, v4

    invoke-static {v12}, Lorg/apache/fontbox/ttf/TTFSubsetter;->log2(I)I

    move-result v3

    int-to-double v3, v3

    invoke-static {v3, v4}, Ljava/lang/Math;->floor(D)D

    move-result-wide v3

    const-wide/high16 v5, 0x4000000000000000L    # 2.0

    invoke-static {v5, v6, v3, v4}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v3

    double-to-int v3, v3

    mul-int/lit8 v3, v3, 0x2

    const/4 v4, 0x4

    invoke-static {v2, v4}, Lorg/apache/fontbox/ttf/TTFSubsetter;->writeUint16(Ljava/io/DataOutputStream;I)V

    mul-int/lit8 v4, v12, 0x4

    mul-int/lit8 v4, v4, 0x2

    add-int/lit8 v4, v4, 0x10

    invoke-static {v2, v4}, Lorg/apache/fontbox/ttf/TTFSubsetter;->writeUint16(Ljava/io/DataOutputStream;I)V

    const/4 v4, 0x0

    invoke-static {v2, v4}, Lorg/apache/fontbox/ttf/TTFSubsetter;->writeUint16(Ljava/io/DataOutputStream;I)V

    mul-int/lit8 v4, v12, 0x2

    invoke-static {v2, v4}, Lorg/apache/fontbox/ttf/TTFSubsetter;->writeUint16(Ljava/io/DataOutputStream;I)V

    invoke-static {v2, v3}, Lorg/apache/fontbox/ttf/TTFSubsetter;->writeUint16(Ljava/io/DataOutputStream;I)V

    div-int/lit8 v5, v3, 0x2

    invoke-static {v5}, Lorg/apache/fontbox/ttf/TTFSubsetter;->log2(I)I

    move-result v5

    invoke-static {v2, v5}, Lorg/apache/fontbox/ttf/TTFSubsetter;->writeUint16(Ljava/io/DataOutputStream;I)V

    sub-int/2addr v4, v3

    invoke-static {v2, v4}, Lorg/apache/fontbox/ttf/TTFSubsetter;->writeUint16(Ljava/io/DataOutputStream;I)V

    const/4 v3, 0x0

    :goto_3
    if-ge v3, v12, :cond_7

    aget v4, v9, v3

    invoke-static {v2, v4}, Lorg/apache/fontbox/ttf/TTFSubsetter;->writeUint16(Ljava/io/DataOutputStream;I)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    :cond_7
    const/4 v3, 0x0

    invoke-static {v2, v3}, Lorg/apache/fontbox/ttf/TTFSubsetter;->writeUint16(Ljava/io/DataOutputStream;I)V

    const/4 v4, 0x0

    :goto_4
    if-ge v4, v12, :cond_8

    aget v3, v8, v4

    invoke-static {v2, v3}, Lorg/apache/fontbox/ttf/TTFSubsetter;->writeUint16(Ljava/io/DataOutputStream;I)V

    add-int/lit8 v4, v4, 0x1

    goto :goto_4

    :cond_8
    const/4 v4, 0x0

    :goto_5
    if-ge v4, v12, :cond_9

    aget v3, v10, v4

    invoke-static {v2, v3}, Lorg/apache/fontbox/ttf/TTFSubsetter;->writeUint16(Ljava/io/DataOutputStream;I)V

    add-int/lit8 v4, v4, 0x1

    goto :goto_5

    :cond_9
    const/4 v4, 0x0

    :goto_6
    if-ge v4, v12, :cond_a

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lorg/apache/fontbox/ttf/TTFSubsetter;->writeUint16(Ljava/io/DataOutputStream;I)V

    add-int/lit8 v4, v4, 0x1

    goto :goto_6

    :cond_a
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    return-object v1

    :cond_b
    :goto_7
    const/4 v1, 0x0

    return-object v1
.end method

.method private buildGlyfTable([J)[B
    .locals 20

    move-object/from16 v1, p0

    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    iget-object v2, v1, Lorg/apache/fontbox/ttf/TTFSubsetter;->ttf:Lorg/apache/fontbox/ttf/TrueTypeFont;

    invoke-virtual {v2}, Lorg/apache/fontbox/ttf/TrueTypeFont;->getGlyph()Lorg/apache/fontbox/ttf/GlyphTable;

    move-result-object v2

    iget-object v3, v1, Lorg/apache/fontbox/ttf/TTFSubsetter;->ttf:Lorg/apache/fontbox/ttf/TrueTypeFont;

    invoke-virtual {v3}, Lorg/apache/fontbox/ttf/TrueTypeFont;->getIndexToLocation()Lorg/apache/fontbox/ttf/IndexToLocationTable;

    move-result-object v3

    invoke-virtual {v3}, Lorg/apache/fontbox/ttf/IndexToLocationTable;->getOffsets()[J

    move-result-object v3

    iget-object v4, v1, Lorg/apache/fontbox/ttf/TTFSubsetter;->ttf:Lorg/apache/fontbox/ttf/TrueTypeFont;

    invoke-virtual {v4}, Lorg/apache/fontbox/ttf/TrueTypeFont;->getOriginalData()Ljava/io/InputStream;

    move-result-object v4

    :try_start_0
    invoke-virtual {v2}, Lorg/apache/fontbox/ttf/TTFTable;->getOffset()J

    move-result-wide v5

    invoke-virtual {v4, v5, v6}, Ljava/io/InputStream;->skip(J)J

    iget-object v2, v1, Lorg/apache/fontbox/ttf/TTFSubsetter;->glyphIds:Ljava/util/SortedSet;

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    const/4 v7, 0x0

    move v8, v7

    const-wide/16 v9, 0x0

    const-wide/16 v11, 0x0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v13

    if-eqz v13, :cond_a

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Ljava/lang/Integer;

    invoke-virtual {v13}, Ljava/lang/Integer;->intValue()I

    move-result v14

    aget-wide v14, v3, v14

    invoke-virtual {v13}, Ljava/lang/Integer;->intValue()I

    move-result v13

    const/16 v16, 0x1

    add-int/lit8 v13, v13, 0x1

    aget-wide v17, v3, v13

    sub-long v5, v17, v14

    add-int/lit8 v13, v8, 0x1

    aput-wide v9, p1, v8

    sub-long v11, v14, v11

    invoke-virtual {v4, v11, v12}, Ljava/io/InputStream;->skip(J)J

    long-to-int v8, v5

    new-array v11, v8, [B

    invoke-virtual {v4, v11}, Ljava/io/InputStream;->read([B)I

    const/4 v12, 0x2

    if-lt v8, v12, :cond_7

    aget-byte v12, v11, v7

    const/4 v7, -0x1

    if-ne v12, v7, :cond_7

    aget-byte v12, v11, v16

    if-ne v12, v7, :cond_7

    const/16 v7, 0xa

    :goto_1
    aget-byte v8, v11, v7

    and-int/lit16 v8, v8, 0xff

    shl-int/lit8 v8, v8, 0x8

    add-int/lit8 v12, v7, 0x1

    aget-byte v12, v11, v12

    and-int/lit16 v12, v12, 0xff

    or-int/2addr v8, v12

    add-int/lit8 v7, v7, 0x2

    aget-byte v12, v11, v7

    and-int/lit16 v12, v12, 0xff

    shl-int/lit8 v12, v12, 0x8

    add-int/lit8 v16, v7, 0x1

    move-object/from16 v18, v2

    aget-byte v2, v11, v16

    and-int/lit16 v2, v2, 0xff

    or-int/2addr v2, v12

    iget-object v12, v1, Lorg/apache/fontbox/ttf/TTFSubsetter;->glyphIds:Ljava/util/SortedSet;

    move-object/from16 v19, v3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v12, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, v1, Lorg/apache/fontbox/ttf/TTFSubsetter;->glyphIds:Ljava/util/SortedSet;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-interface {v3, v12}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_0
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-direct {v1, v2}, Lorg/apache/fontbox/ttf/TTFSubsetter;->getNewGlyphId(Ljava/lang/Integer;)I

    move-result v2

    ushr-int/lit8 v3, v2, 0x8

    int-to-byte v3, v3

    aput-byte v3, v11, v7

    int-to-byte v2, v2

    aput-byte v2, v11, v16

    add-int/lit8 v7, v7, 0x2

    and-int/lit8 v2, v8, 0x1

    if-eqz v2, :cond_1

    add-int/lit8 v7, v7, 0x4

    goto :goto_2

    :cond_1
    add-int/lit8 v7, v7, 0x2

    :goto_2
    and-int/lit16 v2, v8, 0x80

    if-eqz v2, :cond_2

    add-int/lit8 v7, v7, 0x8

    goto :goto_3

    :cond_2
    and-int/lit8 v2, v8, 0x40

    if-eqz v2, :cond_3

    add-int/lit8 v7, v7, 0x4

    goto :goto_3

    :cond_3
    and-int/lit8 v2, v8, 0x8

    if-eqz v2, :cond_4

    add-int/lit8 v7, v7, 0x2

    :cond_4
    :goto_3
    and-int/lit8 v2, v8, 0x20

    if-nez v2, :cond_6

    and-int/lit16 v2, v8, 0x100

    const/16 v3, 0x100

    if-ne v2, v3, :cond_5

    aget-byte v2, v11, v7

    and-int/lit16 v2, v2, 0xff

    shl-int/lit8 v2, v2, 0x8

    add-int/lit8 v3, v7, 0x1

    aget-byte v3, v11, v3

    and-int/lit16 v3, v3, 0xff

    or-int/2addr v2, v3

    add-int/lit8 v7, v7, 0x2

    add-int/2addr v7, v2

    :cond_5
    const/4 v2, 0x0

    invoke-virtual {v0, v11, v2, v7}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    int-to-long v2, v7

    goto :goto_4

    :cond_6
    move-object/from16 v2, v18

    move-object/from16 v3, v19

    goto/16 :goto_1

    :cond_7
    move-object/from16 v18, v2

    move-object/from16 v19, v3

    if-lez v8, :cond_8

    const/4 v2, 0x0

    invoke-virtual {v0, v11, v2, v8}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    int-to-long v2, v8

    :goto_4
    add-long/2addr v9, v2

    :cond_8
    const-wide/16 v2, 0x4

    rem-long v7, v9, v2

    const-wide/16 v11, 0x0

    cmp-long v7, v7, v11

    if-eqz v7, :cond_9

    rem-long v2, v9, v2

    long-to-int v2, v2

    rsub-int/lit8 v2, v2, 0x4

    sget-object v3, Lorg/apache/fontbox/ttf/TTFSubsetter;->PAD_BUF:[B

    const/4 v7, 0x0

    invoke-virtual {v0, v3, v7, v2}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    int-to-long v2, v2

    add-long/2addr v9, v2

    goto :goto_5

    :cond_9
    const/4 v7, 0x0

    :goto_5
    add-long v2, v14, v5

    move-wide v11, v2

    move v8, v13

    move-object/from16 v2, v18

    move-object/from16 v3, v19

    goto/16 :goto_0

    :cond_a
    aput-wide v9, p1, v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v4}, Ljava/io/InputStream;->close()V

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v4}, Ljava/io/InputStream;->close()V

    throw v0
.end method

.method private buildHeadTable()[B
    .locals 5

    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    new-instance v1, Ljava/io/DataOutputStream;

    invoke-direct {v1, v0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    iget-object v2, p0, Lorg/apache/fontbox/ttf/TTFSubsetter;->ttf:Lorg/apache/fontbox/ttf/TrueTypeFont;

    invoke-virtual {v2}, Lorg/apache/fontbox/ttf/TrueTypeFont;->getHeader()Lorg/apache/fontbox/ttf/HeaderTable;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/fontbox/ttf/HeaderTable;->getVersion()F

    move-result v3

    float-to-double v3, v3

    invoke-static {v1, v3, v4}, Lorg/apache/fontbox/ttf/TTFSubsetter;->writeFixed(Ljava/io/DataOutputStream;D)V

    invoke-virtual {v2}, Lorg/apache/fontbox/ttf/HeaderTable;->getFontRevision()F

    move-result v3

    float-to-double v3, v3

    invoke-static {v1, v3, v4}, Lorg/apache/fontbox/ttf/TTFSubsetter;->writeFixed(Ljava/io/DataOutputStream;D)V

    const-wide/16 v3, 0x0

    invoke-static {v1, v3, v4}, Lorg/apache/fontbox/ttf/TTFSubsetter;->writeUint32(Ljava/io/DataOutputStream;J)V

    invoke-virtual {v2}, Lorg/apache/fontbox/ttf/HeaderTable;->getMagicNumber()J

    move-result-wide v3

    invoke-static {v1, v3, v4}, Lorg/apache/fontbox/ttf/TTFSubsetter;->writeUint32(Ljava/io/DataOutputStream;J)V

    invoke-virtual {v2}, Lorg/apache/fontbox/ttf/HeaderTable;->getFlags()I

    move-result v3

    invoke-static {v1, v3}, Lorg/apache/fontbox/ttf/TTFSubsetter;->writeUint16(Ljava/io/DataOutputStream;I)V

    invoke-virtual {v2}, Lorg/apache/fontbox/ttf/HeaderTable;->getUnitsPerEm()I

    move-result v3

    invoke-static {v1, v3}, Lorg/apache/fontbox/ttf/TTFSubsetter;->writeUint16(Ljava/io/DataOutputStream;I)V

    invoke-virtual {v2}, Lorg/apache/fontbox/ttf/HeaderTable;->getCreated()Ljava/util/Calendar;

    move-result-object v3

    invoke-static {v1, v3}, Lorg/apache/fontbox/ttf/TTFSubsetter;->writeLongDateTime(Ljava/io/DataOutputStream;Ljava/util/Calendar;)V

    invoke-virtual {v2}, Lorg/apache/fontbox/ttf/HeaderTable;->getModified()Ljava/util/Calendar;

    move-result-object v3

    invoke-static {v1, v3}, Lorg/apache/fontbox/ttf/TTFSubsetter;->writeLongDateTime(Ljava/io/DataOutputStream;Ljava/util/Calendar;)V

    invoke-virtual {v2}, Lorg/apache/fontbox/ttf/HeaderTable;->getXMin()S

    move-result v3

    invoke-static {v1, v3}, Lorg/apache/fontbox/ttf/TTFSubsetter;->writeSInt16(Ljava/io/DataOutputStream;S)V

    invoke-virtual {v2}, Lorg/apache/fontbox/ttf/HeaderTable;->getYMin()S

    move-result v3

    invoke-static {v1, v3}, Lorg/apache/fontbox/ttf/TTFSubsetter;->writeSInt16(Ljava/io/DataOutputStream;S)V

    invoke-virtual {v2}, Lorg/apache/fontbox/ttf/HeaderTable;->getXMax()S

    move-result v3

    invoke-static {v1, v3}, Lorg/apache/fontbox/ttf/TTFSubsetter;->writeSInt16(Ljava/io/DataOutputStream;S)V

    invoke-virtual {v2}, Lorg/apache/fontbox/ttf/HeaderTable;->getYMax()S

    move-result v3

    invoke-static {v1, v3}, Lorg/apache/fontbox/ttf/TTFSubsetter;->writeSInt16(Ljava/io/DataOutputStream;S)V

    invoke-virtual {v2}, Lorg/apache/fontbox/ttf/HeaderTable;->getMacStyle()I

    move-result v3

    invoke-static {v1, v3}, Lorg/apache/fontbox/ttf/TTFSubsetter;->writeUint16(Ljava/io/DataOutputStream;I)V

    invoke-virtual {v2}, Lorg/apache/fontbox/ttf/HeaderTable;->getLowestRecPPEM()I

    move-result v3

    invoke-static {v1, v3}, Lorg/apache/fontbox/ttf/TTFSubsetter;->writeUint16(Ljava/io/DataOutputStream;I)V

    invoke-virtual {v2}, Lorg/apache/fontbox/ttf/HeaderTable;->getFontDirectionHint()S

    move-result v3

    invoke-static {v1, v3}, Lorg/apache/fontbox/ttf/TTFSubsetter;->writeSInt16(Ljava/io/DataOutputStream;S)V

    const/4 v3, 0x1

    invoke-static {v1, v3}, Lorg/apache/fontbox/ttf/TTFSubsetter;->writeSInt16(Ljava/io/DataOutputStream;S)V

    invoke-virtual {v2}, Lorg/apache/fontbox/ttf/HeaderTable;->getGlyphDataFormat()S

    move-result v2

    invoke-static {v1, v2}, Lorg/apache/fontbox/ttf/TTFSubsetter;->writeSInt16(Ljava/io/DataOutputStream;S)V

    invoke-virtual {v1}, Ljava/io/DataOutputStream;->flush()V

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    return-object v0
.end method

.method private buildHheaTable()[B
    .locals 5

    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    new-instance v1, Ljava/io/DataOutputStream;

    invoke-direct {v1, v0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    iget-object v2, p0, Lorg/apache/fontbox/ttf/TTFSubsetter;->ttf:Lorg/apache/fontbox/ttf/TrueTypeFont;

    invoke-virtual {v2}, Lorg/apache/fontbox/ttf/TrueTypeFont;->getHorizontalHeader()Lorg/apache/fontbox/ttf/HorizontalHeaderTable;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/fontbox/ttf/HorizontalHeaderTable;->getVersion()F

    move-result v3

    float-to-double v3, v3

    invoke-static {v1, v3, v4}, Lorg/apache/fontbox/ttf/TTFSubsetter;->writeFixed(Ljava/io/DataOutputStream;D)V

    invoke-virtual {v2}, Lorg/apache/fontbox/ttf/HorizontalHeaderTable;->getAscender()S

    move-result v3

    invoke-static {v1, v3}, Lorg/apache/fontbox/ttf/TTFSubsetter;->writeSInt16(Ljava/io/DataOutputStream;S)V

    invoke-virtual {v2}, Lorg/apache/fontbox/ttf/HorizontalHeaderTable;->getDescender()S

    move-result v3

    invoke-static {v1, v3}, Lorg/apache/fontbox/ttf/TTFSubsetter;->writeSInt16(Ljava/io/DataOutputStream;S)V

    invoke-virtual {v2}, Lorg/apache/fontbox/ttf/HorizontalHeaderTable;->getLineGap()S

    move-result v3

    invoke-static {v1, v3}, Lorg/apache/fontbox/ttf/TTFSubsetter;->writeSInt16(Ljava/io/DataOutputStream;S)V

    invoke-virtual {v2}, Lorg/apache/fontbox/ttf/HorizontalHeaderTable;->getAdvanceWidthMax()I

    move-result v3

    invoke-static {v1, v3}, Lorg/apache/fontbox/ttf/TTFSubsetter;->writeUint16(Ljava/io/DataOutputStream;I)V

    invoke-virtual {v2}, Lorg/apache/fontbox/ttf/HorizontalHeaderTable;->getMinLeftSideBearing()S

    move-result v3

    invoke-static {v1, v3}, Lorg/apache/fontbox/ttf/TTFSubsetter;->writeSInt16(Ljava/io/DataOutputStream;S)V

    invoke-virtual {v2}, Lorg/apache/fontbox/ttf/HorizontalHeaderTable;->getMinRightSideBearing()S

    move-result v3

    invoke-static {v1, v3}, Lorg/apache/fontbox/ttf/TTFSubsetter;->writeSInt16(Ljava/io/DataOutputStream;S)V

    invoke-virtual {v2}, Lorg/apache/fontbox/ttf/HorizontalHeaderTable;->getXMaxExtent()S

    move-result v3

    invoke-static {v1, v3}, Lorg/apache/fontbox/ttf/TTFSubsetter;->writeSInt16(Ljava/io/DataOutputStream;S)V

    invoke-virtual {v2}, Lorg/apache/fontbox/ttf/HorizontalHeaderTable;->getCaretSlopeRise()S

    move-result v3

    invoke-static {v1, v3}, Lorg/apache/fontbox/ttf/TTFSubsetter;->writeSInt16(Ljava/io/DataOutputStream;S)V

    invoke-virtual {v2}, Lorg/apache/fontbox/ttf/HorizontalHeaderTable;->getCaretSlopeRun()S

    move-result v3

    invoke-static {v1, v3}, Lorg/apache/fontbox/ttf/TTFSubsetter;->writeSInt16(Ljava/io/DataOutputStream;S)V

    invoke-virtual {v2}, Lorg/apache/fontbox/ttf/HorizontalHeaderTable;->getReserved1()S

    move-result v3

    invoke-static {v1, v3}, Lorg/apache/fontbox/ttf/TTFSubsetter;->writeSInt16(Ljava/io/DataOutputStream;S)V

    invoke-virtual {v2}, Lorg/apache/fontbox/ttf/HorizontalHeaderTable;->getReserved2()S

    move-result v3

    invoke-static {v1, v3}, Lorg/apache/fontbox/ttf/TTFSubsetter;->writeSInt16(Ljava/io/DataOutputStream;S)V

    invoke-virtual {v2}, Lorg/apache/fontbox/ttf/HorizontalHeaderTable;->getReserved3()S

    move-result v3

    invoke-static {v1, v3}, Lorg/apache/fontbox/ttf/TTFSubsetter;->writeSInt16(Ljava/io/DataOutputStream;S)V

    invoke-virtual {v2}, Lorg/apache/fontbox/ttf/HorizontalHeaderTable;->getReserved4()S

    move-result v3

    invoke-static {v1, v3}, Lorg/apache/fontbox/ttf/TTFSubsetter;->writeSInt16(Ljava/io/DataOutputStream;S)V

    invoke-virtual {v2}, Lorg/apache/fontbox/ttf/HorizontalHeaderTable;->getReserved5()S

    move-result v3

    invoke-static {v1, v3}, Lorg/apache/fontbox/ttf/TTFSubsetter;->writeSInt16(Ljava/io/DataOutputStream;S)V

    invoke-virtual {v2}, Lorg/apache/fontbox/ttf/HorizontalHeaderTable;->getMetricDataFormat()S

    move-result v3

    invoke-static {v1, v3}, Lorg/apache/fontbox/ttf/TTFSubsetter;->writeSInt16(Ljava/io/DataOutputStream;S)V

    iget-object v3, p0, Lorg/apache/fontbox/ttf/TTFSubsetter;->glyphIds:Ljava/util/SortedSet;

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2}, Lorg/apache/fontbox/ttf/HorizontalHeaderTable;->getNumberOfHMetrics()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v3, v4, v2}, Ljava/util/SortedSet;->subSet(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/SortedSet;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->size()I

    move-result v2

    invoke-static {v1, v2}, Lorg/apache/fontbox/ttf/TTFSubsetter;->writeUint16(Ljava/io/DataOutputStream;I)V

    invoke-virtual {v1}, Ljava/io/DataOutputStream;->flush()V

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    return-object v0
.end method

.method private buildHmtxTable()[B
    .locals 17

    move-object/from16 v1, p0

    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    iget-object v2, v1, Lorg/apache/fontbox/ttf/TTFSubsetter;->ttf:Lorg/apache/fontbox/ttf/TrueTypeFont;

    invoke-virtual {v2}, Lorg/apache/fontbox/ttf/TrueTypeFont;->getHorizontalHeader()Lorg/apache/fontbox/ttf/HorizontalHeaderTable;

    move-result-object v2

    iget-object v3, v1, Lorg/apache/fontbox/ttf/TTFSubsetter;->ttf:Lorg/apache/fontbox/ttf/TrueTypeFont;

    invoke-virtual {v3}, Lorg/apache/fontbox/ttf/TrueTypeFont;->getHorizontalMetrics()Lorg/apache/fontbox/ttf/HorizontalMetricsTable;

    move-result-object v3

    const/4 v4, 0x4

    new-array v5, v4, [B

    iget-object v6, v1, Lorg/apache/fontbox/ttf/TTFSubsetter;->ttf:Lorg/apache/fontbox/ttf/TrueTypeFont;

    invoke-virtual {v6}, Lorg/apache/fontbox/ttf/TrueTypeFont;->getOriginalData()Ljava/io/InputStream;

    move-result-object v6

    :try_start_0
    invoke-virtual {v3}, Lorg/apache/fontbox/ttf/TTFTable;->getOffset()J

    move-result-wide v7

    invoke-virtual {v6, v7, v8}, Ljava/io/InputStream;->skip(J)J

    iget-object v3, v1, Lorg/apache/fontbox/ttf/TTFSubsetter;->glyphIds:Ljava/util/SortedSet;

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    const-wide/16 v7, 0x0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Integer;

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v10

    invoke-virtual {v2}, Lorg/apache/fontbox/ttf/HorizontalHeaderTable;->getNumberOfHMetrics()I

    move-result v11

    const/4 v12, 0x2

    if-ge v10, v11, :cond_0

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v10

    mul-int/2addr v10, v4

    :goto_1
    int-to-long v10, v10

    goto :goto_2

    :cond_0
    invoke-virtual {v2}, Lorg/apache/fontbox/ttf/HorizontalHeaderTable;->getNumberOfHMetrics()I

    move-result v10

    mul-int/2addr v10, v4

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v11

    invoke-virtual {v2}, Lorg/apache/fontbox/ttf/HorizontalHeaderTable;->getNumberOfHMetrics()I

    move-result v13
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    sub-int/2addr v11, v13

    mul-int/2addr v11, v12

    add-int/2addr v10, v11

    goto :goto_1

    :goto_2
    cmp-long v13, v10, v7

    const-string v14, "Unexpected EOF exception parsing glyphId of hmtx table."

    if-eqz v13, :cond_2

    sub-long v7, v10, v7

    :try_start_1
    invoke-virtual {v6, v7, v8}, Ljava/io/InputStream;->skip(J)J

    move-result-wide v15

    cmp-long v7, v7, v15

    if-nez v7, :cond_1

    goto :goto_3

    :cond_1
    new-instance v0, Ljava/io/EOFException;

    invoke-direct {v0, v14}, Ljava/io/EOFException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    :goto_3
    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v7

    invoke-virtual {v2}, Lorg/apache/fontbox/ttf/HorizontalHeaderTable;->getNumberOfHMetrics()I

    move-result v8

    if-ge v7, v8, :cond_3

    move v12, v4

    :cond_3
    const/4 v7, 0x0

    invoke-virtual {v6, v5, v7, v12}, Ljava/io/InputStream;->read([BII)I

    move-result v8

    if-ne v12, v8, :cond_4

    invoke-virtual {v0, v5, v7, v12}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    int-to-long v7, v12

    add-long/2addr v7, v10

    goto :goto_0

    :cond_4
    new-instance v0, Ljava/io/EOFException;

    invoke-direct {v0, v14}, Ljava/io/EOFException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_5
    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-virtual {v6}, Ljava/io/InputStream;->close()V

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v6}, Ljava/io/InputStream;->close()V

    throw v0
.end method

.method private static buildLocaTable([J)[B
    .locals 6

    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    new-instance v1, Ljava/io/DataOutputStream;

    invoke-direct {v1, v0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    array-length v2, p0

    const/4 v3, 0x0

    :goto_0
    if-ge v3, v2, :cond_0

    aget-wide v4, p0, v3

    invoke-static {v1, v4, v5}, Lorg/apache/fontbox/ttf/TTFSubsetter;->writeUint32(Ljava/io/DataOutputStream;J)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {v1}, Ljava/io/DataOutputStream;->flush()V

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object p0

    return-object p0
.end method

.method private buildMaxpTable()[B
    .locals 5

    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    new-instance v1, Ljava/io/DataOutputStream;

    invoke-direct {v1, v0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    iget-object v2, p0, Lorg/apache/fontbox/ttf/TTFSubsetter;->ttf:Lorg/apache/fontbox/ttf/TrueTypeFont;

    invoke-virtual {v2}, Lorg/apache/fontbox/ttf/TrueTypeFont;->getMaximumProfile()Lorg/apache/fontbox/ttf/MaximumProfileTable;

    move-result-object v2

    const-wide/high16 v3, 0x3ff0000000000000L    # 1.0

    invoke-static {v1, v3, v4}, Lorg/apache/fontbox/ttf/TTFSubsetter;->writeFixed(Ljava/io/DataOutputStream;D)V

    iget-object v3, p0, Lorg/apache/fontbox/ttf/TTFSubsetter;->glyphIds:Ljava/util/SortedSet;

    invoke-interface {v3}, Ljava/util/Set;->size()I

    move-result v3

    invoke-static {v1, v3}, Lorg/apache/fontbox/ttf/TTFSubsetter;->writeUint16(Ljava/io/DataOutputStream;I)V

    invoke-virtual {v2}, Lorg/apache/fontbox/ttf/MaximumProfileTable;->getMaxPoints()I

    move-result v3

    invoke-static {v1, v3}, Lorg/apache/fontbox/ttf/TTFSubsetter;->writeUint16(Ljava/io/DataOutputStream;I)V

    invoke-virtual {v2}, Lorg/apache/fontbox/ttf/MaximumProfileTable;->getMaxContours()I

    move-result v3

    invoke-static {v1, v3}, Lorg/apache/fontbox/ttf/TTFSubsetter;->writeUint16(Ljava/io/DataOutputStream;I)V

    invoke-virtual {v2}, Lorg/apache/fontbox/ttf/MaximumProfileTable;->getMaxCompositePoints()I

    move-result v3

    invoke-static {v1, v3}, Lorg/apache/fontbox/ttf/TTFSubsetter;->writeUint16(Ljava/io/DataOutputStream;I)V

    invoke-virtual {v2}, Lorg/apache/fontbox/ttf/MaximumProfileTable;->getMaxCompositeContours()I

    move-result v3

    invoke-static {v1, v3}, Lorg/apache/fontbox/ttf/TTFSubsetter;->writeUint16(Ljava/io/DataOutputStream;I)V

    invoke-virtual {v2}, Lorg/apache/fontbox/ttf/MaximumProfileTable;->getMaxZones()I

    move-result v3

    invoke-static {v1, v3}, Lorg/apache/fontbox/ttf/TTFSubsetter;->writeUint16(Ljava/io/DataOutputStream;I)V

    invoke-virtual {v2}, Lorg/apache/fontbox/ttf/MaximumProfileTable;->getMaxTwilightPoints()I

    move-result v3

    invoke-static {v1, v3}, Lorg/apache/fontbox/ttf/TTFSubsetter;->writeUint16(Ljava/io/DataOutputStream;I)V

    invoke-virtual {v2}, Lorg/apache/fontbox/ttf/MaximumProfileTable;->getMaxStorage()I

    move-result v3

    invoke-static {v1, v3}, Lorg/apache/fontbox/ttf/TTFSubsetter;->writeUint16(Ljava/io/DataOutputStream;I)V

    invoke-virtual {v2}, Lorg/apache/fontbox/ttf/MaximumProfileTable;->getMaxFunctionDefs()I

    move-result v3

    invoke-static {v1, v3}, Lorg/apache/fontbox/ttf/TTFSubsetter;->writeUint16(Ljava/io/DataOutputStream;I)V

    invoke-virtual {v2}, Lorg/apache/fontbox/ttf/MaximumProfileTable;->getMaxInstructionDefs()I

    move-result v3

    invoke-static {v1, v3}, Lorg/apache/fontbox/ttf/TTFSubsetter;->writeUint16(Ljava/io/DataOutputStream;I)V

    invoke-virtual {v2}, Lorg/apache/fontbox/ttf/MaximumProfileTable;->getMaxStackElements()I

    move-result v3

    invoke-static {v1, v3}, Lorg/apache/fontbox/ttf/TTFSubsetter;->writeUint16(Ljava/io/DataOutputStream;I)V

    invoke-virtual {v2}, Lorg/apache/fontbox/ttf/MaximumProfileTable;->getMaxSizeOfInstructions()I

    move-result v3

    invoke-static {v1, v3}, Lorg/apache/fontbox/ttf/TTFSubsetter;->writeUint16(Ljava/io/DataOutputStream;I)V

    invoke-virtual {v2}, Lorg/apache/fontbox/ttf/MaximumProfileTable;->getMaxComponentElements()I

    move-result v3

    invoke-static {v1, v3}, Lorg/apache/fontbox/ttf/TTFSubsetter;->writeUint16(Ljava/io/DataOutputStream;I)V

    invoke-virtual {v2}, Lorg/apache/fontbox/ttf/MaximumProfileTable;->getMaxComponentDepth()I

    move-result v2

    invoke-static {v1, v2}, Lorg/apache/fontbox/ttf/TTFSubsetter;->writeUint16(Ljava/io/DataOutputStream;I)V

    invoke-virtual {v1}, Ljava/io/DataOutputStream;->flush()V

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    return-object v0
.end method

.method private buildNameTable()[B
    .locals 15

    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    new-instance v1, Ljava/io/DataOutputStream;

    invoke-direct {v1, v0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    iget-object v2, p0, Lorg/apache/fontbox/ttf/TTFSubsetter;->ttf:Lorg/apache/fontbox/ttf/TrueTypeFont;

    invoke-virtual {v2}, Lorg/apache/fontbox/ttf/TrueTypeFont;->getNaming()Lorg/apache/fontbox/ttf/NamingTable;

    move-result-object v2

    const/4 v3, 0x0

    if-eqz v2, :cond_d

    iget-object v4, p0, Lorg/apache/fontbox/ttf/TTFSubsetter;->keepTables:Ljava/util/List;

    if-eqz v4, :cond_0

    const-string v5, "name"

    invoke-interface {v4, v5}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    goto/16 :goto_5

    :cond_0
    invoke-virtual {v2}, Lorg/apache/fontbox/ttf/NamingTable;->getNameRecords()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    const/4 v5, 0x0

    move v6, v5

    :cond_1
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lorg/apache/fontbox/ttf/NameRecord;

    invoke-static {v7}, Lorg/apache/fontbox/ttf/TTFSubsetter;->shouldCopyNameRecord(Lorg/apache/fontbox/ttf/NameRecord;)Z

    move-result v7

    if-eqz v7, :cond_1

    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    :cond_2
    invoke-static {v1, v5}, Lorg/apache/fontbox/ttf/TTFSubsetter;->writeUint16(Ljava/io/DataOutputStream;I)V

    invoke-static {v1, v6}, Lorg/apache/fontbox/ttf/TTFSubsetter;->writeUint16(Ljava/io/DataOutputStream;I)V

    mul-int/lit8 v4, v6, 0xc

    const/4 v7, 0x6

    add-int/2addr v4, v7

    invoke-static {v1, v4}, Lorg/apache/fontbox/ttf/TTFSubsetter;->writeUint16(Ljava/io/DataOutputStream;I)V

    if-nez v6, :cond_3

    return-object v3

    :cond_3
    new-array v3, v6, [[B

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v8, v5

    :cond_4
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_9

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lorg/apache/fontbox/ttf/NameRecord;

    invoke-static {v9}, Lorg/apache/fontbox/ttf/TTFSubsetter;->shouldCopyNameRecord(Lorg/apache/fontbox/ttf/NameRecord;)Z

    move-result v10

    if-eqz v10, :cond_4

    invoke-virtual {v9}, Lorg/apache/fontbox/ttf/NameRecord;->getPlatformId()I

    move-result v10

    invoke-virtual {v9}, Lorg/apache/fontbox/ttf/NameRecord;->getPlatformEncodingId()I

    move-result v11

    const/4 v12, 0x3

    const/4 v13, 0x1

    if-ne v10, v12, :cond_5

    if-ne v11, v13, :cond_5

    const-string v10, "UTF-16BE"

    goto :goto_2

    :cond_5
    const/4 v12, 0x2

    const-string v14, "ISO-8859-1"

    if-ne v10, v12, :cond_7

    if-nez v11, :cond_6

    const-string v10, "US-ASCII"

    goto :goto_2

    :cond_6
    if-ne v11, v13, :cond_7

    const-string v10, "UTF16-BE"

    goto :goto_2

    :cond_7
    move-object v10, v14

    :goto_2
    invoke-virtual {v9}, Lorg/apache/fontbox/ttf/NameRecord;->getString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v9}, Lorg/apache/fontbox/ttf/NameRecord;->getNameId()I

    move-result v9

    if-ne v9, v7, :cond_8

    iget-object v9, p0, Lorg/apache/fontbox/ttf/TTFSubsetter;->prefix:Ljava/lang/String;

    if-eqz v9, :cond_8

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v12, p0, Lorg/apache/fontbox/ttf/TTFSubsetter;->prefix:Ljava/lang/String;

    invoke-virtual {v9, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    :cond_8
    invoke-virtual {v11, v10}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v9

    aput-object v9, v3, v8

    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    :cond_9
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v4, v5

    move v7, v4

    :cond_a
    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_b

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lorg/apache/fontbox/ttf/NameRecord;

    invoke-static {v8}, Lorg/apache/fontbox/ttf/TTFSubsetter;->shouldCopyNameRecord(Lorg/apache/fontbox/ttf/NameRecord;)Z

    move-result v9

    if-eqz v9, :cond_a

    invoke-virtual {v8}, Lorg/apache/fontbox/ttf/NameRecord;->getPlatformId()I

    move-result v9

    invoke-static {v1, v9}, Lorg/apache/fontbox/ttf/TTFSubsetter;->writeUint16(Ljava/io/DataOutputStream;I)V

    invoke-virtual {v8}, Lorg/apache/fontbox/ttf/NameRecord;->getPlatformEncodingId()I

    move-result v9

    invoke-static {v1, v9}, Lorg/apache/fontbox/ttf/TTFSubsetter;->writeUint16(Ljava/io/DataOutputStream;I)V

    invoke-virtual {v8}, Lorg/apache/fontbox/ttf/NameRecord;->getLanguageId()I

    move-result v9

    invoke-static {v1, v9}, Lorg/apache/fontbox/ttf/TTFSubsetter;->writeUint16(Ljava/io/DataOutputStream;I)V

    invoke-virtual {v8}, Lorg/apache/fontbox/ttf/NameRecord;->getNameId()I

    move-result v8

    invoke-static {v1, v8}, Lorg/apache/fontbox/ttf/TTFSubsetter;->writeUint16(Ljava/io/DataOutputStream;I)V

    aget-object v8, v3, v4

    array-length v8, v8

    invoke-static {v1, v8}, Lorg/apache/fontbox/ttf/TTFSubsetter;->writeUint16(Ljava/io/DataOutputStream;I)V

    invoke-static {v1, v7}, Lorg/apache/fontbox/ttf/TTFSubsetter;->writeUint16(Ljava/io/DataOutputStream;I)V

    aget-object v8, v3, v4

    array-length v8, v8

    add-int/2addr v7, v8

    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    :cond_b
    :goto_4
    if-ge v5, v6, :cond_c

    aget-object v2, v3, v5

    invoke-virtual {v1, v2}, Ljava/io/OutputStream;->write([B)V

    add-int/lit8 v5, v5, 0x1

    goto :goto_4

    :cond_c
    invoke-virtual {v1}, Ljava/io/DataOutputStream;->flush()V

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    return-object v0

    :cond_d
    :goto_5
    return-object v3
.end method

.method private buildOS2Table()[B
    .locals 5

    iget-object v0, p0, Lorg/apache/fontbox/ttf/TTFSubsetter;->ttf:Lorg/apache/fontbox/ttf/TrueTypeFont;

    invoke-virtual {v0}, Lorg/apache/fontbox/ttf/TrueTypeFont;->getOS2Windows()Lorg/apache/fontbox/ttf/OS2WindowsMetricsTable;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lorg/apache/fontbox/ttf/TTFSubsetter;->keepTables:Ljava/util/List;

    if-eqz v1, :cond_0

    const-string v2, "OS/2"

    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    goto/16 :goto_0

    :cond_0
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    new-instance v2, Ljava/io/DataOutputStream;

    invoke-direct {v2, v1}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    invoke-virtual {v0}, Lorg/apache/fontbox/ttf/OS2WindowsMetricsTable;->getVersion()I

    move-result v3

    invoke-static {v2, v3}, Lorg/apache/fontbox/ttf/TTFSubsetter;->writeUint16(Ljava/io/DataOutputStream;I)V

    invoke-virtual {v0}, Lorg/apache/fontbox/ttf/OS2WindowsMetricsTable;->getAverageCharWidth()S

    move-result v3

    invoke-static {v2, v3}, Lorg/apache/fontbox/ttf/TTFSubsetter;->writeSInt16(Ljava/io/DataOutputStream;S)V

    invoke-virtual {v0}, Lorg/apache/fontbox/ttf/OS2WindowsMetricsTable;->getWeightClass()I

    move-result v3

    invoke-static {v2, v3}, Lorg/apache/fontbox/ttf/TTFSubsetter;->writeUint16(Ljava/io/DataOutputStream;I)V

    invoke-virtual {v0}, Lorg/apache/fontbox/ttf/OS2WindowsMetricsTable;->getWidthClass()I

    move-result v3

    invoke-static {v2, v3}, Lorg/apache/fontbox/ttf/TTFSubsetter;->writeUint16(Ljava/io/DataOutputStream;I)V

    invoke-virtual {v0}, Lorg/apache/fontbox/ttf/OS2WindowsMetricsTable;->getFsType()S

    move-result v3

    invoke-static {v2, v3}, Lorg/apache/fontbox/ttf/TTFSubsetter;->writeSInt16(Ljava/io/DataOutputStream;S)V

    invoke-virtual {v0}, Lorg/apache/fontbox/ttf/OS2WindowsMetricsTable;->getSubscriptXSize()S

    move-result v3

    invoke-static {v2, v3}, Lorg/apache/fontbox/ttf/TTFSubsetter;->writeSInt16(Ljava/io/DataOutputStream;S)V

    invoke-virtual {v0}, Lorg/apache/fontbox/ttf/OS2WindowsMetricsTable;->getSubscriptYSize()S

    move-result v3

    invoke-static {v2, v3}, Lorg/apache/fontbox/ttf/TTFSubsetter;->writeSInt16(Ljava/io/DataOutputStream;S)V

    invoke-virtual {v0}, Lorg/apache/fontbox/ttf/OS2WindowsMetricsTable;->getSubscriptXOffset()S

    move-result v3

    invoke-static {v2, v3}, Lorg/apache/fontbox/ttf/TTFSubsetter;->writeSInt16(Ljava/io/DataOutputStream;S)V

    invoke-virtual {v0}, Lorg/apache/fontbox/ttf/OS2WindowsMetricsTable;->getSubscriptYOffset()S

    move-result v3

    invoke-static {v2, v3}, Lorg/apache/fontbox/ttf/TTFSubsetter;->writeSInt16(Ljava/io/DataOutputStream;S)V

    invoke-virtual {v0}, Lorg/apache/fontbox/ttf/OS2WindowsMetricsTable;->getSuperscriptXSize()S

    move-result v3

    invoke-static {v2, v3}, Lorg/apache/fontbox/ttf/TTFSubsetter;->writeSInt16(Ljava/io/DataOutputStream;S)V

    invoke-virtual {v0}, Lorg/apache/fontbox/ttf/OS2WindowsMetricsTable;->getSuperscriptYSize()S

    move-result v3

    invoke-static {v2, v3}, Lorg/apache/fontbox/ttf/TTFSubsetter;->writeSInt16(Ljava/io/DataOutputStream;S)V

    invoke-virtual {v0}, Lorg/apache/fontbox/ttf/OS2WindowsMetricsTable;->getSuperscriptXOffset()S

    move-result v3

    invoke-static {v2, v3}, Lorg/apache/fontbox/ttf/TTFSubsetter;->writeSInt16(Ljava/io/DataOutputStream;S)V

    invoke-virtual {v0}, Lorg/apache/fontbox/ttf/OS2WindowsMetricsTable;->getSuperscriptYOffset()S

    move-result v3

    invoke-static {v2, v3}, Lorg/apache/fontbox/ttf/TTFSubsetter;->writeSInt16(Ljava/io/DataOutputStream;S)V

    invoke-virtual {v0}, Lorg/apache/fontbox/ttf/OS2WindowsMetricsTable;->getStrikeoutSize()S

    move-result v3

    invoke-static {v2, v3}, Lorg/apache/fontbox/ttf/TTFSubsetter;->writeSInt16(Ljava/io/DataOutputStream;S)V

    invoke-virtual {v0}, Lorg/apache/fontbox/ttf/OS2WindowsMetricsTable;->getStrikeoutPosition()S

    move-result v3

    invoke-static {v2, v3}, Lorg/apache/fontbox/ttf/TTFSubsetter;->writeSInt16(Ljava/io/DataOutputStream;S)V

    invoke-virtual {v0}, Lorg/apache/fontbox/ttf/OS2WindowsMetricsTable;->getFamilyClass()I

    move-result v3

    invoke-static {v2, v3}, Lorg/apache/fontbox/ttf/TTFSubsetter;->writeUint8(Ljava/io/DataOutputStream;I)V

    invoke-virtual {v0}, Lorg/apache/fontbox/ttf/OS2WindowsMetricsTable;->getFamilySubClass()I

    move-result v3

    invoke-static {v2, v3}, Lorg/apache/fontbox/ttf/TTFSubsetter;->writeUint8(Ljava/io/DataOutputStream;I)V

    invoke-virtual {v0}, Lorg/apache/fontbox/ttf/OS2WindowsMetricsTable;->getPanose()[B

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/io/OutputStream;->write([B)V

    const-wide/16 v3, 0x0

    invoke-static {v2, v3, v4}, Lorg/apache/fontbox/ttf/TTFSubsetter;->writeUint32(Ljava/io/DataOutputStream;J)V

    invoke-static {v2, v3, v4}, Lorg/apache/fontbox/ttf/TTFSubsetter;->writeUint32(Ljava/io/DataOutputStream;J)V

    invoke-static {v2, v3, v4}, Lorg/apache/fontbox/ttf/TTFSubsetter;->writeUint32(Ljava/io/DataOutputStream;J)V

    invoke-static {v2, v3, v4}, Lorg/apache/fontbox/ttf/TTFSubsetter;->writeUint32(Ljava/io/DataOutputStream;J)V

    invoke-virtual {v0}, Lorg/apache/fontbox/ttf/OS2WindowsMetricsTable;->getAchVendId()Ljava/lang/String;

    move-result-object v3

    const-string v4, "US-ASCII"

    invoke-virtual {v3, v4}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/io/OutputStream;->write([B)V

    iget-object v3, p0, Lorg/apache/fontbox/ttf/TTFSubsetter;->uniToGID:Ljava/util/SortedMap;

    invoke-interface {v3}, Ljava/util/SortedMap;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    invoke-virtual {v0}, Lorg/apache/fontbox/ttf/OS2WindowsMetricsTable;->getFsSelection()I

    move-result v4

    invoke-static {v2, v4}, Lorg/apache/fontbox/ttf/TTFSubsetter;->writeUint16(Ljava/io/DataOutputStream;I)V

    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lorg/apache/fontbox/ttf/TTFSubsetter;->writeUint16(Ljava/io/DataOutputStream;I)V

    iget-object v3, p0, Lorg/apache/fontbox/ttf/TTFSubsetter;->uniToGID:Ljava/util/SortedMap;

    invoke-interface {v3}, Ljava/util/SortedMap;->lastKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lorg/apache/fontbox/ttf/TTFSubsetter;->writeUint16(Ljava/io/DataOutputStream;I)V

    invoke-virtual {v0}, Lorg/apache/fontbox/ttf/OS2WindowsMetricsTable;->getTypoAscender()I

    move-result v3

    invoke-static {v2, v3}, Lorg/apache/fontbox/ttf/TTFSubsetter;->writeUint16(Ljava/io/DataOutputStream;I)V

    invoke-virtual {v0}, Lorg/apache/fontbox/ttf/OS2WindowsMetricsTable;->getTypoDescender()I

    move-result v3

    invoke-static {v2, v3}, Lorg/apache/fontbox/ttf/TTFSubsetter;->writeUint16(Ljava/io/DataOutputStream;I)V

    invoke-virtual {v0}, Lorg/apache/fontbox/ttf/OS2WindowsMetricsTable;->getTypoLineGap()I

    move-result v3

    invoke-static {v2, v3}, Lorg/apache/fontbox/ttf/TTFSubsetter;->writeUint16(Ljava/io/DataOutputStream;I)V

    invoke-virtual {v0}, Lorg/apache/fontbox/ttf/OS2WindowsMetricsTable;->getWinAscent()I

    move-result v3

    invoke-static {v2, v3}, Lorg/apache/fontbox/ttf/TTFSubsetter;->writeUint16(Ljava/io/DataOutputStream;I)V

    invoke-virtual {v0}, Lorg/apache/fontbox/ttf/OS2WindowsMetricsTable;->getWinDescent()I

    move-result v0

    invoke-static {v2, v0}, Lorg/apache/fontbox/ttf/TTFSubsetter;->writeUint16(Ljava/io/DataOutputStream;I)V

    invoke-virtual {v2}, Ljava/io/DataOutputStream;->flush()V

    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    return-object v0

    :cond_1
    :goto_0
    const/4 v0, 0x0

    return-object v0
.end method

.method private buildPostTable()[B
    .locals 7

    iget-object v0, p0, Lorg/apache/fontbox/ttf/TTFSubsetter;->ttf:Lorg/apache/fontbox/ttf/TrueTypeFont;

    invoke-virtual {v0}, Lorg/apache/fontbox/ttf/TrueTypeFont;->getPostScript()Lorg/apache/fontbox/ttf/PostScriptTable;

    move-result-object v0

    if-eqz v0, :cond_5

    iget-object v1, p0, Lorg/apache/fontbox/ttf/TTFSubsetter;->keepTables:Ljava/util/List;

    if-eqz v1, :cond_0

    const-string v2, "post"

    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    goto/16 :goto_3

    :cond_0
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    new-instance v2, Ljava/io/DataOutputStream;

    invoke-direct {v2, v1}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    const-wide/high16 v3, 0x4000000000000000L    # 2.0

    invoke-static {v2, v3, v4}, Lorg/apache/fontbox/ttf/TTFSubsetter;->writeFixed(Ljava/io/DataOutputStream;D)V

    invoke-virtual {v0}, Lorg/apache/fontbox/ttf/PostScriptTable;->getItalicAngle()F

    move-result v3

    float-to-double v3, v3

    invoke-static {v2, v3, v4}, Lorg/apache/fontbox/ttf/TTFSubsetter;->writeFixed(Ljava/io/DataOutputStream;D)V

    invoke-virtual {v0}, Lorg/apache/fontbox/ttf/PostScriptTable;->getUnderlinePosition()S

    move-result v3

    invoke-static {v2, v3}, Lorg/apache/fontbox/ttf/TTFSubsetter;->writeSInt16(Ljava/io/DataOutputStream;S)V

    invoke-virtual {v0}, Lorg/apache/fontbox/ttf/PostScriptTable;->getUnderlineThickness()S

    move-result v3

    invoke-static {v2, v3}, Lorg/apache/fontbox/ttf/TTFSubsetter;->writeSInt16(Ljava/io/DataOutputStream;S)V

    invoke-virtual {v0}, Lorg/apache/fontbox/ttf/PostScriptTable;->getIsFixedPitch()J

    move-result-wide v3

    invoke-static {v2, v3, v4}, Lorg/apache/fontbox/ttf/TTFSubsetter;->writeUint32(Ljava/io/DataOutputStream;J)V

    invoke-virtual {v0}, Lorg/apache/fontbox/ttf/PostScriptTable;->getMinMemType42()J

    move-result-wide v3

    invoke-static {v2, v3, v4}, Lorg/apache/fontbox/ttf/TTFSubsetter;->writeUint32(Ljava/io/DataOutputStream;J)V

    invoke-virtual {v0}, Lorg/apache/fontbox/ttf/PostScriptTable;->getMaxMemType42()J

    move-result-wide v3

    invoke-static {v2, v3, v4}, Lorg/apache/fontbox/ttf/TTFSubsetter;->writeUint32(Ljava/io/DataOutputStream;J)V

    invoke-virtual {v0}, Lorg/apache/fontbox/ttf/PostScriptTable;->getMinMemType1()J

    move-result-wide v3

    invoke-static {v2, v3, v4}, Lorg/apache/fontbox/ttf/TTFSubsetter;->writeUint32(Ljava/io/DataOutputStream;J)V

    invoke-virtual {v0}, Lorg/apache/fontbox/ttf/PostScriptTable;->getMaxMemType1()J

    move-result-wide v3

    invoke-static {v2, v3, v4}, Lorg/apache/fontbox/ttf/TTFSubsetter;->writeUint32(Ljava/io/DataOutputStream;J)V

    iget-object v3, p0, Lorg/apache/fontbox/ttf/TTFSubsetter;->glyphIds:Ljava/util/SortedSet;

    invoke-interface {v3}, Ljava/util/Set;->size()I

    move-result v3

    invoke-static {v2, v3}, Lorg/apache/fontbox/ttf/TTFSubsetter;->writeUint16(Ljava/io/DataOutputStream;I)V

    new-instance v3, Ljava/util/TreeMap;

    invoke-direct {v3}, Ljava/util/TreeMap;-><init>()V

    iget-object v4, p0, Lorg/apache/fontbox/ttf/TTFSubsetter;->glyphIds:Ljava/util/SortedSet;

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-virtual {v0, v5}, Lorg/apache/fontbox/ttf/PostScriptTable;->getName(I)Ljava/lang/String;

    move-result-object v5

    sget-object v6, Lorg/apache/fontbox/ttf/WGL4Names;->MAC_GLYPH_NAMES_INDICES:Ljava/util/Map;

    invoke-interface {v6, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    if-eqz v6, :cond_1

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v5

    :goto_1
    invoke-static {v2, v5}, Lorg/apache/fontbox/ttf/TTFSubsetter;->writeUint16(Ljava/io/DataOutputStream;I)V

    goto :goto_0

    :cond_1
    invoke-interface {v3, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    if-nez v6, :cond_2

    invoke-interface {v3}, Ljava/util/Map;->size()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v3, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_2
    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v5

    add-int/lit16 v5, v5, 0x102

    goto :goto_1

    :cond_3
    invoke-interface {v3}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_2
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    const-string v4, "US-ASCII"

    invoke-static {v4}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v3

    array-length v4, v3

    invoke-static {v2, v4}, Lorg/apache/fontbox/ttf/TTFSubsetter;->writeUint8(Ljava/io/DataOutputStream;I)V

    invoke-virtual {v2, v3}, Ljava/io/OutputStream;->write([B)V

    goto :goto_2

    :cond_4
    invoke-virtual {v2}, Ljava/io/DataOutputStream;->flush()V

    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    return-object v0

    :cond_5
    :goto_3
    const/4 v0, 0x0

    return-object v0
.end method

.method private getNewGlyphId(Ljava/lang/Integer;)I
    .locals 1

    iget-object v0, p0, Lorg/apache/fontbox/ttf/TTFSubsetter;->glyphIds:Ljava/util/SortedSet;

    invoke-interface {v0, p1}, Ljava/util/SortedSet;->headSet(Ljava/lang/Object;)Ljava/util/SortedSet;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Set;->size()I

    move-result p1

    return p1
.end method

.method private static log2(I)I
    .locals 4

    int-to-double v0, p0

    invoke-static {v0, v1}, Ljava/lang/Math;->log(D)D

    move-result-wide v0

    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    invoke-static {v2, v3}, Ljava/lang/Math;->log(D)D

    move-result-wide v2

    div-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    long-to-int p0, v0

    return p0
.end method

.method private static shouldCopyNameRecord(Lorg/apache/fontbox/ttf/NameRecord;)Z
    .locals 3

    invoke-virtual {p0}, Lorg/apache/fontbox/ttf/NameRecord;->getPlatformId()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lorg/apache/fontbox/ttf/NameRecord;->getPlatformEncodingId()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lorg/apache/fontbox/ttf/NameRecord;->getLanguageId()I

    move-result v0

    const/16 v2, 0x409

    if-ne v0, v2, :cond_0

    invoke-virtual {p0}, Lorg/apache/fontbox/ttf/NameRecord;->getNameId()I

    move-result v0

    if-ltz v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/fontbox/ttf/NameRecord;->getNameId()I

    move-result p0

    const/4 v0, 0x7

    if-ge p0, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method private static toUInt32(II)J
    .locals 4

    .line 1
    int-to-long v0, p0

    const-wide/32 v2, 0xffff

    and-long/2addr v0, v2

    const/16 p0, 0x10

    shl-long/2addr v0, p0

    int-to-long p0, p1

    and-long/2addr p0, v2

    or-long/2addr p0, v0

    return-wide p0
.end method

.method private static toUInt32([B)J
    .locals 7

    .line 2
    const/4 v0, 0x0

    aget-byte v0, p0, v0

    int-to-long v0, v0

    const-wide/16 v2, 0xff

    and-long/2addr v0, v2

    const/16 v4, 0x18

    shl-long/2addr v0, v4

    const/4 v4, 0x1

    aget-byte v4, p0, v4

    int-to-long v4, v4

    and-long/2addr v4, v2

    const/16 v6, 0x10

    shl-long/2addr v4, v6

    or-long/2addr v0, v4

    const/4 v4, 0x2

    aget-byte v4, p0, v4

    int-to-long v4, v4

    and-long/2addr v4, v2

    const/16 v6, 0x8

    shl-long/2addr v4, v6

    or-long/2addr v0, v4

    const/4 v4, 0x3

    aget-byte p0, p0, v4

    int-to-long v4, p0

    and-long/2addr v2, v4

    or-long/2addr v0, v2

    return-wide v0
.end method

.method private static writeFileHeader(Ljava/io/DataOutputStream;I)J
    .locals 5

    const/high16 v0, 0x10000

    invoke-virtual {p0, v0}, Ljava/io/DataOutputStream;->writeInt(I)V

    invoke-virtual {p0, p1}, Ljava/io/DataOutputStream;->writeShort(I)V

    invoke-static {p1}, Ljava/lang/Integer;->highestOneBit(I)I

    move-result v0

    mul-int/lit8 v1, v0, 0x10

    invoke-virtual {p0, v1}, Ljava/io/DataOutputStream;->writeShort(I)V

    invoke-static {v0}, Lorg/apache/fontbox/ttf/TTFSubsetter;->log2(I)I

    move-result v0

    invoke-virtual {p0, v0}, Ljava/io/DataOutputStream;->writeShort(I)V

    mul-int/lit8 v2, p1, 0x10

    sub-int/2addr v2, v1

    invoke-virtual {p0, v2}, Ljava/io/DataOutputStream;->writeShort(I)V

    const-wide/32 v3, 0x10000

    invoke-static {p1, v1}, Lorg/apache/fontbox/ttf/TTFSubsetter;->toUInt32(II)J

    move-result-wide p0

    add-long/2addr p0, v3

    invoke-static {v0, v2}, Lorg/apache/fontbox/ttf/TTFSubsetter;->toUInt32(II)J

    move-result-wide v0

    add-long/2addr p0, v0

    return-wide p0
.end method

.method private static writeFixed(Ljava/io/DataOutputStream;D)V
    .locals 4

    invoke-static {p1, p2}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    sub-double/2addr p1, v0

    const-wide/high16 v2, 0x40f0000000000000L    # 65536.0

    mul-double/2addr p1, v2

    double-to-int v0, v0

    invoke-virtual {p0, v0}, Ljava/io/DataOutputStream;->writeShort(I)V

    double-to-int p1, p1

    invoke-virtual {p0, p1}, Ljava/io/DataOutputStream;->writeShort(I)V

    return-void
.end method

.method private static writeLongDateTime(Ljava/io/DataOutputStream;Ljava/util/Calendar;)V
    .locals 4

    new-instance v0, Ljava/util/GregorianCalendar;

    const/4 v1, 0x0

    const/4 v2, 0x1

    const/16 v3, 0x770

    invoke-direct {v0, v3, v1, v2}, Ljava/util/GregorianCalendar;-><init>(III)V

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    invoke-virtual {p1}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    sub-long/2addr v2, v0

    const-wide/16 v0, 0x3e8

    div-long/2addr v2, v0

    invoke-virtual {p0, v2, v3}, Ljava/io/DataOutputStream;->writeLong(J)V

    return-void
.end method

.method private static writeSInt16(Ljava/io/DataOutputStream;S)V
    .locals 0

    invoke-virtual {p0, p1}, Ljava/io/DataOutputStream;->writeShort(I)V

    return-void
.end method

.method private static writeTableBody(Ljava/io/OutputStream;[B)V
    .locals 2

    array-length v0, p1

    invoke-virtual {p0, p1}, Ljava/io/OutputStream;->write([B)V

    rem-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    sget-object p1, Lorg/apache/fontbox/ttf/TTFSubsetter;->PAD_BUF:[B

    const/4 v1, 0x0

    rsub-int/lit8 v0, v0, 0x4

    invoke-virtual {p0, p1, v1, v0}, Ljava/io/OutputStream;->write([BII)V

    :cond_0
    return-void
.end method

.method private static writeTableHeader(Ljava/io/DataOutputStream;Ljava/lang/String;J[B)J
    .locals 9

    array-length v0, p4

    const-wide/16 v1, 0x0

    const/4 v3, 0x0

    move v4, v3

    :goto_0
    if-ge v4, v0, :cond_0

    aget-byte v5, p4, v4

    int-to-long v5, v5

    const-wide/16 v7, 0xff

    and-long/2addr v5, v7

    rem-int/lit8 v7, v4, 0x4

    mul-int/lit8 v7, v7, 0x8

    rsub-int/lit8 v7, v7, 0x18

    shl-long/2addr v5, v7

    add-long/2addr v1, v5

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_0
    const-wide v4, 0xffffffffL

    and-long v0, v1, v4

    const-string v2, "US-ASCII"

    invoke-virtual {p1, v2}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object p1

    const/4 v2, 0x4

    invoke-virtual {p0, p1, v3, v2}, Ljava/io/DataOutputStream;->write([BII)V

    long-to-int v2, v0

    invoke-virtual {p0, v2}, Ljava/io/DataOutputStream;->writeInt(I)V

    long-to-int v2, p2

    invoke-virtual {p0, v2}, Ljava/io/DataOutputStream;->writeInt(I)V

    array-length v2, p4

    invoke-virtual {p0, v2}, Ljava/io/DataOutputStream;->writeInt(I)V

    invoke-static {p1}, Lorg/apache/fontbox/ttf/TTFSubsetter;->toUInt32([B)J

    move-result-wide p0

    add-long/2addr p0, v0

    add-long/2addr p0, v0

    add-long/2addr p0, p2

    array-length p2, p4

    int-to-long p2, p2

    add-long/2addr p0, p2

    return-wide p0
.end method

.method private static writeUint16(Ljava/io/DataOutputStream;I)V
    .locals 0

    invoke-virtual {p0, p1}, Ljava/io/DataOutputStream;->writeShort(I)V

    return-void
.end method

.method private static writeUint32(Ljava/io/DataOutputStream;J)V
    .locals 0

    long-to-int p1, p1

    invoke-virtual {p0, p1}, Ljava/io/DataOutputStream;->writeInt(I)V

    return-void
.end method

.method private static writeUint8(Ljava/io/DataOutputStream;I)V
    .locals 0

    invoke-virtual {p0, p1}, Ljava/io/DataOutputStream;->writeByte(I)V

    return-void
.end method


# virtual methods
.method public add(I)V
    .locals 3

    iget-object v0, p0, Lorg/apache/fontbox/ttf/TTFSubsetter;->unicodeCmap:Lorg/apache/fontbox/ttf/CmapSubtable;

    invoke-virtual {v0, p1}, Lorg/apache/fontbox/ttf/CmapSubtable;->getGlyphId(I)I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lorg/apache/fontbox/ttf/TTFSubsetter;->uniToGID:Ljava/util/SortedMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, p1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object p1, p0, Lorg/apache/fontbox/ttf/TTFSubsetter;->glyphIds:Ljava/util/SortedSet;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method public addAll(Ljava/util/Set;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p0, v0}, Lorg/apache/fontbox/ttf/TTFSubsetter;->add(I)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public getGIDMap()Ljava/util/Map;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    invoke-direct {p0}, Lorg/apache/fontbox/ttf/TTFSubsetter;->addCompoundReferences()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iget-object v1, p0, Lorg/apache/fontbox/ttf/TTFSubsetter;->glyphIds:Ljava/util/SortedSet;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    const/4 v2, 0x0

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v0, v4, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    return-object v0
.end method

.method public setPrefix(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lorg/apache/fontbox/ttf/TTFSubsetter;->prefix:Ljava/lang/String;

    return-void
.end method

.method public writeToStream(Ljava/io/OutputStream;)V
    .locals 12

    invoke-direct {p0}, Lorg/apache/fontbox/ttf/TTFSubsetter;->addCompoundReferences()V

    new-instance v0, Ljava/io/DataOutputStream;

    invoke-direct {v0, p1}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    :try_start_0
    iget-object p1, p0, Lorg/apache/fontbox/ttf/TTFSubsetter;->glyphIds:Ljava/util/SortedSet;

    invoke-interface {p1}, Ljava/util/Set;->size()I

    move-result p1

    add-int/lit8 p1, p1, 0x1

    new-array p1, p1, [J

    invoke-direct {p0}, Lorg/apache/fontbox/ttf/TTFSubsetter;->buildHeadTable()[B

    move-result-object v1

    invoke-direct {p0}, Lorg/apache/fontbox/ttf/TTFSubsetter;->buildHheaTable()[B

    move-result-object v2

    invoke-direct {p0}, Lorg/apache/fontbox/ttf/TTFSubsetter;->buildMaxpTable()[B

    move-result-object v3

    invoke-direct {p0}, Lorg/apache/fontbox/ttf/TTFSubsetter;->buildNameTable()[B

    move-result-object v4

    invoke-direct {p0}, Lorg/apache/fontbox/ttf/TTFSubsetter;->buildOS2Table()[B

    move-result-object v5

    invoke-direct {p0, p1}, Lorg/apache/fontbox/ttf/TTFSubsetter;->buildGlyfTable([J)[B

    move-result-object v6

    invoke-static {p1}, Lorg/apache/fontbox/ttf/TTFSubsetter;->buildLocaTable([J)[B

    move-result-object p1

    invoke-direct {p0}, Lorg/apache/fontbox/ttf/TTFSubsetter;->buildCmapTable()[B

    move-result-object v7

    invoke-direct {p0}, Lorg/apache/fontbox/ttf/TTFSubsetter;->buildHmtxTable()[B

    move-result-object v8

    invoke-direct {p0}, Lorg/apache/fontbox/ttf/TTFSubsetter;->buildPostTable()[B

    move-result-object v9

    new-instance v10, Ljava/util/TreeMap;

    invoke-direct {v10}, Ljava/util/TreeMap;-><init>()V

    if-eqz v5, :cond_0

    const-string v11, "OS/2"

    invoke-interface {v10, v11, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    if-eqz v7, :cond_1

    const-string v5, "cmap"

    invoke-interface {v10, v5, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    if-eqz v6, :cond_2

    const-string v5, "glyf"

    invoke-interface {v10, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_2
    const-string v5, "head"

    invoke-interface {v10, v5, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v5, "hhea"

    invoke-interface {v10, v5, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v2, "hmtx"

    invoke-interface {v10, v2, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    if-eqz p1, :cond_3

    const-string v2, "loca"

    invoke-interface {v10, v2, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_3
    const-string p1, "maxp"

    invoke-interface {v10, p1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    if-eqz v4, :cond_4

    const-string p1, "name"

    invoke-interface {v10, p1, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_4
    if-eqz v9, :cond_5

    const-string p1, "post"

    invoke-interface {v10, p1, v9}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_5
    iget-object p1, p0, Lorg/apache/fontbox/ttf/TTFSubsetter;->ttf:Lorg/apache/fontbox/ttf/TrueTypeFont;

    invoke-virtual {p1}, Lorg/apache/fontbox/ttf/TrueTypeFont;->getTableMap()Ljava/util/Map;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :cond_6
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lorg/apache/fontbox/ttf/TTFTable;

    invoke-interface {v10, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_6

    iget-object v4, p0, Lorg/apache/fontbox/ttf/TTFSubsetter;->keepTables:Ljava/util/List;

    if-eqz v4, :cond_7

    invoke-interface {v4, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    :cond_7
    iget-object v4, p0, Lorg/apache/fontbox/ttf/TTFSubsetter;->ttf:Lorg/apache/fontbox/ttf/TrueTypeFont;

    invoke-virtual {v4, v2}, Lorg/apache/fontbox/ttf/TrueTypeFont;->getTableBytes(Lorg/apache/fontbox/ttf/TTFTable;)[B

    move-result-object v2

    invoke-interface {v10, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_8
    invoke-interface {v10}, Ljava/util/Map;->size()I

    move-result p1

    invoke-static {v0, p1}, Lorg/apache/fontbox/ttf/TTFSubsetter;->writeFileHeader(Ljava/io/DataOutputStream;I)J

    move-result-wide v2

    invoke-interface {v10}, Ljava/util/Map;->size()I

    move-result p1

    int-to-long v4, p1

    const-wide/16 v6, 0x10

    mul-long/2addr v4, v6

    const-wide/16 v6, 0xc

    add-long/2addr v4, v6

    invoke-interface {v10}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_1
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_9

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/util/Map$Entry;

    invoke-interface {v6}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-interface {v6}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, [B

    invoke-static {v0, v7, v4, v5, v8}, Lorg/apache/fontbox/ttf/TTFSubsetter;->writeTableHeader(Ljava/io/DataOutputStream;Ljava/lang/String;J[B)J

    move-result-wide v7

    add-long/2addr v2, v7

    invoke-interface {v6}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, [B

    array-length v6, v6

    add-int/lit8 v6, v6, 0x3

    div-int/lit8 v6, v6, 0x4

    mul-int/lit8 v6, v6, 0x4

    int-to-long v6, v6

    add-long/2addr v4, v6

    goto :goto_1

    :cond_9
    const-wide v4, 0xffffffffL

    and-long/2addr v2, v4

    const-wide v4, 0xb1b0afbaL

    sub-long/2addr v4, v2

    const/16 p1, 0x18

    ushr-long v2, v4, p1

    long-to-int p1, v2

    int-to-byte p1, p1

    const/16 v2, 0x8

    aput-byte p1, v1, v2

    const/16 p1, 0x10

    ushr-long v6, v4, p1

    long-to-int p1, v6

    int-to-byte p1, p1

    const/16 v3, 0x9

    aput-byte p1, v1, v3

    ushr-long v2, v4, v2

    long-to-int p1, v2

    int-to-byte p1, p1

    const/16 v2, 0xa

    aput-byte p1, v1, v2

    long-to-int p1, v4

    int-to-byte p1, p1

    const/16 v2, 0xb

    aput-byte p1, v1, v2

    invoke-interface {v10}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object p1

    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_2
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_a

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [B

    invoke-static {v0, v1}, Lorg/apache/fontbox/ttf/TTFSubsetter;->writeTableBody(Ljava/io/OutputStream;[B)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_2

    :cond_a
    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V

    return-void

    :catchall_0
    move-exception p1

    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V

    throw p1
.end method

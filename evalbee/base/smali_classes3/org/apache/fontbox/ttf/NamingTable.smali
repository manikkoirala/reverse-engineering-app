.class public Lorg/apache/fontbox/ttf/NamingTable;
.super Lorg/apache/fontbox/ttf/TTFTable;
.source "SourceFile"


# static fields
.field public static final TAG:Ljava/lang/String; = "name"


# instance fields
.field private fontFamily:Ljava/lang/String;

.field private fontSubFamily:Ljava/lang/String;

.field private lookupTable:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;>;>;>;"
        }
    .end annotation
.end field

.field private nameRecords:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lorg/apache/fontbox/ttf/NameRecord;",
            ">;"
        }
    .end annotation
.end field

.field private psName:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lorg/apache/fontbox/ttf/TTFTable;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lorg/apache/fontbox/ttf/NamingTable;->nameRecords:Ljava/util/List;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/fontbox/ttf/NamingTable;->lookupTable:Ljava/util/Map;

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/fontbox/ttf/NamingTable;->fontFamily:Ljava/lang/String;

    iput-object v0, p0, Lorg/apache/fontbox/ttf/NamingTable;->fontSubFamily:Ljava/lang/String;

    iput-object v0, p0, Lorg/apache/fontbox/ttf/NamingTable;->psName:Ljava/lang/String;

    return-void
.end method

.method private getEnglishName(I)Ljava/lang/String;
    .locals 3

    const/4 v0, 0x3

    const/16 v1, 0x409

    const/4 v2, 0x1

    invoke-virtual {p0, p1, v0, v2, v1}, Lorg/apache/fontbox/ttf/NamingTable;->getName(IIII)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v2, v0, v0}, Lorg/apache/fontbox/ttf/NamingTable;->getName(IIII)Ljava/lang/String;

    move-result-object p1

    if-eqz p1, :cond_1

    return-object p1

    :cond_1
    const/4 p1, 0x0

    return-object p1
.end method


# virtual methods
.method public getFontFamily()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/apache/fontbox/ttf/NamingTable;->fontFamily:Ljava/lang/String;

    return-object v0
.end method

.method public getFontSubFamily()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/apache/fontbox/ttf/NamingTable;->fontSubFamily:Ljava/lang/String;

    return-object v0
.end method

.method public getName(IIII)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/apache/fontbox/ttf/NamingTable;->lookupTable:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Map;

    const/4 v0, 0x0

    if-nez p1, :cond_0

    return-object v0

    :cond_0
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-interface {p1, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Map;

    if-nez p1, :cond_1

    return-object v0

    :cond_1
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-interface {p1, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/util/Map;

    if-nez p1, :cond_2

    return-object v0

    :cond_2
    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-interface {p1, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/String;

    return-object p1
.end method

.method public getNameRecords()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List<",
            "Lorg/apache/fontbox/ttf/NameRecord;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/fontbox/ttf/NamingTable;->nameRecords:Ljava/util/List;

    return-object v0
.end method

.method public getPostScriptName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/apache/fontbox/ttf/NamingTable;->psName:Ljava/lang/String;

    return-object v0
.end method

.method public read(Lorg/apache/fontbox/ttf/TrueTypeFont;Lorg/apache/fontbox/ttf/TTFDataStream;)V
    .locals 11

    invoke-virtual {p2}, Lorg/apache/fontbox/ttf/TTFDataStream;->readUnsignedShort()I

    invoke-virtual {p2}, Lorg/apache/fontbox/ttf/TTFDataStream;->readUnsignedShort()I

    move-result v0

    invoke-virtual {p2}, Lorg/apache/fontbox/ttf/TTFDataStream;->readUnsignedShort()I

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, v0, :cond_0

    new-instance v3, Lorg/apache/fontbox/ttf/NameRecord;

    invoke-direct {v3}, Lorg/apache/fontbox/ttf/NameRecord;-><init>()V

    invoke-virtual {v3, p1, p2}, Lorg/apache/fontbox/ttf/NameRecord;->initData(Lorg/apache/fontbox/ttf/TrueTypeFont;Lorg/apache/fontbox/ttf/TTFDataStream;)V

    iget-object v4, p0, Lorg/apache/fontbox/ttf/NamingTable;->nameRecords:Ljava/util/List;

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    move p1, v1

    :goto_1
    const/4 v2, 0x3

    const/4 v3, 0x6

    const/4 v4, 0x2

    const/4 v5, 0x1

    if-ge p1, v0, :cond_6

    iget-object v6, p0, Lorg/apache/fontbox/ttf/NamingTable;->nameRecords:Ljava/util/List;

    invoke-interface {v6, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lorg/apache/fontbox/ttf/NameRecord;

    invoke-virtual {v6}, Lorg/apache/fontbox/ttf/NameRecord;->getStringOffset()I

    move-result v7

    int-to-long v7, v7

    invoke-virtual {p0}, Lorg/apache/fontbox/ttf/TTFTable;->getLength()J

    move-result-wide v9

    cmp-long v7, v7, v9

    if-lez v7, :cond_1

    const/4 v2, 0x0

    :goto_2
    invoke-virtual {v6, v2}, Lorg/apache/fontbox/ttf/NameRecord;->setString(Ljava/lang/String;)V

    goto :goto_4

    :cond_1
    invoke-virtual {p0}, Lorg/apache/fontbox/ttf/TTFTable;->getOffset()J

    move-result-wide v7

    const-wide/16 v9, 0x6

    add-long/2addr v7, v9

    mul-int/lit8 v9, v0, 0x2

    mul-int/2addr v9, v3

    int-to-long v9, v9

    add-long/2addr v7, v9

    invoke-virtual {v6}, Lorg/apache/fontbox/ttf/NameRecord;->getStringOffset()I

    move-result v3

    int-to-long v9, v3

    add-long/2addr v7, v9

    invoke-virtual {p2, v7, v8}, Lorg/apache/fontbox/ttf/TTFDataStream;->seek(J)V

    invoke-virtual {v6}, Lorg/apache/fontbox/ttf/NameRecord;->getPlatformId()I

    move-result v3

    invoke-virtual {v6}, Lorg/apache/fontbox/ttf/NameRecord;->getPlatformEncodingId()I

    move-result v7

    if-ne v3, v2, :cond_3

    if-eq v7, v5, :cond_2

    if-nez v7, :cond_3

    :cond_2
    const-string v2, "UTF-16"

    goto :goto_3

    :cond_3
    const-string v2, "ISO-8859-1"

    if-ne v3, v4, :cond_5

    if-nez v7, :cond_4

    const-string v2, "US-ASCII"

    goto :goto_3

    :cond_4
    if-ne v7, v5, :cond_5

    const-string v2, "ISO-10646-1"

    :cond_5
    :goto_3
    invoke-virtual {v6}, Lorg/apache/fontbox/ttf/NameRecord;->getStringLength()I

    move-result v3

    invoke-virtual {p2, v3, v2}, Lorg/apache/fontbox/ttf/TTFDataStream;->readString(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_2

    :goto_4
    add-int/lit8 p1, p1, 0x1

    goto :goto_1

    :cond_6
    iget-object p1, p0, Lorg/apache/fontbox/ttf/NamingTable;->nameRecords:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object p1

    :goto_5
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result p2

    if-eqz p2, :cond_a

    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object p2

    check-cast p2, Lorg/apache/fontbox/ttf/NameRecord;

    iget-object v0, p0, Lorg/apache/fontbox/ttf/NamingTable;->lookupTable:Ljava/util/Map;

    invoke-virtual {p2}, Lorg/apache/fontbox/ttf/NameRecord;->getNameId()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v0, v6}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    iget-object v0, p0, Lorg/apache/fontbox/ttf/NamingTable;->lookupTable:Ljava/util/Map;

    invoke-virtual {p2}, Lorg/apache/fontbox/ttf/NameRecord;->getNameId()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    new-instance v7, Ljava/util/HashMap;

    invoke-direct {v7}, Ljava/util/HashMap;-><init>()V

    invoke-interface {v0, v6, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_7
    iget-object v0, p0, Lorg/apache/fontbox/ttf/NamingTable;->lookupTable:Ljava/util/Map;

    invoke-virtual {p2}, Lorg/apache/fontbox/ttf/NameRecord;->getNameId()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v0, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    invoke-virtual {p2}, Lorg/apache/fontbox/ttf/NameRecord;->getPlatformId()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v0, v6}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_8

    invoke-virtual {p2}, Lorg/apache/fontbox/ttf/NameRecord;->getPlatformId()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    new-instance v7, Ljava/util/HashMap;

    invoke-direct {v7}, Ljava/util/HashMap;-><init>()V

    invoke-interface {v0, v6, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_8
    invoke-virtual {p2}, Lorg/apache/fontbox/ttf/NameRecord;->getPlatformId()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v0, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    invoke-virtual {p2}, Lorg/apache/fontbox/ttf/NameRecord;->getPlatformEncodingId()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v0, v6}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_9

    invoke-virtual {p2}, Lorg/apache/fontbox/ttf/NameRecord;->getPlatformEncodingId()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    new-instance v7, Ljava/util/HashMap;

    invoke-direct {v7}, Ljava/util/HashMap;-><init>()V

    invoke-interface {v0, v6, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_9
    invoke-virtual {p2}, Lorg/apache/fontbox/ttf/NameRecord;->getPlatformEncodingId()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v0, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    invoke-virtual {p2}, Lorg/apache/fontbox/ttf/NameRecord;->getLanguageId()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {p2}, Lorg/apache/fontbox/ttf/NameRecord;->getString()Ljava/lang/String;

    move-result-object p2

    invoke-interface {v0, v6, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_5

    :cond_a
    invoke-direct {p0, v5}, Lorg/apache/fontbox/ttf/NamingTable;->getEnglishName(I)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lorg/apache/fontbox/ttf/NamingTable;->fontFamily:Ljava/lang/String;

    invoke-direct {p0, v4}, Lorg/apache/fontbox/ttf/NamingTable;->getEnglishName(I)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lorg/apache/fontbox/ttf/NamingTable;->fontSubFamily:Ljava/lang/String;

    invoke-virtual {p0, v3, v5, v1, v1}, Lorg/apache/fontbox/ttf/NamingTable;->getName(IIII)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lorg/apache/fontbox/ttf/NamingTable;->psName:Ljava/lang/String;

    if-nez p1, :cond_b

    const/16 p1, 0x409

    invoke-virtual {p0, v3, v2, v5, p1}, Lorg/apache/fontbox/ttf/NamingTable;->getName(IIII)Ljava/lang/String;

    move-result-object p1

    iput-object p1, p0, Lorg/apache/fontbox/ttf/NamingTable;->psName:Ljava/lang/String;

    :cond_b
    iput-boolean v5, p0, Lorg/apache/fontbox/ttf/TTFTable;->initialized:Z

    return-void
.end method

.class public Lorg/apache/fontbox/ttf/HorizontalMetricsTable;
.super Lorg/apache/fontbox/ttf/TTFTable;
.source "SourceFile"


# static fields
.field public static final TAG:Ljava/lang/String; = "hmtx"


# instance fields
.field private advanceWidth:[I

.field private leftSideBearing:[S

.field private nonHorizontalLeftSideBearing:[S

.field private numHMetrics:I


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lorg/apache/fontbox/ttf/TTFTable;-><init>()V

    return-void
.end method


# virtual methods
.method public getAdvanceWidth(I)I
    .locals 1

    iget v0, p0, Lorg/apache/fontbox/ttf/HorizontalMetricsTable;->numHMetrics:I

    if-ge p1, v0, :cond_0

    iget-object v0, p0, Lorg/apache/fontbox/ttf/HorizontalMetricsTable;->advanceWidth:[I

    aget p1, v0, p1

    return p1

    :cond_0
    iget-object p1, p0, Lorg/apache/fontbox/ttf/HorizontalMetricsTable;->advanceWidth:[I

    array-length v0, p1

    add-int/lit8 v0, v0, -0x1

    aget p1, p1, v0

    return p1
.end method

.method public read(Lorg/apache/fontbox/ttf/TrueTypeFont;Lorg/apache/fontbox/ttf/TTFDataStream;)V
    .locals 7

    invoke-virtual {p1}, Lorg/apache/fontbox/ttf/TrueTypeFont;->getHorizontalHeader()Lorg/apache/fontbox/ttf/HorizontalHeaderTable;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/fontbox/ttf/HorizontalHeaderTable;->getNumberOfHMetrics()I

    move-result v0

    iput v0, p0, Lorg/apache/fontbox/ttf/HorizontalMetricsTable;->numHMetrics:I

    invoke-virtual {p1}, Lorg/apache/fontbox/ttf/TrueTypeFont;->getNumberOfGlyphs()I

    move-result p1

    iget v0, p0, Lorg/apache/fontbox/ttf/HorizontalMetricsTable;->numHMetrics:I

    new-array v1, v0, [I

    iput-object v1, p0, Lorg/apache/fontbox/ttf/HorizontalMetricsTable;->advanceWidth:[I

    new-array v0, v0, [S

    iput-object v0, p0, Lorg/apache/fontbox/ttf/HorizontalMetricsTable;->leftSideBearing:[S

    const/4 v0, 0x0

    move v1, v0

    move v2, v1

    :goto_0
    iget v3, p0, Lorg/apache/fontbox/ttf/HorizontalMetricsTable;->numHMetrics:I

    if-ge v1, v3, :cond_0

    iget-object v3, p0, Lorg/apache/fontbox/ttf/HorizontalMetricsTable;->advanceWidth:[I

    invoke-virtual {p2}, Lorg/apache/fontbox/ttf/TTFDataStream;->readUnsignedShort()I

    move-result v4

    aput v4, v3, v1

    iget-object v3, p0, Lorg/apache/fontbox/ttf/HorizontalMetricsTable;->leftSideBearing:[S

    invoke-virtual {p2}, Lorg/apache/fontbox/ttf/TTFDataStream;->readSignedShort()S

    move-result v4

    aput-short v4, v3, v1

    add-int/lit8 v2, v2, 0x4

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    int-to-long v3, v2

    invoke-virtual {p0}, Lorg/apache/fontbox/ttf/TTFTable;->getLength()J

    move-result-wide v5

    cmp-long v1, v3, v5

    if-gez v1, :cond_3

    iget v1, p0, Lorg/apache/fontbox/ttf/HorizontalMetricsTable;->numHMetrics:I

    sub-int v1, p1, v1

    if-gez v1, :cond_1

    goto :goto_1

    :cond_1
    move p1, v1

    :goto_1
    new-array v1, p1, [S

    iput-object v1, p0, Lorg/apache/fontbox/ttf/HorizontalMetricsTable;->nonHorizontalLeftSideBearing:[S

    :goto_2
    if-ge v0, p1, :cond_3

    int-to-long v3, v2

    invoke-virtual {p0}, Lorg/apache/fontbox/ttf/TTFTable;->getLength()J

    move-result-wide v5

    cmp-long v1, v3, v5

    if-gez v1, :cond_2

    iget-object v1, p0, Lorg/apache/fontbox/ttf/HorizontalMetricsTable;->nonHorizontalLeftSideBearing:[S

    invoke-virtual {p2}, Lorg/apache/fontbox/ttf/TTFDataStream;->readSignedShort()S

    move-result v3

    aput-short v3, v1, v0

    add-int/lit8 v2, v2, 0x2

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    const/4 p1, 0x1

    iput-boolean p1, p0, Lorg/apache/fontbox/ttf/TTFTable;->initialized:Z

    return-void
.end method

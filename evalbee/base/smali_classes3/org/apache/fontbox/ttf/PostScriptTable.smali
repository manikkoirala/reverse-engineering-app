.class public Lorg/apache/fontbox/ttf/PostScriptTable;
.super Lorg/apache/fontbox/ttf/TTFTable;
.source "SourceFile"


# static fields
.field public static final TAG:Ljava/lang/String; = "post"


# instance fields
.field private formatType:F

.field private glyphNames:[Ljava/lang/String;

.field private isFixedPitch:J

.field private italicAngle:F

.field private maxMemType1:J

.field private maxMemType42:J

.field private minMemType1:J

.field private minMemType42:J

.field private underlinePosition:S

.field private underlineThickness:S


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lorg/apache/fontbox/ttf/TTFTable;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/apache/fontbox/ttf/PostScriptTable;->glyphNames:[Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public getFormatType()F
    .locals 1

    iget v0, p0, Lorg/apache/fontbox/ttf/PostScriptTable;->formatType:F

    return v0
.end method

.method public getGlyphNames()[Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lorg/apache/fontbox/ttf/PostScriptTable;->glyphNames:[Ljava/lang/String;

    return-object v0
.end method

.method public getIsFixedPitch()J
    .locals 2

    iget-wide v0, p0, Lorg/apache/fontbox/ttf/PostScriptTable;->isFixedPitch:J

    return-wide v0
.end method

.method public getItalicAngle()F
    .locals 1

    iget v0, p0, Lorg/apache/fontbox/ttf/PostScriptTable;->italicAngle:F

    return v0
.end method

.method public getMaxMemType1()J
    .locals 2

    iget-wide v0, p0, Lorg/apache/fontbox/ttf/PostScriptTable;->maxMemType1:J

    return-wide v0
.end method

.method public getMaxMemType42()J
    .locals 2

    iget-wide v0, p0, Lorg/apache/fontbox/ttf/PostScriptTable;->maxMemType42:J

    return-wide v0
.end method

.method public getMinMemType1()J
    .locals 2

    iget-wide v0, p0, Lorg/apache/fontbox/ttf/PostScriptTable;->minMemType1:J

    return-wide v0
.end method

.method public getMinMemType42()J
    .locals 2

    iget-wide v0, p0, Lorg/apache/fontbox/ttf/PostScriptTable;->minMemType42:J

    return-wide v0
.end method

.method public getName(I)Ljava/lang/String;
    .locals 2

    if-ltz p1, :cond_1

    iget-object v0, p0, Lorg/apache/fontbox/ttf/PostScriptTable;->glyphNames:[Ljava/lang/String;

    if-eqz v0, :cond_1

    array-length v1, v0

    if-le p1, v1, :cond_0

    goto :goto_0

    :cond_0
    aget-object p1, v0, p1

    return-object p1

    :cond_1
    :goto_0
    const/4 p1, 0x0

    return-object p1
.end method

.method public getUnderlinePosition()S
    .locals 1

    iget-short v0, p0, Lorg/apache/fontbox/ttf/PostScriptTable;->underlinePosition:S

    return v0
.end method

.method public getUnderlineThickness()S
    .locals 1

    iget-short v0, p0, Lorg/apache/fontbox/ttf/PostScriptTable;->underlineThickness:S

    return v0
.end method

.method public read(Lorg/apache/fontbox/ttf/TrueTypeFont;Lorg/apache/fontbox/ttf/TTFDataStream;)V
    .locals 9

    invoke-virtual {p2}, Lorg/apache/fontbox/ttf/TTFDataStream;->read32Fixed()F

    move-result v0

    iput v0, p0, Lorg/apache/fontbox/ttf/PostScriptTable;->formatType:F

    invoke-virtual {p2}, Lorg/apache/fontbox/ttf/TTFDataStream;->read32Fixed()F

    move-result v0

    iput v0, p0, Lorg/apache/fontbox/ttf/PostScriptTable;->italicAngle:F

    invoke-virtual {p2}, Lorg/apache/fontbox/ttf/TTFDataStream;->readSignedShort()S

    move-result v0

    iput-short v0, p0, Lorg/apache/fontbox/ttf/PostScriptTable;->underlinePosition:S

    invoke-virtual {p2}, Lorg/apache/fontbox/ttf/TTFDataStream;->readSignedShort()S

    move-result v0

    iput-short v0, p0, Lorg/apache/fontbox/ttf/PostScriptTable;->underlineThickness:S

    invoke-virtual {p2}, Lorg/apache/fontbox/ttf/TTFDataStream;->readUnsignedInt()J

    move-result-wide v0

    iput-wide v0, p0, Lorg/apache/fontbox/ttf/PostScriptTable;->isFixedPitch:J

    invoke-virtual {p2}, Lorg/apache/fontbox/ttf/TTFDataStream;->readUnsignedInt()J

    move-result-wide v0

    iput-wide v0, p0, Lorg/apache/fontbox/ttf/PostScriptTable;->minMemType42:J

    invoke-virtual {p2}, Lorg/apache/fontbox/ttf/TTFDataStream;->readUnsignedInt()J

    move-result-wide v0

    iput-wide v0, p0, Lorg/apache/fontbox/ttf/PostScriptTable;->maxMemType42:J

    invoke-virtual {p2}, Lorg/apache/fontbox/ttf/TTFDataStream;->readUnsignedInt()J

    move-result-wide v0

    iput-wide v0, p0, Lorg/apache/fontbox/ttf/PostScriptTable;->minMemType1:J

    invoke-virtual {p2}, Lorg/apache/fontbox/ttf/TTFDataStream;->readUnsignedInt()J

    move-result-wide v0

    iput-wide v0, p0, Lorg/apache/fontbox/ttf/PostScriptTable;->maxMemType1:J

    iget v0, p0, Lorg/apache/fontbox/ttf/PostScriptTable;->formatType:F

    const/high16 v1, 0x3f800000    # 1.0f

    cmpl-float v1, v0, v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/16 v4, 0x102

    if-nez v1, :cond_0

    new-array p1, v4, [Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/fontbox/ttf/PostScriptTable;->glyphNames:[Ljava/lang/String;

    sget-object p2, Lorg/apache/fontbox/ttf/WGL4Names;->MAC_GLYPH_NAMES:[Ljava/lang/String;

    invoke-static {p2, v3, p1, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto/16 :goto_6

    :cond_0
    const/high16 v1, 0x40000000    # 2.0f

    cmpl-float v1, v0, v1

    if-nez v1, :cond_7

    invoke-virtual {p2}, Lorg/apache/fontbox/ttf/TTFDataStream;->readUnsignedShort()I

    move-result p1

    new-array v0, p1, [I

    new-array v1, p1, [Ljava/lang/String;

    iput-object v1, p0, Lorg/apache/fontbox/ttf/PostScriptTable;->glyphNames:[Ljava/lang/String;

    const/high16 v1, -0x80000000

    move v5, v3

    :goto_0
    const/16 v6, 0x7fff

    if-ge v5, p1, :cond_2

    invoke-virtual {p2}, Lorg/apache/fontbox/ttf/TTFDataStream;->readUnsignedShort()I

    move-result v7

    aput v7, v0, v5

    if-gt v7, v6, :cond_1

    invoke-static {v1, v7}, Ljava/lang/Math;->max(II)I

    move-result v1

    :cond_1
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    :cond_2
    if-lt v1, v4, :cond_3

    sub-int/2addr v1, v4

    add-int/2addr v1, v2

    new-array v5, v1, [Ljava/lang/String;

    move v7, v3

    :goto_1
    if-ge v7, v1, :cond_4

    invoke-virtual {p2}, Lorg/apache/fontbox/ttf/TTFDataStream;->readUnsignedByte()I

    move-result v8

    invoke-virtual {p2, v8}, Lorg/apache/fontbox/ttf/TTFDataStream;->readString(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v5, v7

    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    :cond_3
    const/4 v5, 0x0

    :cond_4
    :goto_2
    if-ge v3, p1, :cond_a

    aget p2, v0, v3

    if-ge p2, v4, :cond_5

    iget-object v1, p0, Lorg/apache/fontbox/ttf/PostScriptTable;->glyphNames:[Ljava/lang/String;

    sget-object v7, Lorg/apache/fontbox/ttf/WGL4Names;->MAC_GLYPH_NAMES:[Ljava/lang/String;

    aget-object p2, v7, p2

    aput-object p2, v1, v3

    goto :goto_3

    :cond_5
    if-lt p2, v4, :cond_6

    if-gt p2, v6, :cond_6

    iget-object v1, p0, Lorg/apache/fontbox/ttf/PostScriptTable;->glyphNames:[Ljava/lang/String;

    add-int/lit16 p2, p2, -0x102

    aget-object p2, v5, p2

    aput-object p2, v1, v3

    goto :goto_3

    :cond_6
    iget-object p2, p0, Lorg/apache/fontbox/ttf/PostScriptTable;->glyphNames:[Ljava/lang/String;

    const-string v1, ".undefined"

    aput-object v1, p2, v3

    :goto_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_7
    const/high16 v1, 0x40200000    # 2.5f

    cmpl-float v0, v0, v1

    if-nez v0, :cond_a

    invoke-virtual {p1}, Lorg/apache/fontbox/ttf/TrueTypeFont;->getNumberOfGlyphs()I

    move-result p1

    new-array v0, p1, [I

    move v1, v3

    :goto_4
    if-ge v1, p1, :cond_8

    invoke-virtual {p2}, Lorg/apache/fontbox/ttf/TTFDataStream;->readSignedByte()I

    move-result v4

    add-int/lit8 v5, v1, 0x1

    add-int/2addr v4, v5

    aput v4, v0, v1

    move v1, v5

    goto :goto_4

    :cond_8
    new-array p1, p1, [Ljava/lang/String;

    iput-object p1, p0, Lorg/apache/fontbox/ttf/PostScriptTable;->glyphNames:[Ljava/lang/String;

    :goto_5
    iget-object p1, p0, Lorg/apache/fontbox/ttf/PostScriptTable;->glyphNames:[Ljava/lang/String;

    array-length p2, p1

    if-ge v3, p2, :cond_a

    sget-object p2, Lorg/apache/fontbox/ttf/WGL4Names;->MAC_GLYPH_NAMES:[Ljava/lang/String;

    aget v1, v0, v3

    aget-object p2, p2, v1

    if-eqz p2, :cond_9

    aput-object p2, p1, v3

    :cond_9
    add-int/lit8 v3, v3, 0x1

    goto :goto_5

    :cond_a
    :goto_6
    iput-boolean v2, p0, Lorg/apache/fontbox/ttf/TTFTable;->initialized:Z

    return-void
.end method

.method public setFormatType(F)V
    .locals 0

    iput p1, p0, Lorg/apache/fontbox/ttf/PostScriptTable;->formatType:F

    return-void
.end method

.method public setGlyphNames([Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lorg/apache/fontbox/ttf/PostScriptTable;->glyphNames:[Ljava/lang/String;

    return-void
.end method

.method public setIsFixedPitch(J)V
    .locals 0

    iput-wide p1, p0, Lorg/apache/fontbox/ttf/PostScriptTable;->isFixedPitch:J

    return-void
.end method

.method public setItalicAngle(F)V
    .locals 0

    iput p1, p0, Lorg/apache/fontbox/ttf/PostScriptTable;->italicAngle:F

    return-void
.end method

.method public setMaxMemType1(J)V
    .locals 0

    iput-wide p1, p0, Lorg/apache/fontbox/ttf/PostScriptTable;->maxMemType1:J

    return-void
.end method

.method public setMaxMemType42(J)V
    .locals 0

    iput-wide p1, p0, Lorg/apache/fontbox/ttf/PostScriptTable;->maxMemType42:J

    return-void
.end method

.method public setMimMemType1(J)V
    .locals 0

    iput-wide p1, p0, Lorg/apache/fontbox/ttf/PostScriptTable;->minMemType1:J

    return-void
.end method

.method public setMinMemType42(J)V
    .locals 0

    iput-wide p1, p0, Lorg/apache/fontbox/ttf/PostScriptTable;->minMemType42:J

    return-void
.end method

.method public setUnderlinePosition(S)V
    .locals 0

    iput-short p1, p0, Lorg/apache/fontbox/ttf/PostScriptTable;->underlinePosition:S

    return-void
.end method

.method public setUnderlineThickness(S)V
    .locals 0

    iput-short p1, p0, Lorg/apache/fontbox/ttf/PostScriptTable;->underlineThickness:S

    return-void
.end method

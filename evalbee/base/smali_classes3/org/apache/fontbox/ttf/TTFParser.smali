.class public Lorg/apache/fontbox/ttf/TTFParser;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private isEmbedded:Z

.field private parseOnDemandOnly:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .line 1
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lorg/apache/fontbox/ttf/TTFParser;-><init>(Z)V

    return-void
.end method

.method public constructor <init>(Z)V
    .locals 1

    .line 2
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lorg/apache/fontbox/ttf/TTFParser;-><init>(ZZ)V

    return-void
.end method

.method public constructor <init>(ZZ)V
    .locals 0

    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean p1, p0, Lorg/apache/fontbox/ttf/TTFParser;->isEmbedded:Z

    iput-boolean p2, p0, Lorg/apache/fontbox/ttf/TTFParser;->parseOnDemandOnly:Z

    return-void
.end method

.method private parseTables(Lorg/apache/fontbox/ttf/TrueTypeFont;Lorg/apache/fontbox/ttf/TTFDataStream;)V
    .locals 2

    invoke-virtual {p1}, Lorg/apache/fontbox/ttf/TrueTypeFont;->getTables()Ljava/util/Collection;

    move-result-object p2

    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object p2

    :cond_0
    :goto_0
    invoke-interface {p2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {p2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/fontbox/ttf/TTFTable;

    invoke-virtual {v0}, Lorg/apache/fontbox/ttf/TTFTable;->getInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p1, v0}, Lorg/apache/fontbox/ttf/TrueTypeFont;->readTable(Lorg/apache/fontbox/ttf/TTFTable;)V

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Lorg/apache/fontbox/ttf/TrueTypeFont;->getHeader()Lorg/apache/fontbox/ttf/HeaderTable;

    move-result-object p2

    if-eqz p2, :cond_d

    invoke-virtual {p1}, Lorg/apache/fontbox/ttf/TrueTypeFont;->getHorizontalHeader()Lorg/apache/fontbox/ttf/HorizontalHeaderTable;

    move-result-object p2

    if-eqz p2, :cond_c

    invoke-virtual {p1}, Lorg/apache/fontbox/ttf/TrueTypeFont;->getMaximumProfile()Lorg/apache/fontbox/ttf/MaximumProfileTable;

    move-result-object p2

    if-eqz p2, :cond_b

    invoke-virtual {p1}, Lorg/apache/fontbox/ttf/TrueTypeFont;->getPostScript()Lorg/apache/fontbox/ttf/PostScriptTable;

    move-result-object p2

    if-nez p2, :cond_3

    iget-boolean p2, p0, Lorg/apache/fontbox/ttf/TTFParser;->isEmbedded:Z

    if-eqz p2, :cond_2

    goto :goto_1

    :cond_2
    new-instance p1, Ljava/io/IOException;

    const-string p2, "post is mandatory"

    invoke-direct {p1, p2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_3
    :goto_1
    invoke-virtual {p1}, Lorg/apache/fontbox/ttf/TrueTypeFont;->getIndexToLocation()Lorg/apache/fontbox/ttf/IndexToLocationTable;

    move-result-object p2

    if-eqz p2, :cond_a

    invoke-virtual {p1}, Lorg/apache/fontbox/ttf/TrueTypeFont;->getGlyph()Lorg/apache/fontbox/ttf/GlyphTable;

    move-result-object p2

    if-eqz p2, :cond_9

    invoke-virtual {p1}, Lorg/apache/fontbox/ttf/TrueTypeFont;->getNaming()Lorg/apache/fontbox/ttf/NamingTable;

    move-result-object p2

    if-nez p2, :cond_5

    iget-boolean p2, p0, Lorg/apache/fontbox/ttf/TTFParser;->isEmbedded:Z

    if-eqz p2, :cond_4

    goto :goto_2

    :cond_4
    new-instance p1, Ljava/io/IOException;

    const-string p2, "name is mandatory"

    invoke-direct {p1, p2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_5
    :goto_2
    invoke-virtual {p1}, Lorg/apache/fontbox/ttf/TrueTypeFont;->getHorizontalMetrics()Lorg/apache/fontbox/ttf/HorizontalMetricsTable;

    move-result-object p2

    if-eqz p2, :cond_8

    iget-boolean p2, p0, Lorg/apache/fontbox/ttf/TTFParser;->isEmbedded:Z

    if-nez p2, :cond_7

    invoke-virtual {p1}, Lorg/apache/fontbox/ttf/TrueTypeFont;->getCmap()Lorg/apache/fontbox/ttf/CmapTable;

    move-result-object p1

    if-eqz p1, :cond_6

    goto :goto_3

    :cond_6
    new-instance p1, Ljava/io/IOException;

    const-string p2, "cmap is mandatory"

    invoke-direct {p1, p2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_7
    :goto_3
    return-void

    :cond_8
    new-instance p1, Ljava/io/IOException;

    const-string p2, "hmtx is mandatory"

    invoke-direct {p1, p2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_9
    new-instance p1, Ljava/io/IOException;

    const-string p2, "glyf is mandatory"

    invoke-direct {p1, p2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_a
    new-instance p1, Ljava/io/IOException;

    const-string p2, "loca is mandatory"

    invoke-direct {p1, p2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_b
    new-instance p1, Ljava/io/IOException;

    const-string p2, "maxp is mandatory"

    invoke-direct {p1, p2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_c
    new-instance p1, Ljava/io/IOException;

    const-string p2, "hhead is mandatory"

    invoke-direct {p1, p2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_d
    new-instance p1, Ljava/io/IOException;

    const-string p2, "head is mandatory"

    invoke-direct {p1, p2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method private readTableDirectory(Lorg/apache/fontbox/ttf/TTFDataStream;)Lorg/apache/fontbox/ttf/TTFTable;
    .locals 4

    const/4 v0, 0x4

    invoke-virtual {p1, v0}, Lorg/apache/fontbox/ttf/TTFDataStream;->readString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "cmap"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Lorg/apache/fontbox/ttf/CmapTable;

    invoke-direct {v1}, Lorg/apache/fontbox/ttf/CmapTable;-><init>()V

    goto/16 :goto_0

    :cond_0
    const-string v1, "glyf"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance v1, Lorg/apache/fontbox/ttf/GlyphTable;

    invoke-direct {v1}, Lorg/apache/fontbox/ttf/GlyphTable;-><init>()V

    goto/16 :goto_0

    :cond_1
    const-string v1, "head"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    new-instance v1, Lorg/apache/fontbox/ttf/HeaderTable;

    invoke-direct {v1}, Lorg/apache/fontbox/ttf/HeaderTable;-><init>()V

    goto/16 :goto_0

    :cond_2
    const-string v1, "hhea"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    new-instance v1, Lorg/apache/fontbox/ttf/HorizontalHeaderTable;

    invoke-direct {v1}, Lorg/apache/fontbox/ttf/HorizontalHeaderTable;-><init>()V

    goto :goto_0

    :cond_3
    const-string v1, "hmtx"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    new-instance v1, Lorg/apache/fontbox/ttf/HorizontalMetricsTable;

    invoke-direct {v1}, Lorg/apache/fontbox/ttf/HorizontalMetricsTable;-><init>()V

    goto :goto_0

    :cond_4
    const-string v1, "loca"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    new-instance v1, Lorg/apache/fontbox/ttf/IndexToLocationTable;

    invoke-direct {v1}, Lorg/apache/fontbox/ttf/IndexToLocationTable;-><init>()V

    goto :goto_0

    :cond_5
    const-string v1, "maxp"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    new-instance v1, Lorg/apache/fontbox/ttf/MaximumProfileTable;

    invoke-direct {v1}, Lorg/apache/fontbox/ttf/MaximumProfileTable;-><init>()V

    goto :goto_0

    :cond_6
    const-string v1, "name"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    new-instance v1, Lorg/apache/fontbox/ttf/NamingTable;

    invoke-direct {v1}, Lorg/apache/fontbox/ttf/NamingTable;-><init>()V

    goto :goto_0

    :cond_7
    const-string v1, "OS/2"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    new-instance v1, Lorg/apache/fontbox/ttf/OS2WindowsMetricsTable;

    invoke-direct {v1}, Lorg/apache/fontbox/ttf/OS2WindowsMetricsTable;-><init>()V

    goto :goto_0

    :cond_8
    const-string v1, "post"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    new-instance v1, Lorg/apache/fontbox/ttf/PostScriptTable;

    invoke-direct {v1}, Lorg/apache/fontbox/ttf/PostScriptTable;-><init>()V

    goto :goto_0

    :cond_9
    const-string v1, "DSIG"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    new-instance v1, Lorg/apache/fontbox/ttf/DigitalSignatureTable;

    invoke-direct {v1}, Lorg/apache/fontbox/ttf/DigitalSignatureTable;-><init>()V

    goto :goto_0

    :cond_a
    invoke-virtual {p0, v0}, Lorg/apache/fontbox/ttf/TTFParser;->readTable(Ljava/lang/String;)Lorg/apache/fontbox/ttf/TTFTable;

    move-result-object v1

    :goto_0
    invoke-virtual {v1, v0}, Lorg/apache/fontbox/ttf/TTFTable;->setTag(Ljava/lang/String;)V

    invoke-virtual {p1}, Lorg/apache/fontbox/ttf/TTFDataStream;->readUnsignedInt()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lorg/apache/fontbox/ttf/TTFTable;->setCheckSum(J)V

    invoke-virtual {p1}, Lorg/apache/fontbox/ttf/TTFDataStream;->readUnsignedInt()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lorg/apache/fontbox/ttf/TTFTable;->setOffset(J)V

    invoke-virtual {p1}, Lorg/apache/fontbox/ttf/TTFDataStream;->readUnsignedInt()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lorg/apache/fontbox/ttf/TTFTable;->setLength(J)V

    return-object v1
.end method


# virtual methods
.method public newFont(Lorg/apache/fontbox/ttf/TTFDataStream;)Lorg/apache/fontbox/ttf/TrueTypeFont;
    .locals 1

    new-instance v0, Lorg/apache/fontbox/ttf/TrueTypeFont;

    invoke-direct {v0, p1}, Lorg/apache/fontbox/ttf/TrueTypeFont;-><init>(Lorg/apache/fontbox/ttf/TTFDataStream;)V

    return-object v0
.end method

.method public parse(Ljava/io/File;)Lorg/apache/fontbox/ttf/TrueTypeFont;
    .locals 2

    .line 1
    new-instance v0, Lorg/apache/fontbox/ttf/RAFDataStream;

    const-string v1, "r"

    invoke-direct {v0, p1, v1}, Lorg/apache/fontbox/ttf/RAFDataStream;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lorg/apache/fontbox/ttf/TTFParser;->parse(Lorg/apache/fontbox/ttf/TTFDataStream;)Lorg/apache/fontbox/ttf/TrueTypeFont;

    move-result-object p1

    return-object p1
.end method

.method public parse(Ljava/io/InputStream;)Lorg/apache/fontbox/ttf/TrueTypeFont;
    .locals 1

    .line 2
    new-instance v0, Lorg/apache/fontbox/ttf/MemoryTTFDataStream;

    invoke-direct {v0, p1}, Lorg/apache/fontbox/ttf/MemoryTTFDataStream;-><init>(Ljava/io/InputStream;)V

    invoke-virtual {p0, v0}, Lorg/apache/fontbox/ttf/TTFParser;->parse(Lorg/apache/fontbox/ttf/TTFDataStream;)Lorg/apache/fontbox/ttf/TrueTypeFont;

    move-result-object p1

    return-object p1
.end method

.method public parse(Ljava/lang/String;)Lorg/apache/fontbox/ttf/TrueTypeFont;
    .locals 1

    .line 3
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lorg/apache/fontbox/ttf/TTFParser;->parse(Ljava/io/File;)Lorg/apache/fontbox/ttf/TrueTypeFont;

    move-result-object p1

    return-object p1
.end method

.method public parse(Lorg/apache/fontbox/ttf/TTFDataStream;)Lorg/apache/fontbox/ttf/TrueTypeFont;
    .locals 4

    .line 4
    invoke-virtual {p0, p1}, Lorg/apache/fontbox/ttf/TTFParser;->newFont(Lorg/apache/fontbox/ttf/TTFDataStream;)Lorg/apache/fontbox/ttf/TrueTypeFont;

    move-result-object v0

    invoke-virtual {p1}, Lorg/apache/fontbox/ttf/TTFDataStream;->read32Fixed()F

    move-result v1

    invoke-virtual {v0, v1}, Lorg/apache/fontbox/ttf/TrueTypeFont;->setVersion(F)V

    invoke-virtual {p1}, Lorg/apache/fontbox/ttf/TTFDataStream;->readUnsignedShort()I

    move-result v1

    invoke-virtual {p1}, Lorg/apache/fontbox/ttf/TTFDataStream;->readUnsignedShort()I

    invoke-virtual {p1}, Lorg/apache/fontbox/ttf/TTFDataStream;->readUnsignedShort()I

    invoke-virtual {p1}, Lorg/apache/fontbox/ttf/TTFDataStream;->readUnsignedShort()I

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    invoke-direct {p0, p1}, Lorg/apache/fontbox/ttf/TTFParser;->readTableDirectory(Lorg/apache/fontbox/ttf/TTFDataStream;)Lorg/apache/fontbox/ttf/TTFTable;

    move-result-object v3

    invoke-virtual {v0, v3}, Lorg/apache/fontbox/ttf/TrueTypeFont;->addTable(Lorg/apache/fontbox/ttf/TTFTable;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    iget-boolean v1, p0, Lorg/apache/fontbox/ttf/TTFParser;->parseOnDemandOnly:Z

    if-nez v1, :cond_1

    invoke-direct {p0, v0, p1}, Lorg/apache/fontbox/ttf/TTFParser;->parseTables(Lorg/apache/fontbox/ttf/TrueTypeFont;Lorg/apache/fontbox/ttf/TTFDataStream;)V

    :cond_1
    return-object v0
.end method

.method public parseEmbedded(Ljava/io/InputStream;)Lorg/apache/fontbox/ttf/TrueTypeFont;
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lorg/apache/fontbox/ttf/TTFParser;->isEmbedded:Z

    new-instance v0, Lorg/apache/fontbox/ttf/MemoryTTFDataStream;

    invoke-direct {v0, p1}, Lorg/apache/fontbox/ttf/MemoryTTFDataStream;-><init>(Ljava/io/InputStream;)V

    invoke-virtual {p0, v0}, Lorg/apache/fontbox/ttf/TTFParser;->parse(Lorg/apache/fontbox/ttf/TTFDataStream;)Lorg/apache/fontbox/ttf/TrueTypeFont;

    move-result-object p1

    return-object p1
.end method

.method public readTable(Ljava/lang/String;)Lorg/apache/fontbox/ttf/TTFTable;
    .locals 0

    new-instance p1, Lorg/apache/fontbox/ttf/TTFTable;

    invoke-direct {p1}, Lorg/apache/fontbox/ttf/TTFTable;-><init>()V

    return-object p1
.end method

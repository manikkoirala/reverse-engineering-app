.class public Lorg/apache/fontbox/ttf/CmapSubtable;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/fontbox/ttf/CmapSubtable$SubHeader;
    }
.end annotation


# static fields
.field private static final LEAD_OFFSET:J = 0xd7c0L

.field private static final SURROGATE_OFFSET:J = -0x35fdc00L


# instance fields
.field private characterCodeToGlyphId:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private glyphIdToCharacterCode:[I

.field private platformEncodingId:I

.field private platformId:I

.field private subTableOffset:J


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/fontbox/ttf/CmapSubtable;->characterCodeToGlyphId:Ljava/util/Map;

    return-void
.end method

.method private newGlyphIdToCharacterCode(I)[I
    .locals 1

    new-array p1, p1, [I

    const/4 v0, -0x1

    invoke-static {p1, v0}, Ljava/util/Arrays;->fill([II)V

    return-object p1
.end method


# virtual methods
.method public getCharacterCode(I)Ljava/lang/Integer;
    .locals 3

    const/4 v0, 0x0

    if-ltz p1, :cond_2

    iget-object v1, p0, Lorg/apache/fontbox/ttf/CmapSubtable;->glyphIdToCharacterCode:[I

    array-length v2, v1

    if-lt p1, v2, :cond_0

    goto :goto_0

    :cond_0
    aget p1, v1, p1

    const/4 v1, -0x1

    if-ne p1, v1, :cond_1

    return-object v0

    :cond_1
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    return-object p1

    :cond_2
    :goto_0
    return-object v0
.end method

.method public getGlyphId(I)I
    .locals 1

    iget-object v0, p0, Lorg/apache/fontbox/ttf/CmapSubtable;->characterCodeToGlyphId:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Ljava/lang/Integer;

    if-nez p1, :cond_0

    const/4 p1, 0x0

    goto :goto_0

    :cond_0
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    :goto_0
    return p1
.end method

.method public getPlatformEncodingId()I
    .locals 1

    iget v0, p0, Lorg/apache/fontbox/ttf/CmapSubtable;->platformEncodingId:I

    return v0
.end method

.method public getPlatformId()I
    .locals 1

    iget v0, p0, Lorg/apache/fontbox/ttf/CmapSubtable;->platformId:I

    return v0
.end method

.method public initData(Lorg/apache/fontbox/ttf/TTFDataStream;)V
    .locals 2

    invoke-virtual {p1}, Lorg/apache/fontbox/ttf/TTFDataStream;->readUnsignedShort()I

    move-result v0

    iput v0, p0, Lorg/apache/fontbox/ttf/CmapSubtable;->platformId:I

    invoke-virtual {p1}, Lorg/apache/fontbox/ttf/TTFDataStream;->readUnsignedShort()I

    move-result v0

    iput v0, p0, Lorg/apache/fontbox/ttf/CmapSubtable;->platformEncodingId:I

    invoke-virtual {p1}, Lorg/apache/fontbox/ttf/TTFDataStream;->readUnsignedInt()J

    move-result-wide v0

    iput-wide v0, p0, Lorg/apache/fontbox/ttf/CmapSubtable;->subTableOffset:J

    return-void
.end method

.method public initSubtable(Lorg/apache/fontbox/ttf/CmapTable;ILorg/apache/fontbox/ttf/TTFDataStream;)V
    .locals 4

    invoke-virtual {p1}, Lorg/apache/fontbox/ttf/TTFTable;->getOffset()J

    move-result-wide v0

    iget-wide v2, p0, Lorg/apache/fontbox/ttf/CmapSubtable;->subTableOffset:J

    add-long/2addr v0, v2

    invoke-virtual {p3, v0, v1}, Lorg/apache/fontbox/ttf/TTFDataStream;->seek(J)V

    invoke-virtual {p3}, Lorg/apache/fontbox/ttf/TTFDataStream;->readUnsignedShort()I

    move-result p1

    const/16 v0, 0x8

    invoke-virtual {p3}, Lorg/apache/fontbox/ttf/TTFDataStream;->readUnsignedShort()I

    if-ge p1, v0, :cond_0

    invoke-virtual {p3}, Lorg/apache/fontbox/ttf/TTFDataStream;->readUnsignedShort()I

    goto :goto_0

    :cond_0
    invoke-virtual {p3}, Lorg/apache/fontbox/ttf/TTFDataStream;->readUnsignedInt()J

    invoke-virtual {p3}, Lorg/apache/fontbox/ttf/TTFDataStream;->readUnsignedInt()J

    :goto_0
    if-eqz p1, :cond_6

    const/4 v1, 0x2

    if-eq p1, v1, :cond_5

    const/4 v1, 0x4

    if-eq p1, v1, :cond_4

    const/4 v1, 0x6

    if-eq p1, v1, :cond_3

    if-eq p1, v0, :cond_2

    const/16 v0, 0xa

    if-eq p1, v0, :cond_1

    packed-switch p1, :pswitch_data_0

    new-instance p2, Ljava/io/IOException;

    new-instance p3, Ljava/lang/StringBuilder;

    invoke-direct {p3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Unknown cmap format:"

    invoke-virtual {p3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {p3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {p2, p1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p2

    :pswitch_0
    invoke-virtual {p0, p3, p2}, Lorg/apache/fontbox/ttf/CmapSubtable;->processSubtype14(Lorg/apache/fontbox/ttf/TTFDataStream;I)V

    goto :goto_1

    :pswitch_1
    invoke-virtual {p0, p3, p2}, Lorg/apache/fontbox/ttf/CmapSubtable;->processSubtype13(Lorg/apache/fontbox/ttf/TTFDataStream;I)V

    goto :goto_1

    :pswitch_2
    invoke-virtual {p0, p3, p2}, Lorg/apache/fontbox/ttf/CmapSubtable;->processSubtype12(Lorg/apache/fontbox/ttf/TTFDataStream;I)V

    goto :goto_1

    :cond_1
    invoke-virtual {p0, p3, p2}, Lorg/apache/fontbox/ttf/CmapSubtable;->processSubtype10(Lorg/apache/fontbox/ttf/TTFDataStream;I)V

    goto :goto_1

    :cond_2
    invoke-virtual {p0, p3, p2}, Lorg/apache/fontbox/ttf/CmapSubtable;->processSubtype8(Lorg/apache/fontbox/ttf/TTFDataStream;I)V

    goto :goto_1

    :cond_3
    invoke-virtual {p0, p3, p2}, Lorg/apache/fontbox/ttf/CmapSubtable;->processSubtype6(Lorg/apache/fontbox/ttf/TTFDataStream;I)V

    goto :goto_1

    :cond_4
    invoke-virtual {p0, p3, p2}, Lorg/apache/fontbox/ttf/CmapSubtable;->processSubtype4(Lorg/apache/fontbox/ttf/TTFDataStream;I)V

    goto :goto_1

    :cond_5
    invoke-virtual {p0, p3, p2}, Lorg/apache/fontbox/ttf/CmapSubtable;->processSubtype2(Lorg/apache/fontbox/ttf/TTFDataStream;I)V

    goto :goto_1

    :cond_6
    invoke-virtual {p0, p3}, Lorg/apache/fontbox/ttf/CmapSubtable;->processSubtype0(Lorg/apache/fontbox/ttf/TTFDataStream;)V

    :goto_1
    return-void

    :pswitch_data_0
    .packed-switch 0xc
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public processSubtype0(Lorg/apache/fontbox/ttf/TTFDataStream;)V
    .locals 5

    const/16 v0, 0x100

    invoke-virtual {p1, v0}, Lorg/apache/fontbox/ttf/TTFDataStream;->read(I)[B

    move-result-object p1

    invoke-direct {p0, v0}, Lorg/apache/fontbox/ttf/CmapSubtable;->newGlyphIdToCharacterCode(I)[I

    move-result-object v1

    iput-object v1, p0, Lorg/apache/fontbox/ttf/CmapSubtable;->glyphIdToCharacterCode:[I

    const/4 v1, 0x0

    :goto_0
    array-length v2, p1

    if-ge v1, v2, :cond_0

    aget-byte v2, p1, v1

    add-int/2addr v2, v0

    rem-int/2addr v2, v0

    iget-object v3, p0, Lorg/apache/fontbox/ttf/CmapSubtable;->glyphIdToCharacterCode:[I

    aput v1, v3, v2

    iget-object v3, p0, Lorg/apache/fontbox/ttf/CmapSubtable;->characterCodeToGlyphId:Ljava/util/Map;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v3, v4, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public processSubtype10(Lorg/apache/fontbox/ttf/TTFDataStream;I)V
    .locals 5

    invoke-virtual {p1}, Lorg/apache/fontbox/ttf/TTFDataStream;->readUnsignedInt()J

    move-result-wide v0

    invoke-virtual {p1}, Lorg/apache/fontbox/ttf/TTFDataStream;->readUnsignedInt()J

    move-result-wide p1

    const-wide/32 v2, 0x7fffffff

    cmp-long v2, p1, v2

    if-gtz v2, :cond_2

    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-ltz v2, :cond_1

    const-wide/32 v2, 0x10ffff

    cmp-long v4, v0, v2

    if-gtz v4, :cond_1

    add-long/2addr v0, p1

    cmp-long p1, v0, v2

    if-gtz p1, :cond_1

    const-wide/32 p1, 0xd800

    cmp-long p1, v0, p1

    if-ltz p1, :cond_0

    const-wide/32 p1, 0xdfff

    cmp-long p1, v0, p1

    if-lez p1, :cond_1

    :cond_0
    return-void

    :cond_1
    new-instance p1, Ljava/io/IOException;

    const-string p2, "Invalid Characters codes"

    invoke-direct {p1, p2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_2
    new-instance p1, Ljava/io/IOException;

    const-string p2, "Invalid number of Characters"

    invoke-direct {p1, p2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public processSubtype12(Lorg/apache/fontbox/ttf/TTFDataStream;I)V
    .locals 26

    move-object/from16 v0, p0

    move/from16 v1, p2

    invoke-virtual/range {p1 .. p1}, Lorg/apache/fontbox/ttf/TTFDataStream;->readUnsignedInt()J

    move-result-wide v2

    invoke-direct {v0, v1}, Lorg/apache/fontbox/ttf/CmapSubtable;->newGlyphIdToCharacterCode(I)[I

    move-result-object v4

    iput-object v4, v0, Lorg/apache/fontbox/ttf/CmapSubtable;->glyphIdToCharacterCode:[I

    const-wide/16 v4, 0x0

    move-wide v6, v4

    :goto_0
    cmp-long v8, v6, v2

    if-gez v8, :cond_8

    invoke-virtual/range {p1 .. p1}, Lorg/apache/fontbox/ttf/TTFDataStream;->readUnsignedInt()J

    move-result-wide v8

    invoke-virtual/range {p1 .. p1}, Lorg/apache/fontbox/ttf/TTFDataStream;->readUnsignedInt()J

    move-result-wide v10

    invoke-virtual/range {p1 .. p1}, Lorg/apache/fontbox/ttf/TTFDataStream;->readUnsignedInt()J

    move-result-wide v12

    cmp-long v14, v8, v4

    const-string v15, "Invalid characters codes"

    if-ltz v14, :cond_7

    const-wide/32 v16, 0x10ffff

    cmp-long v14, v8, v16

    if-gtz v14, :cond_7

    const-wide/32 v18, 0xd800

    cmp-long v14, v8, v18

    const-wide/32 v20, 0xdfff

    if-ltz v14, :cond_0

    cmp-long v14, v8, v20

    if-lez v14, :cond_7

    :cond_0
    cmp-long v14, v10, v4

    if-lez v14, :cond_1

    cmp-long v14, v10, v8

    if-ltz v14, :cond_6

    :cond_1
    cmp-long v14, v10, v16

    if-gtz v14, :cond_6

    cmp-long v14, v10, v18

    if-ltz v14, :cond_2

    cmp-long v14, v10, v20

    if-lez v14, :cond_6

    :cond_2
    move-wide v14, v4

    :goto_1
    sub-long v18, v10, v8

    cmp-long v18, v14, v18

    const-wide/16 v19, 0x1

    if-gtz v18, :cond_5

    add-long v4, v12, v14

    move-wide/from16 v22, v2

    int-to-long v2, v1

    cmp-long v2, v4, v2

    if-gez v2, :cond_4

    add-long v2, v8, v14

    cmp-long v18, v2, v16

    if-lez v18, :cond_3

    const-string v1, "PdfBoxAndroid"

    move-wide/from16 v24, v8

    const-string v8, "Format 12 cmap contains character beyond UCS-4"

    invoke-static {v1, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    :cond_3
    move-wide/from16 v24, v8

    :goto_2
    iget-object v1, v0, Lorg/apache/fontbox/ttf/CmapSubtable;->glyphIdToCharacterCode:[I

    long-to-int v4, v4

    long-to-int v2, v2

    aput v2, v1, v4

    iget-object v1, v0, Lorg/apache/fontbox/ttf/CmapSubtable;->characterCodeToGlyphId:Ljava/util/Map;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-long v14, v14, v19

    move/from16 v1, p2

    move-wide/from16 v2, v22

    move-wide/from16 v8, v24

    const-wide/16 v4, 0x0

    goto :goto_1

    :cond_4
    new-instance v1, Ljava/io/IOException;

    const-string v2, "Character Code greater than Integer.MAX_VALUE"

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_5
    move-wide/from16 v22, v2

    add-long v6, v6, v19

    move/from16 v1, p2

    const-wide/16 v4, 0x0

    goto/16 :goto_0

    :cond_6
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1, v15}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_7
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1, v15}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_8
    return-void
.end method

.method public processSubtype13(Lorg/apache/fontbox/ttf/TTFDataStream;I)V
    .locals 26

    move-object/from16 v0, p0

    invoke-virtual/range {p1 .. p1}, Lorg/apache/fontbox/ttf/TTFDataStream;->readUnsignedInt()J

    move-result-wide v1

    const-wide/16 v3, 0x0

    move-wide v5, v3

    :goto_0
    cmp-long v7, v5, v1

    if-gez v7, :cond_9

    invoke-virtual/range {p1 .. p1}, Lorg/apache/fontbox/ttf/TTFDataStream;->readUnsignedInt()J

    move-result-wide v7

    invoke-virtual/range {p1 .. p1}, Lorg/apache/fontbox/ttf/TTFDataStream;->readUnsignedInt()J

    move-result-wide v9

    invoke-virtual/range {p1 .. p1}, Lorg/apache/fontbox/ttf/TTFDataStream;->readUnsignedInt()J

    move-result-wide v11

    move/from16 v13, p2

    int-to-long v14, v13

    cmp-long v14, v11, v14

    const-string v15, "PdfBoxAndroid"

    if-lez v14, :cond_0

    const-string v1, "Format 13 cmap contains an invalid glyph index"

    invoke-static {v15, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    :cond_0
    cmp-long v14, v7, v3

    const-string v3, "Invalid Characters codes"

    if-ltz v14, :cond_8

    const-wide/32 v18, 0x10ffff

    cmp-long v4, v7, v18

    if-gtz v4, :cond_8

    const-wide/32 v20, 0xd800

    cmp-long v4, v7, v20

    const-wide/32 v22, 0xdfff

    if-ltz v4, :cond_1

    cmp-long v4, v7, v22

    if-lez v4, :cond_8

    :cond_1
    const-wide/16 v16, 0x0

    cmp-long v4, v9, v16

    if-lez v4, :cond_2

    cmp-long v4, v9, v7

    if-ltz v4, :cond_7

    :cond_2
    cmp-long v4, v9, v18

    if-gtz v4, :cond_7

    cmp-long v4, v9, v20

    if-ltz v4, :cond_3

    cmp-long v4, v9, v22

    if-lez v4, :cond_7

    :cond_3
    move-wide/from16 v3, v16

    :goto_1
    sub-long v20, v9, v7

    cmp-long v14, v3, v20

    const-wide/16 v20, 0x1

    move-wide/from16 v22, v1

    if-gtz v14, :cond_6

    add-long v1, v7, v3

    const-wide/32 v24, 0x7fffffff

    cmp-long v14, v1, v24

    if-gtz v14, :cond_5

    cmp-long v14, v1, v18

    if-lez v14, :cond_4

    const-string v14, "Format 13 cmap contains character beyond UCS-4"

    invoke-static {v15, v14}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    iget-object v14, v0, Lorg/apache/fontbox/ttf/CmapSubtable;->glyphIdToCharacterCode:[I

    move-wide/from16 v24, v7

    long-to-int v7, v11

    long-to-int v1, v1

    aput v1, v14, v7

    iget-object v2, v0, Lorg/apache/fontbox/ttf/CmapSubtable;->characterCodeToGlyphId:Ljava/util/Map;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v2, v1, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-long v3, v3, v20

    move-wide/from16 v1, v22

    move-wide/from16 v7, v24

    goto :goto_1

    :cond_5
    new-instance v1, Ljava/io/IOException;

    const-string v2, "Character Code greater than Integer.MAX_VALUE"

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_6
    add-long v5, v5, v20

    move-wide/from16 v3, v16

    goto/16 :goto_0

    :cond_7
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_8
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_9
    :goto_2
    return-void
.end method

.method public processSubtype14(Lorg/apache/fontbox/ttf/TTFDataStream;I)V
    .locals 0

    const-string p1, "PdfBoxAndroid"

    const-string p2, "Format 14 cmap table is not supported and will be ignored"

    invoke-static {p1, p2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public processSubtype2(Lorg/apache/fontbox/ttf/TTFDataStream;I)V
    .locals 13

    const/16 v0, 0x100

    new-array v1, v0, [I

    const/4 v2, 0x0

    move v3, v2

    move v4, v3

    :goto_0
    if-ge v3, v0, :cond_0

    invoke-virtual {p1}, Lorg/apache/fontbox/ttf/TTFDataStream;->readUnsignedShort()I

    move-result v5

    aput v5, v1, v3

    div-int/lit8 v5, v5, 0x8

    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    move-result v4

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_0
    add-int/lit8 v0, v4, 0x1

    new-array v1, v0, [Lorg/apache/fontbox/ttf/CmapSubtable$SubHeader;

    move v3, v2

    :goto_1
    if-gt v3, v4, :cond_1

    invoke-virtual {p1}, Lorg/apache/fontbox/ttf/TTFDataStream;->readUnsignedShort()I

    move-result v7

    invoke-virtual {p1}, Lorg/apache/fontbox/ttf/TTFDataStream;->readUnsignedShort()I

    move-result v8

    invoke-virtual {p1}, Lorg/apache/fontbox/ttf/TTFDataStream;->readSignedShort()S

    move-result v9

    invoke-virtual {p1}, Lorg/apache/fontbox/ttf/TTFDataStream;->readUnsignedShort()I

    move-result v5

    sub-int v6, v0, v3

    add-int/lit8 v6, v6, -0x1

    mul-int/lit8 v6, v6, 0x8

    sub-int/2addr v5, v6

    add-int/lit8 v10, v5, -0x2

    new-instance v12, Lorg/apache/fontbox/ttf/CmapSubtable$SubHeader;

    const/4 v11, 0x0

    move-object v5, v12

    move-object v6, p0

    invoke-direct/range {v5 .. v11}, Lorg/apache/fontbox/ttf/CmapSubtable$SubHeader;-><init>(Lorg/apache/fontbox/ttf/CmapSubtable;IISILorg/apache/fontbox/ttf/CmapSubtable$1;)V

    aput-object v12, v1, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_1
    invoke-virtual {p1}, Lorg/apache/fontbox/ttf/TTFDataStream;->getCurrentPosition()J

    move-result-wide v5

    invoke-direct {p0, p2}, Lorg/apache/fontbox/ttf/CmapSubtable;->newGlyphIdToCharacterCode(I)[I

    move-result-object p2

    iput-object p2, p0, Lorg/apache/fontbox/ttf/CmapSubtable;->glyphIdToCharacterCode:[I

    move p2, v2

    :goto_2
    if-gt p2, v4, :cond_4

    aget-object v0, v1, p2

    invoke-static {v0}, Lorg/apache/fontbox/ttf/CmapSubtable$SubHeader;->access$100(Lorg/apache/fontbox/ttf/CmapSubtable$SubHeader;)I

    move-result v3

    invoke-static {v0}, Lorg/apache/fontbox/ttf/CmapSubtable$SubHeader;->access$200(Lorg/apache/fontbox/ttf/CmapSubtable$SubHeader;)I

    move-result v7

    invoke-static {v0}, Lorg/apache/fontbox/ttf/CmapSubtable$SubHeader;->access$300(Lorg/apache/fontbox/ttf/CmapSubtable$SubHeader;)S

    move-result v8

    invoke-static {v0}, Lorg/apache/fontbox/ttf/CmapSubtable$SubHeader;->access$400(Lorg/apache/fontbox/ttf/CmapSubtable$SubHeader;)I

    move-result v0

    int-to-long v9, v7

    add-long/2addr v9, v5

    invoke-virtual {p1, v9, v10}, Lorg/apache/fontbox/ttf/TTFDataStream;->seek(J)V

    move v7, v2

    :goto_3
    if-ge v7, v0, :cond_3

    shl-int/lit8 v9, p2, 0x8

    add-int v10, v3, v7

    add-int/2addr v9, v10

    invoke-virtual {p1}, Lorg/apache/fontbox/ttf/TTFDataStream;->readUnsignedShort()I

    move-result v10

    if-lez v10, :cond_2

    add-int/2addr v10, v8

    const/high16 v11, 0x10000

    rem-int/2addr v10, v11

    :cond_2
    iget-object v11, p0, Lorg/apache/fontbox/ttf/CmapSubtable;->glyphIdToCharacterCode:[I

    aput v9, v11, v10

    iget-object v11, p0, Lorg/apache/fontbox/ttf/CmapSubtable;->characterCodeToGlyphId:Ljava/util/Map;

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-interface {v11, v9, v10}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v7, v7, 0x1

    goto :goto_3

    :cond_3
    add-int/lit8 p2, p2, 0x1

    goto :goto_2

    :cond_4
    return-void
.end method

.method public processSubtype4(Lorg/apache/fontbox/ttf/TTFDataStream;I)V
    .locals 20

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual/range {p1 .. p1}, Lorg/apache/fontbox/ttf/TTFDataStream;->readUnsignedShort()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    invoke-virtual/range {p1 .. p1}, Lorg/apache/fontbox/ttf/TTFDataStream;->readUnsignedShort()I

    invoke-virtual/range {p1 .. p1}, Lorg/apache/fontbox/ttf/TTFDataStream;->readUnsignedShort()I

    invoke-virtual/range {p1 .. p1}, Lorg/apache/fontbox/ttf/TTFDataStream;->readUnsignedShort()I

    invoke-virtual {v1, v2}, Lorg/apache/fontbox/ttf/TTFDataStream;->readUnsignedShortArray(I)[I

    move-result-object v3

    invoke-virtual/range {p1 .. p1}, Lorg/apache/fontbox/ttf/TTFDataStream;->readUnsignedShort()I

    invoke-virtual {v1, v2}, Lorg/apache/fontbox/ttf/TTFDataStream;->readUnsignedShortArray(I)[I

    move-result-object v4

    invoke-virtual {v1, v2}, Lorg/apache/fontbox/ttf/TTFDataStream;->readUnsignedShortArray(I)[I

    move-result-object v5

    invoke-virtual {v1, v2}, Lorg/apache/fontbox/ttf/TTFDataStream;->readUnsignedShortArray(I)[I

    move-result-object v6

    new-instance v7, Ljava/util/HashMap;

    invoke-direct {v7}, Ljava/util/HashMap;-><init>()V

    invoke-virtual/range {p1 .. p1}, Lorg/apache/fontbox/ttf/TTFDataStream;->getCurrentPosition()J

    move-result-wide v8

    const/4 v10, 0x0

    :goto_0
    if-ge v10, v2, :cond_3

    aget v11, v4, v10

    aget v12, v3, v10

    aget v13, v5, v10

    aget v14, v6, v10

    const v15, 0xffff

    if-eq v11, v15, :cond_2

    if-eq v12, v15, :cond_2

    move v15, v11

    :goto_1
    if-gt v15, v12, :cond_2

    const/high16 v16, 0x10000

    if-nez v14, :cond_0

    add-int v17, v15, v13

    rem-int v17, v17, v16

    move-object/from16 p2, v3

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object/from16 v18, v4

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v7, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v3, v0, Lorg/apache/fontbox/ttf/CmapSubtable;->characterCodeToGlyphId:Ljava/util/Map;

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v19, v5

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v3, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    :cond_0
    move-object/from16 p2, v3

    move-object/from16 v18, v4

    move-object/from16 v19, v5

    div-int/lit8 v3, v14, 0x2

    sub-int v4, v15, v11

    add-int/2addr v3, v4

    sub-int v4, v10, v2

    add-int/2addr v3, v4

    mul-int/lit8 v3, v3, 0x2

    int-to-long v3, v3

    add-long/2addr v3, v8

    invoke-virtual {v1, v3, v4}, Lorg/apache/fontbox/ttf/TTFDataStream;->seek(J)V

    invoke-virtual/range {p1 .. p1}, Lorg/apache/fontbox/ttf/TTFDataStream;->readUnsignedShort()I

    move-result v3

    if-eqz v3, :cond_1

    add-int/2addr v3, v13

    rem-int v3, v3, v16

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v7, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v7, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v4, v0, Lorg/apache/fontbox/ttf/CmapSubtable;->characterCodeToGlyphId:Ljava/util/Map;

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v4, v5, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    :goto_2
    add-int/lit8 v15, v15, 0x1

    move-object/from16 v3, p2

    move-object/from16 v4, v18

    move-object/from16 v5, v19

    goto :goto_1

    :cond_2
    move-object/from16 p2, v3

    move-object/from16 v18, v4

    move-object/from16 v19, v5

    add-int/lit8 v10, v10, 0x1

    move-object/from16 v3, p2

    move-object/from16 v4, v18

    move-object/from16 v5, v19

    goto/16 :goto_0

    :cond_3
    invoke-interface {v7}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_4

    const-string v1, "PdfBoxAndroid"

    const-string v2, "cmap format 4 subtable is empty"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_4
    invoke-interface {v7}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Collections;->max(Ljava/util/Collection;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-direct {v0, v1}, Lorg/apache/fontbox/ttf/CmapSubtable;->newGlyphIdToCharacterCode(I)[I

    move-result-object v1

    iput-object v1, v0, Lorg/apache/fontbox/ttf/CmapSubtable;->glyphIdToCharacterCode:[I

    invoke-interface {v7}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    iget-object v3, v0, Lorg/apache/fontbox/ttf/CmapSubtable;->glyphIdToCharacterCode:[I

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    aput v2, v3, v4

    goto :goto_3

    :cond_5
    return-void
.end method

.method public processSubtype6(Lorg/apache/fontbox/ttf/TTFDataStream;I)V
    .locals 5

    invoke-virtual {p1}, Lorg/apache/fontbox/ttf/TTFDataStream;->readUnsignedShort()I

    move-result v0

    invoke-virtual {p1}, Lorg/apache/fontbox/ttf/TTFDataStream;->readUnsignedShort()I

    move-result v1

    invoke-direct {p0, p2}, Lorg/apache/fontbox/ttf/CmapSubtable;->newGlyphIdToCharacterCode(I)[I

    move-result-object p2

    iput-object p2, p0, Lorg/apache/fontbox/ttf/CmapSubtable;->glyphIdToCharacterCode:[I

    invoke-virtual {p1, v1}, Lorg/apache/fontbox/ttf/TTFDataStream;->readUnsignedShortArray(I)[I

    move-result-object p1

    const/4 p2, 0x0

    :goto_0
    if-ge p2, v1, :cond_0

    iget-object v2, p0, Lorg/apache/fontbox/ttf/CmapSubtable;->glyphIdToCharacterCode:[I

    aget v3, p1, p2

    add-int v4, v0, p2

    aput v4, v2, v3

    iget-object v2, p0, Lorg/apache/fontbox/ttf/CmapSubtable;->characterCodeToGlyphId:Ljava/util/Map;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aget v4, p1, p2

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 p2, p2, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public processSubtype8(Lorg/apache/fontbox/ttf/TTFDataStream;I)V
    .locals 28

    move-object/from16 v0, p0

    move/from16 v1, p2

    const/16 v2, 0x2000

    move-object/from16 v3, p1

    invoke-virtual {v3, v2}, Lorg/apache/fontbox/ttf/TTFDataStream;->readUnsignedByteArray(I)[I

    move-result-object v2

    invoke-virtual/range {p1 .. p1}, Lorg/apache/fontbox/ttf/TTFDataStream;->readUnsignedInt()J

    move-result-wide v4

    const-wide/32 v6, 0x10000

    cmp-long v6, v4, v6

    if-gtz v6, :cond_7

    invoke-direct {v0, v1}, Lorg/apache/fontbox/ttf/CmapSubtable;->newGlyphIdToCharacterCode(I)[I

    move-result-object v6

    iput-object v6, v0, Lorg/apache/fontbox/ttf/CmapSubtable;->glyphIdToCharacterCode:[I

    const-wide/16 v6, 0x0

    move-wide v8, v6

    :goto_0
    cmp-long v10, v8, v4

    if-gez v10, :cond_6

    invoke-virtual/range {p1 .. p1}, Lorg/apache/fontbox/ttf/TTFDataStream;->readUnsignedInt()J

    move-result-wide v10

    invoke-virtual/range {p1 .. p1}, Lorg/apache/fontbox/ttf/TTFDataStream;->readUnsignedInt()J

    move-result-wide v12

    invoke-virtual/range {p1 .. p1}, Lorg/apache/fontbox/ttf/TTFDataStream;->readUnsignedInt()J

    move-result-wide v14

    cmp-long v16, v10, v12

    if-gtz v16, :cond_5

    cmp-long v16, v6, v10

    if-gtz v16, :cond_5

    move-wide v6, v10

    :goto_1
    cmp-long v17, v6, v12

    const-wide/16 v18, 0x1

    if-gtz v17, :cond_4

    const-wide/32 v20, 0x7fffffff

    cmp-long v17, v6, v20

    const-string v3, "[Sub Format 8] Invalid Character code"

    if-gtz v17, :cond_3

    move-wide/from16 v22, v4

    long-to-int v4, v6

    div-int/lit8 v5, v4, 0x8

    aget v5, v2, v5

    const/16 v17, 0x1

    rem-int/lit8 v24, v4, 0x8

    shl-int v17, v17, v24

    and-int v5, v5, v17

    if-nez v5, :cond_0

    goto :goto_2

    :cond_0
    const-wide/32 v4, 0xd7c0

    const/16 v17, 0xa

    shr-long v24, v6, v17

    add-long v24, v24, v4

    const-wide/16 v4, 0x3ff

    and-long/2addr v4, v6

    const-wide/32 v26, 0xdc00

    add-long v4, v4, v26

    shl-long v24, v24, v17

    add-long v24, v24, v4

    const-wide/32 v4, -0x35fdc00

    add-long v4, v24, v4

    cmp-long v17, v4, v20

    if-gtz v17, :cond_2

    long-to-int v4, v4

    :goto_2
    sub-long v24, v6, v10

    move-object v5, v2

    add-long v2, v14, v24

    move-wide/from16 v24, v10

    int-to-long v10, v1

    cmp-long v10, v2, v10

    if-gtz v10, :cond_1

    cmp-long v10, v2, v20

    if-gtz v10, :cond_1

    iget-object v10, v0, Lorg/apache/fontbox/ttf/CmapSubtable;->glyphIdToCharacterCode:[I

    long-to-int v2, v2

    aput v4, v10, v2

    iget-object v3, v0, Lorg/apache/fontbox/ttf/CmapSubtable;->characterCodeToGlyphId:Ljava/util/Map;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v3, v4, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-long v6, v6, v18

    move-object/from16 v3, p1

    move-object v2, v5

    move-wide/from16 v4, v22

    move-wide/from16 v10, v24

    goto :goto_1

    :cond_1
    new-instance v1, Ljava/io/IOException;

    const-string v2, "CMap contains an invalid glyph index"

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_2
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_3
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_4
    move-wide/from16 v22, v4

    move-object v5, v2

    add-long v8, v8, v18

    move-object/from16 v3, p1

    move-wide/from16 v4, v22

    const-wide/16 v6, 0x0

    goto/16 :goto_0

    :cond_5
    new-instance v1, Ljava/io/IOException;

    const-string v2, "Range invalid"

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_6
    return-void

    :cond_7
    new-instance v1, Ljava/io/IOException;

    const-string v2, "CMap ( Subtype8 ) is invalid"

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public setPlatformEncodingId(I)V
    .locals 0

    iput p1, p0, Lorg/apache/fontbox/ttf/CmapSubtable;->platformEncodingId:I

    return-void
.end method

.method public setPlatformId(I)V
    .locals 0

    iput p1, p0, Lorg/apache/fontbox/ttf/CmapSubtable;->platformId:I

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/apache/fontbox/ttf/CmapSubtable;->getPlatformId()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lorg/apache/fontbox/ttf/CmapSubtable;->getPlatformEncodingId()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

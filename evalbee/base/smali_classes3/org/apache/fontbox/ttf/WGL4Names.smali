.class public final Lorg/apache/fontbox/ttf/WGL4Names;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final MAC_GLYPH_NAMES:[Ljava/lang/String;

.field public static final MAC_GLYPH_NAMES_INDICES:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public static final NUMBER_OF_MAC_GLYPHS:I = 0x102


# direct methods
.method public static constructor <clinit>()V
    .locals 5

    const/16 v0, 0x102

    new-array v1, v0, [Ljava/lang/String;

    const-string v2, ".notdef"

    const/4 v3, 0x0

    aput-object v2, v1, v3

    const/4 v2, 0x1

    const-string v4, ".null"

    aput-object v4, v1, v2

    const/4 v2, 0x2

    const-string v4, "nonmarkingreturn"

    aput-object v4, v1, v2

    const/4 v2, 0x3

    const-string v4, "space"

    aput-object v4, v1, v2

    const/4 v2, 0x4

    const-string v4, "exclam"

    aput-object v4, v1, v2

    const/4 v2, 0x5

    const-string v4, "quotedbl"

    aput-object v4, v1, v2

    const/4 v2, 0x6

    const-string v4, "numbersign"

    aput-object v4, v1, v2

    const/4 v2, 0x7

    const-string v4, "dollar"

    aput-object v4, v1, v2

    const/16 v2, 0x8

    const-string v4, "percent"

    aput-object v4, v1, v2

    const/16 v2, 0x9

    const-string v4, "ampersand"

    aput-object v4, v1, v2

    const/16 v2, 0xa

    const-string v4, "quotesingle"

    aput-object v4, v1, v2

    const/16 v2, 0xb

    const-string v4, "parenleft"

    aput-object v4, v1, v2

    const/16 v2, 0xc

    const-string v4, "parenright"

    aput-object v4, v1, v2

    const/16 v2, 0xd

    const-string v4, "asterisk"

    aput-object v4, v1, v2

    const/16 v2, 0xe

    const-string v4, "plus"

    aput-object v4, v1, v2

    const/16 v2, 0xf

    const-string v4, "comma"

    aput-object v4, v1, v2

    const/16 v2, 0x10

    const-string v4, "hyphen"

    aput-object v4, v1, v2

    const/16 v2, 0x11

    const-string v4, "period"

    aput-object v4, v1, v2

    const/16 v2, 0x12

    const-string v4, "slash"

    aput-object v4, v1, v2

    const/16 v2, 0x13

    const-string v4, "zero"

    aput-object v4, v1, v2

    const/16 v2, 0x14

    const-string v4, "one"

    aput-object v4, v1, v2

    const/16 v2, 0x15

    const-string v4, "two"

    aput-object v4, v1, v2

    const/16 v2, 0x16

    const-string v4, "three"

    aput-object v4, v1, v2

    const/16 v2, 0x17

    const-string v4, "four"

    aput-object v4, v1, v2

    const/16 v2, 0x18

    const-string v4, "five"

    aput-object v4, v1, v2

    const/16 v2, 0x19

    const-string v4, "six"

    aput-object v4, v1, v2

    const/16 v2, 0x1a

    const-string v4, "seven"

    aput-object v4, v1, v2

    const/16 v2, 0x1b

    const-string v4, "eight"

    aput-object v4, v1, v2

    const/16 v2, 0x1c

    const-string v4, "nine"

    aput-object v4, v1, v2

    const/16 v2, 0x1d

    const-string v4, "colon"

    aput-object v4, v1, v2

    const/16 v2, 0x1e

    const-string v4, "semicolon"

    aput-object v4, v1, v2

    const/16 v2, 0x1f

    const-string v4, "less"

    aput-object v4, v1, v2

    const/16 v2, 0x20

    const-string v4, "equal"

    aput-object v4, v1, v2

    const/16 v2, 0x21

    const-string v4, "greater"

    aput-object v4, v1, v2

    const/16 v2, 0x22

    const-string v4, "question"

    aput-object v4, v1, v2

    const/16 v2, 0x23

    const-string v4, "at"

    aput-object v4, v1, v2

    const/16 v2, 0x24

    const-string v4, "A"

    aput-object v4, v1, v2

    const/16 v2, 0x25

    const-string v4, "B"

    aput-object v4, v1, v2

    const/16 v2, 0x26

    const-string v4, "C"

    aput-object v4, v1, v2

    const/16 v2, 0x27

    const-string v4, "D"

    aput-object v4, v1, v2

    const/16 v2, 0x28

    const-string v4, "E"

    aput-object v4, v1, v2

    const/16 v2, 0x29

    const-string v4, "F"

    aput-object v4, v1, v2

    const/16 v2, 0x2a

    const-string v4, "G"

    aput-object v4, v1, v2

    const/16 v2, 0x2b

    const-string v4, "H"

    aput-object v4, v1, v2

    const/16 v2, 0x2c

    const-string v4, "I"

    aput-object v4, v1, v2

    const/16 v2, 0x2d

    const-string v4, "J"

    aput-object v4, v1, v2

    const/16 v2, 0x2e

    const-string v4, "K"

    aput-object v4, v1, v2

    const/16 v2, 0x2f

    const-string v4, "L"

    aput-object v4, v1, v2

    const/16 v2, 0x30

    const-string v4, "M"

    aput-object v4, v1, v2

    const/16 v2, 0x31

    const-string v4, "N"

    aput-object v4, v1, v2

    const/16 v2, 0x32

    const-string v4, "O"

    aput-object v4, v1, v2

    const/16 v2, 0x33

    const-string v4, "P"

    aput-object v4, v1, v2

    const/16 v2, 0x34

    const-string v4, "Q"

    aput-object v4, v1, v2

    const/16 v2, 0x35

    const-string v4, "R"

    aput-object v4, v1, v2

    const/16 v2, 0x36

    const-string v4, "S"

    aput-object v4, v1, v2

    const/16 v2, 0x37

    const-string v4, "T"

    aput-object v4, v1, v2

    const/16 v2, 0x38

    const-string v4, "U"

    aput-object v4, v1, v2

    const/16 v2, 0x39

    const-string v4, "V"

    aput-object v4, v1, v2

    const/16 v2, 0x3a

    const-string v4, "W"

    aput-object v4, v1, v2

    const/16 v2, 0x3b

    const-string v4, "X"

    aput-object v4, v1, v2

    const/16 v2, 0x3c

    const-string v4, "Y"

    aput-object v4, v1, v2

    const/16 v2, 0x3d

    const-string v4, "Z"

    aput-object v4, v1, v2

    const/16 v2, 0x3e

    const-string v4, "bracketleft"

    aput-object v4, v1, v2

    const/16 v2, 0x3f

    const-string v4, "backslash"

    aput-object v4, v1, v2

    const/16 v2, 0x40

    const-string v4, "bracketright"

    aput-object v4, v1, v2

    const/16 v2, 0x41

    const-string v4, "asciicircum"

    aput-object v4, v1, v2

    const/16 v2, 0x42

    const-string v4, "underscore"

    aput-object v4, v1, v2

    const/16 v2, 0x43

    const-string v4, "grave"

    aput-object v4, v1, v2

    const/16 v2, 0x44

    const-string v4, "a"

    aput-object v4, v1, v2

    const/16 v2, 0x45

    const-string v4, "b"

    aput-object v4, v1, v2

    const/16 v2, 0x46

    const-string v4, "c"

    aput-object v4, v1, v2

    const/16 v2, 0x47

    const-string v4, "d"

    aput-object v4, v1, v2

    const/16 v2, 0x48

    const-string v4, "e"

    aput-object v4, v1, v2

    const/16 v2, 0x49

    const-string v4, "f"

    aput-object v4, v1, v2

    const/16 v2, 0x4a

    const-string v4, "g"

    aput-object v4, v1, v2

    const/16 v2, 0x4b

    const-string v4, "h"

    aput-object v4, v1, v2

    const/16 v2, 0x4c

    const-string v4, "i"

    aput-object v4, v1, v2

    const/16 v2, 0x4d

    const-string v4, "j"

    aput-object v4, v1, v2

    const/16 v2, 0x4e

    const-string v4, "k"

    aput-object v4, v1, v2

    const/16 v2, 0x4f

    const-string v4, "l"

    aput-object v4, v1, v2

    const/16 v2, 0x50

    const-string v4, "m"

    aput-object v4, v1, v2

    const/16 v2, 0x51

    const-string v4, "n"

    aput-object v4, v1, v2

    const/16 v2, 0x52

    const-string v4, "o"

    aput-object v4, v1, v2

    const/16 v2, 0x53

    const-string v4, "p"

    aput-object v4, v1, v2

    const/16 v2, 0x54

    const-string v4, "q"

    aput-object v4, v1, v2

    const/16 v2, 0x55

    const-string v4, "r"

    aput-object v4, v1, v2

    const/16 v2, 0x56

    const-string v4, "s"

    aput-object v4, v1, v2

    const/16 v2, 0x57

    const-string v4, "t"

    aput-object v4, v1, v2

    const/16 v2, 0x58

    const-string v4, "u"

    aput-object v4, v1, v2

    const/16 v2, 0x59

    const-string v4, "v"

    aput-object v4, v1, v2

    const/16 v2, 0x5a

    const-string v4, "w"

    aput-object v4, v1, v2

    const/16 v2, 0x5b

    const-string v4, "x"

    aput-object v4, v1, v2

    const/16 v2, 0x5c

    const-string v4, "y"

    aput-object v4, v1, v2

    const/16 v2, 0x5d

    const-string v4, "z"

    aput-object v4, v1, v2

    const/16 v2, 0x5e

    const-string v4, "braceleft"

    aput-object v4, v1, v2

    const/16 v2, 0x5f

    const-string v4, "bar"

    aput-object v4, v1, v2

    const/16 v2, 0x60

    const-string v4, "braceright"

    aput-object v4, v1, v2

    const/16 v2, 0x61

    const-string v4, "asciitilde"

    aput-object v4, v1, v2

    const/16 v2, 0x62

    const-string v4, "Adieresis"

    aput-object v4, v1, v2

    const/16 v2, 0x63

    const-string v4, "Aring"

    aput-object v4, v1, v2

    const/16 v2, 0x64

    const-string v4, "Ccedilla"

    aput-object v4, v1, v2

    const/16 v2, 0x65

    const-string v4, "Eacute"

    aput-object v4, v1, v2

    const/16 v2, 0x66

    const-string v4, "Ntilde"

    aput-object v4, v1, v2

    const/16 v2, 0x67

    const-string v4, "Odieresis"

    aput-object v4, v1, v2

    const/16 v2, 0x68

    const-string v4, "Udieresis"

    aput-object v4, v1, v2

    const/16 v2, 0x69

    const-string v4, "aacute"

    aput-object v4, v1, v2

    const/16 v2, 0x6a

    const-string v4, "agrave"

    aput-object v4, v1, v2

    const/16 v2, 0x6b

    const-string v4, "acircumflex"

    aput-object v4, v1, v2

    const/16 v2, 0x6c

    const-string v4, "adieresis"

    aput-object v4, v1, v2

    const/16 v2, 0x6d

    const-string v4, "atilde"

    aput-object v4, v1, v2

    const/16 v2, 0x6e

    const-string v4, "aring"

    aput-object v4, v1, v2

    const/16 v2, 0x6f

    const-string v4, "ccedilla"

    aput-object v4, v1, v2

    const/16 v2, 0x70

    const-string v4, "eacute"

    aput-object v4, v1, v2

    const/16 v2, 0x71

    const-string v4, "egrave"

    aput-object v4, v1, v2

    const/16 v2, 0x72

    const-string v4, "ecircumflex"

    aput-object v4, v1, v2

    const/16 v2, 0x73

    const-string v4, "edieresis"

    aput-object v4, v1, v2

    const/16 v2, 0x74

    const-string v4, "iacute"

    aput-object v4, v1, v2

    const/16 v2, 0x75

    const-string v4, "igrave"

    aput-object v4, v1, v2

    const/16 v2, 0x76

    const-string v4, "icircumflex"

    aput-object v4, v1, v2

    const/16 v2, 0x77

    const-string v4, "idieresis"

    aput-object v4, v1, v2

    const/16 v2, 0x78

    const-string v4, "ntilde"

    aput-object v4, v1, v2

    const/16 v2, 0x79

    const-string v4, "oacute"

    aput-object v4, v1, v2

    const/16 v2, 0x7a

    const-string v4, "ograve"

    aput-object v4, v1, v2

    const/16 v2, 0x7b

    const-string v4, "ocircumflex"

    aput-object v4, v1, v2

    const/16 v2, 0x7c

    const-string v4, "odieresis"

    aput-object v4, v1, v2

    const/16 v2, 0x7d

    const-string v4, "otilde"

    aput-object v4, v1, v2

    const/16 v2, 0x7e

    const-string v4, "uacute"

    aput-object v4, v1, v2

    const/16 v2, 0x7f

    const-string v4, "ugrave"

    aput-object v4, v1, v2

    const/16 v2, 0x80

    const-string v4, "ucircumflex"

    aput-object v4, v1, v2

    const/16 v2, 0x81

    const-string v4, "udieresis"

    aput-object v4, v1, v2

    const/16 v2, 0x82

    const-string v4, "dagger"

    aput-object v4, v1, v2

    const/16 v2, 0x83

    const-string v4, "degree"

    aput-object v4, v1, v2

    const/16 v2, 0x84

    const-string v4, "cent"

    aput-object v4, v1, v2

    const/16 v2, 0x85

    const-string v4, "sterling"

    aput-object v4, v1, v2

    const/16 v2, 0x86

    const-string v4, "section"

    aput-object v4, v1, v2

    const/16 v2, 0x87

    const-string v4, "bullet"

    aput-object v4, v1, v2

    const/16 v2, 0x88

    const-string v4, "paragraph"

    aput-object v4, v1, v2

    const/16 v2, 0x89

    const-string v4, "germandbls"

    aput-object v4, v1, v2

    const/16 v2, 0x8a

    const-string v4, "registered"

    aput-object v4, v1, v2

    const/16 v2, 0x8b

    const-string v4, "copyright"

    aput-object v4, v1, v2

    const/16 v2, 0x8c

    const-string v4, "trademark"

    aput-object v4, v1, v2

    const/16 v2, 0x8d

    const-string v4, "acute"

    aput-object v4, v1, v2

    const/16 v2, 0x8e

    const-string v4, "dieresis"

    aput-object v4, v1, v2

    const/16 v2, 0x8f

    const-string v4, "notequal"

    aput-object v4, v1, v2

    const/16 v2, 0x90

    const-string v4, "AE"

    aput-object v4, v1, v2

    const/16 v2, 0x91

    const-string v4, "Oslash"

    aput-object v4, v1, v2

    const/16 v2, 0x92

    const-string v4, "infinity"

    aput-object v4, v1, v2

    const/16 v2, 0x93

    const-string v4, "plusminus"

    aput-object v4, v1, v2

    const/16 v2, 0x94

    const-string v4, "lessequal"

    aput-object v4, v1, v2

    const/16 v2, 0x95

    const-string v4, "greaterequal"

    aput-object v4, v1, v2

    const/16 v2, 0x96

    const-string v4, "yen"

    aput-object v4, v1, v2

    const/16 v2, 0x97

    const-string v4, "mu"

    aput-object v4, v1, v2

    const/16 v2, 0x98

    const-string v4, "partialdiff"

    aput-object v4, v1, v2

    const/16 v2, 0x99

    const-string v4, "summation"

    aput-object v4, v1, v2

    const/16 v2, 0x9a

    const-string v4, "product"

    aput-object v4, v1, v2

    const/16 v2, 0x9b

    const-string v4, "pi"

    aput-object v4, v1, v2

    const/16 v2, 0x9c

    const-string v4, "integral"

    aput-object v4, v1, v2

    const/16 v2, 0x9d

    const-string v4, "ordfeminine"

    aput-object v4, v1, v2

    const/16 v2, 0x9e

    const-string v4, "ordmasculine"

    aput-object v4, v1, v2

    const/16 v2, 0x9f

    const-string v4, "Omega"

    aput-object v4, v1, v2

    const/16 v2, 0xa0

    const-string v4, "ae"

    aput-object v4, v1, v2

    const/16 v2, 0xa1

    const-string v4, "oslash"

    aput-object v4, v1, v2

    const/16 v2, 0xa2

    const-string v4, "questiondown"

    aput-object v4, v1, v2

    const/16 v2, 0xa3

    const-string v4, "exclamdown"

    aput-object v4, v1, v2

    const/16 v2, 0xa4

    const-string v4, "logicalnot"

    aput-object v4, v1, v2

    const/16 v2, 0xa5

    const-string v4, "radical"

    aput-object v4, v1, v2

    const/16 v2, 0xa6

    const-string v4, "florin"

    aput-object v4, v1, v2

    const/16 v2, 0xa7

    const-string v4, "approxequal"

    aput-object v4, v1, v2

    const/16 v2, 0xa8

    const-string v4, "Delta"

    aput-object v4, v1, v2

    const/16 v2, 0xa9

    const-string v4, "guillemotleft"

    aput-object v4, v1, v2

    const/16 v2, 0xaa

    const-string v4, "guillemotright"

    aput-object v4, v1, v2

    const/16 v2, 0xab

    const-string v4, "ellipsis"

    aput-object v4, v1, v2

    const/16 v2, 0xac

    const-string v4, "nonbreakingspace"

    aput-object v4, v1, v2

    const/16 v2, 0xad

    const-string v4, "Agrave"

    aput-object v4, v1, v2

    const/16 v2, 0xae

    const-string v4, "Atilde"

    aput-object v4, v1, v2

    const/16 v2, 0xaf

    const-string v4, "Otilde"

    aput-object v4, v1, v2

    const/16 v2, 0xb0

    const-string v4, "OE"

    aput-object v4, v1, v2

    const/16 v2, 0xb1

    const-string v4, "oe"

    aput-object v4, v1, v2

    const/16 v2, 0xb2

    const-string v4, "endash"

    aput-object v4, v1, v2

    const/16 v2, 0xb3

    const-string v4, "emdash"

    aput-object v4, v1, v2

    const/16 v2, 0xb4

    const-string v4, "quotedblleft"

    aput-object v4, v1, v2

    const/16 v2, 0xb5

    const-string v4, "quotedblright"

    aput-object v4, v1, v2

    const/16 v2, 0xb6

    const-string v4, "quoteleft"

    aput-object v4, v1, v2

    const/16 v2, 0xb7

    const-string v4, "quoteright"

    aput-object v4, v1, v2

    const/16 v2, 0xb8

    const-string v4, "divide"

    aput-object v4, v1, v2

    const/16 v2, 0xb9

    const-string v4, "lozenge"

    aput-object v4, v1, v2

    const/16 v2, 0xba

    const-string v4, "ydieresis"

    aput-object v4, v1, v2

    const/16 v2, 0xbb

    const-string v4, "Ydieresis"

    aput-object v4, v1, v2

    const/16 v2, 0xbc

    const-string v4, "fraction"

    aput-object v4, v1, v2

    const/16 v2, 0xbd

    const-string v4, "currency"

    aput-object v4, v1, v2

    const/16 v2, 0xbe

    const-string v4, "guilsinglleft"

    aput-object v4, v1, v2

    const/16 v2, 0xbf

    const-string v4, "guilsinglright"

    aput-object v4, v1, v2

    const/16 v2, 0xc0

    const-string v4, "fi"

    aput-object v4, v1, v2

    const/16 v2, 0xc1

    const-string v4, "fl"

    aput-object v4, v1, v2

    const/16 v2, 0xc2

    const-string v4, "daggerdbl"

    aput-object v4, v1, v2

    const/16 v2, 0xc3

    const-string v4, "periodcentered"

    aput-object v4, v1, v2

    const/16 v2, 0xc4

    const-string v4, "quotesinglbase"

    aput-object v4, v1, v2

    const/16 v2, 0xc5

    const-string v4, "quotedblbase"

    aput-object v4, v1, v2

    const/16 v2, 0xc6

    const-string v4, "perthousand"

    aput-object v4, v1, v2

    const/16 v2, 0xc7

    const-string v4, "Acircumflex"

    aput-object v4, v1, v2

    const/16 v2, 0xc8

    const-string v4, "Ecircumflex"

    aput-object v4, v1, v2

    const/16 v2, 0xc9

    const-string v4, "Aacute"

    aput-object v4, v1, v2

    const/16 v2, 0xca

    const-string v4, "Edieresis"

    aput-object v4, v1, v2

    const/16 v2, 0xcb

    const-string v4, "Egrave"

    aput-object v4, v1, v2

    const/16 v2, 0xcc

    const-string v4, "Iacute"

    aput-object v4, v1, v2

    const/16 v2, 0xcd

    const-string v4, "Icircumflex"

    aput-object v4, v1, v2

    const/16 v2, 0xce

    const-string v4, "Idieresis"

    aput-object v4, v1, v2

    const/16 v2, 0xcf

    const-string v4, "Igrave"

    aput-object v4, v1, v2

    const/16 v2, 0xd0

    const-string v4, "Oacute"

    aput-object v4, v1, v2

    const/16 v2, 0xd1

    const-string v4, "Ocircumflex"

    aput-object v4, v1, v2

    const/16 v2, 0xd2

    const-string v4, "apple"

    aput-object v4, v1, v2

    const/16 v2, 0xd3

    const-string v4, "Ograve"

    aput-object v4, v1, v2

    const/16 v2, 0xd4

    const-string v4, "Uacute"

    aput-object v4, v1, v2

    const/16 v2, 0xd5

    const-string v4, "Ucircumflex"

    aput-object v4, v1, v2

    const/16 v2, 0xd6

    const-string v4, "Ugrave"

    aput-object v4, v1, v2

    const/16 v2, 0xd7

    const-string v4, "dotlessi"

    aput-object v4, v1, v2

    const/16 v2, 0xd8

    const-string v4, "circumflex"

    aput-object v4, v1, v2

    const/16 v2, 0xd9

    const-string v4, "tilde"

    aput-object v4, v1, v2

    const/16 v2, 0xda

    const-string v4, "macron"

    aput-object v4, v1, v2

    const/16 v2, 0xdb

    const-string v4, "breve"

    aput-object v4, v1, v2

    const/16 v2, 0xdc

    const-string v4, "dotaccent"

    aput-object v4, v1, v2

    const/16 v2, 0xdd

    const-string v4, "ring"

    aput-object v4, v1, v2

    const/16 v2, 0xde

    const-string v4, "cedilla"

    aput-object v4, v1, v2

    const/16 v2, 0xdf

    const-string v4, "hungarumlaut"

    aput-object v4, v1, v2

    const/16 v2, 0xe0

    const-string v4, "ogonek"

    aput-object v4, v1, v2

    const/16 v2, 0xe1

    const-string v4, "caron"

    aput-object v4, v1, v2

    const/16 v2, 0xe2

    const-string v4, "Lslash"

    aput-object v4, v1, v2

    const/16 v2, 0xe3

    const-string v4, "lslash"

    aput-object v4, v1, v2

    const/16 v2, 0xe4

    const-string v4, "Scaron"

    aput-object v4, v1, v2

    const/16 v2, 0xe5

    const-string v4, "scaron"

    aput-object v4, v1, v2

    const/16 v2, 0xe6

    const-string v4, "Zcaron"

    aput-object v4, v1, v2

    const/16 v2, 0xe7

    const-string v4, "zcaron"

    aput-object v4, v1, v2

    const/16 v2, 0xe8

    const-string v4, "brokenbar"

    aput-object v4, v1, v2

    const/16 v2, 0xe9

    const-string v4, "Eth"

    aput-object v4, v1, v2

    const/16 v2, 0xea

    const-string v4, "eth"

    aput-object v4, v1, v2

    const/16 v2, 0xeb

    const-string v4, "Yacute"

    aput-object v4, v1, v2

    const/16 v2, 0xec

    const-string v4, "yacute"

    aput-object v4, v1, v2

    const/16 v2, 0xed

    const-string v4, "Thorn"

    aput-object v4, v1, v2

    const/16 v2, 0xee

    const-string v4, "thorn"

    aput-object v4, v1, v2

    const/16 v2, 0xef

    const-string v4, "minus"

    aput-object v4, v1, v2

    const/16 v2, 0xf0

    const-string v4, "multiply"

    aput-object v4, v1, v2

    const/16 v2, 0xf1

    const-string v4, "onesuperior"

    aput-object v4, v1, v2

    const/16 v2, 0xf2

    const-string v4, "twosuperior"

    aput-object v4, v1, v2

    const/16 v2, 0xf3

    const-string v4, "threesuperior"

    aput-object v4, v1, v2

    const/16 v2, 0xf4

    const-string v4, "onehalf"

    aput-object v4, v1, v2

    const/16 v2, 0xf5

    const-string v4, "onequarter"

    aput-object v4, v1, v2

    const/16 v2, 0xf6

    const-string v4, "threequarters"

    aput-object v4, v1, v2

    const/16 v2, 0xf7

    const-string v4, "franc"

    aput-object v4, v1, v2

    const/16 v2, 0xf8

    const-string v4, "Gbreve"

    aput-object v4, v1, v2

    const/16 v2, 0xf9

    const-string v4, "gbreve"

    aput-object v4, v1, v2

    const/16 v2, 0xfa

    const-string v4, "Idotaccent"

    aput-object v4, v1, v2

    const/16 v2, 0xfb

    const-string v4, "Scedilla"

    aput-object v4, v1, v2

    const/16 v2, 0xfc

    const-string v4, "scedilla"

    aput-object v4, v1, v2

    const/16 v2, 0xfd

    const-string v4, "Cacute"

    aput-object v4, v1, v2

    const/16 v2, 0xfe

    const-string v4, "cacute"

    aput-object v4, v1, v2

    const/16 v2, 0xff

    const-string v4, "Ccaron"

    aput-object v4, v1, v2

    const/16 v2, 0x100

    const-string v4, "ccaron"

    aput-object v4, v1, v2

    const/16 v2, 0x101

    const-string v4, "dcroat"

    aput-object v4, v1, v2

    sput-object v1, Lorg/apache/fontbox/ttf/WGL4Names;->MAC_GLYPH_NAMES:[Ljava/lang/String;

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    sput-object v1, Lorg/apache/fontbox/ttf/WGL4Names;->MAC_GLYPH_NAMES_INDICES:Ljava/util/Map;

    :goto_0
    if-ge v3, v0, :cond_0

    sget-object v1, Lorg/apache/fontbox/ttf/WGL4Names;->MAC_GLYPH_NAMES_INDICES:Ljava/util/Map;

    sget-object v2, Lorg/apache/fontbox/ttf/WGL4Names;->MAC_GLYPH_NAMES:[Ljava/lang/String;

    aget-object v2, v2, v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v1, v2, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

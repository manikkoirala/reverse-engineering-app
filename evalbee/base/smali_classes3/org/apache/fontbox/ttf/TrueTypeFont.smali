.class public Lorg/apache/fontbox/ttf/TrueTypeFont;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/fontbox/ttf/Type1Equivalent;


# instance fields
.field private data:Lorg/apache/fontbox/ttf/TTFDataStream;

.field private numberOfGlyphs:I

.field private postScriptNames:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field protected tables:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lorg/apache/fontbox/ttf/TTFTable;",
            ">;"
        }
    .end annotation
.end field

.field private unitsPerEm:I

.field private version:F


# direct methods
.method public constructor <init>(Lorg/apache/fontbox/ttf/TTFDataStream;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lorg/apache/fontbox/ttf/TrueTypeFont;->numberOfGlyphs:I

    iput v0, p0, Lorg/apache/fontbox/ttf/TrueTypeFont;->unitsPerEm:I

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/fontbox/ttf/TrueTypeFont;->tables:Ljava/util/Map;

    iput-object p1, p0, Lorg/apache/fontbox/ttf/TrueTypeFont;->data:Lorg/apache/fontbox/ttf/TTFDataStream;

    return-void
.end method

.method private parseUniName(Ljava/lang/String;)I
    .locals 6

    const-string v0, "uni"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    const/4 v1, -0x1

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v2, 0x7

    if-ne v0, v2, :cond_4

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v3, 0x3

    :goto_0
    add-int/lit8 v4, v3, 0x4

    if-gt v4, v0, :cond_2

    :try_start_0
    invoke-virtual {p1, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    const/16 v5, 0x10

    invoke-static {v3, v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v3

    const v5, 0xd7ff

    if-le v3, v5, :cond_0

    const v5, 0xe000

    if-lt v3, v5, :cond_1

    :cond_0
    int-to-char v3, v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :cond_1
    move v3, v4

    goto :goto_0

    :cond_2
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_3

    return v1

    :cond_3
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Ljava/lang/String;->codePointAt(I)I

    move-result p1
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    return p1

    :catch_0
    :cond_4
    return v1
.end method

.method private declared-synchronized readPostScriptNames()V
    .locals 5

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/fontbox/ttf/TrueTypeFont;->postScriptNames:Ljava/util/Map;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lorg/apache/fontbox/ttf/TrueTypeFont;->postScriptNames:Ljava/util/Map;

    invoke-virtual {p0}, Lorg/apache/fontbox/ttf/TrueTypeFont;->getPostScript()Lorg/apache/fontbox/ttf/PostScriptTable;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/fontbox/ttf/TrueTypeFont;->getPostScript()Lorg/apache/fontbox/ttf/PostScriptTable;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/fontbox/ttf/PostScriptTable;->getGlyphNames()[Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    array-length v2, v0

    if-ge v1, v2, :cond_0

    iget-object v2, p0, Lorg/apache/fontbox/ttf/TrueTypeFont;->postScriptNames:Ljava/util/Map;

    aget-object v3, v0, v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public addTable(Lorg/apache/fontbox/ttf/TTFTable;)V
    .locals 2

    iget-object v0, p0, Lorg/apache/fontbox/ttf/TrueTypeFont;->tables:Ljava/util/Map;

    invoke-virtual {p1}, Lorg/apache/fontbox/ttf/TTFTable;->getTag()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public close()V
    .locals 1

    iget-object v0, p0, Lorg/apache/fontbox/ttf/TrueTypeFont;->data:Lorg/apache/fontbox/ttf/TTFDataStream;

    invoke-virtual {v0}, Lorg/apache/fontbox/ttf/TTFDataStream;->close()V

    return-void
.end method

.method public getAdvanceWidth(I)I
    .locals 1

    invoke-virtual {p0}, Lorg/apache/fontbox/ttf/TrueTypeFont;->getHorizontalMetrics()Lorg/apache/fontbox/ttf/HorizontalMetricsTable;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lorg/apache/fontbox/ttf/HorizontalMetricsTable;->getAdvanceWidth(I)I

    move-result p1

    return p1

    :cond_0
    const/16 p1, 0xfa

    return p1
.end method

.method public declared-synchronized getCmap()Lorg/apache/fontbox/ttf/CmapTable;
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/fontbox/ttf/TrueTypeFont;->tables:Ljava/util/Map;

    const-string v1, "cmap"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/fontbox/ttf/CmapTable;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lorg/apache/fontbox/ttf/TTFTable;->getInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0, v0}, Lorg/apache/fontbox/ttf/TrueTypeFont;->readTable(Lorg/apache/fontbox/ttf/TTFTable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getEncoding()Lorg/apache/fontbox/encoding/Encoding;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getFontBBox()Lorg/apache/fontbox/util/BoundingBox;
    .locals 6

    invoke-virtual {p0}, Lorg/apache/fontbox/ttf/TrueTypeFont;->getHeader()Lorg/apache/fontbox/ttf/HeaderTable;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/fontbox/ttf/HeaderTable;->getXMin()S

    move-result v0

    invoke-virtual {p0}, Lorg/apache/fontbox/ttf/TrueTypeFont;->getHeader()Lorg/apache/fontbox/ttf/HeaderTable;

    move-result-object v1

    invoke-virtual {v1}, Lorg/apache/fontbox/ttf/HeaderTable;->getXMax()S

    move-result v1

    invoke-virtual {p0}, Lorg/apache/fontbox/ttf/TrueTypeFont;->getHeader()Lorg/apache/fontbox/ttf/HeaderTable;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/fontbox/ttf/HeaderTable;->getYMin()S

    move-result v2

    invoke-virtual {p0}, Lorg/apache/fontbox/ttf/TrueTypeFont;->getHeader()Lorg/apache/fontbox/ttf/HeaderTable;

    move-result-object v3

    invoke-virtual {v3}, Lorg/apache/fontbox/ttf/HeaderTable;->getYMax()S

    move-result v3

    invoke-virtual {p0}, Lorg/apache/fontbox/ttf/TrueTypeFont;->getUnitsPerEm()I

    move-result v4

    int-to-float v4, v4

    const/high16 v5, 0x447a0000    # 1000.0f

    div-float/2addr v5, v4

    new-instance v4, Lorg/apache/fontbox/util/BoundingBox;

    int-to-float v0, v0

    mul-float/2addr v0, v5

    int-to-float v2, v2

    mul-float/2addr v2, v5

    int-to-float v1, v1

    mul-float/2addr v1, v5

    int-to-float v3, v3

    mul-float/2addr v3, v5

    invoke-direct {v4, v0, v2, v1, v3}, Lorg/apache/fontbox/util/BoundingBox;-><init>(FFFF)V

    return-object v4
.end method

.method public declared-synchronized getGlyph()Lorg/apache/fontbox/ttf/GlyphTable;
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/fontbox/ttf/TrueTypeFont;->tables:Ljava/util/Map;

    const-string v1, "glyf"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/fontbox/ttf/GlyphTable;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lorg/apache/fontbox/ttf/TTFTable;->getInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0, v0}, Lorg/apache/fontbox/ttf/TrueTypeFont;->readTable(Lorg/apache/fontbox/ttf/TTFTable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getHeader()Lorg/apache/fontbox/ttf/HeaderTable;
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/fontbox/ttf/TrueTypeFont;->tables:Ljava/util/Map;

    const-string v1, "head"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/fontbox/ttf/HeaderTable;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lorg/apache/fontbox/ttf/TTFTable;->getInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0, v0}, Lorg/apache/fontbox/ttf/TrueTypeFont;->readTable(Lorg/apache/fontbox/ttf/TTFTable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getHorizontalHeader()Lorg/apache/fontbox/ttf/HorizontalHeaderTable;
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/fontbox/ttf/TrueTypeFont;->tables:Ljava/util/Map;

    const-string v1, "hhea"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/fontbox/ttf/HorizontalHeaderTable;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lorg/apache/fontbox/ttf/TTFTable;->getInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0, v0}, Lorg/apache/fontbox/ttf/TrueTypeFont;->readTable(Lorg/apache/fontbox/ttf/TTFTable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getHorizontalMetrics()Lorg/apache/fontbox/ttf/HorizontalMetricsTable;
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/fontbox/ttf/TrueTypeFont;->tables:Ljava/util/Map;

    const-string v1, "hmtx"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/fontbox/ttf/HorizontalMetricsTable;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lorg/apache/fontbox/ttf/TTFTable;->getInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0, v0}, Lorg/apache/fontbox/ttf/TrueTypeFont;->readTable(Lorg/apache/fontbox/ttf/TTFTable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getIndexToLocation()Lorg/apache/fontbox/ttf/IndexToLocationTable;
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/fontbox/ttf/TrueTypeFont;->tables:Ljava/util/Map;

    const-string v1, "loca"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/fontbox/ttf/IndexToLocationTable;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lorg/apache/fontbox/ttf/TTFTable;->getInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0, v0}, Lorg/apache/fontbox/ttf/TrueTypeFont;->readTable(Lorg/apache/fontbox/ttf/TTFTable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getMaximumProfile()Lorg/apache/fontbox/ttf/MaximumProfileTable;
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/fontbox/ttf/TrueTypeFont;->tables:Ljava/util/Map;

    const-string v1, "maxp"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/fontbox/ttf/MaximumProfileTable;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lorg/apache/fontbox/ttf/TTFTable;->getInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0, v0}, Lorg/apache/fontbox/ttf/TrueTypeFont;->readTable(Lorg/apache/fontbox/ttf/TTFTable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lorg/apache/fontbox/ttf/TrueTypeFont;->getNaming()Lorg/apache/fontbox/ttf/NamingTable;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/fontbox/ttf/TrueTypeFont;->getNaming()Lorg/apache/fontbox/ttf/NamingTable;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/fontbox/ttf/NamingTable;->getPostScriptName()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public declared-synchronized getNaming()Lorg/apache/fontbox/ttf/NamingTable;
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/fontbox/ttf/TrueTypeFont;->tables:Ljava/util/Map;

    const-string v1, "name"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/fontbox/ttf/NamingTable;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lorg/apache/fontbox/ttf/TTFTable;->getInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0, v0}, Lorg/apache/fontbox/ttf/TrueTypeFont;->readTable(Lorg/apache/fontbox/ttf/TTFTable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getNumberOfGlyphs()I
    .locals 2

    iget v0, p0, Lorg/apache/fontbox/ttf/TrueTypeFont;->numberOfGlyphs:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    invoke-virtual {p0}, Lorg/apache/fontbox/ttf/TrueTypeFont;->getMaximumProfile()Lorg/apache/fontbox/ttf/MaximumProfileTable;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lorg/apache/fontbox/ttf/MaximumProfileTable;->getNumGlyphs()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iput v0, p0, Lorg/apache/fontbox/ttf/TrueTypeFont;->numberOfGlyphs:I

    :cond_1
    iget v0, p0, Lorg/apache/fontbox/ttf/TrueTypeFont;->numberOfGlyphs:I

    return v0
.end method

.method public declared-synchronized getOS2Windows()Lorg/apache/fontbox/ttf/OS2WindowsMetricsTable;
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/fontbox/ttf/TrueTypeFont;->tables:Ljava/util/Map;

    const-string v1, "OS/2"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/fontbox/ttf/OS2WindowsMetricsTable;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lorg/apache/fontbox/ttf/TTFTable;->getInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0, v0}, Lorg/apache/fontbox/ttf/TrueTypeFont;->readTable(Lorg/apache/fontbox/ttf/TTFTable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public getOriginalData()Ljava/io/InputStream;
    .locals 1

    iget-object v0, p0, Lorg/apache/fontbox/ttf/TrueTypeFont;->data:Lorg/apache/fontbox/ttf/TTFDataStream;

    invoke-virtual {v0}, Lorg/apache/fontbox/ttf/TTFDataStream;->getOriginalData()Ljava/io/InputStream;

    move-result-object v0

    return-object v0
.end method

.method public getPath(Ljava/lang/String;)Landroid/graphics/Path;
    .locals 2

    invoke-direct {p0}, Lorg/apache/fontbox/ttf/TrueTypeFont;->readPostScriptNames()V

    invoke-virtual {p0, p1}, Lorg/apache/fontbox/ttf/TrueTypeFont;->nameToGID(Ljava/lang/String;)I

    move-result p1

    if-ltz p1, :cond_0

    invoke-virtual {p0}, Lorg/apache/fontbox/ttf/TrueTypeFont;->getMaximumProfile()Lorg/apache/fontbox/ttf/MaximumProfileTable;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/fontbox/ttf/MaximumProfileTable;->getNumGlyphs()I

    move-result v0

    if-lt p1, v0, :cond_1

    :cond_0
    const/4 p1, 0x0

    :cond_1
    invoke-virtual {p0}, Lorg/apache/fontbox/ttf/TrueTypeFont;->getGlyph()Lorg/apache/fontbox/ttf/GlyphTable;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/fontbox/ttf/GlyphTable;->getGlyph(I)Lorg/apache/fontbox/ttf/GlyphData;

    move-result-object p1

    if-nez p1, :cond_2

    new-instance p1, Landroid/graphics/Path;

    invoke-direct {p1}, Landroid/graphics/Path;-><init>()V

    return-object p1

    :cond_2
    invoke-virtual {p1}, Lorg/apache/fontbox/ttf/GlyphData;->getPath()Landroid/graphics/Path;

    move-result-object p1

    invoke-virtual {p0}, Lorg/apache/fontbox/ttf/TrueTypeFont;->getUnitsPerEm()I

    move-result v0

    int-to-float v0, v0

    const/high16 v1, 0x447a0000    # 1000.0f

    div-float/2addr v1, v0

    float-to-double v0, v1

    invoke-static {v0, v1, v0, v1}, Lorg/apache/pdfbox/util/awt/AffineTransform;->getScaleInstance(DD)Lorg/apache/pdfbox/util/awt/AffineTransform;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/pdfbox/util/awt/AffineTransform;->toMatrix()Landroid/graphics/Matrix;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/graphics/Path;->transform(Landroid/graphics/Matrix;)V

    return-object p1
.end method

.method public declared-synchronized getPostScript()Lorg/apache/fontbox/ttf/PostScriptTable;
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/fontbox/ttf/TrueTypeFont;->tables:Ljava/util/Map;

    const-string v1, "post"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/fontbox/ttf/PostScriptTable;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lorg/apache/fontbox/ttf/TTFTable;->getInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0, v0}, Lorg/apache/fontbox/ttf/TrueTypeFont;->readTable(Lorg/apache/fontbox/ttf/TTFTable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized getTableBytes(Lorg/apache/fontbox/ttf/TTFTable;)[B
    .locals 5

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/fontbox/ttf/TrueTypeFont;->data:Lorg/apache/fontbox/ttf/TTFDataStream;

    invoke-virtual {v0}, Lorg/apache/fontbox/ttf/TTFDataStream;->getCurrentPosition()J

    move-result-wide v0

    iget-object v2, p0, Lorg/apache/fontbox/ttf/TrueTypeFont;->data:Lorg/apache/fontbox/ttf/TTFDataStream;

    invoke-virtual {p1}, Lorg/apache/fontbox/ttf/TTFTable;->getOffset()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Lorg/apache/fontbox/ttf/TTFDataStream;->seek(J)V

    iget-object v2, p0, Lorg/apache/fontbox/ttf/TrueTypeFont;->data:Lorg/apache/fontbox/ttf/TTFDataStream;

    invoke-virtual {p1}, Lorg/apache/fontbox/ttf/TTFTable;->getLength()J

    move-result-wide v3

    long-to-int p1, v3

    invoke-virtual {v2, p1}, Lorg/apache/fontbox/ttf/TTFDataStream;->read(I)[B

    move-result-object p1

    iget-object v2, p0, Lorg/apache/fontbox/ttf/TrueTypeFont;->data:Lorg/apache/fontbox/ttf/TTFDataStream;

    invoke-virtual {v2, v0, v1}, Lorg/apache/fontbox/ttf/TTFDataStream;->seek(J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object p1

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public getTableMap()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lorg/apache/fontbox/ttf/TTFTable;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/fontbox/ttf/TrueTypeFont;->tables:Ljava/util/Map;

    return-object v0
.end method

.method public getTables()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection<",
            "Lorg/apache/fontbox/ttf/TTFTable;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lorg/apache/fontbox/ttf/TrueTypeFont;->tables:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method public getUnicodeCmap()Lorg/apache/fontbox/ttf/CmapSubtable;
    .locals 1

    .line 1
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lorg/apache/fontbox/ttf/TrueTypeFont;->getUnicodeCmap(Z)Lorg/apache/fontbox/ttf/CmapSubtable;

    move-result-object v0

    return-object v0
.end method

.method public getUnicodeCmap(Z)Lorg/apache/fontbox/ttf/CmapSubtable;
    .locals 4

    .line 2
    invoke-virtual {p0}, Lorg/apache/fontbox/ttf/TrueTypeFont;->getCmap()Lorg/apache/fontbox/ttf/CmapTable;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 p1, 0x0

    return-object p1

    :cond_0
    const/4 v1, 0x4

    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1}, Lorg/apache/fontbox/ttf/CmapTable;->getSubtable(II)Lorg/apache/fontbox/ttf/CmapSubtable;

    move-result-object v1

    const/4 v3, 0x3

    if-nez v1, :cond_1

    invoke-virtual {v0, v2, v3}, Lorg/apache/fontbox/ttf/CmapTable;->getSubtable(II)Lorg/apache/fontbox/ttf/CmapSubtable;

    move-result-object v1

    :cond_1
    if-nez v1, :cond_2

    const/4 v1, 0x1

    invoke-virtual {v0, v3, v1}, Lorg/apache/fontbox/ttf/CmapTable;->getSubtable(II)Lorg/apache/fontbox/ttf/CmapSubtable;

    move-result-object v1

    :cond_2
    if-nez v1, :cond_3

    invoke-virtual {v0, v3, v2}, Lorg/apache/fontbox/ttf/CmapTable;->getSubtable(II)Lorg/apache/fontbox/ttf/CmapSubtable;

    move-result-object v1

    :cond_3
    if-nez v1, :cond_5

    if-nez p1, :cond_4

    invoke-virtual {v0}, Lorg/apache/fontbox/ttf/CmapTable;->getCmaps()[Lorg/apache/fontbox/ttf/CmapSubtable;

    move-result-object p1

    aget-object v1, p1, v2

    goto :goto_0

    :cond_4
    new-instance p1, Ljava/io/IOException;

    const-string v0, "The TrueType font does not contain a Unicode cmap"

    invoke-direct {p1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_5
    :goto_0
    return-object v1
.end method

.method public getUnitsPerEm()I
    .locals 2

    iget v0, p0, Lorg/apache/fontbox/ttf/TrueTypeFont;->unitsPerEm:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    invoke-virtual {p0}, Lorg/apache/fontbox/ttf/TrueTypeFont;->getHeader()Lorg/apache/fontbox/ttf/HeaderTable;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lorg/apache/fontbox/ttf/HeaderTable;->getUnitsPerEm()I

    move-result v0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    iput v0, p0, Lorg/apache/fontbox/ttf/TrueTypeFont;->unitsPerEm:I

    :cond_1
    iget v0, p0, Lorg/apache/fontbox/ttf/TrueTypeFont;->unitsPerEm:I

    return v0
.end method

.method public getVersion()F
    .locals 1

    iget v0, p0, Lorg/apache/fontbox/ttf/TrueTypeFont;->version:F

    return v0
.end method

.method public getWidth(Ljava/lang/String;)F
    .locals 2

    invoke-virtual {p0, p1}, Lorg/apache/fontbox/ttf/TrueTypeFont;->nameToGID(Ljava/lang/String;)I

    move-result p1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    invoke-virtual {p0, p1}, Lorg/apache/fontbox/ttf/TrueTypeFont;->getAdvanceWidth(I)I

    move-result p1

    invoke-virtual {p0}, Lorg/apache/fontbox/ttf/TrueTypeFont;->getUnitsPerEm()I

    move-result v0

    const/16 v1, 0x3e8

    if-eq v0, v1, :cond_0

    int-to-float p1, p1

    const/high16 v1, 0x447a0000    # 1000.0f

    int-to-float v0, v0

    div-float/2addr v1, v0

    mul-float/2addr p1, v1

    float-to-int p1, p1

    :cond_0
    int-to-float p1, p1

    return p1
.end method

.method public hasGlyph(Ljava/lang/String;)Z
    .locals 0

    invoke-virtual {p0, p1}, Lorg/apache/fontbox/ttf/TrueTypeFont;->nameToGID(Ljava/lang/String;)I

    move-result p1

    if-eqz p1, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public nameToGID(Ljava/lang/String;)I
    .locals 3

    invoke-direct {p0}, Lorg/apache/fontbox/ttf/TrueTypeFont;->readPostScriptNames()V

    iget-object v0, p0, Lorg/apache/fontbox/ttf/TrueTypeFont;->postScriptNames:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-lez v1, :cond_0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p0}, Lorg/apache/fontbox/ttf/TrueTypeFont;->getMaximumProfile()Lorg/apache/fontbox/ttf/MaximumProfileTable;

    move-result-object v2

    invoke-virtual {v2}, Lorg/apache/fontbox/ttf/MaximumProfileTable;->getNumGlyphs()I

    move-result v2

    if-ge v1, v2, :cond_0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result p1

    return p1

    :cond_0
    invoke-direct {p0, p1}, Lorg/apache/fontbox/ttf/TrueTypeFont;->parseUniName(Ljava/lang/String;)I

    move-result p1

    const/4 v0, -0x1

    const/4 v1, 0x0

    if-le p1, v0, :cond_1

    invoke-virtual {p0, v1}, Lorg/apache/fontbox/ttf/TrueTypeFont;->getUnicodeCmap(Z)Lorg/apache/fontbox/ttf/CmapSubtable;

    move-result-object v0

    invoke-virtual {v0, p1}, Lorg/apache/fontbox/ttf/CmapSubtable;->getGlyphId(I)I

    move-result p1

    return p1

    :cond_1
    return v1
.end method

.method public readTable(Lorg/apache/fontbox/ttf/TTFTable;)V
    .locals 5

    iget-object v0, p0, Lorg/apache/fontbox/ttf/TrueTypeFont;->data:Lorg/apache/fontbox/ttf/TTFDataStream;

    invoke-virtual {v0}, Lorg/apache/fontbox/ttf/TTFDataStream;->getCurrentPosition()J

    move-result-wide v0

    iget-object v2, p0, Lorg/apache/fontbox/ttf/TrueTypeFont;->data:Lorg/apache/fontbox/ttf/TTFDataStream;

    invoke-virtual {p1}, Lorg/apache/fontbox/ttf/TTFTable;->getOffset()J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Lorg/apache/fontbox/ttf/TTFDataStream;->seek(J)V

    iget-object v2, p0, Lorg/apache/fontbox/ttf/TrueTypeFont;->data:Lorg/apache/fontbox/ttf/TTFDataStream;

    invoke-virtual {p1, p0, v2}, Lorg/apache/fontbox/ttf/TTFTable;->read(Lorg/apache/fontbox/ttf/TrueTypeFont;Lorg/apache/fontbox/ttf/TTFDataStream;)V

    iget-object p1, p0, Lorg/apache/fontbox/ttf/TrueTypeFont;->data:Lorg/apache/fontbox/ttf/TTFDataStream;

    invoke-virtual {p1, v0, v1}, Lorg/apache/fontbox/ttf/TTFDataStream;->seek(J)V

    return-void
.end method

.method public setVersion(F)V
    .locals 0

    iput p1, p0, Lorg/apache/fontbox/ttf/TrueTypeFont;->version:F

    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    :try_start_0
    invoke-virtual {p0}, Lorg/apache/fontbox/ttf/TrueTypeFont;->getNaming()Lorg/apache/fontbox/ttf/NamingTable;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lorg/apache/fontbox/ttf/TrueTypeFont;->getNaming()Lorg/apache/fontbox/ttf/NamingTable;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/fontbox/ttf/NamingTable;->getPostScriptName()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, "(null)"
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "(null - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, ")"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.class public Lorg/apache/fontbox/ttf/OpenTypeFont;
.super Lorg/apache/fontbox/ttf/TrueTypeFont;
.source "SourceFile"


# direct methods
.method public constructor <init>(Lorg/apache/fontbox/ttf/TTFDataStream;)V
    .locals 0

    invoke-direct {p0, p1}, Lorg/apache/fontbox/ttf/TrueTypeFont;-><init>(Lorg/apache/fontbox/ttf/TTFDataStream;)V

    return-void
.end method


# virtual methods
.method public declared-synchronized getCFF()Lorg/apache/fontbox/ttf/CFFTable;
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lorg/apache/fontbox/ttf/TrueTypeFont;->tables:Ljava/util/Map;

    const-string v1, "CFF "

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/fontbox/ttf/CFFTable;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lorg/apache/fontbox/ttf/TTFTable;->getInitialized()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0, v0}, Lorg/apache/fontbox/ttf/TrueTypeFont;->readTable(Lorg/apache/fontbox/ttf/TTFTable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public hasLayoutTables()Z
    .locals 2

    iget-object v0, p0, Lorg/apache/fontbox/ttf/TrueTypeFont;->tables:Ljava/util/Map;

    const-string v1, "BASE"

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lorg/apache/fontbox/ttf/TrueTypeFont;->tables:Ljava/util/Map;

    const-string v1, "GDEF"

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lorg/apache/fontbox/ttf/TrueTypeFont;->tables:Ljava/util/Map;

    const-string v1, "GPOS"

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lorg/apache/fontbox/ttf/TrueTypeFont;->tables:Ljava/util/Map;

    const-string v1, "GSUB"

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lorg/apache/fontbox/ttf/TrueTypeFont;->tables:Ljava/util/Map;

    const-string v1, "JSTF"

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method public isPostScript()Z
    .locals 2

    iget-object v0, p0, Lorg/apache/fontbox/ttf/TrueTypeFont;->tables:Ljava/util/Map;

    const-string v1, "CFF "

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.class public interface abstract Lorg/apache/fontbox/ttf/Type1Equivalent;
.super Ljava/lang/Object;
.source "SourceFile"


# virtual methods
.method public abstract getEncoding()Lorg/apache/fontbox/encoding/Encoding;
.end method

.method public abstract getFontBBox()Lorg/apache/fontbox/util/BoundingBox;
.end method

.method public abstract getName()Ljava/lang/String;
.end method

.method public abstract getPath(Ljava/lang/String;)Landroid/graphics/Path;
.end method

.method public abstract getWidth(Ljava/lang/String;)F
.end method

.method public abstract hasGlyph(Ljava/lang/String;)Z
.end method

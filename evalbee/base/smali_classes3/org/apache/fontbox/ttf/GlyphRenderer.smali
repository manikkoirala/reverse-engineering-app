.class Lorg/apache/fontbox/ttf/GlyphRenderer;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/fontbox/ttf/GlyphRenderer$Point;
    }
.end annotation


# instance fields
.field private glyphDescription:Lorg/apache/fontbox/ttf/GlyphDescription;


# direct methods
.method public constructor <init>(Lorg/apache/fontbox/ttf/GlyphDescription;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lorg/apache/fontbox/ttf/GlyphRenderer;->glyphDescription:Lorg/apache/fontbox/ttf/GlyphDescription;

    return-void
.end method

.method private calculatePath([Lorg/apache/fontbox/ttf/GlyphRenderer$Point;)Landroid/graphics/Path;
    .locals 11

    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    array-length v1, p1

    const/4 v2, 0x0

    move v3, v2

    move v4, v3

    :goto_0
    if-ge v3, v1, :cond_7

    aget-object v5, p1, v3

    invoke-static {v5}, Lorg/apache/fontbox/ttf/GlyphRenderer$Point;->access$000(Lorg/apache/fontbox/ttf/GlyphRenderer$Point;)Z

    move-result v5

    if-eqz v5, :cond_6

    aget-object v5, p1, v4

    aget-object v6, p1, v3

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    move v8, v4

    :goto_1
    if-gt v8, v3, :cond_0

    aget-object v9, p1, v8

    invoke-interface {v7, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    :cond_0
    aget-object v4, p1, v4

    invoke-static {v4}, Lorg/apache/fontbox/ttf/GlyphRenderer$Point;->access$100(Lorg/apache/fontbox/ttf/GlyphRenderer$Point;)Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v7, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_1
    aget-object v4, p1, v3

    invoke-static {v4}, Lorg/apache/fontbox/ttf/GlyphRenderer$Point;->access$100(Lorg/apache/fontbox/ttf/GlyphRenderer$Point;)Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v7, v2, v6}, Ljava/util/List;->add(ILjava/lang/Object;)V

    goto :goto_2

    :cond_2
    invoke-direct {p0, v5, v6}, Lorg/apache/fontbox/ttf/GlyphRenderer;->midValue(Lorg/apache/fontbox/ttf/GlyphRenderer$Point;Lorg/apache/fontbox/ttf/GlyphRenderer$Point;)Lorg/apache/fontbox/ttf/GlyphRenderer$Point;

    move-result-object v4

    invoke-interface {v7, v2, v4}, Ljava/util/List;->add(ILjava/lang/Object;)V

    invoke-interface {v7, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_2
    invoke-interface {v7, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lorg/apache/fontbox/ttf/GlyphRenderer$Point;

    invoke-direct {p0, v0, v4}, Lorg/apache/fontbox/ttf/GlyphRenderer;->moveTo(Landroid/graphics/Path;Lorg/apache/fontbox/ttf/GlyphRenderer$Point;)V

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v4

    const/4 v5, 0x1

    move v6, v5

    :goto_3
    if-ge v6, v4, :cond_5

    invoke-interface {v7, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lorg/apache/fontbox/ttf/GlyphRenderer$Point;

    invoke-static {v8}, Lorg/apache/fontbox/ttf/GlyphRenderer$Point;->access$100(Lorg/apache/fontbox/ttf/GlyphRenderer$Point;)Z

    move-result v9

    if-eqz v9, :cond_3

    invoke-direct {p0, v0, v8}, Lorg/apache/fontbox/ttf/GlyphRenderer;->lineTo(Landroid/graphics/Path;Lorg/apache/fontbox/ttf/GlyphRenderer$Point;)V

    goto :goto_4

    :cond_3
    add-int/lit8 v9, v6, 0x1

    invoke-interface {v7, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lorg/apache/fontbox/ttf/GlyphRenderer$Point;

    invoke-static {v10}, Lorg/apache/fontbox/ttf/GlyphRenderer$Point;->access$100(Lorg/apache/fontbox/ttf/GlyphRenderer$Point;)Z

    move-result v10

    if-eqz v10, :cond_4

    invoke-interface {v7, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lorg/apache/fontbox/ttf/GlyphRenderer$Point;

    invoke-direct {p0, v0, v8, v6}, Lorg/apache/fontbox/ttf/GlyphRenderer;->quadTo(Landroid/graphics/Path;Lorg/apache/fontbox/ttf/GlyphRenderer$Point;Lorg/apache/fontbox/ttf/GlyphRenderer$Point;)V

    move v6, v9

    goto :goto_4

    :cond_4
    invoke-interface {v7, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lorg/apache/fontbox/ttf/GlyphRenderer$Point;

    invoke-direct {p0, v8, v9}, Lorg/apache/fontbox/ttf/GlyphRenderer;->midValue(Lorg/apache/fontbox/ttf/GlyphRenderer$Point;Lorg/apache/fontbox/ttf/GlyphRenderer$Point;)Lorg/apache/fontbox/ttf/GlyphRenderer$Point;

    move-result-object v9

    invoke-direct {p0, v0, v8, v9}, Lorg/apache/fontbox/ttf/GlyphRenderer;->quadTo(Landroid/graphics/Path;Lorg/apache/fontbox/ttf/GlyphRenderer$Point;Lorg/apache/fontbox/ttf/GlyphRenderer$Point;)V

    :goto_4
    add-int/2addr v6, v5

    goto :goto_3

    :cond_5
    add-int/lit8 v4, v3, 0x1

    :cond_6
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    :cond_7
    return-object v0
.end method

.method private describe(Lorg/apache/fontbox/ttf/GlyphDescription;)[Lorg/apache/fontbox/ttf/GlyphRenderer$Point;
    .locals 10

    invoke-interface {p1}, Lorg/apache/fontbox/ttf/GlyphDescription;->getPointCount()I

    move-result v0

    new-array v0, v0, [Lorg/apache/fontbox/ttf/GlyphRenderer$Point;

    const/4 v1, 0x0

    move v2, v1

    move v3, v2

    :goto_0
    invoke-interface {p1}, Lorg/apache/fontbox/ttf/GlyphDescription;->getPointCount()I

    move-result v4

    if-ge v2, v4, :cond_3

    invoke-interface {p1, v3}, Lorg/apache/fontbox/ttf/GlyphDescription;->getEndPtOfContours(I)I

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v2, :cond_0

    move v4, v5

    goto :goto_1

    :cond_0
    move v4, v1

    :goto_1
    if-eqz v4, :cond_1

    add-int/lit8 v3, v3, 0x1

    :cond_1
    new-instance v6, Lorg/apache/fontbox/ttf/GlyphRenderer$Point;

    invoke-interface {p1, v2}, Lorg/apache/fontbox/ttf/GlyphDescription;->getXCoordinate(I)S

    move-result v7

    invoke-interface {p1, v2}, Lorg/apache/fontbox/ttf/GlyphDescription;->getYCoordinate(I)S

    move-result v8

    invoke-interface {p1, v2}, Lorg/apache/fontbox/ttf/GlyphDescription;->getFlags(I)B

    move-result v9

    and-int/2addr v9, v5

    if-eqz v9, :cond_2

    goto :goto_2

    :cond_2
    move v5, v1

    :goto_2
    invoke-direct {v6, v7, v8, v5, v4}, Lorg/apache/fontbox/ttf/GlyphRenderer$Point;-><init>(IIZZ)V

    aput-object v6, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_3
    return-object v0
.end method

.method private lineTo(Landroid/graphics/Path;Lorg/apache/fontbox/ttf/GlyphRenderer$Point;)V
    .locals 2

    invoke-static {p2}, Lorg/apache/fontbox/ttf/GlyphRenderer$Point;->access$200(Lorg/apache/fontbox/ttf/GlyphRenderer$Point;)I

    move-result v0

    int-to-float v0, v0

    invoke-static {p2}, Lorg/apache/fontbox/ttf/GlyphRenderer$Point;->access$300(Lorg/apache/fontbox/ttf/GlyphRenderer$Point;)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "lineTo: "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {p2}, Lorg/apache/fontbox/ttf/GlyphRenderer$Point;->access$200(Lorg/apache/fontbox/ttf/GlyphRenderer$Point;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {p2}, Lorg/apache/fontbox/ttf/GlyphRenderer$Point;->access$300(Lorg/apache/fontbox/ttf/GlyphRenderer$Point;)I

    move-result p2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    filled-new-array {v0, p2}, [Ljava/lang/Object;

    move-result-object p2

    const-string v0, "%d,%d"

    invoke-static {v0, p2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string p2, "PdfBoxAndroid"

    invoke-static {p2, p1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method private midValue(II)I
    .locals 0

    .line 1
    sub-int/2addr p2, p1

    div-int/lit8 p2, p2, 0x2

    add-int/2addr p1, p2

    return p1
.end method

.method private midValue(Lorg/apache/fontbox/ttf/GlyphRenderer$Point;Lorg/apache/fontbox/ttf/GlyphRenderer$Point;)Lorg/apache/fontbox/ttf/GlyphRenderer$Point;
    .locals 3

    .line 2
    new-instance v0, Lorg/apache/fontbox/ttf/GlyphRenderer$Point;

    invoke-static {p1}, Lorg/apache/fontbox/ttf/GlyphRenderer$Point;->access$200(Lorg/apache/fontbox/ttf/GlyphRenderer$Point;)I

    move-result v1

    invoke-static {p2}, Lorg/apache/fontbox/ttf/GlyphRenderer$Point;->access$200(Lorg/apache/fontbox/ttf/GlyphRenderer$Point;)I

    move-result v2

    invoke-direct {p0, v1, v2}, Lorg/apache/fontbox/ttf/GlyphRenderer;->midValue(II)I

    move-result v1

    invoke-static {p1}, Lorg/apache/fontbox/ttf/GlyphRenderer$Point;->access$300(Lorg/apache/fontbox/ttf/GlyphRenderer$Point;)I

    move-result p1

    invoke-static {p2}, Lorg/apache/fontbox/ttf/GlyphRenderer$Point;->access$300(Lorg/apache/fontbox/ttf/GlyphRenderer$Point;)I

    move-result p2

    invoke-direct {p0, p1, p2}, Lorg/apache/fontbox/ttf/GlyphRenderer;->midValue(II)I

    move-result p1

    invoke-direct {v0, v1, p1}, Lorg/apache/fontbox/ttf/GlyphRenderer$Point;-><init>(II)V

    return-object v0
.end method

.method private moveTo(Landroid/graphics/Path;Lorg/apache/fontbox/ttf/GlyphRenderer$Point;)V
    .locals 2

    invoke-static {p2}, Lorg/apache/fontbox/ttf/GlyphRenderer$Point;->access$200(Lorg/apache/fontbox/ttf/GlyphRenderer$Point;)I

    move-result v0

    int-to-float v0, v0

    invoke-static {p2}, Lorg/apache/fontbox/ttf/GlyphRenderer$Point;->access$300(Lorg/apache/fontbox/ttf/GlyphRenderer$Point;)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Path;->moveTo(FF)V

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "moveTo: "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {p2}, Lorg/apache/fontbox/ttf/GlyphRenderer$Point;->access$200(Lorg/apache/fontbox/ttf/GlyphRenderer$Point;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {p2}, Lorg/apache/fontbox/ttf/GlyphRenderer$Point;->access$300(Lorg/apache/fontbox/ttf/GlyphRenderer$Point;)I

    move-result p2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    filled-new-array {v0, p2}, [Ljava/lang/Object;

    move-result-object p2

    const-string v0, "%d,%d"

    invoke-static {v0, p2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string p2, "PdfBoxAndroid"

    invoke-static {p2, p1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method private quadTo(Landroid/graphics/Path;Lorg/apache/fontbox/ttf/GlyphRenderer$Point;Lorg/apache/fontbox/ttf/GlyphRenderer$Point;)V
    .locals 4

    invoke-static {p2}, Lorg/apache/fontbox/ttf/GlyphRenderer$Point;->access$200(Lorg/apache/fontbox/ttf/GlyphRenderer$Point;)I

    move-result v0

    int-to-float v0, v0

    invoke-static {p2}, Lorg/apache/fontbox/ttf/GlyphRenderer$Point;->access$300(Lorg/apache/fontbox/ttf/GlyphRenderer$Point;)I

    move-result v1

    int-to-float v1, v1

    invoke-static {p3}, Lorg/apache/fontbox/ttf/GlyphRenderer$Point;->access$200(Lorg/apache/fontbox/ttf/GlyphRenderer$Point;)I

    move-result v2

    int-to-float v2, v2

    invoke-static {p3}, Lorg/apache/fontbox/ttf/GlyphRenderer$Point;->access$300(Lorg/apache/fontbox/ttf/GlyphRenderer$Point;)I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Path;->quadTo(FFFF)V

    new-instance p1, Ljava/lang/StringBuilder;

    invoke-direct {p1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "quadTo: "

    invoke-virtual {p1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {p2}, Lorg/apache/fontbox/ttf/GlyphRenderer$Point;->access$200(Lorg/apache/fontbox/ttf/GlyphRenderer$Point;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {p2}, Lorg/apache/fontbox/ttf/GlyphRenderer$Point;->access$300(Lorg/apache/fontbox/ttf/GlyphRenderer$Point;)I

    move-result p2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p2

    invoke-static {p3}, Lorg/apache/fontbox/ttf/GlyphRenderer$Point;->access$200(Lorg/apache/fontbox/ttf/GlyphRenderer$Point;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p3}, Lorg/apache/fontbox/ttf/GlyphRenderer$Point;->access$300(Lorg/apache/fontbox/ttf/GlyphRenderer$Point;)I

    move-result p3

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p3

    filled-new-array {v0, p2, v1, p3}, [Ljava/lang/Object;

    move-result-object p2

    const-string p3, "%d,%d %d,%d"

    invoke-static {p3, p2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p2

    invoke-virtual {p1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    const-string p2, "PdfBoxAndroid"

    invoke-static {p2, p1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method


# virtual methods
.method public getPath()Landroid/graphics/Path;
    .locals 1

    iget-object v0, p0, Lorg/apache/fontbox/ttf/GlyphRenderer;->glyphDescription:Lorg/apache/fontbox/ttf/GlyphDescription;

    invoke-direct {p0, v0}, Lorg/apache/fontbox/ttf/GlyphRenderer;->describe(Lorg/apache/fontbox/ttf/GlyphDescription;)[Lorg/apache/fontbox/ttf/GlyphRenderer$Point;

    move-result-object v0

    invoke-direct {p0, v0}, Lorg/apache/fontbox/ttf/GlyphRenderer;->calculatePath([Lorg/apache/fontbox/ttf/GlyphRenderer$Point;)Landroid/graphics/Path;

    move-result-object v0

    return-object v0
.end method

.class public Lorg/apache/fontbox/ttf/CFFTable;
.super Lorg/apache/fontbox/ttf/TTFTable;
.source "SourceFile"


# static fields
.field public static final TAG:Ljava/lang/String; = "CFF "


# instance fields
.field private cffFont:Lorg/apache/fontbox/cff/CFFFont;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lorg/apache/fontbox/ttf/TTFTable;-><init>()V

    return-void
.end method


# virtual methods
.method public getFont()Lorg/apache/fontbox/cff/CFFFont;
    .locals 1

    iget-object v0, p0, Lorg/apache/fontbox/ttf/CFFTable;->cffFont:Lorg/apache/fontbox/cff/CFFFont;

    return-object v0
.end method

.method public read(Lorg/apache/fontbox/ttf/TrueTypeFont;Lorg/apache/fontbox/ttf/TTFDataStream;)V
    .locals 2

    invoke-virtual {p0}, Lorg/apache/fontbox/ttf/TTFTable;->getLength()J

    move-result-wide v0

    long-to-int p1, v0

    invoke-virtual {p2, p1}, Lorg/apache/fontbox/ttf/TTFDataStream;->read(I)[B

    move-result-object p1

    new-instance p2, Lorg/apache/fontbox/cff/CFFParser;

    invoke-direct {p2}, Lorg/apache/fontbox/cff/CFFParser;-><init>()V

    invoke-virtual {p2, p1}, Lorg/apache/fontbox/cff/CFFParser;->parse([B)Ljava/util/List;

    move-result-object p1

    const/4 p2, 0x0

    invoke-interface {p1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object p1

    check-cast p1, Lorg/apache/fontbox/cff/CFFFont;

    iput-object p1, p0, Lorg/apache/fontbox/ttf/CFFTable;->cffFont:Lorg/apache/fontbox/cff/CFFFont;

    const/4 p1, 0x1

    iput-boolean p1, p0, Lorg/apache/fontbox/ttf/TTFTable;->initialized:Z

    return-void
.end method

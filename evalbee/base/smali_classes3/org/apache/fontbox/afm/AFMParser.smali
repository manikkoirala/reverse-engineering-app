.class public Lorg/apache/fontbox/afm/AFMParser;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final ASCENDER:Ljava/lang/String; = "Ascender"

.field private static final BITS_IN_HEX:I = 0x10

.field public static final CAP_HEIGHT:Ljava/lang/String; = "CapHeight"

.field public static final CC:Ljava/lang/String; = "CC"

.field public static final CHARACTERS:Ljava/lang/String; = "Characters"

.field public static final CHARACTER_SET:Ljava/lang/String; = "CharacterSet"

.field public static final CHARMETRICS_B:Ljava/lang/String; = "B"

.field public static final CHARMETRICS_C:Ljava/lang/String; = "C"

.field public static final CHARMETRICS_CH:Ljava/lang/String; = "CH"

.field public static final CHARMETRICS_L:Ljava/lang/String; = "L"

.field public static final CHARMETRICS_N:Ljava/lang/String; = "N"

.field public static final CHARMETRICS_VV:Ljava/lang/String; = "VV"

.field public static final CHARMETRICS_W:Ljava/lang/String; = "W"

.field public static final CHARMETRICS_W0:Ljava/lang/String; = "W0"

.field public static final CHARMETRICS_W0X:Ljava/lang/String; = "W0X"

.field public static final CHARMETRICS_W0Y:Ljava/lang/String; = "W0Y"

.field public static final CHARMETRICS_W1:Ljava/lang/String; = "W1"

.field public static final CHARMETRICS_W1X:Ljava/lang/String; = "W1X"

.field public static final CHARMETRICS_W1Y:Ljava/lang/String; = "W1Y"

.field public static final CHARMETRICS_WX:Ljava/lang/String; = "WX"

.field public static final CHARMETRICS_WY:Ljava/lang/String; = "WY"

.field public static final CHAR_WIDTH:Ljava/lang/String; = "CharWidth"

.field public static final COMMENT:Ljava/lang/String; = "Comment"

.field public static final DESCENDER:Ljava/lang/String; = "Descender"

.field public static final ENCODING_SCHEME:Ljava/lang/String; = "EncodingScheme"

.field public static final END_CHAR_METRICS:Ljava/lang/String; = "EndCharMetrics"

.field public static final END_COMPOSITES:Ljava/lang/String; = "EndComposites"

.field public static final END_FONT_METRICS:Ljava/lang/String; = "EndFontMetrics"

.field public static final END_KERN_DATA:Ljava/lang/String; = "EndKernData"

.field public static final END_KERN_PAIRS:Ljava/lang/String; = "EndKernPairs"

.field public static final END_TRACK_KERN:Ljava/lang/String; = "EndTrackKern"

.field public static final ESC_CHAR:Ljava/lang/String; = "EscChar"

.field public static final FAMILY_NAME:Ljava/lang/String; = "FamilyName"

.field public static final FONT_BBOX:Ljava/lang/String; = "FontBBox"

.field public static final FONT_NAME:Ljava/lang/String; = "FontName"

.field public static final FULL_NAME:Ljava/lang/String; = "FullName"

.field public static final IS_BASE_FONT:Ljava/lang/String; = "IsBaseFont"

.field public static final IS_FIXED_PITCH:Ljava/lang/String; = "IsFixedPitch"

.field public static final IS_FIXED_V:Ljava/lang/String; = "IsFixedV"

.field public static final ITALIC_ANGLE:Ljava/lang/String; = "ItalicAngle"

.field public static final KERN_PAIR_KP:Ljava/lang/String; = "KP"

.field public static final KERN_PAIR_KPH:Ljava/lang/String; = "KPH"

.field public static final KERN_PAIR_KPX:Ljava/lang/String; = "KPX"

.field public static final KERN_PAIR_KPY:Ljava/lang/String; = "KPY"

.field public static final MAPPING_SCHEME:Ljava/lang/String; = "MappingScheme"

.field public static final NOTICE:Ljava/lang/String; = "Notice"

.field public static final PCC:Ljava/lang/String; = "PCC"

.field public static final START_CHAR_METRICS:Ljava/lang/String; = "StartCharMetrics"

.field public static final START_COMPOSITES:Ljava/lang/String; = "StartComposites"

.field public static final START_FONT_METRICS:Ljava/lang/String; = "StartFontMetrics"

.field public static final START_KERN_DATA:Ljava/lang/String; = "StartKernData"

.field public static final START_KERN_PAIRS:Ljava/lang/String; = "StartKernPairs"

.field public static final START_KERN_PAIRS0:Ljava/lang/String; = "StartKernPairs0"

.field public static final START_KERN_PAIRS1:Ljava/lang/String; = "StartKernPairs1"

.field public static final START_TRACK_KERN:Ljava/lang/String; = "StartTrackKern"

.field public static final STD_HW:Ljava/lang/String; = "StdHW"

.field public static final STD_VW:Ljava/lang/String; = "StdVW"

.field public static final UNDERLINE_POSITION:Ljava/lang/String; = "UnderlinePosition"

.field public static final UNDERLINE_THICKNESS:Ljava/lang/String; = "UnderlineThickness"

.field public static final VERSION:Ljava/lang/String; = "Version"

.field public static final V_VECTOR:Ljava/lang/String; = "VVector"

.field public static final WEIGHT:Ljava/lang/String; = "Weight"

.field public static final X_HEIGHT:Ljava/lang/String; = "XHeight"


# instance fields
.field private final input:Ljava/io/InputStream;


# direct methods
.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lorg/apache/fontbox/afm/AFMParser;->input:Ljava/io/InputStream;

    return-void
.end method

.method private static hexToString(Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    const/4 v1, 0x2

    if-lt v0, v1, :cond_2

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v3, 0x3c

    if-ne v2, v3, :cond_1

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    const/4 v3, 0x1

    sub-int/2addr v2, v3

    invoke-virtual {p0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v2

    const/16 v4, 0x3e

    if-ne v2, v4, :cond_1

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    sub-int/2addr v2, v3

    invoke-virtual {p0, v3, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    div-int/2addr v2, v1

    new-array v1, v2, [B

    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v0, v2, :cond_0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    add-int/lit8 v3, v0, 0x1

    invoke-virtual {p0, v3}, Ljava/lang/String;->charAt(I)C

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    :try_start_0
    div-int/lit8 v3, v0, 0x2

    const/16 v4, 0x10

    invoke-static {v2, v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v2

    int-to-byte v2, v2

    aput-byte v2, v1, v3
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    add-int/lit8 v0, v0, 0x2

    goto :goto_0

    :catch_0
    move-exception p0

    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Error parsing AFM file:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    new-instance p0, Ljava/lang/String;

    const-string v0, "ISO-8859-1"

    invoke-direct {p0, v1, v0}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    return-object p0

    :cond_1
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "String should be enclosed by angle brackets \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p0, "\'"

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Error: Expected hex string of length >= 2 not=\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static isEOL(I)Z
    .locals 1

    const/16 v0, 0xd

    if-eq p0, v0, :cond_1

    const/16 v0, 0xa

    if-ne p0, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p0, 0x1

    :goto_1
    return p0
.end method

.method private static isWhitespace(I)Z
    .locals 1

    const/16 v0, 0x20

    if-eq p0, v0, :cond_1

    const/16 v0, 0x9

    if-eq p0, v0, :cond_1

    const/16 v0, 0xd

    if-eq p0, v0, :cond_1

    const/16 v0, 0xa

    if-ne p0, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p0, 0x1

    :goto_1
    return p0
.end method

.method public static main([Ljava/lang/String;)V
    .locals 10

    new-instance p0, Ljava/io/File;

    const-string v0, "Resources/afm"

    invoke-direct {p0, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object p0

    if-eqz p0, :cond_1

    array-length v0, p0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_1

    aget-object v2, p0, v1

    invoke-virtual {v2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v3

    const-string v4, ".AFM"

    invoke-virtual {v3, v4}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    new-instance v5, Ljava/io/FileInputStream;

    invoke-direct {v5, v2}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    new-instance v6, Lorg/apache/fontbox/afm/AFMParser;

    invoke-direct {v6, v5}, Lorg/apache/fontbox/afm/AFMParser;-><init>(Ljava/io/InputStream;)V

    invoke-virtual {v6}, Lorg/apache/fontbox/afm/AFMParser;->parse()Lorg/apache/fontbox/afm/FontMetrics;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    sget-object v7, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Parsing:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " "

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sub-long/2addr v5, v3

    invoke-virtual {v8, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v7, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method private parseCharMetric()Lorg/apache/fontbox/afm/CharMetric;
    .locals 7

    new-instance v0, Lorg/apache/fontbox/afm/CharMetric;

    invoke-direct {v0}, Lorg/apache/fontbox/afm/CharMetric;-><init>()V

    invoke-direct {p0}, Lorg/apache/fontbox/afm/AFMParser;->readLine()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/util/StringTokenizer;

    invoke-direct {v2, v1}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;)V

    :goto_0
    :try_start_0
    invoke-virtual {v2}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v1

    if-eqz v1, :cond_f

    invoke-virtual {v2}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v1

    const-string v3, "C"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v2}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Lorg/apache/fontbox/afm/CharMetric;->setCharacterCode(I)V

    :goto_1
    invoke-static {v2}, Lorg/apache/fontbox/afm/AFMParser;->verifySemicolon(Ljava/util/StringTokenizer;)V

    goto :goto_0

    :cond_0
    const-string v3, "CH"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v2}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v1

    const/16 v3, 0x10

    invoke-static {v1, v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I

    move-result v1

    invoke-virtual {v0, v1}, Lorg/apache/fontbox/afm/CharMetric;->setCharacterCode(I)V

    goto :goto_1

    :cond_1
    const-string v3, "WX"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {v2}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v1

    invoke-virtual {v0, v1}, Lorg/apache/fontbox/afm/CharMetric;->setWx(F)V

    goto :goto_1

    :cond_2
    const-string v3, "W0X"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {v2}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v1

    invoke-virtual {v0, v1}, Lorg/apache/fontbox/afm/CharMetric;->setW0x(F)V

    goto :goto_1

    :cond_3
    const-string v3, "W1X"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-virtual {v2}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v1

    invoke-virtual {v0, v1}, Lorg/apache/fontbox/afm/CharMetric;->setW0x(F)V

    goto :goto_1

    :cond_4
    const-string v3, "WY"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-virtual {v2}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v1

    invoke-virtual {v0, v1}, Lorg/apache/fontbox/afm/CharMetric;->setWy(F)V

    goto :goto_1

    :cond_5
    const-string v3, "W0Y"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-virtual {v2}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v1

    invoke-virtual {v0, v1}, Lorg/apache/fontbox/afm/CharMetric;->setW0y(F)V

    goto :goto_1

    :cond_6
    const-string v3, "W1Y"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    invoke-virtual {v2}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v1

    invoke-virtual {v0, v1}, Lorg/apache/fontbox/afm/CharMetric;->setW0y(F)V

    goto/16 :goto_1

    :cond_7
    const-string v3, "W"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v6, 0x2

    if-eqz v3, :cond_8

    invoke-virtual {v2}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v3

    new-array v6, v6, [F

    invoke-static {v1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v1

    aput v1, v6, v5

    invoke-static {v3}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v1

    aput v1, v6, v4

    invoke-virtual {v0, v6}, Lorg/apache/fontbox/afm/CharMetric;->setW([F)V

    goto/16 :goto_1

    :cond_8
    const-string v3, "W0"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    invoke-virtual {v2}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v3

    new-array v6, v6, [F

    invoke-static {v1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v1

    aput v1, v6, v5

    invoke-static {v3}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v1

    aput v1, v6, v4

    invoke-virtual {v0, v6}, Lorg/apache/fontbox/afm/CharMetric;->setW0([F)V

    goto/16 :goto_1

    :cond_9
    const-string v3, "W1"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_a

    invoke-virtual {v2}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v3

    new-array v6, v6, [F

    invoke-static {v1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v1

    aput v1, v6, v5

    invoke-static {v3}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v1

    aput v1, v6, v4

    invoke-virtual {v0, v6}, Lorg/apache/fontbox/afm/CharMetric;->setW1([F)V

    goto/16 :goto_1

    :cond_a
    const-string v3, "VV"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_b

    invoke-virtual {v2}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v3

    new-array v6, v6, [F

    invoke-static {v1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v1

    aput v1, v6, v5

    invoke-static {v3}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v1

    aput v1, v6, v4

    invoke-virtual {v0, v6}, Lorg/apache/fontbox/afm/CharMetric;->setVv([F)V

    goto/16 :goto_1

    :cond_b
    const-string v3, "N"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_c

    invoke-virtual {v2}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/fontbox/afm/CharMetric;->setName(Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_c
    const-string v3, "B"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_d

    invoke-virtual {v2}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v5

    new-instance v6, Lorg/apache/fontbox/util/BoundingBox;

    invoke-direct {v6}, Lorg/apache/fontbox/util/BoundingBox;-><init>()V

    invoke-static {v1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v1

    invoke-virtual {v6, v1}, Lorg/apache/fontbox/util/BoundingBox;->setLowerLeftX(F)V

    invoke-static {v3}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v1

    invoke-virtual {v6, v1}, Lorg/apache/fontbox/util/BoundingBox;->setLowerLeftY(F)V

    invoke-static {v4}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v1

    invoke-virtual {v6, v1}, Lorg/apache/fontbox/util/BoundingBox;->setUpperRightX(F)V

    invoke-static {v5}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v1

    invoke-virtual {v6, v1}, Lorg/apache/fontbox/util/BoundingBox;->setUpperRightY(F)V

    invoke-virtual {v0, v6}, Lorg/apache/fontbox/afm/CharMetric;->setBoundingBox(Lorg/apache/fontbox/util/BoundingBox;)V

    goto/16 :goto_1

    :cond_d
    const-string v3, "L"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_e

    invoke-virtual {v2}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lorg/apache/fontbox/afm/Ligature;

    invoke-direct {v4}, Lorg/apache/fontbox/afm/Ligature;-><init>()V

    invoke-virtual {v4, v1}, Lorg/apache/fontbox/afm/Ligature;->setSuccessor(Ljava/lang/String;)V

    invoke-virtual {v4, v3}, Lorg/apache/fontbox/afm/Ligature;->setLigature(Ljava/lang/String;)V

    invoke-virtual {v0, v4}, Lorg/apache/fontbox/afm/CharMetric;->addLigature(Lorg/apache/fontbox/afm/Ligature;)V

    goto/16 :goto_1

    :cond_e
    new-instance v0, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unknown CharMetrics command \'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\'"

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_f
    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error: Corrupt AFM document:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private parseComposite()Lorg/apache/fontbox/afm/Composite;
    .locals 10

    const-string v0, "Error parsing AFM document:"

    new-instance v1, Lorg/apache/fontbox/afm/Composite;

    invoke-direct {v1}, Lorg/apache/fontbox/afm/Composite;-><init>()V

    invoke-direct {p0}, Lorg/apache/fontbox/afm/AFMParser;->readLine()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/util/StringTokenizer;

    const-string v4, " ;"

    invoke-direct {v3, v2, v4}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v2

    const-string v4, "CC"

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    const-string v5, "\'"

    if-eqz v4, :cond_2

    invoke-virtual {v3}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lorg/apache/fontbox/afm/Composite;->setName(Ljava/lang/String;)V

    :try_start_0
    invoke-virtual {v3}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_1

    const/4 v4, 0x0

    :goto_0
    if-ge v4, v2, :cond_1

    new-instance v6, Lorg/apache/fontbox/afm/CompositePart;

    invoke-direct {v6}, Lorg/apache/fontbox/afm/CompositePart;-><init>()V

    invoke-virtual {v3}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v7

    const-string v8, "PCC"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    invoke-virtual {v3}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v7

    :try_start_1
    invoke-virtual {v3}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    invoke-virtual {v3}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v9

    invoke-virtual {v6, v7}, Lorg/apache/fontbox/afm/CompositePart;->setName(Ljava/lang/String;)V

    invoke-virtual {v6, v8}, Lorg/apache/fontbox/afm/CompositePart;->setXDisplacement(I)V

    invoke-virtual {v6, v9}, Lorg/apache/fontbox/afm/CompositePart;->setYDisplacement(I)V

    invoke-virtual {v1, v6}, Lorg/apache/fontbox/afm/Composite;->addPart(Lorg/apache/fontbox/afm/CompositePart;)V
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0

    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :catch_0
    move-exception v1

    new-instance v2, Ljava/io/IOException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_0
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Expected \'PCC\' actual=\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    return-object v1

    :catch_1
    move-exception v1

    new-instance v2, Ljava/io/IOException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_2
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Expected \'CC\' actual=\'"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private parseFontMetric()Lorg/apache/fontbox/afm/FontMetrics;
    .locals 7

    new-instance v0, Lorg/apache/fontbox/afm/FontMetrics;

    invoke-direct {v0}, Lorg/apache/fontbox/afm/FontMetrics;-><init>()V

    invoke-direct {p0}, Lorg/apache/fontbox/afm/AFMParser;->readString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "StartFontMetrics"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    const-string v3, "\'"

    if-eqz v2, :cond_23

    invoke-direct {p0}, Lorg/apache/fontbox/afm/AFMParser;->readFloat()F

    move-result v1

    invoke-virtual {v0, v1}, Lorg/apache/fontbox/afm/FontMetrics;->setAFMVersion(F)V

    :goto_0
    invoke-direct {p0}, Lorg/apache/fontbox/afm/AFMParser;->readString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "EndFontMetrics"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_22

    const-string v2, "FontName"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-direct {p0}, Lorg/apache/fontbox/afm/AFMParser;->readLine()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/fontbox/afm/FontMetrics;->setFontName(Ljava/lang/String;)V

    goto :goto_0

    :cond_0
    const-string v2, "FullName"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-direct {p0}, Lorg/apache/fontbox/afm/AFMParser;->readLine()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/fontbox/afm/FontMetrics;->setFullName(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    const-string v2, "FamilyName"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-direct {p0}, Lorg/apache/fontbox/afm/AFMParser;->readLine()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/fontbox/afm/FontMetrics;->setFamilyName(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    const-string v2, "Weight"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-direct {p0}, Lorg/apache/fontbox/afm/AFMParser;->readLine()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/fontbox/afm/FontMetrics;->setWeight(Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    const-string v2, "FontBBox"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    new-instance v1, Lorg/apache/fontbox/util/BoundingBox;

    invoke-direct {v1}, Lorg/apache/fontbox/util/BoundingBox;-><init>()V

    invoke-direct {p0}, Lorg/apache/fontbox/afm/AFMParser;->readFloat()F

    move-result v2

    invoke-virtual {v1, v2}, Lorg/apache/fontbox/util/BoundingBox;->setLowerLeftX(F)V

    invoke-direct {p0}, Lorg/apache/fontbox/afm/AFMParser;->readFloat()F

    move-result v2

    invoke-virtual {v1, v2}, Lorg/apache/fontbox/util/BoundingBox;->setLowerLeftY(F)V

    invoke-direct {p0}, Lorg/apache/fontbox/afm/AFMParser;->readFloat()F

    move-result v2

    invoke-virtual {v1, v2}, Lorg/apache/fontbox/util/BoundingBox;->setUpperRightX(F)V

    invoke-direct {p0}, Lorg/apache/fontbox/afm/AFMParser;->readFloat()F

    move-result v2

    invoke-virtual {v1, v2}, Lorg/apache/fontbox/util/BoundingBox;->setUpperRightY(F)V

    invoke-virtual {v0, v1}, Lorg/apache/fontbox/afm/FontMetrics;->setFontBBox(Lorg/apache/fontbox/util/BoundingBox;)V

    goto :goto_0

    :cond_4
    const-string v2, "Version"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-direct {p0}, Lorg/apache/fontbox/afm/AFMParser;->readLine()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/fontbox/afm/FontMetrics;->setFontVersion(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_5
    const-string v2, "Notice"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-direct {p0}, Lorg/apache/fontbox/afm/AFMParser;->readLine()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/fontbox/afm/FontMetrics;->setNotice(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_6
    const-string v2, "EncodingScheme"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-direct {p0}, Lorg/apache/fontbox/afm/AFMParser;->readLine()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/fontbox/afm/FontMetrics;->setEncodingScheme(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_7
    const-string v2, "MappingScheme"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-direct {p0}, Lorg/apache/fontbox/afm/AFMParser;->readInt()I

    move-result v1

    invoke-virtual {v0, v1}, Lorg/apache/fontbox/afm/FontMetrics;->setMappingScheme(I)V

    goto/16 :goto_0

    :cond_8
    const-string v2, "EscChar"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    invoke-direct {p0}, Lorg/apache/fontbox/afm/AFMParser;->readInt()I

    move-result v1

    invoke-virtual {v0, v1}, Lorg/apache/fontbox/afm/FontMetrics;->setEscChar(I)V

    goto/16 :goto_0

    :cond_9
    const-string v2, "CharacterSet"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    invoke-direct {p0}, Lorg/apache/fontbox/afm/AFMParser;->readLine()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/fontbox/afm/FontMetrics;->setCharacterSet(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_a
    const-string v2, "Characters"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_b

    invoke-direct {p0}, Lorg/apache/fontbox/afm/AFMParser;->readInt()I

    move-result v1

    invoke-virtual {v0, v1}, Lorg/apache/fontbox/afm/FontMetrics;->setCharacters(I)V

    goto/16 :goto_0

    :cond_b
    const-string v2, "IsBaseFont"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_c

    invoke-direct {p0}, Lorg/apache/fontbox/afm/AFMParser;->readBoolean()Z

    move-result v1

    invoke-virtual {v0, v1}, Lorg/apache/fontbox/afm/FontMetrics;->setIsBaseFont(Z)V

    goto/16 :goto_0

    :cond_c
    const-string v2, "VVector"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    const/4 v4, 0x2

    const/4 v5, 0x1

    const/4 v6, 0x0

    if-eqz v2, :cond_d

    new-array v1, v4, [F

    invoke-direct {p0}, Lorg/apache/fontbox/afm/AFMParser;->readFloat()F

    move-result v2

    aput v2, v1, v6

    invoke-direct {p0}, Lorg/apache/fontbox/afm/AFMParser;->readFloat()F

    move-result v2

    aput v2, v1, v5

    invoke-virtual {v0, v1}, Lorg/apache/fontbox/afm/FontMetrics;->setVVector([F)V

    goto/16 :goto_0

    :cond_d
    const-string v2, "IsFixedV"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_e

    invoke-direct {p0}, Lorg/apache/fontbox/afm/AFMParser;->readBoolean()Z

    move-result v1

    invoke-virtual {v0, v1}, Lorg/apache/fontbox/afm/FontMetrics;->setIsFixedV(Z)V

    goto/16 :goto_0

    :cond_e
    const-string v2, "CapHeight"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_f

    invoke-direct {p0}, Lorg/apache/fontbox/afm/AFMParser;->readFloat()F

    move-result v1

    invoke-virtual {v0, v1}, Lorg/apache/fontbox/afm/FontMetrics;->setCapHeight(F)V

    goto/16 :goto_0

    :cond_f
    const-string v2, "XHeight"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_10

    invoke-direct {p0}, Lorg/apache/fontbox/afm/AFMParser;->readFloat()F

    move-result v1

    invoke-virtual {v0, v1}, Lorg/apache/fontbox/afm/FontMetrics;->setXHeight(F)V

    goto/16 :goto_0

    :cond_10
    const-string v2, "Ascender"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_11

    invoke-direct {p0}, Lorg/apache/fontbox/afm/AFMParser;->readFloat()F

    move-result v1

    invoke-virtual {v0, v1}, Lorg/apache/fontbox/afm/FontMetrics;->setAscender(F)V

    goto/16 :goto_0

    :cond_11
    const-string v2, "Descender"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_12

    invoke-direct {p0}, Lorg/apache/fontbox/afm/AFMParser;->readFloat()F

    move-result v1

    invoke-virtual {v0, v1}, Lorg/apache/fontbox/afm/FontMetrics;->setDescender(F)V

    goto/16 :goto_0

    :cond_12
    const-string v2, "StdHW"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_13

    invoke-direct {p0}, Lorg/apache/fontbox/afm/AFMParser;->readFloat()F

    move-result v1

    invoke-virtual {v0, v1}, Lorg/apache/fontbox/afm/FontMetrics;->setStandardHorizontalWidth(F)V

    goto/16 :goto_0

    :cond_13
    const-string v2, "StdVW"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_14

    invoke-direct {p0}, Lorg/apache/fontbox/afm/AFMParser;->readFloat()F

    move-result v1

    invoke-virtual {v0, v1}, Lorg/apache/fontbox/afm/FontMetrics;->setStandardVerticalWidth(F)V

    goto/16 :goto_0

    :cond_14
    const-string v2, "Comment"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_15

    invoke-direct {p0}, Lorg/apache/fontbox/afm/AFMParser;->readLine()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lorg/apache/fontbox/afm/FontMetrics;->addComment(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_15
    const-string v2, "UnderlinePosition"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_16

    invoke-direct {p0}, Lorg/apache/fontbox/afm/AFMParser;->readFloat()F

    move-result v1

    invoke-virtual {v0, v1}, Lorg/apache/fontbox/afm/FontMetrics;->setUnderlinePosition(F)V

    goto/16 :goto_0

    :cond_16
    const-string v2, "UnderlineThickness"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_17

    invoke-direct {p0}, Lorg/apache/fontbox/afm/AFMParser;->readFloat()F

    move-result v1

    invoke-virtual {v0, v1}, Lorg/apache/fontbox/afm/FontMetrics;->setUnderlineThickness(F)V

    goto/16 :goto_0

    :cond_17
    const-string v2, "ItalicAngle"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_18

    invoke-direct {p0}, Lorg/apache/fontbox/afm/AFMParser;->readFloat()F

    move-result v1

    invoke-virtual {v0, v1}, Lorg/apache/fontbox/afm/FontMetrics;->setItalicAngle(F)V

    goto/16 :goto_0

    :cond_18
    const-string v2, "CharWidth"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_19

    new-array v1, v4, [F

    invoke-direct {p0}, Lorg/apache/fontbox/afm/AFMParser;->readFloat()F

    move-result v2

    aput v2, v1, v6

    invoke-direct {p0}, Lorg/apache/fontbox/afm/AFMParser;->readFloat()F

    move-result v2

    aput v2, v1, v5

    invoke-virtual {v0, v1}, Lorg/apache/fontbox/afm/FontMetrics;->setCharWidth([F)V

    goto/16 :goto_0

    :cond_19
    const-string v2, "IsFixedPitch"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1a

    invoke-direct {p0}, Lorg/apache/fontbox/afm/AFMParser;->readBoolean()Z

    move-result v1

    invoke-virtual {v0, v1}, Lorg/apache/fontbox/afm/FontMetrics;->setFixedPitch(Z)V

    goto/16 :goto_0

    :cond_1a
    const-string v2, "StartCharMetrics"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1d

    invoke-direct {p0}, Lorg/apache/fontbox/afm/AFMParser;->readInt()I

    move-result v1

    :goto_1
    if-ge v6, v1, :cond_1b

    invoke-direct {p0}, Lorg/apache/fontbox/afm/AFMParser;->parseCharMetric()Lorg/apache/fontbox/afm/CharMetric;

    move-result-object v2

    invoke-virtual {v0, v2}, Lorg/apache/fontbox/afm/FontMetrics;->addCharMetric(Lorg/apache/fontbox/afm/CharMetric;)V

    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    :cond_1b
    invoke-direct {p0}, Lorg/apache/fontbox/afm/AFMParser;->readString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "EndCharMetrics"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1c

    goto/16 :goto_0

    :cond_1c
    new-instance v0, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Error: Expected \'EndCharMetrics\' actual \'"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1d
    const-string v2, "StartComposites"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_20

    invoke-direct {p0}, Lorg/apache/fontbox/afm/AFMParser;->readInt()I

    move-result v1

    :goto_2
    if-ge v6, v1, :cond_1e

    invoke-direct {p0}, Lorg/apache/fontbox/afm/AFMParser;->parseComposite()Lorg/apache/fontbox/afm/Composite;

    move-result-object v2

    invoke-virtual {v0, v2}, Lorg/apache/fontbox/afm/FontMetrics;->addComposite(Lorg/apache/fontbox/afm/Composite;)V

    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    :cond_1e
    invoke-direct {p0}, Lorg/apache/fontbox/afm/AFMParser;->readString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "EndComposites"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1f

    goto/16 :goto_0

    :cond_1f
    new-instance v0, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Error: Expected \'EndComposites\' actual \'"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_20
    const-string v2, "StartKernData"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_21

    invoke-direct {p0, v0}, Lorg/apache/fontbox/afm/AFMParser;->parseKernData(Lorg/apache/fontbox/afm/FontMetrics;)V

    goto/16 :goto_0

    :cond_21
    new-instance v0, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unknown AFM key \'"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_22
    return-object v0

    :cond_23
    new-instance v0, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Error: The AFM file should start with StartFontMetrics and not \'"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private parseKernData(Lorg/apache/fontbox/afm/FontMetrics;)V
    .locals 6

    :goto_0
    invoke-direct {p0}, Lorg/apache/fontbox/afm/AFMParser;->readString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "EndKernData"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_c

    const-string v1, "StartTrackKern"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    const/4 v2, 0x0

    const-string v3, "\'"

    if-eqz v1, :cond_2

    invoke-direct {p0}, Lorg/apache/fontbox/afm/AFMParser;->readInt()I

    move-result v0

    :goto_1
    if-ge v2, v0, :cond_0

    new-instance v1, Lorg/apache/fontbox/afm/TrackKern;

    invoke-direct {v1}, Lorg/apache/fontbox/afm/TrackKern;-><init>()V

    invoke-direct {p0}, Lorg/apache/fontbox/afm/AFMParser;->readInt()I

    move-result v4

    invoke-virtual {v1, v4}, Lorg/apache/fontbox/afm/TrackKern;->setDegree(I)V

    invoke-direct {p0}, Lorg/apache/fontbox/afm/AFMParser;->readFloat()F

    move-result v4

    invoke-virtual {v1, v4}, Lorg/apache/fontbox/afm/TrackKern;->setMinPointSize(F)V

    invoke-direct {p0}, Lorg/apache/fontbox/afm/AFMParser;->readFloat()F

    move-result v4

    invoke-virtual {v1, v4}, Lorg/apache/fontbox/afm/TrackKern;->setMinKern(F)V

    invoke-direct {p0}, Lorg/apache/fontbox/afm/AFMParser;->readFloat()F

    move-result v4

    invoke-virtual {v1, v4}, Lorg/apache/fontbox/afm/TrackKern;->setMaxPointSize(F)V

    invoke-direct {p0}, Lorg/apache/fontbox/afm/AFMParser;->readFloat()F

    move-result v4

    invoke-virtual {v1, v4}, Lorg/apache/fontbox/afm/TrackKern;->setMaxKern(F)V

    invoke-virtual {p1, v1}, Lorg/apache/fontbox/afm/FontMetrics;->addTrackKern(Lorg/apache/fontbox/afm/TrackKern;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_0
    invoke-direct {p0}, Lorg/apache/fontbox/afm/AFMParser;->readString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "EndTrackKern"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    goto :goto_0

    :cond_1
    new-instance p1, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Error: Expected \'EndTrackKern\' actual \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_2
    const-string v1, "StartKernPairs"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    const-string v4, "Error: Expected \'EndKernPairs\' actual \'"

    const-string v5, "EndKernPairs"

    if-eqz v1, :cond_5

    invoke-direct {p0}, Lorg/apache/fontbox/afm/AFMParser;->readInt()I

    move-result v0

    :goto_2
    if-ge v2, v0, :cond_3

    invoke-direct {p0}, Lorg/apache/fontbox/afm/AFMParser;->parseKernPair()Lorg/apache/fontbox/afm/KernPair;

    move-result-object v1

    invoke-virtual {p1, v1}, Lorg/apache/fontbox/afm/FontMetrics;->addKernPair(Lorg/apache/fontbox/afm/KernPair;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_3
    invoke-direct {p0}, Lorg/apache/fontbox/afm/AFMParser;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    goto/16 :goto_0

    :cond_4
    new-instance p1, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_5
    const-string v1, "StartKernPairs0"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-direct {p0}, Lorg/apache/fontbox/afm/AFMParser;->readInt()I

    move-result v0

    :goto_3
    if-ge v2, v0, :cond_6

    invoke-direct {p0}, Lorg/apache/fontbox/afm/AFMParser;->parseKernPair()Lorg/apache/fontbox/afm/KernPair;

    move-result-object v1

    invoke-virtual {p1, v1}, Lorg/apache/fontbox/afm/FontMetrics;->addKernPair0(Lorg/apache/fontbox/afm/KernPair;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_6
    invoke-direct {p0}, Lorg/apache/fontbox/afm/AFMParser;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    goto/16 :goto_0

    :cond_7
    new-instance p1, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_8
    const-string v1, "StartKernPairs1"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    invoke-direct {p0}, Lorg/apache/fontbox/afm/AFMParser;->readInt()I

    move-result v0

    :goto_4
    if-ge v2, v0, :cond_9

    invoke-direct {p0}, Lorg/apache/fontbox/afm/AFMParser;->parseKernPair()Lorg/apache/fontbox/afm/KernPair;

    move-result-object v1

    invoke-virtual {p1, v1}, Lorg/apache/fontbox/afm/FontMetrics;->addKernPair1(Lorg/apache/fontbox/afm/KernPair;)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    :cond_9
    invoke-direct {p0}, Lorg/apache/fontbox/afm/AFMParser;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    goto/16 :goto_0

    :cond_a
    new-instance p1, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_b
    new-instance p1, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown kerning data type \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_c
    return-void
.end method

.method private parseKernPair()Lorg/apache/fontbox/afm/KernPair;
    .locals 5

    new-instance v0, Lorg/apache/fontbox/afm/KernPair;

    invoke-direct {v0}, Lorg/apache/fontbox/afm/KernPair;-><init>()V

    invoke-direct {p0}, Lorg/apache/fontbox/afm/AFMParser;->readString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "KP"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-direct {p0}, Lorg/apache/fontbox/afm/AFMParser;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0}, Lorg/apache/fontbox/afm/AFMParser;->readString()Ljava/lang/String;

    move-result-object v2

    :goto_0
    invoke-direct {p0}, Lorg/apache/fontbox/afm/AFMParser;->readFloat()F

    move-result v3

    :goto_1
    invoke-direct {p0}, Lorg/apache/fontbox/afm/AFMParser;->readFloat()F

    move-result v4

    invoke-virtual {v0, v1}, Lorg/apache/fontbox/afm/KernPair;->setFirstKernCharacter(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Lorg/apache/fontbox/afm/KernPair;->setSecondKernCharacter(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Lorg/apache/fontbox/afm/KernPair;->setX(F)V

    invoke-virtual {v0, v4}, Lorg/apache/fontbox/afm/KernPair;->setY(F)V

    goto :goto_2

    :cond_0
    const-string v2, "KPH"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-direct {p0}, Lorg/apache/fontbox/afm/AFMParser;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lorg/apache/fontbox/afm/AFMParser;->hexToString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0}, Lorg/apache/fontbox/afm/AFMParser;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lorg/apache/fontbox/afm/AFMParser;->hexToString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    :cond_1
    const-string v2, "KPX"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    const/4 v3, 0x0

    if-eqz v2, :cond_2

    invoke-direct {p0}, Lorg/apache/fontbox/afm/AFMParser;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0}, Lorg/apache/fontbox/afm/AFMParser;->readString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0}, Lorg/apache/fontbox/afm/AFMParser;->readFloat()F

    move-result v4

    invoke-virtual {v0, v1}, Lorg/apache/fontbox/afm/KernPair;->setFirstKernCharacter(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Lorg/apache/fontbox/afm/KernPair;->setSecondKernCharacter(Ljava/lang/String;)V

    invoke-virtual {v0, v4}, Lorg/apache/fontbox/afm/KernPair;->setX(F)V

    invoke-virtual {v0, v3}, Lorg/apache/fontbox/afm/KernPair;->setY(F)V

    goto :goto_2

    :cond_2
    const-string v2, "KPY"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-direct {p0}, Lorg/apache/fontbox/afm/AFMParser;->readString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0}, Lorg/apache/fontbox/afm/AFMParser;->readString()Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    :goto_2
    return-object v0

    :cond_3
    new-instance v0, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error expected kern pair command actual=\'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\'"

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private readBoolean()Z
    .locals 1

    invoke-direct {p0}, Lorg/apache/fontbox/afm/AFMParser;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method private readFloat()F
    .locals 1

    invoke-direct {p0}, Lorg/apache/fontbox/afm/AFMParser;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v0

    return v0
.end method

.method private readInt()I
    .locals 4

    invoke-direct {p0}, Lorg/apache/fontbox/afm/AFMParser;->readString()Ljava/lang/String;

    move-result-object v0

    :try_start_0
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    return v0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Error parsing AFM document:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private readLine()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    :goto_0
    iget-object v1, p0, Lorg/apache/fontbox/afm/AFMParser;->input:Ljava/io/InputStream;

    invoke-virtual {v1}, Ljava/io/InputStream;->read()I

    move-result v1

    invoke-static {v1}, Lorg/apache/fontbox/afm/AFMParser;->isWhitespace(I)Z

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_0

    :cond_0
    :goto_1
    int-to-char v1, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    iget-object v1, p0, Lorg/apache/fontbox/afm/AFMParser;->input:Ljava/io/InputStream;

    invoke-virtual {v1}, Ljava/io/InputStream;->read()I

    move-result v1

    invoke-static {v1}, Lorg/apache/fontbox/afm/AFMParser;->isEOL(I)Z

    move-result v2

    if-nez v2, :cond_1

    goto :goto_1

    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private readString()Ljava/lang/String;
    .locals 3

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    :goto_0
    iget-object v1, p0, Lorg/apache/fontbox/afm/AFMParser;->input:Ljava/io/InputStream;

    invoke-virtual {v1}, Ljava/io/InputStream;->read()I

    move-result v1

    invoke-static {v1}, Lorg/apache/fontbox/afm/AFMParser;->isWhitespace(I)Z

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_0

    :cond_0
    :goto_1
    int-to-char v1, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    iget-object v1, p0, Lorg/apache/fontbox/afm/AFMParser;->input:Ljava/io/InputStream;

    invoke-virtual {v1}, Ljava/io/InputStream;->read()I

    move-result v1

    invoke-static {v1}, Lorg/apache/fontbox/afm/AFMParser;->isWhitespace(I)Z

    move-result v2

    if-nez v2, :cond_1

    goto :goto_1

    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static verifySemicolon(Ljava/util/StringTokenizer;)V
    .locals 3

    invoke-virtual {p0}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object p0

    const-string v0, ";"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Error: Expected semicolon in stream actual=\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string p0, "\'"

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    invoke-direct {v0, p0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    new-instance p0, Ljava/io/IOException;

    const-string v0, "CharMetrics is missing a semicolon after a command"

    invoke-direct {p0, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p0
.end method


# virtual methods
.method public parse()Lorg/apache/fontbox/afm/FontMetrics;
    .locals 1

    invoke-direct {p0}, Lorg/apache/fontbox/afm/AFMParser;->parseFontMetric()Lorg/apache/fontbox/afm/FontMetrics;

    move-result-object v0

    return-object v0
.end method

.class public Lorg/apache/fontbox/cmap/CodespaceRange;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private end:[B

.field private start:[B


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public getEnd()[B
    .locals 1

    iget-object v0, p0, Lorg/apache/fontbox/cmap/CodespaceRange;->end:[B

    return-object v0
.end method

.method public getStart()[B
    .locals 1

    iget-object v0, p0, Lorg/apache/fontbox/cmap/CodespaceRange;->start:[B

    return-object v0
.end method

.method public isFullMatch(Ljava/util/List;)Z
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Ljava/lang/Byte;",
            ">;)Z"
        }
    .end annotation

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    iget-object v1, p0, Lorg/apache/fontbox/cmap/CodespaceRange;->start:[B

    array-length v1, v1

    const/4 v2, 0x0

    if-lt v0, v1, :cond_3

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    iget-object v1, p0, Lorg/apache/fontbox/cmap/CodespaceRange;->end:[B

    array-length v1, v1

    if-gt v0, v1, :cond_3

    move v0, v2

    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_2

    iget-object v1, p0, Lorg/apache/fontbox/cmap/CodespaceRange;->start:[B

    aget-byte v1, v1, v0

    and-int/lit16 v1, v1, 0xff

    iget-object v3, p0, Lorg/apache/fontbox/cmap/CodespaceRange;->end:[B

    aget-byte v3, v3, v0

    and-int/lit16 v3, v3, 0xff

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Byte;

    invoke-virtual {v4}, Ljava/lang/Byte;->byteValue()B

    move-result v4

    and-int/lit16 v4, v4, 0xff

    if-gt v4, v3, :cond_1

    if-ge v4, v1, :cond_0

    goto :goto_1

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    :goto_1
    return v2

    :cond_2
    const/4 p1, 0x1

    return p1

    :cond_3
    return v2
.end method

.method public isPartialMatch(BI)Z
    .locals 2

    iget-object v0, p0, Lorg/apache/fontbox/cmap/CodespaceRange;->start:[B

    aget-byte v0, v0, p2

    and-int/lit16 v0, v0, 0xff

    iget-object v1, p0, Lorg/apache/fontbox/cmap/CodespaceRange;->end:[B

    aget-byte p2, v1, p2

    and-int/lit16 p2, p2, 0xff

    and-int/lit16 p1, p1, 0xff

    if-gt p1, p2, :cond_0

    if-lt p1, v0, :cond_0

    const/4 p1, 0x1

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    return p1
.end method

.method public matches([B)Z
    .locals 5

    array-length v0, p1

    iget-object v1, p0, Lorg/apache/fontbox/cmap/CodespaceRange;->start:[B

    array-length v1, v1

    const/4 v2, 0x0

    if-lt v0, v1, :cond_3

    array-length v0, p1

    iget-object v1, p0, Lorg/apache/fontbox/cmap/CodespaceRange;->end:[B

    array-length v1, v1

    if-gt v0, v1, :cond_3

    move v0, v2

    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_2

    iget-object v1, p0, Lorg/apache/fontbox/cmap/CodespaceRange;->start:[B

    aget-byte v1, v1, v0

    and-int/lit16 v1, v1, 0xff

    iget-object v3, p0, Lorg/apache/fontbox/cmap/CodespaceRange;->end:[B

    aget-byte v3, v3, v0

    and-int/lit16 v3, v3, 0xff

    aget-byte v4, p1, v0

    and-int/lit16 v4, v4, 0xff

    if-gt v4, v3, :cond_1

    if-ge v4, v1, :cond_0

    goto :goto_1

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    :goto_1
    return v2

    :cond_2
    const/4 p1, 0x1

    return p1

    :cond_3
    return v2
.end method

.method public setEnd([B)V
    .locals 0

    iput-object p1, p0, Lorg/apache/fontbox/cmap/CodespaceRange;->end:[B

    return-void
.end method

.method public setStart([B)V
    .locals 0

    iput-object p1, p0, Lorg/apache/fontbox/cmap/CodespaceRange;->start:[B

    return-void
.end method

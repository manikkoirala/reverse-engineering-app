.class public Lorg/apache/fontbox/cmap/CMapParser;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lorg/apache/fontbox/cmap/CMapParser$Operator;,
        Lorg/apache/fontbox/cmap/CMapParser$LiteralName;
    }
.end annotation


# static fields
.field private static final MARK_END_OF_ARRAY:Ljava/lang/String; = "]"

.field private static final MARK_END_OF_DICTIONARY:Ljava/lang/String; = ">>"


# instance fields
.field private final tokenParserByteBuffer:[B


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0x200

    new-array v0, v0, [B

    iput-object v0, p0, Lorg/apache/fontbox/cmap/CMapParser;->tokenParserByteBuffer:[B

    return-void
.end method

.method private static compare([B[B)I
    .locals 5

    array-length v0, p0

    const/4 v1, 0x0

    :goto_0
    const/4 v2, 0x1

    if-ge v1, v0, :cond_1

    aget-byte v3, p0, v1

    aget-byte v4, p1, v1

    if-ne v3, v4, :cond_0

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    add-int/lit16 v3, v3, 0x100

    rem-int/lit16 v3, v3, 0x100

    add-int/lit16 v4, v4, 0x100

    rem-int/lit16 v4, v4, 0x100

    if-ge v3, v4, :cond_1

    const/4 v2, -0x1

    :cond_1
    return v2
.end method

.method private static createIntFromBytes([B)I
    .locals 3

    const/4 v0, 0x0

    aget-byte v0, p0, v0

    add-int/lit16 v0, v0, 0x100

    rem-int/lit16 v0, v0, 0x100

    array-length v1, p0

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    shl-int/lit8 v0, v0, 0x8

    const/4 v1, 0x1

    aget-byte p0, p0, v1

    add-int/lit16 p0, p0, 0x100

    rem-int/lit16 p0, p0, 0x100

    add-int/2addr v0, p0

    :cond_0
    return v0
.end method

.method private static createStringFromBytes([B)Ljava/lang/String;
    .locals 2

    array-length v0, p0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    new-instance v0, Ljava/lang/String;

    const-string v1, "ISO-8859-1"

    invoke-direct {v0, p0, v1}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    goto :goto_0

    :cond_0
    new-instance v0, Ljava/lang/String;

    const-string v1, "UTF-16BE"

    invoke-direct {v0, p0, v1}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    :goto_0
    return-object v0
.end method

.method private increment([B)V
    .locals 1

    .line 1
    array-length v0, p1

    add-int/lit8 v0, v0, -0x1

    invoke-direct {p0, p1, v0}, Lorg/apache/fontbox/cmap/CMapParser;->increment([BI)V

    return-void
.end method

.method private increment([BI)V
    .locals 2

    .line 2
    if-lez p2, :cond_0

    aget-byte v0, p1, p2

    add-int/lit16 v0, v0, 0x100

    rem-int/lit16 v0, v0, 0x100

    const/16 v1, 0xff

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    aput-byte v0, p1, p2

    add-int/lit8 p2, p2, -0x1

    invoke-direct {p0, p1, p2}, Lorg/apache/fontbox/cmap/CMapParser;->increment([BI)V

    goto :goto_0

    :cond_0
    aget-byte v0, p1, p2

    add-int/lit8 v0, v0, 0x1

    int-to-byte v0, v0

    aput-byte v0, p1, p2

    :goto_0
    return-void
.end method

.method private static isDelimiter(I)Z
    .locals 1

    const/16 v0, 0x25

    if-eq p0, v0, :cond_0

    const/16 v0, 0x2f

    if-eq p0, v0, :cond_0

    const/16 v0, 0x3c

    if-eq p0, v0, :cond_0

    const/16 v0, 0x3e

    if-eq p0, v0, :cond_0

    const/16 v0, 0x5b

    if-eq p0, v0, :cond_0

    const/16 v0, 0x5d

    if-eq p0, v0, :cond_0

    const/16 v0, 0x7b

    if-eq p0, v0, :cond_0

    const/16 v0, 0x7d

    if-eq p0, v0, :cond_0

    const/16 v0, 0x28

    if-eq p0, v0, :cond_0

    const/16 v0, 0x29

    if-eq p0, v0, :cond_0

    const/4 p0, 0x0

    return p0

    :cond_0
    const/4 p0, 0x1

    return p0
.end method

.method private static isWhitespaceOrEOF(I)Z
    .locals 1

    const/4 v0, -0x1

    if-eq p0, v0, :cond_1

    const/16 v0, 0x20

    if-eq p0, v0, :cond_1

    const/16 v0, 0xd

    if-eq p0, v0, :cond_1

    const/16 v0, 0xa

    if-ne p0, v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 p0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 p0, 0x1

    :goto_1
    return p0
.end method

.method private parseBeginbfchar(Ljava/lang/Object;Ljava/io/PushbackInputStream;Lorg/apache/fontbox/cmap/CMap;)V
    .locals 4

    check-cast p1, Ljava/lang/Number;

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1}, Ljava/lang/Number;->intValue()I

    move-result v1

    if-ge v0, v1, :cond_4

    invoke-direct {p0, p2}, Lorg/apache/fontbox/cmap/CMapParser;->parseNextToken(Ljava/io/PushbackInputStream;)Ljava/lang/Object;

    move-result-object v1

    instance-of v2, v1, Lorg/apache/fontbox/cmap/CMapParser$Operator;

    if-eqz v2, :cond_1

    check-cast v1, Lorg/apache/fontbox/cmap/CMapParser$Operator;

    invoke-static {v1}, Lorg/apache/fontbox/cmap/CMapParser$Operator;->access$000(Lorg/apache/fontbox/cmap/CMapParser$Operator;)Ljava/lang/String;

    move-result-object p1

    const-string p2, "endbfchar"

    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_3

    :cond_0
    new-instance p1, Ljava/io/IOException;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string p3, "Error : ~bfchar contains an unexpected operator : "

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {v1}, Lorg/apache/fontbox/cmap/CMapParser$Operator;->access$000(Lorg/apache/fontbox/cmap/CMapParser$Operator;)Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    check-cast v1, [B

    invoke-direct {p0, p2}, Lorg/apache/fontbox/cmap/CMapParser;->parseNextToken(Ljava/io/PushbackInputStream;)Ljava/lang/Object;

    move-result-object v2

    instance-of v3, v2, [B

    if-eqz v3, :cond_2

    check-cast v2, [B

    invoke-static {v2}, Lorg/apache/fontbox/cmap/CMapParser;->createStringFromBytes([B)Ljava/lang/String;

    move-result-object v2

    :goto_1
    invoke-virtual {p3, v1, v2}, Lorg/apache/fontbox/cmap/CMap;->addCharMapping([BLjava/lang/String;)V

    goto :goto_2

    :cond_2
    instance-of v3, v2, Lorg/apache/fontbox/cmap/CMapParser$LiteralName;

    if-eqz v3, :cond_3

    check-cast v2, Lorg/apache/fontbox/cmap/CMapParser$LiteralName;

    invoke-static {v2}, Lorg/apache/fontbox/cmap/CMapParser$LiteralName;->access$100(Lorg/apache/fontbox/cmap/CMapParser$LiteralName;)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    new-instance p1, Ljava/io/IOException;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string p3, "Error parsing CMap beginbfchar, expected{COSString or COSName} and not "

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_4
    :goto_3
    return-void
.end method

.method private parseBeginbfrange(Ljava/lang/Object;Ljava/io/PushbackInputStream;Lorg/apache/fontbox/cmap/CMap;)V
    .locals 9

    check-cast p1, Ljava/lang/Number;

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {p1}, Ljava/lang/Number;->intValue()I

    move-result v2

    if-ge v1, v2, :cond_7

    invoke-direct {p0, p2}, Lorg/apache/fontbox/cmap/CMapParser;->parseNextToken(Ljava/io/PushbackInputStream;)Ljava/lang/Object;

    move-result-object v2

    instance-of v3, v2, Lorg/apache/fontbox/cmap/CMapParser$Operator;

    if-eqz v3, :cond_1

    check-cast v2, Lorg/apache/fontbox/cmap/CMapParser$Operator;

    invoke-static {v2}, Lorg/apache/fontbox/cmap/CMapParser$Operator;->access$000(Lorg/apache/fontbox/cmap/CMapParser$Operator;)Ljava/lang/String;

    move-result-object p1

    const-string p2, "endbfrange"

    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_3

    :cond_0
    new-instance p1, Ljava/io/IOException;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string p3, "Error : ~bfrange contains an unexpected operator : "

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {v2}, Lorg/apache/fontbox/cmap/CMapParser$Operator;->access$000(Lorg/apache/fontbox/cmap/CMapParser$Operator;)Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    check-cast v2, [B

    invoke-direct {p0, p2}, Lorg/apache/fontbox/cmap/CMapParser;->parseNextToken(Ljava/io/PushbackInputStream;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [B

    invoke-direct {p0, p2}, Lorg/apache/fontbox/cmap/CMapParser;->parseNextToken(Ljava/io/PushbackInputStream;)Ljava/lang/Object;

    move-result-object v4

    instance-of v5, v4, Ljava/util/List;

    if-eqz v5, :cond_2

    check-cast v4, Ljava/util/List;

    invoke-interface {v4, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [B

    goto :goto_1

    :cond_2
    move-object v5, v4

    check-cast v5, [B

    const/4 v4, 0x0

    :goto_1
    move v6, v0

    move v7, v6

    :cond_3
    :goto_2
    if-nez v6, :cond_6

    invoke-static {v2, v3}, Lorg/apache/fontbox/cmap/CMapParser;->compare([B[B)I

    move-result v8

    if-ltz v8, :cond_4

    const/4 v6, 0x1

    :cond_4
    invoke-static {v5}, Lorg/apache/fontbox/cmap/CMapParser;->createStringFromBytes([B)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p3, v2, v8}, Lorg/apache/fontbox/cmap/CMap;->addCharMapping([BLjava/lang/String;)V

    invoke-direct {p0, v2}, Lorg/apache/fontbox/cmap/CMapParser;->increment([B)V

    if-nez v4, :cond_5

    invoke-direct {p0, v5}, Lorg/apache/fontbox/cmap/CMapParser;->increment([B)V

    goto :goto_2

    :cond_5
    add-int/lit8 v7, v7, 0x1

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v8

    if-ge v7, v8, :cond_3

    invoke-interface {v4, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [B

    goto :goto_2

    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0

    :cond_7
    :goto_3
    return-void
.end method

.method private parseBegincidchar(Ljava/lang/Object;Ljava/io/PushbackInputStream;Lorg/apache/fontbox/cmap/CMap;)V
    .locals 3

    check-cast p1, Ljava/lang/Number;

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1}, Ljava/lang/Number;->intValue()I

    move-result v1

    if-ge v0, v1, :cond_2

    invoke-direct {p0, p2}, Lorg/apache/fontbox/cmap/CMapParser;->parseNextToken(Ljava/io/PushbackInputStream;)Ljava/lang/Object;

    move-result-object v1

    instance-of v2, v1, Lorg/apache/fontbox/cmap/CMapParser$Operator;

    if-eqz v2, :cond_1

    check-cast v1, Lorg/apache/fontbox/cmap/CMapParser$Operator;

    invoke-static {v1}, Lorg/apache/fontbox/cmap/CMapParser$Operator;->access$000(Lorg/apache/fontbox/cmap/CMapParser$Operator;)Ljava/lang/String;

    move-result-object p1

    const-string p2, "endcidchar"

    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_1

    :cond_0
    new-instance p1, Ljava/io/IOException;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string p3, "Error : ~cidchar contains an unexpected operator : "

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {v1}, Lorg/apache/fontbox/cmap/CMapParser$Operator;->access$000(Lorg/apache/fontbox/cmap/CMapParser$Operator;)Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    check-cast v1, [B

    invoke-direct {p0, p2}, Lorg/apache/fontbox/cmap/CMapParser;->parseNextToken(Ljava/io/PushbackInputStream;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1}, Lorg/apache/fontbox/cmap/CMapParser;->createIntFromBytes([B)I

    move-result v1

    invoke-virtual {p3, v2, v1}, Lorg/apache/fontbox/cmap/CMap;->addCIDMapping(II)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    :goto_1
    return-void
.end method

.method private parseBegincidrange(Ljava/lang/Object;Ljava/io/PushbackInputStream;Lorg/apache/fontbox/cmap/CMap;)V
    .locals 8

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, p1, :cond_4

    invoke-direct {p0, p2}, Lorg/apache/fontbox/cmap/CMapParser;->parseNextToken(Ljava/io/PushbackInputStream;)Ljava/lang/Object;

    move-result-object v1

    instance-of v2, v1, Lorg/apache/fontbox/cmap/CMapParser$Operator;

    if-eqz v2, :cond_1

    check-cast v1, Lorg/apache/fontbox/cmap/CMapParser$Operator;

    invoke-static {v1}, Lorg/apache/fontbox/cmap/CMapParser$Operator;->access$000(Lorg/apache/fontbox/cmap/CMapParser$Operator;)Ljava/lang/String;

    move-result-object p1

    const-string p2, "endcidrange"

    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_3

    :cond_0
    new-instance p1, Ljava/io/IOException;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string p3, "Error : ~cidrange contains an unexpected operator : "

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {v1}, Lorg/apache/fontbox/cmap/CMapParser$Operator;->access$000(Lorg/apache/fontbox/cmap/CMapParser$Operator;)Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    check-cast v1, [B

    invoke-static {v1}, Lorg/apache/fontbox/cmap/CMapParser;->createIntFromBytes([B)I

    move-result v2

    invoke-direct {p0, p2}, Lorg/apache/fontbox/cmap/CMapParser;->parseNextToken(Ljava/io/PushbackInputStream;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [B

    invoke-static {v3}, Lorg/apache/fontbox/cmap/CMapParser;->createIntFromBytes([B)I

    move-result v4

    invoke-direct {p0, p2}, Lorg/apache/fontbox/cmap/CMapParser;->parseNextToken(Ljava/io/PushbackInputStream;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    array-length v6, v1

    const/4 v7, 0x2

    if-gt v6, v7, :cond_2

    array-length v3, v3

    if-gt v3, v7, :cond_2

    int-to-char v1, v2

    int-to-char v2, v4

    invoke-virtual {p3, v1, v2, v5}, Lorg/apache/fontbox/cmap/CMap;->addCIDRange(CCI)V

    goto :goto_2

    :cond_2
    add-int/2addr v4, v5

    sub-int/2addr v4, v2

    :goto_1
    if-gt v5, v4, :cond_3

    invoke-static {v1}, Lorg/apache/fontbox/cmap/CMapParser;->createIntFromBytes([B)I

    move-result v2

    add-int/lit8 v3, v5, 0x1

    invoke-virtual {p3, v5, v2}, Lorg/apache/fontbox/cmap/CMap;->addCIDMapping(II)V

    invoke-direct {p0, v1}, Lorg/apache/fontbox/cmap/CMapParser;->increment([B)V

    move v5, v3

    goto :goto_1

    :cond_3
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_4
    :goto_3
    return-void
.end method

.method private parseBegincodespacerange(Ljava/lang/Object;Ljava/io/PushbackInputStream;Lorg/apache/fontbox/cmap/CMap;)V
    .locals 4

    check-cast p1, Ljava/lang/Number;

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1}, Ljava/lang/Number;->intValue()I

    move-result v1

    if-ge v0, v1, :cond_2

    invoke-direct {p0, p2}, Lorg/apache/fontbox/cmap/CMapParser;->parseNextToken(Ljava/io/PushbackInputStream;)Ljava/lang/Object;

    move-result-object v1

    instance-of v2, v1, Lorg/apache/fontbox/cmap/CMapParser$Operator;

    if-eqz v2, :cond_1

    check-cast v1, Lorg/apache/fontbox/cmap/CMapParser$Operator;

    invoke-static {v1}, Lorg/apache/fontbox/cmap/CMapParser$Operator;->access$000(Lorg/apache/fontbox/cmap/CMapParser$Operator;)Ljava/lang/String;

    move-result-object p1

    const-string p2, "endcodespacerange"

    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_0

    goto :goto_1

    :cond_0
    new-instance p1, Ljava/io/IOException;

    new-instance p2, Ljava/lang/StringBuilder;

    invoke-direct {p2}, Ljava/lang/StringBuilder;-><init>()V

    const-string p3, "Error : ~codespacerange contains an unexpected operator : "

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {v1}, Lorg/apache/fontbox/cmap/CMapParser$Operator;->access$000(Lorg/apache/fontbox/cmap/CMapParser$Operator;)Ljava/lang/String;

    move-result-object p3

    invoke-virtual {p2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p2

    invoke-direct {p1, p2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    check-cast v1, [B

    invoke-direct {p0, p2}, Lorg/apache/fontbox/cmap/CMapParser;->parseNextToken(Ljava/io/PushbackInputStream;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [B

    new-instance v3, Lorg/apache/fontbox/cmap/CodespaceRange;

    invoke-direct {v3}, Lorg/apache/fontbox/cmap/CodespaceRange;-><init>()V

    invoke-virtual {v3, v1}, Lorg/apache/fontbox/cmap/CodespaceRange;->setStart([B)V

    invoke-virtual {v3, v2}, Lorg/apache/fontbox/cmap/CodespaceRange;->setEnd([B)V

    invoke-virtual {p3, v3}, Lorg/apache/fontbox/cmap/CMap;->addCodespaceRange(Lorg/apache/fontbox/cmap/CodespaceRange;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    :goto_1
    return-void
.end method

.method private parseLiteralName(Ljava/lang/Object;Ljava/io/PushbackInputStream;Lorg/apache/fontbox/cmap/CMap;)V
    .locals 2

    check-cast p1, Lorg/apache/fontbox/cmap/CMapParser$LiteralName;

    const-string v0, "WMode"

    invoke-static {p1}, Lorg/apache/fontbox/cmap/CMapParser$LiteralName;->access$100(Lorg/apache/fontbox/cmap/CMapParser$LiteralName;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p2}, Lorg/apache/fontbox/cmap/CMapParser;->parseNextToken(Ljava/io/PushbackInputStream;)Ljava/lang/Object;

    move-result-object p1

    instance-of p2, p1, Ljava/lang/Integer;

    if-eqz p2, :cond_7

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    invoke-virtual {p3, p1}, Lorg/apache/fontbox/cmap/CMap;->setWMode(I)V

    goto/16 :goto_1

    :cond_0
    const-string v0, "CMapName"

    invoke-static {p1}, Lorg/apache/fontbox/cmap/CMapParser$LiteralName;->access$100(Lorg/apache/fontbox/cmap/CMapParser$LiteralName;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0, p2}, Lorg/apache/fontbox/cmap/CMapParser;->parseNextToken(Ljava/io/PushbackInputStream;)Ljava/lang/Object;

    move-result-object p1

    instance-of p2, p1, Lorg/apache/fontbox/cmap/CMapParser$LiteralName;

    if-eqz p2, :cond_7

    check-cast p1, Lorg/apache/fontbox/cmap/CMapParser$LiteralName;

    invoke-static {p1}, Lorg/apache/fontbox/cmap/CMapParser$LiteralName;->access$100(Lorg/apache/fontbox/cmap/CMapParser$LiteralName;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p3, p1}, Lorg/apache/fontbox/cmap/CMap;->setName(Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_1
    const-string v0, "CMapVersion"

    invoke-static {p1}, Lorg/apache/fontbox/cmap/CMapParser$LiteralName;->access$100(Lorg/apache/fontbox/cmap/CMapParser$LiteralName;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-direct {p0, p2}, Lorg/apache/fontbox/cmap/CMapParser;->parseNextToken(Ljava/io/PushbackInputStream;)Ljava/lang/Object;

    move-result-object p1

    instance-of p2, p1, Ljava/lang/Number;

    if-eqz p2, :cond_2

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p1

    :goto_0
    invoke-virtual {p3, p1}, Lorg/apache/fontbox/cmap/CMap;->setVersion(Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_2
    instance-of p2, p1, Ljava/lang/String;

    if-eqz p2, :cond_7

    check-cast p1, Ljava/lang/String;

    goto :goto_0

    :cond_3
    const-string v0, "CMapType"

    invoke-static {p1}, Lorg/apache/fontbox/cmap/CMapParser$LiteralName;->access$100(Lorg/apache/fontbox/cmap/CMapParser$LiteralName;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-direct {p0, p2}, Lorg/apache/fontbox/cmap/CMapParser;->parseNextToken(Ljava/io/PushbackInputStream;)Ljava/lang/Object;

    move-result-object p1

    instance-of p2, p1, Ljava/lang/Integer;

    if-eqz p2, :cond_7

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    invoke-virtual {p3, p1}, Lorg/apache/fontbox/cmap/CMap;->setType(I)V

    goto :goto_1

    :cond_4
    const-string v0, "Registry"

    invoke-static {p1}, Lorg/apache/fontbox/cmap/CMapParser$LiteralName;->access$100(Lorg/apache/fontbox/cmap/CMapParser$LiteralName;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-direct {p0, p2}, Lorg/apache/fontbox/cmap/CMapParser;->parseNextToken(Ljava/io/PushbackInputStream;)Ljava/lang/Object;

    move-result-object p1

    instance-of p2, p1, Ljava/lang/String;

    if-eqz p2, :cond_7

    check-cast p1, Ljava/lang/String;

    invoke-virtual {p3, p1}, Lorg/apache/fontbox/cmap/CMap;->setRegistry(Ljava/lang/String;)V

    goto :goto_1

    :cond_5
    const-string v0, "Ordering"

    invoke-static {p1}, Lorg/apache/fontbox/cmap/CMapParser$LiteralName;->access$100(Lorg/apache/fontbox/cmap/CMapParser$LiteralName;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-direct {p0, p2}, Lorg/apache/fontbox/cmap/CMapParser;->parseNextToken(Ljava/io/PushbackInputStream;)Ljava/lang/Object;

    move-result-object p1

    instance-of p2, p1, Ljava/lang/String;

    if-eqz p2, :cond_7

    check-cast p1, Ljava/lang/String;

    invoke-virtual {p3, p1}, Lorg/apache/fontbox/cmap/CMap;->setOrdering(Ljava/lang/String;)V

    goto :goto_1

    :cond_6
    const-string v0, "Supplement"

    invoke-static {p1}, Lorg/apache/fontbox/cmap/CMapParser$LiteralName;->access$100(Lorg/apache/fontbox/cmap/CMapParser$LiteralName;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result p1

    if-eqz p1, :cond_7

    invoke-direct {p0, p2}, Lorg/apache/fontbox/cmap/CMapParser;->parseNextToken(Ljava/io/PushbackInputStream;)Ljava/lang/Object;

    move-result-object p1

    instance-of p2, p1, Ljava/lang/Integer;

    if-eqz p2, :cond_7

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result p1

    invoke-virtual {p3, p1}, Lorg/apache/fontbox/cmap/CMap;->setSupplement(I)V

    :cond_7
    :goto_1
    return-void
.end method

.method private parseNextToken(Ljava/io/PushbackInputStream;)Ljava/lang/Object;
    .locals 10

    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljava/io/PushbackInputStream;->read()I

    move-result v0

    const/16 v1, 0x9

    if-eq v0, v1, :cond_0

    const/16 v1, 0x20

    if-eq v0, v1, :cond_0

    const/16 v1, 0xd

    if-eq v0, v1, :cond_0

    const/16 v1, 0xa

    if-ne v0, v1, :cond_1

    goto :goto_0

    :cond_1
    const/4 v1, -0x1

    const/4 v2, 0x0

    if-eq v0, v1, :cond_19

    const/16 v3, 0x25

    if-eq v0, v3, :cond_18

    const/16 v3, 0x28

    if-eq v0, v3, :cond_16

    const-string v3, ">>"

    const/16 v4, 0x3c

    const/16 v5, 0x3e

    if-eq v0, v4, :cond_e

    if-eq v0, v5, :cond_c

    const/16 v1, 0x5b

    const-string v3, "]"

    if-eq v0, v1, :cond_b

    const/16 v1, 0x5d

    if-eq v0, v1, :cond_a

    packed-switch v0, :pswitch_data_0

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    :goto_1
    int-to-char v0, v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    invoke-virtual {p1}, Ljava/io/PushbackInputStream;->read()I

    move-result v0

    invoke-static {v0}, Lorg/apache/fontbox/cmap/CMapParser;->isWhitespaceOrEOF(I)Z

    move-result v3

    if-nez v3, :cond_2

    invoke-static {v0}, Lorg/apache/fontbox/cmap/CMapParser;->isDelimiter(I)Z

    move-result v3

    if-nez v3, :cond_2

    invoke-static {v0}, Ljava/lang/Character;->isDigit(I)Z

    move-result v3

    if-nez v3, :cond_2

    goto :goto_1

    :cond_2
    invoke-static {v0}, Lorg/apache/fontbox/cmap/CMapParser;->isDelimiter(I)Z

    move-result v3

    if-nez v3, :cond_3

    invoke-static {v0}, Ljava/lang/Character;->isDigit(I)Z

    move-result v3

    if-eqz v3, :cond_4

    :cond_3
    invoke-virtual {p1, v0}, Ljava/io/PushbackInputStream;->unread(I)V

    :cond_4
    new-instance p1, Lorg/apache/fontbox/cmap/CMapParser$Operator;

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, p0, v0, v2}, Lorg/apache/fontbox/cmap/CMapParser$Operator;-><init>(Lorg/apache/fontbox/cmap/CMapParser;Ljava/lang/String;Lorg/apache/fontbox/cmap/CMapParser$1;)V

    goto :goto_4

    :pswitch_0
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    int-to-char v0, v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    :goto_2
    invoke-virtual {p1}, Ljava/io/PushbackInputStream;->read()I

    move-result v0

    invoke-static {v0}, Lorg/apache/fontbox/cmap/CMapParser;->isWhitespaceOrEOF(I)Z

    move-result v2

    const/16 v3, 0x2e

    if-nez v2, :cond_6

    int-to-char v2, v0

    invoke-static {v2}, Ljava/lang/Character;->isDigit(C)Z

    move-result v4

    if-nez v4, :cond_5

    if-ne v0, v3, :cond_6

    :cond_5
    invoke-virtual {v1, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_2

    :cond_6
    invoke-virtual {p1, v0}, Ljava/io/PushbackInputStream;->unread(I)V

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p1, v3}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    if-ltz v0, :cond_7

    new-instance v2, Ljava/lang/Double;

    invoke-direct {v2, p1}, Ljava/lang/Double;-><init>(Ljava/lang/String;)V

    goto/16 :goto_e

    :cond_7
    new-instance v2, Ljava/lang/Integer;

    invoke-direct {v2, p1}, Ljava/lang/Integer;-><init>(Ljava/lang/String;)V

    goto/16 :goto_e

    :pswitch_1
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    :goto_3
    invoke-virtual {p1}, Ljava/io/PushbackInputStream;->read()I

    move-result v1

    invoke-static {v1}, Lorg/apache/fontbox/cmap/CMapParser;->isWhitespaceOrEOF(I)Z

    move-result v3

    if-nez v3, :cond_8

    invoke-static {v1}, Lorg/apache/fontbox/cmap/CMapParser;->isDelimiter(I)Z

    move-result v3

    if-nez v3, :cond_8

    int-to-char v1, v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_3

    :cond_8
    invoke-static {v1}, Lorg/apache/fontbox/cmap/CMapParser;->isDelimiter(I)Z

    move-result v3

    if-eqz v3, :cond_9

    invoke-virtual {p1, v1}, Ljava/io/PushbackInputStream;->unread(I)V

    :cond_9
    new-instance p1, Lorg/apache/fontbox/cmap/CMapParser$LiteralName;

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, p0, v0, v2}, Lorg/apache/fontbox/cmap/CMapParser$LiteralName;-><init>(Lorg/apache/fontbox/cmap/CMapParser;Ljava/lang/String;Lorg/apache/fontbox/cmap/CMapParser$1;)V

    :goto_4
    move-object v2, p1

    goto/16 :goto_e

    :cond_a
    :goto_5
    move-object v2, v3

    goto/16 :goto_e

    :cond_b
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    :goto_6
    invoke-direct {p0, p1}, Lorg/apache/fontbox/cmap/CMapParser;->parseNextToken(Ljava/io/PushbackInputStream;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_19

    if-eq v0, v3, :cond_19

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_6

    :cond_c
    invoke-virtual {p1}, Ljava/io/PushbackInputStream;->read()I

    move-result p1

    if-ne p1, v5, :cond_d

    goto :goto_5

    :cond_d
    new-instance p1, Ljava/io/IOException;

    const-string v0, "Error: expected the end of a dictionary."

    invoke-direct {p1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_e
    invoke-virtual {p1}, Ljava/io/PushbackInputStream;->read()I

    move-result v0

    if-ne v0, v4, :cond_f

    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    :goto_7
    invoke-direct {p0, p1}, Lorg/apache/fontbox/cmap/CMapParser;->parseNextToken(Ljava/io/PushbackInputStream;)Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Lorg/apache/fontbox/cmap/CMapParser$LiteralName;

    if-eqz v1, :cond_19

    if-eq v0, v3, :cond_19

    invoke-direct {p0, p1}, Lorg/apache/fontbox/cmap/CMapParser;->parseNextToken(Ljava/io/PushbackInputStream;)Ljava/lang/Object;

    move-result-object v1

    check-cast v0, Lorg/apache/fontbox/cmap/CMapParser$LiteralName;

    invoke-static {v0}, Lorg/apache/fontbox/cmap/CMapParser$LiteralName;->access$100(Lorg/apache/fontbox/cmap/CMapParser$LiteralName;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_7

    :cond_f
    const/16 v2, 0x10

    move v3, v1

    move v4, v2

    :goto_8
    const/4 v6, 0x0

    const/4 v7, 0x1

    if-eq v0, v1, :cond_15

    if-eq v0, v5, :cond_15

    const/16 v8, 0x30

    if-lt v0, v8, :cond_10

    const/16 v8, 0x39

    if-gt v0, v8, :cond_10

    add-int/lit8 v0, v0, -0x30

    goto :goto_a

    :cond_10
    const/16 v8, 0x41

    if-lt v0, v8, :cond_11

    const/16 v9, 0x46

    if-gt v0, v9, :cond_11

    :goto_9
    add-int/lit8 v0, v0, 0xa

    sub-int/2addr v0, v8

    goto :goto_a

    :cond_11
    const/16 v8, 0x61

    if-lt v0, v8, :cond_13

    const/16 v9, 0x66

    if-gt v0, v9, :cond_13

    goto :goto_9

    :goto_a
    mul-int/2addr v0, v4

    if-ne v4, v2, :cond_12

    add-int/lit8 v3, v3, 0x1

    iget-object v4, p0, Lorg/apache/fontbox/cmap/CMapParser;->tokenParserByteBuffer:[B

    aput-byte v6, v4, v3

    move v4, v7

    goto :goto_b

    :cond_12
    move v4, v2

    :goto_b
    iget-object v6, p0, Lorg/apache/fontbox/cmap/CMapParser;->tokenParserByteBuffer:[B

    aget-byte v7, v6, v3

    add-int/2addr v7, v0

    int-to-byte v0, v7

    aput-byte v0, v6, v3

    goto :goto_c

    :cond_13
    invoke-static {v0}, Lorg/apache/fontbox/cmap/CMapParser;->isWhitespaceOrEOF(I)Z

    move-result v6

    if-eqz v6, :cond_14

    :goto_c
    invoke-virtual {p1}, Ljava/io/PushbackInputStream;->read()I

    move-result v0

    goto :goto_8

    :cond_14
    new-instance p1, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Error: expected hex character and not "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    int-to-char v2, v0

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_15
    add-int/2addr v3, v7

    new-array v2, v3, [B

    iget-object p1, p0, Lorg/apache/fontbox/cmap/CMapParser;->tokenParserByteBuffer:[B

    invoke-static {p1, v6, v2, v6, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_e

    :cond_16
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    :goto_d
    invoke-virtual {p1}, Ljava/io/PushbackInputStream;->read()I

    move-result v2

    if-eq v2, v1, :cond_17

    const/16 v3, 0x29

    if-eq v2, v3, :cond_17

    int-to-char v2, v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_d

    :cond_17
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_e

    :cond_18
    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    int-to-char v0, v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    invoke-static {p1, v1}, Lorg/apache/fontbox/cmap/CMapParser;->readUntilEndOfLine(Ljava/io/InputStream;Ljava/lang/StringBuffer;)V

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    :cond_19
    :goto_e
    return-object v2

    :pswitch_data_0
    .packed-switch 0x2f
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private parseUsecmap(Ljava/lang/Object;Lorg/apache/fontbox/cmap/CMap;)V
    .locals 0

    check-cast p1, Lorg/apache/fontbox/cmap/CMapParser$LiteralName;

    invoke-static {p1}, Lorg/apache/fontbox/cmap/CMapParser$LiteralName;->access$100(Lorg/apache/fontbox/cmap/CMapParser$LiteralName;)Ljava/lang/String;

    move-result-object p1

    invoke-virtual {p0, p1}, Lorg/apache/fontbox/cmap/CMapParser;->getExternalCMap(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object p1

    invoke-virtual {p0, p1}, Lorg/apache/fontbox/cmap/CMapParser;->parse(Ljava/io/InputStream;)Lorg/apache/fontbox/cmap/CMap;

    move-result-object p1

    invoke-virtual {p2, p1}, Lorg/apache/fontbox/cmap/CMap;->useCmap(Lorg/apache/fontbox/cmap/CMap;)V

    return-void
.end method

.method private static readUntilEndOfLine(Ljava/io/InputStream;Ljava/lang/StringBuffer;)V
    .locals 2

    :goto_0
    invoke-virtual {p0}, Ljava/io/InputStream;->read()I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/16 v1, 0xd

    if-eq v0, v1, :cond_0

    const/16 v1, 0xa

    if-eq v0, v1, :cond_0

    int-to-char v0, v0

    invoke-virtual {p1, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method public getExternalCMap(Ljava/lang/String;)Ljava/io/InputStream;
    .locals 3

    invoke-static {}, Lorg/apache/pdfbox/util/PDFBoxResourceLoader;->isReady()Z

    move-result v0

    const-string v1, "/org/apache/fontbox/resources/cmap/"

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-static {p1}, Lorg/apache/pdfbox/util/PDFBoxResourceLoader;->getStream(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object p1

    return-object p1

    :cond_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getResource(Ljava/lang/String;)Ljava/net/URL;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/net/URL;->openStream()Ljava/io/InputStream;

    move-result-object p1

    return-object p1

    :cond_1
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Error: Could not find referenced cmap stream "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    invoke-direct {v0, p1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public parse(Ljava/io/File;)Lorg/apache/fontbox/cmap/CMap;
    .locals 2

    .line 1
    const/4 v0, 0x0

    :try_start_0
    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    invoke-virtual {p0, v1}, Lorg/apache/fontbox/cmap/CMapParser;->parse(Ljava/io/InputStream;)Lorg/apache/fontbox/cmap/CMap;

    move-result-object p1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-virtual {v1}, Ljava/io/FileInputStream;->close()V

    return-object p1

    :catchall_0
    move-exception p1

    move-object v0, v1

    goto :goto_0

    :catchall_1
    move-exception p1

    :goto_0
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/io/FileInputStream;->close()V

    :cond_0
    throw p1
.end method

.method public parse(Ljava/io/InputStream;)Lorg/apache/fontbox/cmap/CMap;
    .locals 6

    .line 2
    new-instance v0, Ljava/io/PushbackInputStream;

    invoke-direct {v0, p1}, Ljava/io/PushbackInputStream;-><init>(Ljava/io/InputStream;)V

    new-instance p1, Lorg/apache/fontbox/cmap/CMap;

    invoke-direct {p1}, Lorg/apache/fontbox/cmap/CMap;-><init>()V

    const/4 v1, 0x0

    :goto_0
    invoke-direct {p0, v0}, Lorg/apache/fontbox/cmap/CMapParser;->parseNextToken(Ljava/io/PushbackInputStream;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_8

    instance-of v3, v2, Lorg/apache/fontbox/cmap/CMapParser$Operator;

    if-eqz v3, :cond_6

    move-object v3, v2

    check-cast v3, Lorg/apache/fontbox/cmap/CMapParser$Operator;

    invoke-static {v3}, Lorg/apache/fontbox/cmap/CMapParser$Operator;->access$000(Lorg/apache/fontbox/cmap/CMapParser$Operator;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "usecmap"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-direct {p0, v1, p1}, Lorg/apache/fontbox/cmap/CMapParser;->parseUsecmap(Ljava/lang/Object;Lorg/apache/fontbox/cmap/CMap;)V

    goto :goto_1

    :cond_0
    invoke-static {v3}, Lorg/apache/fontbox/cmap/CMapParser$Operator;->access$000(Lorg/apache/fontbox/cmap/CMapParser$Operator;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "endcmap"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    goto :goto_2

    :cond_1
    invoke-static {v3}, Lorg/apache/fontbox/cmap/CMapParser$Operator;->access$000(Lorg/apache/fontbox/cmap/CMapParser$Operator;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "begincodespacerange"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-direct {p0, v1, v0, p1}, Lorg/apache/fontbox/cmap/CMapParser;->parseBegincodespacerange(Ljava/lang/Object;Ljava/io/PushbackInputStream;Lorg/apache/fontbox/cmap/CMap;)V

    goto :goto_1

    :cond_2
    invoke-static {v3}, Lorg/apache/fontbox/cmap/CMapParser$Operator;->access$000(Lorg/apache/fontbox/cmap/CMapParser$Operator;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "beginbfchar"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-direct {p0, v1, v0, p1}, Lorg/apache/fontbox/cmap/CMapParser;->parseBeginbfchar(Ljava/lang/Object;Ljava/io/PushbackInputStream;Lorg/apache/fontbox/cmap/CMap;)V

    goto :goto_1

    :cond_3
    invoke-static {v3}, Lorg/apache/fontbox/cmap/CMapParser$Operator;->access$000(Lorg/apache/fontbox/cmap/CMapParser$Operator;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "beginbfrange"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-direct {p0, v1, v0, p1}, Lorg/apache/fontbox/cmap/CMapParser;->parseBeginbfrange(Ljava/lang/Object;Ljava/io/PushbackInputStream;Lorg/apache/fontbox/cmap/CMap;)V

    goto :goto_1

    :cond_4
    invoke-static {v3}, Lorg/apache/fontbox/cmap/CMapParser$Operator;->access$000(Lorg/apache/fontbox/cmap/CMapParser$Operator;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "begincidchar"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-direct {p0, v1, v0, p1}, Lorg/apache/fontbox/cmap/CMapParser;->parseBegincidchar(Ljava/lang/Object;Ljava/io/PushbackInputStream;Lorg/apache/fontbox/cmap/CMap;)V

    goto :goto_1

    :cond_5
    invoke-static {v3}, Lorg/apache/fontbox/cmap/CMapParser$Operator;->access$000(Lorg/apache/fontbox/cmap/CMapParser$Operator;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "begincidrange"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    invoke-direct {p0, v1, v0, p1}, Lorg/apache/fontbox/cmap/CMapParser;->parseBegincidrange(Ljava/lang/Object;Ljava/io/PushbackInputStream;Lorg/apache/fontbox/cmap/CMap;)V

    goto :goto_1

    :cond_6
    instance-of v1, v2, Lorg/apache/fontbox/cmap/CMapParser$LiteralName;

    if-eqz v1, :cond_7

    invoke-direct {p0, v2, v0, p1}, Lorg/apache/fontbox/cmap/CMapParser;->parseLiteralName(Ljava/lang/Object;Ljava/io/PushbackInputStream;Lorg/apache/fontbox/cmap/CMap;)V

    :cond_7
    :goto_1
    move-object v1, v2

    goto/16 :goto_0

    :cond_8
    :goto_2
    return-object p1
.end method

.method public parsePredefined(Ljava/lang/String;)Lorg/apache/fontbox/cmap/CMap;
    .locals 1

    :try_start_0
    invoke-virtual {p0, p1}, Lorg/apache/fontbox/cmap/CMapParser;->getExternalCMap(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    invoke-virtual {p0, p1}, Lorg/apache/fontbox/cmap/CMapParser;->parse(Ljava/io/InputStream;)Lorg/apache/fontbox/cmap/CMap;

    move-result-object v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/io/InputStream;->close()V

    :cond_0
    return-object v0

    :catchall_0
    move-exception v0

    goto :goto_0

    :catchall_1
    move-exception v0

    const/4 p1, 0x0

    :goto_0
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/io/InputStream;->close()V

    :cond_1
    throw v0
.end method

.class public abstract Lkotlin/sequences/SequencesKt__SequencesKt;
.super Lgl1;
.source "SourceFile"


# direct methods
.method public static final c(Ljava/util/Iterator;)Lcl1;
    .locals 1

    .line 1
    const-string v0, "<this>"

    invoke-static {p0, v0}, Lfg0;->e(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lkotlin/sequences/SequencesKt__SequencesKt$a;

    invoke-direct {v0, p0}, Lkotlin/sequences/SequencesKt__SequencesKt$a;-><init>(Ljava/util/Iterator;)V

    invoke-static {v0}, Lkotlin/sequences/SequencesKt__SequencesKt;->d(Lcl1;)Lcl1;

    move-result-object p0

    return-object p0
.end method

.method public static final d(Lcl1;)Lcl1;
    .locals 1

    .line 1
    const-string v0, "<this>"

    invoke-static {p0, v0}, Lfg0;->e(Ljava/lang/Object;Ljava/lang/String;)V

    instance-of v0, p0, Lpk;

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    new-instance v0, Lpk;

    invoke-direct {v0, p0}, Lpk;-><init>(Lcl1;)V

    move-object p0, v0

    :goto_0
    return-object p0
.end method

.method public static final e(La90;Lc90;)Lcl1;
    .locals 1

    .line 1
    const-string v0, "seedFunction"

    invoke-static {p0, v0}, Lfg0;->e(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "nextFunction"

    invoke-static {p1, v0}, Lfg0;->e(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lha0;

    invoke-direct {v0, p0, p1}, Lha0;-><init>(La90;Lc90;)V

    return-object v0
.end method

.method public static final f(Ljava/lang/Object;Lc90;)Lcl1;
    .locals 2

    .line 1
    const-string v0, "nextFunction"

    invoke-static {p1, v0}, Lfg0;->e(Ljava/lang/Object;Ljava/lang/String;)V

    if-nez p0, :cond_0

    sget-object p0, Lvw;->a:Lvw;

    goto :goto_0

    :cond_0
    new-instance v0, Lha0;

    new-instance v1, Lkotlin/sequences/SequencesKt__SequencesKt$generateSequence$2;

    invoke-direct {v1, p0}, Lkotlin/sequences/SequencesKt__SequencesKt$generateSequence$2;-><init>(Ljava/lang/Object;)V

    invoke-direct {v0, v1, p1}, Lha0;-><init>(La90;Lc90;)V

    move-object p0, v0

    :goto_0
    return-object p0
.end method

.class public abstract synthetic Lbd;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static final a(Lkotlin/coroutines/CoroutineContext;Lq90;)Ljava/lang/Object;
    .locals 4

    .line 1
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    sget-object v1, Lxl;->s:Lxl$b;

    invoke-interface {p0, v1}, Lkotlin/coroutines/CoroutineContext;->get(Lkotlin/coroutines/CoroutineContext$b;)Lkotlin/coroutines/CoroutineContext$a;

    move-result-object v1

    check-cast v1, Lxl;

    if-nez v1, :cond_0

    sget-object v1, Luv1;->a:Luv1;

    invoke-virtual {v1}, Luv1;->b()Lsx;

    move-result-object v1

    sget-object v2, Lgb0;->a:Lgb0;

    invoke-interface {p0, v1}, Lkotlin/coroutines/CoroutineContext;->plus(Lkotlin/coroutines/CoroutineContext;)Lkotlin/coroutines/CoroutineContext;

    move-result-object p0

    :goto_0
    invoke-static {v2, p0}, Lkotlinx/coroutines/CoroutineContextKt;->d(Llm;Lkotlin/coroutines/CoroutineContext;)Lkotlin/coroutines/CoroutineContext;

    move-result-object p0

    goto :goto_4

    :cond_0
    instance-of v2, v1, Lsx;

    const/4 v3, 0x0

    if-eqz v2, :cond_1

    check-cast v1, Lsx;

    goto :goto_1

    :cond_1
    move-object v1, v3

    :goto_1
    if-eqz v1, :cond_4

    invoke-virtual {v1}, Lsx;->D0()Z

    move-result v2

    if-eqz v2, :cond_2

    move-object v3, v1

    :cond_2
    if-nez v3, :cond_3

    goto :goto_2

    :cond_3
    move-object v1, v3

    goto :goto_3

    :cond_4
    :goto_2
    sget-object v1, Luv1;->a:Luv1;

    invoke-virtual {v1}, Luv1;->a()Lsx;

    move-result-object v1

    :goto_3
    sget-object v2, Lgb0;->a:Lgb0;

    goto :goto_0

    :goto_4
    new-instance v2, Lhc;

    invoke-direct {v2, p0, v0, v1}, Lhc;-><init>(Lkotlin/coroutines/CoroutineContext;Ljava/lang/Thread;Lsx;)V

    sget-object p0, Lkotlinx/coroutines/CoroutineStart;->DEFAULT:Lkotlinx/coroutines/CoroutineStart;

    invoke-virtual {v2, p0, v2, p1}, Lkotlinx/coroutines/a;->d1(Lkotlinx/coroutines/CoroutineStart;Ljava/lang/Object;Lq90;)V

    invoke-virtual {v2}, Lhc;->e1()Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method public static synthetic b(Lkotlin/coroutines/CoroutineContext;Lq90;ILjava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 1
    and-int/lit8 p2, p2, 0x1

    if-eqz p2, :cond_0

    sget-object p0, Lkotlin/coroutines/EmptyCoroutineContext;->INSTANCE:Lkotlin/coroutines/EmptyCoroutineContext;

    :cond_0
    invoke-static {p0, p1}, Lad;->e(Lkotlin/coroutines/CoroutineContext;Lq90;)Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.class public final Lp$d;
.super Lp;
.source "SourceFile"

# interfaces
.implements Ljava/util/RandomAccess;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lp;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "d"
.end annotation


# instance fields
.field public final a:Lp;

.field public final b:I

.field public c:I


# direct methods
.method public constructor <init>(Lp;II)V
    .locals 1

    .line 1
    const-string v0, "list"

    invoke-static {p1, v0}, Lfg0;->e(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-direct {p0}, Lp;-><init>()V

    iput-object p1, p0, Lp$d;->a:Lp;

    iput p2, p0, Lp$d;->b:I

    sget-object v0, Lp;->Companion:Lp$a;

    invoke-virtual {p1}, Lkotlin/collections/AbstractCollection;->size()I

    move-result p1

    invoke-virtual {v0, p2, p3, p1}, Lp$a;->c(III)V

    sub-int/2addr p3, p2

    iput p3, p0, Lp$d;->c:I

    return-void
.end method


# virtual methods
.method public get(I)Ljava/lang/Object;
    .locals 2

    .line 1
    sget-object v0, Lp;->Companion:Lp$a;

    iget v1, p0, Lp$d;->c:I

    invoke-virtual {v0, p1, v1}, Lp$a;->a(II)V

    iget-object v0, p0, Lp$d;->a:Lp;

    iget v1, p0, Lp$d;->b:I

    add-int/2addr v1, p1

    invoke-virtual {v0, v1}, Lp;->get(I)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public getSize()I
    .locals 1

    .line 1
    iget v0, p0, Lp$d;->c:I

    return v0
.end method

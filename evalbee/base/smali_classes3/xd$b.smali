.class public Lxd$b;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lxd;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "b"
.end annotation


# instance fields
.field public final a:Ljava/lang/String;

.field public b:I

.field public c:Ljava/lang/StringBuilder;

.field public d:I

.field public e:I


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lxd$b;->b:I

    iput v0, p0, Lxd$b;->d:I

    iput v0, p0, Lxd$b;->e:I

    iput-object p1, p0, Lxd$b;->a:Ljava/lang/String;

    return-void
.end method

.method public static synthetic a(Lxd$b;)I
    .locals 0

    .line 1
    iget p0, p0, Lxd$b;->b:I

    return p0
.end method


# virtual methods
.method public b(C)V
    .locals 1

    .line 1
    invoke-virtual {p0}, Lxd$b;->h()Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    return-void
.end method

.method public c(Ljava/lang/String;)V
    .locals 1

    .line 1
    invoke-virtual {p0}, Lxd$b;->h()Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    return-void
.end method

.method public d()V
    .locals 3

    .line 1
    iget v0, p0, Lxd$b;->e:I

    iget v1, p0, Lxd$b;->d:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lxd$b;->b:I

    add-int/lit8 v1, v0, -0x1

    iput v1, p0, Lxd$b;->d:I

    :goto_0
    iput v0, p0, Lxd$b;->e:I

    goto :goto_1

    :cond_0
    iget v1, p0, Lxd$b;->b:I

    add-int/lit8 v1, v1, -0x1

    if-ne v0, v1, :cond_1

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lxd$b;->h()Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lxd$b;->a:Ljava/lang/String;

    iget v2, p0, Lxd$b;->b:I

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Ljava/lang/String;->charAt(I)C

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :goto_1
    return-void
.end method

.method public e()V
    .locals 2

    .line 1
    iget-object v0, p0, Lxd$b;->c:Ljava/lang/StringBuilder;

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    :cond_0
    iget v0, p0, Lxd$b;->b:I

    iput v0, p0, Lxd$b;->e:I

    iput v0, p0, Lxd$b;->d:I

    return-void
.end method

.method public f()Z
    .locals 2

    .line 1
    iget v0, p0, Lxd$b;->b:I

    iget-object v1, p0, Lxd$b;->a:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public g()Z
    .locals 2

    .line 1
    iget v0, p0, Lxd$b;->d:I

    iget v1, p0, Lxd$b;->e:I

    if-lt v0, v1, :cond_1

    iget-object v0, p0, Lxd$b;->c:Ljava/lang/StringBuilder;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public final h()Ljava/lang/StringBuilder;
    .locals 4

    .line 1
    iget-object v0, p0, Lxd$b;->c:Ljava/lang/StringBuilder;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v1, p0, Lxd$b;->a:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit16 v1, v1, 0x80

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    iput-object v0, p0, Lxd$b;->c:Ljava/lang/StringBuilder;

    :cond_0
    iget v0, p0, Lxd$b;->d:I

    iget v1, p0, Lxd$b;->e:I

    if-ge v0, v1, :cond_1

    iget-object v2, p0, Lxd$b;->c:Ljava/lang/StringBuilder;

    iget-object v3, p0, Lxd$b;->a:Ljava/lang/String;

    invoke-virtual {v2, v3, v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;II)Ljava/lang/StringBuilder;

    iget v0, p0, Lxd$b;->b:I

    iput v0, p0, Lxd$b;->e:I

    iput v0, p0, Lxd$b;->d:I

    :cond_1
    iget-object v0, p0, Lxd$b;->c:Ljava/lang/StringBuilder;

    return-object v0
.end method

.method public i()Ljava/lang/String;
    .locals 3

    .line 1
    iget-object v0, p0, Lxd$b;->c:Ljava/lang/StringBuilder;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lxd$b;->h()Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_1
    :goto_0
    iget-object v0, p0, Lxd$b;->a:Ljava/lang/String;

    iget v1, p0, Lxd$b;->d:I

    iget v2, p0, Lxd$b;->e:I

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public j()C
    .locals 3

    .line 1
    iget-object v0, p0, Lxd$b;->a:Ljava/lang/String;

    iget v1, p0, Lxd$b;->b:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lxd$b;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    return v0
.end method

.method public k()Ljava/lang/String;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lxd$b;->i()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lxd$b;->e()V

    return-object v0
.end method

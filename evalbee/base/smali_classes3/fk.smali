.class public abstract Lfk;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Lxs1;

.field public static final b:Lgn0;

.field public static final c:Lgn0;


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lxs1;

    const-string v1, "REHASH"

    invoke-direct {v0, v1}, Lxs1;-><init>(Ljava/lang/String;)V

    sput-object v0, Lfk;->a:Lxs1;

    new-instance v0, Lgn0;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lgn0;-><init>(Ljava/lang/Object;)V

    sput-object v0, Lfk;->b:Lgn0;

    new-instance v0, Lgn0;

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-direct {v0, v1}, Lgn0;-><init>(Ljava/lang/Object;)V

    sput-object v0, Lfk;->c:Lgn0;

    return-void
.end method

.method public static final synthetic a()Lxs1;
    .locals 1

    .line 1
    sget-object v0, Lfk;->a:Lxs1;

    return-object v0
.end method

.method public static final synthetic b(Ljava/lang/Object;)Lgn0;
    .locals 0

    .line 1
    invoke-static {p0}, Lfk;->d(Ljava/lang/Object;)Lgn0;

    move-result-object p0

    return-object p0
.end method

.method public static final synthetic c()Ljava/lang/Void;
    .locals 1

    .line 1
    invoke-static {}, Lfk;->e()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method public static final d(Ljava/lang/Object;)Lgn0;
    .locals 1

    .line 1
    if-nez p0, :cond_0

    sget-object p0, Lfk;->b:Lgn0;

    goto :goto_0

    :cond_0
    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-static {p0, v0}, Lfg0;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    sget-object p0, Lfk;->c:Lgn0;

    goto :goto_0

    :cond_1
    new-instance v0, Lgn0;

    invoke-direct {v0, p0}, Lgn0;-><init>(Ljava/lang/Object;)V

    move-object p0, v0

    :goto_0
    return-object p0
.end method

.method public static final e()Ljava/lang/Void;
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "not implemented"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

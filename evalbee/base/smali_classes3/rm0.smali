.class public final Lrm0;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Lrm0;

.field public static final b:Z

.field public static final c:Lpm0;


# direct methods
.method public static constructor <clinit>()V
    .locals 3

    .line 1
    new-instance v0, Lrm0;

    invoke-direct {v0}, Lrm0;-><init>()V

    sput-object v0, Lrm0;->a:Lrm0;

    const-string v1, "kotlinx.coroutines.fast.service.loader"

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lqt1;->f(Ljava/lang/String;Z)Z

    invoke-virtual {v0}, Lrm0;->a()Lpm0;

    move-result-object v0

    sput-object v0, Lrm0;->c:Lpm0;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lpm0;
    .locals 7

    .line 1
    const-class v0, Lqm0;

    const/4 v1, 0x0

    :try_start_0
    sget-boolean v2, Lrm0;->b:Z

    if-eqz v2, :cond_0

    sget-object v0, Lc00;->a:Lc00;

    invoke-virtual {v0}, Lc00;->c()Ljava/util/List;

    move-result-object v0

    goto :goto_0

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v2

    invoke-static {v0, v2}, Ljava/util/ServiceLoader;->load(Ljava/lang/Class;Ljava/lang/ClassLoader;)Ljava/util/ServiceLoader;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ServiceLoader;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v0}, Lkotlin/sequences/SequencesKt__SequencesKt;->c(Ljava/util/Iterator;)Lcl1;

    move-result-object v0

    invoke-static {v0}, Lil1;->l(Lcl1;)Ljava/util/List;

    move-result-object v0

    :goto_0
    move-object v2, v0

    check-cast v2, Ljava/lang/Iterable;

    invoke-interface {v2}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_1

    move-object v3, v1

    goto :goto_1

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-nez v4, :cond_2

    goto :goto_1

    :cond_2
    move-object v4, v3

    check-cast v4, Lqm0;

    invoke-interface {v4}, Lqm0;->a()I

    move-result v4

    :cond_3
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    move-object v6, v5

    check-cast v6, Lqm0;

    invoke-interface {v6}, Lqm0;->a()I

    move-result v6

    if-ge v4, v6, :cond_4

    move-object v3, v5

    move v4, v6

    :cond_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-nez v5, :cond_3

    :goto_1
    check-cast v3, Lqm0;

    if-eqz v3, :cond_5

    invoke-static {v3, v0}, Lsm0;->e(Lqm0;Ljava/util/List;)Lpm0;

    move-result-object v0

    if-nez v0, :cond_6

    :cond_5
    const/4 v0, 0x3

    invoke-static {v1, v1, v0, v1}, Lsm0;->b(Ljava/lang/Throwable;Ljava/lang/String;ILjava/lang/Object;)Lww0;

    move-result-object v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_2

    :catchall_0
    move-exception v0

    const/4 v2, 0x2

    invoke-static {v0, v1, v2, v1}, Lsm0;->b(Ljava/lang/Throwable;Ljava/lang/String;ILjava/lang/Object;)Lww0;

    move-result-object v0

    :cond_6
    :goto_2
    return-object v0
.end method

.class public final Li02;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Li02$a;
    }
.end annotation


# static fields
.field public static final b:Li02$a;


# instance fields
.field public final a:J


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Li02$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Li02$a;-><init>(Lgq;)V

    sput-object v0, Li02;->b:Li02$a;

    return-void
.end method

.method public synthetic constructor <init>(J)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Li02;->a:J

    return-void
.end method

.method public static final synthetic a(J)Li02;
    .locals 1

    .line 1
    new-instance v0, Li02;

    invoke-direct {v0, p0, p1}, Li02;-><init>(J)V

    return-object v0
.end method

.method public static c(J)J
    .locals 0

    .line 1
    return-wide p0
.end method

.method public static d(JLjava/lang/Object;)Z
    .locals 4

    .line 1
    instance-of v0, p2, Li02;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    return v1

    :cond_0
    check-cast p2, Li02;

    invoke-virtual {p2}, Li02;->g()J

    move-result-wide v2

    cmp-long p0, p0, v2

    if-eqz p0, :cond_1

    return v1

    :cond_1
    const/4 p0, 0x1

    return p0
.end method

.method public static e(J)I
    .locals 0

    .line 1
    invoke-static {p0, p1}, Ljava/lang/Long;->hashCode(J)I

    move-result p0

    return p0
.end method

.method public static f(J)Ljava/lang/String;
    .locals 0

    .line 1
    invoke-static {p0, p1}, Lg12;->c(J)Ljava/lang/String;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public bridge synthetic compareTo(Ljava/lang/Object;)I
    .locals 4

    .line 1
    check-cast p1, Li02;

    invoke-virtual {p1}, Li02;->g()J

    move-result-wide v0

    invoke-virtual {p0}, Li02;->g()J

    move-result-wide v2

    invoke-static {v2, v3, v0, v1}, Lg12;->b(JJ)I

    move-result p1

    return p1
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .line 1
    iget-wide v0, p0, Li02;->a:J

    invoke-static {v0, v1, p1}, Li02;->d(JLjava/lang/Object;)Z

    move-result p1

    return p1
.end method

.method public final synthetic g()J
    .locals 2

    .line 1
    iget-wide v0, p0, Li02;->a:J

    return-wide v0
.end method

.method public hashCode()I
    .locals 2

    .line 1
    iget-wide v0, p0, Li02;->a:J

    invoke-static {v0, v1}, Li02;->e(J)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 1
    iget-wide v0, p0, Li02;->a:J

    invoke-static {v0, v1}, Li02;->f(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

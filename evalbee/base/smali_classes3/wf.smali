.class public abstract Lwf;
.super Lkotlinx/coroutines/a;
.source "SourceFile"

# interfaces
.implements Lvf;


# instance fields
.field public final d:Lvf;


# direct methods
.method public constructor <init>(Lkotlin/coroutines/CoroutineContext;Lvf;ZZ)V
    .locals 0

    .line 1
    invoke-direct {p0, p1, p3, p4}, Lkotlinx/coroutines/a;-><init>(Lkotlin/coroutines/CoroutineContext;ZZ)V

    iput-object p2, p0, Lwf;->d:Lvf;

    return-void
.end method


# virtual methods
.method public B(Ljava/lang/Throwable;)Z
    .locals 1

    .line 1
    iget-object v0, p0, Lwf;->d:Lvf;

    invoke-interface {v0, p1}, Lzk1;->B(Ljava/lang/Throwable;)Z

    move-result p1

    return p1
.end method

.method public U(Ljava/lang/Throwable;)V
    .locals 2

    .line 1
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-static {p0, p1, v0, v1, v0}, Lkotlinx/coroutines/JobSupport;->T0(Lkotlinx/coroutines/JobSupport;Ljava/lang/Throwable;Ljava/lang/String;ILjava/lang/Object;)Ljava/util/concurrent/CancellationException;

    move-result-object p1

    iget-object v0, p0, Lwf;->d:Lvf;

    invoke-interface {v0, p1}, Lkotlinx/coroutines/channels/ReceiveChannel;->b(Ljava/util/concurrent/CancellationException;)V

    invoke-virtual {p0, p1}, Lkotlinx/coroutines/JobSupport;->S(Ljava/lang/Throwable;)Z

    return-void
.end method

.method public final b(Ljava/util/concurrent/CancellationException;)V
    .locals 2

    .line 1
    invoke-virtual {p0}, Lkotlinx/coroutines/JobSupport;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_0

    return-void

    :cond_0
    if-nez p1, :cond_1

    new-instance p1, Lkotlinx/coroutines/JobCancellationException;

    invoke-static {p0}, Lkotlinx/coroutines/JobSupport;->E(Lkotlinx/coroutines/JobSupport;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p1, v0, v1, p0}, Lkotlinx/coroutines/JobCancellationException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;Lkotlinx/coroutines/n;)V

    :cond_1
    invoke-virtual {p0, p1}, Lwf;->U(Ljava/lang/Throwable;)V

    return-void
.end method

.method public c(Lc90;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lwf;->d:Lvf;

    invoke-interface {v0, p1}, Lzk1;->c(Lc90;)V

    return-void
.end method

.method public e(Lvl;)Ljava/lang/Object;
    .locals 1

    .line 1
    iget-object v0, p0, Lwf;->d:Lvf;

    invoke-interface {v0, p1}, Lkotlinx/coroutines/channels/ReceiveChannel;->e(Lvl;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public final e1()Lvf;
    .locals 0

    .line 1
    return-object p0
.end method

.method public final f1()Lvf;
    .locals 1

    .line 1
    iget-object v0, p0, Lwf;->d:Lvf;

    return-object v0
.end method

.method public iterator()Lkotlinx/coroutines/channels/ChannelIterator;
    .locals 1

    .line 1
    iget-object v0, p0, Lwf;->d:Lvf;

    invoke-interface {v0}, Lkotlinx/coroutines/channels/ReceiveChannel;->iterator()Lkotlinx/coroutines/channels/ChannelIterator;

    move-result-object v0

    return-object v0
.end method

.method public m(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .line 1
    iget-object v0, p0, Lwf;->d:Lvf;

    invoke-interface {v0, p1}, Lzk1;->m(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

.method public n()Lrk1;
    .locals 1

    .line 1
    iget-object v0, p0, Lwf;->d:Lvf;

    invoke-interface {v0}, Lkotlinx/coroutines/channels/ReceiveChannel;->n()Lrk1;

    move-result-object v0

    return-object v0
.end method

.method public p()Ljava/lang/Object;
    .locals 1

    .line 1
    iget-object v0, p0, Lwf;->d:Lvf;

    invoke-interface {v0}, Lkotlinx/coroutines/channels/ReceiveChannel;->p()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public s()Z
    .locals 1

    .line 1
    iget-object v0, p0, Lwf;->d:Lvf;

    invoke-interface {v0}, Lzk1;->s()Z

    move-result v0

    return v0
.end method

.method public v(Lvl;)Ljava/lang/Object;
    .locals 1

    .line 1
    iget-object v0, p0, Lwf;->d:Lvf;

    invoke-interface {v0, p1}, Lkotlinx/coroutines/channels/ReceiveChannel;->v(Lvl;)Ljava/lang/Object;

    move-result-object p1

    invoke-static {}, Lgg0;->d()Ljava/lang/Object;

    return-object p1
.end method

.method public w()Lrk1;
    .locals 1

    .line 1
    iget-object v0, p0, Lwf;->d:Lvf;

    invoke-interface {v0}, Lkotlinx/coroutines/channels/ReceiveChannel;->w()Lrk1;

    move-result-object v0

    return-object v0
.end method

.method public z(Ljava/lang/Object;Lvl;)Ljava/lang/Object;
    .locals 1

    .line 1
    iget-object v0, p0, Lwf;->d:Lvf;

    invoke-interface {v0, p1, p2}, Lzk1;->z(Ljava/lang/Object;Lvl;)Ljava/lang/Object;

    move-result-object p1

    return-object p1
.end method

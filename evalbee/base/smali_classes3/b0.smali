.class public abstract Lb0;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:[Ld0;

.field public b:I

.field public c:I

.field public d:Lcs1;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final synthetic f(Lb0;)I
    .locals 0

    .line 1
    iget p0, p0, Lb0;->b:I

    return p0
.end method

.method public static final synthetic g(Lb0;)[Ld0;
    .locals 0

    .line 1
    iget-object p0, p0, Lb0;->a:[Ld0;

    return-object p0
.end method


# virtual methods
.method public final d()Lvp1;
    .locals 2

    .line 1
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lb0;->d:Lcs1;

    if-nez v0, :cond_0

    new-instance v0, Lcs1;

    iget v1, p0, Lb0;->b:I

    invoke-direct {v0, v1}, Lcs1;-><init>(I)V

    iput-object v0, p0, Lb0;->d:Lcs1;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final h()Ld0;
    .locals 4

    .line 1
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lb0;->a:[Ld0;

    const/4 v1, 0x2

    if-nez v0, :cond_0

    invoke-virtual {p0, v1}, Lb0;->j(I)[Ld0;

    move-result-object v0

    iput-object v0, p0, Lb0;->a:[Ld0;

    goto :goto_0

    :cond_0
    iget v2, p0, Lb0;->b:I

    array-length v3, v0

    if-lt v2, v3, :cond_1

    array-length v2, v0

    mul-int/2addr v2, v1

    invoke-static {v0, v2}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    const-string v1, "copyOf(this, newSize)"

    invoke-static {v0, v1}, Lfg0;->d(Ljava/lang/Object;Ljava/lang/String;)V

    move-object v1, v0

    check-cast v1, [Ld0;

    iput-object v1, p0, Lb0;->a:[Ld0;

    check-cast v0, [Ld0;

    :cond_1
    :goto_0
    iget v1, p0, Lb0;->c:I

    :cond_2
    aget-object v2, v0, v1

    if-nez v2, :cond_3

    invoke-virtual {p0}, Lb0;->i()Ld0;

    move-result-object v2

    aput-object v2, v0, v1

    :cond_3
    add-int/lit8 v1, v1, 0x1

    array-length v3, v0

    if-lt v1, v3, :cond_4

    const/4 v1, 0x0

    :cond_4
    const-string v3, "null cannot be cast to non-null type kotlinx.coroutines.flow.internal.AbstractSharedFlowSlot<kotlin.Any>"

    invoke-static {v2, v3}, Lfg0;->c(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ld0;->a(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iput v1, p0, Lb0;->c:I

    iget v0, p0, Lb0;->b:I

    const/4 v1, 0x1

    add-int/2addr v0, v1

    iput v0, p0, Lb0;->b:I

    iget-object v0, p0, Lb0;->d:Lcs1;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    if-eqz v0, :cond_5

    invoke-virtual {v0, v1}, Lcs1;->Y(I)Z

    :cond_5
    return-object v2

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public abstract i()Ld0;
.end method

.method public abstract j(I)[Ld0;
.end method

.method public final k(Ld0;)V
    .locals 6

    .line 1
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lb0;->b:I

    const/4 v1, -0x1

    add-int/2addr v0, v1

    iput v0, p0, Lb0;->b:I

    iget-object v2, p0, Lb0;->d:Lcs1;

    const/4 v3, 0x0

    if-nez v0, :cond_0

    iput v3, p0, Lb0;->c:I

    :cond_0
    const-string v0, "null cannot be cast to non-null type kotlinx.coroutines.flow.internal.AbstractSharedFlowSlot<kotlin.Any>"

    invoke-static {p1, v0}, Lfg0;->c(Ljava/lang/Object;Ljava/lang/String;)V

    invoke-virtual {p1, p0}, Ld0;->b(Ljava/lang/Object;)[Lvl;

    move-result-object p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    array-length v0, p1

    :goto_0
    if-ge v3, v0, :cond_2

    aget-object v4, p1, v3

    if-eqz v4, :cond_1

    sget-object v5, Lkotlin/Result;->Companion:Lkotlin/Result$a;

    sget-object v5, Lu02;->a:Lu02;

    invoke-static {v5}, Lkotlin/Result;->constructor-impl(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    invoke-interface {v4, v5}, Lvl;->resumeWith(Ljava/lang/Object;)V

    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_2
    if-eqz v2, :cond_3

    invoke-virtual {v2, v1}, Lcs1;->Y(I)Z

    :cond_3
    return-void

    :catchall_0
    move-exception p1

    monitor-exit p0

    throw p1
.end method

.method public final l()I
    .locals 1

    .line 1
    iget v0, p0, Lb0;->b:I

    return v0
.end method

.method public final m()[Ld0;
    .locals 1

    .line 1
    iget-object v0, p0, Lb0;->a:[Ld0;

    return-object v0
.end method

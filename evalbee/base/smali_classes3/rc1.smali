.class public final Lrc1;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lwc;


# instance fields
.field public final a:Lokio/a;

.field public final b:Lho1;

.field public c:Z


# direct methods
.method public constructor <init>(Lho1;)V
    .locals 1

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lokio/a;

    invoke-direct {v0}, Lokio/a;-><init>()V

    iput-object v0, p0, Lrc1;->a:Lokio/a;

    if-eqz p1, :cond_0

    iput-object p1, p0, Lrc1;->b:Lho1;

    return-void

    :cond_0
    new-instance p1, Ljava/lang/NullPointerException;

    const-string v0, "sink == null"

    invoke-direct {p1, v0}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public V(Ljava/lang/String;)Lwc;
    .locals 1

    .line 1
    iget-boolean v0, p0, Lrc1;->c:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lrc1;->a:Lokio/a;

    invoke-virtual {v0, p1}, Lokio/a;->w0(Ljava/lang/String;)Lokio/a;

    invoke-virtual {p0}, Lrc1;->a()Lwc;

    move-result-object p1

    return-object p1

    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "closed"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public a()Lwc;
    .locals 4

    .line 1
    iget-boolean v0, p0, Lrc1;->c:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lrc1;->a:Lokio/a;

    invoke-virtual {v0}, Lokio/a;->e()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    iget-object v2, p0, Lrc1;->b:Lho1;

    iget-object v3, p0, Lrc1;->a:Lokio/a;

    invoke-interface {v2, v3, v0, v1}, Lho1;->write(Lokio/a;J)V

    :cond_0
    return-object p0

    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "closed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public close()V
    .locals 5

    .line 1
    iget-boolean v0, p0, Lrc1;->c:Z

    if-eqz v0, :cond_0

    return-void

    :cond_0
    :try_start_0
    iget-object v0, p0, Lrc1;->a:Lokio/a;

    iget-wide v1, v0, Lokio/a;->b:J

    const-wide/16 v3, 0x0

    cmp-long v3, v1, v3

    if-lez v3, :cond_1

    iget-object v3, p0, Lrc1;->b:Lho1;

    invoke-interface {v3, v0, v1, v2}, Lho1;->write(Lokio/a;J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    :goto_0
    :try_start_1
    iget-object v1, p0, Lrc1;->b:Lho1;

    invoke-interface {v1}, Lho1;->close()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_1

    :catchall_1
    move-exception v1

    if-nez v0, :cond_2

    move-object v0, v1

    :cond_2
    :goto_1
    const/4 v1, 0x1

    iput-boolean v1, p0, Lrc1;->c:Z

    if-eqz v0, :cond_3

    invoke-static {v0}, Ln22;->c(Ljava/lang/Throwable;)V

    :cond_3
    return-void
.end method

.method public flush()V
    .locals 5

    .line 1
    iget-boolean v0, p0, Lrc1;->c:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lrc1;->a:Lokio/a;

    iget-wide v1, v0, Lokio/a;->b:J

    const-wide/16 v3, 0x0

    cmp-long v3, v1, v3

    if-lez v3, :cond_0

    iget-object v3, p0, Lrc1;->b:Lho1;

    invoke-interface {v3, v0, v1, v2}, Lho1;->write(Lokio/a;J)V

    :cond_0
    iget-object v0, p0, Lrc1;->b:Lho1;

    invoke-interface {v0}, Lho1;->flush()V

    return-void

    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "closed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public isOpen()Z
    .locals 1

    .line 1
    iget-boolean v0, p0, Lrc1;->c:Z

    xor-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "buffer("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lrc1;->b:Lho1;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public write(Ljava/nio/ByteBuffer;)I
    .locals 1

    .line 1
    iget-boolean v0, p0, Lrc1;->c:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lrc1;->a:Lokio/a;

    invoke-virtual {v0, p1}, Lokio/a;->write(Ljava/nio/ByteBuffer;)I

    move-result p1

    invoke-virtual {p0}, Lrc1;->a()Lwc;

    return p1

    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "closed"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public write([B)Lwc;
    .locals 1

    .line 2
    iget-boolean v0, p0, Lrc1;->c:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lrc1;->a:Lokio/a;

    invoke-virtual {v0, p1}, Lokio/a;->p0([B)Lokio/a;

    invoke-virtual {p0}, Lrc1;->a()Lwc;

    move-result-object p1

    return-object p1

    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "closed"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public write(Lokio/a;J)V
    .locals 1

    .line 3
    iget-boolean v0, p0, Lrc1;->c:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lrc1;->a:Lokio/a;

    invoke-virtual {v0, p1, p2, p3}, Lokio/a;->write(Lokio/a;J)V

    invoke-virtual {p0}, Lrc1;->a()Lwc;

    return-void

    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string p2, "closed"

    invoke-direct {p1, p2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public writeByte(I)Lwc;
    .locals 1

    .line 1
    iget-boolean v0, p0, Lrc1;->c:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lrc1;->a:Lokio/a;

    invoke-virtual {v0, p1}, Lokio/a;->r0(I)Lokio/a;

    invoke-virtual {p0}, Lrc1;->a()Lwc;

    move-result-object p1

    return-object p1

    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "closed"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public writeInt(I)Lwc;
    .locals 1

    .line 1
    iget-boolean v0, p0, Lrc1;->c:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lrc1;->a:Lokio/a;

    invoke-virtual {v0, p1}, Lokio/a;->t0(I)Lokio/a;

    invoke-virtual {p0}, Lrc1;->a()Lwc;

    move-result-object p1

    return-object p1

    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "closed"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.method public writeShort(I)Lwc;
    .locals 1

    .line 1
    iget-boolean v0, p0, Lrc1;->c:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lrc1;->a:Lokio/a;

    invoke-virtual {v0, p1}, Lokio/a;->u0(I)Lokio/a;

    invoke-virtual {p0}, Lrc1;->a()Lwc;

    move-result-object p1

    return-object p1

    :cond_0
    new-instance p1, Ljava/lang/IllegalStateException;

    const-string v0, "closed"

    invoke-direct {p1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw p1
.end method

.class public abstract Lfa2;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static final a(Lvl;)Ljava/lang/Object;
    .locals 4

    .line 1
    invoke-interface {p0}, Lvl;->getContext()Lkotlin/coroutines/CoroutineContext;

    move-result-object v0

    invoke-static {v0}, Lah0;->i(Lkotlin/coroutines/CoroutineContext;)V

    invoke-static {p0}, Lkotlin/coroutines/intrinsics/IntrinsicsKt__IntrinsicsJvmKt;->c(Lvl;)Lvl;

    move-result-object v1

    instance-of v2, v1, Lmt;

    if-eqz v2, :cond_0

    check-cast v1, Lmt;

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_0
    if-nez v1, :cond_1

    sget-object v0, Lu02;->a:Lu02;

    goto :goto_2

    :cond_1
    iget-object v2, v1, Lmt;->d:Lkotlinx/coroutines/CoroutineDispatcher;

    invoke-virtual {v2, v0}, Lkotlinx/coroutines/CoroutineDispatcher;->q0(Lkotlin/coroutines/CoroutineContext;)Z

    move-result v2

    if-eqz v2, :cond_2

    sget-object v2, Lu02;->a:Lu02;

    invoke-virtual {v1, v0, v2}, Lmt;->k(Lkotlin/coroutines/CoroutineContext;Ljava/lang/Object;)V

    goto :goto_1

    :cond_2
    new-instance v2, Lkotlinx/coroutines/q;

    invoke-direct {v2}, Lkotlinx/coroutines/q;-><init>()V

    invoke-interface {v0, v2}, Lkotlin/coroutines/CoroutineContext;->plus(Lkotlin/coroutines/CoroutineContext;)Lkotlin/coroutines/CoroutineContext;

    move-result-object v0

    sget-object v3, Lu02;->a:Lu02;

    invoke-virtual {v1, v0, v3}, Lmt;->k(Lkotlin/coroutines/CoroutineContext;Ljava/lang/Object;)V

    iget-boolean v0, v2, Lkotlinx/coroutines/q;->b:Z

    if-eqz v0, :cond_4

    invoke-static {v1}, Lnt;->d(Lmt;)Z

    move-result v0

    if-eqz v0, :cond_3

    goto :goto_1

    :cond_3
    move-object v0, v3

    goto :goto_2

    :cond_4
    :goto_1
    invoke-static {}, Lgg0;->d()Ljava/lang/Object;

    move-result-object v0

    :goto_2
    invoke-static {}, Lgg0;->d()Ljava/lang/Object;

    move-result-object v1

    if-ne v0, v1, :cond_5

    invoke-static {p0}, Lzp;->c(Lvl;)V

    :cond_5
    invoke-static {}, Lgg0;->d()Ljava/lang/Object;

    move-result-object p0

    if-ne v0, p0, :cond_6

    return-object v0

    :cond_6
    sget-object p0, Lu02;->a:Lu02;

    return-object p0
.end method

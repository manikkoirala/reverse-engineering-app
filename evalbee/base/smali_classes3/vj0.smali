.class public final Lvj0;
.super Lkotlinx/coroutines/CoroutineDispatcher;
.source "SourceFile"

# interfaces
.implements Lkotlinx/coroutines/h;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lvj0$a;
    }
.end annotation


# static fields
.field public static final h:Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;


# instance fields
.field public final c:Lkotlinx/coroutines/CoroutineDispatcher;

.field public final d:I

.field public final synthetic e:Lkotlinx/coroutines/h;

.field public final f:Lsl0;

.field public final g:Ljava/lang/Object;

.field private volatile runningWorkers:I


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .line 1
    const-class v0, Lvj0;

    const-string v1, "runningWorkers"

    invoke-static {v0, v1}, Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;->newUpdater(Ljava/lang/Class;Ljava/lang/String;)Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;

    move-result-object v0

    sput-object v0, Lvj0;->h:Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;

    return-void
.end method

.method public constructor <init>(Lkotlinx/coroutines/CoroutineDispatcher;I)V
    .locals 0

    .line 1
    invoke-direct {p0}, Lkotlinx/coroutines/CoroutineDispatcher;-><init>()V

    iput-object p1, p0, Lvj0;->c:Lkotlinx/coroutines/CoroutineDispatcher;

    iput p2, p0, Lvj0;->d:I

    instance-of p2, p1, Lkotlinx/coroutines/h;

    if-eqz p2, :cond_0

    check-cast p1, Lkotlinx/coroutines/h;

    goto :goto_0

    :cond_0
    const/4 p1, 0x0

    :goto_0
    if-nez p1, :cond_1

    invoke-static {}, Lmq;->a()Lkotlinx/coroutines/h;

    move-result-object p1

    :cond_1
    iput-object p1, p0, Lvj0;->e:Lkotlinx/coroutines/h;

    new-instance p1, Lsl0;

    const/4 p2, 0x0

    invoke-direct {p1, p2}, Lsl0;-><init>(Z)V

    iput-object p1, p0, Lvj0;->f:Lsl0;

    new-instance p1, Ljava/lang/Object;

    invoke-direct {p1}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lvj0;->g:Ljava/lang/Object;

    return-void
.end method

.method public static final synthetic s0(Lvj0;)Lkotlinx/coroutines/CoroutineDispatcher;
    .locals 0

    .line 1
    iget-object p0, p0, Lvj0;->c:Lkotlinx/coroutines/CoroutineDispatcher;

    return-object p0
.end method

.method public static final synthetic t0(Lvj0;)Ljava/lang/Runnable;
    .locals 0

    .line 1
    invoke-virtual {p0}, Lvj0;->u0()Ljava/lang/Runnable;

    move-result-object p0

    return-object p0
.end method


# virtual methods
.method public c(JLdf;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lvj0;->e:Lkotlinx/coroutines/h;

    invoke-interface {v0, p1, p2, p3}, Lkotlinx/coroutines/h;->c(JLdf;)V

    return-void
.end method

.method public e(JLjava/lang/Runnable;Lkotlin/coroutines/CoroutineContext;)Lut;
    .locals 1

    .line 1
    iget-object v0, p0, Lvj0;->e:Lkotlinx/coroutines/h;

    invoke-interface {v0, p1, p2, p3, p4}, Lkotlinx/coroutines/h;->e(JLjava/lang/Runnable;Lkotlin/coroutines/CoroutineContext;)Lut;

    move-result-object p1

    return-object p1
.end method

.method public f(Lkotlin/coroutines/CoroutineContext;Ljava/lang/Runnable;)V
    .locals 0

    .line 1
    iget-object p1, p0, Lvj0;->f:Lsl0;

    invoke-virtual {p1, p2}, Lsl0;->a(Ljava/lang/Object;)Z

    sget-object p1, Lvj0;->h:Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;

    invoke-virtual {p1, p0}, Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;->get(Ljava/lang/Object;)I

    move-result p1

    iget p2, p0, Lvj0;->d:I

    if-ge p1, p2, :cond_1

    invoke-virtual {p0}, Lvj0;->v0()Z

    move-result p1

    if-eqz p1, :cond_1

    invoke-virtual {p0}, Lvj0;->u0()Ljava/lang/Runnable;

    move-result-object p1

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    new-instance p2, Lvj0$a;

    invoke-direct {p2, p0, p1}, Lvj0$a;-><init>(Lvj0;Ljava/lang/Runnable;)V

    iget-object p1, p0, Lvj0;->c:Lkotlinx/coroutines/CoroutineDispatcher;

    invoke-virtual {p1, p0, p2}, Lkotlinx/coroutines/CoroutineDispatcher;->f(Lkotlin/coroutines/CoroutineContext;Ljava/lang/Runnable;)V

    :cond_1
    :goto_0
    return-void
.end method

.method public h(Lkotlin/coroutines/CoroutineContext;Ljava/lang/Runnable;)V
    .locals 0

    .line 1
    iget-object p1, p0, Lvj0;->f:Lsl0;

    invoke-virtual {p1, p2}, Lsl0;->a(Ljava/lang/Object;)Z

    sget-object p1, Lvj0;->h:Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;

    invoke-virtual {p1, p0}, Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;->get(Ljava/lang/Object;)I

    move-result p1

    iget p2, p0, Lvj0;->d:I

    if-ge p1, p2, :cond_1

    invoke-virtual {p0}, Lvj0;->v0()Z

    move-result p1

    if-eqz p1, :cond_1

    invoke-virtual {p0}, Lvj0;->u0()Ljava/lang/Runnable;

    move-result-object p1

    if-nez p1, :cond_0

    goto :goto_0

    :cond_0
    new-instance p2, Lvj0$a;

    invoke-direct {p2, p0, p1}, Lvj0$a;-><init>(Lvj0;Ljava/lang/Runnable;)V

    iget-object p1, p0, Lvj0;->c:Lkotlinx/coroutines/CoroutineDispatcher;

    invoke-virtual {p1, p0, p2}, Lkotlinx/coroutines/CoroutineDispatcher;->h(Lkotlin/coroutines/CoroutineContext;Ljava/lang/Runnable;)V

    :cond_1
    :goto_0
    return-void
.end method

.method public final u0()Ljava/lang/Runnable;
    .locals 3

    .line 1
    :goto_0
    iget-object v0, p0, Lvj0;->f:Lsl0;

    invoke-virtual {v0}, Lsl0;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    if-nez v0, :cond_1

    iget-object v0, p0, Lvj0;->g:Ljava/lang/Object;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lvj0;->h:Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;

    invoke-virtual {v1, p0}, Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;->decrementAndGet(Ljava/lang/Object;)I

    iget-object v2, p0, Lvj0;->f:Lsl0;

    invoke-virtual {v2}, Lsl0;->c()I

    move-result v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v2, :cond_0

    monitor-exit v0

    const/4 v0, 0x0

    return-object v0

    :cond_0
    :try_start_1
    invoke-virtual {v1, p0}, Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;->incrementAndGet(Ljava/lang/Object;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit v0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1

    :cond_1
    return-object v0
.end method

.method public final v0()Z
    .locals 4

    .line 1
    iget-object v0, p0, Lvj0;->g:Ljava/lang/Object;

    monitor-enter v0

    :try_start_0
    sget-object v1, Lvj0;->h:Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;

    invoke-virtual {v1, p0}, Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;->get(Ljava/lang/Object;)I

    move-result v2

    iget v3, p0, Lvj0;->d:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-lt v2, v3, :cond_0

    monitor-exit v0

    const/4 v0, 0x0

    return v0

    :cond_0
    :try_start_1
    invoke-virtual {v1, p0}, Ljava/util/concurrent/atomic/AtomicIntegerFieldUpdater;->incrementAndGet(Ljava/lang/Object;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit v0

    const/4 v0, 0x1

    return v0

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

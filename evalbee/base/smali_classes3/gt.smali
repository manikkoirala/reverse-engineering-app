.class public final Lgt;
.super Ljava/nio/file/SimpleFileVisitor;
.source "SourceFile"


# instance fields
.field public final a:Z

.field public b:Lh31;

.field public c:Lj8;


# direct methods
.method public constructor <init>(Z)V
    .locals 0

    .line 1
    invoke-direct {p0}, Ljava/nio/file/SimpleFileVisitor;-><init>()V

    iput-boolean p1, p0, Lgt;->a:Z

    new-instance p1, Lj8;

    invoke-direct {p1}, Lj8;-><init>()V

    iput-object p1, p0, Lgt;->c:Lj8;

    return-void
.end method


# virtual methods
.method public a(Ljava/nio/file/Path;Ljava/nio/file/attribute/BasicFileAttributes;)Ljava/nio/file/FileVisitResult;
    .locals 3

    .line 1
    const-string v0, "dir"

    invoke-static {p1, v0}, Lfg0;->e(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "attrs"

    invoke-static {p2, v0}, Lfg0;->e(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lh31;

    invoke-static {p2}, Lft;->a(Ljava/nio/file/attribute/BasicFileAttributes;)Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, Lgt;->b:Lh31;

    invoke-direct {v0, p1, v1, v2}, Lh31;-><init>(Ljava/nio/file/Path;Ljava/lang/Object;Lh31;)V

    iget-object v1, p0, Lgt;->c:Lj8;

    invoke-virtual {v1, v0}, Lj8;->add(Ljava/lang/Object;)Z

    invoke-super {p0, p1, p2}, Ljava/nio/file/SimpleFileVisitor;->preVisitDirectory(Ljava/lang/Object;Ljava/nio/file/attribute/BasicFileAttributes;)Ljava/nio/file/FileVisitResult;

    move-result-object p1

    const-string p2, "super.preVisitDirectory(dir, attrs)"

    invoke-static {p1, p2}, Lfg0;->d(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public final b(Lh31;)Ljava/util/List;
    .locals 3

    .line 1
    const-string v0, "directoryNode"

    invoke-static {p1, v0}, Lfg0;->e(Ljava/lang/Object;Ljava/lang/String;)V

    iput-object p1, p0, Lgt;->b:Lh31;

    invoke-virtual {p1}, Lh31;->d()Ljava/nio/file/Path;

    move-result-object p1

    sget-object v0, Lak0;->a:Lak0;

    iget-boolean v1, p0, Lgt;->a:Z

    invoke-virtual {v0, v1}, Lak0;->b(Z)Ljava/util/Set;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {p0}, Ldt;->a(Ljava/lang/Object;)Ljava/nio/file/FileVisitor;

    move-result-object v2

    invoke-static {p1, v0, v1, v2}, Let;->a(Ljava/nio/file/Path;Ljava/util/Set;ILjava/nio/file/FileVisitor;)Ljava/nio/file/Path;

    iget-object p1, p0, Lgt;->c:Lj8;

    invoke-virtual {p1}, Lj8;->removeFirst()Ljava/lang/Object;

    iget-object p1, p0, Lgt;->c:Lj8;

    new-instance v0, Lj8;

    invoke-direct {v0}, Lj8;-><init>()V

    iput-object v0, p0, Lgt;->c:Lj8;

    return-object p1
.end method

.method public c(Ljava/nio/file/Path;Ljava/nio/file/attribute/BasicFileAttributes;)Ljava/nio/file/FileVisitResult;
    .locals 3

    .line 1
    const-string v0, "file"

    invoke-static {p1, v0}, Lfg0;->e(Ljava/lang/Object;Ljava/lang/String;)V

    const-string v0, "attrs"

    invoke-static {p2, v0}, Lfg0;->e(Ljava/lang/Object;Ljava/lang/String;)V

    new-instance v0, Lh31;

    const/4 v1, 0x0

    iget-object v2, p0, Lgt;->b:Lh31;

    invoke-direct {v0, p1, v1, v2}, Lh31;-><init>(Ljava/nio/file/Path;Ljava/lang/Object;Lh31;)V

    iget-object v1, p0, Lgt;->c:Lj8;

    invoke-virtual {v1, v0}, Lj8;->add(Ljava/lang/Object;)Z

    invoke-super {p0, p1, p2}, Ljava/nio/file/SimpleFileVisitor;->visitFile(Ljava/lang/Object;Ljava/nio/file/attribute/BasicFileAttributes;)Ljava/nio/file/FileVisitResult;

    move-result-object p1

    const-string p2, "super.visitFile(file, attrs)"

    invoke-static {p1, p2}, Lfg0;->d(Ljava/lang/Object;Ljava/lang/String;)V

    return-object p1
.end method

.method public bridge synthetic preVisitDirectory(Ljava/lang/Object;Ljava/nio/file/attribute/BasicFileAttributes;)Ljava/nio/file/FileVisitResult;
    .locals 0

    .line 1
    invoke-static {p1}, Lct;->a(Ljava/lang/Object;)Ljava/nio/file/Path;

    move-result-object p1

    invoke-virtual {p0, p1, p2}, Lgt;->a(Ljava/nio/file/Path;Ljava/nio/file/attribute/BasicFileAttributes;)Ljava/nio/file/FileVisitResult;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic visitFile(Ljava/lang/Object;Ljava/nio/file/attribute/BasicFileAttributes;)Ljava/nio/file/FileVisitResult;
    .locals 0

    .line 1
    invoke-static {p1}, Lct;->a(Ljava/lang/Object;)Ljava/nio/file/Path;

    move-result-object p1

    invoke-virtual {p0, p1, p2}, Lgt;->c(Ljava/nio/file/Path;Ljava/nio/file/attribute/BasicFileAttributes;)Ljava/nio/file/FileVisitResult;

    move-result-object p1

    return-object p1
.end method

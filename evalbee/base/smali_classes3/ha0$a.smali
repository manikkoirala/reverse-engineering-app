.class public final Lha0$a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lha0;->iterator()Ljava/util/Iterator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = null
.end annotation


# instance fields
.field public a:Ljava/lang/Object;

.field public b:I

.field public final synthetic c:Lha0;


# direct methods
.method public constructor <init>(Lha0;)V
    .locals 0

    .line 1
    iput-object p1, p0, Lha0$a;->c:Lha0;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 p1, -0x2

    iput p1, p0, Lha0$a;->b:I

    return-void
.end method


# virtual methods
.method public final b()V
    .locals 2

    .line 1
    iget v0, p0, Lha0$a;->b:I

    const/4 v1, -0x2

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lha0$a;->c:Lha0;

    invoke-static {v0}, Lha0;->a(Lha0;)La90;

    move-result-object v0

    invoke-interface {v0}, La90;->invoke()Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lha0$a;->c:Lha0;

    invoke-static {v0}, Lha0;->b(Lha0;)Lc90;

    move-result-object v0

    iget-object v1, p0, Lha0$a;->a:Ljava/lang/Object;

    invoke-static {v1}, Lfg0;->b(Ljava/lang/Object;)V

    invoke-interface {v0, v1}, Lc90;->invoke(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lha0$a;->a:Ljava/lang/Object;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    const/4 v0, 0x1

    :goto_1
    iput v0, p0, Lha0$a;->b:I

    return-void
.end method

.method public hasNext()Z
    .locals 2

    .line 1
    iget v0, p0, Lha0$a;->b:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lha0$a;->b()V

    :cond_0
    iget v0, p0, Lha0$a;->b:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    :goto_0
    return v1
.end method

.method public next()Ljava/lang/Object;
    .locals 2

    .line 1
    iget v0, p0, Lha0$a;->b:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lha0$a;->b()V

    :cond_0
    iget v0, p0, Lha0$a;->b:I

    if-eqz v0, :cond_1

    iget-object v0, p0, Lha0$a;->a:Ljava/lang/Object;

    const-string v1, "null cannot be cast to non-null type T of kotlin.sequences.GeneratorSequence"

    invoke-static {v0, v1}, Lfg0;->c(Ljava/lang/Object;Ljava/lang/String;)V

    const/4 v1, -0x1

    iput v1, p0, Lha0$a;->b:I

    return-object v0

    :cond_1
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0
.end method

.method public remove()V
    .locals 2

    .line 1
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Operation is not supported for read-only collection"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

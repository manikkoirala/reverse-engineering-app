.class public abstract Lem0;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Iterable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lem0$a;
    }
.end annotation


# static fields
.field public static final d:Lem0$a;


# instance fields
.field public final a:J

.field public final b:J

.field public final c:J


# direct methods
.method public static constructor <clinit>()V
    .locals 2

    .line 1
    new-instance v0, Lem0$a;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lem0$a;-><init>(Lgq;)V

    sput-object v0, Lem0;->d:Lem0$a;

    return-void
.end method

.method public constructor <init>(JJJ)V
    .locals 2

    .line 1
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, 0x0

    cmp-long v0, p5, v0

    if-eqz v0, :cond_1

    const-wide/high16 v0, -0x8000000000000000L

    cmp-long v0, p5, v0

    if-eqz v0, :cond_0

    iput-wide p1, p0, Lem0;->a:J

    invoke-static/range {p1 .. p6}, Lf91;->d(JJJ)J

    move-result-wide p1

    iput-wide p1, p0, Lem0;->b:J

    iput-wide p5, p0, Lem0;->c:J

    return-void

    :cond_0
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Step must be greater than Long.MIN_VALUE to avoid overflow on negation."

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1

    :cond_1
    new-instance p1, Ljava/lang/IllegalArgumentException;

    const-string p2, "Step must be non-zero."

    invoke-direct {p1, p2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw p1
.end method


# virtual methods
.method public final a()J
    .locals 2

    .line 1
    iget-wide v0, p0, Lem0;->a:J

    return-wide v0
.end method

.method public final b()J
    .locals 2

    .line 1
    iget-wide v0, p0, Lem0;->b:J

    return-wide v0
.end method

.method public c()Ldm0;
    .locals 8

    .line 1
    new-instance v7, Lfm0;

    iget-wide v1, p0, Lem0;->a:J

    iget-wide v3, p0, Lem0;->b:J

    iget-wide v5, p0, Lem0;->c:J

    move-object v0, v7

    invoke-direct/range {v0 .. v6}, Lfm0;-><init>(JJJ)V

    return-object v7
.end method

.method public bridge synthetic iterator()Ljava/util/Iterator;
    .locals 1

    .line 1
    invoke-virtual {p0}, Lem0;->c()Ldm0;

    move-result-object v0

    return-object v0
.end method

.class public abstract Leg0;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static final synthetic a(Lkotlin/coroutines/CoroutineContext;La90;)Ljava/lang/Object;
    .locals 0

    .line 1
    invoke-static {p0, p1}, Leg0;->b(Lkotlin/coroutines/CoroutineContext;La90;)Ljava/lang/Object;

    move-result-object p0

    return-object p0
.end method

.method public static final b(Lkotlin/coroutines/CoroutineContext;La90;)Ljava/lang/Object;
    .locals 1

    .line 1
    :try_start_0
    new-instance v0, Lcw1;

    invoke-static {p0}, Lah0;->k(Lkotlin/coroutines/CoroutineContext;)Lkotlinx/coroutines/n;

    move-result-object p0

    invoke-direct {v0, p0}, Lcw1;-><init>(Lkotlinx/coroutines/n;)V

    invoke-virtual {v0}, Lcw1;->d()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    invoke-interface {p1}, La90;->invoke()Ljava/lang/Object;

    move-result-object p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    invoke-virtual {v0}, Lcw1;->a()V

    return-object p0

    :catchall_0
    move-exception p0

    invoke-virtual {v0}, Lcw1;->a()V

    throw p0
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception p0

    new-instance p1, Ljava/util/concurrent/CancellationException;

    const-string v0, "Blocking call was interrupted due to parent cancellation"

    invoke-direct {p1, v0}, Ljava/util/concurrent/CancellationException;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, p0}, Ljava/lang/Throwable;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object p0

    throw p0
.end method

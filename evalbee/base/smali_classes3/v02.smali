.class public final Lv02;
.super Lkotlinx/coroutines/CoroutineDispatcher;
.source "SourceFile"


# static fields
.field public static final c:Lv02;


# direct methods
.method public static constructor <clinit>()V
    .locals 1

    .line 1
    new-instance v0, Lv02;

    invoke-direct {v0}, Lv02;-><init>()V

    sput-object v0, Lv02;->c:Lv02;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lkotlinx/coroutines/CoroutineDispatcher;-><init>()V

    return-void
.end method


# virtual methods
.method public f(Lkotlin/coroutines/CoroutineContext;Ljava/lang/Runnable;)V
    .locals 2

    .line 1
    sget-object p1, Lar;->i:Lar;

    sget-object v0, Lru1;->h:Lfu1;

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, v1}, Ljj1;->t0(Ljava/lang/Runnable;Lfu1;Z)V

    return-void
.end method

.method public h(Lkotlin/coroutines/CoroutineContext;Ljava/lang/Runnable;)V
    .locals 2

    .line 1
    sget-object p1, Lar;->i:Lar;

    sget-object v0, Lru1;->h:Lfu1;

    const/4 v1, 0x1

    invoke-virtual {p1, p2, v0, v1}, Ljj1;->t0(Ljava/lang/Runnable;Lfu1;Z)V

    return-void
.end method

.method public r0(I)Lkotlinx/coroutines/CoroutineDispatcher;
    .locals 1

    .line 1
    invoke-static {p1}, Lwj0;->a(I)V

    sget v0, Lru1;->d:I

    if-lt p1, v0, :cond_0

    return-object p0

    :cond_0
    invoke-super {p0, p1}, Lkotlinx/coroutines/CoroutineDispatcher;->r0(I)Lkotlinx/coroutines/CoroutineDispatcher;

    move-result-object p1

    return-object p1
.end method

.class public interface abstract Lcom/microsoft/schemas/office/excel/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/xmlbeans/XmlObject;


# virtual methods
.method public abstract addNewAnchor()Lorg/apache/xmlbeans/XmlString;
.end method

.method public abstract addNewAutoFill()Lcom/microsoft/schemas/office/excel/STTrueFalseBlank;
.end method

.method public abstract addNewColumn()Lorg/apache/xmlbeans/XmlInteger;
.end method

.method public abstract addNewMoveWithCells()Lcom/microsoft/schemas/office/excel/STTrueFalseBlank;
.end method

.method public abstract addNewRow()Lorg/apache/xmlbeans/XmlInteger;
.end method

.method public abstract addNewSizeWithCells()Lcom/microsoft/schemas/office/excel/STTrueFalseBlank;
.end method

.method public abstract getAnchorArray(I)Ljava/lang/String;
.end method

.method public abstract getColumnArray(I)Ljava/math/BigInteger;
.end method

.method public abstract getObjectType()Lcom/microsoft/schemas/office/excel/STObjectType$Enum;
.end method

.method public abstract getRowArray(I)Ljava/math/BigInteger;
.end method

.method public abstract setAnchorArray(ILjava/lang/String;)V
.end method

.method public abstract setColumnArray(ILjava/math/BigInteger;)V
.end method

.method public abstract setObjectType(Lcom/microsoft/schemas/office/excel/STObjectType$Enum;)V
.end method

.method public abstract setRowArray(ILjava/math/BigInteger;)V
.end method

.class public interface abstract Lcom/microsoft/schemas/office/excel/STObjectType;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lorg/apache/xmlbeans/XmlString;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/microsoft/schemas/office/excel/STObjectType$Enum;
    }
.end annotation


# static fields
.field public static final B0:Lcom/microsoft/schemas/office/excel/STObjectType$Enum;

.field public static final D0:Lcom/microsoft/schemas/office/excel/STObjectType$Enum;

.field public static final E0:Lcom/microsoft/schemas/office/excel/STObjectType$Enum;

.field public static final F0:Lcom/microsoft/schemas/office/excel/STObjectType$Enum;

.field public static final G0:Lcom/microsoft/schemas/office/excel/STObjectType$Enum;

.field public static final l0:Lorg/apache/xmlbeans/SchemaType;

.field public static final m0:Lcom/microsoft/schemas/office/excel/STObjectType$Enum;

.field public static final n0:Lcom/microsoft/schemas/office/excel/STObjectType$Enum;

.field public static final o0:Lcom/microsoft/schemas/office/excel/STObjectType$Enum;

.field public static final p0:Lcom/microsoft/schemas/office/excel/STObjectType$Enum;

.field public static final q0:Lcom/microsoft/schemas/office/excel/STObjectType$Enum;

.field public static final r0:Lcom/microsoft/schemas/office/excel/STObjectType$Enum;

.field public static final s0:Lcom/microsoft/schemas/office/excel/STObjectType$Enum;

.field public static final t0:Lcom/microsoft/schemas/office/excel/STObjectType$Enum;

.field public static final u0:Lcom/microsoft/schemas/office/excel/STObjectType$Enum;

.field public static final v0:Lcom/microsoft/schemas/office/excel/STObjectType$Enum;

.field public static final w0:Lcom/microsoft/schemas/office/excel/STObjectType$Enum;

.field public static final x0:Lcom/microsoft/schemas/office/excel/STObjectType$Enum;

.field public static final y0:Lcom/microsoft/schemas/office/excel/STObjectType$Enum;

.field public static final z0:Lcom/microsoft/schemas/office/excel/STObjectType$Enum;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-class v0, Lcom/microsoft/schemas/office/excel/STObjectType;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    const-string v1, "schemaorg_apache_xmlbeans.system.sF1327CCA741569E70F9CA8C9AF9B44B2"

    invoke-static {v0, v1}, Lorg/apache/xmlbeans/XmlBeans;->typeSystemForClassLoader(Ljava/lang/ClassLoader;Ljava/lang/String;)Lorg/apache/xmlbeans/SchemaTypeSystem;

    move-result-object v0

    const-string v1, "stobjecttype97a7type"

    invoke-interface {v0, v1}, Lorg/apache/xmlbeans/SchemaTypeSystem;->resolveHandle(Ljava/lang/String;)Lorg/apache/xmlbeans/SchemaComponent;

    move-result-object v0

    check-cast v0, Lorg/apache/xmlbeans/SchemaType;

    sput-object v0, Lcom/microsoft/schemas/office/excel/STObjectType;->l0:Lorg/apache/xmlbeans/SchemaType;

    const-string v0, "Button"

    invoke-static {v0}, Lcom/microsoft/schemas/office/excel/STObjectType$Enum;->forString(Ljava/lang/String;)Lcom/microsoft/schemas/office/excel/STObjectType$Enum;

    move-result-object v0

    sput-object v0, Lcom/microsoft/schemas/office/excel/STObjectType;->m0:Lcom/microsoft/schemas/office/excel/STObjectType$Enum;

    const-string v0, "Checkbox"

    invoke-static {v0}, Lcom/microsoft/schemas/office/excel/STObjectType$Enum;->forString(Ljava/lang/String;)Lcom/microsoft/schemas/office/excel/STObjectType$Enum;

    move-result-object v0

    sput-object v0, Lcom/microsoft/schemas/office/excel/STObjectType;->n0:Lcom/microsoft/schemas/office/excel/STObjectType$Enum;

    const-string v0, "Dialog"

    invoke-static {v0}, Lcom/microsoft/schemas/office/excel/STObjectType$Enum;->forString(Ljava/lang/String;)Lcom/microsoft/schemas/office/excel/STObjectType$Enum;

    move-result-object v0

    sput-object v0, Lcom/microsoft/schemas/office/excel/STObjectType;->o0:Lcom/microsoft/schemas/office/excel/STObjectType$Enum;

    const-string v0, "Drop"

    invoke-static {v0}, Lcom/microsoft/schemas/office/excel/STObjectType$Enum;->forString(Ljava/lang/String;)Lcom/microsoft/schemas/office/excel/STObjectType$Enum;

    move-result-object v0

    sput-object v0, Lcom/microsoft/schemas/office/excel/STObjectType;->p0:Lcom/microsoft/schemas/office/excel/STObjectType$Enum;

    const-string v0, "Edit"

    invoke-static {v0}, Lcom/microsoft/schemas/office/excel/STObjectType$Enum;->forString(Ljava/lang/String;)Lcom/microsoft/schemas/office/excel/STObjectType$Enum;

    move-result-object v0

    sput-object v0, Lcom/microsoft/schemas/office/excel/STObjectType;->q0:Lcom/microsoft/schemas/office/excel/STObjectType$Enum;

    const-string v0, "GBox"

    invoke-static {v0}, Lcom/microsoft/schemas/office/excel/STObjectType$Enum;->forString(Ljava/lang/String;)Lcom/microsoft/schemas/office/excel/STObjectType$Enum;

    move-result-object v0

    sput-object v0, Lcom/microsoft/schemas/office/excel/STObjectType;->r0:Lcom/microsoft/schemas/office/excel/STObjectType$Enum;

    const-string v0, "Label"

    invoke-static {v0}, Lcom/microsoft/schemas/office/excel/STObjectType$Enum;->forString(Ljava/lang/String;)Lcom/microsoft/schemas/office/excel/STObjectType$Enum;

    move-result-object v0

    sput-object v0, Lcom/microsoft/schemas/office/excel/STObjectType;->s0:Lcom/microsoft/schemas/office/excel/STObjectType$Enum;

    const-string v0, "LineA"

    invoke-static {v0}, Lcom/microsoft/schemas/office/excel/STObjectType$Enum;->forString(Ljava/lang/String;)Lcom/microsoft/schemas/office/excel/STObjectType$Enum;

    move-result-object v0

    sput-object v0, Lcom/microsoft/schemas/office/excel/STObjectType;->t0:Lcom/microsoft/schemas/office/excel/STObjectType$Enum;

    const-string v0, "List"

    invoke-static {v0}, Lcom/microsoft/schemas/office/excel/STObjectType$Enum;->forString(Ljava/lang/String;)Lcom/microsoft/schemas/office/excel/STObjectType$Enum;

    move-result-object v0

    sput-object v0, Lcom/microsoft/schemas/office/excel/STObjectType;->u0:Lcom/microsoft/schemas/office/excel/STObjectType$Enum;

    const-string v0, "Movie"

    invoke-static {v0}, Lcom/microsoft/schemas/office/excel/STObjectType$Enum;->forString(Ljava/lang/String;)Lcom/microsoft/schemas/office/excel/STObjectType$Enum;

    move-result-object v0

    sput-object v0, Lcom/microsoft/schemas/office/excel/STObjectType;->v0:Lcom/microsoft/schemas/office/excel/STObjectType$Enum;

    const-string v0, "Note"

    invoke-static {v0}, Lcom/microsoft/schemas/office/excel/STObjectType$Enum;->forString(Ljava/lang/String;)Lcom/microsoft/schemas/office/excel/STObjectType$Enum;

    move-result-object v0

    sput-object v0, Lcom/microsoft/schemas/office/excel/STObjectType;->w0:Lcom/microsoft/schemas/office/excel/STObjectType$Enum;

    const-string v0, "Pict"

    invoke-static {v0}, Lcom/microsoft/schemas/office/excel/STObjectType$Enum;->forString(Ljava/lang/String;)Lcom/microsoft/schemas/office/excel/STObjectType$Enum;

    move-result-object v0

    sput-object v0, Lcom/microsoft/schemas/office/excel/STObjectType;->x0:Lcom/microsoft/schemas/office/excel/STObjectType$Enum;

    const-string v0, "Radio"

    invoke-static {v0}, Lcom/microsoft/schemas/office/excel/STObjectType$Enum;->forString(Ljava/lang/String;)Lcom/microsoft/schemas/office/excel/STObjectType$Enum;

    move-result-object v0

    sput-object v0, Lcom/microsoft/schemas/office/excel/STObjectType;->y0:Lcom/microsoft/schemas/office/excel/STObjectType$Enum;

    const-string v0, "RectA"

    invoke-static {v0}, Lcom/microsoft/schemas/office/excel/STObjectType$Enum;->forString(Ljava/lang/String;)Lcom/microsoft/schemas/office/excel/STObjectType$Enum;

    move-result-object v0

    sput-object v0, Lcom/microsoft/schemas/office/excel/STObjectType;->z0:Lcom/microsoft/schemas/office/excel/STObjectType$Enum;

    const-string v0, "Scroll"

    invoke-static {v0}, Lcom/microsoft/schemas/office/excel/STObjectType$Enum;->forString(Ljava/lang/String;)Lcom/microsoft/schemas/office/excel/STObjectType$Enum;

    move-result-object v0

    sput-object v0, Lcom/microsoft/schemas/office/excel/STObjectType;->B0:Lcom/microsoft/schemas/office/excel/STObjectType$Enum;

    const-string v0, "Spin"

    invoke-static {v0}, Lcom/microsoft/schemas/office/excel/STObjectType$Enum;->forString(Ljava/lang/String;)Lcom/microsoft/schemas/office/excel/STObjectType$Enum;

    move-result-object v0

    sput-object v0, Lcom/microsoft/schemas/office/excel/STObjectType;->D0:Lcom/microsoft/schemas/office/excel/STObjectType$Enum;

    const-string v0, "Shape"

    invoke-static {v0}, Lcom/microsoft/schemas/office/excel/STObjectType$Enum;->forString(Ljava/lang/String;)Lcom/microsoft/schemas/office/excel/STObjectType$Enum;

    move-result-object v0

    sput-object v0, Lcom/microsoft/schemas/office/excel/STObjectType;->E0:Lcom/microsoft/schemas/office/excel/STObjectType$Enum;

    const-string v0, "Group"

    invoke-static {v0}, Lcom/microsoft/schemas/office/excel/STObjectType$Enum;->forString(Ljava/lang/String;)Lcom/microsoft/schemas/office/excel/STObjectType$Enum;

    move-result-object v0

    sput-object v0, Lcom/microsoft/schemas/office/excel/STObjectType;->F0:Lcom/microsoft/schemas/office/excel/STObjectType$Enum;

    const-string v0, "Rect"

    invoke-static {v0}, Lcom/microsoft/schemas/office/excel/STObjectType$Enum;->forString(Ljava/lang/String;)Lcom/microsoft/schemas/office/excel/STObjectType$Enum;

    move-result-object v0

    sput-object v0, Lcom/microsoft/schemas/office/excel/STObjectType;->G0:Lcom/microsoft/schemas/office/excel/STObjectType$Enum;

    return-void
.end method

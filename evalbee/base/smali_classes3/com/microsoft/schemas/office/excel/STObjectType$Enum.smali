.class public final Lcom/microsoft/schemas/office/excel/STObjectType$Enum;
.super Lorg/apache/xmlbeans/StringEnumAbstractBase;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/microsoft/schemas/office/excel/STObjectType;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Enum"
.end annotation


# static fields
.field static final INT_BUTTON:I = 0x1

.field static final INT_CHECKBOX:I = 0x2

.field static final INT_DIALOG:I = 0x3

.field static final INT_DROP:I = 0x4

.field static final INT_EDIT:I = 0x5

.field static final INT_GROUP:I = 0x12

.field static final INT_G_BOX:I = 0x6

.field static final INT_LABEL:I = 0x7

.field static final INT_LINE_A:I = 0x8

.field static final INT_LIST:I = 0x9

.field static final INT_MOVIE:I = 0xa

.field static final INT_NOTE:I = 0xb

.field static final INT_PICT:I = 0xc

.field static final INT_RADIO:I = 0xd

.field static final INT_RECT:I = 0x13

.field static final INT_RECT_A:I = 0xe

.field static final INT_SCROLL:I = 0xf

.field static final INT_SHAPE:I = 0x11

.field static final INT_SPIN:I = 0x10

.field private static final serialVersionUID:J = 0x1L

.field public static final table:Lorg/apache/xmlbeans/StringEnumAbstractBase$Table;


# direct methods
.method public static constructor <clinit>()V
    .locals 23

    new-instance v0, Lorg/apache/xmlbeans/StringEnumAbstractBase$Table;

    new-instance v2, Lcom/microsoft/schemas/office/excel/STObjectType$Enum;

    move-object v1, v2

    const-string v3, "Button"

    const/4 v4, 0x1

    invoke-direct {v2, v3, v4}, Lcom/microsoft/schemas/office/excel/STObjectType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v3, Lcom/microsoft/schemas/office/excel/STObjectType$Enum;

    move-object v2, v3

    const-string v4, "Checkbox"

    const/4 v5, 0x2

    invoke-direct {v3, v4, v5}, Lcom/microsoft/schemas/office/excel/STObjectType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v4, Lcom/microsoft/schemas/office/excel/STObjectType$Enum;

    move-object v3, v4

    const-string v5, "Dialog"

    const/4 v6, 0x3

    invoke-direct {v4, v5, v6}, Lcom/microsoft/schemas/office/excel/STObjectType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v5, Lcom/microsoft/schemas/office/excel/STObjectType$Enum;

    move-object v4, v5

    const-string v6, "Drop"

    const/4 v7, 0x4

    invoke-direct {v5, v6, v7}, Lcom/microsoft/schemas/office/excel/STObjectType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v6, Lcom/microsoft/schemas/office/excel/STObjectType$Enum;

    move-object v5, v6

    const-string v7, "Edit"

    const/4 v8, 0x5

    invoke-direct {v6, v7, v8}, Lcom/microsoft/schemas/office/excel/STObjectType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v7, Lcom/microsoft/schemas/office/excel/STObjectType$Enum;

    move-object v6, v7

    const-string v8, "GBox"

    const/4 v9, 0x6

    invoke-direct {v7, v8, v9}, Lcom/microsoft/schemas/office/excel/STObjectType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v8, Lcom/microsoft/schemas/office/excel/STObjectType$Enum;

    move-object v7, v8

    const-string v9, "Label"

    const/4 v10, 0x7

    invoke-direct {v8, v9, v10}, Lcom/microsoft/schemas/office/excel/STObjectType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v9, Lcom/microsoft/schemas/office/excel/STObjectType$Enum;

    move-object v8, v9

    const-string v10, "LineA"

    const/16 v11, 0x8

    invoke-direct {v9, v10, v11}, Lcom/microsoft/schemas/office/excel/STObjectType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v10, Lcom/microsoft/schemas/office/excel/STObjectType$Enum;

    move-object v9, v10

    const-string v11, "List"

    const/16 v12, 0x9

    invoke-direct {v10, v11, v12}, Lcom/microsoft/schemas/office/excel/STObjectType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v11, Lcom/microsoft/schemas/office/excel/STObjectType$Enum;

    move-object v10, v11

    const-string v12, "Movie"

    const/16 v13, 0xa

    invoke-direct {v11, v12, v13}, Lcom/microsoft/schemas/office/excel/STObjectType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v12, Lcom/microsoft/schemas/office/excel/STObjectType$Enum;

    move-object v11, v12

    const-string v13, "Note"

    const/16 v14, 0xb

    invoke-direct {v12, v13, v14}, Lcom/microsoft/schemas/office/excel/STObjectType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v13, Lcom/microsoft/schemas/office/excel/STObjectType$Enum;

    move-object v12, v13

    const-string v14, "Pict"

    const/16 v15, 0xc

    invoke-direct {v13, v14, v15}, Lcom/microsoft/schemas/office/excel/STObjectType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v14, Lcom/microsoft/schemas/office/excel/STObjectType$Enum;

    move-object v13, v14

    const-string v15, "Radio"

    move-object/from16 v20, v0

    const/16 v0, 0xd

    invoke-direct {v14, v15, v0}, Lcom/microsoft/schemas/office/excel/STObjectType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lcom/microsoft/schemas/office/excel/STObjectType$Enum;

    move-object v14, v0

    const-string v15, "RectA"

    move-object/from16 v21, v1

    const/16 v1, 0xe

    invoke-direct {v0, v15, v1}, Lcom/microsoft/schemas/office/excel/STObjectType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lcom/microsoft/schemas/office/excel/STObjectType$Enum;

    move-object v15, v0

    const-string v1, "Scroll"

    move-object/from16 v22, v2

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/microsoft/schemas/office/excel/STObjectType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lcom/microsoft/schemas/office/excel/STObjectType$Enum;

    move-object/from16 v16, v0

    const-string v1, "Spin"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/microsoft/schemas/office/excel/STObjectType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lcom/microsoft/schemas/office/excel/STObjectType$Enum;

    move-object/from16 v17, v0

    const-string v1, "Shape"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lcom/microsoft/schemas/office/excel/STObjectType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lcom/microsoft/schemas/office/excel/STObjectType$Enum;

    move-object/from16 v18, v0

    const-string v1, "Group"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, Lcom/microsoft/schemas/office/excel/STObjectType$Enum;-><init>(Ljava/lang/String;I)V

    new-instance v0, Lcom/microsoft/schemas/office/excel/STObjectType$Enum;

    move-object/from16 v19, v0

    const-string v1, "Rect"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, Lcom/microsoft/schemas/office/excel/STObjectType$Enum;-><init>(Ljava/lang/String;I)V

    move-object/from16 v1, v21

    move-object/from16 v2, v22

    filled-new-array/range {v1 .. v19}, [Lcom/microsoft/schemas/office/excel/STObjectType$Enum;

    move-result-object v0

    move-object/from16 v1, v20

    invoke-direct {v1, v0}, Lorg/apache/xmlbeans/StringEnumAbstractBase$Table;-><init>([Lorg/apache/xmlbeans/StringEnumAbstractBase;)V

    sput-object v1, Lcom/microsoft/schemas/office/excel/STObjectType$Enum;->table:Lorg/apache/xmlbeans/StringEnumAbstractBase$Table;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lorg/apache/xmlbeans/StringEnumAbstractBase;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static forInt(I)Lcom/microsoft/schemas/office/excel/STObjectType$Enum;
    .locals 1

    sget-object v0, Lcom/microsoft/schemas/office/excel/STObjectType$Enum;->table:Lorg/apache/xmlbeans/StringEnumAbstractBase$Table;

    invoke-virtual {v0, p0}, Lorg/apache/xmlbeans/StringEnumAbstractBase$Table;->forInt(I)Lorg/apache/xmlbeans/StringEnumAbstractBase;

    move-result-object p0

    check-cast p0, Lcom/microsoft/schemas/office/excel/STObjectType$Enum;

    return-object p0
.end method

.method public static forString(Ljava/lang/String;)Lcom/microsoft/schemas/office/excel/STObjectType$Enum;
    .locals 1

    sget-object v0, Lcom/microsoft/schemas/office/excel/STObjectType$Enum;->table:Lorg/apache/xmlbeans/StringEnumAbstractBase$Table;

    invoke-virtual {v0, p0}, Lorg/apache/xmlbeans/StringEnumAbstractBase$Table;->forString(Ljava/lang/String;)Lorg/apache/xmlbeans/StringEnumAbstractBase;

    move-result-object p0

    check-cast p0, Lcom/microsoft/schemas/office/excel/STObjectType$Enum;

    return-object p0
.end method

.method private readResolve()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lorg/apache/xmlbeans/StringEnumAbstractBase;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/microsoft/schemas/office/excel/STObjectType$Enum;->forInt(I)Lcom/microsoft/schemas/office/excel/STObjectType$Enum;

    move-result-object v0

    return-object v0
.end method

.class public final Lcom/microsoft/schemas/office/excel/impl/CTClientDataImpl$a;
.super Ljava/util/AbstractList;
.source "SourceFile"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/microsoft/schemas/office/excel/impl/CTClientDataImpl;->getVisibleList()Ljava/util/List;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x11
    name = "a"
.end annotation


# instance fields
.field public final synthetic a:Lcom/microsoft/schemas/office/excel/impl/CTClientDataImpl;


# direct methods
.method public constructor <init>(Lcom/microsoft/schemas/office/excel/impl/CTClientDataImpl;)V
    .locals 0

    iput-object p1, p0, Lcom/microsoft/schemas/office/excel/impl/CTClientDataImpl$a;->a:Lcom/microsoft/schemas/office/excel/impl/CTClientDataImpl;

    invoke-direct {p0}, Ljava/util/AbstractList;-><init>()V

    return-void
.end method


# virtual methods
.method public a(ILcom/microsoft/schemas/office/excel/STTrueFalseBlank$Enum;)V
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/microsoft/schemas/office/excel/impl/CTClientDataImpl$a;->a:Lcom/microsoft/schemas/office/excel/impl/CTClientDataImpl;

    invoke-virtual {v0, p1, p2}, Lcom/microsoft/schemas/office/excel/impl/CTClientDataImpl;->insertVisible(ILcom/microsoft/schemas/office/excel/STTrueFalseBlank$Enum;)V

    return-void
.end method

.method public bridge synthetic add(ILjava/lang/Object;)V
    .locals 0

    check-cast p2, Lcom/microsoft/schemas/office/excel/STTrueFalseBlank$Enum;

    invoke-virtual {p0, p1, p2}, Lcom/microsoft/schemas/office/excel/impl/CTClientDataImpl$a;->a(ILcom/microsoft/schemas/office/excel/STTrueFalseBlank$Enum;)V

    return-void
.end method

.method public b(I)Lcom/microsoft/schemas/office/excel/STTrueFalseBlank$Enum;
    .locals 1

    .line 1
    iget-object v0, p0, Lcom/microsoft/schemas/office/excel/impl/CTClientDataImpl$a;->a:Lcom/microsoft/schemas/office/excel/impl/CTClientDataImpl;

    invoke-virtual {v0, p1}, Lcom/microsoft/schemas/office/excel/impl/CTClientDataImpl;->getVisibleArray(I)Lcom/microsoft/schemas/office/excel/STTrueFalseBlank$Enum;

    move-result-object p1

    return-object p1
.end method

.method public c(I)Lcom/microsoft/schemas/office/excel/STTrueFalseBlank$Enum;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/microsoft/schemas/office/excel/impl/CTClientDataImpl$a;->a:Lcom/microsoft/schemas/office/excel/impl/CTClientDataImpl;

    invoke-virtual {v0, p1}, Lcom/microsoft/schemas/office/excel/impl/CTClientDataImpl;->getVisibleArray(I)Lcom/microsoft/schemas/office/excel/STTrueFalseBlank$Enum;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/schemas/office/excel/impl/CTClientDataImpl$a;->a:Lcom/microsoft/schemas/office/excel/impl/CTClientDataImpl;

    invoke-virtual {v1, p1}, Lcom/microsoft/schemas/office/excel/impl/CTClientDataImpl;->removeVisible(I)V

    return-object v0
.end method

.method public g(ILcom/microsoft/schemas/office/excel/STTrueFalseBlank$Enum;)Lcom/microsoft/schemas/office/excel/STTrueFalseBlank$Enum;
    .locals 2

    .line 1
    iget-object v0, p0, Lcom/microsoft/schemas/office/excel/impl/CTClientDataImpl$a;->a:Lcom/microsoft/schemas/office/excel/impl/CTClientDataImpl;

    invoke-virtual {v0, p1}, Lcom/microsoft/schemas/office/excel/impl/CTClientDataImpl;->getVisibleArray(I)Lcom/microsoft/schemas/office/excel/STTrueFalseBlank$Enum;

    move-result-object v0

    iget-object v1, p0, Lcom/microsoft/schemas/office/excel/impl/CTClientDataImpl$a;->a:Lcom/microsoft/schemas/office/excel/impl/CTClientDataImpl;

    invoke-virtual {v1, p1, p2}, Lcom/microsoft/schemas/office/excel/impl/CTClientDataImpl;->setVisibleArray(ILcom/microsoft/schemas/office/excel/STTrueFalseBlank$Enum;)V

    return-object v0
.end method

.method public bridge synthetic get(I)Ljava/lang/Object;
    .locals 0

    invoke-virtual {p0, p1}, Lcom/microsoft/schemas/office/excel/impl/CTClientDataImpl$a;->b(I)Lcom/microsoft/schemas/office/excel/STTrueFalseBlank$Enum;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic remove(I)Ljava/lang/Object;
    .locals 0

    invoke-virtual {p0, p1}, Lcom/microsoft/schemas/office/excel/impl/CTClientDataImpl$a;->c(I)Lcom/microsoft/schemas/office/excel/STTrueFalseBlank$Enum;

    move-result-object p1

    return-object p1
.end method

.method public bridge synthetic set(ILjava/lang/Object;)Ljava/lang/Object;
    .locals 0

    check-cast p2, Lcom/microsoft/schemas/office/excel/STTrueFalseBlank$Enum;

    invoke-virtual {p0, p1, p2}, Lcom/microsoft/schemas/office/excel/impl/CTClientDataImpl$a;->g(ILcom/microsoft/schemas/office/excel/STTrueFalseBlank$Enum;)Lcom/microsoft/schemas/office/excel/STTrueFalseBlank$Enum;

    move-result-object p1

    return-object p1
.end method

.method public size()I
    .locals 1

    iget-object v0, p0, Lcom/microsoft/schemas/office/excel/impl/CTClientDataImpl$a;->a:Lcom/microsoft/schemas/office/excel/impl/CTClientDataImpl;

    invoke-virtual {v0}, Lcom/microsoft/schemas/office/excel/impl/CTClientDataImpl;->sizeOfVisibleArray()I

    move-result v0

    return v0
.end method
